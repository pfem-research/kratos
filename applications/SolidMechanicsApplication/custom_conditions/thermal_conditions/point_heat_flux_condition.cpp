//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:         JMCarbonell $
//   Maintained by:       $Maintainer:                JMC $
//   Date:                $Date:            November 2020 $
//
//

// System includes

// External includes

// Project includes
#include "custom_conditions/thermal_conditions/point_heat_flux_condition.hpp"

#include "solid_mechanics_application_variables.h"

namespace Kratos
{

//***********************************************************************************
//***********************************************************************************
PointHeatFluxCondition::PointHeatFluxCondition(IndexType NewId, GeometryType::Pointer pGeometry)
    : HeatFluxCondition(NewId, pGeometry)
{
}

//***********************************************************************************
//***********************************************************************************
PointHeatFluxCondition::PointHeatFluxCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : HeatFluxCondition(NewId, pGeometry, pProperties)
{
  mThisIntegrationMethod = GetGeometry().GetDefaultIntegrationMethod();
}

//************************************************************************************
//************************************************************************************
PointHeatFluxCondition::PointHeatFluxCondition(PointHeatFluxCondition const &rOther)
    : HeatFluxCondition(rOther)
{
}

//***********************************************************************************
//***********************************************************************************
Condition::Pointer PointHeatFluxCondition::Create(
    IndexType NewId,
    NodesArrayType const &ThisNodes,
    PropertiesType::Pointer pProperties) const
{
  return Kratos::make_intrusive<PointHeatFluxCondition>(NewId, GetGeometry().Create(ThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************

Condition::Pointer PointHeatFluxCondition::Clone(IndexType NewId, NodesArrayType const &rThisNodes) const
{
  PointHeatFluxCondition NewCondition(NewId, GetGeometry().Create(rThisNodes), pGetProperties());

  NewCondition.SetData(this->GetData());
  NewCondition.SetFlags(this->GetFlags());

  return Kratos::make_intrusive<PointHeatFluxCondition>(NewCondition);
}

//***********************************************************************************
//***********************************************************************************
PointHeatFluxCondition::~PointHeatFluxCondition()
{
}

//************************************************************************************
//************************************************************************************

void PointHeatFluxCondition::InitializeConditionVariables(ConditionVariables &rVariables, const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  const SizeType number_of_nodes = GetGeometry().size();
  const unsigned int local_dimension = GetGeometry().LocalSpaceDimension();
  const SizeType &dimension = GetGeometry().WorkingSpaceDimension();

  rVariables.Initialize(dimension, local_dimension, number_of_nodes);

  //Only one node:
  rVariables.N[0] = 1.0;

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void PointHeatFluxCondition::CalculateExternalHeatFlux(ConditionVariables &rVariables)
{
  KRATOS_TRY

  const SizeType number_of_nodes = GetGeometry().size();

  //FLUX CONDITION:
  rVariables.ExternalScalarValue = 0.0;

  //FORCE CONDITION:
  //defined on condition
  if (this->Has(HEAT_FLUX))
  {
    double &HeatFlux = this->GetValue(HEAT_FLUX);
    for (SizeType i = 0; i < number_of_nodes; i++)
      rVariables.ExternalScalarValue += rVariables.N[i] * HeatFlux;
  }

  //defined on condition nodes
  if (this->Has(HEAT_FLUX_VECTOR))
  {
    Vector &HeatFluxVector = this->GetValue(HEAT_FLUX_VECTOR);
    for (SizeType i = 0; i < number_of_nodes; i++)
    {
      rVariables.ExternalScalarValue += rVariables.N[i] * HeatFluxVector[i];
    }
  }

  //defined on geometry nodes
  for (SizeType i = 0; i < number_of_nodes; i++)
  {
    if (GetGeometry()[i].SolutionStepsDataHas(HEAT_FLUX))
      rVariables.ExternalScalarValue += rVariables.N[i] * GetGeometry()[i].FastGetSolutionStepValue(HEAT_FLUX);
  }

  KRATOS_CATCH("")
}

//*********************************COMPUTE KINEMATICS*********************************
//************************************************************************************

void PointHeatFluxCondition::CalculateKinematics(ConditionVariables &rVariables,
                                             const double &rPointNumber)
{
  KRATOS_TRY

  rVariables.Jacobian = 1.0;

  this->CalculateExternalHeatFlux(rVariables);

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void PointHeatFluxCondition::CalculateConditionSystem(LocalSystemComponents &rLocalSystem,
                                                  const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  //create and initialize condition variables:
  ConditionVariables Variables;
  this->InitializeConditionVariables(Variables, rCurrentProcessInfo);

  //reading integration points
  unsigned int PointNumber = 0;

  //compute element kinematics B, F, DN_DX ...
  this->CalculateKinematics(Variables, PointNumber);

  //calculating weights for integration on the "reference configuration"
  double IntegrationWeight = 1;
  IntegrationWeight = this->CalculateIntegrationWeight(Variables, IntegrationWeight);

  if (rLocalSystem.CalculationFlags.Is(BoundaryCondition::COMPUTE_LHS_MATRIX)) //calculation of the matrix is required
  {
    //contributions to stiffness matrix calculated on the reference config
    this->CalculateAndAddLHS(rLocalSystem, Variables, IntegrationWeight);
  }

  if (rLocalSystem.CalculationFlags.Is(BoundaryCondition::COMPUTE_RHS_VECTOR)) //calculation of the vector is required
  {

    this->CalculateAndAddRHS(rLocalSystem, Variables, IntegrationWeight);
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void PointHeatFluxCondition::CalculateAndAddKuug(MatrixType &rLeftHandSideMatrix,
                                                 ConditionVariables &rVariables,
                                                 double &rIntegrationWeight)

{
  KRATOS_TRY

  HeatFluxCondition::CalculateAndAddKuug(rLeftHandSideMatrix, rVariables, rIntegrationWeight);

  const SizeType number_of_nodes = GetGeometry().PointsNumber();

  for (SizeType i = 0; i < number_of_nodes; i++)
  {
    //define the flux via temperature transfer from body to room
    if (GetGeometry()[i].SolutionStepsDataHas(HEAT_TRANSFER_COEFFICIENT))
    {
      rLeftHandSideMatrix(i,i) -= GetGeometry()[i].FastGetSolutionStepValue(HEAT_TRANSFER_COEFFICIENT) * rIntegrationWeight;
    }
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

int PointHeatFluxCondition::Check(const ProcessInfo &rCurrentProcessInfo) const
{
  KRATOS_TRY

  // Perform base condition checks
  int ErrorCode = 0;
  ErrorCode = HeatFluxCondition::Check(rCurrentProcessInfo);

  return ErrorCode;

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void PointHeatFluxCondition::save(Serializer &rSerializer) const
{
  KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, HeatFluxCondition)
}

void PointHeatFluxCondition::load(Serializer &rSerializer)
{
  KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, HeatFluxCondition)
}

} // Namespace Kratos.
