//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:         JMCarbonell $
//   Maintained by:       $Maintainer:                JMC $
//   Date:                $Date:            November 2020 $
//
//

// System includes

// External includes

// Project includes
#include "custom_conditions/thermal_conditions/line_heat_flux_condition.hpp"

#include "solid_mechanics_application_variables.h"

namespace Kratos
{

//***********************************************************************************
//***********************************************************************************
LineHeatFluxCondition::LineHeatFluxCondition(IndexType NewId, GeometryType::Pointer pGeometry)
    : HeatFluxCondition(NewId, pGeometry)
{
}

//***********************************************************************************
//***********************************************************************************
LineHeatFluxCondition::LineHeatFluxCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : HeatFluxCondition(NewId, pGeometry, pProperties)
{
}

//************************************************************************************
//************************************************************************************
LineHeatFluxCondition::LineHeatFluxCondition(LineHeatFluxCondition const &rOther)
    : HeatFluxCondition(rOther)
{
}

//***********************************************************************************
//***********************************************************************************
Condition::Pointer LineHeatFluxCondition::Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const
{
  return Kratos::make_intrusive<LineHeatFluxCondition>(NewId, GetGeometry().Create(ThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************
Condition::Pointer LineHeatFluxCondition::Clone(IndexType NewId, NodesArrayType const &rThisNodes) const
{

  LineHeatFluxCondition NewCondition(NewId, GetGeometry().Create(rThisNodes), pGetProperties());

  NewCondition.SetData(this->GetData());
  NewCondition.SetFlags(this->GetFlags());

  return Kratos::make_intrusive<LineHeatFluxCondition>(NewCondition);
}

//***********************************************************************************
//***********************************************************************************
LineHeatFluxCondition::~LineHeatFluxCondition()
{
}

//************************************************************************************
//************************************************************************************

void LineHeatFluxCondition::InitializeConditionVariables(ConditionVariables &rVariables, const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  HeatFluxCondition::InitializeConditionVariables(rVariables, rCurrentProcessInfo);

  //calculating the current jacobian from cartesian coordinates to parent coordinates for all integration points [dx_n+1/d£]
  rVariables.j = GetGeometry().Jacobian(rVariables.j, mThisIntegrationMethod);

  //Calculate Total Delta Position
  ElementUtilities::CalculateTotalDeltaPosition(rVariables.DeltaPosition, this->GetGeometry());

  //calculating the reference jacobian from cartesian coordinates to parent coordinates for all integration points [dx_0/d£]
  rVariables.J = GetGeometry().Jacobian(rVariables.J, mThisIntegrationMethod, rVariables.DeltaPosition);

  KRATOS_CATCH("")
}

//*********************************COMPUTE KINEMATICS*********************************
//************************************************************************************

void LineHeatFluxCondition::CalculateKinematics(ConditionVariables &rVariables,
                                               const double &rPointNumber)
{
  KRATOS_TRY

  const SizeType &dimension = GetGeometry().WorkingSpaceDimension();

  //Get the parent coodinates derivative [dN/d£]
  const GeometryType::ShapeFunctionsGradientsType &DN_De = rVariables.GetShapeFunctionsGradients();

  //Get the shape functions for the order of the integration method [N]
  const Matrix &Ncontainer = rVariables.GetShapeFunctions();

  //UL computation

  //get first vector of the plane
  rVariables.Tangent1[0] = rVariables.j[rPointNumber](0, 0); // x_1,e
  rVariables.Tangent1[1] = rVariables.j[rPointNumber](1, 0); // x_2,e

  rVariables.Normal[0] = -rVariables.j[rPointNumber](1, 0); //-x_2,e
  rVariables.Normal[1] = rVariables.j[rPointNumber](0, 0);  // x_1,e

  if (dimension == 3)
  {
    rVariables.Tangent1[2] = rVariables.j[rPointNumber](2, 0); // x_3,e
    rVariables.Normal[2] = rVariables.j[rPointNumber](2, 0);   // x_3,e
  }

  //Jacobian to the deformed configuration
  rVariables.Jacobian = norm_2(rVariables.Normal);

  //std::cout<< " jacobian "<<rVariables.Jacobian<<std::endl;

  //Compute the unit normal and weighted tangents
  if (rVariables.Jacobian > 0)
  {
    rVariables.Normal /= rVariables.Jacobian;
    rVariables.Tangent1 /= rVariables.Jacobian;
  }

  //Jacobian to the last known configuration
  rVariables.Tangent2[0] = rVariables.J[rPointNumber](0, 0); // x_1,e
  rVariables.Tangent2[1] = rVariables.J[rPointNumber](1, 0); // x_2,e
  if (dimension == 3)
  {
    rVariables.Tangent2[2] = rVariables.J[rPointNumber](2, 0); // x_3,e
  }

  rVariables.Jacobian = norm_2(rVariables.Tangent2);

  //Set Shape Functions Values for this integration point
  noalias(rVariables.N) = matrix_row<const Matrix>(Ncontainer, rPointNumber);

  //Set Shape Functions Derivatives [dN/d£] for this integration point
  rVariables.DN_De = DN_De[rPointNumber];

  //Get geometry size
  rVariables.GeometrySize = GetGeometry().Length();

  //Get external load
  this->CalculateExternalHeatFlux(rVariables);

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void LineHeatFluxCondition::CalculateExternalHeatFlux(ConditionVariables &rVariables)
{

  KRATOS_TRY

  const SizeType number_of_nodes = GetGeometry().PointsNumber();

  //FLUX CONDITION:
  rVariables.ExternalScalarValue = 0.0;

  //defined on condition
  if (this->Has(HEAT_FLUX))
  {
    double &HeatFlux = this->GetValue(HEAT_FLUX);
    for (SizeType i = 0; i < number_of_nodes; i++)
      rVariables.ExternalScalarValue += rVariables.N[i] * HeatFlux;
  }

  //defined on condition nodes
  if (this->Has(HEAT_FLUX_VECTOR))
  {
    Vector &HeatFluxVector = this->GetValue(HEAT_FLUX_VECTOR);
    for (SizeType i = 0; i < number_of_nodes; i++)
    {
      rVariables.ExternalScalarValue += rVariables.N[i] * HeatFluxVector[i];
    }
  }

  //defined on geometry nodes
  for (SizeType i = 0; i < number_of_nodes; i++)
  {
    if (GetGeometry()[i].SolutionStepsDataHas(HEAT_FLUX))
      rVariables.ExternalScalarValue += rVariables.N[i] * GetGeometry()[i].FastGetSolutionStepValue(HEAT_FLUX);
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void LineHeatFluxCondition::CalculateAndAddKuug(MatrixType &rLeftHandSideMatrix,
                                                ConditionVariables &rVariables,
                                                double &rIntegrationWeight)

{
  KRATOS_TRY

  HeatFluxCondition::CalculateAndAddKuug(rLeftHandSideMatrix, rVariables, rIntegrationWeight);

  const SizeType &dimension = GetGeometry().WorkingSpaceDimension();
  if (dimension == 2)
  {
    //HeatFluxCondition::CalculateAndAddKuug(rLeftHandSideMatrix, rVariables, rIntegrationWeight);

    const SizeType number_of_nodes = GetGeometry().PointsNumber();

    //Lumped Mass Matrix
    Vector LumpFact(number_of_nodes);
    noalias(LumpFact) = ZeroVector(number_of_nodes);

    LumpFact = GetGeometry().LumpingFactors(LumpFact);

    for (unsigned int i = 0; i < number_of_nodes; i++)
    {
      if (GetGeometry()[i].SolutionStepsDataHas(HEAT_TRANSFER_COEFFICIENT))
      {
        rLeftHandSideMatrix(i,i) += LumpFact[i] * GetGeometry()[i].FastGetSolutionStepValue(HEAT_TRANSFER_COEFFICIENT) * rIntegrationWeight;
      }
    }

  }
  else
  { //3D line flux not considered here
    unsigned int MatSize = this->GetDofsSize();
    if (rLeftHandSideMatrix.size1() != MatSize)
    {
      rLeftHandSideMatrix.resize(MatSize, MatSize, false);
      noalias(rLeftHandSideMatrix) = ZeroMatrix(MatSize, MatSize);
    }
  }

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

int LineHeatFluxCondition::Check(const ProcessInfo &rCurrentProcessInfo) const
{
  KRATOS_TRY

  // Perform base condition checks
  int ErrorCode = 0;
  ErrorCode = HeatFluxCondition::Check(rCurrentProcessInfo);

  return ErrorCode;

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void LineHeatFluxCondition::save(Serializer &rSerializer) const
{
  KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, HeatFluxCondition)
}

void LineHeatFluxCondition::load(Serializer &rSerializer)
{
  KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, HeatFluxCondition)
}

} // Namespace Kratos.
