//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:         JMCarbonell $
//   Maintained by:       $Maintainer:                JMC $
//   Date:                $Date:            November 2020 $
//
//

// System includes

// External includes

// Project includes
#include "custom_conditions/thermal_conditions/axisymmetric_point_heat_flux_condition.hpp"

#include "solid_mechanics_application_variables.h"

namespace Kratos
{

//***********************************************************************************
//***********************************************************************************
AxisymmetricPointHeatFluxCondition::AxisymmetricPointHeatFluxCondition(IndexType NewId, GeometryType::Pointer pGeometry)
    : PointHeatFluxCondition(NewId, pGeometry)
{
}

//***********************************************************************************
//***********************************************************************************
AxisymmetricPointHeatFluxCondition::AxisymmetricPointHeatFluxCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : PointHeatFluxCondition(NewId, pGeometry, pProperties)
{
  mThisIntegrationMethod = GetGeometry().GetDefaultIntegrationMethod();
}

//************************************************************************************
//************************************************************************************
AxisymmetricPointHeatFluxCondition::AxisymmetricPointHeatFluxCondition(AxisymmetricPointHeatFluxCondition const &rOther)
    : PointHeatFluxCondition(rOther)
{
}

//***********************************************************************************
//***********************************************************************************
Condition::Pointer AxisymmetricPointHeatFluxCondition::Create(IndexType NewId,
                                                             NodesArrayType const &ThisNodes,
                                                             PropertiesType::Pointer pProperties) const
{
  return Kratos::make_intrusive<AxisymmetricPointHeatFluxCondition>(NewId, GetGeometry().Create(ThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************

Condition::Pointer AxisymmetricPointHeatFluxCondition::Clone(IndexType NewId, NodesArrayType const &rThisNodes) const
{
  AxisymmetricPointHeatFluxCondition NewCondition(NewId, GetGeometry().Create(rThisNodes), pGetProperties());

  NewCondition.SetData(this->GetData());
  NewCondition.SetFlags(this->GetFlags());

  return Kratos::make_intrusive<AxisymmetricPointHeatFluxCondition>(NewCondition);
}

//***********************************************************************************
//***********************************************************************************
AxisymmetricPointHeatFluxCondition::~AxisymmetricPointHeatFluxCondition()
{
}

//************* COMPUTING  METHODS
//************************************************************************************
//************************************************************************************

//*********************************COMPUTE KINEMATICS*********************************
//************************************************************************************

void AxisymmetricPointHeatFluxCondition::CalculateKinematics(ConditionVariables &rVariables,
                                                            const double &rPointNumber)
{
  KRATOS_TRY

  CalculateRadius(rVariables.CurrentRadius, rVariables.ReferenceRadius);

  rVariables.Jacobian = 1.0;

  this->CalculateExternalHeatFlux(rVariables);

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void AxisymmetricPointHeatFluxCondition::CalculateRadius(double &rCurrentRadius, double &rReferenceRadius)
{

  KRATOS_TRY

  // rCurrentRadius=0;
  // rReferenceRadius=0;

  // //Displacement from the reference to the current configuration
  // array_1d<double, 3 > & CurrentDisplacement  = GetGeometry()[0].FastGetSolutionStepValue(DISPLACEMENT);
  // array_1d<double, 3 > & PreviousDisplacement = GetGeometry()[0].FastGetSolutionStepValue(DISPLACEMENT,1);
  // array_1d<double, 3 > DeltaDisplacement      = CurrentDisplacement-PreviousDisplacement;
  // array_1d<double, 3 > & CurrentPosition      = GetGeometry()[0].Coordinates();
  // array_1d<double, 3 > ReferencePosition      = CurrentPosition - DeltaDisplacement;

  // rCurrentRadius   = CurrentPosition[0];
  // rReferenceRadius = ReferencePosition[0];

  rCurrentRadius = GetGeometry()[0].X();
  rReferenceRadius = GetGeometry()[0].X0();

  KRATOS_CATCH("")
}

//***********************************************************************************
//************************************************************************************

double &AxisymmetricPointHeatFluxCondition::CalculateIntegrationWeight(ConditionVariables &rVariables, double &rIntegrationWeight)
{
  KRATOS_TRY

  rIntegrationWeight *= 2.0 * Globals::Pi * rVariables.CurrentRadius;
  return rIntegrationWeight;

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

int AxisymmetricPointHeatFluxCondition::Check(const ProcessInfo &rCurrentProcessInfo) const
{
  KRATOS_TRY

  // Perform base condition checks
  int ErrorCode = 0;
  ErrorCode = PointHeatFluxCondition::Check(rCurrentProcessInfo);

  return ErrorCode;

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void AxisymmetricPointHeatFluxCondition::save(Serializer &rSerializer) const
{
  KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, PointHeatFluxCondition)
}

void AxisymmetricPointHeatFluxCondition::load(Serializer &rSerializer)
{
  KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, PointHeatFluxCondition)
}

} // Namespace Kratos.
