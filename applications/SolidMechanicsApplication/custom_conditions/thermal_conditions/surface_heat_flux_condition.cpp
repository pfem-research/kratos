//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:         JMCarbonell $
//   Maintained by:       $Maintainer:                JMC $
//   Date:                $Date:            November 2020 $
//
//

// System includes

// External includes

// Project includes
#include "custom_conditions/thermal_conditions/surface_heat_flux_condition.hpp"

#include "solid_mechanics_application_variables.h"

namespace Kratos
{

//***********************************************************************************
//***********************************************************************************
SurfaceHeatFluxCondition::SurfaceHeatFluxCondition(IndexType NewId, GeometryType::Pointer pGeometry)
    : HeatFluxCondition(NewId, pGeometry)
{
}

//***********************************************************************************
//***********************************************************************************
SurfaceHeatFluxCondition::SurfaceHeatFluxCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : HeatFluxCondition(NewId, pGeometry, pProperties)
{
}

//************************************************************************************
//************************************************************************************
SurfaceHeatFluxCondition::SurfaceHeatFluxCondition(SurfaceHeatFluxCondition const &rOther)
    : HeatFluxCondition(rOther)
{
}

//***********************************************************************************
//***********************************************************************************
Condition::Pointer SurfaceHeatFluxCondition::Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const
{
  return Kratos::make_intrusive<SurfaceHeatFluxCondition>(NewId, GetGeometry().Create(ThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************
Condition::Pointer SurfaceHeatFluxCondition::Clone(IndexType NewId, NodesArrayType const &rThisNodes) const
{
  SurfaceHeatFluxCondition NewCondition(NewId, GetGeometry().Create(rThisNodes), pGetProperties());

  NewCondition.SetData(this->GetData());
  NewCondition.SetFlags(this->GetFlags());

  return Kratos::make_intrusive<SurfaceHeatFluxCondition>(NewCondition);
}

//***********************************************************************************
//***********************************************************************************
SurfaceHeatFluxCondition::~SurfaceHeatFluxCondition()
{
}

//************************************************************************************
//************************************************************************************

void SurfaceHeatFluxCondition::InitializeConditionVariables(ConditionVariables &rVariables, const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  HeatFluxCondition::InitializeConditionVariables(rVariables, rCurrentProcessInfo);

  //calculating the current jacobian from cartesian coordinates to parent coordinates for all integration points [dx_n+1/d£]
  rVariables.j = GetGeometry().Jacobian(rVariables.j, mThisIntegrationMethod);

  //Calculate Total Delta Position
  ElementUtilities::CalculateTotalDeltaPosition(rVariables.DeltaPosition, this->GetGeometry());

  //calculating the reference jacobian from cartesian coordinates to parent coordinates for all integration points [dx_0/d£]
  rVariables.J = GetGeometry().Jacobian(rVariables.J, mThisIntegrationMethod, rVariables.DeltaPosition);

  KRATOS_CATCH("")
}

//*********************************COMPUTE KINEMATICS*********************************
//************************************************************************************

void SurfaceHeatFluxCondition::CalculateKinematics(ConditionVariables &rVariables,
                                                  const double &rPointNumber)
{
  KRATOS_TRY

  //Get the parent coodinates derivative [dN/d£]
  const GeometryType::ShapeFunctionsGradientsType &DN_De = rVariables.GetShapeFunctionsGradients();

  //Get the shape functions for the order of the integration method [N]
  const Matrix &Ncontainer = rVariables.GetShapeFunctions();

  //get first vector of the plane
  rVariables.Tangent1[0] = rVariables.J[rPointNumber](0, 0);
  rVariables.Tangent1[1] = rVariables.J[rPointNumber](1, 0);
  rVariables.Tangent1[2] = rVariables.J[rPointNumber](2, 0);

  //get second vector of the plane
  rVariables.Tangent2[0] = rVariables.J[rPointNumber](0, 1);
  rVariables.Tangent2[1] = rVariables.J[rPointNumber](1, 1);
  rVariables.Tangent2[2] = rVariables.J[rPointNumber](2, 1);

  //Compute the  normal
  MathUtils<double>::CrossProduct(rVariables.Normal, rVariables.Tangent1, rVariables.Tangent2);

  //Jacobian to the last known configuration
  double Jacobian = norm_2(rVariables.Normal);

  //auxiliar computation

  //get first vector of the plane
  rVariables.Tangent1[0] = rVariables.j[rPointNumber](0, 0);
  rVariables.Tangent1[1] = rVariables.j[rPointNumber](1, 0);
  rVariables.Tangent1[2] = rVariables.j[rPointNumber](2, 0);

  //get second vector of the plane
  rVariables.Tangent2[0] = rVariables.j[rPointNumber](0, 1);
  rVariables.Tangent2[1] = rVariables.j[rPointNumber](1, 1);
  rVariables.Tangent2[2] = rVariables.j[rPointNumber](2, 1);

  //Compute the  normal
  MathUtils<double>::CrossProduct(rVariables.Normal, rVariables.Tangent1, rVariables.Tangent2);

  //Jacobian to the deformed configuration
  rVariables.Jacobian = norm_2(rVariables.Normal);

  //Compute the unit normal and weighted tangents
  if (rVariables.Jacobian > 0)
  {
    rVariables.Normal /= rVariables.Jacobian;
    rVariables.Tangent1 /= rVariables.Jacobian;
    rVariables.Tangent2 /= rVariables.Jacobian;
  }

  //Jacobian to the last known configuration
  rVariables.Jacobian = Jacobian;

  //Set Shape Functions Values for this integration point
  noalias(rVariables.N) = matrix_row<const Matrix>(Ncontainer, rPointNumber);

  //Set Shape Functions Derivatives [dN/d£] for this integration point
  rVariables.DN_De = DN_De[rPointNumber];

  //Get geometry size
  rVariables.GeometrySize = GetGeometry().Area();

  //Get external stiffness
  this->CalculateExternalHeatFlux(rVariables);

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void SurfaceHeatFluxCondition::CalculateExternalHeatFlux(ConditionVariables &rVariables)
{

  KRATOS_TRY

  const SizeType number_of_nodes = GetGeometry().PointsNumber();

  //FLUX CONDITION:
  rVariables.ExternalScalarValue = 0.0;

  //defined on condition
  if (this->Has(HEAT_FLUX))
  {
    double &HeatFlux = this->GetValue(HEAT_FLUX);
    for (SizeType i = 0; i < number_of_nodes; i++)
      rVariables.ExternalScalarValue += rVariables.N[i] * HeatFlux;
  }

  //defined on condition nodes
  if (this->Has(HEAT_FLUX_VECTOR))
  {
    Vector &HeatFluxVector = this->GetValue(HEAT_FLUX_VECTOR);
    for (SizeType i = 0; i < number_of_nodes; i++)
    {
      rVariables.ExternalScalarValue += rVariables.N[i] * HeatFluxVector[i];
    }
  }

  //defined on geometry nodes
  for (SizeType i = 0; i < number_of_nodes; i++)
  {
    if (GetGeometry()[i].SolutionStepsDataHas(HEAT_FLUX))
      rVariables.ExternalScalarValue += rVariables.N[i] * GetGeometry()[i].FastGetSolutionStepValue(HEAT_FLUX);
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void SurfaceHeatFluxCondition::CalculateAndAddKuug(MatrixType &rLeftHandSideMatrix,
                                                  ConditionVariables &rVariables,
                                                  double &rIntegrationWeight)

{
  KRATOS_TRY

  HeatFluxCondition::CalculateAndAddKuug(rLeftHandSideMatrix, rVariables, rIntegrationWeight);

  const SizeType number_of_nodes = GetGeometry().PointsNumber();

  //Lumped Mass Matrix
  Vector LumpFact(number_of_nodes);
  noalias(LumpFact) = ZeroVector(number_of_nodes);

  LumpFact = GetGeometry().LumpingFactors(LumpFact);

  for (unsigned int i = 0; i < number_of_nodes; i++)
  {
    if (GetGeometry()[i].SolutionStepsDataHas(HEAT_TRANSFER_COEFFICIENT))
    {
      rLeftHandSideMatrix(i,i) += LumpFact[i] * GetGeometry()[i].FastGetSolutionStepValue(HEAT_TRANSFER_COEFFICIENT) * rIntegrationWeight;
    }
  }

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

int SurfaceHeatFluxCondition::Check(const ProcessInfo &rCurrentProcessInfo) const
{
  KRATOS_TRY

  // Perform base condition checks
  int ErrorCode = 0;
  ErrorCode = HeatFluxCondition::Check(rCurrentProcessInfo);

  return ErrorCode;

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void SurfaceHeatFluxCondition::save(Serializer &rSerializer) const
{
  KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, HeatFluxCondition)
}

void SurfaceHeatFluxCondition::load(Serializer &rSerializer)
{
  KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, HeatFluxCondition)
}

} // Namespace Kratos.
