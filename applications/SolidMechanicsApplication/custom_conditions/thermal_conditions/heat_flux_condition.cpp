//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:         JMCarbonell $
//   Maintained by:       $Maintainer:                JMC $
//   Date:                $Date:            November 2020 $
//
//

// System includes

// External includes

// Project includes
#include "custom_conditions/thermal_conditions/heat_flux_condition.hpp"
#include "utilities/beam_math_utilities.hpp"

namespace Kratos
{

//***********************************************************************************
//***********************************************************************************
HeatFluxCondition::HeatFluxCondition()
    : BoundaryCondition()
{
}

//***********************************************************************************
//***********************************************************************************
HeatFluxCondition::HeatFluxCondition(IndexType NewId, GeometryType::Pointer pGeometry)
    : BoundaryCondition(NewId, pGeometry)
{
}

//***********************************************************************************
//***********************************************************************************
HeatFluxCondition::HeatFluxCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : BoundaryCondition(NewId, pGeometry, pProperties)
{
  this->Set(THERMAL);
  mThisIntegrationMethod = GetGeometry().GetDefaultIntegrationMethod();
}

//************************************************************************************
//************************************************************************************
HeatFluxCondition::HeatFluxCondition(HeatFluxCondition const &rOther)
    : BoundaryCondition(rOther)
{
}

//***********************************************************************************
//***********************************************************************************
Condition::Pointer HeatFluxCondition::Create(IndexType NewId,
                                         NodesArrayType const &ThisNodes,
                                         PropertiesType::Pointer pProperties) const
{
  return Kratos::make_intrusive<HeatFluxCondition>(NewId, GetGeometry().Create(ThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************

Condition::Pointer HeatFluxCondition::Clone(IndexType NewId, NodesArrayType const &rThisNodes) const
{
  std::cout << " Call base class HEAT FLUX CONDITION Clone " << std::endl;

  HeatFluxCondition NewCondition(NewId, GetGeometry().Create(rThisNodes), pGetProperties());

  NewCondition.SetData(this->GetData());
  NewCondition.SetFlags(this->GetFlags());

  return Kratos::make_intrusive<HeatFluxCondition>(NewCondition);
}

//***********************************************************************************
//***********************************************************************************
HeatFluxCondition::~HeatFluxCondition()
{
}

//************************************************************************************
//************************************************************************************

void HeatFluxCondition::InitializeConditionVariables(ConditionVariables &rVariables, const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  BoundaryCondition::InitializeConditionVariables(rVariables, rCurrentProcessInfo);

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void HeatFluxCondition::CalculateExternalHeatFlux(ConditionVariables &rVariables)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling the base class CalculateExternalFlux method for a flux condition... " << std::endl;

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void HeatFluxCondition::CalculateAndAddExternalForces(VectorType &rRightHandSideVector,
                                                      ConditionVariables &rVariables,
                                                      double &rIntegrationWeight)

{
  KRATOS_TRY

  const SizeType number_of_nodes = GetGeometry().PointsNumber();

  for (SizeType i = 0; i < number_of_nodes; i++)
  {
    rRightHandSideVector[i] += rVariables.N[i] * rVariables.ExternalScalarValue * rIntegrationWeight;

    //define the flux via temperature transfer from body to room
    if (GetGeometry()[i].SolutionStepsDataHas(HEAT_TRANSFER_COEFFICIENT) && GetGeometry()[i].SolutionStepsDataHas(ROOM_TEMPERATURE))
    {
      rRightHandSideVector[i] -= rVariables.N[i] * GetGeometry()[i].FastGetSolutionStepValue(HEAT_TRANSFER_COEFFICIENT) * (GetGeometry()[i].FastGetSolutionStepValue(TEMPERATURE) - GetGeometry()[i].FastGetSolutionStepValue(ROOM_TEMPERATURE)) * rIntegrationWeight;
    }
  }

  //std::cout<<" ExternalFluxForces ["<<this->Id()<<"]"<<rRightHandSideVector<<std::endl;

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

double &HeatFluxCondition::CalculateAndAddExternalEnergy(double &rEnergy,
                                                     ConditionVariables &rVariables,
                                                     double &rIntegrationWeight,
                                                     const ProcessInfo &rCurrentProcessInfo)

{
  KRATOS_TRY

  return rEnergy;

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

unsigned int HeatFluxCondition::GetDofsSize() const
{
  KRATOS_TRY

  unsigned int size = GetGeometry().PointsNumber();

  return size;

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void HeatFluxCondition::GetDofList(DofsVectorType &rConditionDofList,
                                    const ProcessInfo &rCurrentProcessInfo) const
{
  KRATOS_TRY

  rConditionDofList.resize(0);
  const SizeType number_of_nodes = GetGeometry().PointsNumber();

  for (SizeType i = 0; i < number_of_nodes; i++)
  {
    rConditionDofList.push_back(GetGeometry()[i].pGetDof(TEMPERATURE));
  }

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void HeatFluxCondition::EquationIdVector(EquationIdVectorType &rResult,
                                         const ProcessInfo &rCurrentProcessInfo) const
{
  KRATOS_TRY

  const SizeType number_of_nodes = GetGeometry().PointsNumber();
  unsigned int dofs_size = this->GetDofsSize();

  if (rResult.size() != dofs_size)
    rResult.resize(dofs_size, false);

  for (SizeType i = 0; i < number_of_nodes; i++)
  {
    rResult[i] = GetGeometry()[i].GetDof(TEMPERATURE).EquationId();
  }

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void HeatFluxCondition::GetValuesVector(Vector &rValues, int Step) const
{
  KRATOS_TRY

  const SizeType number_of_nodes = GetGeometry().PointsNumber();
  unsigned int dofs_size = this->GetDofsSize();

  if (rValues.size() != dofs_size)
    rValues.resize(dofs_size, false);

  for (SizeType i = 0; i < number_of_nodes; i++)
  {
    rValues[i] = GetGeometry()[i].FastGetSolutionStepValue(TEMPERATURE, Step);
  }

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void HeatFluxCondition::GetFirstDerivativesVector(Vector &rValues, int Step) const
{
  KRATOS_TRY

  const SizeType number_of_nodes = GetGeometry().PointsNumber();
  unsigned int dofs_size = this->GetDofsSize();

  if (rValues.size() != dofs_size)
    rValues.resize(dofs_size, false);

  for (SizeType i = 0; i < number_of_nodes; i++)
  {
    rValues[i] = 0;
  }

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void HeatFluxCondition::GetSecondDerivativesVector(Vector &rValues, int Step) const
{
  KRATOS_TRY

  const SizeType number_of_nodes = GetGeometry().PointsNumber();
  unsigned int dofs_size = this->GetDofsSize();

  if (rValues.size() != dofs_size)
    rValues.resize(dofs_size, false);

  for (SizeType i = 0; i < number_of_nodes; i++)
  {
    rValues[i] = 0;
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void HeatFluxCondition::InitializeExplicitContributions()
{
  KRATOS_TRY

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void HeatFluxCondition::AddExplicitContribution(const VectorType &rRHS,
                                                const Variable<VectorType> &rRHSVariable,
                                                const Variable<array_1d<double, 3>> &rDestinationVariable,
                                                const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling a flux condition for explicit contribution ... " << std::endl;

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

int HeatFluxCondition::Check(const ProcessInfo &rCurrentProcessInfo) const
{
  KRATOS_TRY

  // Perform base condition checks
  int ErrorCode = 0;
  ErrorCode = BoundaryCondition::Check(rCurrentProcessInfo);

  return ErrorCode;

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void HeatFluxCondition::save(Serializer &rSerializer) const
{
  KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BoundaryCondition)
}

void HeatFluxCondition::load(Serializer &rSerializer)
{
  KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BoundaryCondition)
}

} // Namespace Kratos.
