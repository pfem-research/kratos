//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:         JMCarbonell $
//   Maintained by:       $Maintainer:                JMC $
//   Date:                $Date:                July 2013 $
//
//

#if !defined(KRATOS_AXISYMMETRIC_UPDATED_LAGRANGIAN_ELEMENT_HPP_INCLUDED)
#define KRATOS_AXISYMMETRIC_UPDATED_LAGRANGIAN_ELEMENT_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_elements/solid_elements/large_displacement_element.hpp"

namespace Kratos
{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Axisymmetric Updated Lagrangian Element 2D geometries.

/**
 * Implements a Large Displacement Lagrangian definition for structural analysis.
 * This works for arbitrary geometries in 2D
 */

class KRATOS_API(SOLID_MECHANICS_APPLICATION) AxisymmetricUpdatedLagrangianElement
    : public LargeDisplacementElement
{
public:
    ///@name Type Definitions
    ///@{
    ///Reference type definition for constitutive laws
    typedef ConstitutiveLaw ConstitutiveLawType;
    ///Pointer type for constitutive laws
    typedef ConstitutiveLawType::Pointer ConstitutiveLawPointerType;
    ///StressMeasure from constitutive laws
    typedef ConstitutiveLawType::StressMeasure StressMeasureType;
    ///Type definition for integration methods
    typedef GeometryData::IntegrationMethod IntegrationMethod;
    ///Type for size
    typedef GeometryData::SizeType SizeType;
    ///Type for element variables
    typedef LargeDisplacementElement::ElementDataType ElementDataType;

    /// Counted pointer of AxisymmetricUpdatedLagrangianElement
    KRATOS_CLASS_INTRUSIVE_POINTER_DEFINITION(AxisymmetricUpdatedLagrangianElement);

    ///@}
    ///@name Life Cycle
    ///@{

    /// Default constructors
    AxisymmetricUpdatedLagrangianElement(IndexType NewId, GeometryType::Pointer pGeometry);

    AxisymmetricUpdatedLagrangianElement(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties);

    ///Copy constructor
    AxisymmetricUpdatedLagrangianElement(AxisymmetricUpdatedLagrangianElement const &rOther);

    /// Destructor.
    ~AxisymmetricUpdatedLagrangianElement() override;

    ///@}
    ///@name Operators
    ///@{

    /// Assignment operator.
    AxisymmetricUpdatedLagrangianElement &operator=(AxisymmetricUpdatedLagrangianElement const &rOther);

    ///@}
    ///@name Operations
    ///@{

    /**
     * creates a new total lagrangian updated element pointer
     * @param NewId: the ID of the new element
     * @param ThisNodes: the nodes of the new element
     * @param pProperties: the properties assigned to the new element
     * @return a Pointer to the new element
     */
    Element::Pointer Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const override;

    /**
     * clones the selected element variables, creating a new one
     * @param NewId: the ID of the new element
     * @param ThisNodes: the nodes of the new element
     * @param pProperties: the properties assigned to the new element
     * @return a Pointer to the new element
     */
    Element::Pointer Clone(IndexType NewId, NodesArrayType const &ThisNodes) const override;

    // //************* GETTING METHODS

    //SET

    /**
     * Set a double  Value on the Element Constitutive Law
     */
    void SetValuesOnIntegrationPoints(const Variable<double>& rVariable, const std::vector<double>& rValues, const ProcessInfo& rCurrentProcessInfo) override;

    /**
     * Set a Matrix  Value on the Element Constitutive Law
     */
    void SetValuesOnIntegrationPoints(const Variable<Matrix>& rVariable, const std::vector<Matrix>& rValues, const ProcessInfo& rCurrentProcessInfo) override;

    //GET:

    /**
     * Get on rVariable a double Value from the Element Constitutive Law
     */
    void CalculateOnIntegrationPoints(const Variable<double> &rVariable, std::vector<double> &rValues, const ProcessInfo &rCurrentProcessInfo) override;

    /**
     * Get on rVariable a Matrix Value from the Element Constitutive Law
     */
    void CalculateOnIntegrationPoints(const Variable<Matrix> &rVariable, std::vector<Matrix> &rValues, const ProcessInfo &rCurrentProcessInfo) override;

    //************* STARTING - ENDING  METHODS

    /**
      * Called to initialize the element.
      * Must be called before any calculation is done
      */
    void Initialize(const ProcessInfo &rCurrentProcessInfo) override;

    //************************************************************************************
    //************************************************************************************
    /**
     * This function provides the place to perform checks on the completeness of the input.
     * It is designed to be called only once (or anyway, not often) typically at the beginning
     * of the calculations, so to verify that nothing is missing from the input
     * or that no common error is found.
     * @param rCurrentProcessInfo
     */
    int Check(const ProcessInfo &rCurrentProcessInfo) const override;

    ///@}
    ///@name Access
    ///@{
    ///@}
    ///@name Inquiry
    ///@{
    ///@}
    ///@name Input and output
    ///@{
    ///@}
    ///@name Friends
    ///@{
    ///@}

protected:
    ///@name Protected static Member Variables
    ///@{
    ///@}
    ///@name Protected member Variables
    ///@{

    /**
     * Container for historical total elastic deformation measure
     */
    std::vector<Matrix> mDeformationGradientF0;

    /**
     * Container for the total deformation gradient determinants
     */
    Vector mDeterminantF0;

    ///@}
    ///@name Protected Operators
    ///@{
    AxisymmetricUpdatedLagrangianElement() : LargeDisplacementElement()
    {
    }

    ///@}
    ///@name Protected Operations
    ///@{
    /**
     * Calculation of the Integration Weight
     */
    double &CalculateIntegrationWeight(ElementDataType &rVariables, double &rIntegrationWeight) override;


    /**
     * Calculation of the Geometric Stiffness Matrix. Kuug = BT * S
     */
    void CalculateAndAddKuug(MatrixType &rK,
                             ElementDataType &rVariables,
                             double &rIntegrationWeight) const override;

    /**
     * Initialize Element General Variables
     */
    void InitializeElementData(ElementDataType &rVariables, const ProcessInfo &rCurrentProcessInfo) const override;

    /**
     * Finalize Element Internal Variables
     */
    void FinalizeStepVariables(ElementDataType &rVariables, const double &rPointNumber) override;

    /**
     * Calculate Element Kinematics
     */
    void CalculateKinematics(ElementDataType &rVariables,
                             const double &rPointNumber) const override;

    /**
     * Calculate Radius in the current and deformed geometry
     */
    void CalculateRadius(double &rCurrentRadius,
                         double &rReferenceRadius,
                         const Vector &rN) const;

    /**
     * Calculation of the Deformation Gradient F
     */
    void CalculateDeformationGradient(Matrix &rF,
                                      const Matrix &rDN_DX,
                                      const Matrix &rDeltaPosition,
                                      const double &rCurrentRadius,
                                      const double &rReferenceRadius) const;

    /**
     * Calculation of the Deformation Matrix  BL
     */
    void CalculateDeformationMatrix(Matrix &rB,
                                    const Matrix &rDN_DX,
                                    const Vector &rN,
                                    const double &rCurrentRadius) const;

    /**
     * Get the Historical Deformation Gradient to calculate after finalize the step
     */
    void GetHistoricalVariables(ElementDataType &rVariables,
                                const double &rPointNumber) const override;

    /**
     * Calculation of the Green Lagrange Strain Vector
     */
    void CalculateGreenLagrangeStrain(const Matrix &rF,
                                      Vector &rStrainVector) override;

    /**
     * Calculation of the Almansi Strain Vector
     */
    void CalculateAlmansiStrain(const Matrix &rF,
                                Vector &rStrainVector) override;

    /**
     * Calculation of the Volume Change of the Element
     */
    double &CalculateVolumeChange(double &rVolumeChange, ElementDataType &rVariables) override;

    ///@}
    ///@name Protected  Access
    ///@{
    ///@}
    ///@name Protected Inquiry
    ///@{
    ///@}
    ///@name Protected LifeCycle
    ///@{
    ///@}

private:
    ///@name Static Member Variables
    ///@{
    ///@}
    ///@name Member Variables
    ///@{

    ///@}
    ///@name Private Operators
    ///@{

    ///@}
    ///@name Private Operations
    ///@{

    ///@}
    ///@name Private  Access
    ///@{
    ///@}

    ///@}
    ///@name Serialization
    ///@{
    friend class Serializer;

    // A private default constructor necessary for serialization

    void save(Serializer &rSerializer) const override;

    void load(Serializer &rSerializer) override;

    ///@name Private Inquiry
    ///@{
    ///@}
    ///@name Un accessible methods
    ///@{
    ///@}

}; // Class AxisymmetricUpdatedLagrangianElement

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
///@}

} // namespace Kratos.
#endif // KRATOS_AXISYMMETRIC_UPDATED_LAGRANGIAN_ELEMENT_HPP_INCLUDED  defined
