//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:         JMCarbonell $
//   Maintained by:       $Maintainer:                JMC $
//   Date:                $Date:                July 2015 $
//
//

#if !defined(KRATOS_UPDATED_TOTAL_LAGRANGIAN_U_P_ELEMENT_HPP_INCLUDED)
#define KRATOS_UPDATED_TOTAL_LAGRANGIAN_U_P_ELEMENT_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_elements/solid_elements/updated_lagrangian_U_P_element.hpp"

namespace Kratos
{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Total Updated Lagrangian U-P Element for 3D and 2D geometries. Linear Triangles and Tetrahedra

/**
 * Implements a Large Displacement Lagrangian definition for structural analysis.
 * This works for arbitrary geometries in 3D and 2D
 */

class KRATOS_API(SOLID_MECHANICS_APPLICATION) UpdatedTotalLagrangianUPElement
    : public UpdatedLagrangianUPElement
{
public:
    ///@name Type Definitions
    ///@{
    ///Reference type definition for constitutive laws
    typedef ConstitutiveLaw ConstitutiveLawType;
    ///Pointer type for constitutive laws
    typedef ConstitutiveLawType::Pointer ConstitutiveLawPointerType;
    ///StressMeasure from constitutive laws
    typedef ConstitutiveLawType::StressMeasure StressMeasureType;
    ///Type definition for integration methods
    typedef GeometryData::IntegrationMethod IntegrationMethod;
    ///Type for size
    typedef GeometryData::SizeType SizeType;
    ///Type for element variables
    typedef UpdatedLagrangianUPElement::ElementDataType ElementDataType;

    /// Counted pointer of UpdatedTotalLagrangianUPElement
    KRATOS_CLASS_INTRUSIVE_POINTER_DEFINITION(UpdatedTotalLagrangianUPElement);

    ///@}
    ///@name Life Cycle
    ///@{

    /// Default constructors
    UpdatedTotalLagrangianUPElement(IndexType NewId, GeometryType::Pointer pGeometry);

    UpdatedTotalLagrangianUPElement(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties);

    ///Copy constructor
    UpdatedTotalLagrangianUPElement(UpdatedTotalLagrangianUPElement const &rOther);

    /// Destructor.
    virtual ~UpdatedTotalLagrangianUPElement();

    ///@}
    ///@name Operators
    ///@{

    /// Assignment operator.
    UpdatedTotalLagrangianUPElement &operator=(UpdatedTotalLagrangianUPElement const &rOther);

    ///@}
    ///@name Operations
    ///@{
    /**
     * Returns the currently selected integration method
     * @return current integration method selected
     */
    /**
     * creates a new total lagrangian updated element pointer
     * @param NewId: the ID of the new element
     * @param ThisNodes: the nodes of the new element
     * @param pProperties: the properties assigned to the new element
     * @return a Pointer to the new element
     */
    Element::Pointer Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const override;

    /**
     * clones the selected element variables, creating a new one
     * @param NewId: the ID of the new element
     * @param ThisNodes: the nodes of the new element
     * @param pProperties: the properties assigned to the new element
     * @return a Pointer to the new element
     */
    Element::Pointer Clone(IndexType NewId, NodesArrayType const &ThisNodes) const override;

    //************* STARTING - ENDING  METHODS

    /**
     * This function provides the place to perform checks on the completeness of the input.
     * It is designed to be called only once (or anyway, not often) typically at the beginning
     * of the calculations, so to verify that nothing is missing from the input
     * or that no common error is found.
     * @param rCurrentProcessInfo
     */
    //int Check(const ProcessInfo& rCurrentProcessInfo);

    ///@}
    ///@name Access
    ///@{

    ///@}
    ///@name Inquiry
    ///@{
    ///@}
    ///@name Input and output
    ///@{
    ///@}
    ///@name Friends
    ///@{
    ///@}

protected:
    ///@name Protected static Member Variables
    ///@{
    ///@}
    ///@name Protected member Variables
    ///@{

    ///@}
    ///@name Protected Operators
    ///@{
    UpdatedTotalLagrangianUPElement() : UpdatedLagrangianUPElement()
    {
    }

    ///@}
    ///@name Protected Operations
    ///@{

    /**
     * Calculation and addition of the matrices of the LHS
     */
    void CalculateAndAddLHS(LocalSystemComponents &rLocalSystem,
                            ElementDataType &rVariables,
                            double &rIntegrationWeight) override;

    /**
     * Calculation and addition of the vectors of the RHS
     */
    void CalculateAndAddRHS(LocalSystemComponents &rLocalSystem,
                            ElementDataType &rVariables,
                            Vector &rVolumeForce,
                            double &rIntegrationWeight) override;

    /**
     * Initialize Element General Variables
     */
    void InitializeElementData(ElementDataType &rVariables,
                               const ProcessInfo &rCurrentProcessInfo) const override;

    /**
     * Transform Element General Variables
     */
    void TransformElementData(ElementDataType &rVariables,
                              const double &rPointNumber) override;

    /**
     * Calculate Element Kinematics
     */
    void CalculateKinematics(ElementDataType &rVariables,
                             const double &rPointNumber) const override;

    /**
     * Calculation of the Deformation Matrix  BL
     */
    void CalculateDeformationMatrix(Matrix &rB,
                                    Matrix &rF,
                                    Matrix &rDN_DX) const;

    /**
     * Get the Historical Deformation Gradient to calculate after finalize the step
     */
    void GetHistoricalVariables(ElementDataType &rVariables,
                                const double &rPointNumber) const override;

    /**
     * Calculation of the Volume Change of the Element
     */
    double &CalculateVolumeChange(double &rVolumeChange, ElementDataType &rVariables) override;

    /**
     * Calculation of the DN_DX in the updated configuration
     */
    void CalculatePushForwardDN_DX(ElementDataType &rVariables);

    ///@}
    ///@name Protected  Access
    ///@{
    ///@}
    ///@name Protected Inquiry
    ///@{
    ///@}
    ///@name Protected LifeCycle
    ///@{
    ///@}

private:
    ///@name Static Member Variables
    ///@{
    ///@}
    ///@name Member Variables
    ///@{

    ///@}
    ///@name Private Operators
    ///@{

    ///@}
    ///@name Private Operations
    ///@{

    ///@}
    ///@name Private  Access
    ///@{
    ///@}

    ///@}
    ///@name Serialization
    ///@{
    friend class Serializer;

    // A private default constructor necessary for serialization

    void save(Serializer &rSerializer) const override;

    void load(Serializer &rSerializer) override;

    ///@name Private Inquiry
    ///@{
    ///@}
    ///@name Un accessible methods
    ///@{
    ///@}

}; // Class UpdatedTotalLagrangianUPElement

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
///@}

} // namespace Kratos.
#endif // KRATOS_UPDATED_TOTAL_LAGRANGIAN_U_P_ELEMENT_HPP_INCLUDED  defined
