//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:         JMCarbonell $
//   Maintained by:       $Maintainer:                JMC $
//   Date:                $Date:                July 2013 $
//
//

// System includes

// External includes

// Project includes
#include "custom_elements/solid_elements/large_displacement_element.hpp"
#include "solid_mechanics_application_variables.h"

namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

LargeDisplacementElement::LargeDisplacementElement()
    : SolidElement()
{
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

LargeDisplacementElement::LargeDisplacementElement(IndexType NewId, GeometryType::Pointer pGeometry)
    : SolidElement(NewId, pGeometry)
{
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

LargeDisplacementElement::LargeDisplacementElement(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : SolidElement(NewId, pGeometry, pProperties)
{
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

LargeDisplacementElement::LargeDisplacementElement(LargeDisplacementElement const &rOther)
    : SolidElement(rOther)
{
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

LargeDisplacementElement &LargeDisplacementElement::operator=(LargeDisplacementElement const &rOther)
{
    SolidElement::operator=(rOther);

    return *this;
}

//*********************************OPERATIONS*****************************************
//************************************************************************************

Element::Pointer LargeDisplacementElement::Create(IndexType NewId, NodesArrayType const &rThisNodes, PropertiesType::Pointer pProperties) const
{
    KRATOS_ERROR << " calling the default method Create for a large displacement element " << std::endl;
    return Kratos::make_intrusive<LargeDisplacementElement>(NewId, GetGeometry().Create(rThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************

Element::Pointer LargeDisplacementElement::Clone(IndexType NewId, NodesArrayType const &rThisNodes) const
{

    KRATOS_ERROR << " calling the default method Clone for a large displacement element " << std::endl;

    LargeDisplacementElement NewElement(NewId, GetGeometry().Create(rThisNodes), pGetProperties());

    NewElement.mThisIntegrationMethod = mThisIntegrationMethod;

    if (NewElement.mConstitutiveLawVector.size() != mConstitutiveLawVector.size())
    {
        NewElement.mConstitutiveLawVector.resize(mConstitutiveLawVector.size());

        if (NewElement.mConstitutiveLawVector.size() != NewElement.GetGeometry().IntegrationPointsNumber())
            KRATOS_ERROR << " constitutive law not has the correct size large displacement element " << std::endl;
    }

    NewElement.SetData(this->GetData());
    NewElement.SetFlags(this->GetFlags());

    return Kratos::make_intrusive<LargeDisplacementElement>(NewElement);
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

LargeDisplacementElement::~LargeDisplacementElement()
{
}

//************************************************************************************
//************************************************************************************

void LargeDisplacementElement::GetHistoricalVariables(ElementDataType &rVariables, const double &rPointNumber) const
{
    //Deformation Gradient F ( set to identity )
    unsigned int size = rVariables.F.size1();

    rVariables.detF = 1;
    noalias(rVariables.F) = IdentityMatrix(size);
}

//************************************************************************************
//************************************************************************************

void LargeDisplacementElement::SetElementData(ElementDataType &rVariables,
                                              ConstitutiveLaw::Parameters &rValues,
                                              const int &rPointNumber)
{

    //set previous step for output print purposes
    if (this->Is(SolidElement::FINALIZED_STEP))
    {
        this->GetHistoricalVariables(rVariables, rPointNumber);
    }

    //check inverted element
    this->CheckElementData(rVariables, rPointNumber);

    //Compute F and detF (from 0 to n+1) : store it in H variable and detH
    rVariables.detH = rVariables.detF * rVariables.detF0;
    noalias(rVariables.H) = prod(rVariables.F, rVariables.F0);

    rValues.SetDeterminantF(rVariables.detH);
    rValues.SetDeformationGradientF(rVariables.H);
    rValues.SetStrainVector(rVariables.StrainVector);
    rValues.SetStressVector(rVariables.StressVector);
    rValues.SetConstitutiveMatrix(rVariables.ConstitutiveMatrix);
    rValues.SetShapeFunctionsDerivatives(rVariables.DN_DX);
    rValues.SetShapeFunctionsValues(rVariables.N);
}

//************************************************************************************
//************************************************************************************

void LargeDisplacementElement::CheckElementData(ElementDataType &rVariables, const int &rPointNumber)
{
    KRATOS_TRY

    //to be accurate calculus must stop
    if (rVariables.detF < 0)
    {
      KRATOS_WARNING(" [ELEMENT]") << "["<<this->Id()<<"] FAILED (Iter:"<<rVariables.GetProcessInfo()[NL_ITERATION_NUMBER]<<") "<<std::endl;
      rVariables.detJ = 0;
      const GeometryType &rGeometry = GetGeometry();

      std::cout<<"           Domain Size  : "<<rGeometry.DomainSize()<<std::endl;
      std::cout<<"           Jacobian |F| : "<<rVariables.detF<<" detF0 "<<rVariables.detF0<<std::endl;
      for (auto &i_node : rGeometry)
      {
        const array_1d<double, 3> &CurrentPosition = i_node.Coordinates();
        const array_1d<double, 3> &CurrentDisplacement = i_node.FastGetSolutionStepValue(DISPLACEMENT);
        const array_1d<double, 3> &PreviousDisplacement = i_node.FastGetSolutionStepValue(DISPLACEMENT, 1);
        array_1d<double, 3> PreviousPosition = CurrentPosition - (CurrentDisplacement - PreviousDisplacement);
        std::cout<<"           Node[" << i_node.Id() << "]" << std::endl;
        std::cout<<"                Initial     :" << i_node.GetInitialPosition() << std::endl;
        std::cout<<"                Reference   :" << PreviousPosition << std::endl;
        std::cout<<"                Position    :" << CurrentPosition << std::endl;
        std::cout<<"                Displacement:" << CurrentDisplacement-PreviousDisplacement << " |u|:" <<norm_2(CurrentDisplacement-PreviousDisplacement) << std::endl;
        if (i_node.SolutionStepsDataHas(CONTACT_FORCE))
        {
          const array_1d<double, 3> &ContactForce = i_node.FastGetSolutionStepValue(CONTACT_FORCE);
          if (norm_2(ContactForce) !=0)
            std::cout<<"                Contact:    :" << ContactForce << std::endl;
        }
      }

      this->Set(BLOCKED, true);
      std::cout<<"-----------------------------------------------"<<std::endl;
      KRATOS_ERROR << std::endl;
    }
    else
    {
      if (this->Is(BLOCKED) && this->Is(ACTIVE))
      {
        this->Set(BLOCKED, false);
        KRATOS_WARNING("") << " Undo BLOCKED LargeDispElement " << this->Id() << std::endl;
      }
    }

    KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void LargeDisplacementElement::InitializeSolutionStep(const ProcessInfo &rCurrentProcessInfo)
{
    KRATOS_TRY

    SolidElement::InitializeSolutionStep(rCurrentProcessInfo);
    this->Set(SolidElement::FINALIZED_STEP, false);

    KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void LargeDisplacementElement::FinalizeSolutionStep(const ProcessInfo &rCurrentProcessInfo)
{
    KRATOS_TRY

    SolidElement::FinalizeSolutionStep(rCurrentProcessInfo);

    this->Set(SolidElement::FINALIZED_STEP, true);

    KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void LargeDisplacementElement::CalculateAndAddKuug(MatrixType &rLeftHandSideMatrix,
                                                   ElementDataType &rVariables,
                                                   double &rIntegrationWeight) const

{
    KRATOS_TRY

    const SizeType dimension = GetGeometry().WorkingSpaceDimension();
    const Matrix WeightxStressTensor = rIntegrationWeight * MathUtils<double>::StressVectorToTensor(rVariables.StressVector);
    Matrix ReducedKg(rVariables.DN_DX.size1(), rVariables.DN_DX.size1());
    MathUtils<double>::BDBtProductOperation(ReducedKg, WeightxStressTensor, rVariables.DN_DX);
    // Matrix StressTensor = MathUtils<double>::StressVectorToTensor(rVariables.StressVector);
    // Matrix ReducedKg = prod(rVariables.DN_DX, rIntegrationWeight * Matrix(prod(StressTensor, trans(rVariables.DN_DX)))); //to be optimized
    MathUtils<double>::ExpandAndAddReducedMatrix(rLeftHandSideMatrix, ReducedKg, dimension);

    KRATOS_CATCH("")
}


//*********************************COMPUTE KINETICS***********************************
//************************************************************************************

//this is the implementation for the UpdatedLagrangianFormulation  // specific one for the total lagrangian and total_updated lagrangian is needed
void LargeDisplacementElement::CalculateKinetics(ElementDataType &rVariables, const double &rPointNumber) const
{
  KRATOS_TRY

  //TotalDeltaPosition must not be used in this element as mDeterminantF0 and mDeformationGradientF0 are stored for reduced order
  //however then the storage of variables in the full integration order quadrature must be considered

  const SizeType dimension = GetGeometry().WorkingSpaceDimension();

  //Get the parent coodinates derivative [dN/d£]
  const GeometryType::ShapeFunctionsGradientsType &DN_De = rVariables.GetShapeFunctionsGradients();

  //Get the shape functions for the order of the integration method [N]
  const Matrix &Ncontainer = rVariables.GetShapeFunctions();

  rVariables.DeltaPosition = this->CalculateTotalDeltaPosition(rVariables.DeltaPosition);

  rVariables.j = GetGeometry().Jacobian(rVariables.j, mThisIntegrationMethod, rVariables.DeltaPosition);

  //Calculating the inverse of the jacobian and the parameters needed [d£/dx_n]
  Matrix InvJ;
  MathUtils<double>::InvertMatrix(rVariables.j[rPointNumber], InvJ, rVariables.detJ);

  //Calculating the cartesian derivatives [dN/dx_n] = [dN/d£][d£/dx_0]
  noalias(rVariables.DN_DX) = prod(DN_De[rPointNumber], InvJ);

  //Deformation Gradient F [dx_n+1/dx_0] = [dx_n+1/d£] [d£/dx_0]
  noalias(rVariables.F) = prod(rVariables.j[rPointNumber], InvJ);

  //Determinant of the deformation gradient F
  rVariables.detF = MathUtils<double>::Det(rVariables.F);

  //Determinant of the Deformation Gradient F0
  // (in this element F = F0, then F0 is set to the identity for coherence in the constitutive law)
  rVariables.detF0 = 1;
  rVariables.F0 = identity_matrix<double>(dimension);

  //Set Shape Functions Values for this integration point
  noalias(rVariables.N) = matrix_row<const Matrix>(Ncontainer, rPointNumber);

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void LargeDisplacementElement::CalculateGreenLagrangeStrain(const Matrix &rF, Vector &rStrainVector)
{
    KRATOS_TRY

    const SizeType dimension = GetGeometry().WorkingSpaceDimension();

    //Right Cauchy-Green Calculation
    Matrix C(dimension, dimension);
    noalias(C) = prod(trans(rF), rF);

    if (dimension == 2)
    {

        //Green Lagrange Strain Calculation
        if (rStrainVector.size() != 3)
            rStrainVector.resize(3, false);

        rStrainVector[0] = 0.5 * (C(0, 0) - 1.00);

        rStrainVector[1] = 0.5 * (C(1, 1) - 1.00);

        rStrainVector[2] = C(0, 1); // xy
    }
    else if (dimension == 3)
    {

        //Green Lagrange Strain Calculation
        if (rStrainVector.size() != 6)
            rStrainVector.resize(6, false);

        rStrainVector[0] = 0.5 * (C(0, 0) - 1.00);

        rStrainVector[1] = 0.5 * (C(1, 1) - 1.00);

        rStrainVector[2] = 0.5 * (C(2, 2) - 1.00);

        rStrainVector[3] = C(0, 1); // xy

        rStrainVector[4] = C(1, 2); // yz

        rStrainVector[5] = C(0, 2); // xz
    }
    else
    {
        KRATOS_ERROR << " something is wrong with the dimension green-lagrange strain " << std::endl;
    }

    KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void LargeDisplacementElement::CalculateAlmansiStrain(const Matrix &rF, Vector &rStrainVector)
{
    KRATOS_TRY

    const SizeType dimension = GetGeometry().WorkingSpaceDimension();

    //Left Cauchy-Green Calculation
    Matrix LeftCauchyGreen(dimension, dimension);
    noalias(LeftCauchyGreen) = prod(rF, trans(rF));

    //Calculating the inverse of the jacobian
    Matrix InverseLeftCauchyGreen(dimension, dimension);
    double det_b = 0;
    MathUtils<double>::InvertMatrix(LeftCauchyGreen, InverseLeftCauchyGreen, det_b);

    if (dimension == 2)
    {

        //Almansi Strain Calculation
        rStrainVector[0] = 0.5 * (1.00 - InverseLeftCauchyGreen(0, 0));

        rStrainVector[1] = 0.5 * (1.00 - InverseLeftCauchyGreen(1, 1));

        rStrainVector[2] = -InverseLeftCauchyGreen(0, 1); // xy
    }
    else if (dimension == 3)
    {

        //Almansi Strain Calculation
        if (rStrainVector.size() != 6)
            rStrainVector.resize(6, false);

        rStrainVector[0] = 0.5 * (1.00 - InverseLeftCauchyGreen(0, 0));

        rStrainVector[1] = 0.5 * (1.00 - InverseLeftCauchyGreen(1, 1));

        rStrainVector[2] = 0.5 * (1.00 - InverseLeftCauchyGreen(2, 2));

        rStrainVector[3] = -InverseLeftCauchyGreen(0, 1); // xy

        rStrainVector[4] = -InverseLeftCauchyGreen(1, 2); // yz

        rStrainVector[5] = -InverseLeftCauchyGreen(0, 2); // xz
    }
    else
    {
        KRATOS_ERROR << " something is wrong with the dimension almansi strain " << std::endl;
    }

    KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void LargeDisplacementElement::CalculateOnIntegrationPoints(const Variable<double> &rVariable, std::vector<double> &rValues, const ProcessInfo &rCurrentProcessInfo)
{
    KRATOS_TRY

    SolidElement::CalculateOnIntegrationPoints(rVariable, rValues, rCurrentProcessInfo);

    KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void LargeDisplacementElement::CalculateOnIntegrationPoints(const Variable<Vector> &rVariable, std::vector<Vector> &rValues, const ProcessInfo &rCurrentProcessInfo)
{

    KRATOS_TRY

    const unsigned int &integration_points_number = GetGeometry().IntegrationPointsNumber(mThisIntegrationMethod);

    if (rValues.size() != integration_points_number)
        rValues.resize(integration_points_number);

    if (rVariable == GREEN_LAGRANGE_STRAIN_VECTOR || rVariable == ALMANSI_STRAIN_VECTOR)
    {
        //create and initialize element variables:
        ElementDataType Variables;
        this->InitializeElementData(Variables, rCurrentProcessInfo);

        //reading integration points
        for (unsigned int PointNumber = 0; PointNumber < mConstitutiveLawVector.size(); PointNumber++)
        {
            //compute element kinematics B, F, DN_DX ...
            this->CalculateKinematics(Variables, PointNumber);

            //to take in account previous step writing
            if (this->Is(SolidElement::FINALIZED_STEP))
            {
                this->GetHistoricalVariables(Variables, PointNumber);
                noalias(Variables.H) = prod(Variables.F, Variables.F0);
            }

            //Compute Green-Lagrange Strain
            if (rVariable == GREEN_LAGRANGE_STRAIN_VECTOR)
                this->CalculateGreenLagrangeStrain(Variables.H, Variables.StrainVector);
            else
                this->CalculateAlmansiStrain(Variables.H, Variables.StrainVector);

            if (rValues[PointNumber].size() != Variables.StrainVector.size())
                rValues[PointNumber].resize(Variables.StrainVector.size(), false);

            rValues[PointNumber] = Variables.StrainVector;
        }
    }
    else
    {
        SolidElement::CalculateOnIntegrationPoints(rVariable, rValues, rCurrentProcessInfo);
    }

    KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void LargeDisplacementElement::CalculateOnIntegrationPoints(const Variable<Matrix> &rVariable, std::vector<Matrix> &rValues, const ProcessInfo &rCurrentProcessInfo)
{
    KRATOS_TRY

    SolidElement::CalculateOnIntegrationPoints(rVariable, rValues, rCurrentProcessInfo);

    KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

int LargeDisplacementElement::Check(const ProcessInfo &rCurrentProcessInfo) const
{
    KRATOS_TRY

    // Perform base element checks
    int ErrorCode = 0;
    ErrorCode = SolidElement::Check(rCurrentProcessInfo);

    // Check compatibility with the constitutive law
    ConstitutiveLaw::Features LawFeatures;
    this->GetProperties().GetValue(CONSTITUTIVE_LAW)->GetLawFeatures(LawFeatures);

    // Check that the constitutive law has the correct dimension
    const SizeType dimension = GetGeometry().WorkingSpaceDimension();
    if (dimension == 2)
    {
        if (LawFeatures.mOptions.IsNot(ConstitutiveLaw::PLANE_STRAIN_LAW) && LawFeatures.mOptions.IsNot(ConstitutiveLaw::PLANE_STRESS_LAW) && LawFeatures.mOptions.IsNot(ConstitutiveLaw::AXISYMMETRIC_LAW))
            KRATOS_ERROR << "wrong constitutive law used. This is a 2D element. Expected plane state or axisymmetric :: element id = " << this->Id() << std::endl;
    }

    return ErrorCode;

    KRATOS_CATCH("");
}

//************************************************************************************
//************************************************************************************

void LargeDisplacementElement::save(Serializer &rSerializer) const
{
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, SolidElement)
}

void LargeDisplacementElement::load(Serializer &rSerializer)
{
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, SolidElement)
}

} // Namespace Kratos
