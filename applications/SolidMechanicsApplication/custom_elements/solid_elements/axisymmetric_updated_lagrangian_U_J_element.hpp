//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:         JMCarbonell $
//   Maintained by:       $Maintainer:                JMC $
//   Date:                $Date:                July 2021 $
//
//

#if !defined(KRATOS_AXISYMMETRIC_UPDATED_LAGRANGIAN_U_J_ELEMENT_HPP_INCLUDED)
#define KRATOS_AXISYMMETRIC_UPDATED_LAGRANGIAN_U_J_ELEMENT_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_elements/solid_elements/updated_lagrangian_U_J_element.hpp"

namespace Kratos
{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Axisymmetric Updated Lagrangian U-P Element for 2D geometries. For Linear Triangles

/**
 * Implements a Large Displacement Lagrangian definition for structural analysis.
 * This works for Linear Triangles
 */

class KRATOS_API(SOLID_MECHANICS_APPLICATION) AxisymmetricUpdatedLagrangianUJElement
    : public UpdatedLagrangianUJElement
{
public:
   ///@name Type Definitions
   ///@{
   ///Reference type definition for constitutive laws
   typedef ConstitutiveLaw ConstitutiveLawType;
   ///Pointer type for constitutive laws
   typedef ConstitutiveLawType::Pointer ConstitutiveLawPointerType;
   ///StressMeasure from constitutive laws
   typedef ConstitutiveLawType::StressMeasure StressMeasureType;
   ///Type definition for integration methods
   typedef GeometryData::IntegrationMethod IntegrationMethod;
   ///Type for size
   typedef GeometryData::SizeType SizeType;
   ///Type for element variables
   typedef UpdatedLagrangianUJElement::ElementDataType ElementDataType;

   /// Counted pointer of AxisymmetricUpdatedLagrangianUJElement
   KRATOS_CLASS_INTRUSIVE_POINTER_DEFINITION(AxisymmetricUpdatedLagrangianUJElement);

   ///@}
   ///@name Life Cycle
   ///@{

   /// Default constructors
   AxisymmetricUpdatedLagrangianUJElement(IndexType NewId, GeometryType::Pointer pGeometry);

   AxisymmetricUpdatedLagrangianUJElement(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties);

   ///Copy constructor
   AxisymmetricUpdatedLagrangianUJElement(AxisymmetricUpdatedLagrangianUJElement const &rOther);

   /// Destructor.
   ~AxisymmetricUpdatedLagrangianUJElement() override;

   ///@}
   ///@name Operators
   ///@{

   /// Assignment operator.
   AxisymmetricUpdatedLagrangianUJElement &operator=(AxisymmetricUpdatedLagrangianUJElement const &rOther);

   ///@}
   ///@name Operations
   ///@{
   /**
     * Returns the currently selected integration method
     * @return current integration method selected
     */
   /**
     * creates a new total lagrangian updated element pointer
     * @param NewId: the ID of the new element
     * @param ThisNodes: the nodes of the new element
     * @param pProperties: the properties assigned to the new element
     * @return a Pointer to the new element
     */
   Element::Pointer Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const override;

   /**
     * clones the selected element variables, creating a new one
     * @param NewId: the ID of the new element
     * @param ThisNodes: the nodes of the new element
     * @param pProperties: the properties assigned to the new element
     * @return a Pointer to the new element
     */
   Element::Pointer Clone(IndexType NewId, NodesArrayType const &ThisNodes) const override;

   //************* STARTING - ENDING  METHODS

   /**
      * Called to initialize the element.
      * Must be called before any calculation is done
      */
   void Initialize(const ProcessInfo &rCurrentProcessInfo) override;

   //************* COMPUTING  METHODS

   /**
     * This function provides the place to perform checks on the completeness of the input.
     * It is designed to be called only once (or anyway, not often) typically at the beginning
     * of the calculations, so to verify that nothing is missing from the input
     * or that no common error is found.
     * @param rCurrentProcessInfo
     */
   int Check(const ProcessInfo &rCurrentProcessInfo) const override;

   ///@}
   ///@name Access
   ///@{

   ///@}
   ///@name Inquiry
   ///@{
   ///@}
   ///@name Input and output
   ///@{
   ///@}
   ///@name Friends
   ///@{
   ///@}

protected:
   ///@name Protected static Member Variables
   ///@{
   ///@}
   ///@name Protected member Variables
   ///@{

   ///@}
   ///@name Protected Operators
   ///@{

   AxisymmetricUpdatedLagrangianUJElement() : UpdatedLagrangianUJElement()
   {
   }

   ///@}
   ///@name Protected Operations
   ///@{

   /**
    * Calculation of the Integration Weight
    */
   double &CalculateIntegrationWeight(ElementDataType &rVariables, double &rIntegrationWeight) override;


   /**
     * Calculation of the Material Stiffness Matrix. Kuum = BT * C * B
     */
   void CalculateAndAddKuum(MatrixType &rLeftHandSideMatrix,
                            ElementDataType &rVariables,
                            double &rIntegrationWeight) const override;

   /**
     * Calculation of the Geometric Stiffness Matrix. Kuug = BT * S
     */
   void CalculateAndAddKuug(MatrixType &rLeftHandSideMatrix,
                            ElementDataType &rVariables,
                            double &rIntegrationWeight) const override;

   /**
     * Calculation of the KuJ matrix
     */
   void CalculateAndAddKuJ(MatrixType &rLeftHandSideMatrix,
                           ElementDataType &rVariables,
                           double &rIntegrationWeight) override;

   /**
     * Calculation of the KJu matrix
     */
   void CalculateAndAddKJu(MatrixType &rLeftHandSideMatrix,
                           ElementDataType &rVariables,
                           double &rIntegrationWeight) override;

   /**
     * Calculation of the KJJ matrix
     */
   void CalculateAndAddKJJ(MatrixType &rLeftHandSideMatrix,
                           ElementDataType &rVariables,
                           double &rIntegrationWeight) override;

   /**
      * Calculation of the KJJ Stabilization Term matrix
      */
   void CalculateAndAddKJJStab(MatrixType &rLeftHandSideMatrix,
                               ElementDataType &rVariables,
                               double &rIntegrationWeight) override;

   /**
     * Calculation of the Internal Forces due to Jacobian-Balance
     */
   void CalculateAndAddJacobianForces(VectorType &rRightHandSideVector,
                                      ElementDataType &rVariables,
                                      double &rIntegrationWeight) override;

   /**
     * Calculation of the Internal Forces due to Pressure-Balance
     */
   void CalculateAndAddStabilizedJacobian(VectorType &rRightHandSideVector,
                                          ElementDataType &rVariables,
                                          double &rIntegrationWeight) override;

   /**
     * Initialize Element General Variables
     */
   void InitializeElementData(ElementDataType &rVariables,
                              const ProcessInfo &rCurrentProcessInfo) const override;

   /**
     * Finalize Element Internal Variables
     */
   void FinalizeStepVariables(ElementDataType &rVariables,
                              const double &rPointNumber) override;

   /**
     * Calculate Element Kinematics
     */
   void CalculateKinematics(ElementDataType &rVariables,
                            const double &rPointNumber) const override;

   /**
     * Calculate Radius in the current and deformed geometry
     */
   void CalculateRadius(double &rCurrentRadius,
                        double &rReferenceRadius,
                        const Vector &rN) const;

   /**
     * Calculation of the Deformation Gradient F
     */
   void CalculateDeformationGradient(Matrix &rF,
                                     const Matrix &rDN_DX,
                                     const Matrix &rDeltaPosition,
                                     const double &rCurrentRadius,
                                     const double &rReferenceRadius) const;

   /**
     * Calculation of the Deformation Matrix  BL
     */
   void CalculateDeformationMatrix(Matrix &rB,
                                   const Matrix &rDN_DX,
                                   const Vector &rN,
                                   const double &rCurrentRadius) const;

   /**
     * Get the Historical Deformation Gradient to calculate after finalize the step
     */
   void GetHistoricalVariables(ElementDataType &rVariables,
                               const double &rPointNumber)  const override;

   /**
     * Calculation of the Green Lagrange Strain Vector
     */
   void CalculateGreenLagrangeStrain(const Matrix &rF,
                                     Vector &rStrainVector) override;

   /**
     * Calculation of the Almansi Strain Vector
     */
   void CalculateAlmansiStrain(const Matrix &rF,
                               Vector &rStrainVector) override;
  /**
   * Set Variables of the Element to the Parameters of the Constitutive Law
   */
   void SetElementData(ElementDataType &rVariables,
                       ConstitutiveLaw::Parameters &rValues,
                       const int &rPointNumber) override;

   /**
     * Get element voigt size
     */
   SizeType GetVoigtSize() const override;


   ///@}
   ///@name Protected  Access
   ///@{
   ///@}
   ///@name Protected Inquiry
   ///@{
   ///@}
   ///@name Protected LifeCycle
   ///@{
   ///@}

private:
   ///@name Static Member Variables
   ///@{
   ///@}
   ///@name Member Variables
   ///@{
   ///@}
   ///@name Private Operators
   ///@{
   ///@}
   ///@name Private Operations
   ///@{
   ///@}
   ///@name Private  Access
   ///@{
   ///@}
   ///@}
   ///@name Serialization
   ///@{
   friend class Serializer;

   // A private default constructor necessary for serialization

   void save(Serializer &rSerializer) const override;

   void load(Serializer &rSerializer) override;

   ///@name Private Inquiry
   ///@{
   ///@}
   ///@name Un accessible methods
   ///@{
   ///@}

}; // Class AxisymmetricUpdatedLagrangianUJElement

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
///@}

} // namespace Kratos.
#endif // KRATOS_AXISYMMETRIC_UPDATED_LAGRANGIAN_U_J_ELEMENT_HPP_INCLUDED  defined
