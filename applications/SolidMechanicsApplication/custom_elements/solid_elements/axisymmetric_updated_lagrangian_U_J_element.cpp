//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:         JMCarbonell $
//   Maintained by:       $Maintainer:                JMC $
//   Date:                $Date:                July 2021 $
//
//

// System includes

// External includes

// Project includes
#include "custom_elements/solid_elements/axisymmetric_updated_lagrangian_U_J_element.hpp"
#include "solid_mechanics_application_variables.h"

namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUJElement::AxisymmetricUpdatedLagrangianUJElement(IndexType NewId, GeometryType::Pointer pGeometry)
    : UpdatedLagrangianUJElement(NewId, pGeometry)
{
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUJElement::AxisymmetricUpdatedLagrangianUJElement(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : UpdatedLagrangianUJElement(NewId, pGeometry, pProperties)
{
    //mThisIntegrationMethod = GetGeometry().GetDefaultIntegrationMethod();
    mThisIntegrationMethod = GeometryData::IntegrationMethod::GI_GAUSS_1;
    //mThisIntegrationMethod = GeometryData::IntegrationMethod::GI_GAUSS_2;
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUJElement::AxisymmetricUpdatedLagrangianUJElement(AxisymmetricUpdatedLagrangianUJElement const &rOther)
    : UpdatedLagrangianUJElement(rOther)
{
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUJElement &AxisymmetricUpdatedLagrangianUJElement::operator=(AxisymmetricUpdatedLagrangianUJElement const &rOther)
{
    UpdatedLagrangianUJElement::operator=(rOther);
    return *this;
}

//*********************************OPERATIONS*****************************************
//************************************************************************************

Element::Pointer AxisymmetricUpdatedLagrangianUJElement::Create(IndexType NewId, NodesArrayType const &rThisNodes, PropertiesType::Pointer pProperties) const
{
    return Kratos::make_intrusive<AxisymmetricUpdatedLagrangianUJElement>(NewId, GetGeometry().Create(rThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************

Element::Pointer AxisymmetricUpdatedLagrangianUJElement::Clone(IndexType NewId, NodesArrayType const &rThisNodes) const
{

    AxisymmetricUpdatedLagrangianUJElement NewElement(NewId, GetGeometry().Create(rThisNodes), pGetProperties());

    //-----------//

    NewElement.mThisIntegrationMethod = mThisIntegrationMethod;

    if (NewElement.mConstitutiveLawVector.size() != mConstitutiveLawVector.size())
    {
        NewElement.mConstitutiveLawVector.resize(mConstitutiveLawVector.size());

        if (NewElement.mConstitutiveLawVector.size() != NewElement.GetGeometry().IntegrationPointsNumber())
            KRATOS_THROW_ERROR(std::logic_error, "constitutive law not has the correct size ", NewElement.mConstitutiveLawVector.size());
    }

    for (unsigned int i = 0; i < mConstitutiveLawVector.size(); i++)
    {
        NewElement.mConstitutiveLawVector[i] = mConstitutiveLawVector[i]->Clone();
    }

    //-----------//

    if (NewElement.mDeformationGradientF0.size() != mDeformationGradientF0.size())
        NewElement.mDeformationGradientF0.resize(mDeformationGradientF0.size());

    if (NewElement.mDeformationGradientJ0.size() != mDeformationGradientJ0.size())
        NewElement.mDeformationGradientJ0.resize(mDeformationGradientJ0.size());

    for (unsigned int i = 0; i < mDeformationGradientF0.size(); i++)
    {
        NewElement.mDeformationGradientF0[i] = mDeformationGradientF0[i];
        NewElement.mDeformationGradientJ0[i] = mDeformationGradientJ0[i];
    }

    NewElement.mDeterminantF0 = mDeterminantF0;
    NewElement.mDeterminantJ0 = mDeterminantJ0;

    NewElement.SetData(this->GetData());
    NewElement.SetFlags(this->GetFlags());

    return Kratos::make_intrusive<AxisymmetricUpdatedLagrangianUJElement>(NewElement);
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUJElement::~AxisymmetricUpdatedLagrangianUJElement()
{
}

//************* STARTING - ENDING  METHODS
//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJElement::Initialize(const ProcessInfo &rCurrentProcessInfo)
{
    KRATOS_TRY

    UpdatedLagrangianUJElement::Initialize(rCurrentProcessInfo);

    SizeType integration_points_number = GetGeometry().IntegrationPointsNumber(mThisIntegrationMethod);

    //Resize historic deformation gradient
    if (mDeformationGradientF0.size() != integration_points_number)
        mDeformationGradientF0.resize(integration_points_number);

    if (mDeterminantF0.size() != integration_points_number)
        mDeterminantF0.resize(integration_points_number, false);

    if (mDeformationGradientJ0.size() != integration_points_number)
      mDeformationGradientJ0.resize(integration_points_number);

    if (mDeterminantJ0.size() != integration_points_number)
      mDeterminantJ0.resize(integration_points_number, false);


    for (unsigned int PointNumber = 0; PointNumber < integration_points_number; PointNumber++)
    {
        mDeterminantF0[PointNumber] = 1;
        mDeformationGradientF0[PointNumber].resize(3, 3, false);
        noalias(mDeformationGradientF0[PointNumber]) = IdentityMatrix(3);
        mDeterminantJ0[PointNumber] = 1;
        mDeformationGradientJ0[PointNumber].resize(3, 3, false);
        noalias(mDeformationGradientJ0[PointNumber]) = IdentityMatrix(3);
    }

    //ATTENTION initialize nodal variables(parallelism)
    const SizeType number_of_nodes = GetGeometry().size();
    for (SizeType i = 0; i < number_of_nodes; ++i)
    {
      GetGeometry()[i].SetLock();
      GetGeometry()[i].FastGetSolutionStepValue(JACOBIAN) = 1.0;
      GetGeometry()[i].FastGetSolutionStepValue(JACOBIAN, 1) = 1.0;
      GetGeometry()[i].UnSetLock();
    }

    //stabilization factor
    if (!GetProperties().Has(STABILIZATION_FACTOR_J))
      KRATOS_ERROR << "STABILIZATION_FACTOR_J not defined in properties " << std::endl;

    KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJElement::InitializeElementData(ElementDataType &rVariables, const ProcessInfo &rCurrentProcessInfo) const
{
    KRATOS_TRY

    const SizeType number_of_nodes = GetGeometry().size();
    const SizeType dimension = GetGeometry().WorkingSpaceDimension();
    const unsigned int voigt_size = 4;

    rVariables.Initialize(voigt_size, dimension, number_of_nodes);

    rVariables.F.resize(3, 3, false);
    noalias(rVariables.F) = IdentityMatrix(3);
    rVariables.F0.resize(3, 3, false);
    noalias(rVariables.F0) = IdentityMatrix(3);

    rVariables.H.resize(3, 3, false);
    noalias(rVariables.H) = IdentityMatrix(3);

    //set variables including all integration points values

    //reading shape functions
    rVariables.SetShapeFunctions(GetGeometry().ShapeFunctionsValues(mThisIntegrationMethod));

    //reading shape functions local gradients
    rVariables.SetShapeFunctionsGradients(GetGeometry().ShapeFunctionsLocalGradients(mThisIntegrationMethod));

    //set process info
    rVariables.SetProcessInfo(rCurrentProcessInfo);

    //calculating the current jacobian from cartesian coordinates to parent coordinates for all integration points [dx_n+1/d£]
    rVariables.j = GetGeometry().Jacobian(rVariables.j, mThisIntegrationMethod);

    //Calculate Delta Position
    ElementUtilities::CalculateDeltaPosition(rVariables.DeltaPosition, this->GetGeometry());

    //calculating the reference jacobian from cartesian coordinates to parent coordinates for all integration points [dx_n/d£]
    rVariables.J = GetGeometry().Jacobian(rVariables.J, mThisIntegrationMethod, rVariables.DeltaPosition);

    KRATOS_CATCH("")
}
//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJElement::SetElementData(ElementDataType &rVariables,
                                                            ConstitutiveLaw::Parameters &rValues,
                                                            const int &rPointNumber)
{
  KRATOS_TRY

  //set previous step for output print purposes
  if (this->Is(SolidElement::FINALIZED_STEP))
  {
    this->GetHistoricalVariables(rVariables, rPointNumber);
  }

  //check inverted element
  this->CheckElementData(rVariables, rPointNumber);

  rValues.SetStrainVector(rVariables.StrainVector);
  rValues.SetStressVector(rVariables.StressVector);
  rValues.SetConstitutiveMatrix(rVariables.ConstitutiveMatrix);
  rValues.SetShapeFunctionsDerivatives(rVariables.DN_DX);
  rValues.SetShapeFunctionsValues(rVariables.N);

  //calculate nodal deformation gradient
  const SizeType number_of_nodes = GetGeometry().size();

  // Compute F and detF(from 0 to n+1) : store it in H variable and detH
  rVariables.detH = rVariables.detF * rVariables.detF0;
  noalias(rVariables.H) = prod(rVariables.F, rVariables.F0);

  // Calculate integration point JACOBIAN (total deformation gradient)
  double detJ = 0;
  for (SizeType i = 0; i < number_of_nodes; ++i)
    detJ += rVariables.N[i] * GetGeometry()[i].FastGetSolutionStepValue(JACOBIAN);

  //add the effect of the interpolation
  double power = 0.5;
  rVariables.H *= pow(detJ / rVariables.detH, power);
  rVariables.detH = detJ;

  double detF0;
  Matrix invF0;
  MathUtils<double>::InvertMatrix(rVariables.F0, invF0, detF0);

  if (!this->Is(SolidElement::FINALIZED_STEP))
  {
    detJ = 0;
    for (SizeType i = 0; i < number_of_nodes; ++i)
      detJ += rVariables.N[i] * GetGeometry()[i].FastGetSolutionStepValue(JACOBIAN, 1);
  }

  invF0 /= pow(detJ / rVariables.detF0, power);

  Matrix F(3,3);
  noalias(F) = prod(rVariables.H, invF0);
  noalias(rVariables.H) = prod(F, mDeformationGradientJ0[rPointNumber]);
  //calculate nodal deformation gradient

  //set deformation gradient
  rValues.SetDeterminantF(rVariables.detH);
  rValues.SetDeformationGradientF(rVariables.H);

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJElement::FinalizeStepVariables(ElementDataType &rVariables, const double &rPointNumber)
{
    //update internal (historical) variables
    mDeterminantF0[rPointNumber] = rVariables.detF * rVariables.detF0;
    noalias(mDeformationGradientF0[rPointNumber]) = prod(rVariables.F, rVariables.F0);

    mDeterminantJ0[rPointNumber] = rVariables.detH;
    noalias(mDeformationGradientJ0[rPointNumber]) = rVariables.H;
}

//*********************************COMPUTE KINEMATICS*********************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJElement::CalculateKinematics(ElementDataType &rVariables,
                                                               const double &rPointNumber) const

{
    KRATOS_TRY

    //Get the parent coodinates derivative [dN/d£]
    const GeometryType::ShapeFunctionsGradientsType &DN_De = rVariables.GetShapeFunctionsGradients();

    //Get the shape functions for the order of the integration method [N]
    const Matrix &Ncontainer = rVariables.GetShapeFunctions();

    //Parent to reference configuration
    rVariables.StressMeasure = ConstitutiveLaw::StressMeasure_Cauchy;

    //Calculating the inverse of the jacobian and the parameters needed [d£/dx_n]
    Matrix InvJ;
    MathUtils<double>::InvertMatrix(rVariables.J[rPointNumber], InvJ, rVariables.detJ);

    //Compute cartesian derivatives [dN/dx_n]
    noalias(rVariables.DN_DX) = prod(DN_De[rPointNumber], InvJ);

    //Set Shape Functions Values for this integration point
    noalias(rVariables.N) = matrix_row<const Matrix>(Ncontainer, rPointNumber);

    //Calculate IntegrationPoint radius
    CalculateRadius(rVariables.CurrentRadius, rVariables.ReferenceRadius, rVariables.N);

    //Current Deformation Gradient [dx_n+1/dx_n]
    CalculateDeformationGradient(rVariables.F, rVariables.DN_DX, rVariables.DeltaPosition, rVariables.CurrentRadius, rVariables.ReferenceRadius);

    //Determinant of the deformation gradient F
    rVariables.detF = MathUtils<double>::Det(rVariables.F);

    //Calculating the inverse of the jacobian and the parameters needed [d£/dx_n+1]
    Matrix Invj;
    MathUtils<double>::InvertMatrix(rVariables.j[rPointNumber], Invj, rVariables.detJ); //overwrites detJ

    //Compute cartesian derivatives [dN/dx_n+1]
    noalias(rVariables.DN_DX) = prod(DN_De[rPointNumber], Invj); //overwrites DX now is the current position dx

    //Determinant of the Deformation Gradient F0
    rVariables.detF0 = mDeterminantF0[rPointNumber];
    noalias(rVariables.F0) = mDeformationGradientF0[rPointNumber];

    //Compute the deformation matrix B
    CalculateDeformationMatrix(rVariables.B, rVariables.DN_DX, rVariables.N, rVariables.CurrentRadius);

    KRATOS_CATCH("")
}

//***********************************************************************************
//************************************************************************************

double &AxisymmetricUpdatedLagrangianUJElement::CalculateIntegrationWeight(ElementDataType &rVariables, double &rIntegrationWeight)
{
  KRATOS_TRY

  rIntegrationWeight *= 2.0 * Globals::Pi * rVariables.CurrentRadius;
  return rIntegrationWeight;

  KRATOS_CATCH("")
}

//*************************COMPUTE AXYSIMMETRIC RADIUS********************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJElement::CalculateRadius(double &rCurrentRadius,
                                                           double &rReferenceRadius,
                                                           const Vector &rN) const

{
    KRATOS_TRY

    const SizeType number_of_nodes = GetGeometry().PointsNumber();

    const SizeType dimension = GetGeometry().WorkingSpaceDimension();

    rCurrentRadius = 0;
    rReferenceRadius = 0;

    if (dimension == 2)
    {
        for (SizeType i = 0; i < number_of_nodes; i++)
        {
            rCurrentRadius += rN[i] * GetGeometry()[i].X();
            rReferenceRadius += rN[i] * (GetGeometry()[i].X() + GetGeometry()[i].FastGetSolutionStepValue(DISPLACEMENT_X, 1) - GetGeometry()[i].FastGetSolutionStepValue(DISPLACEMENT_X));
        }
    }

    if (dimension == 3)
    {
        std::cout << " AXISYMMETRIC case and 3D is not possible " << std::endl;
    }

    KRATOS_CATCH("")
}

//*************************COMPUTE DEFORMATION GRADIENT*******************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJElement::CalculateDeformationGradient(Matrix &rF,
                                                                        const Matrix &rDN_DX,
                                                                        const Matrix &rDeltaPosition,
                                                                        const double &rCurrentRadius,
                                                                        const double &rReferenceRadius) const
{
    KRATOS_TRY

    const SizeType number_of_nodes = GetGeometry().PointsNumber();
    const SizeType dimension = GetGeometry().WorkingSpaceDimension();

    rF = identity_matrix<double>(3);

    if (dimension == 2)
    {

        for (SizeType i = 0; i < number_of_nodes; i++)
        {
            rF(0, 0) += rDeltaPosition(i, 0) * rDN_DX(i, 0);
            rF(0, 1) += rDeltaPosition(i, 0) * rDN_DX(i, 1);
            rF(1, 0) += rDeltaPosition(i, 1) * rDN_DX(i, 0);
            rF(1, 1) += rDeltaPosition(i, 1) * rDN_DX(i, 1);
        }

        rF(2, 2) = rCurrentRadius / rReferenceRadius;
    }
    else if (dimension == 3)
    {

        std::cout << " AXISYMMETRIC case and 3D is not possible " << std::endl;
    }
    else
    {

        KRATOS_THROW_ERROR(std::invalid_argument, "something is wrong with the dimension", "");
    }

    KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJElement::CalculateDeformationMatrix(Matrix &rB,
                                                                      const Matrix &rDN_DX,
                                                                      const Vector &rN,
                                                                      const double &rCurrentRadius) const
{
    KRATOS_TRY

    const SizeType number_of_nodes = GetGeometry().PointsNumber();
    const SizeType dimension = GetGeometry().WorkingSpaceDimension();

    rB.clear(); //set all components to zero

    if (dimension == 2)
    {

        for (SizeType i = 0; i < number_of_nodes; i++)
        {
            unsigned int index = 2 * i;

            rB(0, index + 0) = rDN_DX(i, 0);
            rB(1, index + 1) = rDN_DX(i, 1);
            rB(2, index + 0) = rN[i] / rCurrentRadius;
            rB(3, index + 0) = rDN_DX(i, 1);
            rB(3, index + 1) = rDN_DX(i, 0);
        }
    }
    else if (dimension == 3)
    {

        std::cout << " AXISYMMETRIC case and 3D is not possible " << std::endl;
    }
    else
    {

        KRATOS_THROW_ERROR(std::invalid_argument, "something is wrong with the dimension", "");
    }

    KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJElement::CalculateGreenLagrangeStrain(const Matrix &rF,
                                                                        Vector &rStrainVector)
{
    KRATOS_TRY

    const SizeType dimension = GetGeometry().WorkingSpaceDimension();

    //Right Cauchy-Green Calculation
    Matrix C(3, 3);
    noalias(C) = prod(trans(rF), rF);

    if (dimension == 2)
    {

        //Green Lagrange Strain Calculation
        if (rStrainVector.size() != 4)
            rStrainVector.resize(4, false);

        rStrainVector[0] = 0.5 * (C(0, 0) - 1.00);

        rStrainVector[1] = 0.5 * (C(1, 1) - 1.00);

        rStrainVector[2] = 0.5 * (C(2, 2) - 1.00);

        rStrainVector[3] = C(0, 1); // xy
    }
    else if (dimension == 3)
    {

        std::cout << " AXISYMMETRIC case and 3D is not possible " << std::endl;
    }
    else
    {

        KRATOS_THROW_ERROR(std::invalid_argument, "something is wrong with the dimension", "");
    }

    KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJElement::CalculateAlmansiStrain(const Matrix &rF,
                                                                  Vector &rStrainVector)
{
    KRATOS_TRY

    const SizeType dimension = GetGeometry().WorkingSpaceDimension();

    //Left Cauchy-Green Calculation
    Matrix LeftCauchyGreen(rF.size1(), rF.size1());
    noalias(LeftCauchyGreen) = prod(rF, trans(rF));

    //Calculating the inverse of the jacobian
    Matrix InverseLeftCauchyGreen(rF.size1(), rF.size1());
    double det_b = 0;
    MathUtils<double>::InvertMatrix(LeftCauchyGreen, InverseLeftCauchyGreen, det_b);

    if (dimension == 2)
    {

        //Almansi Strain Calculation
        if (rStrainVector.size() != 4)
            rStrainVector.resize(4, false);

        rStrainVector[0] = 0.5 * (1.00 - InverseLeftCauchyGreen(0, 0));

        rStrainVector[1] = 0.5 * (1.00 - InverseLeftCauchyGreen(1, 1));

        rStrainVector[2] = 0.5 * (1.00 - InverseLeftCauchyGreen(2, 2));

        rStrainVector[3] = -InverseLeftCauchyGreen(0, 1); // xy
    }
    else if (dimension == 3)
    {

        std::cout << " AXISYMMETRIC case and 3D is not possible " << std::endl;
    }
    else
    {

        KRATOS_THROW_ERROR(std::invalid_argument, "something is wrong with the dimension", "")
    }

    KRATOS_CATCH("")
}


//******************************** JACOBIAN FORCES  **********************************
//************************************************************************************
void AxisymmetricUpdatedLagrangianUJElement::CalculateAndAddJacobianForces(VectorType &rRightHandSideVector,
                                                                           ElementDataType &rVariables,
                                                                           double &rIntegrationWeight)
{
   KRATOS_TRY

   const unsigned int number_of_nodes = GetGeometry().PointsNumber();
   unsigned int dimension = GetGeometry().WorkingSpaceDimension();

   unsigned int indexp = dimension;

   VectorType Fh = rRightHandSideVector;

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      for (unsigned int j = 0; j < number_of_nodes; j++)
      {

         const double &NodalJacobian = (GetGeometry()[j].GetSolutionStepValue(JACOBIAN));

         rRightHandSideVector[indexp] += rVariables.N[i] * rVariables.N[j] * NodalJacobian * rIntegrationWeight / rVariables.detF0;
      }

      rRightHandSideVector[indexp] -= rVariables.N[i] * rIntegrationWeight;

      indexp += (dimension + 1);
   }

   KRATOS_CATCH("")
}

//****************** STABILIZATION *********************************************************
//************************* defined in the Stab element ************************************

void AxisymmetricUpdatedLagrangianUJElement::CalculateAndAddStabilizedJacobian(VectorType &rRightHandSideVector,
                                                                               ElementDataType &rVariables,
                                                                               double &rIntegrationWeight)
{
   KRATOS_TRY

   const SizeType number_of_nodes = GetGeometry().PointsNumber();
   const SizeType dimension = GetGeometry().WorkingSpaceDimension();

   double AlphaStabilization;
   this->CalculateStabilizationParameter(AlphaStabilization, rVariables.GetProcessInfo());

   double consistent = 1;

   SizeType indexp = dimension;
   for (SizeType i = 0; i < number_of_nodes; ++i)
   {
     for (SizeType j = 0; j < number_of_nodes; ++j)
     {
       consistent = (-1.0) * AlphaStabilization;
       if (i == j)
         consistent = 2.0 * AlphaStabilization;

       const double &rNodalJacobian = GetGeometry()[j].FastGetSolutionStepValue(JACOBIAN);
       rRightHandSideVector[indexp] += consistent * rNodalJacobian * rIntegrationWeight / rVariables.detF0;
     }
     indexp += (dimension + 1);
   }

   KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJElement::CalculateAndAddKuum(MatrixType &rLeftHandSideMatrix,
                                                                 ElementDataType &rVariables,
                                                                 double &rIntegrationWeight) const
{
   KRATOS_TRY

   //assemble into rk the material uu contribution:
   const SizeType number_of_nodes = GetGeometry().PointsNumber();
   SizeType dimension = GetGeometry().WorkingSpaceDimension();
   double dimension_double = 3.0;

   Matrix ConstitutiveMatrix = rVariables.ConstitutiveMatrix;

   SizeType voigtsize = this->GetVoigtSize();

   Matrix DeviatoricTensor = ZeroMatrix(voigtsize, voigtsize);
   Vector Identity = ZeroVector(voigtsize);

   for (unsigned int i = 0; i < voigtsize; ++i)
   {
      DeviatoricTensor(i, i) = 1.0;
   }
   for (unsigned int i = 0; i < 3; i++)
   {
      Identity(i) = 1.0;
      for (unsigned int j = 0; j < 3; j++)
      {
         DeviatoricTensor(i, j) -= 1.0 / dimension_double;
      }
   }

   ConstitutiveMatrix = prod(ConstitutiveMatrix, DeviatoricTensor);

   Matrix AuxMatrix(voigtsize, voigtsize);
   noalias(AuxMatrix) = ZeroMatrix(voigtsize, voigtsize);

   for (unsigned int i = 0; i < voigtsize; i++)
   {
      for (unsigned int j = 0; j < voigtsize; j++)
      {
         ConstitutiveMatrix(i, j) += (1 - 2 / dimension_double) * rVariables.StressVector(i) * Identity(j);
         AuxMatrix(i, j) += rVariables.StressVector(i) * Identity(j);
      }
   }

   const SizeType MatSize = dimension * number_of_nodes;
   MatrixType Kuu(MatSize, MatSize);

   noalias(Kuu) = prod(trans(rVariables.B), rIntegrationWeight * Matrix(prod(ConstitutiveMatrix, rVariables.B)));

   unsigned int indexi = 0;
   unsigned int indexj = 0;
   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      for (unsigned int idim = 0; idim < dimension; idim++)
      {
         indexj = 0;
         for (unsigned int j = 0; j < number_of_nodes; j++)
         {
            for (unsigned int jdim = 0; jdim < dimension; jdim++)
            {
               rLeftHandSideMatrix(indexi + i, indexj + j) += Kuu(indexi, indexj);
               indexj++;
            }
         }
         indexi++;
      }
   }

   KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJElement::CalculateAndAddKuJ(MatrixType &rLeftHandSideMatrix,
                                                                ElementDataType &rVariables,
                                                                double &rIntegrationWeight)
{

   KRATOS_TRY

   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   double dimension_double = 3.0;

   Matrix ConstitutiveMatrix = rVariables.ConstitutiveMatrix;
   SizeType voigtsize = this->GetVoigtSize();

   // Trying to do it new
   Vector Identity = ZeroVector(voigtsize);
   for (unsigned int i = 0; i < 3; i++)
      Identity(i) = 1.0;

   Vector ConstVector = prod(ConstitutiveMatrix, Identity);
   ConstVector /= dimension_double;

   ConstVector += (2.0 / dimension_double - 1.0) * rVariables.StressVector;

   double ElementJacobian = 0.0;

   for (unsigned int i = 0; i < number_of_nodes; i++)
      ElementJacobian += GetGeometry()[i].GetSolutionStepValue(JACOBIAN) * rVariables.N[i];

   ConstVector /= ElementJacobian;

   Vector KuJ = prod(trans(rVariables.B), (ConstVector));

   Matrix SecondMatrix = ZeroMatrix(dimension * number_of_nodes, number_of_nodes);

   for (unsigned int i = 0; i < dimension * number_of_nodes; i++)
   {
      for (unsigned int j = 0; j < number_of_nodes; j++)
      {
         SecondMatrix(i, j) = KuJ(i) * rVariables.N[j];
      }
   }
   SecondMatrix *= rIntegrationWeight;

   // Add the matrix in its place
   MatrixType Kh = rLeftHandSideMatrix;
   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      for (unsigned int idim = 0; idim < dimension; idim++)
      {
         for (unsigned int j = 0; j < number_of_nodes; j++)
         {
            rLeftHandSideMatrix(i * (dimension + 1) + idim, (dimension + 1) * (j + 1) - 1) += SecondMatrix(i * (dimension) + idim, j);
         }
      }
   }


   KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJElement::CalculateAndAddKJu(MatrixType &rLeftHandSideMatrix,
                                                                ElementDataType &rVariables,
                                                                double &rIntegrationWeight)

{
   KRATOS_TRY

   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();

   MatrixType Kh = rLeftHandSideMatrix;

   //contributions to stiffness matrix calculated on the reference configuration
   unsigned int indexp = dimension;

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      for (unsigned int j = 0; j < number_of_nodes; j++)
      {
         int indexup = dimension * j + j;
         for (unsigned int k = 0; k < dimension; k++)
         {
            rLeftHandSideMatrix(indexp, indexup + k) += rVariables.N[i] * rVariables.DN_DX(j, k) * rIntegrationWeight;
            if (k == 0)
               rLeftHandSideMatrix(indexp, indexup + k) += rVariables.N[i] * rVariables.N[j] * (1.0 / rVariables.CurrentRadius) * rIntegrationWeight;
         }
      }
      indexp += (dimension + 1);
   }

   KRATOS_CATCH("")
}

// ^^^^^^^^^^^^^^^^^^^^^ KJJ ***************************************************
// ********************************************************************************
void AxisymmetricUpdatedLagrangianUJElement::CalculateAndAddKJJ(MatrixType &rLeftHandSideMatrix,
                                                                ElementDataType &rVariables,
                                                                double &rIntegrationWeight)
{
   KRATOS_TRY

   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();

   Matrix TotalF = prod(rVariables.F, rVariables.F0);

   //contributions to stiffness matrix calculated on the reference configuration
   unsigned int indexpi = dimension;

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      unsigned int indexpj = dimension;
      for (unsigned int j = 0; j < number_of_nodes; j++)
      {

         rLeftHandSideMatrix(indexpi, indexpj) -= rVariables.N[i] * rVariables.N[j] * rIntegrationWeight / rVariables.detF0;
         indexpj += (dimension + 1);
      }

      indexpi += (dimension + 1);
   }

   KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJElement::CalculateAndAddKJJStab(MatrixType &rLeftHandSideMatrix,
                                                                    ElementDataType &rVariables,
                                                                    double &rIntegrationWeight)
{
   KRATOS_TRY

   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();

   //contributions to stiffness matrix calculated on the reference configuration
   unsigned int indexpi = dimension;
   double consistent = 1.0;

   double AlphaStabilization;
   this->CalculateStabilizationParameter(AlphaStabilization, rVariables.GetProcessInfo());

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      unsigned int indexpj = dimension;
      for (unsigned int j = 0; j < number_of_nodes; j++)
      {
         consistent = (-1.0) * AlphaStabilization;
         if (indexpi == indexpj)
            consistent = 2.0 * AlphaStabilization;

         rLeftHandSideMatrix(indexpi, indexpj) -= consistent * rIntegrationWeight / (rVariables.detF0 / rVariables.detF); //2D

         indexpj += (dimension + 1);
      }

      indexpi += (dimension + 1);
   }

   KRATOS_CATCH("")
}


//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJElement::CalculateAndAddKuug(MatrixType &rLeftHandSideMatrix,
                                                               ElementDataType &rVariables,
                                                               double &rIntegrationWeight) const

{
    KRATOS_TRY

    const SizeType number_of_nodes = GetGeometry().size();
    const unsigned int dimension = GetGeometry().WorkingSpaceDimension();

    const int size = number_of_nodes * dimension;

    Matrix Kuu(size,size);
    noalias(Kuu) = ZeroMatrix(size,size);

    // axisymmetric geometric matrix
    double alpha1 = 0;
    double alpha2 = 0;
    double alpha3 = 0;

    unsigned int indexi = 0;
    unsigned int indexj = 0;

    for (SizeType i = 0; i < number_of_nodes; i++)
    {
        indexj = 0;
        for (SizeType j = 0; j < number_of_nodes; j++)
        {
            alpha1 = rVariables.DN_DX(j, 0) * (rVariables.DN_DX(i, 0) * rVariables.StressVector[0] + rVariables.DN_DX(i, 1) * rVariables.StressVector[3]);
            alpha2 = rVariables.DN_DX(j, 1) * (rVariables.DN_DX(i, 0) * rVariables.StressVector[3] + rVariables.DN_DX(i, 1) * rVariables.StressVector[1]);
            alpha3 = rVariables.N[i] * rVariables.N[j] * rVariables.StressVector[2] * (1.0 / rVariables.CurrentRadius * rVariables.CurrentRadius);

            Kuu(indexi, indexj) += (alpha1 + alpha2 + alpha3) * rIntegrationWeight;
            Kuu(indexi + 1, indexj + 1) += (alpha1 + alpha2) * rIntegrationWeight;

            indexj += 2;
        }

        indexi += 2;
    }

    //assemble into rLeftHandSideMatrix the geometric uu contribution:
    indexi = 0;
    indexj = 0;
    for (unsigned int i = 0; i < number_of_nodes; i++)
    {
      for (unsigned int idim = 0; idim < dimension; idim++)
      {
        indexj = 0;
        for (unsigned int j = 0; j < number_of_nodes; j++)
        {
          for (unsigned int jdim = 0; jdim < dimension; jdim++)
          {
            rLeftHandSideMatrix(indexi + i, indexj + j) += Kuu(indexi, indexj);
            indexj++;
          }
        }
        indexi++;
      }
    }

    KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJElement::GetHistoricalVariables(ElementDataType &rVariables, const double &rPointNumber) const
{
    UpdatedLagrangianUJElement::GetHistoricalVariables(rVariables, rPointNumber);

    rVariables.CurrentRadius = rVariables.ReferenceRadius;
}

//************************************************************************************
//************************************************************************************

SolidElement::SizeType  AxisymmetricUpdatedLagrangianUJElement::GetVoigtSize() const
{
  return 4;
}

//************************************************************************************
//************************************************************************************

int AxisymmetricUpdatedLagrangianUJElement::Check(const ProcessInfo &rCurrentProcessInfo) const
{
    KRATOS_TRY

    // Perform base element checks
    int ErrorCode = 0;
    ErrorCode = UpdatedLagrangianUJElement::Check(rCurrentProcessInfo);

    return ErrorCode;

    KRATOS_CATCH("");
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJElement::save(Serializer &rSerializer) const
{
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, UpdatedLagrangianUJElement)
}

void AxisymmetricUpdatedLagrangianUJElement::load(Serializer &rSerializer)
{
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, UpdatedLagrangianUJElement)
}

} // Namespace Kratos
