//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:           LMonforte $
//   Maintained by:       $Maintainer:                LMV $
//   Date:                $Date:            February 2022 $
//
//

#if !defined(KRATOS_SOIL_THERMAL_ELEMENT_HPP_INCLUDED)
#define KRATOS_SOIL_THERMAL_ELEMENT_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "includes/checks.h"
#include "includes/element.h"
#include "custom_utilities/element_utilities.hpp"

#include "custom_elements/thermal_elements/thermal_element.hpp"

namespace Kratos
{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

class KRATOS_API(SOLID_MECHANICS_APPLICATION) SoilThermalElement
    : public ThermalElement
{
public:
  ///@name Type Definitions_
  ///@{
  ///Reference type definition for constitutive laws
  typedef ConstitutiveLaw ConstitutiveLawType;
  ///Pointer type for constitutive laws
  typedef ConstitutiveLawType::Pointer ConstitutiveLawPointerType;
  ///StressMeasure from constitutive laws
  typedef ConstitutiveLawType::StressMeasure StressMeasureType;
  ///Type definition for integration methods
  typedef GeometryData::IntegrationMethod IntegrationMethod;

  /// Counted pointer of SoilThermalElement
  KRATOS_CLASS_INTRUSIVE_POINTER_DEFINITION(SoilThermalElement);


  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructors.
  SoilThermalElement(IndexType NewId, GeometryType::Pointer pGeometry);
  SoilThermalElement(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties);

  ///Copy constructor
  SoilThermalElement(SoilThermalElement const &rOther);

  /// Destructor.
  ~SoilThermalElement() override;

  ///@}
  ///@name Operators
  ///@{

  /// Assignment operator.
  SoilThermalElement &operator=(SoilThermalElement const &rOther);

  ///@}
  ///@name Operations
  ///@{

  /**
   * creates a new total lagrangian updated element pointer
   * @param NewId: the ID of the new element
   * @param ThisNodes: the nodes of the new element
   * @param pProperties: the properties assigned to the new element
   * @return a Pointer to the new element
   */
  Element::Pointer Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const override;

  /**
   * clones the selected element variables, creating a new one
   * @param NewId: the ID of the new element
   * @param ThisNodes: the nodes of the new element
   * @param pProperties: the properties assigned to the new element
   * @return a Pointer to the new element
   */
  //Element::Pointer Clone(IndexType NewId, NodesArrayType const& ThisNodes) const;

  //************* GETTING METHODS

  /**
   * Get a double  Value on the Element Constitutive Law
   */
  //GET
  /**
   * Get on rVariable a double Value from the Element Constitutive Law
   */
  void CalculateOnIntegrationPoints(const Variable<double> &rVariable, std::vector<double> &rValues, const ProcessInfo &rCurrentProcessInfo) override;

  /**
   * Get on rVariable a Vector Value from the Element Constitutive Law
   */
  void CalculateOnIntegrationPoints(const Variable<Vector> &rVariable, std::vector<Vector> &rValues, const ProcessInfo &rCurrentProcessInfo) override;

  /**
   * Get on rVariable a Matrix Value from the Element Constitutive Law
   */
  void CalculateOnIntegrationPoints(const Variable<Matrix> &rVariable, std::vector<Matrix> &rValues, const ProcessInfo &rCurrentProcessInfo) override;
  //************* STARTING - ENDING  METHODS



  //************* COMPUTING  METHODS

 

  //************************************************************************************
  //************************************************************************************
  /**
   * This function provides the place to perform checks on the completeness of the input.
   * It is designed to be called only once (or anyway, not often) typically at the beginning
   * of the calculations, so to verify that nothing is missing from the input
   * or that no common error is found.
   * @param rCurrentProcessInfo
   */
  int Check(const ProcessInfo &rCurrentProcessInfo) const override;

  //std::string Info() const;

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  //      virtual String Info() const;

  /// Print information about this object.
  //      virtual void PrintInfo(std::ostream& rOStream) const;

  /// Print object's data.
  //      virtual void PrintData(std::ostream& rOStream) const;
  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{

  /**
   * Currently selected integration methods
   */
  IntegrationMethod mThisIntegrationMethod;

  ///@}
  ///@name Protected Operators
  ///@{
  SoilThermalElement() : ThermalElement()
  {
  }

  /**
   * Calculates the elemental contributions
   * \f$ K^e = w\,B^T\,D\,B \f$ and
   * \f$ r^e \f$
   */
   void CalculateElementalSystem(MatrixType &rLeftHandSideMatrix,
                                        VectorType &rRightHandSideVector,
                                        const ProcessInfo &rCurrentProcessInfo,
                                        Flags &rCalculationFlags) override;
  ///@}
  ///@name Protected Operations
  ///@{

  /**
   * Calculate Variation of Thermal Properties
   */
  void CalculateThermalProperties(ElementData &rVariables) const;

  /**
   * Initialize Element General Variables
   */
  void InitializeElementData(ElementData &rVariables, const ProcessInfo &rCurrentProcessInfo) const override;


  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{
  ///@}

  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  void save(Serializer &rSerializer) const override;

  void load(Serializer &rSerializer) override;

  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{
  ///@}

}; // Class SoilThermalElement

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
///@}

} // namespace Kratos.
#endif // KRATOS_SOIL_THERMAL_ELEMENT_HPP_INCLUDED  defined
