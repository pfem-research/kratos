//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:         JMCarbonell $
//   Maintained by:       $Maintainer:                JMC $
//   Date:                $Date:                July 2013 $
//
//

// System includes

// External includes

// Project includes
#include "custom_elements/thermal_elements/thermal_element.hpp"

#include "solid_mechanics_application_variables.h"

namespace Kratos
{

/**
 * Flags related to the element computation
 */
KRATOS_CREATE_LOCAL_FLAG(ThermalElement, COMPUTE_RHS_VECTOR, 0);
KRATOS_CREATE_LOCAL_FLAG(ThermalElement, COMPUTE_LHS_MATRIX, 1);

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

ThermalElement::ThermalElement(IndexType NewId, GeometryType::Pointer pGeometry)
    : Element(NewId, pGeometry)
{
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

ThermalElement::ThermalElement(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : Element(NewId, pGeometry, pProperties)
{
  this->Set(THERMAL);
  mThisIntegrationMethod = GetGeometry().GetDefaultIntegrationMethod();
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

ThermalElement::ThermalElement(ThermalElement const &rOther)
    : Element(rOther), mThisIntegrationMethod(rOther.mThisIntegrationMethod)
{
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

ThermalElement &ThermalElement::operator=(ThermalElement const &rOther)
{
  Element::operator=(rOther);

  mThisIntegrationMethod = rOther.mThisIntegrationMethod;

  return *this;
}

//*********************************OPERATIONS*****************************************
//************************************************************************************

Element::Pointer ThermalElement::Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const
{
  return Kratos::make_intrusive<ThermalElement>(NewId, GetGeometry().Create(ThisNodes), pProperties);
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

ThermalElement::~ThermalElement()
{
}

//************************************************************************************
//************************************************************************************

ThermalElement::IntegrationMethod ThermalElement::GetIntegrationMethod() const
{
  return mThisIntegrationMethod;
}

//************************************************************************************
//************************************************************************************

void ThermalElement::GetDofList(DofsVectorType &rElementalDofList, const ProcessInfo &rCurrentProcessInfo) const
{
  rElementalDofList.resize(0);

  for (unsigned int i = 0; i < GetGeometry().size(); i++)
  {
    rElementalDofList.push_back(GetGeometry()[i].pGetDof(TEMPERATURE));
  }
}

//************************************************************************************
//************************************************************************************

void ThermalElement::EquationIdVector(EquationIdVectorType &rResult, const ProcessInfo &rCurrentProcessInfo) const
{
  unsigned int number_of_nodes = GetGeometry().size();

  if (rResult.size() != number_of_nodes)
    rResult.resize(number_of_nodes, false);

  for (unsigned int i = 0; i < number_of_nodes; i++)
  {
    rResult[i] = GetGeometry()[i].GetDof(TEMPERATURE).EquationId();
  }
}

//*********************************DISPLACEMENT***************************************
//************************************************************************************

void ThermalElement::GetValuesVector(Vector &rValues, int Step) const
{
  const unsigned int number_of_nodes = GetGeometry().size();
  unsigned int MatSize = number_of_nodes;

  if (rValues.size() != MatSize)
    rValues.resize(MatSize, false);

  for (unsigned int i = 0; i < number_of_nodes; i++)
  {
    rValues[i] = GetGeometry()[i].FastGetSolutionStepValue(TEMPERATURE, Step);
  }
}

//************************************VELOCITY****************************************
//************************************************************************************

void ThermalElement::GetFirstDerivativesVector(Vector &rValues, int Step) const
{
  const unsigned int number_of_nodes = GetGeometry().size();
  unsigned int MatSize = number_of_nodes;

  if (rValues.size() != MatSize)
    rValues.resize(MatSize, false);

  for (unsigned int i = 0; i < number_of_nodes; i++)
  {
    rValues[i] = 0;
  }
}

//*********************************ACCELERATION***************************************
//************************************************************************************

void ThermalElement::GetSecondDerivativesVector(Vector &rValues, int Step) const
{
  const unsigned int number_of_nodes = GetGeometry().size();
  unsigned int MatSize = number_of_nodes;

  if (rValues.size() != MatSize)
    rValues.resize(MatSize, false);

  for (unsigned int i = 0; i < number_of_nodes; i++)
  {
    rValues[i] = 0;
  }
}

//*********************************SET DOUBLE VALUE***********************************
//************************************************************************************

void ThermalElement::SetValuesOnIntegrationPoints( const Variable<double>& rVariable,
                                                  const std::vector<double>& rValues,
                                                  const ProcessInfo& rCurrentProcessInfo )
{
  KRATOS_TRY

  KRATOS_CATCH("")
}

//*********************************SET VECTOR VALUE***********************************
//************************************************************************************

void ThermalElement::SetValuesOnIntegrationPoints( const Variable<Vector>& rVariable,
                                                   const std::vector<Vector>& rValues,
                                                   const ProcessInfo& rCurrentProcessInfo )
{
  KRATOS_TRY

  KRATOS_CATCH("")
}

//*********************************SET MATRIX VALUE***********************************
//************************************************************************************

void ThermalElement::SetValuesOnIntegrationPoints( const Variable<Matrix>& rVariable,
                                                   const std::vector<Matrix>& rValues,
                                                   const ProcessInfo& rCurrentProcessInfo )
{
  KRATOS_TRY

  KRATOS_CATCH("")
}


void ThermalElement::CalculateOnIntegrationPoints( const Variable<double> &rVariable,
                                                   std::vector<double> &rValues,
                                                   const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  KRATOS_CATCH("")
}

//**********************************GET VECTOR VALUE**********************************
//************************************************************************************

void ThermalElement::CalculateOnIntegrationPoints( const Variable<Vector> &rVariable,
                                                   std::vector<Vector> &rValues,
                                                   const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  KRATOS_CATCH("")
}

//***********************************GET MATRIX VALUE*********************************
//************************************************************************************

void ThermalElement::CalculateOnIntegrationPoints( const Variable<Matrix> &rVariable,
                                                   std::vector<Matrix> &rValues,
                                                   const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void ThermalElement::Initialize(const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  // std::cout<<" Thermal Initialization "<<std::endl;

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void ThermalElement::InitializeSolutionStep(const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void ThermalElement::FinalizeSolutionStep(const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void ThermalElement::InitializeElementData(ElementData &rVariables, const ProcessInfo &rCurrentProcessInfo) const
{
  KRATOS_TRY

  const unsigned int number_of_nodes = GetGeometry().size();
  const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
  const unsigned int voigt_size = dimension * (dimension + 1) * 0.5;

  rVariables.Initialize(voigt_size, dimension, number_of_nodes);

  //set process info
  rVariables.SetProcessInfo(rCurrentProcessInfo);

  //reading shape functions
  rVariables.SetShapeFunctions(GetGeometry().ShapeFunctionsValues(mThisIntegrationMethod));

  //reading shape functions local gradients
  rVariables.SetShapeFunctionsGradients(GetGeometry().ShapeFunctionsLocalGradients(mThisIntegrationMethod));

  //calculating the current jacobian from cartesian coordinates to parent coordinates for all integration points [dx_n+1/d£]
  rVariables.j = GetGeometry().Jacobian(rVariables.j, mThisIntegrationMethod);

  //Calculate Delta Position
  ElementUtilities::CalculateDeltaPosition(rVariables.DeltaPosition, this->GetGeometry());

  //set variables including all integration points values

  //calculating the reference jacobian from cartesian coordinates to parent coordinates for all integration points [dx_n/d£]
  rVariables.J = GetGeometry().Jacobian(rVariables.J, mThisIntegrationMethod, rVariables.DeltaPosition);

  KRATOS_ERROR_IF(!GetProperties().Has(SPECIFIC_HEAT_CAPACITY) || !GetProperties().Has(THERMAL_CONDUCTIVITY)) << " Thermal element properties not assigned correctly " << std::endl;

  //Thermal properties
  rVariables.HeatCapacity = GetProperties()[SPECIFIC_HEAT_CAPACITY] * GetProperties()[DENSITY]; //s2/(mm·K) * [kg/mm3 = 1e-3 Ns2/mm4]
  rVariables.HeatConductivity = GetProperties()[THERMAL_CONDUCTIVITY];

  this->CalculateThermalProperties(rVariables);

  KRATOS_CATCH("")
}


//*********************************COMPUTE KINEMATICS*********************************
//************************************************************************************

void ThermalElement::CalculateKinematics(ElementData &rVariables,
                                         const double &rPointNumber) const

{
  KRATOS_TRY

  //Get the parent coodinates derivative [dN/d£]
  const GeometryType::ShapeFunctionsGradientsType &DN_De = rVariables.GetShapeFunctionsGradients();

  //Get the shape functions for the order of the integration method [N]
  const Matrix &Ncontainer = rVariables.GetShapeFunctions();

  //Parent to reference configuration
  rVariables.StressMeasure = ConstitutiveLaw::StressMeasure_Cauchy;

  //Calculating the inverse of the jacobian and the parameters needed [d£/dx_n]
  Matrix InvJ;
  MathUtils<double>::InvertMatrix(rVariables.J[rPointNumber], InvJ, rVariables.detJ);

  //Compute cartesian derivatives [dN/dx_n]
  noalias(rVariables.DN_DX) = prod(DN_De[rPointNumber], InvJ);

  //Calculating the inverse of the jacobian and the parameters needed [d£/dx_n+1]
  Matrix Invj;
  MathUtils<double>::InvertMatrix(rVariables.j[rPointNumber], Invj, rVariables.detJ); //overwrites detJ

  //Compute cartesian derivatives [dN/dx_n+1]
  noalias(rVariables.DN_DX) = prod(DN_De[rPointNumber], Invj); //overwrites DX now is the current position dx

  //Set Shape Functions Values for this integration point
  noalias(rVariables.N) = matrix_row<const Matrix>(Ncontainer, rPointNumber);

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void ThermalElement::CalculateThermalProperties(ElementData &rVariables) const
{
  bool TemperatureDependentLaw = false;
  if (GetProperties().Has(TEMPERATURE_DEPENDENT))
  {
    TemperatureDependentLaw = GetProperties()[TEMPERATURE_DEPENDENT];
  }

  if (TemperatureDependentLaw)
  {
    const unsigned int number_of_nodes = GetGeometry().size();

    //double ReferenceTemperature = GetProperties()[REFERENCE_TEMPERATURE];
    double ElementTemperature = 0;

    for (unsigned int j = 0; j < number_of_nodes; j++)
    {
      const double &NodalTemperature = GetGeometry()[j].FastGetSolutionStepValue(TEMPERATURE);
      ElementTemperature += rVariables.N[j] * NodalTemperature;
    }

    if ((ElementTemperature - 273.15) < 0)
    {
      ElementTemperature = 273.15;
      std::cout << " Temperature error on linear thermal properties " << std::endl;
    }

    double HeatCapacity_a = GetProperties()[SPECIFIC_HEAT_CAPACITY_A] * GetProperties()[DENSITY];
    double HeatCapacity_b = GetProperties()[SPECIFIC_HEAT_CAPACITY_B] * GetProperties()[DENSITY];

    rVariables.HeatCapacity = HeatCapacity_a * (ElementTemperature - 273.15) + HeatCapacity_b;

    double HeatConductivity_a = GetProperties()[THERMAL_CONDUCTIVITY_A];
    double HeatConductivity_b = GetProperties()[THERMAL_CONDUCTIVITY_B];

    rVariables.HeatConductivity = HeatConductivity_a * (ElementTemperature - 273.15) + HeatConductivity_b;

    //std::cout<<" HeatCapacity "<<rVariables.HeatCapacity<<" HeatConductivity "<<rVariables.HeatConductivity<<std::endl;
  }
}

//************************************************************************************
//************************************************************************************

void ThermalElement::InitializeSystemMatrices(MatrixType &rLeftHandSideMatrix,
                                              VectorType &rRightHandSideVector,
                                              Flags &rCalculationFlags)
{

  const unsigned int number_of_nodes = GetGeometry().size();

  //resizing as needed the LHS
  unsigned int MatSize = number_of_nodes;

  if (rCalculationFlags.Is(ThermalElement::COMPUTE_LHS_MATRIX)) //calculation of the matrix is required
  {
    if (rLeftHandSideMatrix.size1() != MatSize)
      rLeftHandSideMatrix.resize(MatSize, MatSize, false);

    noalias(rLeftHandSideMatrix) = ZeroMatrix(MatSize, MatSize); //resetting LHS
  }

  //resizing as needed the RHS
  if (rCalculationFlags.Is(ThermalElement::COMPUTE_RHS_VECTOR)) //calculation of the matrix is required

  {
    if (rRightHandSideVector.size() != MatSize)
      rRightHandSideVector.resize(MatSize, false);

    rRightHandSideVector = ZeroVector(MatSize); //resetting RHS
  }
}

//************************************************************************************
//************************************************************************************

void ThermalElement::CalculateElementalSystem(MatrixType &rLeftHandSideMatrix,
                                              VectorType &rRightHandSideVector,
                                              const ProcessInfo &rCurrentProcessInfo,
                                              Flags &rCalculationFlags)
{
  KRATOS_TRY

  //create and initialize element variables:
  ElementData Variables;
  this->InitializeElementData(Variables, rCurrentProcessInfo);

  //reading integration points
  const GeometryType::IntegrationPointsArrayType &integration_points = GetGeometry().IntegrationPoints(mThisIntegrationMethod);

  //mechanical evaluation of the material response (add a Element::Pointer as a member to increase speed)
  bool thermo_mechanical = false;

  std::vector<Vector> StressVector;

  std::vector<ConstitutiveLaw::Pointer> ConstitutiveLawVector;

  if (this->Has(MAIN_ELEMENT))
  {
    thermo_mechanical = true;

    Element &MechanicalElement = *this->GetValue(MAIN_ELEMENT).get();

    MechanicalElement.CalculateOnIntegrationPoints(CAUCHY_STRESS_VECTOR, StressVector, rCurrentProcessInfo);

    MechanicalElement.CalculateOnIntegrationPoints(CONSTITUTIVE_LAW, ConstitutiveLawVector, rCurrentProcessInfo);

    //std::cout << "Element["<<this->Id()<<"] Master["<<MechanicalElement.Id()<<"] "<<StressVector<<std::endl;

  }
  else
  {

    //std::cout << " NO master element for this thermal element " << this->Id() << std::endl;

    thermo_mechanical = false;

    const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
    const unsigned int voigt_size = dimension * (dimension + 1) * 0.5;

    ConstitutiveLawVector.resize(integration_points.size());
    StressVector.resize(integration_points.size());

    for (unsigned int i = 0; i < integration_points.size(); i++)
    {
      StressVector[i] = ZeroVector(voigt_size);
    }
  }

  //auxiliary terms
  double HeatSource = 0;

  for (unsigned int PointNumber = 0; PointNumber < integration_points.size(); PointNumber++)
  {
    //COMPUTE kinematics B,F,DN_DX ...
    this->CalculateKinematics(Variables, PointNumber);

    //calculating weights for integration on the "reference configuration"
    double IntegrationWeight = integration_points[PointNumber].Weight() * Variables.detJ;
    IntegrationWeight = this->CalculateIntegrationWeight(IntegrationWeight);

    if (rCalculationFlags.Is(ThermalElement::COMPUTE_LHS_MATRIX)) //calculation of the matrix is required
    {
      Variables.DeltaPlasticDissipation = 0;
      if (thermo_mechanical)
      {
        if (ConstitutiveLawVector[PointNumber]->Has(DELTA_PLASTIC_DISSIPATION))
        {
          ConstitutiveLawVector[PointNumber]->GetValue(DELTA_PLASTIC_DISSIPATION, Variables.DeltaPlasticDissipation);
          // if(Variables.DeltaPlasticDissipation!=0){
          // std::cout<<" thermo_mechanical element LHS ["<<this->Id()<<"]"<<std::endl;
          // std::cout<<" with DELTA_PLASTIC_DISSIPATION= "<<Variables.DeltaPlasticDissipation<<std::endl;
          // }
        }
      }

      this->CalculateAndAddLHS(rLeftHandSideMatrix, Variables, IntegrationWeight);
    }

    if (rCalculationFlags.Is(ThermalElement::COMPUTE_RHS_VECTOR)) //calculation of the vector is required
    {
      //contribution to external forces
      if (GetProperties().Has(HEAT_SOURCE))
      {
        HeatSource = GetProperties()[HEAT_SOURCE];
        // std::cout<<" HeatSource "<<HeatSource<<std::endl;
      }

      Variables.PlasticDissipation = 0;
      if (thermo_mechanical)
      {
        if (ConstitutiveLawVector[PointNumber]->Has(PLASTIC_DISSIPATION))
        {
          ConstitutiveLawVector[PointNumber]->GetValue(PLASTIC_DISSIPATION, Variables.PlasticDissipation);
          // if(Variables.DeltaPlasticDissipation!=0){
          //   std::cout<<" thermo_mechanical element RHS ["<<this->Id()<<"]"<<std::endl;
          //   std::cout<<" with PLASTIC_DISSIPATION= "<<Variables.PlasticDissipation<<std::endl;
          // }
        }
      }

      this->CalculateAndAddRHS(rRightHandSideVector, Variables, HeatSource, IntegrationWeight);
    }
  }

  // if( Variables.PlasticDissipation!=0 || Variables.DeltaPlasticDissipation!=0 )
  //   {
  	// std::cout<<" Thermal Element "<<this->Id()<<" [ dissipation:"<<Variables.PlasticDissipation<<", delta_dissipation:"<<Variables.DeltaPlasticDissipation<<"] "<<std::endl;
  	// std::cout<<" K "<<rLeftHandSideMatrix<<std::endl;
  	// std::cout<<" F "<<rRightHandSideVector<<std::endl;
    // }

  KRATOS_CATCH("")
}

//***********************COMPUTE LOCAL SYSTEM CONTRIBUTIONS***************************
//************************************************************************************

//************************************************************************************
//************************************************************************************

void ThermalElement::CalculateAndAddLHS(MatrixType &rLeftHandSideMatrix, ElementData &rVariables, double &rIntegrationWeight)
{
  //constant during the step
  this->CalculateAndAddKthermal(rLeftHandSideMatrix, rVariables, rIntegrationWeight);

  //constant during the step
  this->CalculateAndAddMthermal(rLeftHandSideMatrix, rVariables, rIntegrationWeight);

  //changes during the step
  this->CalculateAndAddHthermal(rLeftHandSideMatrix, rVariables, rIntegrationWeight);
}

//************************************************************************************
//************************************************************************************

void ThermalElement::CalculateAndAddRHS(VectorType &rRightHandSideVector, ElementData &rVariables, double &rHeatSource, double &rIntegrationWeight)
{

  // operation performed: rRightHandSideVector += ExtForce*IntegrationWeight
  this->CalculateAndAddExternalForces(rVariables, rRightHandSideVector, rHeatSource, rIntegrationWeight);

  // operation performed: rRightHandSideVector -= ThermalForce*IntegrationWeight
  this->CalculateAndAddThermalForces(rVariables, rRightHandSideVector, rIntegrationWeight);
}

//************************************************************************************
//************************************************************************************

void ThermalElement::CalculateRightHandSide(VectorType &rRightHandSideVector, const ProcessInfo &rCurrentProcessInfo)
{
  //calculation flags
  Flags CalculationFlags;
  CalculationFlags.Set(ThermalElement::COMPUTE_RHS_VECTOR);

  MatrixType LeftHandSideMatrix = Matrix();

  //Initialize sizes for the system components:
  this->InitializeSystemMatrices(LeftHandSideMatrix, rRightHandSideVector, CalculationFlags);

  //Calculate elemental system
  CalculateElementalSystem(LeftHandSideMatrix, rRightHandSideVector, rCurrentProcessInfo, CalculationFlags);
}

//************************************************************************************
//************************************************************************************

void ThermalElement::CalculateLeftHandSide(MatrixType &rLeftHandSideMatrix, const ProcessInfo &rCurrentProcessInfo)
{
  //calculation flags
  Flags CalculationFlags;
  CalculationFlags.Set(ThermalElement::COMPUTE_LHS_MATRIX);

  VectorType RightHandSideVector = Vector();

  //Initialize sizes for the system components:
  this->InitializeSystemMatrices(rLeftHandSideMatrix, RightHandSideVector, CalculationFlags);

  //Calculate elemental system
  CalculateElementalSystem(rLeftHandSideMatrix, RightHandSideVector, rCurrentProcessInfo, CalculationFlags);
}

//************************************************************************************
//************************************************************************************

void ThermalElement::CalculateLocalSystem(MatrixType &rLeftHandSideMatrix, VectorType &rRightHandSideVector, const ProcessInfo &rCurrentProcessInfo)
{

  //calculation flags
  Flags CalculationFlags;
  CalculationFlags.Set(ThermalElement::COMPUTE_LHS_MATRIX);
  CalculationFlags.Set(ThermalElement::COMPUTE_RHS_VECTOR);

  //Initialize sizes for the system components:
  this->InitializeSystemMatrices(rLeftHandSideMatrix, rRightHandSideVector, CalculationFlags);

  //Calculate elemental system
  CalculateElementalSystem(rLeftHandSideMatrix, rRightHandSideVector, rCurrentProcessInfo, CalculationFlags);
}

//************************************************************************************
//************************************************************************************

void ThermalElement::CalculateMassMatrix(MatrixType &rMassMatrix, const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void ThermalElement::CalculateDampingMatrix(MatrixType &rDampingMatrix, const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

double &ThermalElement::CalculateIntegrationWeight(double &rIntegrationWeight) const
{
  const unsigned int dimension = GetGeometry().WorkingSpaceDimension();

  if (dimension == 2)
  {
    if (this->GetProperties().Has(THICKNESS))
      rIntegrationWeight *= GetProperties()[THICKNESS];
  }

  return rIntegrationWeight;
}

//************************************************************************************
//************************************************************************************

void ThermalElement::CalculateAndAddExternalForces(ElementData &rVariables,
                                                   VectorType &rRightHandSideVector,
                                                   double &rHeatSource,
                                                   double &rIntegrationWeight)
{
  KRATOS_TRY

  unsigned int number_of_nodes = GetGeometry().PointsNumber();

  //VectorType Fh = rRightHandSideVector;

  for (unsigned int i = 0; i < number_of_nodes; i++)
  {
    rRightHandSideVector[i] += rIntegrationWeight * rVariables.N[i] * rHeatSource;
  }

  //std::cout<<std::endl;
  //std::cout<<" Fext "<<rRightHandSideVector-Fh<<std::endl;

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

inline void ThermalElement::CalculateAndAddThermalForces(ElementData &rVariables,
                                                         VectorType &rRightHandSideVector,
                                                         double &rIntegrationWeight)

{
  KRATOS_TRY

  const unsigned int number_of_nodes = GetGeometry().PointsNumber();
  const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
  const double &delta_time = rVariables.GetProcessInfo()[DELTA_TIME];
  //VectorType Fh = rRightHandSideVector;
  double DeltaTemperature = 0;
  double NodalTemperature = 0;

  for (unsigned int i = 0; i < number_of_nodes; i++)
  {
    //rRightHandSideVector[i] = 0;
    DeltaTemperature = GetGeometry()[i].FastGetSolutionStepValue(TEMPERATURE) - GetGeometry()[i].FastGetSolutionStepValue(TEMPERATURE, 1);

    rRightHandSideVector[i] += rVariables.N[i] * rVariables.PlasticDissipation * rIntegrationWeight;
    rRightHandSideVector[i] -= rVariables.N[i] * rVariables.HeatCapacity * (DeltaTemperature) * (1.0 / delta_time) * rIntegrationWeight;

    for (unsigned int j = 0; j < number_of_nodes; j++)
    {
      NodalTemperature = rVariables.HeatConductivity * rIntegrationWeight * GetGeometry()[j].FastGetSolutionStepValue(TEMPERATURE);

      for (unsigned int k = 0; k < dimension; k++)
      {
        rRightHandSideVector[i] -= NodalTemperature * (rVariables.DN_DX(i, k) * rVariables.DN_DX(j, k));
      }
    }
  }
  // std::cout<<std::endl;
  // std::cout<<" Fint ["<<GetValue(MAIN_ELEMENT)->Id()<<"] "<<rRightHandSideVector-Fh<<" [ Dissipation: "<<rVariables.PlasticDissipation<<" , HeatConductivity: "<<rVariables.HeatConductivity<<" , HeatCapacity: "<<rVariables.HeatCapacity<<" ]"<<" Nodal temperature "<<NodalTemperature<<" Delta temperature "<<DeltaTemperature<<std::endl;

  // if( rVariables.PlasticDissipation > 0 ){
  //   std::cout<<" ThermalElement Id["<<this->Id()<<"] Dissipation "<<rVariables.PlasticDissipation<<" Fint "<<rRightHandSideVector-Fh<<std::endl;
  //   for ( unsigned int i = 0; i < number_of_nodes; i++ )
  // 	{
  // 	  std::cout<<" Node["<<GetGeometry()[i].Id()<<"] DT: "<<GetGeometry()[i].FastGetSolutionStepValue(TEMPERATURE) - GetGeometry()[i].FastGetSolutionStepValue(TEMPERATURE,1)<<" T "<<GetGeometry()[i].FastGetSolutionStepValue(TEMPERATURE)<<" Capacity "<<rVariables.HeatCapacity * ( GetGeometry()[i].FastGetSolutionStepValue(TEMPERATURE) - GetGeometry()[i].FastGetSolutionStepValue(TEMPERATURE,1) ) * (1.0/delta_time)<<" Conductivity "<<rVariables.HeatConductivity<<std::endl;
  // 	}
  // }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void ThermalElement::CalculateAndAddHthermal(MatrixType &rLeftHandSideMatrix,
                                             ElementData &rVariables,
                                             double &rIntegrationWeight)

{
  KRATOS_TRY

  const unsigned int number_of_nodes = GetGeometry().PointsNumber();

  for (unsigned int i = 0; i < number_of_nodes; i++)
  {
    for (unsigned int j = 0; j < number_of_nodes; j++)
    {
      rLeftHandSideMatrix(i, j) -= rVariables.DeltaPlasticDissipation * rIntegrationWeight * rVariables.N[i] * rVariables.N[j];
    }
  }

  // std::cout<<std::endl;
  // std::cout<<" Htherm added "<<rLeftHandSideMatrix<<" [ DeltaDissipation : "<<rVariables.DeltaPlasticDissipation<<" ]"<<std::endl;

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void ThermalElement::CalculateAndAddKthermal(MatrixType &rLeftHandSideMatrix,
                                             ElementData &rVariables,
                                             double &rIntegrationWeight)

{
  KRATOS_TRY

  const unsigned int number_of_nodes = GetGeometry().PointsNumber();
  const unsigned int dimension = GetGeometry().WorkingSpaceDimension();

  for (unsigned int i = 0; i < number_of_nodes; i++)
  {
    for (unsigned int j = 0; j < number_of_nodes; j++)
    {
      for (unsigned int k = 0; k < dimension; k++)
      {
        rLeftHandSideMatrix(i, j) += rVariables.HeatConductivity * (rVariables.DN_DX(i, k) * rVariables.DN_DX(j, k)) * rIntegrationWeight;
      }
    }
  }

  // std::cout<<std::endl;
  // std::cout<<" Ktherm added "<<rLeftHandSideMatrix<<" [ head conductivity : "<<rVariables.HeatConductivity<<" ] "<<std::endl;

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void ThermalElement::CalculateAndAddMthermal(MatrixType &rLeftHandSideMatrix,
                                             ElementData &rVariables,
                                             double &rIntegrationWeight)

{
  KRATOS_TRY

  const unsigned int number_of_nodes = GetGeometry().PointsNumber();
  const double &delta_time = rVariables.GetProcessInfo()[DELTA_TIME];

  //Lumped Mass Matrix
  Vector LumpFact(number_of_nodes);
  noalias(LumpFact) = ZeroVector(number_of_nodes);

  LumpFact = GetGeometry().LumpingFactors(LumpFact);

  for (unsigned int i = 0; i < number_of_nodes; i++)
  {
    rLeftHandSideMatrix(i,i) += LumpFact[i] * rVariables.HeatCapacity * rIntegrationWeight * (1.0 / delta_time);
  }

  // Lumped Consistent Mass Matrix (triangle and tetrahedron)
  // const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
  // double consistent = 1.0;
  // for (unsigned int i = 0; i < number_of_nodes; i++)
  // {
  //   for (unsigned int j = 0; j < number_of_nodes; j++)
  //   {
  //     consistent = 0.0;
  //     if (i == j)
  //     {
  //       if (dimension == 2)
  //         consistent = 3;
  //       else
  //         consistent = 4;
  //     }

  //     rLeftHandSideMatrix(i, j) += consistent * rVariables.HeatCapacity * rVariables.N[i] * rVariables.N[j] * rIntegrationWeight * (1.0 / delta_time);
  //   }
  // }

  // std::cout<<std::endl;
  // std::cout<<" Mtherm added "<<rLeftHandSideMatrix<<" [ heatcapacity : "<<rVariables.HeatCapacity<<" ]"<<std::endl;

  KRATOS_CATCH("")
}


//************************************************************************************
//************************************************************************************
/**
 * This function provides the place to perform checks on the completeness of the input.
 * It is designed to be called only once (or anyway, not often) typically at the beginning
 * of the calculations, so to verify that nothing is missing from the input
 * or that no common error is found.
 * @param rCurrentProcessInfo
 */
int ThermalElement::Check(const ProcessInfo &rCurrentProcessInfo) const
{
  KRATOS_TRY

  // Perform base element checks
  int ErrorCode = 0;
  ErrorCode = Element::Check(rCurrentProcessInfo);

  return ErrorCode;

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void ThermalElement::save(Serializer &rSerializer) const
{
  KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, Element);
  int IntMethod = int(mThisIntegrationMethod);
  rSerializer.save("IntegrationMethod", IntMethod);
}

void ThermalElement::load(Serializer &rSerializer)
{
  KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, Element);
  int IntMethod;
  rSerializer.load("IntegrationMethod", IntMethod);
  mThisIntegrationMethod = IntegrationMethod(IntMethod);
}

} // Namespace Kratos
