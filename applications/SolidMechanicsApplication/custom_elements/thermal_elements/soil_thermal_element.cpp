//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:           LMonforte $
//   Maintained by:       $Maintainer:                LMV $
//   Date:                $Date:            February 2022 $
//
//

// System includes

// External includes

// Project includes
#include "custom_elements/thermal_elements/soil_thermal_element.hpp"

#include "solid_mechanics_application_variables.h"

// same equations than thermal element, however, the specific heat and conductivity are
// those typical of soils (that vary with the porosity... blah blah).
// i.e. I changed the "constitutive law". I am sure there is a better way to do it.

namespace Kratos
{


//******************************CONSTRUCTOR*******************************************
//************************************************************************************

SoilThermalElement::SoilThermalElement(IndexType NewId, GeometryType::Pointer pGeometry)
    : ThermalElement(NewId, pGeometry)
{
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

SoilThermalElement::SoilThermalElement(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : ThermalElement(NewId, pGeometry, pProperties)
{
  this->Set(THERMAL);
  mThisIntegrationMethod = GetGeometry().GetDefaultIntegrationMethod();
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

SoilThermalElement::SoilThermalElement(SoilThermalElement const &rOther)
    : ThermalElement(rOther), mThisIntegrationMethod(rOther.mThisIntegrationMethod)
{
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

SoilThermalElement &SoilThermalElement::operator=(SoilThermalElement const &rOther)
{
  ThermalElement::operator=(rOther);

  mThisIntegrationMethod = rOther.mThisIntegrationMethod;

  return *this;
}

//*********************************OPERATIONS*****************************************
//************************************************************************************

Element::Pointer SoilThermalElement::Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const
{
  return Kratos::make_intrusive<SoilThermalElement>(NewId, GetGeometry().Create(ThisNodes), pProperties);
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

SoilThermalElement::~SoilThermalElement()
{
}



void SoilThermalElement::CalculateOnIntegrationPoints( const Variable<double> &rVariable,
                                                   std::vector<double> &rValues,
                                                   const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  const unsigned int & integration_points_number = GetGeometry().IntegrationPointsNumber(this->mThisIntegrationMethod);

  if (rValues.size() != integration_points_number)
     rValues.resize(integration_points_number);

  if ( rVariable == SPECIFIC_HEAT_EQUIVALENT || rVariable == THERMAL_CONDUCTIVITY_EQUIVALENT) {
     ElementData Variables;
     this->InitializeElementData(Variables, rCurrentProcessInfo);

     std::vector<double> PorosityVector;

     if (this->Has(MAIN_ELEMENT)) {
        Element &MechanicalElement = *this->GetValue(MAIN_ELEMENT).get();
        MechanicalElement.CalculateOnIntegrationPoints(POROSITY, PorosityVector, rCurrentProcessInfo);
     }

     for (unsigned int PointNumber = 0; PointNumber < integration_points_number; PointNumber++) {
        //COMPUTE kinematics B,F,DN_DX ...
        this->CalculateKinematics(Variables, PointNumber);
        Variables.Porosity = PorosityVector[PointNumber];
        this->CalculateThermalProperties( Variables);
        if ( rVariable == SPECIFIC_HEAT_EQUIVALENT)
           rValues[PointNumber] = Variables.HeatCapacity;
        else
           rValues[PointNumber] = Variables.HeatConductivity;
     }
  }



  KRATOS_CATCH("")
}

//**********************************GET VECTOR VALUE**********************************
//************************************************************************************

void SoilThermalElement::CalculateOnIntegrationPoints( const Variable<Vector> &rVariable,
                                                   std::vector<Vector> &rValues,
                                                   const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  KRATOS_CATCH("")
}

//***********************************GET MATRIX VALUE*********************************
//************************************************************************************

void SoilThermalElement::CalculateOnIntegrationPoints( const Variable<Matrix> &rVariable,
                                                   std::vector<Matrix> &rValues,
                                                   const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void SoilThermalElement::InitializeElementData(ElementData &rVariables, const ProcessInfo &rCurrentProcessInfo) const
{
  KRATOS_TRY

  const unsigned int number_of_nodes = GetGeometry().size();
  const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
  const unsigned int voigt_size = dimension * (dimension + 1) * 0.5;

  rVariables.Initialize(voigt_size, dimension, number_of_nodes);

  //set process info
  rVariables.SetProcessInfo(rCurrentProcessInfo);

  //reading shape functions
  rVariables.SetShapeFunctions(GetGeometry().ShapeFunctionsValues(mThisIntegrationMethod));

  //reading shape functions local gradients
  rVariables.SetShapeFunctionsGradients(GetGeometry().ShapeFunctionsLocalGradients(mThisIntegrationMethod));

  //calculating the current jacobian from cartesian coordinates to parent coordinates for all integration points [dx_n+1/d£]
  rVariables.j = GetGeometry().Jacobian(rVariables.j, mThisIntegrationMethod);

  //Calculate Delta Position
  ElementUtilities::CalculateDeltaPosition(rVariables.DeltaPosition, this->GetGeometry());

  //set variables including all integration points values

  //calculating the reference jacobian from cartesian coordinates to parent coordinates for all integration points [dx_n/d£]
  rVariables.J = GetGeometry().Jacobian(rVariables.J, mThisIntegrationMethod, rVariables.DeltaPosition);

  KRATOS_ERROR_IF(!GetProperties().Has(SPECIFIC_HEAT_SOLID) || !GetProperties().Has(THERMAL_CONDUCTIVITY_SOLID)) << " Thermal element properties not assigned correctly " << std::endl;
  KRATOS_ERROR_IF(!GetProperties().Has(SPECIFIC_HEAT_WATER) || !GetProperties().Has(THERMAL_CONDUCTIVITY_WATER)) << " Thermal element properties not assigned correctly " << std::endl;

  //Thermal properties
  rVariables.HeatCapacity = GetProperties()[SPECIFIC_HEAT_CAPACITY] * GetProperties()[DENSITY]; //mm2/(s·K) * [kg/mm3 = 1e-3 Ns2/mm4]
  rVariables.HeatConductivity = GetProperties()[THERMAL_CONDUCTIVITY];

  this->CalculateThermalProperties(rVariables);

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void SoilThermalElement::CalculateElementalSystem(MatrixType &rLeftHandSideMatrix,
                                              VectorType &rRightHandSideVector,
                                              const ProcessInfo &rCurrentProcessInfo,
                                              Flags &rCalculationFlags)
{
  KRATOS_TRY

  //create and initialize element variables:
  ElementData Variables;
  this->InitializeElementData(Variables, rCurrentProcessInfo);

  //reading integration points
  const GeometryType::IntegrationPointsArrayType &integration_points = GetGeometry().IntegrationPoints(mThisIntegrationMethod);

  //mechanical evaluation of the material response (add a Element::Pointer as a member to increase speed)
  bool thermo_mechanical = false;

  std::vector<Vector> StressVector;

  std::vector<ConstitutiveLaw::Pointer> ConstitutiveLawVector;

  std::vector<double> PorosityVector;

  if (this->Has(MAIN_ELEMENT))
  {
    thermo_mechanical = true;

    Element &MechanicalElement = *this->GetValue(MAIN_ELEMENT).get();

    MechanicalElement.CalculateOnIntegrationPoints(CAUCHY_STRESS_VECTOR, StressVector, rCurrentProcessInfo);

    MechanicalElement.CalculateOnIntegrationPoints(CONSTITUTIVE_LAW, ConstitutiveLawVector, rCurrentProcessInfo);

    MechanicalElement.CalculateOnIntegrationPoints(POROSITY, PorosityVector, rCurrentProcessInfo);

    //std::cout << "Element["<<this->Id()<<"] Master["<<MechanicalElement.Id()<<"] "<<StressVector<<std::endl;

  }
  else
  {

    //std::cout << " NO master element for this thermal element " << this->Id() << std::endl;

    thermo_mechanical = false;

    const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
    const unsigned int voigt_size = dimension * (dimension + 1) * 0.5;

    ConstitutiveLawVector.resize(integration_points.size());
    StressVector.resize(integration_points.size());

    for (unsigned int i = 0; i < integration_points.size(); i++)
    {
      StressVector[i] = ZeroVector(voigt_size);
    }
  }

  //auxiliary terms
  double HeatSource = 0;

  for (unsigned int PointNumber = 0; PointNumber < integration_points.size(); PointNumber++)
  {
    //COMPUTE kinematics B,F,DN_DX ...
    this->CalculateKinematics(Variables, PointNumber);

    //calculating weights for integration on the "reference configuration"
    double IntegrationWeight = integration_points[PointNumber].Weight() * Variables.detJ;
    IntegrationWeight = this->CalculateIntegrationWeight(IntegrationWeight);

    if (rCalculationFlags.Is(ThermalElement::COMPUTE_LHS_MATRIX)) //calculation of the matrix is required
    {
      Variables.DeltaPlasticDissipation = 0;
      if (thermo_mechanical)
      {
        if (ConstitutiveLawVector[PointNumber]->Has(DELTA_PLASTIC_DISSIPATION))
        {
          ConstitutiveLawVector[PointNumber]->GetValue(DELTA_PLASTIC_DISSIPATION, Variables.DeltaPlasticDissipation);
          // if(Variables.DeltaPlasticDissipation!=0){
          // std::cout<<" thermo_mechanical element LHS ["<<this->Id()<<"]"<<std::endl;
          // std::cout<<" with DELTA_PLASTIC_DISSIPATION= "<<Variables.DeltaPlasticDissipation<<std::endl;
          // }
        }
      }

      Variables.Porosity = PorosityVector[PointNumber];
      this->CalculateThermalProperties( Variables);


      this->CalculateAndAddLHS(rLeftHandSideMatrix, Variables, IntegrationWeight);
    }

    if (rCalculationFlags.Is(ThermalElement::COMPUTE_RHS_VECTOR)) //calculation of the vector is required
    {
      //contribution to external forces
      if (GetProperties().Has(HEAT_SOURCE))
      {
        HeatSource = GetProperties()[HEAT_SOURCE];
        // std::cout<<" HeatSource "<<HeatSource<<std::endl;
      }

      Variables.PlasticDissipation = 0;
      if (thermo_mechanical)
      {
        if (ConstitutiveLawVector[PointNumber]->Has(PLASTIC_DISSIPATION))
        {
          ConstitutiveLawVector[PointNumber]->GetValue(PLASTIC_DISSIPATION, Variables.PlasticDissipation);
          // if(Variables.DeltaPlasticDissipation!=0){
          //   std::cout<<" thermo_mechanical element RHS ["<<this->Id()<<"]"<<std::endl;
          //   std::cout<<" with PLASTIC_DISSIPATION= "<<Variables.PlasticDissipation<<std::endl;
          // }
        }
      }

      Variables.Porosity = PorosityVector[PointNumber];
      this->CalculateThermalProperties( Variables);

      this->CalculateAndAddRHS(rRightHandSideVector, Variables, HeatSource, IntegrationWeight);
    }
  }

  // if( Variables.PlasticDissipation!=0 || Variables.DeltaPlasticDissipation!=0 )
  //   {
  	// std::cout<<" Thermal Element "<<this->Id()<<" [ dissipation:"<<Variables.PlasticDissipation<<", delta_dissipation:"<<Variables.DeltaPlasticDissipation<<"] "<<std::endl;
  	// std::cout<<" K "<<rLeftHandSideMatrix<<std::endl;
  	// std::cout<<" F "<<rRightHandSideVector<<std::endl;
    // }

  KRATOS_CATCH("")
}


//************************************************************************************
//************************************************************************************

void SoilThermalElement::CalculateThermalProperties(ElementData &rVariables) const
{
   KRATOS_TRY

  if ( rVariables.Porosity < 0.0001)
     rVariables.Porosity = GetProperties().GetValue(INITIAL_POROSITY);

  double density_mixture0 = GetProperties().GetValue(DENSITY)*1000.0;
  double density_water = GetProperties().GetValue(DENSITY_WATER)*1000.0;
  double porosity0 = GetProperties().GetValue(INITIAL_POROSITY);

  double density_solid = (density_mixture0 - porosity0*density_water)/(1.0 - porosity0);

  double HeatCapacitySoil = GetProperties()[SPECIFIC_HEAT_SOLID] * density_solid;
  double HeatCapacityWater= GetProperties()[SPECIFIC_HEAT_WATER] * density_water;
  rVariables.HeatCapacity = HeatCapacitySoil * ( 1-rVariables.Porosity)  + HeatCapacityWater * rVariables.Porosity;


  double ThermalConductivitySoil = GetProperties()[THERMAL_CONDUCTIVITY_SOLID];
  double ThermalConductivityWater= GetProperties()[THERMAL_CONDUCTIVITY_WATER];

  rVariables.HeatConductivity = pow( ThermalConductivitySoil, 1.0-rVariables.Porosity);
  rVariables.HeatConductivity*= pow( ThermalConductivityWater, rVariables.Porosity);


  KRATOS_CATCH("")
}



//************************************************************************************
//************************************************************************************
/**
 * This function provides the place to perform checks on the completeness of the input.
 * It is designed to be called only once (or anyway, not often) typically at the beginning
 * of the calculations, so to verify that nothing is missing from the input
 * or that no common error is found.
 * @param rCurrentProcessInfo
 */
int SoilThermalElement::Check(const ProcessInfo &rCurrentProcessInfo) const
{
  KRATOS_TRY

  // Perform base element checks
  int ErrorCode = 0;
  ErrorCode = Element::Check(rCurrentProcessInfo);

  return ErrorCode;

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void SoilThermalElement::save(Serializer &rSerializer) const
{
  KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, ThermalElement);
}

void SoilThermalElement::load(Serializer &rSerializer)
{
  KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, ThermalElement);
}

} // Namespace Kratos
