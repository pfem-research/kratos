//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:           LMonforte $
//   Maintained by:       $Maintainer:                 LM $
//   Date:                $Date:                July 2015 $
//
//

#if !defined(KRATOS_AXISYMMETRIC_UPDATED_LAGRANGIAN_U_P_wP_ELEMENT_HPP_INCLUDED)
#define KRATOS_AXISYMMETRIC_UPDATED_LAGRANGIAN_U_P_wP_ELEMENT_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_elements/hydromechanical_elements/axisymmetric_updated_lagrangian_U_Pressure_element.hpp"

namespace Kratos
{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Updated Lagrangian Large Displacement Lagrangian U-wP Element for 3D and 2D geometries. Linear Triangles and Tetrahedra (base class)

class KRATOS_API(SOLID_MECHANICS_APPLICATION) AxisymmetricUpdatedLagrangianUPwPElement
    : public AxisymmetricUpdatedLagrangianUPressureElement
{
public:
      ///@name Type Definitions
      ///@{
      ///Reference type definition for constitutive laws
      typedef ConstitutiveLaw ConstitutiveLawType;
      ///Pointer type for constitutive laws
      typedef ConstitutiveLawType::Pointer ConstitutiveLawPointerType;
      ///StressMeasure from constitutive laws
      typedef ConstitutiveLawType::StressMeasure StressMeasureType;
      ///Type definition for integration methods
      typedef GeometryData::IntegrationMethod IntegrationMethod;

      /// Counted pointer of LargeDisplacementUPElement
      KRATOS_CLASS_INTRUSIVE_POINTER_DEFINITION(AxisymmetricUpdatedLagrangianUPwPElement);
      ///@}

      ///@name Life Cycle
      ///@{

      /// Empty constructor needed for serialization
      AxisymmetricUpdatedLagrangianUPwPElement();

      /// Default constructors
      AxisymmetricUpdatedLagrangianUPwPElement(IndexType NewId, GeometryType::Pointer pGeometry);

      AxisymmetricUpdatedLagrangianUPwPElement(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties);

      ///Copy constructor
      AxisymmetricUpdatedLagrangianUPwPElement(AxisymmetricUpdatedLagrangianUPwPElement const &rOther);

      /// Destructor.
      virtual ~AxisymmetricUpdatedLagrangianUPwPElement();

      ///@}
      ///@name Operators
      ///@{

      /// Assignment operator.
      AxisymmetricUpdatedLagrangianUPwPElement &operator=(AxisymmetricUpdatedLagrangianUPwPElement const &rOther);

      ///@}
      ///@name Operations
      ///@{
      /**
          * Returns the currently selected integration method
          * @return current integration method selected
          */
      /**
          * creates a new total lagrangian updated element pointer
          * @param NewId: the ID of the new element
          * @param ThisNodes: the nodes of the new element
          * @param pProperties: the properties assigned to the new element
          * @return a Pointer to the new element
          */
      Element::Pointer Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const override;

      /**
          * clones the selected element variables, creating a new one
          * @param NewId: the ID of the new element
          * @param ThisNodes: the nodes of the new element
          * @param pProperties: the properties assigned to the new element
          * @return a Pointer to the new element
          */
      Element::Pointer Clone(IndexType NewId, NodesArrayType const &ThisNodes) const override;

      //************* GETTING METHODS

      //************* STARTING - ENDING  METHODS

      /**
          * Sets on rElementalDofList the degrees of freedom of the considered element geometry
          */
      void GetDofList(DofsVectorType &rElementalDofList, const ProcessInfo &rCurrentProcessInfo) const override;

      /**
          * Sets on rResult the ID's of the element degrees of freedom
          */
      void EquationIdVector(EquationIdVectorType &rResult, const ProcessInfo &rCurrentProcessInfo) const override;

      /**
          * Sets on rValues the nodal displacements
          */
      void GetValuesVector(Vector &rValues, int Step = 0) const override;

      /**
          * Sets on rValues the nodal velocities
          */
      void GetFirstDerivativesVector(Vector &rValues, int Step = 0) const override;

      /**
          * Sets on rValues the nodal accelerations
          */
      void GetSecondDerivativesVector(Vector &rValues, int Step = 0) const override;

      //************************************************************************************
      //************************************************************************************
      /**
          * This function provides the place to perform checks on the completeness of the input.
          * It is designed to be called only once (or anyway, not often) typically at the beginning
          * of the calculations, so to verify that nothing is missing from the input
          * or that no common error is found.
          * @param rCurrentProcessInfo
          */
      int Check(const ProcessInfo &rCurrentProcessInfo) const override;

      ///@}
      ///@name Access
      ///@{

      ///@}
      ///@name Inquiry
      ///@{
      ///@}
      ///@name Input and output
      ///@{
      ///@}
      ///@name Friends
      ///@{
      ///@}
protected:
      ///@name Protected static Member Variables
      ///@{
      ///@}
      ///@name Protected member Variables
      ///@{
      ///@}
      ///@name Protected Operators
      ///@{
      ///@}
      ///@name Protected Operations
      ///@{

      /**
          * Calculation and addition of the matrices of the LHS
          */

      void CalculateAndAddLHS(LocalSystemComponents &rLocalSystem,
                              ElementDataType &rVariables,
                              double &rIntegrationWeight) override;

      /**
          * Calculation and addition of the vectors of the RHS
          */

      void CalculateAndAddRHS(LocalSystemComponents &rLocalSystem,
                              ElementDataType &rVariables,
                              Vector &rVolumeForce,
                              double &rIntegrationWeight) override;

      /**
          * Initialize Element General Variables
          */
      void InitializeElementData(ElementDataType &rVariables,
                                 const ProcessInfo &rCurrentProcessInfo) const override;

      /**
          * Calculation of the geometric terms due to the water pressure
          */
      virtual void CalculateAndAddUnconsideredKuuTerms(MatrixType &rK,
                                                       ElementDataType &rVariables,
                                                       double &rIntegrationWeight);

      /**
          * Calculation of the Ku wP Matrix
          */
      virtual void CalculateAndAddKuwP(MatrixType &rK,
                                       ElementDataType &rVariables,
                                       double &rIntegrationWeight);

      /**
          * Calculation of the KwP U Matrix
          */
      virtual void CalculateAndAddKwPu(MatrixType &rK,
                                       ElementDataType &rVariables,
                                       double &rIntegrationWeight);

      /**
          * Calculation of the KwP P Matrix
          */
      virtual void CalculateAndAddKwPP(MatrixType &rK,
                                       ElementDataType &rVariables,
                                       double &rIntegrationWeight);

      /**
          * Calculation of the K wP wP Matrix
          */
      virtual void CalculateAndAddKwPwP(MatrixType &rK,
                                        ElementDataType &rVariables,
                                        double &rIntegrationWeight);

      /**
          * Calculation of the Stabilization Tangent Matrix
          */
      virtual void CalculateAndAddKwPwPStab(MatrixType &rK,
                                            ElementDataType &rVariables,
                                            double &rIntegrationWeight);

      /**
          * Calculation of the External Forces Vector. Fe = N * t + N * b
          */
      void CalculateAndAddExternalForces(VectorType &rRightHandSideVector,
                                         ElementDataType &rVariables,
                                         Vector &rVolumeForce,
                                         double &rIntegrationWeight) const override;

      /**
          * Calculation of the Internal Forces due to Pressure-Balance
          */
      virtual void CalculateAndAddPressureForces(VectorType &rRightHandSideVector,
                                                 ElementDataType &rVariables,
                                                 double &rIntegrationWeight) override;

      /**
          * Calculation of the Internal Forces due to Pressure-Balance
          */
      virtual void CalculateAndAddStabilizedPressure(VectorType &rRightHandSideVector,
                                                     ElementDataType &rVariables,
                                                     double &rIntegrationWeight) override;

      /**
          * Calculation of the Internal Forces due to sigma. Fi = B * sigma
          */
      void CalculateAndAddInternalForces(VectorType &rRightHandSideVector,
                                         ElementDataType &rVariables,
                                         double &rIntegrationWeight) const override;

      /**
          * Calculation of the Mass Balance ( ie water pressure equation)
          */
      virtual void CalculateAndAddWaterPressureForces(VectorType &rRightHandSideVector,
                                                      ElementDataType &rVariables,
                                                      double &rIntegrationWeight);
      /**
          * Stabilization of the MassBalance equation
          */
      virtual void CalculateAndAddStabilizedWaterPressure(VectorType &rRightHandSideVector,
                                                          ElementDataType &rVariables,
                                                          double &rIntegartionWeight);

      /**
          * Initialize System Matrices
          */
      void InitializeSystemMatrices(MatrixType &rLeftHandSideMatrix,
                                    VectorType &rRightHandSideVector,
                                    Flags &rCalculationFlags) const override;

      /**
          * Calculation of the Volume Change of the Element
          */
      virtual double &CalculateVolumeChange(double &rVolumeChange, ElementDataType &rVariables) override;

      void GetConstants(double &rScalingConstant, double &rWaterBulk, double &rPermeability) const;

      virtual double GetElementSize(const Matrix &rDN_DX);

      ///@}
      ///@name Protected  Access
      ///@{
      ///@}
      ///@name Protected Inquiry
      ///@{
      ///@}
      ///@name Protected LifeCycle
      ///@{
      ///@}

private:
      ///@name Static Member Variables
      ///@{
      ///@}
      ///@name Member Variables
      ///@{

      ///@}
      ///@name Private Operators
      ///@{

      ///@}
      ///@name Private Operations
      ///@{

      ///@}
      ///@name Private  Access
      ///@{
      ///@}

      ///@}
      ///@name Serialization
      ///@{
      friend class Serializer;

      // A private default constructor necessary for serialization

      virtual void save(Serializer &rSerializer) const override;

      virtual void load(Serializer &rSerializer) override;

      ///@name Private Inquiry
      ///@{
      ///@}
      ///@name Un accessible methods
      ///@{
      ///@}

}; // Class AxisymmetricUpdatedLagrangianUPwPElement

} // namespace Kratos
#endif // KRATOS_UPDATED_LAGRANGIAN_U_P_wP_ELEMENT_HPP_INCLUDED
