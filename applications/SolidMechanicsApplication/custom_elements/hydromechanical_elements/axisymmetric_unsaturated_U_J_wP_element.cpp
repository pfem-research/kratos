//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:           LMonforte $
//   Maintained by:       $Maintainer:                 LM $
//   Date:                $Date:             January 2022 $
//
//

// System includes

// External includes

// Project includes
#include "custom_elements/hydromechanical_elements/axisymmetric_unsaturated_U_J_wP_element.hpp"
#include "custom_utilities/axisym_unsaturated_water_pressure_utilities_Jacobian.hpp"
#include "solid_mechanics_application_variables.h"

namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************
// Aquest a l'altre no hi és....
AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement::AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement()
    : AxisymmetricUpdatedLagrangianUJacobianElement()
{
   //DO NOT CALL IT: only needed for Register and Serialization!!!
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement::AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement(IndexType NewId, GeometryType::Pointer pGeometry)
    : AxisymmetricUpdatedLagrangianUJacobianElement(NewId, pGeometry)
{
   //DO NOT ADD DOFS HERE!!!
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement::AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : AxisymmetricUpdatedLagrangianUJacobianElement(NewId, pGeometry, pProperties)
{
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement::AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement(AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement const &rOther)
    : AxisymmetricUpdatedLagrangianUJacobianElement(rOther)
{
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement &AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement::operator=(AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement const &rOther)
{
   AxisymmetricUpdatedLagrangianUJacobianElement::operator=(rOther);

   return *this;
}

//*********************************OPERATIONS*****************************************
//************************************************************************************

Element::Pointer AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement::Create(IndexType NewId, NodesArrayType const &rThisNodes, PropertiesType::Pointer pProperties) const
{
   return Kratos::make_intrusive<AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement>(NewId, GetGeometry().Create(rThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************

Element::Pointer AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement::Clone(IndexType NewId, NodesArrayType const &rThisNodes) const
{

   AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement NewElement(NewId, GetGeometry().Create(rThisNodes), pGetProperties());

   //-----------//

   NewElement.mThisIntegrationMethod = mThisIntegrationMethod;

   if (NewElement.mConstitutiveLawVector.size() != mConstitutiveLawVector.size())
   {
      NewElement.mConstitutiveLawVector.resize(mConstitutiveLawVector.size());

      if (NewElement.mConstitutiveLawVector.size() != NewElement.GetGeometry().IntegrationPointsNumber())
         KRATOS_THROW_ERROR(std::logic_error, "constitutive law not has the correct size ", NewElement.mConstitutiveLawVector.size())
   }

   for (unsigned int i = 0; i < mConstitutiveLawVector.size(); i++)
   {
      NewElement.mConstitutiveLawVector[i] = mConstitutiveLawVector[i]->Clone();
   }

   //-----------//

   if (NewElement.mDeformationGradientF0.size() != mDeformationGradientF0.size())
      NewElement.mDeformationGradientF0.resize(mDeformationGradientF0.size());

   for (unsigned int i = 0; i < mDeformationGradientF0.size(); i++)
   {
      NewElement.mDeformationGradientF0[i] = mDeformationGradientF0[i];
   }

   NewElement.mDeterminantF0 = mDeterminantF0;

   NewElement.SetData(this->GetData());
   NewElement.SetFlags(this->GetFlags());

   return Kratos::make_intrusive<AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement>(NewElement);
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement::~AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement()
{
}

//************* GETTING METHODS ******************************************************
//************************************************************************************
//************************************************************************************


void AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement::GetDofList(DofsVectorType &rElementalDofList, const ProcessInfo &rCurrentProcessInfo) const
{
   rElementalDofList.resize(0);

   for (unsigned int i = 0; i < GetGeometry().size(); i++)
   {
      rElementalDofList.push_back(GetGeometry()[i].pGetDof(DISPLACEMENT_X));
      rElementalDofList.push_back(GetGeometry()[i].pGetDof(DISPLACEMENT_Y));
      rElementalDofList.push_back(GetGeometry()[i].pGetDof(JACOBIAN));
      rElementalDofList.push_back(GetGeometry()[i].pGetDof(WATER_PRESSURE));
   }
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement::EquationIdVector(EquationIdVectorType &rResult, const ProcessInfo &rCurrentProcessInfo) const
{
   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   unsigned int element_size = number_of_nodes * dimension + 2 * number_of_nodes;

   if (rResult.size() != element_size)
      rResult.resize(element_size, false);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      int index = i * (dimension + 2);
      rResult[index] = GetGeometry()[i].GetDof(DISPLACEMENT_X).EquationId();
      rResult[index + 1] = GetGeometry()[i].GetDof(DISPLACEMENT_Y).EquationId();
      rResult[index + 2] = GetGeometry()[i].GetDof(JACOBIAN).EquationId();
      rResult[index + 3] = GetGeometry()[i].GetDof(WATER_PRESSURE).EquationId();
   }
}

//*********************************DISPLACEMENT***************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement::GetValuesVector(Vector &rValues, int Step) const
{
   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   unsigned int element_size = number_of_nodes * dimension + 2 * number_of_nodes;

   if (rValues.size() != element_size)
      rValues.resize(element_size, false);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      unsigned int index = i * (dimension + 2);
      rValues[index] = GetGeometry()[i].GetSolutionStepValue(DISPLACEMENT_X, Step);
      rValues[index + 1] = GetGeometry()[i].GetSolutionStepValue(DISPLACEMENT_Y, Step);
      rValues[index + 2] = GetGeometry()[i].GetSolutionStepValue(JACOBIAN, Step);
      rValues[index + 3] = GetGeometry()[i].GetSolutionStepValue(WATER_PRESSURE, Step);
   }
}

//************************************VELOCITY****************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement::GetFirstDerivativesVector(Vector &rValues, int Step) const
{
   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   unsigned int element_size = number_of_nodes * dimension + 2 * number_of_nodes;

   if (rValues.size() != element_size)
      rValues.resize(element_size, false);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      unsigned int index = i * (dimension + 2);
      rValues[index] = GetGeometry()[i].GetSolutionStepValue(VELOCITY_X, Step);
      rValues[index + 1] = GetGeometry()[i].GetSolutionStepValue(VELOCITY_Y, Step);
      rValues[index + 2] = 0;
      rValues[index + 3] = 0;
   }
}

//*********************************ACCELERATION***************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement::GetSecondDerivativesVector(Vector &rValues, int Step) const
{
   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   unsigned int element_size = number_of_nodes * dimension + 2 * number_of_nodes;

   if (rValues.size() != element_size)
      rValues.resize(element_size, false);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      unsigned int index = i * (dimension + 2);
      rValues[index] = GetGeometry()[i].GetSolutionStepValue(ACCELERATION_X, Step);
      rValues[index + 1] = GetGeometry()[i].GetSolutionStepValue(ACCELERATION_Y, Step);
      rValues[index + 2] = 0;
      rValues[index + 3] = 0;
   }
}

// ********************** Initialize General Variables ***********************************
// ***************************************************************************************

void AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement::InitializeElementData(ElementDataType &rVariables, const ProcessInfo &rCurrentProcessInfo) const
{
   AxisymmetricUpdatedLagrangianUJacobianElement::InitializeElementData(rVariables, rCurrentProcessInfo);

   //stabilization factor
   if (!GetProperties().Has(STABILIZATION_FACTOR_WP))
     KRATOS_ERROR << "STABILIZATION_FACTOR_WP not defined in properties " << std::endl;

}

//************************************************************************************
//************************************************************************************

int AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement::Check(const ProcessInfo &rCurrentProcessInfo) const
{
   KRATOS_TRY

   // Perform base element checks
   int ErrorCode = 0;
   ErrorCode = AxisymmetricUpdatedLagrangianUJacobianElement::Check(rCurrentProcessInfo);

   return ErrorCode;

   KRATOS_CATCH("")
}

void AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement::CalculateOnIntegrationPoints(const Variable<double> &rVariable, std::vector<double> &rValues, const ProcessInfo &rCurrentProcessInfo)
{

   KRATOS_TRY

   if ( rVariable == DEGREE_OF_SATURATION) {
      const unsigned int &integration_points_number = mConstitutiveLawVector.size();
      const unsigned int number_of_nodes = GetGeometry().size();

      if (rValues.size() != mConstitutiveLawVector.size())
         rValues.resize(mConstitutiveLawVector.size());
      
      //create and initialize element variables:
      ElementDataType Variables;
      this->InitializeElementData(Variables, rCurrentProcessInfo);

      for (unsigned int PointNumber = 0; PointNumber < integration_points_number; PointNumber++)
      {
         //compute element kinematics B, F, DN_DX ...
         this->CalculateKinematics(Variables, PointNumber);

         double GaussPointPressure = 0.0;
         for (unsigned int i = 0; i < number_of_nodes; ++i)
            GaussPointPressure += GetGeometry()[i].GetSolutionStepValue(WATER_PRESSURE) * Variables.N[i];

         // Compute the degree of saturation
         AxisymUnsaturatedWaterPressureJacobianUtilities WaterUtility;
         double DegreeOfSaturation = WaterUtility.EvaluateRetentionCurve( GetProperties(), GaussPointPressure, DegreeOfSaturation);
         rValues[PointNumber] = DegreeOfSaturation;
      }

   } else if ( rVariable == DEGREE_OF_SATURATION_EFF) {
      const unsigned int &integration_points_number = mConstitutiveLawVector.size();
      const unsigned int number_of_nodes = GetGeometry().size();

      if (rValues.size() != mConstitutiveLawVector.size())
         rValues.resize(mConstitutiveLawVector.size());
      
      //create and initialize element variables:
      ElementDataType Variables;
      this->InitializeElementData(Variables, rCurrentProcessInfo);

      for (unsigned int PointNumber = 0; PointNumber < integration_points_number; PointNumber++)
      {
         //compute element kinematics B, F, DN_DX ...
         this->CalculateKinematics(Variables, PointNumber);

         double GaussPointPressure = 0.0;
         for (unsigned int i = 0; i < number_of_nodes; ++i)
            GaussPointPressure += GetGeometry()[i].GetSolutionStepValue(WATER_PRESSURE) * Variables.N[i];

         // Compute the degree of saturation
         AxisymUnsaturatedWaterPressureJacobianUtilities WaterUtility;
         double DegreeOfSaturation = WaterUtility.EvaluateEffectiveRetentionCurve( GetProperties(), GaussPointPressure, DegreeOfSaturation);
         rValues[PointNumber] = DegreeOfSaturation;
      }

   } else if (rVariable == POROSITY) {
      const unsigned int &integration_points_number = mConstitutiveLawVector.size();
      const unsigned int number_of_nodes = GetGeometry().size();

      const double InitialPorosity = GetProperties()[INITIAL_POROSITY];

      if (rValues.size() != mConstitutiveLawVector.size())
         rValues.resize(mConstitutiveLawVector.size());

      //create and initialize element variables:
      ElementDataType Variables;
      this->InitializeElementData(Variables, rCurrentProcessInfo);
      
      std::vector<double> DetF0;
      this->CalculateOnIntegrationPoints(DETERMINANT_F, DetF0, rCurrentProcessInfo);
         
      double BetaMixed(1.0);
      const Properties & rProperties = GetProperties();
      if ( rProperties.Has(BETA_MIXED) ) {
         BetaMixed = rProperties.GetValue(BETA_MIXED);
      }

      for (unsigned int PointNumber = 0; PointNumber < integration_points_number; PointNumber++)
      {
         //compute element kinematics B, F, DN_DX ...
         this->CalculateKinematics(Variables, PointNumber);

         double Jacobian(0.0);
         for (unsigned int i = 0; i < number_of_nodes; ++i)
            Jacobian += GetGeometry()[i].GetSolutionStepValue(JACOBIAN) * Variables.N[i];
         double ThisJacobian = pow(Jacobian, BetaMixed)*pow(DetF0[PointNumber], 1.0-BetaMixed);
         rValues[PointNumber] = 1.0 - (1.0 - InitialPorosity) / ThisJacobian;


      }
   } else if ( rVariable == WATER_CONTENT) {
      if (rValues.size() != mConstitutiveLawVector.size())
         rValues.resize(mConstitutiveLawVector.size());
   
      std::vector<double> VoidRatio;
      std::vector<double> DegreeOfSaturation;
      this->CalculateOnIntegrationPoints(DEGREE_OF_SATURATION, DegreeOfSaturation, rCurrentProcessInfo);
      this->CalculateOnIntegrationPoints(VOID_RATIO, VoidRatio, rCurrentProcessInfo);

      for (unsigned int PointNumber = 0; PointNumber < mConstitutiveLawVector.size(); PointNumber++)
	 rValues[PointNumber] = DegreeOfSaturation[PointNumber]*VoidRatio[PointNumber];
    } else if (rVariable == VOID_RATIO) {
      
      if (rValues.size() != mConstitutiveLawVector.size())
         rValues.resize(mConstitutiveLawVector.size());

      this->CalculateOnIntegrationPoints(POROSITY, rValues, rCurrentProcessInfo);
      const unsigned int &integration_points_number = mConstitutiveLawVector.size();

      for (unsigned int PointNumber = 0; PointNumber < integration_points_number; PointNumber++)
         rValues[PointNumber] = rValues[PointNumber] / (1.0 - rValues[PointNumber]);

    } else if ( rVariable == UNSAT_TERM) {

       const unsigned int number_of_nodes = GetGeometry().size();

       if (rValues.size() != mConstitutiveLawVector.size())
          rValues.resize(mConstitutiveLawVector.size());

       std::vector<double> DegreeOfSaturation;
       this->CalculateOnIntegrationPoints(DEGREE_OF_SATURATION_EFF, DegreeOfSaturation, rCurrentProcessInfo);

       //create and initialize element variables:
       ElementDataType Variables;
       this->InitializeElementData(Variables, rCurrentProcessInfo);

       for (unsigned int PointNumber = 0; PointNumber < mConstitutiveLawVector.size(); PointNumber++) {
          //compute element kinematics B, F, DN_DX ...
          this->CalculateKinematics(Variables, PointNumber);
          double WaterPressure(0.0);
          for (unsigned int i = 0; i < number_of_nodes; i++)
             WaterPressure += GetGeometry()[i].FastGetSolutionStepValue(WATER_PRESSURE) * Variables.N[i];
          rValues[PointNumber] = DegreeOfSaturation[PointNumber] * WaterPressure;
       }

    } else if (rVariable == VOID_RATIO) {
      
      if (rValues.size() != mConstitutiveLawVector.size())
         rValues.resize(mConstitutiveLawVector.size());

      this->CalculateOnIntegrationPoints(POROSITY, rValues, rCurrentProcessInfo);
      const unsigned int &integration_points_number = mConstitutiveLawVector.size();

      for (unsigned int PointNumber = 0; PointNumber < integration_points_number; PointNumber++)
         rValues[PointNumber] = rValues[PointNumber] / (1.0 - rValues[PointNumber]);
   
    } else if ( rVariable == PERMEABILITY_WATER) {
      
      const unsigned int &integration_points_number = mConstitutiveLawVector.size();
      const unsigned int number_of_nodes = GetGeometry().size();
      
      if (rValues.size() != mConstitutiveLawVector.size())
         rValues.resize(mConstitutiveLawVector.size());
      
      Matrix PermeabilityMatrix = ZeroMatrix(3,3);

      ElementDataType Variables;
      this->InitializeElementData(Variables, rCurrentProcessInfo);
      
      AxisymUnsaturatedWaterPressureJacobianUtilities WaterUtility;

      for (unsigned int PointNumber = 0; PointNumber < integration_points_number; PointNumber++) {

         this->CalculateKinematics( Variables, PointNumber);

         double GaussPointPressure(0.0), Jacobian(0.0);
         for (unsigned int i = 0; i < number_of_nodes; ++i) {
            GaussPointPressure += GetGeometry()[i].GetSolutionStepValue(WATER_PRESSURE) * Variables.N[i];
            Jacobian += GetGeometry()[i].GetSolutionStepValue(JACOBIAN) * Variables.N[i];
         }

         WaterUtility.GetPermeabilityTensorAndReshape( GetProperties(), mDeformationGradientF0[PointNumber], PermeabilityMatrix, GetProperties()[INITIAL_POROSITY], 3, Jacobian, GaussPointPressure);
         rValues[PointNumber] = PermeabilityMatrix(0,0);

      }
   } else {

      UpdatedLagrangianUJacobianElement::CalculateOnIntegrationPoints(rVariable, rValues, rCurrentProcessInfo);
   }

   KRATOS_CATCH("")
}


void AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement::CalculateOnIntegrationPoints(const Variable<Vector> &rVariable, std::vector<Vector> &rValues, const ProcessInfo &rCurrentProcessInfo)
{

   KRATOS_TRY

   UpdatedLagrangianUJacobianElement::CalculateOnIntegrationPoints(rVariable, rValues, rCurrentProcessInfo);

   KRATOS_CATCH("")
}

void AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement::CalculateOnIntegrationPoints(const Variable<Matrix> &rVariable, std::vector<Matrix> &rValues, const ProcessInfo &rCurrentProcessInfo)
{

   KRATOS_TRY


   if (rVariable == TOTAL_CAUCHY_STRESS)
   {
      // only for one gauss point element
      this->CalculateOnIntegrationPoints(CAUCHY_STRESS_TENSOR, rValues, rCurrentProcessInfo);

      const unsigned int &integration_points_number = mConstitutiveLawVector.size();
      const unsigned int number_of_nodes = GetGeometry().size();

      unsigned int ThisDimension = rValues[0].size1();
      Matrix Eye = ZeroMatrix(ThisDimension, ThisDimension);

      for (unsigned int i = 0; i < ThisDimension; ++i)
         Eye(i, i) = 1.0;

      //create and initialize element variables:
      ElementDataType Variables;
      this->InitializeElementData(Variables, rCurrentProcessInfo);

      AxisymUnsaturatedWaterPressureJacobianUtilities WaterUtility;

      for (unsigned int PointNumber = 0; PointNumber < integration_points_number; PointNumber++)
      {
         //compute element kinematics B, F, DN_DX ...
         this->CalculateKinematics(Variables, PointNumber);

         double GaussPointPressure = 0.0;
         for (unsigned int i = 0; i < number_of_nodes; ++i)
            GaussPointPressure += GetGeometry()[i].GetSolutionStepValue(WATER_PRESSURE) * Variables.N[i];
         // Compute the degree of saturation
         double DegreeOfSaturation = WaterUtility.EvaluateEffectiveRetentionCurve( GetProperties(), GaussPointPressure, DegreeOfSaturation);
         rValues[PointNumber] = rValues[PointNumber] + DegreeOfSaturation * GaussPointPressure * Eye;
      }
   } else if ( rVariable == PERMEABILITY_TENSOR) {
      
      const unsigned int &integration_points_number = mConstitutiveLawVector.size();
      const unsigned int number_of_nodes = GetGeometry().size();

      ElementDataType Variables;
      this->InitializeElementData(Variables, rCurrentProcessInfo);
      
      AxisymUnsaturatedWaterPressureJacobianUtilities WaterUtility;

      for (unsigned int PointNumber = 0; PointNumber < integration_points_number; PointNumber++) {

         this->CalculateKinematics(Variables, PointNumber);

         double GaussPointPressure(0.0), Jacobian(0.0);
         for (unsigned int i = 0; i < number_of_nodes; ++i) {
            GaussPointPressure += GetGeometry()[i].GetSolutionStepValue(WATER_PRESSURE) * Variables.N[i];
            Jacobian += GetGeometry()[i].GetSolutionStepValue(JACOBIAN) * Variables.N[i];
         }

         rValues[PointNumber] = ZeroMatrix(3,3);
         WaterUtility.GetPermeabilityTensorAndReshape( GetProperties(), mDeformationGradientF0[PointNumber], rValues[PointNumber], GetProperties()[INITIAL_POROSITY], 3, Jacobian, GaussPointPressure);
      }
   }
   else
   {
      UpdatedLagrangianUJacobianElement::CalculateOnIntegrationPoints(rVariable, rValues, rCurrentProcessInfo);
   }


   KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement::InitializeSystemMatrices(MatrixType &rLeftHandSideMatrix,
                                                                  VectorType &rRightHandSideVector,
                                                                  Flags &rCalculationFlags) const

{

   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();

   //resizing as needed the LHS
   unsigned int MatSize = number_of_nodes * dimension + 2 * number_of_nodes;

   if (rCalculationFlags.Is(LargeDisplacementElement::COMPUTE_LHS_MATRIX)) //calculation of the matrix is required
   {
      if (rLeftHandSideMatrix.size1() != MatSize)
         rLeftHandSideMatrix.resize(MatSize, MatSize, false);

      noalias(rLeftHandSideMatrix) = ZeroMatrix(MatSize, MatSize); //resetting LHS
   }

   //resizing as needed the RHS
   if (rCalculationFlags.Is(LargeDisplacementElement::COMPUTE_RHS_VECTOR)) //calculation of the matrix is required
   {
      if (rRightHandSideVector.size() != MatSize)
         rRightHandSideVector.resize(MatSize, false);

      rRightHandSideVector = ZeroVector(MatSize); //resetting RHS
   }
}

//************* COMPUTING  METHODS
//************************************************************************************
//************************************************************************************

//************************************************************************************
//************************************************************************************
//************************************************************************************
//************************************************************************************
void AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement::CalculatePerturbation(LocalSystemComponents &rLocalSystem, ElementDataType &rVariables, double &rIntegrationWeight, Matrix & rMatrix)
{
   KRATOS_TRY
const unsigned int number_of_nodes = GetGeometry().PointsNumber();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   const double &time_step = rVariables.GetProcessInfo()[DELTA_TIME];
   rVariables.detF0 *= rVariables.detF;
   double DeterminantF = rVariables.detF;
   rVariables.detF = 1.0;

   double IntegrationWeight = rIntegrationWeight * 2.0 * Globals::Pi * rVariables.CurrentRadius / GetProperties()[THICKNESS];

   //contributions of the stiffness matrix calculated on the reference configuration
   MatrixType &rLeftHandSideMatrix = rLocalSystem.GetLeftHandSideMatrix();

   const unsigned int size1 = rVariables.ConstitutiveMatrix.size1();
   const unsigned int size2 = rVariables.ConstitutiveMatrix.size2();
   Vector ConstitutiveVector(size1);
   Matrix OriginalMatrix(size1, size2);
   if ( size1 == size2-1) {
      OriginalMatrix = rVariables.ConstitutiveMatrix;
      rVariables.ConstitutiveMatrix.resize(size1, size1, false);
      for (unsigned int i = 0; i < size1; i++)
         for (unsigned int j = 0; j < size1; j++)
            rVariables.ConstitutiveMatrix(i,j) = OriginalMatrix(i,j);
      /*for (unsigned int i = 0; i < size1; i++)
         ConstitutiveVector(i) = OriginalMatrix(i, size2-1);*/
      noalias( ConstitutiveVector ) = ZeroVector(size1);
   } else {
      noalias( ConstitutiveVector ) = ZeroVector(size1);
   }

   ProcessInfo SomeProcessInfo;
   std::vector<double> Mmodulus;
   SolidElement::CalculateOnIntegrationPoints(M_MODULUS, Mmodulus, SomeProcessInfo);
   double ConstrainedModulus = Mmodulus[0];
   if (ConstrainedModulus < 1e-5)
   {
      const double &YoungModulus = GetProperties()[YOUNG_MODULUS];
      const double &nu = GetProperties()[POISSON_RATIO];
      ConstrainedModulus = YoungModulus * (1.0 - nu) / (1.0 + nu) / (1.0 - 2.0 * nu);
   }
   
   // Reshape the BaseClass LHS and Add the Hydro Part
   AxisymUnsaturatedWaterPressureJacobianUtilities WaterUtility;
   Matrix TotalF = prod(rVariables.F, rVariables.F0);
   int number_of_variables = dimension + 2;
   Vector VolumeForce;
   VolumeForce = this->CalculateVolumeForce(VolumeForce, rVariables);

   // 1. Create (make pointers) variables
   AxisymUnsaturatedWaterPressureJacobianUtilities::HydroMechanicalVariables HMVariables(GetGeometry(), GetProperties());

   HMVariables.SetBMatrix(rVariables.B);
   HMVariables.SetShapeFunctionsDerivatives(rVariables.DN_DX);
   HMVariables.SetDeformationGradient(TotalF);
   HMVariables.SetVolumeForce(VolumeForce);
   HMVariables.SetShapeFunctions(rVariables.N);
   HMVariables.SetConstitutiveVector( ConstitutiveVector);

   HMVariables.DeltaTime = time_step;
   HMVariables.detF0 = rVariables.detF0;
   HMVariables.detF  = DeterminantF;
   HMVariables.CurrentRadius = rVariables.CurrentRadius;
   HMVariables.ConstrainedModulus = ConstrainedModulus;
   HMVariables.number_of_variables = number_of_variables;

   Matrix LeftHandSideMatrix = ZeroMatrix(number_of_nodes*(dimension+2), number_of_nodes*(dimension+2) );
   Matrix BaseClassLeftHandSideMatrix = ZeroMatrix(number_of_nodes*(dimension+1), number_of_nodes*(dimension+1));

   LeftHandSideMatrix = WaterUtility.CalculateAndAddHydromechanicalLHS(HMVariables, rLeftHandSideMatrix, BaseClassLeftHandSideMatrix, IntegrationWeight);
   LeftHandSideMatrix = WaterUtility.CalculateAndAddStabilizationLHS(HMVariables, rLeftHandSideMatrix, IntegrationWeight);

  Matrix Perturbed = LeftHandSideMatrix;
 
  ProcessInfo rCurrentProcessInfo;

  DofsVectorType ElementalDofList;
  this->GetDofList(ElementalDofList, rCurrentProcessInfo);

  unsigned int size = ElementalDofList.size();
  for (unsigned int i = 0; i < size; i++)
  {
    //Set perturbation in "i" dof component
     std::cout << " THIS VARIABLE " << ElementalDofList[i]->Info() << std::endl;
    double &value = ElementalDofList[i]->GetSolutionStepValue();
    double original = value;

    double deltavalue = 1e-6;
    value = original + deltavalue;

    VectorType BaseClassRightHandSideVector = ZeroVector(number_of_nodes*(dimension+1));
    VectorType  RightHandSideVectorI = ZeroVector(number_of_nodes*(dimension+2));
    VectorType  RightHandSideVectorII = ZeroVector(number_of_nodes*(dimension+2));

    CalculateRadius( rVariables.CurrentRadius, rVariables.ReferenceRadius, rVariables.N);
    HMVariables.CurrentRadius = rVariables.CurrentRadius;   

    RightHandSideVectorI = WaterUtility.CalculateAndAddHydromechanicalRHS(HMVariables, RightHandSideVectorI, BaseClassRightHandSideVector, IntegrationWeight);
   RightHandSideVectorI  = WaterUtility.CalculateAndAddStabilization(HMVariables, RightHandSideVectorI, IntegrationWeight);
   
   value = original-deltavalue;
   
    CalculateRadius( rVariables.CurrentRadius, rVariables.ReferenceRadius, rVariables.N);
    HMVariables.CurrentRadius = rVariables.CurrentRadius;    

   RightHandSideVectorII = WaterUtility.CalculateAndAddHydromechanicalRHS(HMVariables, RightHandSideVectorII, BaseClassRightHandSideVector, IntegrationWeight);
   RightHandSideVectorII  = WaterUtility.CalculateAndAddStabilization(HMVariables, RightHandSideVectorII, IntegrationWeight);

    for (unsigned int j = 0; j < size; j++)
    {
      Perturbed(j, i) = (-1) * (RightHandSideVectorI[j] - RightHandSideVectorII[j]) / (2.0 * deltavalue);
    }

    value = original;


  }

  std::cout << " ORIGINAL LHS " << std::endl;
  WriteMatrix(LeftHandSideMatrix);
  std::cout << " PERTURBED " << std::endl;
  WriteMatrix(Perturbed);
  std::cout << " DIFERENCE " << std::endl;
  WriteMatrix(LeftHandSideMatrix-Perturbed);

  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << std::endl;
   rVariables.detF = DeterminantF;
   rVariables.detF0 /= rVariables.detF;
   rVariables.ConstitutiveMatrix = OriginalMatrix;

   KRATOS_CATCH("")
}

void AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement::WriteMatrix( Matrix A)
{
   KRATOS_TRY
   for (unsigned int i = 0; i < A.size1(); i++) {
      for (unsigned int j = 0; j < A.size2(); j++)
         std::cout << A(i,j) << " , ";
      std::cout << std::endl;
   }
   KRATOS_CATCH("");
}
void AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement::CalculateAndAddLHS(LocalSystemComponents &rLocalSystem, ElementDataType &rVariables, double &rIntegrationWeight)
{

   KRATOS_TRY
  
   //Matrix PerturbedMatrix;
   //CalculatePerturbation( rLocalSystem, rVariables, rIntegrationWeight, PerturbedMatrix);

   // define some variables
   const unsigned int number_of_nodes = GetGeometry().PointsNumber();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   const double &time_step = rVariables.GetProcessInfo()[DELTA_TIME];
   rVariables.detF0 *= rVariables.detF;
   double DeterminantF = rVariables.detF;
   rVariables.detF = 1.0;

   //contributions of the stiffness matrix calculated on the reference configuration
   MatrixType &rLeftHandSideMatrix = rLocalSystem.GetLeftHandSideMatrix();

   // ComputeBaseClass LHS
   LocalSystemComponents UJLocalSystem;
   unsigned int MatSize = number_of_nodes * (dimension + 1);
   MatrixType LocalLeftHandSideMatrix = ZeroMatrix(MatSize, MatSize);
   UJLocalSystem.SetLeftHandSideMatrix(LocalLeftHandSideMatrix);

   const unsigned int size1 = rVariables.ConstitutiveMatrix.size1();
   const unsigned int size2 = rVariables.ConstitutiveMatrix.size2();
   Vector ConstitutiveVector(size1);
   if ( size1 == size2-1) {
      Matrix OriginalMatrix(size1, size2);
      OriginalMatrix = rVariables.ConstitutiveMatrix;
      rVariables.ConstitutiveMatrix.resize(size1, size1, false);
      for (unsigned int i = 0; i < size1; i++)
         for (unsigned int j = 0; j < size1; j++)
            rVariables.ConstitutiveMatrix(i,j) = OriginalMatrix(i,j);
      for (unsigned int i = 0; i < size1; i++)
         ConstitutiveVector(i) = OriginalMatrix(i, size2-1);
   } else {
      noalias( ConstitutiveVector ) = ZeroVector(size1);
   }


   // LHS. base class
   AxisymmetricUpdatedLagrangianUJacobianElement::CalculateAndAddLHS(UJLocalSystem, rVariables, rIntegrationWeight);

   double IntegrationWeight = rIntegrationWeight * 2.0 * Globals::Pi * rVariables.CurrentRadius / GetProperties()[THICKNESS];

   // Reshape the BaseClass LHS and Add the Hydro Part
   AxisymUnsaturatedWaterPressureJacobianUtilities WaterUtility;
   Matrix TotalF(3, 3);
   noalias(TotalF) = prod(rVariables.F, rVariables.F0);
   int number_of_variables = dimension + 2; // displ - Jacobian - waterPressure
   Vector VolumeForce;
   VolumeForce = this->CalculateVolumeForce(VolumeForce, rVariables);

   // 1. Create (make pointers) variables
   WaterPressureUtilities::HydroMechanicalVariables HMVariables(GetGeometry(), GetProperties());

   HMVariables.SetBMatrix(rVariables.B);
   HMVariables.SetShapeFunctionsDerivatives(rVariables.DN_DX);
   HMVariables.SetDeformationGradient(TotalF);
   HMVariables.SetVolumeForce(VolumeForce);
   HMVariables.SetShapeFunctions(rVariables.N);
   HMVariables.SetConstitutiveVector( ConstitutiveVector);

   HMVariables.DeltaTime = time_step;
   HMVariables.detF0 = rVariables.detF0;
   HMVariables.detF  = DeterminantF;
   HMVariables.CurrentRadius = rVariables.CurrentRadius;
   //HMVariables.ConstrainedModulus
   HMVariables.number_of_variables = number_of_variables;

   // LHS. hydromechanical problem
   rLeftHandSideMatrix = WaterUtility.CalculateAndAddHydromechanicalLHS(HMVariables, rLeftHandSideMatrix, LocalLeftHandSideMatrix, IntegrationWeight);

   // LHS. water pressure stabilization
   ProcessInfo SomeProcessInfo;
   std::vector<double> Mmodulus;
   SolidElement::CalculateOnIntegrationPoints(M_MODULUS, Mmodulus, SomeProcessInfo);
   double ConstrainedModulus = Mmodulus[0];
   if (ConstrainedModulus < 1e-5)
   {
      const double &YoungModulus = GetProperties()[YOUNG_MODULUS];
      const double &nu = GetProperties()[POISSON_RATIO];
      ConstrainedModulus = YoungModulus * (1.0 - nu) / (1.0 + nu) / (1.0 - 2.0 * nu);
   }
   HMVariables.ConstrainedModulus = ConstrainedModulus;
   rLeftHandSideMatrix = WaterUtility.CalculateAndAddStabilizationLHS(HMVariables, rLeftHandSideMatrix, IntegrationWeight);

   rVariables.detF = DeterminantF;
   rVariables.detF0 /= rVariables.detF;

   KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement::CalculateAndAddRHS(LocalSystemComponents &rLocalSystem, ElementDataType &rVariables, Vector &rVolumeForce, double &rIntegrationWeight)
{
   KRATOS_TRY

   // define some variables
   const unsigned int number_of_nodes = GetGeometry().PointsNumber();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   const double &time_step = rVariables.GetProcessInfo()[DELTA_TIME];
   rVariables.detF0 *= rVariables.detF;
   double DeterminantF = rVariables.detF;
   rVariables.detF = 1.0;

   //contribution of the internal and external forces
   VectorType &rRightHandSideVector = rLocalSystem.GetRightHandSideVector();

   // Compute Base Class RHS
   LocalSystemComponents BaseClassLocalSystem;
   Vector BaseClassRightHandSideVector = ZeroVector((dimension + 1) * number_of_nodes);
   BaseClassLocalSystem.SetRightHandSideVector(BaseClassRightHandSideVector);
   Vector VolumeForce = rVolumeForce;
   VolumeForce *= 0.0;
   AxisymmetricUpdatedLagrangianUJacobianElement::CalculateAndAddRHS(BaseClassLocalSystem, rVariables, VolumeForce, rIntegrationWeight);

   /*if ( this->Id() == 1) {
   std::cout << this->Id() << " " << std::endl;
   std::cout << rVariables.StressVector << std::endl;
   std::cout << BaseClassRightHandSideVector << std::endl;
   }*/
      

   // Reshape the BaseClass RHS and Add the Hydro Part
   AxisymUnsaturatedWaterPressureJacobianUtilities WaterUtility;
   Matrix TotalF = prod(rVariables.F, rVariables.F0);

   int number_of_variables = dimension + 2; // displacement - Jacobian - waterPressure
   double IntegrationWeight = rIntegrationWeight * 2.0 * Globals::Pi * rVariables.CurrentRadius / GetProperties()[THICKNESS];

   // 1. Create (make pointers) variables
   WaterPressureUtilities::HydroMechanicalVariables HMVariables(GetGeometry(), GetProperties());

   HMVariables.SetBMatrix(rVariables.B);
   HMVariables.SetShapeFunctionsDerivatives(rVariables.DN_DX);
   HMVariables.SetDeformationGradient(TotalF);
   HMVariables.SetVolumeForce(rVolumeForce);
   HMVariables.SetShapeFunctions(rVariables.N);

   HMVariables.DeltaTime = time_step;
   HMVariables.detF0 = rVariables.detF0;
   HMVariables.detF  = DeterminantF;
   HMVariables.CurrentRadius = rVariables.CurrentRadius;
   //HMVariables.ConstrainedModulus
   HMVariables.number_of_variables = number_of_variables;


   //if ( this->Id() < 100)
   //   std::cout << this->Id() << " VALUES " << rVariables.detF0 << " and " << rVariables.detF << " and " << rVariables.detH << " and " << DeterminantF <<  std::endl;



   rRightHandSideVector = WaterUtility.CalculateAndAddHydromechanicalRHS(HMVariables, rRightHandSideVector, BaseClassRightHandSideVector, IntegrationWeight);
   /*if ( this->Id() == 1)
   std::cout << " " << rRightHandSideVector << std::endl;*/

   // Add Stab term
   ProcessInfo SomeProcessInfo;
   std::vector<double> Mmodulus;
   SolidElement::CalculateOnIntegrationPoints(M_MODULUS, Mmodulus, SomeProcessInfo);
   double ConstrainedModulus = Mmodulus[0];
   if (ConstrainedModulus < 1e-5)
   {
      const double &YoungModulus = GetProperties()[YOUNG_MODULUS];
      const double &nu = GetProperties()[POISSON_RATIO];
      ConstrainedModulus = YoungModulus * (1.0 - nu) / (1.0 + nu) / (1.0 - 2.0 * nu);
   }
   HMVariables.ConstrainedModulus = ConstrainedModulus;
   rRightHandSideVector = WaterUtility.CalculateAndAddStabilization(HMVariables, rRightHandSideVector, IntegrationWeight);

   /*if ( this->Id() == 1)
   std::cout << " " << rRightHandSideVector << std::endl;*/

   rVariables.detF = DeterminantF;
   rVariables.detF0 /= rVariables.detF;

   KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement::save(Serializer &rSerializer) const
{
   KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, AxisymmetricUpdatedLagrangianUJacobianElement)
}

void AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement::load(Serializer &rSerializer)
{
   KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, AxisymmetricUpdatedLagrangianUJacobianElement)
}

} // namespace Kratos
