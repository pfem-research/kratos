//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:           LMonforte $
//   Maintained by:       $Maintainer:                 LM $
//   Date:                $Date:                July 2015 $
//
//

// System includes

// External includes

// Project includes
#include "custom_elements/hydromechanical_elements/axisymmetric_updated_lagrangian_U_J_wP_element.hpp"
#include "solid_mechanics_application_variables.h"

#include "custom_utilities/axisym_water_pressure_utilities_Jacobian.hpp"

namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************
// Aquest a l'altre no hi és....
AxisymmetricUpdatedLagrangianUJwPElement::AxisymmetricUpdatedLagrangianUJwPElement()
    : AxisymmetricUpdatedLagrangianUJacobianElement()
{
   //DO NOT CALL IT: only needed for Register and Serialization!!!
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUJwPElement::AxisymmetricUpdatedLagrangianUJwPElement(IndexType NewId, GeometryType::Pointer pGeometry)
    : AxisymmetricUpdatedLagrangianUJacobianElement(NewId, pGeometry)
{
   //DO NOT ADD DOFS HERE!!!
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUJwPElement::AxisymmetricUpdatedLagrangianUJwPElement(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : AxisymmetricUpdatedLagrangianUJacobianElement(NewId, pGeometry, pProperties)
{
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUJwPElement::AxisymmetricUpdatedLagrangianUJwPElement(AxisymmetricUpdatedLagrangianUJwPElement const &rOther)
    : AxisymmetricUpdatedLagrangianUJacobianElement(rOther)
{
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUJwPElement &AxisymmetricUpdatedLagrangianUJwPElement::operator=(AxisymmetricUpdatedLagrangianUJwPElement const &rOther)
{
   AxisymmetricUpdatedLagrangianUJacobianElement::operator=(rOther);

   return *this;
}

//*********************************OPERATIONS*****************************************
//************************************************************************************

Element::Pointer AxisymmetricUpdatedLagrangianUJwPElement::Create(IndexType NewId, NodesArrayType const &rThisNodes, PropertiesType::Pointer pProperties) const
{
   return Kratos::make_intrusive<AxisymmetricUpdatedLagrangianUJwPElement>(NewId, GetGeometry().Create(rThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************

Element::Pointer AxisymmetricUpdatedLagrangianUJwPElement::Clone(IndexType NewId, NodesArrayType const &rThisNodes) const
{

   AxisymmetricUpdatedLagrangianUJwPElement NewElement(NewId, GetGeometry().Create(rThisNodes), pGetProperties());

   //-----------//

   NewElement.mThisIntegrationMethod = mThisIntegrationMethod;

   if (NewElement.mConstitutiveLawVector.size() != mConstitutiveLawVector.size())
   {
      NewElement.mConstitutiveLawVector.resize(mConstitutiveLawVector.size());

      if (NewElement.mConstitutiveLawVector.size() != NewElement.GetGeometry().IntegrationPointsNumber())
         KRATOS_THROW_ERROR(std::logic_error, "constitutive law not has the correct size ", NewElement.mConstitutiveLawVector.size())
   }

   for (unsigned int i = 0; i < mConstitutiveLawVector.size(); i++)
   {
      NewElement.mConstitutiveLawVector[i] = mConstitutiveLawVector[i]->Clone();
   }

   //-----------//

   if (NewElement.mDeformationGradientF0.size() != mDeformationGradientF0.size())
      NewElement.mDeformationGradientF0.resize(mDeformationGradientF0.size());

   for (unsigned int i = 0; i < mDeformationGradientF0.size(); i++)
   {
      NewElement.mDeformationGradientF0[i] = mDeformationGradientF0[i];
   }

   NewElement.mDeterminantF0 = mDeterminantF0;

   NewElement.SetData(this->GetData());
   NewElement.SetFlags(this->GetFlags());

   return Kratos::make_intrusive<AxisymmetricUpdatedLagrangianUJwPElement>(NewElement);
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUJwPElement::~AxisymmetricUpdatedLagrangianUJwPElement()
{
}

//************* GETTING METHODS ******************************************************
//************************************************************************************
//************************************************************************************
//************* GETTING METHODS ******************************************************
//************************************************************************************
//************************************************************************************
void AxisymmetricUpdatedLagrangianUJwPElement::CalculateOnIntegrationPoints(const Variable<Matrix> &rVariable, std::vector<Matrix> &rValues, const ProcessInfo &rCurrentProcessInfo)
{

   KRATOS_TRY
   
   const unsigned int &integration_points_number = GetGeometry().IntegrationPointsNumber(mThisIntegrationMethod);
   
   if (rValues.size() != integration_points_number)
      rValues.resize(integration_points_number);

   if ( rVariable == PERMEABILITY_TENSOR) {
      for (unsigned int PointNumber = 0; PointNumber < integration_points_number; PointNumber++) {

         rValues[PointNumber] = ZeroMatrix(3,3);
         WaterPressureUtilities WaterUtility;
         WaterUtility.GetPermeabilityTensorAndReshape( GetProperties(), mDeformationGradientF0[PointNumber], rValues[PointNumber], GetProperties()[INITIAL_POROSITY], 3, MathUtils<double>::Det(mDeformationGradientF0[PointNumber]), 0.0);
      }
   } else {
      AxisymmetricUpdatedLagrangianUJacobianElement::CalculateOnIntegrationPoints( rVariable, rValues, rCurrentProcessInfo);
   }


   KRATOS_CATCH("")
}

void AxisymmetricUpdatedLagrangianUJwPElement::CalculateOnIntegrationPoints(const Variable<Vector> &rVariable, std::vector<Vector> &rValues, const ProcessInfo &rCurrentProcessInfo)
{

   KRATOS_TRY

   const unsigned int &integration_points_number = GetGeometry().IntegrationPointsNumber(mThisIntegrationMethod);

   if (rValues.size() != integration_points_number)
      rValues.resize(integration_points_number);

   UpdatedLagrangianUJacobianElement::CalculateOnIntegrationPoints(rVariable, rValues, rCurrentProcessInfo);

   KRATOS_CATCH("")
}

void AxisymmetricUpdatedLagrangianUJwPElement::GetDofList(DofsVectorType &rElementalDofList, const ProcessInfo &rCurrentProcessInfo) const
{
   rElementalDofList.resize(0);

   for (unsigned int i = 0; i < GetGeometry().size(); i++)
   {
      rElementalDofList.push_back(GetGeometry()[i].pGetDof(DISPLACEMENT_X));
      rElementalDofList.push_back(GetGeometry()[i].pGetDof(DISPLACEMENT_Y));
      rElementalDofList.push_back(GetGeometry()[i].pGetDof(JACOBIAN));
      rElementalDofList.push_back(GetGeometry()[i].pGetDof(WATER_PRESSURE));
   }
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJwPElement::EquationIdVector(EquationIdVectorType &rResult, const ProcessInfo &rCurrentProcessInfo) const
{
   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   unsigned int element_size = number_of_nodes * dimension + 2 * number_of_nodes;

   if (rResult.size() != element_size)
      rResult.resize(element_size, false);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      int index = i * (dimension + 2);
      rResult[index] = GetGeometry()[i].GetDof(DISPLACEMENT_X).EquationId();
      rResult[index + 1] = GetGeometry()[i].GetDof(DISPLACEMENT_Y).EquationId();
      rResult[index + 2] = GetGeometry()[i].GetDof(JACOBIAN).EquationId();
      rResult[index + 3] = GetGeometry()[i].GetDof(WATER_PRESSURE).EquationId();
   }
}

//*********************************DISPLACEMENT***************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJwPElement::GetValuesVector(Vector &rValues, int Step) const
{
   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   unsigned int element_size = number_of_nodes * dimension + 2 * number_of_nodes;

   if (rValues.size() != element_size)
      rValues.resize(element_size, false);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      unsigned int index = i * (dimension + 2);
      rValues[index] = GetGeometry()[i].GetSolutionStepValue(DISPLACEMENT_X, Step);
      rValues[index + 1] = GetGeometry()[i].GetSolutionStepValue(DISPLACEMENT_Y, Step);
      rValues[index + 2] = GetGeometry()[i].GetSolutionStepValue(JACOBIAN, Step);
      rValues[index + 3] = GetGeometry()[i].GetSolutionStepValue(WATER_PRESSURE, Step);
   }
}

//************************************VELOCITY****************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJwPElement::GetFirstDerivativesVector(Vector &rValues, int Step) const
{
   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   unsigned int element_size = number_of_nodes * dimension + 2 * number_of_nodes;

   if (rValues.size() != element_size)
      rValues.resize(element_size, false);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      unsigned int index = i * (dimension + 2);
      rValues[index] = GetGeometry()[i].GetSolutionStepValue(VELOCITY_X, Step);
      rValues[index + 1] = GetGeometry()[i].GetSolutionStepValue(VELOCITY_Y, Step);
      rValues[index + 2] = 0;
      rValues[index + 3] = 0;
   }
}

//*********************************ACCELERATION***************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJwPElement::GetSecondDerivativesVector(Vector &rValues, int Step) const
{
   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   unsigned int element_size = number_of_nodes * dimension + 2 * number_of_nodes;

   if (rValues.size() != element_size)
      rValues.resize(element_size, false);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      unsigned int index = i * (dimension + 2);
      rValues[index] = GetGeometry()[i].GetSolutionStepValue(ACCELERATION_X, Step);
      rValues[index + 1] = GetGeometry()[i].GetSolutionStepValue(ACCELERATION_Y, Step);
      rValues[index + 2] = 0;
      rValues[index + 3] = 0;
   }
}

// ********************** Initialize General Variables ***********************************
// ***************************************************************************************

void AxisymmetricUpdatedLagrangianUJwPElement::InitializeElementData(ElementDataType &rVariables, const ProcessInfo &rCurrentProcessInfo) const
{
   AxisymmetricUpdatedLagrangianUJacobianElement::InitializeElementData(rVariables, rCurrentProcessInfo);

   //stabilization factor
   if (!GetProperties().Has(STABILIZATION_FACTOR_WP))
     KRATOS_ERROR << "STABILIZATION_FACTOR_WP not defined in properties " << std::endl;

}

//************************************************************************************
//************************************************************************************

int AxisymmetricUpdatedLagrangianUJwPElement::Check(const ProcessInfo &rCurrentProcessInfo) const
{
   KRATOS_TRY

   // Perform base element checks
   int ErrorCode = 0;
   ErrorCode = AxisymmetricUpdatedLagrangianUJacobianElement::Check(rCurrentProcessInfo);

   return ErrorCode;

   KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJwPElement::InitializeSystemMatrices(MatrixType &rLeftHandSideMatrix,
                                                                  VectorType &rRightHandSideVector,
                                                                  Flags &rCalculationFlags) const

{

   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();

   //resizing as needed the LHS
   unsigned int MatSize = number_of_nodes * dimension + 2 * number_of_nodes;

   if (rCalculationFlags.Is(LargeDisplacementElement::COMPUTE_LHS_MATRIX)) //calculation of the matrix is required
   {
      if (rLeftHandSideMatrix.size1() != MatSize)
         rLeftHandSideMatrix.resize(MatSize, MatSize, false);

      noalias(rLeftHandSideMatrix) = ZeroMatrix(MatSize, MatSize); //resetting LHS
   }

   //resizing as needed the RHS
   if (rCalculationFlags.Is(LargeDisplacementElement::COMPUTE_RHS_VECTOR)) //calculation of the matrix is required
   {
      if (rRightHandSideVector.size() != MatSize)
         rRightHandSideVector.resize(MatSize, false);

      rRightHandSideVector = ZeroVector(MatSize); //resetting RHS
   }
}

//************* COMPUTING  METHODS
//************************************************************************************
//************************************************************************************

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJwPElement::CalculateAndAddLHS(LocalSystemComponents &rLocalSystem, ElementDataType &rVariables, double &rIntegrationWeight)
{

   KRATOS_TRY

   // define some variables
   const unsigned int number_of_nodes = GetGeometry().PointsNumber();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   const double &time_step = rVariables.GetProcessInfo()[DELTA_TIME];
   rVariables.detF0 *= rVariables.detF;
   double DeterminantF = rVariables.detF;
   rVariables.detF = 1.0;

   //contributions of the stiffness matrix calculated on the reference configuration
   MatrixType &rLeftHandSideMatrix = rLocalSystem.GetLeftHandSideMatrix();

   // ComputeBaseClass LHS
   LocalSystemComponents UJLocalSystem;
   unsigned int MatSize = number_of_nodes * (dimension + 1);
   MatrixType LocalLeftHandSideMatrix = ZeroMatrix(MatSize, MatSize);
   UJLocalSystem.SetLeftHandSideMatrix(LocalLeftHandSideMatrix);

   // LHS. base class
   AxisymmetricUpdatedLagrangianUJacobianElement::CalculateAndAddLHS(UJLocalSystem, rVariables, rIntegrationWeight);

   double IntegrationWeight = rIntegrationWeight * 2.0 * Globals::Pi * rVariables.CurrentRadius / GetProperties()[THICKNESS];

   // Reshape the BaseClass LHS and Add the Hydro Part
   AxisymWaterPressureJacobianUtilities WaterUtility;
   Matrix TotalF = prod(rVariables.F, rVariables.F0);
   int number_of_variables = dimension + 2; // displ - Jacobian - waterPressure
   Vector VolumeForce;
   VolumeForce = this->CalculateVolumeForce(VolumeForce, rVariables);
   // 1. Create (make pointers) variables
   WaterPressureUtilities::HydroMechanicalVariables HMVariables(GetGeometry(), GetProperties());

   HMVariables.SetBMatrix(rVariables.B);
   HMVariables.SetShapeFunctionsDerivatives(rVariables.DN_DX);
   HMVariables.SetDeformationGradient(TotalF);
   HMVariables.SetVolumeForce(VolumeForce);
   HMVariables.SetShapeFunctions(rVariables.N);

   HMVariables.DeltaTime = time_step;
   HMVariables.detF0 = rVariables.detF0;
   HMVariables.detF  = DeterminantF;
   HMVariables.CurrentRadius = rVariables.CurrentRadius;
   //HMVariables.ConstrainedModulus
   HMVariables.number_of_variables = number_of_variables;

   // LHS. hydromechanical problem
   rLeftHandSideMatrix = WaterUtility.CalculateAndAddHydromechanicalLHS(HMVariables, rLeftHandSideMatrix, LocalLeftHandSideMatrix, IntegrationWeight);

   // LHS. water pressure stabilization
   ProcessInfo SomeProcessInfo;
   std::vector<double> Mmodulus;
   SolidElement::CalculateOnIntegrationPoints(M_MODULUS, Mmodulus, SomeProcessInfo);
   double ConstrainedModulus = Mmodulus[0];
   if (ConstrainedModulus < 1e-5)
   {
      const double &YoungModulus = GetProperties()[YOUNG_MODULUS];
      const double &nu = GetProperties()[POISSON_RATIO];
      ConstrainedModulus = YoungModulus * (1.0 - nu) / (1.0 + nu) / (1.0 - 2.0 * nu);
   }
   HMVariables.ConstrainedModulus = ConstrainedModulus;
   rLeftHandSideMatrix = WaterUtility.CalculateAndAddStabilizationLHS(HMVariables, rLeftHandSideMatrix, IntegrationWeight);

   rVariables.detF = DeterminantF;
   rVariables.detF0 /= rVariables.detF;

   KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJwPElement::CalculateAndAddRHS(LocalSystemComponents &rLocalSystem, ElementDataType &rVariables, Vector &rVolumeForce, double &rIntegrationWeight)
{
   KRATOS_TRY

   // define some variables
   const unsigned int number_of_nodes = GetGeometry().PointsNumber();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   const double &time_step = rVariables.GetProcessInfo()[DELTA_TIME];
   rVariables.detF0 *= rVariables.detF;
   double DeterminantF = rVariables.detF;
   rVariables.detF = 1.0;

   //contribution of the internal and external forces
   VectorType &rRightHandSideVector = rLocalSystem.GetRightHandSideVector();

   // Compute Base Class RHS
   LocalSystemComponents BaseClassLocalSystem;
   Vector BaseClassRightHandSideVector = ZeroVector((dimension + 1) * number_of_nodes);
   BaseClassLocalSystem.SetRightHandSideVector(BaseClassRightHandSideVector);
   Vector VolumeForce = rVolumeForce;
   VolumeForce *= 0.0;
   AxisymmetricUpdatedLagrangianUJacobianElement::CalculateAndAddRHS(BaseClassLocalSystem, rVariables, VolumeForce, rIntegrationWeight);

   double IntegrationWeight = rIntegrationWeight * 2.0 * Globals::Pi * rVariables.CurrentRadius / GetProperties()[THICKNESS];

   // Reshape the BaseClass RHS and Add the Hydro Part
   AxisymWaterPressureJacobianUtilities WaterUtility;
   Matrix TotalF = prod(rVariables.F, rVariables.F0);

   int number_of_variables = dimension + 2; // displacement - Jacobian - waterPressure

   // 1. Create (make pointers) variables
   WaterPressureUtilities::HydroMechanicalVariables HMVariables(GetGeometry(), GetProperties());

   HMVariables.SetBMatrix(rVariables.B);
   HMVariables.SetShapeFunctionsDerivatives(rVariables.DN_DX);
   HMVariables.SetDeformationGradient(TotalF);
   HMVariables.SetVolumeForce(rVolumeForce);
   HMVariables.SetShapeFunctions(rVariables.N);

   HMVariables.DeltaTime = time_step;
   HMVariables.detF0 = rVariables.detF0;
   HMVariables.detF  = DeterminantF;
   HMVariables.CurrentRadius = rVariables.CurrentRadius;
   //HMVariables.ConstrainedModulus
   HMVariables.number_of_variables = number_of_variables;

   rRightHandSideVector = WaterUtility.CalculateAndAddHydromechanicalRHS(HMVariables, rRightHandSideVector, BaseClassRightHandSideVector, IntegrationWeight);

   // Add Stab term
   ProcessInfo SomeProcessInfo;
   std::vector<double> Mmodulus;
   SolidElement::CalculateOnIntegrationPoints(M_MODULUS, Mmodulus, SomeProcessInfo);
   double ConstrainedModulus = Mmodulus[0];
   if (ConstrainedModulus < 1e-5)
   {
      const double &YoungModulus = GetProperties()[YOUNG_MODULUS];
      const double &nu = GetProperties()[POISSON_RATIO];
      ConstrainedModulus = YoungModulus * (1.0 - nu) / (1.0 + nu) / (1.0 - 2.0 * nu);
   }
   HMVariables.ConstrainedModulus = ConstrainedModulus;
   rRightHandSideVector = WaterUtility.CalculateAndAddStabilization(HMVariables, rRightHandSideVector, IntegrationWeight);

   rVariables.detF = DeterminantF;
   rVariables.detF0 /= rVariables.detF;

   KRATOS_CATCH("")
}


//************************************************************************************
//************************************************************************************
// Calculate mass matrix. copied from AxisymmetricUpdatedLagrangianUPElement
void AxisymmetricUpdatedLagrangianUJwPElement::CalculateMassMatrix(MatrixType &rMassMatrix, const ProcessInfo &rCurrentProcessInfo)
{
   KRATOS_TRY

   //lumped
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   const unsigned int number_of_nodes = GetGeometry().size();
   unsigned int MatSize = number_of_nodes * dimension + 2*number_of_nodes;

   if (rMassMatrix.size1() != MatSize)
      rMassMatrix.resize(MatSize, MatSize, false);

   rMassMatrix = ZeroMatrix(MatSize, MatSize);

   // Not Lumped Mass Matrix (numerical integration):

   //reading integration points
   IntegrationMethod CurrentIntegrationMethod = mThisIntegrationMethod; //GeometryData::IntegrationMethod::GI_GAUSS_2; //GeometryData::IntegrationMethod::GI_GAUSS_1;

   const GeometryType::IntegrationPointsArrayType &integration_points = GetGeometry().IntegrationPoints(CurrentIntegrationMethod);

   ElementDataType Variables;
   this->InitializeElementData(Variables, rCurrentProcessInfo);

   for (unsigned int PointNumber = 0; PointNumber < integration_points.size(); PointNumber++)
   {

      //compute element kinematics
      this->CalculateKinematics(Variables, PointNumber);

      //getting informations for integration
      double IntegrationWeight = integration_points[PointNumber].Weight() * Variables.detJ * 2.0 * Globals::Pi * Variables.CurrentRadius;

      //compute point volume change
      double PointVolumeChange = 0;
      PointVolumeChange = this->CalculateVolumeChange(PointVolumeChange, Variables);

      double CurrentDensity = PointVolumeChange * GetProperties()[DENSITY];

      for (unsigned int i = 0; i < number_of_nodes; i++)
      {
         unsigned int indexupi = (dimension + 2) * i ;

         for (unsigned int j = 0; j < number_of_nodes; j++)
         {
            unsigned int indexupj = (dimension + 2) * j ;

            for (unsigned int k = 0; k < dimension; k++)
            {
               rMassMatrix(indexupi + k, indexupj + k) += Variables.N[i] * Variables.N[j] * CurrentDensity * IntegrationWeight;
            }
         }
      }
   }

   // Lumped Mass Matrix:

   // double TotalMass = 0;

   // this->CalculateTotalMass( TotalMass, rCurrentProcessInfo );

   // Vector LumpFact = ZeroVector(number_of_nodes);

   // LumpFact = GetGeometry().LumpingFactors( LumpFact );

   // for ( unsigned int i = 0; i < number_of_nodes; i++ )
   // 	{
   // 	  double temp = LumpFact[i] * TotalMass;

   // 	  unsigned int indexup = dimension * i + i;

   // 	  for ( unsigned int j = 0; j < dimension; j++ )
   // 	    {
   // 	      rMassMatrix( indexup+j , indexup+j ) += temp;
   // 	    }
   // 	}

   // std::cout<<std::endl;
   // std::cout<<" Mass Matrix "<<rMassMatrix<<std::endl;

   KRATOS_CATCH("")
}


//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJwPElement::CalculateDampingMatrix(MatrixType &rDampingMatrix, const ProcessInfo &rCurrentProcessInfo)
{
   KRATOS_TRY

   //0.-Initialize the DampingMatrix:
   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();

   //resizing as needed the LHS
   unsigned int MatSize = number_of_nodes * (dimension + 2);

   if (rDampingMatrix.size1() != MatSize)
      rDampingMatrix.resize(MatSize, MatSize, false);

   noalias(rDampingMatrix) = ZeroMatrix(MatSize, MatSize);

   //1.-Calculate StiffnessMatrix:

   MatrixType LHSMatrix = Matrix();

   this->CalculateLeftHandSide(LHSMatrix, rCurrentProcessInfo);

   MatrixType StiffnessMatrix = Matrix();

   if (StiffnessMatrix.size1() != MatSize)
      StiffnessMatrix.resize(MatSize, MatSize, false);

   StiffnessMatrix = ZeroMatrix(MatSize, MatSize);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      //unsigned int indexup = i * ( dimension + 2);

      for (unsigned int j = 0; j < dimension; j++)
      {
         //StiffnessMatrix( indexup+j , indexup+j ) = LHSMatrix( indexup+j , indexup+j ); // això és una mica rotllo perquè agafa Kuu, que és diferent de K de displacement-based.
      }
   }

   //2.-Calculate MassMatrix:

   MatrixType MassMatrix = Matrix();

   this->CalculateMassMatrix(MassMatrix, rCurrentProcessInfo);

   //3.-Get Damping Coeffitients (RAYLEIGH_ALPHA, RAYLEIGH_BETA)
   double alpha = 0;
   if (GetProperties().Has(RAYLEIGH_ALPHA))
   {
      alpha = GetProperties()[RAYLEIGH_ALPHA];
   }
   else if (rCurrentProcessInfo.Has(RAYLEIGH_ALPHA))
   {
      alpha = rCurrentProcessInfo[RAYLEIGH_ALPHA];
   }

   double beta = 0;
   if (GetProperties().Has(RAYLEIGH_BETA))
   {
      beta = GetProperties()[RAYLEIGH_BETA];
   }
   else if (rCurrentProcessInfo.Has(RAYLEIGH_BETA))
   {
      beta = rCurrentProcessInfo[RAYLEIGH_BETA];
   }

   //4.-Compose the Damping Matrix:

   //Rayleigh Damping Matrix: alpha*M + beta*K
   rDampingMatrix = alpha * MassMatrix;
   rDampingMatrix += beta * StiffnessMatrix;

   KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJwPElement::save(Serializer &rSerializer) const
{
   KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, AxisymmetricUpdatedLagrangianUJacobianElement)
}

void AxisymmetricUpdatedLagrangianUJwPElement::load(Serializer &rSerializer)
{
   KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, AxisymmetricUpdatedLagrangianUJacobianElement)
}

} // namespace Kratos
