//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:           LMonforte $
//   Maintained by:       $Maintainer:                 LM $
//   Date:                $Date:             January 2022 $
//
//

#if !defined(KRATOS_AXISYMMETRIC_UNSAT_U_J_wP_ELEMENT_HPP_INCLUDED)
#define KRATOS_AXISYMMETRIC_UNSAT_U_J_wP_ELEMENT_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_elements/hydromechanical_elements/axisymmetric_updated_lagrangian_U_Jacobian_element.hpp"

namespace Kratos
{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Updated Lagrangian Large Displacement Lagrangian U-wP Element for 3D and 2D geometries. Linear Triangles and Tetrahedra (base class)

class KRATOS_API(SOLID_MECHANICS_APPLICATION) AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement
    : public AxisymmetricUpdatedLagrangianUJacobianElement
{
public:
       ///@name Type Definitions
       ///@{
       ///Reference type definition for constitutive laws
       typedef ConstitutiveLaw ConstitutiveLawType;
       ///Pointer type for constitutive laws
       typedef ConstitutiveLawType::Pointer ConstitutiveLawPointerType;
       ///StressMeasure from constitutive laws
       typedef ConstitutiveLawType::StressMeasure StressMeasureType;
       ///Type definition for integration methods
       typedef GeometryData::IntegrationMethod IntegrationMethod;

       /// Counted pointer of LargeDisplacementUPElement
       KRATOS_CLASS_INTRUSIVE_POINTER_DEFINITION(AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement);
       ///@}

       ///@name Life Cycle
       ///@{

       /// Empty constructor needed for serialization
       AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement();

       /// Default constructors
       AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement(IndexType NewId, GeometryType::Pointer pGeometry);

       AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties);

       ///Copy constructor
       AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement(AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement const &rOther);

       /// Destructor.
       virtual ~AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement();

       ///@}
       ///@name Operators
       ///@{

       /// Assignment operator.
       AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement &operator=(AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement const &rOther);

       ///@}
       ///@name Operations
       ///@{
       /**
          * Returns the currently selected integration method
          * @return current integration method selected
          */
       /**
          * creates a new total lagrangian updated element pointer
          * @param NewId: the ID of the new element
          * @param ThisNodes: the nodes of the new element
          * @param pProperties: the properties assigned to the new element
          * @return a Pointer to the new element
          */
       Element::Pointer Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const override;

       /**
          * clones the selected element variables, creating a new one
          * @param NewId: the ID of the new element
          * @param ThisNodes: the nodes of the new element
          * @param pProperties: the properties assigned to the new element
          * @return a Pointer to the new element
          */
       Element::Pointer Clone(IndexType NewId, NodesArrayType const &ThisNodes) const override;


       //GET:

       //on integration points:
       /**
          * Calculate a double Variable on the Element Constitutive Law
          */
       void CalculateOnIntegrationPoints(const Variable<double> &rVariable, std::vector<double> &rValues, const ProcessInfo &rCurrentProcessInfo) override;

       void CalculateOnIntegrationPoints(const Variable<Vector> &rVariable, std::vector<Vector> &rValues, const ProcessInfo &rCurrentProcessInfo) override;

       void CalculateOnIntegrationPoints(const Variable<Matrix> &rVariable, std::vector<Matrix> &rValues, const ProcessInfo &rCurrentProcessInfo) override;

       //************* STARTING - ENDING  METHODS

       /**
          * Sets on rElementalDofList the degrees of freedom of the considered element geometry
          */
       void GetDofList(DofsVectorType &rElementalDofList, const ProcessInfo &rCurrentProcessInfo) const override;

       /**
          * Sets on rResult the ID's of the element degrees of freedom
          */
       void EquationIdVector(EquationIdVectorType &rResult, const ProcessInfo &rCurrentProcessInfo) const override;

       /**
          * Sets on rValues the nodal displacements
          */
       void GetValuesVector(Vector &rValues, int Step = 0) const override;

       /**
          * Sets on rValues the nodal velocities
          */
       void GetFirstDerivativesVector(Vector &rValues, int Step = 0) const override;

       /**
          * Sets on rValues the nodal accelerations
          */
       void GetSecondDerivativesVector(Vector &rValues, int Step = 0) const override;

       //************************************************************************************
       //************************************************************************************
       /**
          * This function provides the place to perform checks on the completeness of the input.
          * It is designed to be called only once (or anyway, not often) typically at the beginning
          * of the calculations, so to verify that nothing is missing from the input
          * or that no common error is found.
          * @param rCurrentProcessInfo
          */
       int Check(const ProcessInfo &rCurrentProcessInfo) const override;

       ///@}
       ///@name Access
       ///@{

       ///@}
       ///@name Inquiry
       ///@{
       ///@}
       ///@name Input and output
       ///@{
       ///@}
       ///@name Friends
       ///@{
       ///@}
protected:
       ///@name Protected static Member Variables
       ///@{
       ///@}
       ///@name Protected member Variables
       ///@{
       ///@}
       ///@name Protected Operators
       ///@{
       ///@}
       ///@name Protected Operations
       ///@{

       /**
          * Calculation and addition of the matrices of the LHS
          */
    void WriteMatrix(Matrix A);
    void CalculatePerturbation(LocalSystemComponents &rLocalSystem,
                            ElementDataType &rVariables,
                            double &rIntegrationWeight, Matrix & rMatrix);

       void CalculateAndAddLHS(LocalSystemComponents &rLocalSystem,
                               ElementDataType &rVariables,
                               double &rIntegrationWeight) override;

       /**
          * Calculation and addition of the vectors of the RHS
          */

       void CalculateAndAddRHS(LocalSystemComponents &rLocalSystem,
                               ElementDataType &rVariables,
                               Vector &rVolumeForce,
                               double &rIntegrationWeight) override;

       /**
          * Initialize Element General Variables
          */
       void InitializeElementData(ElementDataType &rVariables,
                                  const ProcessInfo &rCurrentProcessInfo) const override;

       /**
          * Initialize System Matrices
          */
       void InitializeSystemMatrices(MatrixType &rLeftHandSideMatrix,
                                     VectorType &rRightHandSideVector,
                                     Flags &rCalculationFlags) const override;

       ///@}
       ///@name Protected  Access
       ///@{
       ///@}
       ///@name Protected Inquiry
       ///@{
       ///@}
       ///@name Protected LifeCycle
       ///@{
       ///@}

private:
       ///@name Static Member Variables
       ///@{
       ///@}
       ///@name Member Variables
       ///@{

       ///@}
       ///@name Private Operators
       ///@{

       ///@}
       ///@name Private Operations
       ///@{

       ///@}
       ///@name Private  Access
       ///@{
       ///@}

       ///@}
       ///@name Serialization
       ///@{
       friend class Serializer;

       // A private default constructor necessary for serialization

       void save(Serializer &rSerializer) const override;

       void load(Serializer &rSerializer) override;

       ///@name Private Inquiry
       ///@{
       ///@}
       ///@name Un accessible methods
       ///@{
       ///@}

}; // Class AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement

} // namespace Kratos
#endif // KRATOS_AXISYMMETRIC_UNSAT_U_J_wP_ELEMENT_HPP_INCLUDED
