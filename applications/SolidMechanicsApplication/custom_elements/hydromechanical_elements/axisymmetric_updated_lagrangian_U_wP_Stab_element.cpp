//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:           LMonforte $
//   Maintained by:       $Maintainer:                 LM $
//   Date:                $Date:                July 2015 $
//
//

// System includes

// External includes

// Project includes
#include "custom_elements/hydromechanical_elements/axisymmetric_updated_lagrangian_U_wP_Stab_element.hpp"
#include "solid_mechanics_application_variables.h"
#include "custom_utilities/axisym_water_pressure_utilities.hpp"
namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************
// Aquest a l'altre no hi és....
AxisymmetricUpdatedLagrangianUwPStabElement::AxisymmetricUpdatedLagrangianUwPStabElement()
    : AxisymmetricUpdatedLagrangianUwPElement()
{
   //DO NOT CALL IT: only needed for Register and Serialization!!!
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUwPStabElement::AxisymmetricUpdatedLagrangianUwPStabElement(IndexType NewId, GeometryType::Pointer pGeometry)
    : AxisymmetricUpdatedLagrangianUwPElement(NewId, pGeometry)
{
   //DO NOT ADD DOFS HERE!!!
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUwPStabElement::AxisymmetricUpdatedLagrangianUwPStabElement(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : AxisymmetricUpdatedLagrangianUwPElement(NewId, pGeometry, pProperties)
{
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUwPStabElement::AxisymmetricUpdatedLagrangianUwPStabElement(AxisymmetricUpdatedLagrangianUwPStabElement const &rOther)
    : AxisymmetricUpdatedLagrangianUwPElement(rOther)
{
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUwPStabElement &AxisymmetricUpdatedLagrangianUwPStabElement::operator=(AxisymmetricUpdatedLagrangianUwPStabElement const &rOther)
{
   AxisymmetricUpdatedLagrangianUwPElement::operator=(rOther);
   return *this;
}

//*********************************OPERATIONS*****************************************
//************************************************************************************

Element::Pointer AxisymmetricUpdatedLagrangianUwPStabElement::Create(IndexType NewId, NodesArrayType const &rThisNodes, PropertiesType::Pointer pProperties) const
{
   return Kratos::make_intrusive<AxisymmetricUpdatedLagrangianUwPStabElement>(NewId, GetGeometry().Create(rThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************

Element::Pointer AxisymmetricUpdatedLagrangianUwPStabElement::Clone(IndexType NewId, NodesArrayType const &rThisNodes) const
{

   AxisymmetricUpdatedLagrangianUwPStabElement NewElement(NewId, GetGeometry().Create(rThisNodes), pGetProperties());

   //-----------//

   NewElement.mThisIntegrationMethod = mThisIntegrationMethod;

   if (NewElement.mConstitutiveLawVector.size() != mConstitutiveLawVector.size())
   {
      NewElement.mConstitutiveLawVector.resize(mConstitutiveLawVector.size());

      if (NewElement.mConstitutiveLawVector.size() != NewElement.GetGeometry().IntegrationPointsNumber())
         KRATOS_THROW_ERROR(std::logic_error, "constitutive law not has the correct size ", NewElement.mConstitutiveLawVector.size())
   }

   for (unsigned int i = 0; i < mConstitutiveLawVector.size(); i++)
   {
      NewElement.mConstitutiveLawVector[i] = mConstitutiveLawVector[i]->Clone();
   }

   //-----------//

   if (NewElement.mDeformationGradientF0.size() != mDeformationGradientF0.size())
      NewElement.mDeformationGradientF0.resize(mDeformationGradientF0.size());

   for (unsigned int i = 0; i < mDeformationGradientF0.size(); i++)
   {
      NewElement.mDeformationGradientF0[i] = mDeformationGradientF0[i];
   }

   NewElement.mDeterminantF0 = mDeterminantF0;

   NewElement.SetData(this->GetData());
   NewElement.SetFlags(this->GetFlags());

   return Kratos::make_intrusive<AxisymmetricUpdatedLagrangianUwPStabElement>(NewElement);
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUwPStabElement::~AxisymmetricUpdatedLagrangianUwPStabElement()
{
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUwPStabElement::InitializeElementData(ElementDataType &rVariables,
                                                                  const ProcessInfo &rCurrentProcessInfo) const
{
   KRATOS_TRY

   AxisymmetricUpdatedLagrangianUwPElement::InitializeElementData(rVariables, rCurrentProcessInfo);

   //stabilization factor
   if (!GetProperties().Has(STABILIZATION_FACTOR_WP))
     KRATOS_ERROR << "STABILIZATION_FACTOR_WP not defined in properties " << std::endl;

   KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUwPStabElement::CalculateAndAddLHS(LocalSystemComponents &rLocalSystem, ElementDataType &rVariables, double &rIntegrationWeight)
{

   KRATOS_TRY

   // define some variables
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   rVariables.detF0 *= rVariables.detF;
   double DeterminantF = rVariables.detF;
   rVariables.detF = 1.0;

   // ComputeBaseClass LHS
   double IntegrationWeight = rIntegrationWeight;
   AxisymmetricUpdatedLagrangianUwPElement::CalculateAndAddLHS(rLocalSystem, rVariables, IntegrationWeight);

   //IntegrationWeight = rIntegrationWeight * 2.0 * Globals::Pi * rVariables.CurrentRadius / GetProperties()[THICKNESS];
   IntegrationWeight = rIntegrationWeight; 

   // Add stabilization
   MatrixType &rLeftHandSideMatrix = rLocalSystem.GetLeftHandSideMatrix();
   AxisymWaterPressureUtilities WaterUtility;
   int number_of_variables = dimension + 1;
   ProcessInfo SomeProcessInfo;
   std::vector<double> Mmodulus;
   SolidElement::CalculateOnIntegrationPoints(M_MODULUS, Mmodulus, SomeProcessInfo);
   double ConstrainedModulus = Mmodulus[0];
   if (ConstrainedModulus < 1e-5)
   {
      const double &YoungModulus = GetProperties()[YOUNG_MODULUS];
      const double &nu = GetProperties()[POISSON_RATIO];
      ConstrainedModulus = YoungModulus * (1.0 - nu) / (1.0 + nu) / (1.0 - 2.0 * nu);
   }
   // 1. Create (make pointers) variables
   WaterPressureUtilities::HydroMechanicalVariables HMVariables(GetGeometry(), GetProperties());
   const double &time_step = rVariables.GetProcessInfo()[DELTA_TIME];
   HMVariables.SetBMatrix(rVariables.B);
   HMVariables.SetShapeFunctionsDerivatives(rVariables.DN_DX);
   HMVariables.SetShapeFunctions(rVariables.N);
   Matrix TotalF = prod(rVariables.F, rVariables.F0);
   HMVariables.SetDeformationGradient(TotalF);

   HMVariables.DeltaTime = time_step;
   HMVariables.detF0 = rVariables.detF0;
   HMVariables.detF  = DeterminantF;
   HMVariables.CurrentRadius = rVariables.CurrentRadius;
   HMVariables.ConstrainedModulus = ConstrainedModulus;
   HMVariables.number_of_variables = number_of_variables;

   rLeftHandSideMatrix = WaterUtility.CalculateAndAddStabilizationLHS(HMVariables, rLeftHandSideMatrix, IntegrationWeight);

   rVariables.detF = DeterminantF;
   rVariables.detF0 /= rVariables.detF;

   KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUwPStabElement::CalculateAndAddRHS(LocalSystemComponents &rLocalSystem, ElementDataType &rVariables, Vector &rVolumeForce, double &rIntegrationWeight)
{
   KRATOS_TRY

   // define some variables
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   rVariables.detF0 *= rVariables.detF;
   double DeterminantF = rVariables.detF;
   rVariables.detF = 1.0;

   // Compute Base Class RHS
   double IntegrationWeight = rIntegrationWeight;
   AxisymmetricUpdatedLagrangianUwPElement::CalculateAndAddRHS(rLocalSystem, rVariables, rVolumeForce, rIntegrationWeight);

   // Add stabilization
   Vector &rRightHandSideVector = rLocalSystem.GetRightHandSideVector();
   AxisymWaterPressureUtilities WaterUtility;
   int number_of_variables = dimension + 1;
   //IntegrationWeight = rIntegrationWeight * 2.0 * Globals::Pi * rVariables.CurrentRadius / GetProperties()[THICKNESS];
   IntegrationWeight = rIntegrationWeight; 
   ProcessInfo SomeProcessInfo;
   std::vector<double> Mmodulus;
   SolidElement::CalculateOnIntegrationPoints(M_MODULUS, Mmodulus, SomeProcessInfo);
   double ConstrainedModulus = Mmodulus[0];
   if (ConstrainedModulus < 1e-5)
   {
      const double &YoungModulus = GetProperties()[YOUNG_MODULUS];
      const double &nu = GetProperties()[POISSON_RATIO];
      ConstrainedModulus = YoungModulus * (1.0 - nu) / (1.0 + nu) / (1.0 - 2.0 * nu);
   }

   // 1. Create (make pointers) variables
   WaterPressureUtilities::HydroMechanicalVariables HMVariables(GetGeometry(), GetProperties());
   const double &time_step = rVariables.GetProcessInfo()[DELTA_TIME];
   HMVariables.SetBMatrix(rVariables.B);
   HMVariables.SetShapeFunctionsDerivatives(rVariables.DN_DX);
   HMVariables.SetShapeFunctions(rVariables.N);
   Matrix TotalF = prod(rVariables.F, rVariables.F0);
   HMVariables.SetDeformationGradient(TotalF);

   HMVariables.DeltaTime = time_step;
   HMVariables.detF0 = rVariables.detF0;
   HMVariables.detF  = DeterminantF;
   HMVariables.CurrentRadius = rVariables.CurrentRadius;
   HMVariables.ConstrainedModulus = ConstrainedModulus;
   HMVariables.number_of_variables = number_of_variables;

   rRightHandSideVector = WaterUtility.CalculateAndAddStabilization(HMVariables, rRightHandSideVector, IntegrationWeight);

   rVariables.detF = DeterminantF;
   rVariables.detF0 /= rVariables.detF;

   KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUwPStabElement::save(Serializer &rSerializer) const
{
   KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, AxisymmetricUpdatedLagrangianUwPElement)
}

void AxisymmetricUpdatedLagrangianUwPStabElement::load(Serializer &rSerializer)
{
   KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, AxisymmetricUpdatedLagrangianUwPElement)
}

} // namespace Kratos
