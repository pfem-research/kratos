//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:           LMonforte $
//   Maintained by:       $Maintainer:                 LM $
//   Date:                $Date:             October 2017 $
//
//
//   Implementation of the Fluid Saturated porous media in a u-w-pw Formulation

// System includes

// External includes

// Project includes
#include "custom_elements/hydromechanical_elements/axisymmetric_updated_lagrangian_U_J_W_wP_element.hpp"
#include "includes/constitutive_law.h"
#include "solid_mechanics_application_variables.h"

namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************
AxisymmetricUpdatedLagrangianUJWwPElement::AxisymmetricUpdatedLagrangianUJWwPElement()
    : AxisymmetricUpdatedLagrangianUJacobianElement()
{
   //DO NOT CALL IT: only needed for Register and Serialization!!!
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUJWwPElement::AxisymmetricUpdatedLagrangianUJWwPElement(IndexType NewId, GeometryType::Pointer pGeometry)
    : AxisymmetricUpdatedLagrangianUJacobianElement(NewId, pGeometry)
{
   //DO NOT ADD DOFS HERE!!!
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUJWwPElement::AxisymmetricUpdatedLagrangianUJWwPElement(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : AxisymmetricUpdatedLagrangianUJacobianElement(NewId, pGeometry, pProperties)
{
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUJWwPElement::AxisymmetricUpdatedLagrangianUJWwPElement(AxisymmetricUpdatedLagrangianUJWwPElement const &rOther)
    : AxisymmetricUpdatedLagrangianUJacobianElement(rOther)
{
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUJWwPElement &AxisymmetricUpdatedLagrangianUJWwPElement::operator=(AxisymmetricUpdatedLagrangianUJWwPElement const &rOther)
{
   AxisymmetricUpdatedLagrangianUJacobianElement::operator=(rOther);
   return *this;
}

//*********************************OPERATIONS*****************************************
//************************************************************************************

Element::Pointer AxisymmetricUpdatedLagrangianUJWwPElement::Create(IndexType NewId, NodesArrayType const &rThisNodes, PropertiesType::Pointer pProperties) const
{
   return Kratos::make_intrusive<AxisymmetricUpdatedLagrangianUJWwPElement>(NewId, GetGeometry().Create(rThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************

Element::Pointer AxisymmetricUpdatedLagrangianUJWwPElement::Clone(IndexType NewId, NodesArrayType const &rThisNodes) const
{

   AxisymmetricUpdatedLagrangianUJWwPElement NewElement(NewId, GetGeometry().Create(rThisNodes), pGetProperties());

   //-----------//

   NewElement.mThisIntegrationMethod = mThisIntegrationMethod;

   if (NewElement.mConstitutiveLawVector.size() != mConstitutiveLawVector.size())
   {
      NewElement.mConstitutiveLawVector.resize(mConstitutiveLawVector.size());

      if (NewElement.mConstitutiveLawVector.size() != NewElement.GetGeometry().IntegrationPointsNumber())
         KRATOS_THROW_ERROR(std::logic_error, "constitutive law not has the correct size ", NewElement.mConstitutiveLawVector.size())
   }

   for (unsigned int i = 0; i < mConstitutiveLawVector.size(); i++)
   {
      NewElement.mConstitutiveLawVector[i] = mConstitutiveLawVector[i]->Clone();
   }

   //-----------//

   if (NewElement.mDeformationGradientF0.size() != mDeformationGradientF0.size())
      NewElement.mDeformationGradientF0.resize(mDeformationGradientF0.size());

   for (unsigned int i = 0; i < mDeformationGradientF0.size(); i++)
   {
      NewElement.mDeformationGradientF0[i] = mDeformationGradientF0[i];
   }

   NewElement.mDeterminantF0 = mDeterminantF0;

   NewElement.SetData(this->GetData());
   NewElement.SetFlags(this->GetFlags());

   return Kratos::make_intrusive<AxisymmetricUpdatedLagrangianUJWwPElement>(NewElement);
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUJWwPElement::~AxisymmetricUpdatedLagrangianUJWwPElement()
{
}

//************* GETTING METHODS
//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJWwPElement::GetDofList(DofsVectorType &rElementalDofList, const ProcessInfo &rCurrentProcessInfo) const
{
   rElementalDofList.resize(0);

   for (unsigned int i = 0; i < GetGeometry().size(); i++)
   {
      rElementalDofList.push_back(GetGeometry()[i].pGetDof(DISPLACEMENT_X));
      rElementalDofList.push_back(GetGeometry()[i].pGetDof(DISPLACEMENT_Y));
      rElementalDofList.push_back(GetGeometry()[i].pGetDof(JACOBIAN));
      rElementalDofList.push_back(GetGeometry()[i].pGetDof(WATER_DISPLACEMENT_X));
      rElementalDofList.push_back(GetGeometry()[i].pGetDof(WATER_DISPLACEMENT_Y));
      rElementalDofList.push_back(GetGeometry()[i].pGetDof(WATER_PRESSURE));
   }
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJWwPElement::EquationIdVector(EquationIdVectorType &rResult, const ProcessInfo &rCurrentProcessInfo) const
{
   const unsigned int number_of_nodes = GetGeometry().size();
   unsigned int element_size = number_of_nodes * 6;
   unsigned int dofs_per_node = 6;

   if (rResult.size() != element_size)
      rResult.resize(element_size, false);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      int index = i * dofs_per_node;
      rResult[index] = GetGeometry()[i].GetDof(DISPLACEMENT_X).EquationId();
      rResult[index + 1] = GetGeometry()[i].GetDof(DISPLACEMENT_Y).EquationId();
      rResult[index + 2] = GetGeometry()[i].GetDof(JACOBIAN).EquationId();
      rResult[index + 3] = GetGeometry()[i].GetDof(WATER_DISPLACEMENT_X).EquationId();
      rResult[index + 4] = GetGeometry()[i].GetDof(WATER_DISPLACEMENT_Y).EquationId();
      rResult[index + 5] = GetGeometry()[i].GetDof(WATER_PRESSURE).EquationId();
   }
}

//*********************************DISPLACEMENT***************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJWwPElement::GetValuesVector(Vector &rValues, int Step) const
{
   const unsigned int number_of_nodes = GetGeometry().size();
   unsigned int element_size = number_of_nodes * 6;
   unsigned int dofs_per_node = 6;

   if (rValues.size() != element_size)
      rValues.resize(element_size, false);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      unsigned int index = i * dofs_per_node;
      rValues[index] = GetGeometry()[i].GetSolutionStepValue(DISPLACEMENT_X, Step);
      rValues[index + 1] = GetGeometry()[i].GetSolutionStepValue(DISPLACEMENT_Y, Step);
      rValues[index + 2] = GetGeometry()[i].GetSolutionStepValue(JACOBIAN, Step);
      rValues[index + 3] = GetGeometry()[i].GetSolutionStepValue(WATER_DISPLACEMENT_X, Step);
      rValues[index + 4] = GetGeometry()[i].GetSolutionStepValue(WATER_DISPLACEMENT_Y, Step);
      rValues[index + 5] = GetGeometry()[i].GetSolutionStepValue(WATER_PRESSURE, Step);
   }
}

//************************************VELOCITY****************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJWwPElement::GetFirstDerivativesVector(Vector &rValues, int Step) const
{
   const unsigned int number_of_nodes = GetGeometry().size();
   unsigned int element_size = number_of_nodes * 6;
   unsigned int dofs_per_node = 6;

   if (rValues.size() != element_size)
      rValues.resize(element_size, false);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      unsigned int index = i * dofs_per_node;
      rValues[index] = GetGeometry()[i].GetSolutionStepValue(VELOCITY_X, Step);
      rValues[index + 1] = GetGeometry()[i].GetSolutionStepValue(VELOCITY_Y, Step);
      rValues[index + 2] = 0.0;
      rValues[index + 3] = GetGeometry()[i].GetSolutionStepValue(WATER_VELOCITY_X, Step);
      rValues[index + 4] = GetGeometry()[i].GetSolutionStepValue(WATER_VELOCITY_Y, Step);
      rValues[index + 5] = GetGeometry()[i].GetSolutionStepValue(WATER_PRESSURE_VELOCITY, Step);
   }
}

//*********************************ACCELERATION***************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJWwPElement::GetSecondDerivativesVector(Vector &rValues, int Step) const
{
   const unsigned int number_of_nodes = GetGeometry().size();
   unsigned int element_size = number_of_nodes * 6;
   unsigned int dofs_per_node = 6;

   if (rValues.size() != element_size)
      rValues.resize(element_size, false);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      unsigned int index = i * dofs_per_node;

      rValues[index] = GetGeometry()[i].GetSolutionStepValue(ACCELERATION_X, Step);
      rValues[index + 1] = GetGeometry()[i].GetSolutionStepValue(ACCELERATION_Y, Step);
      rValues[index + 2] = 0.0;
      rValues[index + 3] = GetGeometry()[i].GetSolutionStepValue(WATER_ACCELERATION_X, Step);
      rValues[index + 4] = GetGeometry()[i].GetSolutionStepValue(WATER_ACCELERATION_Y, Step);
      rValues[index + 5] = GetGeometry()[i].GetSolutionStepValue(WATER_PRESSURE_ACCELERATION, Step);
   }
}

//************************************************************************************
//************************************************************************************

int AxisymmetricUpdatedLagrangianUJWwPElement::Check(const ProcessInfo &rCurrentProcessInfo) const
{
   KRATOS_TRY

   // Perform base element checks
   int ErrorCode = 0;
   ErrorCode = AxisymmetricUpdatedLagrangianUJacobianElement::Check(rCurrentProcessInfo);

   return ErrorCode;

   KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJWwPElement::InitializeElementData(ElementDataType &rVariables, const ProcessInfo &rCurrentProcessInfo) const
{
   AxisymmetricUpdatedLagrangianUJacobianElement::InitializeElementData(rVariables, rCurrentProcessInfo);

   // KC permeability constitutive equation checks
   if (!GetProperties().Has(KOZENY_CARMAN))
     KRATOS_ERROR << "KOZENY_CARMAN not defined in properties " << std::endl;

   if (!GetProperties().Has(INITIAL_POROSITY))
     KRATOS_ERROR << "INITIAL_POROSITY not defined in properties " << std::endl;

   if (!GetProperties().Has(STABILIZATION_FACTOR_WP))
     KRATOS_ERROR << "STABILIZATION_FACTOR_WP not defined in properties " << std::endl;
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJWwPElement::InitializeSystemMatrices(MatrixType &rLeftHandSideMatrix,
                                                                         VectorType &rRightHandSideVector,
                                                                         Flags &rCalculationFlags) const

{

   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();

   //resizing as needed the LHS
   unsigned int MatSize = number_of_nodes * (2 * dimension + 2);

   if (rCalculationFlags.Is(LargeDisplacementElement::COMPUTE_LHS_MATRIX)) //calculation of the matrix is required
   {
      if (rLeftHandSideMatrix.size1() != MatSize)
         rLeftHandSideMatrix.resize(MatSize, MatSize, false);

      noalias(rLeftHandSideMatrix) = ZeroMatrix(MatSize, MatSize); //resetting LHS
   }

   //resizing as needed the RHS
   if (rCalculationFlags.Is(LargeDisplacementElement::COMPUTE_RHS_VECTOR)) //calculation of the matrix is required
   {
      if (rRightHandSideVector.size() != MatSize)
         rRightHandSideVector.resize(MatSize, false);

      rRightHandSideVector = ZeroVector(MatSize); //resetting RHS
   }
}

//************* COMPUTING  METHODS
//************************************************************************************
//************************************************************************************

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUJWwPElement::CalculateAndAddLHS(LocalSystemComponents &rLocalSystem, ElementDataType &rVariables, double &rIntegrationWeight)
{

   KRATOS_TRY

   const unsigned int number_of_nodes = GetGeometry().PointsNumber();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   const unsigned int dofs_per_node = 2 * dimension + 2;

   rVariables.detF0 *= rVariables.detF;
   double DeterminantF = rVariables.detF;
   rVariables.detF = 1.0;

   //contributions of the stiffness matrix calculated on the reference configuration
   MatrixType &rLeftHandSideMatrix = rLocalSystem.GetLeftHandSideMatrix();

   // ComputeBaseClass LHS
   LocalSystemComponents UJLocalSystem;
   unsigned int MatSize = number_of_nodes * (dimension + 1);
   MatrixType LocalLeftHandSideMatrix = ZeroMatrix(MatSize, MatSize);
   UJLocalSystem.SetLeftHandSideMatrix(LocalLeftHandSideMatrix);

   // LHS. base class
   AxisymmetricUpdatedLagrangianUJacobianElement::CalculateAndAddLHS(UJLocalSystem, rVariables, rIntegrationWeight);

   double IntegrationWeight = rIntegrationWeight * 2.0 * Globals::Pi * rVariables.CurrentRadius / GetProperties()[THICKNESS];

   // Assemble the Kuj Left Hand Side
   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      for (unsigned int j = 0; j < number_of_nodes; j++)
      {
         for (unsigned int iDim = 0; iDim < dimension + 1; iDim++)
         {
            for (unsigned int jDim = 0; jDim < dimension + 1; jDim++)
            {
               rLeftHandSideMatrix(i * dofs_per_node + iDim, j * dofs_per_node + jDim) += LocalLeftHandSideMatrix(i * (dimension + 1) + iDim, j * (dimension + 1) + jDim);
            }
         }
      }
   }

   this->CalculateAndAddKWwP(rLeftHandSideMatrix, rVariables, IntegrationWeight);

   this->CalculateAndAddKUwP(rLeftHandSideMatrix, rVariables, IntegrationWeight);

   this->CalculateAndAddKPPStab(rLeftHandSideMatrix, rVariables, IntegrationWeight);

   this->CalculateAndAddHighOrderKPP(rLeftHandSideMatrix, rVariables, IntegrationWeight);

   rVariables.detF = DeterminantF;
   rVariables.detF0 /= rVariables.detF;

   KRATOS_CATCH("")
}

// **********************************************************************************
//          Matrix that may appear due to the stabilization matrix
void AxisymmetricUpdatedLagrangianUJWwPElement::CalculateAndAddKPPStab(MatrixType &rLeftHandSideMatrix, ElementDataType &rVariables, double &rIntegrationWeight)
{
   KRATOS_TRY

   KRATOS_CATCH("")
}

// **********************************************************************************
//          Matrix that may appear due to the high order terms
void AxisymmetricUpdatedLagrangianUJWwPElement::CalculateAndAddHighOrderKPP(MatrixType &rLeftHandSideMatrix, ElementDataType &rVariables, double &rIntegrationWeight)
{
   KRATOS_TRY

   KRATOS_CATCH("")
}

//************************************************************************************
//         Matrix due to the the water pressure contribution to the internal forces
void AxisymmetricUpdatedLagrangianUJWwPElement::CalculateAndAddKUwP(MatrixType &rLeftHandSide, ElementDataType &rVariables, double &rIntegrationWeight)
{
   KRATOS_TRY

   const unsigned int number_of_nodes = GetGeometry().PointsNumber();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();

   unsigned int dofs_per_node = 2 * dimension + 2;

   Matrix Q = ZeroMatrix(number_of_nodes * dimension, number_of_nodes);

   SizeType voigtSize = this->GetVoigtSize();
   Matrix m = ZeroMatrix(voigtSize, 1);

   for (unsigned int i = 0; i < 3; i++)
      m(i, 0) = 1.0;
   Matrix partial = prod(trans(rVariables.B), m);

   for (unsigned int i = 0; i < dimension * number_of_nodes; i++)
   {
      for (unsigned int j = 0; j < number_of_nodes; j++)
      {
         Q(i, j) = partial(i, 0) * rVariables.N[j] * rIntegrationWeight;
      }
   }

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      for (unsigned int iDim = 0; iDim < dimension; iDim++)
      {
         for (unsigned int j = 0; j < number_of_nodes; j++)
         {
            rLeftHandSide(i * dofs_per_node + iDim, (j + 1) * dofs_per_node - 1) -= Q(i * dimension + iDim, j);
         }
      }
   }

   KRATOS_CATCH("")
}

//************************************************************************************
//      Water material Matrix
void AxisymmetricUpdatedLagrangianUJWwPElement::CalculateAndAddKWwP(MatrixType &rLeftHandSide, ElementDataType &rVariables, double &rIntegrationWeight)
{
   KRATOS_TRY

   const unsigned int number_of_nodes = GetGeometry().PointsNumber();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();

   unsigned int dofs_per_node = 2 * dimension + 2;

   Matrix SmallMatrix = ZeroMatrix(number_of_nodes * dimension, number_of_nodes);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      for (unsigned int iDim = 0; iDim < dimension; iDim++)
      {
         for (unsigned int j = 0; j < number_of_nodes; j++)
         {
            SmallMatrix(i * dimension + iDim, j) = rVariables.N[i] * rVariables.DN_DX(j, iDim) * rIntegrationWeight;
            //if (iDim==0)
            //   SmallMatrix( i*dimension+iDim, j ) += rVariables.N[i] * rVariables.N(j)/rVariables.CurrentRadius * rIntegrationWeight;
         }
      }
   }

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      for (unsigned int j = 0; j < number_of_nodes; j++)
      {
         for (unsigned int iDim = 0; iDim < dimension; iDim++)
         {
            rLeftHandSide(i * dofs_per_node + (dimension + 1) + iDim, (j + 1) * dofs_per_node - 1) += SmallMatrix(i * dimension + iDim, j);
         }
      }
   }

   KRATOS_CATCH("")
}

// ***********************************************************************************
// ***********************************************************************************
void AxisymmetricUpdatedLagrangianUJWwPElement::CalculateAndAddRHS(LocalSystemComponents &rLocalSystem, ElementDataType &rVariables, Vector &rVolumeForce, double &rIntegrationWeight)
{
   KRATOS_TRY

   // define some variables
   const unsigned int number_of_nodes = GetGeometry().PointsNumber();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   const unsigned int dofs_per_node = 2 * (dimension + 1);

   rVariables.detF0 *= rVariables.detF;
   double DeterminantF = rVariables.detF;
   rVariables.detF = 1.0;

   //contribution of the internal and external forces
   VectorType &rRightHandSideVector = rLocalSystem.GetRightHandSideVector();

   // Compute Base Class RHS
   LocalSystemComponents BaseClassLocalSystem;
   Vector BaseClassRightHandSideVector = ZeroVector((dimension + 1) * number_of_nodes);
   BaseClassLocalSystem.SetRightHandSideVector(BaseClassRightHandSideVector);
   Vector VolumeForce = rVolumeForce;
   VolumeForce *= 0.0;
   AxisymmetricUpdatedLagrangianUJacobianElement::CalculateAndAddRHS(BaseClassLocalSystem, rVariables, VolumeForce, rIntegrationWeight);

   double IntegrationWeight = rIntegrationWeight * 2.0 * Globals::Pi * rVariables.CurrentRadius / GetProperties()[THICKNESS];

   // Assemble the "UJ" RHS
   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      for (unsigned int iDim = 0; iDim < dimension + 1; iDim++)
      {
         rRightHandSideVector(i * dofs_per_node + iDim) += BaseClassRightHandSideVector(i * (dimension + 1) + iDim);
      }
   }

   this->CalculateAndAddExternalForcesUJWwP(rRightHandSideVector, rVariables, rVolumeForce, IntegrationWeight);

   this->CalculateAndAddInternalWaterForces(rRightHandSideVector, rVariables, IntegrationWeight);

   // Calculate the linear momentum of the fluid
   CalculateAndAddFluidLinearMomentum(rRightHandSideVector, rVariables, IntegrationWeight);

   // Calculate mass balance equation
   CalculateAndAddMassBalanceEquation(rRightHandSideVector, rVariables, IntegrationWeight);

   this->CalculateAndAddStabilizationRHS(rRightHandSideVector, rVariables, IntegrationWeight);
   this->CalculateAndAddHighOrderRHS(rRightHandSideVector, rVariables, IntegrationWeight);

   rVariables.detF = DeterminantF;
   rVariables.detF0 /= rVariables.detF;

   KRATOS_CATCH("")
}

// *********************************************************************************
//    part of the RHS that goes directly to the RHS
void AxisymmetricUpdatedLagrangianUJWwPElement::CalculateAndAddStabilizationRHS(VectorType &rRightHandSideVector, ElementDataType &rVariables, double &rIntegrationWeight)
{
   KRATOS_TRY

   KRATOS_CATCH("")
}

// *********************************************************************************
//    High order part of the RHS that goes directly to the RHS
void AxisymmetricUpdatedLagrangianUJWwPElement::CalculateAndAddHighOrderRHS(VectorType &rRightHandSideVector, ElementDataType &rVariables, double &rIntegrationWeight)
{
   KRATOS_TRY

   KRATOS_CATCH("")
}

// **********************************************************************************
//    mass balance equation of the mixture (aka: Darcy's Law )
void AxisymmetricUpdatedLagrangianUJWwPElement::CalculateAndAddMassBalanceEquation(VectorType &rRightHandSideVector, ElementDataType &rVariables, double &rIntegrationWeight)
{
   KRATOS_TRY
   // a convective term may go here. not coded yet.
   KRATOS_CATCH("")
}

// **********************************************************************************
//    linear momentum balance equation of the fluid phase (aka: Darcy's Law )
void AxisymmetricUpdatedLagrangianUJWwPElement::CalculateAndAddFluidLinearMomentum(VectorType &rRightHandSideVector, ElementDataType &rVariables, double &rIntegrationWeight)
{
   KRATOS_TRY

   const unsigned int number_of_nodes = GetGeometry().PointsNumber();
   unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   unsigned int dofs_per_node = 2 * dimension + 2;

   Vector GradP = ZeroVector(dimension);
   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      const double &rWaterPressure = GetGeometry()[i].FastGetSolutionStepValue(WATER_PRESSURE);
      for (unsigned int iDim = 0; iDim < dimension; iDim++)
      {
         GradP(iDim) += rVariables.DN_DX(i, iDim) * rWaterPressure;
      }
   }

   Vector SmallRHS = ZeroVector(dimension * number_of_nodes);
   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      for (unsigned int iDim = 0; iDim < dimension; iDim++)
      {
         SmallRHS(i * dimension + iDim) = rVariables.N(i) * GradP(iDim) * rIntegrationWeight;
      }
   }

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      for (unsigned int iDim = 0; iDim < dimension; iDim++)
      {
         rRightHandSideVector(i * dofs_per_node + (dimension + 1) + iDim) -= SmallRHS(i * dimension + iDim);
      }
   }

   // LMV. a term due to the water pressure weight is missing. Please coded ASAP!!

   KRATOS_CATCH("")
}

// **************************************************************************
// Calculate and Add volumetric loads
void AxisymmetricUpdatedLagrangianUJWwPElement::CalculateAndAddExternalForcesUJWwP(VectorType &rRightHandSideVector, ElementDataType &rVariables,
                                                                             Vector &rVolumeForce, double &rIntegrationWeight)
{
   KRATOS_TRY

   const unsigned int number_of_nodes = GetGeometry().PointsNumber();
   unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   unsigned int dofs_per_node = 2 * dimension + 2;

   rVolumeForce *= rVariables.detF0;
   double density_mixture0 = GetProperties().GetValue(DENSITY);
   if (density_mixture0 > 0)
   {
      rVolumeForce /= density_mixture0;
   }
   else
   {
      return;
   }

   double density_water = GetProperties().GetValue(DENSITY_WATER);
   double porosity0 = GetProperties().GetValue(INITIAL_POROSITY);

   double porosity = 1.0 - (1.0 - porosity0) / rVariables.detF0;
   double density_solid = (density_mixture0 - porosity0 * density_water) / (1.0 - porosity0);
   double density_mixture = (1.0 - porosity) * density_solid + porosity * density_water;

   const VectorType &rN = rVariables.N;
   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      for (unsigned int iDim = 0; iDim < dimension; iDim++)
      {
         rRightHandSideVector(i * dofs_per_node + iDim) += rIntegrationWeight * density_mixture * rN(i) * rVolumeForce(iDim);
      }
   }

   rVolumeForce /= rVariables.detF0;
   rVolumeForce *= density_mixture0;
   return;

   KRATOS_CATCH("")
}

// **********************************************************************************
//         CalculateAndAddInternalForces
void AxisymmetricUpdatedLagrangianUJWwPElement::CalculateAndAddInternalWaterForces(VectorType &rRightHandSideVector,
                                                                             ElementDataType &rVariables,
                                                                             double &rIntegrationWeight)
{
   KRATOS_TRY

   const unsigned int number_of_nodes = GetGeometry().PointsNumber();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   unsigned int dofs_per_node = 2 * dimension + 2;

   double WaterPressure = 0.0;
   for (unsigned int i = 0; i < number_of_nodes; i++)
      WaterPressure += GetGeometry()[i].FastGetSolutionStepValue(WATER_PRESSURE) * rVariables.N[i];

   unsigned int voigt_size = 4;
   Vector StressVector = ZeroVector(voigt_size);
   for (unsigned int i = 0; i < 3; i++)
      StressVector(i) -= WaterPressure;

   VectorType InternalForces = rIntegrationWeight * prod(trans(rVariables.B), StressVector);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      for (unsigned int j = 0; j < dimension; j++)
      {
         rRightHandSideVector(i * dofs_per_node + j) -= InternalForces(i * dimension + j);
      }
   }

   KRATOS_CATCH("")
}

// *********************************************************************************
//         Calculate the Mass matrix
void AxisymmetricUpdatedLagrangianUJWwPElement::CalculateMassMatrix(MatrixType &rMassMatrix, const ProcessInfo &rCurrentProcessInfo)
{
   KRATOS_TRY

   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dofs_per_node = 2 * dimension + 2;
   unsigned int MatSize = number_of_nodes * (2 * dimension + 2);

   if (rMassMatrix.size1() != MatSize)
      rMassMatrix.resize(MatSize, MatSize, false);

   rMassMatrix = ZeroMatrix(MatSize, MatSize);

   //reading integration points
   IntegrationMethod CurrentIntegrationMethod = mThisIntegrationMethod; //GeometryData::IntegrationMethod::GI_GAUSS_2; //GeometryData::IntegrationMethod::GI_GAUSS_1;

   const GeometryType::IntegrationPointsArrayType &integration_points = GetGeometry().IntegrationPoints(CurrentIntegrationMethod);

   ElementDataType Variables;
   this->InitializeElementData(Variables, rCurrentProcessInfo);

   double density_mixture0 = GetProperties()[DENSITY];
   double WaterDensity = GetProperties().GetValue(DENSITY_WATER);
   double porosity0 = GetProperties().GetValue(INITIAL_POROSITY);

   double porosity = 1.0 - (1.0 - porosity0) / Variables.detF0;
   double density_solid = (density_mixture0 - porosity0 * WaterDensity) / (1.0 - porosity0);
   double CurrentDensity = (1.0 - porosity) * density_solid + porosity * WaterDensity;

   for (unsigned int PointNumber = 0; PointNumber < integration_points.size(); PointNumber++)
   {
      //compute element kinematics
      this->CalculateKinematics(Variables, PointNumber);

      //getting informations for integration
      double IntegrationWeight = integration_points[PointNumber].Weight() * Variables.detJ * 2.0 * Globals::Pi * Variables.CurrentRadius;

      for (unsigned int i = 0; i < number_of_nodes; i++)
      {
         for (unsigned int j = 0; j < number_of_nodes; j++)
         {
            for (unsigned int k = 0; k < dimension; k++)
            {
               rMassMatrix(dofs_per_node * i + k, dofs_per_node * j + k) += Variables.N[i] * Variables.N[j] * CurrentDensity * IntegrationWeight;
               rMassMatrix(dofs_per_node * i + (dimension + 1) + k, dofs_per_node * j + k) += Variables.N[i] * Variables.N[j] * WaterDensity * IntegrationWeight;
               rMassMatrix(dofs_per_node * i + k, dofs_per_node * j + (dimension + 1) + k) += Variables.N[i] * Variables.N[j] * WaterDensity * IntegrationWeight;
               rMassMatrix(dofs_per_node * i + (dimension + 1) + k, dofs_per_node * j + (dimension + 1) + k) += Variables.N[i] * Variables.N[j] * WaterDensity * IntegrationWeight / porosity;
            }
         }
      }

      this->CalculateAndAddMassStabilizationMatrix(rMassMatrix, Variables, IntegrationWeight);
   }

   KRATOS_CATCH("")
}

// ********************************************************************************
//      part of the mass matrix that steams from the stabilization factor
void AxisymmetricUpdatedLagrangianUJWwPElement::CalculateAndAddMassStabilizationMatrix(MatrixType &rMassMatrix, ElementDataType &rVariables, double &rIntegrationWeight)
{
   KRATOS_TRY

   KRATOS_CATCH("")
}

// ********************************************************************************
//      part of the damping matrix coming from the high order terms
void AxisymmetricUpdatedLagrangianUJWwPElement::CalculateAndAddHighOrderDampingMatrix(MatrixType &rDampingMatrix, ElementDataType &rVariables, double &rIntegrationWeight)
{
   KRATOS_TRY

   KRATOS_CATCH("")
}

// *********************************************************************************
//         Calculate the Damping matrix
void AxisymmetricUpdatedLagrangianUJWwPElement::CalculateDampingMatrix(MatrixType &rDampingMatrix, const ProcessInfo &rCurrentProcessInfo)
{
   KRATOS_TRY

   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dofs_per_node = 2 * dimension + 2;
   unsigned int MatSize = number_of_nodes * (2 * dimension + 2);

   if (rDampingMatrix.size1() != MatSize)
      rDampingMatrix.resize(MatSize, MatSize, false);

   rDampingMatrix = ZeroMatrix(MatSize, MatSize);

   //reading integration points
   IntegrationMethod CurrentIntegrationMethod = mThisIntegrationMethod; //GeometryData::IntegrationMethod::GI_GAUSS_2; //GeometryData::IntegrationMethod::GI_GAUSS_1;

   const GeometryType::IntegrationPointsArrayType &integration_points = GetGeometry().IntegrationPoints(CurrentIntegrationMethod);

   ElementDataType Variables;
   this->InitializeElementData(Variables, rCurrentProcessInfo);

   double CurrentPermeability = GetProperties()[PERMEABILITY_WATER];

   for (unsigned int PointNumber = 0; PointNumber < integration_points.size(); PointNumber++)
   {
      //compute element kinematics
      this->CalculateKinematics(Variables, PointNumber);

      //std::cout << "N = " << Variables.N << std::endl;

      //getting informations for integration
      double IntegrationWeight = integration_points[PointNumber].Weight() * Variables.detJ * 2.0 * Globals::Pi * Variables.CurrentRadius;

      for (unsigned int i = 0; i < number_of_nodes; i++)
      {
         for (unsigned int j = 0; j < number_of_nodes; j++)
         {
            for (unsigned int k = 0; k < dimension; k++)
            {
               rDampingMatrix(dofs_per_node * i + (dimension + 1) + k, dofs_per_node * j + (dimension + 1) + k) += Variables.N[i] * Variables.N[j] * IntegrationWeight / CurrentPermeability;
            }
         }
      }

      // Q Matrix //

      Matrix Q = ZeroMatrix(number_of_nodes, dimension * number_of_nodes);
      SizeType voigtSize = this->GetVoigtSize();
      Matrix m = ZeroMatrix(1, voigtSize);
      for (unsigned int i = 0; i < 3; i++)
         m(0, i) = 1.0;
      Matrix partial = prod(m, Variables.B);
      for (unsigned int i = 0; i < number_of_nodes; i++)
      {
         for (unsigned int j = 0; j < dimension * number_of_nodes; j++)
         {
            Q(i, j) = Variables.N[i] * partial(0, j) * IntegrationWeight;
         }
      }

      for (unsigned int i = 0; i < number_of_nodes; i++)
      {
         for (unsigned int j = 0; j < number_of_nodes; j++)
         {
            for (unsigned int jDim = 0; jDim < dimension; jDim++)
            {
               rDampingMatrix((i + 1) * dofs_per_node - 1, j * dofs_per_node + jDim) += Q(i, j * dimension + jDim);
               rDampingMatrix((i + 1) * dofs_per_node - 1, j * dofs_per_node + jDim + (dimension + 1)) += Q(i, j * dimension + jDim);
            }
         }
      }

      Matrix Mass = ZeroMatrix(number_of_nodes, number_of_nodes);
      for (unsigned int i = 0; i < number_of_nodes; i++)
      {
         for (unsigned int j = 0; j < number_of_nodes; j++)
         {
            Mass(i, j) += Variables.N[i] * Variables.N[j] * IntegrationWeight;
         }
      }
      const double rWaterBulk = GetProperties()[WATER_BULK_MODULUS];
      Mass /= rWaterBulk;

      for (unsigned int i = 0; i < number_of_nodes; i++)
      {
         for (unsigned int j = 0; j < number_of_nodes; j++)
         {
            rDampingMatrix((i + 1) * dofs_per_node - 1, (j + 1) * dofs_per_node - 1) += Mass(i, j);
         }
      }

      this->CalculateAndAddDampingStabilizationMatrix(rDampingMatrix, Variables, IntegrationWeight);
      this->CalculateAndAddHighOrderDampingMatrix(rDampingMatrix, Variables, IntegrationWeight);

   } // end point
   KRATOS_CATCH("")
}

// ********************************************************************************
//      part of the damping matrix that steams from the stabilization factor
void AxisymmetricUpdatedLagrangianUJWwPElement::CalculateAndAddDampingStabilizationMatrix(MatrixType &rDampingMatrix, ElementDataType &rVariables, double &rIntegrationWeight)
{
   KRATOS_TRY

   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dofs_per_node = 2 * dimension + 2;

   const double &rStabilizationFactor = GetProperties()[STABILIZATION_FACTOR_WP];
   if ((fabs(rStabilizationFactor) > 1.0e-6) && dimension == 2)
   {

      double StabFactor = CalculateStabilizationFactor(rVariables, StabFactor);

      Matrix SmallMatrix = ZeroMatrix(number_of_nodes, number_of_nodes);

      double consistent;
      for (unsigned int i = 0; i < number_of_nodes; i++)
      {
         for (unsigned int j = 0; j < number_of_nodes; j++)
         {
            consistent = -1.0 * StabFactor / 18.0;
            if (i == j)
               consistent = 2.0 * StabFactor / 18.0;
            SmallMatrix(i, j) += consistent * rIntegrationWeight;
         }
      }

      for (unsigned int i = 0; i < number_of_nodes; i++)
      {
         for (unsigned int j = 0; j < number_of_nodes; j++)
         {
            rDampingMatrix((i + 1) * dofs_per_node - 1, (j + 1) * dofs_per_node - 1) += SmallMatrix(i, j);
         }
      }
   }

   KRATOS_CATCH("")
}
//************************************************************************************
//************************************************************************************

double &AxisymmetricUpdatedLagrangianUJWwPElement::CalculateStabilizationFactor(ElementDataType &rVariables, double &rStabFactor)
{
   KRATOS_TRY

   const unsigned int number_of_nodes = GetGeometry().PointsNumber();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   //const double &time_step = rVariables.GetProcessInfo()[DELTA_TIME];
   double ElementSize = 0;
   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      double aux = 0;
      for (unsigned int iDim = 0; iDim < dimension; iDim++)
      {
         aux += rVariables.DN_DX(i, iDim);
      }
      ElementSize += fabs(aux);
   }
   ElementSize *= sqrt(double(dimension));
   ElementSize = 4.0 / ElementSize;

   ProcessInfo SomeProcessInfo;
   std::vector<double> Mmodulus;
   SolidElement::CalculateOnIntegrationPoints(M_MODULUS, Mmodulus, SomeProcessInfo);
   double ConstrainedModulus = Mmodulus[0];
   if (ConstrainedModulus < 1e-5)
   {
      const double &YoungModulus = GetProperties()[YOUNG_MODULUS];
      const double &nu = GetProperties()[POISSON_RATIO];
      ConstrainedModulus = YoungModulus * (1.0 - nu) / (1.0 + nu) / (1.0 - 2.0 * nu);
   }

   double StabilizationFactor = GetProperties().GetValue(STABILIZATION_FACTOR_WP);

   //rStabFactor = 2.0 / ConstrainedModulus - 12.0 * rPermeability * time_step / pow(ElementSize, 2);
   //rStabFactor = 2.0 / ConstrainedModulus*(1-rPermeability*DryDensity/time_step/porosity) - 12.0 * rPermeability * time_step / pow(ElementSize, 2)*(1-porosity)/porosity;

   rStabFactor = 2.0 / ConstrainedModulus; // - 12.0 * rPermeability * time_step / pow(ElementSize, 2);
   rStabFactor *= StabilizationFactor;

   if (rStabFactor < 0)
      rStabFactor = 0;

   return rStabFactor;

   KRATOS_CATCH("")
}

void AxisymmetricUpdatedLagrangianUJWwPElement::save(Serializer &rSerializer) const
{
   KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, AxisymmetricUpdatedLagrangianUJacobianElement)
}

void AxisymmetricUpdatedLagrangianUJWwPElement::load(Serializer &rSerializer)
{
   KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, AxisymmetricUpdatedLagrangianUJacobianElement)
}

} // namespace Kratos
