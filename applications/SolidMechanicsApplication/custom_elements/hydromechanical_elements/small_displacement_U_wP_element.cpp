//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:           LMonforte $
//   Maintained by:       $Maintainer:                 LM $
//   Date:                $Date:             October 2017 $
//
//

//   Implementation of the Fluid Saturated porous media in a u-pw Formulation
//     (Reduction to small displacements)
//      with embeded time integration. I do not know how to make the time integration
//      work with this element
// System includes

// External includes

// Project includes
#include "custom_elements/hydromechanical_elements/small_displacement_U_wP_element.hpp"
#include "includes/constitutive_law.h"
#include "solid_mechanics_application_variables.h"

namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************
SmallDisplacementUwPElement::SmallDisplacementUwPElement()
    : SmallDisplacementElement()
{
   //DO NOT CALL IT: only needed for Register and Serialization!!!
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

SmallDisplacementUwPElement::SmallDisplacementUwPElement(IndexType NewId, GeometryType::Pointer pGeometry)
    : SmallDisplacementElement(NewId, pGeometry)
{
   //DO NOT ADD DOFS HERE!!!
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

SmallDisplacementUwPElement::SmallDisplacementUwPElement(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : SmallDisplacementElement(NewId, pGeometry, pProperties)
{
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

SmallDisplacementUwPElement::SmallDisplacementUwPElement(SmallDisplacementUwPElement const &rOther)
    : SmallDisplacementElement(rOther)
{
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

SmallDisplacementUwPElement &SmallDisplacementUwPElement::operator=(SmallDisplacementUwPElement const &rOther)
{
   SmallDisplacementElement::operator=(rOther);

   return *this;
}

//*********************************OPERATIONS*****************************************
//************************************************************************************

Element::Pointer SmallDisplacementUwPElement::Create(IndexType NewId, NodesArrayType const &rThisNodes, PropertiesType::Pointer pProperties) const
{
   return Kratos::make_intrusive<SmallDisplacementUwPElement>(NewId, GetGeometry().Create(rThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************

Element::Pointer SmallDisplacementUwPElement::Clone(IndexType NewId, NodesArrayType const &rThisNodes) const
{

   SmallDisplacementUwPElement NewElement(NewId, GetGeometry().Create(rThisNodes), pGetProperties());

   //-----------//

   NewElement.mThisIntegrationMethod = mThisIntegrationMethod;

   if (NewElement.mConstitutiveLawVector.size() != mConstitutiveLawVector.size())
   {
      NewElement.mConstitutiveLawVector.resize(mConstitutiveLawVector.size());

      if (NewElement.mConstitutiveLawVector.size() != NewElement.GetGeometry().IntegrationPointsNumber())
         KRATOS_THROW_ERROR(std::logic_error, "constitutive law not has the correct size ", NewElement.mConstitutiveLawVector.size())
   }

   for (unsigned int i = 0; i < mConstitutiveLawVector.size(); i++)
   {
      NewElement.mConstitutiveLawVector[i] = mConstitutiveLawVector[i]->Clone();
   }

   NewElement.SetData(this->GetData());
   NewElement.SetFlags(this->GetFlags());

   return Kratos::make_intrusive<SmallDisplacementUwPElement>(NewElement);
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

SmallDisplacementUwPElement::~SmallDisplacementUwPElement()
{
}

//************* GETTING METHODS
//************************************************************************************
//************************************************************************************

void SmallDisplacementUwPElement::GetDofList(DofsVectorType &rElementalDofList, const ProcessInfo &rCurrentProcessInfo) const
{
   rElementalDofList.resize(0);

   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();

   for (unsigned int i = 0; i < GetGeometry().size(); i++)
   {
      rElementalDofList.push_back(GetGeometry()[i].pGetDof(DISPLACEMENT_X));
      rElementalDofList.push_back(GetGeometry()[i].pGetDof(DISPLACEMENT_Y));

      if (dimension == 3)
         rElementalDofList.push_back(GetGeometry()[i].pGetDof(DISPLACEMENT_Z));


      rElementalDofList.push_back(GetGeometry()[i].pGetDof(WATER_PRESSURE));
   }
}

//************************************************************************************
//************************************************************************************

void SmallDisplacementUwPElement::EquationIdVector(EquationIdVectorType &rResult, const ProcessInfo &rCurrentProcessInfo) const
{
   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   unsigned int element_size = number_of_nodes * ( dimension + 1);
   unsigned int dofs_per_node =  dimension + 1;

   if (rResult.size() != element_size)
      rResult.resize(element_size, false);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      int index = i * dofs_per_node;
      rResult[index] = GetGeometry()[i].GetDof(DISPLACEMENT_X).EquationId();
      rResult[index + 1] = GetGeometry()[i].GetDof(DISPLACEMENT_Y).EquationId();

      if (dimension == 3)
      {
         rResult[index + 2] = GetGeometry()[i].GetDof(DISPLACEMENT_Z).EquationId();
         rResult[index + 3] = GetGeometry()[i].GetDof(WATER_PRESSURE).EquationId();
      }
      else
      {
         rResult[index + 2] = GetGeometry()[i].GetDof(WATER_PRESSURE).EquationId();
      }
   }
}

//*********************************DISPLACEMENT***************************************
//************************************************************************************

void SmallDisplacementUwPElement::GetValuesVector(Vector &rValues, int Step) const
{
   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   unsigned int element_size = number_of_nodes * ( dimension + 1);
   unsigned int dofs_per_node =  dimension + 1;

   if (rValues.size() != element_size)
      rValues.resize(element_size, false);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      unsigned int index = i * dofs_per_node;
      rValues[index] = GetGeometry()[i].GetSolutionStepValue(DISPLACEMENT_X, Step);
      rValues[index + 1] = GetGeometry()[i].GetSolutionStepValue(DISPLACEMENT_Y, Step);

      if (dimension == 3)
      {
         rValues[index + 2] = GetGeometry()[i].GetSolutionStepValue(DISPLACEMENT_Z, Step);
         rValues[index + 3] = GetGeometry()[i].GetSolutionStepValue(WATER_PRESSURE, Step);
      }
      else
      {
         rValues[index + 2] = GetGeometry()[i].GetSolutionStepValue(WATER_PRESSURE, Step);
      }
   }
}

//************************************VELOCITY****************************************
//************************************************************************************

void SmallDisplacementUwPElement::GetFirstDerivativesVector(Vector &rValues, int Step) const
{
   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   unsigned int element_size = number_of_nodes * ( dimension + 1);
   unsigned int dofs_per_node =  dimension + 1;

   if (rValues.size() != element_size)
      rValues.resize(element_size, false);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      unsigned int index = i * dofs_per_node;
      rValues[index] = GetGeometry()[i].GetSolutionStepValue(VELOCITY_X, Step);
      rValues[index + 1] = GetGeometry()[i].GetSolutionStepValue(VELOCITY_Y, Step);

      if (dimension == 3)
      {
         rValues[index + 2] = GetGeometry()[i].GetSolutionStepValue(VELOCITY_Z, Step);
         rValues[index + 3] = GetGeometry()[i].GetSolutionStepValue(WATER_PRESSURE_VELOCITY, Step);
      }
      else
      {
         rValues[index + 2] = GetGeometry()[i].GetSolutionStepValue(WATER_PRESSURE_VELOCITY, Step);
      }
   }
}

//*********************************ACCELERATION***************************************
//************************************************************************************

void SmallDisplacementUwPElement::GetSecondDerivativesVector(Vector &rValues, int Step) const
{
   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   unsigned int element_size = number_of_nodes * ( dimension + 1);
   unsigned int dofs_per_node =  dimension + 1;

   if (rValues.size() != element_size)
      rValues.resize(element_size, false);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      unsigned int index = i * dofs_per_node;

      rValues[index] = GetGeometry()[i].GetSolutionStepValue(ACCELERATION_X, Step);
      rValues[index + 1] = GetGeometry()[i].GetSolutionStepValue(ACCELERATION_Y, Step);

      if (dimension == 3)
      {
         rValues[index + 2] = GetGeometry()[i].GetSolutionStepValue(ACCELERATION_Z, Step);
         rValues[index + 3] = GetGeometry()[i].GetSolutionStepValue(WATER_PRESSURE_ACCELERATION, Step);
      }
      else
      {
         rValues[index + 2] = GetGeometry()[i].GetSolutionStepValue(WATER_PRESSURE_ACCELERATION, Step);
      }
   }
}

//************************************************************************************
//************************************************************************************

int SmallDisplacementUwPElement::Check(const ProcessInfo &rCurrentProcessInfo) const
{
   KRATOS_TRY

   // Perform base element checks
   int ErrorCode = 0;
   ErrorCode = SmallDisplacementElement::Check(rCurrentProcessInfo);

   return ErrorCode;

   KRATOS_CATCH("")
}
// *********************** Calculate Matrix On Integration Points *************************
// ****************************************************************************************
void SmallDisplacementUwPElement::CalculateOnIntegrationPoints(const Variable<Matrix> &rVariable, std::vector<Matrix> &rValues, const ProcessInfo &rCurrentProcessInfo)
{
   if (rValues.size() != mConstitutiveLawVector.size())
   {
      rValues.resize(mConstitutiveLawVector.size());
   }
   if (rVariable == CAUCHY_STRESS_TENSOR)
   {
      //create and initialize element variables:
      ElementDataType Variables;
      this->InitializeElementData(Variables, rCurrentProcessInfo);

      Variables.StressVector = ZeroVector(6); // I WANT TO GET THE THIRD COMPONENT
      //create constitutive law parameters:
      ConstitutiveLaw::Parameters Values(GetGeometry(), GetProperties(), rCurrentProcessInfo);

      //set constitutive law flags:
      Flags &ConstitutiveLawOptions = Values.GetOptions();

      ConstitutiveLawOptions.Set(ConstitutiveLaw::COMPUTE_STRESS);

      //reading integration points
      for (unsigned int PointNumber = 0; PointNumber < mConstitutiveLawVector.size(); PointNumber++)
      {
         //compute element kinematics B, F, DN_DX ...
         this->CalculateKinematics(Variables, PointNumber);

         //set general variables to constitutivelaw parameters
         this->SetElementData(Variables, Values, PointNumber);


         mConstitutiveLawVector[PointNumber]->CalculateMaterialResponseCauchy(Values);

         if ( rValues.size() != mConstitutiveLawVector.size())
            rValues.resize(mConstitutiveLawVector.size());

         if ((rValues[PointNumber].size1() != 3) ||
             (rValues[PointNumber].size2() != 3))
            rValues[PointNumber].resize(3, 3, false);

         rValues[PointNumber] = MathUtils<double>::StressVectorToTensor(Variables.StressVector);
      }
   }
   else if (rVariable == TOTAL_CAUCHY_STRESS)
   {
      // only for one gauss point element
      this->CalculateOnIntegrationPoints(CAUCHY_STRESS_TENSOR, rValues, rCurrentProcessInfo);

      const unsigned int &integration_points_number = mConstitutiveLawVector.size();
      const unsigned int number_of_nodes = GetGeometry().size();

      unsigned int ThisDimension = rValues[0].size1();
      Matrix Eye = ZeroMatrix(ThisDimension, ThisDimension);

      for (unsigned int i = 0; i < ThisDimension; ++i)
         Eye(i, i) = 1.0;

      //create and initialize element variables:
      ElementDataType Variables;
      this->InitializeElementData(Variables, rCurrentProcessInfo);


      for (unsigned int PointNumber = 0; PointNumber < integration_points_number; PointNumber++)
      {
         //compute element kinematics B, F, DN_DX ...
         this->CalculateKinematics(Variables, PointNumber);

         double GaussPointPressure = 0.0;
         for (unsigned int i = 0; i < number_of_nodes; ++i)
            GaussPointPressure += GetGeometry()[i].GetSolutionStepValue(WATER_PRESSURE) * Variables.N[i];
         rValues[PointNumber] = rValues[PointNumber] + GaussPointPressure * Eye;
      }
   }
   else
   {
      SmallDisplacementElement::CalculateOnIntegrationPoints(rVariable, rValues, rCurrentProcessInfo);
   }
}


//************************************************************************************
//************************************************************************************

void SmallDisplacementUwPElement::InitializeElementData(ElementDataType &rVariables, const ProcessInfo &rCurrentProcessInfo) const
{
   SmallDisplacementElement::InitializeElementData(rVariables, rCurrentProcessInfo);
   // KC permeability constitutive equation checks
   if (!GetProperties().Has(KOZENY_CARMAN))
     KRATOS_ERROR << "KOZENY_CARMAN not defined in properties " << std::endl;

   if (!GetProperties().Has(INITIAL_POROSITY))
     KRATOS_ERROR << "INITIAL_POROSITY not defined in properties " << std::endl;
}

//************************************************************************************
//************************************************************************************

void SmallDisplacementUwPElement::InitializeSystemMatrices(MatrixType &rLeftHandSideMatrix,
                                                            VectorType &rRightHandSideVector,
                                                            Flags &rCalculationFlags) const

{

   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();

   //resizing as needed the LHS
   unsigned int MatSize = number_of_nodes * ( dimension + 1);

   if (rCalculationFlags.Is(SolidElement::COMPUTE_LHS_MATRIX)) //calculation of the matrix is required
   {
      if (rLeftHandSideMatrix.size1() != MatSize)
         rLeftHandSideMatrix.resize(MatSize, MatSize, false);

      noalias(rLeftHandSideMatrix) = ZeroMatrix(MatSize, MatSize); //resetting LHS
   }

   //resizing as needed the RHS
   if (rCalculationFlags.Is(SolidElement::COMPUTE_RHS_VECTOR)) //calculation of the matrix is required
   {
      if (rRightHandSideVector.size() != MatSize)
         rRightHandSideVector.resize(MatSize, false);

      rRightHandSideVector = ZeroVector(MatSize); //resetting RHS
   }
}

//************* COMPUTING  METHODS
//************************************************************************************
//************************************************************************************

//************************************************************************************
//************************************************************************************

void SmallDisplacementUwPElement::CalculateAndAddLHS(LocalSystemComponents &rLocalSystem, ElementDataType &rVariables, double &rIntegrationWeight)
{

   KRATOS_TRY

   rVariables.detF0 *= rVariables.detF;
   double DeterminantF = rVariables.detF;
   rVariables.detF = 1.0;

   //contributions of the stiffness matrix calculated on the reference configuration
   MatrixType &rLeftHandSideMatrix = rLocalSystem.GetLeftHandSideMatrix();

   this->CalculateAndAddKuum(rLeftHandSideMatrix, rVariables, rIntegrationWeight);

   this->CalculateAndAddKUwP(rLeftHandSideMatrix, rVariables, rIntegrationWeight);

   this->CalculateAndAddKwPU( rLeftHandSideMatrix, rVariables, rIntegrationWeight);

   this->CalculateAndAddKwPwP( rLeftHandSideMatrix, rVariables, rIntegrationWeight);

   rVariables.detF = DeterminantF;
   rVariables.detF0 /= rVariables.detF;

   //std::cout << " id " << this->Id() << std::endl;
   //std::cout << rLeftHandSideMatrix << std::endl;
   KRATOS_CATCH("")
}

//************************************************************************************
//         Matrix due to the the water pressure contribution to the internal forces
void SmallDisplacementUwPElement::CalculateAndAddKUwP(MatrixType &rLeftHandSide, ElementDataType &rVariables, double &rIntegrationWeight)
{
   KRATOS_TRY

   const unsigned int number_of_nodes = GetGeometry().PointsNumber();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();

   unsigned int dofs_per_node = dimension + 1;

   Matrix Q = ZeroMatrix(number_of_nodes * dimension, number_of_nodes);
   SizeType voigtSize = this->GetVoigtSize();

   Matrix m = ZeroMatrix(voigtSize, 1);
   for (unsigned int i = 0; i < dimension; i++)
      m(i, 0) = 1.0;
   Matrix partial = prod(trans(rVariables.B), m);

   for (unsigned int i = 0; i < dimension * number_of_nodes; i++)
   {
      for (unsigned int j = 0; j < number_of_nodes; j++)
      {
         Q(i, j) = partial(i, 0) * rVariables.N[j] * rIntegrationWeight;
      }
   }

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      for (unsigned int iDim = 0; iDim < dimension; iDim++)
      {
         for (unsigned int j = 0; j < number_of_nodes; j++)
         {
            rLeftHandSide(i * dofs_per_node + iDim, (j + 1) * dofs_per_node - 1) -= Q(i * dimension + iDim, j);
         }
      }
   }

   KRATOS_CATCH("")
}

//************************************************************************************
//         Matrix due to the permeability in the mass balance equation
void SmallDisplacementUwPElement::CalculateAndAddKwPwP(MatrixType &rLeftHandSide, ElementDataType &rVariables, double &rIntegrationWeight)
{
   KRATOS_TRY

   const unsigned int number_of_nodes = GetGeometry().PointsNumber();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();

   unsigned int dofs_per_node = dimension + 1;


   Matrix K(3,3);
   noalias (K) = identity_matrix<double>(3);
   const double & rPermeability = GetProperties().GetValue(PERMEABILITY_WATER);
   K *= rPermeability;

   const Matrix & rDN_DX = rVariables.DN_DX;

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      for (unsigned int j = 0; j < number_of_nodes; j++)
      {
         for (unsigned int iDim = 0; iDim < dimension; iDim++)
         {
            for (unsigned int jDim = 0; jDim < dimension; jDim++)
            {
               rLeftHandSide( i*dofs_per_node + dimension, j*dofs_per_node + dimension) += rDN_DX(i, iDim)* K(iDim,jDim)*rDN_DX(j, jDim) * rIntegrationWeight;
            }
         }
      }
   }

   KRATOS_CATCH("")
}

//************************************************************************************
//         Matrix due to the soil compressibility in the mass balance equation
void SmallDisplacementUwPElement::CalculateAndAddKwPU(MatrixType &rLeftHandSide, ElementDataType &rVariables, double &rIntegrationWeight)
{
   KRATOS_TRY

   const unsigned int number_of_nodes = GetGeometry().PointsNumber();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   const double &time_step = rVariables.GetProcessInfo()[DELTA_TIME];
   unsigned int dofs_per_node = dimension + 1;

   Matrix Q = ZeroMatrix(number_of_nodes, dimension * number_of_nodes);
   SizeType voigtSize = this->GetVoigtSize();

   Matrix m = ZeroMatrix(1, voigtSize);
   for (unsigned int i = 0; i < dimension; i++)
      m(0, i) = 1.0;
   Matrix partial = prod(m, rVariables.B);
   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      for (unsigned int j = 0; j < dimension * number_of_nodes; j++)
      {
         Q(i, j) = rVariables.N[i] * partial(0, j) * rIntegrationWeight / time_step;
      }
   }

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      for (unsigned int j = 0; j < number_of_nodes; j++)
      {
         for (unsigned int jDim = 0; jDim < dimension; jDim++)
         {
            rLeftHandSide( (i) * dofs_per_node + dimension, j * dofs_per_node + jDim) += Q(i, j * dimension + jDim);
         }
      }
   }



   KRATOS_CATCH("")
}


//************************************************************************************
//         Material stiffness matrix
void SmallDisplacementUwPElement::CalculateAndAddKuum(MatrixType &rLeftHandSide, ElementDataType &rVariables, double &rIntegrationWeight) const
{
   KRATOS_TRY

   const unsigned int number_of_nodes = GetGeometry().PointsNumber();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();

   unsigned int dofs_per_node = dimension + 1;

   Matrix SmallMatrix = ZeroMatrix(number_of_nodes * dimension);

   noalias(SmallMatrix) = prod(trans(rVariables.B), rIntegrationWeight * Matrix(prod(rVariables.ConstitutiveMatrix, rVariables.B)));

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      for (unsigned int j = 0; j < number_of_nodes; j++)
      {
         for (unsigned int k = 0; k < dimension; k++)
         {
            for (unsigned int l = 0; l < dimension; l++)
            {
               rLeftHandSide(i * dofs_per_node + k, j * dofs_per_node + l) += SmallMatrix(i * dimension + k, j * dimension + l);
            }
         }
      }
   }

   KRATOS_CATCH("")
}


// ***********************************************************************************
// ***********************************************************************************
void SmallDisplacementUwPElement::CalculateAndAddRHS(LocalSystemComponents &rLocalSystem, ElementDataType &rVariables, Vector &rVolumeForce, double &rIntegrationWeight)
{
   KRATOS_TRY

   rVariables.detF0 *= rVariables.detF;
   double DeterminantF = rVariables.detF;
   rVariables.detF = 1.0;

   //contribution of the internal and external forces
   VectorType &rRightHandSideVector = rLocalSystem.GetRightHandSideVector();

   this->CalculateAndAddExternalForces(rRightHandSideVector, rVariables, rVolumeForce, rIntegrationWeight);

   this->CalculateAndAddInternalForces(rRightHandSideVector, rVariables, rIntegrationWeight);


   // Calculate mass balance equation
   CalculateAndAddMassBalanceEquation(rRightHandSideVector, rVariables, rIntegrationWeight);

   rVariables.detF = DeterminantF;
   rVariables.detF0 /= rVariables.detF;
   //std::cout << " id " << this->Id() << std::endl;
   //std::cout << rRightHandSideVector << std::endl;

   KRATOS_CATCH("")
}

// **********************************************************************************
//    mass balance equation of the mixture (aka: Darcy's Law )
void SmallDisplacementUwPElement::CalculateAndAddMassBalanceEquation(VectorType &rRightHandSideVector, ElementDataType &rVariables, double &rIntegrationWeight)
{
   KRATOS_TRY

   // a convective term may go here. not coded yet.
   const unsigned int number_of_nodes = GetGeometry().PointsNumber();
   unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   unsigned int dofs_per_node = dimension + 1;
   const double &time_step = rVariables.GetProcessInfo()[DELTA_TIME];
   Vector GradP = ZeroVector(dimension);
   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      const double & rWaterPressure = GetGeometry()[i].FastGetSolutionStepValue(WATER_PRESSURE);
      for (unsigned int iDim = 0; iDim < dimension; iDim++)
      {
         GradP(iDim) += rVariables.DN_DX(i, iDim) * rWaterPressure;
      }
   }

   Matrix K(3,3);
   noalias (K) = identity_matrix<double>(3);
   const double & rPermeability = GetProperties().GetValue(PERMEABILITY_WATER);
   K *= rPermeability;

   const Matrix & rDN_DX = rVariables.DN_DX;

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      for (unsigned int iDim = 0; iDim < dimension; iDim++)
      {
         for (unsigned int jDim = 0; jDim < dimension; jDim++)
         {

            rRightHandSideVector( i * dofs_per_node + dimension) -= rDN_DX(i, iDim)*K(iDim,jDim)*GradP(jDim) * rIntegrationWeight;

         }

      }
   }

   // second contribution Soil Compressibility
   const VectorType &rN = rVariables.N;
   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      for (unsigned int j = 0; j < number_of_nodes; j++)
      {

         // DeltaDisplacement
         const array_1d<double, 3> & rCurrentDisplacement = GetGeometry()[j].FastGetSolutionStepValue(DISPLACEMENT);
         const array_1d<double, 3> & rPreviousDisplacement = GetGeometry()[j].FastGetSolutionStepValue(DISPLACEMENT, 1);
         array_1d<double, 3> DeltaDisplacement = rCurrentDisplacement - rPreviousDisplacement;

         for (unsigned int p = 0; p < dimension; ++p)
         {
            // Solid Skeleton volumetric deformation
            rRightHandSideVector(i*dofs_per_node + dimension) -= rN(i) * DeltaDisplacement[p] * rDN_DX(j, p) * rIntegrationWeight / time_step;
         }
      }
   }

   KRATOS_CATCH("")
}


// **************************************************************************
// Calculate and Add volumetric loads
void SmallDisplacementUwPElement::CalculateAndAddExternalForces(VectorType &rRightHandSideVector, ElementDataType &rVariables,
                                                                 Vector &rVolumeForce, double &rIntegrationWeight) const
{
   KRATOS_TRY

   const unsigned int number_of_nodes = GetGeometry().PointsNumber();
   unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   unsigned int dofs_per_node = dimension + 1;

   rVolumeForce *= rVariables.detF0;
   double density_mixture0 = GetProperties().GetValue(DENSITY);
   if (density_mixture0 > 0)
   {
      rVolumeForce /= density_mixture0;
   }
   else
   {
      return;
   }

   double density_water = GetProperties().GetValue(DENSITY_WATER);
   double porosity0 = GetProperties().GetValue(INITIAL_POROSITY);

   double porosity = porosity0; // no se actualiza
   double density_solid = (density_mixture0 - porosity0 * density_water) / (1.0 - porosity0);
   double density_mixture = (1.0 - porosity) * density_solid + porosity * density_water;

   const VectorType &rN = rVariables.N;
   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      for (unsigned int iDim = 0; iDim < dimension; iDim++)
      {
         rRightHandSideVector(i * dofs_per_node + iDim) += rIntegrationWeight * density_mixture * rN(i) * rVolumeForce(iDim);
      }
   }

   rVolumeForce /= rVariables.detF0;
   rVolumeForce *= density_mixture0;
   return;

   KRATOS_CATCH("")
}

// **********************************************************************************
//         CalculateAndAddInternalForces
void SmallDisplacementUwPElement::CalculateAndAddInternalForces(VectorType &rRightHandSideVector,
                                                                 ElementDataType &rVariables,
                                                                 double &rIntegrationWeight) const
{
   KRATOS_TRY

   const unsigned int number_of_nodes = GetGeometry().PointsNumber();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   unsigned int dofs_per_node =  dimension + 1;

   double WaterPressure = 0.0;
   for (unsigned int i = 0; i < number_of_nodes; i++)
      WaterPressure += GetGeometry()[i].FastGetSolutionStepValue(WATER_PRESSURE) * rVariables.N[i];

   Vector StressVector = rVariables.StressVector;
   for (unsigned int i = 0; i < dimension; i++)
      StressVector(i) -= WaterPressure;

   VectorType InternalForces = rIntegrationWeight * prod(trans(rVariables.B), StressVector);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      for (unsigned int j = 0; j < dimension; j++)
      {
         rRightHandSideVector(i * dofs_per_node + j) -= InternalForces(i * dimension + j);
      }
   }

   KRATOS_CATCH("")
}

// *********************************************************************************
//         Calculate the Mass matrix
void SmallDisplacementUwPElement::CalculateMassMatrix(MatrixType &rMassMatrix, const ProcessInfo &rCurrentProcessInfo)
{
   KRATOS_TRY

   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   const unsigned int number_of_nodes = GetGeometry().size();
   unsigned int MatSize = number_of_nodes * ( dimension + 1);

   if (rMassMatrix.size1() != MatSize)
      rMassMatrix.resize(MatSize, MatSize, false);

   rMassMatrix = ZeroMatrix(MatSize, MatSize);


   KRATOS_CATCH("")
}

// *********************************************************************************
//         Calculate the Damping matrix
void SmallDisplacementUwPElement::CalculateDampingMatrix(MatrixType &rDampingMatrix, const ProcessInfo &rCurrentProcessInfo)
{
   KRATOS_TRY

   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dofs_per_node =  dimension + 1;
   unsigned int MatSize = number_of_nodes * ( dimension + 1);

   if (rDampingMatrix.size1() != MatSize)
      rDampingMatrix.resize(MatSize, MatSize, false);

   rDampingMatrix = ZeroMatrix(MatSize, MatSize);

   return;
   //reading integration points
   IntegrationMethod CurrentIntegrationMethod = mThisIntegrationMethod; //GeometryData::IntegrationMethod::GI_GAUSS_2; //GeometryData::IntegrationMethod::GI_GAUSS_1;

   const GeometryType::IntegrationPointsArrayType &integration_points = GetGeometry().IntegrationPoints(CurrentIntegrationMethod);

   ElementDataType Variables;
   this->InitializeElementData(Variables, rCurrentProcessInfo);


   for (unsigned int PointNumber = 0; PointNumber < integration_points.size(); PointNumber++)
   {
      //compute element kinematics
      this->CalculateKinematics(Variables, PointNumber);


      //getting informations for integration
      double IntegrationWeight = integration_points[PointNumber].Weight() * Variables.detJ;

      IntegrationWeight = this->CalculateIntegrationWeight(Variables, IntegrationWeight); // multiplies by thickness


      // Q Matrix //

      Matrix Q = ZeroMatrix(number_of_nodes, dimension * number_of_nodes);
      SizeType voigtSize = this->GetVoigtSize();

      Matrix m = ZeroMatrix(1, voigtSize);
      for (unsigned int i = 0; i < dimension; i++)
         m(0, i) = 1.0;
      Matrix partial = prod(m, Variables.B);
      for (unsigned int i = 0; i < number_of_nodes; i++)
      {
         for (unsigned int j = 0; j < dimension * number_of_nodes; j++)
         {
            Q(i, j) = Variables.N[i] * partial(0, j) * IntegrationWeight;
         }
      }

      for (unsigned int i = 0; i < number_of_nodes; i++)
      {
         for (unsigned int j = 0; j < number_of_nodes; j++)
         {
            for (unsigned int jDim = 0; jDim < dimension; jDim++)
            {
               rDampingMatrix( (i) * dofs_per_node + dimension, j * dofs_per_node + jDim) += Q(i, j * dimension + jDim);
            }
         }
      }


   } // end point

   std::cout << " this->Id() " << this->Id() << std::endl;
   std::cout << rDampingMatrix << std::endl;
   KRATOS_CATCH("")
}


void SmallDisplacementUwPElement::save(Serializer &rSerializer) const
{
   KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, SmallDisplacementElement)
}

void SmallDisplacementUwPElement::load(Serializer &rSerializer)
{
   KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, SmallDisplacementElement)
}

} // namespace Kratos
