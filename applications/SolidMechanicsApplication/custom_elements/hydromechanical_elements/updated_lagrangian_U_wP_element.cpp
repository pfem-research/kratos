//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:           LMonforte $
//   Maintained by:       $Maintainer:                 LM $
//   Date:                $Date:                July 2015 $
//
//

//   Implementation of the Fluid Saturated porous media in a U-Pw Formulation
//     ( There is a ScalingConstant to multiply the mass balance equation for a number because i read it somewhere)
//

// System includes

// External includes

// Project includes
#include "custom_elements/hydromechanical_elements/updated_lagrangian_U_wP_element.hpp"
#include "includes/constitutive_law.h"
#include "solid_mechanics_application_variables.h"
#include "custom_utilities/water_pressure_utilities.hpp"

namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************
UpdatedLagrangianUwPElement::UpdatedLagrangianUwPElement()
    : UpdatedLagrangianElement()
{
   //DO NOT CALL IT: only needed for Register and Serialization!!!
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

UpdatedLagrangianUwPElement::UpdatedLagrangianUwPElement(IndexType NewId, GeometryType::Pointer pGeometry)
    : UpdatedLagrangianElement(NewId, pGeometry)
{
   //DO NOT ADD DOFS HERE!!!
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

UpdatedLagrangianUwPElement::UpdatedLagrangianUwPElement(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : UpdatedLagrangianElement(NewId, pGeometry, pProperties)
{
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

UpdatedLagrangianUwPElement::UpdatedLagrangianUwPElement(UpdatedLagrangianUwPElement const &rOther)
    : UpdatedLagrangianElement(rOther){
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

UpdatedLagrangianUwPElement &UpdatedLagrangianUwPElement::operator=(UpdatedLagrangianUwPElement const &rOther)
{
   UpdatedLagrangianElement::operator=(rOther);
   return *this;
}

//*********************************OPERATIONS*****************************************
//************************************************************************************

Element::Pointer UpdatedLagrangianUwPElement::Create(IndexType NewId, NodesArrayType const &rThisNodes, PropertiesType::Pointer pProperties) const
{
   return Kratos::make_intrusive<UpdatedLagrangianUwPElement>(NewId, GetGeometry().Create(rThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************

Element::Pointer UpdatedLagrangianUwPElement::Clone(IndexType NewId, NodesArrayType const &rThisNodes) const
{

   UpdatedLagrangianUwPElement NewElement(NewId, GetGeometry().Create(rThisNodes), pGetProperties());

   //-----------//

   NewElement.mThisIntegrationMethod = mThisIntegrationMethod;

   if (NewElement.mConstitutiveLawVector.size() != mConstitutiveLawVector.size())
   {
      NewElement.mConstitutiveLawVector.resize(mConstitutiveLawVector.size());

      if (NewElement.mConstitutiveLawVector.size() != NewElement.GetGeometry().IntegrationPointsNumber())
         KRATOS_THROW_ERROR(std::logic_error, "constitutive law not has the correct size ", NewElement.mConstitutiveLawVector.size())
   }

   for (unsigned int i = 0; i < mConstitutiveLawVector.size(); i++)
   {
      NewElement.mConstitutiveLawVector[i] = mConstitutiveLawVector[i]->Clone();
   }

   //-----------//

   if (NewElement.mDeformationGradientF0.size() != mDeformationGradientF0.size())
      NewElement.mDeformationGradientF0.resize(mDeformationGradientF0.size());

   for (unsigned int i = 0; i < mDeformationGradientF0.size(); i++)
   {
      NewElement.mDeformationGradientF0[i] = mDeformationGradientF0[i];
   }

   NewElement.mDeterminantF0 = mDeterminantF0;

   NewElement.SetData(this->GetData());
   NewElement.SetFlags(this->GetFlags());

   return Kratos::make_intrusive<UpdatedLagrangianUwPElement>(NewElement);
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

UpdatedLagrangianUwPElement::~UpdatedLagrangianUwPElement()
{
}

//************* GETTING METHODS
//************************************************************************************
//************************************************************************************

void UpdatedLagrangianUwPElement::GetDofList(DofsVectorType &rElementalDofList, const ProcessInfo &rCurrentProcessInfo) const
{
   rElementalDofList.resize(0);

   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();

   for (unsigned int i = 0; i < GetGeometry().size(); i++)
   {
      rElementalDofList.push_back(GetGeometry()[i].pGetDof(DISPLACEMENT_X));
      rElementalDofList.push_back(GetGeometry()[i].pGetDof(DISPLACEMENT_Y));

      if (dimension == 3)
         rElementalDofList.push_back(GetGeometry()[i].pGetDof(DISPLACEMENT_Z));

      rElementalDofList.push_back(GetGeometry()[i].pGetDof(WATER_PRESSURE));
   }
}

//************************************************************************************
//************************************************************************************

void UpdatedLagrangianUwPElement::EquationIdVector(EquationIdVectorType &rResult, const ProcessInfo &rCurrentProcessInfo) const
{
   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   unsigned int element_size = number_of_nodes * dimension + number_of_nodes;

   if (rResult.size() != element_size)
      rResult.resize(element_size, false);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      int index = i * dimension + i;
      rResult[index] = GetGeometry()[i].GetDof(DISPLACEMENT_X).EquationId();
      rResult[index + 1] = GetGeometry()[i].GetDof(DISPLACEMENT_Y).EquationId();

      if (dimension == 3)
      {
         rResult[index + 2] = GetGeometry()[i].GetDof(DISPLACEMENT_Z).EquationId();
         rResult[index + 3] = GetGeometry()[i].GetDof(WATER_PRESSURE).EquationId();
      }
      else
      {
         rResult[index + 2] = GetGeometry()[i].GetDof(WATER_PRESSURE).EquationId();
      }
   }
}

//*********************************DISPLACEMENT***************************************
//************************************************************************************

void UpdatedLagrangianUwPElement::GetValuesVector(Vector &rValues, int Step) const
{
   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   unsigned int element_size = number_of_nodes * dimension + number_of_nodes;

   if (rValues.size() != element_size)
      rValues.resize(element_size, false);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      unsigned int index = i * dimension + i;
      rValues[index] = GetGeometry()[i].GetSolutionStepValue(DISPLACEMENT_X, Step);
      rValues[index + 1] = GetGeometry()[i].GetSolutionStepValue(DISPLACEMENT_Y, Step);

      if (dimension == 3)
      {
         rValues[index + 2] = GetGeometry()[i].GetSolutionStepValue(DISPLACEMENT_Z, Step);
         rValues[index + 3] = GetGeometry()[i].GetSolutionStepValue(WATER_PRESSURE, Step);
      }
      else
      {
         rValues[index + 2] = GetGeometry()[i].GetSolutionStepValue(WATER_PRESSURE, Step);
      }
   }
}

//************************************VELOCITY****************************************
//************************************************************************************

void UpdatedLagrangianUwPElement::GetFirstDerivativesVector(Vector &rValues, int Step) const
{
   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   unsigned int element_size = number_of_nodes * dimension + number_of_nodes;

   if (rValues.size() != element_size)
      rValues.resize(element_size, false);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      unsigned int index = i * dimension + i;
      rValues[index] = GetGeometry()[i].GetSolutionStepValue(VELOCITY_X, Step);
      rValues[index + 1] = GetGeometry()[i].GetSolutionStepValue(VELOCITY_Y, Step);
      if (dimension == 3)
      {
         rValues[index + 2] = GetGeometry()[i].GetSolutionStepValue(VELOCITY_Z, Step);
         rValues[index + 3] = 0;
      }
      else
      {
         rValues[index + 2] = 0;
      }
   }
}

//*********************************ACCELERATION***************************************
//************************************************************************************

void UpdatedLagrangianUwPElement::GetSecondDerivativesVector(Vector &rValues, int Step) const
{
   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   unsigned int element_size = number_of_nodes * dimension + number_of_nodes;

   if (rValues.size() != element_size)
      rValues.resize(element_size, false);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      unsigned int index = i * dimension + i;
      rValues[index] = GetGeometry()[i].GetSolutionStepValue(ACCELERATION_X, Step);
      rValues[index + 1] = GetGeometry()[i].GetSolutionStepValue(ACCELERATION_Y, Step);

      if (dimension == 3)
      {
         rValues[index + 2] = GetGeometry()[i].GetSolutionStepValue(ACCELERATION_Z, Step);
         rValues[index + 3] = 0;
      }
      else
      {
         rValues[index + 2] = 0;
      }
   }
}

// **************************************************************************
// **************************************************************************

UpdatedLagrangianUwPElement::SizeType UpdatedLagrangianUwPElement::GetNodeDofsSize() const
{
  return (GetGeometry().WorkingSpaceDimension() + 1); //usual size for U-P elements
}
//************************************************************************************
//************************************************************************************

int UpdatedLagrangianUwPElement::Check(const ProcessInfo &rCurrentProcessInfo) const
{
   KRATOS_TRY

   // Perform base element checks
   int ErrorCode = 0;
   ErrorCode = LargeDisplacementElement::Check(rCurrentProcessInfo);

   // Check compatibility with the constitutive law
   ConstitutiveLaw::Features LawFeatures;
   this->GetProperties().GetValue(CONSTITUTIVE_LAW)->GetLawFeatures(LawFeatures);

   // Check that the constitutive law has the correct dimension
   if (LawFeatures.mOptions.Is(ConstitutiveLaw::U_P_LAW))
   {
      KRATOS_ERROR << "wrong constitutive law used. u-p law not compatible :: element id = " << this->Id() << std::endl;
   }

   return ErrorCode;

   KRATOS_CATCH("")
}

// *********************** Calculate double On Integration Points *************************
// ****************************************************************************************
void UpdatedLagrangianUwPElement::CalculateOnIntegrationPoints(const Variable<double> &rVariable, std::vector<double> &rValues, const ProcessInfo &rCurrentProcessInfo)
{

   if (rVariable == POROSITY)
   {

      const unsigned int &integration_points_number = mConstitutiveLawVector.size();
      const double InitialPorosity = GetProperties()[INITIAL_POROSITY];

      if (rValues.size() != mConstitutiveLawVector.size())
         rValues.resize(mConstitutiveLawVector.size());

      std::vector<double> DetF0;
      this->CalculateOnIntegrationPoints(DETERMINANT_F, DetF0, rCurrentProcessInfo);

      if (rValues.size() != integration_points_number)
         rValues.resize(integration_points_number);

      for (unsigned int PointNumber = 0; PointNumber < integration_points_number; PointNumber++)
      {
         rValues[PointNumber] = 1.0 - (1.0 - InitialPorosity) / DetF0[PointNumber];
      }
   }
   else if (rVariable == VOID_RATIO)
   {

      this->CalculateOnIntegrationPoints(POROSITY, rValues, rCurrentProcessInfo);

      const unsigned int &integration_points_number = mConstitutiveLawVector.size();

      for (unsigned int PointNumber = 0; PointNumber < integration_points_number; PointNumber++)
      {
         rValues[PointNumber] = rValues[PointNumber] / (1.0 - rValues[PointNumber]);
      }
   }
   else if (rVariable == DENSITY)
   {
      const unsigned int &integration_points_number = mConstitutiveLawVector.size();

      if (rValues.size() != mConstitutiveLawVector.size())
         rValues.resize(mConstitutiveLawVector.size());

      double DensityRigid, DensityWater, DensityMixtureInitial, DensityMixtureFinal, PorosityInitial, Porosity, Jacobian;

      DensityMixtureInitial = GetProperties()[DENSITY];
      DensityWater = GetProperties()[DENSITY_WATER];
      PorosityInitial = GetProperties()[INITIAL_POROSITY];

      DensityRigid = (DensityMixtureInitial - PorosityInitial * DensityWater) / (1.0 - PorosityInitial);

      Jacobian = mDeterminantF0[0];
      Porosity = 1.0 - (1.0 - PorosityInitial) / Jacobian;

      DensityMixtureFinal = (1.0 - Porosity) * DensityRigid + Porosity * DensityWater;

      for (unsigned int PointNumber = 0; PointNumber < integration_points_number; PointNumber++)
      {
         rValues[PointNumber] = DensityMixtureFinal;
      }
   }

   else
   {
      LargeDisplacementElement::CalculateOnIntegrationPoints(rVariable, rValues, rCurrentProcessInfo);
   }
}


// *********************** Calculate Matrix On Integration Points *************************
// ****************************************************************************************
void UpdatedLagrangianUwPElement::CalculateOnIntegrationPoints(const Variable<Matrix> &rVariable, std::vector<Matrix> &rValues, const ProcessInfo &rCurrentProcessInfo)
{
   if (rValues.size() != mConstitutiveLawVector.size())
   {
      rValues.resize(mConstitutiveLawVector.size());
   }
   if (rVariable == CAUCHY_STRESS_TENSOR)
   {
      //create and initialize element variables:
      ElementDataType Variables;
      this->InitializeElementData(Variables, rCurrentProcessInfo);

      Variables.StressVector = ZeroVector(6); // I WANT TO GET THE THIRD COMPONENT
      //create constitutive law parameters:
      ConstitutiveLaw::Parameters Values(GetGeometry(), GetProperties(), rCurrentProcessInfo);

      //set constitutive law flags:
      Flags &ConstitutiveLawOptions = Values.GetOptions();

      ConstitutiveLawOptions.Set(ConstitutiveLaw::COMPUTE_STRESS);

      //reading integration points
      for (unsigned int PointNumber = 0; PointNumber < mConstitutiveLawVector.size(); PointNumber++)
      {
         //compute element kinematics B, F, DN_DX ...
         this->CalculateKinematics(Variables, PointNumber);

         //set general variables to constitutivelaw parameters
         this->SetElementData(Variables, Values, PointNumber);


         mConstitutiveLawVector[PointNumber]->CalculateMaterialResponseCauchy(Values);

         if ( rValues.size() != mConstitutiveLawVector.size())
            rValues.resize(mConstitutiveLawVector.size());

         if ((rValues[PointNumber].size1() != 3) || (rValues[PointNumber].size2() != 3))
            rValues[PointNumber].resize(3, 3, false);

         rValues[PointNumber] = MathUtils<double>::StressVectorToTensor(Variables.StressVector);
      }
   }
   else if (rVariable == TOTAL_CAUCHY_STRESS)
   {
      // only for one gauss point element
      this->CalculateOnIntegrationPoints(CAUCHY_STRESS_TENSOR, rValues, rCurrentProcessInfo);

      const unsigned int &integration_points_number = mConstitutiveLawVector.size();
      const unsigned int number_of_nodes = GetGeometry().size();

      unsigned int ThisDimension = rValues[0].size1();
      Matrix Eye = ZeroMatrix(ThisDimension, ThisDimension);

      for (unsigned int i = 0; i < ThisDimension; ++i)
         Eye(i, i) = 1.0;

      //create and initialize element variables:
      ElementDataType Variables;
      this->InitializeElementData(Variables, rCurrentProcessInfo);


      for (unsigned int PointNumber = 0; PointNumber < integration_points_number; PointNumber++)
      {
         //compute element kinematics B, F, DN_DX ...
         this->CalculateKinematics(Variables, PointNumber);

         double GaussPointPressure = 0.0;
         for (unsigned int i = 0; i < number_of_nodes; ++i)
            GaussPointPressure += GetGeometry()[i].GetSolutionStepValue(WATER_PRESSURE) * Variables.N[i];
         rValues[PointNumber] = rValues[PointNumber] + GaussPointPressure * Eye;
      }
   } else if ( rVariable == PERMEABILITY_TENSOR) {
      for (unsigned int PointNumber = 0; PointNumber < mConstitutiveLawVector.size(); PointNumber++) {

         rValues[PointNumber] = ZeroMatrix(3,3);
         WaterPressureUtilities WaterUtility;
         WaterUtility.GetPermeabilityTensorAndReshape( GetProperties(), mDeformationGradientF0[PointNumber], rValues[PointNumber], GetProperties()[INITIAL_POROSITY], 3, MathUtils<double>::Det(mDeformationGradientF0[PointNumber]), 0.0);

      }
   }
   else
   {
      LargeDisplacementElement::CalculateOnIntegrationPoints(rVariable, rValues, rCurrentProcessInfo);
   }
}

//************************************************************************************
//************************************************************************************

void UpdatedLagrangianUwPElement::InitializeElementData(ElementDataType &rVariables, const ProcessInfo &rCurrentProcessInfo) const
{
   UpdatedLagrangianElement::InitializeElementData(rVariables, rCurrentProcessInfo);

   // KC permeability constitutive equation checks
   if (!GetProperties().Has(KOZENY_CARMAN))
     KRATOS_ERROR << "KOZENY_CARMAN not defined in properties " << std::endl;

   if (!GetProperties().Has(INITIAL_POROSITY))
     KRATOS_ERROR << "INITIAL_POROSITY not defined in properties " << std::endl;

}

//************************************************************************************
//************************************************************************************

void UpdatedLagrangianUwPElement::InitializeSystemMatrices(MatrixType &rLeftHandSideMatrix,
                                                           VectorType &rRightHandSideVector,
                                                           Flags &rCalculationFlags) const

{

   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();

   //resizing as needed the LHS
   unsigned int MatSize = number_of_nodes * dimension + number_of_nodes;

   if (rCalculationFlags.Is(LargeDisplacementElement::COMPUTE_LHS_MATRIX)) //calculation of the matrix is required
   {
      if (rLeftHandSideMatrix.size1() != MatSize)
         rLeftHandSideMatrix.resize(MatSize, MatSize, false);

      noalias(rLeftHandSideMatrix) = ZeroMatrix(MatSize, MatSize); //resetting LHS
   }

   //resizing as needed the RHS
   if (rCalculationFlags.Is(LargeDisplacementElement::COMPUTE_RHS_VECTOR)) //calculation of the matrix is required
   {
      if (rRightHandSideVector.size() != MatSize)
         rRightHandSideVector.resize(MatSize, false);

      rRightHandSideVector = ZeroVector(MatSize); //resetting RHS
   }
}

//************* COMPUTING  METHODS
//************************************************************************************
//************************************************************************************

//************************************************************************************
//************************************************************************************

void UpdatedLagrangianUwPElement::CalculateAndAddLHS(LocalSystemComponents &rLocalSystem, ElementDataType &rVariables, double &rIntegrationWeight)
{

   KRATOS_TRY

   // define some variables
   const unsigned int number_of_nodes = GetGeometry().PointsNumber();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   const double &time_step = rVariables.GetProcessInfo()[DELTA_TIME];
   rVariables.detF0 *= rVariables.detF;
   double DeterminantF = rVariables.detF;
   rVariables.detF = 1.0;

   //contributions of the stiffness matrix calculated on the reference configuration
   MatrixType &rLeftHandSideMatrix = rLocalSystem.GetLeftHandSideMatrix();

   // ComputeBaseClass LHS
   LocalSystemComponents BaseClassLocalSystem;
   Matrix BaseClassLeftHandSideMatrix = ZeroMatrix(dimension * number_of_nodes, dimension * number_of_nodes);
   BaseClassLocalSystem.SetLeftHandSideMatrix(BaseClassLeftHandSideMatrix);

   UpdatedLagrangianElement::CalculateAndAddLHS(BaseClassLocalSystem, rVariables, rIntegrationWeight);

   // Reshape the BaseClass LHS and Add the Hydro Part
   WaterPressureUtilities WaterUtility;
   Matrix TotalF = prod(rVariables.F, rVariables.F0);
   int number_of_variables = dimension + 1;
   Vector VolumeForce;
   VolumeForce = this->CalculateVolumeForce(VolumeForce, rVariables);

   // 1. Create (make pointers) variables
   WaterPressureUtilities::HydroMechanicalVariables HMVariables(GetGeometry(), GetProperties());

   HMVariables.SetBMatrix(rVariables.B);
   HMVariables.SetShapeFunctionsDerivatives(rVariables.DN_DX);
   HMVariables.SetDeformationGradient(TotalF);
   HMVariables.SetVolumeForce(VolumeForce);
   HMVariables.SetShapeFunctions(rVariables.N);

   HMVariables.DeltaTime = time_step;
   HMVariables.detF0 = rVariables.detF0;
   HMVariables.detF  = DeterminantF;
   //HMVariables.CurrentRadius
   //HMVariables.ConstrainedModulus
   HMVariables.number_of_variables = number_of_variables;

   rLeftHandSideMatrix = WaterUtility.CalculateAndAddHydromechanicalLHS(HMVariables, rLeftHandSideMatrix, BaseClassLeftHandSideMatrix, rIntegrationWeight);

   rVariables.detF = DeterminantF;
   rVariables.detF0 /= rVariables.detF;

   KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void UpdatedLagrangianUwPElement::CalculateAndAddRHS(LocalSystemComponents &rLocalSystem, ElementDataType &rVariables, Vector &rVolumeForce, double &rIntegrationWeight)
{
   KRATOS_TRY

   // define some variables
   const unsigned int number_of_nodes = GetGeometry().PointsNumber();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   const double &time_step = rVariables.GetProcessInfo()[DELTA_TIME];
   rVariables.detF0 *= rVariables.detF;
   double DeterminantF = rVariables.detF;
   rVariables.detF = 1.0;

   //contribution of the internal and external forces
   VectorType &rRightHandSideVector = rLocalSystem.GetRightHandSideVector();

   // Compute Base Class RHS
   LocalSystemComponents BaseClassLocalSystem;
   Vector BaseClassRightHandSideVector = ZeroVector(dimension * number_of_nodes);
   BaseClassLocalSystem.SetRightHandSideVector(BaseClassRightHandSideVector);
   Vector VolumeForce = rVolumeForce;
   VolumeForce *= 0.0;
   UpdatedLagrangianElement::CalculateAndAddRHS(BaseClassLocalSystem, rVariables, VolumeForce, rIntegrationWeight);

   // Reshape the BaseClass RHS and Add the Hydro Part
   WaterPressureUtilities WaterUtility;
   Matrix TotalF = prod(rVariables.F, rVariables.F0);

   int number_of_variables = dimension + 1;

   // 1. Create (make pointers) variables
   WaterPressureUtilities::HydroMechanicalVariables HMVariables(GetGeometry(), GetProperties());

   HMVariables.SetBMatrix(rVariables.B);
   HMVariables.SetShapeFunctionsDerivatives(rVariables.DN_DX);
   HMVariables.SetDeformationGradient(TotalF);
   HMVariables.SetVolumeForce(rVolumeForce);
   HMVariables.SetShapeFunctions(rVariables.N);

   HMVariables.DeltaTime = time_step;
   HMVariables.detF0 = rVariables.detF0;
   HMVariables.detF  = DeterminantF;
   //HMVariables.CurrentRadius
   //HMVariables.ConstrainedModulus
   HMVariables.number_of_variables = number_of_variables;

   rRightHandSideVector = WaterUtility.CalculateAndAddHydromechanicalRHS(HMVariables, rRightHandSideVector, BaseClassRightHandSideVector, rIntegrationWeight);

   rVariables.detF = DeterminantF;
   rVariables.detF0 /= rVariables.detF;

   KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void UpdatedLagrangianUwPElement::CalculateAndAddExternalForces(VectorType &rRightHandSideVector,
                                                 ElementDataType &rVariables,
                                                 Vector &rVolumeForce,
                                                 double &rIntegrationWeight) const

{
  KRATOS_TRY

  const SizeType number_of_nodes = GetGeometry().PointsNumber();
  const SizeType dimension = GetGeometry().WorkingSpaceDimension();
  const SizeType node_dofs = GetGeometry().WorkingSpaceDimension(); // THEN I RESHAPE THIS!

  for (SizeType i = 0; i < number_of_nodes; i++)
  {
    int index = node_dofs * i;
    for (SizeType j = 0; j < dimension; j++)
    {
      rRightHandSideVector[index + j] += rIntegrationWeight * rVariables.N[i] * rVolumeForce[j];
    }
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void UpdatedLagrangianUwPElement::save(Serializer &rSerializer) const
{
   KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, UpdatedLagrangianElement)
}

void UpdatedLagrangianUwPElement::load(Serializer &rSerializer)
{
   KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, UpdatedLagrangianElement)
}

} // namespace Kratos
