//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:           LMonforte $
//   Maintained by:       $Maintainer:                 LM $
//   Date:                $Date:            December 2021 $
//
//

//   Axisymmetric version of the U-Pw formulation

// System includes

// External includes

// Project includes
#include "custom_elements/hydromechanical_elements/axisymmetric_unsaturated_U_wP_element.hpp"
#include "custom_utilities/axisym_unsaturated_water_pressure_utilities.hpp"
#include "solid_mechanics_application_variables.h"

namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************
// Aquest a l'altre no hi és....
AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::AxisymmetricUpdatedLagrangianUnsaturatedUwPElement()
    : AxisymmetricUpdatedLagrangianElement()
{
   //DO NOT CALL IT: only needed for Register and Serialization!!!
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::AxisymmetricUpdatedLagrangianUnsaturatedUwPElement(IndexType NewId, GeometryType::Pointer pGeometry)
    : AxisymmetricUpdatedLagrangianElement(NewId, pGeometry)
{
   //DO NOT ADD DOFS HERE!!!
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::AxisymmetricUpdatedLagrangianUnsaturatedUwPElement(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : AxisymmetricUpdatedLagrangianElement(NewId, pGeometry, pProperties)
{
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::AxisymmetricUpdatedLagrangianUnsaturatedUwPElement(AxisymmetricUpdatedLagrangianUnsaturatedUwPElement const &rOther)
    : AxisymmetricUpdatedLagrangianElement(rOther)
{
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUnsaturatedUwPElement &AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::operator=(AxisymmetricUpdatedLagrangianUnsaturatedUwPElement const &rOther)
{
   AxisymmetricUpdatedLagrangianElement::operator=(rOther);
   return *this;
}

//*********************************OPERATIONS*****************************************
//************************************************************************************

Element::Pointer AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::Create(IndexType NewId, NodesArrayType const &rThisNodes, PropertiesType::Pointer pProperties) const
{
   return Kratos::make_intrusive<AxisymmetricUpdatedLagrangianUnsaturatedUwPElement>(NewId, GetGeometry().Create(rThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************

Element::Pointer AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::Clone(IndexType NewId, NodesArrayType const &rThisNodes) const
{

   AxisymmetricUpdatedLagrangianUnsaturatedUwPElement NewElement(NewId, GetGeometry().Create(rThisNodes), pGetProperties());

   //-----------//

   NewElement.mThisIntegrationMethod = mThisIntegrationMethod;

   if (NewElement.mConstitutiveLawVector.size() != mConstitutiveLawVector.size())
   {
      NewElement.mConstitutiveLawVector.resize(mConstitutiveLawVector.size());

      if (NewElement.mConstitutiveLawVector.size() != NewElement.GetGeometry().IntegrationPointsNumber())
         KRATOS_THROW_ERROR(std::logic_error, "constitutive law not has the correct size ", NewElement.mConstitutiveLawVector.size());
   }

   for (unsigned int i = 0; i < mConstitutiveLawVector.size(); i++)
   {
      NewElement.mConstitutiveLawVector[i] = mConstitutiveLawVector[i]->Clone();
   }

   //-----------//

   if (NewElement.mDeformationGradientF0.size() != mDeformationGradientF0.size())
      NewElement.mDeformationGradientF0.resize(mDeformationGradientF0.size());

   for (unsigned int i = 0; i < mDeformationGradientF0.size(); i++)
   {
      NewElement.mDeformationGradientF0[i] = mDeformationGradientF0[i];
   }

   NewElement.mDeterminantF0 = mDeterminantF0;

   NewElement.SetData(this->GetData());
   NewElement.SetFlags(this->GetFlags());

   return Kratos::make_intrusive<AxisymmetricUpdatedLagrangianUnsaturatedUwPElement>(NewElement);
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::~AxisymmetricUpdatedLagrangianUnsaturatedUwPElement()
{
}

//************* GETTING METHODS
//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::GetDofList(DofsVectorType &rElementalDofList, const ProcessInfo &rCurrentProcessInfo) const
{
   rElementalDofList.resize(0);

   for (unsigned int i = 0; i < GetGeometry().size(); i++)
   {
      rElementalDofList.push_back(GetGeometry()[i].pGetDof(DISPLACEMENT_X));
      rElementalDofList.push_back(GetGeometry()[i].pGetDof(DISPLACEMENT_Y));
      rElementalDofList.push_back(GetGeometry()[i].pGetDof(WATER_PRESSURE));
   }
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::EquationIdVector(EquationIdVectorType &rResult, const ProcessInfo &rCurrentProcessInfo) const
{
   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   unsigned int element_size = number_of_nodes * dimension + number_of_nodes;

   if (rResult.size() != element_size)
      rResult.resize(element_size, false);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      int index = i * dimension + i;
      rResult[index] = GetGeometry()[i].GetDof(DISPLACEMENT_X).EquationId();
      rResult[index + 1] = GetGeometry()[i].GetDof(DISPLACEMENT_Y).EquationId();
      rResult[index + 2] = GetGeometry()[i].GetDof(WATER_PRESSURE).EquationId();
   }
}

//*********************************DISPLACEMENT***************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::GetValuesVector(Vector &rValues, int Step) const
{
   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   unsigned int element_size = number_of_nodes * dimension + number_of_nodes;

   if (rValues.size() != element_size)
      rValues.resize(element_size, false);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      unsigned int index = i * dimension + i;
      rValues[index] = GetGeometry()[i].GetSolutionStepValue(DISPLACEMENT_X, Step);
      rValues[index + 1] = GetGeometry()[i].GetSolutionStepValue(DISPLACEMENT_Y, Step);
      rValues[index + 2] = GetGeometry()[i].GetSolutionStepValue(WATER_PRESSURE, Step);
   }
}

//************************************VELOCITY****************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::GetFirstDerivativesVector(Vector &rValues, int Step) const
{
   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   unsigned int element_size = number_of_nodes * dimension + number_of_nodes;

   if (rValues.size() != element_size)
      rValues.resize(element_size, false);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      unsigned int index = i * dimension + i;
      rValues[index] = GetGeometry()[i].GetSolutionStepValue(VELOCITY_X, Step);
      rValues[index + 1] = GetGeometry()[i].GetSolutionStepValue(VELOCITY_Y, Step);
      rValues[index + 2] = 0;
   }
}

//*********************************ACCELERATION***************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::GetSecondDerivativesVector(Vector &rValues, int Step) const
{
   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   unsigned int element_size = number_of_nodes * dimension + number_of_nodes;

   if (rValues.size() != element_size)
      rValues.resize(element_size, false);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      unsigned int index = i * dimension + i;
      rValues[index] = GetGeometry()[i].GetSolutionStepValue(ACCELERATION_X, Step);
      rValues[index + 1] = GetGeometry()[i].GetSolutionStepValue(ACCELERATION_Y, Step);
      rValues[index + 2] = 0;
   }
}

// **************************************************************************
// **************************************************************************

AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::SizeType AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::GetDofsSize() const
{
   KRATOS_TRY

   const SizeType dimension = GetGeometry().WorkingSpaceDimension();
   const SizeType number_of_nodes = GetGeometry().PointsNumber();

   return number_of_nodes * dimension + number_of_nodes; //usual size for U-P elements

   KRATOS_CATCH("")
}

// *********************** Calculate double On Integration Points *************************
// ****************************************************************************************
void AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::CalculateOnIntegrationPoints(const Variable<double> &rVariable, std::vector<double> &rValues, const ProcessInfo &rCurrentProcessInfo)
{

   if (rVariable == POROSITY)
   {

      const unsigned int &integration_points_number = mConstitutiveLawVector.size();
      const double InitialPorosity = GetProperties()[INITIAL_POROSITY];

      if (rValues.size() != mConstitutiveLawVector.size())
         rValues.resize(mConstitutiveLawVector.size());

      std::vector<double> DetF0;
      this->CalculateOnIntegrationPoints(DETERMINANT_F, DetF0, rCurrentProcessInfo);

      if (rValues.size() != integration_points_number)
         rValues.resize(integration_points_number);

      for (unsigned int PointNumber = 0; PointNumber < integration_points_number; PointNumber++)
      {
         rValues[PointNumber] = 1.0 - (1.0 - InitialPorosity) / DetF0[PointNumber];
      }
   }
   else if (rVariable == VOID_RATIO)
   {

      this->CalculateOnIntegrationPoints(POROSITY, rValues, rCurrentProcessInfo);

      const unsigned int &integration_points_number = mConstitutiveLawVector.size();

      for (unsigned int PointNumber = 0; PointNumber < integration_points_number; PointNumber++)
      {
         rValues[PointNumber] = rValues[PointNumber] / (1.0 - rValues[PointNumber]);
      }
   } else if ( rVariable == WATER_CONTENT) {
      if (rValues.size() != mConstitutiveLawVector.size())
         rValues.resize(mConstitutiveLawVector.size());
   
      std::vector<double> VoidRatio;
      std::vector<double> DegreeOfSaturation;
      this->CalculateOnIntegrationPoints(DEGREE_OF_SATURATION, DegreeOfSaturation, rCurrentProcessInfo);
      this->CalculateOnIntegrationPoints(VOID_RATIO, VoidRatio, rCurrentProcessInfo);

      for (unsigned int PointNumber = 0; PointNumber < mConstitutiveLawVector.size(); PointNumber++)
	 rValues[PointNumber] = DegreeOfSaturation[PointNumber]*VoidRatio[PointNumber];
   } else if (rVariable == DENSITY)
   {
      const unsigned int &integration_points_number = mConstitutiveLawVector.size();

      if (rValues.size() != mConstitutiveLawVector.size())
         rValues.resize(mConstitutiveLawVector.size());

      double DensityRigid, DensityWater, DensityMixtureInitial, DensityMixtureFinal, PorosityInitial, Porosity, Jacobian;

      DensityMixtureInitial = GetProperties()[DENSITY];
      DensityWater = GetProperties()[DENSITY_WATER];
      PorosityInitial = GetProperties()[INITIAL_POROSITY];

      DensityRigid = (DensityMixtureInitial - PorosityInitial * DensityWater) / (1.0 - PorosityInitial);

      Jacobian = mDeterminantF0[0];
      Porosity = 1.0 - (1.0 - PorosityInitial) / Jacobian;

      DensityMixtureFinal = (1.0 - Porosity) * DensityRigid + Porosity * DensityWater;

      for (unsigned int PointNumber = 0; PointNumber < integration_points_number; PointNumber++)
      {
         rValues[PointNumber] = DensityMixtureFinal;
      }
   }
   else if ( rVariable == DEGREE_OF_SATURATION) {
      const unsigned int &integration_points_number = mConstitutiveLawVector.size();
      const unsigned int number_of_nodes = GetGeometry().size();
      if (rValues.size() != mConstitutiveLawVector.size())
         rValues.resize(mConstitutiveLawVector.size());

      //create and initialize element variables:
      ElementDataType Variables;
      this->InitializeElementData(Variables, rCurrentProcessInfo);

      for (unsigned int PointNumber = 0; PointNumber < integration_points_number; PointNumber++)
      {
         //compute element kinematics B, F, DN_DX ...
         this->CalculateKinematics(Variables, PointNumber);

         double GaussPointPressure = 0.0;
         for (unsigned int i = 0; i < number_of_nodes; ++i)
            GaussPointPressure += GetGeometry()[i].GetSolutionStepValue(WATER_PRESSURE) * Variables.N[i];

         // Compute the degree of saturation
         AxisymUnsaturatedWaterPressureUtilities WaterUtility;
         double DegreeOfSaturation = WaterUtility.EvaluateRetentionCurve( GetProperties(), GaussPointPressure, DegreeOfSaturation);
         rValues[PointNumber] = DegreeOfSaturation;

      }
   }
   else if ( rVariable == DEGREE_OF_SATURATION_EFF) {
      const unsigned int &integration_points_number = mConstitutiveLawVector.size();
      const unsigned int number_of_nodes = GetGeometry().size();
      if (rValues.size() != mConstitutiveLawVector.size())
         rValues.resize(mConstitutiveLawVector.size());

      //create and initialize element variables:
      ElementDataType Variables;
      this->InitializeElementData(Variables, rCurrentProcessInfo);

      for (unsigned int PointNumber = 0; PointNumber < integration_points_number; PointNumber++)
      {
         //compute element kinematics B, F, DN_DX ...
         this->CalculateKinematics(Variables, PointNumber);

         double GaussPointPressure = 0.0;
         for (unsigned int i = 0; i < number_of_nodes; ++i)
            GaussPointPressure += GetGeometry()[i].GetSolutionStepValue(WATER_PRESSURE) * Variables.N[i];

         // Compute the degree of saturation
         AxisymUnsaturatedWaterPressureUtilities WaterUtility;
         double DegreeOfSaturation = WaterUtility.EvaluateEffectiveRetentionCurve( GetProperties(), GaussPointPressure, DegreeOfSaturation);
         rValues[PointNumber] = DegreeOfSaturation;

      }
   }
   else
   {
      AxisymmetricUpdatedLagrangianElement::CalculateOnIntegrationPoints(rVariable, rValues, rCurrentProcessInfo);
   }
}

// *********************** Calculate Matrix On Integration Points *************************
// ****************************************************************************************
void AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::CalculateOnIntegrationPoints(const Variable<Matrix> &rVariable, std::vector<Matrix> &rValues, const ProcessInfo &rCurrentProcessInfo)
{
  if (rVariable == TOTAL_CAUCHY_STRESS)
  {
    if (rValues.size() != mConstitutiveLawVector.size())
    {
      rValues.resize(mConstitutiveLawVector.size());
    }
    // only for one gauss point element
    this->CalculateOnIntegrationPoints(CAUCHY_STRESS_TENSOR, rValues, rCurrentProcessInfo);

    const unsigned int &integration_points_number = mConstitutiveLawVector.size();
    const unsigned int number_of_nodes = GetGeometry().size();

    unsigned int ThisDimension = rValues[0].size1();
    Matrix Eye = ZeroMatrix(ThisDimension, ThisDimension);
    AxisymUnsaturatedWaterPressureUtilities WaterUtility;

    for (unsigned int i = 0; i < ThisDimension; ++i)
      Eye(i, i) = 1.0;


    for (unsigned int PointNumber = 0; PointNumber < integration_points_number; PointNumber++)
    {

         double GaussPointPressure = 0.0;
         for (unsigned int i = 0; i < number_of_nodes; ++i)
            GaussPointPressure += GetGeometry()[i].GetSolutionStepValue(WATER_PRESSURE);
         GaussPointPressure /= double(number_of_nodes);

         double DegreeOfSaturation = WaterUtility.EvaluateEffectiveRetentionCurve( GetProperties(), GaussPointPressure, DegreeOfSaturation);
       rValues[PointNumber] = rValues[PointNumber] + DegreeOfSaturation*GaussPointPressure * Eye;
    }
   } else if ( rVariable == PERMEABILITY_TENSOR) {
      
      const unsigned int &integration_points_number = mConstitutiveLawVector.size();
      const unsigned int number_of_nodes = GetGeometry().size();

      ElementDataType Variables;
      this->InitializeElementData(Variables, rCurrentProcessInfo);
      
      AxisymUnsaturatedWaterPressureUtilities WaterUtility;

      for (unsigned int PointNumber = 0; PointNumber < integration_points_number; PointNumber++) {

         this->CalculateKinematics( Variables, PointNumber);

         double GaussPointPressure = 0.0;
         for (unsigned int i = 0; i < number_of_nodes; ++i)
            GaussPointPressure += GetGeometry()[i].GetSolutionStepValue(WATER_PRESSURE) * Variables.N[i];

         rValues[PointNumber] = ZeroMatrix(3,3);
         WaterUtility.GetPermeabilityTensorAndReshape( GetProperties(), mDeformationGradientF0[PointNumber], rValues[PointNumber], GetProperties()[INITIAL_POROSITY], 3, MathUtils<double>::Det(mDeformationGradientF0[PointNumber]), GaussPointPressure);
      }
   }
  else
  {
    AxisymmetricUpdatedLagrangianElement::CalculateOnIntegrationPoints(rVariable, rValues, rCurrentProcessInfo);
  }
}

// ******************** INITIALIZE GENERAL VARIABLES ****************************
// ******************************************************************************
void AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::InitializeElementData(ElementDataType &rVariables, const ProcessInfo &rCurrentProcessInfo) const
{

   AxisymmetricUpdatedLagrangianElement::InitializeElementData(rVariables, rCurrentProcessInfo);

   // KC permeability constitutive equation checks
   if (!GetProperties().Has(KOZENY_CARMAN))
     KRATOS_ERROR << "KOZENY_CARMAN not defined in properties " << std::endl;

   if (!GetProperties().Has(INITIAL_POROSITY))
     KRATOS_ERROR << "INITIAL_POROSITY not defined in properties " << std::endl;
}

void AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::InitializeSystemMatrices(MatrixType &rLeftHandSideMatrix,
                                                                 VectorType &rRightHandSideVector,
                                                                 Flags &rCalculationFlags) const

{

   const unsigned int number_of_nodes = GetGeometry().size();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();

   //resizing as needed the LHS
   unsigned int MatSize = number_of_nodes * dimension + number_of_nodes;

   if (rCalculationFlags.Is(LargeDisplacementElement::COMPUTE_LHS_MATRIX)) //calculation of the matrix is required
   {
      if (rLeftHandSideMatrix.size1() != MatSize)
         rLeftHandSideMatrix.resize(MatSize, MatSize, false);

      noalias(rLeftHandSideMatrix) = ZeroMatrix(MatSize, MatSize); //resetting LHS
   }

   //resizing as needed the RHS
   if (rCalculationFlags.Is(LargeDisplacementElement::COMPUTE_RHS_VECTOR)) //calculation of the matrix is required
   {
      if (rRightHandSideVector.size() != MatSize)
         rRightHandSideVector.resize(MatSize, false);

      rRightHandSideVector = ZeroVector(MatSize); //resetting RHS
   }
}

//************* COMPUTING  METHODS
//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::CalculatePerturbation(LocalSystemComponents &rLocalSystem, ElementDataType &rVariables, double &rIntegrationWeight, Matrix & rMatrix)
{
   KRATOS_TRY
   
   const unsigned int number_of_nodes = GetGeometry().PointsNumber();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   const double &time_step = rVariables.GetProcessInfo()[DELTA_TIME];
   rVariables.detF0 *= rVariables.detF;
   double DeterminantF = rVariables.detF;
   rVariables.detF = 1.0;
   
   double IntegrationWeight = rIntegrationWeight * 2.0 * Globals::Pi * rVariables.CurrentRadius / GetProperties()[THICKNESS];

   //contributions of the stiffness matrix calculated on the reference configuration
   MatrixType &rLeftHandSideMatrix = rLocalSystem.GetLeftHandSideMatrix();

   const unsigned int size1 = rVariables.ConstitutiveMatrix.size1();
   const unsigned int size2 = rVariables.ConstitutiveMatrix.size2();
   Vector ConstitutiveVector(size1);
   Matrix OriginalMatrix(size1, size2);
   if ( size1 == size2-1) {
      OriginalMatrix = rVariables.ConstitutiveMatrix;
      rVariables.ConstitutiveMatrix.resize(size1, size1, false);
      for (unsigned int i = 0; i < size1; i++)
         for (unsigned int j = 0; j < size1; j++)
            rVariables.ConstitutiveMatrix(i,j) = OriginalMatrix(i,j);
      /*for (unsigned int i = 0; i < size1; i++)
         ConstitutiveVector(i) = OriginalMatrix(i, size2-1);*/
      noalias( ConstitutiveVector ) = ZeroVector(size1);
   } else {
      noalias( ConstitutiveVector ) = ZeroVector(size1);
   }

   ProcessInfo SomeProcessInfo;
   std::vector<double> Mmodulus;
   SolidElement::CalculateOnIntegrationPoints(M_MODULUS, Mmodulus, SomeProcessInfo);
   double ConstrainedModulus = Mmodulus[0];
   if (ConstrainedModulus < 1e-5)
   {
      const double &YoungModulus = GetProperties()[YOUNG_MODULUS];
      const double &nu = GetProperties()[POISSON_RATIO];
      ConstrainedModulus = YoungModulus * (1.0 - nu) / (1.0 + nu) / (1.0 - 2.0 * nu);
   }
   
   // Reshape the BaseClass LHS and Add the Hydro Part
   AxisymUnsaturatedWaterPressureUtilities WaterUtility;
   Matrix TotalF = prod(rVariables.F, rVariables.F0);
   int number_of_variables = dimension + 1;
   Vector VolumeForce;
   VolumeForce = this->CalculateVolumeForce(VolumeForce, rVariables);

   // 1. Create (make pointers) variables
   AxisymUnsaturatedWaterPressureUtilities::HydroMechanicalVariables HMVariables(GetGeometry(), GetProperties());

   HMVariables.SetBMatrix(rVariables.B);
   HMVariables.SetShapeFunctionsDerivatives(rVariables.DN_DX);
   HMVariables.SetDeformationGradient(TotalF);
   HMVariables.SetVolumeForce(VolumeForce);
   HMVariables.SetShapeFunctions(rVariables.N);
   HMVariables.SetConstitutiveVector( ConstitutiveVector);

   HMVariables.DeltaTime = time_step;
   HMVariables.detF0 = rVariables.detF0;
   HMVariables.detF  = DeterminantF;
   HMVariables.CurrentRadius = rVariables.CurrentRadius;
   HMVariables.ConstrainedModulus = ConstrainedModulus;
   HMVariables.number_of_variables = number_of_variables;

   Matrix LeftHandSideMatrix = ZeroMatrix(number_of_nodes*(dimension+1), number_of_nodes*(dimension+1) );
   Matrix BaseClassLeftHandSideMatrix = ZeroMatrix(number_of_nodes*dimension, number_of_nodes*dimension);

   LeftHandSideMatrix = WaterUtility.CalculateAndAddHydromechanicalLHS(HMVariables, rLeftHandSideMatrix, BaseClassLeftHandSideMatrix, IntegrationWeight);
   LeftHandSideMatrix = WaterUtility.CalculateAndAddStabilizationLHS(HMVariables, rLeftHandSideMatrix, IntegrationWeight);

  Matrix Perturbed = LeftHandSideMatrix;
 
  ProcessInfo rCurrentProcessInfo;

  DofsVectorType ElementalDofList;
  this->GetDofList(ElementalDofList, rCurrentProcessInfo);

  unsigned int size = ElementalDofList.size();
  for (unsigned int i = 0; i < size; i++)
  {
    //Set perturbation in "i" dof component
    double &value = ElementalDofList[i]->GetSolutionStepValue();
    double original = value;

    double deltavalue = 1e-10;
    value = original + deltavalue;
   

    VectorType BaseClassRightHandSideVector = ZeroVector(number_of_nodes*dimension);
    VectorType  RightHandSideVectorI = ZeroVector(number_of_nodes*(dimension+1));
    VectorType  RightHandSideVectorII = ZeroVector(number_of_nodes*(dimension+1));
    
    CalculateRadius( rVariables.CurrentRadius, rVariables.ReferenceRadius, rVariables.N);
    HMVariables.CurrentRadius = rVariables.CurrentRadius;
   
    RightHandSideVectorI = WaterUtility.CalculateAndAddHydromechanicalRHS(HMVariables, RightHandSideVectorI, BaseClassRightHandSideVector, IntegrationWeight);
   RightHandSideVectorI  = WaterUtility.CalculateAndAddStabilization(HMVariables, RightHandSideVectorI, IntegrationWeight);
   
   value = original-deltavalue;
    
   CalculateRadius( rVariables.CurrentRadius, rVariables.ReferenceRadius, rVariables.N);
    HMVariables.CurrentRadius = rVariables.CurrentRadius;
    
   RightHandSideVectorII = WaterUtility.CalculateAndAddHydromechanicalRHS(HMVariables, RightHandSideVectorII, BaseClassRightHandSideVector, IntegrationWeight);
   RightHandSideVectorII  = WaterUtility.CalculateAndAddStabilization(HMVariables, RightHandSideVectorII, IntegrationWeight);

    for (unsigned int j = 0; j < size; j++)
    {
      Perturbed(j, i) = (-1) * (RightHandSideVectorI[j] - RightHandSideVectorII[j]) / (2.0 * deltavalue);
    }

    value = original;


  }

  std::cout << " ORIGINAL LHS " << std::endl;
  WriteMatrix(LeftHandSideMatrix);
  std::cout << " PERTURBED " << std::endl;
  WriteMatrix(Perturbed);
  std::cout << " DIFERENCE " << std::endl;
  WriteMatrix(LeftHandSideMatrix-Perturbed);

  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << std::endl;
  std::cout << std::endl;
   rVariables.detF = DeterminantF;
   rVariables.detF0 /= rVariables.detF;
   rVariables.ConstitutiveMatrix = OriginalMatrix;

   KRATOS_CATCH("")
}

void AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::WriteMatrix( Matrix A)
{
   KRATOS_TRY
   for (unsigned int i = 0; i < A.size1(); i++) {
      for (unsigned int j = 0; j < A.size2(); j++)
         std::cout << A(i,j) << " , ";
      std::cout << std::endl;
   }
   KRATOS_CATCH("");
}


//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::CalculateAndAddLHS(LocalSystemComponents &rLocalSystem, ElementDataType &rVariables, double &rIntegrationWeight)
{

   KRATOS_TRY

   Matrix A;
   //CalculatePerturbation(rLocalSystem, rVariables, rIntegrationWeight, A);

   // define some variables
   const unsigned int number_of_nodes = GetGeometry().PointsNumber();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   const double &time_step = rVariables.GetProcessInfo()[DELTA_TIME];
   rVariables.detF0 *= rVariables.detF;
   double DeterminantF = rVariables.detF;
   rVariables.detF = 1.0;

   //contributions of the stiffness matrix calculated on the reference configuration
   MatrixType &rLeftHandSideMatrix = rLocalSystem.GetLeftHandSideMatrix();

   // ComputeBaseClass LHS
   LocalSystemComponents BaseClassLocalSystem;
   Matrix BaseClassLeftHandSideMatrix = ZeroMatrix(dimension * number_of_nodes, dimension * number_of_nodes);
   BaseClassLocalSystem.SetLeftHandSideMatrix(BaseClassLeftHandSideMatrix);

   const unsigned int size1 = rVariables.ConstitutiveMatrix.size1();
   const unsigned int size2 = rVariables.ConstitutiveMatrix.size2();
   Vector ConstitutiveVector(size1);
   if ( size1 == size2-1) {
      Matrix OriginalMatrix(size1, size2);
      OriginalMatrix = rVariables.ConstitutiveMatrix;
      rVariables.ConstitutiveMatrix.resize(size1, size1, false);
      for (unsigned int i = 0; i < size1; i++)
         for (unsigned int j = 0; j < size1; j++)
            rVariables.ConstitutiveMatrix(i,j) = OriginalMatrix(i,j);
      for (unsigned int i = 0; i < size1; i++)
         ConstitutiveVector(i) = OriginalMatrix(i, size2-1);
   } else {
      noalias( ConstitutiveVector ) = ZeroVector(size1);
   }

   AxisymmetricUpdatedLagrangianElement::CalculateAndAddLHS(BaseClassLocalSystem, rVariables, rIntegrationWeight);
   
   ProcessInfo SomeProcessInfo;
   std::vector<double> Mmodulus;
   SolidElement::CalculateOnIntegrationPoints(M_MODULUS, Mmodulus, SomeProcessInfo);
   double ConstrainedModulus = Mmodulus[0];
   if (ConstrainedModulus < 1e-5)
   {
      const double &YoungModulus = GetProperties()[YOUNG_MODULUS];
      const double &nu = GetProperties()[POISSON_RATIO];
      ConstrainedModulus = YoungModulus * (1.0 - nu) / (1.0 + nu) / (1.0 - 2.0 * nu);
   }

   double IntegrationWeight = rIntegrationWeight * 2.0 * Globals::Pi * rVariables.CurrentRadius / GetProperties()[THICKNESS];

   // Reshape the BaseClass LHS and Add the Hydro Part
   AxisymUnsaturatedWaterPressureUtilities WaterUtility;
   Matrix TotalF(3, 3);
   noalias(TotalF) = prod(rVariables.F, rVariables.F0);
   int number_of_variables = dimension + 1;
   Vector VolumeForce;
   VolumeForce = this->CalculateVolumeForce(VolumeForce, rVariables);

   // 1. Create (make pointers) variables
   WaterPressureUtilities::HydroMechanicalVariables HMVariables(GetGeometry(), GetProperties());

   HMVariables.SetBMatrix(rVariables.B);
   HMVariables.SetShapeFunctionsDerivatives(rVariables.DN_DX);
   HMVariables.SetDeformationGradient(TotalF);
   HMVariables.SetVolumeForce(VolumeForce);
   HMVariables.SetShapeFunctions(rVariables.N);
   HMVariables.SetConstitutiveVector( ConstitutiveVector);

   HMVariables.DeltaTime = time_step;
   HMVariables.detF0 = rVariables.detF0;
   HMVariables.detF  = DeterminantF;
   HMVariables.CurrentRadius = rVariables.CurrentRadius;
   HMVariables.ConstrainedModulus = ConstrainedModulus;
   HMVariables.number_of_variables = number_of_variables;

   rLeftHandSideMatrix = WaterUtility.CalculateAndAddHydromechanicalLHS(HMVariables, rLeftHandSideMatrix, BaseClassLeftHandSideMatrix, IntegrationWeight);

   rLeftHandSideMatrix = WaterUtility.CalculateAndAddStabilizationLHS(HMVariables, rLeftHandSideMatrix, IntegrationWeight);
   rVariables.detF = DeterminantF;
   rVariables.detF0 /= rVariables.detF;

   KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::CalculateAndAddRHS(LocalSystemComponents &rLocalSystem, ElementDataType &rVariables, Vector &rVolumeForce, double &rIntegrationWeight)
{
   KRATOS_TRY

   // define some variables
   const unsigned int number_of_nodes = GetGeometry().PointsNumber();
   const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
   const double &time_step = rVariables.GetProcessInfo()[DELTA_TIME];
   rVariables.detF0 *= rVariables.detF;
   double DeterminantF = rVariables.detF;
   rVariables.detF = 1.0;

   //contribution of the internal and external forces
   VectorType &rRightHandSideVector = rLocalSystem.GetRightHandSideVector();

   // Compute Base Class RHS
   LocalSystemComponents BaseClassLocalSystem;
   Vector BaseClassRightHandSideVector = ZeroVector(dimension * number_of_nodes);
   BaseClassLocalSystem.SetRightHandSideVector(BaseClassRightHandSideVector);
   Vector VolumeForce = rVolumeForce;
   VolumeForce *= 0.0;
   AxisymmetricUpdatedLagrangianElement::CalculateAndAddRHS(BaseClassLocalSystem, rVariables, VolumeForce, rIntegrationWeight);

   // Reshape the BaseClass RHS and Add the Hydro Part
   AxisymUnsaturatedWaterPressureUtilities WaterUtility;
   Matrix TotalF = prod(rVariables.F, rVariables.F0);
   int number_of_variables = dimension + 1;
   double IntegrationWeight = rIntegrationWeight * 2.0 * Globals::Pi * rVariables.CurrentRadius / GetProperties()[THICKNESS];
   
   ProcessInfo SomeProcessInfo;
   std::vector<double> Mmodulus;
   SolidElement::CalculateOnIntegrationPoints(M_MODULUS, Mmodulus, SomeProcessInfo);
   double ConstrainedModulus = Mmodulus[0];
   if (ConstrainedModulus < 1e-5)
   {
      const double &YoungModulus = GetProperties()[YOUNG_MODULUS];
      const double &nu = GetProperties()[POISSON_RATIO];
      ConstrainedModulus = YoungModulus * (1.0 - nu) / (1.0 + nu) / (1.0 - 2.0 * nu);
   }

   // 1. Create (make pointers) variables
   WaterPressureUtilities::HydroMechanicalVariables HMVariables(GetGeometry(), GetProperties());

   HMVariables.SetBMatrix(rVariables.B);
   HMVariables.SetShapeFunctionsDerivatives(rVariables.DN_DX);
   HMVariables.SetDeformationGradient(TotalF);
   HMVariables.SetVolumeForce(rVolumeForce);
   HMVariables.SetShapeFunctions(rVariables.N);

   HMVariables.DeltaTime = time_step;
   HMVariables.detF0 = rVariables.detF0;
   HMVariables.detF  = DeterminantF;
   HMVariables.CurrentRadius = rVariables.CurrentRadius;
   HMVariables.ConstrainedModulus = ConstrainedModulus;
   HMVariables.number_of_variables = number_of_variables;

   rRightHandSideVector = WaterUtility.CalculateAndAddHydromechanicalRHS(HMVariables, rRightHandSideVector, BaseClassRightHandSideVector, IntegrationWeight);
   rRightHandSideVector = WaterUtility.CalculateAndAddStabilization(HMVariables, rRightHandSideVector, IntegrationWeight);

   rVariables.detF = DeterminantF;
   rVariables.detF0 /= rVariables.detF;

   KRATOS_CATCH("")
}

//************************************CALCULATE TOTAL MASS****************************
//************************************************************************************

double &AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::CalculateTotalMass(double &rTotalMass, const ProcessInfo &rCurrentProcessInfo)
{
    KRATOS_TRY

    //Compute the Volume Change acumulated:
    ElementDataType Variables;
    this->InitializeElementData(Variables, rCurrentProcessInfo);

    const GeometryType::IntegrationPointsArrayType &integration_points = GetGeometry().IntegrationPoints(mThisIntegrationMethod);

    rTotalMass = 0;
    //reading integration points
    for (unsigned int PointNumber = 0; PointNumber < integration_points.size(); PointNumber++)
    {
        //compute element kinematics
        this->CalculateKinematics(Variables, PointNumber);

        //getting informations for integration
        Variables.IntegrationWeight = Variables.detJ * integration_points[PointNumber].Weight();

        //compute point volume change
        double PointVolumeChange = 0;
        PointVolumeChange = this->CalculateVolumeChange(PointVolumeChange, Variables);

        rTotalMass += PointVolumeChange * GetProperties()[DENSITY] * 2.0 * Globals::Pi * Variables.CurrentRadius * Variables.IntegrationWeight;
    }

    return rTotalMass;

    KRATOS_CATCH("")
}

int AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::Check(const ProcessInfo &rCurrentProcessInfo) const
{
   KRATOS_TRY

   // Perform base element checks
   int ErrorCode = 0;
   ErrorCode = AxisymmetricUpdatedLagrangianElement::Check(rCurrentProcessInfo);

   return ErrorCode;

   KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::CalculateAndAddExternalForces(VectorType &rRightHandSideVector,
                                                 ElementDataType &rVariables,
                                                 Vector &rVolumeForce,
                                                 double &rIntegrationWeight) const

{
  KRATOS_TRY

  const SizeType number_of_nodes = GetGeometry().PointsNumber();
  const SizeType dimension = GetGeometry().WorkingSpaceDimension();
  const SizeType node_dofs = GetGeometry().WorkingSpaceDimension(); // THEN I RESHAPE THIS!

  for (SizeType i = 0; i < number_of_nodes; i++)
  {
    int index = node_dofs * i;
    for (SizeType j = 0; j < dimension; j++)
    {
      rRightHandSideVector[index + j] += rIntegrationWeight * rVariables.N[i] * rVolumeForce[j];
    }
  }

  KRATOS_CATCH("")
}


//************************************************************************************
//************************************************************************************

void AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::save(Serializer &rSerializer) const
{
   KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, AxisymmetricUpdatedLagrangianElement)
}

void AxisymmetricUpdatedLagrangianUnsaturatedUwPElement::load(Serializer &rSerializer)
{
   KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, AxisymmetricUpdatedLagrangianElement)
}

} // namespace Kratos
