//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:         JMCarbonell $
//   Maintained by:       $Maintainer:                JMC $
//   Date:                $Date:                 May 2018 $
//
//

// System includes

// External includes

// Project includes
#include "testing/testing.h"
#include "includes/kratos_flags.h"

namespace Kratos
{

namespace Testing
{

KRATOS_TEST_CASE_IN_SUITE(CheckFlags, KratosSolidMechanicsFastSuite)
{
  Flags ThisFlag;
  ThisFlag.Set(FREE_SURFACE);
  ThisFlag.Set(RIGID.AsFalse());
  ThisFlag.Set(SOLID.AsFalse());
  ThisFlag.Set(INTERACTION.AsFalse());

  bool is_equal = false;
  if (ThisFlag.Is({FREE_SURFACE, RIGID.AsFalse(), SOLID.AsFalse(), INTERACTION.AsFalse()}))
    is_equal = true;

  std::cout << " is_equal " << is_equal << std::endl;

  KRATOS_CHECK_EQUAL(is_equal, true);

  is_equal = false;
  if (ThisFlag.IsNot({FREE_SURFACE.AsFalse(), RIGID, SOLID, FLUID}))
    is_equal = true;

  KRATOS_CHECK_EQUAL(is_equal, true);

  is_equal = false;
  if (ThisFlag.Is({FREE_SURFACE.AsFalse(), RIGID, SOLID}))
    is_equal = true;

  KRATOS_CHECK_EQUAL(is_equal, false);
}

} // namespace Testing
} // namespace Kratos.
