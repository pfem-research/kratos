//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:           MPetracca $
//   Maintained by:       $Maintainer:                    $
//   Date:                $Date:           September 2013 $
//
//

// System includes

// External includes

// Project includes
#include "add_cross_sections_to_python.h"
#include "custom_utilities/solid_shell_cross_section.hpp"

#include "custom_elements/shell_elements/shell_thick_element_3D4N.hpp"
#include "custom_elements/shell_elements/shell_thin_element_3D3N.hpp"

namespace Kratos
{
namespace Python
{

namespace py = pybind11;

void Helper_SetCrossSectionsOnIntegrationPoints_Thin(ShellThinElement3D3N &el, const pybind11::list &seclist)
{
  int n = len(seclist);
  std::vector<SolidShellCrossSection::Pointer> shell_sec_list;
  for (int i = 0; i < n; i++)
  {
    auto p = pybind11::cast<SolidShellCrossSection::Pointer>(seclist[i]);
    shell_sec_list.push_back(p);
  }
  el.SetCrossSectionsOnIntegrationPoints(shell_sec_list);
}
void Helper_SetCrossSectionsOnIntegrationPoints_Thick(ShellThickElement3D4N &el, const pybind11::list &seclist)
{
  int n = len(seclist);
  std::vector<SolidShellCrossSection::Pointer> shell_sec_list;
  for (int i = 0; i < n; i++)
  {
    auto p = pybind11::cast<SolidShellCrossSection::Pointer>(seclist[i]);
    shell_sec_list.push_back(p);
  }
  el.SetCrossSectionsOnIntegrationPoints(shell_sec_list);
}

void AddCrossSectionsToPython(pybind11::module &m)
{

  py::class_<SolidShellCrossSection, SolidShellCrossSection::Pointer>(m, "SolidShellCrossSection")
      .def(py::init<>())
      .def("BeginStack", &SolidShellCrossSection::BeginStack)
      .def("AddPly", &SolidShellCrossSection::AddPly)
      .def("EndStack", &SolidShellCrossSection::EndStack)
      .def("SetOffset", &SolidShellCrossSection::SetOffset)
      .def("Clone", &SolidShellCrossSection::Clone)
      .def("NumberOfPlies", &SolidShellCrossSection::NumberOfPlies)
      .def("NumberOfIntegrationPointsAt", &SolidShellCrossSection::NumberOfIntegrationPointsAt)
      .def("SetConstitutiveLawAt", &SolidShellCrossSection::SetConstitutiveLawAt)
      .def("__repr__", &SolidShellCrossSection::Info)
       DECLARE_ADD_THIS_TYPE_TO_PROPERTIES_PYTHON_AS_POINTER(SolidShellCrossSection)
       DECLARE_GET_THIS_TYPE_FROM_PROPERTIES_PYTHON_AS_POINTER(SolidShellCrossSection);

  py::class_<Variable<SolidShellCrossSection::Pointer>, VariableData>(m, "SolidShellCrossSectionVariable");

  py::class_<ShellThinElement3D3N, ShellThinElement3D3N::Pointer, Element>(m, "ThinShellElement3D3N")
      .def("SetCrossSectionsOnIntegrationPoints", &Helper_SetCrossSectionsOnIntegrationPoints_Thin);

  py::class_<ShellThickElement3D4N, ShellThickElement3D4N::Pointer, Element>(m, "ThickShellElement3D4N")
      .def("SetCrossSectionsOnIntegrationPoints", &Helper_SetCrossSectionsOnIntegrationPoints_Thick);
}

} // namespace Python

} // namespace Kratos
