//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:           MPetracca $
//   Maintained by:       $Maintainer:                    $
//   Date:                $Date:           September 2013 $
//
//

#if !defined(ADD_CROSS_SECTIONS_TO_PYTHON_H_INCLUDED)
#define ADD_CROSS_SECTIONS_TO_PYTHON_H_INCLUDED

// System includes
#include <pybind11/pybind11.h>

// External includes

// Project includes
#include "includes/define_python.h"

namespace Kratos
{

namespace Python
{

void AddCrossSectionsToPython(pybind11::module &m);
}

} // namespace Kratos

#endif // ADD_CROSS_SECTIONS_TO_PYTHON_H_INCLUDED
