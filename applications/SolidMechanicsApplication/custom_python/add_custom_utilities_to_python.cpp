//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:         JMCarbonell $
//   Maintained by:       $Maintainer:                JMC $
//   Date:                $Date:                July 2013 $
//
//

// System includes

// External includes

// Project includes
#include "custom_python/add_custom_utilities_to_python.h"

// Utilities
#include "custom_utilities/energy_utilities.h"
#include "custom_utilities/properties_utilities.hpp"

#include "custom_utilities/custom_model_part_io.hpp"

namespace Kratos
{

namespace Python
{

void PropertiesClone(PropertiesUtilities& rPropUtils, Properties::Pointer rOrigin, Properties::Pointer rDestination)
{
  rPropUtils.CloneProperties(rOrigin,rDestination);
}

void  AssignPropertiesToElements(PropertiesUtilities& rPropUtils, ModelPart &rModelPart, Properties::Pointer rProperties, const std::vector<Flags> &rTransferFlags)
{
  rPropUtils. AssignPropertiesToElements(rModelPart,rProperties,rTransferFlags);
}


void AddCustomUtilitiesToPython(pybind11::module &m)
{

  namespace py = pybind11;

  py::class_<CustomModelPartIO, CustomModelPartIO::Pointer, ModelPartIO>(
      m, "CustomModelPartIO")
      .def(py::init<std::string const &>())
      .def(py::init<std::string const &, const Flags>());

  py::class_<EnergyUtilities>(m, "EnergyUtilities")
      .def(py::init<>())
      .def("GetTotalKinematicEnergy", &EnergyUtilities::GetTotalKinematicEnergy)
      .def("CalculateNodalMass", &EnergyUtilities::CalculateNodalMass)
      .def("GetTotalStrainEnergy", &EnergyUtilities::GetTotalStrainEnergy)
      .def("GetGravitationalEnergy", &EnergyUtilities::GetGravitationalEnergy)
      .def("GetExternallyAppliedEnergy", &EnergyUtilities::GetExternallyAppliedEnergy);

  py::class_<PropertiesUtilities>(m, "PropertiesUtilities")
      .def(py::init<>())
      .def("AssignPropertiesToElements", AssignPropertiesToElements)
      .def("CloneProperties", PropertiesClone);

}

} // namespace Python.

} // Namespace Kratos
