# Solid Mechanics Application

This application focuses on the Finite Element Method (FEM) for solids, shells and beams.

The SolidMechanics application team are in charge of all developments related to this application.

## Build

This application is part of the Kratos Multiphysics Platform. Instructions on how to get a copy of the project and run it on your local machine are available for both [Linux](https://github.com/KratosMultiphysics/Kratos/wiki/Linux-Build) and [Windows](https://https://github.com/KratosMultiphysics/Kratos/wiki/Windows-Install) systems.

The guide to build the application can be found in the [Install instructions](https://gitlab.com/pfem-research/kratos/tree/master/INSTALL.md) or in the [Kratos wiki](https://github.com/KratosMultiphysics/Kratos/wiki).

The application to be activated is:

``` cmake
add_app ${KRATOS_APP_DIR}/SolidMechanicsApplication
```

If frequency analysis is needed, this module has to be activated:

``` cmake
-DINCLUDE_FEAST=ON
```

## Theory

Classical FE method theory for small and large displacements.

[original wiki web page](http://kratos-wiki.cimne.upc.edu/index.php/Solid_Mechanics_Application)

## Solid Elements

![Open cylinder pullout animation](tests/tests_data/dynamic_3d_beam.gif)

### Displacement based formulations:

Infinitessimal formulation, Total Lagrangian formulation, Updated Lagrangian formulation.

Bbar element for second order elements and infinitessimal theory.

### Mixed elements:

U-P formulation for linear triangles and linear tetrahedra. Monolitich approach with Bochev stabilization.

## Structural Elements:

### Shells

Co-rotational thick and thin 3D shells theory. Large displacements and large rotations.

![Open cylinder pullout animation](tests/tests_data/shell_roll_up.gif)

### Beams

Geometrically exact theory for 3D beams. Large displacements and large rotations.

![Open cylinder pullout animation](tests/tests_data/beam_roll_down.gif)

### Material Laws

The Material laws are implemented in the [ConstitutiveModelsApplication](https://gitlab.com/pfem-research/kratos/tree/develop/applications/ConstitutiveModelsApplication).

Legacy constitutive laws are located in the ConstitutiveLegacyApplication, you can activate it in the configuration:

``` cmake
add_app ${KRATOS_APP_DIR}/ConstitutiveLegacyApplication
```

### Time integration schemes

The standard schemes for displacement and rotation degrees of freedom. The following schemes can be chosen separately for -displacements- and for -displacements and rotations-:

* Newmak Scheme
* Bossak Scheme
* Simo scheme
* Static scheme ( no time integration is performed )

### Contact mechanics

The mechanics of contact is implemented in the [ContactMechanicsApplication](https://gitlab.com/pfem-research/kratos/tree/develop/applications/ContactMechanicsApplication).

## Available Interfaces

### GiD Interface

It is located in GiD interface repository in [GiD interface repository](https://github.com/KratosMultiphysics/GiDInterface/tree/master/).

Requires [GiD](https://www.gidhome.com/) - Pre and Post Processing software.

## Releases

The last release of the Solid Mechanics application is supplied with GiD the interface, it can be automatically downloaded in Data->Problem_type->Kratos :: Solid (in application market)

## License

The Solid Mechanics application is OPEN SOURCE. The main code and program structure is available and aimed to grow with the need of any user willing to expand it. The BSD (Berkeley Software Distribution) licence allows to use and distribute the existing code without any restriction, but with the possibility to develop new parts of the code on an open or close source depending on the developers.

## Contact

* **Josep Maria Carbonell** - *Core Development* - [cpuigbo@cimne.upc.edu](mailto:cpuigbo@cimne.upc.edu)
