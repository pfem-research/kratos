//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:         JMCarbonell $
//   Maintained by:       $Maintainer:                JMC $
//   Date:                $Date:              August 2019 $
//
//

#if !defined(KRATOS_MANAGE_TIME_STEP_PROCESS_HPP_INCLUDED)
#define KRATOS_MANAGE_TIME_STEP_PROCESS_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "includes/model_part.h"
#include "includes/kratos_parameters.h"
#include "processes/process.h"
#include "includes/kratos_parameters.h"

#include "solid_mechanics_application_variables.h"

namespace Kratos
{

///@name Kratos Classes
///@{

/// The base class for assigning a value to scalar variables or array_1d components processes in Kratos.
/** This function assigns a value to a variable belonging to all of the nodes in a given mesh
*/
class ManageTimeStepProcess : public Process
{
public:
   ///@name Type Definitions
   ///@{

   /// Pointer definition of ManageTimeStepProcess
   KRATOS_CLASS_POINTER_DEFINITION(ManageTimeStepProcess);

   ///@}
   ///@name Life Cycle
   ///@{

   /*ManageTimeStepProcess(ModelPart& rModelPart,
                          double& rMinDeltaTime, double& rMaxDeltaTime,
                          double& rReductionFactor, double& rIncreaseFactor,
                          double& rErrorTolerance, unsigned int& rMinIterations,
                          unsigned int& rMaxIterations, unsigned int& rNumberOfConstantSteps) : Process(Flags()),  mrModelPart(rModelPart), mMinDeltaTime(rMinDeltaTime), mMaxDeltaTime(rMaxDeltaTime), mReductionFactor(rReductionFactor), mIncreaseFactor(rIncreaseFactor), mErrorTolerance(rErrorTolerance), mMinIterations(rMinIterations), mMaxIterations(rMaxIterations), mNumberOfConstantSteps(rNumberOfConstantSteps)
    {
    }*/

   ManageTimeStepProcess(ModelPart &rModelPart,
                         Parameters CustomParameters)
       : mrModelPart(rModelPart)
   {
      KRATOS_TRY

      Parameters DefaultParameters(R"(
            {
                 "time_step": 1.0,
                 "start_time": 0.0,
                 "end_time": 1.0,
                 "adaptive_time_step":{
                     "minimum_time_step": 0.1,
                     "maximum_time_step": 1.0,
                     "reduction_factor": 2.0,
                     "increase_factor": 1.0,
                     "desired_iterations": 2
                 }
            }  )");

      mAdaptiveTimeStep = false;
      if (CustomParameters.Has("adaptive_time_step"))
         mAdaptiveTimeStep = true;

      //validate against defaults -- this also ensures no type mismatch
      CustomParameters.ValidateAndAssignDefaults(DefaultParameters);

      ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();

      if (rCurrentProcessInfo[IS_RESTARTED] == false)
      {
         rCurrentProcessInfo.SetValue(DELTA_TIME, CustomParameters["time_step"].GetDouble());
         rCurrentProcessInfo.SetValue(TIME, CustomParameters["start_time"].GetDouble());
      }

      mTime = rCurrentProcessInfo[TIME];
      mStep = rCurrentProcessInfo[STEP];
      mEndTime = CustomParameters["end_time"].GetDouble();

      if (mAdaptiveTimeStep)
         SetAdaptiveTimeParameters(CustomParameters["adaptive_time_step"]);

      KRATOS_CATCH(" ")
   }

   /// Destructor.
   virtual ~ManageTimeStepProcess() {}

   ///@}
   ///@name Operators
   ///@{

   /// This operator is provided to call the process as a function and simply calls the Execute method.
   void operator()()
   {
      Execute();
   }

   ///@}
   ///@name Operations
   ///@{

   /// Execute method is used to execute the ManageTimeStepProcess algorithms.
   void Execute() override
   {
      KRATOS_TRY

      ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();

      double PreviousDeltaTime = rCurrentProcessInfo[DELTA_TIME];
      double DeltaTime = PreviousDeltaTime;

      if (mAdaptiveTimeStep)
         DeltaTime = PredictTimeStep();

      if ((mAdaptiveTimeStep == false) || (mAccepted == true && mAdaptiveTimeStep == true))
      {

         mTime = rCurrentProcessInfo[TIME] + DeltaTime;
         mrModelPart.CloneTimeStep(mTime);

         rCurrentProcessInfo[STEP] = (++mStep);
         rCurrentProcessInfo[DELTA_TIME] = DeltaTime;
      }
      else
      {
         mTime = rCurrentProcessInfo[TIME] + DeltaTime - PreviousDeltaTime;
         rCurrentProcessInfo[TIME] = mTime;
         rCurrentProcessInfo[DELTA_TIME] = DeltaTime;
      }

      KRATOS_CATCH("")
   }

   /// check if we should "accept" the solution or we should "reject" it
   virtual bool CheckStepConverged(bool &rConverged)
   {
      KRATOS_TRY

      if (rConverged == false && mAdaptiveTimeStep)
      {

         const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();
         const double &rDeltaTime = rCurrentProcessInfo[DELTA_TIME];

         if (rDeltaTime <= mMinDeltaTime)
         {
            rConverged = true;
         }
      }

      mAccepted = rConverged;

      return rConverged;

      KRATOS_CATCH("")
   }

   ///@}
   ///@name Access
   ///@{

   ///@}
   ///@name Inquiry
   ///@{

   ///@}
   ///@name Input and output
   ///@{

   /// Turn back information as a string.
   std::string Info() const override
   {
      return "ManageTimeStepProcess";
   }

   /// Print information about this object.
   void PrintInfo(std::ostream &rOStream) const override
   {
      rOStream << "ManageTimeStepProcess";
   }

   /// Print object's data.
   void PrintData(std::ostream &rOStream) const override
   {
   }

   ///@}
   ///@name Friends
   ///@{
   ///@}

protected:
   ///@name Protected static Member Variables
   ///@{
   ///@}
   ///@name Protected member Variables
   ///@{
   ///@}
   ///@name Protected Operators
   ///@{

   /// Copy constructor.
   ManageTimeStepProcess(ManageTimeStepProcess const &rOther);

   ///@}
   ///@name Protected Operations
   ///@{

   virtual double PredictTimeStep()
   {
      KRATOS_TRY

      //if ( mDesiredIterations < 100) {
      const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();
      const double PreviousTimeStep = rCurrentProcessInfo[DELTA_TIME];
      const int &rNumberOfIterations = rCurrentProcessInfo[NL_ITERATION_NUMBER];

      double NewTimeStep = PreviousTimeStep;

      if (rNumberOfIterations > 0)
      {

         double TimeScaling = pow(double(mDesiredIterations) / double(rNumberOfIterations), 0.25);
         if (TimeScaling > mIncreaseFactor)
            TimeScaling = mIncreaseFactor;
         if (TimeScaling < mReductionFactor)
            TimeScaling = mReductionFactor;

         NewTimeStep = TimeScaling * PreviousTimeStep;
         if (NewTimeStep > mMaxDeltaTime)
            NewTimeStep = mMaxDeltaTime;
         if (NewTimeStep < mMinDeltaTime)
            NewTimeStep = mMinDeltaTime;
      }
      return NewTimeStep;

      // this is a process for implex...
      /*} else {

           ProcessInfo& rCurrentProcessInfo = mrModelPart.GetProcessInfo();

           const double PreviousTimeStep = rCurrentProcessInfo[DELTA_TIME];
           double NewTimeStep = 1.0e32;

           std::vector< double > TimePrediction;

           for (ModelPart::ElementsContainerType::const_iterator pElement = mrModelPart.ElementsBegin(); pElement != mrModelPart.ElementsEnd() ; pElement++)
           {
           pElement->CalculateOnIntegrationPoints( TIME_STEP_PREDICTION, TimePrediction, rCurrentProcessInfo);

           for ( unsigned int ii = 0; ii < TimePrediction.size(); ii++) {
           if ( TimePrediction[ii] < NewTimeStep & TimePrediction[ii] > 0.0) {
           NewTimeStep = TimePrediction[ii];
           }
           }
           }

           double characteristic_value = 0.00001;
           NewTimeStep = sqrt(NewTimeStep * characteristic_value ) * PreviousTimeStep;
           double TimeScaling = NewTimeStep/PreviousTimeStep;

           if ( TimeScaling > mIncreaseFactor)
           TimeScaling = mIncreaseFactor;
           if (TimeScaling < mReductionFactor)
           TimeScaling = mReductionFactor;

           NewTimeStep = TimeScaling * PreviousTimeStep;
           if ( NewTimeStep > mMaxDeltaTime)
           NewTimeStep = mMaxDeltaTime;
           if (NewTimeStep < mMinDeltaTime)
           NewTimeStep = mMinDeltaTime;

           return NewTimeStep;

           }

          */

      KRATOS_CATCH("")
   }

   ///@}
   ///@name Protected  Access
   ///@{
   ///@}
   ///@name Protected Inquiry
   ///@{
   ///@}
   ///@name Protected LifeCycle
   ///@{
   ///@}

private:
   ///@name Static Member Variables
   ///@{
   ///@}
   ///@name Member Variables
   ///@{

   ModelPart &mrModelPart;

   bool mAdaptiveTimeStep;
   bool mAccepted;

   double mTime;
   double mStep;
   double mEndTime;

   double mMinDeltaTime;
   double mMaxDeltaTime;

   double mReductionFactor;
   double mIncreaseFactor;

   double mDesiredIterations;

   ///@}
   ///@name Private Operators
   ///@{
   ///@}
   ///@name Private Operations
   ///@{

   void SetAdaptiveTimeParameters(Parameters CustomParameters)
   {
      KRATOS_TRY

      Parameters DefaultParameters(R"(
      {
                     "minimum_time_step": 0.1,
                     "maximum_time_step": 1.0,
                     "reduction_factor": 2.0,
                     "increase_factor": 1.0,
                     "desired_iterations": 2
      }  )");

      //validate against defaults -- this also ensures no type mismatch
      CustomParameters.ValidateAndAssignDefaults(DefaultParameters);

      mMinDeltaTime = CustomParameters["minimum_time_step"].GetDouble();
      mMaxDeltaTime = CustomParameters["maximum_time_step"].GetDouble();

      mReductionFactor = CustomParameters["reduction_factor"].GetDouble();
      mIncreaseFactor = CustomParameters["increase_factor"].GetDouble();

      mDesiredIterations = CustomParameters["desired_iterations"].GetInt();

      mAccepted = true;

      KRATOS_CATCH(" ")
   }
   ///@}
   ///@name Private  Access
   ///@{

   /// Assignment operator.
   ManageTimeStepProcess &operator=(ManageTimeStepProcess const &rOther);

   ///@}
   ///@name Serialization
   ///@{
   ///@name Private Inquiry
   ///@{
   ///@}
   ///@name Un accessible methods
   ///@{
   ///@}

}; // Class ManageTimeStepProcess

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                ManageTimeStepProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const ManageTimeStepProcess &rThis)
{
   rThis.PrintInfo(rOStream);
   rOStream << std::endl;
   rThis.PrintData(rOStream);

   return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_MANAGE_TIME_STEP_PROCESS_HPP_INCLUDED  defined
