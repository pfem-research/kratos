//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:         JMCarbonell $
//   Maintained by:       $Maintainer:                JMC $
//   Date:                $Date:                June 2019 $
//
//

#if !defined(KRATOS_TIME_DISCRETIZATION_PROCESS_HPP_INCLUDED)
#define KRATOS_TIME_DISCRETIZATION_PROCESS_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "includes/model_part.h"
#include "utilities/openmp_utils.h"
#include "geometries/triangle_2d_3.h"
#include "geometries/triangle_2d_6.h"
#include "geometries/tetrahedra_3d_4.h"
#include "geometries/tetrahedra_3d_10.h"
#include "processes/process.h"

namespace Kratos
{

///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
 */
class TimeDiscretizationProcess
    : public Process
{
public:
  ///@name Type Definitions
  ///@{

  typedef ModelPart::NodesContainerType NodesContainerType;
  typedef ModelPart::ElementsContainerType ElementsContainerType;
  typedef ModelPart::MeshType::GeometryType::PointsArrayType PointsArrayType;

  typedef GlobalPointersVector<Node<3>> NodeWeakPtrVectorType;
  typedef GlobalPointersVector<Element> ElementWeakPtrVectorType;
  typedef GlobalPointersVector<Condition> ConditionWeakPtrVectorType;

  /// Pointer definition of TimeDiscretizationProcess
  KRATOS_CLASS_POINTER_DEFINITION(TimeDiscretizationProcess);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  TimeDiscretizationProcess(ModelPart &rModelPart,
                            Parameters rParameters) : Process(Flags()), mrModelPart(rModelPart)
  {
    KRATOS_TRY

    Parameters default_parameters(R"(
            {
                "model_part_name":"MODEL_PART_NAME",
                "start_time": 0,
                "end_time": 1,
                "time_step": 1,
                "prediction_level": 0,
                "increase_factor": 2,
                "decrease_factor": 0.5,
                "steps_update_delay": 4,
                "milestone_time_step": 1
            }  )");

    // Validate against defaults -- this ensures no type mismatch
    rParameters.ValidateAndAssignDefaults(default_parameters);

    mTime.Initialize(rParameters["time_step"].GetDouble(), rParameters["start_time"].GetDouble(), rParameters["end_time"].GetDouble(), rParameters["milestone_time_step"].GetDouble());

    mTime.SetFactors(rParameters["increase_factor"].GetDouble(), rParameters["decrease_factor"].GetDouble(), rParameters["steps_update_delay"].GetInt());

    mTime.PredictionLevel = rParameters["prediction_level"].GetInt();

    const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();
    if (rCurrentProcessInfo[IS_RESTARTED])
    {
      mTime.TotalTime = rCurrentProcessInfo[TIME];
    }

    KRATOS_CATCH("")
  }

  /// Destructor.
  virtual ~TimeDiscretizationProcess()
  {
  }

  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  void Execute() override
  {

    KRATOS_TRY

    const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();

    //std::cout<<" Back Position ["<<mrModelPart.Nodes().back().Id()<<"] :"<<mrModelPart.Nodes().back().Coordinates()<<" "<<mrModelPart.Nodes().back().GetInitialPosition()<<" Displacement "<<mrModelPart.Nodes().back().Coordinates()-mrModelPart.Nodes().back().GetInitialPosition()<<std::endl;
    //std::cout<<" Back Displacement ["<<mrModelPart.Nodes().back().Id()<<"] :"<<mrModelPart.Nodes().back().FastGetSolutionStepValue(DISPLACEMENT,1)<<"/"<<mrModelPart.Nodes().back().FastGetSolutionStepValue(DISPLACEMENT)<<std::endl;

    if (!rCurrentProcessInfo[CONVERGENCE_ACHIEVED])
    {
      this->ReduceTimeStep();
    }
    else
    {
      this->UpdateTimeStep();
    }

    // it sets TIME and DELTA_TIME internally and Clones the SolutionStepData and ProcessInfo buffers
    mrModelPart.CloneTimeStep(mTime.TotalTime);

    //std::cout<<" CLONE Back Displacement ["<<mrModelPart.Nodes().back().Id()<<"] :"<<mrModelPart.Nodes().back().FastGetSolutionStepValue(DISPLACEMENT,1)<<"/"<<mrModelPart.Nodes().back().FastGetSolutionStepValue(DISPLACEMENT)<<std::endl;

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Operators
  ///@{
  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "TimeDiscretizationProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "TimeDiscretizationProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{

  struct TimeParameters
  {

    int PredictionLevel;

    double MinimumTimeStep;
    double MaximumTimeStep;

    double InitialTimeStep;
    double PreviousTimeStep;
    double CurrentTimeStep;
    double MileStoneTimeStep;
    double PredictedTimeStep;

    double StartTime;
    double EndTime;
    double TotalTime;
    double MileStoneTime;

    double IncreaseFactor;
    double DecreaseFactor;

    int UpdateDelay;
    int DelayCounter;

    double MileStoneRecoveryStep;

    void Initialize(const double &rTimeStep, const double &rStartTime, const double &rEndTime, const double &rMileStoneTimeStep)
    {
      PredictionLevel = 0;

      InitialTimeStep = rTimeStep;
      PreviousTimeStep = rTimeStep;
      CurrentTimeStep = rTimeStep;
      MileStoneTimeStep = rMileStoneTimeStep;
      MileStoneRecoveryStep = 0;

      //set minimum time step
      MinimumTimeStep = 1e-2 * InitialTimeStep;
      //set maximum time step
      MaximumTimeStep = 2 * InitialTimeStep;
      if (MaximumTimeStep > MileStoneTimeStep) //can not be bigger than milestone timestep
        MaximumTimeStep = MileStoneTimeStep;

      StartTime = rStartTime;
      EndTime = rEndTime;
      TotalTime = 0;
      MileStoneTime = MileStoneTimeStep;
      PredictedTimeStep = MaximumTimeStep;

      DelayCounter = 0;
      std::cout << " INITIALIZE TIME PARAMETERS [TimeStep:" << InitialTimeStep << ",StartTime:" << StartTime << ",EndTime:" << EndTime << "]" << std::endl;
    }

    void SetFactors(const double &rIncreaseFactor, const double &rDecreaseFactor, const int &rDelay)
    {
      IncreaseFactor = rIncreaseFactor;
      DecreaseFactor = rDecreaseFactor;
      UpdateDelay = rDelay;
    }

    bool Increase()
    {
      if (!ActiveDelay())
      {
        PreviousTimeStep = CurrentTimeStep;
        CurrentTimeStep *= IncreaseFactor;
        if (MaximumTimeStep <= CurrentTimeStep)
          CurrentTimeStep = MaximumTimeStep;
      }
      else
      {
        PreviousTimeStep = CurrentTimeStep;
      }

      if (TotalTime + CurrentTimeStep > MileStoneTime)
      {
        MileStoneRecoveryStep = PreviousTimeStep;

        // Tolerance
        //const double tolerance = std::numeric_limits<double>::epsilon(); //a zero can be bigger than this limit
        const double tolerance = MinimumTimeStep * 1e-5;

        if((MileStoneTime - TotalTime)>tolerance)
          CurrentTimeStep = MileStoneTime - TotalTime;
      }

      if (CurrentTimeStep != PreviousTimeStep)
        std::cout << "  (-increased timestep [dt:" << CurrentTimeStep << "/pre_dt:" << PreviousTimeStep << "] milestone [dt:" << MileStoneRecoveryStep << "/t:" << MileStoneTime << "]-) (delay:" << DelayCounter << ")" << std::endl;

      return CheckSameTime(PreviousTimeStep, CurrentTimeStep);
    }

    bool Decrease()
    {
      //if( !ActiveDelay() ){
      PreviousTimeStep = CurrentTimeStep;
      CurrentTimeStep *= DecreaseFactor;
      if (CurrentTimeStep <= MinimumTimeStep)
        CurrentTimeStep = MinimumTimeStep;
      //}

      if (CurrentTimeStep != PreviousTimeStep)
        std::cout << "  (-decreased timestep [dt:" << CurrentTimeStep << "/" << PreviousTimeStep << "] milestone [dt:" << MileStoneRecoveryStep << "/t:" << MileStoneTime << "]-) (delay:" << DelayCounter << ")" << std::endl;

      return CheckSameTime(PreviousTimeStep, CurrentTimeStep);
    }

    bool PredictActive()
    {
      if (PredictionLevel >= 0 && EndTime > StartTime)
      {
        return true;
      }
      else
        return false;
    }

    bool Update()
    {

      if (PredictedTimeStep >= CurrentTimeStep)
      {
        if (PredictedTimeStep > CurrentTimeStep)
          std::cout << "  (-predicted step: " << PredictedTimeStep << " > " << CurrentTimeStep << " to increase-) (delay:" << DelayCounter << ")" << std::endl;
        return Increase();
      }
      else
      {
        std::cout << "  (-predicted step: " << PredictedTimeStep << " < " << CurrentTimeStep << " to decrease-) (delay:" << DelayCounter << ")" << std::endl;
        return Decrease();
      }
    }

    bool ActiveDelay()
    {
      if (CheckMileStoneStep())
        return false;

      if (DelayCounter >= UpdateDelay)
      {
        return false;
      }
      else
      {
        return true;
      }
    }

    bool CheckSameTime(const double &rTime, const double &rNewTime)
    {
      double tolerance = InitialTimeStep * 1e-5;
      if (rNewTime > rTime - tolerance && rNewTime < rTime + tolerance)
      {
        ++DelayCounter;
        return true;
      }
      else
      {
        DelayCounter = 0;
        return false;
      }
    }

    bool CheckMileStoneStep()
    {
      if (TotalTime >= MileStoneTime)
      {
        MileStoneTime += MileStoneTimeStep;
      }

      if (MileStoneRecoveryStep != 0)
      {
        CurrentTimeStep = MileStoneRecoveryStep;
        MileStoneRecoveryStep = 0;
        return true;
      }
      return false;
    }
  };

  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{

  void ReduceTimeStep()
  {
    KRATOS_TRY

    this->SetMileStoneTime();

    ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();

    mTime.TotalTime -= mTime.CurrentTimeStep;

    //std::cout<<" Back Position ["<<mrModelPart.Nodes().back().Id()<<"] :"<<mrModelPart.Nodes().back().Coordinates()<<" "<<mrModelPart.Nodes().back().GetInitialPosition()<<std::endl;

    RestorePosition(mrModelPart);

    //std::cout<<" OVWR Back Position ["<<mrModelPart.Nodes().back().Id()<<"] :"<<mrModelPart.Nodes().back().Coordinates()<<" "<<mrModelPart.Nodes().back().GetInitialPosition()<<std::endl;

    unsigned int buffer_size = mrModelPart.GetBufferSize();
    for (unsigned int i = 1; i < buffer_size; ++i)
    {
      //std::cout<<" Back Displacement ["<<mrModelPart.Nodes().back().Id()<<"] :"<<mrModelPart.Nodes().back().FastGetSolutionStepValue(DISPLACEMENT,1)<<"/"<<mrModelPart.Nodes().back().FastGetSolutionStepValue(DISPLACEMENT)<<std::endl;

      mrModelPart.OverwriteSolutionStepData(i, i - 1);

      //std::cout<<" OVWR Back Displacement ["<<mrModelPart.Nodes().back().Id()<<"] :"<<mrModelPart.Nodes().back().FastGetSolutionStepValue(DISPLACEMENT,1)<<"/"<<mrModelPart.Nodes().back().FastGetSolutionStepValue(DISPLACEMENT)<<std::endl;

      if (i > 1)
      {
        rCurrentProcessInfo.pGetPreviousSolutionStepInfo(i - 1) = rCurrentProcessInfo.pGetPreviousSolutionStepInfo(i);
      }
      else
      {
        rCurrentProcessInfo = *rCurrentProcessInfo.pGetPreviousSolutionStepInfo(i);
      }
    }
    //mrModelPart.ReduceTimeStep(mrModelPart,mTime.TotalTime);

    rCurrentProcessInfo.SetValue(DELTA_TIME_CHANGED, mTime.Decrease());

    // update total time
    mTime.TotalTime += mTime.CurrentTimeStep;

    std::cout<<" Reduce Time Step: [DELTA_TIME:"<<mTime.CurrentTimeStep<<",TIME:"<<mTime.TotalTime<<",STEP:"<<rCurrentProcessInfo[STEP]<<"]"<<std::endl;

    KRATOS_CATCH("")
  }

  void UpdateTimeStep()
  {
    KRATOS_TRY

    this->SetMileStoneTime();

    ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();

    this->PredictTimeStep();

    rCurrentProcessInfo.SetValue(DELTA_TIME_CHANGED, mTime.Update());

    // update total time
    mTime.TotalTime += mTime.CurrentTimeStep;

    rCurrentProcessInfo[STEP] += 1;

    std::cout<<" Update Time Step: [DELTA_TIME:"<<mTime.CurrentTimeStep<<",TIME:"<<mTime.TotalTime<<",STEP:"<<rCurrentProcessInfo[STEP]<<"]"<<std::endl;

    KRATOS_CATCH("")
  }

  void SetMileStoneTime()
  {
    KRATOS_TRY

    const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();
    mTime.MileStoneTime = rCurrentProcessInfo[PRINTED_STEP_TIME];

    KRATOS_CATCH("")
  }

  void PredictTimeStep()
  {
    KRATOS_TRY

    if (mTime.PredictActive())
    {
      this->PredictTimeStep(mTime.PredictedTimeStep);
    }

    KRATOS_CATCH("")
  }

  void PredictTimeStep(double &rTimeStep)
  {
    KRATOS_TRY

    rTimeStep = mTime.MaximumTimeStep;

    CheckCriticalElement(rTimeStep);

    KRATOS_CATCH("")
  }

  void CheckCriticalElement(double &rTimeStep)
  {
    KRATOS_TRY

    std::string variable = "VELOCITY";
    if (mrModelPart.NodesBegin()->SolutionStepsDataHas(MESH_VELOCITY))
      variable = "MESH_VELOCITY";

    const Variable<array_1d<double, 3>>& VelocityVariable = KratosComponents<Variable<array_1d<double, 3>>>::Get(variable);

    ModelPart::ElementsContainerType::iterator it_begin = mrModelPart.ElementsBegin();
    const int nelems = mrModelPart.NumberOfElements();
    unsigned int critical_elements = 0;
    const double time_step = rTimeStep;

#pragma omp parallel for reduction(+ : critical_elements)
    for (int i = 0; i < nelems; ++i)
    {
      ModelPart::ElementsContainerType::iterator ie = it_begin + i;

      Element::GeometryType &eGeometry = ie->GetGeometry();

      double maximum = std::numeric_limits<double>::min();
      double minimum = std::numeric_limits<double>::max();

      const double length = eGeometry.Length();

      for (auto &i_node : eGeometry)
      {
        KRATOS_WARNING_IF("",!std::isfinite(norm_2(i_node.FastGetSolutionStepValue(DISPLACEMENT)))) << " Node displacement is NaN node [" <<i_node.Id()<<" ]"<<std::endl;

        double norm = norm_2(i_node.FastGetSolutionStepValue(VelocityVariable));
        if (norm > maximum)
          maximum = norm;
        if (norm < minimum)
          minimum = norm;
      }

      if (minimum != 0)
      {
        if ((maximum / minimum) > 1.5)
        {
          double step_prediction = length / maximum;

          if (step_prediction < time_step)
            ++critical_elements;

          //std::cout<<" maximum "<<maximum<<" step_prediction "<<step_prediction<<" length "<<eGeometry.Length()<<std::endl;
          if (step_prediction < rTimeStep)
          {
#pragma omp critical
            if (step_prediction < rTimeStep)
              rTimeStep = step_prediction;
          }
        }
      }
    }

    if (critical_elements < 5)
      rTimeStep = time_step;

    KRATOS_CATCH("")
  }

  void RestorePosition(ModelPart &rModelPart)
  {
    KRATOS_TRY

    ModelPart::NodesContainerType::iterator it_begin = mrModelPart.NodesBegin();
    const int nnodes = mrModelPart.NumberOfNodes();

#pragma omp parallel for
    for (int i = 0; i < nnodes; ++i)
    {
      ModelPart::NodesContainerType::iterator it = it_begin + i;
      it->Coordinates() = it->GetInitialPosition() + it->FastGetSolutionStepValue(DISPLACEMENT, 1);
    }

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Private Static Member Variables
  ///@{

  ModelPart &mrModelPart;

  TimeParameters mTime;

  ///@}
  ///@name Private Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{

  /// Assignment operator.
  TimeDiscretizationProcess &operator=(TimeDiscretizationProcess const &rOther);

  /// Copy constructor.
  //TimeDiscretizationProcess(TimeDiscretizationProcess const& rOther);

  ///@}

}; // Class TimeDiscretizationProcess

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                TimeDiscretizationProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const TimeDiscretizationProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_TIME_DISCRETIZATION_PROCESS_HPP_INCLUDED  defined
