//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:         JMCarbonell $
//   Maintained by:       $Maintainer:                    $
//   Date:                $Date:                 May 2020 $
//
//

#if !defined(KRATOS_ASSIGN_ELEMENT_TYPE_PROCESS_HPP_INCLUDED)
#define KRATOS_ASSIGN_ELEMENT_TYPE_PROCESS_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "solid_mechanics_application_variables.h"

namespace Kratos
{

  ///@name Kratos Classes
  ///@{

  /// Process to transfer model part entities to a solving submodelpart

  class AssignElementTypeProcess : public Process
  {
  public:
    ///@name Type Definitions
    ///@{

    typedef Element ElementType;

    /// Pointer definition of AssignElementTypeProcess
    KRATOS_CLASS_POINTER_DEFINITION(AssignElementTypeProcess);

    ///@}
    ///@name Life Cycle
    ///@{
    AssignElementTypeProcess(ModelPart &rModelPart, Parameters rParameters) : Process(), mrModelPart(rModelPart)
    {
      KRATOS_TRY

      Parameters default_parameters(R"(
        {
             "element_parts_list": []
        }  )");

      // Validate against defaults -- this ensures no type mismatch
      rParameters.ValidateAndAssignDefaults(default_parameters);

      for (unsigned int i = 0; i < rParameters["element_parts_list"].size(); ++i)
        {
          mModelPartsList.push_back(mrModelPart.pGetSubModelPart(rParameters["element_parts_list"][i]["model_part_name"].GetString()));
          mElementsList.push_back(GetElementType(mModelPartsList.back()->Elements().front(), rParameters["element_parts_list"][i]["element_type"].GetString()));

          mDeletePartsList.push_back(rParameters["element_parts_list"][i]["delete_model_part"].GetBool());
        }

      KRATOS_CATCH("")
    }

    /// Destructor.
    ~AssignElementTypeProcess() override {}

    ///@}
    ///@name Operators
    ///@{

    /// This operator is provided to call the process as a function and simply calls the Execute method.
    void operator()()
    {
      Execute();
    }

    ///@}
    ///@name Operations
    ///@{

    /// Execute method is used to execute the AssignElementTypeProcess algorithms.
    void Execute() override
    {
      KRATOS_TRY

      std::vector<Element::Pointer> elements_vector(mrModelPart.NumberOfElements()+1);

      // Get all elements of main model part
      for (ModelPart::ElementsContainerType::iterator i_elem = mrModelPart.ElementsBegin(); i_elem != mrModelPart.ElementsEnd(); ++i_elem)
      {
        elements_vector[i_elem->Id()] = (*i_elem.base());
      }

      // Set new elements to vector and to visited parts
      for(unsigned int i=0; i < mModelPartsList.size(); ++i)
      {
        for (ModelPart::ElementsContainerType::iterator i_elem = mModelPartsList[i]->ElementsBegin(); i_elem != mModelPartsList[i]->ElementsEnd(); ++i_elem)
          {
            (*i_elem.base()).reset(mElementsList[i]->Create(i_elem->Id(), i_elem->GetGeometry(), i_elem->pGetProperties()).get());
            elements_vector[i_elem->Id()] = *i_elem.base();
          }
        if(mDeletePartsList[i])
          mrModelPart.RemoveSubModelPart(*mModelPartsList[i]);
      }

      // Set new elements to main model part
      bool visited = false;
      for(auto &ii_mp : mModelPartsList)
        if( mrModelPart.Name() == ii_mp->Name() )
          visited = true;

      if(!visited)
        for (ModelPart::ElementsContainerType::iterator i_elem = mrModelPart.ElementsBegin(); i_elem != mrModelPart.ElementsEnd(); ++i_elem)
          {
            (*i_elem.base()).reset(elements_vector[i_elem->Id()].get());
          }

      // Set new elements to not visited model parts
      for (auto &i_mp : mrModelPart.SubModelParts())
      {
        visited = false;
        for(unsigned int i=0; i < mModelPartsList.size(); ++i)
          if(i_mp.Name() == mModelPartsList[i]->Name() && mDeletePartsList[i])
            visited = true;

        if(!visited)
          for (ModelPart::ElementsContainerType::iterator i_elem = i_mp.ElementsBegin(); i_elem != i_mp.ElementsEnd(); ++i_elem)
            {
              (*i_elem.base()).reset(elements_vector[i_elem->Id()].get());
            }
      }


      KRATOS_CATCH("")
    }

    ///@}
    ///@name Access
    ///@{

    ///@}
    ///@name Inquiry
    ///@{

    ///@}
    ///@name Input and output
    ///@{

    /// Turn back information as a string.
    std::string Info() const override
    {
      return "AssignElementTypeProcess";
    }

    /// Print information about this object.
    void PrintInfo(std::ostream &rOStream) const override
    {
      rOStream << "AssignElementTypeProcess";
    }

    /// Print object's data.
    void PrintData(std::ostream &rOStream) const override
    {
    }

    ///@}
    ///@name Friends
    ///@{
    ///@}

  protected:
    ///@name Protected static Member Variables
    ///@{
    ///@}
    ///@name Protected member Variables
    ///@{
    ///@}
    ///@name Protected Operators
    ///@{

    /// Copy constructor.
    AssignElementTypeProcess(AssignElementTypeProcess const &rOther);

    ///@}
    ///@name Protected Operations
    ///@{
    ///@}
    ///@name Protected  Access
    ///@{
    ///@}
    ///@name Protected Inquiry
    ///@{
    ///@}
    ///@name Protected LifeCycle
    ///@{
    ///@}

  private:
    ///@name Static Member Variables
    ///@{
    ///@}
    ///@name Member Variables
    ///@{

    ModelPart &mrModelPart;

    std::vector<ModelPart*> mModelPartsList;
    std::vector<const ElementType*> mElementsList;
    std::vector<bool> mDeletePartsList;

    ///@}
    ///@name Private Operators
    ///@{
    ///@}
    ///@name Private Operations
    ///@{

    const ElementType *GetElementType(const Element& rReferenceElement, const std::string &rElementName)
    {

      std::string dimension = std::to_string(rReferenceElement.GetGeometry().WorkingSpaceDimension());
      std::string number = std::to_string(rReferenceElement.GetGeometry().size());

      // must be set to each part
      std::string name = rElementName + dimension + "D" + number + "N";

      const ElementType *pElementType;

      if( KratosComponents<ElementType>::Has(name) )
        pElementType = &(KratosComponents<ElementType>::Get(name));
      else
        KRATOS_ERROR << "Element name : "<< name << " not registered in Kratos "<<std::endl;


      return pElementType;
    };

    ///@}
    ///@name Private  Access
    ///@{

    /// Assignment operator.
    AssignElementTypeProcess &operator=(AssignElementTypeProcess const &rOther);

    ///@}
    ///@name Serialization
    ///@{
    ///@name Private Inquiry
    ///@{
    ///@}
    ///@name Un accessible methods
    ///@{
    ///@}

  }; // Class AssignElementTypeProcess

  ///@}

  ///@name Type Definitions
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// input stream function
  inline std::istream &operator>>(std::istream &rIStream,
                                  AssignElementTypeProcess &rThis);

  /// output stream function
  inline std::ostream &operator<<(std::ostream &rOStream,
                                  const AssignElementTypeProcess &rThis)
  {
    rThis.PrintInfo(rOStream);
    rOStream << std::endl;
    rThis.PrintData(rOStream);

    return rOStream;
  }
  ///@}

} // namespace Kratos.

#endif // KRATOS_ASSIGN_ELEMENT_TYPE_PROCESS_HPP_INCLUDED  defined
