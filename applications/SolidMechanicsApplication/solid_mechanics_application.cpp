//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:         JMCarbonell $
//   Maintained by:       $Maintainer:                JMC $
//   Date:                $Date:                July 2013 $
//
//

// System includes

// External includes

// Project includes
#include "geometries/triangle_2d_3.h"
#include "geometries/triangle_2d_6.h"

#include "geometries/quadrilateral_2d_4.h"
#include "geometries/quadrilateral_2d_8.h"
#include "geometries/quadrilateral_2d_9.h"

#include "geometries/triangle_3d_3.h"

#include "geometries/quadrilateral_3d_4.h"
#include "geometries/quadrilateral_3d_8.h"
#include "geometries/quadrilateral_3d_9.h"

#include "geometries/tetrahedra_3d_4.h"
#include "geometries/tetrahedra_3d_10.h"

#include "geometries/hexahedra_3d_8.h"
#include "geometries/hexahedra_3d_20.h"
#include "geometries/hexahedra_3d_27.h"

#include "geometries/prism_3d_6.h"
#include "geometries/prism_3d_15.h"

#include "geometries/line_2d_2.h"
#include "geometries/line_3d_2.h"
#include "geometries/line_3d_3.h"
#include "geometries/line_gauss_lobatto_3d_2.h"

#include "geometries/point_2d.h"
#include "geometries/point_3d.h"

#include "includes/element.h"
#include "includes/condition.h"
#include "includes/variables.h"
#include "includes/serializer.h"

#include "solid_mechanics_application.h"

namespace Kratos
{

//Application variables creation: (see solid_mechanics_application_variables.cpp)

//Application Constructor:

KratosSolidMechanicsApplication::KratosSolidMechanicsApplication()
    : KratosApplication("SolidMechanicsApplication"),
      mLinearSolidElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mLinearSolidElement2D4N(
          0, Kratos::make_shared<Quadrilateral2D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),
      mLinearSolidElement3D4N(
          0, Kratos::make_shared<Tetrahedra3D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),
      mLinearSolidElement3D8N(
          0, Kratos::make_shared<Hexahedra3D8<Node<3>>>(
                 Element::GeometryType::PointsArrayType(8))),

      mSmallDisplacementElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mSmallDisplacementElement2D4N(
          0, Kratos::make_shared<Quadrilateral2D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),
      mSmallDisplacementElement2D6N(
          0, Kratos::make_shared<Triangle2D6<Node<3>>>(
                 Element::GeometryType::PointsArrayType(6))),
      mSmallDisplacementElement2D8N(
          0, Kratos::make_shared<Quadrilateral2D8<Node<3>>>(
                 Element::GeometryType::PointsArrayType(8))),
      mSmallDisplacementElement2D9N(
          0, Kratos::make_shared<Quadrilateral2D9<Node<3>>>(
                 Element::GeometryType::PointsArrayType(9))),
      mSmallDisplacementElement3D4N(
          0, Kratos::make_shared<Tetrahedra3D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),
      mSmallDisplacementElement3D6N(
          0, Kratos::make_shared<Prism3D6<Node<3>>>(
                 Element::GeometryType::PointsArrayType(6))),
      mSmallDisplacementElement3D8N(
          0, Kratos::make_shared<Hexahedra3D8<Node<3>>>(
                 Element::GeometryType::PointsArrayType(8))),
      mSmallDisplacementElement3D10N(
          0, Kratos::make_shared<Tetrahedra3D10<Node<3>>>(
                 Element::GeometryType::PointsArrayType(10))),
      mSmallDisplacementElement3D15N(
          0, Kratos::make_shared<Prism3D15<Node<3>>>(
                 Element::GeometryType::PointsArrayType(15))),
      mSmallDisplacementElement3D20N(
          0, Kratos::make_shared<Hexahedra3D20<Node<3>>>(
                 Element::GeometryType::PointsArrayType(20))),
      mSmallDisplacementElement3D27N(
          0, Kratos::make_shared<Hexahedra3D27<Node<3>>>(
                 Element::GeometryType::PointsArrayType(27))),

      mSmallDisplacementBbarElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mSmallDisplacementBbarElement2D4N(
          0, Kratos::make_shared<Quadrilateral2D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),
      mSmallDisplacementBbarElement2D6N(
          0, Kratos::make_shared<Triangle2D6<Node<3>>>(
                 Element::GeometryType::PointsArrayType(6))),
      mSmallDisplacementBbarElement2D8N(
          0, Kratos::make_shared<Quadrilateral2D8<Node<3>>>(
                 Element::GeometryType::PointsArrayType(8))),
      mSmallDisplacementBbarElement2D9N(
          0, Kratos::make_shared<Quadrilateral2D9<Node<3>>>(
                 Element::GeometryType::PointsArrayType(9))),
      mSmallDisplacementBbarElement3D4N(
          0, Kratos::make_shared<Tetrahedra3D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),
      mSmallDisplacementBbarElement3D6N(
          0, Kratos::make_shared<Prism3D6<Node<3>>>(
                 Element::GeometryType::PointsArrayType(6))),
      mSmallDisplacementBbarElement3D8N(
          0, Kratos::make_shared<Hexahedra3D8<Node<3>>>(
                 Element::GeometryType::PointsArrayType(8))),
      mSmallDisplacementBbarElement3D10N(
          0, Kratos::make_shared<Tetrahedra3D10<Node<3>>>(
                 Element::GeometryType::PointsArrayType(10))),
      mSmallDisplacementBbarElement3D15N(
          0, Kratos::make_shared<Prism3D15<Node<3>>>(
                 Element::GeometryType::PointsArrayType(15))),
      mSmallDisplacementBbarElement3D20N(
          0, Kratos::make_shared<Hexahedra3D20<Node<3>>>(
                 Element::GeometryType::PointsArrayType(20))),
      mSmallDisplacementBbarElement3D27N(
          0, Kratos::make_shared<Hexahedra3D27<Node<3>>>(
                 Element::GeometryType::PointsArrayType(27))),

      mAxisymSmallDisplacementElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mAxisymSmallDisplacementElement2D4N(
          0, Kratos::make_shared<Quadrilateral2D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),
      mAxisymSmallDisplacementElement2D6N(
          0, Kratos::make_shared<Triangle2D6<Node<3>>>(
                 Element::GeometryType::PointsArrayType(6))),
      mAxisymSmallDisplacementElement2D8N(
          0, Kratos::make_shared<Quadrilateral2D8<Node<3>>>(
                 Element::GeometryType::PointsArrayType(8))),
      mAxisymSmallDisplacementElement2D9N(
          0, Kratos::make_shared<Quadrilateral2D9<Node<3>>>(
                 Element::GeometryType::PointsArrayType(9))),

      mTotalLagrangianElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mTotalLagrangianElement2D4N(
          0, Kratos::make_shared<Quadrilateral2D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),
      mTotalLagrangianElement2D6N(
          0, Kratos::make_shared<Triangle2D6<Node<3>>>(
                 Element::GeometryType::PointsArrayType(6))),
      mTotalLagrangianElement2D8N(
          0, Kratos::make_shared<Quadrilateral2D8<Node<3>>>(
                 Element::GeometryType::PointsArrayType(8))),
      mTotalLagrangianElement2D9N(
          0, Kratos::make_shared<Quadrilateral2D9<Node<3>>>(
                 Element::GeometryType::PointsArrayType(9))),
      mTotalLagrangianElement3D4N(
          0, Kratos::make_shared<Tetrahedra3D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),
      mTotalLagrangianElement3D6N(
          0, Kratos::make_shared<Prism3D6<Node<3>>>(
                 Element::GeometryType::PointsArrayType(6))),
      mTotalLagrangianElement3D8N(
          0, Kratos::make_shared<Hexahedra3D8<Node<3>>>(
                 Element::GeometryType::PointsArrayType(8))),
      mTotalLagrangianElement3D10N(
          0, Kratos::make_shared<Tetrahedra3D10<Node<3>>>(
                 Element::GeometryType::PointsArrayType(10))),
      mTotalLagrangianElement3D15N(
          0, Kratos::make_shared<Prism3D15<Node<3>>>(
                 Element::GeometryType::PointsArrayType(15))),
      mTotalLagrangianElement3D20N(
          0, Kratos::make_shared<Hexahedra3D20<Node<3>>>(
                 Element::GeometryType::PointsArrayType(20))),
      mTotalLagrangianElement3D27N(
          0, Kratos::make_shared<Hexahedra3D27<Node<3>>>(
                 Element::GeometryType::PointsArrayType(27))),

      mUpdatedTotalLagrangianElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mUpdatedTotalLagrangianElement2D4N(
          0, Kratos::make_shared<Quadrilateral2D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),
      mUpdatedTotalLagrangianElement2D6N(
          0, Kratos::make_shared<Triangle2D6<Node<3>>>(
                 Element::GeometryType::PointsArrayType(6))),
      mUpdatedTotalLagrangianElement2D8N(
          0, Kratos::make_shared<Quadrilateral2D8<Node<3>>>(
                 Element::GeometryType::PointsArrayType(8))),
      mUpdatedTotalLagrangianElement2D9N(
          0, Kratos::make_shared<Quadrilateral2D9<Node<3>>>(
                 Element::GeometryType::PointsArrayType(9))),
      mUpdatedTotalLagrangianElement3D4N(
          0, Kratos::make_shared<Tetrahedra3D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),
      mUpdatedTotalLagrangianElement3D6N(
          0, Kratos::make_shared<Prism3D6<Node<3>>>(
                 Element::GeometryType::PointsArrayType(6))),
      mUpdatedTotalLagrangianElement3D8N(
          0, Kratos::make_shared<Hexahedra3D8<Node<3>>>(
                 Element::GeometryType::PointsArrayType(8))),
      mUpdatedTotalLagrangianElement3D10N(
          0, Kratos::make_shared<Tetrahedra3D10<Node<3>>>(
                 Element::GeometryType::PointsArrayType(10))),
      mUpdatedTotalLagrangianElement3D15N(
          0, Kratos::make_shared<Prism3D15<Node<3>>>(
                 Element::GeometryType::PointsArrayType(15))),
      mUpdatedTotalLagrangianElement3D20N(
          0, Kratos::make_shared<Hexahedra3D20<Node<3>>>(
                 Element::GeometryType::PointsArrayType(20))),
      mUpdatedTotalLagrangianElement3D27N(
          0, Kratos::make_shared<Hexahedra3D27<Node<3>>>(
                 Element::GeometryType::PointsArrayType(27))),
      mUpdatedTotalLagrangianUPElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),

      mUpdatedLagrangianElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mUpdatedLagrangianElement2D4N(
          0, Kratos::make_shared<Quadrilateral2D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),
      mUpdatedLagrangianElement2D6N(
          0, Kratos::make_shared<Triangle2D6<Node<3>>>(
                 Element::GeometryType::PointsArrayType(6))),
      mUpdatedLagrangianElement2D8N(
          0, Kratos::make_shared<Quadrilateral2D8<Node<3>>>(
                 Element::GeometryType::PointsArrayType(8))),
      mUpdatedLagrangianElement2D9N(
          0, Kratos::make_shared<Quadrilateral2D9<Node<3>>>(
                 Element::GeometryType::PointsArrayType(9))),
      mUpdatedLagrangianElement3D4N(
          0, Kratos::make_shared<Tetrahedra3D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),
      mUpdatedLagrangianElement3D6N(
          0, Kratos::make_shared<Prism3D6<Node<3>>>(
                 Element::GeometryType::PointsArrayType(6))),
      mUpdatedLagrangianElement3D8N(
          0, Kratos::make_shared<Hexahedra3D8<Node<3>>>(
                 Element::GeometryType::PointsArrayType(8))),
      mUpdatedLagrangianElement3D10N(
          0, Kratos::make_shared<Tetrahedra3D10<Node<3>>>(
                 Element::GeometryType::PointsArrayType(10))),
      mUpdatedLagrangianElement3D15N(
          0, Kratos::make_shared<Prism3D15<Node<3>>>(
                 Element::GeometryType::PointsArrayType(15))),
      mUpdatedLagrangianElement3D20N(
          0, Kratos::make_shared<Hexahedra3D20<Node<3>>>(
                 Element::GeometryType::PointsArrayType(20))),
      mUpdatedLagrangianElement3D27N(
          0, Kratos::make_shared<Hexahedra3D27<Node<3>>>(
                 Element::GeometryType::PointsArrayType(27))),
      mAxisymUpdatedLagrangianElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mAxisymUpdatedLagrangianElement2D4N(
          0, Kratos::make_shared<Quadrilateral2D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),
      mAxisymUpdatedLagrangianElement2D6N(
          0, Kratos::make_shared<Triangle2D6<Node<3>>>(
                 Element::GeometryType::PointsArrayType(6))),
      mAxisymUpdatedLagrangianElement2D8N(
          0, Kratos::make_shared<Quadrilateral2D8<Node<3>>>(
                 Element::GeometryType::PointsArrayType(8))),
      mAxisymUpdatedLagrangianElement2D9N(
          0, Kratos::make_shared<Quadrilateral2D9<Node<3>>>(
                 Element::GeometryType::PointsArrayType(9))),

      mUpdatedLagrangianVElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mUpdatedLagrangianVElement2D4N(
          0, Kratos::make_shared<Quadrilateral2D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),
      mUpdatedLagrangianVElement2D6N(
          0, Kratos::make_shared<Triangle2D6<Node<3>>>(
                 Element::GeometryType::PointsArrayType(6))),
      mUpdatedLagrangianVElement2D8N(
          0, Kratos::make_shared<Quadrilateral2D8<Node<3>>>(
                 Element::GeometryType::PointsArrayType(8))),
      mUpdatedLagrangianVElement2D9N(
          0, Kratos::make_shared<Quadrilateral2D9<Node<3>>>(
                 Element::GeometryType::PointsArrayType(9))),
      mUpdatedLagrangianVElement3D4N(
          0, Kratos::make_shared<Tetrahedra3D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),
      mUpdatedLagrangianVElement3D6N(
          0, Kratos::make_shared<Prism3D6<Node<3>>>(
                 Element::GeometryType::PointsArrayType(6))),
      mUpdatedLagrangianVElement3D8N(
          0, Kratos::make_shared<Hexahedra3D8<Node<3>>>(
                 Element::GeometryType::PointsArrayType(8))),
      mUpdatedLagrangianVElement3D10N(
          0, Kratos::make_shared<Tetrahedra3D10<Node<3>>>(
                 Element::GeometryType::PointsArrayType(10))),
      mUpdatedLagrangianVElement3D15N(
          0, Kratos::make_shared<Prism3D15<Node<3>>>(
                 Element::GeometryType::PointsArrayType(15))),
      mUpdatedLagrangianVElement3D20N(
          0, Kratos::make_shared<Hexahedra3D20<Node<3>>>(
                 Element::GeometryType::PointsArrayType(20))),
      mUpdatedLagrangianVElement3D27N(
          0, Kratos::make_shared<Hexahedra3D27<Node<3>>>(
                 Element::GeometryType::PointsArrayType(27))),

      mUpdatedLagrangianSegregatedVPElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mUpdatedLagrangianSegregatedVPElement3D4N(
          0, Kratos::make_shared<Tetrahedra3D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),

      mUpdatedLagrangianUPElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mAxisymUpdatedLagrangianUPElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mUpdatedLagrangianUPElement3D4N(
          0, Kratos::make_shared<Tetrahedra3D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),

      mUpdatedLagrangianUJElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mAxisymUpdatedLagrangianUJElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mUpdatedLagrangianUJElement3D4N(
          0, Kratos::make_shared<Tetrahedra3D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),

      mSmallDisplacementBeamElement3D2N(
          0, Kratos::make_shared<Line3D2<Node<3>>>(
                 Element::GeometryType::PointsArrayType(2))),
      mLargeDisplacementBeamElement3D2N(
          0, Kratos::make_shared<Line3D2<Node<3>>>(
                 Element::GeometryType::PointsArrayType(2))),
      mLargeDisplacementBeamElement3D3N(
          0, Kratos::make_shared<Line3D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mLargeDisplacementBeamEMCElement3D2N(
          0, Kratos::make_shared<Line3D2<Node<3>>>(
                 Element::GeometryType::PointsArrayType(2))),
      mLargeDisplacementBeamEMCElement3D3N(
          0, Kratos::make_shared<Line3D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mLargeDisplacementBeamSEMCElement3D2N(
          0, Kratos::make_shared<LineGaussLobatto3D2<Node<3>>>(
                 Element::GeometryType::PointsArrayType(2))),
      mGeometricallyExactRodElement3D2N(
          0, Kratos::make_shared<Line3D2<Node<3>>>(
                 Element::GeometryType::PointsArrayType(2))),
      mLargeDisplacementBeamElement2D2N(
          0, Kratos::make_shared<Line2D2<Node<3>>>(
                 Element::GeometryType::PointsArrayType(2))),

      mShellThickElement3D4N(
          0, Kratos::make_shared<Quadrilateral3D4<Node<3>>>(Element::GeometryType::PointsArrayType(4)), false),
      mShellThickCorotationalElement3D4N(
          0, Kratos::make_shared<Quadrilateral3D4<Node<3>>>(Element::GeometryType::PointsArrayType(4)), true),
      mShellThinElement3D3N(
          0, Kratos::make_shared<Triangle3D3<Node<3>>>(Element::GeometryType::PointsArrayType(3)), false),
      mShellThinCorotationalElement3D3N(
          0, Kratos::make_shared<Triangle3D3<Node<3>>>(Element::GeometryType::PointsArrayType(3)), true),


      mThermalElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mThermalElement2D4N(
          0, Kratos::make_shared<Quadrilateral2D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),
      mThermalElement2D6N(
          0, Kratos::make_shared<Triangle2D6<Node<3>>>(
                 Element::GeometryType::PointsArrayType(6))),
      mThermalElement2D8N(
          0, Kratos::make_shared<Quadrilateral2D8<Node<3>>>(
                 Element::GeometryType::PointsArrayType(8))),
      mThermalElement2D9N(
          0, Kratos::make_shared<Quadrilateral2D9<Node<3>>>(
                 Element::GeometryType::PointsArrayType(9))),
      mThermalElement3D4N(
          0, Kratos::make_shared<Tetrahedra3D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),
      mThermalElement3D6N(
          0, Kratos::make_shared<Prism3D6<Node<3>>>(
                 Element::GeometryType::PointsArrayType(6))),
      mThermalElement3D8N(
          0, Kratos::make_shared<Hexahedra3D8<Node<3>>>(
                 Element::GeometryType::PointsArrayType(8))),
      mThermalElement3D10N(
          0, Kratos::make_shared<Tetrahedra3D10<Node<3>>>(
                 Element::GeometryType::PointsArrayType(10))),
      mThermalElement3D15N(
          0, Kratos::make_shared<Prism3D15<Node<3>>>(
                 Element::GeometryType::PointsArrayType(15))),
      mThermalElement3D20N(
          0, Kratos::make_shared<Hexahedra3D20<Node<3>>>(
                 Element::GeometryType::PointsArrayType(20))),
      mThermalElement3D27N(
          0, Kratos::make_shared<Hexahedra3D27<Node<3>>>(
                 Element::GeometryType::PointsArrayType(27))),

      mAxisymThermalElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mAxisymThermalElement2D4N(
          0, Kratos::make_shared<Quadrilateral2D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),
      mAxisymThermalElement2D6N(
          0, Kratos::make_shared<Triangle2D6<Node<3>>>(
                 Element::GeometryType::PointsArrayType(6))),
      mAxisymThermalElement2D8N(
          0, Kratos::make_shared<Quadrilateral2D8<Node<3>>>(
                 Element::GeometryType::PointsArrayType(8))),
      mAxisymThermalElement2D9N(
          0, Kratos::make_shared<Quadrilateral2D9<Node<3>>>(
                 Element::GeometryType::PointsArrayType(9))),

      mSoilThermalElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mSoilThermalElement3D4N(
          0, Kratos::make_shared<Tetrahedra3D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),
      mSoilAxisymThermalElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),

      mUpdatedLagrangianUwPElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mUpdatedLagrangianUwPElement3D4N(
          0, Kratos::make_shared<Tetrahedra3D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),

      mUpdatedLagrangianUwPStabElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mUpdatedLagrangianUwPStabElement3D4N(
          0, Kratos::make_shared<Tetrahedra3D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),
      mUpdatedLagrangianUwPStabElement2D9N(
          0, Kratos::make_shared<Quadrilateral2D9<Node<3>>>(
                 Element::GeometryType::PointsArrayType(9))),

      mUpdatedLagrangianUnsaturatedUwPElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mAxisymUpdatedLagrangianUnsaturatedUwPElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mUpdatedLagrangianUnsaturatedUJwPElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mAxisymUpdatedLagrangianUnsaturatedUJwPElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),

      mUpdatedLagrangianUWElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mUpdatedLagrangianUWwPElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mUpdatedLagrangianUWwPDMEElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mUpdatedLagrangianUJWwPElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mUpdatedLagrangianUJWwPHOElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mUpdatedLagrangianUJWwPDMEElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mUpdatedLagrangianUJWwPElement3D4N(
          0, Kratos::make_shared<Tetrahedra3D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),
      mUpdatedLagrangianUJWwPDMEElement3D4N(
          0, Kratos::make_shared<Tetrahedra3D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),
      mSmallDisplacementUWwPElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mSmallDisplacementUwPElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mAxisymSmallDisplacementUwPElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mAxisymSmallDisplacementUwPElement2D6N(
          0, Kratos::make_shared<Triangle2D6<Node<3>>>(
                 Element::GeometryType::PointsArrayType(6))),

      mAxisymUpdatedLagrangianUwPElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mAxisymUpdatedLagrangianUwPStabElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mAxisymUpdatedLagrangianUwPStabElement2D6N(
          0, Kratos::make_shared<Triangle2D6<Node<3>>>(
                 Element::GeometryType::PointsArrayType(6))),
      mAxisymUpdatedLagrangianUwPStabElement2D9N(
          0, Kratos::make_shared<Quadrilateral2D9<Node<3>>>(
                 Element::GeometryType::PointsArrayType(9))),

      // mixed elements
      mUpdatedLagrangianUJacobianElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mUpdatedLagrangianUJacobianElement3D4N(
          0, Kratos::make_shared<Tetrahedra3D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),
      mUpdatedLagrangianUJPElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mUpdatedLagrangianUPressureElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),

      mUpdatedLagrangianUJwPElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mUpdatedLagrangianUJwPElement3D4N(
          0, Kratos::make_shared<Tetrahedra3D4<Node<3>>>(
                 Element::GeometryType::PointsArrayType(4))),
      mUpdatedLagrangianUPwPElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),

      mAxisymUpdatedLagrangianUJacobianElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mAxisymUpdatedLagrangianUJwPElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mAxisymUpdatedLagrangianUJWwPElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mAxisymUpdatedLagrangianUJWwPDMEElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mAxisymUpdatedLagrangianUPressureElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),
      mAxisymUpdatedLagrangianUPwPElement2D3N(
          0, Kratos::make_shared<Triangle2D3<Node<3>>>(
                 Element::GeometryType::PointsArrayType(3))),

      mPointLoadCondition3D1N(
          0, Kratos::make_shared<Point3D<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(1))),
      mPointLoadCondition2D1N(
          0, Kratos::make_shared<Point2D<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(1))),
      mAxisymPointLoadCondition2D1N(
          0, Kratos::make_shared<Point2D<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(1))),

      mLineLoadCondition3D2N(
          0, Kratos::make_shared<Line3D2<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(2))),
      mLineLoadCondition3D3N(
          0, Kratos::make_shared<Line3D3<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(3))),
      mLineLoadCondition2D2N(
          0, Kratos::make_shared<Line2D2<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(2))),
      mLineLoadCondition2D3N(
          0, Kratos::make_shared<Line2D3<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(3))),
      mAxisymLineLoadCondition2D2N(
          0, Kratos::make_shared<Line2D2<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(2))),
      mAxisymLineLoadCondition2D3N(
          0, Kratos::make_shared<Line2D3<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(3))),

      mSurfaceLoadCondition3D3N(
          0, Kratos::make_shared<Triangle3D3<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(3))),
      mSurfaceLoadCondition3D4N(
          0, Kratos::make_shared<Quadrilateral3D4<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(4))),
      mSurfaceLoadCondition3D6N(
          0, Kratos::make_shared<Triangle3D6<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(6))),
      mSurfaceLoadCondition3D8N(
          0, Kratos::make_shared<Quadrilateral3D8<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(8))),
      mSurfaceLoadCondition3D9N(
          0, Kratos::make_shared<Quadrilateral3D9<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(9))),

      mPointMomentCondition3D1N(
          0, Kratos::make_shared<Point3D<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(1))),
      mPointMomentCondition2D1N(
          0, Kratos::make_shared<Point2D<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(1))),

      mLineMomentCondition3D2N(
          0, Kratos::make_shared<Line3D2<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(2))),
      mLineMomentCondition3D3N(
          0, Kratos::make_shared<Line3D3<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(3))),
      mLineMomentCondition2D2N(
          0, Kratos::make_shared<Line2D2<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(2))),
      mLineMomentCondition2D3N(
          0, Kratos::make_shared<Line2D3<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(3))),

      mSurfaceMomentCondition3D3N(
          0, Kratos::make_shared<Triangle3D3<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(3))),
      mSurfaceMomentCondition3D4N(
          0, Kratos::make_shared<Quadrilateral3D4<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(4))),
      mSurfaceMomentCondition3D6N(
          0, Kratos::make_shared<Triangle3D6<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(6))),
      mSurfaceMomentCondition3D8N(
          0, Kratos::make_shared<Quadrilateral3D8<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(8))),
      mSurfaceMomentCondition3D9N(
          0, Kratos::make_shared<Quadrilateral3D9<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(9))),

      mPointElasticCondition3D1N(
          0, Kratos::make_shared<Point3D<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(1))),
      mPointElasticCondition2D1N(
          0, Kratos::make_shared<Point2D<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(1))),
      mAxisymPointElasticCondition2D1N(
          0, Kratos::make_shared<Point2D<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(1))),

      mLineElasticCondition3D2N(
          0, Kratos::make_shared<Line3D2<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(2))),
      mLineElasticCondition3D3N(
          0, Kratos::make_shared<Line3D3<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(3))),
      mLineElasticCondition2D2N(
          0, Kratos::make_shared<Line2D2<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(2))),
      mLineElasticCondition2D3N(
          0, Kratos::make_shared<Line2D3<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(3))),
      mAxisymLineElasticCondition2D2N(
          0, Kratos::make_shared<Line2D2<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(2))),
      mAxisymLineElasticCondition2D3N(
          0, Kratos::make_shared<Line2D3<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(3))),

      mSurfaceElasticCondition3D3N(
          0, Kratos::make_shared<Triangle3D3<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(3))),
      mSurfaceElasticCondition3D4N(
          0, Kratos::make_shared<Quadrilateral3D4<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(4))),
      mSurfaceElasticCondition3D6N(
          0, Kratos::make_shared<Triangle3D6<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(6))),
      mSurfaceElasticCondition3D8N(
          0, Kratos::make_shared<Quadrilateral3D8<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(8))),
      mSurfaceElasticCondition3D9N(
          0, Kratos::make_shared<Quadrilateral3D9<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(9))),

      mPointHeatFluxCondition3D1N(
          0, Kratos::make_shared<Point3D<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(1))),
      mPointHeatFluxCondition2D1N(
          0, Kratos::make_shared<Point2D<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(1))),
      mAxisymPointHeatFluxCondition2D1N(
          0, Kratos::make_shared<Point2D<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(1))),

      mLineHeatFluxCondition3D2N(
          0, Kratos::make_shared<Line3D2<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(2))),
      mLineHeatFluxCondition3D3N(
          0, Kratos::make_shared<Line3D3<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(3))),
      mLineHeatFluxCondition2D2N(
          0, Kratos::make_shared<Line2D2<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(2))),
      mLineHeatFluxCondition2D3N(
          0, Kratos::make_shared<Line2D3<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(3))),
      mAxisymLineHeatFluxCondition2D2N(
          0, Kratos::make_shared<Line2D2<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(2))),
      mAxisymLineHeatFluxCondition2D3N(
          0, Kratos::make_shared<Line2D3<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(3))),

      mSurfaceHeatFluxCondition3D3N(
          0, Kratos::make_shared<Triangle3D3<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(3))),
      mSurfaceHeatFluxCondition3D4N(
          0, Kratos::make_shared<Quadrilateral3D4<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(4))),
      mSurfaceHeatFluxCondition3D6N(
          0, Kratos::make_shared<Triangle3D6<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(6))),
      mSurfaceHeatFluxCondition3D8N(
          0, Kratos::make_shared<Quadrilateral3D8<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(8))),
      mSurfaceHeatFluxCondition3D9N(
          0, Kratos::make_shared<Quadrilateral3D9<Node<3>>>(
                 Condition::GeometryType::PointsArrayType(9)))

{
}

void KratosSolidMechanicsApplication::Register()
{
    std::stringstream banner;

    banner << "            ___      _ _    _           \n"
           << "    KRATOS / __| ___| (_)__| |          \n"
           << "           \\__ \\/ _ \\ | / _` |          \n"
           << "           |___/\\___/_|_\\__,_| MECHANICS\n"
           << "Initialize KratosSolidMechanicsApplication...  " << std::endl;

    // mpi initialization
    int mpi_is_initialized = 0;
    int rank = -1;

#ifdef KRATOS_MPI

    MPI_Initialized(&mpi_is_initialized);

    if (mpi_is_initialized)
    {
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    }

#endif

    if (mpi_is_initialized)
    {
        if (rank == 0)
            KRATOS_INFO("") << banner.str();
    }
    else
    {
        KRATOS_INFO("") << banner.str();
    }

    // Register Variables (variables created in solid_mechanics_application_variables.cpp)

    // Generalized eigenvalue problem


    KRATOS_REGISTER_VARIABLE(BUILD_LEVEL)
    KRATOS_REGISTER_VARIABLE(EIGENVALUE_VECTOR)
    KRATOS_REGISTER_VARIABLE(EIGENVECTOR_MATRIX)

    //variables
    KRATOS_REGISTER_VARIABLE(BETA_MIXED)
    KRATOS_REGISTER_VARIABLE(ACCELERATE_TIME)

    KRATOS_REGISTER_VARIABLE(PRESSURE_VELOCITY)
    KRATOS_REGISTER_VARIABLE(PRESSURE_ACCELERATION)

    KRATOS_REGISTER_VARIABLE(SPECIFIC_HEAT_SOLID)
    KRATOS_REGISTER_VARIABLE(SPECIFIC_HEAT_WATER)
    KRATOS_REGISTER_VARIABLE(SPECIFIC_HEAT_EQUIVALENT)

    KRATOS_REGISTER_VARIABLE(THERMAL_CONDUCTIVITY_SOLID)
    KRATOS_REGISTER_VARIABLE(THERMAL_CONDUCTIVITY_WATER)
    KRATOS_REGISTER_VARIABLE(THERMAL_CONDUCTIVITY_EQUIVALENT)


    //solution
    KRATOS_REGISTER_VARIABLE(WRITE_ID)
    KRATOS_REGISTER_VARIABLE(IS_AXISYMMETRIC)

    //geometrical
    KRATOS_REGISTER_VARIABLE(GEOMETRIC_STIFFNESS)

    //beam cross section
    //KRATOS_REGISTER_VARIABLE( BEAM_CROSS_SECTION )
    KRATOS_REGISTER_VARIABLE(CROSS_SECTION_AREA)
    KRATOS_REGISTER_VARIABLE(CROSS_SECTION_RADIUS)
    KRATOS_REGISTER_VARIABLE(CROSS_SECTION_SIDES)

    //shell cross section
    KRATOS_REGISTER_VARIABLE(SHELL_CROSS_SECTION)
    KRATOS_REGISTER_VARIABLE(SHELL_CROSS_SECTION_OUTPUT_PLY_ID)
    KRATOS_REGISTER_VARIABLE(SHELL_CROSS_SECTION_OUTPUT_PLY_LOCATION)

    //shell generalized variables
    KRATOS_REGISTER_VARIABLE(SHELL_STRAIN)
    KRATOS_REGISTER_VARIABLE(SHELL_STRAIN_GLOBAL)
    KRATOS_REGISTER_VARIABLE(SHELL_CURVATURE)
    KRATOS_REGISTER_VARIABLE(SHELL_CURVATURE_GLOBAL)
    KRATOS_REGISTER_VARIABLE(SHELL_FORCE)
    KRATOS_REGISTER_VARIABLE(SHELL_FORCE_GLOBAL)
    KRATOS_REGISTER_VARIABLE(SHELL_MOMENT)
    KRATOS_REGISTER_VARIABLE(SHELL_MOMENT_GLOBAL)

    //nodal load variable (legacy)
    KRATOS_REGISTER_3D_VARIABLE_WITH_COMPONENTS(POINT_LOAD)

    //force loads
    KRATOS_REGISTER_3D_VARIABLE_WITH_COMPONENTS(FORCE_LOAD)
    KRATOS_REGISTER_VARIABLE(FORCE_LOAD_VECTOR)

    KRATOS_REGISTER_3D_VARIABLE_WITH_COMPONENTS(FOLLOWER_FORCE_LOAD)
    KRATOS_REGISTER_VARIABLE(FOLLOWER_FORCE_LOAD_VECTOR)

    //moment loads
    KRATOS_REGISTER_3D_VARIABLE_WITH_COMPONENTS(MOMENT_LOAD)
    KRATOS_REGISTER_VARIABLE(MOMENT_LOAD_VECTOR)

    //elastic loads
    KRATOS_REGISTER_3D_VARIABLE_WITH_COMPONENTS(ELASTIC_LOAD)
    KRATOS_REGISTER_VARIABLE(ELASTIC_LOAD_VECTOR)

    //force pressure
    KRATOS_REGISTER_VARIABLE(POSITIVE_FACE_PRESSURE_VECTOR)
    KRATOS_REGISTER_VARIABLE(NEGATIVE_FACE_PRESSURE_VECTOR)

    //moment pressures
    KRATOS_REGISTER_VARIABLE(PLANE_MOMENT_LOAD)
    KRATOS_REGISTER_VARIABLE(PLANE_MOMENT_LOAD_VECTOR)

    //elastic pressures
    KRATOS_REGISTER_VARIABLE(BALLAST_COEFFICIENT)
    KRATOS_REGISTER_VARIABLE(BALLAST_COEFFICIENT_VECTOR)

    //heat fluxes
    KRATOS_REGISTER_VARIABLE(HEAT_FLUX)
    KRATOS_REGISTER_VARIABLE(HEAT_FLUX_VECTOR)
    KRATOS_REGISTER_VARIABLE(HEAT_TRANSFER_COEFFICIENT)
    KRATOS_REGISTER_VARIABLE(ROOM_TEMPERATURE)

    //element
    KRATOS_REGISTER_VARIABLE(VON_MISES_STRESS)

    //nodal dofs
    KRATOS_REGISTER_3D_VARIABLE_WITH_COMPONENTS(WATER_DISPLACEMENT_REACTION)
    KRATOS_REGISTER_3D_VARIABLE_WITH_COMPONENTS(WATER_VELOCITY_REACTION)
    KRATOS_REGISTER_3D_VARIABLE_WITH_COMPONENTS(WATER_ACCELERATION_REACTION)
    KRATOS_REGISTER_3D_VARIABLE_WITH_COMPONENTS(DISPLACEMENT_REACTION)
    KRATOS_REGISTER_3D_VARIABLE_WITH_COMPONENTS(ROTATION_REACTION)
    KRATOS_REGISTER_3D_VARIABLE_WITH_COMPONENTS(VELOCITY_REACTION)
    KRATOS_REGISTER_VARIABLE(PRESSURE_REACTION)
    KRATOS_REGISTER_VARIABLE(TEMPERATURE_REACTION)

    //reading beam section properties
    KRATOS_REGISTER_VARIABLE(SECTION_HEIGHT)
    KRATOS_REGISTER_VARIABLE(SECTION_WIDTH)
    KRATOS_REGISTER_VARIABLE(INERTIA_X)
    KRATOS_REGISTER_VARIABLE(INERTIA_Y)
    KRATOS_REGISTER_VARIABLE(SECTION_SIZE)

    KRATOS_REGISTER_VARIABLE(YOUNGxAREA)
    KRATOS_REGISTER_VARIABLE(YOUNGxINERTIA_X)
    KRATOS_REGISTER_VARIABLE(YOUNGxINERTIA_Y)
    KRATOS_REGISTER_VARIABLE(SHEARxREDUCED_AREA)
    KRATOS_REGISTER_VARIABLE(SHEARxPOLAR_INERTIA)

    //boundary definition
    KRATOS_REGISTER_VARIABLE(MAIN_ELEMENT)

    //thermal properties
    KRATOS_REGISTER_VARIABLE(SPECIFIC_HEAT_CAPACITY)
    KRATOS_REGISTER_VARIABLE(THERMAL_CONDUCTIVITY)
    KRATOS_REGISTER_VARIABLE(HEAT_SOURCE)

    KRATOS_REGISTER_VARIABLE(TEMPERATURE_DEPENDENT)
    KRATOS_REGISTER_VARIABLE(SPECIFIC_HEAT_CAPACITY_A)
    KRATOS_REGISTER_VARIABLE(SPECIFIC_HEAT_CAPACITY_B)
    KRATOS_REGISTER_VARIABLE(THERMAL_CONDUCTIVITY_A)
    KRATOS_REGISTER_VARIABLE(THERMAL_CONDUCTIVITY_B)

    //pfem solid variables
    KRATOS_REGISTER_VARIABLE(KOZENY_CARMAN)
    KRATOS_REGISTER_VARIABLE(PERMEABILITY_X)
    KRATOS_REGISTER_VARIABLE(PERMEABILITY_Y)
    KRATOS_REGISTER_VARIABLE(PERMEABILITY_Z)
    KRATOS_REGISTER_VARIABLE(PERMEABILITY_TENSOR)
    KRATOS_REGISTER_VARIABLE(ROTATE_PERMEABILITY)
    KRATOS_REGISTER_VARIABLE(JACOBIAN)
    KRATOS_REGISTER_VARIABLE(REACTION_JACOBIAN)
    KRATOS_REGISTER_VARIABLE(WATER_BULK_MODULUS)
    KRATOS_REGISTER_VARIABLE(INITIAL_POROSITY)
    KRATOS_REGISTER_VARIABLE(VOID_RATIO)
    KRATOS_REGISTER_VARIABLE(STABILIZATION_FACTOR_J)
    KRATOS_REGISTER_VARIABLE(STABILIZATION_FACTOR_P)
    KRATOS_REGISTER_VARIABLE(STABILIZATION_FACTOR_WP)
    KRATOS_REGISTER_VARIABLE(RETENTION_CURVE_A)
    KRATOS_REGISTER_VARIABLE(RETENTION_CURVE_B)
    KRATOS_REGISTER_VARIABLE(RETENTION_CURVE_C)
    KRATOS_REGISTER_VARIABLE(RETENTION_CURVE_D)
    KRATOS_REGISTER_VARIABLE(DEGREE_OF_SATURATION)
    KRATOS_REGISTER_VARIABLE(DEGREE_OF_SATURATION_SAT)
    KRATOS_REGISTER_VARIABLE(DEGREE_OF_SATURATION_RES)
    KRATOS_REGISTER_VARIABLE(DEGREE_OF_SATURATION_EFF)
    KRATOS_REGISTER_VARIABLE(WATER_CONTENT)
    KRATOS_REGISTER_VARIABLE(UNSAT_TERM)

    KRATOS_REGISTER_VARIABLE(WATER_PRESSURE_VELOCITY)
    KRATOS_REGISTER_VARIABLE(REACTION_WATER_PRESSURE_VELOCITY)
    KRATOS_REGISTER_VARIABLE(REACTION_WATER_PRESSURE_ACCELERATION)
    KRATOS_REGISTER_VARIABLE(TOTAL_CAUCHY_STRESS)
    KRATOS_REGISTER_3D_VARIABLE_WITH_COMPONENTS(DARCY_FLOW)
    //Register Elements

    //Register solids
    KRATOS_REGISTER_ELEMENT("LinearSolidElement2D3N", mLinearSolidElement2D3N)
    KRATOS_REGISTER_ELEMENT("LinearSolidElement2D4N", mLinearSolidElement2D4N)
    KRATOS_REGISTER_ELEMENT("LinearSolidElement3D4N", mLinearSolidElement3D4N)
    KRATOS_REGISTER_ELEMENT("LinearSolidElement3D8N", mLinearSolidElement3D8N)

    //Register small displacement elements
    KRATOS_REGISTER_ELEMENT("SmallDisplacementSolidElement2D3N", mSmallDisplacementElement2D3N)
    KRATOS_REGISTER_ELEMENT("SmallDisplacementSolidElement2D4N", mSmallDisplacementElement2D4N)
    KRATOS_REGISTER_ELEMENT("SmallDisplacementSolidElement2D6N", mSmallDisplacementElement2D6N)
    KRATOS_REGISTER_ELEMENT("SmallDisplacementSolidElement2D8N", mSmallDisplacementElement2D8N)
    KRATOS_REGISTER_ELEMENT("SmallDisplacementSolidElement2D9N", mSmallDisplacementElement2D9N)
    KRATOS_REGISTER_ELEMENT("SmallDisplacementSolidElement3D4N", mSmallDisplacementElement3D4N)
    KRATOS_REGISTER_ELEMENT("SmallDisplacementSolidElement3D6N", mSmallDisplacementElement3D6N)
    KRATOS_REGISTER_ELEMENT("SmallDisplacementSolidElement3D8N", mSmallDisplacementElement3D8N)
    KRATOS_REGISTER_ELEMENT("SmallDisplacementSolidElement3D10N", mSmallDisplacementElement3D10N)
    KRATOS_REGISTER_ELEMENT("SmallDisplacementSolidElement3D15N", mSmallDisplacementElement3D15N)
    KRATOS_REGISTER_ELEMENT("SmallDisplacementSolidElement3D20N", mSmallDisplacementElement3D20N)
    KRATOS_REGISTER_ELEMENT("SmallDisplacementSolidElement3D27N", mSmallDisplacementElement3D27N)

    //B-bar
    KRATOS_REGISTER_ELEMENT("SmallDisplacementBbarSolidElement2D3N", mSmallDisplacementBbarElement2D3N)
    KRATOS_REGISTER_ELEMENT("SmallDisplacementBbarSolidElement2D4N", mSmallDisplacementBbarElement2D4N)
    KRATOS_REGISTER_ELEMENT("SmallDisplacementBbarSolidElement2D6N", mSmallDisplacementBbarElement2D6N)
    KRATOS_REGISTER_ELEMENT("SmallDisplacementBbarSolidElement2D8N", mSmallDisplacementBbarElement2D8N)
    KRATOS_REGISTER_ELEMENT("SmallDisplacementBbarSolidElement2D9N", mSmallDisplacementBbarElement2D9N)
    KRATOS_REGISTER_ELEMENT("SmallDisplacementBbarSolidElement3D4N", mSmallDisplacementBbarElement3D4N)
    KRATOS_REGISTER_ELEMENT("SmallDisplacementBbarSolidElement3D6N", mSmallDisplacementBbarElement3D6N)
    KRATOS_REGISTER_ELEMENT("SmallDisplacementBbarSolidElement3D8N", mSmallDisplacementBbarElement3D8N)
    KRATOS_REGISTER_ELEMENT("SmallDisplacementBbarSolidElement3D10N", mSmallDisplacementBbarElement3D10N)
    KRATOS_REGISTER_ELEMENT("SmallDisplacementBbarSolidElement3D15N", mSmallDisplacementBbarElement3D15N)
    KRATOS_REGISTER_ELEMENT("SmallDisplacementBbarSolidElement3D20N", mSmallDisplacementBbarElement3D20N)
    KRATOS_REGISTER_ELEMENT("SmallDisplacementBbarSolidElement3D27N", mSmallDisplacementBbarElement3D27N)

    KRATOS_REGISTER_ELEMENT("AxisymSmallDisplacementSolidElement2D3N", mAxisymSmallDisplacementElement2D3N)
    KRATOS_REGISTER_ELEMENT("AxisymSmallDisplacementSolidElement2D4N", mAxisymSmallDisplacementElement2D4N)
    KRATOS_REGISTER_ELEMENT("AxisymSmallDisplacementSolidElement2D6N", mAxisymSmallDisplacementElement2D6N)
    KRATOS_REGISTER_ELEMENT("AxisymSmallDisplacementSolidElement2D8N", mAxisymSmallDisplacementElement2D8N)
    KRATOS_REGISTER_ELEMENT("AxisymSmallDisplacementSolidElement2D9N", mAxisymSmallDisplacementElement2D9N)

    //Register large displacement elements

    //Register total lagrangian
    KRATOS_REGISTER_ELEMENT("TotalLagrangianSolidElement2D3N", mTotalLagrangianElement2D3N)
    KRATOS_REGISTER_ELEMENT("TotalLagrangianSolidElement2D4N", mTotalLagrangianElement2D4N)
    KRATOS_REGISTER_ELEMENT("TotalLagrangianSolidElement2D6N", mTotalLagrangianElement2D6N)
    KRATOS_REGISTER_ELEMENT("TotalLagrangianSolidElement2D8N", mTotalLagrangianElement2D8N)
    KRATOS_REGISTER_ELEMENT("TotalLagrangianSolidElement2D9N", mTotalLagrangianElement2D9N)
    KRATOS_REGISTER_ELEMENT("TotalLagrangianSolidElement3D4N", mTotalLagrangianElement3D4N)
    KRATOS_REGISTER_ELEMENT("TotalLagrangianSolidElement3D6N", mTotalLagrangianElement3D6N)
    KRATOS_REGISTER_ELEMENT("TotalLagrangianSolidElement3D8N", mTotalLagrangianElement3D8N)
    KRATOS_REGISTER_ELEMENT("TotalLagrangianSolidElement3D10N", mTotalLagrangianElement3D10N)
    KRATOS_REGISTER_ELEMENT("TotalLagrangianSolidElement3D15N", mTotalLagrangianElement3D15N)
    KRATOS_REGISTER_ELEMENT("TotalLagrangianSolidElement3D20N", mTotalLagrangianElement3D20N)
    KRATOS_REGISTER_ELEMENT("TotalLagrangianSolidElement3D27N", mTotalLagrangianElement3D27N)

    //Register updated total lagrangian
    KRATOS_REGISTER_ELEMENT("UpdatedTotalLagrangianSolidElement2D3N", mUpdatedTotalLagrangianElement2D3N)
    KRATOS_REGISTER_ELEMENT("UpdatedTotalLagrangianSolidElement2D4N", mUpdatedTotalLagrangianElement2D4N)
    KRATOS_REGISTER_ELEMENT("UpdatedTotalLagrangianSolidElement2D6N", mUpdatedTotalLagrangianElement2D6N)
    KRATOS_REGISTER_ELEMENT("UpdatedTotalLagrangianSolidElement2D8N", mUpdatedTotalLagrangianElement2D8N)
    KRATOS_REGISTER_ELEMENT("UpdatedTotalLagrangianSolidElement2D9N", mUpdatedTotalLagrangianElement2D9N)
    KRATOS_REGISTER_ELEMENT("UpdatedTotalLagrangianSolidElement3D4N", mUpdatedTotalLagrangianElement3D4N)
    KRATOS_REGISTER_ELEMENT("UpdatedTotalLagrangianSolidElement3D6N", mUpdatedTotalLagrangianElement3D6N)
    KRATOS_REGISTER_ELEMENT("UpdatedTotalLagrangianSolidElement3D8N", mUpdatedTotalLagrangianElement3D8N)
    KRATOS_REGISTER_ELEMENT("UpdatedTotalLagrangianSolidElement3D10N", mUpdatedTotalLagrangianElement3D10N)
    KRATOS_REGISTER_ELEMENT("UpdatedTotalLagrangianSolidElement3D15N", mUpdatedTotalLagrangianElement3D15N)
    KRATOS_REGISTER_ELEMENT("UpdatedTotalLagrangianSolidElement3D20N", mUpdatedTotalLagrangianElement3D20N)
    KRATOS_REGISTER_ELEMENT("UpdatedTotalLagrangianSolidElement3D27N", mUpdatedTotalLagrangianElement3D27N)
    KRATOS_REGISTER_ELEMENT("UpdatedTotalLagrangianUPElement2D3N", mUpdatedTotalLagrangianUPElement2D3N)

    //Register updated lagrangian
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianSolidElement2D3N", mUpdatedLagrangianElement2D3N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianSolidElement2D4N", mUpdatedLagrangianElement2D4N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianSolidElement2D6N", mUpdatedLagrangianElement2D6N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianSolidElement2D8N", mUpdatedLagrangianElement2D8N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianSolidElement2D9N", mUpdatedLagrangianElement2D9N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianSolidElement3D4N", mUpdatedLagrangianElement3D4N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianSolidElement3D6N", mUpdatedLagrangianElement3D6N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianSolidElement3D8N", mUpdatedLagrangianElement3D8N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianSolidElement3D10N", mUpdatedLagrangianElement3D10N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianSolidElement3D15N", mUpdatedLagrangianElement3D15N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianSolidElement3D20N", mUpdatedLagrangianElement3D20N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianSolidElement3D27N", mUpdatedLagrangianElement3D27N)

    KRATOS_REGISTER_ELEMENT("AxisymUpdatedLagrangianSolidElement2D3N", mAxisymUpdatedLagrangianElement2D3N)
    KRATOS_REGISTER_ELEMENT("AxisymUpdatedLagrangianSolidElement2D4N", mAxisymUpdatedLagrangianElement2D4N)
    KRATOS_REGISTER_ELEMENT("AxisymUpdatedLagrangianSolidElement2D6N", mAxisymUpdatedLagrangianElement2D6N)
    KRATOS_REGISTER_ELEMENT("AxisymUpdatedLagrangianSolidElement2D8N", mAxisymUpdatedLagrangianElement2D8N)
    KRATOS_REGISTER_ELEMENT("AxisymUpdatedLagrangianSolidElement2D9N", mAxisymUpdatedLagrangianElement2D9N)

    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianVElement2D3N", mUpdatedLagrangianVElement2D3N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianVElement2D4N", mUpdatedLagrangianVElement2D4N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianVElement2D6N", mUpdatedLagrangianVElement2D6N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianVElement2D8N", mUpdatedLagrangianVElement2D8N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianVElement2D9N", mUpdatedLagrangianVElement2D9N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianVElement3D4N", mUpdatedLagrangianVElement3D4N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianVElement3D6N", mUpdatedLagrangianVElement3D6N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianVElement3D8N", mUpdatedLagrangianVElement3D8N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianVElement3D10N", mUpdatedLagrangianVElement3D10N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianVElement3D15N", mUpdatedLagrangianVElement3D15N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianVElement3D20N", mUpdatedLagrangianVElement3D20N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianVElement3D27N", mUpdatedLagrangianVElement3D27N)

    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianSegregatedVPElement2D3N", mUpdatedLagrangianSegregatedVPElement2D3N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianSegregatedVPElement3D4N", mUpdatedLagrangianSegregatedVPElement3D4N)

    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianUPElement2D3N", mUpdatedLagrangianUPElement2D3N)
    KRATOS_REGISTER_ELEMENT("AxisymUpdatedLagrangianUPElement2D3N", mAxisymUpdatedLagrangianUPElement2D3N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianUPElement3D4N", mUpdatedLagrangianUPElement3D4N)

    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianUJElement2D3N", mUpdatedLagrangianUJElement2D3N)
    KRATOS_REGISTER_ELEMENT("AxisymUpdatedLagrangianUJElement2D3N", mAxisymUpdatedLagrangianUJElement2D3N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianUJElement3D4N", mUpdatedLagrangianUJElement3D4N)

    //Register beams
    KRATOS_REGISTER_ELEMENT("SmallDisplacementBeamElement3D2N", mSmallDisplacementBeamElement3D2N)
    KRATOS_REGISTER_ELEMENT("LargeDisplacementBeamElement3D2N", mLargeDisplacementBeamElement3D2N)
    KRATOS_REGISTER_ELEMENT("LargeDisplacementBeamElement3D3N", mLargeDisplacementBeamElement3D3N)
    KRATOS_REGISTER_ELEMENT("LargeDisplacementBeamEMCElement3D2N", mLargeDisplacementBeamEMCElement3D2N)
    KRATOS_REGISTER_ELEMENT("LargeDisplacementBeamEMCElement3D3N", mLargeDisplacementBeamEMCElement3D3N)
    KRATOS_REGISTER_ELEMENT("LargeDisplacementBeamSEMCElement3D2N", mLargeDisplacementBeamSEMCElement3D2N)
    KRATOS_REGISTER_ELEMENT("GeometricallyExactRodElement3D2N", mGeometricallyExactRodElement3D2N)
    KRATOS_REGISTER_ELEMENT("LargeDisplacementBeamElement2D2N", mLargeDisplacementBeamElement2D2N)

    //Register shells
    KRATOS_REGISTER_ELEMENT("ThickShellElement3D4N", mShellThickElement3D4N)
    KRATOS_REGISTER_ELEMENT("ThickShellCorotationalElement3D4N", mShellThickCorotationalElement3D4N)
    KRATOS_REGISTER_ELEMENT("ThinShellElement3D3N", mShellThinElement3D3N)
    KRATOS_REGISTER_ELEMENT("ThinShellCorotationalElement3D3N", mShellThinCorotationalElement3D3N)

    //Register thermal elements
    KRATOS_REGISTER_ELEMENT("ThermalElement2D3N", mThermalElement2D3N)
    KRATOS_REGISTER_ELEMENT("ThermalElement2D4N", mThermalElement2D4N)
    KRATOS_REGISTER_ELEMENT("ThermalElement2D6N", mThermalElement2D6N)
    KRATOS_REGISTER_ELEMENT("ThermalElement2D8N", mThermalElement2D8N)
    KRATOS_REGISTER_ELEMENT("ThermalElement2D9N", mThermalElement2D9N)
    KRATOS_REGISTER_ELEMENT("ThermalElement3D4N", mThermalElement3D4N)
    KRATOS_REGISTER_ELEMENT("ThermalElement3D6N", mThermalElement3D6N)
    KRATOS_REGISTER_ELEMENT("ThermalElement3D8N", mThermalElement3D8N)
    KRATOS_REGISTER_ELEMENT("ThermalElement3D10N", mThermalElement3D10N)
    KRATOS_REGISTER_ELEMENT("ThermalElement3D15N", mThermalElement3D15N)
    KRATOS_REGISTER_ELEMENT("ThermalElement3D20N", mThermalElement3D20N)
    KRATOS_REGISTER_ELEMENT("ThermalElement3D27N", mThermalElement3D27N)

    KRATOS_REGISTER_ELEMENT("AxisymThermalElement2D3N", mAxisymThermalElement2D3N)
    KRATOS_REGISTER_ELEMENT("AxisymThermalElement2D4N", mAxisymThermalElement2D4N)
    KRATOS_REGISTER_ELEMENT("AxisymThermalElement2D6N", mAxisymThermalElement2D6N)
    KRATOS_REGISTER_ELEMENT("AxisymThermalElement2D8N", mAxisymThermalElement2D8N)
    KRATOS_REGISTER_ELEMENT("AxisymThermalElement2D9N", mAxisymThermalElement2D9N)

    KRATOS_REGISTER_ELEMENT("SoilThermalElement2D3N", mSoilThermalElement2D3N)
    KRATOS_REGISTER_ELEMENT("SoilThermalElement3D4N", mSoilThermalElement3D4N)
    KRATOS_REGISTER_ELEMENT("SoilAxisymThermalElement2D3N", mSoilAxisymThermalElement2D3N)

    //Register hidro-mechanical elements
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianUwPElement2D3N", mUpdatedLagrangianUwPElement2D3N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianUwPElement3D4N", mUpdatedLagrangianUwPElement3D4N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianUnsaturatedUwPElement2D3N", mUpdatedLagrangianUnsaturatedUwPElement2D3N)
    KRATOS_REGISTER_ELEMENT("AxisymUpdatedLagrangianUnsaturatedUwPElement2D3N", mAxisymUpdatedLagrangianUnsaturatedUwPElement2D3N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianUnsaturatedUJwPElement2D3N", mUpdatedLagrangianUnsaturatedUJwPElement2D3N)
    KRATOS_REGISTER_ELEMENT("AxisymUpdatedLagrangianUnsaturatedUJwPElement2D3N", mAxisymUpdatedLagrangianUnsaturatedUJwPElement2D3N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianUwPStabElement2D3N", mUpdatedLagrangianUwPStabElement2D3N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianUwPStabElement3D4N", mUpdatedLagrangianUwPStabElement3D4N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianUwPStabElement2D9N", mUpdatedLagrangianUwPStabElement2D9N)

    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianUWElement2D3N", mUpdatedLagrangianUWElement2D3N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianUWwPElement2D3N", mUpdatedLagrangianUWwPElement2D3N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianUWwPDMEElement2D3N", mUpdatedLagrangianUWwPDMEElement2D3N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianUJWwPElement2D3N", mUpdatedLagrangianUJWwPElement2D3N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianUJWwPHOElement2D3N", mUpdatedLagrangianUJWwPHOElement2D3N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianUJWwPDMEElement2D3N", mUpdatedLagrangianUJWwPDMEElement2D3N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianUJWwPElement3D4N", mUpdatedLagrangianUJWwPElement3D4N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianUJWwPDMEElement3D4N", mUpdatedLagrangianUJWwPDMEElement3D4N)
    KRATOS_REGISTER_ELEMENT("SmallDisplacementUWwPElement2D3N", mSmallDisplacementUWwPElement2D3N)
    KRATOS_REGISTER_ELEMENT("SmallDisplacementUwPElement2D3N", mSmallDisplacementUwPElement2D3N)
    KRATOS_REGISTER_ELEMENT("AxisymSmallDisplacementUwPElement2D3N", mAxisymSmallDisplacementUwPElement2D3N)
    KRATOS_REGISTER_ELEMENT("AxisymSmallDisplacementUwPElement2D6N", mAxisymSmallDisplacementUwPElement2D6N)

    KRATOS_REGISTER_ELEMENT("AxisymUpdatedLagrangianUwPElement2D3N", mAxisymUpdatedLagrangianUwPElement2D3N)
    KRATOS_REGISTER_ELEMENT("AxisymUpdatedLagrangianUwPStabElement2D3N", mAxisymUpdatedLagrangianUwPStabElement2D3N)
    KRATOS_REGISTER_ELEMENT("AxisymUpdatedLagrangianUwPStabElement2D6N", mAxisymUpdatedLagrangianUwPStabElement2D6N)
    KRATOS_REGISTER_ELEMENT("AxisymUpdatedLagrangianUwPStabElement2D9N", mAxisymUpdatedLagrangianUwPStabElement2D9N)

    // One-Phase hidro-mechanical elements
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianUJacobianElement2D3N", mUpdatedLagrangianUJacobianElement2D3N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianUJacobianElement3D4N", mUpdatedLagrangianUJacobianElement3D4N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianUJPElement2D3N", mUpdatedLagrangianUJPElement2D3N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianUPressureElement2D3N", mUpdatedLagrangianUPressureElement2D3N)
    KRATOS_REGISTER_ELEMENT("AxisymUpdatedLagrangianUJacobianElement2D3N", mAxisymUpdatedLagrangianUJacobianElement2D3N)
    KRATOS_REGISTER_ELEMENT("AxisymUpdatedLagrangianUPressureElement2D3N", mAxisymUpdatedLagrangianUPressureElement2D3N)

    // Two-phase hidro-mechanical elements
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianUJwPElement2D3N", mUpdatedLagrangianUJwPElement2D3N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianUJwPElement3D4N", mUpdatedLagrangianUJwPElement3D4N)
    KRATOS_REGISTER_ELEMENT("UpdatedLagrangianUPwPElement2D3N", mUpdatedLagrangianUPwPElement2D3N)
    KRATOS_REGISTER_ELEMENT("AxisymUpdatedLagrangianUJwPElement2D3N", mAxisymUpdatedLagrangianUJwPElement2D3N)
    KRATOS_REGISTER_ELEMENT("AxisymUpdatedLagrangianUJWwPElement2D3N", mAxisymUpdatedLagrangianUJWwPElement2D3N)
    KRATOS_REGISTER_ELEMENT("AxisymUpdatedLagrangianUJWwPDMEElement2D3N", mAxisymUpdatedLagrangianUJWwPDMEElement2D3N)
    KRATOS_REGISTER_ELEMENT("AxisymUpdatedLagrangianUPwPElement2D3N", mAxisymUpdatedLagrangianUPwPElement2D3N)

    //Register Conditions
    KRATOS_REGISTER_CONDITION("PointLoad3D1N", mPointLoadCondition3D1N)
    KRATOS_REGISTER_CONDITION("PointLoad2D1N", mPointLoadCondition2D1N)
    KRATOS_REGISTER_CONDITION("AxisymPointLoad2D1N", mAxisymPointLoadCondition2D1N)

    KRATOS_REGISTER_CONDITION("LineLoad3D2N", mLineLoadCondition3D2N)
    KRATOS_REGISTER_CONDITION("LineLoad3D3N", mLineLoadCondition3D3N)
    KRATOS_REGISTER_CONDITION("LineLoad2D2N", mLineLoadCondition2D2N)
    KRATOS_REGISTER_CONDITION("LineLoad2D3N", mLineLoadCondition2D3N)
    KRATOS_REGISTER_CONDITION("AxisymLineLoad2D2N", mAxisymLineLoadCondition2D2N)
    KRATOS_REGISTER_CONDITION("AxisymLineLoad2D3N", mAxisymLineLoadCondition2D3N)

    KRATOS_REGISTER_CONDITION("SurfaceLoad3D3N", mSurfaceLoadCondition3D3N)
    KRATOS_REGISTER_CONDITION("SurfaceLoad3D4N", mSurfaceLoadCondition3D4N)
    KRATOS_REGISTER_CONDITION("SurfaceLoad3D6N", mSurfaceLoadCondition3D6N)
    KRATOS_REGISTER_CONDITION("SurfaceLoad3D8N", mSurfaceLoadCondition3D8N)
    KRATOS_REGISTER_CONDITION("SurfaceLoad3D9N", mSurfaceLoadCondition3D9N)

    KRATOS_REGISTER_CONDITION("PointMoment3D1N", mPointMomentCondition3D1N)
    KRATOS_REGISTER_CONDITION("PointMoment2D1N", mPointMomentCondition2D1N)

    KRATOS_REGISTER_CONDITION("LineMoment2D2N", mLineMomentCondition2D2N)
    KRATOS_REGISTER_CONDITION("LineMoment2D3N", mLineMomentCondition2D3N)
    KRATOS_REGISTER_CONDITION("LineMoment3D2N", mLineMomentCondition3D2N)
    KRATOS_REGISTER_CONDITION("LineMoment3D3N", mLineMomentCondition3D3N)

    KRATOS_REGISTER_CONDITION("SurfaceMoment3D3N", mSurfaceMomentCondition3D3N)
    KRATOS_REGISTER_CONDITION("SurfaceMoment3D4N", mSurfaceMomentCondition3D4N)
    KRATOS_REGISTER_CONDITION("SurfaceMoment3D6N", mSurfaceMomentCondition3D6N)
    KRATOS_REGISTER_CONDITION("SurfaceMoment3D8N", mSurfaceMomentCondition3D8N)
    KRATOS_REGISTER_CONDITION("SurfaceMoment3D9N", mSurfaceMomentCondition3D9N)

    KRATOS_REGISTER_CONDITION("PointSpring3D1N", mPointElasticCondition3D1N)
    KRATOS_REGISTER_CONDITION("PointSpring2D1N", mPointElasticCondition2D1N)
    KRATOS_REGISTER_CONDITION("AxisymPointSpring2D1N", mAxisymPointElasticCondition2D1N)

    KRATOS_REGISTER_CONDITION("LineSpring3D2N", mLineElasticCondition3D2N)
    KRATOS_REGISTER_CONDITION("LineSpring3D3N", mLineElasticCondition3D3N)
    KRATOS_REGISTER_CONDITION("LineSpring2D2N", mLineElasticCondition2D2N)
    KRATOS_REGISTER_CONDITION("LineSpring2D3N", mLineElasticCondition2D3N)
    KRATOS_REGISTER_CONDITION("AxisymLineSpring2D2N", mAxisymLineElasticCondition2D2N)
    KRATOS_REGISTER_CONDITION("AxisymLineSpring2D3N", mAxisymLineElasticCondition2D3N)

    KRATOS_REGISTER_CONDITION("SurfaceSpring3D3N", mSurfaceElasticCondition3D3N)
    KRATOS_REGISTER_CONDITION("SurfaceSpring3D4N", mSurfaceElasticCondition3D4N)
    KRATOS_REGISTER_CONDITION("SurfaceSpring3D6N", mSurfaceElasticCondition3D6N)
    KRATOS_REGISTER_CONDITION("SurfaceSpring3D8N", mSurfaceElasticCondition3D8N)
    KRATOS_REGISTER_CONDITION("SurfaceSpring3D9N", mSurfaceElasticCondition3D9N)

    KRATOS_REGISTER_CONDITION("PointFlux3D1N", mPointHeatFluxCondition3D1N)
    KRATOS_REGISTER_CONDITION("PointFlux2D1N", mPointHeatFluxCondition2D1N)
    KRATOS_REGISTER_CONDITION("AxisymPointFlux2D1N", mAxisymPointHeatFluxCondition2D1N)

    KRATOS_REGISTER_CONDITION("LineFlux3D2N", mLineHeatFluxCondition3D2N)
    KRATOS_REGISTER_CONDITION("LineFlux3D3N", mLineHeatFluxCondition3D3N)
    KRATOS_REGISTER_CONDITION("LineFlux2D2N", mLineHeatFluxCondition2D2N)
    KRATOS_REGISTER_CONDITION("LineFlux2D3N", mLineHeatFluxCondition2D3N)
    KRATOS_REGISTER_CONDITION("AxisymLineFlux2D2N", mAxisymLineHeatFluxCondition2D2N)
    KRATOS_REGISTER_CONDITION("AxisymLineFlux2D3N", mAxisymLineHeatFluxCondition2D3N)

    KRATOS_REGISTER_CONDITION("SurfaceFlux3D3N", mSurfaceHeatFluxCondition3D3N)
    KRATOS_REGISTER_CONDITION("SurfaceFlux3D4N", mSurfaceHeatFluxCondition3D4N)
    KRATOS_REGISTER_CONDITION("SurfaceFlux3D6N", mSurfaceHeatFluxCondition3D6N)
    KRATOS_REGISTER_CONDITION("SurfaceFlux3D8N", mSurfaceHeatFluxCondition3D8N)

}

} // namespace Kratos.
