//------------------------------------------------------------------
//           ___      _ _    _                                     .
//   KRATOS / __| ___| (_)__| |                                    .
//          \__ \/ _ \ | / _` |                                    .
//          |___/\___/_|_\__,_| MECHANICS                          .
//			                                           .
//   License:(BSD)	  SolidMechanicsApplication/license.txt    .
//   Main authors:    Josep Maria Carbonell                        .
//                                                                 .
//------------------------------------------------------------------
//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:         JMCarbonell $
//   Maintained by:       $Maintainer:                JMC $
//   Date:                $Date:                July 2013 $
//
//

#if !defined(KRATOS_SOLID_MECHANICS_APPLICATION_H_INCLUDED)
#define KRATOS_SOLID_MECHANICS_APPLICATION_H_INCLUDED

// System includes
#include <string>
#include <iostream>

// External includes

// Project includes
#include "includes/define.h"
#include "includes/constitutive_law.h"
#include "includes/ublas_interface.h"
#include "includes/kratos_application.h"
#include "containers/flags.h"

//elements

//solid elements
#include "custom_elements/solid_elements/linear_solid_element.hpp"

#include "custom_elements/solid_elements/small_displacement_element.hpp"
#include "custom_elements/solid_elements/small_displacement_bbar_element.hpp"
#include "custom_elements/solid_elements/axisymmetric_small_displacement_element.hpp"

#include "custom_elements/solid_elements/total_lagrangian_element.hpp"
#include "custom_elements/solid_elements/updated_lagrangian_element.hpp"
#include "custom_elements/solid_elements/axisymmetric_updated_lagrangian_element.hpp"

#include "custom_elements/solid_elements/updated_lagrangian_U_P_element.hpp"
#include "custom_elements/solid_elements/axisymmetric_updated_lagrangian_U_P_element.hpp"

#include "custom_elements/solid_elements/updated_lagrangian_V_element.hpp"
#include "custom_elements/solid_elements/updated_lagrangian_segregated_V_P_element.hpp"

#include "custom_elements/solid_elements/updated_lagrangian_U_J_element.hpp"
#include "custom_elements/solid_elements/axisymmetric_updated_lagrangian_U_J_element.hpp"

#include "custom_elements/solid_elements/updated_total_lagrangian_element.hpp"
#include "custom_elements/solid_elements/updated_total_lagrangian_U_P_element.hpp"

//beam elements
#include "custom_elements/beam_elements/beam_element.hpp"
#include "custom_elements/beam_elements/small_displacement_beam_element.hpp"
#include "custom_elements/beam_elements/small_displacement_beam_element_3D2N.hpp"
#include "custom_elements/beam_elements/large_displacement_beam_element.hpp"
#include "custom_elements/beam_elements/large_displacement_beam_emc_element.hpp"
#include "custom_elements/beam_elements/large_displacement_beam_semc_element.hpp"
#include "custom_elements/beam_elements/geometrically_exact_rod_element.hpp"

//shell elements
#include "custom_elements/shell_elements/shell_thick_element_3D4N.hpp"
#include "custom_elements/shell_elements/shell_thin_element_3D3N.hpp"

//thermal elements
#include "custom_elements/thermal_elements/thermal_element.hpp"
#include "custom_elements/thermal_elements/axisymmetric_thermal_element.hpp"
#include "custom_elements/thermal_elements/soil_thermal_element.hpp"
#include "custom_elements/thermal_elements/soil_axisymmetric_thermal_element.hpp"

//hydro elements
#include "custom_elements/hydromechanical_elements/updated_lagrangian_U_wP_element.hpp"
#include "custom_elements/hydromechanical_elements/updated_lagrangian_U_wP_Stab_element.hpp"
#include "custom_elements/hydromechanical_elements/unsaturated_U_wP_element.hpp"
#include "custom_elements/hydromechanical_elements/axisymmetric_unsaturated_U_wP_element.hpp"
#include "custom_elements/hydromechanical_elements/unsaturated_U_J_wP_element.hpp"
#include "custom_elements/hydromechanical_elements/axisymmetric_unsaturated_U_J_wP_element.hpp"

#include "custom_elements/hydromechanical_elements/axisymmetric_updated_lagrangian_U_wP_element.hpp"
#include "custom_elements/hydromechanical_elements/axisymmetric_updated_lagrangian_U_wP_Stab_element.hpp"

#include "custom_elements/hydromechanical_elements/updated_lagrangian_U_W_element.hpp"
#include "custom_elements/hydromechanical_elements/updated_lagrangian_U_W_wP_element.hpp"
#include "custom_elements/hydromechanical_elements/updated_lagrangian_U_W_wP_DME_element.hpp"
#include "custom_elements/hydromechanical_elements/updated_lagrangian_U_J_W_wP_element.hpp"
#include "custom_elements/hydromechanical_elements/updated_lagrangian_U_J_W_wP_HO_element.hpp"
#include "custom_elements/hydromechanical_elements/updated_lagrangian_U_J_W_wP_DME_element.hpp"
#include "custom_elements/hydromechanical_elements/small_displacement_U_W_wP_element.hpp"
#include "custom_elements/hydromechanical_elements/small_displacement_U_wP_element.hpp"
#include "custom_elements/hydromechanical_elements/axisymmetric_small_displacement_U_wP_element.hpp"

#include "custom_elements/hydromechanical_elements/updated_lagrangian_U_Jacobian_element.hpp"
#include "custom_elements/hydromechanical_elements/updated_lagrangian_U_J_P_element.hpp"
#include "custom_elements/hydromechanical_elements/updated_lagrangian_U_Pressure_element.hpp"

#include "custom_elements/hydromechanical_elements/updated_lagrangian_U_P_wP_element.hpp"
#include "custom_elements/hydromechanical_elements/updated_lagrangian_U_J_wP_element.hpp"

#include "custom_elements/hydromechanical_elements/axisymmetric_updated_lagrangian_U_Jacobian_element.hpp"
#include "custom_elements/hydromechanical_elements/axisymmetric_updated_lagrangian_U_J_wP_element.hpp"
#include "custom_elements/hydromechanical_elements/axisymmetric_updated_lagrangian_U_J_W_wP_element.hpp"
#include "custom_elements/hydromechanical_elements/axisymmetric_updated_lagrangian_U_J_W_wP_DME_element.hpp"

#include "custom_elements/hydromechanical_elements/axisymmetric_updated_lagrangian_U_Pressure_element.hpp"
#include "custom_elements/hydromechanical_elements/axisymmetric_updated_lagrangian_U_P_wP_element.hpp"

//conditions
#include "custom_conditions/load_conditions/axisymmetric_point_load_condition.hpp"
#include "custom_conditions/load_conditions/axisymmetric_line_load_condition.hpp"
#include "custom_conditions/load_conditions/surface_load_condition.hpp"

#include "custom_conditions/moment_conditions/point_moment_condition.hpp"
#include "custom_conditions/moment_conditions/line_moment_condition.hpp"
#include "custom_conditions/moment_conditions/surface_moment_condition.hpp"

#include "custom_conditions/elastic_conditions/axisymmetric_point_elastic_condition.hpp"
#include "custom_conditions/elastic_conditions/axisymmetric_line_elastic_condition.hpp"
#include "custom_conditions/elastic_conditions/surface_elastic_condition.hpp"

#include "custom_conditions/thermal_conditions/axisymmetric_point_heat_flux_condition.hpp"
#include "custom_conditions/thermal_conditions/axisymmetric_line_heat_flux_condition.hpp"
#include "custom_conditions/thermal_conditions/surface_heat_flux_condition.hpp"

#include "solid_mechanics_application_variables.h"

namespace Kratos
{
///@name Type Definitions
///@{
typedef array_1d<double, 3> Vector3;
typedef array_1d<double, 6> Vector6;
///@}

///@name Kratos Globals
///@{

//Application variables definition:  (see solid_mechanics_application_variables.h)

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
 */
class KRATOS_API(SOLID_MECHANICS_APPLICATION) KratosSolidMechanicsApplication : public KratosApplication
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of KratosSolidMechanicsApplication
  KRATOS_CLASS_POINTER_DEFINITION(KratosSolidMechanicsApplication);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  KratosSolidMechanicsApplication();

  /// Destructor.
  ~KratosSolidMechanicsApplication() override {}

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  void Register() override;

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "KratosSolidMechanicsApplication";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << Info();
    PrintData(rOStream);
  }

  ///// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
    KRATOS_WATCH("in KratosSolidMechanicsApplication")
    KRATOS_WATCH(KratosComponents<VariableData>::GetComponents().size())
    rOStream << "Variables:" << std::endl;
    KratosComponents<VariableData>().PrintData(rOStream);
    rOStream << std::endl;
    rOStream << "Elements:" << std::endl;
    KratosComponents<Element>().PrintData(rOStream);
    rOStream << std::endl;
    rOStream << "Conditions:" << std::endl;
    KratosComponents<Condition>().PrintData(rOStream);
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  //solid
  const LinearSolidElement mLinearSolidElement2D3N;
  const LinearSolidElement mLinearSolidElement2D4N;
  const LinearSolidElement mLinearSolidElement3D4N;
  const LinearSolidElement mLinearSolidElement3D8N;

  //small displacement
  const SmallDisplacementElement mSmallDisplacementElement2D3N;
  const SmallDisplacementElement mSmallDisplacementElement2D4N;
  const SmallDisplacementElement mSmallDisplacementElement2D6N;
  const SmallDisplacementElement mSmallDisplacementElement2D8N;
  const SmallDisplacementElement mSmallDisplacementElement2D9N;

  const SmallDisplacementElement mSmallDisplacementElement3D4N;
  const SmallDisplacementElement mSmallDisplacementElement3D6N;
  const SmallDisplacementElement mSmallDisplacementElement3D8N;
  const SmallDisplacementElement mSmallDisplacementElement3D10N;
  const SmallDisplacementElement mSmallDisplacementElement3D15N;
  const SmallDisplacementElement mSmallDisplacementElement3D20N;
  const SmallDisplacementElement mSmallDisplacementElement3D27N;

  //B-bar
  const SmallDisplacementBbarElement mSmallDisplacementBbarElement2D3N;
  const SmallDisplacementBbarElement mSmallDisplacementBbarElement2D4N;
  const SmallDisplacementBbarElement mSmallDisplacementBbarElement2D6N;
  const SmallDisplacementBbarElement mSmallDisplacementBbarElement2D8N;
  const SmallDisplacementBbarElement mSmallDisplacementBbarElement2D9N;

  const SmallDisplacementBbarElement mSmallDisplacementBbarElement3D4N;
  const SmallDisplacementBbarElement mSmallDisplacementBbarElement3D6N;
  const SmallDisplacementBbarElement mSmallDisplacementBbarElement3D8N;
  const SmallDisplacementBbarElement mSmallDisplacementBbarElement3D10N;
  const SmallDisplacementBbarElement mSmallDisplacementBbarElement3D15N;
  const SmallDisplacementBbarElement mSmallDisplacementBbarElement3D20N;
  const SmallDisplacementBbarElement mSmallDisplacementBbarElement3D27N;

  const AxisymmetricSmallDisplacementElement mAxisymSmallDisplacementElement2D3N;
  const AxisymmetricSmallDisplacementElement mAxisymSmallDisplacementElement2D4N;
  const AxisymmetricSmallDisplacementElement mAxisymSmallDisplacementElement2D6N;
  const AxisymmetricSmallDisplacementElement mAxisymSmallDisplacementElement2D8N;
  const AxisymmetricSmallDisplacementElement mAxisymSmallDisplacementElement2D9N;

  //large displacement
  const LargeDisplacementElement mLargeDisplacementElement;
  const LargeDisplacementUPElement mLargeDisplacementUPElement;

  //total lagrangian
  const TotalLagrangianElement mTotalLagrangianElement2D3N;
  const TotalLagrangianElement mTotalLagrangianElement2D4N;
  const TotalLagrangianElement mTotalLagrangianElement2D6N;
  const TotalLagrangianElement mTotalLagrangianElement2D8N;
  const TotalLagrangianElement mTotalLagrangianElement2D9N;

  const TotalLagrangianElement mTotalLagrangianElement3D4N;
  const TotalLagrangianElement mTotalLagrangianElement3D6N;
  const TotalLagrangianElement mTotalLagrangianElement3D8N;
  const TotalLagrangianElement mTotalLagrangianElement3D10N;
  const TotalLagrangianElement mTotalLagrangianElement3D15N;
  const TotalLagrangianElement mTotalLagrangianElement3D20N;
  const TotalLagrangianElement mTotalLagrangianElement3D27N;

  //updated total lagrangian
  const UpdatedTotalLagrangianElement mUpdatedTotalLagrangianElement2D3N;
  const UpdatedTotalLagrangianElement mUpdatedTotalLagrangianElement2D4N;
  const UpdatedTotalLagrangianElement mUpdatedTotalLagrangianElement2D6N;
  const UpdatedTotalLagrangianElement mUpdatedTotalLagrangianElement2D8N;
  const UpdatedTotalLagrangianElement mUpdatedTotalLagrangianElement2D9N;

  const UpdatedTotalLagrangianElement mUpdatedTotalLagrangianElement3D4N;
  const UpdatedTotalLagrangianElement mUpdatedTotalLagrangianElement3D6N;
  const UpdatedTotalLagrangianElement mUpdatedTotalLagrangianElement3D8N;
  const UpdatedTotalLagrangianElement mUpdatedTotalLagrangianElement3D10N;
  const UpdatedTotalLagrangianElement mUpdatedTotalLagrangianElement3D15N;
  const UpdatedTotalLagrangianElement mUpdatedTotalLagrangianElement3D20N;
  const UpdatedTotalLagrangianElement mUpdatedTotalLagrangianElement3D27N;

  const UpdatedTotalLagrangianUPElement mUpdatedTotalLagrangianUPElement2D3N;

  //updated lagrangian
  const UpdatedLagrangianElement mUpdatedLagrangianElement2D3N;
  const UpdatedLagrangianElement mUpdatedLagrangianElement2D4N;
  const UpdatedLagrangianElement mUpdatedLagrangianElement2D6N;
  const UpdatedLagrangianElement mUpdatedLagrangianElement2D8N;
  const UpdatedLagrangianElement mUpdatedLagrangianElement2D9N;

  const UpdatedLagrangianElement mUpdatedLagrangianElement3D4N;
  const UpdatedLagrangianElement mUpdatedLagrangianElement3D6N;
  const UpdatedLagrangianElement mUpdatedLagrangianElement3D8N;
  const UpdatedLagrangianElement mUpdatedLagrangianElement3D10N;
  const UpdatedLagrangianElement mUpdatedLagrangianElement3D15N;
  const UpdatedLagrangianElement mUpdatedLagrangianElement3D20N;
  const UpdatedLagrangianElement mUpdatedLagrangianElement3D27N;

  const AxisymmetricUpdatedLagrangianElement mAxisymUpdatedLagrangianElement2D3N;
  const AxisymmetricUpdatedLagrangianElement mAxisymUpdatedLagrangianElement2D4N;
  const AxisymmetricUpdatedLagrangianElement mAxisymUpdatedLagrangianElement2D6N;
  const AxisymmetricUpdatedLagrangianElement mAxisymUpdatedLagrangianElement2D8N;
  const AxisymmetricUpdatedLagrangianElement mAxisymUpdatedLagrangianElement2D9N;

  //velocity based elements
  const UpdatedLagrangianVElement mUpdatedLagrangianVElement2D3N;
  const UpdatedLagrangianVElement mUpdatedLagrangianVElement2D4N;
  const UpdatedLagrangianVElement mUpdatedLagrangianVElement2D6N;
  const UpdatedLagrangianVElement mUpdatedLagrangianVElement2D8N;
  const UpdatedLagrangianVElement mUpdatedLagrangianVElement2D9N;

  const UpdatedLagrangianVElement mUpdatedLagrangianVElement3D4N;
  const UpdatedLagrangianVElement mUpdatedLagrangianVElement3D6N;
  const UpdatedLagrangianVElement mUpdatedLagrangianVElement3D8N;
  const UpdatedLagrangianVElement mUpdatedLagrangianVElement3D10N;
  const UpdatedLagrangianVElement mUpdatedLagrangianVElement3D15N;
  const UpdatedLagrangianVElement mUpdatedLagrangianVElement3D20N;
  const UpdatedLagrangianVElement mUpdatedLagrangianVElement3D27N;

  //segregated VP elements
  const UpdatedLagrangianSegregatedVPElement mUpdatedLagrangianSegregatedVPElement2D3N;
  const UpdatedLagrangianSegregatedVPElement mUpdatedLagrangianSegregatedVPElement3D4N;

  //mixed elements UP
  const UpdatedLagrangianUPElement mUpdatedLagrangianUPElement2D3N;
  const AxisymmetricUpdatedLagrangianUPElement mAxisymUpdatedLagrangianUPElement2D3N;
  const UpdatedLagrangianUPElement mUpdatedLagrangianUPElement3D4N;

  //mixed elements UJ
  const UpdatedLagrangianUJElement mUpdatedLagrangianUJElement2D3N;
  const AxisymmetricUpdatedLagrangianUJElement mAxisymUpdatedLagrangianUJElement2D3N;
  const UpdatedLagrangianUJElement mUpdatedLagrangianUJElement3D4N;

  //beams
  const SmallDisplacementBeamElement mSmallDisplacementBeamElement3D2N;
  const LargeDisplacementBeamElement mLargeDisplacementBeamElement3D2N;
  const LargeDisplacementBeamElement mLargeDisplacementBeamElement3D3N;
  const LargeDisplacementBeamEMCElement mLargeDisplacementBeamEMCElement3D2N;
  const LargeDisplacementBeamEMCElement mLargeDisplacementBeamEMCElement3D3N;
  const LargeDisplacementBeamSEMCElement mLargeDisplacementBeamSEMCElement3D2N;
  const GeometricallyExactRodElement mGeometricallyExactRodElement3D2N;
  const LargeDisplacementBeamElement mLargeDisplacementBeamElement2D2N;

  //shells
  const ShellThickElement3D4N mShellThickElement3D4N;
  const ShellThickElement3D4N mShellThickCorotationalElement3D4N;
  const ShellThinElement3D3N mShellThinElement3D3N;
  const ShellThinElement3D3N mShellThinCorotationalElement3D3N;

  //thermal
  const ThermalElement mThermalElement2D3N;
  const ThermalElement mThermalElement2D4N;
  const ThermalElement mThermalElement2D6N;
  const ThermalElement mThermalElement2D8N;
  const ThermalElement mThermalElement2D9N;

  const ThermalElement mThermalElement3D4N;
  const ThermalElement mThermalElement3D6N;
  const ThermalElement mThermalElement3D8N;
  const ThermalElement mThermalElement3D10N;
  const ThermalElement mThermalElement3D15N;
  const ThermalElement mThermalElement3D20N;
  const ThermalElement mThermalElement3D27N;

  const AxisymmetricThermalElement mAxisymThermalElement2D3N;
  const AxisymmetricThermalElement mAxisymThermalElement2D4N;
  const AxisymmetricThermalElement mAxisymThermalElement2D6N;
  const AxisymmetricThermalElement mAxisymThermalElement2D8N;
  const AxisymmetricThermalElement mAxisymThermalElement2D9N;

  const SoilThermalElement mSoilThermalElement2D3N;
  const SoilThermalElement mSoilThermalElement3D4N;
  const SoilAxisymmetricThermalElement mSoilAxisymThermalElement2D3N;

  //hydro-mechanical
  const UpdatedLagrangianUwPElement mUpdatedLagrangianUwPElement2D3N;
  const UpdatedLagrangianUwPElement mUpdatedLagrangianUwPElement3D4N;
  const UpdatedLagrangianUwPStabElement mUpdatedLagrangianUwPStabElement2D3N;
  const UpdatedLagrangianUwPStabElement mUpdatedLagrangianUwPStabElement3D4N;
  const UpdatedLagrangianUwPStabElement mUpdatedLagrangianUwPStabElement2D9N;

  const UpdatedLagrangianUnsaturatedUwPElement mUpdatedLagrangianUnsaturatedUwPElement2D3N;
  const AxisymmetricUpdatedLagrangianUnsaturatedUwPElement mAxisymUpdatedLagrangianUnsaturatedUwPElement2D3N;
  const UpdatedLagrangianUnsaturatedUJwPElement mUpdatedLagrangianUnsaturatedUJwPElement2D3N;
  const AxisymmetricUpdatedLagrangianUnsaturatedUJwPElement mAxisymUpdatedLagrangianUnsaturatedUJwPElement2D3N;

  const UpdatedLagrangianUWElement mUpdatedLagrangianUWElement2D3N;
  const UpdatedLagrangianUWwPElement mUpdatedLagrangianUWwPElement2D3N;
  const UpdatedLagrangianUWwPDMEElement mUpdatedLagrangianUWwPDMEElement2D3N;
  const UpdatedLagrangianUJWwPElement mUpdatedLagrangianUJWwPElement2D3N;
  const UpdatedLagrangianUJWwPHOElement mUpdatedLagrangianUJWwPHOElement2D3N;
  const UpdatedLagrangianUJWwPDMEElement mUpdatedLagrangianUJWwPDMEElement2D3N;
  const UpdatedLagrangianUJWwPElement mUpdatedLagrangianUJWwPElement3D4N;
  const UpdatedLagrangianUJWwPDMEElement mUpdatedLagrangianUJWwPDMEElement3D4N;
  const SmallDisplacementUWwPElement mSmallDisplacementUWwPElement2D3N;
  const SmallDisplacementUwPElement mSmallDisplacementUwPElement2D3N;
  const AxisymmetricSmallDisplacementUwPElement mAxisymSmallDisplacementUwPElement2D3N;
  const AxisymmetricSmallDisplacementUwPElement mAxisymSmallDisplacementUwPElement2D6N;

  const AxisymmetricUpdatedLagrangianUwPElement mAxisymUpdatedLagrangianUwPElement2D3N;
  const AxisymmetricUpdatedLagrangianUwPStabElement mAxisymUpdatedLagrangianUwPStabElement2D3N;
  const AxisymmetricUpdatedLagrangianUwPStabElement mAxisymUpdatedLagrangianUwPStabElement2D6N;
  const AxisymmetricUpdatedLagrangianUwPStabElement mAxisymUpdatedLagrangianUwPStabElement2D9N;

  const UpdatedLagrangianUJacobianElement mUpdatedLagrangianUJacobianElement2D3N;
  const UpdatedLagrangianUJacobianElement mUpdatedLagrangianUJacobianElement3D4N;
  const UpdatedLagrangianUJPElement mUpdatedLagrangianUJPElement2D3N;
  const UpdatedLagrangianUPressureElement mUpdatedLagrangianUPressureElement2D3N;

  const UpdatedLagrangianUJwPElement mUpdatedLagrangianUJwPElement2D3N;
  const UpdatedLagrangianUJwPElement mUpdatedLagrangianUJwPElement3D4N;
  const UpdatedLagrangianUPwPElement mUpdatedLagrangianUPwPElement2D3N;

  const AxisymmetricUpdatedLagrangianUJacobianElement mAxisymUpdatedLagrangianUJacobianElement2D3N;
  const AxisymmetricUpdatedLagrangianUJwPElement mAxisymUpdatedLagrangianUJwPElement2D3N;
  const AxisymmetricUpdatedLagrangianUJWwPElement mAxisymUpdatedLagrangianUJWwPElement2D3N;
  const AxisymmetricUpdatedLagrangianUJWwPDMEElement mAxisymUpdatedLagrangianUJWwPDMEElement2D3N;

  const AxisymmetricUpdatedLagrangianUPressureElement mAxisymUpdatedLagrangianUPressureElement2D3N;
  const AxisymmetricUpdatedLagrangianUPwPElement mAxisymUpdatedLagrangianUPwPElement2D3N;

  //conditions
  const PointLoadCondition mPointLoadCondition3D1N;
  const PointLoadCondition mPointLoadCondition2D1N;
  const AxisymmetricPointLoadCondition mAxisymPointLoadCondition2D1N;

  const LineLoadCondition mLineLoadCondition3D2N;
  const LineLoadCondition mLineLoadCondition3D3N;
  const LineLoadCondition mLineLoadCondition2D2N;
  const LineLoadCondition mLineLoadCondition2D3N;
  const AxisymmetricLineLoadCondition mAxisymLineLoadCondition2D2N;
  const AxisymmetricLineLoadCondition mAxisymLineLoadCondition2D3N;

  const SurfaceLoadCondition mSurfaceLoadCondition3D3N;
  const SurfaceLoadCondition mSurfaceLoadCondition3D4N;
  const SurfaceLoadCondition mSurfaceLoadCondition3D6N;
  const SurfaceLoadCondition mSurfaceLoadCondition3D8N;
  const SurfaceLoadCondition mSurfaceLoadCondition3D9N;

  const PointMomentCondition mPointMomentCondition3D1N;
  const PointMomentCondition mPointMomentCondition2D1N;

  const LineMomentCondition mLineMomentCondition3D2N;
  const LineMomentCondition mLineMomentCondition3D3N;
  const LineMomentCondition mLineMomentCondition2D2N;
  const LineMomentCondition mLineMomentCondition2D3N;

  const SurfaceMomentCondition mSurfaceMomentCondition3D3N;
  const SurfaceMomentCondition mSurfaceMomentCondition3D4N;
  const SurfaceMomentCondition mSurfaceMomentCondition3D6N;
  const SurfaceMomentCondition mSurfaceMomentCondition3D8N;
  const SurfaceMomentCondition mSurfaceMomentCondition3D9N;

  const PointElasticCondition mPointElasticCondition3D1N;
  const PointElasticCondition mPointElasticCondition2D1N;
  const AxisymmetricPointElasticCondition mAxisymPointElasticCondition2D1N;

  const LineElasticCondition mLineElasticCondition3D2N;
  const LineElasticCondition mLineElasticCondition3D3N;
  const LineElasticCondition mLineElasticCondition2D2N;
  const LineElasticCondition mLineElasticCondition2D3N;
  const AxisymmetricLineElasticCondition mAxisymLineElasticCondition2D2N;
  const AxisymmetricLineElasticCondition mAxisymLineElasticCondition2D3N;

  const SurfaceElasticCondition mSurfaceElasticCondition3D3N;
  const SurfaceElasticCondition mSurfaceElasticCondition3D4N;
  const SurfaceElasticCondition mSurfaceElasticCondition3D6N;
  const SurfaceElasticCondition mSurfaceElasticCondition3D8N;
  const SurfaceElasticCondition mSurfaceElasticCondition3D9N;

  const PointHeatFluxCondition mPointHeatFluxCondition3D1N;
  const PointHeatFluxCondition mPointHeatFluxCondition2D1N;
  const AxisymmetricPointHeatFluxCondition mAxisymPointHeatFluxCondition2D1N;

  const LineHeatFluxCondition mLineHeatFluxCondition3D2N;
  const LineHeatFluxCondition mLineHeatFluxCondition3D3N;
  const LineHeatFluxCondition mLineHeatFluxCondition2D2N;
  const LineHeatFluxCondition mLineHeatFluxCondition2D3N;
  const AxisymmetricLineHeatFluxCondition mAxisymLineHeatFluxCondition2D2N;
  const AxisymmetricLineHeatFluxCondition mAxisymLineHeatFluxCondition2D3N;

  const SurfaceHeatFluxCondition mSurfaceHeatFluxCondition3D3N;
  const SurfaceHeatFluxCondition mSurfaceHeatFluxCondition3D4N;
  const SurfaceHeatFluxCondition mSurfaceHeatFluxCondition3D6N;
  const SurfaceHeatFluxCondition mSurfaceHeatFluxCondition3D8N;
  const SurfaceHeatFluxCondition mSurfaceHeatFluxCondition3D9N;

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  /// Assignment operator.
  KratosSolidMechanicsApplication &operator=(KratosSolidMechanicsApplication const &rOther);

  /// Copy constructor.
  KratosSolidMechanicsApplication(KratosSolidMechanicsApplication const &rOther);

  ///@}

}; // Class KratosSolidMechanicsApplication

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

} // namespace Kratos.

#endif // KRATOS_SOLID_MECHANICS_APPLICATION_H_INCLUDED  defined
