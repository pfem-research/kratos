""" Project: SolidMechanicsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Built-in/Generic Imports
from KratosMultiphysics.analysis_stage import AnalysisStage
import time as timer
import sys
import os

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolidMechanicsApplication as KratosSolid
import KratosMultiphysics.SolidMechanicsApplication.analysis_utilities as analysis_utilities

sys.stdout.flush()

class SolidCompositeAnalysis(AnalysisStage):

    def __init__(self, model, project_parameters="ProjectParameters.json", file_name=None):

        # Print starting date/time
        print(timer.ctime())

        # Time control starts
        self.t0p,self.t0w = analysis_utilities.StartTimeControl()

        # Import input
        self._project_parameters = self._import_project_parameters(project_parameters)

        # Set analysis
        self._stages = []
        if self._project_parameters.Has("stages"):

            # Add custom problem type parameters
            self._project_parameters = self._add_problem_type_parameters(self._project_parameters)

            # Set model name (prepend parent model part name)
            self._project_parameters = self._set_model_name(self._project_parameters)

            # Set echo level
            self.echo_level = 0
            if self._project_parameters.Has("problem_data"):
                problem_data = self._project_parameters["problem_data"]
                if problem_data.Has("echo_level"):
                    self.echo_level = problem_data["echo_level"].GetInt()

            # Set logger severity level
            analysis_utilities.SetSeverityLevel(self.echo_level)

            # Defining the number of threads
            self.threads = analysis_utilities.GetParallelSize()
            if self._project_parameters.Has("problem_data"):
                problem_data = self._project_parameters["problem_data"]
                if problem_data.Has("threads"):
                    self.threads = problem_data["threads"].GetInt()
                    analysis_utilities.SetParallelSize(self.threads)

            print(" ")
            print(self._class_prefix()+" [OMP USING", self.threads, "THREADS]")

            # Set analysis model
            from KratosMultiphysics.SolidMechanicsApplication.solid_analysis_model import SolidAnalysisModel
            self._model = SolidAnalysisModel(model, self._project_parameters["model_settings"])

            # Set analysis stages
            for stage in range(self._project_parameters["stages"].size()):
                from KratosMultiphysics.SolidMechanicsApplication.solid_analysis_stage import SolidAnalysisStage
                self._stages.append(SolidAnalysisStage(self._model,self._project_parameters["stages"][stage]))

            # Import Model
            self._model.ExecuteInitialize()
        else:
            from KratosMultiphysics.SolidMechanicsApplication.solid_analysis import SolidAnalysis
            self._stages.append(SolidAnalysis(model, project_parameters, file_name))

    def Initialize(self):
        print(self._class_prefix()+" Simulation -START- ")
        pass

    def RunSolutionLoop(self):
        counter = 0
        for stage in self._stages:
            print(self._class_prefix()+" STAGE ["+str(counter)+"] -START- ")
            # Run stages sequencially
            stage.Run()
            # Set problem as restarted after 1srt stage to avoid initial operations
            if len(self._stages) != 1:
                self._model.GetProcessInfo().SetValue(KratosMultiphysics.IS_RESTARTED,True)
            counter += 1


    def Finalize(self):
        # Ending the problem (time integration finished)
        print(self._class_prefix()+" Simulation -END- ")
        print(" ")

        # Check solving information for any problem
        # self._solver.InfoCheck() # InfoCheck not implemented yet.
        analysis_utilities.EndTimeControl(self._class_prefix(), self.t0p, self.t0w)

        print(timer.ctime())

    #### Main internal methods ####

    def _import_project_parameters(self, input_parameters):
        parameters = None
        if type(input_parameters) == str:
            if os.path.isfile(input_parameters):
                input_file = open(input_parameters, 'r')
                parameters = KratosMultiphysics.Parameters(input_file.read())
            else:
                raise Exception("Input file "+input_parameters+" file does not exist")
        else:
            parameters = input_parameters
        #print(" PARAMETERS ", parameters.PrettyPrintJsonString())

        return parameters

    def _add_problem_type_parameters(self, input_parameters):
        # Add ALE variables (temporary, better location to add them needed)
        for stage in self._project_parameters["stages"]:
            solver_parameters = stage["solver_settings"]["Parameters"]
            if solver_parameters.Has("time_integration_settings"):
                time_settings = solver_parameters["time_integration_settings"]
                if time_settings.Has("formulation_type"):
                    if time_settings["formulation_type"].GetString() == "ALE":
                        # Add ALE variables (FluidDynamicsApplication)
                        model_settings = input_parameters["model_settings"]
                        ale_variables = ["BODY_FORCE","ADVPROJ","DIVPROJ","NODAL_AREA"]
                        if not model_settings.Has("variables"):
                            model_settings.AddEmptyList("variables")
                        variables= model_settings["variables"]
                        for variable in ale_variables:
                            variables.Append(variable) if variable not in variables else variables
                        return input_parameters
        return input_parameters

    #
    def _set_model_name(self, project_parameters):
        # prepend parent model part name
        model_name = "problem_name"
        if project_parameters.Has("model_settings") and project_parameters["model_settings"].Has("model_name"):
            model_name = project_parameters["model_settings"]["model_name"].GetString()

        process_lists = ["constraints_process_list", "loads_process_list",
                         "problem_process_list", "output_process_list", "check_process_list"]

        for stage in project_parameters["stages"]:
            if stage.Has("solver_settings"):
                solver_parameters = stage["solver_settings"]["Parameters"]
                self._set_solving_model_part_name(solver_parameters, model_name)
                if solver_parameters.Has("solvers"):
                    for solver in solver_parameters["solvers"]:
                        solver_parameters = solver["Parameters"]
                        self._set_solving_model_part_name(solver_parameters, model_name)
                        if solver.Has("processes"):
                            for process in solver["processes"]:
                                self._set_model_part_name(process, model_name)
                else:
                    if solver_parameters.Has("processes"):
                        for process in solver_parameters["processes"]:
                            self._set_model_part_name(process, model_name)

            for list_name in process_lists:
                if stage.Has(list_name):
                    for process in stage[list_name]:
                        self._set_model_part_name(process, model_name)

        return project_parameters
    #
    def _set_solving_model_part_name(self, solver_parameters, model_name):
        # prepend parent model part name
        if solver_parameters.Has("solving_model_part"):
            name = model_name+"."+solver_parameters["solving_model_part"].GetString()
            solver_parameters["solving_model_part"].SetString(name)
        else:
            solver_parameters.AddEmptyValue("solving_model_part").SetString(
                model_name+".computing_domain")
    #
    def _set_model_part_name(self, process, model_name):
        # prepend parent model part name
        parameters = process
        if process.Has("Parameters"):
            parameters = process["Parameters"]
        if parameters.Has("model_part_name"):
            name = parameters["model_part_name"].GetString().split(".")[0]
            if model_name != name:
                name = model_name+"."+parameters["model_part_name"].GetString()
                parameters["model_part_name"].SetString(name)

    @classmethod
    def _class_prefix(self):
        header = "::[---KSM Simulation--]::"
        return header


if __name__ == "__main__":
    SolidCompositeAnalysis().Run()
