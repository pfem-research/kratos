""" Project: SolidMechanicsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Built-in/Generic Imports
import sys
import os
import math

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolidMechanicsApplication as KratosSolid
import KratosMultiphysics.SolversApplication as KratosSolver
import KratosMultiphysics.SolidMechanicsApplication.assign_utilities as assign_utilities

def Factory(custom_settings, Model):
    if(not isinstance(custom_settings, KratosMultiphysics.Parameters)):
        raise Exception(
            "expected input shall be a Parameters object, encapsulating a json string")
    return AssignStringPositionProcess(Model, custom_settings["Parameters"])

# All the processes python should be derived from "Process"


class AssignStringPositionProcess(KratosMultiphysics.Process):
    def __init__(self, Model, custom_settings):
        KratosMultiphysics.Process.__init__(self)

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
             "model_part_name": "MODEL_PART_NAME",
             "variable_name": "VARIABLE_NAME",
             "value": [0.0,0.0,0.0],
             "compound_assignment": "direct",
             "constrained":true,
             "interval": [0.0, "End"],
             "local_axes" : {},
             "flags_list": [],
             "file_name" : "FILE_NAME"
        }
        """)

        # overwrite the default settings with user-provided parameters
        self.settings = custom_settings
        self.settings.ValidateAndAssignDefaults(default_settings)

        self.file_name = self.settings["file_name"].GetString()

        self.settings.RemoveValue("file_name")
        self.settings.RemoveValue("steps")
        import KratosMultiphysics.SolidMechanicsApplication.assign_vector_components_to_nodes_process as assign_vector_process
        self.AssignValueProcesses = assign_vector_process.AssignVectorComponentsToNodesProcess(Model, self.settings)

        self.variable_name = self.settings["variable_name"].GetString()
        self.model = Model

        self.constraints = []
        for i in range(0, self.settings["value"].size()):
            if(self.settings["value"][i].IsNull()):
                self.constraints.append(False)
            else:
                self.constraints.append(True)

    def GetVariables(self):
        nodal_variables = [self.settings["variable_name"].GetString()]
        return nodal_variables

    def ExecuteInitialize(self):
        # set model part
        self.model_part = self.model[self.settings["model_part_name"].GetString()]


        # set time interval
        #self.TimeInterval = assign_utilities.TimeIntervalManager(self.settings["interval"], self.model_part.ProcessInfo)
        time_params = KratosMultiphysics.Parameters("{}")
        time_params.AddValue("interval", self.settings["interval"])
        self.TimeInterval = KratosSolver.TimeInterval(time_params, self.model_part.ProcessInfo)

        # read assignment values
        if os.path.isfile(self.file_name+'.csv'):
            with open(self.file_name+'.csv') as f:
                #self._read_node_variables(f)
                self._read_final_position(f)

        self.AssignValueProcesses.ExecuteInitialize()
        self.FreeDofsProcesses = []
        self.FixDofsProcesses = []

        self._set_free_dofs_processes()

    def ExecuteBeforeSolutionLoop(self):
        self.AssignValueProcesses.ExecuteBeforeSolutionLoop()

    def ExecuteInitializeSolutionStep(self):
        self.AssignValueProcesses.ExecuteInitializeSolutionStep()
        if self.TimeInterval.IsInside():
            self._assign_string_position()
        #else:
            #self._assign_string_final_position()
            #for process in self.FixDofsProcesses:
            #    process.Execute()
        #
    def ExecuteFinalizeSolutionStep(self):
        self.AssignValueProcesses.ExecuteFinalizeSolutionStep()
        #if not self.TimeInterval.IsInside():
        #    for process in self.FixDofsProcesses:
        #        process.Execute()

    def ExecuteFinalize(self):
        pass
        #for process in self.FreeDofsProcesses:
        #    process.Execute()

    ###
    def _assign_string_final_position(self):
        print(" ASSIGN FINAL POSITION ")
        zero = [0.0,0.0,0.0]
        counter = 0
        for node in self.model_part.Nodes:
            if counter < len(self.values):
                node.X0 = node.X
                node.Y0 = node.Y
                node.Z0 = node.Z
                node.SetSolutionStepValue(KratosMultiphysics.KratosGlobals.GetVariable(self.variable_name),zero)
                displacement = node.GetSolutionStepValue(KratosMultiphysics.KratosGlobals.GetVariable(self.variable_name))
                #print(" Id : "+str(node.Id)+" Position ("+str(node.X)+" "+str(node.Y)+" "+str(node.Z)+") Displacement ",displacement)

            counter+=1

    def _assign_string_position(self):
        factor = 1

        if self.TimeInterval.GetRemainingSteps() > 1e-30:
            factor /= self.TimeInterval.GetRemainingSteps()
        if factor > 1 or factor < 0:
            factor = 1

        print(" ASSIGN POSITION ", factor," ", self.TimeInterval.GetRemainingSteps())
        counter = 0
        direction = ["_X","_Y","_Z"]

        for node in self.model_part.Nodes:
            if counter < len(self.values): # tube nodes as added to boundary model_parts as well
                component = 0
                current = [node.X, node.Y, node.Z]
                #print(str(counter+1)+": Node["+str(node.Id)+"]")
                for constraint in self.constraints:
                    if constraint:
                        variable_name = self.variable_name+direction[component]
                        displacement = node.GetSolutionStepValue(KratosMultiphysics.KratosGlobals.GetVariable(variable_name))
                        value = (self.values[counter][component]-current[component])*factor+displacement
                        #print(str(counter+1)+": Node["+str(node.Id)+"] Add value ", variable_name, " ", value, " ", displacement," ",current," ",self.values[counter][component]," ",factor)
                        #values = [x * factor for x in self.values[counter]]
                        node.SetSolutionStepValue(KratosMultiphysics.KratosGlobals.GetVariable(variable_name), value)
                    component+=1
                vector_displacement = node.GetSolutionStepValue(KratosMultiphysics.KratosGlobals.GetVariable(self.variable_name))
                node.X = node.X0+vector_displacement[0]
                node.Y = node.Y0+vector_displacement[1]
                node.Z = node.Z0+vector_displacement[2]
                #print(" Id : "+str(node.Id)+" Position ("+str(node.X)+" "+str(node.Y)+" "+str(node.Z)+") Displacement ",vector_displacement)
            counter+=1

    #
    def _set_free_dofs_processes(self):
        component = 0
        direction = ["_X","_Y","_Z"]
        for constraint in self.constraints:
            if constraint:
                params = KratosMultiphysics.Parameters("{}")
                params.AddEmptyValue("variable_name").SetString(self.variable_name+direction[component])
                params.AddValue("model_part_name", self.settings["model_part_name"])
                params.AddValue("flags_list", self.settings["flags_list"])
                free_dof_process = KratosSolver.FreeScalarDofProcess(self.model_part, params)
                self.FreeDofsProcesses.append(free_dof_process)
                fix_dof_process = KratosSolver.FixScalarDofProcess(self.model_part, params)
                self.FixDofsProcesses.append(fix_dof_process)
            component+=1

    def _read_final_position(self,f):
        self.values = []
        for line in f:
            if line.strip():
                val = line.split(' ')
                self.values.append([float(val[1]),float(val[2]),float(val[3])])

    def _read_node_variables(self,f):
        self.values = []
        initial = []
        current = []
        position = []
        measured = []
        counter = 1
        for line in f:
            if line.strip():
                if counter == 1:
                    val = line.split(' ')
                    previous = [self.convert_to_m(val[0], 'ft'), float(val[1]), float(val[2])]
                    position = previous
                    line = next(f)
                    val = line.split(' ')
                    current = [self.convert_to_m(val[0], 'ft'), float(val[1]), float(val[2])]
                    initial = [0.0, 0.0,-self.convert_to_m(val[0], 'ft')]
                    measured = self.convert_to_xyz(previous, current)
                    self.values.append(position)
                    counter += 1
                else:
                    previous = current
                    val = line.split(' ')
                    current = [self.convert_to_m(val[0], 'ft'), float(val[1]), float(val[2])]
                    initial = [0.0, 0.0,-self.convert_to_m(val[0], 'ft')]
                    measured = self.convert_to_xyz(previous, current)
                position = [x + y for x, y in zip(position, measured)]
                #displacement = [x - y for x, y in zip(position, initial)]
                #self.values.append(displacement)
                self.values.append(position)
                counter += 1

    def convert_to_m(self, val, unit_in):
        IS = {'inch': 0.0254, 'ft': 0.3048, 'ftKB': 304.8}
        return round(float(val.replace(',', ''))*IS[unit_in], 2)

    def convert_to_xyz(self, survey1, survey2):
        # survey [Depth, Inclination, Azimut]
        angle = math.acos(math.cos(math.radians(survey2[1]-survey1[1]))-math.sin(math.radians(
            survey1[1]))*math.sin(math.radians(survey2[1]))*(1.0-math.cos(math.radians(survey2[2]-survey1[2]))))
        ratio_factor = 1
        if angle != 0.0:
            ratio_factor = (2.0/angle)*math.tan(angle*0.5)
        depth = survey2[0]-survey1[0]
        z = -0.5*depth * \
            (math.cos(math.radians(survey1[1])) +
             math.cos(math.radians(survey2[1])))*ratio_factor
        y = 0.5*depth*(math.sin(math.radians(survey1[1]))*math.cos(math.radians(survey1[2]))+math.sin(
            math.radians(survey2[1]))*math.cos(math.radians(survey2[2])))*ratio_factor
        x = 0.5*depth*(math.sin(math.radians(survey1[1]))*math.sin(math.radians(survey1[2]))+math.sin(
            math.radians(survey2[1]))*math.sin(math.radians(survey2[2])))*ratio_factor
        return [x, y, z]
