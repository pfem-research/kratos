""" Project: SolidMechanicsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolidMechanicsApplication as KratosSolid
import KratosMultiphysics.SolversApplication as KratosSolver
import KratosMultiphysics.SolidMechanicsApplication.assign_utilities as assign_utilities

# This proces sets the value of a vector variable using a modulus and direction
# In this case, the dof constraint is false by default


def Factory(custom_settings, Model):
    if(not isinstance(custom_settings, KratosMultiphysics.Parameters)):
        raise Exception(
            "expected input shall be a Parameters object, encapsulating a json string")
    return AssignModulusAndDirectionToConditionsProcess(Model, custom_settings["Parameters"])

# All the processes python should be derived from "Process"


class AssignModulusAndDirectionToConditionsProcess(KratosMultiphysics.Process):
    def __init__(self, Model, custom_settings):
        KratosMultiphysics.Process.__init__(self)

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
             "help" : "This process assigns a modulus and direction value to a conditions vector variable",
             "model_part_name": "MODEL_PART_NAME",
             "variable_name": "VARIABLE_NAME",
             "modulus" : 0.0,
             "direction": [0.0, 0.0, 0.0],
             "compound_assignment": "direct",
             "constrained": false,
             "interval": [0.0, "End"],
             "local_axes" : {}
        }
        """)

        # trick to allow "value" to be a string or a double value
        if(custom_settings.Has("modulus")):
            if(custom_settings["modulus"].IsString()):
                default_settings["modulus"].SetString("0.0")

        # overwrite the default settings with user-provided parameters
        self.settings = custom_settings
        self.settings.ValidateAndAssignDefaults(default_settings)

        # check if variable type is a vector
        self.var = KratosMultiphysics.KratosGlobals.GetVariable(
            self.settings["variable_name"].GetString())
        if((not isinstance(self.var, KratosMultiphysics.VectorVariable)) and (not isinstance(self.var, KratosMultiphysics.Array1DVariable3))):
            raise Exception(
                "Variable type is incorrect. Must be a vector or an array_1d vector.")

        self.model = Model
        self.variable_name = self.settings["variable_name"].GetString()

        # set the value
        self.ValueManager = assign_utilities.FunctionValueManager(
            self.settings["modulus"])
        self.value = self.ValueManager.GetVectorValue(
            self.settings["direction"])

        # set ending flags
        self.interval_ended = False
        self.finalized = False

    def GetVariables(self):
        nodal_variables = [self.settings["variable_name"].GetString()]
        return nodal_variables

    def ExecuteInitialize(self):
        # set model part
        self.model_part = self.model[self.settings["model_part_name"].GetString(
        )]

        # set time interval
        #self.TimeInterval = assign_utilities.TimeIntervalManager(self.settings["interval"], self.model_part.ProcessInfo)
        time_params = KratosMultiphysics.Parameters("{}")
        time_params.AddValue("interval", self.settings["interval"])
        self.TimeInterval = KratosSolver.TimeInterval(time_params, self.model_part.ProcessInfo)

        if not self.model_part.ProcessInfo[KratosMultiphysics.IS_RESTARTED]:
            self.model_part.ProcessInfo.SetValue(
                KratosMultiphysics.INTERVAL_END_TIME, self.TimeInterval.GetEndTime())

        # set processes
        self._CreateAssignmentProcess()

        # self.TimeInterval.SetCurrentTime()
        if self.TimeInterval.IsInitial():
            self.AssignValueProcess.Execute()

    def ExecuteInitializeSolutionStep(self):
        if self.TimeInterval.IsRecoverStep():
            self._ExecuteUnAssignment()

        self._ExecuteAssignment()

    def ExecuteFinalizeSolutionStep(self):
        if not self.interval_ended:
            # self.TimeInterval.SetCurrentTime()
            if self.TimeInterval.IsEnded():
                self.interval_ended = True
                if not self.finalized:
                    self.AssignValueProcess.ExecuteFinalize()
                    self.finalized = True
    #
    def _CreateAssignmentProcess(self):

        params = KratosMultiphysics.Parameters("{}")
        params.AddValue("model_part_name", self.settings["model_part_name"])
        params.AddValue("compound_assignment",
                        self.settings["compound_assignment"])

        params.AddEmptyValue("value")
        params.__setitem__("value", self.settings["direction"])

        if self.ValueManager.IsNumeric():
            params.AddValue("variable_name", self.settings["variable_name"])
            counter = 0
            for i in self.value:
                params["value"][counter].SetDouble(i)
                counter += 1
            self.AssignValueProcess = KratosSolid.AssignVectorToConditionsProcess(
                self.model_part, params)
        else:
            # function values are assigned to a vector variable :: transformation is needed
            if(isinstance(self.var, KratosMultiphysics.Array1DVariable3)):
                variable_name = self.settings["variable_name"].GetString(
                ) + "_VECTOR"
                #print("::[--Assign_Variable--]:: "+variable_name)
                params.AddEmptyValue("variable_name")
                params["variable_name"].SetString(variable_name)
            else:
                params.AddValue("variable_name",
                                self.settings["variable_name"])

            counter = 0
            for i in self.value:
                params["value"][counter].SetDouble(i)
                counter += 1

            params.AddEmptyValue("entity_type").SetString("CONDITIONS")
            self.AssignValueProcess = KratosSolid.AssignVectorFieldToEntitiesProcess(
                self.model_part, self.ValueManager.GetCompiledFunction(), "function", self.ValueManager.IsSpatialFunction(), params)

        # in case of going to previous time step for time step reduction
        self._CreateUnAssignmentProcess(params)

    #
    def _CreateUnAssignmentProcess(self, params):
        params["compound_assignment"].SetString(assign_utilities.GetInverseAssigment(
            self.settings["compound_assignment"].GetString()))
        if self.ValueManager.IsNumeric():
            self.UnAssignValueProcess = KratosSolid.AssignVectorToConditionsProcess(
                self.model_part, params)
        else:
            self.UnAssignValueProcess = KratosSolid.AssignVectorFieldToEntitiesProcess(
                self.model_part, self.ValueManager.GetCompiledFunction(), "function", self.ValueManager.IsSpatialFunction(), params)

    #
    def _ExecuteAssignment(self):
        # self.TimeInterval.SetCurrentTime()
        if self.TimeInterval.IsInside():
            self.AssignValueProcess.Execute()
        else:
            self._ExecuteUnAssignment()

    #
    def _ExecuteUnAssignment(self):
        # self.TimeInterval.SetPreviousTime()
        # if self.TimeInterval.IsInside():
        if self.TimeInterval.WasInside():
            self.UnAssignValueProcess.Execute()
