""" Project: SolidMechanicsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics


class CompositeProblemDefaults(object):

    def __init__(self, project_parameters):
        self._project_parameters = project_parameters

        # Set echo level
        self._echo_level = 0
        if self._project_parameters.Has("problem_data"):
            problem_data = self._project_parameters["problem_data"]
            if problem_data.Has("echo_level"):
                self._echo_level = problem_data["echo_level"].GetInt()

            # Set default composite solving parts
            if problem_data.Has("problem_type"):
                if problem_data["problem_type"].GetString() == "thermo-mechanical":
                    self._set_default_composite_solving_parts()
                    self._set_default_composite_solvers()

            if problem_data.Has("problem_type"):
                if problem_data["problem_type"].GetString() == "thermo-hydro-mechanical":
                    self._set_default_composite_solving_parts(True)
                    self._set_default_composite_solvers()

    #
    def _set_default_composite_solving_parts(self, thermo_hydro_mechanical = False):

        # right now only valid for triangles-tetrahedra thermomechanical problem
        add_composite_parts = False
        if self._project_parameters.Has("model_settings"):
            if self._project_parameters["model_settings"].Has("composite_solving_parts"):
                if self._project_parameters["model_settings"]["composite_solving_parts"].size() == 0:
                    add_composite_parts = True
            else:
                composite_parts_list = KratosMultiphysics.Parameters("""
                {
                "composite_solving_parts": []
                } """)
                self._project_parameters["model_settings"].AddValue("composite_solving_parts",composite_parts_list["composite_solving_parts"])
                add_composite_parts = True

        if add_composite_parts == True:

            composite_settings = KratosMultiphysics.Parameters("""
            {
                "model_part_name": "thermal_computing_domain",
                "assign_flags": ["ACTIVE","THERMAL"],
                "composite_conditions": false,
                "transfer_entities": [{
                    "origin_model_parts_list": ["computing_domain"],
                    "entity_type": "Node"
                    }
                ],
                "generate_entities": [{
                    "origin_model_parts_list": ["computing_domain"],
                    "transfer_flags": ["NOT_RIGID"],
                    "entity_type": "Element",
                    "entity_kratos_type": "ThermalElement2D3N"
                    },{
                    "origin_model_parts_list": ["computing_domain"],
                    "transfer_flags": ["CONTACT"],
                    "entity_type": "Condition",
                    "entity_kratos_type": "ThermalContactDomainPenaltyCondition2D3N"
                    }
                ]
            } """)

            if self._project_parameters["model_settings"]["dimension"].GetDouble() == 3:
                entities = composite_settings["generate_entities"]
                for entity in entities:
                    if entity["entity_type"].GetString() == "Element":
                        entity["entity_kratos_type"].SetString(
                            "ThermalElement3D4N")
                    elif entity["entity_type"].GetString() == "Condition":
                        entity["entity_kratos_type"].SetString(
                            "ThermalContactDomainPenaltyCondition3D4N")
            if self._project_parameters["model_settings"].Has("axisymmetric"):
                if self._project_parameters["model_settings"]["axisymmetric"].GetBool():
                    entities = composite_settings["generate_entities"]
                    for entity in entities:
                        if entity["entity_type"].GetString() == "Element":
                            entity["entity_kratos_type"].SetString(
                                "AxisymThermalElement2D3N")
                        elif entity["entity_type"].GetString() == "Condition":
                            entity["entity_kratos_type"].SetString(
                                "AxisymThermalContactDomainPenaltyCondition2D3N")
            if ( thermo_hydro_mechanical):
                entities = composite_settings["generate_entities"]
                for entity in entities:
                    if entity["entity_type"].GetString() == "Element":
                        entity["entity_kratos_type"].SetString(
                            "Soil" + entity["entity_kratos_type"].GetString() )


            self._project_parameters["model_settings"]["composite_solving_parts"].Append(
                composite_settings)

            if self._echo_level > 0:
                print(self._class_prefix()+" Composite thermo-mechanical parts added")
                if self._echo_level > 1:
                    print(" PARAMETERS ", self._project_parameters["model_settings"].PrettyPrintJsonString())

    #
    def _set_default_composite_solvers(self):
        solver_type = None
        if self._project_parameters.Has("solver_settings"):
            solver_settings = self._project_parameters["solver_settings"]
            solver_type = solver_settings["solver_type"].GetString()
            if "solid_mechanics_" in solver_type:
                solver_type = solver_type.split("solid_mechanics_", 1)[1]


        if solver_type == "composite_solver":
            solvers = self._project_parameters["solver_settings"]["Parameters"]["solvers"]
            if solvers.size() == 1:
                composite_settings = KratosMultiphysics.Parameters("""
                {
                    "solver_type": "solid_mechanics_static_solver",
                    "Parameters": {
                        "time_integration_settings": {
                            "solution_type": "Quasi-static",
                            "analysis_type": "Non-linear",
                            "integration_method": "Static"
                        },
                        "solving_strategy_settings": {
                            "line_search": false,
                            "implex": false,
                            "compute_reactions": false,
                            "reform_dofs_at_each_step": false,
                            "max_iteration": 10
                        },
                        "convergence_criterion_settings": {
                            "convergence_criterion": "Residual_criterion",
                            "residual_relative_tolerance": 1e-5,
                            "residual_absolute_tolerance": 1e-9
                        },
                        "linear_solver_settings": {
                            "solver_type": "sparse_lu",
                            "tolerance": 1e-7,
                            "max_iteration": 5000,
                            "scaling": false
                        },
                        "dofs": ["TEMPERATURE"],
                        "solving_model_part": "thermal_computing_domain"
                    }

                } """)

                solvers.Append(composite_settings)

                if self._echo_level > 0:
                    print(self._class_prefix()+" Composite thermo-mechanical solver added")
                    if self._echo_level > 1:
                        print(" PARAMETERS ", self._project_parameters["model_settings"].PrettyPrintJsonString())

    @classmethod
    def _class_prefix(self):
        header = "::[-Composite Problem-]::"
        return header
