""" Project: SolidMechanicsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolidMechanicsApplication as KratosSolid
import KratosMultiphysics.SolversApplication as KratosSolver
import KratosMultiphysics.SolidMechanicsApplication.assign_utilities as assign_utilities


def Factory(custom_settings, Model):
    if(not isinstance(custom_settings, KratosMultiphysics.Parameters)):
        raise Exception(
            "expected input shall be a Parameters object, encapsulating a json string")
    return AssignScalarToNodesProcess(Model, custom_settings["Parameters"])

# All the processes derived from "Process"


class AssignScalarToNodesProcess(KratosMultiphysics.Process):
    def __init__(self, Model, custom_settings):
        KratosMultiphysics.Process.__init__(self)

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
             "help" : "This process assigns a scalar value to a scalar variable",
             "model_part_name": "MODEL_PART_NAME",
             "variable_name": "VARIABLE_NAME",
             "value": 0.0,
             "compound_assignment": "direct",
             "constrained": true,
             "interval": [0.0, "End"],
             "local_axes" : {},
             "flags_list": []
        }
        """)

        # trick to allow "value" to be a string or a double value
        if(custom_settings.Has("value")):
            if(custom_settings["value"].IsString()):
                default_settings["value"].SetString("0.0")

        # overwrite the default settings with user-provided parameters
        self.settings = custom_settings
        self.settings.ValidateAndAssignDefaults(default_settings)

        # check if variable type is a scalar or a vector component
        self._CheckVariableType(self.settings["variable_name"].GetString())

        self.model = Model
        self.variable_name = self.settings["variable_name"].GetString()

        # set the value
        self.ValueManager = assign_utilities.FunctionValueManager(
            self.settings["value"])

        self.constrained = self.settings["constrained"].GetBool()

        self.entity_type = "NODES"

    def GetVariables(self):
        nodal_variables = [self.settings["variable_name"].GetString()]
        return nodal_variables

    def ExecuteInitialize(self):
        # set model part
        self.model_part = self.model[self.settings["model_part_name"].GetString(
        )]

        # set time interval
        #self.TimeInterval = assign_utilities.TimeIntervalManager(self.settings["interval"], self.model_part.ProcessInfo)
        time_params = KratosMultiphysics.Parameters("{}")
        time_params.AddValue("interval", self.settings["interval"])
        self.TimeInterval = KratosSolver.TimeInterval(time_params, self.model_part.ProcessInfo)

        self.is_restarted = False
        if not self.model_part.ProcessInfo[KratosMultiphysics.IS_RESTARTED]:
            self.model_part.ProcessInfo.SetValue(
                KratosMultiphysics.INTERVAL_END_TIME, self.TimeInterval.GetEndTime())
        else:
            self.is_restarted = True

        # set time integration method
        self.TimeIntegrationMethod = self._SetTimeIntegration(
            self.model_part.ProcessInfo, self.variable_name)

        # set processes
        self.FixDofsProcesses = []
        self.FreeDofsProcesses = []

        params = KratosMultiphysics.Parameters("{}")
        params.AddValue("model_part_name", self.settings["model_part_name"])
        params.AddValue("variable_name", self.settings["variable_name"])
        params.AddValue("flags_list", self.settings["flags_list"])

        # print("FixFreeDofs params", params.PrettyPrintJsonString())

        if((not self.TimeInterval.IsInitial()) and self.constrained == True):
            self._SetFixAndFreeProcesses(self.TimeIntegrationMethod, params)

        params.AddValue("compound_assignment",
                        self.settings["compound_assignment"])

        self._CreateAssignmentProcess(params)

    def ExecuteBeforeSolutionLoop(self):
        # self.TimeInterval.SetCurrentTime()
        if self.TimeInterval.IsInside():
            if self.TimeInterval.IsFixingStep():
                for process in self.FixDofsProcesses:
                    process.Execute()

        if self.TimeInterval.IsInitial() and not self.is_restarted:
            self._ExecuteInitialAssignment()

    def _ExecuteInitialAssignment(self):
        self.AssignValueProcess.Execute()
        # initial assignment no time integration
        # self._IntegrateAssignment()

    def ExecuteInitializeSolutionStep(self):
        if self.TimeInterval.IsRecoverStep():
            self._ExecuteUnAssignment()

        self._ExecuteAssignment()

    def ExecuteFinalizeSolutionStep(self):
        # self.TimeInterval.SetCurrentTime()
        if self.TimeInterval.IsUnfixingStep():
            for process in self.FreeDofsProcesses:
                process.Execute()

    #
    def _ExecuteAssignment(self):
        # self.TimeInterval.SetCurrentTime()
        if self.TimeInterval.IsInside():
            if self.TimeInterval.IsFixingStep():
                for process in self.FixDofsProcesses:
                    process.Execute()
            self.AssignValueProcess.Execute()
            #self._IntegrateAssignment()

    #
    def _ExecuteUnAssignment(self):
        # self.TimeInterval.SetPreviousTime()
        # if self.TimeInterval.IsInside():
        if self.TimeInterval.WasInside():
            self.UnAssignValueProcess.Execute()
            #self._IntegrateAssignment()

    #
    def _CheckVariableType(self, name):
        self.var = KratosMultiphysics.KratosGlobals.GetVariable(name)
        if((not isinstance(self.var, KratosMultiphysics.Array1DVariable3)) and (not isinstance(self.var, KratosMultiphysics.DoubleVariable)) and (not isinstance(self.var, KratosMultiphysics.VectorVariable))):
            raise Exception(
                "Variable type is incorrect. Must be a scalar or a component")

    #
    def _IntegrateAssignment(self):
        if self.TimeIntegrationMethod != None:
            for node in self.model_part.Nodes:
                self.TimeIntegrationMethod.Assign(node)

    #
    def _SetFixAndFreeProcesses(self, TimeIntegrationMethod, params):
        if(TimeIntegrationMethod != None):
            variable_name = params["variable_name"].GetString()
            primary_variable_name = TimeIntegrationMethod.GetPrimaryVariableName()
            if(primary_variable_name != variable_name):
                params["variable_name"].SetString(primary_variable_name)
            #print(" FIX variable: ",params["variable_name"].GetString()," from ", variable_name)

        fix_dof_process = KratosSolver.FixScalarDofProcess(
            self.model_part, params)
        self.FixDofsProcesses.append(fix_dof_process)
        free_dof_process = KratosSolver.FreeScalarDofProcess(
            self.model_part, params)
        self.FreeDofsProcesses.append(free_dof_process)

    #
    def _SetTimeIntegration(self, process_info, variable_name):
        TimeIntegrationMethod = None
        time_integration_container = KratosSolver.ScalarTimeIntegrationMethods()
        if(time_integration_container.HasProcessInfo(KratosSolver.COMPONENT_TIME_INTEGRATION_METHODS, process_info)):
            time_integration_methods = time_integration_container.GetFromProcessInfo(
                KratosSolver.COMPONENT_TIME_INTEGRATION_METHODS, process_info)

            if(time_integration_methods.Has(variable_name)):
                TimeIntegrationMethod = time_integration_methods.Get(
                    variable_name).Clone()
            elif(time_integration_container.HasProcessInfo(KratosSolver.SCALAR_TIME_INTEGRATION_METHODS, process_info)):
                time_integration_methods_scalar = time_integration_container.GetFromProcessInfo(
                    KratosSolver.SCALAR_TIME_INTEGRATION_METHODS, process_info)
                if(time_integration_methods_scalar.Has(variable_name)):
                    TimeIntegrationMethod = time_integration_methods_scalar.Get(
                        variable_name).Clone()
            else:
                method_variable_name = time_integration_methods.GetMethodVariableName(
                    variable_name)
                if(method_variable_name != variable_name):
                    TimeIntegrationMethod = time_integration_methods.Get(
                        method_variable_name).Clone()

        if TimeIntegrationMethod != None:
            # set input variable
            input_variable = KratosMultiphysics.KratosGlobals.GetVariable(
                variable_name)
            TimeIntegrationMethod.SetInputVariable(input_variable)
        # else:
        #    print(self.variable_name+": No time integration ")

        return TimeIntegrationMethod

    #
    def _CreateAssignmentProcess(self, params):
        params["variable_name"].SetString(
            self.settings["variable_name"].GetString())
        if self.ValueManager.IsNumeric():
            params.AddEmptyValue("value").SetDouble(
                self.ValueManager.GetValue())
            params.AddEmptyValue("entity_type").SetString(self.entity_type)
            self.AssignValueProcess = KratosSolid.AssignScalarToEntitiesProcess(
                self.model_part, params)
        else:
            if self.ValueManager.IsCurrentValue():
                self.AssignValueProcess = KratosMultiphysics.Process()  # void process
            else:
                params.AddEmptyValue("entity_type").SetString(self.entity_type)
                self.AssignValueProcess = KratosSolid.AssignScalarFieldToEntitiesProcess(
                    self.model_part, self.ValueManager.GetCompiledFunction(), "function", self.ValueManager.IsSpatialFunction(), params)

        # in case of going to previous time step for time step reduction
        self._CreateUnAssignmentProcess(params)

    #
    def _CreateUnAssignmentProcess(self, params):
        params["compound_assignment"].SetString(assign_utilities.GetInverseAssigment(
            self.settings["compound_assignment"].GetString()))
        if self.ValueManager.IsNumeric():
            self.UnAssignValueProcess = KratosSolid.AssignScalarToEntitiesProcess(
                self.model_part, params)
        else:
            if self.ValueManager.IsCurrentValue():
                self.UnAssignValueProcess = KratosMultiphysics.Process()  # void process
            else:
                self.UnAssignValueProcess = KratosSolid.AssignScalarFieldToEntitiesProcess(
                    self.model_part, self.ValueManager.GetCompiledFunction(), "function", self.ValueManager.IsSpatialFunction(), params)
