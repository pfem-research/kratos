""" Project: SolidMechanicsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Built-in/Generic Imports
import time as timer
import sys
import os

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolidMechanicsApplication as KratosSolid
import KratosMultiphysics.SolidMechanicsApplication.analysis_utilities as analysis_utilities

import KratosMultiphysics.analysis_stage as BaseProcess

sys.stdout.flush()

class SolidAnalysisStage(BaseProcess.AnalysisStage):

    def __init__(self, analysis_model, stage_parameters):
        # Time control starts
        self.t0p,self.t0w = analysis_utilities.StartTimeControl()

        # Stage parameters
        self._stage_parameters = self._set_custom_project_parameters(stage_parameters)

        # Set echo level
        self.echo_level = 0
        if self._stage_parameters.Has("problem_data"):
            problem_data = self._stage_parameters["problem_data"]
            if problem_data.Has("echo_level"):
                self.echo_level = problem_data["echo_level"].GetInt()

        # Set the analysis model (it is not the KratosMultiphysics.Model)
        self._model = analysis_model

        # Start solver (model parts are not created yet)
        self._solver = self._get_solver()

        # Start processes (model parts are not created yet)
        self._processes = self._get_processes()

        # Set the solution step variables before importing model parts
        self._model.SetVariables(self._solver.GetVariables()+self._processes.GetVariables()+self._get_output_variables())

    def Initialize(self):
        self.main_model_part = self._model.GetMainModelPart()

        # Set problem data
        self._get_problem_data()

        # Set time settings
        self._get_time_settings()

        # Initialize Solver
        self._solver.SetEchoLevel(self.echo_level)
        self._solver.ExecuteInitialize()

        # Initiliaze processes
        self._processes.ExecuteInitialize()

        # Initialize graphical output (GiD)
        output_model_part = self._model.GetOutputModelPart()
        self._output = self._get_graphical_output(output_model_part)
        self._output.ExecuteInitialize()

        # Adaptive solution
        if self.time_process is not None:
            self.time_process.ExecuteInitialize()
            self._solver.SetAdaptiveTime(True)

        # Initialize solution Loop
        self.InitializeSolutionLoop()

    def InitializeSolutionLoop(self):
        # Processes to be executed before solution loop
        self._processes.ExecuteBeforeSolutionLoop()

        # First execution before solution loop
        self._model.ExecuteBeforeSolutionLoop()

        # First execution before solution loop
        self._solver.ExecuteBeforeSolutionLoop()
        self.process_info[KratosSolid.CONVERGENCE_ACHIEVED] = True


        # Writing a initial state results file or single file (if no restart)
        if analysis_utilities.IsNotRestarted(self.process_info):
            print(self._class_prefix()+" Print Initial State ")
            self._output.ExecuteBeforeSolutionLoop()
        else:
            # Write output results GiD: (frequency writing is controlled internally)
            self._output.PrintOutput()
            print(self._class_prefix(
            )+" [ Print Restart State ("+str(self.process_info[KratosMultiphysics.PRINTED_STEP])+") ]")


        # Print model_part and properties
        if self.echo_level > 0:
            print("")
            print(self.main_model_part)
            for properties in self.main_model_part.Properties:
                print(properties)

        print(" ")
        print(self._class_prefix()+" Analysis Stage -START- ")

        sys.stdout.flush()

    def RunSolutionLoop(self):
        # Solving the problem (time integration)
        while self.time < self.end_time:

            self.InitializeSolutionStep()

            if self.SolveSolutionStep():
                self.FinalizeSolutionStep()
            else:
                if self.time_process is not None:
                    self.FinalizeNonConvergedStep()
                else:
                    self.FinalizeSolutionStep()

            if self.echo_level >= 0:
                sys.stdout.flush()

    def PredictTimeStep(self):
        # Predict time step from time integration
        if self.time_process is not None:
            self.time_process.Execute()
        else:
            if ( self.process_info.Has(KratosSolid.ACCELERATE_TIME)):
                #do something
                acc = self.process_info[KratosSolid.ACCELERATE_TIME]
                if ( acc > 1.0 and acc < 10.0):
                    self.process_info[KratosMultiphysics.DELTA_TIME] = self.process_info[KratosMultiphysics.DELTA_TIME] * acc
            self.time += self.process_info[KratosMultiphysics.DELTA_TIME]
            self.process_info[KratosMultiphysics.STEP] += 1
            self.main_model_part.CloneTimeStep(self.time)

        self.time_step = self.process_info[KratosMultiphysics.DELTA_TIME]
        self.time = self.process_info[KratosMultiphysics.TIME]
        self.step = self.process_info[KratosMultiphysics.STEP]

        if self.echo_level >= 0:
            print("\033[32m{}\033[m".format("  [STEP:"+str(self.step)+" TIME:"+"{:.2e}".format(self.time, 5)+"] ") +
                  " (-dt:"+"{:.2e}".format(self.time_step)+"/end:"+"{:.2e}".format(self.end_time)+"-)")

    def InitializeSolutionStep(self):
        #clock_time = analysis_utilities.StartTimeMeasuring()

        # Predict time step
        self.PredictTimeStep()

        # Processes to be executed at the begining of the solution step
        self._processes.ExecuteInitializeSolutionStep()

        # Execution at the begining of the solution step
        self._model.ExecuteInitializeSolutionStep()

        # Execution at the begining of the solution step
        self._output.ExecuteInitializeSolutionStep()

        #analisys_utilities.StopTimeMeasuring(self._class_prefix(), clock_time, "Initialize Step", self.echo_level);

    def SolveSolutionStep(self):
        clock_time = analysis_utilities.StartTimeMeasuring()

        # All steps included (1)(2)(3)
        converged = self._solver.Solve()

        # Step by step (1)
        # self._solver.InitializeSolutionStep()

        # Step by step (2)
        # converged = self._solver.SolveSolutionStep()

        # Step by step (3)
        # self._solver.FinalizeSolutionStep()

        print("  (-CPU TIME:%.2f" % round((timer.process_time()-clock_time), 2)+"s-)")

        #analisys_utilities.StopTimeMeasuring(self._class_prefix(), clock_time, "Solve Step", self.echo_level)

        self.process_info[KratosSolid.CONVERGENCE_ACHIEVED] = converged

        return converged

    def FinalizeSolutionStep(self):
        #clock_time = analysis_utilities.StartTimeMeasuring()

        # Execution at the end of the solution step
        self._output.ExecuteFinalizeSolutionStep()

        # Processes to be executed at the end of the solution step
        self._processes.ExecuteFinalizeSolutionStep()

        # Execution at the end of the solution step
        self._model.ExecuteFinalizeSolutionStep()

        # Execute output at the end of the solution step
        self.OutputSolutionStep()

        self.non_converged_steps -= 1
        #analisys_utilities.StopTimeMeasuring(self._class_prefix(), clock_time, "Finalize Step", self.echo_level)

    def FinalizeNonConvergedStep(self):
        # force reduction if possible
        if self.process_info[KratosSolid.DIVERGENCE_ACHIEVED]:
            self.non_converged_steps = 0

        # continue anyway if
        finalize = False
        if self.non_converged_steps > 0 and self.time_step < self.maximum_time_step:
            finalize = True
            print(self._class_prefix() + " [ not converged step : preserving time_step ] (time-delay :",self.non_converged_steps,")")
        if self.time_step <= self.maximum_time_step * 0.0101:
            finalize = True
            print(self._class_prefix() + " [ not converged step : preserving time_step ] (minimum-reached)")

        if finalize:  # continue anyway
            self.non_converged_steps -= 1
            self.process_info[KratosSolid.CONVERGENCE_ACHIEVED] = True
            self._solver.FinalizeSolutionStep()
            self.FinalizeSolutionStep()
        else:
            self.non_converged_steps = self.time_change_delay
            print(self._class_prefix() + " [ not converged step : reducing time_step ]")

    def OutputSolutionStep(self):
        # Processes to be executed before witting the output
        self._processes.ExecuteBeforeOutputStep()

        # Execution before witting the output
        self._model.ExecuteBeforeOutputStep()

        # Write output results GiD: (frequency writing is controlled internally)
        self._print_output()

        # Processes to be executed after witting the output
        self._processes.ExecuteAfterOutputStep()

        # Execution before witting the output
        self._model.ExecuteAfterOutputStep()

    def Finalize(self):
        # Ending the problem (time integration finished)
        self._output.ExecuteFinalize()

        self._processes.ExecuteFinalize()

        print(self._class_prefix()+" Analysis Stage -END- ")
        print(" ")

        # Check solving information for any problem
        # self._solver.InfoCheck() # InfoCheck not implemented yet.
        analysis_utilities.EndTimeControl(self._class_prefix(), self.t0p, self.t0w)

    #### Main internal methods ####
    def _print_output(self):
        if self._stage_parameters.Has("output_configuration"):
            if self._output.IsOutputStep():
                self._output.PrintOutput()
                print(self._class_prefix(
                )+" [ Print Output ("+str(self.process_info[KratosMultiphysics.PRINTED_STEP])+") ]")
                # Write EigenValues
        if self.process_info.Has(KratosSolid.EIGENVALUE_VECTOR):
            current_vals = [
                ev for ev in self.main_model_part.ProcessInfo[KratosSolid.EIGENVALUE_VECTOR]]
            print(" EIGENVALUES ", current_vals)

    def _get_output_variables(self):
        output_variables = []
        if self._stage_parameters.Has("output_configuration"):
            output_settings = self._stage_parameters["output_configuration"]["result_file_configuration"]
            if output_settings.Has("nodal_results"):
                nodal_results = output_settings["nodal_results"]
                for variable in nodal_results.values():
                    output_variables.append(variable.GetString())
        return output_variables

    def _get_solver(self):
        import KratosMultiphysics
        if self._stage_parameters["solver_settings"].Has("kratos_module"):
            kratos_module_name = self._stage_parameters["solver_settings"]["kratos_module"].GetString()
        else:
            kratos_module_name = "KratosMultiphysics.SolversApplication"

        name = self._stage_parameters["solver_settings"]["solver_type"].GetString()
        if "solid_mechanics_" in name:
            name = self._stage_parameters["solver_settings"]["solver_type"].GetString().split("solid_mechanics_", 1)[1]

        solver_module_name = kratos_module_name+"." + name

        import importlib
        solver_module = importlib.import_module(solver_module_name)

        return solver_module.CreateSolver(self._stage_parameters["solver_settings"]["Parameters"], self._model.GetModel())

    def _get_problem_data(self):
        if self._is_fluid_problem(self._stage_parameters):
            if self._stage_parameters.Has("problem_data"):
                problem_data = self._stage_parameters["problem_data"]
                if problem_data.Has("gravity_vector"):
                    self.process_info = self._model.GetProcessInfo()
                    self.process_info.SetValue(KratosMultiphysics.GRAVITY, problem_data["gravity_vector"].GetVector())

    def _get_time_settings(self):
        self.process_info = self._model.GetProcessInfo()

        # Get time parameters
        if analysis_utilities.IsNotRestarted(self.process_info):
            if self._stage_parameters.Has("time_settings"):
                time_settings = self._stage_parameters["time_settings"]
                if self._stage_parameters["time_settings"].Has("time_step"):
                    self.process_info.SetValue(
                        KratosMultiphysics.DELTA_TIME, time_settings["time_step"].GetDouble())
                if self._stage_parameters["time_settings"].Has("start_time"):
                    self.process_info.SetValue(
                        KratosMultiphysics.TIME, time_settings["start_time"].GetDouble())
        else:
            if self._stage_parameters.Has("time_settings"):
                time_settings = self._stage_parameters["time_settings"]
                if self._stage_parameters["time_settings"].Has("time_step"):
                    self.process_info.SetValue(
                        KratosMultiphysics.DELTA_TIME, time_settings["time_step"].GetDouble())

        # Set time parameters
        self.step = self.process_info[KratosMultiphysics.STEP]
        self.time = self.process_info[KratosMultiphysics.TIME]
        self.time_step = self.process_info[KratosMultiphysics.DELTA_TIME]

        self.end_time = self.time + self.time_step
        self.time_process = None
        self.non_converged_steps = 0
        self.time_change_delay = 4
        self.maximum_time_step = 0
        if self._stage_parameters.Has("time_settings"):
            if self._stage_parameters["time_settings"].Has("end_time"):
                self.end_time = self._stage_parameters["time_settings"]["end_time"].GetDouble()
                self.maximum_time_step = self._stage_parameters["time_settings"]["time_step"].GetDouble()
            if self._stage_parameters["time_settings"].Has("time_process"):
                process = self._stage_parameters["time_settings"]["time_process"]
                if process.Has("steps_update_delay"):
                    self.time_change_delay = process["steps_update_delay"].GetDouble()
                process["Parameters"].AddEmptyValue(
                    "end_time").SetDouble(self.end_time)
                process["Parameters"].AddEmptyValue(
                    "start_time").SetDouble(self.time)
                process["Parameters"].AddEmptyValue(
                    "time_step").SetDouble(self.time_step)
                process["Parameters"].AddEmptyValue(
                    "model_part_name").SetString(self.main_model_part.Name)
                import importlib
                time_process_module_name = None
                if process.Has("kratos_module") and process.Has("python_module"):
                    time_process_module_name = process["kratos_module"].GetString()+"."+process["python_module"].GetString()
                if process.Has("process_module"):
                    time_process_module_name = "KratosMultiphysics."+process["process_module"].GetString()
                time_process_module = importlib.import_module(time_process_module_name)
                self.time_process = time_process_module.Factory(
                    process, self._model.GetModel())

        print(" TIME SETTINGS : (START time:",self.process_info[KratosMultiphysics.TIME]," TIME step: ",self.process_info[KratosMultiphysics.DELTA_TIME]," END time: ",str(self.end_time)+")")

    def _set_custom_project_parameters(self, parameters):
        if self._is_fluid_problem(parameters):
            import KratosMultiphysics.PfemApplication.automatic_pfem_fluid_processes as automatic_pfem_fluid_processes
            PfemDefault = automatic_pfem_fluid_processes.FluidDefaultPfemProcesses(parameters)
            return PfemDefault.AddFluidProcesses()
        else:
            return parameters

    def _is_fluid_problem(self, parameters):
        if parameters.Has("problem_data") and parameters["problem_data"].Has("domain_type"):
            if parameters["problem_data"]["domain_type"].GetString() == "Fluid":
                return True
        return False

    def _get_processes(self):
        # Obtain the list of the processes to be applied
        import KratosMultiphysics.SolidMechanicsApplication.process_handler as process_handler

        # get processes parameters
        processes_parameters = self._get_processes_parameters()

        domain_model = self._model.GetModel()
        return process_handler.ProcessHandler(domain_model, processes_parameters)

    def _get_processes_parameters(self):
        processes_parameters = KratosMultiphysics.Parameters("{}")
        processes_parameters.AddEmptyValue("echo_level").SetInt(self.echo_level)
        process_lists = ["constraints_process_list", "loads_process_list",
                         "problem_process_list", "output_process_list", "check_process_list"]
        for list_name in process_lists:
            if self._stage_parameters.Has(list_name):
                processes_parameters.AddValue(list_name, self._stage_parameters[list_name])

        return processes_parameters

    def _get_graphical_output(self, output_model_part):

        # Time parameters
        if analysis_utilities.IsRestarted(self.process_info):
            import KratosMultiphysics
            self.process_info[KratosMultiphysics.PRINTED_STEP] -= 1

        # Output settings start
        if self._stage_parameters.Has("output_configuration"):
            import KratosMultiphysics.gid_output_process
            self._output_settings = self._stage_parameters["output_configuration"]
            problem_name = "results_output"
            if self._stage_parameters["problem_data"].Has("problem_name"):
                problem_name = self._stage_parameters["problem_data"]["problem_name"].GetString()
            else:
                print(
                    " problem name not supplied -> generic name used : results_output ")
                print(self._class_prefix() +
                      " Output Ready [File: "+problem_name+".*.post.* ]")
            return KratosMultiphysics.gid_output_process.GiDOutputProcess(output_model_part, problem_name, self._output_settings)
        else:
            print(self._class_prefix()+" No Output")
            import KratosMultiphysics
            return KratosMultiphysics.Process()

    @classmethod
    def _class_prefix(self):
        header = "::[---KSM StageSolve--]::"
        return header

if __name__ == "__main__":
    Solution().Run()
