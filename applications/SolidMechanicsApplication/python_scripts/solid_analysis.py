""" Project: SolidMechanicsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Built-in/Generic Imports
import time as timer
import sys
import os

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolidMechanicsApplication as KratosSolid
import KratosMultiphysics.SolidMechanicsApplication.analysis_utilities as analysis_utilities

import KratosMultiphysics.SolidMechanicsApplication.solid_analysis_stage as BaseProcess

sys.stdout.flush()

class SolidAnalysis(BaseProcess.SolidAnalysisStage):

    def __init__(self, model, project_parameters="ProjectParameters.json", file_name=None):
        # Print starting date/time
        print(timer.ctime())

        # Time control starts
        self.t0p,self.t0w = analysis_utilities.StartTimeControl()

        # Import input
        self._stage_parameters = self._set_custom_project_parameters(self._import_project_parameters(project_parameters))

        # Set echo level
        self.echo_level = 0
        if self._stage_parameters.Has("problem_data"):
            problem_data = self._stage_parameters["problem_data"]
            if problem_data.Has("echo_level"):
                self.echo_level = problem_data["echo_level"].GetInt()

        # Set input file name
        self._set_input_file_name(file_name)

        # Set logger severity level
        analysis_utilities.SetSeverityLevel(self.echo_level)

        # Start model manager
        self._model = self._get_model(model)

        # Defining the number of threads
        self.threads = analysis_utilities.GetParallelSize()
        if self._stage_parameters.Has("problem_data"):
            problem_data = self._stage_parameters["problem_data"]
            if problem_data.Has("threads"):
                self.threads = problem_data["threads"].GetInt()
                analysis_utilities.SetParallelSize(self.threads)

        print(" ")
        print(self._class_prefix()+" [OMP USING", self.threads, "THREADS]")
        # Set solver-model-processes manager initialization
        self._initialize_and_import_project()


    def Initialize(self):
        # Set solver-model-processes manager initialization (this is done in the __init__)
        # self._initialize_and_import_project()

        # Import materials
        if analysis_utilities.IsNotRestarted(self._model.GetProcessInfo()):
            self._import_materials()

        BaseProcess.SolidAnalysisStage.Initialize(self)

    def Finalize(self):
        BaseProcess.SolidAnalysisStage.Finalize(self)
        print(timer.ctime())

    #### Main internal methods ####

    def _import_project_parameters(self, input_file):
        import KratosMultiphysics.SolidMechanicsApplication.input_manager as input_manager
        self.input_manager = input_manager.InputManager(input_file)
        return self.input_manager.GetProjectParameters()

    def _initialize_and_import_project(self):
        # Start solver
        self._solver = self._get_solver()

        # Start processes
        self._processes = self._get_processes()

        # Get Solution Step Variables
        self._set_solution_step_variables()

        # Initialize model (read and import)
        self._model.ExecuteInitialize()

        sys.stdout.flush()


    def _set_input_file_name(self, file_name):
        if file_name is not None:
            if self._stage_parameters.Has("problem_data") is False:
                void_parameters = KratosMultiphysics.Parameters("{}")
                self._stage_parameters.AddValue(
                    "problem_data", void_parameters)

            if self._stage_parameters["problem_data"].Has("problem_name"):
                self._stage_parameters["problem_data"]["problem_name"].SetString(
                    file_name)
            else:
                self._stage_parameters["problem_data"].AddEmptyValue(
                    "problem_name").SetString(file_name)

            if self._stage_parameters["model_settings"].Has("input_file_settings") is False:
                void_parameters = KratosMultiphysics.Parameters("{}")
                self._stage_parameters["model_settings"].AddValue(
                    "input_file_settings", void_parameters)

            if self._stage_parameters["model_settings"]["input_file_settings"].Has("name"):
                self._stage_parameters["model_settings"]["input_file_settings"]["name"].SetString(
                    file_name)
            else:
                self._stage_parameters["model_settings"]["input_file_settings"].AddEmptyValue(
                    "name").SetString(file_name)

    def _set_solution_step_variables(self):
        solver_variables = self._solver.GetVariables()
        self._model.SetVariables(solver_variables)

        processes_variables = self._processes.GetVariables()
        self._model.SetVariables(processes_variables)

        output_variables = self._get_output_variables()
        self._model.SetVariables(output_variables)

    def _get_model(self, Model):
        import KratosMultiphysics.SolidMechanicsApplication.model_manager as model_manager
        return model_manager.ModelManager(Model, self._stage_parameters["model_settings"])

    def _import_materials(self):
        # Assign material to model_parts (if Materials.json exists)
        import KratosMultiphysics.process_factory

        if(os.path.isfile("Materials.json") or self.input_manager.HasMaterialFile()):

            MaterialParameters = self.input_manager.GetMaterialParameters()

            if MaterialParameters.Has("material_models_list"):

                import KratosMultiphysics.ConstitutiveModelsApplication as KratosMaterials

                assign_materials_processes = self._construct_material_processes_list(MaterialParameters["material_models_list"])

                for process in assign_materials_processes:
                    process.Execute()

                self._model.CleanModel()

        elif os.path.isfile("materials.py"):  # legacy

            import KratosMultiphysics.SolidMechanicsApplication.constitutive_law_python_utility as constitutive_law_utils
            self.main_model_part = self._model.GetMainModelPart()
            self.process_info = self.main_model_part.ProcessInfo
            constitutive_law = constitutive_law_utils.ConstitutiveLawUtility(
                self.main_model_part, self.process_info[KratosMultiphysics.SPACE_DIMENSION])

            constitutive_law.Initialize()

            self._model.CleanModel()

            print("::[-----Material------]:: Reading file: materials.py ")

        else:
            raise Exception(" MATERIALS NOT DEFINED (Materials.json or Materials.py not found)")


    def _construct_material_processes_list(self, process_list):
        # build list
        list_of_processes = []
        # add not added sorted processes
        for process in process_list.values():
            list_of_processes.append(self._construct_process(process))
        return list_of_processes

    def _construct_process(self, process):
        import importlib
        import KratosMultiphysics
        process_module_name = None
        if process.Has("kratos_module") and process.Has("python_module"):
            if not '.' in process["kratos_module"].GetString():
                process_module_name = "KratosMultiphysics."+process["kratos_module"].GetString()+"."+process["python_module"].GetString()
            else:
                process_module_name = process["kratos_module"].GetString()+"."+process["python_module"].GetString()
        if process.Has("process_module"):
            process_module_name = "KratosMultiphysics."+process["process_module"].GetString()
        process_module = importlib.import_module(process_module_name)
        return(process_module.Factory(process, self._model.GetModel()))


    @classmethod
    def _class_prefix(self):
        header = "::[---KSM Simulation--]::"
        return header


if __name__ == "__main__":
    Solution().Run()
