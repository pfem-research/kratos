""" Project: SolidMechanicsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolidMechanicsApplication as KratosSolid

# This proces sets the value of a scalar variable to conditions

import KratosMultiphysics.SolidMechanicsApplication.assign_scalar_to_conditions_process as BaseProcess


def Factory(custom_settings, Model):
    if(not isinstance(custom_settings, KratosMultiphysics.Parameters)):
        raise Exception(
            "Expected input shall be a Parameters object, encapsulating a json string")
    return AssignScalarToElementsProcess(Model, custom_settings["Parameters"])


class AssignScalarToElementsProcess(BaseProcess.AssignScalarToConditionsProcess):
    def __init__(self, Model, custom_settings):
        BaseProcess.AssignScalarToConditionsProcess.__init__(
            self, Model, custom_settings)
        self.entity_type = "ELEMENTS"
