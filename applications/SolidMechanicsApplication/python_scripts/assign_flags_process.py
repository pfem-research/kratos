""" Project: SolidMechanicsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolidMechanicsApplication as KratosSolid


def Factory(custom_settings, Model):
    if(not isinstance(custom_settings, KratosMultiphysics.Parameters)):
        raise Exception(
            "expected input shall be a Parameters object, encapsulating a json string")
    return AssignFlagsProcess(Model, custom_settings["Parameters"])

# All the processes python should be derived from "Process"


class AssignFlagsProcess(KratosMultiphysics.Process):
    def __init__(self, Model, custom_settings):
        KratosMultiphysics.Process.__init__(self)

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
             "help": "This process assigns a flag to entities",
             "model_part_name": "MODEL_PART_NAME",
             "entity_type" : null,
             "flag_name": null,
             "entity_type_list": [],
             "flags_list": []
        }
        """)

        # check null value assinged
        if custom_settings.Has("entity_type"):
            if custom_settings["entity_type"].IsString():
                default_settings["entity_type"].SetString("")
        if custom_settings.Has("flag_name"):
            if custom_settings["flag_name"].IsString():
                default_settings["flag_name"].SetString("")

        # overwrite the default settings with user-provided parameters
        self.settings = custom_settings
        self.settings.ValidateAndAssignDefaults(default_settings)

        #print(" FLAGS_PROCESS ", self.settings.PrettyPrintJsonString())

        self.model = Model

    def ExecuteInitialize(self):

        # set model part
        self.model_part = self.model[self.settings["model_part_name"].GetString(
        )]

        # single entity and flag assignment
        if not self.settings["entity_type"].IsNull() and not self.settings["flag_name"].IsNull():
            flags_list = []
            flags_list.append(self._get_flag(self.settings["flag_name"].GetString()))
            KratosSolid.AssignFlagsToEntitiesProcess(self.model_part, self.settings["entity_type"].GetString(), flags_list).Execute()

        # multiple entities and flags assignment
        flags_list = []
        for flag in self.settings["flags_list"]:
            flags_list.append(self._get_flag(flag.GetString()))


    def _get_flag(self,flag_name):

            if flag_name.find("NOT_")==0:
                return KratosMultiphysics.KratosGlobals.GetFlag(flag_name[4:]).AsFalse()
            else:
                return KratosMultiphysics.KratosGlobals.GetFlag(flag_name)
