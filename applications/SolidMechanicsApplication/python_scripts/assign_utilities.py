""" Project: SolidMechanicsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""


# Built-in/Generic Imports
import sys
from math import *

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolidMechanicsApplication as KratosSolid

# Utilities
def GetInverseAssigment(compound_assignment):
    if compound_assignment == "direct":
        return "direct"
    if compound_assignment == "addition":
        return "subtraction"
    if compound_assignment == "subtraction":
        return "addition"
    if compound_assignment == "multiplication":
        return "division"
    if compound_assignment == "division":
        return "multiplication"


class compiled_time_spatial_function:
    def __init__(self, compiled_function):
        self.compiled_function = compiled_function

    def function(self, x, y, z, t):
        return eval(self.compiled_function)


class FunctionValueManager:
    def __init__(self, input_value):

        # set the value
        self.value_is_numeric = False
        self.value_is_spatial_function = True
        self.value_is_current_value = False

        self.value = 1.0
        if input_value.IsNumber():
            self.value_is_numeric = True
            self.value = input_value.GetDouble()
        else:
            self.function_expression = input_value.GetString()

            if(self.function_expression == "current"):
                self.value_is_current_value = True
            else:
                if (sys.version_info > (3, 0)):
                    self.compiled_function = compiled_time_spatial_function(
                        compile(self.function_expression, '', 'eval', optimize=2))
                else:
                    self.compiled_function = compiled_time_spatial_function(
                        compile(self.function_expression, '', 'eval'))

                if(self.function_expression.find("x") == -1 and
                   self.function_expression.find("y") == -1 and
                   self.function_expression.find("z") == -1):  # depends on time only
                    self.value_is_spatial_function = False

    def IsNumeric(self):
        return self.value_is_numeric

    def GetValue(self):
        return self.value

    def IsCurrentValue(self):
        return self.value_is_current_value

    def GetCompiledFunction(self):
        return self.compiled_function

    def IsSpatialFunction(self):
        return self.value_is_spatial_function

    def GetVectorValue(self, input_direction):
        direction = self.GetNormalizedDirection(input_direction)
        direction = [i*self.value for i in direction]
        return direction

    def GetNormalizedDirection(self, input_direction):
        # check normalized direction
        direction = []
        scalar_prod = 0
        for i in input_direction:
            value = i.GetDouble()
            direction.append(value)
            scalar_prod = scalar_prod + value*value
        norm = sqrt(scalar_prod)
        if norm != 0.0:
            direction = [i/norm for i in direction]
        else:
            direction = [i*0.0 for i in direction]
            print("WARNING::DIRECTION IS ZERO")

        return direction


class TimeIntervalManager:
    def __init__(self, input_interval, process_info):

        # set the interval
        self.interval_started = False
        self.interval_ended = False

        self.interval = []
        self.interval.append(input_interval[0].GetDouble())
        if input_interval[1].IsString():
            if(input_interval[1].GetString() == "End"):
                self.interval.append(sys.float_info.max)
        elif(input_interval[1].IsDouble() or input_interval[1].IsInt()):
            self.interval.append(input_interval[1].GetDouble())

        self.interval_string = "custom"
        if(self.interval[0] == 0.0 and self.interval[1] == 0.0):
            self.interval_string = "initial"
        elif(self.interval[0] < 0):
            self.interval_string = "start"
            self.interval[0] = 0.0

        self.process_info = process_info

        self.SetCurrentTime()

    #
    def GetStartTime(self):
        return self.interval[0]

    #
    def GetEndTime(self):
        return self.interval[1]

    #
    def GetDeltaTime(self):
        return self.process_info[KratosMultiphysics.DELTA_TIME]

    #
    def SetCurrentTime(self):
        self.delta_time = self.process_info[KratosMultiphysics.DELTA_TIME]
        self.current_time = self.process_info[KratosMultiphysics.TIME]
        self.previous_time = self.current_time-self.delta_time

    #
    def SetPreviousTime(self):
        self.delta_time = self.process_info.GetPreviousSolutionStepInfo()[
            KratosMultiphysics.DELTA_TIME]
        self.current_time = self.process_info.GetPreviousSolutionStepInfo()[
            KratosMultiphysics.TIME]
        self.previous_time = self.current_time-self.delta_time

    #
    def IsInitial(self):
        if(self.IsInside() and (self.interval_string == "initial" or self.interval_string == "start")):
            return True
        else:
            return False

    #
    def IsInside(self):
        self.SetCurrentTime()
        # arithmetic floating point tolerance
        tolerance = self.delta_time * 0.001

        if(self.current_time >= (self.interval[0] - tolerance) and self.current_time <= (self.interval[1] + tolerance)):
            self.interval_ended = False
            return True
        else:
            return False

    #
    def WasInside(self):
        self.SetPreviousTime()
        # arithmetic floating point tolerance
        tolerance = self.delta_time * 0.001

        if(self.current_time >= (self.interval[0] - tolerance) and self.current_time <= (self.interval[1] + tolerance)):
            self.interval_ended = False
            return True
        else:
            return False

    #
    def IsEnded(self):
        self.SetCurrentTime()
        # arithmetic floating point tolerance
        tolerance = self.delta_time * 0.001

        if((self.current_time + self.delta_time) > (self.interval[1] + tolerance)):
            return True
        else:
            return False

    #
    def IsFixingStep(self):

        if(self.interval_started == False):
            self.interval_started = True
            return True
        else:
            interval_time = self.process_info[KratosMultiphysics.INTERVAL_END_TIME]

            if(self.previous_time == interval_time):
                return True
            else:
                return False

    #
    def IsUnfixingStep(self):

        if(self.interval_ended == False):

            # arithmetic floating point tolerance
            tolerance = self.delta_time * 0.001

            if((self.current_time + self.delta_time) > (self.interval[1] + tolerance)):
                self.interval_ended = True
                self.process_info.SetValue(
                    KratosMultiphysics.INTERVAL_END_TIME, self.current_time)
                return True
            else:
                return False
        else:
            return False
    #

    def IsRecoverStep(self):
        if self.process_info.Has(KratosSolid.DELTA_TIME_CHANGED):
            if self.process_info[KratosSolid.DELTA_TIME_CHANGED] is True:
                return True
            else:
                return False
        else:
            return False
