""" Project: SolidMechanicsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Built-in/Generic Imports
import os
from math import sqrt

# Kratos Imports
import KratosMultiphysics


def Factory(custom_settings, Model):
    if(not isinstance(custom_settings, KratosMultiphysics.Parameters)):
        raise Exception(
            "Expected input shall be a Parameters object, encapsulating a json string")
    return NodalVariableRecordingProcess(Model, custom_settings["Parameters"])


class NodalVariableRecordingProcess(KratosMultiphysics.Process):
    #
    def __init__(self, Model, custom_settings):

        KratosMultiphysics.Process.__init__(self)

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
            "model_part_name"     : "MODEL_PART_NAME",
            "variable_name"       : "VARIABLE_NAME",
            "output_file_name"    : "FILE_NAME",
            "single_entities"     : false,
            "operation"           : "Sum"
        }
        """)

        # overwrite the default settings with user-provided parameters
        self.settings = custom_settings
        self.settings.ValidateAndAssignDefaults(default_settings)

        # set up model
        self.model = Model

        # sum all entities
        self.single_entities = self.settings["single_entities"].GetBool()

        # restart path
        self.problem_path = os.getcwd()

        # variable
        self.variable_name = self.settings["variable_name"].GetString()
        self.kratos_variable = KratosMultiphysics.KratosGlobals.GetVariable(
            self.variable_name)

        # operations : Sum,Max,Min,Mean  when single_entities is false
        self.operations = {"Sum": "Total", "Max": "Maximum",
                           "Min": "Minimum", "Mean": "Mean"}

        self.echo_level = 1
        if(self.echo_level > 0):
            print(self._class_prefix()+" Ready")

    #
    def GetVariables(self):
        nodal_variables = [self.settings["variable_name"].GetString()]
        return nodal_variables

    #
    def ExecuteInitialize(self):

        self.model_part = self.model[self.settings["model_part_name"].GetString(
        )]

        # Set path and headers
        if self.model_part.ProcessInfo[KratosMultiphysics.IS_RESTARTED]:

            current_step = self.GetCurrentStep()

            self.recording_path_list = []

            if(not self.single_entities):
                self.recording_path = os.path.join(
                    self.problem_path, self.settings["output_file_name"].GetString() + ".post.csv")
                self.CleanPostRestartData(self.recording_path, current_step)

            else:
                for node in self.model_part.GetNodes():
                    self.recording_path = os.path.join(self.problem_path, self.settings["output_file_name"].GetString(
                    ) + "_node_" + str(node.Id) + ".post.csv")

                    self.recording_path_list.append(self.recording_path)

                    self.CleanPostRestartData(
                        self.recording_path, current_step)

        else:
            self.InitializeRecordingHeaders()

    #
    def InitializeRecordingHeaders(self):

        self.recording_path_list = []

        if(not self.single_entities):

            first_node = self.first(self.model_part.GetNodes())

            self.recording_path = os.path.join(
                self.problem_path, self.settings["output_file_name"].GetString() + ".post.csv")

            line_header = "R,STEP,TIME,AREA"

            line_header = self.GetHeaderType(first_node,line_header,self.settings["operation"].GetString())

            line_header = line_header + "\n"

            if(os.path.exists(self.recording_path) == True):
                source = self.recording_path
                os.remove(source)

            # print file header
            recording_file = open(self.recording_path, "w")
            recording_file.write(line_header)
            recording_file.close()

        else:

            for node in self.model_part.GetNodes():
                self.recording_path = os.path.join(self.problem_path, self.settings["output_file_name"].GetString(
                ) + "_node_" + str(node.Id) + ".post.csv")

                self.recording_path_list.append(self.recording_path)

                line_header = "R,STEP,TIME"

                line_header = self.GetHeaderType(node,line_header,self.settings["operation"].GetString())

                line_header = line_header + "\n"

                if(os.path.exists(self.recording_path) == True):
                    source = self.recording_path
                    os.remove(source)

                # print file header
                recording_file = open(self.recording_path, "w")
                recording_file.write(line_header)
                recording_file.close()

    #
    def GetHeaderType(self, node, line_header, operation):

        variable_value = node.GetSolutionStepValue(self.kratos_variable)

        if hasattr(variable_value, "__len__") and len(variable_value) == 3:
            if operation == "Sum" or operation == "Mean":
                return (line_header + "," + str(self.operations[self.settings["operation"].GetString()]) + "_" + str(self.variable_name) + "," + str(self.variable_name) + "_X," + str(self.variable_name) + "_Y," + str(self.variable_name) + "_Z" + ",SPECIFIC_" + str(self.variable_name))
            elif operation == "Max" or operation == "Min":
                return (line_header + "," + str(self.operations[self.settings["operation"].GetString()]) + "_" + str(self.variable_name) + "," + "X," + "Y," + "Z")
        else:
            if operation == "Sum" or operation == "Mean":
                return (line_header + "," + str(self.operations[self.settings["operation"].GetString()]) + "_" + str(self.variable_name) + ",SPECIFIC_" + str(self.variable_name))
            elif operation == "Max" or operation == "Min":
                return (line_header + "," + str(self.operations[self.settings["operation"].GetString()]) + "_" + str(self.variable_name) + "," + "X," + "Y," + "Z")


    # Reconstruct .list and .csv (last build using a restart ID to be rebuild)
    #
    def CleanPostRestartData(self, file_path, restart_step):

        if os.path.exists(file_path):

            text = "R," + str(restart_step) + ","

            # if printing all steps:
            # line_number = restart_step+1

            line_number = self.get_line_number(text, file_path)

            if(line_number != None):
                self.truncate_file_in_line(line_number, file_path)
                print(" Text found: ", text, " in line ",
                      line_number, "....  cleaned")

    #
    @classmethod
    def truncate_file_in_line(self, line_number, file_path):

        source = open(file_path, "r+")

        source.seek(0)

        for line in range(line_number):
            source.readline()

        line_number = source.tell()
        source.truncate(line_number)
        source.close()

    #
    @classmethod
    def get_line_number(self, text, file_path):

        with open(file_path) as f:
            for i, line in enumerate(f, 1):
                if text in line:
                    return i

    ###

    #
    def ExecuteFinalizeSolutionStep(self):
        self.RecordStepVariable()

    ###

    #

    def GetCurrentTime(self):
        return self.model_part.ProcessInfo[KratosMultiphysics.TIME]

    #
    def GetCurrentStep(self):
        return self.model_part.ProcessInfo[KratosMultiphysics.STEP]

    #
    def GetCurrentArea(self):
        #contact area
        condition_variable = KratosMultiphysics.KratosGlobals.GetVariable("CONTACT_AREA")
        #model part
        main_model_part = self.model_part.GetParentModelPart()
        return self.GetConditionVariable(main_model_part,condition_variable)

    #
    def GetVectorVariableValueOperation(self, nodal_variable, operation):

        if operation == "Sum":
            return self.GetVectorVariableValueSum(nodal_variable)
        elif operation == "Max":
            return self.GetVectorVariableValueMax(nodal_variable)
        elif operation == "Min":
            return self.GetVectorVariableValueMin(nodal_variable)
        elif operation == "Mean":
            return self.GetVectorVariableValueMean(nodal_variable)

    #
    def GetScalarVariableValueOperation(self, nodal_variable, operation):

        if operation == "Sum":
            return self.GetScalarVariableValueSum(nodal_variable)
        elif operation == "Max":
            return self.GetScalarVariableValueMax(nodal_variable)
        elif operation == "Min":
            return self.GetScalarVariableValueMin(nodal_variable)
        elif operation == "Mean":
            return self.GetScalarVariableValueMean(nodal_variable)

    #
    def GetVectorVariableValueSum(self, nodal_variable):

        variable_value=[0.0, 0.0, 0.0]
        for node in self.model_part.GetNodes():
            nodal_value=node.GetSolutionStepValue(nodal_variable)
            for i in range(len(nodal_value)):
                variable_value[i] += nodal_value[i]

        return self.GetVariableModulus(variable_value), variable_value

    #
    def GetVectorVariableValueMean(self, nodal_variable):

        variable_value=[0.0, 0.0, 0.0]
        total_value=[0.0, 0.0, 0.0]
        for node in self.model_part.GetParentModelPart().GetNodes():
            nodal_value=node.GetSolutionStepValue(nodal_variable)
            for i in range(len(nodal_value)):
                total_value[i] += nodal_value[i]
        for node in self.model_part.GetNodes():
            nodal_value=node.GetSolutionStepValue(nodal_variable)
            for i in range(len(nodal_value)):
                variable_value[i] += nodal_value[i]
        for i in range(len(variable_value)):
            variable_value[i]=variable_value[i]/total_value[i]

        return self.GetVariableModulus(variable_value), variable_value

    #
    def GetVectorVariableValueMax(self, nodal_variable):

        value_position=[0.0, 0.0, 0.0]
        max_value=0
        for node in self.model_part.GetNodes():
            nodal_value=self.GetVariableModulus(
                node.GetSolutionStepValue(nodal_variable))
            if nodal_value > max_value:
                max_value=nodal_value
                value_position[0]=node.X
                value_position[1]=node.Y
                value_position[2]=node.Z

        return max_value, value_position

    #
    def GetVectorVariableValueMin(self, nodal_variable):

        value_position=[0.0, 0.0, 0.0]
        min_value=1e30
        for node in self.model_part.GetNodes():
            nodal_value=self.GetVariableModulus(
                node.GetSolutionStepValue(nodal_variable))
            if nodal_value < min_value:
                min_value=nodal_value
                value_position[0]=node.X
                value_position[1]=node.Y
                value_position[2]=node.Z

        return min_value, value_position

    #
    def GetScalarVariableValueSum(self, nodal_variable):

        variable_value=0
        for node in self.model_part.GetNodes():
            nodal_value=node.GetSolutionStepValue(nodal_variable)
            variable_value=variable_value + nodal_value

        return variable_value, None
    #
    def GetScalarVariableValueMean(self, nodal_variable):

        variable_value=0
        total_value=0
        for node in self.model_part.GetParentModelPart().GetNodes():
            nodal_value=node.GetSolutionStepValue(nodal_variable)
            total_value += nodal_value
        for node in self.model_part.GetNodes():
            nodal_value=node.GetSolutionStepValue(nodal_variable)
            variable_value += nodal_value

        variable_value /= total_value

        return variable_value, None

    #
    def GetScalarVariableValueMax(self, nodal_variable):

        value_position=[0.0, 0.0, 0.0]
        max_value=0
        for node in self.model_part.GetNodes():
            nodal_value=node.GetSolutionStepValue(nodal_variable)
            if nodal_value > max_value:
                max_value=nodal_value
                value_position[0]=node.X
                value_position[1]=node.Y
                value_position[2]=node.Z

        return max_value, value_position

    #
    def GetScalarVariableValueMin(self, nodal_variable):

        value_position=[0.0, 0.0, 0.0]
        min_value=1e30
        for node in self.model_part.GetNodes():
            nodal_value=node.GetSolutionStepValue(nodal_variable)
            if nodal_value < min_value:
                min_value=nodal_value
                value_position[0]=node.X
                value_position[1]=node.Y
                value_position[2]=node.Z

        return min_value, value_position

    #
    def GetConditionVariable(self, model_part, condition_variable):
        variable_value=0
        for condition in model_part.GetConditions():
            condition_values = condition.CalculateOnIntegrationPoints(condition_variable,self.model_part.ProcessInfo)
            for value in condition_values:
                variable_value = variable_value + value

        return variable_value

    #
    @classmethod
    def GetVariableModulus(self, nodal_variable):

        modulus=0
        for var in nodal_variable:
            modulus=modulus + var * var

        return sqrt(modulus)

    #
    @classmethod
    def first(self, iterable, condition = lambda x: True):

        for item in iterable:
            if condition(item):
                return item

        raise ValueError('No satisfactory value found')

    #
    def RecordStepVariable(self):

        current_time= self.GetCurrentTime()
        current_step= self.GetCurrentStep()

        if(not self.single_entities):

            first_node= self.first(self.model_part.GetNodes())

            current_area = self.GetCurrentArea()

            line_value= "R," + str(current_step) + "," + str(current_time) + "," + str(current_area)

            variable_value= first_node.GetSolutionStepValue(self.kratos_variable)

            if(hasattr(variable_value, "__len__")):
                variable_value, position_vector= self.GetVectorVariableValueOperation(
                    self.kratos_variable, self.settings["operation"].GetString())
            else:
                variable_value, position_vector=self.GetScalarVariableValueOperation(
                    self.kratos_variable, self.settings["operation"].GetString())

            if variable_value != None:
                if(hasattr(variable_value, "__len__")):
                    if(len(variable_value) == 3):
                        line_value=line_value + "," + str(variable_value[0]) + "," + str(variable_value[1]) + "," + str(variable_value[2])
                    else:
                        line_value=line_value + "," + str(variable_value)
                else:
                    line_value=line_value + "," + str(variable_value)

            if position_vector != None:
                if(hasattr(position_vector, "__len__")):
                    if(len(position_vector) == 3):
                        line_value=line_value + "," + str(position_vector[0]) + "," + str(
                                position_vector[1]) + "," + str(position_vector[2])
                    else:
                        line_value=line_value + "," + str(position_vector)
                else:
                    line_value=line_value + "," + str(position_vector)

            if variable_value != None:
                if(not hasattr(variable_value, "__len__") or len(variable_value) != 3):
                    if(current_area!=0):
                        line_value=line_value + "," + str(variable_value/current_area)
                    else:
                        line_value=line_value + "," + str(0)

            line_value=line_value + "\n"

            recording_file=open(self.recording_path, "a")
            recording_file.write(line_value)
            recording_file.close()

        else:

            counter=0
            for node in self.model_part.GetNodes():

                self.recording_path=self.recording_path_list[counter]

                line_value="R," + str(current_step) + "," + str(current_time)

                variable_value=node.GetSolutionStepValue(
                    self.kratos_variable)

                if(hasattr(variable_value, "__len__")):
                    if(len(variable_value) == 3):
                        variable_modulus=self.GetVariableModulus(
                            variable_value)
                        line_value=line_value + "," + str(variable_modulus) + "," + str(
                            variable_value[0]) + "," + str(variable_value[1]) + "," + str(variable_value[2])
                    else:
                        line_value=line_value + "," + str(variable_value)
                else:
                    line_value=line_value + "," + str(variable_value)

                line_value=line_value + "\n"

                recording_file=open(self.recording_path, "a")
                recording_file.write(line_value)
                recording_file.close()
                counter += 1

    @classmethod
    def _class_prefix(self):
        header="::[--Record_Variable--]::"
        return header
