""" Project: SolidMechanicsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolidMechanicsApplication as KratosSolid

def Factory(custom_settings, Model):
    if(not isinstance(custom_settings, KratosMultiphysics.Parameters)):
        raise Exception(
            "expected input shall be a Parameters object, encapsulating a json string")
    return CompositePartsProcess(Model, custom_settings["Parameters"])

# All the processes python should be derived from "Process"
class CompositePartsProcess(KratosMultiphysics.Process):
    def __init__(self, Model, custom_settings):

        KratosMultiphysics.Process.__init__(self)

        default_settings = KratosMultiphysics.Parameters("""
        {
            "model_part_name": "thermal_computing_domain",
            "assign_flags": ["ACTIVE","THERMAL"],
            "composite_conditions": false,
            "transfer_entities": [{
                "origin_model_parts_list": ["computing_domain"],
                "entity_type": "Node"
                }
            ],
            "generate_entities": [{
                "origin_model_parts_list": ["computing_domain"],
                "transfer_flags": ["NOT_RIGID"],
                "entity_type": "Element",
                "entity_kratos_type": "ThermalElement2D3N"
                },{
                "origin_model_parts_list": ["computing_domain"],
                "transfer_flags": ["CONTACT"],
                "entity_type": "Condition",
                "entity_kratos_type": "ThermalContactDomainPenaltyCondition2D3N"
                }
            ]
        }
        """)

        # Overwrite the default settings with user-provided parameters
        self.settings = custom_settings
        self.settings.ValidateAndAssignDefaults(default_settings)

        # Set void model
        self.model = Model

    ########

    #
    def ExecuteInitialize(self):
        # set model part
        self.main_model_part = self.model[self.model.GetModelPartNames()[0]].GetParentModelPart()

        # Process info
        self.process_info = self.main_model_part.ProcessInfo

        # Check_element types
        self._check_element_dimension()

        # build composite solving parts
        self._build_composite_solving_parts()
    #
    def ExecuteBeforeSolutionLoop(self):
        self._update_composite_solving_parts()

    #
    def ExecuteInitializeSolutionStep(self):
        if self._update_solving_parts():
            self._update_composite_solving_parts()
    #
    def ExecuteFinalizeSolutionStep(self):
        pass
    #
    def ExecuteBeforeOutputStep(self):
        if self._domain_parts_changed():
            self._clean_composite_solving_parts()
        if self._update_solving_parts():
            self._update_composite_solving_parts()
    #
    def ExecuteAfterOutputStep(self):
        if self._update_solving_parts():
            self._update_composite_solving_parts()

    ########

    def GetComputingModelPart(self):
        return self.main_model_part.GetSubModelPart("computing_domain")

    #### internal methods ####
    #
    def _check_element_dimension(self):
        if self.process_info[KratosMultiphysics.SPACE_DIMENSION] == 3:
            entities = self.settings["generate_entities"]
            for entity in entities:
                if entity["entity_type"].GetString() == "Element":
                    entity["entity_kratos_type"].SetString(
                        "ThermalElement3D4N")
                elif entity["entity_type"].GetString() == "Condition":
                    entity["entity_kratos_type"].SetString(
                        "ThermalContactDomainPenaltyCondition3D4N")

    def _build_composite_solving_parts(self):
        self.current_update_time = self.process_info[KratosMultiphysics.TIME]
        print(self._class_prefix()+" Build Part: "+ self.settings["model_part_name"].GetString())
        self.solving_part_transfer = KratosSolid.TransferSolvingModelPartProcess(self.main_model_part, self.settings)

        # Add extra conditions from other submodelparts that are "THERMAL"
        self.main_model_part = self.model[self.model.GetModelPartNames()[0]].GetParentModelPart()
        transfer_process = KratosSolid.TransferEntitiesProcess(
            self.main_model_part.GetSubModelPart( self.settings["model_part_name"].GetString()), self.main_model_part, "Conditions", [KratosMultiphysics.THERMAL] )
        transfer_process.Execute()

        #
    def _update_composite_solving_parts(self):
        self.current_update_time = self.process_info[KratosMultiphysics.TIME]
        #print(self._class_prefix()+" Update Solving Parts")
        self.solving_part_transfer.Execute()

        transfer_process = KratosSolid.TransferEntitiesProcess(
            self.main_model_part.GetSubModelPart( self.settings["model_part_name"].GetString()), self.main_model_part, "Conditions", [KratosMultiphysics.THERMAL] )
        transfer_process.Execute()

    #
    def _clean_composite_solving_parts(self):
        #print(self._class_prefix()+" Clean Solving Parts")
        self.solving_part_transfer.ExecuteFinalizeSolutionStep()

    #
    def _update_solving_parts(self):
        update_time = False
        if self._domain_parts_updated():
            update_time = not self._check_current_time_step(self.current_update_time)

        return update_time

    #
    def _domain_parts_changed(self):
        update_time = False
        if not update_time and self.process_info.Has(KratosSolid.MESHING_STEP_TIME):
            update_time = self._check_current_time_step(
                self.process_info[KratosSolid.MESHING_STEP_TIME])
            #print(" MESHING_STEP_TIME ",self.process_info[KratosSolid.MESHING_STEP_TIME], update_time)

        if not update_time and self.process_info.Has(KratosSolid.CONTACT_STEP_TIME):
            update_time = self._check_current_time_step(
                self.process_info[KratosSolid.CONTACT_STEP_TIME])
            #print(" CONTACT_STEP_TIME ",self.process_info[KratosSolid.CONTACT_STEP_TIME], update_time)

        return update_time

    #
    def _domain_parts_updated(self):
        update_time = False
        if not self._is_not_restarted():
            if self.process_info.Has(KratosSolid.RESTART_STEP_TIME):
                update_time = self._check_current_time_step(
                    self.process_info[KratosSolid.RESTART_STEP_TIME])
                #print(" RESTART_STEP_TIME ",self.process_info[KratosSolid.RESTART_STEP_TIME], update_time)

        if not update_time and self.process_info.Has(KratosSolid.MESHING_STEP_TIME):
            update_time = self._check_previous_time_step(
                self.process_info[KratosSolid.MESHING_STEP_TIME])
            #print(" MESHING_STEP_TIME ",self.process_info[KratosSolid.MESHING_STEP_TIME], update_time)

        if not update_time and self.process_info.Has(KratosSolid.CONTACT_STEP_TIME):
            update_time = self._check_previous_time_step(
                self.process_info[KratosSolid.CONTACT_STEP_TIME])
            #print(" CONTACT_STEP_TIME ",self.process_info[KratosSolid.CONTACT_STEP_TIME], update_time)

        return update_time
    #

    def _check_current_time_step(self, step_time):
        current_time = self.process_info[KratosMultiphysics.TIME]
        delta_time = self.process_info[KratosMultiphysics.DELTA_TIME]
        # arithmetic floating point tolerance
        tolerance = delta_time * 0.001

        if(step_time > current_time-tolerance and step_time < current_time+tolerance):
            return True
        else:
            return False
    #

    def _check_previous_time_step(self, step_time):
        current_time = self.process_info[KratosMultiphysics.TIME]
        delta_time = self.process_info[KratosMultiphysics.DELTA_TIME]
        previous_time = current_time - delta_time

        # arithmetic floating point tolerance
        tolerance = delta_time * 0.001

        if(step_time > previous_time-tolerance and step_time < previous_time+tolerance):
            return True
        else:
            return False
    #

    def _is_not_restarted(self):
        if self.process_info[KratosMultiphysics.IS_RESTARTED]:
            return False
        else:
            return True
    #
    @classmethod
    def _class_prefix(self):
        header = "::[-Composite Process-]::"
        return header
