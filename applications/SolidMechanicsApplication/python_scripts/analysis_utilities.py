""" Project: SolidMechanicsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Built-in/Generic Imports
import time as timer
import sys
import os

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolidMechanicsApplication as KratosSolid

# Utilities

def StartTimeControl():
    sys.stdout.flush()
    # Measure process time and wall time
    return timer.process_time(), timer.perf_counter()

def EndTimeControl(prefix, t0p, t0w):
    # Measure process time
    tfp = timer.process_time()
    # Measure wall time
    tfw = timer.perf_counter()
    print(prefix+" [Elapsed Time = %.2f" % (tfw - t0w),
          "seconds] (%.2f" % (tfp - t0p), "seconds of cpu/s time)")


def IsRestarted(process_info):
    if process_info[KratosMultiphysics.IS_RESTARTED]:
        return True
    else:
        return False
def IsNotRestarted(process_info):
    return not IsRestarted(process_info)

def SetSeverityLevel(echo_level):
    # Set echo level
    severity = KratosMultiphysics.Logger.Severity.WARNING
    if echo_level == 1:
        severity = KratosMultiphysics.Logger.Severity.INFO
    elif echo_level == 2:
        severity = KratosMultiphysics.Logger.Severity.DETAIL
    elif echo_level == 3:
        severity = KratosMultiphysics.Logger.Severity.DEBUG
    elif echo_level == 4:
        severity = KratosMultiphysics.Logger.Severity.TRACE

    KratosMultiphysics.Logger.GetDefaultOutput().SetSeverity(severity)

def SetParallelSize(num_threads):
    KratosMultiphysics.ParallelUtilities.SetNumThreads(int(num_threads))

def GetParallelSize():
    return KratosMultiphysics.ParallelUtilities.GetNumThreads()

def StartTimeMeasuring():
    # Measure process time
    time_ip = timer.process_time()
    return time_ip

def StopTimeMeasuring(prefix, time_ip, process, echo_level):
    # Measure process time
    time_fp = timer.process_time()
    if echo_level > 1:
        used_time = time_fp - time_ip
        print(prefix+" [ %.2f" %
              round(used_time, 2), "s", process, " ] ")
