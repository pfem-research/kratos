""" Project: SolidMechanicsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Built-in/Generic Imports
import os

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolidMechanicsApplication as KratosSolid


class RestartUtility:
    #
    def __init__(self, model_part, file_name):

        # set up model_part
        self.model_part = model_part

        # restart path
        self.restart_file_name = file_name
        self.restart_path = os.path.join(os.getcwd(), self.restart_file_name)

        # label variables
        self.label_type = "step"

        # control variables
        self.output_control_type = "time"
        self.output_time_step = 0
    
        # counters
        self.printed_step_count = 0
        self.step_count = 0
        self.next_output = 0

        self.echo_level = 1
        if(self.echo_level > 0):
            print(self._class_prefix()+" Ready")

    ###
    #
    def SetFileLabelType(self, label):
        self.label_type = label    
    #
    def SetOutputControl(self, type, time_step):
        self.output_control_type = type 
        self.output_time_step = time_step
        # Set current time parameters
        if self.model_part.ProcessInfo[KratosMultiphysics.IS_RESTARTED]:
            self.step_count = self.model_part.ProcessInfo[KratosMultiphysics.STEP]
            self.printed_step_count = self.model_part.ProcessInfo[
                KratosMultiphysics.PRINTED_RESTART_STEP]

            if self.output_control_type == "time":
                self.next_output = self._get_pretty_time(
                    self.model_part.ProcessInfo[KratosMultiphysics.TIME])
            else:
                self.next_output = self.model_part.ProcessInfo[KratosMultiphysics.STEP]

            self._schedule_next_output()  # loaded condides with the saved
        else:
            self.next_output = self.output_time_step
    #
    def UpdateStepCounter(self):
        self.step_count += 1    
    #
    def LoadRestart(self, label):
        restart_file = self.restart_path + "__" + str(label)
        if(os.path.exists(restart_file+".rest") == False):
            raise Exception("Restart file not found: "+restart_file+".rest")
            
        print("   Loading Restart file: ",self.restart_file_name+"__" +str(label)+".rest ")

        serializer = serializer = self._get_serializer(restart_file)
        serializer.Load(self.model_part.Name, self.model_part)

        self.model_part.ProcessInfo[KratosMultiphysics.IS_RESTARTED] = True
        self.model_part.ProcessInfo[KratosSolid.RESTART_STEP_TIME] = self.model_part.ProcessInfo[KratosMultiphysics.TIME]

    #
    def SaveRestart(self):
        if(self._is_restart_step()):
            # Print the output
            time = self._get_pretty_time(
                self.model_part.ProcessInfo[KratosMultiphysics.TIME])
            step = self.model_part.ProcessInfo[KratosMultiphysics.STEP]
            self.printed_step_count += 1
            self.model_part.ProcessInfo[KratosMultiphysics.PRINTED_RESTART_STEP] = self.printed_step_count
            if self.label_type == "time":
                label = time
            else:
                label = self.printed_step_count

            if(self.echo_level > 0):
                print(self._class_prefix() +
                    " SAVE [step:"+str(step)+"] [label:"+str(label)+"]")

            restart_file = self.restart_path + "__" + str(label)

            serializer = self._get_serializer(restart_file)
            serializer.Save(self.model_part.Name, self.model_part)

            # schedule next output
            self._schedule_next_output()  

    #
    def CleanPreviousResults(self):
        print(self._class_prefix()+" Clean Previous result files ")
        self._clean_previous_file_types(self._get_file_endings())
    #
    def CleanPosteriorResults(self):
        if self.label_type == "time":
            restart_step = self._get_pretty_time(self.model_part.ProcessInfo[KratosMultiphysics.TIME])
        else:
            restart_step = self.model_part.ProcessInfo[KratosMultiphysics.STEP]
            if self.model_part.ProcessInfo[KratosMultiphysics.PRINTED_STEP] > restart_step:
                restart_step = self.model_part.ProcessInfo[KratosMultiphysics.PRINTED_STEP] 

        print(self._class_prefix()+" Clean post restart files [STEP:"+str(restart_step)+"]")
        # remove posterior results after restart:
        file_endings = [".post.bin", ".post.msh", ".post.res"]
        self._clean_posterior_file_types(restart_step, file_endings)
        # remove posterior graphs after restart:
        file_endings = [".graph.png", ".rest"]
        self._clean_posterior_file_types(restart_step+1, file_endings)

        # Reconstruct .list and .csv (last build using a restart ID to be rebuild)

    ###
    def _get_file_endings(self):
        return [".post.bin", ".post.msh", ".post.res", 
                ".graph.png", ".post.lst", ".post.csv", ".rest"]
    #
    def _get_trace_type(self):
        # set serializer flag
        # return KratosMultiphysics.SerializerTraceType.SERIALIZER_TRACE_ALL   # ascii
        # return KratosMultiphysics.SerializerTraceType.SERIALIZER_TRACE_ERROR # ascii
        return KratosMultiphysics.SerializerTraceType.SERIALIZER_NO_TRACE  # binary

    #
    def _get_serializer(self, restart_file):
        serializer = KratosMultiphysics.FileSerializer(restart_file, self._get_trace_type())
        serializer.Set(KratosMultiphysics.Serializer.SHALLOW_GLOBAL_POINTERS_SERIALIZATION)
        return serializer
    #
    def _schedule_next_output(self):
        if(self.output_time_step > 0.0):  # note: if == 0 always active
            if self.output_control_type == "time":
                time = self._get_pretty_time(
                    self.model_part.ProcessInfo[KratosMultiphysics.TIME])
                while(self._get_pretty_time(self.next_output) <= time):
                    self.next_output += self.output_time_step
            else:
                while(self.next_output <= self.step_count):
                    self.next_output += self.output_time_step
    #
    def _is_restart_step(self):
        if self.output_control_type == "time":
            return (self._get_pretty_time(self.model_part.ProcessInfo[KratosMultiphysics.TIME]) >= self._get_pretty_time(self.next_output))
        else:
            return (self.step_count >= self.next_output)

    #
    def _is_int(self, s):
        try:
            int(s)
            return True
        except ValueError:
            return False

    #
    def _clean_previous_file_types(self, file_endings):
        problem_path = os.getcwd()
        for file_end in file_endings:
            filelist = [f for f in os.listdir(
                problem_path) if f.endswith(file_end)]

            for f in filelist:
                try:
                    os.remove(f)
                except OSError:
                    pass

    #
    def _clean_posterior_file_types(self, restart_step, file_endings):
        problem_path = os.getcwd()
        filelist = []
        for file_end in file_endings:
            for f in os.listdir(problem_path):
                if(f.endswith(file_end)):
                    # if f.name = problem_tested_145.post.bin
                    file_parts = f.split('_') # you get ["problem","tested","145.post.bin"]
                    end_parts = file_parts[len(file_parts)-1].split(".") # you get ["145","post","bin"]
                    print_id = end_parts[0]  # you get "145"

                    if(self._is_int(print_id)):
                        if(int(print_id) > int(restart_step)):
                            filelist.append(f)
        for f in filelist:
            try:
                os.remove(f)
            except OSError:
                pass

    #
    def _get_pretty_time(self, time):
        pretty_time = "{0:.12g}".format(time)
        pretty_time = float(pretty_time)
        return pretty_time
    #
    @classmethod
    def _class_prefix(self):
        header = "::[---Restart Utils---]::"
        return header
