""" Project: SolidMechanicsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolidMechanicsApplication as KratosSolid


def Factory(settings, Model):
    if(not isinstance(settings, KratosMultiphysics.Parameters)):
        raise Exception(
            "Expected input shall be a Parameters object, encapsulating a json string")
    return RestartProcess(Model, settings["Parameters"])


class RestartProcess(KratosMultiphysics.Process):
    #
    def __init__(self, Model, custom_settings):

        KratosMultiphysics.Process.__init__(self)

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
            "model_part_name"     : "DEFINE_MODEL_PART_NAME",
            "save_restart"        : true,
            "restart_file_name"   : "problem_name",
            "restart_file_label"  : "step",
            "output_control_type" : "step",
            "output_frequency"    : 1.0,
            "json_output"         : false
        }
        """)

        # overwrite the default settings with user-provided parameters
        self.settings = custom_settings

        self.settings.ValidateAndAssignDefaults(default_settings)

        self.save_restart = self.settings["save_restart"].GetBool()

        # set up model
        self.model = Model

        self.echo_level = 1
        if(self.echo_level > 0):
            print(self._class_prefix()+" Ready")

    ###
    #
    def ExecuteInitialize(self):

        self.model_part = self.model[self.settings["model_part_name"].GetString()]

        import KratosMultiphysics.SolidMechanicsApplication.restart_python_utility as restart_utility
        self.restart_utility = restart_utility.RestartUtility(self.model_part, self.settings["restart_file_name"].GetString())
        
        self.restart_utility.SetFileLabelType(self.settings["restart_file_label"].GetString())
        self.restart_utility.SetOutputControl(self.settings["output_control_type"].GetString(), self.settings["output_frequency"].GetDouble())

        # Copy to a restart folder the posterior files, delete from problem folder
        if self.save_restart:
            self.restart_utility.CleanPosteriorResults()
        else:
            self.restart_utility.CleanPreviousResults()

    #
    def ExecuteInitializeSolutionStep(self):
        self.restart_utility.UpdateStepCounter()

    #
    def ExecuteAfterOutputStep(self):
        if self.save_restart:
            self.restart_utility.SaveRestart()

    ###
    #
    @classmethod
    def _class_prefix(self):
        header = "::[--Restart Process--]::"
        return header
