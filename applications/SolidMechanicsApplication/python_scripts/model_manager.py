""" Project: SolidMechanicsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Built-in/Generic Imports
import sys
import os

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolidMechanicsApplication as KratosSolid

# Base class to develop other solvers


class ModelManager(object):
    """The base class for solid mechanic model build process.

    This class provides functions for importing and exporting models,
    adding nodal variables and dofs.

    """

    def __init__(self, Model, custom_settings):

        default_settings = KratosMultiphysics.Parameters("""
        {
           "model_name": "solid_domain",
           "dimension": 3,
           "axisymmetric": false,
           "bodies_list": [],
           "domain_parts_list": [],
           "material_parts_list": [],
           "element_parts_list": [],
           "processes_parts_list": [],
           "output_model_part": "output_domain",
           "solving_model_part": "computing_domain",
           "composite_solving_parts": [],
           "input_file_settings": {
                "type" : "mdpa",
                "name" : "unknown_name",
                "label": 0
           },
           "variables":[]
        }
        """)

        # attention dofs mover to solid_solver
        if custom_settings.Has("dofs"):
            custom_settings.RemoveValue("dofs")
            print(" WARNING: [MODEL_MANAGER] dofs moved to SolidSolver")

        # Overwrite the default settings with user-provided parameters
        self.settings = custom_settings
        self.settings.ValidateAndAssignDefaults(default_settings)
        self.settings["input_file_settings"].ValidateAndAssignDefaults(
            default_settings["input_file_settings"])

        # Set void model
        self.model = Model
        self.main_model_part = self._get_main_model_part()

        # Process Info
        self.process_info = self.main_model_part.ProcessInfo

        # Variables settings
        self.nodal_variables = []

        # Composite solving parts
        self.solving_parts_processes = []

    ########

    #
    def ExecuteInitialize(self):
        self.ImportModel()
        for process in self.solving_parts_processes:
            process.ExecuteInitialize()
    #
    def ExecuteBeforeSolutionLoop(self):
        for process in self.solving_parts_processes:
            process.ExecuteBeforeSolutionLoop()
    #

    def ExecuteInitializeSolutionStep(self):
        for process in self.solving_parts_processes:
            process.ExecuteInitializeSolutionStep()
    #

    def ExecuteFinalizeSolutionStep(self):
        for process in self.solving_parts_processes:
            process.ExecuteFinalizeSolutionStep()

    #
    def ExecuteBeforeOutputStep(self):
        for process in self.solving_parts_processes:
            process.ExecuteBeforeOutputStep()

    #
    def ExecuteAfterOutputStep(self):
        for process in self.solving_parts_processes:
            process.ExecuteAfterOutputStep()

    ########

    #
    def ImportModel(self):

        self._add_variables()

        #print(self._class_prefix()+" Importing model part.")
        input_filename = self.settings["input_file_settings"]["name"].GetString()

        if(self.settings["input_file_settings"]["type"].GetString() == "mdpa"):

            self.main_model_part.ProcessInfo[KratosMultiphysics.IS_RESTARTED] = False
            # Import model part from mdpa file.
            print(self._class_prefix()+" Reading file: " +
                  input_filename + ".mdpa")
            #print("   " + os.path.join(os.getcwd(), input_filename) + ".mdpa ")
            sys.stdout.flush()

            self.main_model_part.ProcessInfo.SetValue(
                KratosMultiphysics.SPACE_DIMENSION, self.settings["dimension"].GetInt())
            self.main_model_part.ProcessInfo.SetValue(
                KratosMultiphysics.DOMAIN_SIZE, self.settings["dimension"].GetInt())  # Legacy

            if self.settings["dimension"].GetInt() == 2:
                self.main_model_part.ProcessInfo.SetValue(
                    KratosSolid.IS_AXISYMMETRIC, self.settings["axisymmetric"].GetBool())

            # KratosMultiphysics.ModelPartIO(input_filename).ReadModelPart(self.main_model_part)
            KratosSolid.CustomModelPartIO(
                input_filename).ReadModelPart(self.main_model_part)

            # Check and prepare computing model part and import constitutive laws.
            self._execute_after_reading()

            # Somewhere must ask if you want to clean previous files
            self._clean_previous_result_files()

        elif(self.settings["input_file_settings"]["type"].GetString() == "rest"):
            # Import model part from restart file.
            file_label = None
            if self.settings["input_file_settings"]["label"].IsInt():
                file_label = str(
                    self.settings["input_file_settings"]["label"].GetInt())
            elif self.settings["input_file_settings"]["label"].IsDouble():
                file_label = self._float_to_str(
                    self.settings["input_file_settings"]["label"].GetDouble())
            else:
                raise Exception("Restart file label is not double or int")

            import KratosMultiphysics.SolidMechanicsApplication.restart_python_utility as restart_utility
            restart_utility.RestartUtility(self.main_model_part, input_filename).LoadRestart(file_label)
            # print("   Finished loading model part from restart file ")
        else:
            raise Exception("Other input options are not yet implemented.")

        # build composite solving parts
        self._build_composite_solving_processes()

        dofs = self.main_model_part.NumberOfNodes(
        ) * self.main_model_part.ProcessInfo[KratosMultiphysics.SPACE_DIMENSION]
        #print (self._class_prefix()+" Finished importing model part")
        print(self._class_prefix()+" Model Ready (DOFs:"+str(dofs)+")")

    #
    def ExportModel(self):
        name_out_file = self.settings["input_file_settings"]["name"].GetString(
        )+".out"
        file = open(name_out_file + ".mdpa", "w")
        file.close()
        # Model part writing
        KratosMultiphysics.ModelPartIO(
            name_out_file, KratosMultiphysics.IO.WRITE).WriteModelPart(self.main_model_part)

    #
    def CleanModel(self):
        self._clean_body_parts()
        self._clean_material_parts()

    ########

    def GetProcessInfo(self):
        return self.process_info

    def GetModel(self):
        return self.model

    def GetMainModelPart(self):
        return self.main_model_part

    def GetComputingModelPart(self):
        return self.main_model_part.GetSubModelPart(self.settings["solving_model_part"].GetString())

    def GetOutputModelPart(self):
        # return self.main_model_part.GetSubModelPart(self.settings["output_model_part"].GetString())
        return self.main_model_part.GetSubModelPart(self.settings["solving_model_part"].GetString())

    def SaveRestart(self):
        pass  # one should write the restart file here

    def SetVariables(self, variables):
        self.nodal_variables = self.nodal_variables + variables

    def HasFluidParts(self):
        if self.settings.Has("bodies_list"):
            for body in self.settings["bodies_list"]:
                if bodies_list[i]["body_type"].GetString() == "Fluid":
                    return True
        if not self.settings["domain_parts_list"][0].IsString():
            for part_list in self.settings["domain_parts_list"]:
                if part_list.Has("fluid_parts"):
                    return True
        return False

    #### Model manager internal methods ####
    def _get_main_model_part(self):
        if not hasattr(self, '_main_model_part'):
            self.main_model_part = self._create_main_model_part()
        return self.main_model_part

    def _get_main_model_part(self):
        if not hasattr(self, '_main_model_part'):
            self.main_model_part = self._create_main_model_part()
        return self.main_model_part

    def _create_main_model_part(self):
        main_model_part = self.model.CreateModelPart(
            self.settings["model_name"].GetString())
        return main_model_part

    def _create_sub_model_part(self, part_name):
        self.main_model_part.CreateSubModelPart(part_name)

    def _add_variables(self):

        self._set_input_variables()

        self.nodal_variables = list(set(self.nodal_variables))

        self.nodal_variables = [self.nodal_variables[i] for i in range(
            0, len(self.nodal_variables)) if self.nodal_variables[i] != 'NOT_DEFINED']
        self.nodal_variables.sort()

        print(" Variables :", self.nodal_variables)

        for variable in self.nodal_variables:
            self.main_model_part.AddNodalSolutionStepVariable(
                KratosMultiphysics.KratosGlobals.GetVariable(variable))
            #print(" Added variable ", KratosMultiphysics.KratosGlobals.GetVariable(variable),"(",variable,")")

        print(self._class_prefix()+" General Variables ADDED")

    def _set_input_variables(self):
        for variable in self.settings["variables"].values():
            self.nodal_variables.append(variable.GetString())

    #
    def _execute_after_reading(self):

        # Assign element types
        if self._has_elements():
            self._assign_elements()

        # Build bodies
        if self._has_bodies():
            self._build_bodies()

        # Build solving model parts
        self._build_solving_model_part()

        # Build output model part
        # self._create_sub_model_part(self.settings["output_model_part"].GetString())

    #

    def _assign_elements(self):
        element_settings = KratosMultiphysics.Parameters("{}")
        element_settings.AddValue(
            "element_parts_list", self.settings["element_parts_list"])
        KratosSolid.AssignElementTypeProcess(
            self.main_model_part, element_settings).Execute()
    #
    def _build_bodies(self):

        # construct body model parts:
        solid_body_model_parts = []
        fluid_body_model_parts = []
        rigid_body_model_parts = []

        void_flags = []

        bodies_list = self.settings["bodies_list"]
        for i in range(bodies_list.size()):
            # create body model part
            body_model_part_name = bodies_list[i]["body_name"].GetString()
            self.main_model_part.CreateSubModelPart(body_model_part_name)
            body_model_part = self.main_model_part.GetSubModelPart(
                body_model_part_name)

            print(self._class_prefix()+" Body Created: "+body_model_part_name)
            body_model_part.ProcessInfo = self.main_model_part.ProcessInfo
            body_model_part.Properties = self.main_model_part.Properties

            # build body from their parts
            body_parts_name_list = bodies_list[i]["parts_list"]
            body_parts_list = []
            for j in range(body_parts_name_list.size()):
                body_parts_list.append(self.main_model_part.GetSubModelPart(
                    body_parts_name_list[j].GetString()))

            body_model_part_type = bodies_list[i]["body_type"].GetString()

            for part in body_parts_list:
                entity_type = "Nodes"
                if (body_model_part_type == "Fluid"):
                    part.Set(KratosMultiphysics.FLUID)
                    assign_flags = [KratosMultiphysics.FLUID]
                    transfer_process = KratosSolid.TransferEntitiesProcess(
                        body_model_part, part, entity_type, void_flags, assign_flags)
                    transfer_process.Execute()
                elif (body_model_part_type == "Solid"):
                    part.Set(KratosMultiphysics.SOLID)
                    assign_flags = [KratosMultiphysics.SOLID]
                    transfer_process = KratosSolid.TransferEntitiesProcess(
                        body_model_part, part, entity_type, void_flags, assign_flags)
                    transfer_process.Execute()
                elif (body_model_part_type == "Rigid"):
                    part.Set(KratosMultiphysics.RIGID)
                    assign_flags = [KratosMultiphysics.RIGID]
                    transfer_process = KratosSolid.TransferEntitiesProcess(
                        body_model_part, part, entity_type, void_flags, assign_flags)
                    transfer_process.Execute()

                entity_type = "Elements"
                transfer_process = KratosSolid.TransferEntitiesProcess(
                    body_model_part, part, entity_type)
                transfer_process.Execute()
                entity_type = "Conditions"
                transfer_process = KratosSolid.TransferEntitiesProcess(
                    body_model_part, part, entity_type)
                transfer_process.Execute()

            if(body_model_part_type == "Solid"):
                body_model_part.Set(KratosMultiphysics.SOLID)
                solid_body_model_parts.append(
                    self.main_model_part.GetSubModelPart(body_model_part_name))
            if(body_model_part_type == "Fluid"):
                body_model_part.Set(KratosMultiphysics.FLUID)
                fluid_body_model_parts.append(
                    self.main_model_part.GetSubModelPart(body_model_part_name))
            if(body_model_part_type == "Rigid"):
                body_model_part.Set(KratosMultiphysics.RIGID)
                rigid_body_model_parts.append(
                    self.main_model_part.GetSubModelPart(body_model_part_name))

    #
    def _build_solving_model_part(self):

        # The solving_model_part is labeled 'KratosMultiphysics.ACTIVE' flag (in order to recover it)
        self._create_sub_model_part(
            self.settings["solving_model_part"].GetString())

        solving_model_part = self.main_model_part.GetSubModelPart(
            self.settings["solving_model_part"].GetString())
        solving_model_part.ProcessInfo = self.main_model_part.ProcessInfo
        solving_model_part.Properties = self.main_model_part.Properties

        # Set flag to identify the solving_model_part
        solving_model_part.Set(KratosMultiphysics.ACTIVE)

        # Set nodes to the solving_model_part
        entity_type = "Nodes"
        transfer_process = KratosSolid.TransferEntitiesProcess(
            solving_model_part, self.main_model_part, entity_type)
        transfer_process.Execute()

        # Add elements from domain parts
        entity_type = "Elements"
        # Previous interface with body assignment
        if self.settings["domain_parts_list"][0].IsString():
            for part in self.settings["domain_parts_list"]:
                domain_part = self.main_model_part.GetSubModelPart(
                    part.GetString())
                transfer_process = KratosSolid.TransferEntitiesProcess(
                    solving_model_part, domain_part, entity_type)
                transfer_process.Execute()
                # set flag to identify if is a fluid/solid solving_model_part
                if(domain_part.Is(KratosMultiphysics.FLUID)):
                    solving_model_part.Set(KratosMultiphysics.FLUID)
                elif(domain_part.Is(KratosMultiphysics.SOLID)):
                    solving_model_part.Set(KratosMultiphysics.SOLID)
        else:  # Next interface with part type assignment
            parts_types = {"solid_parts": KratosMultiphysics.SOLID, "fluid_parts": KratosMultiphysics.FLUID,
                           "rigid_parts": KratosMultiphysics.RIGID, "beam_parts": KratosMultiphysics.SOLID,
                           "wall_parts": KratosMultiphysics.RIGID, "shell_parts": KratosMultiphysics.SOLID}
            for part_type, value in parts_types.items():
                for part_list in self.settings["domain_parts_list"].values():
                    if part_list.Has(part_type):
                        for part in part_list[part_type].values():
                            domain_part = self.main_model_part.GetSubModelPart(
                                part.GetString())
                            assign_flags = [value]
                            domain_part.Set(value)
                            nodes_type = "Nodes"
                            KratosSolid.AssignFlagsToEntitiesProcess(
                                domain_part, nodes_type, assign_flags).Execute()

                            KratosSolid.AssignFlagsToEntitiesProcess(
                                domain_part, entity_type, assign_flags).Execute()
                            KratosSolid.TransferEntitiesProcess(
                                solving_model_part, domain_part, entity_type).Execute()


        # Add conditions from process parts
        entity_type = "Conditions"
        for part in self.settings["processes_parts_list"].values():
            process_part = self.main_model_part.GetSubModelPart(
                part.GetString())
            process_part.Set(KratosMultiphysics.BOUNDARY)
            # condition flags as BOUNDARY or CONTACT are reserved to composite or contact conditions (do not set it here)
            thermal_as_false = KratosMultiphysics.KratosGlobals.GetFlag('THERMAL').AsFalse()
            transfer_process = KratosSolid.TransferEntitiesProcess(
                solving_model_part, process_part, entity_type, [thermal_as_false] )
            transfer_process.Execute()


    #
    def _build_composite_solving_processes(self):
        import KratosMultiphysics
        import KratosMultiphysics.SolidMechanicsApplication.composite_parts_process as composite_process
        print(self._class_prefix()+" Composite Solving Parts")
        for part in self.settings["composite_solving_parts"].values():
            print(self._class_prefix()+" Build Part: "+part["model_part_name"].GetString())
            solving_part_process = composite_process.CompositePartsProcess(self.model, part)
            self.solving_parts_processes.append(solving_part_process)

    #
    def _clean_body_parts(self):
        # delete body parts: (materials have to be already assigned)
        if self._has_bodies():
            bodies_list = self.settings["bodies_list"]
            for i in range(bodies_list.size()):
                # get body parts
                body_parts_name_list = bodies_list[i]["parts_list"]
                for j in range(body_parts_name_list.size()):
                    self.main_model_part.RemoveSubModelPart(
                        body_parts_name_list[j].GetString())
                    #print(self._class_prefix()+" Body Part Removed: "+ body_parts_name_list[j].GetString())

    #
    def _clean_material_parts(self):
        # if materials in different parts
        if self.settings.Has("material_parts_list"):
            domain_parts = []
            # previous interface with body assignment
            if self.settings["domain_parts_list"][0].IsString():
                for part in self.settings["domain_parts_list"]:
                    domain_parts.append(part.GetString())
            else:  # new interface with domain assignment
                part_types = ["solid_parts","fluid_parts","rigid_parts","wall_parts","beam_parts","shell_parts"]
                for part_type in part_types:
                    for part_list in self.settings["domain_parts_list"].values():
                        if part_list.Has(part_type):
                            for part in part_list[part_type].values():
                                domain_parts.append(part.GetString())
            print("DOMAIN_PARTS",domain_parts)
            # clear material model parts (saves model_part reconstruction after meshing, avoids incoherences)
            for material_part in self.settings["material_parts_list"].values():
                if not material_part.GetString() in domain_parts:
                    self.main_model_part.RemoveSubModelPart(
                        material_part.GetString())
    #
    def _has_bodies(self):
        if self.settings.Has("bodies_list"):
            if self.settings["bodies_list"].size() > 0:
                return True
        return False

    #
    def _has_elements(self):
        if(self.settings["element_parts_list"].size() > 0):
            return True
        return False

    #
    def _clean_previous_result_files(self):

        file_endings = [".post.bin", ".post.msh",
                        ".post.res", ".post.lst", ".post.csv", ".rest"]
        problem_path = os.getcwd()
        for file_end in file_endings:
            filelist = [f for f in os.listdir(
                problem_path) if f.endswith(file_end)]

            for f in filelist:
                try:
                    os.remove(f)
                except OSError:
                    pass
    #
    @classmethod
    def _float_to_str(self, f):
        float_string = repr(f)
        if 'e' in float_string:  # detect scientific notation
            digits, exp = float_string.split('e')
            digits = digits.replace('.', '').replace('-', '')
            exp = int(exp)
            # minus 1 for decimal point in the sci notation
            zero_padding = '0' * (abs(int(exp)) - 1)
            sign = '-' if f < 0 else ''
            if exp > 0:
                float_string = '{}{}{}.0'.format(sign, digits, zero_padding)
            else:
                float_string = '{}0.{}{}'.format(sign, zero_padding, digits)
        return float_string

    #
    @classmethod
    def _class_prefix(self):
        header = "::[---Model_Manager---]::"
        return header
