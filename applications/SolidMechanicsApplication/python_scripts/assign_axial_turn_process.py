""" Project: SolidMechanicsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolidMechanicsApplication as KratosSolid
import KratosMultiphysics.SolidMechanicsApplication.assign_utilities as assign_utilities
import KratosMultiphysics.SolidMechanicsApplication.assign_scalar_to_nodes_process as BaseProcess


def Factory(custom_settings, Model):
    if(not isinstance(custom_settings, KratosMultiphysics.Parameters)):
        raise Exception(
            "expected input shall be a Parameters object, encapsulating a json string")
    return AssignAxialTurnToNodesProcess(Model, custom_settings["Parameters"])

# All the processes python should be derived from "Process"


class AssignAxialTurnToNodesProcess(BaseProcess.AssignScalarToNodesProcess):
    def __init__(self, Model, custom_settings):
        KratosMultiphysics.Process.__init__(self)

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
             "help" : "This process assigns a rotation to nodes given by a rotation vector, modulus and direction",
             "model_part_name": "MODEL_PART_NAME",
             "variable_name": "VARIABLE_NAME",
             "modulus" : 0.0,
             "direction": [0.0, 0.0, 1.0],
             "center": [0.0, 0.0, 0.0],
             "compound_assignment": "direct",
             "constrained": true,
             "interval": [0.0, "End"],
             "flags_list": []
        }
        """)

        # trick to allow "value" to be a string or a double value
        if(custom_settings.Has("modulus")):
            if(custom_settings["modulus"].IsString()):
                default_settings["modulus"].SetString("0.0")

        # overwrite the default settings with user-provided parameters
        self.settings = custom_settings
        self.settings.ValidateAndAssignDefaults(default_settings)

        self.custom_settings = custom_settings

        # check if variable type is a vector
        self.var = KratosMultiphysics.KratosGlobals.GetVariable(
            self.settings["variable_name"].GetString())
        if(not isinstance(self.var, KratosMultiphysics.Array1DVariable3)):
            raise Exception(
                "Variable type is incorrect. Must be a three-component vector.")

        # assign scalar process
        params = KratosMultiphysics.Parameters("{}")
        params.AddValue("model_part_name", self.settings["model_part_name"])
        params.AddValue("variable_name", self.settings["variable_name"])
        params.AddValue("value", self.settings["modulus"])
        params.AddValue("constrained", self.settings["constrained"])
        params.AddValue("interval", self.settings["interval"])
        params.AddValue("flags_list", self.settings["flags_list"])

        BaseProcess.AssignScalarToNodesProcess.__init__(self, Model, params)

    #
    def _ExecuteAssignment(self):
        # self.TimeInterval.SetCurrentTime()
        if self.TimeInterval.IsInside():
            if self.TimeInterval.IsFixingStep():
                for process in self.FixDofsProcesses:
                    process.Execute()
            #print(" Execute ", self.variable_name," interval ",self.interval)
            self.AssignValueProcess.Execute()
            self._IntegrateAssignment()

    #
    def _ExecuteUnAssignment(self):
        # self.TimeInterval.SetPreviousTime()
        # if self.TimeInterval.IsInside():
        if self.TimeInterval.WasInside():
            self.UnAssignValueProcess.Execute()
            self._IntegrateAssignment()

    #
    def _SetTimeIntegration(self, process_info, variable_name):

        self.TimeIntegrationMethods = []
        components = ["_X", "_Y", "_Z"]
        TimeIntegrationMethod = None
        for i in components:
            TimeIntegrationMethod = super(AssignAxialTurnToNodesProcess, self)._SetTimeIntegration(
                self.model_part.ProcessInfo, self.variable_name+i)
            self.TimeIntegrationMethods.append(TimeIntegrationMethod)

        return TimeIntegrationMethod

    #
    def _IntegrateAssignment(self):
        for method in self.TimeIntegrationMethods:
            if(method != None):
                for node in self.model_part.Nodes:
                    method.Assign(node)

    #
    def _SetFixAndFreeProcesses(self, TimeIntegrationMethod, params):

        variable_name = params["variable_name"].GetString()
        components = ["_X", "_Y", "_Z"]
        counter = 0
        for i in components:
            params["variable_name"].SetString(variable_name+i)
            super(AssignAxialTurnToNodesProcess, self)._SetFixAndFreeProcesses(
                self.TimeIntegrationMethods[counter], params)
            counter += 1
        params["variable_name"].SetString(variable_name)

    #
    def _CheckVariableType(self, name):

        self.var = KratosMultiphysics.KratosGlobals.GetVariable(name)
        if(not isinstance(self.var, KratosMultiphysics.Array1DVariable3)):
            raise Exception(
                "Variable type is incorrect. Must be an array_1d vector")

    #
    def _CreateAssignmentProcess(self, params):

        # the rotation or angular dynamic variables are only to set the rigid body process correctly
        # inside they assign displacement or linear dynamic variables
        variable_name = params["variable_name"].GetString()

        """
        components = ["_X","_Y","_Z"]
        counter = 0
        for method in self.TimeIntegrationMethods:
            if( method != None ):
                primary_variable_name = method.GetPrimaryVariableName()
                variable_name_component = variable_name+components[counter]
                if primary_variable_name != variable_name_component:
                    params["variable_name"].SetString(primary_variable_name.split(components[counter],1)[0])
                counter+=1
        """

        params.AddValue("direction", self.custom_settings["direction"])
        params.AddValue("center", self.custom_settings["center"])
        if self.ValueManager.IsNumeric():
            params.AddEmptyValue("modulus").SetDouble(
                self.ValueManager.GetValue())
            self.AssignValueProcess = KratosSolid.AssignRotationAboutAnAxisToNodesProcess(
                self.model_part, params)
        else:
            self.AssignValueProcess = KratosSolid.AssignRotationFieldAboutAnAxisToNodesProcess(
                self.model_part, self.ValueManager.GetCompiledFunction(), "function", self.ValueManager.IsSpatialFunction(), params)

        # in case of going to previous time step for time step reduction
        self._CreateUnAssignmentProcess(params)

    #
    def _CreateUnAssignmentProcess(self, params):
        params["compound_assignment"].SetString(assign_utilities.GetInverseAssigment(
            self.settings["compound_assignment"].GetString()))
        if self.ValueManager.IsNumeric():
            self.UnAssignValueProcess = KratosSolid.AssignRotationAboutAnAxisToNodesProcess(
                self.model_part, params)
        else:
            self.UnAssignValueProcess = KratosSolid.AssignRotationFieldAboutAnAxisToNodesProcess(
                self.model_part, self.ValueManager.GetCompiledFunction(), "function", self.ValueManager.IsSpatialFunction(), params)
