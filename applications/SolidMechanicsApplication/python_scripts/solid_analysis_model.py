""" Project: SolidMechanicsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Built-in/Generic Imports
import sys
import os

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolidMechanicsApplication as KratosSolid


class SolidAnalysisModel(object):
    """The base class for solid mechanic model build process.
    This class provides functions for importing and exporting models,
    adding nodal variables and dofs.
    """

    def __init__(self, model, custom_settings):

        default_settings = KratosMultiphysics.Parameters("""
        {
        "model_name": "problem_name",
        "dimension": 3,
        "model_parts": {
           "domain_parts_list": [],
           "material_parts_list": [],
           "element_parts_list": [],
           "processes_parts_list": []
        },
        "input_file_settings": {
           "name": "problem_name",
           "type": "mdpa",
           "label": 0
        },
        "restart_settings": {
           "save_restart"        : false,
           "restart_file_name"   : "problem_name",
           "restart_file_label"  : "step",
           "output_control_type" : "step",
           "output_frequency"    : 1.0,
           "json_output"         : false
        },
        "variables":[]
        }
        """)

        # Overwrite the default settings with user-provided parameters
        self.settings = custom_settings
        self.settings.ValidateAndAssignDefaults(default_settings)
        self.settings["input_file_settings"].ValidateAndAssignDefaults(
            default_settings["input_file_settings"])
        self.settings["restart_settings"].ValidateAndAssignDefaults(
            default_settings["restart_settings"])

        # Set void model
        self.model = model
        self.main_model_part = self._get_main_model_part()

        # Process Info
        self.process_info = self.main_model_part.ProcessInfo

        # Restart process
        self.restart_process = self._get_restart_process(self.settings["restart_settings"])

        # Variables settings
        self.nodal_variables = []


    ########

    #
    def ExecuteInitialize(self):
        self.ImportModel()
        self.restart_process.ExecuteInitialize()
    #
    def ExecuteBeforeSolutionLoop(self):
        pass
    #
    def ExecuteInitializeSolutionStep(self):
        self.restart_process.ExecuteInitializeSolutionStep()
    #
    def ExecuteFinalizeSolutionStep(self):
        pass
    #
    def ExecuteBeforeOutputStep(self):
        pass
    #
    def ExecuteAfterOutputStep(self):
        self.restart_process.ExecuteAfterOutputStep()

    ########
    #
    def SetVariables(self, variables):
        self.nodal_variables = self.nodal_variables + variables
    #
    def ImportModel(self):
        # Variables from processes / solvers / output for all stages must be set previously
        self._set_solution_step_variables()

        file_type = self.settings["input_file_settings"]["type"].GetString()
        if(file_type == "mdpa"):
            # Import model part from mdpa file.
            input_file = self.settings["input_file_settings"]["name"].GetString()
            print(self._class_prefix()+" Reading file: " +input_file + ".mdpa")
            sys.stdout.flush()

            KratosSolid.CustomModelPartIO(input_file).ReadModelPart(self.main_model_part)
            # KratosMultiphysics.ModelPartIO(input_file).ReadModelPart(self.main_model_part)

            dimension = self.settings["dimension"].GetInt()
            self.main_model_part.ProcessInfo.SetValue(KratosMultiphysics.IS_RESTARTED, False)
            self.main_model_part.ProcessInfo.SetValue(KratosMultiphysics.SPACE_DIMENSION, dimension)
            self.main_model_part.ProcessInfo.SetValue(KratosMultiphysics.DOMAIN_SIZE, dimension)  # Legacy

            # Check and prepare computing model part and import constitutive laws.
            self._execute_after_reading()

        elif(file_type == "rest"):
            # Import model part from restart file.
            restart_file, restart_path = self._get_restart_path()
            print(self._class_prefix()+" Loading Restart file: " +restart_file + ".rest")
            sys.stdout.flush()

            serializer = self.restart_process.GetSerializer(restart_path)
            serializer.Load(self.main_model_part.Name, self.main_model_part)

            current_time = self.main_model_part.ProcessInfo[KratosMultiphysics.TIME]
            self.main_model_part.ProcessInfo.SetValue(KratosMultiphysics.IS_RESTARTED, True)
            self.main_model_part.ProcessInfo.SetValue(KratosSolid.RESTART_STEP_TIME, current_time)

        else:
            raise Exception(" Model input options for : "+file_type+", are not yet implemented.")

        dofs = self.main_model_part.NumberOfNodes(
        ) * self.main_model_part.ProcessInfo[KratosMultiphysics.SPACE_DIMENSION]
        print(self._class_prefix()+" Model Ready (DOFs:"+str(dofs)+")")

        sys.stdout.flush()
    #
    def ExportModel(self):
        name_out_file = self.settings["input_file_settings"]["name"].GetString(
        )+".out"
        file = open(name_out_file + ".mdpa", "w")
        file.close()
        # Model part writing
        KratosMultiphysics.ModelPartIO(
            name_out_file, KratosMultiphysics.IO.WRITE).WriteModelPart(self.main_model_part)

    ########

    def GetProcessInfo(self):
        return self.process_info

    def GetModel(self):
        return self.model

    def GetMainModelPart(self):
        return self.main_model_part

    def GetComputingModelPart(self):
        return self.main_model_part.GetSubModelPart("computing_domain")

    def GetOutputModelPart(self):
        return self.GetComputingModelPart()

    def HasFluidParts(self):
        domain_parts = self.settings["model_parts"]["domain_parts_list"]
        if not domains[0].IsString():
            for part_list in domain_parts:
                if part_list.Has("fluid_parts_list"):
                    return True
        return False

    #### Analysis Model internal methods ####

    def _get_main_model_part(self):
        if not hasattr(self, 'main_model_part'):
            self.main_model_part = self._create_main_model_part()
        return self.main_model_part

    def _create_main_model_part(self):
        main_model_part = self.model.CreateModelPart(
            self.settings["model_name"].GetString())
        return main_model_part

    def _create_sub_model_part(self, part_name):
        self.main_model_part.CreateSubModelPart(part_name)
    #
    def _import_materials(self):
        import KratosMultiphysics
        # Assign material to model_parts (if Materials.json exists)
        if os.path.isfile("Materials.json"):
            materials_file = open("Materials.json", 'r')
            material_parameters = KratosMultiphysics.Parameters(materials_file.read())
            if material_parameters.Has("material_models_list"):
                import KratosMultiphysics.ConstitutiveModelsApplication
                # Set model name (prepend parent model part name)
                for material in material_parameters["material_models_list"]:
                    self._prepend_parent_model_part_name(material,self.settings["model_name"].GetString())
                    self._construct_process(material).Execute()
        elif os.path.isfile("materials.py"):  # legacy
            import KratosMultiphysics.SolidMechanicsApplication.constitutive_law_python_utility as constitutive_law_utils
            constitutive_law = constitutive_law_utils.ConstitutiveLawUtility(
                self.main_model_part, self.process_info[KratosMultiphysics.SPACE_DIMENSION])
            constitutive_law.Initialize()
            print("::[-----Material------]:: Reading file: materials.py ")
        else:
            raise Exception(" MATERIALS NOT DEFINED (Materials.json or Materials.py not found)")

        self._clean_material_parts()
    #
    def _get_restart_path(self):
        file_label = None
        input_file = self.settings["input_file_settings"]
        if input_file["label"].IsInt():
            file_label = str(input_file["label"].GetInt())
        elif input_file["label"].IsDouble():
            file_label = self._float_to_str(input_file["label"].GetDouble())
        else:
            raise Exception("Restart file label is not double or int")

        restart_file = input_file["name"] + "__" + file_label
        restart_path = os.path.join(os.getcwd(), restart_file)
        if(os.path.exists(restart_path+".rest") == False):
            raise Exception("Restart file not found: " +restart_path + ".rest")

        return restart_file, restart_path
    #
    def _get_restart_process(self, process):
        restart_process = KratosMultiphysics.Parameters("""{ }""")
        process.AddEmptyValue("model_part_name").SetString(self.main_model_part.Name)
        restart_process.AddEmptyValue("process_module").SetString("SolidMechanicsApplication.restart_process")
        restart_process.AddValue("Parameters", process)
        return self._construct_process(restart_process)
    #
    def _construct_process(self, process):
        import importlib
        import KratosMultiphysics
        import KratosMultiphysics.process_factory
        process_module_name = None
        if process.Has("process_module"):
            process_module_name = "KratosMultiphysics."+process["process_module"].GetString()
        process_module = importlib.import_module(process_module_name)
        return process_module.Factory(process, self.model)
    #
    def _set_input_variables(self):
        for variable in self.settings["variables"]:
            self.nodal_variables.append(variable.GetString())
    #
    def _set_solution_step_variables(self):

        self._set_input_variables()

        self.nodal_variables = list(set(self.nodal_variables))

        self.nodal_variables = [self.nodal_variables[i] for i in range(
            0, len(self.nodal_variables)) if self.nodal_variables[i] != 'NOT_DEFINED']
        self.nodal_variables.sort()

        print(" Variables :", self.nodal_variables)

        for variable in self.nodal_variables:
            self.main_model_part.AddNodalSolutionStepVariable(
                KratosMultiphysics.KratosGlobals.GetVariable(variable))
            #print(" Added variable ", KratosMultiphysics.KratosGlobals.GetVariable(variable),"(",variable,")")

        print(self._class_prefix()+" General Variables ADDED")

    #
    def _execute_after_reading(self):
        # Assign element types
        if self._has_elements():
            self._assign_elements()

        # Import materials
        self._import_materials()

        # Build solving model parts
        self._build_solving_model_part()

        # Somewhere must ask if you want to clean previous files
        self.restart_process.CleanPreviousResults()
    #
    def _assign_elements(self):
        element_settings = KratosMultiphysics.Parameters("{}")
        element_settings.AddValue("element_parts_list", self.settings["model_parts"]["element_parts_list"])
        KratosSolid.AssignElementTypeProcess(self.main_model_part, element_settings).Execute()

    #
    def _build_solving_model_part(self):
        # The solving_model_part is labeled 'KratosMultiphysics.ACTIVE' flag (in order to recover it)
        solving_model_part_name = "computing_domain"
        self._create_sub_model_part(solving_model_part_name)

        solving_model_part = self.main_model_part.GetSubModelPart(
            solving_model_part_name)
        solving_model_part.ProcessInfo = self.main_model_part.ProcessInfo
        solving_model_part.Properties = self.main_model_part.Properties

        # Set flag to identify the solving_model_part
        solving_model_part.Set(KratosMultiphysics.ACTIVE)

        # Add nodes from main model part
        entity_type = "Nodes"
        transfer_process = KratosSolid.TransferEntitiesProcess(
            solving_model_part, self.main_model_part, entity_type)
        transfer_process.Execute()

        # Add elements from domain parts
        entity_type = "Elements"
        parts_types = {"solid_parts": KratosMultiphysics.SOLID, "fluid_parts": KratosMultiphysics.FLUID,
                       "rigid_parts": KratosMultiphysics.RIGID, "beam_parts": KratosMultiphysics.SOLID,
                       "shell_parts": KratosMultiphysics.SOLID}
        for part_type, value in parts_types.items():
            for part_list in self.settings["model_parts"]["domain_parts_list"]:
                if part_list.Has(part_type):
                    for part in part_list[part_type]:
                        domain_part = self.main_model_part.GetSubModelPart(
                            part.GetString())
                        KratosSolid.TransferEntitiesProcess(
                            solving_model_part, domain_part, entity_type).Execute()
                        assign_flags = [value]
                        domain_part.Set(value)
                        nodes_type = "Nodes"
                        KratosSolid.AssignFlagsToEntitiesProcess(
                            domain_part, nodes_type, assign_flags).Execute()

        # Add conditions from process parts
        entity_type = "Conditions"
        for part in self.settings["model_parts"]["processes_parts_list"]:
            process_part = self.main_model_part.GetSubModelPart(
                part.GetString())
            process_part.Set(KratosMultiphysics.BOUNDARY)
            # condition flags as BOUNDARY or CONTACT are reserved to composite or contact conditions (do not set it here)
            transfer_process = KratosSolid.TransferEntitiesProcess(
                solving_model_part, process_part, entity_type)
            transfer_process.Execute()
    #
    def _clean_material_parts(self):
        # if materials in different parts
        if self.settings["model_parts"].Has("material_parts_list"):
            domain_parts = []
            # previous interface with body assignment
            if self.settings["model_parts"]["domain_parts_list"][0].IsString():
                for part in self.settings["model_parts"]["domain_parts_list"]:
                    domain_parts.append(part.GetString())
            else:  # new interface with domain assignment
                part_types = ["solid_parts", "fluid_parts",
                              "rigid_parts", "beam_parts", "shell_parts"]
                for part_type in part_types:
                    for part_list in self.settings["model_parts"]["domain_parts_list"]:
                        if part_list.Has(part_type):
                            for part in part_list[part_type]:
                                domain_parts.append(part.GetString())
            print("DOMAIN_PARTS", domain_parts)
            # clear material model parts (saves model_part reconstruction after meshing, avoids incoherences)
            for material_part in self.settings["model_parts"]["material_parts_list"]:
                if not material_part.GetString() in domain_parts:
                    self.main_model_part.RemoveSubModelPart(
                        material_part.GetString())
    #
    def _prepend_parent_model_part_name(self, process, model_name):
        # prepend parent model part name
        parameters = process
        if process.Has("Parameters"):
            parameters = process["Parameters"]
        if parameters.Has("model_part_name"):
            name = parameters["model_part_name"].GetString().split(".")[0]
            if model_name != name:
                name = model_name+"."+parameters["model_part_name"].GetString()
                parameters["model_part_name"].SetString(name)
    #
    def _has_elements(self):
        if(self.settings["model_parts"]["element_parts_list"].size() > 0):
            return True
        return False

    #
    @classmethod
    def _float_to_str(self, f):
        float_string = repr(f)
        if 'e' in float_string:  # detect scientific notation
            digits, exp = float_string.split('e')
            digits = digits.replace('.', '').replace('-', '')
            exp = int(exp)
            # minus 1 for decimal point in the sci notation
            zero_padding = '0' * (abs(int(exp)) - 1)
            sign = '-' if f < 0 else ''
            if exp > 0:
                float_string = '{}{}{}.0'.format(sign, digits, zero_padding)
            else:
                float_string = '{}0.{}{}'.format(sign, zero_padding, digits)
        return float_string

    #
    @classmethod
    def _class_prefix(self):
        header = "::[---Model_Manager---]::"
        return header
