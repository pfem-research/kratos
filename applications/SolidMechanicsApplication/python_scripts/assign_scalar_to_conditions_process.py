""" Project: SolidMechanicsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolidMechanicsApplication as KratosSolid
import KratosMultiphysics.SolversApplication as KratosSolver

# This proces sets the value of a scalar variable to conditions
import KratosMultiphysics.SolidMechanicsApplication.assign_utilities as assign_utilities
import KratosMultiphysics.SolidMechanicsApplication.assign_scalar_to_nodes_process as BaseProcess


def Factory(custom_settings, Model):
    if(not isinstance(custom_settings, KratosMultiphysics.Parameters)):
        raise Exception(
            "Expected input shall be a Parameters object, encapsulating a json string")
    return AssignScalarToConditionsProcess(Model, custom_settings["Parameters"])


class AssignScalarToConditionsProcess(BaseProcess.AssignScalarToNodesProcess):
    def __init__(self, Model, custom_settings):
        BaseProcess.AssignScalarToNodesProcess.__init__(
            self, Model, custom_settings)
        self.entity_type = "CONDITIONS"
        self.interval_ended = False

    def ExecuteInitialize(self):

        # set model part
        self.model_part = self.model[self.settings["model_part_name"].GetString(
        )]

        # set time interval
        #self.TimeInterval = assign_utilities.TimeIntervalManager(self.settings["interval"], self.model_part.ProcessInfo)
        time_params = KratosMultiphysics.Parameters("{}")
        time_params.AddValue("interval", self.settings["interval"])
        self.TimeInterval = KratosSolver.TimeInterval(time_params, self.model_part.ProcessInfo)

        if(self.model_part.ProcessInfo[KratosMultiphysics.IS_RESTARTED] == False):
            self.model_part.ProcessInfo.SetValue(
                KratosMultiphysics.INTERVAL_END_TIME, self.TimeInterval.GetEndTime())

        # set processes
        self.FixDofsProcesses = []
        self.FreeDofsProcesses = []

        params = KratosMultiphysics.Parameters("{}")
        params.AddValue("model_part_name", self.settings["model_part_name"])
        params.AddValue("compound_assignment",
                        self.settings["compound_assignment"])

        self._CreateAssignmentProcess(params)

        # self.TimeInterval.SetCurrentTime()
        if self.TimeInterval.IsInitial():
            self.AssignValueProcess.Execute()

    def ExecuteFinalizeSolutionStep(self):
        if not self.interval_ended:
            # self.TimeInterval.SetCurrentTime()
            if self.TimeInterval.IsEnded():
                self.interval_ended = True
                if not self.finalized:
                    self.AssignValueProcess.ExecuteFinalize()
                    self.finalized = True

    def _ExecuteAssignment(self):
        if self.TimeInterval.IsInside():
            self.AssignValueProcess.Execute()

    def _ExecuteUnAssignment(self):
        if self.TimeInterval.IsInside():
            self.UnAssignValueProcess.Execute()

    def _CreateAssignmentProcess(self, params):
        if self.ValueManager.IsNumeric():
            params.AddValue("variable_name", self.settings["variable_name"])
            params.AddEmptyValue("value").SetDouble(
                self.ValueManager.GetValue())
            params.AddEmptyValue("entity_type").SetString(self.entity_type)
            self.AssignValueProcess = KratosSolid.AssignScalarToEntitiesProcess(
                self.model_part, params)
        else:
            # function values are assigned to a vector variable :: transformation is needed
            if(isinstance(self.var, KratosMultiphysics.DoubleVariable)):
                variable_name = self.settings["variable_name"].GetString(
                ) + "_VECTOR"
                #print("::[--Assign_Variable--]:: "+variable_name)
                params.AddEmptyValue("variable_name")
                params["variable_name"].SetString(variable_name)
            else:
                params.AddValue("variable_name",
                                self.settings["variable_name"])

            params.AddEmptyValue("entity_type").SetString(self.entity_type)
            self.AssignValueProcess = KratosSolid.AssignScalarFieldToEntitiesProcess(
                self.model_part, self.ValueManager.GetCompiledFunction(), "function", self.ValueManager.IsSpatialFunction(), params)

    def _CreateUnAssignmentProcess(self, params):
        params["compound_assignment"].SetString(self.GetInverseAssigment(
            self.settings["compound_assignment"].GetString()))
        if self.ValueManager.IsNumeric():
            self.UnAssignValueProcess = KratosSolid.AssignScalarToEntitiesProcess(
                self.model_part, params)
        else:
            self.UnAssignValueProcess = KratosSolid.AssignScalarFieldToEntitiesProcess(
                self.model_part, self.ValueManager.GetCompiledFunction(), "function", self.ValueManager.IsSpatialFunction(), params)
