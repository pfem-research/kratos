//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:           LMonforte $
//   Maintained by:       $Maintainer:                 LM $
//   Date:                $Date:            December 2021 $
//
//
#if !defined(KRATOS_UNSAT_AXISYM_WATER_PRESSURE_UTILITIE_HPP_INCLUDED)
#define KRATOS_UNSAT_AXISYM_WATER_PRESSURE_UTILITIE_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_utilities/unsaturated_water_pressure_utilities.hpp"

namespace Kratos
{

class KRATOS_API(SOLID_MECHANICS_APPLICATION) AxisymUnsaturatedWaterPressureUtilities
    : public UnsaturatedWaterPressureUtilities
{

public:
   typedef Matrix MatrixType;

   typedef Vector VectorType;

   typedef unsigned int IndexType;

   typedef unsigned int SizeType;

   typedef Properties PropertiesType;

   typedef Node<3> NodeType;
   typedef Geometry<NodeType> GeometryType;

   

public:
   AxisymUnsaturatedWaterPressureUtilities();

   virtual ~AxisymUnsaturatedWaterPressureUtilities(){};
   
   
protected:
   virtual void GetVoigtSize(const unsigned int &dimension, unsigned int &voigtsize, unsigned int &principal_dimension) override;
   
   VectorType &CalculateMassBalance_AddDisplacementPart(HydroMechanicalVariables &rVariables, VectorType &rLocalRHS, const double &rIntegrationWeight) override;
   
   virtual MatrixType &ComputeWaterPressureKUwP(HydroMechanicalVariables &rVariables, MatrixType &rLocalLHS, const double &rIntegrationWeight) override;
   
   MatrixType &   ComputeLinearization_Displacement_Displacement(HydroMechanicalVariables & rVariables, MatrixType & rLocalLHS, const double & rIntegrationWeight) override;
   MatrixType &   ComputeLinearization_Displacement_WaterPressure(HydroMechanicalVariables & rVariables, MatrixType & rLocalLHS, const double & rIntegrationWeight) override;
   
   MatrixType &   ComputeLinearization_DegreeOfSaturation_Displacement(HydroMechanicalVariables & rVariables, MatrixType & rLocalLHS, const double & rIntegrationWeight) override;
   MatrixType &   ComputeLinearization_WaterPressure_Displacement(HydroMechanicalVariables & rVariables, MatrixType & rLocalLHS, const double & rIntegrationWeight) override;
}; // end Class UnsaturatedWaterPressureUtilities

} // namespace Kratos

#endif // KRATOS_UNSAT_AXISYM_WATER_PRESSURE_UTILITIE_HPP_INCLUDED

