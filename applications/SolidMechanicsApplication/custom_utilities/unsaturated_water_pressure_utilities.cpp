//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:           LMonforte $
//   Maintained by:       $Maintainer:                 LM $
//   Date:                $Date:            December 2021 $
//

#include "custom_utilities/unsaturated_water_pressure_utilities.hpp"
#include "solid_mechanics_application_variables.h"

namespace Kratos
{

   UnsaturatedWaterPressureUtilities::UnsaturatedWaterPressureUtilities(): WaterPressureUtilities()
   {
   }

   // ****************** CALCULATE AND ADD RHS VECTOR *********************************************************
   // ******************************** Add water pressure to Internal and add mass balance equation ***********
   Vector &UnsaturatedWaterPressureUtilities::CalculateAndAddHydromechanicalRHS(HydroMechanicalVariables &rVariables, Vector &rRightHandSide, const Vector &rBaseClassRHS, const double &rIntegrationWeight)
   {
      KRATOS_TRY

      const unsigned int number_of_nodes = rVariables.GetGeometry().PointsNumber();
      const unsigned int dimension = rVariables.GetGeometry().WorkingSpaceDimension();

      // 1. Reshape BaseClass RHS
      rRightHandSide = this->AddReshapeBaseClassRHS(rRightHandSide, rBaseClassRHS, rVariables.number_of_variables, number_of_nodes);

      // 2a. Compute and Addmechanical forces contribution to internal forces
      Vector LocalRHS;
      LocalRHS = CalculateWaterInternalForcesContribution(rVariables, LocalRHS, rIntegrationWeight);
      rRightHandSide = AddReshapeWaterInternalForcesContribution(rRightHandSide, LocalRHS, rVariables.number_of_variables, number_of_nodes, dimension);

      LocalRHS = CalculateVolumeForcesContribution(rVariables, LocalRHS, rIntegrationWeight);
      rRightHandSide = AddReshapeWaterInternalForcesContribution(rRightHandSide, LocalRHS, rVariables.number_of_variables, number_of_nodes, dimension);

      // 2b. Compute the balance of mixture
      LocalRHS = CalculateMassBalance_WaterPressurePart(rVariables, LocalRHS, rIntegrationWeight);
      LocalRHS = CalculateMassBalance_AddDegreeOfSaturationPart(rVariables, LocalRHS, rIntegrationWeight);
      LocalRHS = this->CalculateMassBalance_AddDisplacementPart(rVariables, LocalRHS, rIntegrationWeight);
      
      rRightHandSide = this->AddReshapeWaterPressureForces(rRightHandSide, LocalRHS, rVariables.number_of_variables, number_of_nodes);

      return rRightHandSide;

      KRATOS_CATCH("")
   }



   // ******************************  CALCULATE AND ADD LHS MATRIX *************************************
   // ***************************************************************************************************
   Matrix &UnsaturatedWaterPressureUtilities::CalculateAndAddHydromechanicalLHS(HydroMechanicalVariables &rVariables, Matrix &rLeftHandSide, const Matrix &rBaseClassLHS, const double &rIntegrationWeight)
   {
      KRATOS_TRY

      const unsigned int number_of_nodes = rVariables.GetGeometry().PointsNumber();
      const unsigned int dimension = rVariables.GetGeometry().WorkingSpaceDimension();

      // 1. Reshape BaseClass LHS
      rLeftHandSide = AddReshapeBaseClassLHS(rLeftHandSide, rBaseClassLHS, dimension, rVariables.number_of_variables, number_of_nodes);

      // 2. Add water Pressure geometric stiffness matrix
      Matrix LocalLHS;

      LocalLHS = this->ComputeWaterPressureKuug(rVariables, LocalLHS, rIntegrationWeight);
      rLeftHandSide = AddReshapeKUU(rLeftHandSide, LocalLHS, dimension, rVariables.number_of_variables, number_of_nodes);

      // 3. Add KUwP
      LocalLHS = this->ComputeWaterPressureKUwP(rVariables, LocalLHS, rIntegrationWeight);
      rLeftHandSide = AddReshapeKUwP(rLeftHandSide, LocalLHS, dimension, rVariables.number_of_variables, number_of_nodes);

      // 4. Mass Balance equation. Term by term
      double IntegrationWeight = (rIntegrationWeight / rVariables.detF0);

      LocalLHS = ComputeLinearization_WaterPressure_WaterPressure( rVariables, LocalLHS, IntegrationWeight);
      rLeftHandSide = AddReshapeKwPwP(rLeftHandSide, LocalLHS, dimension, rVariables.number_of_variables, number_of_nodes);

      LocalLHS = this->ComputeLinearization_WaterPressure_Displacement( rVariables, LocalLHS, IntegrationWeight);
      rLeftHandSide = AddReshapeKwPU(rLeftHandSide, LocalLHS, dimension, rVariables.number_of_variables, number_of_nodes);

      
      LocalLHS = ComputeLinearization_DegreeOfSaturation_WaterPressure(rVariables, LocalLHS, IntegrationWeight);
      rLeftHandSide = AddReshapeKwPwP(rLeftHandSide, LocalLHS, dimension, rVariables.number_of_variables, number_of_nodes);

      LocalLHS = this->ComputeLinearization_DegreeOfSaturation_Displacement(rVariables, LocalLHS, IntegrationWeight);
      rLeftHandSide = this->AddReshapeSolidSkeletonDeformationMatrix( rLeftHandSide, LocalLHS, dimension, rVariables.number_of_variables, number_of_nodes);

     
      LocalLHS = this->ComputeLinearization_Displacement_WaterPressure(rVariables, LocalLHS, IntegrationWeight);
      rLeftHandSide = AddReshapeKwPwP(rLeftHandSide, LocalLHS, dimension, rVariables.number_of_variables, number_of_nodes);
      
      LocalLHS = this->ComputeLinearization_Displacement_Displacement(rVariables, LocalLHS, IntegrationWeight);
      rLeftHandSide = this->AddReshapeSolidSkeletonDeformationMatrix(rLeftHandSide, LocalLHS, dimension, rVariables.number_of_variables, number_of_nodes);

      return rLeftHandSide;

      KRATOS_CATCH("")
   }

   // **** Geometric like term due to the variation of the density effect on the linear momentum balance ******
   // *********************************************************************************************************
   Matrix &UnsaturatedWaterPressureUtilities::ComputeDensityChangeTerm(HydroMechanicalVariables &rVariables, MatrixType &rLocalLHS, const double &rIntegrationWeight)
   {
      KRATOS_TRY

      const unsigned int number_of_nodes = rVariables.GetGeometry().PointsNumber();
      const unsigned int dimension = rVariables.GetGeometry().WorkingSpaceDimension();
      VectorType VolumeForce = rVariables.GetVolumeForce(); //lets see
      VolumeForce *= rVariables.detF0;                      // due to the volumechange

      rLocalLHS.resize(dimension * number_of_nodes, dimension * number_of_nodes);
      noalias(rLocalLHS) = ZeroMatrix(dimension * number_of_nodes, dimension * number_of_nodes);

      double density_mixture0 = rVariables.GetProperties().GetValue(DENSITY);
      if (density_mixture0 > 0)
      {
         VolumeForce /= density_mixture0;
      }
      else
      {
         return rLocalLHS;
      }

      double density_water = rVariables.GetProperties().GetValue(DENSITY_WATER);

      const MatrixType &rDN_DX = rVariables.GetShapeFunctionsDerivatives();
      const VectorType &rN = rVariables.GetShapeFunctions();

      for (unsigned int i = 0; i < number_of_nodes; i++)
      {
         for (unsigned int iDim = 0; iDim < dimension; iDim++)
         {
            for (unsigned int p = 0; p < number_of_nodes; p++)
            {
               for (unsigned int pDim = 0; pDim < dimension; pDim++)
               {
                  rLocalLHS(i * dimension + iDim, p * dimension + pDim) -= rN(i) * VolumeForce(iDim) * rDN_DX(p, pDim);
               }
            }
         }
      }

      rLocalLHS *= (rIntegrationWeight * density_water);
      return rLocalLHS;

      KRATOS_CATCH("")
   }


   // ******* Tangent to Mass conservation with Respect WaterPressure ***********************************
   // ***************************************************************************************************
   Matrix &UnsaturatedWaterPressureUtilities::ComputeLinearization_WaterPressure_WaterPressure(HydroMechanicalVariables &rVariables, Matrix &rLocalLHS, const double &rIntegrationWeight)
   {
      KRATOS_TRY

      const unsigned int number_of_nodes = rVariables.GetGeometry().PointsNumber();
      const unsigned int dimension = rVariables.GetGeometry().WorkingSpaceDimension();
      const MatrixType &rDN_DX = rVariables.GetShapeFunctionsDerivatives();

      // 1. Some constants
      double ScalingConstant;
      GetScalingConstant(ScalingConstant, rVariables.GetProperties());

      Matrix K;
      double initial_porosity = rVariables.GetProperties().GetValue(INITIAL_POROSITY);
      double VolumeChange = this->CalculateVolumeChange(rVariables.GetGeometry(), rVariables.GetShapeFunctions(), rVariables.GetDeformationGradient(), rVariables.GetProperties(), rVariables.detF0);
      double WaterPressureOld = GetWaterPressureOld( rVariables.GetShapeFunctions(), rVariables.GetGeometry(),  WaterPressureOld);
      this->GetPermeabilityTensor(rVariables.GetProperties(), rVariables.GetDeformationGradient(), K, initial_porosity, VolumeChange, WaterPressureOld);

      rLocalLHS.resize(number_of_nodes, number_of_nodes);
      noalias(rLocalLHS) = ZeroMatrix(number_of_nodes, number_of_nodes);

      for (unsigned int i = 0; i < number_of_nodes; i++) {
         for (unsigned int j = 0; j < number_of_nodes; j++) {
            for (unsigned p = 0; p < dimension; p++) {
               for (unsigned q = 0; q < dimension; q++) {
                  rLocalLHS(i, j) -= rVariables.DeltaTime * rDN_DX(i, p) * K(p, q) * rDN_DX(j, q);
               }
            }
         }
      }

      rLocalLHS *= (rIntegrationWeight * ScalingConstant);

      return rLocalLHS;

      KRATOS_CATCH("")
   }

   // ******* Tangent to Mass conservation with Respect WaterPressure ***********************************
   // ***************************************************************************************************
   Matrix &UnsaturatedWaterPressureUtilities::ComputeLinearization_WaterPressure_Displacement(HydroMechanicalVariables &rVariables, Matrix &rLocalLHS, const double &rIntegrationWeight)
   {
      KRATOS_TRY

      const GeometryType &rGeometry = rVariables.GetGeometry();

      const unsigned int number_of_nodes = rGeometry.PointsNumber();
      const unsigned int dimension = rGeometry.WorkingSpaceDimension();

      // 1. Some constants
      double ScalingConstant;
      GetScalingConstant(ScalingConstant, rVariables.GetProperties());

      //2. Gravity & Permeability
      Vector b(dimension);
      noalias(b) = ZeroVector(dimension);
      double WaterDensity = rVariables.GetProperties().GetValue(DENSITY_WATER);
      b(dimension - 1) = -10.0 * WaterDensity;
      Matrix K;
      double initial_porosity = rVariables.GetProperties().GetValue(INITIAL_POROSITY);

      double VolumeChange = this->CalculateVolumeChange(rGeometry, rVariables.GetShapeFunctions(), rVariables.GetDeformationGradient(), rVariables.GetProperties(), rVariables.detF0 );
      double WaterPressureOld = GetWaterPressureOld( rVariables.GetShapeFunctions(), rVariables.GetGeometry(), WaterPressureOld);
      this->GetPermeabilityTensorAndReshape(rVariables.GetProperties(), rVariables.GetDeformationGradient(), K, initial_porosity, dimension, VolumeChange, WaterPressureOld);

      const MatrixType &rDN_DX = rVariables.GetShapeFunctionsDerivatives();
      Vector GradP = ZeroVector(dimension);

      for (unsigned int iN = 0; iN < number_of_nodes; iN++) {
         for (unsigned int dim = 0; dim < dimension; dim++) {
            GradP(dim) += rGeometry[iN].FastGetSolutionStepValue(WATER_PRESSURE) * rDN_DX(iN, dim);
         }
      }

      Vector DarcyFlow(dimension);
      noalias(DarcyFlow) = prod(K, GradP - b);

      rLocalLHS.resize(number_of_nodes, number_of_nodes * dimension);
      noalias(rLocalLHS) = ZeroMatrix(number_of_nodes, number_of_nodes * dimension);

      Matrix AllwaysTerm = rLocalLHS;

      for (unsigned int i = 0; i < number_of_nodes; i++) {
         for (unsigned int iDim = 0; iDim < dimension; iDim++) {
            for (unsigned int qDim = 0; qDim < dimension; qDim++) {
               for (unsigned int q = 0; q < number_of_nodes; q++) {
                  rLocalLHS(i, q * dimension + iDim) += rDN_DX(i, iDim) * DarcyFlow(qDim) * rDN_DX(q, qDim);
               }
            }
         }
      }

      // Try To do it the other way
      Matrix SpatialTerm = rLocalLHS;
      for (unsigned int i = 0; i < number_of_nodes; i++) {
         for (unsigned int iDim = 0; iDim < dimension; iDim++) {
            for (unsigned int qDim = 0; qDim < dimension; qDim++) {
               for (unsigned int pDim = 0; pDim < dimension; pDim++) {
                  for (unsigned int a = 0; a < number_of_nodes; a++) {
                     rLocalLHS(i, a * dimension + pDim) -= rDN_DX(i, iDim) * K(iDim, qDim) * GradP(pDim) * rDN_DX(a, qDim);
                  }
               }
            }
         }
      }

      rLocalLHS *= (rIntegrationWeight * ScalingConstant * rVariables.DeltaTime);

      return rLocalLHS;

      KRATOS_CATCH("")
   }


   // ******* Tangent to Mass conservation with Respect WaterPressure ***********************************
   // ***************************************************************************************************
   Matrix &UnsaturatedWaterPressureUtilities::ComputeLinearization_DegreeOfSaturation_Displacement(HydroMechanicalVariables &rVariables, Matrix &rLocalLHS, const double &rIntegrationWeight)
   {
      KRATOS_TRY

      const GeometryType &rGeometry = rVariables.GetGeometry();

      const unsigned int number_of_nodes = rGeometry.PointsNumber();
      const unsigned int dimension = rGeometry.WorkingSpaceDimension();

      // 1. Some constants
      double ScalingConstant;
      GetScalingConstant(ScalingConstant, rVariables.GetProperties());


      const MatrixType &rDN_DX = rVariables.GetShapeFunctionsDerivatives();
      const VectorType &rN = rVariables.GetShapeFunctions();

      double WaterPressure(0.0);
      double WaterPressureOld(0.0);
      for (unsigned int i = 0; i < number_of_nodes; i++) {
         WaterPressure += rGeometry[i].GetSolutionStepValue(WATER_PRESSURE) * rN(i);
         WaterPressureOld += rGeometry[i].GetSolutionStepValue(WATER_PRESSURE,1) * rN(i);
      }

      double DegreeOfSaturation = EvaluateRetentionCurve(rVariables.GetProperties(), WaterPressure, DegreeOfSaturation);
      double DegreeOfSaturationOld = EvaluateRetentionCurve(rVariables.GetProperties(), WaterPressureOld, DegreeOfSaturationOld);

      double DeltaSr = DegreeOfSaturation-DegreeOfSaturationOld;

      const double & rInitialPorosity = rVariables.GetProperties().GetValue(INITIAL_POROSITY);


      rLocalLHS.resize(number_of_nodes, number_of_nodes * dimension);
      noalias(rLocalLHS) = ZeroMatrix(number_of_nodes, number_of_nodes * dimension);

      for (unsigned int i = 0; i < number_of_nodes; i++) {
         for (unsigned int qDim = 0; qDim < dimension; qDim++) {
            for (unsigned int q = 0; q < number_of_nodes; q++) {
               rLocalLHS(i, q * dimension + qDim) += rN(i) * DeltaSr * (1.0-rInitialPorosity)/rVariables.detF0 * rDN_DX(q, qDim);
            }
         }
      }

      rLocalLHS *= (rIntegrationWeight * ScalingConstant);

      return rLocalLHS;

      KRATOS_CATCH("")
   }
   // ******* Tangent to Mass conservation with Respect WaterPressure ***********************************
   // ***************************************************************************************************
   Matrix &UnsaturatedWaterPressureUtilities::ComputeLinearization_DegreeOfSaturation_WaterPressure(HydroMechanicalVariables &rVariables, Matrix &rLocalLHS, const double &rIntegrationWeight)
   {
      KRATOS_TRY

      const unsigned int number_of_nodes = rVariables.GetGeometry().PointsNumber();
      const VectorType &rN = rVariables.GetShapeFunctions();

      const GeometryType &rGeometry = rVariables.GetGeometry();

      // 1. Some constants
      double ScalingConstant;
      GetScalingConstant(ScalingConstant, rVariables.GetProperties());


      rLocalLHS.resize(number_of_nodes, number_of_nodes);
      noalias(rLocalLHS) = ZeroMatrix(number_of_nodes, number_of_nodes);

      double VolumeChange = this->CalculateVolumeChange(rGeometry, rVariables.GetShapeFunctions(), rVariables.GetDeformationGradient(), rVariables.GetProperties(), rVariables.detF0 );
      const double & rInitialPorosity = rVariables.GetProperties().GetValue(INITIAL_POROSITY);
      double Porosity = 1.0 - (1.0 - rInitialPorosity) / VolumeChange;

      double WaterPressure = 0.0;
      for (unsigned int i = 0; i < number_of_nodes; i++)
         WaterPressure += rGeometry[i].GetSolutionStepValue(WATER_PRESSURE) * rN(i);

      double dSr_dWaterPressure = DerivativeRetentionCurve( rVariables.GetProperties(), WaterPressure, dSr_dWaterPressure);


      for (unsigned int i = 0; i < number_of_nodes; i++) {
         for (unsigned int j = 0; j < number_of_nodes; j++) {
            rLocalLHS(i, j) += rN(i) *  Porosity * dSr_dWaterPressure * rN(j);
         }
      }

      rLocalLHS *= (rIntegrationWeight * ScalingConstant);

      return rLocalLHS;

      KRATOS_CATCH("")
   }

   Matrix &UnsaturatedWaterPressureUtilities::ComputeLinearization_Displacement_Displacement(HydroMechanicalVariables &rVariables, Matrix &rLocalLHS, const double &rIntegrationWeight)
   {
      KRATOS_TRY

      const GeometryType &rGeometry = rVariables.GetGeometry();
      const unsigned int number_of_nodes = rGeometry.PointsNumber();
      const unsigned int dimension = rGeometry.WorkingSpaceDimension();

      // 1. Some constants
      double ScalingConstant;
      GetScalingConstant(ScalingConstant, rVariables.GetProperties());

      // 2. Calculate Matrix
      rLocalLHS.resize(number_of_nodes, number_of_nodes * dimension);
      noalias(rLocalLHS) = ZeroMatrix(number_of_nodes, number_of_nodes * dimension);

      const MatrixType &rDN_DX = rVariables.GetShapeFunctionsDerivatives();
      const VectorType &rN = rVariables.GetShapeFunctions();

      double WaterPressure(0.0);
      for (unsigned int i = 0; i < number_of_nodes; i++)
         WaterPressure += rN(i) * rGeometry[i].FastGetSolutionStepValue(WATER_PRESSURE);
      double DegreeOfSaturation = EvaluateRetentionCurve(rVariables.GetProperties(), WaterPressure, DegreeOfSaturation);

      // VelocityGradient
      Matrix Velocity_DX = ZeroMatrix(dimension, dimension);
      for (unsigned int k = 0; k < number_of_nodes; k++) {
         const array_1d<double, 3> &CurrentDisplacement = rGeometry[k].FastGetSolutionStepValue(DISPLACEMENT);
         const array_1d<double, 3> &PreviousDisplacement = rGeometry[k].FastGetSolutionStepValue(DISPLACEMENT, 1);
         array_1d<double, 3> DeltaDisplacement = CurrentDisplacement - PreviousDisplacement;
         for (unsigned int j = 0; j < dimension; j++) {
            for (unsigned int i = 0; i < dimension; i++) {
               Velocity_DX(i, j) += DeltaDisplacement[i] * rDN_DX(k, j);
            }
         }
      }

      for (unsigned int i = 0; i < number_of_nodes; i++) {
         for (unsigned int p = 0; p < number_of_nodes; p++) {
            for (unsigned int pDim = 0; pDim < dimension; pDim++) { 

               rLocalLHS(i, p * dimension + pDim) += rN(i) * rDN_DX(p, pDim);

               // LD Term
               for (unsigned int l = 0; l < dimension; l++) {
                  rLocalLHS(i, p * dimension + pDim) -= rN(i) * Velocity_DX(l, pDim) * rDN_DX(p, l);
               }
            }
         }
      }

      rLocalLHS *= (rIntegrationWeight * ScalingConstant) * DegreeOfSaturation;

      return rLocalLHS;
      KRATOS_CATCH("")
   }

   Matrix &UnsaturatedWaterPressureUtilities::ComputeLinearization_Displacement_WaterPressure(HydroMechanicalVariables &rVariables, Matrix &rLocalLHS, const double &rIntegrationWeight)
   {

      KRATOS_TRY
      const unsigned int number_of_nodes = rVariables.GetGeometry().PointsNumber();
      const unsigned int dimension = rVariables.GetGeometry().WorkingSpaceDimension();
      const MatrixType &rDN_DX = rVariables.GetShapeFunctionsDerivatives();
      const VectorType &rN = rVariables.GetShapeFunctions();

      const GeometryType &rGeometry = rVariables.GetGeometry();

      // 1. Some constants
      double ScalingConstant;
      GetScalingConstant(ScalingConstant, rVariables.GetProperties());

      Matrix K;
      double initial_porosity = rVariables.GetProperties().GetValue(INITIAL_POROSITY);
      double VolumeChange = this->CalculateVolumeChange(rVariables.GetGeometry(), rVariables.GetShapeFunctions(), rVariables.GetDeformationGradient(), rVariables.GetProperties(), rVariables.detF0);
      double WaterPressureOld = GetWaterPressureOld( rVariables.GetShapeFunctions(), rGeometry,  WaterPressureOld);
      this->GetPermeabilityTensor(rVariables.GetProperties(), rVariables.GetDeformationGradient(), K, initial_porosity, VolumeChange, WaterPressureOld);

      rLocalLHS.resize(number_of_nodes, number_of_nodes);
      noalias(rLocalLHS) = ZeroMatrix(number_of_nodes, number_of_nodes);

      double WaterPressure(0.0);
      for (unsigned int i = 0; i < number_of_nodes; i++)
         WaterPressure += rGeometry[i].GetSolutionStepValue(WATER_PRESSURE) * rN(i);
      double dSr_dWaterPressure = DerivativeRetentionCurve( rVariables.GetProperties(), WaterPressure, dSr_dWaterPressure);

      double VolumeStrain(0);
      // DeltaDisplacement
      for (unsigned int i = 0; i < number_of_nodes; i++) {
         for (unsigned int p = 0; p < dimension; p++) {
            const array_1d<double, 3> &CurrentDisplacement = rGeometry[i].FastGetSolutionStepValue(DISPLACEMENT);
            const array_1d<double, 3> &PreviousDisplacement = rGeometry[i].FastGetSolutionStepValue(DISPLACEMENT, 1);
            array_1d<double, 3> DeltaDisplacement = CurrentDisplacement - PreviousDisplacement;
            VolumeStrain += rDN_DX(i,p)*DeltaDisplacement[p];
         }
      }




      for (unsigned int i = 0; i < number_of_nodes; i++) {
         for (unsigned int j = 0; j < number_of_nodes; j++) {
            rLocalLHS(i, j) += rN(i) *  VolumeStrain * dSr_dWaterPressure * rN(j);
         }
      }
      rLocalLHS *= (rIntegrationWeight * ScalingConstant);

      return rLocalLHS;

      KRATOS_CATCH("")
   }




   // ***************** Tangent To water Contribution to internal forces *********************************
   // ****************************************************************************************************
   Matrix &UnsaturatedWaterPressureUtilities::ComputeWaterPressureKUwP(HydroMechanicalVariables &rVariables, MatrixType &rLocalLHS, const double &rIntegrationWeight)
   {
      KRATOS_TRY

      const GeometryType &rGeometry = rVariables.GetGeometry();
      const VectorType &rN = rVariables.GetShapeFunctions();
      const MatrixType &rDN_DX = rVariables.GetShapeFunctionsDerivatives();
      const MatrixType &rB = rVariables.GetBMatrix();
      const VectorType & rConstitutiveVector = rVariables.GetConstitutiveVector();

      const unsigned int number_of_nodes = rVariables.GetGeometry().PointsNumber();
      const unsigned int dimension = rVariables.GetGeometry().WorkingSpaceDimension();

      rLocalLHS.resize(number_of_nodes * dimension, number_of_nodes);
      noalias(rLocalLHS) = ZeroMatrix(number_of_nodes * dimension, number_of_nodes);


      double WaterPressure(0.0);
      for (unsigned int i = 0; i < number_of_nodes; i++)
         WaterPressure += rN(i) * rGeometry[i].FastGetSolutionStepValue(WATER_PRESSURE);
      double DegreeOfSaturation_Eff = EvaluateEffectiveRetentionCurve(rVariables.GetProperties(), WaterPressure, DegreeOfSaturation_Eff);
      double dSrEff_dWaterPressure = DerivativeEffectiveRetentionCurve( rVariables.GetProperties(), WaterPressure, dSrEff_dWaterPressure);


      double term = DegreeOfSaturation_Eff + WaterPressure * dSrEff_dWaterPressure;

      for (unsigned int i = 0; i < number_of_nodes; i++)
      {
         for (unsigned int j = 0; j < number_of_nodes; j++)
         {
            for (unsigned int dim = 0; dim < dimension; dim++)
            {
               rLocalLHS(i * dimension + dim, j) += rDN_DX(i, dim) * term * rN(j) * rIntegrationWeight;
            }
         }
      }

      Vector BD;
      BD = prod( trans(rB), rConstitutiveVector);
      for (unsigned int i = 0; i < number_of_nodes; i++) {
         for (unsigned int j = 0; j < number_of_nodes; j++) {
            for (unsigned int dim = 0; dim < dimension; dim++) {
               rLocalLHS(i * dimension + dim, j) += BD(i*dimension+dim) * rN(j) * rIntegrationWeight;
            }
         }
      }

      return rLocalLHS;

      KRATOS_CATCH("")
   }

   // ************ Contribution of water pressure to geometric stiffness *********************************
   // ****************************************************************************************************
   Matrix &UnsaturatedWaterPressureUtilities::ComputeWaterPressureKuug(HydroMechanicalVariables &rVariables, Matrix &rLocalLHS, const double &rIntegrationWeight)
   {
      KRATOS_TRY

      const GeometryType &rGeometry = rVariables.GetGeometry();
      const unsigned int number_of_nodes = rVariables.GetGeometry().PointsNumber();
      const unsigned int dimension = rVariables.GetGeometry().WorkingSpaceDimension();

      unsigned int voigtsize, principal_dimension;
      this->GetVoigtSize(dimension, voigtsize, principal_dimension);

      double WaterPressure = 0.0;
      const VectorType &rN = rVariables.GetShapeFunctions();

      for (unsigned int i = 0; i < number_of_nodes; i++)
         WaterPressure += rN(i) * rGeometry[i].FastGetSolutionStepValue(WATER_PRESSURE);
      double DegreeOfSaturation_Eff = EvaluateEffectiveRetentionCurve(rVariables.GetProperties(), WaterPressure, DegreeOfSaturation_Eff);


      // the term is  pw * ( one times one - 2 I)  (not the symmetric, but who cares
      Matrix StiffnessMatrix = ZeroMatrix(voigtsize, voigtsize);
      for (unsigned int i = 0; i < principal_dimension; i++)
      {
         for (unsigned int j = 0; j < principal_dimension; j++)
         {
            StiffnessMatrix(i, j) = 1.0;
         }
      }

      double voigtNumber = 1.0;
      for (unsigned int i = 0; i < voigtsize; i++)
      {
         if (i >= principal_dimension)
            voigtNumber = 0.5;
         StiffnessMatrix(i, i) -= voigtNumber;
      }

      StiffnessMatrix *= WaterPressure*DegreeOfSaturation_Eff;

      const MatrixType &rB = rVariables.GetBMatrix();
      rLocalLHS = prod(trans(rB), rIntegrationWeight * Matrix(prod(StiffnessMatrix, rB)));

      return rLocalLHS;

      KRATOS_CATCH("")
   }

   // ****************** COMPUTE THE FORCES DUE TO GRAVITY WITH THE DIFFERENT DEN *********************
   // *************************************************************************************************
   Vector &UnsaturatedWaterPressureUtilities::CalculateVolumeForcesContribution(HydroMechanicalVariables &rVariables, VectorType &rLocalRHS, const double &rIntegrationWeight)
   {
      KRATOS_TRY

      const unsigned int number_of_nodes = rVariables.GetGeometry().PointsNumber();
      unsigned int dimension = rVariables.GetGeometry().WorkingSpaceDimension();

      Vector VolumeForce = rVariables.GetVolumeForce();
      VolumeForce *= rVariables.detF0;

      rLocalRHS.resize(number_of_nodes * dimension, false);
      noalias(rLocalRHS) = ZeroVector(number_of_nodes * dimension);

      double density_mixture0 = rVariables.GetProperties().GetValue(DENSITY);
      if (density_mixture0 > 0)
      {
         VolumeForce /= density_mixture0;
      }
      else
      {
         return rLocalRHS;
      }

      double density_water = rVariables.GetProperties().GetValue(DENSITY_WATER);
      double porosity0 = rVariables.GetProperties().GetValue(INITIAL_POROSITY);

      double porosity = 1.0 - (1.0 - porosity0) / rVariables.detF0;
      double density_solid = (density_mixture0 - porosity0 * density_water) / (1.0 - porosity0);
      double density_mixture = (1.0 - porosity) * density_solid + porosity * density_water;

      const VectorType &rN = rVariables.GetShapeFunctions();
      for (unsigned int i = 0; i < number_of_nodes; i++)
      {
         for (unsigned int iDim = 0; iDim < dimension; iDim++)
         {
            rLocalRHS(i * dimension + iDim) += rIntegrationWeight * density_mixture * rN(i) * VolumeForce(iDim);
         }
      }

      return rLocalRHS;

      KRATOS_CATCH("")
   }
   // ****************** COMPUTE WATER PRESSURE CONTRIBUTION TO THE LINEAR MOMENTUM EQUATION ***********
   // **************************************************************************************************
   Vector &UnsaturatedWaterPressureUtilities::CalculateWaterInternalForcesContribution(HydroMechanicalVariables &rVariables, Vector &rRightHandSideVector, const double &rIntegrationWeight)
   {
      KRATOS_TRY

      const GeometryType &rGeometry = rVariables.GetGeometry();
      const VectorType &rN = rVariables.GetShapeFunctions();

      const unsigned int number_of_nodes = rGeometry.PointsNumber();
      unsigned int dimension = rGeometry.WorkingSpaceDimension();

      double WaterPressure = 0.0;
      for (unsigned int i = 0; i < number_of_nodes; i++)
      {
         WaterPressure += rGeometry[i].GetSolutionStepValue(WATER_PRESSURE) * rN(i);
      }

      double DegreeOfSaturation_Eff = EvaluateEffectiveRetentionCurve(rVariables.GetProperties(), WaterPressure, DegreeOfSaturation_Eff);


      unsigned int voigtsize, principal_dimension;
      this->GetVoigtSize(dimension, voigtsize, principal_dimension);
      Vector WaterPressureVector = ZeroVector(voigtsize);

      for (unsigned int i = 0; i < principal_dimension; i++) // something must be placed here for the axisym ...
         WaterPressureVector(i) = 1.0;
      WaterPressureVector *= (WaterPressure * DegreeOfSaturation_Eff);

      const MatrixType rB = rVariables.GetBMatrix();
      rRightHandSideVector.resize(number_of_nodes * dimension, false);
      noalias(rRightHandSideVector) = -rIntegrationWeight * prod(trans(rB), WaterPressureVector);

      return rRightHandSideVector;

      KRATOS_CATCH("")
   }

   // ***************** COMPUTE WATER PRESSURE RHS (mass balance equation, without solid skeleton deformation) ************
   // *********************************************************************************************************************
   Vector &UnsaturatedWaterPressureUtilities::CalculateMassBalance_WaterPressurePart(HydroMechanicalVariables &rVariables, VectorType &rRightHandSideVector, const double &rIntegrationWeight)
   {

      KRATOS_TRY

      // 1. Some constants
      double ScalingConstant;
      GetScalingConstant(ScalingConstant, rVariables.GetProperties());
      const VectorType &rN = rVariables.GetShapeFunctions();
      const MatrixType &rDN_DX = rVariables.GetShapeFunctionsDerivatives();

      // 2. Geometric
      const GeometryType &rGeometry = rVariables.GetGeometry();
      const unsigned int number_of_nodes = rGeometry.PointsNumber();
      unsigned int dimension = rGeometry.WorkingSpaceDimension();

      bool density_considered = false;
      if (rGeometry[0].SolutionStepsDataHas(VOLUME_ACCELERATION))
      {
         const array_1d<double, 3> VolAcc = rGeometry[0].FastGetSolutionStepValue(VOLUME_ACCELERATION);
         if ((fabs(VolAcc[0]) + fabs(VolAcc[1]) + fabs(VolAcc[2])) > 1.0e-12)
         {
            density_considered = true;
         }
      }

      //3. Gravity & Permeability
      Vector b = ZeroVector(dimension);
      double WaterDensity = 0.0;
      if (density_considered)
         WaterDensity = rVariables.GetProperties().GetValue(DENSITY_WATER);
      b(dimension - 1) = -10.0 * WaterDensity;

      double initial_porosity = rVariables.GetProperties().GetValue(INITIAL_POROSITY);
      double VolumeChange = this->CalculateVolumeChange(rGeometry, rN, rVariables.GetDeformationGradient(), rVariables.GetProperties(), rVariables.detF0 );

      Matrix K;
      double WaterPressureOld = GetWaterPressureOld( rVariables.GetShapeFunctions(), rVariables.GetGeometry(),  WaterPressureOld);
      this->GetPermeabilityTensor(rVariables.GetProperties(), rVariables.GetDeformationGradient(), K, initial_porosity, VolumeChange, WaterPressureOld);

      rRightHandSideVector.resize(number_of_nodes);
      noalias(rRightHandSideVector) = ZeroVector(number_of_nodes);

      for (unsigned int i = 0; i < number_of_nodes; i++)
      {
         for (unsigned int j = 0; j < number_of_nodes; j++)
         {

            // DeltaPressure
            const double &CurrentPressure = rGeometry[j].FastGetSolutionStepValue(WATER_PRESSURE);

            for (unsigned int p = 0; p < dimension; ++p)
            {

               for (unsigned int q = 0; q < dimension; ++q)
               {
                  // Darcy flow
                  rRightHandSideVector(i) += rVariables.DeltaTime * rDN_DX(i, p) * K(p, q) * rDN_DX(j, q) * CurrentPressure * rIntegrationWeight * ScalingConstant / rVariables.detF0;

                  if (j == 0)
                  {
                     // Gravity term of the DARCY FLOW.
                     rRightHandSideVector(i) += rVariables.DeltaTime * rDN_DX(i, p) * K(p, q) * b(q) * rIntegrationWeight * ScalingConstant / rVariables.detF0;
                  }
               }
            }
         }
      }

      return rRightHandSideVector;

      KRATOS_CATCH("")
   }


   // *************** RHS of the Hydromechanical Problem. Add the solid skeleton movement part *****************************
   // **********************************************************************************************************************
   Vector &UnsaturatedWaterPressureUtilities::CalculateMassBalance_AddDegreeOfSaturationPart(HydroMechanicalVariables &rVariables, VectorType &rRightHandSideVector, const double &rIntegrationWeight)
   {
      KRATOS_TRY

      double ScalingConstant;
      GetScalingConstant(ScalingConstant, rVariables.GetProperties());

      // 2. Geometric
      const GeometryType &rGeometry = rVariables.GetGeometry();
      const unsigned int number_of_nodes = rGeometry.PointsNumber();

      const VectorType &rN = rVariables.GetShapeFunctions();

      double WaterPressure = 0.0;
      double WaterPressureOld = 0.0;
      for (unsigned int i = 0; i < number_of_nodes; i++)
      {
         WaterPressure += rGeometry[i].GetSolutionStepValue(WATER_PRESSURE) * rN(i);
         WaterPressureOld += rGeometry[i].GetSolutionStepValue(WATER_PRESSURE,1) * rN(i);
      }

      double DegreeOfSaturation = EvaluateRetentionCurve(rVariables.GetProperties(), WaterPressure, DegreeOfSaturation);
      double DegreeOfSaturationOld = EvaluateRetentionCurve(rVariables.GetProperties(), WaterPressureOld, DegreeOfSaturationOld);


      double VolumeChange = this->CalculateVolumeChange(rGeometry, rVariables.GetShapeFunctions(), rVariables.GetDeformationGradient(), rVariables.GetProperties(), rVariables.detF0 );
      const double & rInitialPorosity = rVariables.GetProperties().GetValue(INITIAL_POROSITY);
      double Porosity = 1.0 - (1.0 - rInitialPorosity) / VolumeChange;

      for (unsigned int i = 0; i < number_of_nodes; i++) {
         rRightHandSideVector(i) -= rN(i) * Porosity * (DegreeOfSaturation-DegreeOfSaturationOld) *  rIntegrationWeight * ScalingConstant / rVariables.detF0;
      }

      return rRightHandSideVector;

      KRATOS_CATCH("")
   }



   // *************** RHS of the Hydromechanical Problem. Add the solid skeleton movement part *****************************
   // **********************************************************************************************************************
   Vector &UnsaturatedWaterPressureUtilities::CalculateMassBalance_AddDisplacementPart(HydroMechanicalVariables &rVariables, VectorType &rRightHandSideVector, const double &rIntegrationWeight)
   {
      KRATOS_TRY

      double ScalingConstant;
      GetScalingConstant(ScalingConstant, rVariables.GetProperties());

      // 2. Geometric
      const GeometryType &rGeometry = rVariables.GetGeometry();
      const unsigned int number_of_nodes = rGeometry.PointsNumber();
      unsigned int dimension = rGeometry.WorkingSpaceDimension();

      const VectorType &rN = rVariables.GetShapeFunctions();
      const MatrixType &rDN_DX = rVariables.GetShapeFunctionsDerivatives();

      double WaterPressure = 0.0;
      for (unsigned int i = 0; i < number_of_nodes; i++)
      {
         WaterPressure += rGeometry[i].GetSolutionStepValue(WATER_PRESSURE) * rN(i);
      }

      double DegreeOfSaturation = EvaluateRetentionCurve(rVariables.GetProperties(), WaterPressure, DegreeOfSaturation);

      for (unsigned int i = 0; i < number_of_nodes; i++) {
         for (unsigned int j = 0; j < number_of_nodes; j++) {

            // DeltaDisplacement
            const array_1d<double, 3> &CurrentDisplacement = rGeometry[j].FastGetSolutionStepValue(DISPLACEMENT);
            const array_1d<double, 3> &PreviousDisplacement = rGeometry[j].FastGetSolutionStepValue(DISPLACEMENT, 1);
            array_1d<double, 3> DeltaDisplacement = CurrentDisplacement - PreviousDisplacement;

            for (unsigned int p = 0; p < dimension; ++p) {
               // Solid Skeleton volumetric deformation
               rRightHandSideVector(i) -= rN(i) * DegreeOfSaturation*DeltaDisplacement[p] * rDN_DX(j, p) * rIntegrationWeight * ScalingConstant / rVariables.detF0;
            }
         }
      }

      return rRightHandSideVector;

      KRATOS_CATCH("")
   }

   double & UnsaturatedWaterPressureUtilities::EvaluateEffectiveRetentionCurve( const PropertiesType & rProperties, const double & rWaterPressure, double & rDegreeOfSaturation)
   {
      KRATOS_TRY

      if ( rWaterPressure <= 0.0) {
         rDegreeOfSaturation = 1.0;
         return rDegreeOfSaturation;
      }


      const double & rA = rProperties.GetValue(RETENTION_CURVE_A);
      const double & rB = rProperties.GetValue(RETENTION_CURVE_B);
      const double & rC = rProperties.GetValue(RETENTION_CURVE_C);

      rDegreeOfSaturation = pow( 1 + pow(rWaterPressure/rA, rB), -rC);
      
      return rDegreeOfSaturation;

      KRATOS_CATCH("")
   }

   double & UnsaturatedWaterPressureUtilities::EvaluateRetentionCurve( const PropertiesType & rProperties, const double & rWaterPressure, double & rDegreeOfSaturation)
   {
      KRATOS_TRY

      double SrRes(0.0);
      double SrSat(1.0);
      if ( rProperties.Has(DEGREE_OF_SATURATION_RES) )
            SrRes = rProperties.GetValue(DEGREE_OF_SATURATION_RES);
      if (rProperties.Has(DEGREE_OF_SATURATION_SAT) )
            SrSat = rProperties.GetValue(DEGREE_OF_SATURATION_SAT);
      
      if ( SrRes > SrSat)
         KRATOS_ERROR << " Bad parameters for the Water retentioncurve, SrRes > SrSat " << std::endl;

      rDegreeOfSaturation = EvaluateEffectiveRetentionCurve( rProperties, rWaterPressure, rDegreeOfSaturation);
      rDegreeOfSaturation = SrRes + (SrSat-SrRes)*rDegreeOfSaturation;
      
      return rDegreeOfSaturation;

      KRATOS_CATCH("")
   }

   double & UnsaturatedWaterPressureUtilities::DerivativeEffectiveRetentionCurve( const PropertiesType & rProperties, const double & rWaterPressure, double & rDerivative)
   {
      KRATOS_TRY

      if ( rWaterPressure <= 0.0) {
         rDerivative = 0.0;
         return rDerivative;
      }


      const double & rA = rProperties.GetValue(RETENTION_CURVE_A);
      const double & rB = rProperties.GetValue(RETENTION_CURVE_B);
      const double & rC = rProperties.GetValue(RETENTION_CURVE_C);

      rDerivative = -rB*rC /rA * pow( 1+  pow(rWaterPressure/rA, rB), -rC-1.0) * pow( rWaterPressure/rA, rB-1.0);
      
      double DegreeOfSaturation = this->EvaluateRetentionCurve( rProperties, rWaterPressure, DegreeOfSaturation);
      //std::cout << " WATER PRESSURE " << rWaterPressure << " DEGREE OF SATURATION " << DegreeOfSaturation << " DERIVATIVE " << rDerivative << std::endl;
      return rDerivative;

      KRATOS_CATCH("")
   }
   double & UnsaturatedWaterPressureUtilities::DerivativeRetentionCurve( const PropertiesType & rProperties, const double & rWaterPressure, double & rDerivative)
   {
      KRATOS_TRY


      double SrRes(0.0), SrSat(1.0);
      if ( rProperties.Has(DEGREE_OF_SATURATION_RES) )
            SrRes = rProperties.GetValue(DEGREE_OF_SATURATION_RES);
      if (rProperties.Has(DEGREE_OF_SATURATION_SAT) )
            SrSat = rProperties.GetValue(DEGREE_OF_SATURATION_SAT);


      rDerivative = DerivativeEffectiveRetentionCurve( rProperties, rWaterPressure, rDerivative);
      rDerivative *= (SrSat-SrRes);
      
      double DegreeOfSaturation = this->EvaluateRetentionCurve( rProperties, rWaterPressure, DegreeOfSaturation);
      //std::cout << " WATER PRESSURE " << rWaterPressure << " DEGREE OF SATURATION " << DegreeOfSaturation << " DERIVATIVE " << rDerivative << std::endl;
      return rDerivative;

      KRATOS_CATCH("")
   }

void UnsaturatedWaterPressureUtilities::GetPermeabilityTensor(const PropertiesType &rProperties, const Matrix &rF, Matrix &rPermeabilityTensor, const double &rInitialPorosity, const double &rVolumeChange, const double & rWaterPressure)
{
   KRATOS_TRY


   WaterPressureUtilities::GetPermeabilityTensor( rProperties, rF, rPermeabilityTensor, rInitialPorosity, rVolumeChange, rWaterPressure);
      
   const double & rLambda = rProperties.GetValue(RETENTION_CURVE_C);
   double D(0.5);
   if (rProperties.Has(RETENTION_CURVE_D) )
       D = rProperties.GetValue(RETENTION_CURVE_D);
   double DegreeOfSaturation = EvaluateRetentionCurve(rProperties, rWaterPressure, DegreeOfSaturation);
   double ThisTerm = pow( DegreeOfSaturation, D)*pow( 1.0 - pow(1.0 - pow(DegreeOfSaturation, 1/rLambda), rLambda), 2.0);
   rPermeabilityTensor *= ThisTerm;
   //std::cout << " DegreeOfSaturation " << DegreeOfSaturation << " this Term " << ThisTerm << " permeability " << rPermeabilityTensor(0,0) << std::endl;


   KRATOS_CATCH("")
}

} // namespace Kratos
// end namespace kratos
