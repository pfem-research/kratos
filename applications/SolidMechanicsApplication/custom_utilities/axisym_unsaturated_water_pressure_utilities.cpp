//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:           LMonforte $
//   Maintained by:       $Maintainer:                 LM $
//   Date:                $Date:            December 2021 $
//

#include "custom_utilities/axisym_unsaturated_water_pressure_utilities.hpp"
#include "solid_mechanics_application_variables.h"

namespace Kratos
{
   AxisymUnsaturatedWaterPressureUtilities::AxisymUnsaturatedWaterPressureUtilities(): UnsaturatedWaterPressureUtilities()
   {
   }

   // ***************** Tangent To water Contribution to internal forces *********************************
   // ****************************************************************************************************
   Matrix &AxisymUnsaturatedWaterPressureUtilities::ComputeWaterPressureKUwP(HydroMechanicalVariables &rVariables, MatrixType &rLocalLHS, const double &rIntegrationWeight)
   {
      KRATOS_TRY

      const GeometryType &rGeometry = rVariables.GetGeometry();
      const VectorType &rN = rVariables.GetShapeFunctions();
      const MatrixType &rDN_DX = rVariables.GetShapeFunctionsDerivatives();
      const MatrixType &rB = rVariables.GetBMatrix();
      const VectorType & rConstitutiveVector = rVariables.GetConstitutiveVector();

      const unsigned int number_of_nodes = rVariables.GetGeometry().PointsNumber();
      const unsigned int dimension = rVariables.GetGeometry().WorkingSpaceDimension();
   
      rLocalLHS.resize(number_of_nodes * dimension, number_of_nodes);
      noalias(rLocalLHS) = ZeroMatrix(number_of_nodes * dimension, number_of_nodes);


      double WaterPressure(0.0);
      for (unsigned int i = 0; i < number_of_nodes; i++)
         WaterPressure += rN(i) * rGeometry[i].FastGetSolutionStepValue(WATER_PRESSURE);
      double DegreeOfSaturation_Eff = EvaluateEffectiveRetentionCurve(rVariables.GetProperties(), WaterPressure, DegreeOfSaturation_Eff);
      double dSrEff_dWaterPressure = DerivativeEffectiveRetentionCurve( rVariables.GetProperties(), WaterPressure, dSrEff_dWaterPressure);


      double term = DegreeOfSaturation_Eff + WaterPressure * dSrEff_dWaterPressure;

      for (unsigned int i = 0; i < number_of_nodes; i++)
      {
         for (unsigned int j = 0; j < number_of_nodes; j++)
         {
            for (unsigned int dim = 0; dim < dimension; dim++)
            {
               rLocalLHS(i * dimension + dim, j) += rDN_DX(i, dim) * term * rN(j) * rIntegrationWeight;
               if (dim == 0)
                  rLocalLHS(i * dimension + dim, j) += rN(i) * term * rN(j) * (1.0 / rVariables.CurrentRadius) * rIntegrationWeight;
            }
         }
      }

      Vector BD;
      BD = prod( trans(rB), rConstitutiveVector);
      for (unsigned int i = 0; i < number_of_nodes; i++) {
         for (unsigned int j = 0; j < number_of_nodes; j++) {
            for (unsigned int dim = 0; dim < dimension; dim++) {
               rLocalLHS(i * dimension + dim, j) += BD(i*dimension+dim) * rN(j) * rIntegrationWeight;
            }
         }
      }

      return rLocalLHS;

      KRATOS_CATCH("")
   }

   // *************** RHS of the Hydromechanical Problem. Add the solid skeleton movement part *****************************
   // **********************************************************************************************************************
   Vector &AxisymUnsaturatedWaterPressureUtilities::CalculateMassBalance_AddDisplacementPart(HydroMechanicalVariables &rVariables, VectorType &rRightHandSideVector, const double &rIntegrationWeight)
   {
      KRATOS_TRY

      double ScalingConstant;
      GetScalingConstant(ScalingConstant, rVariables.GetProperties());

      // 2. Geometric
      const GeometryType &rGeometry = rVariables.GetGeometry();
      const unsigned int number_of_nodes = rGeometry.PointsNumber();
      unsigned int dimension = rGeometry.WorkingSpaceDimension();

      const VectorType &rN = rVariables.GetShapeFunctions();
      const MatrixType &rDN_DX = rVariables.GetShapeFunctionsDerivatives();

      double WaterPressure = 0.0;
      for (unsigned int i = 0; i < number_of_nodes; i++)
      {
         WaterPressure += rGeometry[i].GetSolutionStepValue(WATER_PRESSURE) * rN(i);
      }

      double DegreeOfSaturation = EvaluateRetentionCurve(rVariables.GetProperties(), WaterPressure, DegreeOfSaturation);

      for (unsigned int i = 0; i < number_of_nodes; i++) {
         for (unsigned int j = 0; j < number_of_nodes; j++) {

            // DeltaDisplacement
            const array_1d<double, 3> &CurrentDisplacement = rGeometry[j].FastGetSolutionStepValue(DISPLACEMENT);
            const array_1d<double, 3> &PreviousDisplacement = rGeometry[j].FastGetSolutionStepValue(DISPLACEMENT, 1);
            array_1d<double, 3> DeltaDisplacement = CurrentDisplacement - PreviousDisplacement;

            for (unsigned int p = 0; p < dimension; ++p) {
               // Solid Skeleton volumetric deformation
               rRightHandSideVector(i) -= rN(i) * DegreeOfSaturation*DeltaDisplacement[p] * rDN_DX(j, p) * rIntegrationWeight * ScalingConstant / rVariables.detF0;
            
               if (p == 0)
                  rRightHandSideVector(i) -= rN(i) * DegreeOfSaturation * DeltaDisplacement[p] *  rN(j) * (1.0 / rVariables.CurrentRadius) * rIntegrationWeight * ScalingConstant / rVariables.detF0;
            }
         }
      }

      return rRightHandSideVector;

      KRATOS_CATCH("")
   }

   Matrix &AxisymUnsaturatedWaterPressureUtilities::ComputeLinearization_Displacement_Displacement(HydroMechanicalVariables &rVariables, Matrix &rLocalLHS, const double &rIntegrationWeight)
   {
      KRATOS_TRY

      const GeometryType &rGeometry = rVariables.GetGeometry();
      const unsigned int number_of_nodes = rGeometry.PointsNumber();
      const unsigned int dimension = rGeometry.WorkingSpaceDimension();

      // 1. Some constants
      double ScalingConstant;
      GetScalingConstant(ScalingConstant, rVariables.GetProperties());

      // 2. Calculate Matrix
      rLocalLHS.resize(number_of_nodes, number_of_nodes * dimension);
      noalias(rLocalLHS) = ZeroMatrix(number_of_nodes, number_of_nodes * dimension);

      const MatrixType &rDN_DX = rVariables.GetShapeFunctionsDerivatives();
      const VectorType &rN = rVariables.GetShapeFunctions();

      double WaterPressure(0.0);
      for (unsigned int i = 0; i < number_of_nodes; i++)
         WaterPressure += rN(i) * rGeometry[i].FastGetSolutionStepValue(WATER_PRESSURE);
      double DegreeOfSaturation = EvaluateRetentionCurve(rVariables.GetProperties(), WaterPressure, DegreeOfSaturation);

      // VelocityGradient
      Matrix Velocity_DX = ZeroMatrix(dimension, dimension);
      for (unsigned int k = 0; k < number_of_nodes; k++) {
         const array_1d<double, 3> &CurrentDisplacement = rGeometry[k].FastGetSolutionStepValue(DISPLACEMENT);
         const array_1d<double, 3> &PreviousDisplacement = rGeometry[k].FastGetSolutionStepValue(DISPLACEMENT, 1);
         array_1d<double, 3> DeltaDisplacement = CurrentDisplacement - PreviousDisplacement;
         for (unsigned int j = 0; j < dimension; j++) {
            for (unsigned int i = 0; i < dimension; i++) {
               Velocity_DX(i, j) += DeltaDisplacement[i] * rDN_DX(k, j);
            }
         }
      }

      for (unsigned int i = 0; i < number_of_nodes; i++) {
         for (unsigned int p = 0; p < number_of_nodes; p++) {
            for (unsigned int pDim = 0; pDim < dimension; pDim++) { 

               rLocalLHS(i, p * dimension + pDim) += rN(i) * rDN_DX(p, pDim);
               
               if (pDim == 0)
                  rLocalLHS(i, p * dimension + pDim) += rN(i) * rN(p) * (1.0 / rVariables.CurrentRadius);

               // LD Term
               /*for (unsigned int l = 0; l < dimension; l++) {
                  rLocalLHS(i, p * dimension + pDim) -= rN(i) * Velocity_DX(l, pDim) * rDN_DX(p, l);
               }*/
            }
         }
      }

      rLocalLHS *= (rIntegrationWeight * ScalingConstant) * DegreeOfSaturation;

      return rLocalLHS;
      KRATOS_CATCH("")
   }

   Matrix &AxisymUnsaturatedWaterPressureUtilities::ComputeLinearization_Displacement_WaterPressure(HydroMechanicalVariables &rVariables, Matrix &rLocalLHS, const double &rIntegrationWeight)
   {

      KRATOS_TRY
      const unsigned int number_of_nodes = rVariables.GetGeometry().PointsNumber();
      const unsigned int dimension = rVariables.GetGeometry().WorkingSpaceDimension();
      const MatrixType &rDN_DX = rVariables.GetShapeFunctionsDerivatives();
      const VectorType &rN = rVariables.GetShapeFunctions();

      const GeometryType &rGeometry = rVariables.GetGeometry();

      // 1. Some constants
      double ScalingConstant;
      GetScalingConstant(ScalingConstant, rVariables.GetProperties());

      Matrix K;
      double initial_porosity = rVariables.GetProperties().GetValue(INITIAL_POROSITY);
      double VolumeChange = this->CalculateVolumeChange(rVariables.GetGeometry(), rVariables.GetShapeFunctions(), rVariables.GetDeformationGradient(), rVariables.GetProperties(), rVariables.detF0 );
      double WaterPressureOld = GetWaterPressureOld( rVariables.GetShapeFunctions(), rGeometry,  WaterPressureOld);
      this->GetPermeabilityTensor(rVariables.GetProperties(), rVariables.GetDeformationGradient(), K, initial_porosity, VolumeChange, WaterPressureOld);

      rLocalLHS.resize(number_of_nodes, number_of_nodes);
      noalias(rLocalLHS) = ZeroMatrix(number_of_nodes, number_of_nodes);

      double WaterPressure(0.0);
      for (unsigned int i = 0; i < number_of_nodes; i++)
         WaterPressure += rGeometry[i].GetSolutionStepValue(WATER_PRESSURE) * rN(i);
      double dSr_dWaterPressure = DerivativeRetentionCurve( rVariables.GetProperties(), WaterPressure, dSr_dWaterPressure);

      double VolumeStrain(0);
      // DeltaDisplacement
      for (unsigned int i = 0; i < number_of_nodes; i++) {
         for (unsigned int p = 0; p < dimension; p++) {
            const array_1d<double, 3> &CurrentDisplacement = rGeometry[i].FastGetSolutionStepValue(DISPLACEMENT);
            const array_1d<double, 3> &PreviousDisplacement = rGeometry[i].FastGetSolutionStepValue(DISPLACEMENT, 1);
            array_1d<double, 3> DeltaDisplacement = CurrentDisplacement - PreviousDisplacement;
            VolumeStrain += rDN_DX(i,p)*DeltaDisplacement[p];
         }
      }




      for (unsigned int i = 0; i < number_of_nodes; i++) {
         for (unsigned int j = 0; j < number_of_nodes; j++) {
            rLocalLHS(i, j) += rN(i) *  VolumeStrain * dSr_dWaterPressure * rN(j);
         }
      }
      rLocalLHS *= (rIntegrationWeight * ScalingConstant);

      return rLocalLHS;

      KRATOS_CATCH("")
   }

   // ******* Tangent to Mass conservation with Respect WaterPressure ***********************************
   // ***************************************************************************************************
   Matrix &AxisymUnsaturatedWaterPressureUtilities::ComputeLinearization_DegreeOfSaturation_Displacement(HydroMechanicalVariables &rVariables, Matrix &rLocalLHS, const double &rIntegrationWeight)
   {
      KRATOS_TRY

      const GeometryType &rGeometry = rVariables.GetGeometry();

      const unsigned int number_of_nodes = rGeometry.PointsNumber();
      const unsigned int dimension = rGeometry.WorkingSpaceDimension();

      // 1. Some constants
      double ScalingConstant;
      GetScalingConstant(ScalingConstant, rVariables.GetProperties());


      const MatrixType &rB = rVariables.GetBMatrix();
      const VectorType &rN = rVariables.GetShapeFunctions();

      double WaterPressure(0.0);
      double WaterPressureOld(0.0);
      for (unsigned int i = 0; i < number_of_nodes; i++) {
         WaterPressure += rGeometry[i].GetSolutionStepValue(WATER_PRESSURE) * rN(i);
         WaterPressureOld += rGeometry[i].GetSolutionStepValue(WATER_PRESSURE,1) * rN(i);
      }

      double DegreeOfSaturation = EvaluateRetentionCurve(rVariables.GetProperties(), WaterPressure, DegreeOfSaturation);
      double DegreeOfSaturationOld = EvaluateRetentionCurve(rVariables.GetProperties(), WaterPressureOld, DegreeOfSaturationOld);

      double DeltaSr = DegreeOfSaturation-DegreeOfSaturationOld;

      const double & rInitialPorosity = rVariables.GetProperties().GetValue(INITIAL_POROSITY);


      rLocalLHS.resize(number_of_nodes, number_of_nodes * dimension);
      noalias(rLocalLHS) = ZeroMatrix(number_of_nodes, number_of_nodes * dimension);

      Vector mVector(4);
      noalias (mVector ) = ZeroVector(4);
      for (unsigned int i = 0; i < 3; i++)
         mVector(i) = 1.0;

      MatrixType AuxMatrix(number_of_nodes, 4);
      noalias( AuxMatrix ) = ZeroMatrix(number_of_nodes, 4);
      for (unsigned int i = 0; i < number_of_nodes; i++)
         for (unsigned int j = 0; j < 4; j++)
            AuxMatrix(i,j) = rN(i)*mVector(j);



      rLocalLHS = prod(AuxMatrix, rB) * DeltaSr * (1.0-rInitialPorosity)/rVariables.detF0;
      rLocalLHS *= (rIntegrationWeight * ScalingConstant);
      
      return rLocalLHS;


      KRATOS_CATCH("")
   }

   // ******* Tangent to Mass conservation with Respect WaterPressure ***********************************
   // ***************************************************************************************************
   Matrix &AxisymUnsaturatedWaterPressureUtilities::ComputeLinearization_WaterPressure_Displacement(HydroMechanicalVariables &rVariables, Matrix &rLocalLHS, const double &rIntegrationWeight)
   {
      KRATOS_TRY

      const GeometryType &rGeometry = rVariables.GetGeometry();

      const unsigned int number_of_nodes = rGeometry.PointsNumber();
      const unsigned int dimension = rGeometry.WorkingSpaceDimension();

      // 1. Some constants
      double ScalingConstant;
      GetScalingConstant(ScalingConstant, rVariables.GetProperties());

      //2. Gravity & Permeability
      Vector b(dimension);
      noalias(b) = ZeroVector(dimension);
      double WaterDensity = rVariables.GetProperties().GetValue(DENSITY_WATER);
      b(dimension - 1) = -10.0 * WaterDensity;
      Matrix K;
      double initial_porosity = rVariables.GetProperties().GetValue(INITIAL_POROSITY);

      double VolumeChange = this->CalculateVolumeChange(rGeometry, rVariables.GetShapeFunctions(), rVariables.GetDeformationGradient(), rVariables.GetProperties(), rVariables.detF0 );
      double WaterPressureOld = GetWaterPressureOld( rVariables.GetShapeFunctions(), rVariables.GetGeometry(), WaterPressureOld);
      this->GetPermeabilityTensorAndReshape(rVariables.GetProperties(), rVariables.GetDeformationGradient(), K, initial_porosity, dimension, VolumeChange, WaterPressureOld);

      const MatrixType &rDN_DX = rVariables.GetShapeFunctionsDerivatives();
      Vector GradP = ZeroVector(dimension);

      for (unsigned int iN = 0; iN < number_of_nodes; iN++) {
         for (unsigned int dim = 0; dim < dimension; dim++) {
            GradP(dim) += rGeometry[iN].FastGetSolutionStepValue(WATER_PRESSURE) * rDN_DX(iN, dim);
         }
      }

      Vector DarcyFlow(dimension);
      noalias(DarcyFlow) = prod(K, GradP - b);

      rLocalLHS.resize(number_of_nodes, number_of_nodes * dimension);
      noalias(rLocalLHS) = ZeroMatrix(number_of_nodes, number_of_nodes * dimension);

      return rLocalLHS;

      for (unsigned int i = 0; i < number_of_nodes; i++) {
         for (unsigned int iDim = 0; iDim < dimension; iDim++) {
            for (unsigned int qDim = 0; qDim < dimension; qDim++) {
               for (unsigned int q = 0; q < number_of_nodes; q++) {
                  rLocalLHS(i, q * dimension + iDim) += rDN_DX(i, iDim) * DarcyFlow(qDim) * rDN_DX(q, qDim);
               }
            }
         }
      }

      // Try To do it the other way
      for (unsigned int i = 0; i < number_of_nodes; i++) {
         for (unsigned int iDim = 0; iDim < dimension; iDim++) {
            for (unsigned int qDim = 0; qDim < dimension; qDim++) {
               for (unsigned int pDim = 0; pDim < dimension; pDim++) {
                  for (unsigned int a = 0; a < number_of_nodes; a++) {
                     rLocalLHS(i, a * dimension + pDim) -= rDN_DX(i, iDim) * K(iDim, qDim) * GradP(pDim) * rDN_DX(a, qDim);
                  }
               }
            }
         }
      }

      rLocalLHS *= (rIntegrationWeight * ScalingConstant * rVariables.DeltaTime);

      return rLocalLHS;

      KRATOS_CATCH("")
   }


   void AxisymUnsaturatedWaterPressureUtilities::GetVoigtSize(const unsigned int &dimension, unsigned int &voigtsize, unsigned int &principal_dimension)
   {
      voigtsize = 4;
      principal_dimension = 3;
   }

}
