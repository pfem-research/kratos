//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:           LMonforte $
//   Maintained by:       $Maintainer:                 LM $
//   Date:                $Date:                July 2015 $
//
//
#if !defined(KRATOS_AXISYM_WATER_PRESSURE_UTILITIES_HPP_INCLUDED)
#define KRATOS_AXISYM_WATER_PRESSURE_UTILITIES_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_utilities/water_pressure_utilities.hpp"

namespace Kratos
{

class KRATOS_API(SOLID_MECHANICS_APPLICATION) AxisymWaterPressureUtilities
    : public WaterPressureUtilities
{

public:
   typedef Matrix MatrixType;

   typedef Vector VectorType;

   typedef unsigned int IndexType;

   typedef unsigned int SizeType;

   typedef Properties PropertiesType;

   typedef Node<3> NodeType;
   typedef Geometry<NodeType> GeometryType;

   AxisymWaterPressureUtilities();

   virtual ~AxisymWaterPressureUtilities(){};

protected:
   // Get Properties
   virtual void GetVoigtSize(const unsigned int &dimension, unsigned int &voigtsize, unsigned int &principal_dimension) override;

   // COMPUTE RHS
   virtual VectorType &CalculateMassBalance_AddDisplacementPart(HydroMechanicalVariables &rVariables, VectorType &rLocalRHS, const double &rIntegrationWeight) override;

   // COMPUTE LHS

   virtual MatrixType &ComputeWaterPressureKUwP(HydroMechanicalVariables &rVariables, MatrixType &rLocalLHS, const double &rIntegrationWeight) override;

   virtual MatrixType &ComputeSolidSkeletonDeformationMatrix(HydroMechanicalVariables &rVariables, MatrixType &rLocalLHS, const double &rIntegrationWeight) override;

   virtual MatrixType &ComputeDensityChangeTerm(HydroMechanicalVariables &rVariables, MatrixType &rLocalLHS, const double &rIntegrationWeight) override;

}; // end Class AxisymWaterPressureUtilities

} // namespace Kratos

#endif // KRATOS_AXISYM_WATER_PRESSURE_UTILITIE_HPP_INCLUDED
