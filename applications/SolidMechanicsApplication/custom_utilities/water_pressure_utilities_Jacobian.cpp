//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:           LMonforte $
//   Maintained by:       $Maintainer:                 LM $
//   Date:                $Date:                July 2015 $
//

#include "custom_utilities/water_pressure_utilities_Jacobian.hpp"
#include "solid_mechanics_application_variables.h"

namespace Kratos
{

WaterPressureJacobianUtilities::WaterPressureJacobianUtilities()
{
}

// *************** RHS of the Hydromechanical Problem. Add the solid skeleton movement part *****************************
// **********************************************************************************************************************
Vector &WaterPressureJacobianUtilities::CalculateMassBalance_AddDisplacementPart(HydroMechanicalVariables &rVariables, VectorType &rRightHandSideVector, const double &rIntegrationWeight)
{
   KRATOS_TRY

   double ScalingConstant;
   GetScalingConstant(ScalingConstant, rVariables.GetProperties());

   // 2. Geometric
   const GeometryType &rGeometry = rVariables.GetGeometry();
   const VectorType &rN = rVariables.GetShapeFunctions();
   const unsigned int number_of_nodes = rGeometry.PointsNumber();
      
   double BetaMixed(1.0);
   if ( rVariables.GetProperties().Has(BETA_MIXED) ) {
      BetaMixed = rVariables.GetProperties()[BETA_MIXED];
   }

   double Jacobian_GP(0.0), JacobianNew(0.0), JacobianOld(0.0);
   for (unsigned int i = 0; i < number_of_nodes; i++) {
      Jacobian_GP += rN(i) * rGeometry[i].FastGetSolutionStepValue(JACOBIAN);
      JacobianNew += rN(i) * rGeometry[i].FastGetSolutionStepValue(JACOBIAN);
      JacobianOld += rN(i) * rGeometry[i].FastGetSolutionStepValue(JACOBIAN, 1);
   }

   JacobianNew = pow(JacobianNew, BetaMixed) * pow( rVariables.detF0, 1.0-BetaMixed);
   JacobianOld = pow(JacobianOld, BetaMixed) * pow( rVariables.detF0/rVariables.detF, 1.0-BetaMixed);

   double DeltaJacobian = JacobianNew-JacobianOld;

   for (unsigned int i = 0; i < number_of_nodes; i++) {
      rRightHandSideVector(i) -= rN(i) * (DeltaJacobian / Jacobian_GP) * rIntegrationWeight * ScalingConstant / rVariables.detF0;
   }

   return rRightHandSideVector;

   KRATOS_CATCH("")
}

// ****** Tanget To Mass conservation, part of the solid skeleton deformation for displ form *********
// ***************************************************************************************************
Matrix &WaterPressureJacobianUtilities::ComputeSolidSkeletonDeformationMatrix(HydroMechanicalVariables &rVariables, MatrixType &rLocalLHS, const double &rIntegrationWeight)
{
   KRATOS_TRY

   const GeometryType &rGeometry = rVariables.GetGeometry();
   const unsigned int number_of_nodes = rGeometry.PointsNumber();

   // 1. Some constants
   double ScalingConstant;
   GetScalingConstant(ScalingConstant, rVariables.GetProperties());

   rLocalLHS.resize(number_of_nodes, number_of_nodes);
   noalias(rLocalLHS) = ZeroMatrix(number_of_nodes, number_of_nodes);

   const VectorType &rN = rVariables.GetShapeFunctions();

   double Jacobian_GP = 0;
   for (unsigned int i = 0; i < number_of_nodes; i++)
      Jacobian_GP += rN(i) * rGeometry[i].FastGetSolutionStepValue(JACOBIAN);

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      for (unsigned int j = 0; j < number_of_nodes; j++)
      {
         rLocalLHS(i, j) += rN(i) * rN(j) / Jacobian_GP;
      }
   }

   // Some LD Term (?).
   double DeltaJacobian_GP = 0;

   for (unsigned int j = 0; j < number_of_nodes; j++)
   {
      // Delta Jacobian
      const double &rCurrentJacobian = rGeometry[j].FastGetSolutionStepValue(JACOBIAN);
      const double &rPreviousJacobian = rGeometry[j].FastGetSolutionStepValue(JACOBIAN, 1);
      double DeltaJacobian = rCurrentJacobian - rPreviousJacobian;
      DeltaJacobian_GP += rN(j) * DeltaJacobian;
   }

   for (unsigned int i = 0; i < number_of_nodes; i++)
   {
      for (unsigned int j = 0; j < number_of_nodes; j++)
      {
         rLocalLHS(i, j) -= rN(i) * (DeltaJacobian_GP / pow(Jacobian_GP, 2.0)) * rN(j);
      }
   }

   rLocalLHS *= (rIntegrationWeight * ScalingConstant);

   return rLocalLHS;

   KRATOS_CATCH("")
}

// ***************** RESHAPE MATRIX: matrix different in Jacobian Elements ******************************************************
// ******************************************************************************************************************************
Matrix &WaterPressureJacobianUtilities::AddReshapeSolidSkeletonDeformationMatrix(Matrix &rLeftHandSide, const Matrix &rLocalLHS, const unsigned int dimension, const unsigned int number_of_variables, const unsigned int number_of_nodes)
{
   KRATOS_TRY

   rLeftHandSide = AddReshapeKwPMixed(rLeftHandSide, rLocalLHS, dimension, number_of_variables, number_of_nodes);

   return rLeftHandSide;

   KRATOS_CATCH("")
}

double WaterPressureJacobianUtilities::CalculateVolumeChange(const GeometryType &rGeometry, const Vector &rN, const Matrix &rTotalF, const Properties & rProperties, const double & rDetF0)
{
   KRATOS_TRY

   const unsigned int number_of_nodes = rGeometry.PointsNumber();

   double VolumeChange = 0;
   for (unsigned int i = 0; i < number_of_nodes; i++)
      VolumeChange += rN(i) * rGeometry[i].FastGetSolutionStepValue(JACOBIAN);

   return VolumeChange;

   KRATOS_CATCH("")
}
} // namespace Kratos
