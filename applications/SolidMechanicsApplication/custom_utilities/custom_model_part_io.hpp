//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:         JMCarbonell $
//   Maintained by:       $Maintainer:                    $
//   Date:                $Date:               March 2020 $
//
//

#if !defined(KRATOS_CUSTOM_MODEL_PART_IO_HPP_INCLUDED)
#define KRATOS_CUSTOM_MODEL_PART_IO_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "includes/model_part_io.h"

namespace Kratos
{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// An IO class for reading and writing a modelpart
/** This class reads and writes all modelpart data including the meshes.
*/
class KRATOS_API(SOLID_MECHANICS_APPLICATION) CustomModelPartIO 
    : public ModelPartIO
{
public:
    ///@name Type Definitions
    ///@{

    /// Pointer definition of CustomModelPartIO
    KRATOS_CLASS_POINTER_DEFINITION(CustomModelPartIO);

    typedef IO BaseType;
    typedef ModelPartIO DerivedType;

    typedef DerivedType::NodeType NodeType;
    typedef DerivedType::MeshType MeshType;
    typedef DerivedType::NodesContainerType NodesContainerType;
    typedef DerivedType::PropertiesContainerType PropertiesContainerType;
    typedef DerivedType::ElementsContainerType ElementsContainerType;
    typedef DerivedType::ConditionsContainerType ConditionsContainerType;
    typedef DerivedType::ConnectivitiesContainerType ConnectivitiesContainerType;

    typedef std::vector<std::ostream *> OutputFilesContainerType;
    typedef std::size_t SizeType;

    // Prevents this class from hidding IO::WriteProperties(Properties)
    using BaseType::WriteProperties;

    ///@}
    ///@name Life Cycle
    ///@{

    /// Constructor with filenames.
    CustomModelPartIO(
        std::string const &Filename,
        const Flags Options = IO::READ | IO::IGNORE_VARIABLES_ERROR.AsFalse() | IO::SKIP_TIMER);

    /// Constructor with stream.
    CustomModelPartIO(
        Kratos::shared_ptr<std::iostream> Stream,
        const Flags Options = IO::IGNORE_VARIABLES_ERROR.AsFalse() | IO::SKIP_TIMER);

    /// Destructor.
    ~CustomModelPartIO() override;

    ///@}
    ///@name Operators
    ///@{

    ///@}
    ///@name Operations
    ///@{

    ///@}
    ///@name Access
    ///@{

    ///@}
    ///@name Inquiry
    ///@{

    ///@}
    ///@name Input and output
    ///@{

    /// Turn back information as a string.
    std::string Info() const override
    {
        return "CustomModelPartIO";
    }

    /// Print information about this object.
    void PrintInfo(std::ostream &rOStream) const override
    {
        rOStream << "CustomModelPartIO";
    }

    /// Print object's data.
    void PrintData(std::ostream &rOStream) const override
    {
    }

    ///@}
    ///@name Friends
    ///@{

    ///@}

protected:
    ///@name Protected static Member Variables
    ///@{

    ///@}
    ///@name Protected member Variables
    ///@{

    ///@}
    ///@name Protected Operators
    ///@{

    ///@}
    ///@name Protected Operations
    ///@{

    void ReadElementsBlock(ModelPart &rModelPart) override;

    void ReadElementsBlock(NodesContainerType &rThisNodes, PropertiesContainerType &rThisProperties, ElementsContainerType &rThisElements) override;

    void ReadConditionsBlock(NodesContainerType &rThisNodes, PropertiesContainerType &rThisProperties, ConditionsContainerType &rThisConditions) override;


    ///@}
    ///@name Protected  Access
    ///@{

    ///@}
    ///@name Protected Inquiry
    ///@{

    ///@}
    ///@name Protected LifeCycle
    ///@{

    ///@}

private:
    ///@name Static Member Variables
    ///@{

    ///@}
    ///@name Member Variables
    ///@{

    ///@}
    ///@name Private Operators
    ///@{

    ///@}
    ///@name Private Operations
    ///@{

    Properties::Pointer ReadPropertiesId(PropertiesContainerType& rThisProperties);

    ///@}
    ///@name Private  Access
    ///@{

    ///@}
    ///@name Private Inquiry
    ///@{

    ///@}
    ///@name Un accessible methods
    ///@{

    /// Assignment operator.
    CustomModelPartIO &operator=(CustomModelPartIO const &rOther);

    /// Copy constructor.
    CustomModelPartIO(CustomModelPartIO const &rOther);

    ///@}

}; // Class CustomModelPartIO

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

//   /// input stream function
//   inline std::istream& operator >> (std::istream& rIStream,
//                  CustomModelPartIO& rThis);

//   /// output stream function
//   inline std::ostream& operator << (std::ostream& rOStream,
//                  const CustomModelPartIO& rThis)
//     {
//       rThis.PrintInfo(rOStream);
//       rOStream << std::endl;
//       rThis.PrintData(rOStream);

//       return rOStream;
//     }
//   ///@}

} // namespace Kratos.

#endif // KRATOS_CUSTOM_MODEL_PART_IO_HPP_INCLUDED  defined
