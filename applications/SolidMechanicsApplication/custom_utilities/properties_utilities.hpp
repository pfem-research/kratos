//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:         JMCarbonell $
//   Maintained by:       $Maintainer:                    $
//   Date:                $Date:              August 2020 $
//
//

#if !defined(KRATOS_PROPERTIES_UTILITIES_HPP_INCLUDED)
#define KRATOS_PROPERTIES_UTILITIES_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "includes/model_part.h"

namespace Kratos
{

class PropertiesUtilities
{
public:
  ///@name Type Definitions
  ///@{

  //definition of the size type
  typedef std::size_t  IndexType;

  ///definition of properties type
  typedef Properties PropertiesType;

  ///definition of properties container type
  typedef PointerVectorSet<PropertiesType, IndexedObject> PropertiesContainerType;

  ///@}

  /**
   * @brief Clone properties object
   * @param pPropertiesOrigin
   * @param pPropertiesDestination
   */
  static inline void AssignPropertiesToElements(ModelPart &rModelPart, PropertiesType::Pointer pProperties, const std::vector<Flags> &rTransferFlags)
  {
    // range based loops
    const int nelements = rModelPart.Elements().size();

    if (nelements != 0)
      {
        ModelPart::ElementsContainerType::iterator it_begin = rModelPart.ElementsBegin();
        const unsigned int size = rTransferFlags.size();
#pragma omp parallel for
        for (int i = 0; i < nelements; i++)
          {
            ModelPart::ElementsContainerType::iterator i_elem = it_begin + i;
            for (auto &i_node : i_elem->GetGeometry())
              {
                unsigned int match = 0;
                for (auto &i_flag : rTransferFlags)
                  {
                    if (i_node.Is(i_flag))
                      ++match;
                  }
                if (match==size)
                  {
                    i_elem->SetProperties(pProperties);
                    break;
                  }
              }
          }
      }
  }

  static inline void CloneProperties(PropertiesType::Pointer pPropertiesOrigin,PropertiesType::Pointer pPropertiesDestination)
  {
    IndexType Id = pPropertiesDestination->GetId();
    *pPropertiesDestination = *pPropertiesOrigin;
    pPropertiesDestination->SetId(Id);
  }

};

} // namespace Kratos.

#endif // KRATOS_PROPERTIES_UTILITIES_HPP_INCLUDED
