//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:           LMonforte $
//   Maintained by:       $Maintainer:                 LM $
//   Date:                $Date:            December 2021 $
//

#include "custom_utilities/unsaturated_water_pressure_utilities_Jacobian.hpp"
#include "solid_mechanics_application_variables.h"

namespace Kratos
{

   UnsaturatedWaterPressureJacobianUtilities::UnsaturatedWaterPressureJacobianUtilities(): UnsaturatedWaterPressureUtilities()
   {
   }
   // ******************************  CALCULATE AND ADD LHS MATRIX *************************************
   // ***************************************************************************************************
   Matrix &UnsaturatedWaterPressureJacobianUtilities::CalculateAndAddHydromechanicalLHS(HydroMechanicalVariables &rVariables, Matrix &rLeftHandSide, const Matrix &rBaseClassLHS, const double &rIntegrationWeight)
   {
      KRATOS_TRY

      const unsigned int number_of_nodes = rVariables.GetGeometry().PointsNumber();
      const unsigned int dimension = rVariables.GetGeometry().WorkingSpaceDimension();

      // 1. Reshape BaseClass LHS
      rLeftHandSide = AddReshapeBaseClassLHS(rLeftHandSide, rBaseClassLHS, dimension, rVariables.number_of_variables, number_of_nodes);

      // 2. Add water Pressure geometric stiffness matrix
      Matrix LocalLHS;

      LocalLHS = this->ComputeWaterPressureKuug(rVariables, LocalLHS, rIntegrationWeight);
      rLeftHandSide = AddReshapeKUU(rLeftHandSide, LocalLHS, dimension, rVariables.number_of_variables, number_of_nodes);

      // 3. Add KUwP
      LocalLHS = this->ComputeWaterPressureKUwP(rVariables, LocalLHS, rIntegrationWeight);
      rLeftHandSide = AddReshapeKUwP(rLeftHandSide, LocalLHS, dimension, rVariables.number_of_variables, number_of_nodes);

      // 4. Mass Balance equation. Term by term
      double IntegrationWeight = (rIntegrationWeight / rVariables.detF0);

      LocalLHS = ComputeLinearization_WaterPressure_WaterPressure( rVariables, LocalLHS, IntegrationWeight);
      rLeftHandSide = AddReshapeKwPwP(rLeftHandSide, LocalLHS, dimension, rVariables.number_of_variables, number_of_nodes);

      LocalLHS = this->ComputeLinearization_WaterPressure_Displacement( rVariables, LocalLHS, IntegrationWeight);
      rLeftHandSide = AddReshapeKwPU(rLeftHandSide, LocalLHS, dimension, rVariables.number_of_variables, number_of_nodes);

      
      LocalLHS = ComputeLinearization_DegreeOfSaturation_WaterPressure(rVariables, LocalLHS, IntegrationWeight);
      rLeftHandSide = AddReshapeKwPwP(rLeftHandSide, LocalLHS, dimension, rVariables.number_of_variables, number_of_nodes);

      LocalLHS = this->ComputeLinearization_DegreeOfSaturation_Displacement(rVariables, LocalLHS, IntegrationWeight);
      rLeftHandSide = AddReshapeKwPU( rLeftHandSide, LocalLHS, dimension, rVariables.number_of_variables, number_of_nodes);

      LocalLHS = this->ComputeLinearization_DegreeOfSaturation_Jacobian(rVariables, LocalLHS, IntegrationWeight);
      rLeftHandSide = this->AddReshapeSolidSkeletonDeformationMatrix( rLeftHandSide, LocalLHS, dimension, rVariables.number_of_variables, number_of_nodes);
     
      LocalLHS = this->ComputeLinearization_Displacement_WaterPressure(rVariables, LocalLHS, IntegrationWeight);
      rLeftHandSide = AddReshapeKwPwP(rLeftHandSide, LocalLHS, dimension, rVariables.number_of_variables, number_of_nodes);
      
      LocalLHS = this->ComputeLinearization_Displacement_Displacement(rVariables, LocalLHS, IntegrationWeight);
      rLeftHandSide = AddReshapeKwPU( rLeftHandSide, LocalLHS, dimension, rVariables.number_of_variables, number_of_nodes);
      
      LocalLHS = this->ComputeLinearization_Displacement_Jacobian(rVariables, LocalLHS, IntegrationWeight);
      rLeftHandSide = this->AddReshapeSolidSkeletonDeformationMatrix(rLeftHandSide, LocalLHS, dimension, rVariables.number_of_variables, number_of_nodes);

      return rLeftHandSide;

      KRATOS_CATCH("")
   }


   // *************** RHS of the Hydromechanical Problem. Add the solid skeleton movement part *****************************
   // **********************************************************************************************************************
   Vector &UnsaturatedWaterPressureJacobianUtilities::CalculateMassBalance_AddDisplacementPart(HydroMechanicalVariables &rVariables, VectorType &rRightHandSideVector, const double &rIntegrationWeight)
   {
      KRATOS_TRY

      double ScalingConstant;
      GetScalingConstant(ScalingConstant, rVariables.GetProperties());

      // 2. Geometric
      const GeometryType &rGeometry = rVariables.GetGeometry();
      const unsigned int number_of_nodes = rGeometry.PointsNumber();

      const VectorType &rN = rVariables.GetShapeFunctions();

      double WaterPressure(0.0), Jacobian_GP(0.0), DeltaJacobian(0.0);
      double JacobianNew(0.0), JacobianOld(0.0);
      for (unsigned int i = 0; i < number_of_nodes; i++) {
         WaterPressure += rGeometry[i].GetSolutionStepValue(WATER_PRESSURE) * rN(i);
         Jacobian_GP += rGeometry[i].FastGetSolutionStepValue(JACOBIAN) * rN(i);
         JacobianNew += rGeometry[i].FastGetSolutionStepValue(JACOBIAN) * rN(i);
         JacobianOld += rGeometry[i].FastGetSolutionStepValue(JACOBIAN,1) * rN(i);
      }

      
      double BetaMixed(1.0);
      if ( rVariables.GetProperties().Has(BETA_MIXED) ) {
         BetaMixed = rVariables.GetProperties()[BETA_MIXED];
      }
      
      JacobianNew = pow( JacobianNew, BetaMixed) * pow(rVariables.detF0, 1.0-BetaMixed);
      JacobianOld = pow( JacobianOld, BetaMixed)*pow(rVariables.detF0/rVariables.detF, 1.0-BetaMixed);
      Jacobian_GP = JacobianNew;
      DeltaJacobian = JacobianNew-JacobianOld;

      double DegreeOfSaturation = EvaluateRetentionCurve(rVariables.GetProperties(), WaterPressure, DegreeOfSaturation);

      for (unsigned int i = 0; i < number_of_nodes; i++) {
            // Solid Skeleton volumetric deformation
            rRightHandSideVector(i) -= rN(i) * DegreeOfSaturation * (DeltaJacobian/Jacobian_GP) * rIntegrationWeight * ScalingConstant / rVariables.detF0;
      }

      return rRightHandSideVector;

      KRATOS_CATCH("")
   }

   // ****** Tanget To Mass conservation, part of the solid skeleton deformation for displ form *********
   // ***************************************************************************************************
   Matrix &UnsaturatedWaterPressureJacobianUtilities::ComputeLinearization_Displacement_Displacement(HydroMechanicalVariables &rVariables, MatrixType &rLocalLHS, const double &rIntegrationWeight)
   {
      KRATOS_TRY

      const GeometryType &rGeometry = rVariables.GetGeometry();
      const unsigned int number_of_nodes = rGeometry.PointsNumber();
      const unsigned int dimension = rGeometry.WorkingSpaceDimension();

      // 1. Some constants
      double ScalingConstant;
      GetScalingConstant(ScalingConstant, rVariables.GetProperties());

      double BetaMixed(1.0);
      if ( rVariables.GetProperties().Has(BETA_MIXED) ) {
         BetaMixed = rVariables.GetProperties()[BETA_MIXED];
      }

      // 2. Calculate Matrix
      rLocalLHS.resize(number_of_nodes, number_of_nodes * dimension);
      noalias(rLocalLHS) = ZeroMatrix(number_of_nodes, number_of_nodes * dimension);

      const MatrixType &rDN_DX = rVariables.GetShapeFunctionsDerivatives();
      const VectorType &rN = rVariables.GetShapeFunctions();

      double WaterPressure(0.0);
      for (unsigned int i = 0; i < number_of_nodes; i++)
         WaterPressure += rN(i) * rGeometry[i].FastGetSolutionStepValue(WATER_PRESSURE);
      double DegreeOfSaturation = EvaluateRetentionCurve(rVariables.GetProperties(), WaterPressure, DegreeOfSaturation);


      for (unsigned int i = 0; i < number_of_nodes; i++) {
         for (unsigned int p = 0; p < number_of_nodes; p++) {
            for (unsigned int pDim = 0; pDim < dimension; pDim++) { 
               rLocalLHS(i, p * dimension + pDim) += rN(i) * rDN_DX(p, pDim);
            }
         }
      }

      double VolumeChange = this->CalculateVolumeChange(rGeometry, rVariables.GetShapeFunctions(), rVariables.GetDeformationGradient(), rVariables.GetProperties(), rVariables.detF0 );

      double JacobianOld(0);
      for (unsigned int j = 0; j < number_of_nodes; j++) 
         JacobianOld += rGeometry[j].FastGetSolutionStepValue(JACOBIAN,1) * rN(j);

      double VolumeOld = pow( JacobianOld, BetaMixed)*pow(rVariables.detF0/rVariables.detF, 1.0-BetaMixed);

      rLocalLHS *= (rIntegrationWeight * ScalingConstant) * DegreeOfSaturation * ( 1- BetaMixed) * VolumeOld/VolumeChange;

      return rLocalLHS;

      KRATOS_CATCH("")
   }
   
   // ****** Tanget To Mass conservation, part of the solid skeleton deformation for displ form *********
   // ***************************************************************************************************
   Matrix &UnsaturatedWaterPressureJacobianUtilities::ComputeLinearization_Displacement_Jacobian(HydroMechanicalVariables &rVariables, MatrixType &rLocalLHS, const double &rIntegrationWeight)
   {
      KRATOS_TRY

      const GeometryType &rGeometry = rVariables.GetGeometry();
      const unsigned int number_of_nodes = rGeometry.PointsNumber();

      // 1. Some constants
      double ScalingConstant;
      GetScalingConstant(ScalingConstant, rVariables.GetProperties());
      
      double BetaMixed(1.0);
      if ( rVariables.GetProperties().Has(BETA_MIXED) ) {
         BetaMixed = rVariables.GetProperties()[BETA_MIXED];
      }

      rLocalLHS.resize(number_of_nodes, number_of_nodes);
      noalias(rLocalLHS) = ZeroMatrix(number_of_nodes, number_of_nodes);

      const VectorType &rN = rVariables.GetShapeFunctions();

      double WaterPressure(0.0), Jacobian_GP(0.0), JacobianOld(0.0);
      for (unsigned int i = 0; i < number_of_nodes; i++) {
         WaterPressure += rGeometry[i].GetSolutionStepValue(WATER_PRESSURE) * rN(i);
         Jacobian_GP += rGeometry[i].FastGetSolutionStepValue(JACOBIAN) * rN(i);
         JacobianOld += rGeometry[i].FastGetSolutionStepValue(JACOBIAN,1) * rN(i);
      }

      double DegreeOfSaturation = EvaluateRetentionCurve(rVariables.GetProperties(), WaterPressure, DegreeOfSaturation);
      double VolumeChange = this->CalculateVolumeChange(rGeometry, rVariables.GetShapeFunctions(), rVariables.GetDeformationGradient(), rVariables.GetProperties(), rVariables.detF0 );
      double VolumeOld = pow( JacobianOld, BetaMixed)*pow(rVariables.detF0/rVariables.detF, 1.0-BetaMixed);

      for (unsigned int i = 0; i < number_of_nodes; i++) {
         for (unsigned int j = 0; j < number_of_nodes; j++) {
            rLocalLHS(i, j) += rN(i) * DegreeOfSaturation * (VolumeOld / VolumeChange) * BetaMixed / Jacobian_GP * rN(j);
         }
      }

      rLocalLHS *= (rIntegrationWeight * ScalingConstant);

      return rLocalLHS;

      KRATOS_CATCH("")
   }

   Matrix &UnsaturatedWaterPressureJacobianUtilities::ComputeLinearization_Displacement_WaterPressure(HydroMechanicalVariables &rVariables, Matrix &rLocalLHS, const double &rIntegrationWeight)
   {

      KRATOS_TRY
      const unsigned int number_of_nodes = rVariables.GetGeometry().PointsNumber();
      const VectorType &rN = rVariables.GetShapeFunctions();

      const GeometryType &rGeometry = rVariables.GetGeometry();

      // 1. Some constants
      double ScalingConstant;
      GetScalingConstant(ScalingConstant, rVariables.GetProperties());

      Matrix K;
      double initial_porosity = rVariables.GetProperties().GetValue(INITIAL_POROSITY);
      double VolumeChange = this->CalculateVolumeChange(rVariables.GetGeometry(), rVariables.GetShapeFunctions(), rVariables.GetDeformationGradient(), rVariables.GetProperties() , rVariables.detF0) ;
      double WP_OLD = GetWaterPressureOld( rN, rGeometry, WP_OLD);
      this->GetPermeabilityTensor(rVariables.GetProperties(), rVariables.GetDeformationGradient(), K, initial_porosity, VolumeChange, WP_OLD);

      rLocalLHS.resize(number_of_nodes, number_of_nodes);
      noalias(rLocalLHS) = ZeroMatrix(number_of_nodes, number_of_nodes);

      double WaterPressure(0.0), Jacobian_GP(0.0), DeltaJacobian(0.0);
      double JacobianNew(0.0), JacobianOld(0.0);
      for (unsigned int i = 0; i < number_of_nodes; i++) {
         WaterPressure += rGeometry[i].GetSolutionStepValue(WATER_PRESSURE) * rN(i);
         Jacobian_GP += rGeometry[i].FastGetSolutionStepValue(JACOBIAN) * rN(i);
         JacobianNew += rGeometry[i].FastGetSolutionStepValue(JACOBIAN) * rN(i);
         JacobianOld += rGeometry[i].FastGetSolutionStepValue(JACOBIAN,1) * rN(i);
      }

      double BetaMixed(1.0);
      if ( rVariables.GetProperties().Has(BETA_MIXED) ) {
         BetaMixed = rVariables.GetProperties()[BETA_MIXED];
      }

      JacobianNew = pow( JacobianNew, BetaMixed) * pow(rVariables.detF0, 1.0-BetaMixed);
      JacobianOld = pow( JacobianOld, BetaMixed)*pow(rVariables.detF0/rVariables.detF, 1.0-BetaMixed);
      Jacobian_GP = JacobianNew;
      DeltaJacobian = JacobianNew-JacobianOld;

      double dSr_dWaterPressure = DerivativeRetentionCurve( rVariables.GetProperties(), WaterPressure, dSr_dWaterPressure);

      for (unsigned int i = 0; i < number_of_nodes; i++) {
         for (unsigned int j = 0; j < number_of_nodes; j++) {
            // Delta Jacobian
            rLocalLHS(i, j) += rN(i) *  (DeltaJacobian/Jacobian_GP) * dSr_dWaterPressure * rN(j);
         }
      }
      rLocalLHS *= (rIntegrationWeight * ScalingConstant);

      return rLocalLHS;

      KRATOS_CATCH("")
   }

   
   Matrix &UnsaturatedWaterPressureJacobianUtilities::ComputeLinearization_DegreeOfSaturation_Displacement(HydroMechanicalVariables &rVariables, MatrixType &rLocalLHS, const double &rIntegrationWeight)
   {
      KRATOS_TRY

      const GeometryType &rGeometry = rVariables.GetGeometry();
      const unsigned int number_of_nodes = rGeometry.PointsNumber();
      const unsigned int dimension = rGeometry.WorkingSpaceDimension();

      // 1. Some constants
      double ScalingConstant;
      GetScalingConstant(ScalingConstant, rVariables.GetProperties());

      rLocalLHS.resize(number_of_nodes, number_of_nodes * dimension);
      noalias(rLocalLHS) = ZeroMatrix(number_of_nodes, number_of_nodes * dimension);

      const VectorType &rN = rVariables.GetShapeFunctions();
      const MatrixType &rDN_DX = rVariables.GetShapeFunctionsDerivatives();

      double WaterPressure(0.0), WaterPressureOld(0.0), Jacobian_GP(0.0);
      for (unsigned int i = 0; i < number_of_nodes; i++) {
         WaterPressure += rGeometry[i].GetSolutionStepValue(WATER_PRESSURE) * rN(i);
         WaterPressureOld += rGeometry[i].GetSolutionStepValue(WATER_PRESSURE,1) * rN(i);
         Jacobian_GP += rGeometry[i].GetSolutionStepValue(JACOBIAN) * rN(i);
      }
      
      double VolumeChange = this->CalculateVolumeChange(rVariables.GetGeometry(), rVariables.GetShapeFunctions(), rVariables.GetDeformationGradient(), rVariables.GetProperties() , rVariables.detF0);

      double BetaMixed(1.0);
      if ( rVariables.GetProperties().Has(BETA_MIXED) ) {
         BetaMixed = rVariables.GetProperties()[BETA_MIXED];
      }

      double DegreeOfSaturation = EvaluateRetentionCurve(rVariables.GetProperties(), WaterPressure, DegreeOfSaturation);
      double DegreeOfSaturationOld = EvaluateRetentionCurve(rVariables.GetProperties(), WaterPressureOld, DegreeOfSaturationOld);

      double DeltaSr = DegreeOfSaturation-DegreeOfSaturationOld;
      
      const double & rInitialPorosity = rVariables.GetProperties().GetValue(INITIAL_POROSITY);

      for (unsigned int i = 0; i < number_of_nodes; i++) {
         for (unsigned int qDim = 0; qDim < dimension; qDim++) {
            for (unsigned int q = 0; q < number_of_nodes; q++) {
               rLocalLHS(i, q * dimension + qDim) += rN(i) * DeltaSr * (1.0-rInitialPorosity)/VolumeChange * (1.0-BetaMixed) * rDN_DX(q, qDim);
            }
         }
      }

      rLocalLHS *= (rIntegrationWeight * ScalingConstant);

      return rLocalLHS;

      KRATOS_CATCH("")
   }

   Matrix &UnsaturatedWaterPressureJacobianUtilities::ComputeLinearization_DegreeOfSaturation_Jacobian(HydroMechanicalVariables &rVariables, MatrixType &rLocalLHS, const double &rIntegrationWeight)
   {
      KRATOS_TRY

      const GeometryType &rGeometry = rVariables.GetGeometry();
      const unsigned int number_of_nodes = rGeometry.PointsNumber();

      // 1. Some constants
      double ScalingConstant;
      GetScalingConstant(ScalingConstant, rVariables.GetProperties());

      rLocalLHS.resize(number_of_nodes, number_of_nodes);
      noalias(rLocalLHS) = ZeroMatrix(number_of_nodes, number_of_nodes);

      const VectorType &rN = rVariables.GetShapeFunctions();

      double WaterPressure(0.0), WaterPressureOld(0.0), Jacobian_GP(0.0);
      for (unsigned int i = 0; i < number_of_nodes; i++) {
         WaterPressure += rGeometry[i].GetSolutionStepValue(WATER_PRESSURE) * rN(i);
         WaterPressureOld += rGeometry[i].GetSolutionStepValue(WATER_PRESSURE,1) * rN(i);
         Jacobian_GP += rGeometry[i].GetSolutionStepValue(JACOBIAN) * rN(i);
      }
      
      double VolumeChange = this->CalculateVolumeChange(rVariables.GetGeometry(), rVariables.GetShapeFunctions(), rVariables.GetDeformationGradient(), rVariables.GetProperties() , rVariables.detF0);

      double BetaMixed(1.0);
      if ( rVariables.GetProperties().Has(BETA_MIXED) ) {
         BetaMixed = rVariables.GetProperties()[BETA_MIXED];
      }

      double DegreeOfSaturation = EvaluateRetentionCurve(rVariables.GetProperties(), WaterPressure, DegreeOfSaturation);
      double DegreeOfSaturationOld = EvaluateRetentionCurve(rVariables.GetProperties(), WaterPressureOld, DegreeOfSaturationOld);

      
      const double & rInitialPorosity = rVariables.GetProperties().GetValue(INITIAL_POROSITY);

      for (unsigned int i = 0; i < number_of_nodes; i++) {
         for (unsigned int j = 0; j < number_of_nodes; j++) {
            rLocalLHS(i, j) += rN(i) *(DegreeOfSaturation-DegreeOfSaturationOld) * ( 1.0 - rInitialPorosity ) / VolumeChange * BetaMixed/Jacobian_GP * rN(j);
         }
      }

      rLocalLHS *= (rIntegrationWeight * ScalingConstant);

      return rLocalLHS;

      KRATOS_CATCH("")
   }

   // ***************** RESHAPE MATRIX: matrix different in Jacobian Elements ******************************************************
   // ******************************************************************************************************************************
   Matrix &UnsaturatedWaterPressureJacobianUtilities::AddReshapeSolidSkeletonDeformationMatrix(Matrix &rLeftHandSide, const Matrix &rLocalLHS, const unsigned int dimension, const unsigned int number_of_variables, const unsigned int number_of_nodes)
   {
      KRATOS_TRY

      rLeftHandSide = AddReshapeKwPMixed(rLeftHandSide, rLocalLHS, dimension, number_of_variables, number_of_nodes);

      return rLeftHandSide;

      KRATOS_CATCH("")
   }


   double UnsaturatedWaterPressureJacobianUtilities::CalculateVolumeChange(const GeometryType &rGeometry, const Vector &rN, const Matrix &rTotalF, const Properties & rProperties, const double & rDetF0)
   {
      KRATOS_TRY

      const unsigned int number_of_nodes = rGeometry.PointsNumber();

      double VolumeChange = 0;
      for (unsigned int i = 0; i < number_of_nodes; i++)
         VolumeChange += rN(i) * rGeometry[i].FastGetSolutionStepValue(JACOBIAN);

      if ( rProperties.Has(BETA_MIXED) ) {
         const double & rBetaMixed = rProperties[BETA_MIXED];
         double VolumeChange2 = MathUtils<double>::Det(rTotalF);
         VolumeChange = pow(VolumeChange, rBetaMixed)*pow(VolumeChange2, 1.0-rBetaMixed);
      }

      return VolumeChange;

      KRATOS_CATCH("")
   }



} // namespace Kratos
// end namespace kratos
