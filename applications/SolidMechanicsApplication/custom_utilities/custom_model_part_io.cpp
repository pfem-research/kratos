//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:         JMCarbonell $
//   Maintained by:       $Maintainer:                    $
//   Date:                $Date:               March 2020 $
//
//

// System includes

// External includes

// Project includes
#include "custom_utilities/custom_model_part_io.hpp"

namespace Kratos
{
/// Constructor with  filenames.
CustomModelPartIO::CustomModelPartIO(std::string const& Filename, const Flags Options)
    : ModelPartIO(Filename,Options)
{

}

/// Constructor with stream
CustomModelPartIO::CustomModelPartIO(Kratos::shared_ptr<std::iostream> Stream, const Flags Options)
    : ModelPartIO(Stream,Options)
{

}

/// Destructor.
CustomModelPartIO::~CustomModelPartIO()
{

}

void CustomModelPartIO::ReadElementsBlock(ModelPart& rModelPart)
{
    KRATOS_TRY

    SizeType id;
    SizeType node_id;
    SizeType number_of_read_elements = 0;


    std::string word;
    std::string element_name;

    this->ReadWord(element_name);
    KRATOS_INFO("CustomModelPartIO") << "  [Reading Elements : ";

    if(!KratosComponents<Element>::Has(element_name))
    {
        std::stringstream buffer;
        buffer << "Element " << element_name << " is not registered in Kratos.";
        buffer << " Please check the spelling of the element name and see if the application which containing it, is registered correctly.";
        buffer << " [Line " << mNumberOfLines << " ]";
        KRATOS_ERROR << buffer.str() << std::endl;;
        return;
    }

    Element const& r_clone_element = KratosComponents<Element>::Get(element_name);
    SizeType number_of_nodes = r_clone_element.GetGeometry().size();
    Element::NodesArrayType temp_element_nodes;
    ModelPart::ElementsContainerType aux_elements;

    while(!mpStream->eof())
    {
        this->ReadWord(word); // Reading the element id or End
        if(this->CheckEndBlock("Elements", word))
            break;

        this->ExtractValue(word,id);

        Properties::Pointer p_temp_properties = ReadPropertiesId(rModelPart.rProperties());
        temp_element_nodes.clear();

        for(SizeType i = 0 ; i < number_of_nodes ; i++)
        {
            this->ReadWord(word); // Reading the node id;
            this->ExtractValue(word, node_id);
            temp_element_nodes.push_back( *(this->FindKey(rModelPart.Nodes(), this->ReorderedNodeId(node_id), "Node").base()));
        }

        aux_elements.push_back(r_clone_element.Create(this->ReorderedElementId(id), temp_element_nodes, p_temp_properties));
        number_of_read_elements++;

    }
    KRATOS_INFO("") << number_of_read_elements << " elements read] [Type: " <<element_name << "]" << std::endl;
    aux_elements.Unique();

    rModelPart.AddElements(aux_elements.begin(), aux_elements.end());

    KRATOS_CATCH("")
}

void CustomModelPartIO::ReadElementsBlock(NodesContainerType& rThisNodes, PropertiesContainerType& rThisProperties, ElementsContainerType& rThisElements)
{
    KRATOS_TRY

    SizeType id;
    SizeType node_id;
    SizeType number_of_read_elements = 0;


    std::string word;
    std::string element_name;

    this->ReadWord(element_name);
    KRATOS_INFO("CustomModelPartIO") << "  [Reading Elements : ";

    if(!KratosComponents<Element>::Has(element_name))
    {
        std::stringstream buffer;
        buffer << "Element " << element_name << " is not registered in Kratos.";
        buffer << " Please check the spelling of the element name and see if the application which containing it, is registered correctly.";
        buffer << " [Line " << mNumberOfLines << " ]";
        KRATOS_ERROR << buffer.str() << std::endl;;
        return;
    }

    Element const& r_clone_element = KratosComponents<Element>::Get(element_name);
    SizeType number_of_nodes = r_clone_element.GetGeometry().size();
    Element::NodesArrayType temp_element_nodes;


    while(!mpStream->eof())
    {
        this->ReadWord(word); // Reading the element id or End
        if(this->CheckEndBlock("Elements", word))
            break;

        this->ExtractValue(word,id);

        Properties::Pointer p_temp_properties = ReadPropertiesId(rThisProperties);
        temp_element_nodes.clear();

        for(SizeType i = 0 ; i < number_of_nodes ; i++)
        {
            this->ReadWord(word); // Reading the node id;
            this->ExtractValue(word, node_id);
            temp_element_nodes.push_back( *(this->FindKey(rThisNodes, this->ReorderedNodeId(node_id), "Node").base()));
        }

        rThisElements.push_back(r_clone_element.Create(this->ReorderedElementId(id), temp_element_nodes, p_temp_properties));
        number_of_read_elements++;

    }
    KRATOS_INFO("") << number_of_read_elements << " elements read] [Type: " <<element_name << "]" << std::endl;
    rThisElements.Unique();

    KRATOS_CATCH("")
}

void CustomModelPartIO::ReadConditionsBlock(NodesContainerType& rThisNodes, PropertiesContainerType& rThisProperties, ConditionsContainerType& rThisConditions)
{
    KRATOS_TRY
    
    SizeType id;
    SizeType node_id;
    SizeType number_of_read_conditions = 0;


    std::string word;
    std::string condition_name;

    this->ReadWord(condition_name);
    KRATOS_INFO("CustomModelPartIO") << "  [Reading Conditions : ";

    if(!KratosComponents<Condition>::Has(condition_name))
    {
        std::stringstream buffer;
        buffer << "Condition " << condition_name << " is not registered in Kratos.";
        buffer << " Please check the spelling of the condition name and see if the application containing it is registered correctly.";
        buffer << " [Line " << mNumberOfLines << " ]";
        KRATOS_ERROR << buffer.str() << std::endl;;
        return;
    }

    Condition const& r_clone_condition = KratosComponents<Condition>::Get(condition_name);
    SizeType number_of_nodes = r_clone_condition.GetGeometry().size();
    Condition::NodesArrayType temp_condition_nodes;

    while(!mpStream->eof())
    {
        this->ReadWord(word); // Reading the condition id or End
        if(this->CheckEndBlock("Conditions", word))
            break;

        this->ExtractValue(word,id);

        Properties::Pointer p_temp_properties = ReadPropertiesId(rThisProperties);
        temp_condition_nodes.clear();

        for(SizeType i = 0 ; i < number_of_nodes ; i++)
        {
            this->ReadWord(word); // Reading the node id;
            this->ExtractValue(word, node_id);
            temp_condition_nodes.push_back( *(this->FindKey(rThisNodes, this->ReorderedNodeId(node_id), "Node").base()));
        }

        rThisConditions.push_back(r_clone_condition.Create(this->ReorderedConditionId(id), temp_condition_nodes, p_temp_properties));
        number_of_read_conditions++;
    }
    KRATOS_INFO("") << number_of_read_conditions << " conditions read] [Type: " << condition_name << "]" << std::endl;
    rThisConditions.Unique();
    
    KRATOS_CATCH("")
}


Properties::Pointer CustomModelPartIO::ReadPropertiesId(PropertiesContainerType& rThisProperties)
{
    KRATOS_TRY

    SizeType properties_id;

    std::string word;

    Properties::Pointer p_temp_properties = Kratos::make_shared<Properties>();

    if(rThisProperties.size() > 0)
    {
        this->ReadWord(word); // Reading the properties id;
        this->ExtractValue(word, properties_id);
        if (rThisProperties.back().Id() >= properties_id)
            p_temp_properties = *(this->FindKey(rThisProperties, properties_id, "Properties").base());
        else
            std::cout << " properties id " << rThisProperties.back().Id() << " properties_id " << properties_id << std::endl;
    }
    // else
    // {
    //     KRATOS_ERROR << " NO PROPERTIES " << rThisProperties.size() << std::endl;
    // }

    return p_temp_properties;

    KRATOS_CATCH("")
}

}  // namespace Kratos.
