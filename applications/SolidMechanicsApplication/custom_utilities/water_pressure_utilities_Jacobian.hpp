//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:           LMonforte $
//   Maintained by:       $Maintainer:                 LM $
//   Date:                $Date:                July 2015 $
//
//
#if !defined(KRATOS_WATER_PRESSURE_JACOBIAN_UTILITIE_HPP_INCLUDED)
#define KRATOS_WATER_PRESSURE_JACOBIAN_UTILITIE_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_utilities/water_pressure_utilities.hpp"

namespace Kratos
{

class KRATOS_API(SOLID_MECHANICS_APPLICATION) WaterPressureJacobianUtilities
    : public WaterPressureUtilities
{

public:
   typedef Matrix MatrixType;

   typedef Vector VectorType;

   typedef unsigned int IndexType;

   typedef unsigned int SizeType;

   typedef Properties PropertiesType;

   typedef Node<3> NodeType;
   typedef Geometry<NodeType> GeometryType;

   WaterPressureJacobianUtilities();

   virtual ~WaterPressureJacobianUtilities(){};

protected:
   virtual VectorType &CalculateMassBalance_AddDisplacementPart(HydroMechanicalVariables &rVariables, VectorType &rLocalLHS, const double &rIntegrationWeight) override;

   virtual double CalculateVolumeChange(const GeometryType &rGeometry, const Vector &rN, const Matrix &rTotalF, const Properties & rProperties, const double & rDetF0) override;

   // CALCULATE LHS

   virtual MatrixType &ComputeSolidSkeletonDeformationMatrix(HydroMechanicalVariables &rVariables, MatrixType &rLocalLHS, const double &rIntegrationWeight) override;

   // RESHAPE LHS

   virtual MatrixType &AddReshapeSolidSkeletonDeformationMatrix(MatrixType &rLeftHandSide, const MatrixType &rBaseClassLHS, const unsigned int dimension, const unsigned int number_of_variables, const unsigned int number_of_nodes) override;

}; // end Class WaterPressureJacobianUtilities

} // namespace Kratos

#endif // KRATOS_WATER_PRESSURE_JACOBIAN_UTILITIE_HPP_INCLUDED
