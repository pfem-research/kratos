//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:           LMonforte $
//   Maintained by:       $Maintainer:                 LM $
//   Date:                $Date:            December 2021 $
//
//
#if !defined(KRATOS_AXISYM_UNSAT_WATER_PRESSURE_UTILITIES_JACOBIAN_HPP_INCLUDED)
#define KRATOS_AXISYM_UNSAT_WATER_PRESSURE_UTILITIES_JACOBIAN_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_utilities/axisym_unsaturated_water_pressure_utilities.hpp"

namespace Kratos
{

class KRATOS_API(SOLID_MECHANICS_APPLICATION) AxisymUnsaturatedWaterPressureJacobianUtilities
    : public AxisymUnsaturatedWaterPressureUtilities
{

public:
   typedef Matrix MatrixType;

   typedef Vector VectorType;

   typedef unsigned int IndexType;

   typedef unsigned int SizeType;

   typedef Properties PropertiesType;

   typedef Node<3> NodeType;
   typedef Geometry<NodeType> GeometryType;

   

public:
   AxisymUnsaturatedWaterPressureJacobianUtilities();

   virtual ~AxisymUnsaturatedWaterPressureJacobianUtilities(){};

   MatrixType &CalculateAndAddHydromechanicalLHS(HydroMechanicalVariables &rVariables, MatrixType &rLeftHandSide, const MatrixType &rBaseClassLHS, const double &rIntegrationWeight) override;



protected:
   // Get Properties


   // CALCULATE RHS

   VectorType &CalculateMassBalance_AddDisplacementPart(HydroMechanicalVariables &rVariables, VectorType &rLocalRHS, const double &rIntegrationWeight) override;
   
   double CalculateVolumeChange(const GeometryType &rGeometry, const Vector &rN, const Matrix &rTotalF, const Properties & rProperties, const double & rDetF0) override;
   
   MatrixType &   ComputeLinearization_DegreeOfSaturation_Displacement(HydroMechanicalVariables & rVariables, MatrixType & rLocalLHS, const double & rIntegrationWeight) override;

   MatrixType &   ComputeLinearization_DegreeOfSaturation_Jacobian(HydroMechanicalVariables & rVariables, MatrixType & rLocalLHS, const double & rIntegrationWeight);
   
   MatrixType &   ComputeLinearization_Displacement_Displacement(HydroMechanicalVariables & rVariables, MatrixType & rLocalLHS, const double & rIntegrationWeight) override;
   
   MatrixType &   ComputeLinearization_Displacement_Jacobian(HydroMechanicalVariables & rVariables, MatrixType & rLocalLHS, const double & rIntegrationWeight);
   
   MatrixType &   ComputeLinearization_Displacement_WaterPressure(HydroMechanicalVariables & rVariables, MatrixType & rLocalLHS, const double & rIntegrationWeight) override;
   
   MatrixType &AddReshapeSolidSkeletonDeformationMatrix(MatrixType &rLeftHandSide, const MatrixType &rBaseClassLHS, const unsigned int dimension, const unsigned int number_of_variables, const unsigned int number_of_nodes) override;

}; // end Class AxisymUnsaturatedWaterPressureJacobianUtilities

} // namespace Kratos

#endif // KRATOS_AXISYM_UNSAT_WATER_PRESSURE_UTILITIES_JACOBIAN_HPP_INCLUDED
