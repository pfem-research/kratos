//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:           LMonforte $
//   Maintained by:       $Maintainer:                 LM $
//   Date:                $Date:            December 2021 $
//
//
#if !defined(KRATOS_UNSAT_WATER_PRESSURE_UTILITIE_HPP_INCLUDED)
#define KRATOS_UNSAT_WATER_PRESSURE_UTILITIE_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_utilities/water_pressure_utilities.hpp"

namespace Kratos
{

class KRATOS_API(SOLID_MECHANICS_APPLICATION) UnsaturatedWaterPressureUtilities
    : public WaterPressureUtilities
{

public:
   typedef Matrix MatrixType;

   typedef Vector VectorType;

   typedef unsigned int IndexType;

   typedef unsigned int SizeType;

   typedef Properties PropertiesType;

   typedef Node<3> NodeType;
   typedef Geometry<NodeType> GeometryType;

   

public:
   UnsaturatedWaterPressureUtilities();

   virtual ~UnsaturatedWaterPressureUtilities(){};

   VectorType &CalculateAndAddHydromechanicalRHS(HydroMechanicalVariables &rVariables, VectorType &rRightHandSide, const VectorType &rBaseClassRHS, const double &rIntegrationWeight) override;


   MatrixType &CalculateAndAddHydromechanicalLHS(HydroMechanicalVariables &rVariables, MatrixType &rLeftHandSide, const MatrixType &rBaseClassLHS, const double &rIntegrationWeight) override;

   double & EvaluateRetentionCurve( const PropertiesType & rProperties, const double & rWaterPressure, double & rDegreeOfSaturation);

   double & EvaluateEffectiveRetentionCurve( const PropertiesType & rProperties, const double & rWaterPressure, double & rDegreeOfSaturation);
   
   void GetPermeabilityTensor(const PropertiesType &rProperties, const Matrix &rTotalF, Matrix &rK, const double &rInitial_porosity, const double &rVolume, const double & rWaterPressure) override;

protected:
   // Get Properties

   double & DerivativeRetentionCurve( const PropertiesType & rProperties, const double & rWaterPressure, double & rDerivative);
   
   double & DerivativeEffectiveRetentionCurve( const PropertiesType & rProperties, const double & rWaterPressure, double & rDerivative);

   // CALCULATE RHS
   VectorType &CalculateMassBalance_WaterPressurePart(HydroMechanicalVariables &rVariables, VectorType &rLocalRHS, const double &rIntegrationWeight);
   
   VectorType &CalculateMassBalance_AddDegreeOfSaturationPart(HydroMechanicalVariables &rVariables, VectorType &rLocalRHS, const double &rIntegrationWeight);

   VectorType &CalculateMassBalance_AddDisplacementPart(HydroMechanicalVariables &rVariables, VectorType &rLocalRHS, const double &rIntegrationWeight) override;

   VectorType &CalculateWaterInternalForcesContribution(HydroMechanicalVariables &rVariables, VectorType &rRightHandSideVector, const double &rIntegrationWeight);

   VectorType &CalculateVolumeForcesContribution(HydroMechanicalVariables &rVariables, VectorType &rRightHandSideVector, const double &rIntegrationWeight);



   // CALCULATE LHS
   MatrixType &ComputeWaterPressureKuug(HydroMechanicalVariables &rVariables, MatrixType &rLocalLHS, const double &rIntegrationWeight);

   MatrixType &ComputeWaterPressureKUwP(HydroMechanicalVariables &rVariables, MatrixType &rLocalLHS, const double &rIntegrationWeight) override;


   MatrixType &ComputeDensityChangeTerm(HydroMechanicalVariables &rVariables, MatrixType &rLocalLHS, const double &rIntegrationWeight) override;


   virtual MatrixType &   ComputeLinearization_Displacement_Displacement(HydroMechanicalVariables & rVariables, MatrixType & rLocalLHS, const double & rIntegrationWeight);
   virtual MatrixType &   ComputeLinearization_Displacement_WaterPressure(HydroMechanicalVariables & rVariables, MatrixType & rLocalLHS, const double & rIntegrationWeight);
   
   virtual MatrixType &   ComputeLinearization_WaterPressure_Displacement(HydroMechanicalVariables & rVariables, MatrixType & rLocalLHS, const double & rIntegrationWeight);
   MatrixType &   ComputeLinearization_WaterPressure_WaterPressure(HydroMechanicalVariables & rVariables, MatrixType & rLocalLHS, const double & rIntegrationWeight);

   virtual MatrixType &   ComputeLinearization_DegreeOfSaturation_Displacement(HydroMechanicalVariables & rVariables, MatrixType & rLocalLHS, const double & rIntegrationWeight);
   MatrixType &   ComputeLinearization_DegreeOfSaturation_WaterPressure(HydroMechanicalVariables & rVariables, MatrixType & rLocalLHS, const double & rIntegrationWeight);



}; // end Class UnsaturatedWaterPressureUtilities

} // namespace Kratos

#endif // KRATOS_WATER_PRESSURE_UTILITIE_HPP_INCLUDED
