//
//   Project Name:        KratosConstitutiveLegacyApplication $
//   Developed by:        $Developer:             JMCarbonell $
//   Maintained by:       $Maintainer:                    JMC $
//   Date:                $Date:                 January 2020 $
//
//

#include "constitutive_legacy_application_variables.h"

namespace Kratos
{

//variables
KRATOS_CREATE_VARIABLE(double, PENALTY_PARAMETER)
KRATOS_CREATE_VARIABLE(double, PRECONSOLIDATION)
KRATOS_CREATE_VARIABLE(double, STRESS_INV_P)
KRATOS_CREATE_VARIABLE(double, STRESS_INV_J2)
KRATOS_CREATE_VARIABLE(double, STRESS_INV_THETA)
KRATOS_CREATE_VARIABLE(double, VOLUMETRIC_PLASTIC)
KRATOS_CREATE_VARIABLE(double, INCR_SHEAR_PLASTIC)
KRATOS_CREATE_VARIABLE(Matrix, ELASTIC_LEFT_CAUCHY_GREEN_TENSOR)
KRATOS_CREATE_VARIABLE(Vector, ELASTIC_LEFT_CAUCHY_GREEN_VECTOR)

} // namespace Kratos
