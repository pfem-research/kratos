//-------------------------------------------------------------
//          ___             _   _ _        _   _
//  KRATOS / __|___ _ _  __| |_(_) |_ _  _| |_(_)_ _____
//        | (__/ _ \ ' \(_-<  _| |  _| || |  _| \ V / -_)
//         \___\___/_||_/__/\__|_|\__|\_,_|\__|_|\_/\___|LEGACY
//
//  License:(BSD)    ConstitutiveLegacyApplication/license.txt
//
//  Main authors:    Josep Maria Carbonell
//                   Lluis Monforte
//
//-------------------------------------------------------------
//
//   Project Name:        KratosConstitutiveLegacyApplication $
//   Developed by:        $Developer:             JMCarbonell $
//   Maintained by:       $Maintainer:                    JMC $
//   Date:                $Date:                 January 2020 $
//
//

#if !defined(KRATOS_CONSTITUTIVE_LEGACY_APPLICATION_H_INCLUDED)
#define KRATOS_CONSTITUTIVE_LEGACY_APPLICATION_H_INCLUDED

// System includes

// External includes

// Project includes
#include "includes/kratos_application.h"

//flow rules
#include "custom_constitutive/custom_flow_rules/non_linear_associative_plastic_flow_rule.hpp"
#include "custom_constitutive/custom_flow_rules/linear_associative_plastic_flow_rule.hpp"
#include "custom_constitutive/custom_flow_rules/isotropic_damage_flow_rule.hpp"
#include "custom_constitutive/custom_flow_rules/non_linear_rate_dependent_plastic_flow_rule.hpp"
#include "custom_constitutive/custom_flow_rules/non_associative_explicit_flow_rule.hpp"
#include "custom_constitutive/custom_flow_rules/borja_cam_clay_explicit_plastic_flow_rule.hpp"
#include "custom_constitutive/custom_flow_rules/J2_explicit_plastic_flow_rule.hpp"
#include "custom_constitutive/custom_flow_rules/tresca_explicit_plastic_flow_rule.hpp"

//yield criteria
#include "custom_constitutive/custom_yield_criteria/mises_huber_yield_criterion.hpp"
#include "custom_constitutive/custom_yield_criteria/simo_ju_yield_criterion.hpp"
#include "custom_constitutive/custom_yield_criteria/modified_mises_yield_criterion.hpp"
#include "custom_constitutive/custom_yield_criteria/mises_huber_thermal_yield_criterion.hpp"
#include "custom_constitutive/custom_yield_criteria/cam_clay_yield_criterion.hpp"
#include "custom_constitutive/custom_yield_criteria/J2_yield_criterion.hpp"
#include "custom_constitutive/custom_yield_criteria/tresca_yield_criterion.hpp"
#include "custom_constitutive/custom_yield_criteria/new_tresca_yield_criterion.hpp"

//hardening laws
#include "custom_constitutive/custom_hardening_laws/non_linear_isotropic_kinematic_hardening_law.hpp"
#include "custom_constitutive/custom_hardening_laws/linear_isotropic_kinematic_hardening_law.hpp"
#include "custom_constitutive/custom_hardening_laws/exponential_damage_hardening_law.hpp"
#include "custom_constitutive/custom_hardening_laws/modified_exponential_damage_hardening_law.hpp"
#include "custom_constitutive/custom_hardening_laws/non_linear_isotropic_kinematic_thermal_hardening_law.hpp"
#include "custom_constitutive/custom_hardening_laws/johnson_cook_thermal_hardening_law.hpp"
#include "custom_constitutive/custom_hardening_laws/baker_johnson_cook_thermal_hardening_law.hpp"
#include "custom_constitutive/custom_hardening_laws/cam_clay_hardening_law.hpp"

//constitutive laws
#include "custom_constitutive/hyperelastic_3D_law.hpp"
#include "custom_constitutive/hyperelastic_plane_strain_2D_law.hpp"
#include "custom_constitutive/hyperelastic_axisym_2D_law.hpp"

#include "custom_constitutive/hyperelastic_U_P_3D_law.hpp"
#include "custom_constitutive/hyperelastic_U_P_plane_strain_2D_law.hpp"
#include "custom_constitutive/hyperelastic_U_P_axisym_2D_law.hpp"

#include "custom_constitutive/linear_elastic_3D_law.hpp"
#include "custom_constitutive/linear_elastic_plane_strain_2D_law.hpp"
#include "custom_constitutive/linear_elastic_plane_stress_2D_law.hpp"
#include "custom_constitutive/linear_elastic_axisym_2D_law.hpp"
#include "custom_constitutive/linear_elastic_orthotropic_3D_law.hpp"

#include "custom_constitutive/hyperelastic_plastic_J2_3D_law.hpp"
#include "custom_constitutive/hyperelastic_plastic_J2_plane_strain_2D_law.hpp"
#include "custom_constitutive/hyperelastic_plastic_J2_axisym_2D_law.hpp"

#include "custom_constitutive/hyperelastic_plastic_U_P_J2_3D_law.hpp"
#include "custom_constitutive/hyperelastic_plastic_U_P_J2_plane_strain_2D_law.hpp"
#include "custom_constitutive/hyperelastic_plastic_U_P_J2_axisym_2D_law.hpp"

#include "custom_constitutive/isotropic_damage_simo_ju_3D_law.hpp"
#include "custom_constitutive/isotropic_damage_simo_ju_plane_strain_2D_law.hpp"
#include "custom_constitutive/isotropic_damage_simo_ju_plane_stress_2D_law.hpp"

#include "custom_constitutive/isotropic_damage_modified_mises_3D_law.hpp"
#include "custom_constitutive/isotropic_damage_modified_mises_plane_strain_2D_law.hpp"
#include "custom_constitutive/isotropic_damage_modified_mises_plane_stress_2D_law.hpp"

#include "custom_constitutive/hyperelastic_plastic_thermal_J2_plane_strain_2D_law.hpp"
#include "custom_constitutive/hyperelastic_plastic_thermal_johnson_cook_plane_strain_2D_law.hpp"
#include "custom_constitutive/hyperelastic_plastic_thermal_baker_johnson_cook_plane_strain_2D_law.hpp"

#include "custom_constitutive/hyperelastic_plastic_thermal_U_P_J2_3D_law.hpp"
#include "custom_constitutive/hyperelastic_plastic_thermal_U_P_J2_plane_strain_2D_law.hpp"
#include "custom_constitutive/hyperelastic_plastic_thermal_U_P_J2_axisym_2D_law.hpp"
#include "custom_constitutive/hyperelastic_plastic_thermal_U_P_johnson_cook_plane_strain_2D_law.hpp"
#include "custom_constitutive/hyperelastic_plastic_thermal_U_P_johnson_cook_axisym_2D_law.hpp"
#include "custom_constitutive/hyperelastic_plastic_thermal_U_P_baker_johnson_cook_plane_strain_2D_law.hpp"

#include "custom_constitutive/borja_hencky_cam_clay_3D_law.hpp"
#include "custom_constitutive/borja_hencky_cam_clay_axisym_2D_law.hpp"
#include "custom_constitutive/borja_hencky_cam_clay_plane_strain_2D_law.hpp"
#include "custom_constitutive/hencky_J2_plane_strain_2D_law.hpp"
#include "custom_constitutive/hencky_J2_axisym_2D_law.hpp"
#include "custom_constitutive/hencky_tresca_3D_law.hpp"
#include "custom_constitutive/hencky_tresca_plane_strain_2D_law.hpp"
#include "custom_constitutive/new_hencky_tresca_plane_strain_2D_law.hpp"
#include "custom_constitutive/hencky_tresca_axisym_2D_law.hpp"
#include "custom_constitutive/new_hencky_tresca_axisym_2D_law.hpp"
#include "custom_constitutive/hencky_U_P_J2_axisym_2D_law.hpp"
#include "custom_constitutive/hencky_U_P_J2_plane_strain_2D_law.hpp"
#include "custom_constitutive/hencky_U_P_Tresca_axisym_2D_law.hpp"
#include "custom_constitutive/hencky_U_P_Tresca_plane_strain_2D_law.hpp"
#include "constitutive_legacy_application_variables.h"

namespace Kratos
{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
*/
class KRATOS_API(CONSTITUTIVE_LEGACY_APPLICATION) KratosConstitutiveLegacyApplication : public KratosApplication
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of KratosConstitutiveLegacyApplication
  KRATOS_CLASS_POINTER_DEFINITION(KratosConstitutiveLegacyApplication);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  KratosConstitutiveLegacyApplication();

  /// Destructor.
  ~KratosConstitutiveLegacyApplication() override {}

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  void Register() override;

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "KratosConstitutiveLegacyApplication";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << Info();
    PrintData(rOStream);
  }

  ///// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
    KRATOS_WATCH("in KratosConstitutiveLegacyApplication");
    KRATOS_WATCH(KratosComponents<VariableData>::GetComponents().size());

    rOStream << "Variables:" << std::endl;
    KratosComponents<VariableData>().PrintData(rOStream);
    rOStream << std::endl;
    rOStream << "Elements:" << std::endl;
    KratosComponents<Element>().PrintData(rOStream);
    rOStream << std::endl;
    rOStream << "Conditions:" << std::endl;
    KratosComponents<Condition>().PrintData(rOStream);
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  // static const ApplicationCondition  msApplicationCondition;

  ///@}
  ///@name Member Variables
  ///@{

  //constitutive laws

  //Hyperelastic laws
  const HyperElastic3DLaw mHyperElastic3DLaw;
  const HyperElasticPlaneStrain2DLaw mHyperElasticPlaneStrain2DLaw;
  const HyperElasticAxisym2DLaw mHyperElasticAxisym2DLaw;

  //Hyperelastic laws U-P
  const HyperElasticUP3DLaw mHyperElasticUP3DLaw;
  const HyperElasticUPPlaneStrain2DLaw mHyperElasticUPPlaneStrain2DLaw;
  const HyperElasticUPAxisym2DLaw mHyperElasticUPAxisym2DLaw;

  //Linear Elastic laws
  const LinearElastic3DLaw mLinearElastic3DLaw;
  const LinearElasticPlaneStrain2DLaw mLinearElasticPlaneStrain2DLaw;
  const LinearElasticPlaneStress2DLaw mLinearElasticPlaneStress2DLaw;
  const LinearElasticAxisym2DLaw mLinearElasticAxisym2DLaw;

  //Hyperelastic Plastic J2 specilization laws
  const HyperElasticPlasticJ23DLaw mHyperElasticPlasticJ23DLaw;
  const HyperElasticPlasticJ2PlaneStrain2DLaw mHyperElasticPlasticJ2PlaneStrain2DLaw;
  const HyperElasticPlasticJ2Axisym2DLaw mHyperElasticPlasticJ2Axisym2DLaw;

  //Hyperelastic Plastic J2 specilization laws U-P
  const HyperElasticPlasticUPJ23DLaw mHyperElasticPlasticUPJ23DLaw;
  const HyperElasticPlasticUPJ2PlaneStrain2DLaw mHyperElasticPlasticUPJ2PlaneStrain2DLaw;
  const HyperElasticPlasticUPJ2Axisym2DLaw mHyperElasticPlasticUPJ2Axisym2DLaw;

  //Isotropic Damage Laws
  const IsotropicDamageSimoJu3DLaw mIsotropicDamageSimoJu3DLaw;
  const IsotropicDamageSimoJuPlaneStrain2DLaw mIsotropicDamageSimoJuPlaneStrain2DLaw;
  const IsotropicDamageSimoJuPlaneStress2DLaw mIsotropicDamageSimoJuPlaneStress2DLaw;

  const IsotropicDamageModifiedMises3DLaw mIsotropicDamageModifiedMises3DLaw;
  const IsotropicDamageModifiedMisesPlaneStrain2DLaw mIsotropicDamageModifiedMisesPlaneStrain2DLaw;
  const IsotropicDamageModifiedMisesPlaneStress2DLaw mIsotropicDamageModifiedMisesPlaneStress2DLaw;

  //Thermal Laws
  const HyperElasticPlasticThermalJ2PlaneStrain2DLaw mHyperElasticPlasticThermalJ2PlaneStrain2DLaw;
  const HyperElasticPlasticThermalJohnsonCookPlaneStrain2DLaw mHyperElasticPlasticThermalJohnsonCookPlaneStrain2DLaw;
  const HyperElasticPlasticThermalBakerJohnsonCookPlaneStrain2DLaw mHyperElasticPlasticThermalBakerJohnsonCookPlaneStrain2DLaw;

  const HyperElasticPlasticThermalUPJ23DLaw mHyperElasticPlasticThermalUPJ23DLaw;
  const HyperElasticPlasticThermalUPJ2PlaneStrain2DLaw mHyperElasticPlasticThermalUPJ2PlaneStrain2DLaw;
  const HyperElasticPlasticThermalUPJ2Axisym2DLaw mHyperElasticPlasticThermalUPJ2Axisym2DLaw;
  const HyperElasticPlasticThermalUPJohnsonCookPlaneStrain2DLaw mHyperElasticPlasticThermalUPJohnsonCookPlaneStrain2DLaw;
  const HyperElasticPlasticThermalUPJohnsonCookAxisym2DLaw mHyperElasticPlasticThermalUPJohnsonCookAxisym2DLaw;
  const HyperElasticPlasticThermalUPBakerJohnsonCookPlaneStrain2DLaw mHyperElasticPlasticThermalUPBakerJohnsonCookPlaneStrain2DLaw;

  //Solid Laws
  const BorjaHenckyCamClayPlastic3DLaw mBorjaHenckyCamClayPlastic3DLaw;
  const BorjaHenckyCamClayPlasticAxisym2DLaw mBorjaHenckyCamClayPlasticAxisym2DLaw;
  const BorjaHenckyCamClayPlasticPlaneStrain2DLaw mBorjaHenckyCamClayPlasticPlaneStrain2DLaw;
  const HenckyJ2PlasticPlaneStrain2DLaw mHenckyJ2PlasticPlaneStrain2DLaw;
  const HenckyJ2PlasticAxisym2DLaw mHenckyJ2PlasticAxisym2DLaw;
  const HenckyTrescaPlasticAxisym2DLaw mHenckyTrescaPlasticAxisym2DLaw;
  const NewHenckyTrescaPlasticAxisym2DLaw mNewHenckyTrescaPlasticAxisym2DLaw;
  const HenckyTrescaPlasticPlaneStrain2DLaw mHenckyTrescaPlasticPlaneStrain2DLaw;
  const NewHenckyTrescaPlasticPlaneStrain2DLaw mNewHenckyTrescaPlasticPlaneStrain2DLaw;
  const HenckyTresca3DLaw mHenckyTresca3DLaw;
  const HenckyPlasticUPJ2Axisym2DLaw mHenckyPlasticUPJ2Axisym2DLaw;
  const HenckyPlasticUPJ2PlaneStrain2DLaw mHenckyPlasticUPJ2PlaneStrain2DLaw;
  const HenckyPlasticUPTrescaAxisym2DLaw mHenckyPlasticUPTrescaAxisym2DLaw;
  const HenckyPlasticUPTrescaPlaneStrain2DLaw mHenckyPlasticUPTrescaPlaneStrain2DLaw;
  
  //Flow Rules
  const NonLinearAssociativePlasticFlowRule mNonLinearAssociativePlasticFlowRule;
  const LinearAssociativePlasticFlowRule mLinearAssociativePlasticFlowRule;
  const IsotropicDamageFlowRule mIsotropicDamageFlowRule;
  const NonLinearRateDependentPlasticFlowRule mNonLinearRateDependentPlasticFlowRule;
  const J2ExplicitFlowRule mJ2ExplicitFlowRule;
  const TrescaExplicitFlowRule mTrescaExplicitFlowRule;
  const BorjaCamClayExplicitFlowRule mBorjaCamClayExplicitFlowRule;

  //Yield Criteria
  const MisesHuberYieldCriterion mMisesHuberYieldCriterion;
  const SimoJuYieldCriterion mSimoJuYieldCriterion;
  const ModifiedMisesYieldCriterion mModifiedMisesYieldCriterion;
  const MisesHuberThermalYieldCriterion mMisesHuberThermalYieldCriterion;
  const J2YieldCriterion mJ2YieldCriterion;
  const TrescaYieldCriterion mTrescaYieldCriterion;
  const NewTrescaYieldCriterion mNewTrescaYieldCriterion;
  const CamClayYieldCriterion mCamClayYieldCriterion;

  //Hardening Laws
  const NonLinearIsotropicKinematicHardeningLaw mNonLinearIsotropicKinematicHardeningLaw;
  const LinearIsotropicKinematicHardeningLaw mLinearIsotropicKinematicHardeningLaw;
  const ExponentialDamageHardeningLaw mExponentialDamageHardeningLaw;
  const ModifiedExponentialDamageHardeningLaw mModifiedExponentialDamageHardeningLaw;

  const NonLinearIsotropicKinematicThermalHardeningLaw mNonLinearIsotropicKinematicThermalHardeningLaw;
  const JohnsonCookThermalHardeningLaw mJohnsonCookThermalHardeningLaw;
  const BakerJohnsonCookThermalHardeningLaw mBakerJohnsonCookThermalHardeningLaw;

  const CamClayHardeningLaw mCamClayHardeningLaw;
  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  /// Assignment operator.
  KratosConstitutiveLegacyApplication &operator=(KratosConstitutiveLegacyApplication const &rOther);

  /// Copy constructor.
  KratosConstitutiveLegacyApplication(KratosConstitutiveLegacyApplication const &rOther);

  ///@}

}; // Class KratosConstitutiveLegacyApplication

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

} // namespace Kratos.

#endif // KRATOS_CONSTITUTIVE_LEGACY_APPLICATION_H_INCLUDED  defined
