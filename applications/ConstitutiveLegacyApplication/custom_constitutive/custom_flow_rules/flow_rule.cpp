//
//   Project Name:        KratosConstitutiveLegacyApplication $
//   Developed by:        $Developer:             JMCarbonell $
//   Maintained by:       $Maintainer:                    JMC $
//   Date:                $Date:                 January 2020 $
//
//

// System includes

// External includes

// Project includes
#include "custom_constitutive/custom_flow_rules/flow_rule.hpp"

namespace Kratos
{

KRATOS_CREATE_LOCAL_FLAG(FlowRule, IMPLEX_ACTIVE, 0);
KRATOS_CREATE_LOCAL_FLAG(FlowRule, PLASTIC_REGION, 1);
KRATOS_CREATE_LOCAL_FLAG(FlowRule, PLASTIC_RATE_REGION, 2);
KRATOS_CREATE_LOCAL_FLAG(FlowRule, RETURN_MAPPING_COMPUTED, 3);

} // namespace Kratos.
