//
//   Project Name:        KratosConstitutiveLegacyApplication $
//   Developed by:        $Developer:               LMonforte $
//   Maintained by:       $Maintainer:                     LM $
//   Date:                $Date:                 January 2020 $
//
//

// System includes

// External includes

// Project includes
#include "custom_constitutive/new_hencky_tresca_plane_strain_2D_law.hpp"

#include "constitutive_legacy_application_variables.h"

namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

NewHenckyTrescaPlasticPlaneStrain2DLaw::NewHenckyTrescaPlasticPlaneStrain2DLaw()
    : HenckyTrescaPlasticPlaneStrain2DLaw()
{
  mpFlowRule = FlowRule::Pointer(new TrescaExplicitFlowRule()); //not relevant since it is associated
  mpYieldCriterion = YieldCriterion::Pointer(new NewTrescaYieldCriterion());
  mpHardeningLaw = HardeningLaw::Pointer(new HardeningLaw());
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

NewHenckyTrescaPlasticPlaneStrain2DLaw::NewHenckyTrescaPlasticPlaneStrain2DLaw(FlowRulePointer pFlowRule, YieldCriterionPointer pYieldCriterion, HardeningLawPointer pHardeningLaw)
{
  mpHardeningLaw = pHardeningLaw;
  mpYieldCriterion = YieldCriterion::Pointer(new NewTrescaYieldCriterion(mpHardeningLaw));
  mpFlowRule = pFlowRule;
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

NewHenckyTrescaPlasticPlaneStrain2DLaw::NewHenckyTrescaPlasticPlaneStrain2DLaw(const NewHenckyTrescaPlasticPlaneStrain2DLaw &rOther)
    : HenckyTrescaPlasticPlaneStrain2DLaw(rOther)
{
}

//********************************CLONE***********************************************
//************************************************************************************

ConstitutiveLaw::Pointer NewHenckyTrescaPlasticPlaneStrain2DLaw::Clone() const
{
  NewHenckyTrescaPlasticPlaneStrain2DLaw::Pointer p_clone(new NewHenckyTrescaPlasticPlaneStrain2DLaw(*this));
  return p_clone;
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

NewHenckyTrescaPlasticPlaneStrain2DLaw::~NewHenckyTrescaPlasticPlaneStrain2DLaw()
{
}

} // Namespace Kratos
