//
//   Project Name:        KratosConstitutiveLegacyApplication $
//   Developed by:        $Developer:               LMonforte $
//   Maintained by:       $Maintainer:                     LM $
//   Date:                $Date:                 January 2020 $
//
//

// System includes

// External includes

// Project includes
#include "custom_constitutive/hencky_tresca_plane_strain_2D_law.hpp"

#include "constitutive_legacy_application_variables.h"

namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

HenckyTrescaPlasticPlaneStrain2DLaw::HenckyTrescaPlasticPlaneStrain2DLaw()
    : NonLinearHenckyElasticPlasticPlaneStrain2DLaw()
{
  mpHardeningLaw = HardeningLaw::Pointer(new HardeningLaw());
  mpYieldCriterion = YieldCriterion::Pointer(new TrescaYieldCriterion(mpHardeningLaw));
  mpFlowRule = FlowRule::Pointer(new TrescaExplicitFlowRule(mpYieldCriterion));
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

HenckyTrescaPlasticPlaneStrain2DLaw::HenckyTrescaPlasticPlaneStrain2DLaw(FlowRulePointer pFlowRule, YieldCriterionPointer pYieldCriterion, HardeningLawPointer pHardeningLaw)
{
  mpHardeningLaw = pHardeningLaw;
  mpYieldCriterion = YieldCriterion::Pointer(new TrescaYieldCriterion(mpHardeningLaw));
  mpFlowRule = pFlowRule;
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

HenckyTrescaPlasticPlaneStrain2DLaw::HenckyTrescaPlasticPlaneStrain2DLaw(const HenckyTrescaPlasticPlaneStrain2DLaw &rOther)
    : NonLinearHenckyElasticPlasticPlaneStrain2DLaw(rOther)
{
}

//********************************CLONE***********************************************
//************************************************************************************

ConstitutiveLaw::Pointer HenckyTrescaPlasticPlaneStrain2DLaw::Clone() const
{
  HenckyTrescaPlasticPlaneStrain2DLaw::Pointer p_clone(new HenckyTrescaPlasticPlaneStrain2DLaw(*this));
  return p_clone;
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

HenckyTrescaPlasticPlaneStrain2DLaw::~HenckyTrescaPlasticPlaneStrain2DLaw()
{
}

} // Namespace Kratos
