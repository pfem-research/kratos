//
//   Project Name:        KratosConstitutiveLegacyApplication $
//   Developed by:        $Developer:               LMonforte $
//   Maintained by:       $Maintainer:                     LM $
//   Date:                $Date:                 January 2020 $
//
//

// System includes

// External includes

// Project includes
#include "custom_constitutive/hencky_tresca_3D_law.hpp"

#include "constitutive_legacy_application_variables.h"

namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

HenckyTresca3DLaw::HenckyTresca3DLaw()
    : NonLinearHenckyElasticPlastic3DLaw()
{
  mpHardeningLaw = HardeningLaw::Pointer(new HardeningLaw());
  mpYieldCriterion = YieldCriterion::Pointer(new TrescaYieldCriterion(mpHardeningLaw));
  mpFlowRule = FlowRule::Pointer(new TrescaExplicitFlowRule(mpYieldCriterion));
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

HenckyTresca3DLaw::HenckyTresca3DLaw(FlowRulePointer pFlowRule, YieldCriterionPointer pYieldCriterion, HardeningLawPointer pHardeningLaw)
{
  mpHardeningLaw = pHardeningLaw;
  mpYieldCriterion = YieldCriterion::Pointer(new TrescaYieldCriterion(mpHardeningLaw));
  mpFlowRule = pFlowRule;
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

HenckyTresca3DLaw::HenckyTresca3DLaw(const HenckyTresca3DLaw &rOther)
    : NonLinearHenckyElasticPlastic3DLaw(rOther)
{
}

//********************************CLONE***********************************************
//************************************************************************************

ConstitutiveLaw::Pointer HenckyTresca3DLaw::Clone() const
{
  HenckyTresca3DLaw::Pointer p_clone(new HenckyTresca3DLaw(*this));
  return p_clone;
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

HenckyTresca3DLaw::~HenckyTresca3DLaw()
{
}

} // Namespace Kratos
