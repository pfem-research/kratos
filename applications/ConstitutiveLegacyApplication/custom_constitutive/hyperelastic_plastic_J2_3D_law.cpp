//
//   Project Name:        KratosConstitutiveLegacyApplication $
//   Developed by:        $Developer:             JMCarbonell $
//   Maintained by:       $Maintainer:                    JMC $
//   Date:                $Date:                 January 2020 $
//
//

// System includes

// External includes

// Project includes
#include "custom_constitutive/custom_flow_rules/linear_associative_plastic_flow_rule.hpp"
#include "custom_constitutive/custom_yield_criteria/mises_huber_yield_criterion.hpp"
#include "custom_constitutive/custom_hardening_laws/linear_isotropic_kinematic_hardening_law.hpp"
#include "custom_constitutive/hyperelastic_plastic_J2_3D_law.hpp"

#include "constitutive_legacy_application_variables.h"

namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

HyperElasticPlasticJ23DLaw::HyperElasticPlasticJ23DLaw()
    : HyperElasticPlastic3DLaw()
{
  mpHardeningLaw = HardeningLaw::Pointer(new LinearIsotropicKinematicHardeningLaw());
  mpYieldCriterion = YieldCriterion::Pointer(new MisesHuberYieldCriterion(mpHardeningLaw));
  mpFlowRule = FlowRule::Pointer(new LinearAssociativePlasticFlowRule(mpYieldCriterion));
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

HyperElasticPlasticJ23DLaw::HyperElasticPlasticJ23DLaw(FlowRulePointer pFlowRule, YieldCriterionPointer pYieldCriterion, HardeningLawPointer pHardeningLaw)
{
  mpHardeningLaw = pHardeningLaw;
  mpYieldCriterion = YieldCriterion::Pointer(new MisesHuberYieldCriterion(mpHardeningLaw));
  mpFlowRule = pFlowRule;
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

HyperElasticPlasticJ23DLaw::HyperElasticPlasticJ23DLaw(const HyperElasticPlasticJ23DLaw &rOther)
    : HyperElasticPlastic3DLaw(rOther)
{
}

//********************************CLONE***********************************************
//************************************************************************************

ConstitutiveLaw::Pointer HyperElasticPlasticJ23DLaw::Clone() const
{
  return Kratos::make_shared<HyperElasticPlasticJ23DLaw>(*this);
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

HyperElasticPlasticJ23DLaw::~HyperElasticPlasticJ23DLaw()
{
}

} // Namespace Kratos
