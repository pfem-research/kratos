//-------------------------------------------------------------
//          ___             _   _ _        _   _
//  KRATOS / __|___ _ _  __| |_(_) |_ _  _| |_(_)_ _____
//        | (__/ _ \ ' \(_-<  _| |  _| || |  _| \ V / -_)
//         \___\___/_||_/__/\__|_|\__|\_,_|\__|_|\_/\___|LEGACY
//
//  License:(BSD)    ConstitutiveLegacyApplication/license.txt
//
//  Main authors:    Josep Maria Carbonell 
//                   Lluis Monforte
//
//-------------------------------------------------------------
//
//   Project Name:        KratosConstitutiveLegacyApplication $
//   Developed by:        $Developer:             JMCarbonell $
//   Maintained by:       $Maintainer:                    JMC $
//   Date:                $Date:                 January 2020 $
//
//

#if !defined(KRATOS_CONSTITUTIVE_LEGACY_APPLICATION_VARIABLES_H_INCLUDED)
#define KRATOS_CONSTITUTIVE_LEGACY_APPLICATION_VARIABLES_H_INCLUDED

// System includes

// External includes

// Project includes
#include "includes/define.h"
#include "includes/variables.h"
#include "includes/mat_variables.h"

namespace Kratos
{
// variables
KRATOS_DEFINE_APPLICATION_VARIABLE(CONSTITUTIVE_LEGACY_APPLICATION, double, PENALTY_PARAMETER)
KRATOS_DEFINE_APPLICATION_VARIABLE(CONSTITUTIVE_LEGACY_APPLICATION, double, PRECONSOLIDATION)
KRATOS_DEFINE_APPLICATION_VARIABLE(CONSTITUTIVE_LEGACY_APPLICATION, double, STRESS_INV_P)
KRATOS_DEFINE_APPLICATION_VARIABLE(CONSTITUTIVE_LEGACY_APPLICATION, double, STRESS_INV_J2)
KRATOS_DEFINE_APPLICATION_VARIABLE(CONSTITUTIVE_LEGACY_APPLICATION, double, STRESS_INV_THETA)
KRATOS_DEFINE_APPLICATION_VARIABLE(CONSTITUTIVE_LEGACY_APPLICATION, double, VOLUMETRIC_PLASTIC)
KRATOS_DEFINE_APPLICATION_VARIABLE(CONSTITUTIVE_LEGACY_APPLICATION, double, INCR_SHEAR_PLASTIC)
KRATOS_DEFINE_APPLICATION_VARIABLE(CONSTITUTIVE_LEGACY_APPLICATION, Matrix, ELASTIC_LEFT_CAUCHY_GREEN_TENSOR)
KRATOS_DEFINE_APPLICATION_VARIABLE(CONSTITUTIVE_LEGACY_APPLICATION, Vector, ELASTIC_LEFT_CAUCHY_GREEN_VECTOR)

} // namespace Kratos

#endif /* KRATOS_CONSTITUTIVE_LEGACY_APPLICATION_VARIABLES_H_INCLUDED */
