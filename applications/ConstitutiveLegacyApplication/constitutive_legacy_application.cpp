//
//   Project Name:        KratosConstitutiveLegacyApplication $
//   Developed by:        $Developer:             JMCarbonell $
//   Maintained by:       $Maintainer:                    JMC $
//   Date:                $Date:                 January 2020 $
//
//

// System includes

// External includes

// Project includes
#include "constitutive_legacy_application.h"
#include "constitutive_legacy_application_variables.h"

namespace Kratos
{

KratosConstitutiveLegacyApplication::KratosConstitutiveLegacyApplication()
    : KratosApplication("ConstitutiveLegacyApplication")
{
}

void KratosConstitutiveLegacyApplication::Register()
{
  std::stringstream banner;

  banner << "             ___             _   _ _        _   _                \n"
         << "    KRATOS  / __|___ _ _  __| |_(_) |_ _  _| |_(_)_ _____        \n"
         << "           | (__/ _ \\ ' \\(_-<  _| |  _| || |  _| \\ V / -_)       \n"
         << "            \\___\\___/_||_/__/\\__|_|\\__|\\_,_|\\__|_|\\_/\\___| LEGACY\n"
         << "Initialize KratosConstitutiveLegacyApplication...  " << std::endl;

  // mpi initialization
  int mpi_is_initialized = 0;
  int rank = -1;

#ifdef KRATOS_MPI

  MPI_Initialized(&mpi_is_initialized);

  if (mpi_is_initialized)
  {
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  }

#endif

  if (mpi_is_initialized)
  {
    if (rank == 0)
      KRATOS_INFO("") << banner.str();
  }
  else
  {
    KRATOS_INFO("") << banner.str();
  }

  //variables
  KRATOS_REGISTER_VARIABLE(PENALTY_PARAMETER)
  KRATOS_REGISTER_VARIABLE(PRECONSOLIDATION)
  KRATOS_REGISTER_VARIABLE(STRESS_INV_P)
  KRATOS_REGISTER_VARIABLE(STRESS_INV_J2)
  KRATOS_REGISTER_VARIABLE(STRESS_INV_THETA)
  KRATOS_REGISTER_VARIABLE(VOLUMETRIC_PLASTIC)
  KRATOS_REGISTER_VARIABLE(INCR_SHEAR_PLASTIC)
  KRATOS_REGISTER_VARIABLE(ELASTIC_LEFT_CAUCHY_GREEN_TENSOR)
  KRATOS_REGISTER_VARIABLE(ELASTIC_LEFT_CAUCHY_GREEN_VECTOR)

  //Register Constitutive Laws with KRATOS_REGISTER to the Kernel
  //Linear Elastic laws
  KRATOS_REGISTER_CONSTITUTIVE_LAW("LinearElasticIsotropic3DLaw", mLinearElastic3DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("LinearElasticIsotropicPlaneStrain2DLaw", mLinearElasticPlaneStrain2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("LinearElasticIsotropicPlaneStress2DLaw", mLinearElasticPlaneStress2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("LinearElasticIsotropicAxisym2DLaw", mLinearElasticAxisym2DLaw);
  //Hyperelastic laws
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HyperElasticIsotropic3DLaw", mHyperElastic3DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HyperElasticIsotropicPlaneStrain2DLaw", mHyperElasticPlaneStrain2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HyperElasticIsotropicAxisym2DLaw", mHyperElasticAxisym2DLaw);
  //Hyperelastic laws U-P
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HyperElasticUP3DLaw", mHyperElasticUP3DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HyperElasticUPPlaneStrain2DLaw", mHyperElasticUPPlaneStrain2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HyperElasticUPAxisym2DLaw", mHyperElasticUPAxisym2DLaw);
  //Hyperelastic Plastic J2 specilization laws
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HyperElasticPlasticJ23DLaw", mHyperElasticPlasticJ23DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HyperElasticPlasticJ2PlaneStrain2DLaw", mHyperElasticPlasticJ2PlaneStrain2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HyperElasticPlasticJ2Axisym2DLaw", mHyperElasticPlasticJ2Axisym2DLaw);
  //Hyperelastic Plastic J2 specilization laws U-P
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HyperElasticPlasticUPJ23DLaw", mHyperElasticPlasticUPJ23DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HyperElasticPlasticUPJ2PlaneStrain2DLaw", mHyperElasticPlasticUPJ2PlaneStrain2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HyperElasticPlasticUPJ2Axisym2DLaw", mHyperElasticPlasticUPJ2Axisym2DLaw);
  //Isotropic Damage laws
  KRATOS_REGISTER_CONSTITUTIVE_LAW("IsotropicDamageSimoJu3DLaw", mIsotropicDamageSimoJu3DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("IsotropicDamageSimoJuPlaneStrain2DLaw", mIsotropicDamageSimoJuPlaneStrain2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("IsotropicDamageSimoJuPlaneStress2DLaw", mIsotropicDamageSimoJuPlaneStress2DLaw);

  KRATOS_REGISTER_CONSTITUTIVE_LAW("IsotropicDamageModifiedMises3DLaw", mIsotropicDamageModifiedMises3DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("IsotropicDamageModifiedMisesPlaneStrain2DLaw", mIsotropicDamageModifiedMisesPlaneStrain2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("IsotropicDamageModifiedMisesPlaneStress2DLaw", mIsotropicDamageModifiedMisesPlaneStress2DLaw);

  //Thermo-Mechanical laws
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HyperElasticPlasticThermalJ2PlaneStrain2DLaw", mHyperElasticPlasticThermalJ2PlaneStrain2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HyperElasticPlasticThermalJohnsonCookPlaneStrain2DLaw", mHyperElasticPlasticThermalJohnsonCookPlaneStrain2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HyperElasticPlasticThermalBakerJohnsonCookPlaneStrain2DLaw", mHyperElasticPlasticThermalBakerJohnsonCookPlaneStrain2DLaw);

  KRATOS_REGISTER_CONSTITUTIVE_LAW("HyperElasticPlasticThermalUPJ23DLaw", mHyperElasticPlasticThermalUPJ23DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HyperElasticPlasticThermalUPJ2PlaneStrain2DLaw", mHyperElasticPlasticThermalUPJ2PlaneStrain2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HyperElasticPlasticThermalUPJ2Axisym2DLaw", mHyperElasticPlasticThermalUPJ2Axisym2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HyperElasticPlasticThermalUPJohnsonCookPlaneStrain2DLaw", mHyperElasticPlasticThermalUPJohnsonCookPlaneStrain2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HyperElasticPlasticThermalUPJohnsonCookAxisym2DLaw", mHyperElasticPlasticThermalUPJohnsonCookAxisym2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HyperElasticPlasticThermalUPBakerJohnsonCookPlaneStrain2DLaw", mHyperElasticPlasticThermalUPBakerJohnsonCookPlaneStrain2DLaw);

  //Soil Laws
  KRATOS_REGISTER_CONSTITUTIVE_LAW("BorjaHenckyCamClayPlastic3DLaw", mBorjaHenckyCamClayPlastic3DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("BorjaHenckyCamClayPlasticAxisym2DLaw", mBorjaHenckyCamClayPlasticAxisym2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("BorjaHenckyCamClayPlasticPlaneStrain2DLaw", mBorjaHenckyCamClayPlasticPlaneStrain2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HenckyJ2PlasticPlaneStrain2DLaw", mHenckyJ2PlasticPlaneStrain2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HenckyJ2PlasticAxisym2DLaw", mHenckyJ2PlasticAxisym2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HenckyTrescaPlasticAxisym2DLaw", mHenckyTrescaPlasticAxisym2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("NewHenckyTrescaPlasticAxisym2DLaw", mNewHenckyTrescaPlasticAxisym2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HenckyTrescaPlasticPlaneStrain2DLaw", mHenckyTrescaPlasticPlaneStrain2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("NewHenckyTrescaPlasticAxisym2DLaw", mNewHenckyTrescaPlasticAxisym2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HenckyTresca3DLaw", mHenckyTresca3DLaw);

  KRATOS_REGISTER_CONSTITUTIVE_LAW("HenckyPlasticUPJ2Axisym2DLaw", mHenckyPlasticUPJ2Axisym2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HenckyPlasticUPJ2PlaneStrain2DLaw", mHenckyPlasticUPJ2PlaneStrain2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HenckyPlasticUPTrescaAxisym2DLaw", mHenckyPlasticUPTrescaAxisym2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("HenckyPlasticUPTrescaPlaneStrain2DLaw", mHenckyPlasticUPTrescaPlaneStrain2DLaw);

  //Register Flow Rules
  Serializer::Register("NonLinearAssociativePlasticFlowRule", mNonLinearAssociativePlasticFlowRule);
  Serializer::Register("LinearAssociativePlasticFlowRule", mLinearAssociativePlasticFlowRule);
  Serializer::Register("IsotropicDamageFlowRule", mIsotropicDamageFlowRule);
  Serializer::Register("NonLinearRateDependentPlasticFlowRule", mNonLinearRateDependentPlasticFlowRule);
  Serializer::Register("TrescaExplicitFlowRule", mTrescaExplicitFlowRule);
  Serializer::Register("J2ExplicitFlowRule", mJ2ExplicitFlowRule);
  Serializer::Register("BorjaCamClayExplicitFlowRule", mBorjaCamClayExplicitFlowRule);

  //Register Yield Criterion
  Serializer::Register("MisesHuberYieldCriterion", mMisesHuberYieldCriterion);
  Serializer::Register("SimoJuYieldCriterion", mSimoJuYieldCriterion);
  Serializer::Register("ModifiedMisesYieldCriterion", mModifiedMisesYieldCriterion);
  Serializer::Register("MisesHuberThermalYieldCriterion", mMisesHuberThermalYieldCriterion);
  Serializer::Register("J2YieldCriterion", mJ2YieldCriterion);
  Serializer::Register("NewTrescaYieldCriterion", mNewTrescaYieldCriterion);
  Serializer::Register("TrescaYieldCriterion", mTrescaYieldCriterion);
  Serializer::Register("CamClayYieldCriterion", mCamClayYieldCriterion);

  //Register Hardening Laws
  Serializer::Register("CamClayHardeningLaw", mCamClayHardeningLaw);
  Serializer::Register("NonLinearIsotropicKinematicHardeningLaw", mNonLinearIsotropicKinematicHardeningLaw);
  Serializer::Register("LinearIsotropicKinematicHardeningLaw", mLinearIsotropicKinematicHardeningLaw);
  Serializer::Register("ExponentialDamageHardeningLaw", mExponentialDamageHardeningLaw);
  Serializer::Register("ModifiedExponentialDamageHardeningLaw", mModifiedExponentialDamageHardeningLaw);
  Serializer::Register("NonLinearIsotropicKinematicThermalHardeningLaw", mNonLinearIsotropicKinematicThermalHardeningLaw);
  Serializer::Register("JohnsonCookThermalHardeningLaw", mJohnsonCookThermalHardeningLaw);
  Serializer::Register("BakerJohnsonCookThermalHardeningLaw", mBakerJohnsonCookThermalHardeningLaw);
}
} // namespace Kratos.
