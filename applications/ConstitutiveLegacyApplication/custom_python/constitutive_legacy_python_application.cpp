//
//   Project Name:        KratosConstitutiveLegacyApplication $
//   Developed by:        $Developer:             JMCarbonell $
//   Maintained by:       $Maintainer:                    JMC $
//   Date:                $Date:                 January 2020 $
//
//

// System includes

#if defined(KRATOS_PYTHON)
// External includes
#include <pybind11/pybind11.h>

// Project includes
#include "includes/define_python.h"
#include "constitutive_legacy_application.h"
#include "constitutive_legacy_application_variables.h"
#include "custom_python/add_custom_constitutive_laws_to_python.h"

namespace Kratos
{
namespace Python
{

namespace py = pybind11;
PYBIND11_MODULE(KratosConstitutiveLegacyApplication, m)
{
  py::class_<KratosConstitutiveLegacyApplication,
             KratosConstitutiveLegacyApplication::Pointer,
             KratosApplication>(m, "KratosConstitutiveLegacyApplication")
      .def(py::init<>());

  AddCustomConstitutiveLawsToPython(m);

  //registering variables in python
  KRATOS_REGISTER_IN_PYTHON_VARIABLE(m, PRECONSOLIDATION)
  KRATOS_REGISTER_IN_PYTHON_VARIABLE(m, STRESS_INV_P)
  KRATOS_REGISTER_IN_PYTHON_VARIABLE(m, STRESS_INV_J2)
  KRATOS_REGISTER_IN_PYTHON_VARIABLE(m, STRESS_INV_THETA)
  KRATOS_REGISTER_IN_PYTHON_VARIABLE(m, VOLUMETRIC_PLASTIC)
  KRATOS_REGISTER_IN_PYTHON_VARIABLE(m, INCR_SHEAR_PLASTIC)
}

} // namespace Python.
} // namespace Kratos.

#endif // KRATOS_PYTHON defined
