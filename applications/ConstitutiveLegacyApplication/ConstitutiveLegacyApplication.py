""" Project: ConstitutiveLegacyApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Kratos Imports
from KratosMultiphysics import _ImportApplication

from KratosConstitutiveLegacyApplication import *
application = KratosConstitutiveLegacyApplication()
application_name = "KratosConstitutiveLegacyApplication"
application_folder = "ConstitutiveLegacyApplication"

_ImportApplication(application, application_name)
