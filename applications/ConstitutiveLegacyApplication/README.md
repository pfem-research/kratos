# Constitutive Legacy Application

This application is the legacy of the Constitutive Laws for solids and soils implemented in the applications:

    SolidMechanicsApplication
    PfemSolidMechanicsApplication

## Legacy

It means that there is an up to date application where the materials constitutive laws are implemented. It is:
    
    ConstitutiveModelsApplication

## License

The ConstitutiveLegacy application is OPEN SOURCE. The main code and program structure is available and aimed to grow with the need of any user willing to expand it. The BSD (Berkeley Software Distribution) licence allows to use and distribute the existing code without any restriction, but with the possibility to develop new parts of the code on an open or close source depending on the developers.

## Contact

* **Josep Maria Carbonell** - *Core Development* - [cpuigbo@cimne.upc.edu](mailto:cpuigbo@cimne.upc.edu)
* **Lluís Monforte Vila** - *Core Development* - [lluis.monforte@upc.edu](mailto:lluis.monforte@upc.edu)