//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

#if !defined(KRATOS_COMPOSITE_CONDITION_HPP_INCLUDED)
#define KRATOS_COMPOSITE_CONDITION_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "includes/condition.h"
#include "includes/kratos_flags.h"

namespace Kratos
{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{


/**
 * Implements a Composite Condition
 */

class KRATOS_API(MESHERS_APPLICATION) CompositeCondition
    : public Condition
{
public:
    ///@name Type Definitions
    ///@{
    typedef Condition ConditionType;
    typedef PointerVectorSet<ConditionType, IndexedObject> ConditionsContainerType;
    typedef ConditionsContainerType::iterator ConditionIterator;
    typedef ConditionsContainerType::const_iterator ConditionConstantIterator;
    typedef GeometryData::SizeType SizeType;

    /// Counted pointer of CompositeCondition
    KRATOS_CLASS_INTRUSIVE_POINTER_DEFINITION(CompositeCondition);

    ///@}
    ///@name Life Cycle
    ///@{

    /// Default constructors.
    CompositeCondition(IndexType NewId, GeometryType::Pointer pGeometry);

    CompositeCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties);

    ///Copy constructor
    CompositeCondition(CompositeCondition const &rOther);

    /// Destructor.
    virtual ~CompositeCondition();

    ///@}
    ///@name Operators
    ///@{

    /// Assignment operator.
    CompositeCondition &operator=(CompositeCondition const &rOther);

    ///@}
    ///@name Operations
    ///@{

    SizeType NumberOfChildren() const
    {
        return mChildConditions.size();
    }

    /** Inserts a condition in the composite.
    */
    void AddChild(ConditionType::Pointer pNewChildCondition);

    /** Returns the Condition::Pointer  corresponding to it's identifier */
    ConditionType::Pointer pGetChild(IndexType ChildConditionId)
    {
        return (mChildConditions)(ChildConditionId);
    }

    /** Returns a reference condition corresponding to it's identifier */
    ConditionType &GetChild(IndexType ChildConditionId)
    {
        return (mChildConditions)[ChildConditionId];
    }

    /** Remove the condition with given Id from composite.
    */
    void RemoveChild(IndexType ChildConditionId)
    {
        mChildConditions.erase(ChildConditionId);
    }

    /** Remove given condition from composite.
    */
    void RemoveChild(ConditionType &ThisChildCondition)
    {
        mChildConditions.erase(ThisChildCondition.Id());
    }

    /** Remove given condition from composite.
    */
    void RemoveChild(ConditionType::Pointer pThisChildCondition)
    {
        mChildConditions.erase(pThisChildCondition->Id());
    }

    ConditionIterator ChildConditionsBegin()
    {
        return mChildConditions.begin();
    }

    ConditionConstantIterator ChildConditionsBegin() const
    {
        return mChildConditions.begin();
    }

    ConditionIterator ChildConditionsEnd()
    {
        return mChildConditions.end();
    }

    ConditionConstantIterator ChildConditionsEnd() const
    {
        return mChildConditions.end();
    }

    ConditionsContainerType &ChildConditions()
    {
        return mChildConditions;
    }

    ConditionsContainerType::Pointer pChildConditions()
    {
        return ConditionsContainerType::Pointer(&mChildConditions);
    }

    void SetChildConditions(ConditionsContainerType::Pointer pOtherChildConditions)
    {
        mChildConditions = (*pOtherChildConditions);
    }

    ConditionsContainerType::ContainerType &ChildConditionsArray()
    {
        return mChildConditions.GetContainer();
    }

    /**
     * creates a new condition pointer
     * @param NewId: the ID of the new condition
     * @param ThisNodes: the nodes of the new condition
     * @param pProperties: the properties assigned to the new condition
     * @return a Pointer to the new condition
     */
    Condition::Pointer Create(IndexType NewId, NodesArrayType const &ThisNodes,
                              PropertiesType::Pointer pProperties) const override;

    /**
     * creates a new condition pointer
     * @param NewId: the ID of the new condition
     * @param pGeom: the geometry to be employed
     * @param pProperties: the properties assigned to the new condition
     * @return a Pointer to the new condition
     */
    Condition::Pointer Create(IndexType NewId,
                              GeometryType::Pointer pGeom,
                              PropertiesType::Pointer pProperties) const override;

    /**
     * clones the selected condition variables, creating a new one
     * @param NewId: the ID of the new condition
     * @param ThisNodes: the nodes of the new condition
     * @param pProperties: the properties assigned to the new condition
     * @return a Pointer to the new condition
     */
    Condition::Pointer Clone(IndexType NewId, NodesArrayType const &ThisNodes) const override;

    //************* GETTING METHODS

    /**
     * Returns the currently selected integration method
     * @return current integration method selected
     */
    IntegrationMethod GetIntegrationMethod() const override;

    /**
     * Sets on rConditionalDofList the degrees of freedom of the considered element geometry
     */
    void GetDofList(DofsVectorType &rConditionalDofList, const ProcessInfo &rCurrentProcessInfo) const override;

    /**
     * Sets on rResult the ID's of the element degrees of freedom
     */
    void EquationIdVector(EquationIdVectorType &rResult, const ProcessInfo &rCurrentProcessInfo) const override;

    /**
     * Sets on rValues the nodal displacements
     */
    void GetValuesVector(Vector &rValues, int Step = 0) const override;

    /**
     * Sets on rValues the nodal velocities
     */
    void GetFirstDerivativesVector(Vector &rValues, int Step = 0) const override;

    /**
     * Sets on rValues the nodal accelerations
     */
    void GetSecondDerivativesVector(Vector &rValues, int Step = 0) const override;


    //SET
    /**
     * Set a Vector Value on the Condition Constitutive Law
     */
    void SetValuesOnIntegrationPoints(const Variable<Vector> &rVariable, const std::vector<Vector> &rValues, const ProcessInfo &rCurrentProcessInfo) override;

    /**
     * Set a Matrix Value on the Condition Constitutive Law
     */
    void SetValuesOnIntegrationPoints(const Variable<Matrix> &rVariable, const std::vector<Matrix> &rValues, const ProcessInfo &rCurrentProcessInfo) override;

    //GET:
    //on integration points:
    /**
     * Calculate a double Variable on the Condition Constitutive Law
     */
    void CalculateOnIntegrationPoints(const Variable<double> &rVariable, std::vector<double> &rOutput, const ProcessInfo &rCurrentProcessInfo) override;

    /**
     * Calculate a array_1d Variable on the Condition Constitutive Law
     */
    void CalculateOnIntegrationPoints(const Variable<array_1d<double, 3> > &rVariable, std::vector<array_1d<double, 3> > &rOutput, const ProcessInfo &rCurrentProcessInfo) override;

    /**
     * Calculate a Vector Variable on the Condition Constitutive Law
     */
    void CalculateOnIntegrationPoints(const Variable<Vector> &rVariable, std::vector<Vector> &rOutput, const ProcessInfo &rCurrentProcessInfo) override;

    /**
     * Calculate a Matrix Variable on the Condition Constitutive Law
     */
    void CalculateOnIntegrationPoints(const Variable<Matrix> &rVariable, std::vector<Matrix> &rOutput, const ProcessInfo &rCurrentProcessInfo) override;

    //************* STARTING - ENDING  METHODS

    /**
      * Called to initialize the element.
      * Must be called before any calculation is done
      */
    void Initialize(const ProcessInfo &rCurrentProcessInfo) override;

    /**
     * Called at the beginning of each solution step
     */
    void InitializeSolutionStep(const ProcessInfo &rCurrentProcessInfo) override;

    /**
     * this is called for non-linear analysis at the beginning of the iteration process
     */
    void InitializeNonLinearIteration(const ProcessInfo &rCurrentProcessInfo) override;

    /**
     * Called at the end of eahc solution step
     */
    void FinalizeSolutionStep(const ProcessInfo &rCurrentProcessInfo) override;

    //************* COMPUTING  METHODS

    /**
     * this is called during the assembling process in order
     * to calculate all elemental contributions to the global system
     * matrix and the right hand side
     * @param rLeftHandSideMatrix: the elemental left hand side matrix
     * @param rRightHandSideVector: the elemental right hand side
     * @param rCurrentProcessInfo: the current process info instance
     */
    void CalculateLocalSystem(MatrixType &rLeftHandSideMatrix, VectorType &rRightHandSideVector, const ProcessInfo &rCurrentProcessInfo) override;

    /**
      * this is called during the assembling process in order
      * to calculate the elemental right hand side vector only
      * @param rRightHandSideVector: the elemental right hand side vector
      * @param rCurrentProcessInfo: the current process info instance
      */
    void CalculateRightHandSide(VectorType &rRightHandSideVector, const ProcessInfo &rCurrentProcessInfo) override;

    /**
     * this is called during the assembling process in order
     * to calculate the elemental left hand side vector only
     * @param rLeftHandSideVector: the elemental left hand side vector
     * @param rCurrentProcessInfo: the current process info instance
     */
    void CalculateLeftHandSide(MatrixType &rLeftHandSideMatrix, const ProcessInfo &rCurrentProcessInfo) override;

    /**
      * this is called during the assembling process in order
      * to calculate the elemental mass matrix
      * @param rMassMatrix: the elemental mass matrix
      * @param rCurrentProcessInfo: the current process info instance
      */
    void CalculateMassMatrix(MatrixType &rMassMatrix, const ProcessInfo &rCurrentProcessInfo) override;

    /**
      * this is called during the assembling process in order
      * to calculate the elemental damping matrix
      * @param rDampingMatrix: the elemental damping matrix
      * @param rCurrentProcessInfo: the current process info instance
      */
    void CalculateDampingMatrix(MatrixType &rDampingMatrix, const ProcessInfo &rCurrentProcessInfo) override;

    /**
     * this function is designed to make the element to assemble an rRHS vector
     * identified by a variable rRHSVariable by assembling it to the nodes on the variable
     * rDestinationVariable.
     * @param rRHSVector: input variable containing the RHS vector to be assembled
     * @param rRHSVariable: variable describing the type of the RHS vector to be assembled
     * @param rDestinationVariable: variable in the database to which the rRHSvector will be assembled
     * @param rCurrentProcessInfo: the current process info instance
     */
    void AddExplicitContribution(const VectorType &rRHS, const Variable<VectorType> &rRHSVariable,
                                 const Variable<array_1d<double, 3>> &rDestinationVariable,
                                 const ProcessInfo &rCurrentProcessInfo) override;



    void Calculate(const Variable<double> &rVariable, double &rOutput, const ProcessInfo &rCurrentProcessInfo) override;

    //************************************************************************************
    //************************************************************************************
    /**
     * This function provides the place to perform checks on the completeness of the input.
     * It is designed to be called only once (or anyway, not often) typically at the beginning
     * of the calculations, so to verify that nothing is missing from the input
     * or that no common error is found.
     * @param rCurrentProcessInfo
     */
    int Check(const ProcessInfo &rCurrentProcessInfo) const override;

    ///@}
    ///@name Access
    ///@{
    ///@}
    ///@name Inquiry
    ///@{
    ///@}
    ///@name Input and output
    ///@{

    /// Turn back information as a string.

    std::string Info() const override
    {
        std::stringstream buffer;
        buffer << "Condition CompositeCondition #" << Id();
        return buffer.str();
    }

    /// Print information about this object.

    void PrintInfo(std::ostream &rOStream) const override
    {
        rOStream << "Condition CompositeCondition #" << Id();
    }

    ///@}
    ///@name Friends
    ///@{
    ///@}

protected:
    ///@name Protected static Member Variables
    ///@{
    ///@}
    ///@name Protected member Variables
    ///@{
    ///@}
    ///@name Protected Operators
    ///@{
    ///@}
    ///@name Protected Operations
    ///@{
    ///@}
    ///@name Protected  Access
    ///@{
    ///@}
    ///@name Protected Inquiry
    ///@{
    ///@}
    ///@name Protected LifeCycle
    ///@{
    ///@}

private:
    ///@name Private static Member Variables
    ///@{
    ///@}
    ///@name Private member Variables
    ///@{
    ///@}

    ConditionsContainerType mChildConditions;

    bool mInitializedChildren;

    ///@}
    ///@name Private Operators
    ///@{

    ///@}
    ///@name Private Operations
    ///@{

    //initialize children from set value CHILDREN_CONDITIONS
    void InitializeChildren();

    //check problem type definition and if coincides return active true
    bool IsActive(ConditionConstantIterator iChildCondition, const ProcessInfo &rCurrentProcessInfo) const;
    bool IsActive(ConditionIterator iChildCondition, const ProcessInfo &rCurrentProcessInfo) const;

    //set specific data value to condition children
    template <class TVariableType>
    void SetValueToChildren(const TVariableType &rVariable)
    {

        for (ConditionIterator cn = mChildConditions.begin(); cn != mChildConditions.end(); ++cn)
        {
            typename TVariableType::Type const &rValue = this->GetValue(rVariable);
            cn->SetValue(rVariable, rValue);
        }
    }

    /**
     * Get element size from the dofs
     */
    virtual SizeType GetDofsSize(const ProcessInfo &rCurrentProcessInfo) const;

    ///@}
    ///@name Private  Access
    ///@{
    ///@}
    ///@}
    ///@name Serialization
    ///@{

    friend class Serializer;

    void save(Serializer &rSerializer) const override;
    void load(Serializer &rSerializer) override;

    ///@name Private Inquiry
    ///@{
    ///@}
    ///@name Un accessible methods
    ///@{

    // A private default constructor necessary for serialization
    CompositeCondition() : Condition() {}

    ///@}

}; // Class CompositeCondition

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
///@}

} // namespace Kratos.
#endif // KRATOS_COMPOSITE_CONDITION_HPP_INCLUDED  defined
