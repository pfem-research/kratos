""" Project: MeshersApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""


# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.MeshersApplication as KratosMeshers
import KratosMultiphysics.MeshersApplication.mesher as mesher


def CreateMesher(main_model_part, meshing_parameters):
    return PostRefiningMesher(main_model_part, meshing_parameters)


class PostRefiningMesher(mesher.Mesher):
    #
    def SetPreMeshingProcesses(self):
        # no process to start pre refining process input domain
        pass

    #
    def SetPostMeshingProcesses(self):

        # processes to rebuild domain
        processes = []

        # The order set is the order of execution:
        processes.append(KratosMeshers.GenerateNewNodes(self.model_part, self.MeshingParameters, self.echo_level))
        processes.append(KratosMeshers.SelectElements(self.model_part, self.MeshingParameters, self.echo_level))
        processes.append(KratosMeshers.GenerateNewElements(self.model_part, self.MeshingParameters, self.echo_level))
        processes.append(KratosMeshers.GenerateNewConditions(self.model_part, self.MeshingParameters, self.echo_level))

        [self.mesher.SetPostMeshingProcess(process) for process in processes]

    #
    def _get_mesher_info(self):
        return "Refine the domain"

    #
    def _get_mesher_flags(self):
        # set mesher flags: to set options for the mesher (triangle 2D, tetgen 3D)
        # REFINE
        mesher_flags = ""
        if self.dimension == 2:
            if self._constrained():
                mesher_flags = "pYJq1.4arnCQ"
            else:
                mesher_flags = "YJq1.4arnQ"

            """ if some process creates a custom insertion of a list of nodes
            #"riYYJQ" "riYYJQ" "riJQ" "riQ"
            if self._constrained():
                mesher_flags = "rinYYJQ"
            else:
                mesher_flags = "rinJQ"
            """
        elif self.dimension == 3:
            if self._constrained():
                mesher_flags = "pMYJq2.4arnCBQF"
            else:
                mesher_flags = "YJq1.4arnBQF" # with Y boundary not refined
                #mesher_flags = "rqanJFB" # m list of point sizes it blocks

            """ if some process creates a custom insertion of a list of nodes
            if self._constrained():
                mesher_flags = "rinYYJBQF"
            else:
                mesher_flags = "rinJBQF"
            """

        return mesher_flags
    #
    def _get_execution_options(self):
        execution_options = KratosMultiphysics.Flags()

        # set for the post_refining process
        if self.MeshingParameters.GetInfoParameters().GetTessellationInsertion():
            print(" TESSELLATION INSERTION ")
            execution_options.Set(
                KratosMeshers.MesherData.INITIALIZE_MESHER_INPUT, True)
            execution_options.Set(
                KratosMeshers.MesherData.TRANSFER_KRATOS_NODES_TO_MESHER, True)
        else:
            execution_options.Set(
                KratosMeshers.MesherData.INITIALIZE_MESHER_INPUT, False)

        if self._constrained():
            execution_options.Set(
                KratosMeshers.MesherData.TRANSFER_KRATOS_FACES_TO_MESHER, True)

        execution_options.Set(
            KratosMeshers.MesherData.FINALIZE_MESHER_INPUT,  True)

        execution_options.Set(
            KratosMeshers.MesherData.SELECT_TESSELLATION_ELEMENTS, True) #can be set to FALSE and work
        execution_options.Set(
            KratosMeshers.MesherData.KEEP_ISOLATED_NODES, False)

        return execution_options

    #
    @classmethod
    def _class_prefix(self):
        header = "::[---Post Refining---]::"
        return header
