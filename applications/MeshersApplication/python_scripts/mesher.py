""" Project: MeshersApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""


# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.MeshersApplication as KratosMeshers


def CreateMesher(main_model_part, meshing_parameters):
    return Mesher(main_model_part, meshing_parameters)


class Mesher(object):

    #
    def __init__(self, main_model_part, meshing_parameters):

        self.echo_level = 1
        self.main_model_part = main_model_part
        self.MeshingParameters = meshing_parameters

        self.model_part = self.main_model_part
        if(self.main_model_part.Name != self.MeshingParameters.GetSubModelPartName()):
            self.model_part = self.main_model_part.GetSubModelPart(
                self.MeshingParameters.GetSubModelPartName())

        print(self._class_prefix()+" Ready")

    #
    def Initialize(self, dimension):

        self.dimension = dimension

        # set mesher
        if self.dimension == 2:
            self.mesher = KratosMeshers.TriangularMesh2DMesher()
        elif self.dimension == 3:
            self.mesher = KratosMeshers.TetrahedralMesh3DMesher()

        self.mesher.SetEchoLevel(self.echo_level)
        self.mesher.SetMeshingParameters(self.MeshingParameters)

        self.SetPreMeshingProcesses()
        self.SetPostMeshingProcesses()

        self.mesher.Initialize()

    #
    def InitializeMeshing(self):

        self.MeshingParameters.SetExecutionOptions(self._get_execution_options())

        self.MeshingParameters.InitializeMeshing()

        self.MeshingParameters.SetTessellationInfo(self._get_mesher_info())
        self.MeshingParameters.SetTessellationFlags(self._get_mesher_flags())

    #
    def SetPreMeshingProcesses(self):
        # List of processes to be executed before meshing
        pass

    #
    def SetPostMeshingProcesses(self):

        # processes to rebuild domain
        processes = []

        # The order set is the order of execution:
        processes.append(KratosMeshers.GenerateNewNodes(self.model_part, self.MeshingParameters, self.echo_level))
        processes.append(KratosMeshers.SelectElements(self.model_part, self.MeshingParameters, self.echo_level))
        processes.append(KratosMeshers.GenerateNewElements(self.model_part, self.MeshingParameters, self.echo_level))
        processes.append(KratosMeshers.GenerateNewConditions(self.model_part, self.MeshingParameters, self.echo_level))

        [self.mesher.SetPostMeshingProcess(process) for process in processes]

    #
    def FinalizeMeshing(self):

         # reset execution options
        execution_options = KratosMultiphysics.Flags()
        self.MeshingParameters.SetExecutionOptions(self._reset_execution_options(execution_options))

        self.MeshingParameters.FinalizeMeshing()

    #
    def ExecuteMeshing(self):

        self.InitializeMeshing()  # set execution flags and mesher flags

        self.mesher.ExecuteMeshing(self.model_part)

        self.FinalizeMeshing()  # set execution flags and mesher flags

    #
    def _constrained(self):
        meshing_options = self.MeshingParameters.GetOptions()
        if meshing_options.Is(KratosMeshers.MesherData.CONSTRAINED):
            return True
        return False
    #
    def _refine(self):
        meshing_options = self.MeshingParameters.GetOptions()
        if meshing_options.Is(KratosMeshers.MesherData.REFINE):
            return True
        return False

    #
    def _get_mesher_info(self):
        return "General mesher"

    #
    def _get_mesher_flags(self):
        # set mesher flags: to set options for the mesher (triangle 2D, tetgen 3D)
        mesher_flags = ""
        print("no mesher flags")
        if self.dimension == 2:
            pass
            # REFINE
            # ADD NODES
            # to add_nodes automatically and refine the mesh ("q"-quality mesh and "a"-area constraint switches)
            # "YYJaqrn" "YJq1.4arn" "Jq1.4arn"
            # refine
            #mesher_flags = "YJq1.4arnQ"
            # refine constrained
            #mesher_flags = "pYJq1.4arnCQ"

            # INSERT NODES
            # to insert a set of given points and refine the mesh
            # "rinYYJQ" "rinYYJQ" "rinJQ" "rinQ"
            # refine
            #mesher_flags = "rinJQ"
            # refine constrained
            #mesher_flags = "rinYYJQ"

            # refine without adding nodes
            #mesher_flags = "YJrnQ"

            # RECONNECT
            # to reconnect a set of points only
            #mesher_flags = "nQP"
            # constrained
            #mesher_flags = "pnBYYQ"

            # BOUNDARY SEARCH
            # to get conectivities, boundaries and neighbours only
            #mesher_flags = "ncEBQ"
        elif self.dimension == 3:
            pass

        return mesher_flags

    #
    def _get_execution_options(self):
        execution_options = KratosMultiphysics.Flags()
        self._reset_execution_options(execution_options)
        return execution_options

    #
    def _reset_execution_options(self, execution_options):
        # all flags
        execution_options.Set(
            KratosMeshers.MesherData.INITIALIZE_MESHER_INPUT, False)
        execution_options.Set(
            KratosMeshers.MesherData.FINALIZE_MESHER_INPUT, False)

        execution_options.Set(
            KratosMeshers.MesherData.TRANSFER_KRATOS_NODES_TO_MESHER, False)
        execution_options.Set(
            KratosMeshers.MesherData.TRANSFER_KRATOS_ELEMENTS_TO_MESHER, False)
        execution_options.Set(
            KratosMeshers.MesherData.TRANSFER_KRATOS_NEIGHBOURS_TO_MESHER, False)
        execution_options.Set(
            KratosMeshers.MesherData.TRANSFER_KRATOS_FACES_TO_MESHER, False)

        execution_options.Set(
            KratosMeshers.MesherData.SELECT_TESSELLATION_ELEMENTS, False)
        execution_options.Set(
            KratosMeshers.MesherData.KEEP_ISOLATED_NODES, False)

        return execution_options

    #
    def SetEchoLevel(self, echo_level):
        self.echo_level = echo_level

    #
    @classmethod
    def _class_prefix(self):
        header = "::[-------Mesher------]::"
        return header
