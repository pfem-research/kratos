""" Project: MeshersApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""


# Built-in/Generic Imports
import time as timer

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.MeshersApplication as KratosMeshers


def Factory(settings, Model):
    if(not isinstance(settings, KratosMultiphysics.Parameters)):
        raise Exception(
            "Expected input shall be a Parameters object, encapsulating a json string")
    return VariablesSmoothingProcess(Model, settings["Parameters"])


class VariablesSmoothingProcess(KratosMultiphysics.Process):
    #
    def __init__(self, Model, custom_settings):

        KratosMultiphysics.Process.__init__(self)

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
            "echo_level"              : 0,
            "model_part_name"         : "Domain",
            "execution_control_type"  : "step",
            "execution_frequency"     : 1.0,
            "execution_before_output" : true,
            "smoothing" : true,
            "smoothing_options": {
               "variables"         : ["DETERMINANT_F"],
               "smoothing_factor"  : 0.25,
               "smoothing_type"    : "spr_threshold",
               "variable_threshold": {
                    "PLASTIC_DISSIPATION":100
               }
             }
        }
        """)
        # variables : ["DETERMINANT_F","TOTAL_DEFORMATION_GRADIENT","INTERNAL_VARIABLES"],
        # smoothing types : default, spr, default_threshold, spr_threshold, specific_threshold, spr_specific_threshold

        Recover = False
        if ( custom_settings.Has("smoothing_options") ):
            if custom_settings["smoothing_options"].Has("variable_threshold"):
                saveMe = custom_settings["smoothing_options"]["variable_threshold"].Clone()
                custom_settings['smoothing_options'].RemoveValue('variable_threshold')
                Recover = True

        # overwrite the default settings with user-provided parameters
        self.settings = custom_settings
        self.settings.RecursivelyValidateAndAssignDefaults(default_settings)
        if ( Recover):
            self.settings["smoothing_options"]["variable_threshold"] = saveMe

        self.counter = 1
        self.echo_level = self.settings["echo_level"].GetInt()
        self.execution_before_output = self.settings["execution_before_output"].GetBool()

        self.model = Model

        # prepend parent model part name
        model_name = self.settings["model_part_name"].GetString()

        self.report = False
        if self.echo_level > 0:
            self.report = True

    #
    def ExecuteInitialize(self):

        self.model_part = self.model[self.settings["model_part_name"].GetString(
        )]

        # find node neighbours
        if(self.model_part.ProcessInfo[KratosMeshers.INITIALIZED_DOMAINS] == False):
            self._SearchNodeNeighbours(self.model_part, self.echo_level)

        self.control = KratosMeshers.ExecutionControl(self.model_part.ProcessInfo, self.settings["execution_control_type"].GetString(), self.settings["execution_frequency"].GetDouble());

        # set variables to smooth
        self.variables, self.factor = self._get_smoothing_variables()
        self.threshold_variable, self.threshold_value = self._get_threshold()

        self.smoothing_active = True if self.variables != None else False
        self.on_threshold = True if self.threshold_variable != None else False

        self.spr_method = True  if "spr" in self.settings["smoothing_options"]["smoothing_type"].GetString() else False
        self.specific_threshold = True  if "specific" in self.settings["smoothing_options"]["smoothing_type"].GetString() else False

        # check specific variable types (other variables must be added)
        if self.on_threshold:
            specific_variables = ["PLASTIC_DISSIPATION","DELTA_PLASTIC_DISSIPATION"]
            if self.threshold_variable.Name() in specific_variables:
                self.specific_threshold = True;
            print(self._class_prefix()+" Specific_threshold active for:", self.threshold_variable.Name())

        print(self._class_prefix()+" (smoothing:"+str(self.smoothing_active)+"|threshold:"+str(self.on_threshold)+"|specific:"+str(self.specific_threshold)+"|spr:"+str(self.spr_method)+")")
        print(self._class_prefix()+" Ready")

    ###

    #
    def ExecuteInitializeSolutionStep(self):
        self.control.Update()

    #
    def ExecuteBeforeOutputStep(self):
        if self.smoothing_active:
            if self.execution_before_output:
                if self.control.IsExecutionStep():
                    self.SmoothVariables()
                    if self.echo_level > 0:
                        print(self._class_prefix() + " [ Smoothing (before output) End ]")

    #
    def ExecuteAfterOutputStep(self):
        if self.smoothing_active:
            if not self.execution_before_output:
                if self.control.IsExecutionStep():
                    self.SmoothVariables()
                    if self.echo_level > 0:
                        print(self._class_prefix() + " [ Smoothing (after output) End ]")

    ###
    #
    def SmoothVariables(self):

        if(self.echo_level >= 0):
            print(self._class_prefix() +
                  " [ Smoothing (call:"+str(self.counter)+") ]", self.model_part.Name)

        clock_time = self._start_time_measuring()

        if self.spr_method:
            if self.on_threshold:
                KratosMeshers.TransferUtilities().SmoothSPRElementalValuesOnThreshold(self.model_part, self.variables, self.threshold_variable, self.threshold_value, self.specific_threshold, self.factor)
            else:
                KratosMeshers.TransferUtilities().SmoothSPRElementalValues(self.model_part, self.variables, self.factor)
        else:
            if self.on_threshold:
                KratosMeshers.TransferUtilities().SmoothElementalValuesOnThreshold(self.model_part, self.variables, self.threshold_variable, self.threshold_value, self.specific_threshold, self.factor)
            else:
                KratosMeshers.TransferUtilities().SmoothElementalValues(self.model_part, self.variables, self.factor)

        self._stop_time_measuring(clock_time, "Variables Smoothing", self.report);

        self.counter += 1

        # schedule next execution
        self.control.SetNextExecution()


    #
    def GetVariables(self):
        nodal_variables = []  # variables smoothing
        # print(self._class_prefix()+" Variables added")
        return nodal_variables

    ###
    #
    def _get_threshold(self):
        threshold_variable = None
        threshold_value = 0.0
        if self.settings["smoothing"].GetBool():
            if "threshold" in self.settings["smoothing_options"]["smoothing_type"].GetString():
                variables = self.settings["smoothing_options"]["variable_threshold"]
                for key, value in variables.items():
                    threshold_variable = KratosMultiphysics.KratosGlobals.GetVariable(key)
                    threshold_value = value.GetDouble()

        return threshold_variable, threshold_value

    #
    def _get_smoothing_variables(self):
        data_variables = None
        smoothing_factor = 1.0
        if self.settings["smoothing"].GetBool():
            data_variables = KratosMeshers.DataVariables()
            smoothing_variables = self.settings["smoothing_options"]["variables"]
            print(" smoothing_variables ", smoothing_variables)
            for variable in smoothing_variables:
                data_variables.SetVariable( KratosMultiphysics.KratosGlobals.GetVariable( variable.GetString() ) )
            smoothing_factor = self.settings["smoothing_options"]["smoothing_factor"].GetDouble()

        return data_variables, smoothing_factor

    #
    @classmethod
    def _SearchNodeNeighbours(self, model_part, echo_level):

        # set search options:
        number_of_avg_elems = 10
        number_of_avg_nodes = 10

        # define search utility
        nodal_neighbour_search = KratosMeshers.NodalNeighboursSearch(
            model_part, echo_level, number_of_avg_elems, number_of_avg_nodes)

        # execute search:
        nodal_neighbour_search.Execute()

        print(self._class_prefix()+" Nodal Search executed ")


    def _start_time_measuring(self):
        # Measure process time
        time_ip = timer.perf_counter() #timer.process_time()
        return time_ip

    def _stop_time_measuring(self, time_ip, process, report):
        # Measure process time
        time_fp = timer.perf_counter() #timer.process_time()
        if report:
            used_time = time_fp - time_ip
            print(self._class_prefix()+" [ %.2f" %
                  round(used_time, 2), "s", process, " ] ")
    #
    @classmethod
    def _class_prefix(self):
        header = "::[-Smoothing_Process-]::"
        return header
