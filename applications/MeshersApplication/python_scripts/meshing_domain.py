""" Project: MeshersApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""


# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.MeshersApplication as KratosMeshers


def CreateDomain(Model, custom_settings):
    if custom_settings.Has("meshing_strategy"):
        custom_settings = TransformSettings(custom_settings)
    return MeshingDomain(Model, custom_settings)

class MeshingDomain(object):

    # constructor. the constructor shall only take care of storing the settings
    # and the pointer to the main_model part.
    ##
    # real construction shall be delayed to the function "Initialize" which
    # will be called once the mesher is already filled
    def __init__(self, Model, custom_settings):

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
            "model_part_name": "model_part_name",
            "process_module": "MeshersApplication.meshing_strategy",
            "Parameters":{
                "remesh": true,
                "meshing_options": {
                   "constrained": true,
                   "mesh_smoothing": true,
                   "variables_smoothing": true,
                   "execution_control_type": "step",
                   "mesh_smoothing_frequency": 1.0,
                   "variables_smoothing_frequency": 1.0,
                   "spr_transfer": false
                },
                "refine": true,
                "refine_options": {
                   "interior": {
                       "refine": "distance_and_threshold",
                       "coarsen": "distance_and_error",
                       "distance_mesh_size": 0.025,
                       "variable_threshold": {
                           "PLASTIC_DISSIPATION":100
                       },
                       "specific_threshold": false,
                       "variable_error": {
                           "PLASTIC_DISSIPATION":2
                       }
                   },
                   "boundary": {
                       "refine": "none",
                       "coarsen": "none",
                       "distance_mesh_size": 0.025,
                       "variable_threshold": {
                           "PLASTIC_DISSIPATION":100
                       },
                       "specific_threshold": false,
                       "variable_error": {
                           "PLASTIC_DISSIPATION":2
                       }
                   },
                   "refine_boxes": false,
                   "refining_boxes": []

                }
            }
        }
        """)

        # overwrite the default settings with user-provided parameters
        self.settings = custom_settings
        self.settings.ValidateAndAssignDefaults(default_settings)
        self.echo_level = 0

        self.model = Model

        self.active_remeshing = False
        if(self.settings["Parameters"]["remesh"].GetBool()):
            self.active_remeshing = True

        #print(" MESHING_DOMAIN ", self.settings.PrettyPrintJsonString())

    ####

    def Initialize(self):

        self.model_part = self.model[self.settings["model_part_name"].GetString()]
        self.dimension = self.model_part.ProcessInfo[KratosMultiphysics.SPACE_DIMENSION]

        if self.Fluid():
            self.settings["process_module"].SetString("PfemApplication.fluid_meshing_strategy")
            KratosMeshers.SetFlagsToEntities(self.model_part,self.echo_level).Execute()

        # Construct the meshing strategy
        if self.Active():
            import importlib
            module_name = self.settings["process_module"].GetString()
            if not "KratosMultiphysics" in module_name:
                module_name = "KratosMultiphysics."+module_name
            meshing_module = importlib.import_module(module_name)
            self.MeshingStrategy = meshing_module.CreateMeshingStrategy(self.model_part, self.settings["Parameters"])

            # Meshing Stratety
            self.MeshingStrategy.SetEchoLevel(self.echo_level)
            self.MeshingStrategy.Initialize()

        # Print Status
        self._print_status()

    ####

    #
    def ExecuteMeshing(self):
        if(self.active_remeshing):
            self.MeshingStrategy.GenerateMesh()

    #
    def Check(self):

        # set the domain labels to mesh mesher
        critical_mesh_size = self.settings["Parameters"]["refine_options"]["interior"]["distance_mesh_size"].GetDouble()

        critical_radius = KratosMeshers.ModelPartUtilities().CalculateSmallerMeshSize(self.model_part)
        print(" Smaller mesh size:", critical_radius,"critical mesh size:",critical_mesh_size)

        zero_normals = False
        for node in self.model_part.Nodes:
            if node.Is(KratosMultiphysics.BOUNDARY) and node.IsNot(KratosMultiphysics.ISOLATED):
                normal = node.GetSolutionStepValue(KratosMultiphysics.NORMAL)
                if  normal[0] == 0.0 and normal[1] == 0.0 and normal[2] == 0.0:
                    print(" Warning: some boundary normals are zero, node ["+str(node.Id)+"] normal:",normal)
                    zero_normals = True
                    break
        if zero_normals:
            print(" Warning: some boundary normals are zero ")
            raise(" CHECK: domains and processes/conditions must be in different ModelParts ")

    #
    def Active(self):
        return self.active_remeshing

    #
    def Fluid(self):
        if self.model_part.Is(KratosMultiphysics.FLUID):
            return True
        else:
            return False
    #
    def SetEchoLevel(self, echo_level):
        self.echo_level = echo_level

    #
    def GetVariables(self):

        nodal_variables = []
        return nodal_variables

    #
    def _print_status(self):
        BodyType = ""
        if(self.model_part.Is(KratosMultiphysics.SOLID)):
            BodyType = "SOLID"
        elif(self.model_part.Is(KratosMultiphysics.FLUID)):
            BodyType = "FLUID"
        elif(self.model_part.Is(KratosMultiphysics.RIGID)):
            BodyType = "RIGID"

        Remesh = "No"
        if(self.active_remeshing):
            Remesh = "Yes"

        print(self._class_prefix()+" " +
              self.settings["model_part_name"].GetString()+" ("+BodyType+") "+Remesh)

    #
    @classmethod
    def _class_prefix(self):
        header = "::[---Meshed_Domain---]::"
        return header



## *************** LEGACY TRANSFORM ***************** ##


def CreateMeshingDomain(Model, custom_settings):
    custom_settings = TransformSettings(custom_settings)
    return MeshingDomain(Model, custom_settings)

def TransformSettings(custom_settings):

    # settings string in json format
    default_settings = KratosMultiphysics.Parameters("""
    {
        "python_module": "meshing_domain",
        "process_module": "MeshersApplication.meshing_domain",
        "model_part_name": "model_part_name",
        "alpha_shape": 2.4,
        "offset_factor": 0.0,
        "meshing_strategy":{
           "python_module": "meshing_strategy",
           "process_module": "MeshersApplication.meshing_strategy",
           "meshing_frequency": 0.0,
           "remesh": false,
           "refine": false,
           "reconnect": false,
           "transfer": false,
           "constrained": false,
           "mesh_smoothing": false,
           "variables_smoothing": false,
           "elemental_variables_to_smooth":[ "DETERMINANT_F" ],
           "reference_element_type": "Element2D3N",
           "reference_condition_type": "CompositeCondition2D2N",
           "additional_smoothing": false,
           "additional_smoothing_variable": [],
           "additional_smoothing_magnitude": 0.01
        },
        "spatial_bounding_box":{
           "upper_point": [0.0, 0.0, 0.0],
           "lower_point": [0.0, 0.0, 0.0],
           "velocity": [0.0, 0.0, 0.0]
        },
        "refining_parameters":{
           "critical_size": 0.0,
           "threshold_variable": "PLASTIC_STRAIN",
           "reference_threshold" : 0.0,
           "error_variable": "NORM_ISOCHORIC_STRESS",
           "reference_error" : 0.0,
           "add_nodes": true,
           "insert_nodes": false,
           "remove_nodes": {
               "apply_removal": false,
               "on_distance": false,
               "on_threshold": false,
               "on_error": false
           },
           "remove_boundary": {
               "apply_removal": false,
               "on_distance": false,
               "on_threshold": false,
               "on_error": false
           },
           "refine_elements": {
               "apply_refinement": false,
               "on_distance": false,
               "on_threshold": false,
               "on_error": false
           },
           "refine_boundary": {
               "apply_refinement": false,
               "on_distance": false,
               "on_threshold": false,
               "on_error": false
           },
           "refining_box":{
               "refine_in_box_only": false,
               "radius": 0.0,
               "center": [0.0, 0.0, 0.0],
               "velocity": [0.0, 0.0, 0.0]
           }
        },
        "elemental_variables_to_transfer":[]
    }
    """)

    if not custom_settings["meshing_strategy"].Has("process_module"):
        module_name = custom_settings["meshing_strategy"]["python_module"].GetString().split(".",1)[1]
        custom_settings["meshing_strategy"].AddEmptyValue("process_module").SetString(module_name)

    # overwrite the default settings with user-provided parameters
    old_settings = custom_settings
    old_settings.ValidateAndAssignDefaults(default_settings)
    old_settings["meshing_strategy"].ValidateAndAssignDefaults(default_settings["meshing_strategy"])
    old_settings["refining_parameters"].ValidateAndAssignDefaults(default_settings["refining_parameters"])

    # transform to new settings with user-provided parameters
    new_settings = KratosMultiphysics.Parameters("""
    {
        "model_part_name": "model_part_name",
        "process_module": "MeshersApplication.meshing_strategy",
        "Parameters":{
            "remesh": true,
            "meshing_options": {
               "constrained": true,
               "mesh_smoothing": true,
               "variables_smoothing":true
            },
            "refine": true,
            "refine_options": {
               "interior": {
                   "refine": "distance_and_threshold",
                   "coarsen": "distance_and_error",
                   "distance_mesh_size": 0.025,
                   "variable_threshold": {
                       "PLASTIC_DISSIPATION":100
                   },
                   "specific_threshold": false,
                   "variable_error": {
                       "PLASTIC_DISSIPATION":2
                   }
               },
               "boundary": {
                   "refine": "none",
                   "coarsen": "none",
                   "distance_mesh_size": 0.025,
                   "variable_threshold": {
                       "PLASTIC_DISSIPATION":100
                   },
                   "specific_threshold": false,
                   "variable_error": {
                       "PLASTIC_DISSIPATION":2
                   }
               },
               "refine_boxes": false,
               "refining_boxes": [{
                  "bounding_box_settings": {
                     "bounding_box_type": "SpatialBoundingBox",
                     "bounding_box_parameters": {
                        "parameters_list": [{
                           "upper_point": [0.0, 0.0, 0.0],
                           "lower_point": [0.0, 0.0, 0.0]
                        }],
                       "velocity" : [0.0, 0.0, 0.0]
                     }
                  },
                  "refine_options": {}
                }
               ]
            }
        }
    }
    """)

    new_settings["process_module"] = old_settings["meshing_strategy"]["process_module"]
    new_settings["model_part_name"] = old_settings["model_part_name"]

    # transform meshing options
    new_settings["Parameters"]["remesh"] = old_settings["meshing_strategy"]["remesh"]
    new_settings["Parameters"]["meshing_options"]["constrained"] =  old_settings["meshing_strategy"]["constrained"]
    new_settings["Parameters"]["meshing_options"]["mesh_smoothing"] =  old_settings["meshing_strategy"]["mesh_smoothing"]

    # transform refine options
    new_settings["Parameters"]["refine"] = old_settings["meshing_strategy"]["refine"]

    new_settings["Parameters"]["refine_options"]["interior"]["refine"].SetString(GetRefineOption(old_settings["refining_parameters"]["refine_elements"],"apply_refinement"))
    new_settings["Parameters"]["refine_options"]["interior"]["coarsen"].SetString(GetRefineOption(old_settings["refining_parameters"]["remove_nodes"],"apply_removal"))

    new_settings["Parameters"]["refine_options"]["interior"]["distance_mesh_size"] = old_settings["refining_parameters"]["critical_size"]
    new_settings["Parameters"]["refine_options"]["interior"].RemoveValue("variable_threshold")
    new_settings["Parameters"]["refine_options"]["interior"].AddEmptyValue("variable_threshold").AddEmptyValue(old_settings["refining_parameters"]["threshold_variable"].GetString()).SetDouble(old_settings["refining_parameters"]["reference_threshold"].GetDouble())
    new_settings["Parameters"]["refine_options"]["interior"].RemoveValue("variable_error")
    new_settings["Parameters"]["refine_options"]["interior"].AddEmptyValue("variable_error").AddEmptyValue(old_settings["refining_parameters"]["error_variable"].GetString()).SetDouble(old_settings["refining_parameters"]["reference_error"].GetDouble())

    new_settings["Parameters"]["refine_options"]["boundary"]["refine"].SetString(GetRefineOption(old_settings["refining_parameters"]["refine_boundary"],"apply_refinement"))
    new_settings["Parameters"]["refine_options"]["boundary"]["coarsen"].SetString(GetRefineOption(old_settings["refining_parameters"]["remove_boundary"],"apply_removal"))

    new_settings["Parameters"]["refine_options"]["boundary"]["distance_mesh_size"] = old_settings["refining_parameters"]["critical_size"]
    new_settings["Parameters"]["refine_options"]["boundary"].RemoveValue("variable_threshold")
    new_settings["Parameters"]["refine_options"]["boundary"].AddEmptyValue("variable_threshold").AddEmptyValue(old_settings["refining_parameters"]["threshold_variable"].GetString()).SetDouble(old_settings["refining_parameters"]["reference_threshold"].GetDouble())
    new_settings["Parameters"]["refine_options"]["boundary"].RemoveValue("variable_error")
    new_settings["Parameters"]["refine_options"]["boundary"].AddEmptyValue("variable_error").AddEmptyValue(old_settings["refining_parameters"]["error_variable"].GetString()).SetDouble(old_settings["refining_parameters"]["reference_error"].GetDouble())


    #print(" NEW MESHING_STRATEGY settings ", new_settings.PrettyPrintJsonString())
    #input("Press Enter to continue...")

    return new_settings

def GetRefineOption(option, applied):
    name = "none"
    if option[applied].GetBool():
        if option["on_distance"].GetBool():
            name = "distance"
            if option["on_threshold"].GetBool():
                if option["on_error"].GetBool():
                    name = "all"
                else:
                    name = "distance_and_threshold"
            else:
                if option["on_error"].GetBool():
                    name = "distance_and_error"
        else:
            if option["on_threshold"].GetBool():
                name = "threshold"
                if option["on_error"].GetBool():
                    name = "threshold_and_error"
            else:
                if option["on_error"].GetBool():
                    name = "error"
    return name
