""" Project: MeshersApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""


# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.MeshersApplication as KratosMeshers


class DomainUtilities(object):

    #
    def __init__(self):
        pass

    #
    def InitializeDomains(self, model_part, echo_level):

        if(model_part.ProcessInfo[KratosMeshers.INITIALIZED_DOMAINS] == False):

            # initialize the mesher
            print(self._class_prefix()+" Initialize", model_part.Name)

            # find node neighbours
            self.SearchNodeNeighbours(model_part, echo_level)

            # find element neighbours
            self.SearchElementNeighbours(model_part, echo_level)

            # set the domain labels to elements and conditions
            entities = ["Elements","Conditions"]
            for entity in entities:
                KratosMeshers.ModelPartUtilities().SetModelPartNameToEntities(model_part, entity)

            # find skin and boundary normals
            if not model_part.ProcessInfo[KratosMultiphysics.IS_RESTARTED]:
                # build boundary of a volumetric body domain
                self.BuildModelPartBoundary(model_part, echo_level)

                # search nodal h
                self.SearchNodalH(model_part, echo_level)

                # add rigid and solid boundary nodes to fluid domains:
                self.AddBoundaryNodesToFluidDomains(model_part)

                # set the domain labels to nodes
                KratosMeshers.ModelPartUtilities().SetModelPartNameToEntities(model_part,"Nodes")
            else:
                skin_build = KratosMeshers.BuildModelPartBoundary(
                    model_part, model_part.Name, echo_level)
                print(self._class_prefix()+" Search Masters Restart ")
                skin_build.ClearConditionMasters()
                skin_build.SearchConditionMasters()

            model_part.ProcessInfo.SetValue(
                KratosMeshers.INITIALIZED_DOMAINS, True)

            if(echo_level > 0):
                print(self._class_prefix()+" Resultant ModelPart")
                print(model_part)

    #
    @classmethod
    def SearchNodeNeighbours(self, model_part, echo_level):

        # set search options:
        number_of_avg_elems = 10
        number_of_avg_nodes = 10

        # define search utility
        nodal_neighbour_search = KratosMeshers.NodalNeighboursSearch(
            model_part, echo_level, number_of_avg_elems, number_of_avg_nodes)

        # execute search:
        nodal_neighbour_search.Execute()

        print(self._class_prefix()+" Nodal Search executed ")

    #
    @classmethod
    def SearchElementNeighbours(self, model_part, echo_level):

        dimension = model_part.ProcessInfo[KratosMultiphysics.SPACE_DIMENSION]
        # set search options:
        number_of_avg_elems = 10

        # define search utility
        elemental_neighbour_search = KratosMeshers.ElementalNeighboursSearch(
            model_part, dimension, echo_level, number_of_avg_elems)

        # execute search:
        elemental_neighbour_search.Execute()

        if(echo_level > 0):
            print(self._class_prefix()+" Elemental Search executed ")

    #
    @classmethod
    def BuildModelPartBoundary(self, model_part, echo_level):

        print(self._class_prefix()+" Build Mesh Boundary ")
        # set building options:

        # define building utility
        skin_build = KratosMeshers.BuildModelPartBoundary(
            model_part, model_part.Name, echo_level)

        # execute building:
        skin_build.Execute()

        print(self._class_prefix()+" Boundary Built ")
        # execute set flags to model part
        set_flags = KratosMeshers.SetFlagsToEntities(
            model_part, echo_level)
        set_flags.Execute()

        print(self._class_prefix()+" Flags set ")

        # calculate boundary normals
        self.ComputeBoundaryNormals(model_part, echo_level)

        # Search condition masters: (check)
        # skin_build.SearchConditionMasters() # done in skin_build during build composite conditions

        # Check wall normals and Repair: (this must be reviewed)
        # as it takes as reference the fluid domain it fails in some walls
        # normals_calculation = KratosMeshers.BoundaryNormals()
        # normals_calculation.CheckWallNormalsAndRepair(model_part, echo_level)

        if(echo_level > 0):
            print(self._class_prefix()+" Mesh Boundary Build executed ")

    ###

    #
    @classmethod
    def SearchNodalH(self, model_part, echo_level):

        # define search utility
        nodal_h_search = KratosMultiphysics.FindNodalHProcess(model_part)
        # execute search:
        nodal_h_search.Execute()

        # check print
        #self.PrintNodalH(model_part)

        if(echo_level > 0):
            print(self._class_prefix()+" Nodal H Search executed ")

    #
    @classmethod
    def PrintNodalH(self, model_part):
        for node in model_part.Nodes:
            nodal_h  = node.GetSolutionStepValue(KratosMultiphysics.NODAL_H);
            print (node.Id," nodal_h:",nodal_h)
    #
    @classmethod
    def ComputeBoundaryNormals(self, model_part, echo_level):

        # define calculation utility
        normals_calculation = KratosMeshers.BoundaryNormals()

        # execute calculation:
        # (scaled normals)
        normals_calculation.CalculateWeightedBoundaryNormals(
            model_part, echo_level)
        # (unit normals)
        # normals_calculation.CalculateUnitBoundaryNormals(model_part, self.echo_level)

        if(echo_level > 0):
            print(self._class_prefix()+" Boundary Normals computed ")

    #
    @classmethod
    def AddBoundaryNodesToFluidDomains(self, model_part):

        exist_fluid_domain = False
        for part in model_part.SubModelParts:
            if part.Is(KratosMultiphysics.FLUID):
                exist_fluid_domain = True
                break

        if(exist_fluid_domain):

            print(self._class_prefix()+" Add boundary nodes to fluid domains ")

            try:
                import KratosMultiphysics.SolidMechanicsApplication as KratosSolid
            except:
                raise Exception(
                    "SolidMechanicsApplication not imported and needed in this operation")

            transfer_flags = [KratosMultiphysics.BOUNDARY,
                              KratosMultiphysics.FLUID.AsFalse()]

            entity_type = "Nodes"
            for fluid_part in model_part.SubModelParts:
                if (fluid_part.IsNot(KratosMultiphysics.ACTIVE) and fluid_part.Is(KratosMultiphysics.FLUID)):
                    for part in model_part.SubModelParts:
                        if part.IsNot(KratosMultiphysics.ACTIVE):
                            if(part.Is(KratosMultiphysics.SOLID) or part.Is(KratosMultiphysics.RIGID)):
                                transfer_process = KratosSolid.TransferEntitiesProcess(
                                    fluid_part, part, entity_type, transfer_flags)
                                transfer_process.Execute()
    #
    @classmethod
    def GetVariables(self):
        nodal_variables = ['NORMAL', 'NODAL_H', 'SHRINK_FACTOR']
        return nodal_variables
    #
    @classmethod
    def _class_prefix(self):
        header = "::[--Domain Utilities-]::"
        return header
