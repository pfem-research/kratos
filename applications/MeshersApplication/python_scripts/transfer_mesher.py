""" Project: MeshersApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""


# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.MeshersApplication as KratosMeshers
import KratosMultiphysics.MeshersApplication.mesher as mesher


def CreateMesher(main_model_part, meshing_parameters):
    return TransferMesher(main_model_part, meshing_parameters)


class TransferMesher(mesher.Mesher):

    #
    def __init__(self, main_model_part, meshing_parameters):

        mesher.Mesher.__init__(self, main_model_part, meshing_parameters)

    #
    def InitializeMeshing(self):

        self.MeshingParameters.InitializeMeshing()

        # set execution flags: to set the options to be executed in methods and processes
        execution_options = KratosMultiphysics.Flags()

        execution_options.Set(
            KratosMeshers.MesherData.INITIALIZE_MESHER_INPUT, True)
        execution_options.Set(
            KratosMeshers.MesherData.FINALIZE_MESHER_INPUT, True)

        execution_options.Set(
            KratosMeshers.MesherData.TRANSFER_KRATOS_NODES_TO_MESHER, True)
        execution_options.Set(
            KratosMeshers.MesherData.TRANSFER_KRATOS_ELEMENTS_TO_MESHER, True)
        execution_options.Set(
            KratosMeshers.MesherData.TRANSFER_KRATOS_NEIGHBOURS_TO_MESHER, True)

        execution_options.Set(
            KratosMeshers.MesherData.SELECT_TESSELLATION_ELEMENTS, True)
        execution_options.Set(
            KratosMeshers.MesherData.KEEP_ISOLATED_NODES, False)

        self.MeshingParameters.SetExecutionOptions(execution_options)

    #

    def SetPreMeshingProcesses(self):
        # nothing to do: only transfer
        pass

   #
    def SetPostMeshingProcesses(self):

        # processes to rebuild domain
        processes = []

        # The order set is the order of execution:
        processes.append(KratosMeshers.GenerateNewElements(self.model_part, self.MeshingParameters, self.echo_level))
        processes.append(KratosMeshers.GenerateNewConditions(self.model_part, self.MeshingParameters, self.echo_level))

        [self.mesher.SetPostMeshingProcess(process) for process in processes]

    #
    @classmethod
    def _class_prefix(self):
        header = "::[--Transfer Mesher--]::"
        return header
