""" Project: MeshersApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""


# Built-in/Generic Imports
import time as timer

# Kratos Imports5
import KratosMultiphysics
import KratosMultiphysics.MeshersApplication as KratosMeshers


def Factory(settings, Model):
    if(not isinstance(settings, KratosMultiphysics.Parameters)):
        raise Exception(
            "Expected input shall be a Parameters object, encapsulating a json string")
    return RemeshDomainsProcess(Model, settings["Parameters"])


class RemeshDomainsProcess(KratosMultiphysics.Process):
    #
    def __init__(self, Model, custom_settings):

        KratosMultiphysics.Process.__init__(self)

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
            "echo_level"              : 1,
            "model_part_name"         : "Meshing Domain",
            "execution_control_type"  : "step",
            "execution_frequency"     : 1.0,
            "execution_before_output" : true,
            "domains"       : []
        }
        """)

        # legacy transform
        custom_settings = TransformSettings(custom_settings)

        # overwrite the default settings with user-provided parameters
        self.settings = custom_settings
        self.settings.ValidateAndAssignDefaults(default_settings)

        self.counter = 1;
        self.echo_level = self.settings["echo_level"].GetInt()
        self.execution_before_output = self.settings["execution_before_output"].GetBool()

        self.model = Model

        # prepend parent model part name
        model_name = self.settings["model_part_name"].GetString()
        counter = 1
        for domain in self.settings["domains"].values():
            parameters = domain
            if parameters.Has("model_part_name"):
                if not model_name in parameters["model_part_name"].GetString():
                    name = model_name+"."+parameters["model_part_name"].GetString()
                    parameters["model_part_name"].SetString(name)
                    print("model part name ",name)
            else:
                name = model_name+".contact_domain"+str(counter)
                parameters.AddEmptyValue("model_part_name").SetString(name)
                counter+=1

        # construct meshing domains
        self._ConstructDomains()

        # meshing fluid domains
        self.fluid_domains = False

        # print meshing time
        self.report = False
        if self.echo_level >= 0:
            self.report = True

        # set execution control
        self._set_default_executions()

        # create smoothing process
        self.smoothing_processes = self._create_smoothing_processes()
    #
    def ExecuteInitialize(self):

        self.main_model_part = self.model[self.settings["model_part_name"].GetString(
        )]
        self.dimension = self.main_model_part.ProcessInfo[KratosMultiphysics.SPACE_DIMENSION]

        self.control = KratosMeshers.ExecutionControl(self.main_model_part.ProcessInfo, self.settings["execution_control_type"].GetString(), self.settings["execution_frequency"].GetDouble());

        # mesh mesher initial values
        self.remesh_domains_active = False
        for domain in self.domains:
            if domain.Active():
                self.remesh_domains_active = True

        # initialize all meshing domains
        self._InitializeDomains()

        if self.fluid_domains and self.remesh_domains_active:
            self.counter = 0
            self.RemeshDomains()

        for process in self.smoothing_processes:
            process.ExecuteInitialize()

        print(self._class_prefix()+" Ready")

    ###
    #
    def ExecuteInitializeSolutionStep(self):
        self.control.Update()
        for process in self.smoothing_processes:
            process.ExecuteInitializeSolutionStep()

    #
    def ExecuteBeforeOutputStep(self):
        if self.remesh_domains_active:
            if self.execution_before_output:
                if self.control.IsExecutionStep():
                    self.RemeshDomains()
                    if self.echo_level >= 0:
                        print(self._class_prefix() + " [ Mesh Generation (before output) End ]")
        for process in self.smoothing_processes:
            process.ExecuteBeforeOutputStep()
    #
    def ExecuteAfterOutputStep(self):
        if self.remesh_domains_active:
            if not self.execution_before_output:
                if self.control.IsExecutionStep():
                    self.RemeshDomains()
                    if self.echo_level >= 0:
                        print(self._class_prefix() + " [ Mesh Generation (after output) End ]")
        for process in self.smoothing_processes:
            process.ExecuteAfterOutputStep()
    ###

    #
    def RemeshDomains(self):

        if(self.echo_level >= 0):
            print(self._class_prefix() +
                  " [ Mesh Generation (call:"+str(self.counter)+") ]")

        #num_threads = KratosMultiphysics.ParallelUtilities.GetNumThreads()
        #KratosMultiphysics.ParallelUtilities.SetNumThreads(1)
        #print(self._class_prefix()+" [ Meshing threads:"+str(KratosMultiphysics.ParallelUtilities.GetNumThreads())+" ]")

        self.model_manager = self._GetModelManager()

        clock_time = self._start_time_measuring()
        self.model_manager.ExecuteInitialize()
        self._stop_time_measuring(clock_time, "Model Mesh Initialize", self.report);

        clock_time = self._start_time_measuring()
        # serial
        for domain in self.domains:
            domain.ExecuteMeshing()
        self._stop_time_measuring(clock_time, "Domains meshing Finalized", self.report);

        if(self.echo_level > 1):
            print("")
            print(self.main_model_part)

        clock_time = self._start_time_measuring()
        self.model_manager.ExecuteFinalize()
        self._stop_time_measuring(clock_time, "Model Mesh Finalize", self.report);

        #KratosMultiphysics.ParallelUtilities.SetNumThreads(num_threads)

        self.counter += 1

        # set meshing step time
        self._SetMeshingStepTime()

        # schedule next meshing
        self.control.SetNextExecution()


    #
    def GetVariables(self):
        import KratosMultiphysics.MeshersApplication.domain_utilities as domain_utilities
        nodal_variables = domain_utilities.DomainUtilities().GetVariables()
        nodal_variables = nodal_variables + ['MEAN_ERROR']  # removing nodes
        nodal_variables = nodal_variables + ['CONTACT_FORCE']  # removing nodes

        for domain in self.domains:
            nodal_variables = nodal_variables + domain.GetVariables()

        # print(self._class_prefix()+" Variables added")

        return nodal_variables

    ###
    #
    def _set_default_executions(self):
        for domain in self.settings["domains"].values():
            if not domain["Parameters"]["meshing_options"].Has("execution_control_type"):
                domain["Parameters"]["meshing_options"].AddEmptyValue("execution_control_type").SetString(self.settings["execution_control_type"].GetString())
            if not domain["Parameters"]["meshing_options"].Has("mesh_smoothing_frequency"):
                domain["Parameters"]["meshing_options"].AddEmptyValue("mesh_smoothing_frequency").SetDouble(self.settings["execution_frequency"].GetDouble())
            if not domain["Parameters"]["meshing_options"].Has("variables_smoothing_frequency"):
                domain["Parameters"]["meshing_options"].AddEmptyValue("variables_smoothing_frequency").SetDouble(self.settings["execution_frequency"].GetDouble())
    #
    def _create_smoothing_processes(self):
        import KratosMultiphysics.MeshersApplication.variables_smoothing_process as smoothing_process
        smoothing_processes = []
        for domain in self.settings["domains"]:
            smoothing = None
            if domain.Has("meshing_strategy"):
                smoothing = domain["meshing_strategy"]["variables_smoothing"].GetBool()
            else:
                smoothing = domain["Parameters"]["meshing_options"]["variables_smoothing"].GetBool()

            default_settings = KratosMultiphysics.Parameters("""
            {
                "model_part_name": "model_part_name",
                "execution_control_type"  : "step",
                "execution_frequency"     : 1.0
            }
            """)
            default_settings["model_part_name"].SetString(domain["model_part_name"].GetString())
            if domain["Parameters"]["meshing_options"].Has("execution_control_type"):
                default_settings["execution_control_type"].SetString(domain["Parameters"]["meshing_options"]["execution_control_type"].GetString())
            else:
               default_settings["execution_control_type"].SetString(self.settings["execution_control_type"].GetString())

            if domain["Parameters"]["meshing_options"].Has("execution_frequency"):
               default_settings["execution_frequency"].SetDouble(domain["Parameters"]["meshing_options"]["execution_frequency"].GetDouble())
            else:
               default_settings["execution_frequency"].SetDouble(self.settings["execution_frequency"].GetDouble())

            if smoothing:
                smoothing_processes.append(smoothing_process.VariablesSmoothingProcess(self.model, default_settings))
        return smoothing_processes
    #
    def _ConstructDomains(self):
        self.domains = []
        for i in range(self.settings["domains"].size()):
            self.domains.append(self._domains_module().CreateDomain(self.model,self.settings["domains"][i]))
    #
    def _GetModelManager(self):
        execution_options = KratosMultiphysics.Flags()
        if self.fluid_domains:
            execution_options.Set(KratosMeshers.MesherData.KEEP_ISOLATED_NODES, True)
        return KratosMeshers.ModelStructure(self.main_model_part, execution_options, self.echo_level)

    #
    def _SetMeshingStepTime(self):
        time = self.main_model_part.ProcessInfo[KratosMultiphysics.TIME]
        self.main_model_part.ProcessInfo.SetValue(
            KratosMeshers.MESHING_STEP_TIME, time)

    #
    def _InitializeDomains(self):
        self._initialize_domains()

        for domain in self.domains:
            if domain.Active():
                domain.SetEchoLevel(self.echo_level)
                domain.Initialize()
                domain.Check()
                if domain.Fluid():
                    self.fluid_domains = True
    #
    def _domains_module(self):
        import importlib
        module_name = "KratosMultiphysics.MeshersApplication.meshing_domain"
        domain_module = importlib.import_module(module_name)
        return domain_module

    #
    def _initialize_domains(self):
        if(self.main_model_part.ProcessInfo[KratosMeshers.INITIALIZED_DOMAINS] == False):
            print(self._class_prefix()+" Initialize Domains")
            import KratosMultiphysics.MeshersApplication.domain_utilities as domain_utilities
            domain_utils = domain_utilities.DomainUtilities()
            domain_utils.InitializeDomains(self.main_model_part, self.echo_level)

    def _start_time_measuring(self):
        # Measure process time
        time_ip = timer.perf_counter() #timer.process_time()
        return time_ip

    def _stop_time_measuring(self, time_ip, process, report):
        # Measure process time
        time_fp = timer.perf_counter() #timer.process_time()
        if report:
            used_time = time_fp - time_ip
            print(self._class_prefix()+" [ %.2f" %
                  round(used_time, 2), "s", process, " ] ")

    def _is_not_restarted(self):
        process_info = self.main_model_part.ProcessInfo
        if process_info[KratosMultiphysics.IS_RESTARTED]:
            return False
        else:
            return True
    #
    @classmethod
    def _class_prefix(self):
        header = "::[--Meshing_Process--]::"
        return header


## *************** LEGACY TRANSFORM ***************** ##
def TransformSettings(custom_settings):
    if custom_settings.Has("meshing_control_type"):
        custom_settings.AddEmptyValue("execution_control_type").SetString(custom_settings["meshing_control_type"].GetString())
        custom_settings.RemoveValue("meshing_control_type")
    if custom_settings.Has("meshing_frequency"):
        custom_settings.AddEmptyValue("execution_frequency").SetDouble(custom_settings["meshing_frequency"].GetDouble())
        custom_settings.RemoveValue("meshing_frequency")
    if custom_settings.Has("meshing_before_output"):
        custom_settings.AddEmptyValue("execution_before_output").SetBool(custom_settings["meshing_before_output"].GetBool())
        custom_settings.RemoveValue("meshing_before_output")
    if custom_settings.Has("meshing_domains"):
        custom_settings.AddValue("domains",custom_settings["meshing_domains"])
        custom_settings.RemoveValue("meshing_domains")

    return custom_settings
