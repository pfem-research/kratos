""" Project: MeshersApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""


# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.MeshersApplication as KratosMeshers
import KratosMultiphysics.MeshersApplication.mesher as mesher


def CreateMesher(main_model_part, meshing_parameters):
    return PreRefiningMesher(main_model_part, meshing_parameters)


class PreRefiningMesher(mesher.Mesher):
    #
    def SetPreMeshingProcesses(self):

        # processes to refine elements /refine boundary
        processes = []

        # The order set is the order of execution:
        processes.append(KratosMeshers.RefineElementsOnThreshold(self.model_part, self.MeshingParameters, self.echo_level))
        processes.append(KratosMeshers.RefineElementsInEdges(self.model_part, self.MeshingParameters, self.echo_level))
        processes.append(KratosMeshers.RefineConditions(self.model_part, self.MeshingParameters, self.echo_level))

        processes.append(KratosMeshers.CoarsenElements(self.model_part, self.MeshingParameters, self.echo_level))
        processes.append(KratosMeshers.CoarsenConditions(self.model_part, self.MeshingParameters, self.echo_level))

        [self.mesher.SetPreMeshingProcess(process) for process in processes]

    #
    def SetPostMeshingProcesses(self):

        # processes to rebuild domain
        processes = []

        # The order set is the order of execution:
        processes.append(KratosMeshers.GenerateNewNodes(self.model_part, self.MeshingParameters, self.echo_level)) if self._constrained() else None
        processes.append(KratosMeshers.SelectElements(self.model_part, self.MeshingParameters, self.echo_level))
        processes.append(KratosMeshers.RefineElementsOnSize(self.model_part, self.MeshingParameters, self.echo_level)) if self._refine() else None

        [self.mesher.SetPostMeshingProcess(process) for process in processes]


    #
    def FinalizeMeshing(self):
        # no finalize post refining process
        pass

    #
    def _get_mesher_info(self):
        return "Prepare domain for refinement"

    #
    def _get_mesher_flags(self):
        # set mesher flags: to set options for the mesher (triangle 2D, tetgen 3D)
        # RECONNECT
        mesher_flags = ""
        if self.dimension == 2:
            if self._constrained():
                mesher_flags = "pnBYYQ"
            else:
                mesher_flags = "nQP"
        elif self.dimension == 3:
            if self._constrained():
                mesher_flags = "pnMYJBFO4/4V"
                #mesher_flags = "R1/2T1e-12lb0pnMYJBFO4/4V"  #l incremental flip  #b0 no sort #R1/2 remove 2% elements
            else:
                mesher_flags = "nJFMQO4/4"

        return mesher_flags

    #
    def _get_execution_options(self):
        execution_options = KratosMultiphysics.Flags()

        execution_options.Set(
            KratosMeshers.MesherData.INITIALIZE_MESHER_INPUT, True)
        execution_options.Set(
            KratosMeshers.MesherData.FINALIZE_MESHER_INPUT,  False)

        execution_options.Set(
            KratosMeshers.MesherData.TRANSFER_KRATOS_NODES_TO_MESHER, True)
        execution_options.Set(
            KratosMeshers.MesherData.TRANSFER_KRATOS_ELEMENTS_TO_MESHER, False)
        execution_options.Set(
            KratosMeshers.MesherData.TRANSFER_KRATOS_NEIGHBOURS_TO_MESHER, False)

        if self._constrained():
            execution_options.Set(
                KratosMeshers.MesherData.TRANSFER_KRATOS_FACES_TO_MESHER, True)

        execution_options.Set(
            KratosMeshers.MesherData.SELECT_TESSELLATION_ELEMENTS, True)

        execution_options.Set(
            KratosMeshers.MesherData.KEEP_ISOLATED_NODES, False)

        return execution_options

    #
    @classmethod
    def _class_prefix(self):
        header = "::[----Pre Refining---]::"
        return header
