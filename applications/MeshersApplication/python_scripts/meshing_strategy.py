""" Project: MeshersApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""


# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.MeshersApplication as KratosMeshers


def CreateMeshingStrategy(model_part, custom_settings):
    return MeshingStrategy(model_part, custom_settings)

class MeshingStrategy(object):

    #
    def __init__(self, model_part, custom_settings):

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
                "remesh": true,
                "meshing_options": {
                   "alpha_shape": 0.0,
                   "offset_factor": 0.0,
                   "constrained": true,
                   "mesh_smoothing": true,
                   "variables_smoothing":true,
                   "execution_control_type": "step",
                   "mesh_smoothing_frequency": 1.0,
                   "variables_smoothing_frequency": 1.0,
                   "spr_transfer": false
                },
                "refine": true,
                "refine_options": {
                   "interior": {
                       "refine": "distance_and_threshold",
                       "coarsen": "distance_and_error",
                       "distance_mesh_size": 0.025,
                       "variable_threshold": {
                           "PLASTIC_DISSIPATION":100
                       },
                       "specific_threshold": false,
                       "variable_error": {
                           "PLASTIC_DISSIPATION":2
                       }
                   },
                   "boundary": {
                       "refine": "none",
                       "coarsen": "none",
                       "distance_mesh_size": 0.025,
                       "variable_threshold": {
                           "PLASTIC_DISSIPATION":100
                       },
                       "specific_threshold": false,
                       "variable_error": {
                           "PLASTIC_DISSIPATION":2
                       }
                   },
                   "refine_boxes": false,
                   "refining_boxes": [{
                      "bounding_box_settings": {
                         "bounding_box_type": "SpatialBoundingBox",
                         "bounding_box_parameters": {
                            "parameters_list": [{
                               "upper_point": [0.0, 0.0, 0.0],
                               "lower_point": [0.0, 0.0, 0.0]
                            }],
                           "velocity" : [0.0, 0.0, 0.0]
                         }
                      },
                      "refine_options": {}
                    }
                   ]
                },
                "contact": false,
                "contact_options":{},
                "reconnect": true
        }
        """)

        # overwrite the default settings with user-provided parameters
        self.settings = custom_settings
        self.settings.ValidateAndAssignDefaults(default_settings)
        self.settings["meshing_options"].RecursivelyValidateAndAssignDefaults(default_settings["meshing_options"])
        self.settings["refine_options"].ValidateAndAssignDefaults(default_settings["refine_options"])
        self.settings["refine_options"]["interior"].ValidateAndAssignDefaults(default_settings["refine_options"]["interior"])
        self.settings["refine_options"]["boundary"].ValidateAndAssignDefaults(default_settings["refine_options"]["boundary"])

        #print(" MESHING_STRATEGY ", self.settings.PrettyPrintJsonString())

        self.echo_level = 0
        self.model_part = model_part

    #
    def Initialize(self):
        self.dimension = self.model_part.ProcessInfo[KratosMultiphysics.SPACE_DIMENSION]
        # set meshing parameters
        self.MeshingParameters = self._get_meshing_parameters()

        # set smoothing control
        self.SmoothingControl = KratosMeshers.ExecutionControl(self.model_part.ProcessInfo, self.settings["meshing_options"]["execution_control_type"].GetString(), self.settings["meshing_options"]["mesh_smoothing_frequency"].GetDouble());
        self.MeshingParameters.SetSmoothingControl(self.SmoothingControl)

        # mesh meshers for the current strategy
        self.meshers = self._get_meshers()

        # initialize meshers
        for mesher in self.meshers:
            mesher.SetEchoLevel(self.echo_level)
            mesher.Initialize(self.dimension)

        print(self._class_prefix()+" Ready")

    #
    def SetEchoLevel(self, echo_level):
        self.echo_level = echo_level

    #
    def InitializeMeshGeneration(self):
        self.MeshingParameters.GetInfoParameters().Initialize(self.model_part)
        self._set_mesh_info()

    #
    def GenerateMesh(self):
        self.InitializeMeshGeneration()

        for mesher in self.meshers:
            mesher.ExecuteMeshing()
            #self.PrintNodalH("Mesher")

        self.FinalizeMeshGeneration()

    #
    def PrintNodalH(self, text):
        for node in self.model_part.Nodes:
            nodal_h  = node.GetSolutionStepValue(KratosMultiphysics.NODAL_H)
            print(text+" nodal_h:",nodal_h)
    #
    def FinalizeMeshGeneration(self):
        self._set_mesh_info()

    #### Meshing strategy internal methods ####

    #
    def _get_domain_size_parameters(self):

        alpha_shape = {"2":2.0, "3":2.0}

        element_size, boundary_size = self._calculate_mesh_sizes()

        print(self._class_prefix()+" Global mesh sizes: (interior:" + f"{element_size:2.6f}" + "/boundary:" + f"{boundary_size:2.6f}"+")")

        element_volume = self._calculate_mean_volume(element_size)

        domain_size = {"alpha_shape":alpha_shape[str(self.dimension)], "element_size":element_size, "boundary_size":boundary_size, "element_volume": element_volume}

        return domain_size

    #
    def _calculate_mesh_sizes(self):
        interior_mesh_size = self.settings["refine_options"]["interior"]["distance_mesh_size"].GetDouble()
        if interior_mesh_size <= 0:
            number_of_nodes = 0
            mean_nodal_size = 0
            for node in self.model_part.Nodes: #slow
                if (node.IsNot(KratosMultiphysics.RIGID)):
                    number_of_nodes += 1
                    mean_nodal_size = mean_nodal_size + node.GetSolutionStepValue(KratosMultiphysics.NODAL_H)

            interior_mesh_size = 0.25 * mean_nodal_size / number_of_nodes

        boundary_mesh_size = self.settings["refine_options"]["boundary"]["distance_mesh_size"].GetDouble()
        if boundary_mesh_size <= 0:
            boundary_mesh_size = interior_mesh_size * 3

        return interior_mesh_size, boundary_mesh_size

    #
    def _calculate_mean_volume(self, distance_mesh_size):
        # set mean area or mean volume
        domain_volume = KratosMeshers.ModelPartUtilities().CalculateModelPartVolume(self.model_part)

        number_of_elements = self.model_part.NumberOfElements()
        dimension = self.model_part.ProcessInfo[KratosMultiphysics.SPACE_DIMENSION]

        factor = float(number_of_elements*(dimension+1))
        mean_volume = (4.0*distance_mesh_size)**dimension
        if(factor != 0):
            mean_volume = domain_volume/factor

        return mean_volume

    #
    def _get_meshing_parameters(self):

        # Create MeshingParameters
        parameters = KratosMeshers.MeshingParameters()
        parameters.Initialize()

        # set scale factor
        parameters.SetScaleFactor(1.0)

        # set model part name
        parameters.SetSubModelPartName(self.model_part.Name)

        if self.settings["remesh"].GetBool():
            domain_sizes = self._get_domain_size_parameters()
            alpha_shape = self.settings["meshing_options"]["alpha_shape"].GetDouble()
            if alpha_shape == 0.0:
                alpha_shape = domain_sizes["alpha_shape"]
            parameters.SetAlphaParameter(alpha_shape)
            offset_factor = self.settings["meshing_options"]["offset_factor"].GetDouble()
            if offset_factor == 0.0:
                offset_factor = domain_sizes["boundary_size"]/4.0
            parameters.SetOffsetFactor(offset_factor)

            print(self._class_prefix()+" Global mesh factors: (alpha:" + f"{alpha_shape:2.6f}" + "/offset:" + f"{offset_factor:2.6f}"+")")

            self.settings["refine_options"]["interior"]["distance_mesh_size"].SetDouble(domain_sizes["element_size"])
            self.settings["refine_options"]["boundary"]["distance_mesh_size"].SetDouble(domain_sizes["boundary_size"])

            parameters.SetInfoParameters(self._get_info_parameters())
            parameters.SetTransferVariables(self._get_transfer_variables())

        if self.settings["refine"].GetBool():
            self._set_refining_parameters(parameters)

        # set meshing options
        parameters.SetOptions(self._get_meshing_options())

        # set reference elements and condition
        if self.dimension == 2:
            parameters.SetReferenceElement("Element2D3N")
            parameters.SetReferenceCondition("CompositeCondition2D2N")
        elif self.dimension == 3:
            parameters.SetReferenceElement("Element3D4N")
            parameters.SetReferenceCondition("CompositeCondition3D3N")

        return parameters

    #
    def _get_info_parameters(self):
        parameters = KratosMeshers.MeshingInfoParameters()
        parameters.Initialize(self.model_part)
        return parameters

    #
    def _set_refining_parameters(self, parameters):

        self.RefineData = KratosMeshers.RefineData(self.settings["refine_options"]["interior"],self.settings["refine_options"]["boundary"])
        parameters.SetRefineData(self.RefineData)

        self._set_refining_boxes(parameters)

        return parameters
    #
    def _set_refining_boxes(self, parameters):

        if self.settings["refine_options"]["refine_boxes"].GetBool():
            boxes = self.settings["refine_boxes"]
            for box in boxes:
                refine_data = KratosMeshers.RefineData(box["refine_options"]["interior"],box["refine_options"]["boundary"])

                module_name = "KratosMultiphysics.MeshersApplication"
                if box.Has("process_module"):
                    module_name = "KratosMultiphysics."+process["process_module"].GetString()
                import importlib
                box_module = importlib.import_module(module_name)

                refine_data.SetRefineBox(box_module.Factory(box["bounding_box_settings"]))

                parameters.SetRefineDataItem(refine_data)

    #
    def _get_meshing_options(self):

        meshing_options = KratosMultiphysics.Flags()

        # mesher options
        meshing_options.Set(KratosMeshers.MesherData.REMESH, self.settings["remesh"].GetBool())
        meshing_options.Set(KratosMeshers.MesherData.REFINE, self.settings["refine"].GetBool())
        meshing_options.Set(KratosMeshers.MesherData.CONTACT_SEARCH, self.settings["contact"].GetBool())

        # meshing options
        meshing_options.Set(KratosMeshers.MesherData.CONSTRAINED, self.settings["meshing_options"]["constrained"].GetBool())
        meshing_options.Set(KratosMeshers.MesherData.MESH_SMOOTHING, self.settings["meshing_options"]["mesh_smoothing"].GetBool())
        meshing_options.Set(KratosMeshers.MesherData.SPR_TRANSFER, self.settings["meshing_options"]["spr_transfer"].GetBool())

        return meshing_options

    #
    def _get_meshers(self):

        meshers = []

        if(self.echo_level > 0):
            print(self._class_prefix()+" ["+self.MeshingParameters.GetSubModelPartName()+" model part ] (REMESH:", self.settings["remesh"].GetBool(
            ), "/ REFINE:", self.settings["refine"].GetBool(), ")")
        import importlib
        for mesher in self._get_meshers_list():
            meshing_module = importlib.import_module(mesher)
            new_mesher = meshing_module.CreateMesher(self.model_part, self.MeshingParameters)
            meshers.append(new_mesher)

        return meshers

    #
    def _get_meshers_list(self):

        meshers_list = []

        if(self.settings["remesh"].GetBool() and self.settings["refine"].GetBool()):
            meshers_list.append(
                "KratosMultiphysics.MeshersApplication.pre_refining_mesher")
            meshers_list.append(
                "KratosMultiphysics.MeshersApplication.post_refining_mesher")

        elif(self.settings["remesh"].GetBool()):
            meshers_list.append(
                "KratosMultiphysics.MeshersApplication.reconnect_mesher")

        else:
            meshers_list.append(
                "KratosMultiphysics.MeshersApplication.transfer_mesher")

        return meshers_list

    #
    def _set_mesh_info(self):

        info_parameters = self.MeshingParameters.GetInfoParameters()

        current_nodes = self.model_part.NumberOfNodes()
        current_elements = self.model_part.NumberOfElements()
        current_conditions = self.model_part.NumberOfConditions()

        previous_nodes = info_parameters.GetNumberOfNodes()
        previous_elements = info_parameters.GetNumberOfElements()
        previous_conditions = info_parameters.GetNumberOfConditions()

        nodes = current_nodes-previous_nodes
        info_parameters.SetNumberOfNewNodes(nodes if nodes > 0 else 0)

        elements = current_elements-previous_elements
        info_parameters.SetNumberOfNewElements(elements if elements > 0 else 0)

        conditions = current_conditions-previous_conditions
        info_parameters.SetNumberOfNewConditions(conditions if conditions > 0 else 0)

        info_parameters.SetNumberOfNodes(current_nodes)
        info_parameters.SetNumberOfElements(current_elements)
        info_parameters.SetNumberOfConditions(current_conditions)

    #
    def _get_transfer_variables(self):
        data_variables = KratosMeshers.DataVariables()
        transfer_variables = ["DETERMINANT_F","INTERNAL_VARIABLES","INTERNAL_THERMAL_VARIABLES","INTERNAL_STRENGTH_VECTOR","INTERNAL_PLASTIC_STRAIN_VECTOR","TOTAL_DEFORMATION_GRADIENT"]

        for variable in transfer_variables:
            data_variables.SetVariable( KratosMultiphysics.KratosGlobals.GetVariable(variable) )

        return data_variables

    #
    @classmethod
    def _class_prefix(self):
        header = "::[--Meshing Strategy-]::"
        return header
