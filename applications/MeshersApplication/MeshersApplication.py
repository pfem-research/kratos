""" Project: MeshersApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Kratos Imports
from KratosMultiphysics import _ImportApplication

from KratosMeshersApplication import *
application = KratosMeshersApplication()
application_name = "KratosMeshersApplication"
application_folder = "MeshersApplication"

_ImportApplication(application, application_name)
