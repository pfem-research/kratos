//--------------------------------------------------------
//        __  __         _                               .
// KRATOS|  \/  |___ ___| |_  ___ _ _ ___                .
//       | |\/| / -_|_-<| ' \/ -_| '_|_-<                .
//       |_|  |_\___|__/|_||_\___|_| /__/ APPLICATION    .
//                                                       .
//  License:(BSD)   MeshersApplication/license.txt       .
//  Main authors:   Josep Maria Carbonell                .
//                        ..                             .
//--------------------------------------------------------
//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

#if !defined(KRATOS_MESHERS_APPLICATION_VARIABLES_H_INCLUDED)
#define KRATOS_MESHERS_APPLICATION_VARIABLES_H_INCLUDED

// System includes

// External includes

// Project includes
#include "includes/variables.h"
#include "includes/kratos_flags.h"
#include "containers/pointer_vector_set.h"
#include "includes/element.h"
#include "includes/condition.h"
#include "includes/indexed_object.h"
#include "includes/global_pointer_variables.h"

namespace Kratos
{
///@name Type Definitions
///@{
typedef PointerVectorSet<Condition, IndexedObject> ConditionContainerType;

typedef Node<3>::WeakPointer NodeWeakPtrType;
typedef Element::WeakPointer ElementWeakPtrType;
typedef Condition::WeakPointer ConditionWeakPtrType;

typedef GlobalPointersVector<Node<3>> NodeWeakPtrVectorType;
typedef GlobalPointersVector<Element> ElementWeakPtrVectorType;
typedef GlobalPointersVector<Condition> ConditionWeakPtrVectorType;
///@}

///@name Kratos Globals
///@{

//Define Variables

//nodal dofs
KRATOS_DEFINE_3D_APPLICATION_VARIABLE_WITH_COMPONENTS(MESHERS_APPLICATION, OFFSET)
KRATOS_DEFINE_APPLICATION_VARIABLE(MESHERS_APPLICATION, double, SHRINK_FACTOR)

//domain definition
KRATOS_DEFINE_APPLICATION_VARIABLE(MESHERS_APPLICATION, bool, IS_AXISYMMETRIC)
KRATOS_DEFINE_APPLICATION_VARIABLE(MESHERS_APPLICATION, bool, INITIALIZED_DOMAINS)
KRATOS_DEFINE_APPLICATION_VARIABLE(MESHERS_APPLICATION, double, MESHING_STEP_TIME)
KRATOS_DEFINE_APPLICATION_VARIABLE(MESHERS_APPLICATION, double, RESTART_STEP_TIME)
KRATOS_DEFINE_APPLICATION_VARIABLE(MESHERS_APPLICATION, std::string, MODEL_PART_NAME)
KRATOS_DEFINE_APPLICATION_VARIABLE(MESHERS_APPLICATION, std::vector<std::string>, MODEL_PART_NAMES)

//boundary definition
KRATOS_DEFINE_APPLICATION_VARIABLE(MESHERS_APPLICATION, int, RIGID_WALL)

//custom neighbor and masters
//KRATOS_DEFINE_APPLICATION_VARIABLE(MESHERS_APPLICATION, NodeWeakPtrType, MAIN_NODE)
//KRATOS_DEFINE_APPLICATION_VARIABLE(MESHERS_APPLICATION, ElementWeakPtrType, MAIN_ELEMENT)
//KRATOS_DEFINE_APPLICATION_VARIABLE(MESHERS_APPLICATION, ConditionWeakPtrType, MAIN_CONDITION)

KRATOS_DEFINE_APPLICATION_VARIABLE(MESHERS_APPLICATION, NodeWeakPtrVectorType, PRIMARY_NODES)
KRATOS_DEFINE_APPLICATION_VARIABLE(MESHERS_APPLICATION, ElementWeakPtrVectorType, PRIMARY_ELEMENTS)
KRATOS_DEFINE_APPLICATION_VARIABLE(MESHERS_APPLICATION, ConditionWeakPtrVectorType, PRIMARY_CONDITIONS)
KRATOS_DEFINE_APPLICATION_VARIABLE(MESHERS_APPLICATION, ConditionWeakPtrVectorType, SECONDARY_CONDITIONS)

//condition variables
KRATOS_DEFINE_APPLICATION_VARIABLE(MESHERS_APPLICATION, ConditionContainerType, CHILDREN_CONDITIONS)

//mesher criteria
KRATOS_DEFINE_APPLICATION_VARIABLE(MESHERS_APPLICATION, double, MEAN_ERROR)

//ALE Variables
KRATOS_DEFINE_3D_APPLICATION_VARIABLE_WITH_COMPONENTS(MESHERS_APPLICATION, MESH_DISPLACEMENT)
KRATOS_DEFINE_3D_APPLICATION_VARIABLE_WITH_COMPONENTS(MESHERS_APPLICATION, MESH_ACCELERATION)

///@}

} // namespace Kratos

#endif /* KRATOS_MESHERS_APPLICATION_VARIABLES_H_INCLUDED */
