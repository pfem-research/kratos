//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:         July 2020 $
//
//

#if !defined(KRATOS_REFINE_DATA_HPP_INCLUDED)
#define KRATOS_REFINE_DATA_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_bounding/spatial_bounding_box.hpp"

#include "meshers_application_variables.h"

namespace Kratos
{
  ///@addtogroup MeshersApplication
  ///@{

  ///@name Kratos Globals
  ///@{

  ///@}
  ///@name Type Definitions
  ///@{

  ///@}
  ///@name  Enum's
  ///@{

  ///@}
  ///@name  Functions
  ///@{

  ///@}
  ///@name Kratos Classes
  ///@{

  /// Short class definition.
  /** Detail class definition.
   */
  class KRATOS_API(MESHERS_APPLICATION) RefineData
  {
  public:
    ///@name Type Definitions
    ///@{

    //refine options
    KRATOS_DEFINE_LOCAL_FLAG(REFINE);
    KRATOS_DEFINE_LOCAL_FLAG(REFINE_ON_DISTANCE);
    KRATOS_DEFINE_LOCAL_FLAG(REFINE_ON_ERROR);
    KRATOS_DEFINE_LOCAL_FLAG(REFINE_ON_THRESHOLD);

    //coarsen options
    KRATOS_DEFINE_LOCAL_FLAG(COARSEN);
    KRATOS_DEFINE_LOCAL_FLAG(COARSEN_ON_DISTANCE);
    KRATOS_DEFINE_LOCAL_FLAG(COARSEN_ON_ERROR);
    KRATOS_DEFINE_LOCAL_FLAG(COARSEN_ON_THRESHOLD);

    struct RefineInfo
    {
      //number of entities refined/removed due to criterion:
      unsigned int on_distance;
      unsigned int on_threshold;
      unsigned int on_error;

      unsigned int in_contact;
      unsigned int in_concave_boundary;
      unsigned int in_convex_boundary;

      void Initialize()
      {
        on_distance = 0;
        on_threshold = 0;
        on_error = 0;
        in_contact = 0;
        in_concave_boundary = 0;
        in_convex_boundary = 0;
      }

      void EchoStats()
      {
        std::cout << "   Refine Info   [on_distance:" << on_distance << ", on_threshold:" << on_threshold << ", on_error:" << on_error << "]" << std::endl;
        std::cout << "                 [in_contact:" << in_contact << ", in_concave_boundary:" << in_concave_boundary << ", in_convex_boundary:" << in_convex_boundary << "]" << std::endl;
      }
    };


    struct RefineParameters
    {

      KRATOS_CLASS_POINTER_DEFINITION(RefineParameters);

    private:


    public:
      //Default Constructor
      RefineParameters() {}

      //Constructor
      RefineParameters(Parameters rParameters)
      {

        KRATOS_TRY

          Parameters default_parameters(R"(
            {
               "refine": "distance_and_threshold",
               "coarsen": "distance_and_error",
               "distance_mesh_size": 0.025,
               "variable_threshold": {
                   "PLASTIC_DISSIPATION":100
               },
               "specific_threshold": false,
               "variable_error": {
                   "PLASTIC_DISSIPATION":2
               }
            }  )");

        // Validate against defaults -- this ensures no type mismatch
        rParameters.ValidateAndAssignDefaults(default_parameters);

        const Parameters variable_threshold = rParameters["variable_threshold"];
        for (auto variable = variable_threshold.begin(); variable != variable_threshold.end(); ++variable)
        {
            std::string variable_name = variable.name();
            ThresholdVariables.push_back(std::make_pair(&KratosComponents<Variable<double>>().Get(variable_name),variable_threshold[variable_name].GetDouble()));
        }

        const Parameters variable_error = rParameters["variable_error"];
        for (auto variable = variable_error.begin(); variable != variable_error.end(); ++variable)
        {
            std::string variable_name = variable.name();
            ErrorVariables.push_back(std::make_pair(&KratosComponents<Variable<double>>().Get(variable_name),variable_error[variable_name].GetDouble()));
        }

        DistanceMeshSize = rParameters["distance_mesh_size"].GetDouble(); //initialize

        SpecificThreshold = rParameters["specific_threshold"].GetBool();

        // Set options
        Options.Set(REFINE,(rParameters["refine"].GetString().find("none") != std::string::npos) ? false : true);
        Options.Set(REFINE_ON_DISTANCE,(rParameters["refine"].GetString().find("distance") != std::string::npos) ? true : false);
        Options.Set(REFINE_ON_ERROR,(rParameters["refine"].GetString().find("error") != std::string::npos) ? true : false);
        Options.Set(REFINE_ON_THRESHOLD,(rParameters["refine"].GetString().find("threshold") != std::string::npos) ? true : false);

        Options.Set(COARSEN,(rParameters["coarsen"].GetString().find("none") != std::string::npos) ? false : true);
        Options.Set(COARSEN_ON_DISTANCE,(rParameters["coarsen"].GetString().find("distance") != std::string::npos) ? true : false);
        Options.Set(COARSEN_ON_ERROR,(rParameters["coarsen"].GetString().find("error") != std::string::npos) ? true : false);
        Options.Set(COARSEN_ON_THRESHOLD,(rParameters["coarsen"].GetString().find("threshold") != std::string::npos) ? true : false);

        KRATOS_CATCH("")
      }


      // variables and values
      std::vector<std::pair<const Variable<double>*, double> > ThresholdVariables;
      std::vector<std::pair<const Variable<double>*, double> > ErrorVariables;

      //configuration refining options
      Flags Options;

      //smaller interior distance mesh size
      double DistanceMeshSize;

      //specific threshold (i.e. Area*Variable)
      bool SpecificThreshold;

      // info parameters
      RefineInfo Info;

    };

    /// Pointer definition of RefineData
    KRATOS_CLASS_POINTER_DEFINITION(RefineData);

    ///@}
    ///@name Public member Variables
    ///@{

    RefineParameters Interior;
    RefineParameters Boundary;

    bool RefineBoxSetFlag;
    SpatialBoundingBox::Pointer RefineBox;

    ///@}
    ///@name Life Cycle
    ///@{

    /// Default constructor.
    RefineData() {}

    /// Constructor.
    RefineData(Parameters InteriorParameters, Parameters BoundaryParameters)
    {
      KRATOS_TRY

      Interior = RefineParameters(InteriorParameters);
      Boundary = RefineParameters(BoundaryParameters);

      // Spatial box must be supplied to be considered
      RefineBoxSetFlag = false;

      KRATOS_CATCH("")
    }

    /// Copy constructor.
    RefineData(RefineData const &rOther):Interior(rOther.Interior),Boundary(rOther.Boundary){}

    /// Clone.
    RefineData::Pointer Clone() const
    {
      return Kratos::make_shared<RefineData>(*this);
    }

    /// Destructor.
    virtual ~RefineData() {}

    ///@}
    ///@name Operators
    ///@{

    ///@}
    ///@name Operations
    ///@{
    void SetRefineBox(SpatialBoundingBox::Pointer pRefiningBox)
    {
      RefineBoxSetFlag = true;
      RefineBox = pRefiningBox;
    }

    ///@}
    ///@name Access
    ///@{

    ///@}
    ///@name Inquiry
    ///@{

    ///@}
    ///@name Input and output
    ///@{

    /// Turn back information as a string.
    virtual std::string Info() const
    {
      std::stringstream buffer;
      buffer << "RefineData";
      return buffer.str();
    }

    /// Print information about this object.
    virtual void PrintInfo(std::ostream &rOStream) const
    {
      rOStream << "RefineData";
    }

    /// Print object's data.
    virtual void PrintData(std::ostream &rOStream) const
    {
      rOStream << "RefineData Data";
    }

    ///@}
    ///@name Friends
    ///@{

    ///@}

  protected:
    ///@name Protected static Member Variables
    ///@{

    ///@}
    ///@name Protected member Variables
    ///@{

    ///@}
    ///@name Protected Operators
    ///@{

    ///@}
    ///@name Protected Operations
    ///@{

    ///@}
    ///@name Protected  Access
    ///@{

    ///@}
    ///@name Protected Inquiry
    ///@{

    ///@}
    ///@name Protected LifeCycle
    ///@{

    ///@}

  private:
    ///@name Static Member Variables
    ///@{

    ///@}
    ///@name Member Variables
    ///@{

    ///@}
    ///@name Private Operators
    ///@{

    /// Assignment operator.
    RefineData &operator=(RefineData const &rOther);

    ///@}
    ///@name Private Operations
    ///@{

    ///@}
    ///@name Private  Access
    ///@{

    ///@}
    ///@name Private Inquiry
    ///@{

    ///@}
    ///@name Un accessible methods
    ///@{

    ///@}

  }; // Class RefineData

  ///@}

  ///@name Type Definitions
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// input stream function
  inline std::istream &operator>>(std::istream &rIStream,
                                  RefineData &rThis);

  /// output stream function
  inline std::ostream &operator<<(std::ostream &rOStream,
                                  const RefineData &rThis)
  {
    rThis.PrintInfo(rOStream);
    rOStream << " : " << std::endl;
    rThis.PrintData(rOStream);

    return rOStream;
  }
  ///@}

  ///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_REFINE_DATA_HPP_INCLUDED  defined
