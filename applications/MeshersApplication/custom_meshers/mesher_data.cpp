//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        March 2019 $
//
//

// System includes

// External includes

// Project includes
#include "custom_meshers/mesher_data.hpp"
#include "custom_utilities/mesher_utilities.hpp"

namespace Kratos
{

//meshing options (configuration)
KRATOS_CREATE_LOCAL_FLAG(MesherData, REMESH, 0);
KRATOS_CREATE_LOCAL_FLAG(MesherData, REFINE, 1);
KRATOS_CREATE_LOCAL_FLAG(MesherData, RECONNECT, 2);
KRATOS_CREATE_LOCAL_FLAG(MesherData, CONSTRAINED, 3);
KRATOS_CREATE_LOCAL_FLAG(MesherData, CONTACT_SEARCH, 4);
KRATOS_CREATE_LOCAL_FLAG(MesherData, MESH_SMOOTHING, 5);
KRATOS_CREATE_LOCAL_FLAG(MesherData, SPR_TRANSFER, 6);

//execution options (select)
KRATOS_CREATE_LOCAL_FLAG(MesherData, INITIALIZE_MESHER_INPUT, 0);
KRATOS_CREATE_LOCAL_FLAG(MesherData, FINALIZE_MESHER_INPUT, 1);

KRATOS_CREATE_LOCAL_FLAG(MesherData, TRANSFER_KRATOS_NODES_TO_MESHER, 2);
KRATOS_CREATE_LOCAL_FLAG(MesherData, TRANSFER_KRATOS_ELEMENTS_TO_MESHER, 3);
KRATOS_CREATE_LOCAL_FLAG(MesherData, TRANSFER_KRATOS_NEIGHBOURS_TO_MESHER, 4);
KRATOS_CREATE_LOCAL_FLAG(MesherData, TRANSFER_KRATOS_FACES_TO_MESHER, 5);

KRATOS_CREATE_LOCAL_FLAG(MesherData, SELECT_TESSELLATION_ELEMENTS, 6);
KRATOS_CREATE_LOCAL_FLAG(MesherData, KEEP_ISOLATED_NODES, 7);

//*******************************************************************************************
//*******************************************************************************************

void MesherData::SetNodes(ModelPart &rModelPart,
                          MeshingParameters &rMeshingVariables)
{
  KRATOS_TRY

  //std::cout<<" Set Nodes "<<std::endl;

  const unsigned int dimension = rModelPart.ElementsBegin()->GetGeometry().WorkingSpaceDimension();

  //*********************************************************************
  //input mesh: NODES
  MesherData::MeshContainer &InMesh = rMeshingVariables.InMesh;

  InMesh.CreatePointList(rModelPart.Nodes().size(), dimension);

  double *PointList = InMesh.GetPointList();
  int &NumberOfPoints = InMesh.GetNumberOfPoints();

  if (!rMeshingVariables.InputInitializedFlag)
  {
    rMeshingVariables.NodeMaxId = ModelPartUtilities::GetMaxNodeId(rModelPart);
    if ((int)rMeshingVariables.NodalPreIds.size() != rMeshingVariables.NodeMaxId + 1)
      rMeshingVariables.NodalPreIds.resize(rMeshingVariables.NodeMaxId + 1);

    std::fill(rMeshingVariables.NodalPreIds.begin(), rMeshingVariables.NodalPreIds.end(), 0);
  }

  //writing the points coordinates in a vector and reordening the Id's
  ModelPart::NodesContainerType::iterator nodes_begin = rModelPart.NodesBegin();

  int base = 0;
  int direct = 1;

  for (int i = 0; i < NumberOfPoints; ++i)
  {
    //from now on it is consecutive
    if (!rMeshingVariables.InputInitializedFlag)
    {
      //std::cout<<" Set Node id "<<(nodes_begin + i)->Id()<<" "<<direct<<std::endl;
      rMeshingVariables.NodalPreIds[direct] = (nodes_begin + i)->Id();
      (nodes_begin + i)->SetId(direct++);
      // if (rMeshingVariables.NodalPreIds[direct] > rMeshingVariables.NodeMaxId)
      //   rMeshingVariables.NodeMaxId = rMeshingVariables.NodalPreIds[direct];
    }

    const array_1d<double, 3> &Coordinates = (nodes_begin + i)->Coordinates();

    if (rMeshingVariables.Options.Is(MesherData::CONSTRAINED))
    {

      if ((nodes_begin + i)->Is(BOUNDARY))
      {

        array_1d<double, 3> &Normal = (nodes_begin + i)->FastGetSolutionStepValue(NORMAL); //BOUNDARY_NORMAL must be set as nodal variable
        double Shrink = (nodes_begin + i)->FastGetSolutionStepValue(SHRINK_FACTOR);        //SHRINK_FACTOR   must be set as nodal variable

        array_1d<double, 3> Offset;

        Normal /= norm_2(Normal);
        for (unsigned int j = 0; j < dimension; ++j)
        {
          Offset[j] = ((-1) * Normal[j] * Shrink * rMeshingVariables.OffsetFactor * 0.25);
        }

        for (unsigned int j = 0; j < dimension; ++j)
        {
          PointList[base + j] = (Coordinates[j] + Offset[j]) * rMeshingVariables.Scale;
        }

        // std::cout<<" B Node ["<<(nodes_begin + i)->Id()<<"] "<<Coordinates<<" "<<Offset<<" "<<Normal<<" "<<Shrink<<std::endl;
      }
      else
      {
        for (unsigned int j = 0; j < dimension; ++j)
        {
          PointList[base + j] = Coordinates[j] * rMeshingVariables.Scale;
        }

        // std::cout<<" Node ["<<(nodes_begin + i)->Id()<<"] "<<Coordinates<<std::endl;
      }
    }
    else
    {
      for (unsigned int j = 0; j < dimension; ++j)
      {
        PointList[base + j] = Coordinates[j] * rMeshingVariables.Scale;
      }
    }

    base += dimension;
  }

  //InMesh.SetPointList(PointList);

  KRATOS_CATCH("")
}

//*******************************************************************************************
//*******************************************************************************************

void MesherData::SetElements(ModelPart &rModelPart,
                             MeshingParameters &rMeshingVariables)
{
  KRATOS_TRY

  std::cout<<" Set elements "<<std::endl;
  //*********************************************************************
  //input mesh: ELEMENTS
  ModelPart::ElementsContainerType::iterator element_begin = rModelPart.ElementsBegin();

  const unsigned int nds = element_begin->GetGeometry().size();

  MesherData::MeshContainer &InMesh = rMeshingVariables.InMesh;

  InMesh.CreateElementList(rModelPart.Elements().size(), nds);

  int *ElementList = InMesh.GetElementList();
  int &NumberOfElements = InMesh.GetNumberOfElements();

  int base = 0;
  for (unsigned int el = 0; el < (unsigned int)NumberOfElements; ++el)
  {
    Geometry<Node<3>> &geom = (element_begin + el)->GetGeometry();

    for (unsigned int i = 0; i < nds; ++i)
    {
      ElementList[base + i] = geom[i].Id();
    }
    base += nds;
  }

  KRATOS_CATCH("")
}


//*******************************************************************************************
//*******************************************************************************************

RefineData &MesherData::GetRefineData(Condition&rCondition,
                                      MeshingParameters &rMeshingVariables,
                                      const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  for (auto &refine : rMeshingVariables.RefineVector)
  {
    if (refine->RefineBoxSetFlag == true)
    {
      if (MesherUtilities::CheckConditionInBox(rCondition, *refine->RefineBox, rCurrentProcessInfo))
      {
        return *refine;
      }
    }
  }

  return *rMeshingVariables.Refine;

  KRATOS_CATCH("")
}

RefineData &MesherData::GetRefineData(Element &rElement,
                                      MeshingParameters &rMeshingVariables,
                                      const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  for (auto &refine : rMeshingVariables.RefineVector)
  {
    if (refine->RefineBoxSetFlag == true)
    {
      if (MesherUtilities::CheckElementInBox(rElement, *refine->RefineBox, rCurrentProcessInfo))
      {
        return *refine;
      }
    }
  }

  return *rMeshingVariables.Refine;

  KRATOS_CATCH("")
}

RefineData &MesherData::GetRefineData(Geometry<Node<3>> &rGeometry,
                                      MeshingParameters &rMeshingVariables,
                                      const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  for (auto &refine : rMeshingVariables.RefineVector)
  {
    if (refine->RefineBoxSetFlag == true)
    {
      if (MesherUtilities::CheckVerticesInBox(rGeometry, *refine->RefineBox, rCurrentProcessInfo))
      {
        return *refine;
      }
    }
  }

  return *rMeshingVariables.Refine;

  KRATOS_CATCH("")
}

RefineData &MesherData::GetRefineData(Node<3> &rNode,
                                      MeshingParameters &rMeshingVariables,
                                      const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  for (auto &refine : rMeshingVariables.RefineVector)
  {
    if (refine->RefineBoxSetFlag == true)
    {
      if (refine->RefineBox->IsInside(rNode, rCurrentProcessInfo[TIME]))
      {
        return *refine;
      }
    }
  }

  return *rMeshingVariables.Refine;

  KRATOS_CATCH("")
}

} // namespace Kratos.
