//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        March 2019 $
//
//

#if !defined(KRATOS_MESHER_DATA_HPP_INCLUDED)
#define KRATOS_MESHER_DATA_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_meshers/refine_data.hpp"
#include "custom_utilities/transfer_utilities.hpp"
#include "custom_utilities/execution_control_utilities.hpp"

#include "meshers_application_variables.h"

namespace Kratos
{
///@addtogroup MeshersApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
class KRATOS_API(MESHERS_APPLICATION) MesherData
{
public:
      ///@name Type Definitions
      ///@{
      typedef ModelPart::PropertiesType PropertiesType;
      typedef TransferUtilities::DataVariables DataVariablesType;

      //meshing options (configuration)
      KRATOS_DEFINE_LOCAL_FLAG(REMESH);
      KRATOS_DEFINE_LOCAL_FLAG(REFINE);
      KRATOS_DEFINE_LOCAL_FLAG(RECONNECT);
      KRATOS_DEFINE_LOCAL_FLAG(CONSTRAINED);
      KRATOS_DEFINE_LOCAL_FLAG(CONTACT_SEARCH);
      KRATOS_DEFINE_LOCAL_FLAG(MESH_SMOOTHING);
      KRATOS_DEFINE_LOCAL_FLAG(SPR_TRANSFER);

      //execution options (select)
      KRATOS_DEFINE_LOCAL_FLAG(INITIALIZE_MESHER_INPUT);
      KRATOS_DEFINE_LOCAL_FLAG(FINALIZE_MESHER_INPUT);

      KRATOS_DEFINE_LOCAL_FLAG(TRANSFER_KRATOS_NODES_TO_MESHER);
      KRATOS_DEFINE_LOCAL_FLAG(TRANSFER_KRATOS_ELEMENTS_TO_MESHER);
      KRATOS_DEFINE_LOCAL_FLAG(TRANSFER_KRATOS_NEIGHBOURS_TO_MESHER);
      KRATOS_DEFINE_LOCAL_FLAG(TRANSFER_KRATOS_FACES_TO_MESHER);

      KRATOS_DEFINE_LOCAL_FLAG(SELECT_TESSELLATION_ELEMENTS);
      KRATOS_DEFINE_LOCAL_FLAG(KEEP_ISOLATED_NODES);


      struct MeshContainer
      {

      KRATOS_CLASS_POINTER_DEFINITION(MeshContainer);

      protected:
            double *mpPointList;
            int *mpElementList;

            double *mpElementSizeList;
            int *mpElementNeighbourList;

            int mNumberOfPoints;
            int mNumberOfElements;

      public:
            //flags to set when the pointers are created (true) or deleted (false)
            bool PointListFlag;
            bool ElementListFlag;
            bool ElementSizeListFlag;
            bool ElementNeighbourListFlag;

            void SetPointList(double *&rPointList) { mpPointList = rPointList; }
            void SetElementList(int *&rElementList) { mpElementList = rElementList; };
            void SetElementSizeList(double *&rElementSizeList) { mpElementSizeList = rElementSizeList; };
            void SetElementNeighbourList(int *&rElementNeighbourList) { mpElementNeighbourList = rElementNeighbourList; };
            void SetNumberOfPoints(int &rNumberOfPoints) { mNumberOfPoints = rNumberOfPoints; };
            void SetNumberOfElements(int &rNumberOfElements) { mNumberOfElements = rNumberOfElements; };

            double *GetPointList() { return mpPointList; };
            int *GetElementList() { return mpElementList; };
            double *GetElementSizeList() { return mpElementSizeList; };
            int *GetElementNeighbourList() { return mpElementNeighbourList; };

            int &GetNumberOfPoints() { return mNumberOfPoints; };
            int &GetNumberOfElements() { return mNumberOfElements; };

            void CreatePointList(const unsigned int NumberOfPoints, const unsigned int Dimension)
            {
                  if (mpPointList)
                  {
                        delete[] mpPointList;
                  }
                  mNumberOfPoints = NumberOfPoints;
                  mpPointList = new double[NumberOfPoints * Dimension];
                  PointListFlag = true;
            }

            void CreateElementList(const unsigned int NumberOfElements, const unsigned int NumberOfVertices)
            {
                  if (mpElementList)
                  {
                        delete[] mpElementList;
                  }
                  mNumberOfElements = NumberOfElements;
                  mpElementList = new int[NumberOfElements * NumberOfVertices];
                  ElementListFlag = true;
            }

            void CreateElementSizeList(const unsigned int NumberOfElements)
            {
                  if (mpElementSizeList)
                  {
                        delete[] mpElementSizeList;
                  }
                  mpElementSizeList = new double[NumberOfElements];
                  ElementSizeListFlag = true;
            }

            void CreateElementNeighbourList(const unsigned int NumberOfElements, const unsigned int NumberOfFaces)
            {
                  if (mpElementNeighbourList)
                  {
                        delete[] mpElementNeighbourList;
                  }
                  mpElementNeighbourList = new int[NumberOfElements * NumberOfFaces];
                  ElementNeighbourListFlag = true;
            }

            void Initialize()
            {
                  mpPointList = (double *)NULL;
                  mpElementList = (int *)NULL;
                  mpElementSizeList = (double *)NULL;
                  mpElementNeighbourList = (int *)NULL;
                  mNumberOfPoints = 0;
                  mNumberOfElements = 0;

                  PointListFlag = false;
                  ElementListFlag = false;
                  ElementSizeListFlag = false;
                  ElementNeighbourListFlag = false;
            }

            void Finalize()
            {
                  if (mpPointList && PointListFlag)
                  {
                        delete[] mpPointList;
                  }

                  if (mpElementList && ElementListFlag)
                  {
                        delete[] mpElementList;
                  }

                  if (mpElementSizeList && ElementSizeListFlag)
                  {
                        delete[] mpElementSizeList;
                  }

                  if (mpElementNeighbourList && ElementNeighbourListFlag)
                  {
                        delete[] mpElementNeighbourList;
                  }

                  Initialize();
            }
      };

      struct NumberEntities
      {
        unsigned int Elements;
        unsigned int Nodes;
        unsigned int Conditions;
        unsigned int BoundaryNodes;

        void clear()
        {
          Elements = 0;
          Nodes = 0;
          Conditions = 0;
          BoundaryNodes = 0;
        }
      };

      struct MeshingInfoParameters
      {
        KRATOS_CLASS_POINTER_DEFINITION(MeshingInfoParameters);

      public:
        //initial
        NumberEntities Initial;
        //current (result after remeshing)
        NumberEntities Current;
        //inserted by a process
        NumberEntities Inserted;
        //removed by a process
        NumberEntities Removed;

        //smoothing check
        bool ExceededThreshold;

        //tessellation insertion
        bool TessellationInsertion;

        void Clear()
        {
          Initial.clear();
          Current.clear();
          Inserted.clear();
          Removed.clear();

          ExceededThreshold = false;
          TessellationInsertion = false;
        }

        void Initialize(ModelPart &rModelPart)
        {
          Initial.Elements = rModelPart.NumberOfElements();
          Initial.Nodes = rModelPart.NumberOfNodes();
          Initial.Elements = rModelPart.NumberOfElements();
          Initial.Conditions = rModelPart.NumberOfConditions();
          Current.clear();
          Inserted.clear();
          Removed.clear();
          ExceededThreshold = false;
          TessellationInsertion = false;
        }

        double GetNumberOfNodes() {return Current.Nodes;}
        double GetNumberOfElements() {return Current.Elements;}
        double GetNumberOfConditions() {return Current.Conditions;}

        void SetNumberOfNodes(const double &Nodes) {Current.Nodes = Nodes;}
        void SetNumberOfElements(const double &Elements) {Current.Elements = Elements;}
        void SetNumberOfConditions(const double &Conditions) {Current.Conditions = Conditions;}

        void SetNumberOfNewNodes(const double &Nodes) {Inserted.Nodes = Nodes;}
        void SetNumberOfNewElements(const double &Elements) {Inserted.Elements = Elements;}
        void SetNumberOfNewConditions(const double &Conditions) {Inserted.Conditions = Conditions;}

        void SetTessellationInsertion(bool inserted) { TessellationInsertion = inserted;}

        bool CheckGeometricalSmoothing()
        {
          if (Inserted.Nodes > Initial.Nodes * 0.001 || Removed.Nodes > Initial.Nodes * 0.001)
            return true;
          else if ((Inserted.Nodes + Removed.Nodes) > Initial.Nodes * 0.002)
            return true;

          return false;
        }

        bool CheckMechanicalSmoothing()
        {
          if (ExceededThreshold || Current.Elements-Initial.Elements != 0)
            return true;
          return false;
        }

        bool GetTessellationInsertion()
        {
          return TessellationInsertion;
        }

        int GetInsertedNodes()
        {
          return Inserted.Nodes;
        }
      };


      struct MeshingParameters
      {

            KRATOS_CLASS_POINTER_DEFINITION(MeshingParameters);

      protected:
            //Pointer variables
            const Element *mpReferenceElement;
            const Condition *mpReferenceCondition;

	    //Laplacian smoothing control
            ExecutionControl *SmoothingControl;

      public:
            //SubModelPart Name
            std::string SubModelPartName;

            //General configuration flags
            Flags Options;

            //Local execution flags
            Flags ExecutionOptions; //configuration meshing options

            //General configuration variables
            double AlphaParameter;
            double OffsetFactor;
            double Scale;

            //Options for the mesher
            std::string TessellationFlags;
            std::string TessellationInfo;

            //Local execution variablesg
            bool InputInitializedFlag;
            bool MeshElementsSelectedFlag;

            unsigned int ConditionMaxId;
            unsigned int ElementMaxId;
            unsigned int NodeMaxId;

            std::vector<int> NodalPreIds;
            std::vector<int> PreservedElements;

            std::vector<BoundedVector<double, 3>> Holes;

            //Mesher pointers to the mesh structures
            MeshContainer InMesh;
            MeshContainer OutMesh;

            std::vector<std::vector<int>> NeighbourList;

            //Global Meshing info
            MeshingInfoParameters::Pointer Info;

            //Global Refining parameters
            RefineData::Pointer Refine;

            //Local Refining parameters defined by boxes
            std::vector<RefineData::Pointer> RefineVector;

            //Global Tranfer parameters
            DataVariablesType::Pointer Transfer;

            //Properties
            PropertiesType::Pointer Properties;

            //Global Meshing box
            bool MeshingBoxSetFlag;
            SpatialBoundingBox::Pointer MeshingBox;


            void Set(Flags ThisFlag)
            {
                  Options.Set(ThisFlag);
            }

            void Reset(Flags ThisFlag)
            {
                  Options.Reset(ThisFlag);
            }

            void SetOptions(const Flags &rOptions)
            {
                  Options = rOptions;
            }

            void SetSubModelPartName(std::string const &rSubModelPartName)
            {
                  SubModelPartName = rSubModelPartName;
            }

            void SetExecutionOptions(const Flags &rOptions)
            {
                  ExecutionOptions = rOptions;
            }

            void SetSmoothingControl(ExecutionControl &rControl)
            {
                  SmoothingControl = &rControl;
            }

            void SetTessellationFlags(std::string rFlags)
            {
                  TessellationFlags = rFlags;
            }

            void SetTessellationInfo(std::string rInfo)
            {
                  TessellationInfo = rInfo;
            }

            void SetAlphaParameter(const double rAlpha)
            {
                  AlphaParameter = rAlpha;
            }

            void SetOffsetFactor(const double rOffsetFactor)
            {
                  OffsetFactor = rOffsetFactor;
            }

            void SetScaleFactor(const double rScaleFactor)
            {
                  Scale = rScaleFactor;
            }

            void SetInfoParameters(MeshingInfoParameters::Pointer rInfo)
            {
                  Info = rInfo;
            }

            void SetRefineData(RefineData::Pointer rRefine)
            {
                  Refine = rRefine;
            }

            void SetRefineDataVector(std::vector<RefineData::Pointer> &rRefineVector)
            {
                  RefineVector = rRefineVector;
            }

            void SetRefineDataItem(RefineData::Pointer rRefine)
            {
                  RefineVector.push_back(rRefine);
            }

            void SetProperties(PropertiesType::Pointer rProperties)
            {
                  Properties = rProperties;
            }

            void SetMeshingBox(SpatialBoundingBox::Pointer pMeshingBox)
            {
                  MeshingBoxSetFlag = true;
                  MeshingBox = pMeshingBox;
            }

            void SetTransferVariables(DataVariablesType::Pointer rTransferVariables)
            {
                 Transfer = rTransferVariables;
            }

            void SetReferenceElement(const Element &rElement)
            {
                  mpReferenceElement = &rElement;
            };

            void SetReferenceCondition(const Condition &rCondition)
            {
                  mpReferenceCondition = &rCondition;
            };

            void SetHoles(std::vector<BoundedVector<double, 3>> &rHoles)
            {
                  Holes = rHoles;
            }

            std::vector<BoundedVector<double, 3>> &GetHoles()
            {
                  return Holes;
            }

            std::string GetSubModelPartName()
            {
                  return SubModelPartName;
            }

            Flags GetOptions()
            {
                  return Options;
            }

            MeshingInfoParameters::Pointer GetInfoParameters()
            {
                  return Info;
            }

            RefineData::Pointer GetRefineData()
            {
                  return Refine;
            }

	    ExecutionControl& GetSmoothingControl()
            {
	          return *SmoothingControl;
            }

            std::vector<RefineData::Pointer> GetRefineDataVector()
            {
                  return RefineVector;
            }

            PropertiesType::Pointer GetProperties()
            {
                  return Properties;
            }

            DataVariablesType::Pointer GetTransferVariables()
            {
                  return Transfer;
            }

            Element const &GetReferenceElement()
            {
                  return *mpReferenceElement;
            }

            Condition const &GetReferenceCondition()
            {
                  return *mpReferenceCondition;
            }

            void Initialize()
            {

                  InputInitializedFlag = false;

                  AlphaParameter = 0;

                  OffsetFactor = 0;

                  Scale = 1;

                  MeshingBoxSetFlag = false;

                  MeshElementsSelectedFlag = false;

                  ConditionMaxId = 0;
                  ElementMaxId = 0;
                  NodeMaxId = 0;

                  InMesh.Initialize();
                  OutMesh.Initialize();

            }

            bool IsInterior(Flags Flag)
            {
                  if (Refine->Interior.Options.Is(Flag))
                        return true;
                  for (auto& refine : RefineVector)
                  {
                        if (refine->Interior.Options.Is(Flag))
                              return true;
                  }
                  return false;
            }

            bool IsBoundary(Flags Flag)
            {
                  if (Refine->Boundary.Options.Is(Flag))
                        return true;
                  for (auto& refine : RefineVector)
                  {
                        if (refine->Boundary.Options.Is(Flag))
                              return true;
                  }
                  return false;
            }

            void InitializeMeshing()
            {
                  MeshElementsSelectedFlag = false;
                  PreservedElements.clear();
                  PreservedElements.resize(0);
            }

            void FinalizeMeshing()
            {
                  MeshElementsSelectedFlag = false;
                  PreservedElements.clear();
                  PreservedElements.resize(0);
            }
      };

      /// Pointer definition of MesherData
      KRATOS_CLASS_POINTER_DEFINITION(MesherData);

      ///@}
      ///@name Life Cycle
      ///@{

      /// Default constructor.
      MesherData() {}

      /// Copy constructor.
      MesherData(MesherData const &rOther) {}

      /// Clone.
      MesherData::Pointer Clone() const
      {
            return Kratos::make_shared<MesherData>(*this);
      }

      /// Destructor.
      virtual ~MesherData() {}

      ///@}
      ///@name Operators
      ///@{

      ///@}
      ///@name Operations
      ///@{

      /**
     *  Set Nodes to mesh
     */
      void SetNodes(ModelPart &rModelPart,
                    MeshingParameters &rMeshingVariables);

      /**
     * Set Elements to mesh
     */
      void SetElements(ModelPart &rModelPart,
                       MeshingParameters &rMeshingVariables);

      /**
     * Get Refine data to mesh
     */
      RefineData &GetRefineData(Condition &rCondition,
                                MeshingParameters &rMeshingVariables,
                                const ProcessInfo &rCurrentProcessInfo);

      RefineData &GetRefineData(Element &rElement,
                                MeshingParameters &rMeshingVariables,
                                const ProcessInfo &rCurrentProcessInfo);

      RefineData &GetRefineData(Geometry<Node<3>> &rGeometry,
                                MeshingParameters &rMeshingVariables,
                                const ProcessInfo &rCurrentProcessInfo);

      RefineData &GetRefineData(Node<3> &rNode,
                                MeshingParameters &rMeshingVariables,
                                const ProcessInfo &rCurrentProcessInfo);
      ///@}
      ///@name Access
      ///@{

      ///@}
      ///@name Inquiry
      ///@{

      ///@}
      ///@name Input and output
      ///@{

      /// Turn back information as a string.
      virtual std::string Info() const
      {
            std::stringstream buffer;
            buffer << "MesherData";
            return buffer.str();
      }

      /// Print information about this object.
      virtual void PrintInfo(std::ostream &rOStream) const
      {
            rOStream << "MesherData";
      }

      /// Print object's data.
      virtual void PrintData(std::ostream &rOStream) const
      {
            rOStream << "MesherData Data";
      }

      ///@}
      ///@name Friends
      ///@{

      ///@}

protected:
      ///@name Protected static Member Variables
      ///@{

      ///@}
      ///@name Protected member Variables
      ///@{

      ///@}
      ///@name Protected Operators
      ///@{

      ///@}
      ///@name Protected Operations
      ///@{

      ///@}
      ///@name Protected  Access
      ///@{

      ///@}
      ///@name Protected Inquiry
      ///@{

      ///@}
      ///@name Protected LifeCycle
      ///@{

      ///@}

private:
      ///@name Static Member Variables
      ///@{

      ///@}
      ///@name Member Variables
      ///@{

      ///@}
      ///@name Private Operators
      ///@{

      /// Assignment operator.
      MesherData &operator=(MesherData const &rOther);

      ///@}
      ///@name Private Operations
      ///@{

      ///@}
      ///@name Private  Access
      ///@{

      ///@}
      ///@name Private Inquiry
      ///@{

      ///@}
      ///@name Un accessible methods
      ///@{

      ///@}

}; // Class MesherData

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                MesherData &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const MesherData &rThis)
{
      rThis.PrintInfo(rOStream);
      rOStream << " : " << std::endl;
      rThis.PrintData(rOStream);

      return rOStream;
}
///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_MESHER_DATA_HPP_INCLUDED  defined
