//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

#if !defined(KRATOS_LAPLACIAN_SMOOTHING_HPP_INCLUDED)
#define KRATOS_LAPLACIAN_SMOOTHING_HPP_INCLUDED

// System includes

// Project includes
#include "custom_utilities/transfer_utilities.hpp"
#include "custom_utilities/model_part_utilities.hpp"
#include "custom_utilities/geometry_utilities.hpp"
#include "utilities/builtin_timer.h"

#include "meshers_application_variables.h"

namespace Kratos
{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Applies a recollocation of the nodes improving the mesh shape
   *  variables are interpolated to the new positions
   */
class LaplacianSmoothing
{
public:
  ///@name Type Definitions
  ///@{

  typedef Node<3> NodeType;
  typedef array_1d<double,3> VectorType;
  typedef Geometry<NodeType> GeometryType;

  typedef Bucket<3, Node<3>, std::vector<Node<3>::Pointer>, Node<3>::Pointer, std::vector<Node<3>::Pointer>::iterator, std::vector<double>::iterator> BucketType;
  typedef Tree<KDTreePartition<BucketType>> KdtreeType; //Kdtree

  /// Pointer definition of data transfer
  KRATOS_CLASS_POINTER_DEFINITION(LaplacianSmoothing);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  LaplacianSmoothing(
    ) {}

  /// Destructor.
  virtual ~LaplacianSmoothing() {}

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{


  static inline void ApplyMeshSmoothing(ModelPart &rModelPart,
                                        const int *pElements,
                                        const std::vector<int> &PreservedElements,
                                        const unsigned int &number_of_points,
                                        const double &offset_factor)
  {

    KRATOS_TRY

    // BuiltinTimer time_counter;

    const unsigned int dimension = rModelPart.GetProcessInfo()[SPACE_DIMENSION];

    const unsigned int nds = rModelPart.Elements().front().GetGeometry().size();

    std::vector<std::vector<int>> list_of_neighbour_nodes;

    GetNeighbourNodes(rModelPart, list_of_neighbour_nodes, PreservedElements, pElements, number_of_points, nds);

    //MOVE NODES: INNER
    if (!(rModelPart.Is(FLUID) && dimension == 3)) //not working in 3D fluid, some elements get inverted, it must be performed before meshing
      ApplyDomainSmoothing(rModelPart, PreservedElements, pElements, list_of_neighbour_nodes, number_of_points);

    // std::cout<<"    LAPLACIAN smoothing time = "<< time_counter.ElapsedSeconds() <<std::endl;
    // BuiltinTimer time_boundary_counter;

    //MOVE NODES: BOUNDARY INNER DISPLACEMENT PROJECTION
    //SetInsideProjection(rModelPart, number_of_points, offset_factor, list_of_neighbour_nodes); //Commented because calculation is less robust

    //MOVE NODES: BOUNDARY
    if (rModelPart.IsNot(FLUID) && dimension == 3)
      ApplyBoundarySmoothing(rModelPart, PreservedElements, pElements, list_of_neighbour_nodes, number_of_points);

    // std::cout<<"    LAPLACIAN smoothing boundary time = "<< time_boundary_counter.ElapsedSeconds() <<std::endl;
    // BuiltinTimer time_project_counter;

    //TRANSFER VARIABLES TO NEW NODES POSITION:
    TransferUtilities::InterpolateNodalVariables(rModelPart);

    // std::cout<<"    LAPLACIAN smoothing project time = "<< time_project_counter.ElapsedSeconds() <<std::endl;

    KRATOS_CATCH("")
  }


  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{
  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{
  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{

  /// Assignment operator.
  LaplacianSmoothing &operator=(LaplacianSmoothing const &rOther);

  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Unaccessible methods
  ///@{


  static inline void ApplyDomainSmoothing(ModelPart &rModelPart,
                                          const std::vector<int> &PreservedElements,
                                          const int *pElements,
                                          std::vector<std::vector<int>> &list_of_neighbour_nodes,
                                          const unsigned int &number_of_points)
  {
    KRATOS_TRY

    ModelPart::NodesContainerType &rNodes = rModelPart.Nodes();

    //MOVE NODES: LAPLACIAN SMOOTHING:

    double convergence_tol  = 0.001;
    double smoothing_factor = 0.1;
    double smoothing_iters  = 3; //4

    double length = 0;
    double max_length = 0;
    double new_max_length = 0;

    if(SetFlagsUtilities::CountFlagsInEntities(rModelPart,"Nodes",{TO_ERASE})!=0)
      std::cout<<" Node TO_ERASE from heritage in Laplacian Smoothing "<<std::endl;

    std::vector<Flags> BlockFlags = {BOUNDARY, STRUCTURE, TO_ERASE, INSIDE, ISOLATED, MARKER};

    ModelPart::NodesContainerType::iterator it_begin = rModelPart.NodesBegin();

    unsigned int size = (rNodes.size() < number_of_points) ? rNodes.size() : number_of_points;
    double iters = 0;
    bool converged = false;
    while (iters <= smoothing_iters && converged == false)
    {
      //convergence variables
      max_length = new_max_length;
      new_max_length = 0;

      for (unsigned int i = 0; i < size; i++)
      {
        ModelPart::NodesContainerType::iterator it_node = it_begin + i;
        std::vector<int>& nNodes = list_of_neighbour_nodes[i + 1];
        const unsigned int &number_of_neighbours = nNodes.size();

        bool accept = false;

        if (number_of_neighbours > 1 && it_node->IsNot(BlockFlags))
          accept = true;

        // if (number_of_neighbours > 1)
        // {
        //   unsigned int blocked_nodes = 0;
        //   for (unsigned int j = 0; j < number_of_neighbours; ++j)
        //   {
        //     ModelPart::NodesContainerType::iterator it_nnode = it_begin + (nNodes[j] - 1);
        //     if (it_nnode->Or(BlockFlags))
        //       ++blocked_nodes;
        //   }

        //   if (blocked_nodes > 0){
        //     std::cout<<" Laplacian blocked nodes "<<blocked_nodes<<std::endl;
        //     accept = false;
        //   }
        // }

        if ( accept) {
           ElementWeakPtrVectorType & neig = it_node->GetValue(NEIGHBOUR_ELEMENTS);
           unsigned int prop = -10;
           for (auto &i_elem: neig) {
              unsigned int thisProp = i_elem.GetProperties().Id();
              if ( prop == -10) {
                 prop = thisProp;
              } else if (thisProp != prop) {
                 accept = false;
              }

           }
        }

        if (accept)
        {
          bool boundary_weight = true;

          length = SmoothDistance(i, it_begin, nNodes, smoothing_factor, boundary_weight);

          if (new_max_length < length)
            new_max_length = length;
        }
      }

      if ((new_max_length - max_length) / new_max_length < convergence_tol)
      {
        converged = true;
        std::cout << "   Laplacian smoothing convergence achieved " << std::endl;
      }

      iters++;
    }

    if (iters == smoothing_iters && !converged)
    {
      std::cout << "     WARNING: Laplacian smoothing convergence NOT achieved (iters:" << iters << ")" << std::endl;
    }

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void ApplyBoundarySmoothing(ModelPart &rModelPart,
                                            const std::vector<int> &PreservedElements,
                                            const int *pElements,
                                            std::vector<std::vector<int>> &list_of_neighbour_nodes,
                                            const unsigned int &number_of_points)
  {
    KRATOS_TRY

    ModelPart::NodesContainerType &rNodes = rModelPart.Nodes();

    const unsigned int nds = rModelPart.Elements().front().GetGeometry().size();

    const unsigned int dimension = rModelPart.GetProcessInfo()[SPACE_DIMENSION];

    //*******************************************************************
    //NEIGBOUR NODES:

    std::vector<int> list_of_neighbour_candidates(number_of_points+1);
    std::fill(list_of_neighbour_candidates.begin(), list_of_neighbour_candidates.end(), 0);

    GetCoplanarBoundaryNeighbourNodes(rNodes, list_of_neighbour_nodes, list_of_neighbour_candidates, PreservedElements, pElements, number_of_points, nds);

    //*******************************************************************
    //MOVE BOUNDARY NODES: LAPLACIAN SMOOTHING:

    double convergence_tol  = 0.001;
    double smoothing_factor = 0.1; //0.1
    double smoothing_iters  = 4;    //3,4

    if (dimension == 2)
      smoothing_factor = 0.01;

    double length = 0;
    double max_length = 0;
    double new_max_length = 0;

    std::vector<Flags> BlockFlags = {BOUNDARY.AsFalse(), TO_ERASE};

    ModelPart::NodesContainerType::iterator it_begin = rModelPart.NodesBegin();

    unsigned int size = (rNodes.size() < number_of_points) ? rNodes.size() : number_of_points;
    double iters = 0;
    bool converged = false;
    while (iters <= smoothing_iters && converged == false)
    {
      //std::cout<<" Iter "<< iters <<std::endl;
      //convergence variables
      max_length = new_max_length;
      new_max_length = 0;

      for (unsigned int i = 0; i < size; ++i)
      {
        ModelPart::NodesContainerType::iterator it_node = it_begin + i;
        std::vector<int>& nNodes = list_of_neighbour_nodes[i + 1];
        const unsigned int &number_of_neighbours = nNodes.size();

        if (number_of_neighbours > 1 && it_node->IsNot(BlockFlags) && list_of_neighbour_candidates[i + 1] == 0)
        {
          bool boundary_weight = false;

          length = SmoothDistance(i, it_begin, nNodes, smoothing_factor, boundary_weight);

          if (new_max_length < length)
            new_max_length = length;
        }
      }

      if ((new_max_length - max_length) / new_max_length < convergence_tol)
      {
        converged = true;
        std::cout << "   Laplacian smoothing convergence achieved " << std::endl;
      }

      iters++;
    }

    if (iters == smoothing_iters && !converged)
    {
      std::cout << "     WARNING: Boundary Laplacian smoothing convergence NOT achieved (iters:" << iters << ")" << std::endl;
    }

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double SmoothDistance(const int &node_id, ModelPart::NodesContainerType::iterator &it_begin, const std::vector<int> &nNodes, const  double &rsmoothing_factor, const bool boundary_weight)
  {
    array_1d<double,3> D;
    noalias(D) = ZeroVector(3);

    double total_weight = 0;
    array_1d<double, 3> TotalDistance;
    noalias(TotalDistance) = ZeroVector(3);

    bool simple = true; //weight = 1;
    double weight = 0;
    double length = 0;

    //point position
    ModelPart::NodesContainerType::iterator it_node = it_begin + node_id;
    array_1d<double, 3> &P = it_node->Coordinates();

    //std::cout<<" Initial Position: "<<P<<std::endl;
    const unsigned int &number_of_neighbours = nNodes.size();

    for (unsigned int j = 0; j < number_of_neighbours; ++j)
    {
      //neighbour position
      ModelPart::NodesContainerType::iterator it_nnode = it_begin + (nNodes[j] - 1);
      const array_1d<double, 3> &Q = it_nnode->Coordinates();

      noalias(D) = P - Q;

      length = sqrt(D[0] * D[0] + D[1] * D[1] + D[2] * D[2]);

      if (simple)
      {
        weight = 1;
      }
      else
      {
        if (length != 0)
          weight = (1.0 / length);
        else
          weight = 0;
      }

      if (boundary_weight)
        weight *= GetBoundaryFactor(it_nnode);

      TotalDistance += (weight * (Q - P));
      total_weight += weight;
    }

    if (total_weight != 0)
      noalias(D) = (rsmoothing_factor / total_weight) * TotalDistance;
    else
      noalias(D) = ZeroVector(3);

    noalias(P) += D;

    return length;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double GetBoundaryFactor(ModelPart::NodesContainerType::iterator &it_nnode)
  {
    if (it_nnode->Is(BOUNDARY))
    {
      bool contact_active = false;
      if (it_nnode->SolutionStepsDataHas(CONTACT_FORCE))
      {
        array_1d<double, 3> &ContactForce = it_nnode->FastGetSolutionStepValue(CONTACT_FORCE);
        if (norm_2(ContactForce) != 0)
          contact_active = true;
      }

      if (contact_active)
        return 0.8;  //(0,1] contact_weight;
      else
        return 0.95;  //(0,1] boundary_weight;
    }
    else{
      if (it_nnode->Is(INTERACTION))
        return 0.8;
    }

    return 1;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void GetNeighbourNodes(ModelPart &rModelPart, std::vector<std::vector<int>> &list_of_neighbour_nodes, const std::vector<int> &PreservedElements, const int *pElements, const unsigned int &number_of_points, const unsigned int &nds, const std::vector<Flags> SelectFlags = {})
  {
    KRATOS_TRY

    if (list_of_neighbour_nodes.size() != number_of_points + 1)
      list_of_neighbour_nodes.resize(number_of_points + 1);

    std::vector<int> fill_vector;
    std::fill(list_of_neighbour_nodes.begin(), list_of_neighbour_nodes.end(), fill_vector);

    bool neighb_set = false;
    int neighb_size = 0;

    unsigned int inode = 0;
    unsigned int jnode = 0;

    ModelPart::NodesContainerType::iterator it_begin = rModelPart.NodesBegin();

    for (unsigned int el = 0; el < PreservedElements.size(); ++el)
    {
      if (PreservedElements[el]>0)
      {
        //a) Create list of node neigbours (list_of_neighbour_nodes)
        for (unsigned int ipn = 0; ipn < nds; ++ipn)
        {
          inode = pElements[el * nds + ipn];
          //std::cout<<" i_node "<<inode<<std::endl;
          if( inode-1 > number_of_points )
            std::cout << " reached id "<< inode-1 <<std::endl;
          if((it_begin + inode - 1)->Is(SelectFlags))
          {
            for (unsigned int jpn = 0; jpn < nds; ++jpn)
            {
              jnode = pElements[el * nds + jpn];

              if( jnode-1 > number_of_points)
                std::cout << " reached id "<< jnode-1 <<std::endl;

              if (ipn != jpn && (it_begin + jnode - 1)->Is(SelectFlags))
              {
                //add unique node neigbour
                neighb_size = list_of_neighbour_nodes[inode].size();
                neighb_set = false;
                for (int npn = 0; npn < neighb_size; ++npn)
                {
                  if (list_of_neighbour_nodes[inode][npn] == jnode)
                  {
                    neighb_set = true;
                    break;
                  }
                }
                if (neighb_set == false)
                {
                  list_of_neighbour_nodes[inode].push_back(jnode);
                }
              }
            }
          }
        }
      }
    }

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void GetBoundaryNeighbourNodes(ModelPart &rModelPart, std::vector<std::vector<int>> &list_of_neighbour_nodes, const std::vector<int> &PreservedElements, const int *pElements, const unsigned int &number_of_points, const unsigned int &nds)
  {
    GetNeighbourNodes(rModelPart,list_of_neighbour_nodes,PreservedElements,pElements,number_of_points,nds,{BOUNDARY});
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void GetCoplanarBoundaryNeighbourNodes(ModelPart::NodesContainerType &rNodes,
                                                       std::vector<std::vector<int>> &list_of_neighbour_nodes,
                                                       std::vector<int> &neigbour_candidates,
                                                       const std::vector<int> &PreservedElements,
                                                       const int *pElements,
                                                       const unsigned int &number_of_points, const unsigned int &nds)
  {

    KRATOS_TRY

    if (list_of_neighbour_nodes.size() != number_of_points + 1)
      list_of_neighbour_nodes.resize(number_of_points + 1);

    std::vector<int> fill_vector;
    std::fill(list_of_neighbour_nodes.begin(), list_of_neighbour_nodes.end(), fill_vector);

    bool neighb_set = false;
    int neighb_size = 0;
    bool candidate = false;

    unsigned int inode = 0;
    unsigned int jnode = 0;

    ModelPart::NodesContainerType::iterator it_begin = rNodes.begin();

    for (unsigned int el = 0; el < PreservedElements.size(); ++el)
    {
      if (PreservedElements[el]>0)
      {
        //std::cout<<" ELEMENT "<<el<<std::endl;
        //a) Create list of node neigbours (list_of_neighbour_nodes)
        for (unsigned int ipn = 0; ipn < nds; ++ipn)
        {
          inode = pElements[el * nds + ipn];
          if( inode-1 > number_of_points )
            std::cout << " b reached id "<< inode-1 <<std::endl;
          if ((it_begin + inode - 1)->Is(BOUNDARY))
          {
            for (unsigned int jpn = 0; jpn < nds; ++jpn)
            {
              jnode = pElements[el * nds + jpn];
              if( jnode-1 > number_of_points)
                std::cout << " b reached id "<< jnode-1 <<std::endl;
              if (ipn != jpn && (it_begin + jnode - 1)->Is(BOUNDARY))
              {
                //add unique node neigbour
                neighb_size = list_of_neighbour_nodes[inode].size();
                neighb_set = false;
                for (int npn = 0; npn < neighb_size; ++npn)
                {
                  if (list_of_neighbour_nodes[inode][npn] == jnode)
                  {
                    neighb_set = true;
                    break;
                  }
                }
                if (neighb_set == false)
                {

                  if (GeometryUtilities::CheckCoplanar(*(*(it_begin + inode - 1).base()), *(*(it_begin + jnode - 1).base()), candidate))
                  {
                    list_of_neighbour_nodes[inode].push_back(jnode);
                    // std::cout<<" Add node ["<<pElements[el*nds+ipn]<<"] "<<list_of_neighbour_nodes[pElements[el*nds+ipn]]<<" "<<pElements[el*nds+jpn]<<std::endl;
                    // if(rNodes[pElements[el*nds+ipn]].Is(NEW_ENTITY))
                    //    std::cout<<" boundary node ["<<pElements[el*nds+ipn]<<"] is new entity "<<std::endl;
                    // if(rNodes[pElements[el*nds+jpn]].Is(NEW_ENTITY))
                    //    std::cout<<" neighbour boundary node ["<<pElements[el*nds+jpn]<<"] is new entity "<<std::endl;
                  }
                  else
                  {
                    if (candidate)
                      neigbour_candidates[inode] += 1;
                  }
                }
              }
            }
          }
        }
      }
    }

    KRATOS_CATCH("")
  }


  //*******************************************************************************************
  //*******************************************************************************************

  //Note : to increase de robustness, the proposal here is to detect the first layer/interaction nodes, via setting ranks to nodes
  // then ensure that the first layer nodes have a certain distance to the boundary when it is compressed. Bigger than the offset applied

  static inline void SetInsideProjection(ModelPart &rModelPart,
                                         const unsigned int &number_of_points,
                                         const double &offset_factor,
                                         const std::vector<std::vector<int>> &list_of_neighbour_nodes)
  {

    KRATOS_TRY

    std::vector<double> nodes_ranks = SetRanks(rModelPart, number_of_points, list_of_neighbour_nodes);
    std::vector<double> nodes_layer = SetFirstLayer(rModelPart, number_of_points, list_of_neighbour_nodes);

    double movement_factor = 1.2;
    double contact_factor = 2.0;
    const array_1d<double, 3> ZeroPoint(3, 0.0);

    std::vector<array_1d<double, 3>> initial_nodes_distances(number_of_points + 1);
    std::fill(initial_nodes_distances.begin(), initial_nodes_distances.end(), ZeroPoint);

    array_1d<double, 3> DeltaDisplacement;

    ModelPart::NodesContainerType::iterator it_begin = rModelPart.NodesBegin();
    for (unsigned int i = 0; i < number_of_points; ++i)
    {
      ModelPart::NodesContainerType::iterator it_node = it_begin + i;

      //if(nodes_ranks[i+1]<=1)
      if (nodes_ranks[i + 1] < 1)
      {
        noalias(DeltaDisplacement) = it_node->FastGetSolutionStepValue(DISPLACEMENT) - it_node->FastGetSolutionStepValue(DISPLACEMENT, 1);

        const array_1d<double, 3> &Normal = it_node->FastGetSolutionStepValue(NORMAL);

        double projection = inner_prod(DeltaDisplacement, Normal);

        if (projection<0)
        {
          double factor = movement_factor;
          if (it_node->SolutionStepsDataHas(CONTACT_FORCE))
          {
            array_1d<double, 3> &ContactForce = it_node->FastGetSolutionStepValue(CONTACT_FORCE);
            if (norm_2(ContactForce) != 0)
              factor *= contact_factor;
          }

          initial_nodes_distances[i + 1] = factor * projection * Normal;
        }
      }

      //layer modification
      array_1d<double, 3> D;
      noalias(D) = ZeroVector(3);

      if (nodes_layer[i + 1] == 1)
      {
        const std::vector<int>& nNodes = list_of_neighbour_nodes[i + 1];
        unsigned int number_of_neighbours = nNodes.size();

        ModelPart::NodesContainerType::iterator it_node = it_begin + i;
        array_1d<double, 3> &P = it_node->Coordinates();

        unsigned int shared_node = 1;
        for (unsigned int j = 0; j < number_of_neighbours; ++j)
        {
          ModelPart::NodesContainerType::iterator it_nnode = it_begin + (nNodes[j] - 1);

          if (it_node->Is(BOUNDARY))
          {
            const array_1d<double, 3> &Q = it_nnode->Coordinates();
            const array_1d<double, 3> &Normal = it_nnode->FastGetSolutionStepValue(NORMAL); //BOUNDARY_NORMAL must be set as nodal variable

            noalias(D) = Q - P;

            double projection = inner_prod(D, Normal);

            double secure_offset_factor = 1.1;

            if (projection < offset_factor && offset_factor != 0)
              initial_nodes_distances[i + 1] = (-1) * (secure_offset_factor) * (offset_factor - projection) * Normal;
          }
        }
      }
    }

    double smoothing_factor = 0.15;
    double smoothing_iters = 10;

    double iters = 0;
    while (iters < smoothing_iters)
    {
      double total_weight = 0;
      double weight = 0;
      array_1d<double, 3> TotalDistance;
      array_1d<double, 3> Distance;
      array_1d<double, 3> OffsetDistance;

      //convergence variables
      for (unsigned int i = 0; i < number_of_points; ++i)
      {

        TotalDistance = initial_nodes_distances[i + 1];
        OffsetDistance.clear();

        if (nodes_ranks[i + 1] > 0)
        {

          if (nodes_layer[i + 1] == 1)
            OffsetDistance = TotalDistance;

          unsigned int number_of_neighbours = list_of_neighbour_nodes[i + 1].size();

          unsigned int shared_node = 1;

          total_weight = 0;
          weight = 0;
          Distance.clear();

          for (unsigned int j = 0; j < number_of_neighbours; ++j)
          {
            //neighbour
            shared_node = list_of_neighbour_nodes[i + 1][j];

            weight = 1.0 / (nodes_ranks[shared_node] + 1.0);
            total_weight += weight;
            Distance += initial_nodes_distances[shared_node] * weight;
          }

          if (total_weight != 0)
            Distance *= (1.0 / total_weight);
          else
            Distance = initial_nodes_distances[i + 1];

          TotalDistance += smoothing_factor * (Distance - initial_nodes_distances[i + 1]);
        }

        if (nodes_layer[i + 1] == 1 && norm_2(OffsetDistance) > norm_2(TotalDistance) + 1e-10)
        {
          TotalDistance = OffsetDistance;
          std::cout << " Layer Correction " << norm_2(OffsetDistance) << " > " << norm_2(TotalDistance) << std::endl;
        }

        initial_nodes_distances[i + 1] = TotalDistance;
      }

      iters++;
    }

    for (unsigned int i = 0; i < number_of_points; ++i)
    {
      if (nodes_ranks[i + 1] > 0)
      {
        ModelPart::NodesContainerType::iterator it_node = it_begin + i;
        //std::cout<<" Projection Set "<<initial_nodes_distances[i + 1]<<" offset factor "<<offset_factor<<"rank "<<nodes_ranks[i + 1]<<std::endl;
        it_node->Coordinates() += initial_nodes_distances[i + 1];
      }
    }

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline std::vector<double> SetRanks(ModelPart &rModelPart,
                                             const unsigned int &number_of_points,
                                             const std::vector<std::vector<int>> &list_of_neighbour_nodes)
  {
    KRATOS_TRY

    //set ranks
    std::vector<double> nodes_ranks;
    nodes_ranks.resize(number_of_points + 1);
    std::fill(nodes_ranks.begin(), nodes_ranks.end(), 0);

    //initial assignation
    ModelPart::NodesContainerType::iterator it_begin = rModelPart.NodesBegin();
    for (unsigned int i = 0; i < number_of_points; ++i)
    {
      ModelPart::NodesContainerType::iterator it_node = it_begin + i;
      bool contact_active = false;
      if (it_node->SolutionStepsDataHas(CONTACT_FORCE))
      {
        array_1d<double, 3> &ContactForceNormal = it_node->FastGetSolutionStepValue(CONTACT_FORCE);
        if (norm_2(ContactForceNormal) != 0)
          contact_active = true;
      }

      if (!contact_active && it_node->IsNot(BOUNDARY))
      {
        nodes_ranks[i + 1] = 5;
      }
      // else if( norm_2(ContactForceNormal)==0 && it_node->Is(BOUNDARY)){
      //   nodes_ranks[i+1]=1;
      // }
    }

    //RANK assignation:
    double rang_assign = 0;
    double rang_top = 5; //3;

    while (rang_assign < rang_top)
    {
      for (unsigned int i = 0; i < number_of_points; ++i)
      {
        if (nodes_ranks[i+1] == rang_assign)
        {
          //Rank 0
          unsigned int shared_node = 1;
          unsigned int number_of_neighbours = list_of_neighbour_nodes[i + 1].size();
          for (unsigned int j = 0; j < number_of_neighbours; ++j)
          {
            shared_node = list_of_neighbour_nodes[i + 1][j];

            if (nodes_ranks[shared_node] > rang_assign)
              nodes_ranks[shared_node] = rang_assign + 1;
          }
        }
      }
      rang_assign++;
    }

    return nodes_ranks;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline std::vector<double> SetFirstLayer(ModelPart &rModelPart,
                                                  const unsigned int &number_of_points,
                                                  const std::vector<std::vector<int>> &list_of_neighbour_nodes)
  {
    KRATOS_TRY

    //set ranks
    std::vector<double> nodes_layer;
    nodes_layer.resize(number_of_points+1);
    std::fill(nodes_layer.begin(), nodes_layer.end(), 0);

    //initial assignation
    ModelPart::NodesContainerType::iterator it_begin = rModelPart.NodesBegin();
    for (unsigned int i = 0; i < number_of_points; ++i)
    {
      ModelPart::NodesContainerType::iterator it_node = it_begin + i;
      if (it_node->IsNot(BOUNDARY))
      {
        nodes_layer[i+1] = 2;
      }
    }

    //LAYER assignation:
    double layer_assign = 0;
    double layer_top = 1;

    while (layer_assign < layer_top)
    {
      for (unsigned int i = 0; i < number_of_points; ++i)
      {
        if (nodes_layer[i+1] == layer_assign)
        {
          //Rank 0
          unsigned int shared_node = 1;
          unsigned int number_of_neighbours = list_of_neighbour_nodes[i+1].size();
          for (unsigned int j = 0; j < number_of_neighbours; ++j)
          {
            shared_node = list_of_neighbour_nodes[i+1][j];

            if (nodes_layer[shared_node] > layer_assign)
              nodes_layer[shared_node] = layer_assign + 1;
          }
        }
      }

      layer_assign++;
    }

    return nodes_layer;

    KRATOS_CATCH("")
  }

  ///@}

}; // Class LaplacianSmoothing

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
///@}

} // namespace Kratos.

#endif // KRATOS_LAPLACIAN_SMOOTHING_HPP_INCLUDED  defined
