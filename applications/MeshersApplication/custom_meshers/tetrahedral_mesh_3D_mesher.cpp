//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

// System includes

// External includes

// Project includes
//#include "geometries/tetrahedra_3d_4.h"
#include "custom_meshers/tetrahedral_mesh_3D_mesher.hpp"

#include "meshers_application_variables.h"
#include "custom_processes/print_mesh_output_mesher_process.hpp"

namespace Kratos
{

//*******************************************************************************************
//*******************************************************************************************

void TetrahedralMesh3DMesher::Generate(ModelPart &rModelPart,
                                       MeshingParametersType &rMeshingVariables)
{

  KRATOS_TRY

  BuiltinTimer start_time_counter;

  this->StartEcho(rModelPart, "DELAUNAY Remesh");

  //*********************************************************************

  ////////////////////////////////////////////////////////////
  this->ExecutePreMeshingProcesses();
  ////////////////////////////////////////////////////////////

  //*********************************************************************

  //Creating the containers for the input and output
  tetgenio in;
  tetgenio out;

  BuildInput(rModelPart, rMeshingVariables, in);

  //*********************************************************************

  if (this->GetEchoLevel() >= 0)
    std::cout << " [ PRE MESHING (TIME = " << start_time_counter.ElapsedSeconds() << ") ] " << std::endl;

  BuiltinTimer time_counter;

  //Generate Mesh
  ////////////////////////////////////////////////////////////
  int fail = GenerateTessellation(rMeshingVariables, in, out);
  ////////////////////////////////////////////////////////////

  if(fail || out.numberoftetrahedra == 0){ //retry
    if( rMeshingVariables.Options.Is(MesherData::CONSTRAINED) && rMeshingVariables.Options.IsNot(MesherData::CONTACT_SEARCH) ){
      std::cout << " [ RETRY MESHING (Allow steiner points) ] " << std::endl;
      DeleteOutContainer(rMeshingVariables.OutMesh, out);
      std::string str = rMeshingVariables.TessellationFlags;
      rMeshingVariables.TessellationFlags = "pnJBFV";
      ////////////////////////////////////////////////////////////
      fail = GenerateTessellation(rMeshingVariables,in,out);
      ////////////////////////////////////////////////////////////
      rMeshingVariables.TessellationFlags = str;
    }
  }

  if (fail)
    std::cout<<" failure type : " << fail <<std::endl;

  if (in.numberofpoints != out.numberofpoints)
  {
    std::cout << "  [MESH GENERATION point insertion (initial = " << in.numberofpoints << " final = " << out.numberofpoints << ")] " << std::endl;
  }

  if (out.numberofpoints > in.numberofpoints)
    rMeshingVariables.Info->SetTessellationInsertion(true);
  else
    rMeshingVariables.Info->SetTessellationInsertion(false);

  if (out.numberoftetrahedra == 0 || out.numberofpoints == 0)
    KRATOS_ERROR << "  CRITICAL ERROR, MESH NOT GENERATED "<< std::endl;

  //print out the mesh generation time
  if (this->GetEchoLevel() >= 0)
    std::cout << "  [MESH GENERATION (TIME = " << time_counter.ElapsedSeconds() << ")] " << std::endl;

  BuiltinTimer end_time_counter;

  //*********************************************************************

  //GetOutput
  SetToContainer(rMeshingVariables.OutMesh, out);

  //*********************************************************************

  // if (this->GetEchoLevel() > 0)
  // {
  //   if (rMeshingVariables.ExecutionOptions.Is(MesherData::FINALIZE_MESHER_INPUT)){
  //     //Print input mesh
  //     PrintMeshOutputMesherProcess print_output(rModelPart,rMeshingVariables,"output",1);
  //     print_output.Execute();
  //   }
  // }

  ////////////////////////////////////////////////////////////
  this->ExecutePostMeshingProcesses();
  ////////////////////////////////////////////////////////////

  //*********************************************************************

  //Free input memory or keep it to transfer it for next mesh generation
  if (rMeshingVariables.ExecutionOptions.Is(MesherData::FINALIZE_MESHER_INPUT))
  {
    DeleteInContainer(rMeshingVariables.InMesh, in);
    rMeshingVariables.InputInitializedFlag = false;
  }

  //*********************************************************************

  //Free output memory
  if (rMeshingVariables.Options.Is(MesherData::REMESH))
    DeleteOutContainer(rMeshingVariables.OutMesh, out);

  this->EndEcho(rModelPart, "DELAUNAY Remesh");


  if (this->GetEchoLevel() >= 0)
    std::cout << " [ POST MESHING (TIME = " << end_time_counter.ElapsedSeconds() << ") ] " << std::endl;



  KRATOS_CATCH("")
}

//*******************************************************************************************
//*******************************************************************************************

int TetrahedralMesh3DMesher::GenerateTessellation(MeshingParametersType &rMeshingVariables,
                                                  tetgenio &in,
                                                  tetgenio &out)
{
  KRATOS_TRY

  //if not remesh return true
  if (rMeshingVariables.Options.IsNot(MesherData::REMESH))
  {
    // out.pointlist             = in.pointlist;
    // out.numberofpoints        = in.numberofpoints;
    // out.tetrahedronlist       = in.tetrahedronlist;
    // out.numberoftetrahedra    = in.numberoftetrahedra;
    // out.tetrahedronvolumelist = in.tetrahedronvolumelist;
    // out.neighborlist          = in.neighborlist;
    out = in;
    return 1;
  }

  int fail = 0;

  if (this->GetEchoLevel() > 0)
    std::cout << " [ REMESH: (in POINTS " << in.numberofpoints << ") " << std::endl;

  this->CheckPoints(in);
  //this->WritePoints(in);

  std::string str = rMeshingVariables.TessellationFlags;
  char *meshing_options = new char[str.length() + 1];
  strcpy(meshing_options, str.c_str());

  // if (rMeshingVariables.Options.Is(MesherData::CONSTRAINED) && in.numberoftetrahedra == 0)
  // {
  //   std::cout<<" CheckVertices only "<<std::endl;
  //   char* check_facets = "dVI";
  //   tetrahedralize(check_facets, &in, &out);
  // }

  //perform the meshing
  try
  {
    tetrahedralize(meshing_options, &in, &out);
  }

  catch (int error_code)
  {

    switch (TetgenErrors(error_code))
    {
    case INPUT_MEMORY_ERROR:
      std::cout << " input memory error " << std::endl;
      fail = 1;
      break;
    case INTERNAL_ERROR:
      std::cout << " internal error " << std::endl;
      fail = 2;
      break;
    case INVALID_GEOMETRY_ERROR:
      std::cout << " invalid geometry error " << std::endl;
      fail = 3;
      break;
    default:
      fail = 0;
      //create new connections
      if (this->GetEchoLevel() > 0)
        std::cout << " tetrahedralization done " << std::endl;
      break;
    }
  }

  delete[] meshing_options;

  //this->CheckFaces(out);
  //this->CheckInOutPoints(in, out);
  //this->WritePoints(out);
  //this->WriteTetrahedra(out);

  if (rMeshingVariables.Options.IsNot(MesherData::REFINE) && in.numberofpoints < out.numberofpoints)
  {
    fail = 3;
    std::cout << "  warning : [NODES ADDED] check mesh geometry " << std::endl;
  }

  if (this->GetEchoLevel() > 0)
  {
    std::cout << "  -( " << rMeshingVariables.TessellationInfo << " )- " << std::endl;
    std::cout << "  (out ELEMENTS " << out.numberoftetrahedra << ") " << std::endl;
    std::cout << "  (out POINTS " << out.numberofpoints << ") :  REMESH ]; " << std::endl;
    std::cout << std::endl;
  }

  return fail;

  KRATOS_CATCH("")
}

//*******************************************************************************************
//*******************************************************************************************

void TetrahedralMesh3DMesher::BuildInput(ModelPart &rModelPart,
                                         MeshingParametersType &rMeshingVariables,
                                         tetgenio &in)
{
  KRATOS_TRY

  if (rMeshingVariables.ExecutionOptions.Is(MesherData::INITIALIZE_MESHER_INPUT))
  {
    //Reorder and optimize model part (to help mesher sorting operations)
    // std::cout<<" Optimize Ordering "<<std::endl;
    // ModelPartUtilities::ReorderAndOptimizeModelPart(rModelPart); //raises segmentation fault
    // ModelPartUtilities::ReorderConditions(rModelPart);

    //Set Nodes
    if (rMeshingVariables.ExecutionOptions.Is(MesherData::TRANSFER_KRATOS_NODES_TO_MESHER))
      this->SetNodes(rModelPart, rMeshingVariables);

    //Set Elements
    if (rMeshingVariables.ExecutionOptions.Is(MesherData::TRANSFER_KRATOS_ELEMENTS_TO_MESHER))
      this->SetElements(rModelPart, rMeshingVariables);

    //Set Neighbours
    if (rMeshingVariables.ExecutionOptions.Is(MesherData::TRANSFER_KRATOS_NEIGHBOURS_TO_MESHER))
      this->SetNeighbours(rModelPart, rMeshingVariables);

    rMeshingVariables.InputInitializedFlag = true;
  }

  //input mesh: NODES
  in.firstnumber = 1;
  in.mesh_dim = 3;
  GetFromContainer(rMeshingVariables.InMesh, in);

  //Set Faces
  if (rMeshingVariables.ExecutionOptions.Is(MesherData::TRANSFER_KRATOS_FACES_TO_MESHER))
  {
    std::cout<<" TRANSFER FACETS "<<std::endl;
    this->SetFaces(rModelPart, rMeshingVariables, in);
  }

  std::cout<<" InMesh [nodes:"<<rMeshingVariables.InMesh.GetNumberOfPoints()<<", elements:"<<rMeshingVariables.InMesh.GetNumberOfElements()<<"]"<<std::endl;

  // if (rMeshingVariables.ExecutionOptions.Is(MesherData::FINALIZE_MESHER_INPUT)){
  //   //Print input mesh
  //   PrintMeshOutputMesherProcess print_output(rModelPart,rMeshingVariables,"input",1);
  //   print_output.Execute();
  // }

  KRATOS_CATCH("")
}

//*******************************************************************************************
//*******************************************************************************************

void TetrahedralMesh3DMesher::SetFaces(ModelPart &rModelPart,
                                       MeshingParametersType &rMeshingVariables,
                                       tetgenio &in)
{
  KRATOS_TRY

  //*********************************************************************

  if (in.facetlist)
  {
    delete[] in.facetlist;
    in.numberoffacets = 0;
  }

  if (in.facetmarkerlist)
  {
    delete[] in.facetmarkerlist;
  }

  if (in.holelist)
  {
    delete[] in.holelist;
    in.numberofholes = 0;
  }

  if (in.regionlist)
  {
    delete[] in.regionlist;
    in.numberofregions = 0;
  }

  //PART 2: facet list (we can have holes in facets != volume holes)

  in.numberoffacets = rModelPart.NumberOfConditions();
  in.facetmarkerlist = new int[in.numberoffacets];
  in.facetlist = new tetgenio::facet[in.numberoffacets];

  ModelPart::ConditionsContainerType::iterator conditions_begin = rModelPart.ConditionsBegin();

  // check if refining and coarsening processes created same connectivity facets
  // if (in.numberoftetrahedra == 0)
  // {
  //   std::cout<<" Check Coincident Facets "<<std::endl;
  //   ModelPartUtilities::CheckConditionFacets(rModelPart);
  // }

  //facets
  tetgenio::facet *f;
  tetgenio::polygon *p;

  for (int fc = 0; fc < in.numberoffacets; fc++)
  {
    f = &in.facetlist[fc];

    f->numberofpolygons = 1;
    f->polygonlist = new tetgenio::polygon[f->numberofpolygons];
    f->numberofholes = 0;
    f->holelist = NULL;
    p = &f->polygonlist[0];
    p->numberofvertices = 3; //face is a triangle
    p->vertexlist = new int[p->numberofvertices];

    if ((conditions_begin + fc)->Is(TO_ERASE))
      std::cout << " ERROR: condition to erase present " << std::endl;

    Geometry<Node<3>> &rGeometry = (conditions_begin + fc)->GetGeometry();

    for (int nd = 0; nd < 3; nd++)
    {
      p->vertexlist[nd] = rGeometry[nd].Id();
    }

    in.facetmarkerlist[fc] = 0; //boundary marker to preserve facets
  }

  //PART 3: (volume) hole list

  //holes
  in.numberofholes = 0;
  in.holelist = (REAL *)NULL;

  //PART 4: region attributes list

  //regions
  in.numberofregions = 1;
  in.regionlist = new REAL[in.numberofregions * 5];

  double inside_factor = 3;
  Geometry<Node<3>> &rGeometry = (conditions_begin)->GetGeometry();
  array_1d<double, 3> &Normal = rGeometry[0].FastGetSolutionStepValue(NORMAL);

  double Shrink = rGeometry[0].FastGetSolutionStepValue(SHRINK_FACTOR);

  //std::cout << " Normal [NodeId= " << rGeometry[0].Id() << "] " << Normal << std::endl;

  double NormNormal = norm_2(Normal);
  if (NormNormal != 0)
    Normal /= NormNormal;

  array_1d<double, 3> Offset = (-1) * Normal * Shrink * rMeshingVariables.OffsetFactor * inside_factor;

  const array_1d<double, 3> &rCoordinates = rGeometry[0].Coordinates();
  //inside point of the region:
  for (unsigned int j = 0; j < 3; j++)
  {
    in.regionlist[j] = (rCoordinates[j] + Offset[j]) * rMeshingVariables.Scale;
  }

  //region attribute (regional attribute or marker "A" must be switched)
  in.regionlist[3] = 0;

  //region maximum volume attribute (maximum volume attribute "a" (with no number following) must be switched)
  in.regionlist[4] = -1;

  std::cout << " Number of facets " << in.numberoffacets << " region (" << in.regionlist[0] << ", " << in.regionlist[1] << ", " << in.regionlist[2] << ") normal:" << Normal << " Offset " << rMeshingVariables.OffsetFactor * inside_factor << std::endl;

  KRATOS_CATCH("")
}

//*******************************************************************************************
//*******************************************************************************************
void TetrahedralMesh3DMesher::GetFromContainer(MesherData::MeshContainer &rMesh, tetgenio &tr)
{
  KRATOS_TRY

  //get pointers
  tr.pointlist = rMesh.GetPointList();
  tr.tetrahedronlist = rMesh.GetElementList();
  tr.tetrahedronvolumelist = rMesh.GetElementSizeList();
  tr.neighborlist = rMesh.GetElementNeighbourList();

  if (rMesh.GetNumberOfPoints() != 0)
    tr.numberofpoints = rMesh.GetNumberOfPoints();

  if (rMesh.GetNumberOfElements() != 0)
    tr.numberoftetrahedra = rMesh.GetNumberOfElements();

  KRATOS_CATCH("")
}

//*******************************************************************************************
//*******************************************************************************************

void TetrahedralMesh3DMesher::SetToContainer(MesherData::MeshContainer &rMesh, tetgenio &tr)
{

  KRATOS_TRY

  //set pointers
  rMesh.SetPointList(tr.pointlist);
  rMesh.SetElementList(tr.tetrahedronlist);
  rMesh.SetElementSizeList(tr.tetrahedronvolumelist);
  rMesh.SetElementNeighbourList(tr.neighborlist);

  // copy the numbers
  if (tr.numberofpoints != 0)
  {
    rMesh.SetNumberOfPoints(tr.numberofpoints);
  }

  if (tr.numberoftetrahedra != 0)
  {
    rMesh.SetNumberOfElements(tr.numberoftetrahedra);
  }

  std::cout<<" OutputMesh [nodes:"<<rMesh.GetNumberOfPoints()<<", elements:"<<rMesh.GetNumberOfElements()<<"]"<<std::endl;

  KRATOS_CATCH("")
}

//*******************************************************************************************
//*******************************************************************************************

void TetrahedralMesh3DMesher::DeleteInContainer(MesherData::MeshContainer &rMesh, tetgenio &tr)
{
  KRATOS_TRY

  //delete mesher container
  rMesh.Finalize();
  //ClearTetgenIO(tr); // blocks tetgen automatic destructor deletetion of a NULL pointer []
  //tr.deinitialize();
  //tr.initialize();

  KRATOS_CATCH("")
}

//*******************************************************************************************
//*******************************************************************************************

void TetrahedralMesh3DMesher::DeleteOutContainer(MesherData::MeshContainer &rMesh, tetgenio &tr)
{
  KRATOS_TRY

  //delete mesher container
  //rMesh.Finalize();
  //ClearTetgenIO(tr); // blocks tetgen automatic destructor deletetion of a NULL pointer []
  tr.deinitialize();
  tr.initialize();

  KRATOS_CATCH("")
}


//*******************************************************************************************
//*******************************************************************************************

void TetrahedralMesh3DMesher::CheckFaces(tetgenio &tr)
{
  KRATOS_TRY

  unsigned int faces = 0;
  for (int el = 0; el < tr.numberoftetrahedra; el++)
  {
    for (int fc = 0; fc < 4; fc++)
    {
      if (tr.neighborlist[el * 4 + fc] == -1)
        ++faces;
    }
  }

  std::cout<<" faces "<<faces<<std::endl;

  KRATOS_CATCH(" ")
}


//*******************************************************************************************
//*******************************************************************************************

void TetrahedralMesh3DMesher::WriteTetrahedra(tetgenio &tr)
{
  KRATOS_TRY

  std::cout << " Write Tetrahedra " << std::endl;
  for (int el = 0; el < tr.numberoftetrahedra; el++)
  {
    std::cout << "   TETRAHEDRON " << el << " : [ _";
    for (int pn = 0; pn < 4; pn++)
    {
      std::cout << tr.tetrahedronlist[el * 4 + pn] << "_";
    }
    std::cout << " ] " << std::endl; //  Volume: "<<tr.tetrahedronvolumelist[el]<<std::endl;
  }

  KRATOS_CATCH(" ")
}

//*******************************************************************************************
//*******************************************************************************************

void TetrahedralMesh3DMesher::WritePoints(tetgenio &tr)
{
  KRATOS_TRY

  int base = 0;
  std::cout << " numberofpoints " << tr.numberofpoints << " dimension " << tr.mesh_dim << std::endl;
  for (int nd = 0; nd < tr.numberofpoints; nd++)
  {
    std::cout << "   Point " << nd + 1 << " : [ ";
    std::cout << tr.pointlist[base] << " " << tr.pointlist[base + 1] << " " << tr.pointlist[base + 2] << " ]" << std::endl;
    ;
    base += 3;
  }

  KRATOS_CATCH(" ")
}

//*******************************************************************************************
//*******************************************************************************************

void TetrahedralMesh3DMesher::CheckPoints(tetgenio &tr)
{
  KRATOS_TRY

  int base = 0;
  for (int nd = 0; nd < tr.numberofpoints; nd++)
  {
    if(!std::isfinite(tr.pointlist[base]) && !std::isfinite(tr.pointlist[base+1]) && !std::isfinite(tr.pointlist[base+2]))
      KRATOS_ERROR << " InPoint is NaN  point [" << nd << "]" << std::endl;
    base += 3;
  }

  KRATOS_CATCH(" ")
}

//*******************************************************************************************
//*******************************************************************************************

void TetrahedralMesh3DMesher::CheckInOutPoints(tetgenio &in, tetgenio &out)
{
  KRATOS_TRY

  if (in.numberofpoints != out.numberofpoints)
    std::cout << "  Input and Output points amount is not the same : [in:" << in.numberofpoints << ",out:" << out.numberofpoints << "]" << std::endl;

  int base = 0;
  bool coincide = true;
  for (int nd = 0; nd < in.numberofpoints; nd++)
  {
    // std::cout<<"   Point "<<nd+1<<" : [ ";
    // std::cout<<in.pointlist[base]<<" "<<in.pointlist[base+1]<<" "<<in.pointlist[base+2]<<" ]"<<std::endl;
    // std::cout<<" ["<<out.pointlist[base]<<" "<<out.pointlist[base+1]<<" "<<out.pointlist[base+2]<<" ]"<<std::endl;

    if (fabs(in.pointlist[base] - out.pointlist[base]) > 1e-8)
      coincide = false;
    if (fabs(in.pointlist[base + 1] - out.pointlist[base + 1]) > 1e-8)
      coincide = false;
    if (fabs(in.pointlist[base + 2] - out.pointlist[base + 2]) > 1e-8)
      coincide = false;

    base += 3;
  }

  if (coincide == false)
    std::cout << "  Input and Output points not coincide " << std::endl;

  KRATOS_CATCH(" ")
}

//*******************************************************************************************
//*******************************************************************************************

void TetrahedralMesh3DMesher::ClearTetgenIO(tetgenio &tr)
{
  KRATOS_TRY

  tr.pointlist = (REAL *)NULL;
  tr.numberofpoints = 0;
  tr.numberofpointattributes = 0;

  tr.tetrahedronlist = (int *)NULL;
  tr.tetrahedronvolumelist = (REAL *)NULL;
  tr.neighborlist = (int *)NULL;
  tr.numberoftetrahedra = 0;

  KRATOS_CATCH(" ")
}

} // Namespace Kratos
