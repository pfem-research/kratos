//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:         July 2020 $
//
//

// System includes

// External includes

// Project includes
#include "custom_meshers/refine_data.hpp"

namespace Kratos
{

//refine options
KRATOS_CREATE_LOCAL_FLAG(RefineData, REFINE, 0);
KRATOS_CREATE_LOCAL_FLAG(RefineData, REFINE_ON_DISTANCE, 1);
KRATOS_CREATE_LOCAL_FLAG(RefineData, REFINE_ON_ERROR, 2);
KRATOS_CREATE_LOCAL_FLAG(RefineData, REFINE_ON_THRESHOLD, 3);
KRATOS_CREATE_LOCAL_FLAG(RefineData, COARSEN, 4);
KRATOS_CREATE_LOCAL_FLAG(RefineData, COARSEN_ON_DISTANCE, 5);
KRATOS_CREATE_LOCAL_FLAG(RefineData, COARSEN_ON_ERROR, 6);
KRATOS_CREATE_LOCAL_FLAG(RefineData, COARSEN_ON_THRESHOLD, 7);

} // namespace Kratos.
