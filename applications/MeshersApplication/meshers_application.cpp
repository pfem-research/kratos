//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

// System includes

// External includes

// Project includes
#include "includes/define.h"

#include "geometries/triangle_2d_3.h"
#include "geometries/triangle_2d_6.h"
#include "geometries/triangle_3d_3.h"

#include "geometries/tetrahedra_3d_4.h"
#include "geometries/tetrahedra_3d_10.h"

#include "geometries/line_2d_2.h"

#include "geometries/point_2d.h"
#include "geometries/point_3d.h"

#include "includes/element.h"
#include "includes/condition.h"

// Core applications
#include "meshers_application.h"

namespace Kratos
{
//Create Variables

KratosMeshersApplication::KratosMeshersApplication() : KratosApplication("MeshersApplication"),
                                                       mCompositeCondition2D2N(0, Kratos::make_shared<Line2D2<Node<3>>>(Condition::GeometryType::PointsArrayType(2))),
                                                       mCompositeCondition3D3N(0, Kratos::make_shared<Triangle3D3<Node<3>>>(Condition::GeometryType::PointsArrayType(3)))
{
}

void KratosMeshersApplication::Register()
{
  std::stringstream banner;

  banner << "            __  __         _                            \n"
         << "    KRATOS |  \\/  |___ ___| |_  ___ _ _ ___              \n"
         << "           | |\\/| / -_|_-<| ' \\/ -_| '_|_-<              \n"
         << "           |_|  |_\\___|__/|_||_\\___|_| /__/ APPLICATION  \n"
         << "Initialize KratosMeshersApplication..." << std::endl;

  // mpi initialization
  int mpi_is_initialized = 0;
  int rank = -1;

#ifdef KRATOS_MPI

  MPI_Initialized(&mpi_is_initialized);

  if (mpi_is_initialized)
  {
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  }

#endif

  if (mpi_is_initialized)
  {
    if (rank == 0)
      KRATOS_INFO("") << banner.str();
  }
  else
  {
    KRATOS_INFO("") << banner.str();
  }

  //Register Variables (variables created in meshers_application_variables.cpp)

  //geometrical definition
  KRATOS_REGISTER_3D_VARIABLE_WITH_COMPONENTS(OFFSET)
  KRATOS_REGISTER_VARIABLE(SHRINK_FACTOR)

  //domain definition
  KRATOS_REGISTER_VARIABLE(IS_AXISYMMETRIC)
  KRATOS_REGISTER_VARIABLE(INITIALIZED_DOMAINS)
  KRATOS_REGISTER_VARIABLE(MESHING_STEP_TIME)
  KRATOS_REGISTER_VARIABLE(RESTART_STEP_TIME)
  KRATOS_REGISTER_VARIABLE(MODEL_PART_NAME)
  KRATOS_REGISTER_VARIABLE(MODEL_PART_NAMES)

  //boundary definition
  KRATOS_REGISTER_VARIABLE(RIGID_WALL)

  //KRATOS_REGISTER_VARIABLE(MAIN_NODE)
  //KRATOS_REGISTER_VARIABLE(MAIN_ELEMENT)
  //KRATOS_REGISTER_VARIABLE(MAIN_CONDITION)

  KRATOS_REGISTER_VARIABLE(PRIMARY_NODES)
  KRATOS_REGISTER_VARIABLE(PRIMARY_ELEMENTS)
  KRATOS_REGISTER_VARIABLE(PRIMARY_CONDITIONS)
  KRATOS_REGISTER_VARIABLE(SECONDARY_CONDITIONS)

  //condition variables
  KRATOS_REGISTER_VARIABLE(CHILDREN_CONDITIONS)

  //mesher criteria
  KRATOS_REGISTER_VARIABLE(MEAN_ERROR)

  //ALE Variables
  KRATOS_REGISTER_3D_VARIABLE_WITH_COMPONENTS(MESH_DISPLACEMENT)
  KRATOS_REGISTER_3D_VARIABLE_WITH_COMPONENTS(MESH_ACCELERATION)

  //Register Conditions
  KRATOS_REGISTER_CONDITION("CompositeCondition2D2N", mCompositeCondition2D2N)
  KRATOS_REGISTER_CONDITION("CompositeCondition3D3N", mCompositeCondition3D3N)

  //Register bounding boxes
  Serializer::Register("SpatialBoundingBox", mSpatialBoundingBox);

}

} // namespace Kratos.
