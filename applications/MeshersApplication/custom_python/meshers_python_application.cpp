//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

// System includes
#if defined(KRATOS_PYTHON)

// External includes

// Project includes
#include "custom_python/add_custom_processes_to_python.h"
#include "custom_python/add_custom_utilities_to_python.h"
#include "custom_python/add_custom_meshers_to_python.h"
#include "custom_python/add_custom_bounding_to_python.h"

#include "meshers_application.h"

namespace Kratos
{

namespace Python
{

namespace py = pybind11;

PYBIND11_MODULE(KratosMeshersApplication, m)
{

  py::class_<KratosMeshersApplication,
             KratosMeshersApplication::Pointer,
             KratosApplication>(m, "KratosMeshersApplication")
      .def(py::init<>());

  AddCustomProcessesToPython(m);
  AddCustomUtilitiesToPython(m);
  AddCustomMeshersToPython(m);
  AddCustomBoundingToPython(m);

  //registering variables in python ( if must to be seen from python )

  KRATOS_REGISTER_IN_PYTHON_VARIABLE(m, INITIALIZED_DOMAINS)
  KRATOS_REGISTER_IN_PYTHON_VARIABLE(m, MESHING_STEP_TIME)
  KRATOS_REGISTER_IN_PYTHON_VARIABLE(m, RESTART_STEP_TIME)
  KRATOS_REGISTER_IN_PYTHON_VARIABLE(m, RIGID_WALL)
  KRATOS_REGISTER_IN_PYTHON_VARIABLE(m, MEAN_ERROR)
  KRATOS_REGISTER_IN_PYTHON_VARIABLE(m, OFFSET)
  KRATOS_REGISTER_IN_PYTHON_VARIABLE(m, SHRINK_FACTOR)
}

} // namespace Python.

} // namespace Kratos.

#endif // KRATOS_PYTHON defined
