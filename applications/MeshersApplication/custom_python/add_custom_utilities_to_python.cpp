//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

// System includes

// External includes

// Project includes
#include "custom_python/add_custom_utilities_to_python.h"

//Utilities
#include "custom_utilities/boundary_normals_utilities.hpp"
#include "custom_utilities/model_part_utilities.hpp"
#include "custom_utilities/set_flags_utilities.hpp"
#include "custom_utilities/transfer_utilities.hpp"
#include "custom_utilities/execution_control_utilities.hpp"

namespace Kratos
{

namespace Python
{

// transfer methods
void SetDoubleVariable(TransferUtilities::DataVariables &rDataVariables, const Variable<double> &rVariable)
{
  rDataVariables.SetVariable(rVariable);
}

void SetArray1DVariable(TransferUtilities::DataVariables &rDataVariables, const Variable<array_1d<double, 3>> &rVariable)
{
  rDataVariables.SetVariable(rVariable);
}

void SetVectorVariable(TransferUtilities::DataVariables &rDataVariables, const Variable<Vector> &rVariable)
{
  rDataVariables.SetVariable(rVariable);
}

void SetMatrixVariable(TransferUtilities::DataVariables &rDataVariables, const Variable<Matrix> &rVariable)
{
  rDataVariables.SetVariable(rVariable);
}

void AddCustomUtilitiesToPython(pybind11::module &m)
{

  namespace py = pybind11;


  //***************MODEL PART UTILITIES************//
  py::class_<ModelPartUtilities>(m, "ModelPartUtilities")
    .def(py::init<>())
    .def_static("SetModelPartNameToEntities", &ModelPartUtilities::SetModelPartNameToEntities)
    .def_static("CalculateSmallerMeshSize", &ModelPartUtilities::CalculateSmallerMeshSize)
    .def_static("CalculateModelPartVolume", &ModelPartUtilities::CalculateModelPartVolume)
    .def_static("SetGravityInProcessInfo", &ModelPartUtilities::SetGravityInProcessInfo)
    .def_static("GetMaxPropertiesId", &ModelPartUtilities::GetMaxPropertiesId);

  //***************NORMALS UTILITIES**************//

  py::class_<BoundaryNormalsUtilities>(m, "BoundaryNormals")
    .def(py::init<>())
    .def("CalculateWeightedBoundaryNormals", &BoundaryNormalsUtilities::CalculateWeightedBoundaryNormals)
    .def("CalculateUnitBoundaryNormals", &BoundaryNormalsUtilities::CalculateUnitBoundaryNormals)
    .def("CheckWallNormalsAndRepair", &BoundaryNormalsUtilities::CheckWallNormalsAndRepair);


  //**************TRANSFER UTILITIES*************//

  py::class_<TransferUtilities>(m, "TransferUtilities")
    .def(py::init<>())
    .def_static("SmoothElementalValues", &TransferUtilities::SmoothElementalValues)
    .def_static("SmoothElementalValuesOnThreshold", &TransferUtilities::SmoothElementalValuesOnThreshold)
    .def_static("SmoothSPRElementalValues", &TransferUtilities::SmoothSPRElementalValues)
    .def_static("SmoothSPRElementalValuesOnThreshold", &TransferUtilities::SmoothSPRElementalValuesOnThreshold)
    .def_static("TransferElementalValuesToNodes", &TransferUtilities::TransferElementalValuesToNodes)
    .def_static("TransferNodalValuesToElements", &TransferUtilities::TransferNodalValuesToElements)
    .def_static("TransferNodalValuesToElementsOnThreshold", &TransferUtilities::TransferNodalValuesToElementsOnThreshold);


  // Data transfer variables
  py::class_<TransferUtilities::DataVariables, typename TransferUtilities::DataVariables::Pointer>(m, "DataVariables")
    .def(py::init<>())
    .def("Initialize", &TransferUtilities::DataVariables::Initialize)
    .def("Set", &TransferUtilities::DataVariables::Set)
    .def("Reset", &TransferUtilities::DataVariables::Reset)
    .def("SetOptions", &TransferUtilities::DataVariables::SetOptions)
    .def("GetOptions", &TransferUtilities::DataVariables::GetOptions)
    .def("SetVariable", SetDoubleVariable)
    .def("SetVariable", SetArray1DVariable)
    .def("SetVariable", SetVectorVariable)
    .def("SetVariable", SetMatrixVariable);

  // Execution control utility
  py::class_<ExecutionControl>(m, "ExecutionControl")
    .def(py::init<>())
    .def(py::init<ProcessInfo&, std::string, double>())
    .def("Initialize", &ExecutionControl::Initialize)
    .def("Update", &ExecutionControl::Update)
    .def("GetExecutionTime", &ExecutionControl::GetExecutionTime)
    .def("GetExecutionStep", &ExecutionControl::GetExecutionStep)
    .def("SetNextExecution", &ExecutionControl::SetNextExecution)
    .def("IsExecutionStep", &ExecutionControl::IsExecutionStep)
    .def("IsRestarted", &ExecutionControl::IsRestarted);

}

} // namespace Python.

} // Namespace Kratos
