//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

// System includes

// External includes

// Project includes
#include "custom_python/add_custom_meshers_to_python.h"

// Meshers
#include "custom_meshers/triangular_mesh_2D_mesher.hpp"
#include "custom_meshers/tetrahedral_mesh_3D_mesher.hpp"

// Data
#include "custom_meshers/mesher_data.hpp"

namespace Kratos
{

namespace Python
{

// remeshing methods
void SetReferenceElement(MesherData::MeshingParameters &rMeshingParameters, char *ElementName)
{
  rMeshingParameters.SetReferenceElement(KratosComponents<Element>::Get(ElementName));
}

void SetReferenceCondition(MesherData::MeshingParameters &rMeshingParameters, char *ConditionName)
{
  rMeshingParameters.SetReferenceCondition(KratosComponents<Condition>::Get(ConditionName));
}

void AddCustomMeshersToPython(pybind11::module &m)
{
  namespace py = pybind11;

  //class that allos remeshing and adaptive refining (inserting and erasing nodes)
  py::class_<Mesher, typename Mesher::Pointer>(m, "Mesher")
    .def(py::init<>())
    .def("Initialize", &Mesher::Initialize)
    .def("InitializeMesher", &Mesher::InitializeMesher)
    .def("FinalizeMesher", &Mesher::FinalizeMesher)
    .def("SetMeshingParameters", &Mesher::SetMeshingParameters)
    .def("SetPreMeshingProcess", &Mesher::SetPreMeshingProcess)
    .def("SetPostMeshingProcess", &Mesher::SetPostMeshingProcess)
    .def("SetPreMeshingProcessVector", &Mesher::SetPreMeshingProcessVector)
    .def("SetPostMeshingProcessVector", &Mesher::SetPostMeshingProcessVector)
    .def("SetEchoLevel", &Mesher::SetEchoLevel)
    .def("ExecuteMeshing", &Mesher::ExecuteMeshing);

  //class that allows 3D adaptive remeshing (inserting and erasing nodes)
  py::class_<TetrahedralMesh3DMesher, typename TetrahedralMesh3DMesher::Pointer, Mesher>(m, "TetrahedralMesh3DMesher")
    .def(py::init<>());

  //class that allows 2D adaptive remeshing (inserting and erasing nodes)
  py::class_<TriangularMesh2DMesher, typename TriangularMesh2DMesher::Pointer, Mesher>(m, "TriangularMesh2DMesher")
    .def(py::init<>());


  //mesher data flags
  py::class_<MesherData>(m, "MesherData")
    .def(py::init<>())
    .def_readonly_static("REMESH", &MesherData::REMESH)
    .def_readonly_static("REFINE", &MesherData::REFINE)
    .def_readonly_static("RECONNECT", &MesherData::RECONNECT)
    .def_readonly_static("CONSTRAINED", &MesherData::CONSTRAINED)
    .def_readonly_static("CONTACT_SEARCH", &MesherData::CONTACT_SEARCH)
    .def_readonly_static("MESH_SMOOTHING", &MesherData::MESH_SMOOTHING)
    .def_readonly_static("SPR_TRANSFER", &MesherData::SPR_TRANSFER)

    .def_readonly_static("INITIALIZE_MESHER_INPUT", &MesherData::INITIALIZE_MESHER_INPUT)
    .def_readonly_static("FINALIZE_MESHER_INPUT", &MesherData::FINALIZE_MESHER_INPUT)
    .def_readonly_static("TRANSFER_KRATOS_NODES_TO_MESHER", &MesherData::TRANSFER_KRATOS_NODES_TO_MESHER)
    .def_readonly_static("TRANSFER_KRATOS_ELEMENTS_TO_MESHER", &MesherData::TRANSFER_KRATOS_ELEMENTS_TO_MESHER)
    .def_readonly_static("TRANSFER_KRATOS_NEIGHBOURS_TO_MESHER", &MesherData::TRANSFER_KRATOS_NEIGHBOURS_TO_MESHER)
    .def_readonly_static("TRANSFER_KRATOS_FACES_TO_MESHER", &MesherData::TRANSFER_KRATOS_FACES_TO_MESHER)

    .def_readonly_static("SELECT_TESSELLATION_ELEMENTS", &MesherData::SELECT_TESSELLATION_ELEMENTS)
    .def_readonly_static("KEEP_ISOLATED_NODES", &MesherData::KEEP_ISOLATED_NODES);

  //mesher info parameters
  py::class_<MesherData::MeshingInfoParameters, typename MesherData::MeshingInfoParameters::Pointer>(m, "MeshingInfoParameters")
    .def(py::init<>())
    .def("Initialize", &MesherData::MeshingInfoParameters::Initialize)
    .def("Clear", &MesherData::MeshingInfoParameters::Clear)
    .def("CheckMechanicalSmoothing", &MesherData::MeshingInfoParameters::CheckMechanicalSmoothing)
    .def("SetNumberOfNodes", &MesherData::MeshingInfoParameters::SetNumberOfNodes)
    .def("SetNumberOfElements", &MesherData::MeshingInfoParameters::SetNumberOfElements)
    .def("SetNumberOfConditions", &MesherData::MeshingInfoParameters::SetNumberOfConditions)
    .def("SetNumberOfNewNodes", &MesherData::MeshingInfoParameters::SetNumberOfNewNodes)
    .def("SetNumberOfNewElements", &MesherData::MeshingInfoParameters::SetNumberOfNewElements)
    .def("SetNumberOfNewConditions", &MesherData::MeshingInfoParameters::SetNumberOfNewConditions)
    .def("GetNumberOfNodes", &MesherData::MeshingInfoParameters::GetNumberOfNodes)
    .def("GetNumberOfElements", &MesherData::MeshingInfoParameters::GetNumberOfElements)
    .def("GetNumberOfConditions", &MesherData::MeshingInfoParameters::GetNumberOfConditions)
    .def("GetTessellationInsertion", &MesherData::MeshingInfoParameters::GetTessellationInsertion);

  //refine parameters
  py::class_<RefineData, RefineData::Pointer>(m, "RefineData")
    .def(py::init<>())
    .def(py::init<Parameters,Parameters>())
    .def("SetRefineBox", &RefineData::SetRefineBox);

  //meshing parameters
  py::class_<MesherData::MeshingParameters, typename MesherData::MeshingParameters::Pointer>(m, "MeshingParameters")
    .def(py::init<>())
    .def("Initialize", &MesherData::MeshingParameters::Initialize)
    .def("Set", &MesherData::MeshingParameters::Set)
    .def("Reset", &MesherData::MeshingParameters::Reset)
    .def("SetOptions", &MesherData::MeshingParameters::SetOptions)
    .def("SetSubModelPartName", &MesherData::MeshingParameters::SetSubModelPartName)
    .def("SetExecutionOptions", &MesherData::MeshingParameters::SetExecutionOptions)
    .def("SetSmoothingControl", &MesherData::MeshingParameters::SetSmoothingControl)
    .def("SetTessellationFlags", &MesherData::MeshingParameters::SetTessellationFlags)
    .def("SetTessellationInfo", &MesherData::MeshingParameters::SetTessellationInfo)
    .def("SetAlphaParameter", &MesherData::MeshingParameters::SetAlphaParameter)
    .def("SetOffsetFactor", &MesherData::MeshingParameters::SetOffsetFactor)
    .def("SetScaleFactor", &MesherData::MeshingParameters::SetScaleFactor)
    .def("SetInfoParameters", &MesherData::MeshingParameters::SetInfoParameters)
    .def("SetRefineData", &MesherData::MeshingParameters::SetRefineData)
    .def("SetRefineItem", &MesherData::MeshingParameters::SetRefineDataItem)
    .def("SetProperties", &MesherData::MeshingParameters::SetProperties)
    .def("SetTransferVariables", &MesherData::MeshingParameters::SetTransferVariables)
    .def("SetMeshingBox", &MesherData::MeshingParameters::SetMeshingBox)
    .def("SetReferenceElement", SetReferenceElement)
    .def("SetReferenceCondition", SetReferenceCondition)
    .def("GetInfoParameters", &MesherData::MeshingParameters::GetInfoParameters)
    .def("GetRefineData", &MesherData::MeshingParameters::GetRefineData)
    .def("GetRefineDataVector", &MesherData::MeshingParameters::GetRefineDataVector)
    .def("GetSubModelPartName", &MesherData::MeshingParameters::GetSubModelPartName)
    .def("GetOptions", &MesherData::MeshingParameters::GetOptions)
    .def("InitializeMeshing", &MesherData::MeshingParameters::InitializeMeshing)
    .def("FinalizeMeshing", &MesherData::MeshingParameters::FinalizeMeshing);

}

} // namespace Python.

} // Namespace Kratos
