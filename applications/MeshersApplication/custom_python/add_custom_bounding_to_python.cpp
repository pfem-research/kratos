//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

// System includes

// External includes

// Project includes
#include "custom_python/add_custom_bounding_to_python.h"

// Bounding Boxes
#include "custom_bounding/spatial_bounding_box.hpp"

namespace Kratos
{

namespace Python
{
typedef SpatialBoundingBox::Pointer BoundingBoxPointerType;
typedef std::vector<SpatialBoundingBox::Pointer> BoundingBoxContainerType;

void Push_Back_Bounding_Box(BoundingBoxContainerType &ThisBoundingBoxContainer,
                            BoundingBoxPointerType ThisBoundingBox)
{
  ThisBoundingBoxContainer.push_back(ThisBoundingBox);
}

void AddCustomBoundingToPython(pybind11::module &m)
{

  namespace py = pybind11;

  //bounding box container
  py::class_<BoundingBoxContainerType>(m, "BoundingBoxContainer")
      .def(py::init<>())
      .def("PushBack", Push_Back_Bounding_Box);

  //spatial bounding box
  py::class_<SpatialBoundingBox, BoundingBoxPointerType>(m, "SpatialBoundingBox")
      .def(py::init<>())
      .def(py::init<ModelPart &, Parameters>())
      .def(py::init<Parameters>())
      .def(py::init<Vector, Vector>())
      .def("SetAxisymmetric", &SpatialBoundingBox::SetAxisymmetric)
      .def("SetDimension", &SpatialBoundingBox::SetDimension)
      .def("SetUpperPoint", &SpatialBoundingBox::SetUpperPoint)
      .def("SetLowerPoint", &SpatialBoundingBox::SetLowerPoint)
      .def("SetRigidBodyCenter", &SpatialBoundingBox::SetRigidBodyCenter)
      .def("CreateBoundingBoxBoundaryMesh", &SpatialBoundingBox::CreateBoundingBoxBoundaryMesh)
      .def("UpdateBoxPosition", &SpatialBoundingBox::UpdateBoxPosition)
      .def("Clone", &SpatialBoundingBox::Clone)
      .def("__repr__", &SpatialBoundingBox::Info)
      DECLARE_HAS_THIS_TYPE_PROPERTIES_PYTHON_AS_POINTER(SpatialBoundingBox)
      DECLARE_ADD_THIS_TYPE_TO_PROPERTIES_PYTHON_AS_POINTER(SpatialBoundingBox)
      DECLARE_GET_THIS_TYPE_FROM_PROPERTIES_PYTHON_AS_POINTER(SpatialBoundingBox);

  //to define it as a variable
  py::class_<Variable<BoundingBoxPointerType>, VariableData>(m, "SpatialBoundingBoxVariable")
      .def("__repr__", &Variable<BoundingBoxPointerType>::Info);
}

} // namespace Python.

} // Namespace Kratos
