//--------------------------------------------------------
//        __  __         _                               .
// KRATOS|  \/  |___ ___| |_  ___ _ _ ___                .
//       | |\/| / -_|_-<| ' \/ -_| '_|_-<                .
//       |_|  |_\___|__/|_||_\___|_| /__/ APPLICATION    .
//                                                       .
//  License:(BSD)   MeshersApplication/license.txt       .
//  Main authors:   Josep Maria Carbonell                .
//                        ..                             .
//--------------------------------------------------------
//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

#if !defined(KRATOS_MESHERS_APPLICATION_H_INCLUDED)
#define KRATOS_MESHERS_APPLICATION_H_INCLUDED

// System includes

// External includes

// Project includes

// Core applications

//conditions
#include "custom_conditions/composite_condition.hpp"
#include "custom_bounding/spatial_bounding_box.hpp"

#include "custom_bounding/spatial_bounding_box.hpp"

#include "includes/kratos_application.h"

#include "meshers_application_variables.h"

namespace Kratos
{
///@name Type	Definitions
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
class KRATOS_API(MESHERS_APPLICATION) KratosMeshersApplication : public KratosApplication
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of KratosMeshersApplication
  KRATOS_CLASS_POINTER_DEFINITION(KratosMeshersApplication);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  KratosMeshersApplication();

  /// Destructor.
  virtual ~KratosMeshersApplication() {}

  ///@}
  ///@name Operators
  ///@{
  ///@}
  ///@name Operations
  ///@{

  void Register() override;

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "KratosMeshersApplication";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << Info();
    PrintData(rOStream);
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
    KRATOS_WATCH("in KratosMeshersApplication")
    KRATOS_WATCH(KratosComponents<VariableData>::GetComponents().size())
    rOStream << "Variables:" << std::endl;
    KratosComponents<VariableData>().PrintData(rOStream);
    rOStream << std::endl;
    rOStream << "Conditions:" << std::endl;
    KratosComponents<Condition>().PrintData(rOStream);
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{
  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{

  const CompositeCondition mCompositeCondition2D2N;
  const CompositeCondition mCompositeCondition3D3N;

  //bounding boxes
  const SpatialBoundingBox mSpatialBoundingBox;

  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{

  /// Assignment operator.
  KratosMeshersApplication &operator=(KratosMeshersApplication const &rOther);

  /// Copy constructor.
  KratosMeshersApplication(KratosMeshersApplication const &rOther);

  ///@}

}; // Class KratosMeshersApplication

///@}

///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
///@}

} // namespace Kratos.

#endif // KRATOS_MESHERS_APPLICATION_H_INCLUDED  defined
