//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:         June 2020 $
//
//

#if !defined(KRATOS_BOUNDING_BOX_DATA_HPP_INCLUDED)
#define KRATOS_BOUNDING_BOX_DATA_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "meshers_application_variables.h"

namespace Kratos
{
    ///@addtogroup MeshersApplication
    ///@{
    ///@name Kratos Globals
    ///@{
    ///@}
    ///@name Type Definitions
    ///@{
    ///@}
    ///@name  Enum's
    ///@{
    ///@}
    ///@name  Functions
    ///@{
    ///@}
    ///@name Kratos Classes
    ///@{

    /// Short class definition.
    /** Detail class definition.
     */
    class KRATOS_API(MESHERS_APPLICATION) BoundingBoxData
    {
    public:
        ///@name Type Definitions
        ///@{
        typedef array_1d<double, 3> PointType;
        typedef ModelPart::NodeType NodeType;
        typedef ModelPart::NodesContainerType NodesContainerType;
        typedef NodesContainerType::Pointer NodesContainerTypePointer;
        typedef BeamMathUtils<double> BeamMathUtilsType;
        typedef Quaternion<double> QuaternionType;

        struct BoxParameters
        {
            KRATOS_CLASS_POINTER_DEFINITION(BoxParameters);

        private:
            const NodeType *mpNode;

            PointType *mpNormal;
            PointType *mpTangent;
            PointType *mpRelativeDisplacement;

            double *mpGapNormal;
            double *mpGapTangent;

            double mRadius;
            int mContactFace;

        public:
            /**
             * Constructor.
             */
            BoxParameters()
            {
                //Initialize pointers to NULL
                mpNode = NULL;

                mpNormal = NULL;
                mpTangent = NULL;
                mpRelativeDisplacement = NULL;

                mpGapNormal = NULL;
                mpGapTangent = NULL;

                mRadius = 0.0;
                mContactFace = 0;
            };

            /**
             * Constructor with Node
             */
            BoxParameters(const NodeType &rNode)
            {
                mpNode = &rNode;
                mpNormal = NULL;
                mpTangent = NULL;

                mpRelativeDisplacement = NULL;

                mpGapNormal = NULL;
                mpGapTangent = NULL;

                mRadius = 0.0;
                mContactFace = 0;
            };

            /**
             * Constructor with Node and Contact Parameters
             */
            BoxParameters(const NodeType &rNode, double &rGapNormal, double &rGapTangent, PointType &rNormal, PointType &rTangent, PointType &rDisplacement)
            {
                mpNode = &rNode;
                mpNormal = &rNormal;
                mpTangent = &rTangent;

                mpRelativeDisplacement = &rDisplacement;

                mpGapNormal = &rGapNormal;
                mpGapTangent = &rGapTangent;

                mRadius = 0.0;
                mContactFace = 0;
            };

            //set parameters
            void SetNode(const NodeType &rNode) { mpNode = &rNode; };
            void SetNormal(PointType &rNormal) { mpNormal = &rNormal; };
            void SetTangent(PointType &rTangent) { mpTangent = &rTangent; };
            void SetRelativeDisplacement(PointType &rDisplacement) { mpRelativeDisplacement = &rDisplacement; };

            void SetGapNormal(double &rGapNormal) { mpGapNormal = &rGapNormal; };
            void SetGapTangent(double &rGapTangent) { mpGapTangent = &rGapTangent; };

            void SetRadius(double Radius) { mRadius = Radius; };
            void SetContactFace(int ContactFace) { mContactFace = ContactFace; };

            //get parameters
            const NodeType &GetNode() { return *mpNode; };
            const PointType &GetVelocity() { return mpNode->FastGetSolutionStepValue(VELOCITY); };
            const PointType &GetCurrentDisplacement() { return mpNode->FastGetSolutionStepValue(DISPLACEMENT, 0); };
            const PointType &GetPreviousDisplacement() { return mpNode->FastGetSolutionStepValue(DISPLACEMENT, 1); };

            PointType GetDeltaDisplacement() const { return (mpNode->FastGetSolutionStepValue(DISPLACEMENT, 0) - mpNode->FastGetSolutionStepValue(DISPLACEMENT, 1)); };

            PointType &GetNormal() { return *mpNormal; };
            PointType &GetTangent() { return *mpTangent; };
            PointType &GetRelativeDisplacement() { return *mpRelativeDisplacement; };

            double &GetGapNormal() { return *mpGapNormal; };
            double &GetGapTangent() { return *mpGapTangent; };

            double &GetRadius() { return mRadius; };
            int &GetContactFace() { return mContactFace; };

        }; // struct BoxParameters end

        struct BoxVariables
        {

            KRATOS_CLASS_POINTER_DEFINITION(BoxVariables);

        public:

            int Dimension;     // 2D or 3D
            bool Axisymmetric; // true or false
            int Convexity;     // 1 or -1  if "in" is inside or outside respectively
            double Radius;     // box radius

            PointType InitialUpperPoint; // box highest point
            PointType InitialLowerPoint; // box lowest point
            PointType InitialCenter;     // center current position

            PointType UpperPoint; // box highest point
            PointType LowerPoint; // box lowest point
            PointType Center;     // center current position

            PointType Velocity;        // box velocity
            PointType AngularVelocity; // box rotation

            QuaternionType InitialLocalQuaternion; //initial local axes for the box
            QuaternionType LocalQuaternion;        //local axes for the box

            void Initialize()
            {
                Dimension = 3;
                Axisymmetric = false;
                Convexity = 1;
                Radius = 0;

                UpperPoint.clear();
                LowerPoint.clear();
                Center.clear();

                InitialUpperPoint.clear();
                InitialLowerPoint.clear();
                InitialCenter.clear();

                Velocity.clear();
                AngularVelocity.clear();

                Matrix InitialLocalMatrix = IdentityMatrix(3);
                InitialLocalQuaternion = QuaternionType::FromRotationMatrix(InitialLocalMatrix);
                LocalQuaternion = QuaternionType::FromRotationMatrix(InitialLocalMatrix);
            }

            void SetInitialValues()
            {
                InitialUpperPoint = UpperPoint;
                InitialLowerPoint = LowerPoint;
                InitialCenter = Center;
            }

            void UpdatePosition(PointType &Displacement)
            {
                UpperPoint = InitialUpperPoint + Displacement;
                LowerPoint = InitialLowerPoint + Displacement;
                Center = InitialCenter + Displacement;
            }

            void Print()
            {
                std::cout << " [--SPATIAL-BOX--] " << std::endl;
                std::cout << "  [Center:" << Center << std::endl;
                std::cout << "  [UpperPoint:" << UpperPoint << std::endl;
                std::cout << "  [LowerPoint:" << LowerPoint << std::endl;
                std::cout << " [--------] " << std::endl;
            }

        private:

            friend class Serializer;

            void save(Serializer &rSerializer) const
            {
                rSerializer.save("Dimension", Dimension);
                rSerializer.save("Axisymmetric", Axisymmetric);
                rSerializer.save("Convexity", Convexity);
                rSerializer.save("Radius", Radius);
                rSerializer.save("InitialUpperPoint", InitialUpperPoint);
                rSerializer.save("InitialLowerPoint", InitialLowerPoint);
                rSerializer.save("InitialCenter", InitialCenter);
                rSerializer.save("UpperPoint", UpperPoint);
                rSerializer.save("LowerPoint", LowerPoint);
                rSerializer.save("Center", Center);
                rSerializer.save("Velocity", Velocity);
                rSerializer.save("AngularVelocity", AngularVelocity);
                rSerializer.save("InitialLocalQuaternion", InitialLocalQuaternion);
                rSerializer.save("LocalQuaternion", LocalQuaternion);
            }

            void load(Serializer &rSerializer)
            {
                rSerializer.load("Dimension", Dimension);
                rSerializer.load("Axisymmetric", Axisymmetric);
                rSerializer.load("Convexity", Convexity);
                rSerializer.load("Radius", Radius);
                rSerializer.load("InitialUpperPoint", InitialUpperPoint);
                rSerializer.load("InitialLowerPoint", InitialLowerPoint);
                rSerializer.load("InitialCenter", InitialCenter);
                rSerializer.load("UpperPoint", UpperPoint);
                rSerializer.load("LowerPoint", LowerPoint);
                rSerializer.load("Center", Center);
                rSerializer.load("Velocity", Velocity);
                rSerializer.load("AngularVelocity", AngularVelocity);
                rSerializer.load("InitialLocalQuaternion", InitialLocalQuaternion);
                rSerializer.load("LocalQuaternion", LocalQuaternion);
            }

        };

        /// Pointer definition of BoundingBoxData
        KRATOS_CLASS_POINTER_DEFINITION(BoundingBoxData);

        ///@}
        ///@name Life Cycle
        ///@{

        /// Default constructor.
        BoundingBoxData() {}

        /// Copy constructor.
        BoundingBoxData(BoundingBoxData const &rOther) {}

        /// Clone.
        BoundingBoxData::Pointer Clone() const
        {
            return Kratos::make_shared<BoundingBoxData>(*this);
        }

        /// Destructor.
        virtual ~BoundingBoxData() {}

        ///@}
        ///@name Operators
        ///@{
        ///@}
        ///@name Operations
        ///@{
        ///@}
        ///@name Access
        ///@{
        ///@}
        ///@name Inquiry
        ///@{
        ///@}
        ///@name Input and output
        ///@{

        /// Turn back information as a string.
        virtual std::string Info() const
        {
            std::stringstream buffer;
            buffer << "BoundingBoxData";
            return buffer.str();
        }

        /// Print information about this object.
        virtual void PrintInfo(std::ostream &rOStream) const
        {
            rOStream << "BoundingBoxData";
        }

        /// Print object's data.
        virtual void PrintData(std::ostream &rOStream) const
        {
            rOStream << "BoundingBoxData Data";
        }

        ///@}
        ///@name Friends
        ///@{
        ///@}

    protected:
        ///@name Protected static Member Variables
        ///@{
        ///@}
        ///@name Protected member Variables
        ///@{
        ///@}
        ///@name Protected Operators
        ///@{
        ///@}
        ///@name Protected Operations
        ///@{
        ///@}
        ///@name Protected  Access
        ///@{
        ///@}
        ///@name Protected Inquiry
        ///@{
        ///@}
        ///@name Protected LifeCycle
        ///@{
        ///@}

    private:
        ///@name Static Member Variables
        ///@{
        ///@}
        ///@name Member Variables
        ///@{
        ///@}
        ///@name Private Operators
        ///@{
        ///@}
        ///@name Private Operations
        ///@{
        ///@}
        ///@name Private  Access
        ///@{
        ///@}
        ///@name Private Inquiry
        ///@{
        ///@}
        ///@name Un accessible methods
        ///@{
        ///@}

    }; // Class BoundingBoxData

    ///@}

    ///@name Type Definitions
    ///@{
    ///@}
    ///@name Input and output
    ///@{

    /// input stream function
    inline std::istream &operator>>(std::istream &rIStream,
                                    BoundingBoxData &rThis);

    /// output stream function
    inline std::ostream &operator<<(std::ostream &rOStream,
                                    const BoundingBoxData &rThis)
    {
        rThis.PrintInfo(rOStream);
        rOStream << " : " << std::endl;
        rThis.PrintData(rOStream);

        return rOStream;
    }
    ///@}
    ///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_BOUNDING_BOX_DATA_HPP_INCLUDED  defined
