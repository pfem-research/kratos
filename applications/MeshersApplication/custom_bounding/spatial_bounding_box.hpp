//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

#if !defined(KRATOS_SPATIAL_BOUNDING_BOX_HPP_INCLUDED)
#define KRATOS_SPATIAL_BOUNDING_BOX_HPP_INCLUDED

// System includes
#include <limits>

#include "includes/kratos_parameters.h"
#include "includes/model_part.h"
#include "utilities/beam_math_utilities.hpp"
#include "custom_utilities/model_part_utilities.hpp"
#include "custom_utilities/mesher_properties_extensions.hpp"
#include "custom_bounding/bounding_box_data.hpp"

namespace Kratos
{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
*/

class KRATOS_API(MESHERS_APPLICATION) SpatialBoundingBox
{
public:
  typedef array_1d<double, 3> PointType;
  typedef ModelPart::NodeType NodeType;
  typedef ModelPart::NodesContainerType NodesContainerType;
  typedef NodesContainerType::Pointer NodesContainerTypePointer;
  typedef BeamMathUtils<double> BeamMathUtilsType;
  typedef Quaternion<double> QuaternionType;

  typedef BoundingBoxData::BoxParameters BoxParametersType;
  typedef BoundingBoxData::BoxVariables  BoxVariablesType;

public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of SpatialBoundingBox
  KRATOS_CLASS_POINTER_DEFINITION(SpatialBoundingBox);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  SpatialBoundingBox()
  {
    KRATOS_TRY

    mBox.Initialize();
    mRigidBodyCenterSupplied = false;
    //std::cout<< " Calling Bounding Box empty constructor" <<std::endl;

    KRATOS_CATCH("")
  }


  //**************************************************************************
  //**************************************************************************

  SpatialBoundingBox(ModelPart &rModelPart, Parameters CustomParameters) : SpatialBoundingBox(CustomParameters)
  {
  }

  //**************************************************************************
  //**************************************************************************

  SpatialBoundingBox(Parameters CustomParameters)
  {

    KRATOS_TRY

    Parameters DefaultParameters(R"(
    {
        "parameters_list":[{
              "upper_point": [0.0, 0.0, 0.0],
              "lower_point": [0.0, 0.0, 0.0],
              "convexity": 1
         }],
         "plane_size": 1.0,
         "velocity": [0.0, 0.0, 0.0],
         "angular_velocity": [0.0, 0.0, 0.0],
         "local_axes":[ [1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0] ]
    }  )");

    //validate against defaults -- this also ensures no type mismatch
    CustomParameters.ValidateAndAssignDefaults(DefaultParameters);

    if (CustomParameters["parameters_list"].IsArray() == true && CustomParameters["parameters_list"].size() != 1)
    {
      KRATOS_THROW_ERROR(std::runtime_error, "paramters_list for the Spatial BBX must contain only one term", CustomParameters.PrettyPrintJsonString());
    }

    mBox.Initialize();

    Parameters BoxParameters = CustomParameters["parameters_list"][0];

    mBox.UpperPoint[0] = BoxParameters["upper_point"][0].GetDouble();
    mBox.UpperPoint[1] = BoxParameters["upper_point"][1].GetDouble();
    mBox.UpperPoint[2] = BoxParameters["upper_point"][2].GetDouble();

    mBox.LowerPoint[0] = BoxParameters["lower_point"][0].GetDouble();
    mBox.LowerPoint[1] = BoxParameters["lower_point"][1].GetDouble();
    mBox.LowerPoint[2] = BoxParameters["lower_point"][2].GetDouble();

    mBox.Center = 0.5 * (mBox.UpperPoint + mBox.LowerPoint);
    mBox.Radius = 0.5 * norm_2(mBox.UpperPoint - mBox.LowerPoint);

    mBox.Velocity[0] = CustomParameters["velocity"][0].GetDouble();
    mBox.Velocity[1] = CustomParameters["velocity"][1].GetDouble();
    mBox.Velocity[2] = CustomParameters["velocity"][2].GetDouble();

    mBox.AngularVelocity[0] = CustomParameters["angular_velocity"][0].GetDouble();
    mBox.AngularVelocity[1] = CustomParameters["angular_velocity"][1].GetDouble();
    mBox.AngularVelocity[2] = CustomParameters["angular_velocity"][2].GetDouble();

    mBox.Convexity = BoxParameters["convexity"].GetInt();

    Matrix InitialLocalMatrix = IdentityMatrix(3);

    unsigned int size = CustomParameters["local_axes"].size();

    for (unsigned int i = 0; i < size; i++)
    {
      Parameters LocalAxesRow = CustomParameters["local_axes"][i];

      InitialLocalMatrix(0, i) = LocalAxesRow[0].GetDouble(); //column disposition
      InitialLocalMatrix(1, i) = LocalAxesRow[1].GetDouble();
      InitialLocalMatrix(2, i) = LocalAxesRow[2].GetDouble();
    }

    //set to local frame
    this->MapToLocalFrame(mBox.InitialLocalQuaternion, mBox);

    BeamMathUtilsType::MapToCurrentLocalFrame(mBox.InitialLocalQuaternion, mBox.Velocity);
    BeamMathUtilsType::MapToCurrentLocalFrame(mBox.InitialLocalQuaternion, mBox.AngularVelocity);

    mBox.SetInitialValues();

    mRigidBodyCenterSupplied = false;

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  SpatialBoundingBox(const PointType &rLowerPoint, const PointType &rUpperPoint)
  {
    KRATOS_TRY

    mBox.Initialize();
    mBox.UpperPoint = rUpperPoint;
    mBox.LowerPoint = rLowerPoint;

    mBox.Center = 0.5 * (rUpperPoint + rLowerPoint);
    mBox.Radius = 0.5 * norm_2(rUpperPoint - rLowerPoint);

    //set to local frame
    this->MapToLocalFrame(mBox.InitialLocalQuaternion, mBox);

    BeamMathUtilsType::MapToCurrentLocalFrame(mBox.InitialLocalQuaternion, mBox.Velocity);
    BeamMathUtilsType::MapToCurrentLocalFrame(mBox.InitialLocalQuaternion, mBox.AngularVelocity);

    mBox.SetInitialValues();

    mRigidBodyCenterSupplied = false;

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  SpatialBoundingBox(const PointType &rCenter, const double &rRadius)
  {
    KRATOS_TRY

    mBox.Initialize();
    mBox.Center = rCenter;
    mBox.Radius = rRadius;

    PointType Side;
    Side[0] = mBox.Radius;
    Side[1] = mBox.Radius;
    Side[2] = mBox.Radius;

    mBox.UpperPoint = mBox.Center + Side;
    mBox.LowerPoint = mBox.Center - Side;

    //set to local frame
    this->MapToLocalFrame(mBox.InitialLocalQuaternion, mBox);

    mBox.SetInitialValues();

    mRigidBodyCenterSupplied = false;

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  SpatialBoundingBox(ModelPart &rModelPart, const double &rRadius, double factor = 0)
  {
    KRATOS_TRY

    double max = std::numeric_limits<double>::max();
    double min = std::numeric_limits<double>::min();

    unsigned int dimension = 2;
    if (rModelPart.NumberOfElements() > 0)
      dimension = rModelPart.ElementsBegin()->GetGeometry().WorkingSpaceDimension();
    else if (rModelPart.NumberOfConditions() > 0)
      dimension = rModelPart.ConditionsBegin()->GetGeometry().WorkingSpaceDimension();
    else
      KRATOS_ERROR << " spatial_bounding_box: the supplied ModelPart does not have elements or conditions " << std::endl;

    PointType Maximum;
    PointType Minimum;

    for (unsigned int i = 0; i < 3; ++i)
    {
      Maximum[i] = min;
      Minimum[i] = max;
    }

    //Get inside point of the subdomains

    for (ModelPart::NodesContainerType::iterator in = rModelPart.NodesBegin(); in != rModelPart.NodesEnd(); ++in)
    {
      if (in->Is(BOUNDARY))
      {
        //get maximum
        if (Maximum[0] < in->X())
          Maximum[0] = in->X();

        if (Maximum[1] < in->Y())
          Maximum[1] = in->Y();

        if (Maximum[2] < in->Z())
          Maximum[2] = in->Z();

        //get minimum
        if (Minimum[0] > in->X())
          Minimum[0] = in->X();

        if (Minimum[1] > in->Y())
          Minimum[1] = in->Y();

        if (Minimum[2] > in->Z())
          Minimum[2] = in->Z();
      }
    }

    mBox.Initialize();

    mBox.Center = 0.5 * (Maximum + Minimum);

    mBox.Radius = 0.5 * norm_2(Maximum-Minimum) + rRadius;

    PointType Side(dimension);
    Side[0] = mBox.Radius + mBox.Radius * factor;
    Side[1] = mBox.Radius + mBox.Radius * factor;
    Side[2] = mBox.Radius + mBox.Radius * factor;

    mBox.UpperPoint = mBox.Center + Side;
    mBox.LowerPoint = mBox.Center - Side;

    //set to local frame
    this->MapToLocalFrame(mBox.InitialLocalQuaternion, mBox);

    mRigidBodyCenterSupplied = false;

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  /// Assignment operator.
  virtual SpatialBoundingBox &operator=(SpatialBoundingBox const &rOther)
  {
    KRATOS_TRY

    mpRigidBodyCenter = rOther.mpRigidBodyCenter;
    mRigidBodyCenterSupplied = rOther.mRigidBodyCenterSupplied;
    mBox = rOther.mBox;

    return *this;

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  /// Copy constructor.
  SpatialBoundingBox(SpatialBoundingBox const &rOther)
      : mpRigidBodyCenter(rOther.mpRigidBodyCenter), mRigidBodyCenterSupplied(rOther.mRigidBodyCenterSupplied), mBox(rOther.mBox)
  {
  }

  //**************************************************************************
  //**************************************************************************

  /// Destructor.
  virtual ~SpatialBoundingBox(){};


  /**
   * Clone function (has to be implemented by any derived class)
   * @return a pointer to a new instance of this friction law
   */
  virtual SpatialBoundingBox::Pointer Clone() const
  {
    return Kratos::make_shared<SpatialBoundingBox>(*this);
  };


  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  //**************************************************************************
  //**************************************************************************

  virtual void UpdateBoxPosition(const double &rCurrentTime)
  {

    KRATOS_TRY

    mBox.LocalQuaternion = this->UpdateLocalBase(rCurrentTime);

    PointType Displacement = this->GetBoxDisplacement(rCurrentTime);

    mBox.UpdatePosition(Displacement);

    this->MapToLocalFrame(mBox.LocalQuaternion, mBox);

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  // Used in the search detection (loop nodes in parallel region)
  virtual bool IsInside(NodeType &rNode, const double &rCurrentTime, double Radius = 0)
  {

    KRATOS_TRY

    bool inside = true;

    PointType Displacement = this->GetBoxDisplacement(rCurrentTime);

    mBox.UpdatePosition(Displacement);

    this->MapToLocalFrame(mBox.LocalQuaternion, mBox);

    PointType LocalPoint = rNode.Coordinates();
    BeamMathUtilsType::MapToCurrentLocalFrame(mBox.InitialLocalQuaternion, LocalPoint);

    if ((mBox.UpperPoint[0] >= LocalPoint[0] && mBox.LowerPoint[0] <= LocalPoint[0]) && (mBox.UpperPoint[1] >= LocalPoint[1] && mBox.LowerPoint[1] <= LocalPoint[1]) && (mBox.UpperPoint[2] >= LocalPoint[2] && mBox.LowerPoint[2] <= LocalPoint[2]))
    {
      inside = true;
    }
    else
    {
      inside = false;
    }

    QuaternionType LocaQuaternionlConjugate = mBox.LocalQuaternion.conjugate();
    this->MapToLocalFrame(LocaQuaternionlConjugate, mBox);

    return inside;

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  virtual bool IsInside(const NodeType &rNode) const
  {

    KRATOS_TRY

    bool inside = true;

    //check if the box is not set
    unsigned int count = 0;
    for (unsigned int i = 0; i < mBox.Center.size(); ++i)
    {
      if (mBox.UpperPoint[i] == mBox.LowerPoint[i])
      {
        count++;
      }
    }

    if (count == mBox.Center.size())
    {
      std::cout << " IsInside:: warning spatial BOX not set " << std::endl;
      return true;
    }
    //check if the box is not set

    PointType LocalPoint = rNode.Coordinates();
    BeamMathUtilsType::MapToCurrentLocalFrame(mBox.InitialLocalQuaternion, LocalPoint);

    // std::cout<<" Local Point "<<LocalPoint<<std::endl;
    // std::cout<<" Upper "<<mBox.UpperPoint<<" Lower "<<mBox.LowerPoint<<std::endl;
    // if(!(mBox.UpperPoint[0]>=LocalPoint[0] && mBox.LowerPoint[0]<=LocalPoint[0]) )
    // 	std::cout<<" first not fit "<<std::endl;
    // if(!(mBox.UpperPoint[1]>=LocalPoint[1] && mBox.LowerPoint[1]<=LocalPoint[1]) )
    // 	std::cout<<" second not fit "<<std::endl;
    // if(!(mBox.UpperPoint[2]>=LocalPoint[2] && mBox.LowerPoint[2]<=LocalPoint[2]) )
    // 	std::cout<<" third not fit "<<std::endl;

    if ((mBox.UpperPoint[0] >= LocalPoint[0] && mBox.LowerPoint[0] <= LocalPoint[0]) && (mBox.UpperPoint[1] >= LocalPoint[1] && mBox.LowerPoint[1] <= LocalPoint[1]) && (mBox.UpperPoint[2] >= LocalPoint[2] && mBox.LowerPoint[2] <= LocalPoint[2]))
    {
      inside = true;
    }
    else
    {
      inside = false;
    }

    return inside;

    KRATOS_CATCH("")
  }

  //************************************************************************************
  //************************************************************************************
  virtual bool IsInside(BoxParametersType &rValues, const ProcessInfo &rCurrentProcessInfo) const
  {
    KRATOS_TRY

    std::cout << "Calling empty method" << std::endl;
    return false;

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************
  virtual void GetParametricDirections(BoxParametersType &rValues, Vector &rT1, Vector &rT2)
  {
    KRATOS_TRY

    //std::cout<< "Calling empty method directions" <<std::endl;

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Access
  ///@{

  // SET

  void SetUpperPoint(PointType &rUpperPoint)
  {
    mBox.UpperPoint = rUpperPoint;
    mBox.InitialUpperPoint = rUpperPoint;
  }

  //**************************************************************************
  //**************************************************************************

  void SetLowerPoint(PointType &rLowerPoint)
  {
    mBox.LowerPoint = rLowerPoint;
    mBox.InitialLowerPoint = rLowerPoint;
  }

  //**************************************************************************
  //**************************************************************************

  void SetVelocity(PointType &rVelocity)
  {
    mBox.Velocity = rVelocity;
    BeamMathUtilsType::MapToCurrentLocalFrame(mBox.InitialLocalQuaternion, mBox.Velocity);
  }

  //**************************************************************************
  //**************************************************************************

  void SetAngularVelocity(PointType &rAngularVelocity)
  {
    mBox.AngularVelocity = rAngularVelocity;
    BeamMathUtilsType::MapToCurrentLocalFrame(mBox.InitialLocalQuaternion, mBox.AngularVelocity);
  }

  //**************************************************************************
  //**************************************************************************

  void SetDimension(int dimension)
  {
    mBox.Dimension = dimension;
  }

  //**************************************************************************
  //**************************************************************************

  void SetAxisymmetric()
  {
    mBox.Axisymmetric = true;
  }

  //**************************************************************************
  //**************************************************************************

  void SetRigidBodyCenter(NodeType::Pointer pCenter)
  {
    mpRigidBodyCenter = pCenter;
    mRigidBodyCenterSupplied = true;
  }

  // GET

  //**************************************************************************
  //**************************************************************************

  virtual double GetRadius()
  {
    return mBox.Radius;
  }

  //**************************************************************************
  //**************************************************************************

  virtual double GetRadius(const PointType &rPoint)
  {
    return mBox.Radius;
  }

  //**************************************************************************
  //**************************************************************************

  virtual PointType GetVelocity()
  {
    PointType Velocity;
    if (mRigidBodyCenterSupplied)
    {
      array_1d<double, 3> &rVelocity = mpRigidBodyCenter->FastGetSolutionStepValue(VELOCITY);
      for (unsigned int i = 0; i < 3; ++i)
        Velocity[i] = rVelocity[i];
    }
    else
    {
      Velocity = mBox.Velocity;
      BeamMathUtilsType::MapToReferenceLocalFrame(mBox.InitialLocalQuaternion, Velocity);
    }
    return Velocity;
  }

  //**************************************************************************
  //**************************************************************************

  virtual PointType GetCenter()
  {
    BeamMathUtilsType::MapToReferenceLocalFrame(mBox.InitialLocalQuaternion, mBox.Center);
    return mBox.Center;
  }

  //**************************************************************************
  //**************************************************************************

  virtual PointType GetCenter(const PointType &rPoint)
  {
    KRATOS_WARNING("") << "Calling spatial bounding box base class method " << std::endl;
    BeamMathUtilsType::MapToReferenceLocalFrame(mBox.InitialLocalQuaternion, mBox.Center);
    return mBox.Center;
  }

  //**************************************************************************
  //**************************************************************************

  /// Compute inside holes
  std::vector<PointType> GetHoles(ModelPart &rModelPart)
  {
    //Get inside point of the subdomains
    ModelPart::ConditionsContainerType::iterator condition_begin = rModelPart.ConditionsBegin();
    const unsigned int dimension = condition_begin->GetGeometry().WorkingSpaceDimension();

    std::vector<PointType> Holes;
    PointType Point(dimension);
    for (ModelPart::NodesContainerType::iterator in = rModelPart.NodesBegin(); in != rModelPart.NodesEnd(); ++in)
    {
      if (in->IsNot(BOUNDARY))
      {
        Point[0] = in->X();
        Point[1] = in->Y();

        if (dimension > 2)
          Point[2] = in->Z();

        Holes.push_back(Point);
        break;
      }
      else
      {
        array_1d<double, 3> &Normal = in->FastGetSolutionStepValue(NORMAL);

        std::cout << " Normal " << Normal << std::endl;
        double tolerance = 0.25;

        Point[0] = in->X() - Normal[0] * tolerance;
        Point[1] = in->Y() - Normal[1] * tolerance;
        if (dimension > 2)
          Point[2] = in->Z() - Normal[2] * tolerance;

        Holes.push_back(Point);
        break;
      }
    }

    return Holes;
  }

  //**************************************************************************
  //**************************************************************************

  /// Compute vertices
  void GetVertices(std::vector<PointType> &rVertices, const double &rCurrentTime, const unsigned int &rDimension)
  {

    PointType Displacement = this->GetBoxDisplacement(rCurrentTime);

    PointType Reference = mBox.UpperPoint + Displacement;

    PointType Side = mBox.UpperPoint - mBox.LowerPoint;

    Reference[1] -= Side[1];

    //point 0
    rVertices.push_back(Reference);

    Reference[1] += Side[1];

    //point 1
    rVertices.push_back(Reference);

    Reference[0] -= Side[0];

    //point 2
    rVertices.push_back(Reference);

    Reference[1] -= Side[1];

    //point 3
    rVertices.push_back(Reference);

    if (rDimension == 3)
    {

      Reference = mBox.LowerPoint + Displacement;

      Reference[0] += Side[0];

      //point 4
      rVertices.push_back(Reference);

      Reference[0] -= Side[0];

      //point 5
      rVertices.push_back(Reference);

      Reference[1] += Side[1];

      //point 6
      rVertices.push_back(Reference);

      Reference[0] += Side[0];

      //point 7
      rVertices.push_back(Reference);
    }
  }

  //************************************************************************************
  //************************************************************************************

  void GetTriangularFaces(DenseMatrix<unsigned int> &rFaces, const unsigned int &rDimension)
  {
    KRATOS_TRY

    if (rDimension == 2)
    {

      if (rFaces.size1() != 4 || rFaces.size2() != 2)
        rFaces.resize(4, 2, false);

      rFaces(0, 0) = 0;
      rFaces(0, 1) = 1;

      rFaces(1, 0) = 1;
      rFaces(1, 1) = 2;

      rFaces(2, 0) = 2;
      rFaces(2, 1) = 3;

      rFaces(3, 0) = 3;
      rFaces(3, 1) = 4;
    }
    else if (rDimension == 3)
    {

      if (rFaces.size1() != 12 || rFaces.size2() != 3)
        rFaces.resize(12, 3, false);

      rFaces(0, 0) = 0;
      rFaces(0, 1) = 1;
      rFaces(0, 2) = 3;

      rFaces(1, 0) = 3;
      rFaces(1, 1) = 1;
      rFaces(1, 2) = 2;

      rFaces(2, 0) = 3;
      rFaces(2, 1) = 2;
      rFaces(2, 2) = 6;

      rFaces(3, 0) = 6;
      rFaces(3, 1) = 5;
      rFaces(3, 2) = 3;

      rFaces(4, 0) = 5;
      rFaces(4, 1) = 6;
      rFaces(4, 2) = 7;

      rFaces(5, 0) = 7;
      rFaces(5, 1) = 4;
      rFaces(5, 2) = 5;

      rFaces(6, 0) = 0;
      rFaces(6, 1) = 4;
      rFaces(6, 2) = 7;

      rFaces(7, 0) = 7;
      rFaces(7, 1) = 1;
      rFaces(7, 2) = 0;

      rFaces(8, 0) = 0;
      rFaces(8, 1) = 3;
      rFaces(8, 2) = 5;

      rFaces(9, 0) = 5;
      rFaces(9, 1) = 4;
      rFaces(9, 2) = 0;

      rFaces(10, 0) = 1;
      rFaces(10, 1) = 7;
      rFaces(10, 2) = 6;

      rFaces(11, 0) = 6;
      rFaces(11, 1) = 2;
      rFaces(11, 2) = 1;
    }

    KRATOS_CATCH("")
  }

  void GetQuadrilateralFaces(DenseMatrix<unsigned int> &rFaces, const unsigned int &rDimension)
  {
    KRATOS_TRY

    if (rDimension == 2)
    {

      if (rFaces.size1() != 4 || rFaces.size2() != 2)
        rFaces.resize(4, 2, false);

      rFaces(0, 0) = 0;
      rFaces(0, 1) = 1;

      rFaces(1, 0) = 1;
      rFaces(1, 1) = 2;

      rFaces(2, 0) = 2;
      rFaces(2, 1) = 3;

      rFaces(3, 0) = 3;
      rFaces(3, 1) = 4;
    }
    else if (rDimension == 3)
    {

      if (rFaces.size1() != 6 || rFaces.size2() != 4)
        rFaces.resize(6, 4, false);

      rFaces(0, 0) = 0;
      rFaces(0, 1) = 1;
      rFaces(0, 2) = 2;
      rFaces(0, 3) = 3;

      rFaces(1, 0) = 3;
      rFaces(1, 1) = 2;
      rFaces(1, 2) = 6;
      rFaces(1, 3) = 5;

      rFaces(2, 0) = 5;
      rFaces(2, 1) = 6;
      rFaces(2, 2) = 7;
      rFaces(2, 3) = 4;

      rFaces(3, 0) = 4;
      rFaces(3, 1) = 7;
      rFaces(3, 2) = 1;
      rFaces(3, 3) = 0;

      rFaces(4, 0) = 0;
      rFaces(4, 1) = 3;
      rFaces(4, 2) = 5;
      rFaces(4, 3) = 4;

      rFaces(5, 0) = 1;
      rFaces(5, 1) = 7;
      rFaces(5, 2) = 6;
      rFaces(5, 3) = 2;
    }

    KRATOS_CATCH("")
  }

  //************************************************************************************
  //************************************************************************************

  virtual void CreateBoundingBoxBoundaryMesh(ModelPart &rModelPart, int linear_partitions = 4, int angular_partitions = 4)
  {
    KRATOS_TRY

    std::cout << " Calling Spatial Bounding Box empty boundary mesh creation" << std::endl;

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  virtual std::string Info() const
  {
    return "SpatialBoundingBox";
  }

  /// Print information about this object.
  virtual void PrintInfo(std::ostream &rOStream) const
  {
    rOStream << Info();
  }

  /// Print object's data.
  virtual void PrintData(std::ostream &rOStream) const
  {
    rOStream << mBox.UpperPoint << " , " << mBox.LowerPoint;
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  NodeType::Pointer mpRigidBodyCenter;

  bool mRigidBodyCenterSupplied;

  BoxVariablesType mBox;

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  //**************************************************************************
  //**************************************************************************

  void MapToLocalFrame(QuaternionType &rQuaternion, BoxVariablesType &rBox)
  {
    KRATOS_TRY

    BeamMathUtilsType::MapToCurrentLocalFrame(rQuaternion, rBox.UpperPoint);
    BeamMathUtilsType::MapToCurrentLocalFrame(rQuaternion, rBox.LowerPoint);
    BeamMathUtilsType::MapToCurrentLocalFrame(rQuaternion, rBox.Center);

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  QuaternionType UpdateLocalBase(const double &rCurrentTime)
  {
    PointType Rotation;
    noalias(Rotation) = ZeroVector(3);

    if (mRigidBodyCenterSupplied)
    {
      if (mpRigidBodyCenter->SolutionStepsDataHas(ROTATION))
      {
        const array_1d<double, 3> &CurrentRotation = mpRigidBodyCenter->FastGetSolutionStepValue(ROTATION);
        for (int i = 0; i < 3; ++i)
          Rotation[i] = CurrentRotation[i];
      }
    }
    else
    {
      Rotation = mBox.AngularVelocity * rCurrentTime;
    }
    //local base rotation
    BeamMathUtilsType::MapToCurrentLocalFrame(mBox.InitialLocalQuaternion, Rotation);
    return QuaternionType::FromRotationVector(Rotation);
  }

  //**************************************************************************
  //**************************************************************************

  PointType GetBoxDisplacement(const double &rCurrentTime)
  {

    PointType Displacement;
    noalias(Displacement) = ZeroVector(3);

    if (mRigidBodyCenterSupplied)
    {
      const array_1d<double, 3> &CurrentDisplacement = mpRigidBodyCenter->FastGetSolutionStepValue(DISPLACEMENT);
      for (int i = 0; i < 3; ++i)
        Displacement[i] = CurrentDisplacement[i];
      //local base rotation
      BeamMathUtilsType::MapToCurrentLocalFrame(mBox.InitialLocalQuaternion, Displacement);
    }
    else
    {
      Displacement = mBox.Velocity * rCurrentTime;
    }

    return Displacement;
  }

  //**************************************************************************
  //**************************************************************************

  PointType GetBoxDeltaDisplacement(const double &rCurrentTime, const double &rPreviousTime) const
  {
    PointType Displacement;
    noalias(Displacement) = ZeroVector(3);

    if (mRigidBodyCenterSupplied)
    {
      const array_1d<double, 3> &CurrentDisplacement = mpRigidBodyCenter->FastGetSolutionStepValue(DISPLACEMENT, 0);
      const array_1d<double, 3> &PreviousDisplacement = mpRigidBodyCenter->FastGetSolutionStepValue(DISPLACEMENT, 1);
      Displacement = CurrentDisplacement - PreviousDisplacement;
      BeamMathUtilsType::MapToCurrentLocalFrame(mBox.InitialLocalQuaternion, Displacement);
    }
    else{
      Displacement = mBox.Velocity * (rCurrentTime - rPreviousTime);
      BeamMathUtilsType::MapToCurrentLocalFrame(mBox.InitialLocalQuaternion, Displacement);
    }

    return Displacement;
  }

  //**************************************************************************
  //**************************************************************************

  void ComputeContactTangent(BoxParametersType &rValues, const ProcessInfo &rCurrentProcessInfo) const
  {
    KRATOS_TRY

    PointType &rNormal = rValues.GetNormal();
    PointType &rTangent = rValues.GetTangent();
    double &rGapTangent = rValues.GetGapTangent();

    PointType &rRelativeDisplacement = rValues.GetRelativeDisplacement();

    noalias(rTangent) = ZeroVector(3);

    //1.-compute contact tangent (following relative movement)
    PointType PointDeltaDisplacement = rValues.GetDeltaDisplacement();

    //2.-compute tangent direction
    PointType BoxDeltaDisplacement = this->GetBoxDeltaDisplacement(rCurrentProcessInfo[TIME], rCurrentProcessInfo.GetPreviousTimeStepInfo()[TIME]);

    rRelativeDisplacement = BoxDeltaDisplacement - PointDeltaDisplacement;

    rTangent = (rRelativeDisplacement)-inner_prod(rRelativeDisplacement, rNormal) * rNormal;

    double norm = norm_2(rNormal);
    if (norm == 0)
      noalias(rTangent) = ZeroVector(3);

    //3.-compute  normal gap
    rGapTangent = norm_2(rTangent);

    if (rGapTangent)
      rTangent /= rGapTangent;

    //std::cout<<" Normal "<<rNormal<<" Tangent "<<rTangent<<" gapT "<<rGapTangent<<std::endl;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline unsigned int GetMaxNodeId(ModelPart &rModelPart)
  {
    KRATOS_TRY

    return ModelPartUtilities::GetMaxNodeId(rModelPart);

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline unsigned int GetMaxElementId(ModelPart &rModelPart)
  {
    KRATOS_TRY

    return ModelPartUtilities::GetMaxElementId(rModelPart);

    KRATOS_CATCH("")
  }

  //************************************************************************************
  //************************************************************************************

  NodeType::Pointer CreateNode(ModelPart &rModelPart, PointType &rPoint, const unsigned int &rNodeId)
  {

    KRATOS_TRY

    NodeType::Pointer Node = rModelPart.CreateNewNode(rNodeId, rPoint[0], rPoint[1], rPoint[2]);

    //generating the dofs
    NodeType::DofsContainerType &reference_dofs = (rModelPart.NodesBegin())->GetDofs();

    for (NodeType::DofsContainerType::iterator i_dof = reference_dofs.begin(); i_dof != reference_dofs.end(); ++i_dof)
    {
      NodeType::DofType &rDof = **i_dof;
      NodeType::DofType::Pointer pNewDof = Node->pAddDof(rDof);

      (pNewDof)->FixDof();
    }

    //generating step data:
    // unsigned int buffer_size = (rModelPart.NodesBegin())->GetBufferSize();
    // unsigned int step_data_size = rModelPart.GetNodalSolutionStepDataSize();
    // for(unsigned int step = 0; step<buffer_size; ++step)
    // 	{
    // 	  double* NodeData = Node->SolutionStepData().Data(step);
    // 	  double* ReferenceData = (rModelPart.NodesBegin())->SolutionStepData().Data(step);

    // 	  //copying this data in the position of the vector we are interested in
    // 	  for(unsigned int j= 0; j<step_data_size; ++j)
    // 	    {
    // 	      NodeData[j] = ReferenceData[j];
    // 	    }
    // 	}

    return Node;

    KRATOS_CATCH("")
  }

  //************************************************************************************
  //************************************************************************************

  void CalculateOrthonormalBase(PointType &rDirectionVectorX, PointType &rDirectionVectorY, PointType &rDirectionVectorZ)
  {
    KRATOS_TRY

    PointType GlobalY = ZeroVector(3);
    GlobalY[1] = 1.0;

    PointType GlobalZ = ZeroVector(3);
    GlobalZ[2] = 1.0;

    // local x-axis (e1_local) is the beam axis  (in GID is e3_local)
    double VectorNorm = MathUtils<double>::Norm(rDirectionVectorX);
    if (VectorNorm != 0)
      rDirectionVectorX /= VectorNorm;

    // local y-axis (e2_local) (in GID is e1_local)
    rDirectionVectorY = ZeroVector(3);

    double tolerance = 1.0 / 64.0;
    if (fabs(rDirectionVectorX[0]) < tolerance && fabs(rDirectionVectorX[1]) < tolerance)
    {
      MathUtils<double>::CrossProduct(rDirectionVectorY, GlobalY, rDirectionVectorX);
    }
    else
    {
      MathUtils<double>::CrossProduct(rDirectionVectorY, GlobalZ, rDirectionVectorX);
    }

    VectorNorm = MathUtils<double>::Norm(rDirectionVectorY);
    if (VectorNorm != 0)
      rDirectionVectorY /= VectorNorm;

    // local z-axis (e3_local) (in GID is e2_local)
    MathUtils<double>::CrossProduct(rDirectionVectorZ, rDirectionVectorX, rDirectionVectorY);

    VectorNorm = MathUtils<double>::Norm(rDirectionVectorZ);
    if (VectorNorm != 0)
      rDirectionVectorZ /= VectorNorm;

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Serialization
  ///@{

  friend class Serializer;

  virtual void save(Serializer &rSerializer) const
  {
    rSerializer.save("mpRigidBodyCenter", mpRigidBodyCenter);
    rSerializer.save("mRigidBodyCenterSupplied", mRigidBodyCenterSupplied);
    rSerializer.save("mBox", mBox);
  }

  virtual void load(Serializer &rSerializer)
  {
    rSerializer.load("mpRigidBodyCenter", mpRigidBodyCenter);
    rSerializer.load("mRigidBodyCenterSupplied", mRigidBodyCenterSupplied);
    rSerializer.load("mBox", mBox);
  }
  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

public:
  DECLARE_HAS_THIS_TYPE_PROPERTIES
  DECLARE_ADD_THIS_TYPE_TO_PROPERTIES
  DECLARE_GET_THIS_TYPE_FROM_PROPERTIES

}; // Class SpatialBoundingBox

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                SpatialBoundingBox &rThis)
{
  return rIStream;
}

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const SpatialBoundingBox &rThis)
{
  return rOStream << rThis.Info();
}
///@}

///@} addtogroup block


} // namespace Kratos.

#endif // KRATOS_SPATIAL_BOUNDING_BOX_HPP_INCLUDED  defined
