//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

#include "meshers_application_variables.h"
#include "utilities/stl_vector_io.h"
#include "containers/global_pointers_vector.h"

namespace Kratos
{
///@name Type Definitions
///@{
typedef array_1d<double, 3> Vector3;
typedef array_1d<double, 6> Vector6;

///@name Kratos Globals
///@{

//Create Variables

//geometrical definition
KRATOS_CREATE_3D_VARIABLE_WITH_COMPONENTS(OFFSET)
KRATOS_CREATE_VARIABLE(double, SHRINK_FACTOR)

//domain definition
KRATOS_CREATE_VARIABLE(bool, IS_AXISYMMETRIC)
KRATOS_CREATE_VARIABLE(bool, INITIALIZED_DOMAINS)
KRATOS_CREATE_VARIABLE(double, MESHING_STEP_TIME)
KRATOS_CREATE_VARIABLE(double, RESTART_STEP_TIME)
KRATOS_CREATE_VARIABLE(std::string, MODEL_PART_NAME)
KRATOS_CREATE_VARIABLE(std::vector<std::string>, MODEL_PART_NAMES)

//boundary definition
KRATOS_CREATE_VARIABLE(int, RIGID_WALL)

//custom neighbor and masters
//KRATOS_CREATE_VARIABLE(NodeWeakPtrType, MAIN_NODE)
//KRATOS_CREATE_VARIABLE(ElementWeakPtrType, MAIN_ELEMENT)
//KRATOS_CREATE_VARIABLE(ConditionWeakPtrType, MAIN_CONDITION)

KRATOS_CREATE_VARIABLE(NodeWeakPtrVectorType, PRIMARY_NODES)
KRATOS_CREATE_VARIABLE(ElementWeakPtrVectorType, PRIMARY_ELEMENTS)
KRATOS_CREATE_VARIABLE(ConditionWeakPtrVectorType, PRIMARY_CONDITIONS)
KRATOS_CREATE_VARIABLE(ConditionWeakPtrVectorType, SECONDARY_CONDITIONS)

//condition variables
KRATOS_CREATE_VARIABLE(ConditionContainerType, CHILDREN_CONDITIONS)

//mesher criteria
KRATOS_CREATE_VARIABLE(double, MEAN_ERROR)

//ALE Variables
KRATOS_CREATE_3D_VARIABLE_WITH_COMPONENTS(MESH_DISPLACEMENT)
KRATOS_CREATE_3D_VARIABLE_WITH_COMPONENTS(MESH_ACCELERATION)

///@}

} // namespace Kratos
