# Meshers Application

In this application the algorithms for the mesh generation are implemented. 

## Prerequisites

This application is an interface to call Triangle and Tetgen libraries to perform a Delaunay Tessellation in 2D and 3D respectively.

## License

The Meshers application is OPEN SOURCE. The main code and program structure is available and aimed to grow with the need of any user willing to expand it. The BSD (Berkeley Software Distribution) licence allows to use and distribute the existing code without any restriction, but with the possibility to develop new parts of the code on an open or close source depending on the developers.
The derived libraries used (Triange and Tetgen) have their own proprietary license.

## Contact

* **Josep Maria Carbonell** - *Core Development* - [cpuigbo@cimne.upc.edu](mailto:cpuigbo@cimne.upc.edu)
