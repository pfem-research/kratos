//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:      October 2022 $
//
//

#if !defined(KRATOS_KINEMATIC_UTILITIES_HPP_INCLUDED)
#define KRATOS_KINEMATIC_UTILITIES_HPP_INCLUDED

// External includes

// System includes

// Project includes
#include "geometries/triangle_2d_3.h"
#include "geometries/tetrahedra_3d_4.h"

#include "custom_utilities/geometry_utilities.hpp"

namespace Kratos
{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
 */
class KRATOS_API(MESHERS_APPLICATION) KinematicUtilities
{
public:
  ///@name Type Definitions
  ///@{

  typedef Node<3> NodeType;
  typedef array_1d<double,3> VectorType;
  typedef Geometry<NodeType> GeometryType;
  typedef Geometry<NodeType>::PointsArrayType PointsArrayType;

  typedef std::size_t IndexType;
  typedef GeometryType::CoordinatesArrayType CoordinatesArrayType;

  typedef GlobalPointersVector<NodeType> NodeWeakPtrVectorType;
  typedef GlobalPointersVector<Element> ElementWeakPtrVectorType;
  typedef GlobalPointersVector<Condition> ConditionWeakPtrVectorType;

  /// Pointer definition of KinematicUtilities
  KRATOS_CLASS_POINTER_DEFINITION(KinematicUtilities);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  KinematicUtilities() {} //

  /// Destructor.
  virtual ~KinematicUtilities() {}

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{


  //*******************************************************************************************
  //*******************************************************************************************

  static inline const Variable<array_1d<double, 3>>& GetMeshVariable(const NodeType &rNode, std::string variable)
  {
    if (rNode.SolutionStepsDataHas(KratosComponents<Variable<array_1d<double, 3>>>::Get("MESH_"+variable)))
      variable = "MESH_"+variable;

    return KratosComponents<Variable<array_1d<double, 3>>>::Get(variable);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline const Variable<array_1d<double, 3>>& GetMeshVariable(const GeometryType &rGeometry, std::string variable)
  {
    if (rGeometry[0].SolutionStepsDataHas(KratosComponents<Variable<array_1d<double, 3>>>::Get("MESH_"+variable)))
      variable = "MESH_"+variable;

    return KratosComponents<Variable<array_1d<double, 3>>>::Get(variable);
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline const Variable<array_1d<double, 3>>& GetMeshVelocityVariable(const GeometryType &rGeometry)
  {
    std::string variable = "VELOCITY";
    if (rGeometry[0].SolutionStepsDataHas(MESH_VELOCITY))
      variable = "MESH_VELOCITY";

    return KratosComponents<Variable<array_1d<double, 3>>>::Get(variable);
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline const Variable<array_1d<double, 3>>& GetMeshAccelerationVariable(const GeometryType &rGeometry)
  {
    std::string variable = "ACCELERATION";
    if (rGeometry[0].SolutionStepsDataHas(MESH_ACCELERATION))
      variable = "MESH_ACCELERATION";

    return KratosComponents<Variable<array_1d<double, 3>>>::Get(variable);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double CheckFreeSurfaceApproachingEdges(GeometryType &rVertices, const double& rTimeStep, double tolerance = 0.5)
  {
    for (auto &i_node : rVertices)
    {
      if (i_node.Or({FREE_SURFACE,ISOLATED}) && i_node.IsNot(STRUCTURE))
      {
        for (auto &j_node : rVertices)
        {
          if (j_node.Or({FREE_SURFACE,ISOLATED}) && j_node.IsNot(STRUCTURE) && (i_node.Id() != j_node.Id()))
          {
            bool neighbour = false;
            NodeWeakPtrVectorType &nNodes = i_node.GetValue(NEIGHBOUR_NODES);
            for (auto &i_nnode : nNodes)
              if(i_nnode.Id() == j_node.Id())
                neighbour = true;

            if (!neighbour)
              if (GetApproachingPointDistance(i_node,j_node,rTimeStep,tolerance))
                return true;
          }
        }
      }
    }
    return false;
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline double CheckFreeSurfaceApproachingWall(GeometryType &rVertices, const double& rTimeStep, double tolerance = 0.5)
  {
    for (auto &i_node : rVertices)
    {
      if (i_node.Or({FREE_SURFACE,ISOLATED}) && i_node.IsNot(STRUCTURE))
      {
        for (auto &i_nnode : rVertices)
        {
          if (i_nnode.Is(STRUCTURE) && (i_nnode.Id() != i_node.Id()))
          {
            if (GetApproachingPointDistance(i_node,i_nnode,rTimeStep,tolerance))
              return true;
          }
        }
      }
    }
    return false;
  }

  //**************************************************************************
  //**************************************************************************

  static inline bool GetApproachingPointDistance(NodeType &rNode, NodeType &rEdgeNode, const double& rTimeStep, double tolerance = 0.5)
  {
    const Variable<array_1d<double, 3>>& VelocityVariable = GetMeshVariable(rNode,"VELOCITY");

    array_1d<double, 3>& Normal = rEdgeNode.FastGetSolutionStepValue(NORMAL); // STRUCTURE or (FREE_SURFACE and NOT STRUCTURE)


    array_1d<double, 3> DistancePre = (rEdgeNode.Coordinates() - rNode.Coordinates());
    DistancePre = inner_prod(DistancePre, Normal) * Normal;

    array_1d<double, 3> PositionNode = rNode.Coordinates() + rNode.FastGetSolutionStepValue(VelocityVariable) * rTimeStep;
    array_1d<double, 3> PositionEdge = rEdgeNode.Coordinates() + rEdgeNode.FastGetSolutionStepValue(VelocityVariable) * rTimeStep;

    array_1d<double, 3> DistancePost = (PositionEdge-PositionNode);
    DistancePost = inner_prod(DistancePost, Normal) * Normal;

    // std::cout<<" inode "<<rNode.Id()<<" enode "<<rEdgeNode.Id()<<std::endl;

    double distance_pre = norm_2(DistancePre);
    double distance_post = norm_2(DistancePost);

    // std::cout<<" DistancePre "<<DistancePre<<" distance_pre "<<distance_pre<<std::endl;
    // std::cout<<" DistancePost "<<DistancePost<<" distance_post "<<distance_post<<std::endl;

    if (distance_pre !=0){
      double distance_reduction = ((distance_pre-distance_post)/distance_pre);
      // std::cout<<" distance_reduction "<<distance_reduction<<std::endl;
      if (distance_post > 0 && distance_reduction > tolerance){ //approaching too fast
        //std::cout<<" ACTIVE distance_reduction "<<distance_reduction<<std::endl;
        return true;
      }
    }

    return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double CheckFreeSurfaceApproachingWall(GeometryType &rVertices)
  {
    for (auto &i_node : rVertices)
    {
      if (i_node.Or({FREE_SURFACE,ISOLATED}) && i_node.IsNot(STRUCTURE))
      {
        for (auto &i_nnode : rVertices)
        {
          if (i_nnode.Is(STRUCTURE) && (i_nnode.Id() != i_node.Id()))
          {
            if (CheckApproachingPoint(i_node,i_nnode, 0.0))
              return true;
          }
        }
      }
    }
    return false;
  }

  //**************************************************************************
  //**************************************************************************

  static inline bool CheckApproachingPoint(NodeType &rNode, NodeType &rEdgeNode, double tolerance = 0)
  {
    const Variable<array_1d<double, 3>>& VelocityVariable = GetMeshVariable(rNode,"VELOCITY");

    array_1d<double, 3> Normal = rEdgeNode.FastGetSolutionStepValue(NORMAL);
    if (norm_2(Normal))
      Normal /= norm_2(Normal);

    array_1d<double, 3> VelocityIncrement = rNode.FastGetSolutionStepValue(VelocityVariable) - rEdgeNode.FastGetSolutionStepValue(VelocityVariable);

    double velocity = norm_2(VelocityIncrement);
    if (velocity != 0)
      VelocityIncrement /= velocity;

    double projection = inner_prod(Normal, VelocityIncrement);
    //std::cout<<" velocity increment "<<VelocityIncrement<<" Normal "<<Normal<<" approaching "<<projection<<" tolerance "<<tolerance<<std::endl;
    if (projection > tolerance)
      return true;
    else
      return false;
  }

  //**************************************************************************
  //**************************************************************************

  // static inline bool CheckApproachingEdge(NodeType &rNode, NodeType &rEdgeNodeA, NodeType &rEdgeNodeB)
  // {
  //   const Variable<array_1d<double, 3>>& VelocityVariable = GetMeshVariable(rNode,"VELOCITY");

  //   bool approaching_edge = false;

  //   array_1d<double, 3> EdgeVelocity = 0.5 * (rEdgeNodeA.FastGetSolutionStepValue(VelocityVariable) + rEdgeNodeB.FastGetSolutionStepValue(VelocityVariable));
  //   array_1d<double, 3> MidPoint = 0.5 * (rEdgeNodeA.Coordinates() + rEdgeNodeB.Coordinates());

  //   array_1d<double, 3> Direction;
  //   GeometryUtilities::GetDirectionToEdge(Direction, rEdgeNodeA, rEdgeNodeB);

  //   array_1d<double, 3> Distance = (MidPoint - rNode.Coordinates());
  //   double distance = inner_prod(Distance, Direction);
  //   Distance = distance * Direction;

  //   array_1d<double, 3> VelocityDirection = rNode.FastGetSolutionStepValue(VelocityVariable);

  //   double velocity = norm_2(VelocityDirection);
  //   if (velocity != 0)
  //     VelocityDirection /= velocity;

  //   //different velocity directions
  //   if (inner_prod(EdgeVelocity, VelocityDirection) < 0)
  //   {
  //     //node velocity direction towards edge
  //     if (inner_prod(Distance, VelocityDirection) > 0)
  //       approaching_edge = true;
  //     else
  //       approaching_edge = false;
  //   } //same velocity directions
  //   else if (velocity > 0.1 * norm_2(EdgeVelocity))
  //   {
  //     //node velocity direction towards edge
  //     if (inner_prod(Distance, VelocityDirection) > 0)
  //       approaching_edge = true;
  //     else
  //       approaching_edge = false;
  //   }

  //   return approaching_edge;
  // }

  //**************************************************************************
  //**************************************************************************

  // static inline bool CheckApproachingFace(NodeType &rNode, NodeType &rEdgeNodeA, NodeType &rEdgeNodeB, NodeType &rEdgeNodeC)
  // {
  //   const Variable<array_1d<double, 3>>& VelocityVariable = GetMeshVariable(rNode,"VELOCITY");

  //   bool approaching_face = false;

  //   array_1d<double, 3> EdgeVelocity = (1.0 / 3.0) * (rEdgeNodeA.FastGetSolutionStepValue(VelocityVariable) + rEdgeNodeB.FastGetSolutionStepValue(VelocityVariable) + rEdgeNodeC.FastGetSolutionStepValue(VelocityVariable));
  //   array_1d<double, 3> MidPoint = (1.0 / 3.0) * (rEdgeNodeA.Coordinates() + rEdgeNodeB.Coordinates() + rEdgeNodeC.Coordinates());

  //   array_1d<double, 3> Direction;
  //   GeometryUtilities::GetDirectionToFace(Direction, rEdgeNodeA, rEdgeNodeB, rEdgeNodeC);

  //   array_1d<double, 3> Distance = (MidPoint - rNode.Coordinates());
  //   double distance = inner_prod(Distance, Direction);
  //   Distance = distance * Direction;

  //   array_1d<double, 3> VelocityDirection = rNode.FastGetSolutionStepValue(VelocityVariable);

  //   double velocity = norm_2(VelocityDirection);
  //   if (velocity != 0)
  //     VelocityDirection /= velocity;

  //   //different velocity directions
  //   if (inner_prod(EdgeVelocity, VelocityDirection) < 0)
  //   {
  //     //node velocity direction towards edge
  //     if (inner_prod(Distance, VelocityDirection) > 0)
  //       approaching_face = true;
  //     else
  //       approaching_face = false;
  //   } //same velocity directions
  //   else if (velocity > 0.1 * norm_2(EdgeVelocity))
  //   {
  //     //node velocity direction towards edge
  //     if (inner_prod(Distance, VelocityDirection) > 0)
  //       approaching_face = true;
  //     else
  //       approaching_face = false;
  //   }

  //   return approaching_face;
  // }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void AddScalarVariables(NodeType &iNode, const NodeType &jNode, const std::vector<std::string> &rVariables, const double weight, unsigned int step = 0)
  {
    for (auto &i_variable : rVariables)
      iNode.FastGetSolutionStepValue(KratosComponents<Variable<double>>::Get(i_variable), step) += weight * jNode.FastGetSolutionStepValue(KratosComponents<Variable<double>>::Get(i_variable), step);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void MultiplyScalarVariables(NodeType &iNode, const std::vector<std::string> &rVariables, const double weight, unsigned int step = 0)
  {
    for (auto &i_variable : rVariables)
      iNode.FastGetSolutionStepValue(KratosComponents<Variable<double>>::Get(i_variable), step) *= weight;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void AddVectorVariables(NodeType &iNode, const NodeType &jNode, const std::vector<std::string> &rVariables, const double weight, unsigned int step = 0)
  {
    for (auto &i_variable : rVariables)
      noalias(iNode.FastGetSolutionStepValue(KratosComponents<Variable<array_1d<double, 3>>>::Get(i_variable), step)) += weight * jNode.FastGetSolutionStepValue(KratosComponents<Variable<array_1d<double, 3>>>::Get(i_variable), step);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void MultiplyVectorVariables(NodeType &iNode, const std::vector<std::string> &rVariables, const double weight, unsigned int step = 0)
  {
    for (auto &i_variable : rVariables)
      iNode.FastGetSolutionStepValue(KratosComponents<Variable<array_1d<double, 3>>>::Get(i_variable), step) *= weight;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SmoothNodeKinematics(NodeType &rNode, NodeWeakPtrVectorType &FreeNeighbours, const double weight)
  {

    bool mesh_movement = false;
    if (rNode.SolutionStepsDataHas(MESH_DISPLACEMENT))
      mesh_movement = true;

    std::vector<std::string> Variables = {"VELOCITY","ACCELERATION"};
    std::vector<std::string> MeshVariables = {"MESH_VELOCITY","MESH_ACCELERATION"};

    //give weight to current value of fluid variables
    MultiplyVectorVariables(rNode,Variables,weight,0);
    MultiplyVectorVariables(rNode,Variables,weight,1);

    if (mesh_movement)
    {
      MultiplyVectorVariables(rNode,MeshVariables,weight,0);
      MultiplyVectorVariables(rNode,MeshVariables,weight,1);
    }

    double total_weight = weight;
    double new_weight = 0;
    for (auto &i_nnode : FreeNeighbours)
    {
      new_weight = norm_2(rNode.Coordinates() - i_nnode.Coordinates());
      if (new_weight != 0)
        new_weight = 1.0 / new_weight;

      AddVectorVariables(rNode,i_nnode,Variables,new_weight,0);
      AddVectorVariables(rNode,i_nnode,Variables,new_weight,1);

      if (mesh_movement)
      {
        AddVectorVariables(rNode,i_nnode,MeshVariables,new_weight,0);
        AddVectorVariables(rNode,i_nnode,MeshVariables,new_weight,1);
      }

      total_weight += new_weight;
    }

    double quotient = 1.0 / total_weight;
    MultiplyVectorVariables(rNode,Variables,quotient,0);
    MultiplyVectorVariables(rNode,Variables,quotient,1);

    if (mesh_movement)
    {
      MultiplyVectorVariables(rNode,MeshVariables,quotient,0);
      MultiplyVectorVariables(rNode,MeshVariables,quotient,1);
    }

  }

  //**************************************************************************
  //**************************************************************************
  static inline bool MoveBoundaryNodeAndSmoothKinematics(NodeType &rNode, const std::vector<Flags> PositionFlags = {FREE_SURFACE} , const std::vector<Flags> VariableFlags = {FREE_SURFACE, STRUCTURE.AsFalse()}, const double &smooth_factor = 1.0)
  {
    bool moved_node = false;
    //std::cout<<" Boundary to Move Pre ["<<rNode.Id()<<"] "<<rNode.Coordinates()<<std::endl;
    unsigned int FreeSurfaceNodes = 0;
    NodeWeakPtrVectorType &nNodes = rNode.GetValue(NEIGHBOUR_NODES);
    NodeWeakPtrVectorType FreeNeighbours;
    for (auto i_nnodes(nNodes.begin()); i_nnodes != nNodes.end(); ++i_nnodes)
    {
      if (i_nnodes->Is(PositionFlags))
      {
        FreeNeighbours.push_back(*i_nnodes.base());
        ++FreeSurfaceNodes;
      }
    }

    double Distance = 0;
    if (FreeSurfaceNodes == 2)
    {
      array_1d<double, 3> MidPoint = 0.5 * (FreeNeighbours.front().Coordinates() + FreeNeighbours.back().Coordinates());
      array_1d<double, 3> Direction = (FreeNeighbours.front().Coordinates() - FreeNeighbours.back().Coordinates());

      if (norm_2(Direction))
        Direction /= norm_2(Direction);

      array_1d<double, 3> Displacement = inner_prod((MidPoint - rNode.Coordinates()), Direction) * Direction * smooth_factor;
      noalias(rNode.Coordinates()) += Displacement;
      noalias(rNode.GetInitialPosition()) += Displacement;

      Distance = norm_2(Displacement);

      moved_node = true;
    }
    else if (FreeSurfaceNodes > 2)
    {
      array_1d<double, 3> MidPoint;
      noalias(MidPoint) = ZeroVector(3);
      double quotient = 1.0 / double(FreeSurfaceNodes);

      for (auto &i_fnnode : FreeNeighbours)
      {
        MidPoint += i_fnnode.Coordinates();
      }

      MidPoint *= quotient;

      array_1d<double, 3> Displacement = (MidPoint - rNode.Coordinates());
      Displacement *= smooth_factor;

      noalias(rNode.Coordinates()) += Displacement;
      noalias(rNode.GetInitialPosition()) += Displacement;

      Distance = norm_2(Displacement);

      moved_node = true;
    }
    else
    {
      std::cout << " Boundary node with only one FREE_SURFACE neighbour smooth " << std::endl;
    }

    if (moved_node)
    {
      FreeNeighbours.clear();
      FreeNeighbours.resize(0);
      for (auto i_nnodes(nNodes.begin()); i_nnodes != nNodes.end(); ++i_nnodes)
      {
        if (i_nnodes->Is(VariableFlags))
        {
          FreeNeighbours.push_back(*i_nnodes.base());
          ++FreeSurfaceNodes;
        }
      }

      if (Distance != 0)
      {
        //give weight to current value of fluid variables
        double weight = 1.0 / Distance;
        SmoothNodeKinematics(rNode,FreeNeighbours,weight);
      }
    }

    return moved_node;

  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void MoveNodeAndSmoothKinematics(NodeType &rNode)
  {
    NodeWeakPtrVectorType &nNodes = rNode.GetValue(NEIGHBOUR_NODES);

    bool mesh_movement = false;
    if (rNode.SolutionStepsDataHas(MESH_DISPLACEMENT))
      mesh_movement = true;

    std::vector<std::string> Variables = {"VELOCITY","ACCELERATION"};
    std::vector<std::string> MeshVariables = {"MESH_VELOCITY","MESH_ACCELERATION"};

    double weight = 1.0;
    for (auto &i_nnode : nNodes)
    {
      noalias(rNode.Coordinates()) += i_nnode.Coordinates();
      AddVectorVariables(rNode,i_nnode,{"DISPLACEMENT"},1.0,0);
      AddVectorVariables(rNode,i_nnode,{"DISPLACEMENT"},1.0,1);

      AddVectorVariables(rNode,i_nnode,Variables,weight,0);
      AddVectorVariables(rNode,i_nnode,Variables,weight,1);

      AddScalarVariables(rNode,i_nnode,{"PRESSURE"},weight,0);
      AddScalarVariables(rNode,i_nnode,{"PRESSURE"},weight,1);

      if (mesh_movement)
      {
        AddVectorVariables(rNode,i_nnode,MeshVariables,weight,0);
        AddVectorVariables(rNode,i_nnode,MeshVariables,weight,1);
      }
    }

    double quotient = 1.0 / double(nNodes.size() + 1);
    MultiplyVectorVariables(rNode,{"DISPLACEMENT"},quotient,0);
    MultiplyVectorVariables(rNode,{"DISPLACEMENT"},quotient,1);

    MultiplyVectorVariables(rNode,Variables,quotient,0);
    MultiplyVectorVariables(rNode,Variables,quotient,1);

    MultiplyScalarVariables(rNode,{"PRESSURE"},quotient,0);
    MultiplyScalarVariables(rNode,{"PRESSURE"},quotient,1);

    rNode.Coordinates() *= quotient;
    rNode.GetInitialPosition() = Point(rNode.Coordinates() - rNode.FastGetSolutionStepValue(DISPLACEMENT));

    if (mesh_movement)
    {
      MultiplyVectorVariables(rNode,MeshVariables,quotient,0);
      MultiplyVectorVariables(rNode,MeshVariables,quotient,1);
    }

  }

  //**************************************************************************
  //**************************************************************************
  static inline void MoveAndProjectMeshVelocity(NodeType &rNode, const array_1d<double, 3> &rDirection, const double &rDistance)
  {
    if(ProjectMeshVelocity(rNode, rDirection))
      MoveNode(rNode,rDirection,rDistance);
  }

  //**************************************************************************
  //**************************************************************************

  static inline void MoveNode(NodeType &rNode, const array_1d<double, 3> &rDirection, const double &rDistance)
  {
    noalias(rNode.Coordinates()) -= rDistance * rDirection;
    noalias(rNode.GetInitialPosition()) -= rDistance * rDirection;
  }

  //**************************************************************************
  //**************************************************************************

  static inline bool ProjectNodeVariable(NodeType &rNode, const array_1d<double, 3> &rDirection, const Variable<array_1d<double, 3>>& rVariable, const double factor = 0.5)
  {
    array_1d<double, 3> &VariableValue = rNode.FastGetSolutionStepValue(rVariable);
    if (norm_2(VariableValue) != 0)
    {
      double sign = 0;
      double projection = inner_prod(VariableValue, rDirection);
      if (projection > 0) //approaching
        sign = -1;

      VariableValue += sign * factor *projection * rDirection;

      return true;
    }

    return false;
  }

  //**************************************************************************
  //**************************************************************************

  static inline bool ProjectMeshVelocity(NodeType &rNode, const array_1d<double, 3> &rDirection, const double factor = 0.5)
  {
    //if( rNode.Is(INSIDE) ){ //modifies the results a lot
    //KinematicUtilities::ProjectNodeVariable(rNode, rDirection, ACCELERATION);
    //KinematicUtilities::ProjectNodeVariable(rNode, rDirection, VELOCITY);
    //}
    ProjectNodeVariable(rNode, rDirection, GetMeshVariable(rNode,"ACCELERATION"));
    return ProjectNodeVariable(rNode, rDirection, GetMeshVariable(rNode,"VELOCITY"));
  }

  //**************************************************************************
  //**************************************************************************

  static inline double CheckMeanPressure(GeometryType &rGeometry)
  {
    double pressure=0;
    for (auto &i_node : rGeometry)
    {
      pressure+=i_node.FastGetSolutionStepValue(PRESSURE);
    }

    pressure/= (double)rGeometry.size();

    return pressure;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckRelativeApproach(GeometryType &rGeometry, const double &rRelativeFactor, const double &rTimeStep)
  {
    const Variable<array_1d<double, 3>>& rVariable = GetMeshVelocityVariable(rGeometry);
    if (CheckRelativeVelocities(rGeometry, rRelativeFactor) && CalculateDomainSizeDecrease(rGeometry, rTimeStep, rVariable))
      return true;
    else
      return false;
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckMeshVelocity(GeometryType &rGeometry)
  {
    if (rGeometry[0].SolutionStepsDataHas(MESH_VELOCITY))
    {
      array_1d<double, 3> MeanVelocity;
      noalias(MeanVelocity) = ZeroVector(3);
      array_1d<double, 3> MeanNormal;
      noalias(MeanNormal) = ZeroVector(3);
      for (auto &i_node : rGeometry)
      {
        MeanVelocity += i_node.FastGetSolutionStepValue(VELOCITY);
        if (i_node.Is(FREE_SURFACE))
          MeanNormal += i_node.FastGetSolutionStepValue(NORMAL);
      }

      for (auto &i_node : rGeometry)
      {
        if (i_node.Is(STRUCTURE))
        {
          const array_1d<double, 3>& MeshVelocity = i_node.FastGetSolutionStepValue(MESH_VELOCITY);
          const array_1d<double, 3>& Velocity = i_node.FastGetSolutionStepValue(VELOCITY);
          if ( inner_prod(MeshVelocity,Velocity) <= 0 )
          {
            if (norm_2(Velocity) != 0)
            {
              if ( norm_2(MeshVelocity) > norm_2(Velocity) * 4  )
                if ( inner_prod(MeshVelocity,MeanNormal) > 0.05 )
                  return true;
            }
            else{
              if ( inner_prod(MeshVelocity,MeanVelocity) <= 0 )
                if ( norm_2(MeshVelocity) > norm_2(MeanVelocity) )
                  if ( inner_prod(MeshVelocity,MeanNormal) > 0.05 )
                    return true;
            }
          }
        }
      }
    }

    return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckRelativeVelocities(GeometryType &rGeometry, const double &rRelativeFactor)
  {
    const Variable<array_1d<double, 3>>& rVariable = GetMeshVelocityVariable(rGeometry);

    double maximum = std::numeric_limits<double>::min();
    double minimum = std::numeric_limits<double>::max();

    double value = 0;
    for (auto &i_node : rGeometry)
    {
      value = norm_2(i_node.FastGetSolutionStepValue(rVariable));
      if (value > maximum)
        maximum = value;
      if (value < minimum)
        minimum = value;
    }

    value = maximum-minimum;
    if (minimum != 0)
      value/=minimum;

    if (value > rRelativeFactor)
      return true;

    return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CalculateDomainSizeDecrease(const GeometryType &rGeometry, const double &rFactor, const Variable<array_1d<double,3>>& rVariable, double rTolerance = 0)
  {
    if(CalculateDomainSizeChange(rGeometry, rFactor, rVariable) < -rTolerance)
      return true;
    else
      return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CalculateDomainSizeDecrease(const GeometryType &rGeometry, const double &rFactor, double rTolerance = 0)
  {
    const Variable<array_1d<double, 3>>& rVariable = GetMeshVelocityVariable(rGeometry);
    if(CalculateDomainSizeChange(rGeometry, rFactor, rVariable) < -rTolerance)
      return true;
    else
      return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CalculateDomainSizeDecrease(const GeometryType &rGeometry, const double &rFactor, double &rVolume, double rTolerance = 0)
  {
    const Variable<array_1d<double, 3>>& rVariable = GetMeshVelocityVariable(rGeometry);
    rVolume = CalculateDomainSizeChange(rGeometry, rFactor, rVariable);
    if(rVolume < -rTolerance)
      return true;
    else
      return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double CalculateDomainSizeChange(const GeometryType &rGeometry, const double &rFactor, const Variable<array_1d<double,3>>& rVariable)
  {
    if (rGeometry.size() == 3)
    {
      double current_radius = GeometryUtilities::CalculateTriangleArea(rGeometry[0].X(), rGeometry[0].Y(),
                                                    rGeometry[1].X(), rGeometry[1].Y(),
                                                    rGeometry[2].X(), rGeometry[2].Y());

      const array_1d<double,3> &V0 = rGeometry[0].FastGetSolutionStepValue(rVariable);
      const array_1d<double,3> &V1 = rGeometry[1].FastGetSolutionStepValue(rVariable);
      const array_1d<double,3> &V2 = rGeometry[2].FastGetSolutionStepValue(rVariable);

      double updated_radius = GeometryUtilities::CalculateTriangleArea(rGeometry[0].X()+rFactor*V0[0], rGeometry[0].Y()+rFactor*V0[1],
                                                    rGeometry[1].X()+rFactor*V1[0], rGeometry[1].Y()+rFactor*V1[1],
                                                    rGeometry[2].X()+rFactor*V2[0], rGeometry[2].Y()+rFactor*V2[1]);
      return (updated_radius-current_radius);
    }
    else{
      double current_radius = GeometryUtilities::CalculateTetrahedronVolume(rGeometry[0].X(), rGeometry[0].Y(), rGeometry[0].Z(),
                                                         rGeometry[1].X(), rGeometry[1].Y(), rGeometry[1].Z(),
                                                         rGeometry[2].X(), rGeometry[2].Y(), rGeometry[2].Z(),
                                                         rGeometry[3].X(), rGeometry[3].Y(), rGeometry[3].Z());

      const array_1d<double,3> &V0 = rGeometry[0].FastGetSolutionStepValue(rVariable);
      const array_1d<double,3> &V1 = rGeometry[1].FastGetSolutionStepValue(rVariable);
      const array_1d<double,3> &V2 = rGeometry[2].FastGetSolutionStepValue(rVariable);
      const array_1d<double,3> &V3 = rGeometry[2].FastGetSolutionStepValue(rVariable);

      double updated_radius = GeometryUtilities::CalculateTetrahedronVolume(rGeometry[0].X()+rFactor*V0[0], rGeometry[0].Y()+rFactor*V0[1], rGeometry[0].Z()+rFactor*V0[2],
                                                         rGeometry[1].X()+rFactor*V1[0], rGeometry[1].Y()+rFactor*V1[1], rGeometry[1].Z()+rFactor*V1[2],
                                                         rGeometry[2].X()+rFactor*V2[0], rGeometry[2].Y()+rFactor*V2[1], rGeometry[2].Z()+rFactor*V2[2],
                                                         rGeometry[3].X()+rFactor*V3[0], rGeometry[3].Y()+rFactor*V3[1], rGeometry[3].Z()+rFactor*V3[2]);

      return (updated_radius-current_radius);
    }

    return 0;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckVolumeDecrease(GeometryType &rVertices, const unsigned int &rDimension, const double &rTimeStep, const double &rTolerance, double &VolumeChange)
  {
    bool decrease = false;

    for (auto &i_node : rVertices)
      if (i_node.Is(MODIFIED))
        return true;

    if (rDimension == 2)
    {
      Triangle2D3<NodeType> CurrentTriangle(rVertices);
      double CurrentArea = CurrentTriangle.Area();

      //new volume with a 1.0 * DeltaDisplacement
      double MovedArea = GetMovedVolume(rVertices, rDimension, rTimeStep, 1.0);

      //std::cout<<" control fluid  "<<MovedArea<<" "<<CurrentArea<<std::endl;
      VolumeChange = CurrentArea - MovedArea;

      if (MovedArea + rTolerance < CurrentArea)
        decrease = true;
    }
    else if (rDimension == 3)
    {

      Tetrahedra3D4<NodeType> CurrentTetrahedron(rVertices);
      double CurrentVolume = CurrentTetrahedron.Volume();

      //new volume with a 1.0 * DeltaDisplacement
      double MovedVolume = GetMovedVolume(rVertices, rDimension, rTimeStep, 1.0);

      //std::cout<<" control fluid  "<<MovedVolume<<" "<<CurrentVolume<<std::endl;
      VolumeChange = CurrentVolume - MovedVolume;

      if (MovedVolume + rTolerance < CurrentVolume)
        decrease = true;
    }

    return decrease;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double GetMovedVolume(GeometryType &rVertices, const unsigned int &rDimension, const double &rTimeStep, double MovementFactor)
  {
    const Variable<array_1d<double, 3>>& VelocityVariable = GetMeshVariable(rVertices[0],"VELOCITY");

    double MovedVolume = 0.0;
    if (rDimension == 2)
    {
      //Triangle geometry
      array_1d<double, 3> P0;
      noalias(P0) = rVertices[0].Coordinates() + MovementFactor * rTimeStep * rVertices[0].FastGetSolutionStepValue(VelocityVariable);
      array_1d<double, 3> P1;
      noalias(P1) = rVertices[1].Coordinates() + MovementFactor * rTimeStep * rVertices[1].FastGetSolutionStepValue(VelocityVariable);
      double x10 = P1[0] - P0[0];
      double y10 = P1[1] - P0[1];

      noalias(P1) = rVertices[2].Coordinates() + MovementFactor * rTimeStep * rVertices[2].FastGetSolutionStepValue(VelocityVariable);
      double x20 = P1[0] - P0[0];
      double y20 = P1[1] - P0[1];

      MovedVolume = 0.5 * (x10 * y20 - y10 * x20);
    }
    else if (rDimension == 3)
    {
      //Tetrahedron geometry
      const double onesixth = 1.0 / 6.0;

      array_1d<double, 3> P0;
      noalias(P0) = rVertices[0].Coordinates() + MovementFactor * rTimeStep * rVertices[0].FastGetSolutionStepValue(VelocityVariable);

      array_1d<double, 3> P1;
      noalias(P1) = rVertices[1].Coordinates() + MovementFactor * rTimeStep * rVertices[1].FastGetSolutionStepValue(VelocityVariable);

      double x10 = P1[0] - P0[0];
      double y10 = P1[1] - P0[1];
      double z10 = P1[2] - P0[2];

      noalias(P1) = rVertices[2].Coordinates() + MovementFactor * rTimeStep * rVertices[2].FastGetSolutionStepValue(VelocityVariable);

      double x20 = P1[0] - P0[0];
      double y20 = P1[1] - P0[1];
      double z20 = P1[2] - P0[2];

      noalias(P1) = rVertices[3].Coordinates() + MovementFactor * rTimeStep * rVertices[3].FastGetSolutionStepValue(VelocityVariable);

      double x30 = P1[0] - P0[0];
      double y30 = P1[1] - P0[1];
      double z30 = P1[2] - P0[2];

      MovedVolume = onesixth * (x10 * y20 * z30 - x10 * y30 * z20 + y10 * z20 * x30 - y10 * x20 * z30 + z10 * x20 * y30 - z10 * y20 * x30);

      // if(MovedVolume<0)
      //   std::cout<<" VOLUME negative "<<std::endl;
    }

    return MovedVolume;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  // static inline double CheckFreeSurfaceApproachingWall(GeometryType &rVertices, const double &rDistance, const double &rTimeStep)
  // {
  //   const Variable<array_1d<double, 3>>& VelocityVariable = GetMeshVariable(rVertices[0],"VELOCITY");

  //   bool approaching_face = false;
  //   bool check_face = true;

  //   array_1d<double, 3> EdgeVelocity;
  //   noalias(EdgeVelocity) = ZeroVector(3);
  //   array_1d<double, 3> EdgePoint;
  //   noalias(EdgePoint) = ZeroVector(3);
  //   double wall_nodes = 0;

  //   array_1d<double, 3> FreeSurfaceVelocity;
  //   noalias(FreeSurfaceVelocity) = ZeroVector(3);
  //   array_1d<double, 3> FreeSurfacePoint;
  //   noalias(FreeSurfacePoint) = ZeroVector(3);
  //   double freesurface_nodes = 0;

  //   for (auto &i_node : rVertices)
  //   {
  //     if (i_node.Or({RIGID,SOLID}))
  //     {
  //       EdgeVelocity += i_node.FastGetSolutionStepValue(VelocityVariable);
  //       EdgePoint += i_node.Coordinates();
  //       ++wall_nodes;
  //     }
  //     else if (i_node.Or({FREE_SURFACE,ISOLATED}))
  //     {
  //       FreeSurfaceVelocity += i_node.FastGetSolutionStepValue(VelocityVariable);
  //       FreeSurfacePoint += i_node.Coordinates();
  //       ++freesurface_nodes;

  //       NodeWeakPtrVectorType &nNodes = i_node.GetValue(NEIGHBOUR_NODES);
  //       unsigned int count_rigid = 0;
  //       for (auto &i_nnode : nNodes)
  //       {
  //         if (i_nnode.Or({RIGID,SOLID}))
  //           ++count_rigid;
  //       }

  //       if (count_rigid < nNodes.size() - 2)
  //         check_face = false;
  //     }
  //   }
  //   if (wall_nodes != 0)
  //   {
  //     EdgeVelocity /= wall_nodes;
  //     EdgePoint /= wall_nodes;
  //   }

  //   if (freesurface_nodes != 0)
  //   {
  //     FreeSurfaceVelocity /= freesurface_nodes;
  //     FreeSurfacePoint /= freesurface_nodes;
  //   }

  //   if (wall_nodes != 0 && freesurface_nodes != 0 && check_face)
  //   {
  //     FreeSurfaceVelocity -= EdgeVelocity;
  //     FreeSurfacePoint -= EdgePoint;

  //     //different velocity directions
  //     if (inner_prod(FreeSurfaceVelocity, FreeSurfacePoint) > 0)
  //     {

  //       if (norm_2(FreeSurfaceVelocity) * rTimeStep > rDistance)
  //         approaching_face = false;
  //       else
  //         approaching_face = true;
  //     }
  //     else
  //     {
  //       approaching_face = true;
  //     }

  //     //std::cout<<" fs_nodes "<<freesurface_nodes<<" wl_nodes "<<wall_nodes<<" VEL "<<FreeSurfaceVelocity<<" PT "<<FreeSurfacePoint<<" approach "<<approaching_face<<" "<<inner_prod( FreeSurfaceVelocity, FreeSurfacePoint )<<std::endl;
  //   }
  //   else
  //   {
  //     approaching_face = true;
  //     //std::cout<<" no checked face "<<std::endl;
  //   }

  //   return approaching_face;

  // }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double GetDeformationGradientDeterminant(GeometryType &rVertices, const unsigned int &rDimension, const double &rTimeStep)
  {
    //Deformation Gradient determinant
    unsigned int number_of_nodes = rVertices.size();

    //Configuration increment
    Matrix DeltaPosition(number_of_nodes, rDimension);
    for (unsigned int i = 0; i < number_of_nodes; i++)
    {
      // const array_1d<double, 3 > & CurrentDisplacement  = rVertices[i].FastGetSolutionStepValue(DISPLACEMENT);
      // const array_1d<double, 3 > & PreviousDisplacement = rVertices[i].FastGetSolutionStepValue(DISPLACEMENT,1);

      // for ( unsigned int j = 0; j < rDimension; j++ )
      // {
      //   DeltaPosition(i,j) = CurrentDisplacement[j]-PreviousDisplacement[j];
      // }
      const array_1d<double, 3> &CurrentVelocity = rVertices[i].FastGetSolutionStepValue(VELOCITY);
      for (unsigned int j = 0; j < rDimension; j++)
      {
        DeltaPosition(i, j) = rTimeStep * CurrentVelocity[j];
      }
    }

    //Compute cartesian derivatives [dN/dx_n]
    Matrix DN_DX;

    if (rDimension == 2)
    {
      Triangle2D3<NodeType> Triangle(rVertices);

      DenseVector<Matrix> J;
      J = Triangle.Jacobian(J, GeometryData::IntegrationMethod::GI_GAUSS_1, DeltaPosition);

      //Calculating the inverse of the jacobian and the parameters needed [d£/dx_n]
      Matrix InvJ;
      double detJ;
      MathUtils<double>::InvertMatrix(J[0], InvJ, detJ, 0.0);

      const Matrix &DN_De = Triangle.ShapeFunctionLocalGradient(0, GeometryData::IntegrationMethod::GI_GAUSS_1);
      DN_DX = prod(DN_De, InvJ);
    }
    else if (rDimension == 3)
    {

      Tetrahedra3D4<NodeType> Tetrahedron(rVertices);

      DenseVector<Matrix> J;
      J = Tetrahedron.Jacobian(J, GeometryData::IntegrationMethod::GI_GAUSS_1, DeltaPosition);

      //Calculating the inverse of the jacobian and the parameters needed [d£/dx_n]
      Matrix InvJ;
      double detJ;
      MathUtils<double>::InvertMatrix(J[0], InvJ, detJ, 0.0);

      const Matrix &DN_De = Tetrahedron.ShapeFunctionLocalGradient(0, GeometryData::IntegrationMethod::GI_GAUSS_1);
      DN_DX = prod(DN_De, InvJ);
    }

    Matrix F(rDimension, rDimension);
    noalias(F) = ZeroMatrix(rDimension, rDimension);
    for (unsigned int i = 0; i < rDimension; i++)
    {
      for (unsigned int j = 0; j < rDimension; j++)
      {
        for (unsigned int k = 0; k < number_of_nodes; k++)
        {
          F(i, j) += rVertices[k].Coordinates()[i] * DN_DX(k, j);
        }
      }
    }
    double detF = MathUtils<double>::Det(F);

    return detF;
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{
  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{
  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{

  /// Assignment operator.
  KinematicUtilities &operator=(KinematicUtilities const &rOther);

  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Unaccessible methods
  ///@{
  ///@}

}; // Class KinematicUtilities

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_KINEMATIC_UTILITIES_HPP_INCLUDED  defined
