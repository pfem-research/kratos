//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

#if !defined(KRATOS_TRANSFER_UTILITIES_HPP_INCLUDED)
#define KRATOS_TRANSFER_UTILITIES_HPP_INCLUDED

// System includes

// Project includes
#include "custom_utilities/model_part_utilities.hpp"
#include "custom_utilities/set_flags_utilities.hpp"
#include "custom_utilities/search_utilities.hpp"
#include "custom_utilities/geometry_utilities.hpp"
#include "custom_utilities/mesher_utilities.hpp"

namespace Kratos
{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
class KRATOS_API(MESHERS_APPLICATION) TransferUtilities
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of data transfer
  KRATOS_CLASS_POINTER_DEFINITION(TransferUtilities);

  typedef Node<3> NodeType;
  typedef array_1d<double,3> VectorType;
  typedef Geometry<NodeType> GeometryType;

  typedef Bucket<3, Node<3>, std::vector<Node<3>::Pointer>, Node<3>::Pointer, std::vector<Node<3>::Pointer>::iterator, std::vector<double>::iterator> BucketType;
  typedef Tree<KDTreePartition<BucketType>> KdtreeType; //Kdtree

  typedef GlobalPointersVector<Node<3>> NodeWeakPtrVectorType;
  typedef GlobalPointersVector<Element> ElementWeakPtrVectorType;
  typedef GlobalPointersVector<Condition> ConditionWeakPtrVectorType;

  typedef VariablesListDataValueContainer VariablesContainer;


  struct DataVariables
  {

    KRATOS_CLASS_POINTER_DEFINITION(DataVariables);

    Flags Options;

    std::vector<const Variable<double> *> DoubleVariables;
    std::vector<const Variable<array_1d<double, 3>> *> Array1DVariables;
    std::vector<const Variable<Vector> *> VectorVariables;
    std::vector<const Variable<Matrix> *> MatrixVariables;

    bool VariablesSetFlag;

    // setting refining variables (generally for python interface)
    void Set(Flags ThisFlag)
    {
      Options.Set(ThisFlag);
    };

    void Reset(Flags ThisFlag)
    {
      Options.Reset(ThisFlag);
    };

    void SetOptions(const Flags &rOptions)
    {
      Options = rOptions;
    };

    void SetVariable(const Variable<double> &pVariable)
    {
      VariablesSetFlag = true;
      DoubleVariables.push_back(&pVariable);
    }

    void SetVariable(const Variable<array_1d<double, 3>> &pVariable)
    {
      VariablesSetFlag = true;
      Array1DVariables.push_back(&pVariable);
    }

    void SetVariable(const Variable<Vector> &pVariable)
    {
      VariablesSetFlag = true;
      VectorVariables.push_back(&pVariable);
    }

    void SetVariable(const Variable<Matrix> &pVariable)
    {
      VariablesSetFlag = true;
      MatrixVariables.push_back(&pVariable);
    }

    Flags GetOptions()
    {
      return Options;
    };

    void Initialize()
    {
      VariablesSetFlag = false;
    };
  };

  struct DataValues
  {
    double DoubleVariable;
    array_1d<double, 3> Array1DVariable;
    Vector VectorVariable;
    Matrix MatrixVariable;

    void Initialize(const unsigned int &dimension, const unsigned int &voigt_size)
    {
      DoubleVariable = 0;
      noalias(Array1DVariable) = ZeroVector(3);
      VectorVariable.resize(voigt_size);
      noalias(VectorVariable) = ZeroVector(voigt_size);
      MatrixVariable.resize(dimension, dimension, false);
      noalias(MatrixVariable) = IdentityMatrix(dimension);
    }
  };

  struct DataValuesArrays
  {
    unsigned int array_size;
    std::vector<double> DoubleValueArray;
    std::vector<array_1d<double, 3>> Array1DValueArray;
    std::vector<Vector> VectorValueArray;
    std::vector<Matrix> MatrixValueArray;

    void Initialize(const unsigned int &size)
    {
      array_size = size;
      DoubleValueArray.resize(size);
      Array1DValueArray.resize(size);
      VectorValueArray.resize(size);
      MatrixValueArray.resize(size);
    }

    void Initialize (const DataVariables &rDataVariables)
    {
      DoubleValueArray.resize(rDataVariables.DoubleVariables.size());
      Array1DValueArray.resize(rDataVariables.Array1DVariables.size());
      VectorValueArray.resize(rDataVariables.VectorVariables.size());
      MatrixValueArray.resize(rDataVariables.MatrixVariables.size());
    }

  };

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  TransferUtilities() {}

  /// Copy constructor.
  TransferUtilities(TransferUtilities const &rOther) {}

  /// Destructor.
  virtual ~TransferUtilities() {}

  ///@}
  ///@name Operators
  ///@{
  ///@}
  ///@name Operations
  ///@{


  //*******************************************************************************************
  //*******************************************************************************************

  static inline void AddNodeStepData(NodeType &rNode, const GeometryType& rGeometry, const unsigned int &step_data_size)
  {
    const unsigned int size = rGeometry.size();
    double value = 1.0 / double(size);
    Vector ShapeFunctions(size);
    std::fill(ShapeFunctions.begin(), ShapeFunctions.end(), value);

    double alpha = 1; // smoothing data factor
    InterpolateStepData(rNode, rGeometry, ShapeFunctions, step_data_size, alpha);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void AddConditionData(const Condition &rOldCondition, Condition &rNewCondition)
  {
    //set variables
    //rNewCondition.SetValue(PRIMARY_ELEMENTS, rOldCondition.GetValue(PRIMARY_ELEMENTS));
    rNewCondition.SetValue(PRIMARY_NODES, rOldCondition.GetValue(PRIMARY_NODES));
    rNewCondition.SetValue(NORMAL, rOldCondition.GetValue(NORMAL));
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void InitializeBoundaryData(Condition &rCurrentCondition,
                                            const DataVariables &rDataVariables,
                                            const ProcessInfo &rCurrentProcessInfo)
  {
    unsigned int dimension = rCurrentProcessInfo[SPACE_DIMENSION];
    unsigned int voigt_size = dimension * (dimension + 1) * 0.5; //axisymmetric, processinfo is needed
    if (rCurrentProcessInfo[IS_AXISYMMETRIC]){
      ++voigt_size; // vector size
      ++dimension; // matrix size
    }

    DataValues rValues;
    rValues.Initialize(dimension, voigt_size);

    if (rDataVariables.VariablesSetFlag)
      TransferInitialBoundaryData(rCurrentCondition, rDataVariables, rValues);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void TransferInitialBoundaryData(Condition &rCurrentCondition,
                                                 const DataVariables &rDataVariables,
                                                 const DataValues &rValues)
  {
    //double
    for (const auto &i_var : rDataVariables.DoubleVariables)
      if (!rCurrentCondition.Has(*i_var))
        rCurrentCondition.SetValue(*i_var, rValues.DoubleVariable);

    //array_1d
    for (const auto &i_var : rDataVariables.Array1DVariables)
      if (!rCurrentCondition.Has(*i_var))
        rCurrentCondition.SetValue(*i_var, rValues.Array1DVariable);

    //Vector
    for (const auto &i_var : rDataVariables.VectorVariables)
      if (!rCurrentCondition.Has(*i_var))
        rCurrentCondition.SetValue(*i_var, rValues.VectorVariable);

    //Matrix
    for (const auto &i_var : rDataVariables.MatrixVariables)
      if (!rCurrentCondition.Has(*i_var))
        rCurrentCondition.SetValue(*i_var, rValues.MatrixVariable);
  }


  //*******************************************************************************************
  //*******************************************************************************************
  static inline void TransferBoundaryData(Element &rCurrentElement,
                                          Condition &rCurrentCondition,
                                          const DataVariables &rDataVariables,
                                          const ProcessInfo &rCurrentProcessInfo)
  {
    unsigned int dimension = rCurrentProcessInfo[SPACE_DIMENSION];
    unsigned int voigt_size = dimension * (dimension + 1) * 0.5;
    if (rCurrentProcessInfo[IS_AXISYMMETRIC]){
      ++voigt_size; // vector size
      ++dimension; // matrix size
    }

    DataValues Values;
    Values.Initialize(dimension, voigt_size);

    //initialize to zero
    if (rDataVariables.VariablesSetFlag)
      TransferInitialBoundaryData(rCurrentCondition, rDataVariables, Values);

    unsigned int integration_points_number = (rCurrentElement.GetGeometry()).IntegrationPointsNumber(rCurrentElement.GetIntegrationMethod());

    DataValuesArrays ValueArrays;
    ValueArrays.Initialize(integration_points_number);

    //transfer element values to condition
    TransferCurrentBoundaryData(rCurrentElement, rCurrentCondition, rDataVariables, Values, ValueArrays, rCurrentProcessInfo);
  }


  //*******************************************************************************************
  //*******************************************************************************************

  // interpolate nodal variables using DataValueContainer
  static inline void InterpolateStepData(NodeType &rNode,
                                         const GeometryType &rGeometry,
                                         const Vector &rShapeFunctions,
                                         const unsigned int &step_data_size,
                                         const double alpha)
  {
    InterpolateStepData(rNode.SolutionStepData(), rNode, rGeometry, rShapeFunctions, step_data_size, alpha);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  // interpolate nodal variables using DataValueContainer
  static inline void InterpolateStepData(VariablesContainer &rValueContainer,
                                         const NodeType &rNode,
                                         const GeometryType &rGeometry,
                                         const Vector &rShapeFunctions,
                                         const unsigned int &step_data_size,
                                         const double alpha)
  {

    const unsigned int &buffer_size = rNode.GetBufferSize();
    rValueContainer = rNode.SolutionStepData(); //copy

    double data;
    for (unsigned int step = 0; step < buffer_size; ++step)
    {
      //getting the data of the solution step
      double *step_data = rValueContainer.Data(step);

      std::vector<double*> nodes_data;
      for (auto &i_node : rGeometry)
        nodes_data.push_back(i_node.SolutionStepData().Data(step));

      //copying this data in the position of the vector we are interested in
      for (unsigned int j = 0; j < step_data_size; ++j)
      {
        data = step_data[j] * (1 - alpha);
        for (unsigned int i = 0; i < rGeometry.size(); ++i)
          data += (alpha) * (rShapeFunctions[i] * nodes_data[i][j]);
        step_data[j] = data;
      }
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  // interpolate nodal variables identifying variable types
  static inline void InterpolateStepData(NodeType &rNode,
                                         const GeometryType &rGeometry,
                                         const Vector &rShapeFunctions,
                                         const VariablesList &rVariablesList,
                                         const double alpha = 1.0)
  {
    InterpolateStepData(rNode.SolutionStepData(), rNode, rGeometry, rShapeFunctions, rVariablesList, alpha);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  // interpolate nodal variables identifying variable types
  static inline void InterpolateStepData(VariablesContainer &rValueContainer,
                                         const NodeType &rNode,
                                         const GeometryType &rGeometry,
                                         const Vector &rShapeFunctions,
                                         const VariablesList &rVariablesList,
                                         const double alpha = 1.0)
  {
    rValueContainer = rNode.SolutionStepData(); //copy

    const unsigned int buffer_size = rNode.GetBufferSize();
    const unsigned int size = rGeometry.size();

    bool all_null = true;
    for (auto &i_shape : rShapeFunctions)
      if (i_shape > std::numeric_limits<double>::epsilon())
        all_null = false;

    if (all_null)
      KRATOS_ERROR << "SOMETHING is wrong with the Interpolation Functions" << std::endl;

    for (const auto &i_variable : rVariablesList)
    {
      std::string variable_name = i_variable.Name();
      double data;
      if (KratosComponents<Variable<double>>::Has(variable_name))
      {
        const Variable<double>& variable = KratosComponents<Variable<double>>::Get(variable_name);
        for (unsigned int step = 0; step < buffer_size; ++step)
        {
          //getting the data of the solution step
          double &node_data = rValueContainer.FastGetValue(variable, step);

          std::vector<const double *> nodes_data;
          for (auto &i_node : rGeometry)
            nodes_data.push_back(&i_node.FastGetSolutionStepValue(variable, step));

          if (alpha != 1)
          {
            data = node_data * (1 - alpha);
            for (unsigned int i = 0; i < size; ++i)
              data += (alpha) * (rShapeFunctions[i] * (*nodes_data[i]));
          }
          else
          {
            data = (rShapeFunctions[0] * (*nodes_data[0]));
            for (unsigned int i = 1; i < size; ++i)
              data += (rShapeFunctions[i] * (*nodes_data[i]));
          }
          node_data = data;
        }

      }
      else if (KratosComponents<Variable<array_1d<double, 3>>>::Has(variable_name))
      {
        const Variable<array_1d<double, 3>>& variable = KratosComponents<Variable<array_1d<double, 3>>>::Get(variable_name);
        array_1d<double, 3> data;
        for (unsigned int step = 0; step < buffer_size; ++step)
        {
          //getting the data of the solution step
          array_1d<double, 3> &node_data = rValueContainer.FastGetValue(variable, step);
          std::vector<const array_1d<double, 3> *> nodes_data;
          for (auto &i_node : rGeometry)
            nodes_data.push_back(&i_node.FastGetSolutionStepValue(variable, step));

          if (alpha != 1)
          {
            data = node_data * (1 - alpha);
            for (unsigned int i = 0; i < size; ++i)
              data += (alpha) * (rShapeFunctions[i] * (*nodes_data[i]));
          }
          else
          {
            data = (rShapeFunctions[0] * (*nodes_data[0]));
            for (unsigned int i = 1; i < size; ++i)
              data += (rShapeFunctions[i] * (*nodes_data[i]));
          }
          node_data = data;
        }
      }
      else if (KratosComponents<Variable<Matrix>>::Has(variable_name))
      {
        const Variable<Matrix>& variable = KratosComponents<Variable<Matrix>>::Get(variable_name);
        Matrix data;
        for (unsigned int step = 0; step < buffer_size; ++step)
        {
          //getting the data of the solution step
          Matrix &node_data = rValueContainer.FastGetValue(variable, step);
          std::vector<const Matrix *> nodes_data;
          for (auto &i_node : rGeometry)
            nodes_data.push_back(&i_node.FastGetSolutionStepValue(variable, step));

          if (node_data.size1() > 0 && node_data.size2())
          {
            bool same_size = true;

            for (unsigned int i = 0; i < size; ++i)
              if (node_data.size1() != (*nodes_data[i]).size1() && node_data.size2() != (*nodes_data[i]).size2())
                same_size = false;

            if (same_size)
            {
              if (alpha != 1)
              {
                data = node_data * (1 - alpha);
                for (unsigned int i = 0; i < size; ++i)
                  data += (alpha) * (rShapeFunctions[i] * (*nodes_data[i]));
              }
              else
              {
                data = (rShapeFunctions[0] * (*nodes_data[0]));
                for (unsigned int i = 1; i < size; ++i)
                  data += (rShapeFunctions[i] * (*nodes_data[i]));
              }
              node_data = data;
            }
          }
        }
      }
      else if (KratosComponents<Variable<Vector>>::Has(variable_name))
      {
        const Variable<Vector>& variable = KratosComponents<Variable<Vector>>::Get(variable_name);
        Vector data;
        for (unsigned int step = 0; step < buffer_size; ++step)
        {
          //getting the data of the solution step
          Vector &node_data = rValueContainer.FastGetValue(variable, step);

          std::vector<const Vector *> nodes_data;
          for (auto &i_node : rGeometry)
            nodes_data.push_back(&i_node.FastGetSolutionStepValue(variable, step));

          if (node_data.size() > 0)
          {
            bool same_size = true;
            for (unsigned int i = 0; i < size; ++i)
              if (node_data.size() != (*nodes_data[i]).size())
                same_size = false;

            if (same_size)
            {
              if (alpha != 1)
              {
                data = node_data * (1 - alpha);
                for (unsigned int i = 0; i < size; ++i)
                  data += (alpha) * (rShapeFunctions[i] * (*nodes_data[i]));
              }
              else
              {
                data = (rShapeFunctions[0] * (*nodes_data[0]));
                for (unsigned int i = 1; i < size; ++i)
                  data += (rShapeFunctions[i] * (*nodes_data[i]));
              }
              node_data = data;
            }
          }
        }
      }
      else if (KratosComponents<Variable<std::string>>::Has(variable_name))
      {
        //NO INTERPOLATION
        const Variable<std::string>& variable = KratosComponents<Variable<std::string>>::Get(variable_name);
        for (unsigned int step = 0; step < buffer_size; ++step)
        {
          //assign data from the first node
          rValueContainer.FastGetValue(variable, step) = rGeometry[0].FastGetSolutionStepValue(variable, step);
        }
      }
      else if (KratosComponents<Variable<int>>::Has(variable_name))
      {
        //NO INTERPOLATION
        const Variable<int>& variable = KratosComponents<Variable<int>>::Get(variable_name);
        for (unsigned int step = 0; step < buffer_size; ++step)
        {
          //assign data from the first node
          rValueContainer.FastGetValue(variable, step) = rGeometry[0].FastGetSolutionStepValue(variable, step);
        }
      }
      else if (KratosComponents<Variable<bool>>::Has(variable_name))
      {
        //NO INTERPOLATION
        const Variable<bool>& variable = KratosComponents<Variable<bool>>::Get(variable_name);
        for (unsigned int step = 0; step < buffer_size; ++step)
        {
          //assign data from the first node
          rValueContainer.FastGetValue(variable, step) = rGeometry[0].FastGetSolutionStepValue(variable, step);
        }
      }
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CreateElementsCPP(ModelPart &rModelPart,
                                       std::vector<GeometryType> &rElementGeometries)
  {

    std::vector<NodeType::Pointer> current_centers(rElementGeometries.size());
    IndexPartition<unsigned int>(rElementGeometries.size()).for_each(
        [&](unsigned int i){
          //current_centers[i] = Kratos::make_intrusive<NodeType>(i+1, rElementGeometries[i].Center());
          current_centers[i] = Kratos::make_intrusive<NodeType>(i+1, GeometryUtilities::CurrentCenter(rElementGeometries[i]));
        }
    );

    std::vector<unsigned int> ids(ModelPartUtilities::GetMaxElementId(rModelPart) + 1);
    int i = 0;
    for (auto& i_elem : rModelPart.Elements())
      ids[i_elem.Id()] = i++;

    std::vector<NodeType::Pointer> previous_centers(rModelPart.NumberOfElements());
    block_for_each(rModelPart.Elements(),[&](Element &i_elem){
      const unsigned int &id = i_elem.Id();
      //previous_centers[ids[id]] = Kratos::make_intrusive<NodeType>(id, i_elem.GetGeometry().Center());
      previous_centers[ids[id]] = Kratos::make_intrusive<NodeType>(id, GeometryUtilities::OriginalCenter(i_elem.GetGeometry()));
    }
    );

    //create search tree
    KdtreeType::Pointer SearchTree;
    SearchUtilities::CreateSearchTree(SearchTree, previous_centers, 20);

    //make a loop on temporal elements
    std::vector<Element::Pointer> ElementsVector(rElementGeometries.size());
    ModelPart::ElementsContainerType::iterator it_begin = rModelPart.ElementsBegin();

    block_for_each(current_centers,[&](NodeType::Pointer &i_center){
      double distance;
      const unsigned int &id = i_center->Id();
      ModelPart::ElementsContainerType::iterator it_elem = it_begin + ids[(SearchTree->SearchNearestPoint(*i_center, distance))->Id()];

      ElementsVector[id-1] = it_elem->Clone(id, rElementGeometries[id-1]);

      // label non-coincident projections for selective post SPR
      //if (it_elem->Is(VISITED) || distance>1e-11)
      if (it_elem->Is(VISITED))
        ElementsVector[id-1]->Set(VISITED,false);
      else{
        ElementsVector[id-1]->Set(VISITED,true);
        it_elem->Set(VISITED,true);
      }
    });

    ModelPart::ElementsContainerType NewElements(ElementsVector.begin(), ElementsVector.end());

    //setting new elements
    rModelPart.Elements().swap(NewElements);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CreateElementsSPR(ModelPart &rModelPart,
                                       const DataVariables &rDataVariables,
                                       std::vector<GeometryType> &rElementGeometries)
  {
    unsigned int number_of_nodes = ModelPartUtilities::GetMaxNodeId(rModelPart) + 1;
    std::vector<DataValuesArrays> NodalDataList(number_of_nodes);
    GetSPRNodalData(rModelPart, NodalDataList, rDataVariables);

    CreateElementsCPP(rModelPart, rElementGeometries);

    //make a loop on temporal elements
    // std::vector<Element::Pointer> ElementsVector(rElementGeometries.size());
    // ModelPart::ElementsContainerType::iterator it_begin = rModelPart.ElementsBegin();

    // IndexPartition<unsigned int>(rElementGeometries.size()).for_each(
    //     [&](unsigned int i){
    //       ElementsVector[i] = it_begin->Clone(i+1, rElementGeometries[i]);
    //     }
    // );
    // ModelPart::ElementsContainerType NewElements(ElementsVector.begin(), ElementsVector.end());
    // //setting new elements
    // rModelPart.Elements().swap(NewElements);

    AssignElementalValues(rModelPart, NodalDataList, rDataVariables);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void GetSPRNodalData(ModelPart &rModelPart,
                                     std::vector<DataValuesArrays> &NodalDataList,
                                     const DataVariables &rDataVariables)
  {
    KRATOS_TRY

    const ProcessInfo &rCurrentProcessInfo = rModelPart.GetProcessInfo();
    const GeometryType &rModelGeometry = rModelPart.Elements().front().GetGeometry();
    const unsigned int integration_points_number = rModelGeometry.IntegrationPointsNumber(rModelGeometry.GetDefaultIntegrationMethod());
    const unsigned int &dimension = rCurrentProcessInfo[SPACE_DIMENSION];

    block_for_each(rModelPart.Nodes(),[&](NodeType &i_node){
    //for (auto &i_node : rModelPart.Nodes()){
      if (i_node.IsNot(RIGID))
      {
        DataValuesArrays Nodal;
        Nodal.Initialize(rDataVariables);
        bool success = false;
        ElementWeakPtrVectorType &nElements = i_node.GetValue(NEIGHBOUR_ELEMENTS);
        if(nElements.size() > dimension)
        {
          success = GetSPRValues(i_node, nElements, Nodal, rDataVariables, rCurrentProcessInfo);
        }
        else{
          ElementWeakPtrVectorType nExtendedElements = nElements;
          if (SearchUtilities::ExtendPatchNeighbours(i_node, nExtendedElements, dimension)){
            success = GetSPRValues(i_node, nExtendedElements, Nodal, rDataVariables, rCurrentProcessInfo);
          }
        }
        if (!success){
          DataValuesArrays Elemental;
          Elemental.Initialize(integration_points_number);

          if (nElements.size()!=0){
            GetElementValues(i_node, nElements, Nodal, Elemental, rDataVariables, rCurrentProcessInfo);
          }
          else{ // this is a problem new created node with no neighbours
            InitializeDataValues(rModelPart.Elements().front(), Nodal, Elemental, rDataVariables, rCurrentProcessInfo);
          }
        }
        NodalDataList[i_node.Id()] = Nodal;
      }

    });

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void AssignElementalValues(ModelPart &rModelPart,
                                           std::vector<DataValuesArrays> &rNodalDataList,
                                           const DataVariables &rDataVariables)
  {
    KRATOS_TRY

    const ProcessInfo &rCurrentProcessInfo = rModelPart.GetProcessInfo();
    const GeometryType &rModelGeometry = rModelPart.Elements().front().GetGeometry();
    const unsigned int integration_points_number = rModelGeometry.IntegrationPointsNumber(rModelGeometry.GetDefaultIntegrationMethod());

    block_for_each(rModelPart.Elements(),[&](Element &i_elem){
    //for (auto &i_elem : rModelPart.Elements()){
      if (i_elem.IsNot(VISITED))
      {
        std::cout<<" SPR element "<<i_elem.Id()<<std::endl;
        const GeometryType &rGeometry = i_elem.GetGeometry();
        const Matrix &Ncontainer = rGeometry.ShapeFunctionsValues(rGeometry.GetDefaultIntegrationMethod());
        const unsigned int &size = rGeometry.size();

        DataValuesArrays Elemental;
        Elemental.Initialize(integration_points_number);

        //shape functions
        Vector N;
        //double
        for (unsigned int i = 0; i < rDataVariables.DoubleVariables.size(); ++i)
        {
          std::fill(Elemental.DoubleValueArray.begin(), Elemental.DoubleValueArray.end(), 0.0);

          for (unsigned int j = 0; j < integration_points_number; ++j)
          {
            N = row(Ncontainer, j);
            //nodal value
            for (unsigned int k = 0; k < size; ++k)
              Elemental.DoubleValueArray[j] += N[k] * rNodalDataList[rGeometry[k].Id()].DoubleValueArray[i];
          }

          i_elem.SetValuesOnIntegrationPoints(*(rDataVariables.DoubleVariables[i]), Elemental.DoubleValueArray, rCurrentProcessInfo);
        }
        //array_1d
        for (unsigned int i = 0; i < rDataVariables.Array1DVariables.size(); ++i)
        {
          for (unsigned int j = 0; j < integration_points_number; ++j)
            Elemental.Array1DValueArray[j].clear();

          for (unsigned int j = 0; j < integration_points_number; ++j)
          {
            N = row(Ncontainer, j);
            //nodal value
            for (unsigned int k = 0; k < size; ++k)
              Elemental.Array1DValueArray[j] += N[k] * rNodalDataList[rGeometry[k].Id()].Array1DValueArray[i];
          }

          i_elem.SetValuesOnIntegrationPoints(*(rDataVariables.Array1DVariables[i]), Elemental.Array1DValueArray, rCurrentProcessInfo);
        }
        //Vector
        for (unsigned int i = 0; i < rDataVariables.VectorVariables.size(); ++i)
        {
          Elemental.VectorValueArray.resize(0);
          i_elem.CalculateOnIntegrationPoints(*(rDataVariables.VectorVariables[i]), Elemental.VectorValueArray, rCurrentProcessInfo);
          for (unsigned int j = 0; j < integration_points_number; ++j)
          {
            if (Elemental.VectorValueArray[j].size() != 0){
              Elemental.VectorValueArray[j] = ZeroVector(Elemental.VectorValueArray[j].size());
            }
            else
              Elemental.VectorValueArray[j] = ZeroVector(0); //¿value?
          }

          for (unsigned int j = 0; j < integration_points_number; ++j)
          {
            N = row(Ncontainer, j);
            //nodal value
            for (unsigned int k = 0; k < size; ++k)
            {
              Elemental.VectorValueArray[j] += N[k] * rNodalDataList[rGeometry[k].Id()].VectorValueArray[i];
            }

          }
          i_elem.SetValuesOnIntegrationPoints(*(rDataVariables.VectorVariables[i]), Elemental.VectorValueArray, rCurrentProcessInfo);
        }
        //Matrix
        for (unsigned int i = 0; i < rDataVariables.MatrixVariables.size(); ++i)
        {
          Elemental.MatrixValueArray.resize(0);
          i_elem.CalculateOnIntegrationPoints(*(rDataVariables.MatrixVariables[i]), Elemental.MatrixValueArray, rCurrentProcessInfo);

          for (unsigned int j = 0; j < integration_points_number; ++j)
          {
            if (Elemental.MatrixValueArray[j].size1() != 0 && Elemental.MatrixValueArray[j].size2() != 0)
              Elemental.MatrixValueArray[j] = ZeroMatrix(Elemental.MatrixValueArray[j].size1(), Elemental.MatrixValueArray[j].size2());
            else
              Elemental.MatrixValueArray[j] = ZeroMatrix(0, 0); //¿value?
          }

          for (unsigned int j = 0; j < integration_points_number; ++j)
          {
            N = row(Ncontainer, j);
            //nodal value
            for (unsigned int k = 0; k < size; ++k)
              Elemental.MatrixValueArray[j] += N[k] * rNodalDataList[rGeometry[k].Id()].MatrixValueArray[i];
          }

          i_elem.SetValuesOnIntegrationPoints(*(rDataVariables.MatrixVariables[i]), Elemental.MatrixValueArray, rCurrentProcessInfo);
        }
      }
    });

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void GetVariablesDataList(ModelPart &rModelPart,
                                          std::vector<VariablesListDataValueContainer> &rVariablesListVector,
                                          std::vector<bool> &rUniquePosition)
  {

    unsigned int number_of_nodes = ModelPartUtilities::GetMaxNodeId(rModelPart) + 1;
    rVariablesListVector.resize(number_of_nodes);
    rUniquePosition.resize(number_of_nodes);
    std::fill(rUniquePosition.begin(), rUniquePosition.end(), false);

    std::vector<Flags> BlockFlags = {TO_ERASE, INSIDE, ISOLATED};

    //geometry
    //const VariablesList &rVariablesList = rModelPart.GetNodalSolutionStepVariablesList();
    const unsigned int &step_data_size = rModelPart.GetNodalSolutionStepDataSize();

    //Create new node information //crashes in parallel
    //block_for_each(rModelPart.Elements(),[&](Element &i_elem){
    for (auto &i_elem : rModelPart.Elements()){
      //coordinates
      const GeometryType &rGeometry = i_elem.GetGeometry();
      std::vector<VectorType> VerticesCoordinates;
      for (auto &i_node : rGeometry)
        VerticesCoordinates.push_back(i_node.GetInitialPosition()+i_node.FastGetSolutionStepValue(DISPLACEMENT));
      //shape functions
      Vector ShapeFunctions;
      for (auto &i_node : rGeometry)
      {
        const unsigned int& id = i_node.Id();
        if (i_node.IsNot(BlockFlags) && !rUniquePosition[id]) // is false
        {
          if (GeometryUtilities::CalculatePosition(VerticesCoordinates, i_node.Coordinates(), ShapeFunctions)) //is_inside
          {
            //InterpolateStepData(rVariablesListVector[id], i_node, i_elem.GetGeometry(), ShapeFunctions, rVariablesList, 1.0); // [0,1] smoothing level of the interpolation

            //no thread safe
            InterpolateStepData(rVariablesListVector[id], i_node, rGeometry, ShapeFunctions, step_data_size, 1.0); // [0,1] smoothing level of the interpolation
            rUniquePosition[id] = true;
          }
        }
      }
    }//);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SetNodeModelParts(ModelPart &rModelPart, const GeometryType& rGeometry, NodeType &rNode)
  {
    //set model part name
    rNode.SetValue(MODEL_PART_NAME, rModelPart.Name());

    //set boundary model part name list from one of the condition nodes
    std::vector<std::vector<std::string>> SubModelPartsLists;
    for (auto& i_node: rGeometry)
    {
      SubModelPartsLists.push_back(i_node.GetValue(MODEL_PART_NAMES));
      std::sort(SubModelPartsLists.back().begin(), SubModelPartsLists.back().end());
    }
    std::vector<std::string> SubModelParts = SubModelPartsLists.front();

    for (unsigned int i=1; i<rGeometry.size(); ++i)
    {
      std::vector<std::string> CommonModelParts;
      std::set_intersection(SubModelParts.begin(), SubModelParts.end(),
                            SubModelPartsLists[i].begin(), SubModelPartsLists[i].end(), std::back_inserter(CommonModelParts));
      SubModelParts = CommonModelParts;
    }
    rNode.SetValue(MODEL_PART_NAMES, SubModelParts);

  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void ClearNodeContactForce(const GeometryType& rGeometry, NodeType &rNode)
  {
    //clear contact force
    bool clear_contact = false;
    for (auto &i_node : rGeometry)
      if (i_node.SolutionStepsDataHas(CONTACT_FORCE))
      {
        if (norm_2(i_node.FastGetSolutionStepValue(CONTACT_FORCE)) == 0)
          clear_contact = true;
      }
      else
        clear_contact = true;

    if (clear_contact)
      noalias(rNode.FastGetSolutionStepValue(CONTACT_FORCE)) = ZeroVector(3);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SetNodeVariables(ModelPart &rModelPart, const GeometryType& rGeometry, NodeType &rNode)
  {
    //set nodal_h
    //rNode.FastGetSolutionStepValue(NODAL_H) = rGeometry.Length();

    //set original position
    array_1d<double, 3> &InitialPosition = rNode.GetInitialPosition();
    InitialPosition = rNode.Coordinates() - rNode.FastGetSolutionStepValue(DISPLACEMENT);

    //set model parts
    SetNodeModelParts(rModelPart, rGeometry, rNode);

    //clear contact force
    ClearNodeContactForce(rGeometry, rNode);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SetBoundaryNodeVariables(ModelPart &rModelPart, const GeometryType& rGeometry, const GeometryType& rBoundary, NodeType &rNode)
  {
    //set flags
    rNode.Set(BOUNDARY);

    //set nodal_h
    //rNode.FastGetSolutionStepValue(NODAL_H) = rBoundary.Length();

    //set normal
    array_1d<double, 3> &rNormal = rNode.FastGetSolutionStepValue(NORMAL);
    if (rGeometry.size() == rBoundary.size())
      GeometryUtilities::CalculateBoundaryUnitNormal(rGeometry,rNormal);
    //std::cout<<" Calculate NORMAL "<<rNormal<<std::endl;

    //set original position
    array_1d<double, 3> &InitialPosition = rNode.GetInitialPosition();
    InitialPosition = rNode.Coordinates() - rNode.FastGetSolutionStepValue(DISPLACEMENT);

    //set model parts
    SetNodeModelParts(rModelPart, rBoundary, rNode);

    //clear contact force
    ClearNodeContactForce(rGeometry, rNode);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SearchVariablesDataList(ModelPart &rModelPart,
                                             std::vector<VariablesListDataValueContainer> &rVariablesListVector,
                                             std::vector<bool> &rUniquePosition)
  {
    const unsigned int &step_data_size = rModelPart.GetNodalSolutionStepDataSize();

    //create search tree
    KdtreeType::Pointer SearchTree;
    std::vector<Node<3>::Pointer> list_of_nodes;
    ModelPartUtilities::CreateSearchTree(rModelPart, SearchTree, list_of_nodes, 20, {TO_ERASE.AsFalse()});

    //Find out where the new nodes belong to:
    unsigned int number_of_nodes = ModelPartUtilities::GetMaxNodeId(rModelPart) + 1;

    rVariablesListVector.resize(number_of_nodes);
    rUniquePosition.resize(number_of_nodes);
    std::fill(rUniquePosition.begin(), rUniquePosition.end(), false);

    //VariablesList &rVariablesList = rModelPart.GetNodalSolutionStepVariablesList();

    //find the center and "radius" of the element
    double radius = 0;
    Node<3> center(0, 0.0, 0.0, 0.0);
    Vector ShapeFunctions;

    //all of the nodes in this list will be preserved
    unsigned int num_neighbours = list_of_nodes.size();
    std::vector<Node<3>::Pointer> neighbours(num_neighbours);
    std::vector<double> neighbour_distances(num_neighbours);

    //geometry
    const unsigned int number_of_vertices = rModelPart.Elements().front().GetGeometry().size();
    std::vector<VectorType> VerticesCoordinates(number_of_vertices);

    std::vector<Flags> BlockFlags = {TO_ERASE, INSIDE, ISOLATED};
    unsigned int n_points_in_radius;
    for (const auto& i_elem : rModelPart.Elements())
    {
      //coordinates
      unsigned int i = 0;
      for (auto &i_node : i_elem.GetGeometry())
        VerticesCoordinates[i++] = i_node.GetInitialPosition()+i_node.FastGetSolutionStepValue(DISPLACEMENT);

      GeometryUtilities::CalculateCenterAndRadius(VerticesCoordinates, center.Coordinates(), radius);
      n_points_in_radius = SearchTree->SearchInRadius(center, radius * 0.98, neighbours.begin(), neighbour_distances.begin(), num_neighbours);
      //std::cout<<" Element ["<<i_elem.Id()<<"] points in radius:"<<n_points_in_radius<<std::endl;

      //check if inside and eventually interpolate
      for (std::vector<Node<3>::Pointer>::iterator i_nnode = neighbours.begin(); i_nnode != neighbours.begin() + n_points_in_radius; ++i_nnode)
      {
        const unsigned int& id = (*i_nnode)->Id();
        if ((*i_nnode)->IsNot(BlockFlags) && !rUniquePosition[id]) // is false
        {
          if (GeometryUtilities::CalculatePosition(VerticesCoordinates, (*i_nnode)->Coordinates(), ShapeFunctions)) //is_inside
          {
            //InterpolateStepData(rVariablesListVector[id], **i_nnode, i_elem.GetGeometry(), ShapeFunctions, rVariablesList, 1.0); // [0,1] smoothing level of the interpolation

            InterpolateStepData(rVariablesListVector[id], **i_nnode, i_elem.GetGeometry(), ShapeFunctions, step_data_size, 1.0); // [0,1] smoothing level of the interpolation

            rUniquePosition[id] = true;
          }
        }
      }
    }
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline void InterpolateNodalVariables(ModelPart &rModelPart)
  {
    std::vector<VariablesListDataValueContainer> VariablesListVector;
    std::vector<bool> UniquePosition;

    // the interpolation of the variables from data_transfer_utilities is the more time consuming part
    GetVariablesDataList(rModelPart,VariablesListVector,UniquePosition);

    //alternative method with search tree
    //SearchVariablesDataList(rModelPart,VariablesListVector,UniquePosition);

    //Create new node information
    block_for_each(rModelPart.Nodes(),[&](NodeType &i_node){
      // for (auto &i_node : rModelPart.Nodes())
      // {
      const unsigned int& id = i_node.Id();
      if (UniquePosition[id]) // is true
      {
        // information will be projected nodes:
        i_node.SolutionStepData() = VariablesListVector[id];
      }
    }
    );
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void TransferCurrentBoundaryData(Element &rCurrentElement,
                                                 Condition &rCurrentCondition,
                                                 const DataVariables &rDataVariables,
                                                 DataValues &rValues,
                                                 DataValuesArrays &rValueArrays,
                                                 const ProcessInfo &rCurrentProcessInfo)
  {
    //double
    for (unsigned int i = 0; i < rDataVariables.DoubleVariables.size(); ++i)
    {
      rCurrentElement.CalculateOnIntegrationPoints(*(rDataVariables.DoubleVariables[i]), rValueArrays.DoubleValueArray, rCurrentProcessInfo);

      //if there is more than one integration point, an average or an interpolation is need
      rValues.DoubleVariable = rValueArrays.DoubleValueArray[0];
      for (unsigned int j = 1; j < rValueArrays.array_size; ++j)
      {
        rValues.DoubleVariable += rValueArrays.DoubleValueArray[j];
      }
      rValues.DoubleVariable *= (1.0 / double(rValueArrays.array_size));
      rCurrentCondition.SetValue(*(rDataVariables.DoubleVariables[i]), rValues.DoubleVariable);
    }

    //array_1d
    for (unsigned int i = 0; i < rDataVariables.Array1DVariables.size(); ++i)
    {
      rCurrentElement.CalculateOnIntegrationPoints(*(rDataVariables.Array1DVariables[i]), rValueArrays.Array1DValueArray, rCurrentProcessInfo);

      //if there is more than one integration point, an average or an interpolation is need
      rValues.Array1DVariable = rValueArrays.Array1DValueArray[0];
      for (unsigned int j = 1; j < rValueArrays.array_size; ++j)
      {
        rValues.Array1DVariable += rValueArrays.Array1DValueArray[j];
      }
      rValues.Array1DVariable *= (1.0 / double(rValueArrays.array_size));
      rCurrentCondition.SetValue(*(rDataVariables.Array1DVariables[i]), rValues.Array1DVariable);
    }

    //Vector
    for (unsigned int i = 0; i < rDataVariables.VectorVariables.size(); ++i)
    {
      rValueArrays.VectorValueArray.resize(0);
      rCurrentElement.CalculateOnIntegrationPoints(*(rDataVariables.VectorVariables[i]), rValueArrays.VectorValueArray, rCurrentProcessInfo);

      //if there is more than one integration point, an average or an interpolation is need
      rValues.VectorVariable = rValueArrays.VectorValueArray[0];
      for (unsigned int j = 1; j < rValueArrays.array_size; ++j)
      {
        rValues.VectorVariable += rValueArrays.VectorValueArray[j];
      }
      rValues.VectorVariable *= (1.0 / double(rValueArrays.array_size));
      rCurrentCondition.SetValue(*(rDataVariables.VectorVariables[i]), rValues.VectorVariable);
    }

    //Matrix
    for (unsigned int i = 0; i < rDataVariables.MatrixVariables.size(); ++i)
    {
      rValueArrays.MatrixValueArray.resize(0);
      rCurrentElement.CalculateOnIntegrationPoints(*(rDataVariables.MatrixVariables[i]), rValueArrays.MatrixValueArray, rCurrentProcessInfo);

      //if there is more than one integration point, an average or an interpolation is need
      rValues.MatrixVariable = rValueArrays.MatrixValueArray[0];
      for (unsigned int j = 1; j < rValueArrays.array_size; ++j)
      {
        rValues.MatrixVariable += rValueArrays.MatrixValueArray[i];
      }
      rValues.MatrixVariable *= (1.0 / double(rValueArrays.array_size));
      rCurrentCondition.SetValue(*(rDataVariables.MatrixVariables[i]), rValues.MatrixVariable);
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void TransferNodalValuesToLabeledElements(ModelPart &rModelPart,
                                                          const DataVariables &rDataVariables,
                                                          const Flags Label,
                                                          const double alpha = 0.25)
  {
    //double alpha = 0.25; //[0,1] //smoothing level of the Jacobian
    const ProcessInfo &rCurrentProcessInfo = rModelPart.GetProcessInfo();
    const GeometryType &rModelGeometry = rModelPart.Elements().front().GetGeometry();
    unsigned int integration_points_number = rModelGeometry.IntegrationPointsNumber(rModelGeometry.GetDefaultIntegrationMethod());

    block_for_each(rModelPart.Elements(),[&](Element &i_elem){
    //for (auto &i_elem : rModelPart.Elements()){

      const GeometryType &rGeometry = i_elem.GetGeometry();

      if (i_elem.IsNot(RIGID) && SetFlagsUtilities::CheckNodeFlag(rGeometry,{Label}))
      {
        const Matrix &Ncontainer = rGeometry.ShapeFunctionsValues(rGeometry.GetDefaultIntegrationMethod());
        const unsigned int &size = rGeometry.size();

        DataValuesArrays Nodal, Elemental;
        Nodal.Initialize(integration_points_number);
        Elemental.Initialize(integration_points_number);

        //shape functions
        Vector N;

        //double
        for (unsigned int i = 0; i < rDataVariables.DoubleVariables.size(); ++i)
        {
          //elemental value
          i_elem.CalculateOnIntegrationPoints(*(rDataVariables.DoubleVariables[i]), Elemental.DoubleValueArray, rCurrentProcessInfo);

          std::fill(Nodal.DoubleValueArray.begin(), Nodal.DoubleValueArray.end(), 0.0);

          for (unsigned int j = 0; j < integration_points_number; ++j)
          {
            N = row(Ncontainer, j);

            //nodal value
            for (unsigned int k = 0; k < size; ++k)
              Nodal.DoubleValueArray[j] += N[k] * rGeometry[k].FastGetSolutionStepValue(*(rDataVariables.DoubleVariables[i]));

            Nodal.DoubleValueArray[j] *= (alpha);
            Nodal.DoubleValueArray[j] += (1 - alpha) * Elemental.DoubleValueArray[j];
          }

          i_elem.SetValuesOnIntegrationPoints(*(rDataVariables.DoubleVariables[i]), Nodal.DoubleValueArray, rCurrentProcessInfo);
        }

        //array_1d
        for (unsigned int i = 0; i < rDataVariables.Array1DVariables.size(); ++i)
        {
          //elemental value
          i_elem.CalculateOnIntegrationPoints(*(rDataVariables.Array1DVariables[i]), Elemental.Array1DValueArray, rCurrentProcessInfo);

          for (unsigned int j = 0; j < integration_points_number; ++j)
            Nodal.Array1DValueArray[j].clear();

          for (unsigned int j = 0; j < integration_points_number; ++j)
          {
            N = row(Ncontainer, j);

            //nodal value
            for (unsigned int k = 0; k < size; ++k)
              Nodal.Array1DValueArray[j] += N[k] * rGeometry[k].FastGetSolutionStepValue(*(rDataVariables.Array1DVariables[i]));

            Nodal.Array1DValueArray[j] *= (alpha);
            Nodal.Array1DValueArray[j] += (1 - alpha) * Elemental.Array1DValueArray[j];
          }

          i_elem.SetValuesOnIntegrationPoints(*(rDataVariables.Array1DVariables[i]), Nodal.Array1DValueArray, rCurrentProcessInfo);
        }

        //Vector
        for (unsigned int i = 0; i < rDataVariables.VectorVariables.size(); ++i)
        {
          //elemental value
          i_elem.CalculateOnIntegrationPoints(*(rDataVariables.VectorVariables[i]), Elemental.VectorValueArray, rCurrentProcessInfo);

          for (unsigned int j = 0; j < integration_points_number; ++j)
          {
            if (Elemental.VectorValueArray[j].size() != 0)
              Nodal.VectorValueArray[j] = ZeroVector(Elemental.VectorValueArray[j].size());
            else
              Nodal.VectorValueArray[j] = ZeroVector(0); //¿value?
          }

          for (unsigned int j = 0; j < integration_points_number; ++j)
          {
            N = row(Ncontainer, j);

            //nodal value
            for (unsigned int k = 0; k < size; ++k)
              Nodal.VectorValueArray[j] += N[k] * rGeometry[k].FastGetSolutionStepValue(*(rDataVariables.VectorVariables[i]));

            Nodal.VectorValueArray[j] *= (alpha);
            Nodal.VectorValueArray[j] += (1 - alpha) * Elemental.VectorValueArray[j];
          }

          i_elem.SetValuesOnIntegrationPoints(*(rDataVariables.VectorVariables[i]), Nodal.VectorValueArray, rCurrentProcessInfo);
        }

        //Matrix

        for (unsigned int i = 0; i < rDataVariables.MatrixVariables.size(); ++i)
        {
          //elemental value
          i_elem.CalculateOnIntegrationPoints(*(rDataVariables.MatrixVariables[i]), Elemental.MatrixValueArray, rCurrentProcessInfo);

          for (unsigned int j = 0; j < integration_points_number; ++j)
          {
            if (Elemental.MatrixValueArray[j].size1() != 0 && Elemental.MatrixValueArray[j].size2() != 0)
              Nodal.MatrixValueArray[j] = ZeroMatrix(Elemental.MatrixValueArray[j].size1(), Elemental.MatrixValueArray[j].size2());
            else
              Nodal.MatrixValueArray[j] = ZeroMatrix(0, 0); //¿value?
          }

          for (unsigned int j = 0; j < integration_points_number; ++j)
          {
            N = row(Ncontainer, j);

            //nodal value
            for (unsigned int k = 0; k < size; ++k)
              Nodal.MatrixValueArray[j] += N[k] * rGeometry[k].FastGetSolutionStepValue(*(rDataVariables.MatrixVariables[i]));

            Nodal.MatrixValueArray[j] *= (alpha);
            Nodal.MatrixValueArray[j] += (1 - alpha) * Elemental.MatrixValueArray[j];
          }

          i_elem.SetValuesOnIntegrationPoints(*(rDataVariables.MatrixVariables[i]), Nodal.MatrixValueArray, rCurrentProcessInfo);
        }
      }
    });
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline void LabelNodesOnThreshold(ModelPart &rModelPart,
                                           const Variable<double> &rCriticalVariable,
                                           const double &rCriticalValue,
                                           const Flags Label,
                                           const bool specific = false)
  {
    const ProcessInfo &rCurrentProcessInfo = rModelPart.GetProcessInfo();

    block_for_each(rModelPart.Elements(),[&](Element &i_elem){
      if (MesherUtilities::CheckThreshold(i_elem, rCriticalVariable, rCriticalValue, rCurrentProcessInfo, specific))
        for (auto& i_node : i_elem.GetGeometry())
          i_node.Set(Label);
    });
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline void TransferNodalValuesToElementsOnThreshold(ModelPart &rModelPart,
                                                              const DataVariables &rDataVariables,
                                                              const Variable<double> &rCriticalVariable,
                                                              const double &rCriticalValue,
                                                              const bool specific = false,
                                                              const double alpha = 0.25)
  {
    KRATOS_TRY

    LabelNodesOnThreshold(rModelPart, rCriticalVariable, rCriticalValue, VISITED, specific);

    TransferNodalValuesToLabeledElements(rModelPart, rDataVariables, VISITED, alpha);

    SetFlagsUtilities::SetFlagsToNodes(rModelPart, {{VISITED}}, {VISITED.AsFalse()});

    KRATOS_CATCH("")
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline void TransferNodalValuesToElements(ModelPart &rModelPart,
                                                   const DataVariables &rDataVariables,
                                                   const double alpha = 0.25)
  {
    KRATOS_TRY

    SetFlagsUtilities::SetFlagsToNodes(rModelPart, {{VISITED.AsFalse()}}, {VISITED});

    TransferNodalValuesToLabeledElements(rModelPart, rDataVariables, VISITED, alpha);

    SetFlagsUtilities::SetFlagsToNodes(rModelPart, {{VISITED}}, {VISITED.AsFalse()});

    KRATOS_CATCH("")
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline void TransferElementalValuesToNodes(ModelPart &rModelPart,
                                                    const DataVariables &rDataVariables)
  {
    const ProcessInfo &rCurrentProcessInfo = rModelPart.GetProcessInfo();
    GeometryType &rModelGeometry = rModelPart.Elements().front().GetGeometry();
    const unsigned int integration_points_number = rModelGeometry.IntegrationPointsNumber(rModelGeometry.GetDefaultIntegrationMethod());

    DataValuesArrays Nodal, Elemental;
    Nodal.Initialize(rDataVariables);
    Elemental.Initialize(integration_points_number);

    const unsigned int buffer_size = rModelPart.GetBufferSize();
    VariablesList &variables_list = rModelPart.GetNodalSolutionStepVariablesList();

    for (auto &i_node : rModelPart.Nodes())
    {
      if (i_node.IsNot(RIGID))
      {
        //fill variables that are non assigned vectors
        FillVectorData(i_node, variables_list);

        //Initialize variables
        InitializeDataValues(rModelPart.Elements().front(), Nodal, Elemental, rDataVariables, rCurrentProcessInfo);

        ElementWeakPtrVectorType &nElements = i_node.GetValue(NEIGHBOUR_ELEMENTS);
        double Area = 0;
        double ElementArea = 0;

        for (auto &i_nelem : nElements)
        {
          const GeometryType &rGeometry = i_nelem.GetGeometry();
          ElementArea = rGeometry.Area();
          Area += ElementArea;

          //double
          for (unsigned int i = 0; i < rDataVariables.DoubleVariables.size(); ++i)
          {
            //elemental value
            i_nelem.CalculateOnIntegrationPoints(*(rDataVariables.DoubleVariables[i]), Elemental.DoubleValueArray, rCurrentProcessInfo);
            for (unsigned int j = 0; j < integration_points_number; ++j)
              Nodal.DoubleValueArray[i] += Elemental.DoubleValueArray[j] * ElementArea / double(integration_points_number);
          }

          //Array1D
          for (unsigned int i = 0; i < rDataVariables.Array1DVariables.size(); ++i)
          {
            //elemental value
            i_nelem.CalculateOnIntegrationPoints(*(rDataVariables.Array1DVariables[i]), Elemental.Array1DValueArray, rCurrentProcessInfo);
            for (unsigned int j = 0; j < integration_points_number; ++j)
              Nodal.Array1DValueArray[i] += Elemental.Array1DValueArray[j] * ElementArea / double(integration_points_number);
          }

          //Vector
          for (unsigned int i = 0; i < rDataVariables.VectorVariables.size(); ++i)
          {
            //elemental value
            i_nelem.CalculateOnIntegrationPoints(*(rDataVariables.VectorVariables[i]), Elemental.VectorValueArray, rCurrentProcessInfo);
            for (unsigned int j = 0; j < integration_points_number; ++j)
              Nodal.VectorValueArray[i] += Elemental.VectorValueArray[j] * ElementArea / double(integration_points_number);
          }

          //Matrix
          for (unsigned int i = 0; i < rDataVariables.MatrixVariables.size(); ++i)
          {
            //elemental value
            i_nelem.CalculateOnIntegrationPoints(*(rDataVariables.MatrixVariables[i]), Elemental.MatrixValueArray, rCurrentProcessInfo);
            for (unsigned int j = 0; j < integration_points_number; ++j)
              Nodal.MatrixValueArray[i] += Elemental.MatrixValueArray[j] * ElementArea / double(integration_points_number);
          }
        }

        if (Area != 0)
        {
          //double
          for (unsigned int i = 0; i < rDataVariables.DoubleVariables.size(); ++i)
          {
            Nodal.DoubleValueArray[i] /= Area;

            if (i_node.SolutionStepsDataHas(*(rDataVariables.DoubleVariables[i])))
              i_node.FastGetSolutionStepValue(*(rDataVariables.DoubleVariables[i])) = Nodal.DoubleValueArray[i];
            else
              KRATOS_WARNING("") << " TRANSFER: Something Wrong in node [" << i_node.Id() << "] : variable " << *(rDataVariables.DoubleVariables[i]) << " was not defined " << std::endl;
          }
          //Array1D
          for (unsigned int i = 0; i < rDataVariables.Array1DVariables.size(); ++i)
          {
            Nodal.Array1DValueArray[i] /= Area;

            if (i_node.SolutionStepsDataHas(*(rDataVariables.Array1DVariables[i])))
              i_node.FastGetSolutionStepValue(*(rDataVariables.Array1DVariables[i])) = Nodal.Array1DValueArray[i];
            else
              KRATOS_WARNING("") << " TRANSFER: Something Wrong in node [" << i_node.Id() << "] : variable " << *(rDataVariables.Array1DVariables[i]) << " was not defined " << std::endl;
          }
          //Vector
          for (unsigned int i = 0; i < rDataVariables.VectorVariables.size(); ++i)
          {
            Nodal.VectorValueArray[i] /= Area;

            if (i_node.SolutionStepsDataHas(*(rDataVariables.VectorVariables[i])))
            {
              i_node.FastGetSolutionStepValue(*(rDataVariables.VectorVariables[i])) = Nodal.VectorValueArray[i];
              //fill buffer if empty
              for (unsigned int step = 1; step < buffer_size; ++step)
              {
                if (i_node.FastGetSolutionStepValue(*(rDataVariables.VectorVariables[i]), step).size() == 0)
                {
                  i_node.FastGetSolutionStepValue(*(rDataVariables.VectorVariables[i]), step) = Nodal.VectorValueArray[i];
                  i_node.FastGetSolutionStepValue(*(rDataVariables.VectorVariables[i]), step).clear();
                }
              }
            }
            else
              KRATOS_WARNING("") << " TRANSFER: Something Wrong in node [" << i_node.Id() << "] : variable " << *(rDataVariables.VectorVariables[i]) << " was not defined " << std::endl;

          }
          //Matrix
          for (unsigned int i = 0; i < rDataVariables.MatrixVariables.size(); ++i)
          {
            Nodal.MatrixValueArray[i] /= Area;

            if (i_node.SolutionStepsDataHas(*(rDataVariables.MatrixVariables[i])))
            {
              i_node.FastGetSolutionStepValue(*(rDataVariables.MatrixVariables[i])) = Nodal.MatrixValueArray[i];
              //fill buffer if is empty
              for (unsigned int step = 1; step < buffer_size; ++step)
              {
                if (i_node.FastGetSolutionStepValue(*(rDataVariables.MatrixVariables[i]), step).size1() == 0 &&
                    i_node.FastGetSolutionStepValue(*(rDataVariables.MatrixVariables[i]), step).size2() == 0)
                {
                  i_node.FastGetSolutionStepValue(*(rDataVariables.MatrixVariables[i]), step) = Nodal.MatrixValueArray[i];
                  i_node.FastGetSolutionStepValue(*(rDataVariables.MatrixVariables[i]), step).clear();
                }
              }
            }
            else
              KRATOS_WARNING("") << " TRANSFER: Something Wrong in node [" << i_node.Id() << "] : variable " << *(rDataVariables.MatrixVariables[i]) << " was not defined " << std::endl;

          }
        }
        else
          KRATOS_WARNING("") << " TRANSFER: Something Wrong in node [" << i_node.Id() << "] : Area = 0 (neighbours: " << nElements.size() << ") " << std::endl;
      }
    }
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SmoothSPRElementalValuesOnThreshold(ModelPart &rModelPart,
                                                         const DataVariables &rDataVariables,
                                                         const Variable<double> &rCriticalVariable,
                                                         const double &rCriticalValue,
                                                         const bool specific = false,
                                                         const double alpha = 1.0)
  {
    KRATOS_TRY

    LabelNodesOnThreshold(rModelPart, rCriticalVariable, rCriticalValue, VISITED, specific);

    SPRLabeledElementalValues(rModelPart, rDataVariables, VISITED, alpha);

    SetFlagsUtilities::SetFlagsToNodes(rModelPart, {{VISITED}}, {VISITED.AsFalse()});

    KRATOS_CATCH("")
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SmoothSPRElementalValues(ModelPart &rModelPart,
                                              const DataVariables &rDataVariables,
                                              const double alpha = 1.0)
  {
    KRATOS_TRY

    SetFlagsUtilities::SetFlagsToNodes(rModelPart, {{VISITED.AsFalse()}}, {VISITED});

    SPRLabeledElementalValues(rModelPart, rDataVariables, VISITED, alpha);

    SetFlagsUtilities::SetFlagsToNodes(rModelPart, {{VISITED}}, {VISITED.AsFalse()});

    KRATOS_CATCH("")
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SPRLabeledElementalValues(ModelPart &rModelPart,
                                               const DataVariables &rDataVariables,
                                               const Flags Label,
                                               const double alpha = 1.0)
  {
    KRATOS_TRY

    const ProcessInfo &rCurrentProcessInfo = rModelPart.GetProcessInfo();
    const GeometryType &rModelGeometry = rModelPart.Elements().front().GetGeometry();
    const unsigned int integration_points_number = rModelGeometry.IntegrationPointsNumber(rModelGeometry.GetDefaultIntegrationMethod());
    const unsigned int &dimension = rCurrentProcessInfo[SPACE_DIMENSION];

    unsigned int number_of_nodes = ModelPartUtilities::GetMaxNodeId(rModelPart) + 1;
    std::vector<DataValuesArrays> NodalDataList(number_of_nodes);

    block_for_each(rModelPart.Nodes(),[&](NodeType &i_node){
    //for (auto &i_node : rModelPart.Nodes()){
      if (i_node.IsNot(RIGID))
      {
        DataValuesArrays Nodal;
        Nodal.Initialize(rDataVariables);
        bool success = false;
        ElementWeakPtrVectorType &nElements = i_node.GetValue(NEIGHBOUR_ELEMENTS);
        if(nElements.size() > dimension)
        {
          success = GetSPRValues(i_node, nElements, Nodal, rDataVariables, rCurrentProcessInfo);
        }
        else{
          ElementWeakPtrVectorType nExtendedElements = nElements;
          if (SearchUtilities::ExtendPatchNeighbours(i_node, nExtendedElements, dimension)){
            success = GetSPRValues(i_node, nExtendedElements, Nodal, rDataVariables, rCurrentProcessInfo);
          }
        }
        if (!success){
          DataValuesArrays Elemental;
          Elemental.Initialize(integration_points_number);
          GetElementValues(i_node, nElements, Nodal, Elemental, rDataVariables, rCurrentProcessInfo);
        }

        NodalDataList[i_node.Id()] = Nodal;
      }

    });

    RecoverElementalValues(rModelPart, NodalDataList, rDataVariables, Label, alpha);

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SmoothElementalValuesOnThreshold(ModelPart &rModelPart,
                                                      const DataVariables &rDataVariables,
                                                      const Variable<double> &rCriticalVariable,
                                                      const double &rCriticalValue,
                                                      const bool specific = false,
                                                      const double alpha = 0.25)
  {
    KRATOS_TRY

    LabelNodesOnThreshold(rModelPart, rCriticalVariable, rCriticalValue, VISITED, specific);

    SmoothLabeledElementalValues(rModelPart, rDataVariables, VISITED, alpha);

    SetFlagsUtilities::SetFlagsToNodes(rModelPart, {{VISITED}}, {VISITED.AsFalse()});

    KRATOS_CATCH("")
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SmoothElementalValues(ModelPart &rModelPart,
                                           const DataVariables &rDataVariables,
                                           const double alpha = 0.25)
  {
    KRATOS_TRY

    SetFlagsUtilities::SetFlagsToNodes(rModelPart, {{VISITED.AsFalse()}}, {VISITED});

    SmoothLabeledElementalValues(rModelPart, rDataVariables, VISITED, alpha);

    SetFlagsUtilities::SetFlagsToNodes(rModelPart, {{VISITED}}, {VISITED.AsFalse()});

    KRATOS_CATCH("")
  }



  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SmoothLabeledElementalValues(ModelPart &rModelPart,
                                                  const DataVariables &rDataVariables,
                                                  const Flags Label,
                                                  const double alpha = 0.25)
  {
    KRATOS_TRY

    const ProcessInfo &rCurrentProcessInfo = rModelPart.GetProcessInfo();
    const GeometryType &rModelGeometry = rModelPart.Elements().front().GetGeometry();
    const unsigned int integration_points_number = rModelGeometry.IntegrationPointsNumber(rModelGeometry.GetDefaultIntegrationMethod());

    unsigned int number_of_nodes = ModelPartUtilities::GetMaxNodeId(rModelPart) + 1;
    std::vector<DataValuesArrays> NodalDataList(number_of_nodes);

    block_for_each(rModelPart.Nodes(),[&](NodeType &i_node){
    //for (auto &i_node : rModelPart.Nodes()){
      if (i_node.IsNot(RIGID))
      {
        DataValuesArrays Nodal, Elemental;
        Nodal.Initialize(rDataVariables);
        Elemental.Initialize(integration_points_number);

        ElementWeakPtrVectorType &nElements = i_node.GetValue(NEIGHBOUR_ELEMENTS);
        GetElementValues(i_node, nElements, Nodal, Elemental, rDataVariables, rCurrentProcessInfo);
        NodalDataList[i_node.Id()] = Nodal;
      }
    });

    RecoverElementalValues(rModelPart, NodalDataList, rDataVariables, Label, alpha);

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{
  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{


  //*******************************************************************************************
  //*******************************************************************************************

  static inline void FillVectorData(NodeType &rNode,
                                    const VariablesList &rVariablesList)
  {
    unsigned int buffer_size = rNode.GetBufferSize();

    for (const auto &i_variable : rVariablesList)
    {
      std::string variable_name = i_variable.Name();
      if (KratosComponents<Variable<Vector>>::Has(variable_name))
      {
        //std::cout<<"Vector"<<std::endl;
        const Variable<Vector>& variable = KratosComponents<Variable<Vector>>::Get(variable_name);
        for (unsigned int step = 0; step < buffer_size; ++step)
        {
          //getting the data of the solution step
          Vector &node_data = rNode.FastGetSolutionStepValue(variable, step);

          if (node_data.size() == 0)
          {
            node_data = ZeroVector(1);
          }
        }
      }
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void InitializeDataValues(Element &rElement,
                                          DataValuesArrays &rNodalDataValues,
                                          DataValuesArrays &rElementalDataValues,
                                          const DataVariables &rDataVariables,
                                          const ProcessInfo &rCurrentProcessInfo)
  {
    //double
    std::fill(rNodalDataValues.DoubleValueArray.begin(), rNodalDataValues.DoubleValueArray.end(), 0.0);

    //Array1D
    for (unsigned int i = 0; i < rDataVariables.Array1DVariables.size(); ++i)
      rNodalDataValues.Array1DValueArray[i].clear();

    //Vector
    for (unsigned int i = 0; i < rDataVariables.VectorVariables.size(); ++i)
    {
      rElementalDataValues.VectorValueArray.resize(0);
      rElement.CalculateOnIntegrationPoints(*(rDataVariables.VectorVariables[i]), rElementalDataValues.VectorValueArray, rCurrentProcessInfo);
      if (rElementalDataValues.VectorValueArray[0].size() != 0){
        rNodalDataValues.VectorValueArray[i] = ZeroVector(rElementalDataValues.VectorValueArray[0].size());
      }
      else{
        rNodalDataValues.VectorValueArray[i] = ZeroVector(0); //¿value?
      }
    }

    //Matrix
    for (unsigned int i = 0; i < rDataVariables.MatrixVariables.size(); ++i)
    {
      rElementalDataValues.MatrixValueArray.resize(0);
      rElement.CalculateOnIntegrationPoints(*(rDataVariables.MatrixVariables[i]), rElementalDataValues.MatrixValueArray, rCurrentProcessInfo);
      if (rElementalDataValues.MatrixValueArray[0].size1() != 0 && rElementalDataValues.MatrixValueArray[0].size2() != 0){
        rNodalDataValues.MatrixValueArray[i] = ZeroMatrix(rElementalDataValues.MatrixValueArray[0].size1(), rElementalDataValues.MatrixValueArray[0].size2());
      }
      else{
        rNodalDataValues.MatrixValueArray[i] = ZeroMatrix(0, 0); //¿value?
      }
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  template<unsigned int TDim>
  static inline void CalculatePatchCoordinates(ElementWeakPtrVectorType &nElements,
                                               std::vector<std::vector<VectorType>> &rCoordinatesContainer,
                                               VectorType &rPatchSizes)
  {
    KRATOS_TRY

    Point global_point;
    unsigned int i = 0;
    for (auto &i_nelem : nElements)
    {
      // get integration points
      GeometryType &rGeometry = i_nelem.GetGeometry();
      const GeometryType::IntegrationPointsArrayType &integration_points = rGeometry.IntegrationPoints(i_nelem.GetIntegrationMethod());
      const unsigned int & number_of_points = integration_points.size();
      std::vector<VectorType> coordinates_vector(number_of_points);

      for (unsigned int point = 0; point < number_of_points; ++point)
      {
        //rGeometry.GlobalCoordinates(global_point, integration_points[point]);
        //Calculates coordinates from current coordinates
        //GeometryUtilities::GlobalCoordinates(global_point, rGeometry, integration_points[point]);
        //Calculates coordinates from initial position and displacement
        GeometryUtilities::GlobalPosition(global_point, rGeometry, integration_points[point]);
        coordinates_vector[point] = global_point.Coordinates();
      }

      rCoordinatesContainer[i++] = coordinates_vector;
    }

    //normalize
    VectorType Maximum;
    VectorType Minimum;

    std::fill(Maximum.begin(),Maximum.end(),std::numeric_limits<double>::min());
    std::fill(Minimum.begin(),Minimum.end(),std::numeric_limits<double>::max());

    for (auto& i_point : rCoordinatesContainer)
    {
      for (unsigned int point = 0; point < i_point.size(); ++point)
      {
        for (unsigned int i=0; i<TDim; ++i)
        {
          if(Maximum[i]<i_point[point][i])
            Maximum[i]=i_point[point][i];
          if(Minimum[i]>i_point[point][i])
            Minimum[i]=i_point[point][i];
        }
      }
    }

    rPatchSizes = Maximum-Minimum;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  template<unsigned int TDim>
  static inline bool CalculatePatch(const NodeType &i_node,
                                    ElementWeakPtrVectorType &nElements,
                                    const Variable<double> &rVariable,
                                    double &rOutput,
                                    const ProcessInfo &rCurrentProcessInfo)
  {
    KRATOS_TRY

    // This is GP patch: triangle and tetrahedra have only one GP by default
    const GeometryType::IntegrationPointsArrayType &default_integration_points = nElements.front().GetGeometry().IntegrationPoints(nElements.front().GetIntegrationMethod());
    const unsigned int & number_of_points = default_integration_points.size();

    // Our interest is to assemble the system A and b to solve a local problem for the element and estimate the new element size
    BoundedMatrix<double, TDim + 1, TDim + 1> A;
    noalias(A) = ZeroMatrix(TDim + 1,TDim + 1);
    BoundedVector<double, TDim + 1> b;
    noalias(b) = ZeroVector(TDim + 1);

    BoundedVector<double, TDim + 1> p_k;

    const unsigned int &neighbours_size = nElements.size();
    if (neighbours_size < TDim)
      std::cout<< "SPR neighbours are insufficient :" << neighbours_size << std::endl;

    std::vector<std::vector<VectorType>> CoordinatesContainer(neighbours_size);
    VectorType PatchSizes;

    CalculatePatchCoordinates<TDim>(nElements, CoordinatesContainer, PatchSizes);

    std::vector<double> Values(number_of_points);

    unsigned int i=0;
    for (auto &i_nelem : nElements)
    {
      // get elemental variable
      i_nelem.CalculateOnIntegrationPoints(rVariable, Values, rCurrentProcessInfo);
      for (unsigned int point = 0; point < number_of_points; ++point) {
        p_k[0] = 1.0;
        p_k[1] = (CoordinatesContainer[i][point][0] - i_node.X())/PatchSizes[0];
        p_k[2] = (CoordinatesContainer[i][point][1] - i_node.Y())/PatchSizes[1];
        if(TDim == 3)
          p_k[3] = (CoordinatesContainer[i][point][2] - i_node.Z())/PatchSizes[2];

        // Finally we add the contributions to our local system (A, b)
        noalias(A) += outer_prod(p_k, p_k);
        noalias(b) += p_k * Values[point];
      }
      ++i;
    }

    double det;
    BoundedMatrix<double, TDim + 1, TDim + 1> invA;

    MathUtils<double>::InvertMatrix(A, invA, det, -1);

    //check if the system was very bad conditioned
    if(det < 1e-12)
      return false;

    const BoundedVector<double, TDim + 1> result = prod(invA, b);

    rOutput = result[0];

    return true;

    KRATOS_CATCH("")
  }


  //*******************************************************************************************
  //*******************************************************************************************

  template<class TType, unsigned int TDim, unsigned int TSize>
  static inline bool CalculatePatch(const NodeType &i_node,
                                    ElementWeakPtrVectorType &nElements,
                                    const Variable<TType> &rVariable,
                                    TType &rOutput,
                                    const ProcessInfo &rCurrentProcessInfo)
  {
    KRATOS_TRY

    // This is GP patch: triangle and tetrahedra have only one GP by default
    const GeometryType::IntegrationPointsArrayType &default_integration_points = nElements.front().GetGeometry().IntegrationPoints(nElements.front().GetIntegrationMethod());
    const unsigned int & number_of_points = default_integration_points.size();

    // Our interest is to assemble the system A and b to solve a local problem for the element and estimate the new element size
    BoundedMatrix<double, TDim + 1, TDim + 1> A;
    noalias(A) = ZeroMatrix(TDim + 1,TDim + 1);
    BoundedMatrix<double, TDim + 1, TSize> b;
    noalias(b) = ZeroMatrix(TDim + 1,TSize);

    BoundedMatrix<double, 1, TSize> value;
    BoundedMatrix<double, 1, TDim + 1> p_k;

    const unsigned int &neighbours_size = nElements.size();
    if (neighbours_size < TDim)
      std::cout<< "SPR neighbours are insufficient :" << neighbours_size << std::endl;

    std::vector<std::vector<VectorType>> CoordinatesContainer(neighbours_size);
    VectorType PatchSizes;

    CalculatePatchCoordinates<TDim>(nElements, CoordinatesContainer, PatchSizes);

    std::vector<TType> Values(number_of_points);

    unsigned int i=0;
    bool pk_set = false;
    for (auto &i_nelem : nElements)
    {
      // get elemental variable
      i_nelem.CalculateOnIntegrationPoints(rVariable, Values, rCurrentProcessInfo);
      for (unsigned int point = 0; point < number_of_points; ++point) {
        if (Values[point].size() != 0){
          p_k(0,0) = 1.0;
          p_k(0,1) = (CoordinatesContainer[i][point][0] - i_node.X())/PatchSizes[0];
          p_k(0,2) = (CoordinatesContainer[i][point][1] - i_node.Y())/PatchSizes[1];
          if(TDim == 3)
            p_k(0,3) = (CoordinatesContainer[i][point][2] - i_node.Z())/PatchSizes[2];

          IndexType size = Values[point].size();

          for(IndexType j = 0; j < size; ++j)
            value(0,j) = Values[point][j];

          for(IndexType j = size; j < TSize; ++j)
            value(0,j) = 0;

          // Finally we add the contributions to our local system (A, b)
          noalias(A) += prod(trans(p_k), p_k);
          noalias(b) += prod(trans(p_k), value);
          pk_set = true;
        }
      }
      ++i;
    }

    if(pk_set)
    {
      double det;
      BoundedMatrix<double, TDim + 1, TDim + 1> invA;

      MathUtils<double>::InvertMatrix(A, invA, det, -1);

      //check if the system was very bad conditioned
      if(det < 1e-12)
        return false;

      const BoundedMatrix<double, TDim + 1, TSize> result = prod(invA, b);

      rOutput = row(result, 0);

      return true;
    }
    else
      return false;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  template<unsigned int TDim, unsigned int TSize>
  static inline bool CalculatePatch(const NodeType &i_node,
                                    ElementWeakPtrVectorType &nElements,
                                    const Variable<Matrix> &rVariable,
                                    Matrix &rOutput,
                                    const ProcessInfo &rCurrentProcessInfo)
  {
    KRATOS_TRY

    // This is GP patch: triangle and tetrahedra have only one GP by default
    const GeometryType::IntegrationPointsArrayType &default_integration_points = nElements.front().GetGeometry().IntegrationPoints(nElements.front().GetIntegrationMethod());
    const unsigned int & number_of_points = default_integration_points.size();

    // Our interest is to assemble the system A and b to solve a local problem for the element and estimate the new element size
    BoundedMatrix<double, TDim + 1, TDim + 1>  A;
    noalias(A) = ZeroMatrix(TDim + 1,TDim + 1);
    BoundedVector<BoundedMatrix<double, TDim + 1, TSize>, TSize> b;
    BoundedMatrix<double, TDim + 1, TSize> b_col;

    noalias(b_col) = ZeroMatrix(TDim + 1,TSize);
    std::fill(b.begin(),b.end(),b_col);

    BoundedMatrix<double, 1, TSize> value;
    BoundedMatrix<double, 1, TDim + 1> p_k;

    const unsigned int &neighbours_size = nElements.size();
    if (neighbours_size < TDim)
      std::cout<< "SPR neighbours are insufficient :" << neighbours_size << std::endl;

    std::vector<std::vector<VectorType>> CoordinatesContainer(neighbours_size);
    VectorType PatchSizes;

    CalculatePatchCoordinates<TDim>(nElements, CoordinatesContainer, PatchSizes);

    std::vector<Matrix> Values(number_of_points);

    unsigned int i=0;
    for (auto &i_nelem : nElements)
    {
      // get elemental variable
      i_nelem.CalculateOnIntegrationPoints(rVariable, Values, rCurrentProcessInfo);

      for (unsigned int point = 0; point < number_of_points; ++point) {
        p_k(0,0) = 1.0;
        p_k(0,1) = (CoordinatesContainer[i][point][0] - i_node.X())/PatchSizes[0];
        p_k(0,2) = (CoordinatesContainer[i][point][1] - i_node.Y())/PatchSizes[1];
        if(TDim == 3)
          p_k(0,3) = (CoordinatesContainer[i][point][2] - i_node.Z())/PatchSizes[2];

        // Finally we add the contributions to our local system (A, b)
        noalias(A) += prod(trans(p_k), p_k);

        for(IndexType j = 0; j < TSize; ++j)
        {
          for(IndexType k = 0; k < TSize; ++k)
            value(0,k) = Values[point](j,k);

          noalias(b[j]) += prod(trans(p_k), value);
        }
      }
      ++i;
    }

    double det;
    BoundedMatrix<double, TDim + 1, TDim + 1> invA;

    MathUtils<double>::InvertMatrix(A, invA, det, -1);

    //check if the system was very bad conditioned
    if(det < 1e-12)
      return false;

    rOutput.resize(TSize,TSize);

    for(IndexType j = 0; j < TSize; ++j)
    {
      const BoundedMatrix<double, TDim + 1, TSize> result = prod(invA, b[j]);
      for(IndexType k = 0; k < TSize; ++k)
      {
        rOutput(j,k) = row(result, 0)[k];
      }
    }

    return true;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool GetSPRValues(const NodeType &i_node,
                                  ElementWeakPtrVectorType &nElements,
                                  DataValuesArrays &rNodal,
                                  const DataVariables &rDataVariables,
                                  const ProcessInfo &rCurrentProcessInfo)
  {
    KRATOS_TRY

    const unsigned int dimension = rCurrentProcessInfo[SPACE_DIMENSION];
    const bool is_axisymmetric = rCurrentProcessInfo[IS_AXISYMMETRIC];

    bool success = false;

    //double
    for (unsigned int i = 0; i < rDataVariables.DoubleVariables.size(); ++i)
    {
      if (dimension == 2)
        success = CalculatePatch<2>(i_node, nElements, *(rDataVariables.DoubleVariables[i]), rNodal.DoubleValueArray[i], rCurrentProcessInfo); //SPR value
      else
        success = CalculatePatch<3>(i_node, nElements, *(rDataVariables.DoubleVariables[i]), rNodal.DoubleValueArray[i], rCurrentProcessInfo); //SPR value
    }
    //Array1D
    for (unsigned int i = 0; i < rDataVariables.Array1DVariables.size(); ++i)
    {
      if (dimension == 2)
        success = CalculatePatch<VectorType,2,3>(i_node, nElements, *(rDataVariables.Array1DVariables[i]), rNodal.Array1DValueArray[i], rCurrentProcessInfo); //SPR value
      else
        success = CalculatePatch<VectorType,3,6>(i_node, nElements, *(rDataVariables.Array1DVariables[i]), rNodal.Array1DValueArray[i], rCurrentProcessInfo); //SPR value
    }
    //Vector
    for (unsigned int i = 0; i < rDataVariables.VectorVariables.size(); ++i)
    {
      // get elemental variable size
      if (dimension == 2){
        if (is_axisymmetric)
          success = CalculatePatch<Vector,2,4>(i_node, nElements, *(rDataVariables.VectorVariables[i]), rNodal.VectorValueArray[i], rCurrentProcessInfo); //SPR value
        else
          success = CalculatePatch<Vector,2,6>(i_node, nElements, *(rDataVariables.VectorVariables[i]), rNodal.VectorValueArray[i], rCurrentProcessInfo); //SPR value
      }
      else
        success = CalculatePatch<Vector,3,6>(i_node, nElements, *(rDataVariables.VectorVariables[i]), rNodal.VectorValueArray[i], rCurrentProcessInfo); //SPR value
    }
    //Matrix (must be implemented)
    for (unsigned int i = 0; i < rDataVariables.MatrixVariables.size(); ++i)
    {
      // get elemental variable size
      if (dimension == 2){
        if (is_axisymmetric)
          success = CalculatePatch<3,3>(i_node, nElements, *(rDataVariables.MatrixVariables[i]), rNodal.MatrixValueArray[i], rCurrentProcessInfo); //SPR value
        else
          success = CalculatePatch<2,2>(i_node, nElements, *(rDataVariables.MatrixVariables[i]), rNodal.MatrixValueArray[i], rCurrentProcessInfo); //SPR value
      }
      else
        success = CalculatePatch<3,3>(i_node, nElements, *(rDataVariables.MatrixVariables[i]), rNodal.MatrixValueArray[i], rCurrentProcessInfo); //SPR value
    }

    return success;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void GetElementValues(const NodeType &i_node,
                                      ElementWeakPtrVectorType &nElements,
                                      DataValuesArrays &rNodal,
                                      DataValuesArrays &rElemental,
                                      const DataVariables &rDataVariables,
                                      const ProcessInfo &rCurrentProcessInfo)
  {
    KRATOS_TRY

    const GeometryType &rModelGeometry = nElements.front().GetGeometry();
    const unsigned int integration_points_number = rModelGeometry.IntegrationPointsNumber(rModelGeometry.GetDefaultIntegrationMethod());
    const double points_number = double(integration_points_number);

    //Initialize variables
    InitializeDataValues(nElements.front(), rNodal, rElemental, rDataVariables, rCurrentProcessInfo);

    double Area = 0;
    double ElementArea = 0;

    for (auto &i_nelem : nElements)
    {
      const GeometryType &rGeometry = i_nelem.GetGeometry();
      ElementArea = rGeometry.Area();
      Area += ElementArea;

      //double
      for (unsigned int i = 0; i < rDataVariables.DoubleVariables.size(); ++i)
      {
        //elemental value
        i_nelem.CalculateOnIntegrationPoints(*(rDataVariables.DoubleVariables[i]), rElemental.DoubleValueArray, rCurrentProcessInfo);
        for (unsigned int j = 0; j < integration_points_number; ++j)
          rNodal.DoubleValueArray[i] += rElemental.DoubleValueArray[j] * ElementArea / points_number;
      }

      //Array1D
      for (unsigned int i = 0; i < rDataVariables.Array1DVariables.size(); ++i)
      {
        //elemental value
        i_nelem.CalculateOnIntegrationPoints(*(rDataVariables.Array1DVariables[i]), rElemental.Array1DValueArray, rCurrentProcessInfo);
        for (unsigned int j = 0; j < integration_points_number; ++j)
          rNodal.Array1DValueArray[i] += rElemental.Array1DValueArray[j] * ElementArea / points_number;
      }

      //Vector
      for (unsigned int i = 0; i < rDataVariables.VectorVariables.size(); ++i)
      {
        //elemental value
        i_nelem.CalculateOnIntegrationPoints(*(rDataVariables.VectorVariables[i]), rElemental.VectorValueArray, rCurrentProcessInfo);
        for (unsigned int j = 0; j < integration_points_number; ++j)
          rNodal.VectorValueArray[i] += rElemental.VectorValueArray[j] * ElementArea / points_number;
      }

      //Matrix
      for (unsigned int i = 0; i < rDataVariables.MatrixVariables.size(); ++i)
      {
        //elemental value
        i_nelem.CalculateOnIntegrationPoints(*(rDataVariables.MatrixVariables[i]), rElemental.MatrixValueArray, rCurrentProcessInfo);
        for (unsigned int j = 0; j < integration_points_number; ++j)
          rNodal.MatrixValueArray[i] += rElemental.MatrixValueArray[j] * ElementArea / points_number;
      }
    }

    if (Area != 0)
    {
      //double
      for (unsigned int i = 0; i < rDataVariables.DoubleVariables.size(); ++i)
      {
        rNodal.DoubleValueArray[i] /= Area;
      }
      //Array1D
      for (unsigned int i = 0; i < rDataVariables.Array1DVariables.size(); ++i)
      {
        rNodal.Array1DValueArray[i] /= Area;
      }
      //Vector
      for (unsigned int i = 0; i < rDataVariables.VectorVariables.size(); ++i)
      {
        rNodal.VectorValueArray[i] /= Area;
      }
      //Matrix
      for (unsigned int i = 0; i < rDataVariables.MatrixVariables.size(); ++i)
      {
        rNodal.MatrixValueArray[i] /= Area;
      }
    }
    else
      KRATOS_WARNING("") << " TRANSFER: Something Wrong in node [" << i_node.Id() << "] : Area = 0 (neighbours: " << nElements.size() << ") " << std::endl;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void RecoverElementalValues(ModelPart &rModelPart,
                                            std::vector<DataValuesArrays> &rNodalDataList,
                                            const DataVariables &rDataVariables,
                                            const Flags Label,
                                            const double alpha = 1)
  {
    KRATOS_TRY

    const ProcessInfo &rCurrentProcessInfo = rModelPart.GetProcessInfo();
    const GeometryType &rModelGeometry = rModelPart.Elements().front().GetGeometry();
    const unsigned int integration_points_number = rModelGeometry.IntegrationPointsNumber(rModelGeometry.GetDefaultIntegrationMethod());

    //double alpha = 0.25; //[0,1] //smoothing level of the Jacobian

    block_for_each(rModelPart.Elements(),[&](Element &i_elem){
   //for (auto &i_elem : rModelPart.Elements()){

      const GeometryType &rGeometry = i_elem.GetGeometry();

      if (i_elem.IsNot(RIGID) && SetFlagsUtilities::CheckNodeFlag(rGeometry,{Label}))
      {
        const Matrix &Ncontainer = rGeometry.ShapeFunctionsValues(rGeometry.GetDefaultIntegrationMethod());
        const unsigned int &size = rGeometry.size();

        DataValuesArrays Nodal, Elemental;
        Nodal.Initialize(integration_points_number);
        Elemental.Initialize(integration_points_number);

        //shape functions
        Vector N;

        //double
        for (unsigned int i = 0; i < rDataVariables.DoubleVariables.size(); ++i)
        {
          //elemental value
          i_elem.CalculateOnIntegrationPoints(*(rDataVariables.DoubleVariables[i]), Elemental.DoubleValueArray, rCurrentProcessInfo);

          std::fill(Nodal.DoubleValueArray.begin(), Nodal.DoubleValueArray.end(), 0.0);

          for (unsigned int j = 0; j < integration_points_number; ++j)
          {
            N = row(Ncontainer, j);

            //nodal value
            for (unsigned int k = 0; k < size; ++k)
              Nodal.DoubleValueArray[j] += N[k] * rNodalDataList[rGeometry[k].Id()].DoubleValueArray[i];

            Nodal.DoubleValueArray[j] *= (alpha);
            Nodal.DoubleValueArray[j] += (1 - alpha) * Elemental.DoubleValueArray[j];
            //if(Nodal.DoubleValueArray[j]<0)
            //std::cout<<"ID "<<i_elem.Id()<<" "<<Nodal.DoubleValueArray[j]<<" "<<Elemental.DoubleValueArray[j]<<std::endl;
          }

          i_elem.SetValuesOnIntegrationPoints(*(rDataVariables.DoubleVariables[i]), Nodal.DoubleValueArray, rCurrentProcessInfo);
        }

        //array_1d
        for (unsigned int i = 0; i < rDataVariables.Array1DVariables.size(); ++i)
        {
          //elemental value
          i_elem.CalculateOnIntegrationPoints(*(rDataVariables.Array1DVariables[i]), Elemental.Array1DValueArray, rCurrentProcessInfo);

          for (unsigned int j = 0; j < integration_points_number; ++j)
            Nodal.Array1DValueArray[j].clear();

          for (unsigned int j = 0; j < integration_points_number; ++j)
          {
            N = row(Ncontainer, j);

            //nodal value
            for (unsigned int k = 0; k < size; ++k)
              Nodal.Array1DValueArray[j] += N[k] * rNodalDataList[rGeometry[k].Id()].Array1DValueArray[i];

            Nodal.Array1DValueArray[j] *= (alpha);
            Nodal.Array1DValueArray[j] += (1 - alpha) * Elemental.Array1DValueArray[j];
          }

          i_elem.SetValuesOnIntegrationPoints(*(rDataVariables.Array1DVariables[i]), Nodal.Array1DValueArray, rCurrentProcessInfo);
        }

        //Vector
        for (unsigned int i = 0; i < rDataVariables.VectorVariables.size(); ++i)
        {
          //elemental value
          i_elem.CalculateOnIntegrationPoints(*(rDataVariables.VectorVariables[i]), Elemental.VectorValueArray, rCurrentProcessInfo);

          for (unsigned int j = 0; j < integration_points_number; ++j)
          {
            if (Elemental.VectorValueArray[j].size() != 0)
              Nodal.VectorValueArray[j] = ZeroVector(Elemental.VectorValueArray[j].size());
            else
              Nodal.VectorValueArray[j] = ZeroVector(0); //¿value?
          }

          for (unsigned int j = 0; j < integration_points_number; ++j)
          {
            N = row(Ncontainer, j);

            //nodal value
            for (unsigned int k = 0; k < size; ++k)
              Nodal.VectorValueArray[j] += N[k] * rNodalDataList[rGeometry[k].Id()].VectorValueArray[i];

            Nodal.VectorValueArray[j] *= (alpha);
            Nodal.VectorValueArray[j] += (1 - alpha) * Elemental.VectorValueArray[j];
          }

          i_elem.SetValuesOnIntegrationPoints(*(rDataVariables.VectorVariables[i]), Nodal.VectorValueArray, rCurrentProcessInfo);
        }

        //Matrix
        for (unsigned int i = 0; i < rDataVariables.MatrixVariables.size(); ++i)
        {
          //elemental value
          i_elem.CalculateOnIntegrationPoints(*(rDataVariables.MatrixVariables[i]), Elemental.MatrixValueArray, rCurrentProcessInfo);

          for (unsigned int j = 0; j < integration_points_number; ++j)
          {
            if (Elemental.MatrixValueArray[j].size1() != 0 && Elemental.MatrixValueArray[j].size2() != 0)
              Nodal.MatrixValueArray[j] = ZeroMatrix(Elemental.MatrixValueArray[j].size1(), Elemental.MatrixValueArray[j].size2());
            else
              Nodal.MatrixValueArray[j] = ZeroMatrix(0, 0); //¿value?
          }

          for (unsigned int j = 0; j < integration_points_number; ++j)
          {
            N = row(Ncontainer, j);

            //nodal value
            for (unsigned int k = 0; k < size; ++k)
              Nodal.MatrixValueArray[j] += N[k] * rNodalDataList[rGeometry[k].Id()].MatrixValueArray[i];

            Nodal.MatrixValueArray[j] *= (alpha);
            Nodal.MatrixValueArray[j] += (1 - alpha) * Elemental.MatrixValueArray[j];
            //std::cout<<"ID "<<i_elem.Id()<<" "<<Nodal.MatrixValueArray[j]<<" "<<Elemental.MatrixValueArray[j]<<std::endl;
          }

          i_elem.SetValuesOnIntegrationPoints(*(rDataVariables.MatrixVariables[i]), Nodal.MatrixValueArray, rCurrentProcessInfo);
        }
      }
    });

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{

  /// Assignment operator.
  TransferUtilities &operator=(TransferUtilities const &rOther);

  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Unaccessible methods
  ///@{
  ///@}

}; // Class TransferUtilities

///@}

///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
///@}

} // namespace Kratos.

#endif //  KRATOS_TRANSFER_UTILITITES_HPP_INCLUDED defined
