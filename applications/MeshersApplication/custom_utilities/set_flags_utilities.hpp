//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        March 2021 $
//
//

#if !defined(KRATOS_SET_FLAGS_UTILITIES_HPP_INCLUDED)
#define KRATOS_SET_FLAGS_UTILITIES_HPP_INCLUDED

// External includes

// System includes

// Project includes
#include "includes/model_part.h"
#include "spatial_containers/spatial_containers.h"
#include "meshers_application_variables.h"

namespace Kratos
{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
 */
class KRATOS_API(MESHERS_APPLICATION) SetFlagsUtilities
{
public:
  ///@name Type Definitions
  ///@{
  typedef Node<3> NodeType;
  typedef array_1d<double,3> VectorType;
  typedef Geometry<NodeType> GeometryType;

  typedef ModelPart::ElementType ElementType;
  typedef ModelPart::ConditionType ConditionType;

  typedef ModelPart::ElementsContainerType ElementsContainerType;
  typedef ModelPart::NodesContainerType NodesContainerType;
  typedef ModelPart::MeshType::GeometryType::PointsArrayType PointsArrayType;

  typedef GlobalPointersVector<Node<3>> NodeWeakPtrVectorType;
  typedef GlobalPointersVector<Element> ElementWeakPtrVectorType;
  typedef GlobalPointersVector<Condition> ConditionWeakPtrVectorType;

  typedef Bucket<3, Node<3>, std::vector<Node<3>::Pointer>, Node<3>::Pointer, std::vector<Node<3>::Pointer>::iterator, std::vector<double>::iterator> BucketType;
  typedef Tree<KDTreePartition<BucketType>> KdtreeType; //Kdtree

  /// Pointer definition of SetFlagsUtilities
  KRATOS_CLASS_POINTER_DEFINITION(SetFlagsUtilities);

   ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  SetFlagsUtilities() {} //

  /// Destructor.
  virtual ~SetFlagsUtilities() {}

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  //*******************************************************************************************
  //*******************************************************************************************

  //set flags array to nodes
  static inline void SetFlags(NodeType &rNode, const std::vector<Flags> FlagsArray, bool value)
  {
    KRATOS_TRY

    for (const auto &i_flags : FlagsArray)
      rNode.Set(i_flags,value);

    KRATOS_CATCH("")
  }


  //*******************************************************************************************
  //*******************************************************************************************

  //set flag to domain nodes (the only with new_entities present)
  static inline void SetFlagsToDomainModelParts(ModelPart &rModelPart)
  {
    KRATOS_TRY

    for (auto &i_mp : rModelPart.SubModelParts())
    {
      if (i_mp.Is(SOLID)){
        SetFlagsToNodes(i_mp,{{}},{SOLID.AsFalse()});
        SetFlagsToElementNodes(i_mp,{{}},{SOLID});
      }
      else if (i_mp.Is(FLUID)){
        // do not reset FLUID flag in ISOLATED nodes
        SetFlagsToNodes(i_mp,{{ISOLATED.AsFalse()}},{FLUID.AsFalse()});
        SetFlagsToElementNodes(i_mp,{{}},{FLUID});
      }
    }

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SetFlagsToBoundaryFaces(ModelPart &rModelPart, const std::vector<Flags> FlagsArray = {BOUNDARY})
  {
    KRATOS_TRY

    std::vector<Flags> FlagsToReset;
    for (const auto &i_flags : FlagsArray)
      FlagsToReset.push_back(i_flags.AsFalse());

    SetFlagsToElementsAndItsNodes(rModelPart,FlagsToReset,FlagsToReset);

    //elements with faces in the boundary
    block_for_each(rModelPart.Elements(),[&](Element& i_elem){
      GeometryType &rGeometry = i_elem.GetGeometry();
      //elements with faces in the working space dimension
      if (rGeometry.WorkingSpaceDimension() == rGeometry.LocalSpaceDimension())
      {
        DenseMatrix<unsigned int> lpofa; //points that define the faces
        rGeometry.NodesInFaces(lpofa);
        ElementWeakPtrVectorType &nElements = i_elem.GetValue(NEIGHBOUR_ELEMENTS);
        unsigned int iface = 0;
        for (auto &i_nelem : nElements)
        {
          if (i_nelem.Id() == i_elem.Id()) // If there is no shared element in face nf (the Id coincides)
          {
            //i_elem.Set(FlagsArray);
            for (unsigned int i = 1; i < rGeometry.FacesNumber(); ++i)
              rGeometry[lpofa(i, iface)].Set(FlagsArray); //set boundary particles
          }
          iface++;
        }
      }
      else
      {
        //set nodes to BOUNDARY for elements outside of the working space dimension
        for (auto &i_node : rGeometry)
          i_node.Set(FlagsArray);
      }
    });

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  //set fixed nodes as NodeType::INTERFACE  to not be removed in the meshing
  static inline void SetFlagsToFixedDofs(ModelPart &rModelPart, const std::vector<Flags> FlagsArray = {INTERFACE})
  {
    KRATOS_TRY

    block_for_each(rModelPart.Nodes(),[&](NodeType &i_node){
      for (const auto &i_dof : i_node.GetDofs())
      {
        if (i_dof->IsFixed())
        {
          i_node.Set(FlagsArray);
          break;
        }
      }
    });

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  //set fixed nodes as NodeType::INTERFACE  to not be removed in the meshing
  static inline bool CheckAndResetNodeFlag(ModelPart &rModelPart, const Flags Flag)
  {
    KRATOS_TRY

    bool flag_reset = false;
    block_for_each(rModelPart.Nodes(),[&](NodeType &i_node){
      if(i_node.Is(Flag)){
        i_node.Set(Flag,false);
        flag_reset = true;
      }
    });

    return flag_reset;

    KRATOS_CATCH("")
  }
  //*******************************************************************************************
  //*******************************************************************************************

  static inline unsigned int CountNodesFlag(const GeometryType &rGeometry, const std::vector<Flags> ControlArray)
  {
    KRATOS_TRY

    unsigned int counter = 0;
    for (auto &i_node : rGeometry)
      if(i_node.Is(ControlArray))
        ++counter;

    return counter;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SetFlagsToNodes(const GeometryType &rGeometry, const std::vector<Flags> AssignArray)
  {
    KRATOS_TRY

    for (auto &i_node : rGeometry)
      i_node.Set(AssignArray);

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SetFlagsToNodes(const GeometryType &rGeometry, const std::vector<std::vector<Flags>> ControlArray, const std::vector<Flags> AssignArray)
  {
    KRATOS_TRY

    for (auto &i_node : rGeometry)
      for (const auto &i_flags : ControlArray)
        if (i_node.Is(i_flags))
          i_node.Set(AssignArray);

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline std::vector<unsigned int> CountNodesFlags(const GeometryType &rGeometry, const std::vector<std::vector<Flags>> ControlArray)
  {
    KRATOS_TRY

    unsigned int size = ControlArray.size();
    std::vector<unsigned int> Counter(size,0);
    for (auto &i_node : rGeometry)
      for (unsigned int i=0; i<size; ++i)
	if(i_node.Is(ControlArray[i]))
	  Counter[i]+=1;

    return Counter;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline unsigned int CountNeighbourNodesFlag(NodeType &rNode, unsigned int& rNumberOfNodes, const std::vector<Flags> ControlArray)
  {
    KRATOS_TRY

    NodeWeakPtrVectorType &nNodes = rNode.GetValue(NEIGHBOUR_NODES);
    rNumberOfNodes = nNodes.size();
    unsigned int counter = 0;
    for (auto &i_node : nNodes)
      if(i_node.Is(ControlArray))
        ++counter;

    return counter;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CountGeometryFlags(const GeometryType &rGeometry, std::map<std::string, unsigned int>& rFlagsMap)
  {
    KRATOS_TRY

    for (auto &i_node : rGeometry)
      for (auto &[flag, number] : rFlagsMap)
        if (i_node.Is(KratosComponents<Flags>::Get(flag)))
          ++number;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CountNeighbourNodesFlags(NodeType &rNode, std::map<std::string, unsigned int>& rFlagsMap)
  {
    KRATOS_TRY

    NodeWeakPtrVectorType &nNodes = rNode.GetValue(NEIGHBOUR_NODES);
    for (auto &i_node : nNodes)
      for (auto &[flag, number] : rFlagsMap)
        if (i_node.Is(KratosComponents<Flags>::Get(flag)))
          ++number;

    KRATOS_CATCH("")
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckAllNodesFlag(const GeometryType &rGeometry, const std::vector<Flags> ControlArray)
  {
    KRATOS_TRY

    for (auto &i_node : rGeometry)
      if(i_node.IsNot(ControlArray))
        return false;

    return true;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckNodeFlag(const GeometryType &rGeometry, const std::vector<Flags> ControlArray)
  {
    KRATOS_TRY

    for (auto &i_node : rGeometry)
      if(i_node.Is(ControlArray))
        return true;

    return false;

    KRATOS_CATCH("")
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SetFlagsToOtherElementNodes(GeometryType &rGeometry, const std::vector<Flags> ControlArray = {STRUCTURE}, const std::vector<Flags> FlagsArray = {INTERACTION})
  {
    KRATOS_TRY

    bool set_flag = false;
    for (auto &i_node : rGeometry)
      if(i_node.Is(ControlArray)){
        set_flag = true;
        break;
      }
    if(set_flag)
      for (auto &i_node : rGeometry)
        if(i_node.IsNot(ControlArray)) //Other element nodes
          i_node.Set(FlagsArray);

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SetFlagsToOtherElementNodes(ModelPart &rModelPart, const std::vector<Flags> ControlArray = {STRUCTURE}, const std::vector<Flags> FlagsArray = {INTERACTION})
  {
    KRATOS_TRY

    //elements with no face in the boundary && elements in the boundary layer
    block_for_each(rModelPart.Elements(),[&](Element& i_elem){
      GeometryType &rGeometry = i_elem.GetGeometry();
      SetFlagsToOtherElementNodes(rGeometry,ControlArray,FlagsArray);
    });

    KRATOS_CATCH("")
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SetFlagsToElementsIfExistInNodes(ModelPart &rModelPart, const std::vector<Flags> ControlFlags = {FREE_SURFACE}, std::vector<Flags> FlagsArray = {BOUNDARY})
  {
    KRATOS_TRY

    //elements with no face in the boundary && elements in the boundary layer
    block_for_each(rModelPart.Elements(),[&](Element& i_elem){
      GeometryType &rGeometry = i_elem.GetGeometry();
      for (auto &i_node : rGeometry)
        if(i_node.Is(ControlFlags))
          i_elem.Set(FlagsArray);
    });

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SetFlagsToConditionsIfExistInNodes(ModelPart &rModelPart, const std::vector<Flags> ControlFlags = {STRUCTURE, FREE_SURFACE}, std::vector<Flags> FlagsArray = {INTERACTION})
  {
    KRATOS_TRY

    //conditions in the interaction layer
    block_for_each(rModelPart.Conditions(),[&](Condition& i_cond){
      GeometryType &rGeometry = i_cond.GetGeometry();
      for (auto &i_node : rGeometry)
        if(i_node.Is(ControlFlags))
          i_cond.Set(FlagsArray);
    });

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SetFlagsToElementsIfExistInFaceNodes(ModelPart &rModelPart, const std::vector<Flags> ControlFlags = {STRUCTURE}, std::vector<Flags> FlagsArray = {INTERACTION})
  {
    KRATOS_TRY

    //elements with no face in the boundary && elements in the boundary layer
    block_for_each(rModelPart.Elements(),[&](Element& i_elem){
      GeometryType &rGeometry = i_elem.GetGeometry();
      unsigned int face_nodes = rGeometry.size();
      for (auto &i_node : rGeometry)
        if(i_node.Is(ControlFlags))
          --face_nodes;
      if(face_nodes <= 1)
        i_elem.Set(FlagsArray);
    });

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SetFlagsToElementsAndItsNodes(ModelPart &rModelPart, const std::vector<Flags> FlagsToElements, const std::vector<Flags> FlagsToNodes)
  {
    block_for_each(rModelPart.Elements(),[&](Element& i_elem){
      GeometryType &rGeometry = i_elem.GetGeometry();
      i_elem.Set(FlagsToElements);
      for (auto &i_node : rGeometry)
        i_node.Set(FlagsToNodes);
    });
  }



  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SetFlagsToNodes(ModelPart::NodesContainerType& rNodes, const std::vector<std::vector<Flags>> ControlFlags, const std::vector<Flags> AssignFlags)
  {
    block_for_each(rNodes,[&](NodeType& i_node){
      for (const auto &i_flags : ControlFlags)
        if (i_node.Is(i_flags))
          i_node.Set(AssignFlags);
    });
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SetFlagsToElementNodes(ModelPart &rModelPart, const std::vector<std::vector<Flags>> ControlFlags, const std::vector<Flags> AssignFlags)
  {
    block_for_each(rModelPart.Elements(),[&](Element& i_elem){
      GeometryType &rGeometry = i_elem.GetGeometry();
      for (const auto &i_flags : ControlFlags)
        if (i_elem.Is(i_flags))
          for (auto &i_node : rGeometry)
            i_node.Set(AssignFlags);
    });
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SetFlagsToNodes(ModelPart &rModelPart, const std::vector<std::vector<Flags>> ControlFlags, const std::vector<Flags> AssignFlags)
  {
    block_for_each(rModelPart.Nodes(),[&](NodeType& i_node){
      for (const auto &i_flags : ControlFlags)
        if (i_node.Is(i_flags))
          i_node.Set(AssignFlags);
    });
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SetFlagsToElements(ModelPart &rModelPart, const std::vector<std::vector<Flags>> ControlFlags, const std::vector<Flags> AssignFlags)
  {
    block_for_each(rModelPart.Elements(),[&](Element& i_elem){
      for (const auto &i_flags : ControlFlags)
        if (i_elem.Is(i_flags))
          i_elem.Set(AssignFlags);
    });
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SetFlagsToConditions(ModelPart &rModelPart, const std::vector<std::vector<Flags>> ControlFlags, const std::vector<Flags> AssignFlags)
  {
    block_for_each(rModelPart.Conditions(),[&](Condition& i_cond){
      for (const auto &i_flags : ControlFlags)
        if (i_cond.Is(i_flags))
          i_cond.Set(AssignFlags);
    });
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SetFlagsToEntities(ModelPart &rModelPart, const std::string EntityType, const std::vector<std::vector<Flags>> ControlFlags, const std::vector<Flags> AssignFlags)
  {
    if (EntityType == "Nodes")
    {
      block_for_each(rModelPart.Nodes(),[&](NodeType& i_node){
        for (const auto &i_flags : ControlFlags)
          if (i_node.Is(i_flags))
            i_node.Set(AssignFlags);
      });
    }
    else if (EntityType == "Elements")
    {
      block_for_each(rModelPart.Elements(),[&](Element& i_elem){
        for (const auto &i_flags : ControlFlags)
          if (i_elem.Is(i_flags))
            i_elem.Set(AssignFlags);
      });
    }
    else if (EntityType == "Conditions")
    {
      block_for_each(rModelPart.Conditions(),[&](Condition& i_cond){
        for (const auto &i_flags : ControlFlags)
          if (i_cond.Is(i_flags))
            i_cond.Set(AssignFlags);
      });
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline unsigned int CountFlagsInEntities(ModelPart &rModelPart, const std::string EntityType, const std::vector<Flags> FlagsArray = {})
  {
    unsigned int counter = 0;
    if (EntityType == "Nodes")
    {
      for(auto &i_node : rModelPart.Nodes())
        if (i_node.Is(FlagsArray))
          ++counter;
      std::cout<<" Nodes Flags detected ["<<counter<<"] of ["<<rModelPart.Nodes().size()<<"]"<<std::endl;
    }
    else if (EntityType == "Elements")
    {
      for(auto &i_elem : rModelPart.Elements())
        if (i_elem.Is(FlagsArray))
          ++counter;
      std::cout<<" Elements Flags detected ["<<counter<<"] of ["<<rModelPart.Elements().size()<<"]"<<std::endl;
    }
    else if (EntityType == "Conditions")
    {
      for(auto &i_cond : rModelPart.Conditions())
        if (i_cond.Is(FlagsArray))
          ++counter;
      std::cout<<" Conditions Flags detected ["<<counter<<"] of ["<<rModelPart.Conditions().size()<<"]"<<std::endl;
    }
    return counter;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CheckFlagInEntities(ModelPart &rModelPart, const std::string EntityType, const std::vector<Flags> FlagsArray = {})
  {
    if (EntityType == "Nodes")
    {
      block_for_each(rModelPart.Nodes(),[&](NodeType& i_node){
        for (const auto &i_flag : FlagsArray)
          if (i_node.Is(i_flag))
            std::cout<<" node ["<<i_node.Id()<<"] Flag detected "<<std::endl;
      });
    }
    else if (EntityType == "Elements")
    {
      block_for_each(rModelPart.Elements(),[&](Element& i_elem){
        for (const auto &i_flag : FlagsArray)
          if (i_elem.Is(i_flag))
            std::cout<<" element ["<<i_elem.Id()<<"] Flag detected "<<std::endl;
      });
    }
    else if (EntityType == "Conditions")
    {
      block_for_each(rModelPart.Conditions(),[&](Condition& i_cond){
        for (const auto &i_flag : FlagsArray)
          if (i_cond.Is(i_flag))
            std::cout<<" condition ["<<i_cond.Id()<<"] Flag detected "<<std::endl;
      });
    }
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{
  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{
  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{

  /// Assignment operator.
  SetFlagsUtilities &operator=(SetFlagsUtilities const &rOther);

  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Unaccessible methods
  ///@{
  ///@}

}; // Class SetFlagsUtilities

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_SET_FLAGS_UTILITIES_HPP_INCLUDED  defined
