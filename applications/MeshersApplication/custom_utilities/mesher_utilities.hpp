//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

#if !defined(KRATOS_MESHER_UTILITIES_HPP_INCLUDED)
#define KRATOS_MESHER_UTILITIES_HPP_INCLUDED

// External includes

// System includes

// Project includes
#include "geometries/triangle_2d_3.h"
#include "geometries/tetrahedra_3d_4.h"

#include "custom_bounding/spatial_bounding_box.hpp"

#include "meshers_application_variables.h"

namespace Kratos
{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
 */
class KRATOS_API(MESHERS_APPLICATION) MesherUtilities
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of MesherUtilities
  KRATOS_CLASS_POINTER_DEFINITION(MesherUtilities);

  typedef Node<3> NodeType;
  typedef Geometry<NodeType> GeometryType;

  typedef GlobalPointersVector<NodeType> NodeWeakPtrVectorType;
  typedef GlobalPointersVector<Element> ElementWeakPtrVectorType;
  typedef GlobalPointersVector<Condition> ConditionWeakPtrVectorType;

  enum ContactElementType //(contact domain definition)
  {
    NonContact,   //this is not a contact element
    PointToFace,  //2D/3D classical well defined node to face contact
    EdgeToEdge,   //3D edge to edge complex contact element
    PointToPoint, //2D/3D not compatible element belong to multiple bodies
    SelfContact,  //2D/3D self contact
    Undefined     //to be defined later
  };

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  MesherUtilities() {} //

  /// Destructor.
  virtual ~MesherUtilities() {}

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  bool SameSubdomain(GeometryType &rGeometry);

  unsigned int NumberOfSubdomains(GeometryType &rGeometry);

  bool CheckFluidMeniscus(GeometryType &rGeometry, const unsigned int &dimension);

  bool CheckFluidMeniscus2(GeometryType &rGeometry, const unsigned int &dimension);

  ContactElementType CheckContactElement(GeometryType &rGeometry);

  bool CheckContactShape(GeometryType &rGeometry);

  bool CheckGeometryShape(GeometryType &rGeometry, int &rShape);

  //writes a list of particles telling if they are set as boundary or not
  void CheckParticles(ModelPart &rModelPart);

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckConditionInBox(Condition &rCondition, SpatialBoundingBox &rRefiningBox, const ProcessInfo &rCurrentProcessInfo)
  {
    KRATOS_TRY

    for (auto &i_node: rCondition.GetGeometry())
      if (!rRefiningBox.IsInside(i_node, rCurrentProcessInfo[TIME]))
        return false;

    return true;

    KRATOS_CATCH("")
  }

  static inline bool CheckElementInBox(Element &rElement, SpatialBoundingBox &rRefiningBox, const ProcessInfo &rCurrentProcessInfo)
  {
    KRATOS_TRY

    for (auto &i_node: rElement.GetGeometry())
      if (!rRefiningBox.IsInside(i_node, rCurrentProcessInfo[TIME]))
        return false;

    return true;

    KRATOS_CATCH("")
  }

  static inline bool CheckVerticesInBox(GeometryType &rGeometry, SpatialBoundingBox &rRefiningBox, const ProcessInfo &rCurrentProcessInfo)
  {
    KRATOS_TRY

    for (auto &i_node: rGeometry)
      if (!rRefiningBox.IsInside(i_node, rCurrentProcessInfo[TIME]))
        return false;

    return true;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckContactActive(const GeometryType &rConditionGeometry, bool &rSemiActiveContact, std::vector<bool> &rSemiActiveNodes);

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SmoothFluidVariables(NodeType &rNode, const NodeType &nNode, const double &weight, unsigned int step = 0)
  {
    noalias(rNode.FastGetSolutionStepValue(VELOCITY, step)) += weight * nNode.FastGetSolutionStepValue(VELOCITY, step);
    noalias(rNode.FastGetSolutionStepValue(ACCELERATION, step)) += weight * nNode.FastGetSolutionStepValue(ACCELERATION, step);

    rNode.FastGetSolutionStepValue(PRESSURE, step) += weight * nNode.FastGetSolutionStepValue(PRESSURE, step);
  }

  static inline void SmoothFluidMeshVariables(NodeType &rNode, const NodeType &nNode, const double &weight, unsigned int step = 0)
  {
    noalias(rNode.FastGetSolutionStepValue(MESH_VELOCITY, step)) += weight * nNode.FastGetSolutionStepValue(MESH_VELOCITY, step);
    noalias(rNode.FastGetSolutionStepValue(MESH_ACCELERATION, step)) += weight * nNode.FastGetSolutionStepValue(MESH_ACCELERATION, step);
  }

  static inline void MultiplyFluidVariables(NodeType &rNode, const double &weight, unsigned int step = 0)
  {
    rNode.FastGetSolutionStepValue(VELOCITY, step) *= weight;
    rNode.FastGetSolutionStepValue(ACCELERATION, step) *= weight;

    rNode.FastGetSolutionStepValue(PRESSURE, step) *= weight;
  }

  static inline void MultiplyFluidMeshVariables(NodeType &rNode, const double &weight, unsigned int step = 0)
  {
    rNode.FastGetSolutionStepValue(MESH_VELOCITY, step) *= weight;
    rNode.FastGetSolutionStepValue(MESH_ACCELERATION, step) *= weight;
  }

  static inline bool CheckThreshold(Element &rElement, const Variable<double> &rCriticalVariable, const double &rCriticalValue, const ProcessInfo &rCurrentProcessInfo, const bool specific = false)
  {
    std::vector<double> Value;
    rElement.CalculateOnIntegrationPoints(rCriticalVariable, Value, rCurrentProcessInfo);

    double threshold_value = 0;
    for (auto& v : Value)
      threshold_value += v;

    threshold_value /= double(Value.size());

    if (specific)
      threshold_value *= rElement.GetGeometry().DomainSize();

    if (threshold_value > rCriticalValue)
      return true;
    else
      return false;
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  virtual std::string Info() const
  {
    std::stringstream buffer;
    buffer << "MesherUtilities";
    return buffer.str();
  }

  /// Print information about this object.
  virtual void PrintInfo(std::ostream &rOStream) const
  {
    rOStream << "MesherUtilities";
  }

  /// Print object's data.
  virtual void PrintData(std::ostream &rOStream) const
  {
    rOStream << "MesherUtilities Data";
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{
  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{

  /// Assignment operator.
  MesherUtilities &operator=(MesherUtilities const &rOther);

  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Unaccessible methods
  ///@{
  ///@}

}; // Class MesherUtilities

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                MesherUtilities &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const MesherUtilities &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_MESHER_UTILITIES_HPP_INCLUDED  defined
