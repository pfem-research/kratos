//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        March 2021 $
//
//

#if !defined(KRATOS_MODEL_PART_UTILITIES_HPP_INCLUDED)
#define KRATOS_MODEL_PART_UTILITIES_HPP_INCLUDED

// External includes

// System includes

// Project includes
#include "includes/model_part.h"
#include "spatial_containers/spatial_containers.h"
#include "custom_utilities/set_flags_utilities.hpp"
#include "meshers_application_variables.h"

namespace Kratos
{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
 */
class KRATOS_API(MESHERS_APPLICATION) ModelPartUtilities
{
public:
  ///@name Type Definitions
  ///@{
  typedef Node<3> NodeType;
  typedef array_1d<double,3> VectorType;
  typedef Geometry<NodeType> GeometryType;

  typedef ModelPart::ElementType ElementType;
  typedef ModelPart::ConditionType ConditionType;

  typedef ModelPart::ElementsContainerType ElementsContainerType;
  typedef ModelPart::NodesContainerType NodesContainerType;
  typedef ModelPart::MeshType::GeometryType::PointsArrayType PointsArrayType;

  typedef GlobalPointersVector<Node<3>> NodeWeakPtrVectorType;
  typedef GlobalPointersVector<Element> ElementWeakPtrVectorType;
  typedef GlobalPointersVector<Condition> ConditionWeakPtrVectorType;

  typedef Bucket<3, Node<3>, std::vector<Node<3>::Pointer>, Node<3>::Pointer, std::vector<Node<3>::Pointer>::iterator, std::vector<double>::iterator> BucketType;
  typedef Tree<KDTreePartition<BucketType>> KdtreeType; //Kdtree

  /// Pointer definition of ModelPartUtilities
  KRATOS_CLASS_POINTER_DEFINITION(ModelPartUtilities);

   ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  ModelPartUtilities() {} //

  /// Destructor.
  virtual ~ModelPartUtilities() {}

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  //*******************************************************************************************
  //*******************************************************************************************
  static inline void SetGravityInProcessInfo(ModelPart &rModelPart)
  {
    array_1d<double,3> Gravity;
    for (auto &i_node : rModelPart.Nodes())
    {
      if(i_node.SolutionStepsDataHas(VOLUME_ACCELERATION))
      {
        noalias(Gravity) = i_node.FastGetSolutionStepValue(VOLUME_ACCELERATION);
        double norm_gravity = norm_2(Gravity);
        if (norm_gravity != 0){
          Gravity /= norm_gravity;
          rModelPart.GetProcessInfo()[GRAVITY] = Gravity;
          break;
        }
      }

      if (i_node.SolutionStepsDataHas(BODY_FORCE))
      {
        noalias(Gravity) = i_node.FastGetSolutionStepValue(BODY_FORCE);
        double norm_gravity = norm_2(Gravity);
        if (norm_gravity != 0)
        {
          Gravity /= norm_gravity;
          rModelPart.GetProcessInfo()[GRAVITY] = Gravity;
          break;
        }
      }
    }

  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CreateSearchTree(ModelPart &rModelPart, KdtreeType::Pointer& rSearchTree, std::vector<Node<3>::Pointer>& rTreeKnots, const unsigned int bucket_size, const std::vector<Flags> ControlFlags = {})
  {
    //create the list of the nodes to be check during the search
    rTreeKnots.reserve(rModelPart.NumberOfNodes());
    for (auto i_node(rModelPart.NodesBegin()); i_node != rModelPart.NodesEnd(); ++i_node)
    {
      if (i_node->Is(ControlFlags))
        rTreeKnots.push_back(*i_node.base());
    }
    rSearchTree = Kratos::shared_ptr<KdtreeType>(new KdtreeType(rTreeKnots.begin(), rTreeKnots.end(), bucket_size));
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SetModelPartNameToEntities(ModelPart &rModelPart, const std::string EntityType)
  {
    const unsigned int &number_of_submodelparts = rModelPart.NumberOfSubModelParts();

    if (number_of_submodelparts > 0)
    {
        if (EntityType == "Nodes")
        {
            for (auto &i_mp : rModelPart.SubModelParts())
            {
              const int &number_of_nodes = i_mp.NumberOfNodes();
              if (number_of_nodes != 0)
              {
                if (i_mp.Is(BOUNDARY))
                {
                    std::vector<std::string> ModelPartNames;
                    for (auto &i_node : i_mp.Nodes())
                    {
                      if (!i_node.Has(MODEL_PART_NAMES))
                        i_node.SetValue(MODEL_PART_NAMES, ModelPartNames);
                      else
                        i_node.GetValue(MODEL_PART_NAMES).resize(0);
                    }
                }
              }
            }

            for (auto &i_mp : rModelPart.SubModelParts())
            {
                const int &number_of_nodes = i_mp.NumberOfNodes();
                if (number_of_nodes != 0)
                {
                    if (i_mp.Is(BOUNDARY))
                    {
                      const std::string &name = i_mp.Name();
                      block_for_each(i_mp.Nodes(),[&](NodeType& i_node){// not threadsafe if not initialized variable
                        i_node.GetValue(MODEL_PART_NAMES).push_back(name);
                      });
                    }
                    else if (i_mp.IsNot(ACTIVE) && i_mp.IsNot(BOUNDARY) && i_mp.IsNot(CONTACT))
                    {
                      const std::string &name = i_mp.Name();
                      block_for_each(i_mp.Nodes(),[&](NodeType& i_node){// not threadsafe if not initialized variable
                        i_node.SetValue(MODEL_PART_NAME, name);
                      });
                    }
                }
            }
        }
        else if (EntityType == "Elements")
        {
            for (auto &i_mp : rModelPart.SubModelParts())
            {
                if (i_mp.Is(BOUNDARY) || i_mp.IsNot(ACTIVE))
                {
                    const int &number_of_elements = i_mp.NumberOfElements();
                    if (number_of_elements != 0)
                    {
                      const std::string &name = i_mp.Name();
                      block_for_each(i_mp.Elements(),[&](Element& i_elem){// not threadsafe if not initialized variable
                        i_elem.SetValue(MODEL_PART_NAME, name);
                      });
                    }
                }
            }
        }
        else if (EntityType == "Conditions")
        {
            for (auto &i_mp : rModelPart.SubModelParts())
            {
                if (i_mp.Is(BOUNDARY))
                {
                    const int &number_of_elements = i_mp.NumberOfElements();
                    const int &number_of_conditions = i_mp.NumberOfConditions();
                    if (number_of_conditions != 0 && number_of_elements == 0)
                    {
                      const std::string &name = i_mp.Name();
                      block_for_each(i_mp.Conditions(),[&](Condition& i_cond){// not threadsafe if not initialized variable
                        i_cond.SetValue(MODEL_PART_NAME, name);
                      });
                    }
                }
            }
        }
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double CalculateModelPartVolume(ModelPart &rModelPart)
  {
    const unsigned int dimension = rModelPart.GetProcessInfo()[SPACE_DIMENSION];
    double Volume = 0;
    if (dimension == 2)
    {
        for (auto &i_elem : rModelPart.Elements())
        {
        if (i_elem.GetGeometry().WorkingSpaceDimension() == 2)
            Volume += i_elem.GetGeometry().Area();
        }
    }
    else
    { //dimension == 3
        for (auto &i_elem : rModelPart.Elements())
        {
        if (i_elem.GetGeometry().WorkingSpaceDimension() == 3)
            Volume += i_elem.GetGeometry().Volume();
        }
    }

    return Volume;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CheckElements(ModelPart &rModelPart)
  {
    block_for_each(rModelPart.Elements(),[&](Element& i_elem){
      i_elem.Check(rModelPart.GetProcessInfo());
    });
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CheckConditions(ModelPart &rModelPart)
  {
    block_for_each(rModelPart.Conditions(),[&](Condition& i_cond){
      i_cond.Check(rModelPart.GetProcessInfo());
    });
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double CalculateSmallerMeshSize(ModelPart &rModelPart)
  {
    double min_h = std::numeric_limits<double>::max();

    for (auto &i_node : rModelPart.Nodes())
    {
        double &nodal_h = i_node.FastGetSolutionStepValue(NODAL_H);
        if (nodal_h < min_h)
            min_h = nodal_h;
    }
    return min_h;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline unsigned int GetMaxPropertiesId(ModelPart &rModelPart)
  {
    unsigned int max_id = rModelPart.rProperties().back().Id();

    for (auto &i_prop : rModelPart.rProperties())
    {
      if (i_prop.Id() > max_id)
        max_id = i_prop.Id();
    }

    return max_id;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline unsigned int GetMaxNodeId(ModelPart &rModelPart)
  {
    unsigned int max_id = rModelPart.Nodes().back().Id();

    for (auto &i_node : rModelPart.Nodes())
       if (i_node.Id() > max_id)
        max_id = i_node.Id();

    return max_id;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline unsigned int GetMaxConditionId(ModelPart &rModelPart)
  {
    unsigned int max_id = rModelPart.Conditions().back().Id();

    for (auto &i_cond : rModelPart.Conditions())
      if (i_cond.Id() > max_id)
        max_id = i_cond.Id();

    return max_id;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline unsigned int GetMaxElementId(ModelPart &rModelPart)
  {
    unsigned int max_id = rModelPart.Elements().back().Id();

    for (auto &i_elem : rModelPart.Elements())
      if (i_elem.Id() > max_id)
        max_id = i_elem.Id();

    return max_id;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void AddNodes(ModelPart &rModelPart, std::vector<NodeType::Pointer> &list_of_nodes)
  {
    if (list_of_nodes.size())
    {
      //add new conditions: ( SOLID body model part )
      for (std::vector<NodeType::Pointer>::iterator i_node = list_of_nodes.begin(); i_node != list_of_nodes.end(); ++i_node)
      {
        rModelPart.Nodes().push_back(*(i_node));
      }
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void AddConditions(ModelPart &rModelPart, std::vector<ConditionType::Pointer> &list_of_conditions)
  {
    if (list_of_conditions.size())
    {
      EraseConditions(rModelPart);

      //add new conditions: ( SOLID body model part )
      for (std::vector<ConditionType::Pointer>::iterator i_cond = list_of_conditions.begin(); i_cond != list_of_conditions.end(); ++i_cond)
      {
        if ((*i_cond)->Is(TO_ERASE))
          KRATOS_ERROR<<" Adding Condition TO_ERASE "<<std::endl;
        rModelPart.Conditions().push_back(*(i_cond));
      }
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void EraseConditions(ModelPart &rModelPart, const std::vector<Flags> Flags = {TO_ERASE.AsFalse()})
  {
    ModelPart::ConditionsContainerType PreservedConditions;
    PreservedConditions.reserve(rModelPart.Conditions().size());
    PreservedConditions.swap(rModelPart.Conditions());

    for (ModelPart::ConditionsContainerType::iterator i_cond = PreservedConditions.begin(); i_cond != PreservedConditions.end(); ++i_cond)
    {
      if (i_cond->Is(Flags))
        rModelPart.Conditions().push_back(*(i_cond.base()));
    }
    rModelPart.Conditions().Sort();
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline void EraseNodes(ModelPart &rModelPart, const std::vector<Flags> Flags = {TO_ERASE.AsFalse()})
  {
    ModelPart::NodesContainerType PreservedNodes;
    PreservedNodes.reserve(rModelPart.Nodes().size());
    PreservedNodes.swap(rModelPart.Nodes());

    for (ModelPart::NodesContainerType::iterator i_node = PreservedNodes.begin(); i_node != PreservedNodes.end(); ++i_node)
    {
      if (i_node->Is(Flags))
        rModelPart.Nodes().push_back(*(i_node.base()));
    }
    rModelPart.Nodes().Sort();
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CalculateNodalError(ModelPart &rModelPart, std::vector<double> &rError, std::vector<int> &rIds, const Variable<double> &rVariable)
  {
    std::vector<double> ElementalError;
    std::vector<int> elems_ids;

    CalculateElementalError(rModelPart, ElementalError, elems_ids, rVariable);

    if (!rError.size())
      rError.resize(rModelPart.NumberOfNodes() + 1);

    std::fill(rError.begin(), rError.end(), 100);

    if (!rIds.size())
      rIds.resize(ModelPartUtilities::GetMaxNodeId(rModelPart) + 1);

    std::fill(rIds.begin(), rIds.end(), 0);

    double NodalMeanError = 0;

    int id = 1;
    for (auto &i_node : rModelPart.Nodes())
    {
      if (i_node.IsNot(NEW_ENTITY))
      {
        ElementWeakPtrVectorType &nElements = i_node.GetValue(NEIGHBOUR_ELEMENTS);

        NodalMeanError = 0;
        for (auto &i_nelem : nElements)
        {
          NodalMeanError += ElementalError[elems_ids[i_nelem.Id()]];
        }

        rIds[i_node.Id()] = id;
        rError[id] = NodalMeanError / double(nElements.size());
      }
      else
      {
        rIds[i_node.Id()] = id;
        rError[id] = 0;
      }

      id++;
    }

  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CalculateElementalError(ModelPart &rModelPart, std::vector<double> &rError, std::vector<int> &rIds, const Variable<double>& rVariable)
  {
    const ProcessInfo &rCurrentProcessInfo = rModelPart.GetProcessInfo();

    std::vector<double> ElementVariable(ModelPartUtilities::GetMaxElementId(rModelPart) + 1);
    std::fill(ElementVariable.begin(), ElementVariable.end(), 0);

    std::vector<int> elems_ids;
    elems_ids.resize(ModelPartUtilities::GetMaxElementId(rModelPart) + 1); //mesh 0
    std::fill(elems_ids.begin(), elems_ids.end(), 0);

    double VariableMax = std::numeric_limits<double>::min();
    double VariableMin = std::numeric_limits<double>::max();

    std::vector<double> Value(1);

    unsigned int id = 1;
    for (ModelPart::ElementsContainerType::const_iterator ie = rModelPart.ElementsBegin(); ie != rModelPart.ElementsEnd(); ++ie)
    {
      (ie)->CalculateOnIntegrationPoints(rVariable, Value, rCurrentProcessInfo);

      elems_ids[ie->Id()] = id;
      ElementVariable[id] = Value[0];

      if (ElementVariable[id] > VariableMax)
        VariableMax = ElementVariable[id];

      if (ElementVariable[id] < VariableMin)
        VariableMin = ElementVariable[id];

      id++;
    }

    std::vector<double> NodalError(rModelPart.NumberOfNodes() + 1);
    std::fill(NodalError.begin(), NodalError.end(), 0);

    std::vector<int> nodes_ids(ModelPartUtilities::GetMaxNodeId(rModelPart) + 1); //mesh 0
    std::fill(nodes_ids.begin(), nodes_ids.end(), 0);

    double PatchSize = 0;
    double PatchError = 0;

    double Size = 0;
    double Error = 0;

    id = 1;
    for (auto &i_node : rModelPart.Nodes())
    {
      if (i_node.IsNot(NEW_ENTITY))
      {
        PatchSize = 0;
        PatchError = 0;

        ElementWeakPtrVectorType &nElements = i_node.GetValue(NEIGHBOUR_ELEMENTS);

        for (auto &i_nelem : nElements)
        {

          Geometry<Node<3>> &rGeometry = i_nelem.GetGeometry();

          Size = rGeometry.DomainSize(); //Area(); or Volume();
          Error = ElementVariable[elems_ids[i_nelem.Id()]] * Size;

          PatchSize += Size;
          PatchError += Error;
        }

        if (PatchSize != 0)
        {
          nodes_ids[i_node.Id()] = id;
          NodalError[id] = PatchError / PatchSize;
        }
        else
        {
          KRATOS_WARNING("") <<" Size surrounding node: " << i_node.Id() << " is null " << std::endl;
        }
      }
      else
      {
        nodes_ids[i_node.Id()] = id;
        NodalError[id] = 0;
      }

      id++;
    }

    rError.resize(ModelPartUtilities::GetMaxElementId(rModelPart) + 1);
    std::fill(rError.begin(), rError.end(), 100);

    rIds.resize(ModelPartUtilities::GetMaxElementId(rModelPart) + 1); //mesh 0
    std::fill(rIds.begin(), rIds.end(), 0);

    double VariableVariation = VariableMax - VariableMin;

    if (VariableVariation == 0)
    {
      VariableVariation = 1;
      KRATOS_WARNING("") << rVariable << " min-max errors are the same ( MinVar= " << VariableMin << ", MaxVar= " << VariableMax << " )" << std::endl;
    }
    else
    {
      id = 1;
      for (auto &i_elem : rModelPart.Elements())
      {
        PointsArrayType &vertices = i_elem.GetGeometry().Points();

        PatchError = 0;

        unsigned int NumberOfVertices = vertices.size();
        for (unsigned int i = 0; i < NumberOfVertices; ++i)
        {
          PatchError += NodalError[nodes_ids[vertices[i].Id()]];
        }

        if (NumberOfVertices != 0)
        {
          PatchError /= double(NumberOfVertices);
        }
        else
        {
          KRATOS_WARNING("") << " Number of Vertices of the Element: " << i_elem.Id() << " is null " << std::endl;
        }

        rIds[i_elem.Id()] = id;
        rError[id] = fabs((PatchError - ElementVariable[id]) / VariableVariation) * 100;

        id++;
      }
    }

  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void RecoverPositionAndSmoothInitial(ModelPart &rModelPart)
  {
    std::cout<<" Recover position and smooth initial "<<std::endl;
    block_for_each(
      rModelPart.Nodes(),
      [&](NodeType &i_node){

        const array_1d<double, 3> &displacement = i_node.FastGetSolutionStepValue(DISPLACEMENT);
        VectorType CurrentPosition = i_node.GetInitialPosition()+displacement;
        //recover the original position of the node
        if (i_node.IsNot(BOUNDARY))
        {
          //recover the original position of the node
          VectorType &InitialPosition = i_node.GetInitialPosition();
          InitialPosition = i_node.Coordinates() - displacement;
        }
        else //boundary node with possible fixed conditions
        {
          if (i_node.HasDofFor(DISPLACEMENT_X))
            if (i_node.pGetDof(DISPLACEMENT_X)->IsFixed() == false)
              i_node.X0() = i_node.X() - displacement[0];
          if (i_node.HasDofFor(DISPLACEMENT_Y))
            if (i_node.pGetDof(DISPLACEMENT_Y)->IsFixed() == false)
              i_node.Y0() = i_node.Y() - displacement[1];
          if (i_node.HasDofFor(DISPLACEMENT_Z))
            if (i_node.pGetDof(DISPLACEMENT_Z)->IsFixed() == false)
              i_node.Z0() = i_node.Z() - displacement[2];
        }

        i_node.Coordinates() = CurrentPosition;
      }
      );
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void RecoverInitialPosition(ModelPart &rModelPart)
  {
    block_for_each(rModelPart.Nodes(),[&](NodeType &i_node){
      const array_1d<double, 3> &displacement = i_node.FastGetSolutionStepValue(DISPLACEMENT);
      //recover the original position of the node
      if (i_node.IsNot(BOUNDARY))
      {
        //recover the original position of the node
        VectorType &InitialPosition = i_node.GetInitialPosition();
        InitialPosition = i_node.Coordinates() - displacement;
      }
      else //boundary node with possible fixed conditions
      {
        //VectorType InitialPosition = i_node.GetInitialPosition();
        if (i_node.HasDofFor(DISPLACEMENT_X))
          if (i_node.IsFixed(DISPLACEMENT_X) == false)
            i_node.X0() = i_node.X() - displacement[0];
        if (i_node.HasDofFor(DISPLACEMENT_Y))
          if (i_node.IsFixed(DISPLACEMENT_Y) == false)
            i_node.Y0() = i_node.Y() - displacement[1];
        if (i_node.HasDofFor(DISPLACEMENT_Z))
          if (i_node.IsFixed(DISPLACEMENT_Z) == false)
            i_node.Z0() = i_node.Z() - displacement[2];
        // if (norm_2(i_node.GetInitialPosition()-InitialPosition)!=0)
        //   std::cout<<" BOUNDARY NODE MOVEMENT "<<i_node.Id()<<std::endl;
      }
    }
    );
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void RecoverCurrentPosition(ModelPart &rModelPart)
  {
    block_for_each(
      rModelPart.Nodes(),
      [&](NodeType &i_node){
        //recover the original position of the node
        i_node.Coordinates() = i_node.GetInitialPosition() + i_node.FastGetSolutionStepValue(DISPLACEMENT);
      }
      );
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SortConditions(ModelPart &rModelPart)
  {
    unsigned int consecutive_index = 1;
    for (auto &i_cond : rModelPart.Conditions())
      i_cond.SetId(++consecutive_index);

    rModelPart.Conditions().Sort();
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SortElements(ModelPart &rModelPart)
  {
    unsigned int consecutive_index = 1;
    for (auto &i_elem : rModelPart.Elements())
      i_elem.SetId(++consecutive_index);

    rModelPart.Elements().Sort();
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SortNodes(ModelPart &rModelPart)
  {
    unsigned int consecutive_index = 1;
    for (auto &i_node : rModelPart.Nodes())
      i_node.SetId(++consecutive_index);

    rModelPart.Nodes().Sort();
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SetPreviousIdNodes(ModelPart &rModelPart, std::vector<int>& rPreviousIds)
  {
    for (auto &i_node : rModelPart.Nodes())
      i_node.SetId(rPreviousIds[i_node.Id()]);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SetConsecutiveIdConditions(ModelPart &rModelPart)
  {
    unsigned int consecutive_index = 1;
    for (auto &i_cond : rModelPart.Conditions())
      i_cond.SetId(++consecutive_index);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SetUniqueIdContactConditions(ModelPart &rModelPart)
  {
    unsigned int consecutive_index = 0;
    //Renumerate conditions to add in the end of the Elements array (for writing purposes in the ID)
    //mrMainModelPart.Elements().Sort();
    int LastElementId = ModelPartUtilities::GetMaxElementId(rModelPart);
    int LastConditionId = ModelPartUtilities::GetMaxConditionId(rModelPart);
    if (LastElementId > LastConditionId)
    {
      consecutive_index = LastElementId + 1;
    }
    else
    {
      consecutive_index = LastConditionId + 1;
    }

    for (auto &i_cond : rModelPart.Conditions())
    {
      if (i_cond.Is(CONTACT))
      {
        i_cond.SetId(consecutive_index++);
      }
    }

    rModelPart.Conditions().Unique();
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CheckCloseNodes(ModelPart &rModelPart, double distance)
  {
    //create search tree
    KdtreeType::Pointer SearchTree;
    std::vector<Node<3>::Pointer> list_of_nodes;
    ModelPartUtilities::CreateSearchTree(rModelPart, SearchTree, list_of_nodes, 20);

    //all of the nodes in this list will be preserved
    unsigned int num_neighbours = 100;
    std::vector<Node<3>::Pointer> neighbours(num_neighbours);
    std::vector<double> neighbour_distances(num_neighbours);

    //radius means the distance, if the distance between two nodes is closer to radius -> mark for removing
    unsigned int n_points_in_radius;

    for (auto &i_node : rModelPart.Nodes())
    {
      double size_for_distance_inside =distance;

      // the neighbour_distance returned by SearchInRadius is squared (distance to the power of two)
      n_points_in_radius = SearchTree->SearchInRadius(i_node, size_for_distance_inside, neighbours.begin(), neighbour_distances.begin(), num_neighbours);

      if (n_points_in_radius > 1)
      {
        KRATOS_WARNING("")  << n_points_in_radius << " Points too close at mesh ; distance: " << sqrt(neighbour_distances.front()) << std::endl;
      }
    }

  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CheckConditionsSize(ModelPart &rModelPart, double size)
  {
    //block_for_each(rModelPart.Nodes(),[&](NodeType &i_node){
    for (auto &i_cond : rModelPart.Conditions()){
      if (i_cond.GetGeometry().DomainSize() < size)
        KRATOS_WARNING("")  << i_cond.Id() << " Condition domain size too small: " << i_cond.GetGeometry().DomainSize() << std::endl;
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CheckNodalH(ModelPart &rModelPart)
  {
    //block_for_each(rModelPart.Nodes(),[&](NodeType &i_node){
    for (auto &i_node : rModelPart.Nodes()){
      if(!std::isfinite(i_node.FastGetSolutionStepValue(NODAL_H)))
        KRATOS_ERROR << " nodal H i NaN  node [" << i_node.Id() << "]" << std::endl;
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CheckNodalNormal(ModelPart &rModelPart)
  {
    //block_for_each(rModelPart.Nodes(),[&](NodeType &i_node){
    for (auto &i_node : rModelPart.Nodes()){
      if(!std::isfinite(norm_2(i_node.FastGetSolutionStepValue(NORMAL))))
        KRATOS_ERROR << " NORMAL is NaN  node [" << i_node.Id() << "]" << std::endl;
    }
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CheckNodalDisplacement(ModelPart &rModelPart)
  {
    //block_for_each(rModelPart.Nodes(),[&](NodeType &i_node){
    for (auto &i_node : rModelPart.Nodes()){
      const array_1d<double, 3> &CurrentDisplacement = i_node.FastGetSolutionStepValue(DISPLACEMENT);
      const array_1d<double, 3> &PreviousDisplacement = i_node.FastGetSolutionStepValue(DISPLACEMENT, 1);
      const double norm = norm_2(CurrentDisplacement-PreviousDisplacement);
      if(norm>1e-6)
      {
        std::cout<<"           Node[" << i_node.Id() << "] Displacement |u|:" << norm << std::endl;
        std::cout<<"                Current  :" << CurrentDisplacement << std::endl;
        std::cout<<"                Previous :" << PreviousDisplacement << std::endl;
        std::cout<<"           ---------------------------------------------------------------- " << std::endl;
        std::cout<<std::endl;
      }
    }//);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline NodeType::Pointer CreateFreeNode(ModelPart &rModelPart, unsigned int id, const double x, const double y, const double z)
  {
    //assign data to dofs
    NodeType::DofsContainerType &ReferenceDofs = rModelPart.Nodes().front().GetDofs();
    //create a new node
    NodeType::Pointer pNode = Kratos::make_intrusive<NodeType>(id,x,y,z);

    //giving model part variables list to the node
    pNode->SetSolutionStepVariablesList(&(rModelPart.GetNodalSolutionStepVariablesList()));

    //set buffer size
    pNode->SetBufferSize(rModelPart.GetBufferSize());

    //generating the dofs
    for (Node<3>::DofsContainerType::iterator i_dof = ReferenceDofs.begin(); i_dof != ReferenceDofs.end(); ++i_dof)
    {
      NodeType::DofType &rDof = **i_dof;
      NodeType::DofType::Pointer pNewDof = pNode->pAddDof(rDof);
      (pNewDof)->FreeDof();
    }

    return pNode;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SetNodeNeighbourConditions(ModelPart &rModelPart, const std::vector<Flags> Flags = {})
  {
    ClearNodeNeighbourConditions(rModelPart);
    AddNodeNeighbourConditions(rModelPart,Flags);
    //CheckNodeNeighbourConditions(rModelPart);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void AddNodeNeighbourConditions(ModelPart &rModelPart, const std::vector<Flags> Flags = {})
  {
    for (auto i_cond(rModelPart.ConditionsBegin()); i_cond != rModelPart.ConditionsEnd(); ++i_cond)
    {
      if (i_cond->Is(Flags) && i_cond->GetGeometry().size() > 1)
        for (auto &i_node : i_cond->GetGeometry()){
          i_node.GetValue(NEIGHBOUR_CONDITIONS).push_back(*i_cond.base());
        }
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CheckNodeNeighbourConditions(ModelPart &rModelPart)
  {
    for (auto i_cond(rModelPart.ConditionsBegin()); i_cond != rModelPart.ConditionsEnd(); ++i_cond)
    {
      std::cout<<" condition["<<i_cond->Id()<<"] n: "<<i_cond->GetValue(NORMAL)<<std::endl;
    }

    for (auto &i_node : rModelPart.Nodes()){
      ConditionWeakPtrVectorType &nConditions = i_node.GetValue(NEIGHBOUR_CONDITIONS);
      for (auto &i_cond : nConditions) //characterize first layer faces
      {
        std::cout<<" [" <<i_cond.Id()<<"]: "<<std::endl;
        std::cout<<" NORMAL : "<<i_cond.GetValue(NORMAL)<<std::endl;
      }
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void ClearNodeNeighbourConditions(ModelPart &rModelPart)
  {
    // Clear node neighbour conditions
    for (auto &i_node : rModelPart.Nodes())
      i_node.GetValue(NEIGHBOUR_CONDITIONS).clear();

  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void EraseDuplicatedFacets(const ModelPart &rModelPart)
  {
    block_for_each(rModelPart.Conditions(),[&](Condition& i_cond){

      std::vector<int> v1;
      for (auto &i_node : i_cond.GetGeometry())
        v1.push_back(i_node.Id());

      for (auto &j_cond : rModelPart.Conditions())
      {
        if (i_cond.Id() != j_cond.Id() && j_cond.IsNot(TO_ERASE))
        {
          std::vector<int> v2;
          for (auto &j_node : j_cond.GetGeometry())
            v2.push_back(j_node.Id());

          if (std::is_permutation(v1.begin(), v1.end(), v2.begin()))
            i_cond.Set(TO_ERASE);
        }
      }
     });
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CheckConditionFacets(const ModelPart &rModelPart)
  {
    block_for_each(rModelPart.Conditions(),[&](Condition& i_cond){
    // for (auto &i_cond : rModelPart.Conditions())
    // {
      std::vector<int> v1;
      for (auto &i_node : i_cond.GetGeometry())
        v1.push_back(i_node.Id());

      //std::sort(v1.begin(), v1.end());
      for (auto &j_cond : rModelPart.Conditions())
      {
        if (i_cond.Id() != j_cond.Id())
        {
          std::vector<int> v2;
          for (auto &j_node : j_cond.GetGeometry())
            v2.push_back(j_node.Id());

          //std::sort(v2.begin(), v2.end());

          if (std::is_permutation(v1.begin(), v1.end(), v2.begin()))
            KRATOS_ERROR << " CONDITION ["<<i_cond.Id()<<"] ["<<j_cond.Id()<<"] duplicated "<<v1<<" "<<v2<<std::endl;
          // if (v1 == v2)
          //   KRATOS_ERROR << " CONDITION ["<<i_cond.Id()<<"] ["<<j_cond.Id()<<"] equal "<<v1<<" "<<v2<<std::endl;
        }
      }
      //}
     });
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void ReorderAndOptimizeModelPart(ModelPart &rModelPart)
  {

    KRATOS_TRY

    // Set consecutive ids and sort entities (nodes, elements, conditions)
    SortEntities(rModelPart);

    // Reorder Nodes (optimize nodes ordering)
    ReorderNodes(rModelPart);

    // Reorder Elements (optional)
    // ReorderElements(rModelPart);

    // Reorder Conditions (optional)
    // ReorderConditions(rModelPart);

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SortEntities(ModelPart &rModelPart)
  {
    KRATOS_TRY

    rModelPart.Nodes().Unique();      //Unique (sort included)
    rModelPart.Elements().Unique();   //Unique (sort included)
    rModelPart.Conditions().Unique(); //Unique (sort included)

    SortNodesIsolated(rModelPart);

// #pragma omp parallel for
//     for (int i = 0; i < static_cast<int>(rModelPart.Nodes().size()); ++i)
//       (rModelPart.NodesBegin() + i)->SetId(i + 1);

#pragma omp parallel for
    for (int i = 0; i < static_cast<int>(rModelPart.Elements().size()); ++i)
      (rModelPart.ElementsBegin() + i)->SetId(i + 1);
    rModelPart.Elements().Sort();

#pragma omp parallel for
    for (int i = 0; i < static_cast<int>(rModelPart.Conditions().size()); ++i)
      (rModelPart.ConditionsBegin() + i)->SetId(i + 1);
    rModelPart.Conditions().Sort();

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SortNodesIsolated(ModelPart &rModelPart)
  {
    KRATOS_TRY

    //Sort Nodes, set ISOLATED nodes at end
    unsigned int consecutive_index = 1;
    unsigned int reverse_index = rModelPart.NumberOfNodes();
    for (auto &i_node : rModelPart.Nodes())
    {
      if (i_node.IsNot(ISOLATED))
        i_node.SetId(consecutive_index++);
      else
        i_node.SetId(reverse_index--);
    }

    rModelPart.Nodes().Sort();

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void ReorderNodes(ModelPart &rModelPart)
  {
    KRATOS_TRY

    const unsigned int n = rModelPart.Nodes().size();

    //Build graph (neighbour nodes of each node)
    std::vector<std::set<std::size_t>> graph(n);

    for (int i = 0; i < static_cast<int>(rModelPart.Elements().size()); ++i)
    {
      const auto &elem = rModelPart.ElementsBegin() + i;
      const auto &geom = elem->GetGeometry();
      if (!SetFlagsUtilities::CheckNodeFlag(geom,{TO_ERASE})){
        if ( geom.size() > 1) {
          for (unsigned int k = 0; k < geom.size(); ++k) {
            for (unsigned int l = 0; l < geom.size(); ++l)
              graph[geom[k].Id() - 1].insert(geom[l].Id() - 1); //the -1 is because the ids of our nodes start in 1
          }
        } else {
          for ( unsigned int k = 0; k < geom.size(); ++k) {
            if ( geom[k].Id() - 1 >= graph.size() ) {
              std::cout << " WARNING: THERE IS A ELEMENT ASSOCIATED TO A NODE THAT PROBABLY DOES NOT EXIST" << std::endl;
            }
          }
        }
      }
      else
        std::cout<<" detected element node to erase "<<std::endl;
    }

    //avoid conditions loop (may have sense for contact conditions)
    if (false){
      for (int i = 0; i < static_cast<int>(rModelPart.Conditions().size()); ++i)
      {
        const auto &cond = rModelPart.ConditionsBegin() + i;
        const auto &geom = cond->GetGeometry();
        if (!SetFlagsUtilities::CheckNodeFlag(geom,{TO_ERASE})){
          if ( geom.size() > 1) {
            for (unsigned int k = 0; k < geom.size(); ++k) {
              for (unsigned int l = 0; l < geom.size(); ++l) {
                graph[geom[k].Id() - 1].insert(geom[l].Id() - 1); //the -1 is because the ids of our nodes start in 1
              }
            }
          } else {
            for ( unsigned int k = 0; k < geom.size(); ++k) {
              if ( geom[k].Id() - 1 >= graph.size() ) {
                std::cout << " WARNING: THERE IS A CONDITION ASSOCIATED TO A NODE THAT PROBABLY DOES NOT EXIST" << std::endl;
              }
            }
          }
        }
        else
          std::cout<<" detected condition node to erase "<<std::endl;
      }
    }

    //Build graph matrix for CuthillMckee implementation (by Pooyan)
    CompressedMatrix graph_csr(n, n);
    for (unsigned int i = 0; i < n; ++i){
      for (const auto &k : graph[i])
        graph_csr.push_back(i, k, 1);
      //correction isolated nodes, no neighbours
      if (graph[i].size() == 0)
	graph_csr.push_back(i, 0, 0);
    }

    //Apply CuthillMcKee ordering (true: reverse)
    std::vector<int> invperm(graph_csr.size1());
    CuthillMcKee<true>().get<CompressedMatrix>(graph_csr, invperm);

    // #pragma omp parallel for
    // for (int i = 0; i < static_cast<int>(rModelPart.Nodes().size()); ++i)
    //   (rModelPart.NodesBegin() + i)->SetId(invperm[i] + 1);

    IndexPartition<std::size_t>(rModelPart.Nodes().size()).for_each([&](std::size_t Index){
                (rModelPart.NodesBegin() + Index)->SetId(invperm[Index]+1);
    });

    //Sort Nodes
    rModelPart.Nodes().Sort();

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void ReorderElements(ModelPart &rModelPart)
  {
    KRATOS_TRY

    // Expects element ids are ordered 1 ... Elements().size().
    std::vector<std::size_t> element_ids(rModelPart.NumberOfElements());
    std::vector<std::size_t> node_ids(rModelPart.NumberOfElements());
    #pragma omp parallel for
    for (int i = 0; i < static_cast<int>(element_ids.size()); ++i)
    {
      auto it = rModelPart.ElementsBegin() + i;
      element_ids.at(it->Id() - 1) = it->Id();
      std::size_t node_id = it->GetGeometry()[0].Id();
      for (const auto &r_node : it->GetGeometry().Points())
        node_id = std::min(node_id, r_node.Id());
      node_ids.at(it->Id() - 1) = node_id;
    }
    std::stable_sort(element_ids.begin(), element_ids.end(),
                     [&node_ids](const std::size_t &i, const std::size_t &j) {
                       return node_ids[i - 1] < node_ids[j - 1];
                     });

    // #pragma omp parallel for
    // for (int i = 0; i < static_cast<int>(element_ids.size()); ++i)
    //   (rModelPart.ElementsBegin() + element_ids[i] - 1)->SetId(i + 1);

    IndexPartition<std::size_t>(element_ids.size()).for_each([&](std::size_t Index){
                (rModelPart.ElementsBegin() + element_ids[Index] - 1)->SetId(Index + 1);
    });

    rModelPart.Elements().Sort();

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void ReorderConditions(ModelPart &rModelPart)
  {
    KRATOS_TRY

    // Expects condition ids are ordered 1 ... Conditions().size().
    std::vector<std::size_t> condition_ids(rModelPart.NumberOfConditions());
    std::vector<std::size_t> node_ids(rModelPart.NumberOfConditions());
    #pragma omp parallel for
    for (int i = 0; i < static_cast<int>(condition_ids.size()); ++i)
    {
      auto it = rModelPart.ConditionsBegin() + i;
      condition_ids.at(it->Id() - 1) = it->Id();
      std::size_t node_id = it->GetGeometry()[0].Id();
      for (const auto &r_node : it->GetGeometry().Points())
        node_id = std::min(node_id, r_node.Id());
      node_ids.at(it->Id() - 1) = node_id;
    }
    std::stable_sort(condition_ids.begin(), condition_ids.end(),
                     [&node_ids](const std::size_t &i, const std::size_t &j) {
                       return node_ids[i - 1] < node_ids[j - 1];
                     });

    // #pragma omp parallel for
    // for (int i = 0; i < static_cast<int>(condition_ids.size()); ++i)
    //   (rModelPart.ConditionsBegin() + condition_ids[i] - 1)->SetId(i + 1);

    IndexPartition<std::size_t>(condition_ids.size()).for_each([&](std::size_t Index){
                (rModelPart.ConditionsBegin() + condition_ids[Index] - 1)->SetId(Index + 1);
    });

    rModelPart.Conditions().Sort();

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{
  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{
  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{

  /// Assignment operator.
  ModelPartUtilities &operator=(ModelPartUtilities const &rOther);

  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{

  //*******************************************************************************************
  //*******************************************************************************************

  //Same implementation in the core by Pooyan.
  template <bool reverse = false>
  struct CuthillMcKee
  {
    template <class Matrix>
    static void get(const Matrix &A, std::vector<int> &invperm)
    {
      const int n = A.size1();
      std::vector<int> perm(n);

      /* The data structure used to sort and traverse the level sets:
       *
       * The current level set is currentLevelSet;
       * In this level set, there are nodes with degrees from 0 (not really
       * useful) to maxDegreeInCurrentLevelSet.
       * firstWithDegree[i] points to a node with degree i, or to -1 if it
       * does not exist. nextSameDegree[firstWithDegree[i]] points to the
       * second node with that degree, etc.
       * While the level set is being traversed, the structure for the next
       * level set is generated; nMDICLS will be the next
       * maxDegreeInCurrentLevelSet and nFirstWithDegree will be
       * firstWithDegree.
       */
      int initialNode = 0; // node to start search
      int maxDegree = 0;

      std::vector<int> degree(n);
      std::vector<int> levelSet(n, 0);
      std::vector<int> nextSameDegree(n, -1);

      for (int i = 0; i < n; ++i)
      {
        degree[i] = A.index1_data()[i+1] - A.index1_data()[i]; //backend::row_nonzeros(A, i);
	if (std::abs(degree[i]) > n)
	  std::cout<<" something happens in degree["<<i<<"] "<<A.index1_data()[i+1]<<" or "<<A.index1_data()[i]<<std::endl;
        maxDegree = std::max(maxDegree, degree[i]);
      }

      std::vector<int> firstWithDegree(maxDegree + 1, -1);
      std::vector<int> nFirstWithDegree(maxDegree + 1);

      // Initialize the first level set, made up by initialNode alone
      perm[0] = initialNode;
      int currentLevelSet = 1;
      levelSet[initialNode] = currentLevelSet;
      int maxDegreeInCurrentLevelSet = degree[initialNode];
      firstWithDegree[maxDegreeInCurrentLevelSet] = initialNode;

      // Main loop
      for (int next = 1; next < n;)
      {
        int nMDICLS = 0;
        std::fill(nFirstWithDegree.begin(), nFirstWithDegree.end(), -1);
        bool empty = true; // used to detect different connected components

        int firstVal = reverse ? maxDegreeInCurrentLevelSet : 0;
        int finalVal = reverse ? -1 : maxDegreeInCurrentLevelSet + 1;
        int increment = reverse ? -1 : 1;

        for (int soughtDegree = firstVal; soughtDegree != finalVal; soughtDegree += increment)
        {
          int node = firstWithDegree[soughtDegree];
          while (node > 0)
          {
            // Visit neighbors
            for (auto a = A.index1_data()[node] /*backend::row_begin(A, node)*/; a < A.index1_data()[node + 1]; ++a)
            {
              int c = A.index2_data()[a]; //a.col();
              if (levelSet[c] == 0)
              {
                levelSet[c] = currentLevelSet + 1;
                perm[next] = c;
                ++next;
                empty = false; // this level set is not empty
                nextSameDegree[c] = nFirstWithDegree[degree[c]];
                nFirstWithDegree[degree[c]] = c;
                nMDICLS = std::max(nMDICLS, degree[c]);
              }
            }
            node = nextSameDegree[node];
          }
        }

        ++currentLevelSet;
        maxDegreeInCurrentLevelSet = nMDICLS;
        for (int i = 0; i <= nMDICLS; ++i)
          firstWithDegree[i] = nFirstWithDegree[i];

        if (empty)
        {
          // The graph contains another connected component that we
          // cannot reach.  Search for a node that has not yet been
          // included in a level set, and start exploring from it.
          for (int i = 0; i < n; ++i)
          {
            if (levelSet[i] == 0)
            {
              perm[next] = i;
              ++next;
              levelSet[i] = currentLevelSet;
              maxDegreeInCurrentLevelSet = degree[i];
	      // check
	      if (std::abs(maxDegreeInCurrentLevelSet)>firstWithDegree.size()){
		std::cout<<" maxDegreeInCurrentLevelSet "<<maxDegreeInCurrentLevelSet<<" firstWithDegree.size() "<<firstWithDegree.size()<<" node "<<i<<std::endl;
		std::cout<<" degree "<<degree<<std::endl;
	      }
              firstWithDegree[maxDegreeInCurrentLevelSet] = i;
              break;
            }
          }

        }
      }

      //computing the inverse permutation
      #pragma omp parallel for
      for (int i = 0; i < n; ++i)
        invperm[perm[i]] = i;
    }
  };

  ///@}
  ///@name Unaccessible methods
  ///@{
  ///@}

}; // Class ModelPartUtilities

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_MODEL_PART_UTILITIES_HPP_INCLUDED  defined
