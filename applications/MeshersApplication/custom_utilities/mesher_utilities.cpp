//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

// System includes

// External includes

// Project includes
#include "custom_utilities/geometry_utilities.hpp"
#include "custom_utilities/mesher_utilities.hpp"

namespace Kratos
{

//*******************************************************************************************
//*******************************************************************************************

bool MesherUtilities::SameSubdomain(GeometryType &rGeometry)
{

  KRATOS_TRY

  std::string DomainName = rGeometry[0].GetValue(MODEL_PART_NAME); //MODEL_PART_NAME must be set as nodal variable

  const unsigned int size = rGeometry.size();
  for (unsigned int i = 1; i < size; ++i)
    if (DomainName != rGeometry[i].GetValue(MODEL_PART_NAME))
      return false;

  return true;

  KRATOS_CATCH("")
}

//*******************************************************************************************
//*******************************************************************************************

unsigned int MesherUtilities::NumberOfSubdomains(GeometryType &rGeometry)
{

  KRATOS_TRY

  std::vector<std::string> SubdomainNames;
  for(auto &i_node : rGeometry)
    SubdomainNames.push_back(i_node.GetValue(MODEL_PART_NAME));

  unsigned int number = 0;

  std::map<std::string, int> SubdomainNamesMap;
  for(auto &name : SubdomainNames)
  {
    auto result = SubdomainNamesMap.insert(std::pair<std::string, int>(name, 1));
    if (result.second == false)
        result.first->second++;
  }

  for(auto &name : SubdomainNamesMap)
    if (name.second > number)
      number = name.second;

  return number;

  KRATOS_CATCH("")
}

//*******************************************************************************************
//*******************************************************************************************

MesherUtilities::ContactElementType MesherUtilities::CheckContactElement(GeometryType &rGeometry)
{

  KRATOS_TRY

  const unsigned int size = rGeometry.size();

  //Identify subdomains: (non self-contact elements)
  for(auto &i_node : rGeometry)
  {
    if (i_node.IsNot(BOUNDARY)){
      KRATOS_WARNING("")<<" NOT CONTACT TYPE "<<std::endl;
      return MesherUtilities::NonContact;
    }
    if (i_node.Is(NEW_ENTITY)) //review this case
    {
      KRATOS_WARNING("")<<" NOT DEFINED CONTACT TYPE : NEW_ENTITY IS PRESENT "<<std::endl;
      return MesherUtilities::Undefined;
    }
  }

  //Identify subdomains: (non selfcontact elements)
  unsigned int samesbd = NumberOfSubdomains(rGeometry);
  //2D: 3:Self-Contact / 2:PointToFace / 1:PointToPoint
  //3D: 4:Self-Contact / 3:PointToFace / 2:EdgeToEdge / 1:PontToFace

  if (rGeometry.WorkingSpaceDimension() == 3)
    switch (samesbd)
    {
    case 4:
      return MesherUtilities::SelfContact;
    case 3:
      return MesherUtilities::PointToFace;
    case 2:
      return MesherUtilities::EdgeToEdge;
    case 1:
      return MesherUtilities::PointToPoint;
    default:
      return MesherUtilities::Undefined;
    }
  else{
    switch (samesbd)
    {
    case 3:
      return MesherUtilities::SelfContact;
    case 2:
      return MesherUtilities::PointToFace;
    case 1:
      return MesherUtilities::PointToPoint;
    default:
      return MesherUtilities::Undefined;
    }
  }

  KRATOS_CATCH("")
}




//*******************************************************************************************
//*******************************************************************************************

bool MesherUtilities::CheckFluidMeniscus(GeometryType &rGeometry, const unsigned int &dimension)
{

  KRATOS_TRY

  array_1d<double, 3> Gravity;
  bool gravity_set = false;
  if (rGeometry[0].SolutionStepsDataHas(VOLUME_ACCELERATION))
  {
    noalias(Gravity) = rGeometry[0].FastGetSolutionStepValue(VOLUME_ACCELERATION);
    if (norm_2(Gravity) == 0)
    {
      if (rGeometry[0].SolutionStepsDataHas(BODY_FORCE))
      {
        noalias(Gravity) = rGeometry[0].FastGetSolutionStepValue(BODY_FORCE);
        if (norm_2(Gravity) != 0)
        {
          gravity_set = true;
        }
      }
    }
    else
    {
      gravity_set = true;
    }
  }

  for (auto &i_node : rGeometry)
  {
    if (i_node.Is(FREE_SURFACE) && i_node.AndNot({RIGID, SOLID}))
    { //no wall free surface
      array_1d<double, 3> SurfaceNormal;
      noalias(SurfaceNormal) = ZeroVector(3);
      array_1d<double, 3> Normal;
      noalias(Normal) = ZeroVector(3);
      array_1d<double, 3> Edge;
      noalias(Edge) = ZeroVector(3);
      std::vector<array_1d<double, 3>> PlaneVectors;

      NodeWeakPtrVectorType &nNodes = i_node.GetValue(NEIGHBOUR_NODES);

      double counter = 0;
      for (auto &i_nnode : nNodes) //characterize surface without interaction nodes
      {
        if (i_nnode.Is(FREE_SURFACE) && i_nnode.AndNot({RIGID,INTERACTION}))
        {
          Normal += i_nnode.FastGetSolutionStepValue(NORMAL);
          Edge += i_nnode.Coordinates();
          PlaneVectors.push_back(i_nnode.Coordinates() - i_node.Coordinates());
          ++counter;
        }
      }

      //std::cout<<" nodei ["<<i_node.Id()<<"] coodinates "<<i_node.Coordinates()<<" normal "<<Normal<<std::endl;

      if (counter > 1)
      {
        //std::cout<<" NORMAL "<<Normal/norm_2(Normal)<<std::endl;
        MathUtils<double>::UnitCrossProduct(SurfaceNormal, PlaneVectors.front(), PlaneVectors.back());
        if (inner_prod(SurfaceNormal, Normal) < 0)
          Normal = -SurfaceNormal;
        else
          Normal = SurfaceNormal;
        //std::cout<<" Computed NORMAL "<<Normal<<std::endl;
      }

      double norm = norm_2(Normal);

      if (norm == 0)
      { //characterize surface with interaction nodes
        counter = 0;
        noalias(Normal) = ZeroVector(3);
        noalias(Edge) = ZeroVector(3);
        PlaneVectors.clear();
        PlaneVectors.resize(0);
        for (auto &i_nnode : nNodes)
        {
          if (i_nnode.Is(FREE_SURFACE) && i_nnode.IsNot(RIGID))
          {
            Normal += i_nnode.FastGetSolutionStepValue(NORMAL);
            Edge += i_nnode.Coordinates();
            PlaneVectors.push_back(i_nnode.Coordinates() - i_node.Coordinates());
            ++counter;
          }
        }

        if (counter > 1)
        {
          //std::cout<<" NORMAL INTERACTION "<<Normal/norm_2(Normal)<<std::endl;
          MathUtils<double>::UnitCrossProduct(SurfaceNormal, PlaneVectors.front(), PlaneVectors.back());
          if (inner_prod(SurfaceNormal, Normal) < 0)
            Normal = -SurfaceNormal;
          else
            Normal = SurfaceNormal;
          //std::cout<<" Computed NORMAL "<<Normal<<std::endl;
        }
        norm = norm_2(Normal);
      }

      if (norm != 0)
      {
        Normal /= norm;
        Edge /= counter;

        double divider = norm_2(Edge);
        Normal -= inner_prod(Normal, divider * Edge) * divider * Edge;

        //mitigate effect descending interaction nodes
        // array_1d<double,3> Surface = i_node.Coordinates()-Edge;
        // norm = norm_2(Surface);
        // if(norm!=0)
        //   Surface /= norm;

        // if( inner_prod(Surface,Normal) > 0.7 ){
        //   Edge += i_node.Coordinates();
        //   Edge *= 0.5;
        // }

        for (auto &j_node : rGeometry)
        {
          if (i_node.Id() != j_node.Id())
          {

            if (j_node.Or({RIGID,SOLID}) && j_node.Or({FLUID.AsFalse(),FREE_SURFACE}))
            {

              array_1d<double, 3> Meniscus = j_node.Coordinates() - Edge;
              norm = norm_2(Meniscus);
              if (norm != 0)
              {
                Meniscus /= norm;

                //std::cout<<" nodej ["<<i_node.Id()<<"] Edge "<<Edge<<" projection "<<inner_prod(Edge,Normal)<<std::endl;
                double projection = -1;
                if (gravity_set)
                  projection = inner_prod(Meniscus, Gravity) / norm_2(Gravity);

                //std::cout<<" Gravity "<<Gravity<<" projection "<<projection<<std::endl;

                // if meniscus is two vertical and direction is opposite to gravity
                if (inner_prod(Meniscus, Normal) > 0.25 && projection < -0.1)
                {

                  //std::cout<<" not accepted if "<<inner_prod(i_node.FastGetSolutionStepValue(NORMAL),j_node.FastGetSolutionStepValue(NORMAL))<<" < "<<0.25<<std::endl;

                  // if free_surface is not parallel to wall
                  projection = inner_prod(Normal, j_node.FastGetSolutionStepValue(NORMAL));
                  if (projection < 0.7)
                    return false;

                  // if meniscus is too large
                  // double Mean = 0;
                  // for(auto& i_dist : PlaneVectors)
                  //   Mean += norm_2(PlaneVectors);
                  // Mean /= double(PlaneVectors.size());
                  // if( projection < 0.9 && (norm_2(j_node.Coordinates()-i_node.Coordinates()) > 3*Mean) ){
                  //   return false;
                  // }
                }
              }
            }
          }
        }
      }
    }
  }

  return true;

  KRATOS_CATCH("")
}

//*******************************************************************************************
//*******************************************************************************************

bool MesherUtilities::CheckFluidMeniscus2(GeometryType &rGeometry, const unsigned int &dimension)
{

  KRATOS_TRY

  array_1d<double, 3> Gravity;
  bool gravity_set = false;
  if (rGeometry[0].SolutionStepsDataHas(VOLUME_ACCELERATION))
  {
    noalias(Gravity) = rGeometry[0].FastGetSolutionStepValue(VOLUME_ACCELERATION);
    double norm_gravity = norm_2(Gravity);
    if (norm_gravity == 0)
    {
      if (rGeometry[0].SolutionStepsDataHas(BODY_FORCE))
      {
        noalias(Gravity) = rGeometry[0].FastGetSolutionStepValue(BODY_FORCE);
        norm_gravity = norm_2(Gravity);
        if (norm_gravity != 0)
        {
          gravity_set = true;
          Gravity /= norm_gravity;
        }
      }
    }
    else
    {
      gravity_set = true;
      Gravity /= norm_gravity;
    }
  }

  for (auto &i_node : rGeometry)
  {
    if (i_node.Is(FREE_SURFACE) && i_node.AndNot({RIGID,SOLID}))
    { //no wall free surface
      array_1d<double, 3> Normal;
      noalias(Normal) = ZeroVector(3);
      array_1d<double, 3> Edge;
      noalias(Edge) = ZeroVector(3);

      NodeWeakPtrVectorType &nNodes = i_node.GetValue(NEIGHBOUR_NODES);

      double counter = 0;
      for (auto &i_nnode : nNodes) //characterize surface without interaction nodes
      {
        if (i_nnode.Is(FREE_SURFACE) && i_nnode.AndNot({RIGID,INTERACTION}))
        {
          NodeWeakPtrVectorType &nnNodes = i_nnode.GetValue(NEIGHBOUR_NODES);
          for (auto &ni_nnode : nnNodes) //characterize surface without interaction nodes
          {
            if (ni_nnode.Is(FREE_SURFACE) && ni_nnode.AndNot({RIGID,INTERACTION}))
            {
              Normal += ni_nnode.FastGetSolutionStepValue(NORMAL);
              Edge += ni_nnode.Coordinates();
              ++counter;
            }
          }
          if (dimension == 2)
          {
            Normal += i_nnode.FastGetSolutionStepValue(NORMAL);
            Edge += i_nnode.Coordinates();
            ++counter;
          }
        }
      }

      double norm = norm_2(Normal);

      if (norm == 0)
      { //characterize surface with interaction nodes
        counter = 0;
        noalias(Normal) = ZeroVector(3);
        noalias(Edge) = ZeroVector(3);
        for (auto &i_nnode : nNodes)
        {
          if (i_nnode.Is(FREE_SURFACE) && i_nnode.IsNot(RIGID))
          {
            NodeWeakPtrVectorType &nnNodes = i_nnode.GetValue(NEIGHBOUR_NODES);
            for (auto &ni_nnode : nnNodes) //characterize surface without interaction nodes
            {
              if (ni_nnode.Is(FREE_SURFACE) && ni_nnode.AndNot({RIGID,INTERACTION}))
              {
                Normal += ni_nnode.FastGetSolutionStepValue(NORMAL);
                Edge += ni_nnode.Coordinates();
                ++counter;
              }
            }
            if (dimension == 2)
            {
              Normal += i_nnode.FastGetSolutionStepValue(NORMAL);
              Edge += i_nnode.Coordinates();
              ++counter;
            }
          }
        }

        norm = norm_2(Normal);
      }

      if (norm != 0)
      {
        Normal /= norm;
        Edge /= counter;

        // this operation is wrong:
        // double divider = norm_2(Edge);
        // Normal -= inner_prod(Normal,divider*Edge)*divider*Edge;

        norm = norm_2(Normal);
        if (norm != 0)
          Normal /= norm;

        //mitigate effect descending interaction nodes
        // array_1d<double,3> Surface = i_node.Coordinates()-Edge;
        // norm = norm_2(Surface);
        // if(norm!=0)
        //   Surface /= norm;

        // if( inner_prod(Surface,Normal) > 0.7 ){
        //   Edge += i_node.Coordinates();
        //   Edge *= 0.5;
        // }

        for (auto &j_node : rGeometry)
        {
          if (i_node.Id() != j_node.Id())
          {

            if (j_node.Or({RIGID,SOLID}) && j_node.Or({FLUID.AsFalse(),FREE_SURFACE}))
            {

              array_1d<double, 3> Meniscus = j_node.Coordinates() - Edge;
              norm = norm_2(Meniscus);
              if (norm != 0)
              {
                Meniscus /= norm;

                //std::cout<<" nodej ["<<i_node.Id()<<"] Edge "<<Edge<<" projection "<<inner_prod(Edge,Normal)<<std::endl;

                double direction = inner_prod(Meniscus, Normal);

                double projection = 1;
                if (gravity_set)
                  projection = inner_prod(Meniscus, Gravity) / norm_2(Gravity);
                else if (direction > 0.4)
                  projection = -1; // to check possible release

                //std::cout<<" Gravity "<<Gravity<<" projection "<<projection<<std::endl;

                // if meniscus is two vertical and direction is opposite to gravity
                if (direction > 0.2 && projection < 0.0)
                {

                  //check real meniscus direction
                  Meniscus = j_node.Coordinates() - i_node.Coordinates();
                  norm = norm_2(Meniscus);
                  if (norm != 0)
                  {
                    Meniscus /= norm;
                    if (gravity_set)
                      projection = inner_prod(Meniscus, Gravity);

                    if (projection < -0.1)
                    {
                      // if free_surface is not parallel to wall
                      direction = inner_prod(Normal, j_node.FastGetSolutionStepValue(NORMAL));
                      if (direction < 0.7)
                      {
                        //std::cout<<" not accepted :: direction "<<projection<<" direction "<<direction<<" gravity_set "<<gravity_set<<std::endl;
                        return false;
                      }
                      // else{
                      //   std::cout<<" accepted :: direction "<<projection<<" direction "<<direction<<" gravity_set "<<gravity_set<<std::endl;
                      // }
                    }
                    // else{
                    //   std::cout<<" accepted :: projection "<<projection<<" direction "<<direction<<" gravity_set "<<gravity_set<<std::endl;
                    // }
                  }

                  // if meniscus is too large
                  // double Mean = 0;
                  // for(auto& i_dist : PlaneVectors)
                  //   Mean += norm_2(PlaneVectors);
                  // Mean /= double(PlaneVectors.size());
                  // if( projection < 0.9 && (norm_2(j_node.Coordinates()-i_node.Coordinates()) > 3*Mean) ){
                  //   return false;
                  // }
                }
                // else{
                //   std::cout<<" accepted :: meniscus "<<projection<<" direction "<<direction<<" gravity_set "<<gravity_set<<std::endl;
                // }
              }
            }
          }
        }
      }
    }
  }

  return true;

  KRATOS_CATCH("")
}

//*******************************************************************************************
//*******************************************************************************************

bool MesherUtilities::CheckContactShape(GeometryType &rGeometry)
{
  KRATOS_TRY

  //check sliver
  const unsigned int size = rGeometry.size();
  //check if is a sliver and the distorsion of the sliver
  double MaximumSideLength = 0;
  double MinimumSideLength = 0;

  //compare side lengths
  double RelativeSideLength = 0;
  RelativeSideLength = GeometryUtilities::GetAndCompareSideLenghts(rGeometry, MaximumSideLength, MinimumSideLength);

  //double CriticalVolume = pow(MinimumSideLength, size - 1);
  double CriticalVolume = 1e-4 * pow(MaximumSideLength, size - 1);
  double Volume = rGeometry.Volume();
  if (Volume <= CriticalVolume)
  {
    //check if it is a contact element (contact domain definition)
    ContactElementType ContactType = CheckContactElement(rGeometry);

    if (ContactType == NonContact || ContactType == PointToPoint)
    {
      KRATOS_WARNING("")<<" NOT CONTACT OR POINT TO POINT"<<std::endl;
      return false;
    }
    else if (ContactType == EdgeToEdge)
    {
      // double CriticalRelativeSideLength = (double)size*2; //edge relative length (3,4)
      // if (RelativeSideLength > CriticalRelativeSideLength)
      //   return false;
      // else
      // KRATOS_WARNING("")<<" PATCH-B "<<std::endl;
      // if (GeometryUtilities::CheckContactSliver(rGeometry))
      // {
      // 	  return true;
      // }
      // else{
      // 	  KRATOS_WARNING("")<<" Not a EDGE SLIVER "<<std::endl;
      // 	  return false;
      // }
      return true;
    }
    else if (ContactType == PointToFace)
    {
      // double CriticalRelativeSideLength = (double)size*5; //edge relative length (3,4) * 2
      // if (RelativeSideLength > CriticalRelativeSideLength)
      //   return false;
      // else
      // KRATOS_WARNING("")<<" PATCH-A "<<std::endl;
      // if (GeometryUtilities::CheckContactSliver(rGeometry))
      // {
      // 	  return true;
      // }
      // else{
      // 	  KRATOS_WARNING("")<<" Not a FACE SLIVER "<<std::endl;
      // 	  return false;
      // }
      return true;
    }
    else
    {
      //KRATOS_WARNING("")<<" UNDEFINED "<<std::endl;
      return true;
    }
  }
  else{
    //KRATOS_WARNING("")<<" Not a volume SLIVER V:"<<Volume<<" CV:"<<CriticalVolume<<std::endl;
    return false;
  }

  KRATOS_CATCH("")
}

//*******************************************************************************************
//*******************************************************************************************

bool MesherUtilities::CheckGeometryShape(GeometryType &rGeometry, int &rShape)
{
  KRATOS_TRY

  bool sliver = false;
  bool distorted = false;

  const unsigned int size = rGeometry.size();

  //check if is a sliver and the distorsion of the sliver

  double Volume = 0;
  double MaximumSideLength = 0;
  double MinimumSideLength = 0;

  double CriticalRelativeSideLength = (double)size * 5; //edge relative length (3,4)

  Volume = rGeometry.Volume();

  //compare side lengths
  double RelativeSideLength = 0;
  RelativeSideLength = GeometryUtilities::GetAndCompareSideLenghts(rGeometry, MaximumSideLength, MinimumSideLength);

  if (RelativeSideLength > CriticalRelativeSideLength)
  {
    //std::cout<<" RelativeSideLength "<<RelativeSideLength<<std::endl;
    distorted = true;
  }

  double CriticalVolume = 1e-12 * pow(MinimumSideLength, size - 1);

  //check sliver (volume)
  if (Volume < CriticalVolume)
  {
    //std::cout<<" Sliver (volume) "<<Volume<<" "<<CriticalVolume<<std::endl;
    sliver = true;
  }

  //check sliver (volume + normals and faces)
  if (!sliver)
  {
    sliver = GeometryUtilities::CheckSliver(rGeometry);
    // if(sliver)
    // 	std::cout<<" Sliver (faces) "<<sliver<<std::endl;
  }

  //check if it is a contact element (contact domain definition)
  ContactElementType ContactType = CheckContactElement(rGeometry);

  if (ContactType != NonContact)
  {

    //std::cout<<" contact type "<<std::endl;

    if (ContactType == PointToFace)
    { //POINT_FACE

      //check the projection of the slave vertex on the geometry face
      double AreaTolerance = 2; //if is outside of the face (only a 2*FaceArea deviation is allowed)
      double FaceArea = 0;
      double ProjectedArea = 0;
      //FaceArea = ComputeFaceArea(rGeometry, SlaveVertex);
      //ProjectedArea = ComputePointToFaceProjection(rGeometry, SlaveVertex);

      if (ProjectedArea < 0)
      { // projection outside of the face

        if (FaceArea < AreaTolerance * fabs(ProjectedArea))
          distorted = true;
      }
    }
    else if (ContactType == EdgeToEdge)
    { //EDGE_EDGE

      //compare vertex normals (detect coplanar faces and orthogonal faces)
      //if( !CheckVertexNormals(rGeometry) )
      distorted = true;
    }
    else if (ContactType == PointToPoint)
    { //POINT_POINT

      //compare vertex normals (detect coplanar faces and orthogonal faces)
      //if( !CheckVertexNormals(rGeometry) )
      distorted = true;
    }
  }

  //std::cout<<" DISTORTED "<<distorted<<" SLIVER "<<sliver<<std::endl;

  if (sliver)
    rShape = 1;
  else
    rShape = 0;

  if (distorted)
    return false; //returns false if the geometry has not the correct shape and has to be removed
  else
    return true; //returns true if the geometry has the correct shape and is kept

  KRATOS_CATCH("")
}

//*******************************************************************************************
//*******************************************************************************************

void MesherUtilities::CheckParticles(ModelPart &rModelPart)
{
  KRATOS_TRY

  int NumberOfNodes = rModelPart.NumberOfNodes();
  std::cout << " Number of Nodes " << NumberOfNodes << std::endl;
  for (int id = 1; id <= NumberOfNodes; ++id)
  {
    std::cout << " Check node: " << id << std::endl;
    if (rModelPart.Nodes()[id].Is(BOUNDARY))
    {
      std::cout << " Node : " << id << " is boundary " << std::endl;
    }
    else
    {
      std::cout << " Node : " << id << " is not boundary " << std::endl;
    }
  }

  KRATOS_CATCH("")
}

//*******************************************************************************************
//*******************************************************************************************

bool MesherUtilities::CheckContactActive(const GeometryType &rConditionGeometry, bool &rSemiActiveContact, std::vector<bool> &rSemiActiveNodes)
{
  KRATOS_TRY

  unsigned int size = rConditionGeometry.size();
  unsigned int counter = 0;

  rSemiActiveContact = false;
  rSemiActiveNodes.resize(size);
  std::fill(rSemiActiveNodes.begin(), rSemiActiveNodes.end(), false);

  for (unsigned int i = 0; i < size; ++i)
  {

    bool contact_active = false;
    if (rConditionGeometry[i].SolutionStepsDataHas(CONTACT_FORCE))
    {
      const array_1d<double, 3> &ContactForceNormal = rConditionGeometry[i].FastGetSolutionStepValue(CONTACT_FORCE);

      if (norm_2(ContactForceNormal) > 0)
        contact_active = true;
    }

    if (contact_active)
    {
      rSemiActiveContact = true;
      rSemiActiveNodes[i] = true;
      counter++;
    }
  }

  if (counter == size)
    return true;
  else
    return false;

  KRATOS_CATCH("")
}

} // Namespace Kratos
