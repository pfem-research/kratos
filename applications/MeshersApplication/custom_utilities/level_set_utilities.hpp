//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:     November 2022 $
//
//

// AUTHOR: Eduardo Fernandez Sanchez -  ULiege 2022

#if !defined(KRATOS_LEVEL_SET_UTILITIES_HPP_INCLUDED)
#define KRATOS_LEVEL_SET_UTILITIES_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "includes/model_part.h"
#include "spaces/ublas_space.h"
#include "linear_solvers/amgcl_solver.h"
#include "linear_solvers/reorderer.h"
#include "custom_bounding/spatial_bounding_box.hpp"
#include "linear_system/linear_solvers/superlu_direct_solver.hpp"

namespace Kratos
{

/// Short class definition.
class LevelSetUtilities
{
public:
  ///@name Type Definitions
  ///@{
  typedef ModelPart::NodesContainerType NodesContainerType;

  //definitions for spatial search
  typedef array_1d<double, 3> PointType;
  typedef std::vector<PointType> PointVectorType;
  typedef std::vector<PointType*> PointPointerVectorType;
  typedef Node<3> NodeType;
  typedef std::vector<NodeType> NodeVectorType;
  typedef NodeType::Pointer NodePointerType;
  typedef std::vector<NodePointerType> NodePointerVectorType;
  typedef NodePointerVectorType::iterator NodePointerIterator;

  typedef Kratos::Vector DenseVectorType;
  typedef Kratos::Matrix DenseMatrixType;
  typedef boost::numeric::ublas::vector<double> SparseVectorType;
  typedef UblasSpace<double, CompressedMatrix, SparseVectorType> SparseSpaceType;
  typedef UblasSpace<double, DenseMatrixType, DenseVectorType> LocalSpaceType;
  typedef LinearSolver<SparseSpaceType, LocalSpaceType> LinearSolverType;

  typedef Reorderer<SparseSpaceType,  LocalSpaceType > ReordererType;
  typedef AMGCLSolver<SparseSpaceType,  LocalSpaceType, ReordererType > AMGCLSolverType;
  typedef SuperLUDirectSolver<SparseSpaceType,  LocalSpaceType, ReordererType > SuperLUDirectSolverType;

  typedef typename SparseSpaceType::MatrixType SystemMatrixType;
  typedef typename SparseSpaceType::VectorType SystemVectorType;

  //structure for a spline segment type
  typedef struct
  {
    unsigned int dimension;

    PointPointerVectorType Boundary; // Boundary knots
    PointVectorType Interior; // Interior knots
    PointVectorType Exterior; // Exterior knots

    std::vector<double> LinearTerms;
    std::vector<double> RadialTerms;

  } LevelSetType;

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  LevelSetUtilities()
  {
  };

  /// Copy constructor.
  LevelSetUtilities(LevelSetUtilities const &rOther) : mLevelSetKnots(rOther.mLevelSetKnots)
  {
  };

  /// Destructor.
  ~LevelSetUtilities(){};

  ///@}
  ///@name Operators
  ///@{
  ///@}
  ///@name Operations
  ///@{

  void CreateLevelSetFunction(ModelPart &rModelPart, double h_size)
  {
    KRATOS_TRY

    const ProcessInfo &rCurrentProcessInfo = rModelPart.GetProcessInfo();

    mLevelSetKnots.dimension = rCurrentProcessInfo[SPACE_DIMENSION];

    for (auto &i_node: rModelPart.Nodes())
      if(i_node.Is(BOUNDARY) && i_node.Is(FLUID) && i_node.IsNot(ISOLATED))
        mLevelSetKnots.Boundary.push_back(&i_node.Coordinates());

    for (auto &i_elem: rModelPart.Elements())
      if(i_elem.Or({FREE_SURFACE,INTERACTION}))
        mLevelSetKnots.Interior.push_back(i_elem.GetGeometry().Center());

    SpatialBoundingBox Box(rModelPart, h_size * 4);
    Box.GetVertices(mLevelSetKnots.Exterior, rCurrentProcessInfo, mLevelSetKnots.dimension);

    std::cout<<" size Interior "<<mLevelSetKnots.Interior.size()<<std::endl;
    std::cout<<" size Exterior "<<mLevelSetKnots.Exterior.size()<<std::endl;
    std::cout<<" size Boundary "<<mLevelSetKnots.Boundary.size()<<std::endl;

    this->CreateLevelSetFunction(mLevelSetKnots, h_size);

    KRATOS_CATCH("")
  }

  //************************************************************************************
  //************************************************************************************

  void CreateLevelSetFunction(LevelSetType& rLevelSetKnots, double h_size)
  {
    KRATOS_TRY

    /* This function :
       1) Builds the System A Dx = b, where A is a symmetric (and hollow) matrix.
            *Only the lower triangle is filled using a column major loop.
            *Columns are arranged as [Interior | Boundary | Exterior].

       2) Solves the System and get "Dx"

       3) Gets the polynomial coefficients from "Dx" and store them in memory.
    */

    unsigned int npi = rLevelSetKnots.Interior.size();
    unsigned int npb = rLevelSetKnots.Boundary.size();
    unsigned int npe = rLevelSetKnots.Exterior.size();
    unsigned int npt = npi + npb + npe;

    //1.- Build the system
    unsigned int system_size = npt + rLevelSetKnots.dimension + 1;
    SystemMatrixType A = CompressedMatrix(system_size, system_size);
    SystemVectorType Dx = ZeroVector(system_size);
    SystemVectorType b = ZeroVector(system_size);

    std::cout<<" Matrices initialized "<<system_size<<std::endl;

    std::vector<double> minimum_interior(npb , h_size * 1e10); // because std::numeric_limits<double>::max() gives different results

    // INTERIOR POINTS
    //#pragma omp parallel for
    for (unsigned int col = 0; col < npi; ++col)
    {
      const PointType& j_node = rLevelSetKnots.Interior[col];

      double distance = 0.0;
      for (unsigned int row = col+1; row < npi; ++row)
      {
        const PointType& i_node = rLevelSetKnots.Interior[row];
        distance = norm_2(j_node-i_node);
        A(row, col) = distance;
      }


      // distance to edge points and take the minimum (this is the solution for PHI)
      double minimum = h_size * 1e10; // because std::numeric_limits<double>::max() gives different results

      for(unsigned int row = 0; row < npb; ++row)
      {
        const PointType& i_node = *(rLevelSetKnots.Boundary[row]);
        distance = norm_2(j_node-i_node);
        minimum = std::min(distance, minimum);
        minimum_interior[row] = std::min(distance,minimum_interior[row]);
        A(row+npi, col) = distance;
      }

      // solution
      b[col] = minimum;

      // distance to exterior points
      for(unsigned int row = 0 ; row < npe ; ++row)
      {
        const PointType& i_node = rLevelSetKnots.Exterior[row];
        distance = norm_2(j_node-i_node);
        A(row+npi+npb, col) = distance;
      }


      // orthogonality condition
      A(npt, col) = 1.0;
      for(unsigned int i = 0 ; i < rLevelSetKnots.dimension ; ++i)
        A(npt+1+i, col) = j_node[i];

    }

    std::vector<double> minimum_exterior(npe , h_size * 1e10); // because std::numeric_limits<double>::max() gives different results

    std::cout<<" Interior points filled "<<npi<<std::endl;

    // BOUNDARY POINTS
    //#pragma omp parallel for
    for(unsigned int col = 0 ; col < npb ; ++col)
    {
      const PointType& j_node = *(rLevelSetKnots.Boundary[col]);
      double distance = 0.0;
      // distance to edge points (skip row = col)
      for(unsigned int row = col+1 ; row < npb ; ++row)
      {
        const PointType& i_node = *(rLevelSetKnots.Boundary[row]);
        distance = norm_2(j_node-i_node);
        A(row+npi, col+npi) = distance;
      }

      // solution (already initilized at 0.0)
      // b[col] = 0.0;

      // distance to exterior points
      for(unsigned int row = 0 ; row < npe ; ++row)
      {
        const PointType& i_node = rLevelSetKnots.Exterior[row];
        distance = norm_2(j_node-i_node);
        minimum_exterior[row] = std::min(distance,minimum_exterior[row]);
        A(row+npi+npb, col+npi) = distance;
      }

      // orthogonality condition
      A(npt, col+npi) = 1.0;
      for(unsigned int i = 0 ; i < rLevelSetKnots.dimension ; ++i)
        A(npt+1+i, col+npi) = j_node[i];
    }


    std::cout<<" Boundary points filled "<<npb<<std::endl;

    // EXTERIOR POINTS
    //#pragma omp parallel for
    for(unsigned int col = 0 ; col < npe ; ++col)
    {
      const PointType& j_node = rLevelSetKnots.Exterior[col];
      double distance = 0.0;
      // solution
      b[col+npi+npb] = - minimum_exterior[col];

      // distance to exterior points
      for(unsigned int row = col + 1 ; row < npe ; ++row)
      {
        const PointType& i_node = rLevelSetKnots.Exterior[row];
        distance = norm_2(j_node-i_node);
        A(row+npi+npb, col+npi+npb) = distance;
      }

      // orthogonality condition
      A(npt, col+npi+npb) = 1.0;

      for(int i = 0 ; i < rLevelSetKnots.dimension ; ++i)
        A(npt+1+i, col+npi+npb) = j_node[i];
    }

    std::cout<<" Exterior points filled "<<npe<<std::endl;

    //2.- Solve the system

    double epsilon = *std::min_element(minimum_interior.begin(), minimum_interior.end());

    // symmetric matrix
    for(unsigned int col = 0 ; col < system_size ; ++col)
      for(unsigned int row = col+1 ; row < system_size ; ++row)
        A(col,row) = A(row,col);


    // #pragma omp parallel for
    // for(unsigned int col = 0 ; col < npt ; ++col)
    // {
    //   A(col, col) = 0.001 * epsilon;
    // }

    // std::cout<<" A "<<A<<std::endl;

    // Parameters solver_parameters =  Parameters(R"({})");
    // LinearSolverType::Pointer pSolver = LinearSolverType::Pointer( new AMGCLSolverType(solver_parameters) );

    Parameters solver_parameters =  Parameters(R"({})");
    LinearSolverType::Pointer pSolver = LinearSolverType::Pointer( new SuperLUDirectSolverType(solver_parameters) );

    pSolver->Solve(A, Dx, b);

    std::cout<<" LevelSet System Solved "<<0.001*epsilon<<std::endl;

    //3.- Save the solution

    // signed linear coefficients
    rLevelSetKnots.LinearTerms.resize(rLevelSetKnots.dimension + 1);
    rLevelSetKnots.LinearTerms[0] = Dx[npt];
    for(unsigned int i = 0; i < rLevelSetKnots.dimension; ++i)
      rLevelSetKnots.LinearTerms[1+i] = Dx[npt+1+i];

    // signed radial coefficients
    rLevelSetKnots.RadialTerms.resize(npt);
    #pragma omp parallel for
    for(int i = 0; i < npt; ++i)
      rLevelSetKnots.RadialTerms[i]  = Dx[i];


    KRATOS_CATCH("")
  }

  //************************************************************************************
  //************************************************************************************

  double CalculatePointProjection(const NodeType &rNode) const
  {
    KRATOS_TRY

    const PointType& rPoint = rNode.Coordinates();

    return CalculatePointProjection(rPoint);

    KRATOS_CATCH("")
  }

  //************************************************************************************
  //************************************************************************************

  double CalculatePointProjection(const PointType &rPoint) const
  {
    KRATOS_TRY

    double phi  = mLevelSetKnots.LinearTerms[0];

    // linear polynomial
    for(unsigned int k = 0 ; k < mLevelSetKnots.dimension ; ++k)
      phi += mLevelSetKnots.LinearTerms[1+k] * rPoint[k];

    // radial polynomial (interior)
    unsigned int npi = mLevelSetKnots.Interior.size();
    for(unsigned int n = 0 ; n < npi; ++n)
      phi += mLevelSetKnots.RadialTerms[n] * norm_2(rPoint-(mLevelSetKnots.Interior[n]));

    // radial polynomial (boundary)
    unsigned int npb = mLevelSetKnots.Boundary.size();
    for(unsigned int n = 0 ; n < npb; ++n)
      phi += mLevelSetKnots.RadialTerms[n+npi] * norm_2(rPoint-(*mLevelSetKnots.Boundary[n]));

    // radial polynomial (exterior)
    unsigned int npe = mLevelSetKnots.Exterior.size();
    for(unsigned int n = 0; n < npe; n++)
      phi += mLevelSetKnots.RadialTerms[n+npi+npb] * norm_2(rPoint-(mLevelSetKnots.Exterior[n]));

    return phi;

    KRATOS_CATCH("")
  }

  //************************************************************************************
  //************************************************************************************

  void PrintPointsXYQ(ModelPart &rModelPart, std::vector<PointType> &rPointList, std::string Name)
  {
    KRATOS_TRY

    const int &step = rModelPart.GetProcessInfo()[STEP];

    std::string FileName;
    FileName += rModelPart.Name();
    FileName += "_points_";
    FileName += Name;
    FileName += "_";
    FileName += std::to_string(step);
    FileName += ".txt";

    std::ofstream File;

    File.open(FileName);

    for (unsigned int pn = 0; pn < rPointList.size(); pn++)
    {
      std::string Point;
      for (unsigned int i = 0; i < 2; i++)
      {
        Point += " ";
        Point += to_string_precision(rPointList[pn][i]);
      }
      Point += " ";
      Point += to_string_precision(CalculatePointProjection(rPointList[pn]));

      Point += " \n";

      File << Point;
    }

    File.close();

    KRATOS_CATCH(" ")
  }

  //************************************************************************************
  //************************************************************************************

  template <typename T>
  std::string to_string_precision(const T a_value, const int n = 12)
  {
    std::ostringstream out;
    out.precision(n);
    out << std::fixed << a_value;
    return out.str();
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{
  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{
  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}
private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{

  LevelSetType mLevelSetKnots;

  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Unaccessible methods
  ///@{
  ///@}

}; // Class LevelSetUtilities

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{

///@}

} // namespace Kratos.

#endif // KRATOS_LEVEL_SET_UTILITIES_HPP_INCLUDED defined
