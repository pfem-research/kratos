//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        March 2021 $
//
//

#if !defined(KRATOS_GEOMETRY_UTILITIES_HPP_INCLUDED)
#define KRATOS_GEOMETRY_UTILITIES_HPP_INCLUDED

// External includes

// System includes
#include <numeric>
#include <vector>

// Project includes
#include "geometries/geometry.h"
#include "meshers_application_variables.h"
#include "custom_utilities/set_flags_utilities.hpp"

namespace Kratos
{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
 */
class KRATOS_API(MESHERS_APPLICATION) GeometryUtilities
{
public:
  ///@name Type Definitions
  ///@{

  typedef Node<3> NodeType;
  typedef array_1d<double,3> VectorType;
  typedef Geometry<NodeType> GeometryType;
  typedef Geometry<NodeType>::PointsArrayType PointsArrayType;

  typedef BoundedVector<double,2> Point2DType;


  typedef std::size_t IndexType;
  typedef GeometryType::CoordinatesArrayType CoordinatesArrayType;

  typedef GlobalPointersVector<Node<3>> NodeWeakPtrVectorType;
  typedef GlobalPointersVector<Element> ElementWeakPtrVectorType;
  typedef GlobalPointersVector<Condition> ConditionWeakPtrVectorType;

  /// Pointer definition of GeometryUtilities
  KRATOS_CLASS_POINTER_DEFINITION(GeometryUtilities);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  GeometryUtilities() {} //

  /// Destructor.
  virtual ~GeometryUtilities() {}

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{



  //**************************************************************************
  //**************************************************************************

  static inline double GetDistanceToNode(NodeType &rNode, NodeType &rEdgeNode, array_1d<double, 3> &rDirection)
  {
    rDirection = rEdgeNode.FastGetSolutionStepValue(NORMAL);
    if (norm_2(rDirection) != 0)
      rDirection /= norm_2(rDirection);

    array_1d<double, 3> Distance = (rEdgeNode.Coordinates() - rNode.Coordinates());
    double distance = fabs(inner_prod(Distance, rDirection));

    return distance;
  }

  //**************************************************************************
  //**************************************************************************

  static inline double GetDistanceToEdge(NodeType &rNode, NodeType &rEdgeNodeA, NodeType &rEdgeNodeB, array_1d<double, 3> &rDirection)
  {
    array_1d<double, 3> MidPoint = 0.5 * (rEdgeNodeA.Coordinates() + rEdgeNodeB.Coordinates());

    GetDirectionToEdge(rDirection, rEdgeNodeA, rEdgeNodeB);

    double distance = fabs(inner_prod((MidPoint - rNode.Coordinates()), rDirection));

    return distance;
  }

  //**************************************************************************
  //**************************************************************************

  static inline double GetDistanceToFace(NodeType &rNode, NodeType &rEdgeNodeA, NodeType &rEdgeNodeB, NodeType &rEdgeNodeC, array_1d<double, 3> &rDirection)
  {
    array_1d<double, 3> MidPoint = (1.0 / 3.0) * (rEdgeNodeA.Coordinates() + rEdgeNodeB.Coordinates() + rEdgeNodeC.Coordinates());

    GetDirectionToFace(rDirection, rEdgeNodeA, rEdgeNodeB, rEdgeNodeC);

    double distance = fabs(inner_prod((MidPoint - rNode.Coordinates()), rDirection));

    return distance;
  }

  //**************************************************************************
  //**************************************************************************

  static inline void GetDirectionToEdge(array_1d<double, 3> &rDirection, NodeType &rEdgeNodeA, NodeType &rEdgeNodeB)
  {
    //get wall direction from normals:
    if (rEdgeNodeA.FastGetSolutionStepValue(SHRINK_FACTOR) == 1 && rEdgeNodeB.FastGetSolutionStepValue(SHRINK_FACTOR) == 1)
    {
      rDirection = 0.5 * (rEdgeNodeA.FastGetSolutionStepValue(NORMAL) + rEdgeNodeB.FastGetSolutionStepValue(NORMAL));
    }
    else
    {
      if (rEdgeNodeA.FastGetSolutionStepValue(SHRINK_FACTOR) == 1)
      {
        rDirection = rEdgeNodeA.FastGetSolutionStepValue(NORMAL);
      }
      else if (rEdgeNodeB.FastGetSolutionStepValue(SHRINK_FACTOR) == 1)
      {
        rDirection = rEdgeNodeB.FastGetSolutionStepValue(NORMAL);
      }
      else
      {
        rDirection = 0.5 * (rEdgeNodeA.FastGetSolutionStepValue(NORMAL) + rEdgeNodeB.FastGetSolutionStepValue(NORMAL));
      }
    }

    if (norm_2(rDirection))
      rDirection /= norm_2(rDirection);
  }

  //**************************************************************************
  //**************************************************************************

  static inline  void GetDirectionToFace(array_1d<double, 3> &rDirection, NodeType &rEdgeNodeA, NodeType &rEdgeNodeB, NodeType &rEdgeNodeC)
  {

    array_1d<double, 3> VectorB = rEdgeNodeB.Coordinates() - rEdgeNodeA.Coordinates();
    array_1d<double, 3> VectorC = rEdgeNodeC.Coordinates() - rEdgeNodeA.Coordinates();

    MathUtils<double>::CrossProduct(rDirection, VectorB, VectorC);

    if (norm_2(rDirection))
      rDirection /= norm_2(rDirection);
  }


  //*******************************************************************************************
  //*******************************************************************************************

  template<class TVectorType>
      static inline double normalize(TVectorType& rV)
  {
    double norm = norm_2(rV);
    if (norm != 0)
      rV /= norm;
    return norm;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckCoplanar(const NodeType &rNodeA, const NodeType &rNodeB, bool &candidate)
  {

    const array_1d<double, 3> &NormalA = rNodeA.FastGetSolutionStepValue(NORMAL);
    const array_1d<double, 3> &NormalB = rNodeB.FastGetSolutionStepValue(NORMAL);

    const double projection = fabs(1.0 - inner_prod(NormalA, NormalB));

    candidate = false;
    if (projection < 1e-5)
      return true;
    else
    {
      if (projection < 0.75)
        candidate = true;
      return false;
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckCurvature(const GeometryType &rGeometry, const double min = 0.0, const double max = 0.99)
  {
    unsigned int size = rGeometry.size();
    std::vector<array_1d<double, 3>> Normals(size);

    for (unsigned int i = 0; i < size; ++i)
    {
      const array_1d<double, 3> &Normal = rGeometry[i].FastGetSolutionStepValue(NORMAL);
      Normals[i] = -Normal;
    }

    return CheckCurvature(Normals, min, max);
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckCurvature(const std::vector<array_1d<double, 3>> &rNormals, const double min = 0.0, const double max = 0.99)
  {
    bool curvature = true;
    unsigned int size = rNormals.size();
    unsigned int counter = 0;
    double modulus = 0;
    if(size>2){
      for (unsigned int i = 1; i < size; ++i)
      {
        modulus = inner_prod(rNormals[i - 1], rNormals[i]);
        if (modulus > min && modulus < max)
          ++counter;
      }
    }
    modulus = inner_prod(rNormals[0], rNormals[size - 1]);
    if (modulus > min && modulus < max)
      ++counter;

    if (counter < size-1)
      curvature = false;

    return curvature;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckContactCurvature(const GeometryType &rGeometry, std::vector<array_1d<double, 3>> &rNormals)
  {
    unsigned int size = rGeometry.size();

    array_1d<double, 3> Normal;
    noalias(Normal) = ZeroVector(3);
    rNormals.resize(size);
    std::fill(rNormals.begin(), rNormals.end(), Normal);

    double modulus = 1.0;
    bool contact_normal = false;
    for (unsigned int i = 0; i < size; ++i)
    {
      const array_1d<double, 3> &Normal = rGeometry[i].FastGetSolutionStepValue(NORMAL);
      rNormals[i] = -Normal;
      if (rGeometry[i].SolutionStepsDataHas(CONTACT_NORMAL))
      {
        const array_1d<double, 3> &ContactNormal = rGeometry[i].FastGetSolutionStepValue(CONTACT_NORMAL);
        modulus = norm_2(ContactNormal);
        if (modulus!=0){
          rNormals[i] = (1.0 / modulus) * ContactNormal;
          contact_normal = true;
        }
      }
    }

    // if no CONTACT_NORMAL is assigned in some condition nodes
    // the curvature can be evaluated using normals
    bool curvature = CheckCurvature(rNormals);

    //double check now with boundary normals
    //avoid artificial pointing contacts to plane surfaces
    //check curvature in semi-contact elements
    if (curvature && contact_normal)
      curvature = CheckCurvature(rGeometry, 0.0, 0.99);

    //std::cout<<" curvature "<<curvature<<" Contact Normals "<<rNormals<<std::endl;

    return curvature;

  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckRigidOuterCentre(const GeometryType &rGeometry)
  {
    unsigned int rigid_nodes = 0;
    const unsigned int size = rGeometry.size();

    for (auto &i_node : rGeometry)
      if (i_node.Is(RIGID))
        rigid_nodes += 1;


    if (rigid_nodes >= size - 1)
    {
      //calculate baricenter
      std::vector<VectorType> Vertices;

      VectorType Center, Vertex, Corner;
      noalias(Center) = ZeroVector(3);

      for (auto &i_node : rGeometry)
      {
        Vertex = i_node.Coordinates();
        Vertices.push_back(Vertex);
        Center += Vertex;
      }

      Center /= (double)size;

      static constexpr double tolerance = 0.15; //0.01745; //0.05;

      int numouter = 0;
      int numnodes = 0;

      for (unsigned int i = 0; i < size; ++i)
      {
        if (rGeometry[i].Is(RIGID))
        {
          //normal is a unit vector
          const VectorType &Normal = rGeometry[i].FastGetSolutionStepValue(NORMAL);

          //change position to be the vector from the vertex to the geometry center
          Corner = Center - Vertices[i];

          normalize(Corner);

          double projection = inner_prod(Corner, Normal);

          if (projection > tolerance)
            ++numouter;
          ++numnodes;
        }
      }

      if (rigid_nodes == size)
      {
        if (numouter > 0)
          return true;
      }
      else if (rigid_nodes == size - 1)
      {
        if (numouter == numnodes)
          return true;
      }
    }

    return false; //if is outside the body

  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckWallNeighbours(const GeometryType &rGeometry)
  {
    for (auto &i_node : rGeometry)
    {
      bool found = false;
      if (i_node.Is(STRUCTURE)){
        for (auto &j_node : rGeometry)
        {
          if (i_node.Id() != j_node.Id()){
            if (j_node.Is(STRUCTURE)){
              ElementWeakPtrVectorType &nElements = i_node.GetValue(NEIGHBOUR_ELEMENTS);
              for (auto &i_elem : nElements){
                GeometryType& eGeometry = i_elem.GetGeometry();
                for (auto &i_nnode : eGeometry)
                {
                  if (i_nnode.Id() == j_node.Id())
                    found = true;
                }
              }
            }
          }
        }
        if (!found)
          return false;
      }
    }
    return false;
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckWallFaceOuterCentre(const GeometryType &rGeometry)
  {
    //calculate baricenter
    VectorType Center, Normal, Direction;
    noalias(Center) = ZeroVector(3);
    for (auto &i_node : rGeometry)
      Center += i_node.Coordinates();
    Center /= (double)rGeometry.size();

    bool found_face = false;
    //calculate baricenter-face projection
    for (auto &i_node : rGeometry)
    {
      if (i_node.Is({STRUCTURE,RIGID,SOLID.AsFalse()})){
        ElementWeakPtrVectorType &nElements = i_node.GetValue(NEIGHBOUR_ELEMENTS);
        for (auto &i_elem : nElements){
          GeometryType& eGeometry = i_elem.GetGeometry();
          if(eGeometry.size()<rGeometry.size()){
            int face = 0;
            for (auto &j_node : eGeometry)
            {
              for (auto &k_node : rGeometry)
              {
                if (j_node.Id()==k_node.Id())
                  ++face;
              }
            }
            if(face == eGeometry.size()){
              found_face = true;
              GetFaceNormal(eGeometry, Normal); // I must set outside normal normal to face elements !
              Direction = eGeometry.Center() - Center;
              //std::cout<<" R Normal "<<Normal<<" Direction "<<Direction<<std::endl;
              if(inner_prod(Direction,Normal)<0)
                return true; //is outside the body
            }
          }
        }
      }
    }
    //two loops to check fisrt the RIGID boundaries
    for (auto &i_node : rGeometry)
    {
      if (i_node.Is({STRUCTURE,SOLID,RIGID.AsFalse()})){
        ConditionWeakPtrVectorType &nConditions = i_node.GetValue(NEIGHBOUR_CONDITIONS);
        for (auto &i_cond : nConditions){
          GeometryType& cGeometry = i_cond.GetGeometry();
          if(cGeometry.size()<rGeometry.size()){
            bool solid_condition = true;
            for (auto &j_node : cGeometry){
              if(j_node.IsNot(SOLID)){
                solid_condition = false;
                break;
              }
            }
            int face = 0;
            for (auto &j_node : cGeometry){
              for (auto &k_node : rGeometry)
              {
                if (j_node.Id()==k_node.Id())
                  ++face;
              }
            }
            if(solid_condition && face == cGeometry.size()){
              found_face = true;
              //GetFaceNormal(cGeometry, Normal); //it gives solid outside boundary direction
              Normal = cGeometry.GetValue(NORMAL); //it gives the fluid outside boundary direction // equivalent to wall normals
              Direction = cGeometry.Center() - Center;
              //std::cout<<" S Normal "<<Normal<<" Direction "<<Direction<<" C normal "<<cGeometry.GetValue(NORMAL)<<std::endl;
              if(inner_prod(Direction,Normal)<0)
                return true; //is outside the body
            }
          }
        }
      }
    }

    if (!found_face)
      return CheckWallOuterCentre(rGeometry);
    else
      return false; //if is inside the body domain returns false
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckWallOuterCentre(const GeometryType &rGeometry)
  {
    //calculate baricenter-face projection //temporary solution ....
    // for (auto &i_node : rGeometry)
    //   if (i_node.Is(SOLID) && i_node.Is(RIGID))
    //     return !CheckInnerCentre(rGeometry);

    //calculate baricenter
    VectorType Center, Normal, Direction;
    noalias(Center) = ZeroVector(3);
    for (auto &i_node : rGeometry)
      Center += i_node.Coordinates();
    Center /= (double)rGeometry.size();

    //calculate baricenter-face projection
    for (auto &i_node : rGeometry)
    {
      if (i_node.Is({STRUCTURE,RIGID,SOLID.AsFalse()})){
        ElementWeakPtrVectorType &nElements = i_node.GetValue(NEIGHBOUR_ELEMENTS);
        for (auto &i_elem : nElements){
          GeometryType& eGeometry = i_elem.GetGeometry();
          if(eGeometry.size()<rGeometry.size()){
            GetFaceNormal(eGeometry, Normal); // I must set outside normal normal to face elements !
            Direction = eGeometry.Center() - Center;
            //std::cout<<" R Normal "<<Normal<<" Direction "<<Direction<<std::endl;
            if(inner_prod(Direction,Normal)<0)
              return true; //is outside the body
          }
        }
      }
    }
    //two loops to check fisrt the RIGID boundaries
    for (auto &i_node : rGeometry)
    {
      if (i_node.Is({STRUCTURE,SOLID,RIGID.AsFalse()})){
        ConditionWeakPtrVectorType &nConditions = i_node.GetValue(NEIGHBOUR_CONDITIONS);
        for (auto &i_cond : nConditions){
          GeometryType& cGeometry = i_cond.GetGeometry();
          bool solid_condition = true;
          for (auto &i_node : cGeometry){
            if(i_node.IsNot(SOLID)){
              solid_condition = false;
              break;
            }
          }
          if(solid_condition){
            //GetFaceNormal(cGeometry, Normal); //it gives solid outside boundary direction
            Normal = cGeometry.GetValue(NORMAL); //it gives the fluid outside boundary direction // equivalent to wall normals
            Direction = cGeometry.Center() - Center;
            //std::cout<<" S Normal "<<Normal<<" Direction "<<Direction<<" C normal "<<cGeometry.GetValue(NORMAL)<<std::endl;
            if(inner_prod(Direction,Normal)<0)
              return true; //is outside the body
          }
        }
      }
    }


    return false; //if is inside the body domain returns false
  }

  //*******************************************************************************************
  //*******************************************************************************************

  // the geometry winding must be supplied correctly
  static inline void GetFaceNormal(const GeometryType &rGeometry, VectorType& rNormal)
  {
    if (rGeometry.size() == 2){
      rNormal[0] = -(rGeometry[1].Y() - rGeometry[0].Y());
      rNormal[1] = rGeometry[1].X() - rGeometry[0].X();
      rNormal[2] = 0.00;
    }
    else if (rGeometry.size() == 3)
    {
      VectorType Normal, v1, v2;
      v1[0] = rGeometry[1].X() - rGeometry[0].X();
      v1[1] = rGeometry[1].Y() - rGeometry[0].Y();
      v1[2] = rGeometry[1].Z() - rGeometry[0].Z();

      v2[0] = rGeometry[2].X() - rGeometry[0].X();
      v2[1] = rGeometry[2].Y() - rGeometry[0].Y();
      v2[2] = rGeometry[2].Z() - rGeometry[0].Z();

      MathUtils<double>::CrossProduct(rNormal, v1, v2);
      //std::cout<<" v1 "<<v1<<" v2 "<<v2<<" n "<<rNormal<<std::endl;
    }

    double norm = norm_2(rNormal);
    if(norm!=0)
      rNormal /= norm;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckInnerCentre(const GeometryType &rGeometry)
  {
    unsigned int boundary_nodes = 0;
    const unsigned int size = rGeometry.size();

    for (auto &i_node : rGeometry)
      if (i_node.Is(BOUNDARY))
        boundary_nodes += 1;

    if (boundary_nodes == size)
    {
      //calculate baricenter
      std::vector<VectorType> Vertices;

      VectorType Center, Vertex, Corner;
      noalias(Center) = ZeroVector(3);

      for (auto &i_node : rGeometry)
      {
        Vertex = i_node.Coordinates();
        Vertices.push_back(Vertex);
        Center += Vertex;
      }

      Center /= (double)size;

      static constexpr double tolerance = 0.05; //0.01745; //0.05;

      unsigned int outer = 0;
      for (unsigned int i = 0; i < size; ++i)
      {
        //normal is a unit vector
        const VectorType &Normal = rGeometry[i].FastGetSolutionStepValue(NORMAL);

        //change position to be the vector from the vertex to the geometry center
        Corner = Center - Vertices[i];

        normalize(Corner);

        double projection = inner_prod(Corner, Normal);

        if (projection > tolerance)
          outer++;
      }

      // is not inner if all normals point outside (2D wall normals sometimes undefined)
      if (outer==size)
        return false;
    }

    return true; //if is inside the body domain returns true

  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckOuterCentre(const GeometryType &rGeometry, const double &rOffsetFactor, const bool SelfContact)
  {
    unsigned int boundary_nodes = 0;
    const unsigned int size = rGeometry.size();

    for (auto &i_node : rGeometry)
      if (i_node.Is(BOUNDARY))
        boundary_nodes += 1;

    bool outer = false;

    if (boundary_nodes == size)
    {
      //calculate baricenter
      std::vector<VectorType> Vertices;

      VectorType Center, Vertex, Corner;
      noalias(Center) = ZeroVector(3);

      for (auto &i_node : rGeometry)
      {
        const VectorType &Normal = i_node.FastGetSolutionStepValue(NORMAL);
        const double& Shrink = i_node.FastGetSolutionStepValue(SHRINK_FACTOR);

        Vertex = Normal;
        Vertex *= -Shrink * rOffsetFactor;
        Vertex += i_node.Coordinates();

        Vertices.push_back(Vertex);
        Center += Vertex;
      }

      Center /= (double)size;

      static constexpr double ortho = 0.15;
      static constexpr double slope = 0.25; //error assumed for some elements in the corners < 45 degrees
      static constexpr double extra = 0.95;

      int numouter = 0;
      int numextra = 0;
      int numcoplanar = 0;
      int numsamedirection = 0;
      int numorthogonal = 0;

      VectorType Coplanar = rGeometry[0].FastGetSolutionStepValue(NORMAL);
      normalize(Coplanar);

      for (unsigned int i = 0; i < size; ++i)
      {
        const VectorType &Normal = rGeometry[i].FastGetSolutionStepValue(NORMAL);

        //change position to be the vector from the vertex to the geometry center
        Corner = Center - Vertices[i];

        normalize(Corner);

        double projection = inner_prod(Corner, Normal);

        if (projection > 0)
        {
          if (projection > slope)
          {
            numouter++;
          }
          else
          {
            if (projection < extra)
              numextra++;
          }
        }

        double coplanar = inner_prod(Coplanar, Normal);

        if (coplanar > 0)
        {
          numsamedirection++;
        }

        if (coplanar > extra)
        {
          numcoplanar++;
        }

        if (fabs(coplanar) <= ortho)
        {
          numorthogonal++;
        }
      }

      int num = (int)size;

      if (numouter == num)
        outer = true;

      if (numouter == (num - 1) && numextra == 1)
        outer = true;

      if (numouter > 0 && (numextra > 0 && numorthogonal > 0))// && !SelfContact)
      {
        outer = true;
        //std::cout << "   Element with " << num << " corners accepted : case1 " << std::endl;
      }

      if (numouter == 0 && (numextra > (num - 2) && numorthogonal > 0)) // && !SelfContact)
      {
        outer = true;
        std::cout << "   Element with " << num << " corners accepted : case2 " << std::endl;
      }

      if (numcoplanar == num)
        outer = false;

      if (numsamedirection == num && numorthogonal == 0)
        outer = false;

      // if(numorthogonal>=1)
      //   outer=false;

    }

    return outer; //if is outside the body domain returns true
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckContactSliver(const GeometryType &rGeometry)
  {
    std::vector<Vector> FaceNormals;
    GetUnitFaceNormals(rGeometry,FaceNormals);

    //std::cout<<" UnitFace Normals "<<FaceNormals.size()<<std::endl;

    //check coincident normals
    std::vector<int> FaceCoincidentNormals(rGeometry.FacesNumber());
    std::fill(FaceCoincidentNormals.begin(), FaceCoincidentNormals.end(), 0);

    unsigned int NonZeroNormals = 0;
    unsigned int i_normal = 0;
    for (unsigned int i = 0; i < FaceNormals.size(); ++i)
    {
      if (norm_2(FaceNormals[i]) > 1e-19){
        NonZeroNormals++;
        i_normal = i;
      }
      for (unsigned int j = i + 1; j < FaceNormals.size(); ++j)
      {
        double projection = inner_prod(FaceNormals[i], FaceNormals[j]);
        std::cout<<" projection "<<projection<<std::endl;
        if (fabs(projection) >= 0.99)
        {
          FaceCoincidentNormals[i] += 1;
          FaceCoincidentNormals[j] += 1;
        }
      }
    }

    unsigned int CoincidentNormals = 0;
    for (unsigned int i = 0; i < FaceNormals.size(); ++i)
      CoincidentNormals += FaceCoincidentNormals[i];

    //CoincidentNormals: 12 => all-faces coincident
    //CoincidentNormals:  6 => 3-faces coincident
    //CoincidentNormals:  4 => 2-faces/2-faces coincident
    //CoincidentNormals:  2 => 2-faces coincident

    if (CoincidentNormals>=(NonZeroNormals*(NonZeroNormals-1)))
    {
      //check faces normal vs vertex normals
      unsigned int non_parallel = 0;
      unsigned int orthogonal = 0;

      for(auto &i_node : rGeometry)
      {
        double projection = fabs(inner_prod(FaceNormals[i_normal], i_node.FastGetSolutionStepValue(NORMAL)));
        if (projection < 0.9)
          non_parallel++;
        if (projection < 0.08)
          orthogonal++;
      }

      //if ((non_parallel>=4 && NonZeroNormals == FaceNormals.size()) || orthogonal>0){  //contact sliver on edges
      if (orthogonal>0){  //contact sliver on edges
        std::cout<<" non-parallel "<<non_parallel<<" orthogonal "<<orthogonal<<" "<<FaceNormals<<std::endl;
        return false;
      }
      else
        return true;
    }
    else{
      std::cout<<" Coincident normals "<<CoincidentNormals<<"  "<<FaceNormals<<std::endl;
      return false;
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckSharedFace(const GeometryType &rGeometry, unsigned int number = 1)
  {
   unsigned int size = rGeometry.size();
    std::vector<unsigned int> permuta;
    if (size == 4)
      permuta = {0, 1, 2, 3, 0, 1};
    else if (size == 3)
      permuta = {0, 1, 2, 0, 1};
    else
      KRATOS_ERROR << " element with not supported geometry vertices :"<< size <<std::endl;
    unsigned int j = 0;
    unsigned int faces = 0;
    for (auto &i_node : rGeometry) // loop on the 4 faces
    {
      ConditionWeakPtrVectorType &nConditions = i_node.GetValue(NEIGHBOUR_CONDITIONS);
      std::vector<int> v1;
      for (unsigned int i=0; i<size-1; ++i)
        v1.push_back(rGeometry[permuta[j+i]].Id());

      for (auto &i_cond : nConditions)
      {
        std::vector<int> v2;
        for (auto &j_node : i_cond.GetGeometry())
          v2.push_back(j_node.Id());

        if (std::is_permutation(v1.begin(), v1.end(), v2.begin())){
          ++faces;
          break;
        }
      }
      if (faces>=number)
        return true;
      ++j;
    }
    return false;
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckSliver(const GeometryType &rGeometry)
  {
    Vector VectorZero(3);
    noalias(VectorZero) = ZeroVector(3);

    std::vector<Vector> FaceNormals(rGeometry.FacesNumber());
    std::fill(FaceNormals.begin(), FaceNormals.end(), VectorZero);

    std::vector<double> FaceAreas(rGeometry.FacesNumber());
    std::fill(FaceAreas.begin(), FaceAreas.end(), 0.0);

    double MaximumFaceArea = std::numeric_limits<double>::min();
    double MinimumFaceArea = std::numeric_limits<double>::max();

    DenseMatrix<unsigned int> lpofa; //points that define the faces
    rGeometry.NodesInFaces(lpofa);
    DenseVector<unsigned int> lnofa; //number of nodes per face (3)
    rGeometry.NumberNodesInFaces(lnofa);

    //calculate face normals
    Vector FirstVectorPlane(3);
    noalias(FirstVectorPlane) = VectorZero;
    Vector SecondVectorPlane(3);
    noalias(SecondVectorPlane) = VectorZero;

    double FaceArea = 0;
    for (unsigned int i = 0; i < rGeometry.FacesNumber(); ++i)
    {
      std::vector<Vector> FaceCoordinates(lnofa[i]); // (3)
      std::fill(FaceCoordinates.begin(), FaceCoordinates.end(), VectorZero);
      for (unsigned int j = 0; j < lnofa[i]; ++j)
      {
        noalias(FaceCoordinates[j]) = rGeometry[lpofa(j + 1, i)].Coordinates();
      }

      if (lnofa[i] > 2)
      {
        noalias(FirstVectorPlane) = FaceCoordinates[1] - FaceCoordinates.front();
        noalias(SecondVectorPlane) = FaceCoordinates.back() - FaceCoordinates.front();
      }
      else
      {
        KRATOS_ERROR << "2D check sliver not implemented" << std::endl;
      }

      noalias(FaceNormals[i]) = MathUtils<double>::CrossProduct(FirstVectorPlane, SecondVectorPlane);

      FaceArea = normalize(FaceNormals[i]);

      if (FaceArea < MinimumFaceArea)
        MinimumFaceArea = FaceArea;
      if (FaceArea > MaximumFaceArea)
        MaximumFaceArea = FaceArea;
    }

    //check areas
    if (MaximumFaceArea >= MinimumFaceArea * 1.0e2)
      return true;

    //check coincident normals
    std::vector<int> FaceCoincidentNormals(rGeometry.FacesNumber());
    std::fill(FaceCoincidentNormals.begin(), FaceCoincidentNormals.end(), 0);

    unsigned int CoincidentNormals = 0;
    for (unsigned int i = 0; i < lpofa.size2(); ++i)
    {
      for (unsigned int j = i + 1; j < lpofa.size2(); ++j)
      {
        double projection = inner_prod(FaceNormals[i], FaceNormals[j]);
        if (fabs(projection) >= 0.99)
        {
          FaceCoincidentNormals[i] += 1;
          FaceCoincidentNormals[j] += 1;
        }
      }

      CoincidentNormals += FaceCoincidentNormals[i];
    }

    //CoincidentNormals: 12 => all-faces coincident
    //CoincidentNormals:  6 => 3-faces coincident
    //CoincidentNormals:  4 => 2-faces/2-faces coincident
    //CoincidentNormals:  2 => 2-faces coincident

    const unsigned int size = rGeometry.size();
    unsigned int NumberOfBoundaryNodes = 0;
    for (unsigned int i = 0; i < size; ++i)
      if (rGeometry[i].Is(BOUNDARY))
        NumberOfBoundaryNodes += 1;

    // for(unsigned int i=0; i<FaceNormals.size(); ++i)
    //     std::cout<<"FaceNormal ["<<i<<"] "<<FaceNormals[i]<<std::endl;


    if (NumberOfBoundaryNodes == size)
    { //boundary elements
      if (CoincidentNormals >= 4)
      {
        return true;
      }
    }
    else if (NumberOfBoundaryNodes >= 2)
    {
      if (CoincidentNormals >= 6)
      {
        return true;
      }
    }
    else
    { //inside elements
      if (CoincidentNormals >= 12)
      {
        return true;
      }
    }

    return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckBlocked(const GeometryType &rGeometry, const unsigned int &boundary_faces)
  {

    if (rGeometry.WorkingSpaceDimension() == 3 && rGeometry.LocalSpaceDimension() == 3)
    {
      if (!SetFlagsUtilities::CheckAllNodesFlag(rGeometry,{FREE_SURFACE}))
        return false;

      //a1 : slope x for plane on the first triangular face of the tetrahedra (nodes A,B,C)
      //b1 : slope y for plane on the first triangular face of the tetrahedra (nodes A,B,C)
      //c1 : slope z for plane on the first triangular face of the tetrahedra (nodes A,B,C)
      double a1 = (rGeometry[1].Coordinates()[1] - rGeometry[0].Coordinates()[1]) * (rGeometry[2].Coordinates()[2] - rGeometry[0].Coordinates()[2]) - (rGeometry[2].Coordinates()[1] - rGeometry[0].Coordinates()[1]) * (rGeometry[1].Coordinates()[2] - rGeometry[0].Coordinates()[2]);
      double b1 = (rGeometry[1].Coordinates()[2] - rGeometry[0].Coordinates()[2]) * (rGeometry[2].Coordinates()[0] - rGeometry[0].Coordinates()[0]) - (rGeometry[2].Coordinates()[2] - rGeometry[0].Coordinates()[2]) * (rGeometry[1].Coordinates()[0] - rGeometry[0].Coordinates()[0]);
      double c1 = (rGeometry[1].Coordinates()[0] - rGeometry[0].Coordinates()[0]) * (rGeometry[2].Coordinates()[1] - rGeometry[0].Coordinates()[1]) - (rGeometry[2].Coordinates()[0] - rGeometry[0].Coordinates()[0]) * (rGeometry[1].Coordinates()[1] - rGeometry[0].Coordinates()[1]);

      //a2 : slope x for plane on the second triangular face of the tetrahedra (nodes A,B,D)
      //b2 : slope y for plane on the second triangular face of the tetrahedra (nodes A,B,D)
      //c2 : slope z for plane on the second triangular face of the tetrahedra (nodes A,B,D)
      double a2 = (rGeometry[1].Coordinates()[1] - rGeometry[0].Coordinates()[1]) * (rGeometry[3].Coordinates()[2] - rGeometry[0].Coordinates()[2]) - (rGeometry[3].Coordinates()[1] - rGeometry[0].Coordinates()[1]) * (rGeometry[1].Coordinates()[2] - rGeometry[0].Coordinates()[2]);
      double b2 = (rGeometry[1].Coordinates()[2] - rGeometry[0].Coordinates()[2]) * (rGeometry[3].Coordinates()[0] - rGeometry[0].Coordinates()[0]) - (rGeometry[3].Coordinates()[2] - rGeometry[0].Coordinates()[2]) * (rGeometry[1].Coordinates()[0] - rGeometry[0].Coordinates()[0]);
      double c2 = (rGeometry[1].Coordinates()[0] - rGeometry[0].Coordinates()[0]) * (rGeometry[3].Coordinates()[1] - rGeometry[0].Coordinates()[1]) - (rGeometry[3].Coordinates()[0] - rGeometry[0].Coordinates()[0]) * (rGeometry[1].Coordinates()[1] - rGeometry[0].Coordinates()[1]);

      //a3 : slope x for plane on the third triangular face of the tetrahedra (nodes B,C,D)
      //b3 : slope y for plane on the third triangular face of the tetrahedra (nodes B,C,D)
      //c3 : slope z for plane on the third triangular face of the tetrahedra (nodes B,C,D)
      double a3 = (rGeometry[1].Coordinates()[1] - rGeometry[2].Coordinates()[1]) * (rGeometry[3].Coordinates()[2] - rGeometry[2].Coordinates()[2]) - (rGeometry[3].Coordinates()[1] - rGeometry[2].Coordinates()[1]) * (rGeometry[1].Coordinates()[2] - rGeometry[2].Coordinates()[2]);
      double b3 = (rGeometry[1].Coordinates()[2] - rGeometry[2].Coordinates()[2]) * (rGeometry[3].Coordinates()[0] - rGeometry[2].Coordinates()[0]) - (rGeometry[3].Coordinates()[2] - rGeometry[2].Coordinates()[2]) * (rGeometry[1].Coordinates()[0] - rGeometry[2].Coordinates()[0]);
      double c3 = (rGeometry[1].Coordinates()[0] - rGeometry[2].Coordinates()[0]) * (rGeometry[3].Coordinates()[1] - rGeometry[2].Coordinates()[1]) - (rGeometry[3].Coordinates()[0] - rGeometry[2].Coordinates()[0]) * (rGeometry[1].Coordinates()[1] - rGeometry[2].Coordinates()[1]);

      //a4 : slope x for plane on the fourth triangular face of the tetrahedra (nodes A,C,D)
      //b4 : slope y for plane on the fourth triangular face of the tetrahedra (nodes A,C,D)
      //c4 : slope z for plane on the fourth triangular face of the tetrahedra (nodes A,C,D)
      double a4 = (rGeometry[0].Coordinates()[1] - rGeometry[2].Coordinates()[1]) * (rGeometry[3].Coordinates()[2] - rGeometry[2].Coordinates()[2]) - (rGeometry[3].Coordinates()[1] - rGeometry[2].Coordinates()[1]) * (rGeometry[0].Coordinates()[2] - rGeometry[2].Coordinates()[2]);
      double b4 = (rGeometry[0].Coordinates()[2] - rGeometry[2].Coordinates()[2]) * (rGeometry[3].Coordinates()[0] - rGeometry[2].Coordinates()[0]) - (rGeometry[3].Coordinates()[2] - rGeometry[2].Coordinates()[2]) * (rGeometry[0].Coordinates()[0] - rGeometry[2].Coordinates()[0]);
      double c4 = (rGeometry[0].Coordinates()[0] - rGeometry[2].Coordinates()[0]) * (rGeometry[3].Coordinates()[1] - rGeometry[2].Coordinates()[1]) - (rGeometry[3].Coordinates()[0] - rGeometry[2].Coordinates()[0]) * (rGeometry[0].Coordinates()[1] - rGeometry[2].Coordinates()[1]);

      double cosAngle12 = (a1 * a2 + b1 * b2 + c1 * c2) / (sqrt(pow(a1, 2) + pow(b1, 2) + pow(c1, 2)) * sqrt(pow(a2, 2) + pow(b2, 2) + pow(c2, 2)));
      double cosAngle13 = (a1 * a3 + b1 * b3 + c1 * c3) / (sqrt(pow(a1, 2) + pow(b1, 2) + pow(c1, 2)) * sqrt(pow(a3, 2) + pow(b3, 2) + pow(c3, 2)));
      double cosAngle14 = (a1 * a4 + b1 * b4 + c1 * c4) / (sqrt(pow(a1, 2) + pow(b1, 2) + pow(c1, 2)) * sqrt(pow(a4, 2) + pow(b4, 2) + pow(c4, 2)));
      double cosAngle23 = (a3 * a2 + b3 * b2 + c3 * c2) / (sqrt(pow(a3, 2) + pow(b3, 2) + pow(c3, 2)) * sqrt(pow(a2, 2) + pow(b2, 2) + pow(c2, 2)));
      double cosAngle24 = (a4 * a2 + b4 * b2 + c4 * c2) / (sqrt(pow(a4, 2) + pow(b4, 2) + pow(c4, 2)) * sqrt(pow(a2, 2) + pow(b2, 2) + pow(c2, 2)));
      double cosAngle34 = (a4 * a3 + b4 * b3 + c4 * c3) / (sqrt(pow(a4, 2) + pow(b4, 2) + pow(c4, 2)) * sqrt(pow(a3, 2) + pow(b3, 2) + pow(c3, 2)));

      if ((fabs(cosAngle12) > 0.99 || fabs(cosAngle13) > 0.99 || fabs(cosAngle14) > 0.99 || fabs(cosAngle23) > 0.99 || fabs(cosAngle24) > 0.99 || fabs(cosAngle34) > 0.99) &&  boundary_faces > 1)
      {
        return true;
      }
      else if ((fabs(cosAngle12) > 0.995 || fabs(cosAngle13) > 0.995 || fabs(cosAngle14) > 0.995 || fabs(cosAngle23) > 0.995 || fabs(cosAngle24) > 0.995 || fabs(cosAngle34) > 0.995) && boundary_faces == 1)
      {
        return true;
      }
      else if ((fabs(cosAngle12) > 0.999 || fabs(cosAngle13) > 0.999 || fabs(cosAngle14) > 0.999 || fabs(cosAngle23) > 0.999 || fabs(cosAngle24) > 0.999 || fabs(cosAngle34) > 0.999))
      {
        return true;
      }
    }

    return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool GetGravityDirection(const GeometryType &rGeometry, array_1d<double,3> &rGravity)
  {
    for (auto &i_node : rGeometry)
    {
      if(i_node.SolutionStepsDataHas(VOLUME_ACCELERATION))
      {
        noalias(rGravity) = i_node.FastGetSolutionStepValue(VOLUME_ACCELERATION);
        double norm_gravity = norm_2(rGravity);
        if (norm_gravity != 0){
          rGravity /= norm_gravity;
          return true;
        }
      }

      if (i_node.SolutionStepsDataHas(BODY_FORCE))
      {
        noalias(rGravity) = i_node.FastGetSolutionStepValue(BODY_FORCE);
        double norm_gravity = norm_2(rGravity);
        if (norm_gravity != 0)
        {
          rGravity /= norm_gravity;
          return true;
        }
      }
    }

    return false;
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline unsigned int GetFaceNormalsAndCenters(NodeType &rNode, std::vector<array_1d<double,3>> &rNormals, std::vector<array_1d<double,3>> &rCenters, bool recurrence = true)
  {
    NodeWeakPtrVectorType &nNodes = rNode.GetValue(NEIGHBOUR_NODES);

    unsigned int counter = 0;
    for (auto &i_nnode : nNodes)
    {
      if (i_nnode.And({FREE_SURFACE,STRUCTURE.AsFalse()})){
        counter = GetCloseFaceNormalsAndCenters(i_nnode, rNormals, rCenters);
        if (counter!=0)
          break;
      }
    }

    if (recurrence){
      if (counter==0){
        for (auto &i_nnode : nNodes)
        {
          if (i_nnode.And({FREE_SURFACE,STRUCTURE.AsFalse()})){
            counter = GetFaceNormalsAndCenters(i_nnode, rNormals, rCenters, false);
            if(counter!= 0){
              break;
            }
          }
        }
      }
      if (counter==0)
        counter = GetCloseFaceNormalsAndCenters(rNode, rNormals, rCenters);
    }

    return counter;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline unsigned int GetCloseFaceNormalsAndCenters(NodeType &rNode, std::vector<array_1d<double,3>> &rNormals, std::vector<array_1d<double,3>> &rCenters)
  {
    ConditionWeakPtrVectorType &nConditions = rNode.GetValue(NEIGHBOUR_CONDITIONS);

    unsigned int counter = 0;
    if(rNode.Is(FREE_SURFACE))
    {
      for (auto &i_cond : nConditions) //characterize first layer faces
      {
        if( i_cond.IsNot(INTERACTION) && i_cond.Is(FREE_SURFACE) ){
          rNormals.push_back(i_cond.GetValue(NORMAL)); //usually the area and in-plane normals must be checked
          rCenters.push_back(i_cond.GetGeometry().Center());
          ++counter;
        }
      }
    }

    return counter;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckFluidMeniscus(const GeometryType &rGeometry, const std::vector<Flags> ReferenceNodeFlags, double slope = 0.4)
  {
    bool checked = false;
    for (auto &i_node : rGeometry)
    {
      if (i_node.Is(ReferenceNodeFlags)) //only free_surface interaction nodes
      {
        // std::cout<<" [MENISCUS]("<<i_node.Id()<<")"<<std::endl;
        //characterize surface
        std::vector<array_1d<double,3>> Normals, Centers;
        unsigned int counter = GetFaceNormalsAndCenters(i_node, Normals, Centers);

        if (counter!=0)
        {
          for (auto &j_node : rGeometry)
          {
            if (i_node.Id() != j_node.Id())
            {
              if (j_node.Is(STRUCTURE) && (j_node.Is(FREE_SURFACE) || j_node.IsNot(FLUID))) //previous meniscus or new meniscus
              {
                checked = true;
                // std::cout<<" [j]("<<j_node.Id()<<")"<<std::endl;
                array_1d<double, 3> Meniscus = j_node.Coordinates() - i_node.Coordinates();
                double norm = norm_2(Meniscus);
                if (norm != 0)
                {
                  Meniscus /= norm;

                  for(unsigned int i =0; i < Normals.size(); ++i)
                  {
                    array_1d<double, 3> Surface = i_node.Coordinates()-Centers[i];
                    array_1d<double, 3> Tangent = j_node.FastGetSolutionStepValue(NORMAL);
                    norm = norm_2(Surface);
                    if (norm!=0)
                      Surface /= norm;
                    double direction = inner_prod(Tangent, Meniscus); //normalized [-1,1]
                    double projection = inner_prod(Tangent, Normals[i]); //normalized [-1,1]

                    if(direction > 0 && std::acos(projection) > 1.48 ) //rad
                    {
                      double meniscus = inner_prod(Meniscus, Normals[i]); //normalized [-1,1]
                      // std::cout<<" i_normal "<<i_normal<<" meniscus "<<Meniscus<<" projection "<<projection<<" slope "<<slope<<std::endl;
                      double wall_slope = slope;

                      //std::cout<<" Normals/Centers ["<<i<<"] "<<Normals[i]<<" "<<Centers[i]<<" p "<<projection<<" direction "<<direction<<std::endl;
                      if (meniscus > wall_slope)
                        return false;

                    }
                  }

                }
              }
            }
          }
        }
        else{
          std::cout<<" [MENISCUS]("<<i_node.Id()<<") with zero NORMAL "<<std::endl;
        }
      }
    }

    // if (!checked)
    //   std::cout<<" [MENISCUS] not CHECKED : accepted "<<std::endl;

    return true;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckGravityFluidMeniscus(const GeometryType &rGeometry, const array_1d<double,3> &rGravity, const std::vector<Flags> ReferenceNodeFlags, double slope = 0.4)
  {
    bool gravity_check = false;
    if (norm_2(rGravity) != 0)
      gravity_check = true;

    for (auto &i_node : rGeometry)
    {
      if (i_node.Is(ReferenceNodeFlags)) //only free_surface interaction nodes
      {
        // std::cout<<" [MENISCUS]("<<i_node.Id()<<")"<<std::endl;
        //characterize surface
        std::vector<array_1d<double,3>> Normals, Centers;
        unsigned int counter = GetFaceNormalsAndCenters(i_node, Normals, Centers);

        if (counter!=0)
        {
          for (auto &j_node : rGeometry)
          {
            if (i_node.Id() != j_node.Id())
            {
              if (j_node.Is(STRUCTURE) && (j_node.Is(FREE_SURFACE) || j_node.IsNot(FLUID))) //previous meniscus or new meniscus
              {
                // std::cout<<" [j]("<<j_node.Id()<<")"<<std::endl;
                array_1d<double, 3> Meniscus = j_node.Coordinates() - i_node.Coordinates();
                double norm = norm_2(Meniscus);
                if (norm != 0)
                {
                  Meniscus /= norm;
                  double direction = -1;
                  if (gravity_check){
                    direction = inner_prod(Meniscus, rGravity);
                  }
                  if(direction < -0.1){

                    for(unsigned int i =0; i < Normals.size(); ++i)
                    {
                      if (gravity_check){
                        array_1d<double, 3> Tangent = i_node.Coordinates()-Centers[i];
                        norm = norm_2(Tangent);
                        if (norm!=0)
                          Tangent /= norm;
                        direction = inner_prod(Tangent, rGravity);
                      }

                      if(direction < 0.25){

                        double projection = inner_prod(Meniscus, Normals[i]);
                        // std::cout<<" i_normal "<<i_normal<<" meniscus "<<Meniscus<<" projection "<<projection<<" slope "<<slope<<std::endl;
                        double wall_slope = slope;

                        if (direction>0)
                          wall_slope += 0.5 * direction; //correction if the free_surface has tilt

                        //std::cout<<" Normals/Centers ["<<i<<"] "<<Normals[i]<<" "<<Centers[i]<<" p "<<projection<<" direction "<<direction<<std::endl;
                        if (projection > wall_slope)
                          return false;

                      }
                    }
                  }
                }
              }
            }
          }
        }
        else{
          std::cout<<" [MENISCUS]("<<i_node.Id()<<") with zero NORMAL  new(0)/old(1): "<<gravity_check<<std::endl;
        }
      }
    }
    return true;
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckNewFluidMeniscus(const GeometryType &rGeometry, const array_1d<double,3> &rGravity, double slope = 0.4)
  {
    std::vector<Flags> ReferenceNodeFlags = {FREE_SURFACE,INTERACTION};
    bool interaction = false;
    for (auto &i_node : rGeometry){
      if (i_node.And({FREE_SURFACE,INTERACTION})){
        interaction =  true;
        break;
      }
    }
    if (!interaction)
      ReferenceNodeFlags = {FREE_SURFACE,STRUCTURE.AsFalse()};

    // bool interaction = false;
    // bool fluid_structure = false;
    // for (auto &i_node : rGeometry){
    //   if (i_node.And({FREE_SURFACE,INTERACTION})) //only free_surface interaction nodes
    //     interaction = true;
    //   if (i_node.Is(STRUCTURE) && i_node.IsNot(FLUID)) //has a wall non fluid node
    //     fluid_structure = true;
    // }
    // if (!interaction && fluid_structure){
    //   ReferenceNodeFlags = {INTERACTION}; //case that the free surface element has disappeared and the menistus is formed with an interior node
    //   std::cout<<" CHECKING A FLuiD MENisCUS WITH AN INTERIOR NODE "<<std::endl;
    // }

    return CheckFluidMeniscus(rGeometry, ReferenceNodeFlags, slope);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckOldFluidMeniscus(const GeometryType &rGeometry, const array_1d<double,3> &rGravity, double slope = 0.4)
  {

    array_1d<double, 3> Gravity;
    if (!GetGravityDirection(rGeometry,Gravity)){
      Gravity = rGravity;
      double norm_gravity = norm_2(Gravity);
      if (norm_gravity != 0)
        Gravity /= norm_gravity;
    }

    std::vector<Flags> ReferenceNodeFlags = {FREE_SURFACE,STRUCTURE.AsFalse()};
    return CheckGravityFluidMeniscus(rGeometry, rGravity, ReferenceNodeFlags, slope);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double GetAndCompareSideLenghts(const GeometryType &rGeometry, double &rMaximumSideLength, double &rMinimumSideLength)
  {
    rMaximumSideLength = std::numeric_limits<double>::min();
    rMinimumSideLength = std::numeric_limits<double>::max();

    DenseMatrix<unsigned int> lpofa;
    rGeometry.NodesInFaces(lpofa);

    double SideLength = 0;
    for (unsigned int i = 0; i < lpofa.size2(); ++i)
    {

      for (unsigned int j = 1; j < lpofa.size1(); ++j)
      {
        SideLength = norm_2(rGeometry[lpofa(0, i)].Coordinates() - rGeometry[lpofa(j, i)].Coordinates());

        if (SideLength < rMinimumSideLength)
          rMinimumSideLength = SideLength;

        if (SideLength > rMaximumSideLength)
          rMaximumSideLength = SideLength;
      }
    }



    return (rMaximumSideLength / rMinimumSideLength);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double CompareFaceAreas(const GeometryType &rGeometry, double &rMaximumFaceArea, double &rMinimumFaceArea)
  {
    std::vector<Vector> FaceNormals;
    GetFaceNormals(rGeometry,FaceNormals);

    rMaximumFaceArea = std::numeric_limits<double>::min();
    rMinimumFaceArea = std::numeric_limits<double>::max();

    double FaceArea = 0;
    for (unsigned int i = 0; i < FaceNormals.size(); ++i)
    {
      FaceArea = norm_2(FaceNormals[i]);

      if (FaceArea < rMinimumFaceArea)
        rMinimumFaceArea = FaceArea;
      if (FaceArea > rMaximumFaceArea)
        rMaximumFaceArea = FaceArea;
    }

    return (rMaximumFaceArea / rMinimumFaceArea);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void GetUnitFaceNormals(const GeometryType &rGeometry, std::vector<Vector> &rFaceNormals)
  {
    GetFaceNormals(rGeometry,rFaceNormals);
    for(auto &normal: rFaceNormals)
      normalize(normal);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void GetFaceNormals(const GeometryType &rGeometry, std::vector<Vector> &rFaceNormals)
  {

    Vector VectorZero(3);
    noalias(VectorZero) = ZeroVector(3);

    if(rFaceNormals.size() != rGeometry.FacesNumber())
      rFaceNormals.resize(rGeometry.FacesNumber());
    std::fill(rFaceNormals.begin(), rFaceNormals.end(), VectorZero);

    DenseMatrix<unsigned int> lpofa; //points that define the faces
    rGeometry.NodesInFaces(lpofa);
    DenseVector<unsigned int> lnofa; //number of nodes per face (3)
    rGeometry.NumberNodesInFaces(lnofa);

    //calculate face normals
    Vector FirstVectorPlane(3);
    noalias(FirstVectorPlane) = VectorZero;
    Vector SecondVectorPlane(3);
    noalias(SecondVectorPlane) = VectorZero;

    for (unsigned int i = 0; i < rGeometry.FacesNumber(); ++i)
    {
      std::vector<Vector> FaceCoordinates(lnofa[i]); // (3)
      std::fill(FaceCoordinates.begin(), FaceCoordinates.end(), VectorZero);
      for (unsigned int j = 0; j < lnofa[i]; ++j)
      {
        noalias(FaceCoordinates[j]) = rGeometry[lpofa(j + 1, i)].Coordinates();
      }

      if (lnofa[i] > 2)
      {
        noalias(FirstVectorPlane) = FaceCoordinates[1] - FaceCoordinates.front();
        noalias(SecondVectorPlane) = FaceCoordinates.back() - FaceCoordinates.front();
      }
      else
      {
        KRATOS_ERROR << "2D check sliver not implemented" << std::endl;
      }

      noalias(rFaceNormals[i]) = MathUtils<double>::CrossProduct(FirstVectorPlane, SecondVectorPlane);
    }

  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double FindBoundaryH(NodeType &BoundaryPoint)
  {
    double havg = 0.00;

    if (BoundaryPoint.GetValue(NEIGHBOUR_NODES).size() != 0)
    {
      double xc = BoundaryPoint.X();
      double yc = BoundaryPoint.Y();
      double zc = BoundaryPoint.Z();

      double h_nodes = 0;
      double h = 1000.0;

      NodeWeakPtrVectorType &nNodes = BoundaryPoint.GetValue(NEIGHBOUR_NODES);
      for (auto &i_nnode : nNodes)
      {
        if (i_nnode.Is(BOUNDARY))
        {
          double x = i_nnode.X();
          double y = i_nnode.Y();
          double z = i_nnode.Z();
          double l = (x - xc) * (x - xc);
          l += (y - yc) * (y - yc);
          l += (z - zc) * (z - zc);

          if (l < h)
            h = l;

          h = sqrt(h);
          havg += h;
          h_nodes += 1;
        }
      }

      havg /= h_nodes;
    }

    return havg;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  //returns false if it should be removed
  static inline bool AlphaShape(const double AlphaParameter, const GeometryType &rGeometry, const unsigned int dimension, const double MeanMeshSize)
  {
    //calculate geometry radius and volume
    double volume = 0;

    std::vector<Vector> Vertices;
    for (auto& i_node : rGeometry)
      Vertices.push_back(i_node.Coordinates());

    double radius = CalculateRadius(Vertices, dimension, volume);

    // double CriticalVolume = 1e-12 * pow(h, size-1);
    double alpha_radius = AlphaParameter * MeanMeshSize;

    if (radius < 0) //degenerated element
      return false;
    else if (radius < alpha_radius)
      return true;

    return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  //returns false if it should be removed
  static inline bool AlphaShape(const double AlphaParameter, const GeometryType &rGeometry, const unsigned int dimension)
  {
    const unsigned int size = rGeometry.size();

    //calculate geometry radius, volume and average h
    double volume = 0;
    double h = 0;

    std::vector<Vector> Vertices;
    for (auto& i_node : rGeometry)
    {
      Vertices.push_back(i_node.Coordinates());
      h += i_node.FastGetSolutionStepValue(NODAL_H);
    }

    h /= (double)size;

    double radius = CalculateRadius(Vertices, dimension, volume);

    double critical_volume = 1e-12 * pow(h, size - 1);
    double alpha_radius = AlphaParameter * h;

    //std::cout<<" ratius "<<radius<<" h "<<h<<" alpha "<<alpha_radius<<" volume "<<volume<<" critical "<<critical_volume<<std::endl;

    if (volume < critical_volume) //sliver
      return false;
    else if (radius < alpha_radius)
      return true;

    return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  //returns false if it should be removed
  static inline bool ShrankAlphaShape(const double AlphaParameter, const GeometryType &rGeometry, double &rOffsetFactor, const unsigned int dimension)
  {
    const unsigned int size = rGeometry.size();

    //calculate geometry radius and volume
    double volume = 0;

    //calculate average h and  average h of boundary face
    double h = 0;
    double h_face = 0;

    std::vector<Vector> Vertices;
    Vector Vertex(3);
    for (auto& i_node : rGeometry)
    {
      // const VectorType &Normal = i_node.FastGetSolutionStepValue(NORMAL);
      // const double &shrink = i_node.FastGetSolutionStepValue(SHRINK_FACTOR);
      // Vertex = i_node.Coordinates() - Normal * shrink * rOffsetFactor;
      //std::cout<<" offset "<<i_node.FastGetSolutionStepValue(OFFSET)<<" OffSet "<<- Normal * shrink * rOffsetFactor<<std::endl;

      Vertex = i_node.Coordinates() + i_node.FastGetSolutionStepValue(OFFSET);

      Vertices.push_back(Vertex);

      h += i_node.FastGetSolutionStepValue(NODAL_H);
      h_face += FindBoundaryH(i_node);
    }

    h /= (double)size;
    h_face /= (double)size;
    if (h_face > h){
      h = h_face;
    }

    double radius = CalculateRadius(Vertices, dimension, volume);

    double critical_volume = 1e-2 * pow(h, size - 1);
    double alpha_radius = dimension * AlphaParameter * h;

    if (volume < critical_volume){ //sliver
      return false;
    }
    else if (radius < alpha_radius){
      return true;
    }

    return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CalculateCircumCenter(const double x0, const double y0,
                                           const double x1, const double y1,
                                           const double x2, const double y2,
                                           double &xc, double &yc)
  {
    double xba, yba, xca, yca;
    double balength, calength;
    double denominator;

    //Use coordinates relative to point a (point 0)
    xba = x1-x0;
    yba = y1-y0;

    xca = x2-x0;
    yca = y2-y0;

    //Squares of lengths of the edges incident to a
    balength = xba * xba + yba * yba;
    calength = xca * xca + yca * yca;

    //Calculate the denominator of the formulae
    denominator = 0.5 / (xba * yca - yba * xca);

    //Calculate offset (from `a') of circumcenter
    xc = x0 + (yca * balength - yba * calength) * denominator;
    yc = y0 + (xba * calength - xca * balength) * denominator;

    // OPTION 1:
    // BoundedMatrix<double, 2, 2> mJ;    //local jacobian
    // BoundedMatrix<double, 2, 2> mJinv; //inverse jacobian

    // //calculation of the jacobian  //coordinate center point 0
    // mJ(0,0) = x1-x0;
    // mJ(0,1) = y1-y0;

    // mJ(1,0) = x2-x0;
    // mJ(1,1) = y2-y0;

    // mJ *= 2.0;

    // //calculation of the determinant (volume/2)
    // double Volume = mJ(0, 0) * mJ(1, 1) - mJ(0, 1) * mJ(1, 0); //detJ

    // //calculation of the inverse of the jacobian
    // mJinv(0, 0) = mJ(1, 1);
    // mJinv(0, 1) = -mJ(0, 1);
    // mJinv(1, 0) = -mJ(1, 0);
    // mJinv(1, 1) = mJ(0, 0);

    // mJinv /= Volume;

    // //calculation of the center
    // Vector Center = ZeroVector(2); //center pos

    // //center point 0
    // Center[0] += (x1*x1)-(x0*x0)+(y1*y1)-(y0*y0);
    // Center[1] += (x2*x2)-(x0*x0)+(y2*y2)-(y0*y0);

    // Center = prod(mJinv, Center);

    // xc = Center[0];
    // yc = Center[1];
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CalculateLargeEdgeCenter(const GeometryType &rGeometry, VectorType &rCenter)
  {
    if (rGeometry.WorkingSpaceDimension() == 3 && rGeometry.size() == 3) //linear surface triangle 3D
    {
        CalculateLargeEdgeCenter(rGeometry[0].X(), rGeometry[0].Y(), rGeometry[0].Z(),
                                 rGeometry[1].X(), rGeometry[1].Y(), rGeometry[1].Z(),
                                 rGeometry[2].X(), rGeometry[2].Y(), rGeometry[2].Z(),
                                 rCenter[0], rCenter[1], rCenter[2]);
    }
    else
    {
      KRATOS_ERROR << "Non valid Geometry size" << std::endl;
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CalculateLargeEdgeCenter(const double x0, const double y0, const double z0,
                                              const double x1, const double y1, const double z1,
                                              const double x2, const double y2, const double z2,
                                              double &xc, double &yc, double &zc)
  {
    VectorType V0,V1,V2,C;
    V0[0] = x0; V0[1] = y0; V0[2] = z0;
    V1[0] = x1; V1[1] = y1; V1[2] = z1;
    V2[0] = x2; V2[1] = y2; V2[2] = z2;

    double a, b, c;
    a = norm_2(V2-V1);
    b = norm_2(V0-V2);
    c = norm_2(V1-V0);

    if (a>=b && a>=c)
      C = 0.5*(V2+V1);
    else if (b>=c && b>=a)
      C = 0.5*(V0+V2);
    else
      C = 0.5*(V1+V0);

    xc = C[0]; yc = C[1]; zc = C[2];
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CalculateInnerCircumCenter(const double x0, const double y0, const double z0,
                                                const double x1, const double y1, const double z1,
                                                const double x2, const double y2, const double z2,
                                                double &xc, double &yc, double &zc)
  {
    CalculateCircumCenter(x0,y0,z0,x1,y1,z1,x2,y2,z2,xc,yc,zc);
    //CalculateCenter(x0,y0,z0,x1,y1,z1,x2,y2,z2,xc,yc,zc); // incenter

    VectorType V0,V1,V2,C,D;
    V0[0] = x0; V0[1] = y0; V0[2] = z0;
    V1[0] = x1; V1[1] = y1; V1[2] = z1;
    V2[0] = x2; V2[1] = y2; V2[2] = z2;
    C[0] = xc; C[1] = yc; C[2] = zc;


    double a, b, c;
    a = norm_2(V2-V1);
    b = norm_2(V0-V2);
    c = norm_2(V1-V0);

    std::vector<VectorType> Vertices(3);

    if (a>=b && a>=c)
    {
      Vertices[0]=V0;
      Vertices[1]=V1;
      Vertices[2]=V2;
    }
    else if (b>=c && b>=a)
    {
      Vertices[0]=V1;
      Vertices[1]=V2;
      Vertices[2]=V0;
    }
    else
    {
      Vertices[0]=V2;
      Vertices[1]=V0;
      Vertices[2]=V1;
    }


    if (CalculateLineIntersection(Vertices[0],Vertices[1],Vertices[2],C,D))
    {
      xc = D[0];
      yc = D[1];
      zc = D[2];
    }
    else if (CalculateLineIntersection(Vertices[1],Vertices[2],Vertices[0],C,D))
    {
      xc = D[0];
      yc = D[1];
      zc = D[2];
    }
    else if (CalculateLineIntersection(Vertices[2],Vertices[0],Vertices[1],C,D))
    {
      xc = D[0];
      yc = D[1];
      zc = D[2];
    }
  }


  //*******************************************************************************************
  //*******************************************************************************************
  // Triangles must be expressed anti-clockwise
  static inline bool CheckSurfaceTrianglesIntersection(const VectorType &V0,
						       const VectorType &V1,
						       const VectorType &V2,
						       const VectorType &W0,
						       const VectorType &W1,
						       const VectorType &W2,
                                                       const VectorType &Normal)
  {
    // Project one triangle on another, both in a given plane
    VectorType v0, v1, v2;
    v1 = V1 - inner_prod(V1-V0, Normal) * Normal;
    v2 = V2 - inner_prod(V2-V0, Normal) * Normal;

    VectorType w0, w1, w2;
    w0 = W0 - inner_prod(W0-V0, Normal) * Normal;
    w1 = W1 - inner_prod(W1-V0, Normal) * Normal;
    w2 = W2 - inner_prod(W2-V0, Normal) * Normal;

    return CheckCoplanarSurfaceTrianglesIntersection(V0,v1,v2,w0,w1,w2);
  }


  //*******************************************************************************************
  //*******************************************************************************************

  // Triangles must be expressed anti-clockwise
  static inline bool CheckSurfaceTrianglesIntersection(const VectorType &V0,
						       const VectorType &V1,
						       const VectorType &V2,
						       const VectorType &W0,
						       const VectorType &W1,
						       const VectorType &W2)
  {
    // Project one triangle in the plane of the other triangle

    // Normal of the plane
    VectorType Normal;
    MathUtils<double>::CrossProduct(Normal, V1-V0, V2-V0); //considering counterclockwise
    double n = norm_2(Normal);
    Normal /= n;

    VectorType w0, w1, w2;
    w0 = W0 - inner_prod(W0-V0, Normal) * Normal;
    w1 = W1 - inner_prod(W1-V0, Normal) * Normal;
    w2 = W2 - inner_prod(W2-V0, Normal) * Normal;

    return CheckCoplanarSurfaceTrianglesIntersection(V0,V1,V2,w0,w1,w2);
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckCoplanarSurfaceTrianglesIntersection(const VectorType &V0,
							       const VectorType &V1,
							       const VectorType &V2,
							       const VectorType &W0,
							       const VectorType &W1,
							       const VectorType &W2)
  {

    // Normal of the plane
    VectorType Normal;
    MathUtils<double>::CrossProduct(Normal, V1-V0, V2-V0); //considering counterclockwise
    double n = norm_2(Normal);
    Normal /= n;
    // Considering they are coplanar change coordinates to 2D
    VectorType D1 = V1-V0;
    double s = norm_2(D1);
    D1 /= s;
    VectorType E1, E2;
    MathUtils<double>::CrossProduct(E1, D1, Normal);
    MathUtils<double>::CrossProduct(E2, Normal, E1);
    // 3D to 2D transformation matrix
    BoundedMatrix<double, 2, 3> C;
    C(0,0)=E1[0];
    C(0,1)=E1[1];
    C(0,2)=E1[2];
    C(1,0)=E2[0];
    C(1,1)=E2[1];
    C(1,2)=E2[2];
    // Get 2D coordinates
    Point2DType v0, v1, v2, w0, w1, w2;
    v0 = prod(C,V0);
    v1 = prod(C,V1);
    v2 = prod(C,V2);
    w0 = prod(C,W0);
    w1 = prod(C,W1);
    w2 = prod(C,W2);

    return Check2DTrianglesIntersection(v0,v1,v2,w0,w1,w2);

  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckPointInSurfaceTriangle(const VectorType &V0,
                                                 const VectorType &V1,
                                                 const VectorType &V2,
                                                 const VectorType &P,
                                                 const VectorType &rNormal)
  {
    // Normal of the plane
    VectorType Normal;
    MathUtils<double>::CrossProduct(Normal, V1-V0, V2-V0);
    Normal = inner_prod(Normal,rNormal)*rNormal;
    double n = norm_2(Normal);
    Normal /= n;

    // This projection is really needed ?
    VectorType W1, W2;
    W1 = V1 - inner_prod(V1-V0, Normal) * Normal;
    W2 = V2 - inner_prod(V2-V0, Normal) * Normal;

    // Project third point on the surface
    VectorType PP;
    PP = P - inner_prod(P-V0, Normal) * Normal;

    // Considering they are coplanar change coordinates to 2D
    VectorType D1 = V1-V0;
    double s = norm_2(D1);
    D1 /= s;
    VectorType E1, E2;
    MathUtils<double>::CrossProduct(E1, D1, Normal);
    MathUtils<double>::CrossProduct(E2, Normal, E1);
    // 3D to 2D transformation matrix
    BoundedMatrix<double, 2, 3> C;
    C(0,0)=E1[0];
    C(0,1)=E1[1];
    C(0,2)=E1[2];
    C(1,0)=E2[0];
    C(1,1)=E2[1];
    C(1,2)=E2[2];
    // Get 2D coordinates
    Point2DType v0, v1, v2, p;
    v0 = prod(C,V0);
    v1 = prod(C,W1);
    v2 = prod(C,W2);
    p = prod(C,PP);

    // Check triangle winding 2D
    bool allowReversed = true;
    double detTri = Det2D(v0, v1, v2);
    if(detTri < 0.0)
    {
      if (allowReversed)
      {
        Point2DType a = v2;
        v2 = v1;
        v1 = a;
      }
      else
        std::cout<<" TRIANGLE FACE NOT COUNTER-CLOCK WISE :: INTERSECTION ERRORS "<<detTri<<std::endl;
    }

    return GeometryUtilities::Check2DPointInTriangle(v0,v1,v2,p);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckPointInSurfaceTriangle(const VectorType &V0,
                                                 const VectorType &V1,
                                                 const VectorType &V2,
                                                 const VectorType &P)
  {
    // Normal of the plane
    VectorType Normal;
    MathUtils<double>::CrossProduct(Normal, V1-V0, V2-V0);
    double n = norm_2(Normal);
    Normal /= n;

    // This projection is really needed ?
    VectorType W1, W2;
    W1 = V1 - inner_prod(V1-V0, Normal) * Normal;
    W2 = V2 - inner_prod(V2-V0, Normal) * Normal;

    // Project third point on the surface
    VectorType PP;
    PP = P - inner_prod(P-V0, Normal) * Normal;

    // Considering they are coplanar change coordinates to 2D
    VectorType D1 = V1-V0;
    double s = norm_2(D1);
    D1 /= s;
    VectorType E1, E2;
    MathUtils<double>::CrossProduct(E1, D1, Normal);
    MathUtils<double>::CrossProduct(E2, Normal, E1);
    // 3D to 2D transformation matrix
    BoundedMatrix<double, 2, 3> C;
    C(0,0)=E1[0];
    C(0,1)=E1[1];
    C(0,2)=E1[2];
    C(1,0)=E2[0];
    C(1,1)=E2[1];
    C(1,2)=E2[2];
    // Get 2D coordinates
    Point2DType v0, v1, v2, p;
    v0 = prod(C,V0);
    v1 = prod(C,W1);
    v2 = prod(C,W2);
    p = prod(C,PP);

    // Check triangle winding 2D
    bool allowReversed = true;
    double detTri = Det2D(v0, v1, v2);
    if(detTri < 0.0)
    {
      if (allowReversed)
      {
        Point2DType a = v2;
        v2 = v1;
        v1 = a;
      }
      else
        std::cout<<" TRIANGLE FACE NOT COUNTER-CLOCK WISE :: INTERSECTION ERRORS "<<detTri<<std::endl;
    }

    return GeometryUtilities::Check2DPointInTriangle(v0,v1,v2,p);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CheckSurfaceTriangleWinding(VectorType &V0,
                                                 VectorType &V1,
                                                 VectorType &V2,
                                                 bool allowReversed = false)
  {
    // Normal of the plane
    VectorType Normal;
    MathUtils<double>::CrossProduct(Normal, V1-V0, V2-V0); //considering counterclockwise
    double n = norm_2(Normal);
    Normal /= n;

    VectorType W1, W2;
    W1 = V1 - inner_prod(V1-V0, Normal) * Normal;
    W2 = V2 - inner_prod(V2-V0, Normal) * Normal;

    // Considering they are coplanar change coordinates to 2D
    VectorType D1 = V1-V0;
    double s = norm_2(D1);
    D1 /= s;
    VectorType E1, E2;
    MathUtils<double>::CrossProduct(E1, D1, Normal);
    MathUtils<double>::CrossProduct(E2, Normal, E1);

    // 3D to 2D transformation matrix
    BoundedMatrix<double, 2, 3> C;
    C(0,0)=E1[0];
    C(0,1)=E1[1];
    C(0,2)=E1[2];
    C(1,0)=E2[0];
    C(1,1)=E2[1];
    C(1,2)=E2[2];

    // Get 2D coordinates
    Point2DType v0, v1, v2;
    v0 = prod(C,V0);
    v1 = prod(C,W1);
    v2 = prod(C,W2);

    // Check triangle winding 2D
    double detTri = Det2D(v0, v1, v2);
    if(detTri < 0.0)
    {
      if (allowReversed)
      {
        Point2DType a = V2;
        V2 = V1;
        V1 = a;
      }
      else
        std::cout<<" TRIANGLE FACE NOT COUNTER-CLOCK WISE :: INTERSECTION ERRORS "<<detTri<<std::endl;
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************
  static inline double Det2D(const Point2DType &p1, const Point2DType &p2, const Point2DType &p3)
  {
    return p1[0]*(p2[1]-p3[1])+p2[0]*(p3[1]-p1[1])+p3[0]*(p1[1]-p2[1]);
  }

  static inline bool BoundaryCollideChk(const Point2DType &p1, const Point2DType &p2, const Point2DType &p3, double eps)
  {
    return Det2D(p1, p2, p3) < eps;
  }

  static inline bool BoundaryDoesntCollideChk(const Point2DType &p1, const Point2DType &p2, const Point2DType &p3, double eps)
  {
    return Det2D(p1, p2, p3) <= eps;
  }

  static inline void CheckTriangleWinding(Point2DType &p1, Point2DType &p2, Point2DType &p3, bool allowReversed)
  {
    double detTri = Det2D(p1, p2, p3);
    if(detTri < 0.0)
    {
      if (allowReversed)
      {
        Point2DType a = p3;
        p3 = p2;
        p2 = a;
      }
      else{
        if(std::abs(detTri)>1e-4)
          std::cout<<" TRIANGLE FACE NOT COUNTER-CLOCK WISE :: INTERSECTION ERRORS "<<detTri<<std::endl;
      }
    }
  }

  static inline bool Check2DPointInTriangle(const Point2DType &V0,
					    const Point2DType &V1,
					    const Point2DType &V2,
					    const Point2DType &P,
					    double tol = 1e-15)
  {
    double A =  std::abs(Det2D(V0, V1, V2));
    double AT = std::abs(Det2D(P, V1, V2)) +  std::abs(Det2D(V0, P, V2)) +  std::abs(Det2D(V0, V1, P));
    if (fabs(A-AT) <= tol)
      return true;
    else
      return false;
  }

  // https://rosettacode.org/wiki/Determine_if_two_triangles_overlap
  // Triangles must be expressed anti-clockwise
  static inline bool Check2DTrianglesIntersection(const Point2DType &V0,
						  const Point2DType &V1,
						  const Point2DType &V2,
						  const Point2DType &W0,
						  const Point2DType &W1,
						  const Point2DType &W2)
  {
    bool onBoundary = false; //if true, faces contacting one point or one edge are accepted
    bool allowReversed = false;
    double eps = 0.0;

    BoundedVector<Point2DType,3> t1;
    t1[0] = V0;
    t1[1] = V1;
    t1[2] = V2;
    BoundedVector<Point2DType,3> t2;
    t2[0] = W0;
    t2[1] = W1;
    t2[2] = W2;

    //Triangles must be expressed anti-clockwise
    CheckTriangleWinding(t1[0], t1[1], t1[2], allowReversed);
    CheckTriangleWinding(t2[0], t2[1], t2[2], allowReversed);

    bool (*chkEdge)(const Point2DType &, const Point2DType &, const Point2DType &, double) = NULL;
    if(onBoundary) //Points on the boundary are considered as colliding
      chkEdge = BoundaryCollideChk;
    else //Points on the boundary are not considered as colliding
      chkEdge = BoundaryDoesntCollideChk;

    //Check if it contains baricenter triangle 1
    Point2DType B = (W0+W1+W2)/3.0;
    if (Check2DPointInTriangle(V0,V1,V2,B))
      return true;

    //Check if it contains baricenter triangle 2
    B = (V0+V1+V2)/3.0;
    if (Check2DPointInTriangle(W0,W1,W2,B))
      return true;

    //For edge E of triangle 1,
    for(int i=0; i<3; i++)
    {
      int j=(i+1)%3;

      //Check all points of triangle 2 lay on the external side of the edge E. If
      //they do, the triangles do not collide.
      if (chkEdge(t1[i], t1[j], t2[0], eps) &&
	  chkEdge(t1[i], t1[j], t2[1], eps) &&
	  chkEdge(t1[i], t1[j], t2[2], eps))
	return false;
    }

    //For edge E of triangle 2,
    for(int i=0; i<3; i++)
    {
      int j=(i+1)%3;

      //Check all points of triangle 1 lay on the external side of the edge E. If
      //they do, the triangles do not collide.
      if (chkEdge(t2[i], t2[j], t1[0], eps) &&
	  chkEdge(t2[i], t2[j], t1[1], eps) &&
	  chkEdge(t2[i], t2[j], t1[2], eps))
	return false;
    }

    //The triangles collide
    return true;

  }

  //*******************************************************************************************
  //*******************************************************************************************

  //GEOMETRICAL THEORY
  // The method was to
  // use the two equations that represent the lines:

  //   L1 = V1 + aD1
  //   L2 = V0 + bD2

  // Where D1 and D2 are the director vectors
  // V1 point which belongs to L1
  // V0 point which belongs to L2

  // => V1 + aD1 = V0 + bD2
  // => aD1 = (V0-V1) + bD2

  // applying the vectorial (x D2) in the two sides of the equation:

  // a(D1 x D2) = (V0-V1) x D2

  // once we have "a" we can replace it in the 1st equation to get the
  // point of intersection

  static inline bool CalculateLineIntersection(const VectorType &V0,
                                               const VectorType &V1,
                                               const VectorType &V2,
                                               const VectorType &C,
                                               VectorType &D)
  {
    VectorType D1 = V2-V1;
    VectorType D2 = C-V0;

    double s = norm_2(D1);
    double c = norm_2(D2);
    D1 /= s;
    D2 /= c;

    VectorType V0_V1 = V0-V1;
    VectorType D1xD2;
    MathUtils<double>::CrossProduct(D1xD2, D1, D2);

    VectorType V0_V1xD2;
    MathUtils<double>::CrossProduct(V0_V1xD2, V0_V1, D2);

    double a = 0;
    if (norm_2(D1xD2) != 0){
      a = norm_2(V0_V1xD2) / norm_2(D1xD2);
      for (unsigned int i=0; i<3; ++i)
      {
        if(D1xD2[i]!=0 && V0_V1xD2[i]!=0){
          a *= (V0_V1xD2[i] / D1xD2[i])/a;
          break;
        }
      }
    }
    else{
      a = 0;
    }

    double tol = s*1e-1;
    if (a > tol && a < s-tol)
    {
      D = V1 + a*D1;
      //std::cout<<"a ("<<V0_V1xD2[0] / D1xD2[0]<<" "<<V0_V1xD2[1] / D1xD2[1]<<" "<<V0_V1xD2[2] / D1xD2[2]<<std::endl;

      //improve position
      D2 = D-V0;
      double d = norm_2(D2);
      if (c < 0.6*d){  //if it is inside the triangle
        D = V0 + 0.6*D2;
      }
      else{
        D = 0.5*(D + 0.5*(V2+V1)); // take the side but set it closer to center of the side
      }

      return true;
    }

    return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CalculateCircumCenter(const double x0, const double y0, const double z0,
                                           const double x1, const double y1, const double z1,
                                           const double x2, const double y2, const double z2,
                                           double &xc, double &yc, double &zc)
  {
    double xba, yba, zba, xca, yca, zca;
    double balength, calength;
    double xcrossbc, ycrossbc, zcrossbc;
    double denominator;

    //Use coordinates relative to point a (point 0)
    xba = x1-x0;
    yba = y1-y0;
    zba = z1-z0;

    xca = x2-x0;
    yca = y2-y0;
    zca = z2-z0;

    //Squares of lengths of the edges incident to a
    balength = xba * xba + yba * yba + zba * zba;
    calength = xca * xca + yca * yca + zca * zca;

    //Cross product of these edges
    xcrossbc = yba * zca - yca * zba;
    ycrossbc = zba * xca - zca * xba;
    zcrossbc = xba * yca - xca * yba;

    //Calculate the denominator of the formulae
    denominator = 0.5 / (xcrossbc * xcrossbc + ycrossbc * ycrossbc + zcrossbc * zcrossbc);

    //Calculate offset (from `a') of circumcenter
    xc = x0+((balength * yca - calength * yba) * zcrossbc -
             (balength * zca - calength * zba) * ycrossbc) * denominator;
    yc = y0+((balength * zca - calength * zba) * xcrossbc -
             (balength * xca - calength * xba) * zcrossbc) * denominator;
    zc = z0+((balength * xca - calength * xba) * ycrossbc -
             (balength * yca - calength * yba) * xcrossbc) * denominator;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double CalculateRadius(const std::vector<Vector> &rVertices, const unsigned int &dimension, double &rVolume)
  {
    double radius = 0;
    if (dimension == 2)
    {
      BoundedMatrix<double, 2, 2> mJ;    //local jacobian
      BoundedMatrix<double, 2, 2> mJinv; //inverse jacobian

      //calculation of the jacobian  //coordinate center point 0
      for (unsigned int i = 0; i < dimension; ++i)
      {
        for (unsigned int j = 0; j < dimension; ++j)
        {
          mJ(i, j) = rVertices[i + 1][j] - rVertices[0][j];
        }
      }

      mJ *= 2.0;

      //calculation of the determinant (volume/2)
      rVolume = mJ(0, 0) * mJ(1, 1) - mJ(0, 1) * mJ(1, 0); //detJ

      //calculation of the inverse of the jacobian
      mJinv(0, 0) = mJ(1, 1);
      mJinv(0, 1) = -mJ(0, 1);
      mJinv(1, 0) = -mJ(1, 0);
      mJinv(1, 1) = mJ(0, 0);

      mJinv /= rVolume;

      //calculation of the center
      Vector Center = ZeroVector(2); //center pos

      //center point 0
      for (unsigned int i = 0; i < dimension; ++i)
      {
        for (unsigned int j = 0; j < dimension; ++j)
        {
          Center[i] += (rVertices[i + 1][j] * rVertices[i + 1][j]);
          Center[i] -= (rVertices[0][j] * rVertices[0][j]);
        }
      }

      Center = prod(mJinv, Center);

      //calculate the element radius
      Center[0] -= rVertices[0][0];
      Center[1] -= rVertices[0][1];

      radius = norm_2(Center);
    }
    else if (dimension == 3)
    {

      BoundedVector<double, 3> mRHS;     //center pos
      BoundedMatrix<double, 3, 3> mJ;    //local jacobian
      BoundedMatrix<double, 3, 3> mJinv; //inverse jacobian

      //calculation of the jacobian  //coordinate center point 0
      for (unsigned int i = 0; i < dimension; ++i)
      {
        for (unsigned int j = 0; j < dimension; ++j)
        {
          mJ(i, j) = rVertices[i + 1][j] - rVertices[0][j];
        }
      }

      //calculation of the inverse of the jacobian
      //first column
      mJinv(0, 0) = mJ(1, 1) * mJ(2, 2) - mJ(1, 2) * mJ(2, 1);
      mJinv(1, 0) = -mJ(1, 0) * mJ(2, 2) + mJ(1, 2) * mJ(2, 0);
      mJinv(2, 0) = mJ(1, 0) * mJ(2, 1) - mJ(1, 1) * mJ(2, 0);
      //second column
      mJinv(0, 1) = -mJ(0, 1) * mJ(2, 2) + mJ(0, 2) * mJ(2, 1);
      mJinv(1, 1) = mJ(0, 0) * mJ(2, 2) - mJ(0, 2) * mJ(2, 0);
      mJinv(2, 1) = -mJ(0, 0) * mJ(2, 1) + mJ(0, 1) * mJ(2, 0);
      //third column
      mJinv(0, 2) = mJ(0, 1) * mJ(1, 2) - mJ(0, 2) * mJ(1, 1);
      mJinv(1, 2) = -mJ(0, 0) * mJ(1, 2) + mJ(0, 2) * mJ(1, 0);
      mJinv(2, 2) = mJ(0, 0) * mJ(1, 1) - mJ(0, 1) * mJ(1, 0);

      //calculation of the determinant (volume/6)
      rVolume = mJ(0, 0) * mJinv(0, 0) + mJ(0, 1) * mJinv(1, 0) + mJ(0, 2) * mJinv(2, 0); //detJ

      //calculation of the center
      Vector Center = ZeroVector(3); //center pos

      mRHS[0] = (mJ(0, 0) * mJ(0, 0) + mJ(0, 1) * mJ(0, 1) + mJ(0, 2) * mJ(0, 2));
      mRHS[1] = (mJ(1, 0) * mJ(1, 0) + mJ(1, 1) * mJ(1, 1) + mJ(1, 2) * mJ(1, 2));
      mRHS[2] = (mJ(2, 0) * mJ(2, 0) + mJ(2, 1) * mJ(2, 1) + mJ(2, 2) * mJ(2, 2));

      //bxc
      Center[0] = (+mRHS[0]) * (mJ(1, 1) * mJ(2, 2) - mJ(1, 2) * mJ(2, 1));
      Center[1] = (-mRHS[0]) * (mJ(1, 0) * mJ(2, 2) - mJ(1, 2) * mJ(2, 0));
      Center[2] = (+mRHS[0]) * (mJ(1, 0) * mJ(2, 1) - mJ(1, 1) * mJ(2, 0));

      //cxa
      Center[0] += (+mRHS[1]) * (mJ(2, 1) * mJ(0, 2) - mJ(2, 2) * mJ(0, 1));
      Center[1] += (-mRHS[1]) * (mJ(2, 0) * mJ(0, 2) - mJ(2, 2) * mJ(0, 0));
      Center[2] += (+mRHS[1]) * (mJ(2, 0) * mJ(0, 1) - mJ(2, 1) * mJ(0, 0));

      //axb
      Center[0] += (+mRHS[2]) * (mJ(0, 1) * mJ(1, 2) - mJ(0, 2) * mJ(1, 1));
      Center[1] += (-mRHS[2]) * (mJ(0, 0) * mJ(1, 2) - mJ(0, 2) * mJ(1, 0));
      Center[2] += (+mRHS[2]) * (mJ(0, 0) * mJ(1, 1) - mJ(0, 1) * mJ(1, 0));

      Center /= (2.0 * rVolume);

      //calculate the element radius
      radius = norm_2(Center);
    }

    return radius;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double CalculateSideLength(const NodeType &P1, const NodeType &P2)
  {
    return sqrt((P1.X() - P2.X()) * (P1.X() - P2.X()) + (P1.Y() - P2.Y()) * (P1.Y() - P2.Y()) + (P1.Z() - P2.Z()) * (P1.Z() - P2.Z()));
  };

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double CalculateFaceSize(GeometryType &rGeometry)
  {

    if (rGeometry.size() == 2)
    {
      return CalculateSideLength(rGeometry[0], rGeometry[1]);
    }
    else if (rGeometry.size() == 3)
    {
      return 2 * CalculateTriangleRadius(rGeometry);
    }
    else
    {
      std::cout << "BOUNDARY SIZE NOT COMPUTED :: geometry not correct" << std::endl;
      return 0;
    }
  };

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CalculateEdgesSize(const GeometryType &rGeometry, std::vector<double>& rEdges)
  {
    if (rGeometry.size() == 2)
    {
      rEdges.resize(1);
      rEdges[0] =  CalculateSideLength(rGeometry[0], rGeometry[1]);
    }
    else if (rGeometry.size() == 3)
    {
      rEdges.resize(3);
      rEdges[0] = CalculateSideLength(rGeometry[1], rGeometry[2]);
      rEdges[1] = CalculateSideLength(rGeometry[0], rGeometry[2]);
      rEdges[2] = CalculateSideLength(rGeometry[0], rGeometry[1]);
    }
    else
    {
      rEdges.resize(6);
      rEdges[0] = CalculateSideLength(rGeometry[1], rGeometry[2]);
      rEdges[1] = CalculateSideLength(rGeometry[0], rGeometry[2]);
      rEdges[2] = CalculateSideLength(rGeometry[0], rGeometry[1]);

      rEdges[3] = CalculateSideLength(rGeometry[0], rGeometry[3]);
      rEdges[4] = CalculateSideLength(rGeometry[1], rGeometry[3]);
      rEdges[5] = CalculateSideLength(rGeometry[2], rGeometry[3]);
    }

  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CalculateEdgesSize(const GeometryType &rGeometry, std::vector<double>& rEdges, std::vector<Flags> NodeFlags, std::vector<Flags> EdgeFlags)
  {
    if (rGeometry.size() == 2)
    {
      if((rGeometry[0].Is(NodeFlags) && rGeometry[1].Is(EdgeFlags)) || (rGeometry[0].Is(EdgeFlags) && rGeometry[1].Is(NodeFlags)))
        rEdges.push_back(CalculateSideLength(rGeometry[0], rGeometry[1]));
    }
    else if (rGeometry.size() == 3)
    {
      if((rGeometry[1].Is(NodeFlags) && rGeometry[2].Is(EdgeFlags)) || (rGeometry[1].Is(EdgeFlags) && rGeometry[2].Is(NodeFlags)))
        rEdges.push_back(CalculateSideLength(rGeometry[1], rGeometry[2]));
      if((rGeometry[0].Is(NodeFlags) && rGeometry[2].Is(EdgeFlags)) || (rGeometry[0].Is(EdgeFlags) && rGeometry[2].Is(NodeFlags)))
        rEdges.push_back(CalculateSideLength(rGeometry[0], rGeometry[2]));
      if((rGeometry[0].Is(NodeFlags) && rGeometry[1].Is(EdgeFlags)) || (rGeometry[0].Is(EdgeFlags) && rGeometry[1].Is(NodeFlags)))
        rEdges.push_back(CalculateSideLength(rGeometry[0], rGeometry[1]));
    }
    else
    {
      if((rGeometry[1].Is(NodeFlags) && rGeometry[2].Is(EdgeFlags)) || (rGeometry[1].Is(EdgeFlags) && rGeometry[2].Is(NodeFlags)))
        rEdges.push_back(CalculateSideLength(rGeometry[1], rGeometry[2]));
      if((rGeometry[0].Is(NodeFlags) && rGeometry[2].Is(EdgeFlags)) || (rGeometry[0].Is(EdgeFlags) && rGeometry[2].Is(NodeFlags)))
        rEdges.push_back(CalculateSideLength(rGeometry[0], rGeometry[2]));
      if((rGeometry[0].Is(NodeFlags) && rGeometry[1].Is(EdgeFlags)) || (rGeometry[0].Is(EdgeFlags) && rGeometry[1].Is(NodeFlags)))
        rEdges.push_back(CalculateSideLength(rGeometry[0], rGeometry[1]));

      if((rGeometry[0].Is(NodeFlags) && rGeometry[3].Is(EdgeFlags)) || (rGeometry[0].Is(EdgeFlags) && rGeometry[3].Is(NodeFlags)))
        rEdges.push_back(CalculateSideLength(rGeometry[0], rGeometry[3]));
      if((rGeometry[1].Is(NodeFlags) && rGeometry[3].Is(EdgeFlags)) || (rGeometry[1].Is(EdgeFlags) && rGeometry[3].Is(NodeFlags)))
        rEdges.push_back(CalculateSideLength(rGeometry[1], rGeometry[3]));
      if((rGeometry[2].Is(NodeFlags) && rGeometry[3].Is(EdgeFlags)) || (rGeometry[2].Is(EdgeFlags) && rGeometry[3].Is(NodeFlags)))
        rEdges.push_back(CalculateSideLength(rGeometry[2], rGeometry[3]));
    }

  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double CalculateMaxEdgeSize(const GeometryType &rGeometry)
  {
    std::vector<double> edges;
    CalculateEdgesSize(rGeometry, edges);
    return *std::max_element(edges.begin(), edges.end());
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double CalculateMinEdgeSize(const GeometryType &rGeometry)
  {
    std::vector<double> edges;
    CalculateEdgesSize(rGeometry, edges);
    return *std::min_element(edges.begin(), edges.end());
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double CompareWallEdgeSizes(const GeometryType &rGeometry, const double& rSize)
  {
    std::vector<double> wall_edges;
    CalculateEdgesSize(rGeometry, wall_edges, {STRUCTURE}, {STRUCTURE});
    if(wall_edges.size()==0){
      //CalculateEdgesSize(rGeometry, wall_edges, {FREE_SURFACE,STRUCTURE.AsFalse()}, {FREE_SURFACE,STRUCTURE.AsFalse()});
      for (auto &i_node : rGeometry)
      {
        if(i_node.Is(STRUCTURE)){
          NodeWeakPtrVectorType &nNodes = i_node.GetValue(NEIGHBOUR_NODES);
          for (auto &i_nnode : nNodes)
          {
            if(i_nnode.Is(STRUCTURE))
              wall_edges.push_back(CalculateSideLength(i_node, i_nnode));
          }
        }
      }
    }


    if(wall_edges.size()!=0){

      // std::cout<<" WALL edge_sizes "<<wall_edges<<" max "<<*std::max_element(wall_edges.begin(), wall_edges.end())<<std::endl;

      // check maximum length
      double max_wall_edge = *std::max_element(wall_edges.begin(), wall_edges.end());
      double mean_wall_edge = std::accumulate(wall_edges.begin(), wall_edges.end(), 0.0)/(double)wall_edges.size();

      if (max_wall_edge > 12*rSize){ //maximum size allowed
        //std::cout<<" max_wall_edge "<<max_wall_edge<<" rSize "<<rSize<<std::endl;
        return 1e3; // thousand times the size
      }

      // check compare
      std::vector<double> edges;
      CalculateEdgesSize(rGeometry, edges, {FREE_SURFACE,STRUCTURE.AsFalse()}, {STRUCTURE});

      if(edges.size()!=0){

        double max_edge = *std::max_element(edges.begin(), edges.end());

        // std::cout<<" edges "<<edges<<" max "<<*std::max_element(edges.begin(), edges.end())<<" max wall edge "<<max_wall_edge<<" mean wall edge "<<mean_wall_edge<<" compare "<<max_edge/mean_wall_edge<<" Size "<<rSize<<std::endl;

        if (max_wall_edge > 12*rSize){ //maximum size allowed
          //std::cout<<" max_wall_edge "<<max_wall_edge<<" rSize "<<rSize<<std::endl;
          return 1e3; // thousand times the size
        }
        //return max_edge/max_wall_edge; //relative size
        return max_edge/mean_wall_edge; //relative size
      }

    }
    return 1; // same size
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double CompareEdgeSizes(const GeometryType &rGeometry)
  {
    std::vector<double> edges;
    CalculateEdgesSize(rGeometry, edges);

    auto it = std::minmax_element(edges.begin(), edges.end());
    double minimum = *it.first;
    double maximum = *it.second;

    return maximum/minimum;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline VectorType CalculateAnglesSize(const std::vector<double> &edges)
  {
    VectorType angles;

    angles[0] = std::acos((edges[1]*edges[1]+edges[2]*edges[2]-edges[0]*edges[0])/(2.0*edges[1]*edges[2]));
    angles[1] = std::acos((edges[0]*edges[0]+edges[2]*edges[2]-edges[1]*edges[1])/(2.0*edges[0]*edges[2]));
    angles[2] = std::acos((edges[0]*edges[0]+edges[1]*edges[1]-edges[2]*edges[2])/(2.0*edges[0]*edges[1]));

    angles *= (180.0 / Globals::Pi);

    return angles;

  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CalculateEdgesProjection(const NodeType &P0, const NodeType &P1, const NodeType &P2, double &rProjection)
  {
    rProjection = (P0.X() - P1.X()) * (P2.X() - P0.X()) + (P0.Y() - P1.Y()) * (P2.Y() - P0.Y()) + (P0.Z() - P1.Z()) * (P2.Z() - P0.Z());

    double L1 = CalculateSideLength(P0, P1);
    double L2 = CalculateSideLength(P0, P2);

    if (L1!=0 && L2 !=0)
      rProjection /= (L1 * L2);
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CalculateNormalsProjection(const Condition &rCondition1, const Condition &rCondition2, double &rProjection)
  {
    VectorType N1 = rCondition1.GetValue(NORMAL);
    VectorType N2 = rCondition2.GetValue(NORMAL);

    rProjection = inner_prod(N1, N2);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double CalculateTriangleRadius(const GeometryType &rGeometry)
  {

    double L1 = CalculateSideLength(rGeometry[0], rGeometry[1]);
    double L2 = CalculateSideLength(rGeometry[1], rGeometry[2]);
    double L3 = CalculateSideLength(rGeometry[2], rGeometry[0]);

    double Area = rGeometry.Area();

    //inradius
    double Rcrit = Area * 2 / (L1 + L2 + L3);

    return Rcrit;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double CalculateTetrahedronRadius(const GeometryType &rGeometry)
  {

    //edges
    double L1 = CalculateSideLength(rGeometry[0], rGeometry[1]);
    double L2 = CalculateSideLength(rGeometry[1], rGeometry[2]);
    double L3 = CalculateSideLength(rGeometry[2], rGeometry[3]);
    double L4 = CalculateSideLength(rGeometry[3], rGeometry[0]);
    double L5 = CalculateSideLength(rGeometry[3], rGeometry[1]);
    double L6 = CalculateSideLength(rGeometry[2], rGeometry[0]);

    //inradius
    double S = 0.5 * (L1 + L4 + L5); //semiperimeter
    double R1 = sqrt(S * (S - L1) * (S - L4) * (S - L5)) / S;

    S = 0.5 * (L2 + L3 + L5); //semiperimeter
    double R2 = sqrt(S * (S - L2) * (S - L3) * (S - L5)) / S;

    S = 0.5 * (L3 + L4 + L6); //semiperimeter
    double R3 = sqrt(S * (S - L3) * (S - L4) * (S - L6)) / S;

    S = 0.5 * (L1 + L2 + L6); //semiperimeter
    double R4 = sqrt(S * (S - L1) * (S - L2) * (S - L6)) / S;

    S = 1.0 / (R1 * R1) + 1.0 / (R2 * R2) + 1.0 / (R3 * R3) + 1.0 / (R4 * R4);

    double Rcrit = sqrt(2.0 / S); //this is always bigger than the inradius

    return Rcrit;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double CalculateDomainSizeChange(const GeometryType &rGeometry, const double &rFactor, const Variable<array_1d<double,3>>& rVariable)
  {
    if (rGeometry.size() == 3)
    {
      double current_radius = CalculateTriangleArea(rGeometry[0].X(), rGeometry[0].Y(),
                                                    rGeometry[1].X(), rGeometry[1].Y(),
                                                    rGeometry[2].X(), rGeometry[2].Y());

      const array_1d<double,3> &V0 = rGeometry[0].FastGetSolutionStepValue(rVariable);
      const array_1d<double,3> &V1 = rGeometry[1].FastGetSolutionStepValue(rVariable);
      const array_1d<double,3> &V2 = rGeometry[2].FastGetSolutionStepValue(rVariable);

      double updated_radius = CalculateTriangleArea(rGeometry[0].X()+rFactor*V0[0], rGeometry[0].Y()+rFactor*V0[1],
                                                    rGeometry[1].X()+rFactor*V1[0], rGeometry[1].Y()+rFactor*V1[1],
                                                    rGeometry[2].X()+rFactor*V2[0], rGeometry[2].Y()+rFactor*V2[1]);
      return (updated_radius-current_radius);
    }
    else{
      double current_radius = CalculateTetrahedronVolume(rGeometry[0].X(), rGeometry[0].Y(), rGeometry[0].Z(),
                                                         rGeometry[1].X(), rGeometry[1].Y(), rGeometry[1].Z(),
                                                         rGeometry[2].X(), rGeometry[2].Y(), rGeometry[2].Z(),
                                                         rGeometry[3].X(), rGeometry[3].Y(), rGeometry[3].Z());

      const array_1d<double,3> &V0 = rGeometry[0].FastGetSolutionStepValue(rVariable);
      const array_1d<double,3> &V1 = rGeometry[1].FastGetSolutionStepValue(rVariable);
      const array_1d<double,3> &V2 = rGeometry[2].FastGetSolutionStepValue(rVariable);
      const array_1d<double,3> &V3 = rGeometry[2].FastGetSolutionStepValue(rVariable);

      double updated_radius = CalculateTetrahedronVolume(rGeometry[0].X()+rFactor*V0[0], rGeometry[0].Y()+rFactor*V0[1], rGeometry[0].Z()+rFactor*V0[2],
                                                         rGeometry[1].X()+rFactor*V1[0], rGeometry[1].Y()+rFactor*V1[1], rGeometry[1].Z()+rFactor*V1[2],
                                                         rGeometry[2].X()+rFactor*V2[0], rGeometry[2].Y()+rFactor*V2[1], rGeometry[2].Z()+rFactor*V2[2],
                                                         rGeometry[3].X()+rFactor*V3[0], rGeometry[3].Y()+rFactor*V3[1], rGeometry[3].Z()+rFactor*V3[2]);

      return (updated_radius-current_radius);
    }

    return 0;
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline double CalculateElementRadius(const GeometryType &rGeometry)
  {
    if (rGeometry.size() == 3)
      return CalculateTriangleRadius(rGeometry);
    else
      return CalculateTetrahedronRadius(rGeometry);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double CalculateElementRadius(const GeometryType &rGeometry, double &rDomainSize)
  {

    if (rGeometry.size() == 3)
      return CalculateTriangleRadius(rGeometry[0].X(), rGeometry[0].Y(),
                                     rGeometry[1].X(), rGeometry[1].Y(),
                                     rGeometry[2].X(), rGeometry[2].Y(),
                                     rDomainSize);
    else
      return CalculateTetrahedronRadius(rGeometry[0].X(), rGeometry[0].Y(), rGeometry[0].Z(),
                                        rGeometry[1].X(), rGeometry[1].Y(), rGeometry[1].Z(),
                                        rGeometry[2].X(), rGeometry[2].Y(), rGeometry[2].Z(),
                                        rGeometry[3].X(), rGeometry[3].Y(), rGeometry[3].Z(),
                                        rDomainSize);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double CalculateTriangleArea(const double x0, const double y0,
                                             const double x1, const double y1,
                                             const double x2, const double y2)
  {
    return 0.5 * ((x1 - x0) * (y2 - y0) - (y1 - y0) * (x2 - x0));
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double CalculateTriangleRadius(const double x0, const double y0,
                                               const double x1, const double y1,
                                               const double x2, const double y2,
                                               double &Area)
  {

    double L1 = sqrt((x0 - x1) * (x0 - x1) + (y0 - y1) * (y0 - y1));
    double L2 = sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
    double L3 = sqrt((x2 - x0) * (x2 - x0) + (y2 - y0) * (y2 - y0));

    Area = fabs(0.5 * ((x0 * y1) - (x0 * y2) - (x1 * y0) + (x1 * y2) + (x2 * y0) - (x2 * y1)));

    //inradius
    double Rcrit = Area * 2 / (L1 + L2 + L3);

    return Rcrit;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double CalculateTetrahedronVolume(const double x0, const double y0, const double z0,
                                                  const double x1, const double y1, const double z1,
                                                  const double x2, const double y2, const double z2,
                                                  const double x3, const double y3, const double z3)
  {
    //volume
    double Volume = 0;

    Volume = CalculateDeterminant(x1, y1, z1, x2, y2, z2, x3, y3, z3);
    Volume -= CalculateDeterminant(x0, y0, z0, x2, y2, z2, x3, y3, z3);
    Volume += CalculateDeterminant(x0, y0, z0, x1, y1, z1, x3, y3, z3);
    Volume -= CalculateDeterminant(x0, y0, z0, x1, y1, z1, x2, y2, z2);

    Volume *= (1.0 / 6.0);

    return Volume;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double CalculateTetrahedronRadius(const double x0, const double y0, const double z0,
                                                  const double x1, const double y1, const double z1,
                                                  const double x2, const double y2, const double z2,
                                                  const double x3, const double y3, const double z3,
                                                  double &Volume)
  {

    //edges
    double L1 = sqrt((x0 - x1) * (x0 - x1) + (y0 - y1) * (y0 - y1) + (z0 - z1) * (z0 - z1));
    double L2 = sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2) + (z1 - z2) * (z1 - z2));
    double L3 = sqrt((x2 - x3) * (x2 - x3) + (y2 - y3) * (y2 - y3) + (z2 - z3) * (z2 - z3));
    double L4 = sqrt((x3 - x0) * (x3 - x0) + (y3 - y0) * (y3 - y0) + (z3 - z0) * (z3 - z0));
    double L5 = sqrt((x3 - x1) * (x3 - x1) + (y3 - y1) * (y3 - y1) + (z3 - z1) * (z3 - z1));
    double L6 = sqrt((x2 - x0) * (x2 - x0) + (y2 - y0) * (y2 - y0) + (z2 - z0) * (z2 - z0));

    //volume
    Volume = CalculateTetrahedronVolume(x0, y0, z0, x1, y1, z1, x2, y2, z2, x3, y3, z3);

    //inradius
    double S = 0.5 * (L1 + L4 + L5); //semiperimeter
    double R1 = sqrt(S * (S - L1) * (S - L4) * (S - L5)) / S;

    S = 0.5 * (L2 + L3 + L5); //semiperimeter
    double R2 = sqrt(S * (S - L2) * (S - L3) * (S - L5)) / S;

    S = 0.5 * (L3 + L4 + L6); //semiperimeter
    double R3 = sqrt(S * (S - L3) * (S - L4) * (S - L6)) / S;

    S = 0.5 * (L1 + L2 + L6); //semiperimeter
    double R4 = sqrt(S * (S - L1) * (S - L2) * (S - L6)) / S;

    S = 1.0 / (R1 * R1) + 1.0 / (R2 * R2) + 1.0 / (R3 * R3) + 1.0 / (R4 * R4);

    double Rcrit = sqrt(2.0 / S); //this is always bigger than the inradius

    return Rcrit;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double CalculateDeterminant(const double x0, const double y0, const double z0,
                                            const double x1, const double y1, const double z1,
                                            const double x2, const double y2, const double z2)
  {
    return (x0 * y1 * z2 - x0 * y2 * z1 - x1 * y0 * z2 + x1 * y2 * z0 + x2 * y0 * z1 - x2 * y1 * z0);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline double CalculateAverageSideLength(const double x0, const double y0,
                                                  const double x1, const double y1,
                                                  const double x2, const double y2)
  {
    double length_0 = sqrt(x0 * x0 + y0 * y0);
    double length_1 = sqrt(x1 * x1 + y1 * y1);
    double length_2 = sqrt(x2 * x2 + y2 * y2);

    return 0.5 * (length_0 + length_1 + length_2);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CalculatePosition(const std::vector<VectorType> &rVerticesCoordinates,
                                       const VectorType &rCenter, Vector &rShapeFunctions)
  {

    if (rVerticesCoordinates.size() == 3)
    {

      return CalculatePosition(rVerticesCoordinates[0][0], rVerticesCoordinates[0][1],
                               rVerticesCoordinates[1][0], rVerticesCoordinates[1][1],
                               rVerticesCoordinates[2][0], rVerticesCoordinates[2][1],
                               rCenter[0], rCenter[1], rShapeFunctions);
    }
    else if (rVerticesCoordinates.size() == 4)
    {

      return CalculatePosition(rVerticesCoordinates[0][0], rVerticesCoordinates[0][1], rVerticesCoordinates[0][2],
                               rVerticesCoordinates[1][0], rVerticesCoordinates[1][1], rVerticesCoordinates[1][2],
                               rVerticesCoordinates[2][0], rVerticesCoordinates[2][1], rVerticesCoordinates[2][2],
                               rVerticesCoordinates[3][0], rVerticesCoordinates[3][1], rVerticesCoordinates[3][2],
                               rCenter[0], rCenter[1], rCenter[2], rShapeFunctions);
    }
    else
    {
      KRATOS_ERROR << "Number of points supplied out of range ERROR" << std::endl;
    }

    return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CalculatePosition(const double &x0, const double &y0, const double &z0,
                                       const double &x1, const double &y1, const double &z1,
                                       const double &x2, const double &y2, const double &z2,
                                       const double &x3, const double &y3, const double &z3,
                                       const double &xc, const double &yc, const double &zc,
                                       Vector &rShapeFunctions)
  {
    double volume = CalculateTetrahedronVolume(x0, y0, z0, x1, y1, z1, x2, y2, z2, x3, y3, z3);

    if (volume < 1e-25)
      KRATOS_WARNING("") << " ERROR LS: tetrahedral element with zero area found: " << volume << " position (" << x0 << ", " << y0 << ", " << z0 << ") (" << x1 << ", " << y1 << ", " << z1 << ") (" << x2 << ", " << y2 << ", " << z2 << ") (" << x3 << ", " << y3 << ", " << z3 << ") " << std::endl;

    if (rShapeFunctions.size() != 4)
      rShapeFunctions.resize(4);

    rShapeFunctions[0] = CalculateTetrahedronVolume(xc, yc, zc, x1, y1, z1, x2, y2, z2, x3, y3, z3);
    rShapeFunctions[1] = CalculateTetrahedronVolume(x0, y0, z0, xc, yc, zc, x2, y2, z2, x3, y3, z3);
    rShapeFunctions[2] = CalculateTetrahedronVolume(x0, y0, z0, x1, y1, z1, xc, yc, zc, x3, y3, z3);
    rShapeFunctions[3] = CalculateTetrahedronVolume(x0, y0, z0, x1, y1, z1, x2, y2, z2, xc, yc, zc);

    rShapeFunctions *= 1.0/volume;

    static constexpr double lower_limit = -1e-5;
    static constexpr double upper_limit = 1.0 - lower_limit;

    if (rShapeFunctions[0] >= lower_limit && rShapeFunctions[1] >= lower_limit && rShapeFunctions[2] >= lower_limit && rShapeFunctions[3] >= lower_limit &&
        rShapeFunctions[0] <= upper_limit && rShapeFunctions[1] <= upper_limit && rShapeFunctions[2] <= upper_limit && rShapeFunctions[3] <= upper_limit) //if the xc yc zc is inside the tetrahedron
      return true;

    return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CalculatePosition(const double &x0, const double &y0,
                                       const double &x1, const double &y1,
                                       const double &x2, const double &y2,
                                       const double &xc, const double &yc,
                                       Vector &rShapeFunctions)
  {
    double area = CalculateTriangleArea(x0, y0, x1, y1, x2, y2);

    if (area < 1e-15)
      KRATOS_WARNING("") << "  Triangle element with zero ("<<area<<") area found: vertices position: [("<< x0 << "," << y0 <<") ("<< x1 << "," << y1 <<") ("<< x2 << "," << y2 << ")] " << std::endl;

    if (rShapeFunctions.size() != 3)
      rShapeFunctions.resize(3);

    rShapeFunctions[0] = CalculateTriangleArea(x1, y1, x2, y2, xc, yc);
    rShapeFunctions[1] = CalculateTriangleArea(x2, y2, x0, y0, xc, yc);
    rShapeFunctions[2] = CalculateTriangleArea(x0, y0, x1, y1, xc, yc);

    rShapeFunctions *= 1.0/area;

    static constexpr double lower_limit = -1e-5;
    static constexpr double upper_limit = 1.0 - lower_limit;

    if (rShapeFunctions[0] >= lower_limit && rShapeFunctions[1] >= lower_limit && rShapeFunctions[2] >= lower_limit && rShapeFunctions[0] <= upper_limit && rShapeFunctions[1] <= upper_limit && rShapeFunctions[2] <= upper_limit) //if the xc yc is inside the triangle
      return true;

    return false;
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline Point CurrentCenter(const GeometryType &rGeometry)
  {
    Point center(0,0,0);

    for (auto &i_node : rGeometry)
      center.Coordinates() += i_node.Coordinates();

    const double temp = 1.0/double(rGeometry.size());

    center.Coordinates() *= temp;

    return center;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline Point OriginalCenter(const GeometryType &rGeometry)
  {
    Point center(0,0,0);

    for (auto &i_node : rGeometry)
      center.Coordinates() += i_node.GetInitialPosition() + i_node.FastGetSolutionStepValue(DISPLACEMENT);

    const double temp = 1.0/double(rGeometry.size());

    center.Coordinates() *= temp;

    return center;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CalculatePosition(const GeometryType &rGeometry, const VectorType &rCenter, Vector &rShapeFunctions)
  {
    if (rGeometry.WorkingSpaceDimension() == 2)
    {
      // if (rGeometry.size() == 2) //linear surface line 2D //to implement
      //   return CalculatePosition(rGeometry[0].X(), rGeometry[0].Y(),
      //                            rGeometry[1].X(), rGeometry[1].Y(),
      //                            rCenter[0], rCenter[1], rShapeFunctions;
      if (rGeometry.size() == 3) //linear triangle 2D
        return CalculatePosition(rGeometry[0].X(), rGeometry[0].Y(),
                                 rGeometry[1].X(), rGeometry[1].Y(),
                                 rGeometry[2].X(), rGeometry[2].Y(),
                                 rCenter[0], rCenter[1], rShapeFunctions);
      else
        KRATOS_ERROR << "Non valid Geometry size" << std::endl;
    }
    else if (rGeometry.WorkingSpaceDimension() == 3)
    {
      // if (rGeometry.size() == 3) //linear surface triangle 3D //to implement
      //   return CalculatePosition(rGeometry[0].X(), rGeometry[0].Y(), rGeometry[0].Z(),
      //                            rGeometry[1].X(), rGeometry[1].Y(), rGeometry[1].Z(),
      //                            rGeometry[2].X(), rGeometry[2].Y(), rGeometry[2].Z(),
      //                            rCenter[0], rCenter[1], rCenter[2], rShapeFunctions);
      if (rGeometry.size() == 4) //linear tetrahedron 3D
        return CalculatePosition(rGeometry[0].X(), rGeometry[0].Y(), rGeometry[0].Z(),
                                 rGeometry[1].X(), rGeometry[1].Y(), rGeometry[1].Z(),
                                 rGeometry[2].X(), rGeometry[2].Y(), rGeometry[2].Z(),
                                 rGeometry[3].X(), rGeometry[3].Y(), rGeometry[3].Z(),
                                 rCenter[0], rCenter[1], rCenter[2], rShapeFunctions);
      else
        KRATOS_ERROR << "Non valid Geometry size" << std::endl;
    }
    else
      KRATOS_ERROR << "Non valid WorkingSpaceDimension" << std::endl;

    return false;
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CalculateCircumCenter(const GeometryType &rGeometry, VectorType &rCenter)
  {
    if (rGeometry.WorkingSpaceDimension() == 2)
    {
      if (rGeometry.size() == 2) //linear surface line 2D
        CalculateCenter(rGeometry[0].X(), rGeometry[0].Y(),
                        rGeometry[1].X(), rGeometry[1].Y(),
                        rCenter[0], rCenter[1]);
      else if (rGeometry.size() == 3) //linear triangle 2D
        CalculateCircumCenter(rGeometry[0].X(), rGeometry[0].Y(),
                              rGeometry[1].X(), rGeometry[1].Y(),
                              rGeometry[2].X(), rGeometry[2].Y(),
                              rCenter[0], rCenter[1]);
      else
        KRATOS_ERROR << "Non valid Geometry size" << std::endl;
    }
    else if (rGeometry.WorkingSpaceDimension() == 3)
    {
      if (rGeometry.size() == 3) //linear surface triangle 3D
        CalculateLargeEdgeCenter(rGeometry[0].X(), rGeometry[0].Y(), rGeometry[0].Z(),
                                 rGeometry[1].X(), rGeometry[1].Y(), rGeometry[1].Z(),
                                 rGeometry[2].X(), rGeometry[2].Y(), rGeometry[2].Z(),
                                 rCenter[0], rCenter[1], rCenter[2]);
        // CalculateInnerCircumCenter(rGeometry[0].X(), rGeometry[0].Y(), rGeometry[0].Z(),
        //                            rGeometry[1].X(), rGeometry[1].Y(), rGeometry[1].Z(),
        //                            rGeometry[2].X(), rGeometry[2].Y(), rGeometry[2].Z(),
        //                            rCenter[0], rCenter[1], rCenter[2]);
      else if (rGeometry.size() == 4) //linear tetrahedron 3D
        CalculateCenter(rGeometry[0].X(), rGeometry[0].Y(), rGeometry[0].Z(),
                        rGeometry[1].X(), rGeometry[1].Y(), rGeometry[1].Z(),
                        rGeometry[2].X(), rGeometry[2].Y(), rGeometry[2].Z(),
                        rGeometry[3].X(), rGeometry[3].Y(), rGeometry[3].Z(),
                        rCenter[0], rCenter[1], rCenter[2]);
      else
        KRATOS_ERROR << "Non valid Geometry size" << std::endl;
    }
    else
      KRATOS_ERROR << "Non valid WorkingSpaceDimension" << std::endl;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CalculateCenter(const GeometryType &rGeometry, VectorType &rCenter)
  {
    if (rGeometry.WorkingSpaceDimension() == 2)
    {
      if (rGeometry.size() == 2) //linear surface line 2D
        CalculateCenter(rGeometry[0].X(), rGeometry[0].Y(),
                        rGeometry[1].X(), rGeometry[1].Y(),
                        rCenter[0], rCenter[1]);
      else if (rGeometry.size() == 3) //linear triangle 2D
        CalculateCenter(rGeometry[0].X(), rGeometry[0].Y(),
                        rGeometry[1].X(), rGeometry[1].Y(),
                        rGeometry[2].X(), rGeometry[2].Y(),
                        rCenter[0], rCenter[1]);
      else
        KRATOS_ERROR << "Non valid Geometry size" << std::endl;
    }
    else if (rGeometry.WorkingSpaceDimension() == 3)
    {
      if (rGeometry.size() == 3) //linear surface triangle 3D
        CalculateCenter(rGeometry[0].X(), rGeometry[0].Y(), rGeometry[0].Z(),
                        rGeometry[1].X(), rGeometry[1].Y(), rGeometry[1].Z(),
                        rGeometry[2].X(), rGeometry[2].Y(), rGeometry[2].Z(),
                        rCenter[0], rCenter[1], rCenter[2]);
      else if (rGeometry.size() == 4) //linear tetrahedron 3D
        CalculateCenter(rGeometry[0].X(), rGeometry[0].Y(), rGeometry[0].Z(),
                        rGeometry[1].X(), rGeometry[1].Y(), rGeometry[1].Z(),
                        rGeometry[2].X(), rGeometry[2].Y(), rGeometry[2].Z(),
                        rGeometry[3].X(), rGeometry[3].Y(), rGeometry[3].Z(),
                        rCenter[0], rCenter[1], rCenter[2]);
      else
        KRATOS_ERROR << "Non valid Geometry size" << std::endl;
    }
    else
      KRATOS_ERROR << "Non valid WorkingSpaceDimension" << std::endl;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CalculateCenterAndRadius(const GeometryType &rGeometry, VectorType &rCenter, double &rRadius)
  {
    if (rGeometry.WorkingSpaceDimension() == 2)
    {
      if (rGeometry.size() == 2) //linear surface line 2D
        CalculateCenterAndRadius(rGeometry[0].X(), rGeometry[0].Y(),
                                 rGeometry[1].X(), rGeometry[1].Y(),
                                 rCenter[0], rCenter[1], rRadius);
      else if (rGeometry.size() == 3) //linear triangle 2D
        CalculateCenterAndRadius(rGeometry[0].X(), rGeometry[0].Y(),
                                 rGeometry[1].X(), rGeometry[1].Y(),
                                 rGeometry[2].X(), rGeometry[2].Y(),
                                 rCenter[0], rCenter[1], rRadius);
      else
        KRATOS_ERROR << "Non valid Geometry size" << std::endl;
    }
    else if (rGeometry.WorkingSpaceDimension() == 3)
    {
      if (rGeometry.size() == 3) //linear surface triangle 3D
        CalculateCenterAndRadius(rGeometry[0].X(), rGeometry[0].Y(), rGeometry[0].Z(),
                                 rGeometry[1].X(), rGeometry[1].Y(), rGeometry[1].Z(),
                                 rGeometry[2].X(), rGeometry[2].Y(), rGeometry[2].Z(),
                                 rCenter[0], rCenter[1], rCenter[2], rRadius);
      else if (rGeometry.size() == 4) //linear tetrahedron 3D
        CalculateCenterAndRadius(rGeometry[0].X(), rGeometry[0].Y(), rGeometry[0].Z(),
                                 rGeometry[1].X(), rGeometry[1].Y(), rGeometry[1].Z(),
                                 rGeometry[2].X(), rGeometry[2].Y(), rGeometry[2].Z(),
                                 rGeometry[3].X(), rGeometry[3].Y(), rGeometry[3].Z(),
                                 rCenter[0], rCenter[1], rCenter[2], rRadius);
      else
        KRATOS_ERROR << "Non valid Geometry size" << std::endl;
    }
    else
      KRATOS_ERROR << "Non valid WorkingSpaceDimension" << std::endl;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CalculateCenterAndRadius(const std::vector<VectorType> &rVerticesCoordinates,
                                              VectorType &rCenter, double &rRadius)
  {

    if (rVerticesCoordinates.size() == 3)
    {
      CalculateCenterAndRadius(rVerticesCoordinates[0][0], rVerticesCoordinates[0][1],
                               rVerticesCoordinates[1][0], rVerticesCoordinates[1][1],
                               rVerticesCoordinates[2][0], rVerticesCoordinates[2][1],
                               rCenter[0], rCenter[1], rRadius);
    }
    else if (rVerticesCoordinates.size() == 4)
    {
      CalculateCenterAndRadius(rVerticesCoordinates[0][0], rVerticesCoordinates[0][1], rVerticesCoordinates[0][2],
                               rVerticesCoordinates[1][0], rVerticesCoordinates[1][1], rVerticesCoordinates[1][2],
                               rVerticesCoordinates[2][0], rVerticesCoordinates[2][1], rVerticesCoordinates[2][2],
                               rVerticesCoordinates[3][0], rVerticesCoordinates[3][1], rVerticesCoordinates[3][2],
                               rCenter[0], rCenter[1], rCenter[2], rRadius);
    }
    else
    {
      KRATOS_ERROR << "Number of points supplied out of range ERROR" << std::endl;
    }
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CalculateCenter(const double x0, const double y0,
                                     const double x1, const double y1,
                                     double &xc, double &yc)
  {
    xc = 0.5 * (x0 + x1);
    yc = 0.5 * (y0 + y1);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CalculateCenter(const double x0, const double y0, const double z0,
                                     const double x1, const double y1, const double z1,
                                     const double x2, const double y2, const double z2,
                                     double &xc, double &yc, double &zc)
  {
    xc = 0.3333333333333333333 * (x0 + x1 + x2);
    yc = 0.3333333333333333333 * (y0 + y1 + y2);
    zc = 0.3333333333333333333 * (z0 + z1 + z2);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CalculateCenter(const double x0, const double y0,
                                     const double x1, const double y1,
                                     const double x2, const double y2,
                                     double &xc, double &yc)
  {
    xc = 0.3333333333333333333 * (x0 + x1 + x2);
    yc = 0.3333333333333333333 * (y0 + y1 + y2);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CalculateCenter(const double x0, const double y0, const double z0,
                                     const double x1, const double y1, const double z1,
                                     const double x2, const double y2, const double z2,
                                     const double x3, const double y3, const double z3,
                                     double &xc, double &yc, double &zc)
  {
    xc = 0.25 * (x0 + x1 + x2 + x3);
    yc = 0.25 * (y0 + y1 + y2 + y3);
    zc = 0.25 * (z0 + z1 + z2 + z3);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CalculateCenterAndRadius(const double x0, const double y0,
                                              const double x1, const double y1,
                                              double &xc, double &yc, double &R)
  {
    xc = 0.5 * (x0 + x1);
    yc = 0.5 * (y0 + y1);

    R = sqrt((xc - x0) * (xc - x0) + (yc - y0) * (yc - y0));
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CalculateCenterAndRadius(const double x0, const double y0, const double z0,
                                              const double x1, const double y1, const double z1,
                                              const double x2, const double y2, const double z2,
                                              double &xc, double &yc, double &zc, double &R)
  {
    xc = 0.3333333333333333333 * (x0 + x1 + x2);
    yc = 0.3333333333333333333 * (y0 + y1 + y2);
    zc = 0.3333333333333333333 * (z0 + z1 + z2);

    double R1 = (xc - x0) * (xc - x0) + (yc - y0) * (yc - y0) + (zc - z0) * (zc - z0);
    double R2 = (xc - x1) * (xc - x1) + (yc - y1) * (yc - y1) + (zc - z1) * (zc - z1);
    double R3 = (xc - x2) * (xc - x2) + (yc - y2) * (yc - y2) + (zc - z2) * (zc - z2);

    R = R1;
    if (R2 > R)
      R = R2;
    if (R3 > R)
      R = R3;

    R = sqrt(R);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CalculateCenterAndRadius(const double x0, const double y0,
                                              const double x1, const double y1,
                                              const double x2, const double y2,
                                              double &xc, double &yc, double &R)
  {
    xc = 0.3333333333333333333 * (x0 + x1 + x2);
    yc = 0.3333333333333333333 * (y0 + y1 + y2);

    double R1 = (xc - x0) * (xc - x0) + (yc - y0) * (yc - y0);
    double R2 = (xc - x1) * (xc - x1) + (yc - y1) * (yc - y1);
    double R3 = (xc - x2) * (xc - x2) + (yc - y2) * (yc - y2);

    R = R1;
    if (R2 > R)
      R = R2;
    if (R3 > R)
      R = R3;

    R = sqrt(R);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CalculateCenterAndRadius(const double x0, const double y0, const double z0,
                                              const double x1, const double y1, const double z1,
                                              const double x2, const double y2, const double z2,
                                              const double x3, const double y3, const double z3,
                                              double &xc, double &yc, double &zc, double &R)
  {
    xc = 0.25 * (x0 + x1 + x2 + x3);
    yc = 0.25 * (y0 + y1 + y2 + y3);
    zc = 0.25 * (z0 + z1 + z2 + z3);

    double R1 = (xc - x0) * (xc - x0) + (yc - y0) * (yc - y0) + (zc - z0) * (zc - z0);
    double R2 = (xc - x1) * (xc - x1) + (yc - y1) * (yc - y1) + (zc - z1) * (zc - z1);
    double R3 = (xc - x2) * (xc - x2) + (yc - y2) * (yc - y2) + (zc - z2) * (zc - z2);
    double R4 = (xc - x3) * (xc - x3) + (yc - y3) * (yc - y3) + (zc - z3) * (zc - z3);

    R = R1;
    if (R2 > R)
      R = R2;
    if (R3 > R)
      R = R3;
    if (R4 > R)
      R = R4;

    R = sqrt(R);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void GlobalCoordinates(CoordinatesArrayType &rResult,
                                       GeometryType &rGeometry,
                                       CoordinatesArrayType const& LocalCoordinates)
  {
    noalias(rResult) = ZeroVector(3);

    Vector N( rGeometry.size() );
    rGeometry.ShapeFunctionsValues( N, LocalCoordinates );

    for (IndexType i = 0; i < rGeometry.size(); ++i)
      noalias(rResult) += N[i] * rGeometry[i].Coordinates();
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void GlobalPosition(CoordinatesArrayType &rResult,
                                    GeometryType &rGeometry,
                                    CoordinatesArrayType const& LocalCoordinates)
  {
    noalias(rResult) = ZeroVector(3);

    Vector N( rGeometry.size() );
    rGeometry.ShapeFunctionsValues( N, LocalCoordinates );

    for (IndexType i = 0; i < rGeometry.size(); ++i)
      noalias(rResult) += N[i] * (rGeometry[i].GetInitialPosition() + rGeometry[i].FastGetSolutionStepValue(DISPLACEMENT));
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CircleInterpolation(const GeometryType &rGeometry, array_1d<double, 3> &rVector)
  {
    KRATOS_TRY

    bool is_curved = false;
    std::vector<array_1d<double, 3>> normals;

    is_curved = GeometryUtilities::CheckContactCurvature(rGeometry, normals);

    array_1d<double, 3> normal_direction;
    normal_direction.clear();

    array_1d<double, 3> tangent_direction;
    tangent_direction.clear();

    if (is_curved)
    {

      //compute the saggita
      double projection = 0.0;
      for (unsigned int i = 0; i < 3; i++)
        projection += normals[0][i] * normals[1][i];

      projection = std::sqrt(projection);

      double angle = std::acos(projection);

      double face_size = CalculateMaxEdgeSize(rGeometry);

      double sagitta = 0.5 * face_size * std::tan(0.25 * angle);

      //correction vector according to contact curvature
      normal_direction = normals[0] + normals[1];

      double modulus = norm_2(normal_direction);
      if (modulus)
        normal_direction /= modulus;

      normal_direction *= sagitta;

      //check correct curvature convexity
      tangent_direction = rGeometry[0] - rGeometry[1];
      modulus = norm_2(tangent_direction);
      if (modulus)
        tangent_direction /= modulus;

      //note:  two possible directions depending on the NORMAL_CONTACT definition
      if (inner_prod(normals[0], tangent_direction) > 0)
        normal_direction *= (-1.0);
    }

    rVector = normal_direction;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void HermiteInterpolation(const GeometryType &rGeometry, array_1d<double, 3> &rVector)
  {
    KRATOS_TRY

    bool is_curved = false;
    std::vector<array_1d<double, 3>> normals;

    is_curved = CheckContactCurvature(rGeometry, normals);

    if (is_curved)
    {
      HermiteInterpolation(rGeometry[0], rGeometry[1], normals[0], normals[1], rVector, 0.5);
    }
    else{
      std::cout<<" not curved correction "<<normals<<" node ("<<rGeometry[0].Coordinates()<<", "<< rGeometry[1].Coordinates()<<")"<<std::endl;
    }

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void HermiteTriangleInterpolation(const GeometryType &rGeometry, array_1d<double, 3> &rVector)
  {
    KRATOS_TRY

    bool is_curved = false;
    std::vector<array_1d<double, 3>> normals;

    is_curved = GeometryUtilities::CheckContactCurvature(rGeometry, normals);

    std::cout << " is curved " << is_curved << " normals "<< normals << std::endl;

    for (auto n : normals)
      std::cout << " contact normal " << n << std::endl;

    double baricenter = 2.0 / 3.0;
    array_1d<double, 3> MidPoint;
    MidPoint.clear();

    array_1d<double, 3> Normal;
    Normal.clear();

    std::vector<array_1d<double, 3>> Curves(3);
    std::fill(Curves.begin(), Curves.end(), Normal);

    if (is_curved)
    {

      //first curve (node to face midpoint)
      MidPoint = 0.5 * (rGeometry[2] + rGeometry[1]);
      Normal = 0.5 * (normals[2] + normals[1]);

      HermiteInterpolation(rGeometry[0], MidPoint, normals[0], Normal, Curves[0], baricenter);

      //second curve (node to face midpoint)
      MidPoint = 0.5 * (rGeometry[2] + rGeometry[0]);
      Normal = 0.5 * (normals[2] + normals[0]);

      HermiteInterpolation(rGeometry[1], MidPoint, normals[1], Normal, Curves[1], baricenter);

      //first curve (node to face midpoint)
      MidPoint = 0.5 * (rGeometry[1] + rGeometry[0]);
      Normal = 0.5 * (normals[1] + normals[0]);

      HermiteInterpolation(rGeometry[2], MidPoint, normals[2], Normal, Curves[2], baricenter);

      double max = norm_2(Curves[0]);
      std::cout << " Curves[" << 0 << "] " << Curves[0] << std::endl;
      double ic = 0;
      for (unsigned int i = 1; i < 3; ++i)
      {
        std::cout << " Curves[" << i << "] " << Curves[i] << std::endl;
        double norm = norm_2(Curves[i]);
        if (norm >= max)
        {
          ic = i;
          max = norm;
        }
      }

      rVector += Curves[ic];

      //rVector *= 1.0/3.0;
    }

    std::cout << " correction " << rVector << std::endl;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void HermiteInterpolation(const array_1d<double, 3> &rP1, const array_1d<double, 3> &rP2, const array_1d<double, 3> &rN1, const array_1d<double, 3> &rN2, array_1d<double, 3> &rD, double s)
  {

    KRATOS_TRY

    //compute points distance 1-2
    array_1d<double, 3> T1 = rP2 - rP1;

    //compute tangents
    double projection = 0.0;
    for (unsigned int i = 0; i < 3; ++i)
      projection += T1[i] * rN1[i];

    T1 -= (projection * rN1);

    double modulus = norm_2(T1);
    if (modulus)
      T1 /= modulus;

    //compute points distance 2-1
    array_1d<double, 3> T2 = rP1 - rP2;

    //compute tangents
    projection = 0.0;
    for (unsigned int i = 0; i < 3; ++i)
      projection += T2[i] * rN2[i];

    T2 -= (projection * rN2);

    modulus = norm_2(T2);
    if (modulus)
      T2 /= modulus;

    T2 *= (-1);

    //compute normalized s-point position s in [0,1]
    array_1d<double, 3> M = s * rP2 - s * rP1;

    modulus = norm_2(rP2 - rP1);
    if (modulus)
      projection = norm_2(M) / modulus;

    //hermite basis functions
    double h00 = 2.0 * projection * projection * projection - 3.0 * projection * projection + 1.0;
    double h10 = projection * projection * projection - 2.0 * projection * projection + projection;
    double h01 = -2.0 * projection * projection * projection + 3.0 * projection * projection;
    double h11 = projection * projection * projection - projection * projection;

    //hermite interpolation polinomial
    rD = h00 * rP1;
    rD += h10 * modulus * T1;
    rD += h01 * rP2;
    rD += h11 * modulus * T2;

    //increment of position
    M = (s * rP2 + (1 - s) * rP1);

    rD -= M;

    //note:  two possible directions depending on the NORMAL_CONTACT sign
    //check correct curvature convexity
    T1 = rP2 - rP1;
    modulus = norm_2(T1);
    if (modulus)
      T1 /= modulus;

    if( inner_prod(rN1,rN2)< 0.01 ) //orthogonal
      rD *= 0.0;

    double d1 = inner_prod(rN1, T1);
    double d2 = inner_prod(rN2, -T1);
    std::cout << " d1 " << d1 << " " << rN1 <<" "<< T1 << std::endl;
    std::cout << " d2 " << d2 << " " << rN2 <<" "<< -T1 << std::endl;
    if (d1 * d2 > 0)
    {
      if (d1 > 0)
        rD *= (-1.0);
    }
    else
    {
      if(std::abs(d1)>10*std::abs(d2))
        rD *= (-1.0);
      else if (std::abs(d2)>10*std::abs(d1))
        rD *= 1.0;
      else
        rD *= 0.0;
    }

    //note:  consider allways pointing inside the domain (Jan 2021)
    if(inner_prod(rN1, rD)<0 || inner_prod(rN2, rD)<0) //check convex and reduce distance outside correction
      rD *= -0.5;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool CheckPointInLine(const VectorType& P1, const VectorType& P2, const VectorType& P)
  {
    double side   = norm_2(P1-P2);
    double length = norm_2(P2-P)+norm_2(P1-P);
    double tol    = 1e-6*side;
    if(length<side+tol && length>side-tol)
      return true;

    return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline unsigned int CheckPointInLine(const NodeType &rNode, const GeometryType& rGeometry)
  {
    VectorType P1, P2, P = rNode.Coordinates();
    //line 1:  0-1
    P1 = rGeometry[0].Coordinates();
    P2 = rGeometry[1].Coordinates();
    if(CheckPointInLine(P1,P2,P))
      return 1;

    if (rGeometry.size()==3)
    {
      //line 2:  1-2
      P1 = rGeometry[1].Coordinates();
      P2 = rGeometry[2].Coordinates();
      if(CheckPointInLine(P1,P2,P))
        return 2;
      //line 3:  2-0
      P1 = rGeometry[2].Coordinates();
      P2 = rGeometry[0].Coordinates();
      if(CheckPointInLine(P1,P2,P))
        return 3;
    }

    return 0;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline GeometryType GetBoundaryEdge(const NodeType &rNode, const GeometryType& rGeometry, NodeType::Pointer &pNode)
  {

    //std::cout<<" ID  point "<<rNode.Id()<<" geometry ("<<rGeometry[0].Id()<<","<<rGeometry[1].Id()<<","<<rGeometry[2].Id()<<")"<<std::endl;

    if (rGeometry.size()==3)
    {
      VectorType P = rNode.Coordinates();
      PointsArrayType Points(2);
      //line 1:  0-1
      Points(0) = rGeometry(0);
      Points(1) = rGeometry(1);
      pNode = rGeometry(2);
      if(CheckPointInLine(Points[0].Coordinates(),Points[1].Coordinates(),P))
        return GeometryType(Points);

      //line 2:  1-2
      Points(0) = rGeometry(1);
      Points(1) = rGeometry(2);
      pNode = rGeometry(0);
      if(CheckPointInLine(Points[0].Coordinates(),Points[1].Coordinates(),P))
        return GeometryType(Points);

      //line 3:  2-0
      Points(0) = rGeometry(2);
      Points(1) = rGeometry(0);
      pNode = rGeometry(1);
      if(CheckPointInLine(Points[0].Coordinates(),Points[1].Coordinates(),P))
        return GeometryType(Points);
    }

    return rGeometry;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline GeometryType GetBoundaryGeometry(const NodeType &rNode, const GeometryType& rGeometry)
  {
    if (rGeometry.size()==3)
    {
      VectorType P = rNode.Coordinates();
      PointsArrayType Points(2);
      //line 1:  0-1
      Points(0) = rGeometry(0);
      Points(1) = rGeometry(1);
      if(CheckPointInLine(Points[0].Coordinates(),Points[1].Coordinates(),P))
        return GeometryType(Points);

      //line 2:  1-2
      Points(0) = rGeometry(1);
      Points(1) = rGeometry(2);
      if(CheckPointInLine(Points[0].Coordinates(),Points[1].Coordinates(),P))
        return GeometryType(Points);

      //line 3:  2-0
      Points(0) = rGeometry(2);
      Points(1) = rGeometry(0);
      if(CheckPointInLine(Points[0].Coordinates(),Points[1].Coordinates(),P))
        return GeometryType(Points);
    }

    return rGeometry;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void AddNodeFixedDofs(NodeType &rNode, const GeometryType& rGeometry)
  {
    unsigned int side = 0;
    if (rGeometry.size()>2)
      side = CheckPointInLine(rNode,rGeometry);

    if (side == 0){
      for (auto &i_dof : rNode.GetDofs())
      {
        bool fixed = true;
        for (auto &i_node : rGeometry)
        {
          if (!i_node.IsFixed(i_dof->GetVariable()))
          {
            fixed = false;
            break;
          }
        }
        if (fixed){
          //std::cout<<" Fix "<<i_dof->GetVariable().Name()<<" side "<<side<<std::endl;
          i_dof->FixDof();
        }
      }
    }
    else{
      std::vector<NodeType::Pointer> rPoints;
      if (side == 1)
      {
        rPoints.push_back(rGeometry(0));
        rPoints.push_back(rGeometry(1));
      }
      else if (side == 2)
      {
        rPoints.push_back(rGeometry(1));
        rPoints.push_back(rGeometry(2));
      }
      else if (side == 3)
      {
        rPoints.push_back(rGeometry(2));
        rPoints.push_back(rGeometry(0));
      }

      for (auto &i_dof : rNode.GetDofs())
      {
        bool fixed = true;
        for (auto &i_node : rPoints)
        {
          if (!i_node->IsFixed(i_dof->GetVariable()))
          {
            fixed = false;
            break;
          }
        }
        if (fixed){
          std::cout<<" Fix "<<i_dof->GetVariable().Name()<<" side "<<side<<std::endl;
          i_dof->FixDof();
        }
      }
    }

  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CalculateBoundaryUnitNormal(const GeometryType &rGeometry, array_1d<double,3> &rNormal)
  {
    if (rGeometry.size() == 3)
    {
      array_1d<double,3> v1, v2;
      v1[0] = rGeometry[1].X() - rGeometry[0].X();
      v1[1] = rGeometry[1].Y() - rGeometry[0].Y();
      v1[2] = rGeometry[1].Z() - rGeometry[0].Z();

      v2[0] = rGeometry[2].X() - rGeometry[0].X();
      v2[1] = rGeometry[2].Y() - rGeometry[0].Y();
      v2[2] = rGeometry[2].Z() - rGeometry[0].Z();

      MathUtils<double>::CrossProduct(rNormal, v1, v2);
    }
    else{
      rNormal[0] = rGeometry[1].Y() - rGeometry[0].Y();
      rNormal[1] = -(rGeometry[1].X() - rGeometry[0].X());
      rNormal[2] = 0.00;
    }

    double norm = norm_2(rNormal);
    if (norm!=0)
      rNormal /= norm;
    else
      KRATOS_ERROR <<" norm "<<norm<<" NORMAL "<<rNormal<<std::endl;
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{
  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{
  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{

  /// Assignment operator.
  GeometryUtilities &operator=(GeometryUtilities const &rOther);

  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Unaccessible methods
  ///@{
  ///@}

}; // Class GeometryUtilities

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_GEOMETRY_UTILITIES_HPP_INCLUDED  defined
