//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        March 2021 $
//
//

#if !defined(KRATOS_SEARCH_UTILITIES_HPP_INCLUDED)
#define KRATOS_SEARCH_UTILITIES_HPP_INCLUDED

// External includes

// System includes

// Project includes
#include "spatial_containers/spatial_containers.h"

namespace Kratos
{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
 */
class KRATOS_API(MESHERS_APPLICATION) SearchUtilities
{
public:
  ///@name Type Definitions
  ///@{

  typedef Node<3> NodeType;
  typedef array_1d<double,3> VectorType;
  typedef Geometry<NodeType> GeometryType;
  typedef ModelPart::ConditionType ConditionType;

  typedef PointerVectorSet<ConditionType, IndexedObject> ConditionsContainerType;
  typedef ConditionsContainerType::iterator ConditionIterator;
  typedef ConditionsContainerType::const_iterator ConditionConstantIterator;

  typedef Bucket<3, Node<3>, std::vector<Node<3>::Pointer>, Node<3>::Pointer, std::vector<Node<3>::Pointer>::iterator, std::vector<double>::iterator> BucketType;
  typedef Tree<KDTreePartition<BucketType>> KdtreeType; //Kdtree
  typedef ModelPart::MeshType::GeometryType::PointsArrayType PointsArrayType;


  /// Pointer definition of SearchUtilities
  KRATOS_CLASS_POINTER_DEFINITION(SearchUtilities);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  SearchUtilities() {} //

  /// Destructor.
  virtual ~SearchUtilities() {}

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void GetConditionNodeIndices(const GeometryType &rGeometry, const NodeType &rNode, std::vector<unsigned int> &indices)
  {
    static const std::vector<unsigned int> permuta = {0, 1, 2, 0, 1};
    unsigned int i = FindConditionNodeIndex(rGeometry,rNode);
    indices = {permuta[i],permuta[i+1],permuta[i+2]};
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline unsigned int FindConditionNodeIndex(const GeometryType &rGeometry, const NodeType &rNode)
  {
    for (unsigned int i = 0; i < rGeometry.size(); ++i)
      if (rNode.Id() == rGeometry[i].Id())
        return i;

    return 0;
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline void CreateSearchTree(KdtreeType::Pointer& rSearchTree, std::vector<Node<3>::Pointer>& rTreeKnots, unsigned int bucket_size)
  {
    rSearchTree = Kratos::shared_ptr<KdtreeType>(new KdtreeType(rTreeKnots.begin(), rTreeKnots.end(), bucket_size));
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool FindMainFaceConditionAndReorderVertices(const ModelPart::ConditionsContainerType &rConditions, GeometryType &rGeometry, Condition::Pointer &pMainCondition, DenseMatrix<unsigned int> &lpofa)
  {
    //Ordered vertices
    Geometry<Node<3>> Vertices;

    if (rGeometry.size() == 3)
    { //triangles of 3 nodes

      for (auto i_cond(rConditions.begin()); i_cond != rConditions.end(); ++i_cond)
      {
        //2D edges:
        if (i_cond->IsNot(CONTACT))
        {
          const GeometryType &rConditionGeometry = i_cond->GetGeometry();
          for (unsigned int iface = 0; iface < lpofa.size2(); ++iface)
          {
            //detection for contact elements clockwise numeration of the contact geometry
            if (rConditionGeometry[0].Id() == rGeometry[lpofa(2, iface)].Id() && rConditionGeometry[1].Id() == rGeometry[lpofa(1, iface)].Id())
            {
              Vertices.push_back(rGeometry(lpofa(1, iface)));
              Vertices.push_back(rGeometry(lpofa(2, iface)));
              Vertices.push_back(rGeometry(lpofa(0, iface)));
              rGeometry = Vertices;
              pMainCondition = *i_cond.base();
              return true;
            }
            // else if(rConditionGeometry[0].Id() == rGeometry[lpofa(1, iface)].Id() && rConditionGeometry[1].Id() == rGeometry[lpofa(2, iface)].Id())
            // {
            // 	  Vertices.push_back(rGeometry(lpofa(1, iface)));
            // 	  Vertices.push_back(rGeometry(lpofa(2, iface)));
            // 	  Vertices.push_back(rGeometry(lpofa(0, iface)));
            // 	  rGeometry = Vertices;
            // 	  pMainCondition = *i_cond.base();
            // 	  return true;
            // }
          }
        }
      }
    }
    else if (rGeometry.size() == 4)
    { //tetrahedra of 4 nodes

      for (auto i_cond(rConditions.begin()); i_cond != rConditions.end(); ++i_cond)
      {
        //3D faces:
        if (i_cond->IsNot(CONTACT))
        {
          GeometryType &rConditionGeometry = i_cond->GetGeometry();

          for (unsigned int iface = 0; iface < lpofa.size2(); ++iface)
          {
            //detection for contact elements clockwise numeration of the contact geometry
            if (rConditionGeometry[0].Id() == rGeometry[lpofa(3, iface)].Id() && rConditionGeometry[1].Id() == rGeometry[lpofa(2, iface)].Id() && rConditionGeometry[2].Id() == rGeometry[lpofa(1, iface)].Id())
            {
              Vertices.push_back(rGeometry(lpofa(1, iface)));
              Vertices.push_back(rGeometry(lpofa(3, iface)));
              Vertices.push_back(rGeometry(lpofa(2, iface)));
              Vertices.push_back(rGeometry(lpofa(0, iface)));
              rGeometry = Vertices;
              pMainCondition = *i_cond.base();
              return true;
            }
            else if (rConditionGeometry[0].Id() == rGeometry[lpofa(2, iface)].Id() && rConditionGeometry[1].Id() == rGeometry[lpofa(1, iface)].Id() && rConditionGeometry[2].Id() == rGeometry[lpofa(3, iface)].Id())
            {
              Vertices.push_back(rGeometry(lpofa(1, iface)));
              Vertices.push_back(rGeometry(lpofa(3, iface)));
              Vertices.push_back(rGeometry(lpofa(2, iface)));
              Vertices.push_back(rGeometry(lpofa(0, iface)));
              rGeometry = Vertices;
              pMainCondition = *i_cond.base();
              return true;
            }
            else if (rConditionGeometry[0].Id() == rGeometry[lpofa(1, iface)].Id() && rConditionGeometry[1].Id() == rGeometry[lpofa(3, iface)].Id() && rConditionGeometry[2].Id() == rGeometry[lpofa(2, iface)].Id())
            {
              Vertices.push_back(rGeometry(lpofa(1, iface)));
              Vertices.push_back(rGeometry(lpofa(3, iface)));
              Vertices.push_back(rGeometry(lpofa(2, iface)));
              Vertices.push_back(rGeometry(lpofa(0, iface)));
              rGeometry = Vertices;
              pMainCondition = *i_cond.base();
              return true;
            }
          }
        }
      }
    }

    return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool FindMainEdgeConditionsAndReorderVertices(ModelPart::ConditionsContainerType &rConditions, GeometryType &rGeometry, Condition::Pointer &pMainCondition, GlobalPointersVector<Condition> &rPrimaryConditions, DenseMatrix<unsigned int> &lpofa)
  {
    unsigned int main_conditions = 0;

    //Permute
    static constexpr unsigned int e_permute[7] = {0, 1, 2, 3, 0, 1, 2};
    static constexpr unsigned int c_permute[4] = {0, 1, 2, 0};

    bool found_edge = false;
    bool verified_edge = false;
    std::vector<unsigned int> local_shared_edge(2);

    if (rGeometry.size() == 3)
    { //triangles of 3 nodes
      //skip this case
    }
    else if (rGeometry.size() == 4)
    { //tetraheda of 4 nodes

      bool condition_found = false;
      //check if it is EDGE_TO_EDGE element sharing only edges with the conditions
      for (auto i_cond(rConditions.begin()); i_cond != rConditions.end(); ++i_cond)
      {
        //3D edges: there are 4 possibilities
        if (i_cond->IsNot(CONTACT))
        {
          GeometryType &rConditionGeometry = i_cond->GetGeometry();

          for (unsigned int e_edge = 0; e_edge < 4; ++e_edge)  //element edge  ::  4 possibilities
          {
            for (unsigned int c_edge = 0; c_edge < 3; ++c_edge)  //condition edge  :: 3 possibilities
            {
              for (unsigned int i = 1; i <= 3; ++i) // vertices :: 3 vertices
              {
                if (rConditionGeometry[c_permute[c_edge]].Id() == rGeometry[e_permute[e_edge]].Id() &&
                    rConditionGeometry[c_permute[c_edge+1]].Id() == rGeometry[e_permute[e_edge + i]].Id())
                {
		  if(found_edge && !verified_edge){
		    //check master condition normal to verify is not orthogonal to the contact face
		    unsigned int coincident = 0;
		    const array_1d<double, 3> &rMainNormal = pMainCondition->GetValue(NORMAL);
		    const array_1d<double, 3> &rNormal = i_cond->GetValue(NORMAL);
		    double projection = inner_prod(rNormal, rMainNormal);
		    if (fabs(projection) > 0.955) //same direction
		      coincident += 1;

		    for (auto &i_cond : rPrimaryConditions)
		    {
		      const array_1d<double, 3> &rFaceNormal = i_cond.GetValue(NORMAL);
		      double projection = inner_prod(rFaceNormal, rMainNormal);
		      if (fabs(projection) > 0.955) //not the same direction
			coincident += 1;
		    }

		    if (coincident < 2) //at least, the itself and another face coincident
		      found_edge = false;
		    else
		      verified_edge = true;
		  }

                  if(!found_edge){ //select primary condition (first one)
                    pMainCondition = *i_cond.base();
                    local_shared_edge[0] = e_permute[e_edge];
                    local_shared_edge[1] = e_permute[e_edge+i];
                    found_edge = true;
                  }

                  rPrimaryConditions.push_back(*i_cond.base());
                  ++main_conditions;
                  condition_found = true;
                  break;
                }
              }
              if (condition_found)
                break;
            }
            if (condition_found)
              break;
          }
        }

        if (main_conditions >= 4)  //sharing two edges / 2 faces per edge / maximum 4 primary conditions
          break;
        else
          condition_found = false;
      }
    }

    if (main_conditions == 0)
    {
      std::cout << " WARNING:: Boundary Condition NOT FOUND after CONTACT MESHING SEARCH " << std::endl;
      std::cout << " Condition Nodes[ ";
      for (unsigned int i = 0; i < rGeometry.size(); ++i)
      {
        std::cout << " " << rGeometry[i].Id();
      }
      std::cout << " ]" << std::endl;
      return false;
    }
    else{

      //reorder Vertices
      static constexpr unsigned int f_permute[4] = {2, 3, 1, 2};
      //Ordered vertices
      Geometry<Node<3>> Vertices;
      Vertices.push_back(rGeometry(local_shared_edge[0]));
      Vertices.push_back(rGeometry(local_shared_edge[1]));

      for (unsigned int i = 1; i <= 3; ++i)
      {
        if (lpofa(i,local_shared_edge.front()) == local_shared_edge.back())
        {
          Vertices.push_back(rGeometry(lpofa(f_permute[i-1],local_shared_edge.front())));
          Vertices.push_back(rGeometry(lpofa(f_permute[i],local_shared_edge.front())));
          //std::cout<<" k "<<lpofa(f_permute[i-1],local_shared_edge.front()) <<" l "<<lpofa(f_permute[i],local_shared_edge.front()) <<std::endl;
          break;
        }
      }
      rGeometry = Vertices;

      return true;
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool FindMasterFaceCondition(const ModelPart::ConditionsContainerType &rConditions, const Condition::Pointer &pCondition, Condition::Pointer &pMasterCondition)
  {
    const GeometryType &rGeometry = pCondition->GetGeometry();
    DenseMatrix<unsigned int> lpofa; //points that define the faces
    rGeometry.NodesInFaces(lpofa);

    if (rGeometry.size() == 3)
    { //triangles of 3 nodes

      for (auto i_cond(rConditions.begin()); i_cond != rConditions.end(); ++i_cond)
      {
        //2D edges:
        if (i_cond->IsNot(CONTACT))
        {
          const GeometryType &rConditionGeometry = i_cond->GetGeometry();
          for (unsigned int iface = 0; iface < lpofa.size2(); ++iface)
          {
            if ((rConditionGeometry[0].Id() == rGeometry[lpofa(1, iface)].Id() && rConditionGeometry[1].Id() == rGeometry[lpofa(2, iface)].Id()) ||
                (rConditionGeometry[0].Id() == rGeometry[lpofa(2, iface)].Id() && rConditionGeometry[1].Id() == rGeometry[lpofa(1, iface)].Id()))
            {
              pMasterCondition = *i_cond.base();
              return true;
            }
          }
        }
      }
    }
    else if (rGeometry.size() == 4)
    { //tetraheda of 4 nodes

      for (auto i_cond(rConditions.begin()); i_cond != rConditions.end(); ++i_cond)
      {
        //3D faces:
        if (i_cond->IsNot(CONTACT))
        {
          GeometryType &rConditionGeometry = i_cond->GetGeometry();

          for (unsigned int iface = 0; iface < lpofa.size2(); ++iface)
          {
            //detection for contact elements clockwise numeration of the contact geometry.
            if ((rConditionGeometry[2].Id() == rGeometry[lpofa(1, iface)].Id() && rConditionGeometry[1].Id() == rGeometry[lpofa(2, iface)].Id() && rConditionGeometry[0].Id() == rGeometry[lpofa(3, iface)].Id()) ||
                (rConditionGeometry[2].Id() == rGeometry[lpofa(3, iface)].Id() && rConditionGeometry[1].Id() == rGeometry[lpofa(1, iface)].Id() && rConditionGeometry[0].Id() == rGeometry[lpofa(2, iface)].Id()) ||
                (rConditionGeometry[2].Id() == rGeometry[lpofa(2, iface)].Id() && rConditionGeometry[1].Id() == rGeometry[lpofa(3, iface)].Id() && rConditionGeometry[0].Id() == rGeometry[lpofa(1, iface)].Id()))
            {
              pMasterCondition = *i_cond.base();
              return true;
            }
          }
        }
      }
    }

    pMasterCondition = pCondition;
    // if (rGeometry.size() == 3){
    //   std::cout << " WARNING:: Boundary Condition NOT FOUND after CONTACT MESHING SEARCH " << std::endl;
    //   std::cout << " Condition Nodes[ ";
    //   for (unsigned int i = 0; i < rGeometry.size(); ++i)
    //   {
    //     std::cout << " " << rGeometry[i].Id();
    //   }
    //   std::cout << " ]" << std::endl;
    // }
    return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool FindMasterEdgeConditions(ModelPart::ConditionsContainerType &rConditions, const Condition::Pointer &pCondition, GlobalPointersVector<Condition> &rMasterConditions)
  {
    const GeometryType &rGeometry = pCondition->GetGeometry();
    DenseMatrix<unsigned int> lpofa; //points that define the faces
    rGeometry.NodesInFaces(lpofa);

    unsigned int master_conditions = 0;

    //Permute
    static constexpr unsigned int permute[7] = {0, 1, 2, 3, 0, 1, 2};

    if (rGeometry.size() == 3)
    { //triangles of 3 nodes
      //skip this case
    }
    else if (rGeometry.size() == 4)
    { //tetraheda of 4 nodes

      bool condition_found = false;
      //check if it is EDGE_TO_EDGE element sharing only edges with the conditions
      for (auto i_cond(rConditions.begin()); i_cond != rConditions.end(); ++i_cond)
      {
        //3D edges: there are 4 possibilities
        if (i_cond->IsNot(CONTACT))
        {
          GeometryType &rConditionGeometry = i_cond->GetGeometry();

          for (unsigned int iedge = 0; iedge < 4; ++iedge)
          {
            if (((rConditionGeometry[0].Id() == rGeometry[permute[iedge]].Id() && rConditionGeometry[1].Id() == rGeometry[permute[iedge + 1]].Id()) ||
                 (rConditionGeometry[0].Id() == rGeometry[permute[iedge]].Id() && rConditionGeometry[1].Id() == rGeometry[permute[iedge + 2]].Id()) ||
                 (rConditionGeometry[0].Id() == rGeometry[permute[iedge]].Id() && rConditionGeometry[1].Id() == rGeometry[permute[iedge + 3]].Id())) ||
                ((rConditionGeometry[1].Id() == rGeometry[permute[iedge]].Id() && rConditionGeometry[2].Id() == rGeometry[permute[iedge + 1]].Id()) ||
                 (rConditionGeometry[1].Id() == rGeometry[permute[iedge]].Id() && rConditionGeometry[2].Id() == rGeometry[permute[iedge + 2]].Id()) ||
                 (rConditionGeometry[1].Id() == rGeometry[permute[iedge]].Id() && rConditionGeometry[2].Id() == rGeometry[permute[iedge + 3]].Id())) ||
                ((rConditionGeometry[2].Id() == rGeometry[permute[iedge]].Id() && rConditionGeometry[0].Id() == rGeometry[permute[iedge + 1]].Id()) ||
                 (rConditionGeometry[2].Id() == rGeometry[permute[iedge]].Id() && rConditionGeometry[0].Id() == rGeometry[permute[iedge + 2]].Id()) ||
                 (rConditionGeometry[2].Id() == rGeometry[permute[iedge]].Id() && rConditionGeometry[0].Id() == rGeometry[permute[iedge + 3]].Id())))
            {
              rMasterConditions.push_back(*i_cond.base());
              condition_found = true;
              break;
            }
          }
        }

        if (condition_found)
        {
          ++master_conditions;
          //std::cout<<" Master Edge Condition Found "<<master_conditions<<std::endl;
          if (master_conditions > 3)
            break;
          else
            condition_found = false;
        }
      }
    }

    if (master_conditions == 0)
    {
      std::cout << " WARNING:: Boundary Condition NOT FOUND after CONTACT MESHING SEARCH " << std::endl;

      std::cout << " Condition Nodes[ ";
      for (unsigned int i = 0; i < rGeometry.size(); ++i)
      {
        std::cout << " " << rGeometry[i].Id();
      }
      std::cout << " ]" << std::endl;
      return false;
    }
    else
      return true;

  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool FindMasterCondition(ModelPart::ConditionsContainerType &rConditions, const Condition::Pointer &pCondition, Condition::Pointer &pMasterCondition, NodeType &pSlaveNode)
  {
    const GeometryType &rGeometry = pCondition->GetGeometry();
    DenseMatrix<unsigned int> lpofa; //points that define the faces
    rGeometry.NodesInFaces(lpofa);

    for (auto i_cond(rConditions.begin()); i_cond != rConditions.end(); ++i_cond)
    {
      //2D edges:
      if (i_cond->IsNot(CONTACT))
      {
        const GeometryType &rConditionGeom = i_cond->GetGeometry();

        for (unsigned int i = 0; i < lpofa.size2(); ++i)
        {
          if ((rConditionGeom[0].Id() == rGeometry[lpofa(1, i)].Id() && rConditionGeom[1].Id() == rGeometry[lpofa(2, i)].Id()) ||
              (rConditionGeom[0].Id() == rGeometry[lpofa(2, i)].Id() && rConditionGeom[1].Id() == rGeometry[lpofa(1, i)].Id()))
          {
            pMasterCondition = *i_cond.base();
            pSlaveNode = rGeometry[lpofa(0, i)];
            return true;
          }
        }
      }
    }
    return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool FindCondition(const GeometryType &rConditionGeometry, const GeometryType &rGeometry, const DenseMatrix<unsigned int> &lpofa, const DenseVector<unsigned int> &lnofa, const unsigned int &iface)
  {
    // not equivalent geometry sizes for boundary conditions:
    if (rConditionGeometry.size() != lnofa[iface])
      return false;

    // line boundary condition:
    if (lnofa[iface] == 2)
    {
      if ((rConditionGeometry[0].Id() == rGeometry[lpofa(1, iface)].Id() && rConditionGeometry[1].Id() == rGeometry[lpofa(2, iface)].Id()) ||
          (rConditionGeometry[0].Id() == rGeometry[lpofa(2, iface)].Id() && rConditionGeometry[1].Id() == rGeometry[lpofa(1, iface)].Id()))
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    else if (lnofa[iface] == 3) //3D faces:
    {
      if ((rConditionGeometry[0].Id() == rGeometry[lpofa(1, iface)].Id() && rConditionGeometry[1].Id() == rGeometry[lpofa(2, iface)].Id() && rConditionGeometry[2].Id() == rGeometry[lpofa(3, iface)].Id()) ||
          (rConditionGeometry[0].Id() == rGeometry[lpofa(3, iface)].Id() && rConditionGeometry[1].Id() == rGeometry[lpofa(1, iface)].Id() && rConditionGeometry[2].Id() == rGeometry[lpofa(2, iface)].Id()) ||
          (rConditionGeometry[0].Id() == rGeometry[lpofa(2, iface)].Id() && rConditionGeometry[1].Id() == rGeometry[lpofa(3, iface)].Id() && rConditionGeometry[2].Id() == rGeometry[lpofa(1, iface)].Id()))
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    else
    {
      KRATOS_THROW_ERROR(std::logic_error, "Wrong Condition Number of Face Nodes", lnofa[iface]);
    }

    return false;
  }

  //**************************************************************************
  //**************************************************************************

  static inline bool FindCondition(const GeometryType &rConditionGeometry, const std::vector<unsigned int> &rFace)
  {

    // not equivalent geometry sizes for boundary conditions:
    if (rConditionGeometry.size() != rFace.size())
      return false;

    // line boundary condition:
    if (rFace.size() == 2)
    {
      if ((rConditionGeometry[0].Id() == rFace[0] && rConditionGeometry[1].Id() == rFace[1]) ||
          (rConditionGeometry[0].Id() == rFace[1] && rConditionGeometry[1].Id() == rFace[0]))
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    else if (rFace.size() == 3) //3D faces:
    {
      if ((rConditionGeometry[0].Id() == rFace[0] && rConditionGeometry[1].Id() == rFace[1] && rConditionGeometry[2].Id() == rFace[2]) ||
          (rConditionGeometry[0].Id() == rFace[1] && rConditionGeometry[1].Id() == rFace[2] && rConditionGeometry[2].Id() == rFace[0]) ||
          (rConditionGeometry[0].Id() == rFace[2] && rConditionGeometry[1].Id() == rFace[0] && rConditionGeometry[2].Id() == rFace[1]))
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    else
    {
      KRATOS_THROW_ERROR(std::logic_error, "Wrong Condition Number of Face Nodes: ", rFace.size());
    }

    return false;
  }

  //**************************************************************************
  //**************************************************************************

  static inline bool FindCondition(const std::vector<unsigned int> &cFace, const std::vector<unsigned int> &eFace)
  {

    // not equivalent geometry sizes for boundary conditions:
    if (cFace.size() != eFace.size())
      return false;

    // line boundary condition:
    if (eFace.size() == 2)
    {
      if ((cFace[0] == eFace[0] && cFace[1] == eFace[1]) ||
          (cFace[0] == eFace[1] && cFace[1] == eFace[0]))
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    else if (eFace.size() == 3) //3D faces:
    {
      if ((cFace[0] == eFace[0] && cFace[1] == eFace[1] && cFace[2] == eFace[2]) ||
          (cFace[0] == eFace[1] && cFace[1] == eFace[2] && cFace[2] == eFace[0]) ||
          (cFace[0] == eFace[2] && cFace[1] == eFace[0] && cFace[2] == eFace[1]))
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    else
    {
      KRATOS_THROW_ERROR(std::logic_error, "Wrong Condition Number of Face Nodes: ", eFace.size());
    }

    return false;
  }

  //**************************************************************************
  //**************************************************************************

  static inline bool FindNodeInCondition(const GeometryType &rConditionGeometry, const std::vector<unsigned int> &rFace)
  {

    // not equivalent geometry sizes for boundary conditions:
    if (rConditionGeometry.size() != rFace.size())
      return false;

    // line boundary condition:
    if (rFace.size() == 2)
    {
      if (rConditionGeometry[0].Id() == rFace[0] ||
          rConditionGeometry[1].Id() == rFace[1] ||
          rConditionGeometry[0].Id() == rFace[1] ||
          rConditionGeometry[1].Id() == rFace[0])
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    else if (rFace.size() == 3) //3D faces:
    {
      if (rConditionGeometry[0].Id() == rFace[0] ||
          rConditionGeometry[1].Id() == rFace[1] ||
          rConditionGeometry[2].Id() == rFace[2] ||
          rConditionGeometry[0].Id() == rFace[1] ||
          rConditionGeometry[1].Id() == rFace[2] ||
          rConditionGeometry[2].Id() == rFace[0] ||
          rConditionGeometry[0].Id() == rFace[2] ||
          rConditionGeometry[1].Id() == rFace[0] ||
          rConditionGeometry[2].Id() == rFace[1])
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    else
    {
      KRATOS_THROW_ERROR(std::logic_error, "Wrong Condition Number of Face Nodes: ", rFace.size());
    }

    return false;
  }

  //**************************************************************************
  //**************************************************************************

  static inline bool FindNodeInCondition(const std::vector<unsigned int> &cFace, const std::vector<unsigned int> &eFace)
  {

    // not equivalent geometry sizes for boundary conditions:
    if (cFace.size() != eFace.size())
      return false;

    // line boundary condition:
    if (eFace.size() == 2)
    {
      if (cFace[0] == eFace[0] ||
          cFace[1] == eFace[1] ||
          cFace[0] == eFace[1] ||
          cFace[1] == eFace[0])
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    else if (eFace.size() == 3) //3D faces:
    {
      if (cFace[0] == eFace[0] ||
          cFace[1] == eFace[1] ||
          cFace[2] == eFace[2] ||
          cFace[0] == eFace[1] ||
          cFace[1] == eFace[2] ||
          cFace[2] == eFace[0] ||
          cFace[0] == eFace[2] ||
          cFace[1] == eFace[0] ||
          cFace[2] == eFace[1])
      {
        return true;
      }
      else
      {
        return false;
      }
    }
    else
    {
      KRATOS_THROW_ERROR(std::logic_error, "Wrong Condition Number of Face Nodes: ", eFace.size());
    }

    return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  template <class TDataType>
      static inline void AddUniqueWeakPointer(GlobalPointersVector<TDataType> &v, const typename TDataType::WeakPointer candidate)
  {
    typename GlobalPointersVector<TDataType>::iterator i = v.begin();
    typename GlobalPointersVector<TDataType>::iterator endit = v.end();
    while (i != endit && (i)->Id() != (candidate)->Id())
      ++i;
    if (i == endit)
      v.push_back(candidate);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  template <class TDataType>
      static inline void AddUniqueSharedPointer(std::vector<typename TDataType::Pointer> &v, const typename TDataType::Pointer candidate)
  {
    typename std::vector<typename TDataType::Pointer>::iterator i = v.begin();
    typename std::vector<typename TDataType::Pointer>::iterator endit = v.end();
    while (i != endit && (*i)->Id() != candidate->Id())
      ++i;
    if (i == endit)
      v.push_back(candidate);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool ExtendPatchNeighbours(NodeType &i_node,
                                           ElementWeakPtrVectorType &nElements,
                                           const unsigned int size)
  {
    KRATOS_TRY
    if(i_node.Is(BOUNDARY))
    {
      ExtendNeighbours(i_node, nElements, {RIGID.AsFalse(),BOUNDARY.AsFalse()});
      if (nElements.size()< size)
        ExtendNeighbours(i_node, nElements, {RIGID.AsFalse(),BOUNDARY});
    }
    else{
      ExtendNeighbours(i_node, nElements, {RIGID.AsFalse(),BOUNDARY.AsFalse()});
    }

    if (nElements.size()>size)
      return true;

    return false;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void ExtendNeighbours(NodeType &i_node,
                                      ElementWeakPtrVectorType &nElements,
                                      const std::vector<Flags> Flags = {})
  {
    KRATOS_TRY

    //search closest connected point
    unsigned int id = 0;
    double minimum = std::numeric_limits<double>::max();

    if (!i_node.Has(NEIGHBOUR_NODES))
      std::cout<<" Node ["<<i_node.Id()<<"] HAS NO NEIGHBOUR NODES "<<std::endl;

    NodeWeakPtrVectorType &nNodes = i_node.GetValue(NEIGHBOUR_NODES);
    unsigned int i=0;
    for (auto& i_nnode : nNodes)
    {
      if( (i_nnode.Id() != i_node.Id()) && i_nnode.Is(Flags)){
        const double distance = norm_2(i_node.Coordinates()-i_nnode.Coordinates());
        if(distance<minimum)
        {
          id = i;
          minimum=distance;
        }
      }
      ++i;
    }

    if( id != 0 )
    {
      ElementWeakPtrVectorType &nExtraElements = nNodes[id].GetValue(NEIGHBOUR_ELEMENTS);
      for (auto &i_extra : nExtraElements)
      {
        AddUniqueWeakPointer<Element>(nElements, &i_extra);
      }
    }

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SetNodeNeighbourConditions(std::vector<ConditionType::Pointer> &rConditions, const std::vector<Flags> Flags = {})
  {
    ClearNodeNeighbourConditions(rConditions);
    AddNodeNeighbourConditions(rConditions, Flags);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SetNodeNeighbourConditions(ModelPart::ConditionsContainerType &rConditions, const std::vector<Flags> Flags = {})
  {
    ClearNodeNeighbourConditions(rConditions);
    AddNodeNeighbourConditions(rConditions, Flags);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void AddNodeNeighbourConditions(std::vector<ConditionType::Pointer> &rConditions, const std::vector<Flags> Flags = {})
  {
    KRATOS_TRY

    for (auto i_cond(rConditions.begin()); i_cond != rConditions.end(); ++i_cond)
    {
      if ((*i_cond)->Is(Flags))
        for (auto &i_node : (*i_cond)->GetGeometry())
          i_node.GetValue(NEIGHBOUR_CONDITIONS).push_back(*i_cond);
    }

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void AddNodeNeighbourConditions(ModelPart::ConditionsContainerType &rConditions, const std::vector<Flags> Flags = {})
  {
    KRATOS_TRY

    for (auto i_cond(rConditions.begin()); i_cond != rConditions.end(); ++i_cond)
    {
      if (i_cond->Is(Flags))
        for (auto &i_node : i_cond->GetGeometry())
          i_node.GetValue(NEIGHBOUR_CONDITIONS).push_back(*i_cond.base());
    }

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void ClearNodeNeighbourConditions(std::vector<ConditionType::Pointer> &rConditions, const std::vector<Flags> Flags = {})
  {
    for (auto i_cond(rConditions.begin()); i_cond != rConditions.end(); ++i_cond)
    {
      if ((*i_cond)->Is(Flags))
        for (auto &i_node : (*i_cond)->GetGeometry())
          i_node.GetValue(NEIGHBOUR_CONDITIONS).clear();
    }
  }


  //*******************************************************************************************
  //*******************************************************************************************

  static inline void ClearNodeNeighbourConditions(ModelPart::ConditionsContainerType &rConditions, const std::vector<Flags> Flags = {})
  {
    for (auto i_cond(rConditions.begin()); i_cond != rConditions.end(); ++i_cond)
    {
      if (i_cond->Is(Flags))
        for (auto &i_node : i_cond->GetGeometry())
          i_node.GetValue(NEIGHBOUR_CONDITIONS).clear();
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool FindEdgeSharedCondition(const unsigned int &rId, std::vector<ConditionType::Pointer> &rConditions, const GeometryType& rEdgeGeometry, Condition::Pointer &pEdgeSharedCondition, NodeType::Pointer &pOtherNode)
  {
    DenseMatrix<unsigned int> lpofa; //points that define the faces
    (*rConditions.front()).GetGeometry().NodesInFaces(lpofa);

    for (auto i_cond(rConditions.begin()); i_cond != rConditions.end(); ++i_cond)
    {
      if((*i_cond)->Id() != rId){
        const GeometryType &rGeometry = (*i_cond)->GetGeometry();

        for (unsigned int i = 0; i < lpofa.size2(); ++i)
        {
          if ((rEdgeGeometry[0].Id() == rGeometry[lpofa(1, i)].Id() && rEdgeGeometry[1].Id() == rGeometry[lpofa(2, i)].Id()) ||
              (rEdgeGeometry[0].Id() == rGeometry[lpofa(2, i)].Id() && rEdgeGeometry[1].Id() == rGeometry[lpofa(1, i)].Id()))
          {
            pEdgeSharedCondition = *i_cond;
            pOtherNode = rGeometry(lpofa(0, i));
            //std::cout<<" geometry ID "<<(*i_cond)->Id()<<" != "<<rId<<" ("<<rGeometry[0].Id()<<","<<rGeometry[1].Id()<<","<<rGeometry[2].Id()<<")"<<std::endl;

            return true;
          }
        }
      }
    }

    return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline bool FindEdgeSharedCondition(const unsigned int &rId, ModelPart::ConditionsContainerType &rConditions, const GeometryType& rEdgeGeometry, Condition::Pointer &pEdgeSharedCondition, NodeType::Pointer &pOtherNode)
  {
    DenseMatrix<unsigned int> lpofa; //points that define the faces
    rConditions.begin()->GetGeometry().NodesInFaces(lpofa);

    for (auto i_cond(rConditions.begin()); i_cond != rConditions.end(); ++i_cond)
    {
      if(i_cond->Id() != rId){
        const GeometryType &rGeometry = i_cond->GetGeometry();

        for (unsigned int i = 0; i < lpofa.size2(); ++i)
        {
          if ((rEdgeGeometry[0].Id() == rGeometry[lpofa(1, i)].Id() && rEdgeGeometry[1].Id() == rGeometry[lpofa(2, i)].Id()) ||
              (rEdgeGeometry[0].Id() == rGeometry[lpofa(2, i)].Id() && rEdgeGeometry[1].Id() == rGeometry[lpofa(1, i)].Id()))
          {
            pEdgeSharedCondition = *i_cond.base();
            pOtherNode = rGeometry(lpofa(0, i));
            //std::cout<<" All geometry ID "<<i_cond->Id()<<" != "<<rId<<" ("<<rGeometry[0].Id()<<","<<rGeometry[1].Id()<<","<<rGeometry[2].Id()<<")"<<std::endl;

            return true;
          }
        }
      }
    }

    return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  static inline void SetConditionToErase(Condition &rCondition, bool erase = true)
  {
    //set to erase
    rCondition.Set(NEW_ENTITY, !erase);
    rCondition.Set(TO_ERASE, erase);

    ConditionsContainerType &ChildrenConditions = rCondition.GetValue(CHILDREN_CONDITIONS);
    for (ConditionConstantIterator cn = ChildrenConditions.begin(); cn != ChildrenConditions.end(); ++cn)
      cn->Set(TO_ERASE, erase);
    //std::cout<<" Condition "<<rCondition.Id()<<" set to erase "<<std::endl;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  template< class T >
  static inline void ReorderWithIndices(std::vector<T>& v, const std::vector<size_t>& order)  {
    for ( int s = 1, d; s < order.size(); ++ s ) {
      for ( d = order[s]; d < s; d = order[d] ) ;
      if ( d == s ) while ( d = order[d], d != s ) swap( v[s], v[d] );
    }
  }

  template< class T , class Q>
  static inline void ReorderWithIndices(std::vector<T>& v, std::vector<Q>& n, const std::vector<size_t>& order)  {
    for ( int s = 1, d; s < order.size(); ++ s ) {
      for ( d = order[s]; d < s; d = order[d] ) ;
      if ( d == s ) while ( d = order[d], d != s ){
          swap( v[s], v[d] );
          swap( n[s], n[d] );
        }
    }
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{
  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{
  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{

  /// Assignment operator.
  SearchUtilities &operator=(SearchUtilities const &rOther);

  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Unaccessible methods
  ///@{
  ///@}

}; // Class SearchUtilities

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_SEARCH_UTILITIES_HPP_INCLUDED  defined
