//
//   Project Name:        KratosSolversApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:      October 2022 $
//
//

#if !defined(KRATOS_EXECUTION_CONTROL_HPP_INCLUDED)
#define KRATOS_EXECUTION_CONTROL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "includes/kratos_parameters.h"

namespace Kratos
{

///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Methods to give support to time interval
 */
class ExecutionControl
{
  ///@name Type Definitions
  ///@{

  /// Pointer definition of ExecutionControl
  KRATOS_CLASS_POINTER_DEFINITION(ExecutionControl);

protected:

  struct ControlData
  {
    double time_counter, delta_time, next_time_execution;
    int step_counter, delta_step, next_step_execution;
    bool is_time;
  };


public:

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  ExecutionControl() : mpProcessInfo(nullptr){};

  /// Constructor.
  ExecutionControl(ProcessInfo &rCurrentProcessInfo, std::string type, double time_step) : mpProcessInfo(&rCurrentProcessInfo)
  {
    KRATOS_TRY

    if (type == "time"){
      mControl.is_time = true;
      mControl.delta_time = time_step;
    }
    else{
      mControl.is_time = false;
      mControl.delta_step = (int)time_step;
    }

    Initialize();

    KRATOS_CATCH("")

  }

  /// Destructor.
  virtual ~ExecutionControl(){}

  ///@}
  ///@name Operators
  ///@{
  ///@}
  ///@name Operations
  ///@{
  ///@}
  ///@name Access
  ///@{

  void Initialize()
  {
    if (mControl.is_time){
      if (IsRestarted()){
     	mControl.next_time_execution = (*mpProcessInfo)[TIME];
	mControl.time_counter = mControl.next_time_execution;
      }
      else{
	mControl.next_time_execution = mControl.delta_time;
	mControl.time_counter = 0.0;
      }
    }
    else{
      if (IsRestarted()){
	mControl.next_step_execution = (*mpProcessInfo)[STEP];
	mControl.step_counter = mControl.next_step_execution;
      }
      else{
	mControl.next_step_execution = mControl.delta_step;
	mControl.step_counter = 0;
      }
    }
  }

  void Update()
  {
    if (mControl.is_time)
      mControl.time_counter = (*mpProcessInfo)[TIME];
    else
      mControl.step_counter += 1;
  }

  double GetExecutionTime() const
  {
    return mControl.next_time_execution;
  }

  int GetExecutionStep() const
  {
    return mControl.next_step_execution;
  }

  ///@}
  ///@name Inquiry
  ///@{

  void SetNextExecution()
  {
    if (mControl.is_time){
      if (mControl.next_time_execution <= mControl.time_counter)
	mControl.next_time_execution += mControl.delta_time;
    }
    else{
      if (mControl.next_step_execution <= mControl.step_counter)
	mControl.next_step_execution += mControl.delta_step;
    }
  }


  bool IsExecutionStep()
  {
    if (mControl.is_time){
      if (mControl.time_counter>=mControl.next_time_execution)
	return true;
      else
	return false;
    }
    else{
      if (mControl.step_counter>=mControl.next_step_execution)
	return true;
      else
	return false;
    }
  }

  bool Execute()
  {
    Update();
    if(IsExecutionStep()){
      SetNextExecution();
      return true;
    }
    return false;
  }

  bool IsRestarted() const
  {
    return (*mpProcessInfo)[IS_RESTARTED];
  }

  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}
private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{

  ProcessInfo* mpProcessInfo;

  ControlData mControl;

  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Unaccessible methods
  ///@{
  ///@}

}; // Class ExecutionControl


} // namespace Kratos.

#endif // KRATOS_EXECUTION_CONTROL_HPP_INCLUDED
