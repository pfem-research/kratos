Definition of the use and meaning of flags in Pfem Applications :

##### Usage in NODES:

## Domain definition flags: (related to the type of material)
FLUID                                      # fluid node
RIGID                                      # rigid node
SOLID                                      # solid node

## Rigid bodies:
MASTER                                     # main node defining the kinematics (Center of Gravity for free rigid body elements)
SLAVE                                      # slave node (the MASTER node is defining the kinematics) : RIGID node in rigid body elements

## Characteristic domain definition: (related to the position of the node)
BOUNDARY                                   # boundary node
CONTACT                                    # contact boundary node
INLET                                      # inlet boundary node
SLIP                                       # slip boundary node (for the eulerian slip conditions)
INTERFACE                                  # fixed node or with an applied boundary condition
OUTLET                                     # layer next to free_surface
OLD_ENTITY                                 # layer of structure next to free_surface

STRUCTURE : (RIGID || SOLID) && BOUNDARY   # used to identify fluid structrural boundaries
FREE_SURFACE : FLUID && BOUNDARY           # node belonging to a fluid boundary exterior face (some STRUCTURE nodes can be FREE_SURFACE as they belong to an exterior face)

INTERACTION                                # node belonging to the layer next to STRUCTURE boundary
INSIDE                                     # inner eulerian node

ISOLATED                                   # flying node (not belonging to any element)
MARKER                                     # node belonging to an Sliver element

## Global wildcard flags to identify entities:

## Used in step processes: (valid only during the remeshing procedure and next calculation step)
NEW_ENTITY                                 # inserted new node (eulerian strategy checks and modifies kinematics)
MODIFIED                                   # define a moved boundary node (instead of being erased)

## Used in remeshing processes: (valid only during the remeshing procedure)
TO_ERASE                                   # node to be erased
TO_REFINE                                  # node belonging to an element to be refined

## Local wildcard flags to identify entities:

## Used in remeshing processes: (valid only inside a particular remeshing process)
TO_SPLIT                                   # node belonging to an edge to be splitted
VISITED
SELECTED
BLOCKED

# Unused flags:
THERMAL
ACTIVE
MPI_BOUNDARY
PERIODIC


##### Usage in ELEMENTS:

## Domain definition flags: (related to the type of material / domain)
FLUID                                      # fluid element
RIGID                                      # rigid element
SOLID                                      # solid element
CONTACT                                    # contact element
THERMAL                                    # thermal element
STRUCTURE                                  # structural element (beam, shell)

## Characteristic domain definition:
ACTIVE                                     # element to be considered in the computation
ISOLATED                                   # edge element of formed by FREE_SURFACE flying nodes and neighbours only
BLOCKED                                    # non-compute active element
FREE_SURFACE                               # boundary element with a face in the free surface
BOUNDARY                                   # belongs to the free surface boundary layer
INTERACTION				   # belongs to the interaction boundary layer
OUTLET                                     # type of free surface isolated and rigid element : fluid computation purposes

## Global wildcard flags to identify entities:

## Used in remeshing processes: (valid only during the remeshing procedure)
NEW_ENTITY                                 # inserted new element
TO_ERASE                                   # element to be erased
TO_REFINE                                  # element to be refined

VISITED

##### Usage in CONDITIONS:

## Domain definition flags: (related to the type of material / domain)
CONTACT                                    # contact condition
THERMAL                                    # thermal condition
BOUNDARY                                   # composite condition

## Characteristic domain definition:
ACTIVE                                     # condition to be considered in the computation
FREE_SURFACE                               # edged tetrahedron contact condition / no face in the domain surface
INTERACTION				   # belongs to the interaction boundary layer

## Global wildcard flags to identify entities:

## Used in remeshing processes: (valid only during the remeshing procedure)
NEW_ENTITY                                 # inserted new condition
TO_ERASE                                   # condition to be erased
TO_REFINE                                  # condition to be refined


##### Usage in MODEL PARTS:

## Domain definition flags: (related to the type of material / domain)
FLUID                                      # fluid modelpart
RIGID                                      # rigid modelpart
SOLID                                      # solid modelpart
CONTACT                                    # contact modelpart
THERMAL                                    # thermal modelpart

## Characteristic domain definition:
ACTIVE                                     # modelpart to be considered in the computation
BOUNDARY                                   # modelpart containing boundary conditions

##### Usage in PROCESS INFO:

## Domain definition flags: (related to the type of material)
FLUID                                      # fluid problem
SOLID                                      # solid problem
THERMAL                                    # thermal problem