//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

#if !defined(KRATOS_BUILD_MODEL_PART_BOUNDARY_PROCESS_HPP_INCLUDED)
#define KRATOS_BUILD_MODEL_PART_BOUNDARY_PROCESS_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "geometries/line_2d_2.h"
#include "geometries/line_2d_3.h"
#include "geometries/line_3d_2.h"
#include "geometries/triangle_3d_3.h"
#include "geometries/triangle_3d_6.h"

#include "custom_conditions/composite_condition.hpp"
#include "custom_utilities/boundary_normals_utilities.hpp"
#include "custom_utilities/set_flags_utilities.hpp"
#include "custom_processes/mesher_process.hpp"

namespace Kratos
{

///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
 */
class BuildModelPartBoundaryProcess
    : public MesherProcess
{
public:
  ///@name Type Definitions
  ///@{
  typedef Node<3> NodeType;
  typedef array_1d<double,3> VectorType;
  typedef ModelPart::ConditionType ConditionType;
  typedef ConditionType::GeometryType GeometryType;
  typedef GeometryType::PointsArrayType PointsArrayType;

  typedef Node<3>::WeakPointer NodeWeakPtrType;
  typedef Element::WeakPointer ElementWeakPtrType;
  typedef Condition::WeakPointer ConditionWeakPtrType;

  typedef ModelPart::NodesContainerType NodesContainerType;
  typedef ModelPart::ElementsContainerType ElementsContainerType;
  typedef ModelPart::ConditionsContainerType ConditionsContainerType;

  typedef GlobalPointersVector<Node<3>> NodeWeakPtrVectorType;
  typedef GlobalPointersVector<Element> ElementWeakPtrVectorType;
  typedef GlobalPointersVector<Condition> ConditionWeakPtrVectorType;

  /// Pointer definition of BuildModelPartBoundaryProcess
  KRATOS_CLASS_POINTER_DEFINITION(BuildModelPartBoundaryProcess);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  BuildModelPartBoundaryProcess(ModelPart &rModelPart,
                                std::string const rModelPartName,
                                int EchoLevel = 0)
      : mrModelPart(rModelPart)
  {
    mModelPartName = rModelPartName;
    mEchoLevel = EchoLevel;
  }

  /// Destructor.
  virtual ~BuildModelPartBoundaryProcess()
  {
  }

  ///@}
  ///@name Operators
  ///@{

  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  void Execute() override
  {

    KRATOS_TRY

    bool success = false;

    //double begin_time = OpenMPUtils::GetCurrentTime();

    unsigned int NumberOfSubModelParts = mrModelPart.NumberOfSubModelParts();

    if (mModelPartName == mrModelPart.Name())
    {

      for (auto &i_mp : mrModelPart.SubModelParts())
      {

        if (mEchoLevel >= 1)
          std::cout << " [ Construct Boundary on ModelPart [" << i_mp.Name() << "] ]" << std::endl;

        success = UniqueSkinSearch(i_mp);

        if (!success)
        {
          std::cout << "  ERROR: BOUNDARY CONSTRUCTION FAILED ModelPart : [" << i_mp.Name() << "] " << std::endl;
        }
        // else
        // {
        //   //PrintSkin(i_mp);
        // }
      }
    }
    else
    {

      if (mEchoLevel >= 1)
        std::cout << " [ Construct Boundary on ModelPart[" << mModelPartName << "] ]" << std::endl;

      ModelPart &rModelPart = mrModelPart.GetSubModelPart(mModelPartName);
      success = UniqueSkinSearch(rModelPart);

      if (!success)
      {
        std::cout << "  ERROR: BOUNDARY CONSTRUCTION FAILED on ModelPart : [" << rModelPart.Name() << "] " << std::endl;
      }
      // else
      // {
      //   //PrintSkin(rModelPart);
      // }
    }

    if (NumberOfSubModelParts > 1)
    {
      SetMainModelPartConditions();
      SetComputingModelPart();
    }

    // if( mEchoLevel >= 0 ){
    //   double end_time = OpenMPUtils::GetCurrentTime();
    //   std::cout<<"  BUILD MODEl Time = "<<end_time-begin_time<<std::endl;
    // }
    // begin_time = OpenMPUtils::GetCurrentTime();

    //ComputeBoundaryNormals BoundUtils;
    // BoundaryNormalsUtilities BoundaryComputation;
    // if (mModelPartName == mrModelPart.Name())
    // {
    //   BoundaryComputation.CalculateWeightedBoundaryNormals(mrModelPart, mEchoLevel);
    // }
    // else
    // {
    //   ModelPart &rModelPart = mrModelPart.GetSubModelPart(mModelPartName);
    //   BoundaryComputation.CalculateWeightedBoundaryNormals(rModelPart, mEchoLevel);
    // }

    // if (mEchoLevel >= 1)
    //   std::cout << "  Boundary Normals Computed and Assigned ] " << std::endl;

    // if( mEchoLevel >= 0 ){
    //   double end_time = OpenMPUtils::GetCurrentTime();
    //   std::cout<<"  CALCULATE NORMALS Time = "<<end_time-begin_time<<" ]"<<std::endl;
    // }

    KRATOS_CATCH("")
  }

  void CheckMasterElement(int Id, const ElementWeakPtrType &old_nelem, const ElementWeakPtrType &new_nelem)
  {
    if (mEchoLevel > 1)
    {
      if (old_nelem.get()->Id() != new_nelem.get()->Id())
        std::cout << "Condition " << Id << " WARNING: master elements (" << old_nelem.get()->Id() << " != " << new_nelem.get()->Id() << ")" << std::endl;
    }
  }

  void CheckMasterNode(int Id, const GlobalPointer<Node<3>> &old_nnode, const Node<3>::Pointer &new_nnode)
  {
    if (mEchoLevel >= 1)
    {
      if (old_nnode->Id() != new_nnode->Id())
        std::cout << "Condition " << Id << " WARNING: master nodes (" << old_nnode->Id() << " != " << new_nnode->Id() << ")" << std::endl;
    }
  }


  bool ClearConditionMasters()
  {
    KRATOS_TRY

    this->ClearMasterEntities(mrModelPart.Conditions());

    return true;

    KRATOS_CATCH("")
  }


  bool SearchConditionMasters()
  {
    KRATOS_TRY

    //NODE NEIGHBOUR CONDITIONS
    ModelPartUtilities::SetNodeNeighbourConditions(mrModelPart);
    std::cout<<" SET NODE NEIGHBOUR CONDITIONS "<<std::endl;

    int composite_conditions = 0;
    int total_conditions = 0;
    int counter = 0;

    bool found = false;

    bool first_assignment_nodes = false;
    bool first_assignment_elements = false;

    if (mEchoLevel > 1)
      std::cout << " [ START SEARCH CONDITIONS MASTERS : " << std::endl;

    for (auto &i_cond : mrModelPart.Conditions())
    {
      if (i_cond.Is(BOUNDARY)) //composite condition
        ++composite_conditions;

      if (mEchoLevel > 2)
      {
        std::cout << " Masters::Condition (" << i_cond.Id() << ")";
        ElementWeakPtrVectorType &MasterElements = i_cond.GetValue(PRIMARY_ELEMENTS);
        if (MasterElements.size() != 0)
        {
          // if(!MasterElements(0).expired())
          std::cout << " ME size "<<MasterElements.size()<<std::endl;
          std::cout << " ME=" << MasterElements.front().Id() << " (size:" << MasterElements.size() << ")";
        }
        NodeWeakPtrVectorType &MasterNodes = i_cond.GetValue(PRIMARY_NODES);
        if (MasterNodes.size() != 0)
        {
          // if(!MasterNodes(0).expired())
          std::cout << " MN= " << MasterNodes.front().Id() << " (size:" << MasterNodes.size() << ")";
        }
        std::cout << std::endl;
      }

      //********************************************************************

      DenseMatrix<unsigned int> lpofa; //connectivities of points defining faces
      DenseVector<unsigned int> lnofa; //number of points defining faces

      GeometryType &cGeometry = i_cond.GetGeometry();
      unsigned int size = cGeometry.size();

      bool perform_search = true;
      for (unsigned int i = 0; i < size; ++i)
      {
        if (cGeometry[i].Is(RIGID)) //if is a rigid wall do not search; else do search
          perform_search = false;
      }

      if (i_cond.Is(CONTACT))
        perform_search = false;

      //********************************************************************
      found = false;

      if (perform_search)
      {
        if (size == 2)
        {
          ElementWeakPtrVectorType &nElements1 = cGeometry[0].GetValue(NEIGHBOUR_ELEMENTS);
          ElementWeakPtrVectorType &nElements2 = cGeometry[1].GetValue(NEIGHBOUR_ELEMENTS);

          if (nElements1.size() == 0 || nElements2.size() == 0)
            KRATOS_WARNING("") << " NO SIZE in NEIGHBOUR_ELEMENTS " << std::endl;

          for (auto i_nelem(nElements1.begin()); i_nelem != nElements1.end(); ++i_nelem)
          {
            //if(!i_nelem.base()->expired()){

            for (auto j_nelem(nElements2.begin()); j_nelem != nElements2.end(); ++j_nelem)
            {
              //if(!j_nelem.base()->expired()){

              if (j_nelem->Id() == i_nelem->Id() && !found)
              {

                ElementWeakPtrVectorType &rMasterElements = i_cond.GetValue(PRIMARY_ELEMENTS);

                if (rMasterElements.size())
                {
                  this->CheckMasterElement(i_cond.Id(), rMasterElements(0), *i_nelem.base());
                  rMasterElements.clear();
                }
                else
                {
                  first_assignment_elements = true;
                }

                rMasterElements.push_back(*i_nelem.base());

                GeometryType &eGeometry = i_nelem->GetGeometry();

                //get matrix nodes in faces
                eGeometry.NodesInFaces(lpofa);
                eGeometry.NumberNodesInFaces(lnofa);

                int node = 0;
                for (unsigned int iface = 0; iface < eGeometry.size(); ++iface)
                {
                  std::vector<unsigned int> element_face;
                  for (unsigned int i = 1; i <= lnofa[iface]; ++i)
                    element_face.push_back(eGeometry[lpofa(i, iface)].Id());

                  found = SearchUtilities::FindCondition(cGeometry, element_face);
                  //found = SearchUtilities.FindCondition(cGeometry,eGeometry,lpofa,lnofa,iface);

                  if (found)
                  {
                    node = iface;
                    break;
                  }
                }

                if (found)
                {
                  NodeWeakPtrVectorType &rMasterNodes = i_cond.GetValue(PRIMARY_NODES);

                  if (rMasterNodes.size())
                  {
                    this->CheckMasterNode(i_cond.Id(), rMasterNodes(0), eGeometry(lpofa(0, node)));
                    rMasterNodes.clear();
                  }
                  else
                  {
                    first_assignment_nodes = true;
                  }

                  rMasterNodes.push_back(eGeometry(lpofa(0, node)));
                }
                else
                {
                  std::cout << " 2N Geometry MAIN_NODE not FOUND : something is wrong " << std::endl;
                }
              }
              //}
            }
            //}
          }

        }
        if (size == 3)
        {

          ElementWeakPtrVectorType &nElements1 = cGeometry[0].GetValue(NEIGHBOUR_ELEMENTS);
          ElementWeakPtrVectorType &nElements2 = cGeometry[1].GetValue(NEIGHBOUR_ELEMENTS);
          ElementWeakPtrVectorType &nElements3 = cGeometry[2].GetValue(NEIGHBOUR_ELEMENTS);

          if (nElements1.size() == 0 || nElements2.size() == 0 || nElements3.size() == 0)
            KRATOS_WARNING("") << " NO SIZE in NEIGHBOUR_ELEMENTS " << std::endl;

          for (auto i_nelem(nElements1.begin()); i_nelem != nElements1.end(); ++i_nelem)
          {
            //if(!i_nelem.base()->expired()){
            for (auto j_nelem(nElements2.begin()); j_nelem != nElements2.end(); ++j_nelem)
            {
              //if(!j_nelem.base()->expired()){
              if (j_nelem->Id() == i_nelem->Id() && !found)
              {
                for (auto k_nelem(nElements3.begin()); k_nelem != nElements3.end(); ++k_nelem)
                {
                  //if(!k_nelem.base()->expired()){
                  if (k_nelem->Id() == i_nelem->Id() && !found)
                  {
                    ElementWeakPtrVectorType &rMasterElements = i_cond.GetValue(PRIMARY_ELEMENTS);

                    if (rMasterElements.size())
                    {
                      this->CheckMasterElement(i_cond.Id(), rMasterElements(0), *i_nelem.base());
                      rMasterElements.clear();
                    }
                    else
                    {
                      first_assignment_elements = true;
                    }

                    rMasterElements.push_back(*i_nelem.base());

                    GeometryType &eGeometry = i_nelem->GetGeometry();

                    //get matrix nodes in faces
                    eGeometry.NodesInFaces(lpofa);
                    eGeometry.NumberNodesInFaces(lnofa);

                    int node = 0;
                    for (unsigned int iface = 0; iface < eGeometry.size(); ++iface)
                    {
                      std::vector<unsigned int> element_face;
                      for (unsigned int i = 1; i <= lnofa[iface]; ++i)
                        element_face.push_back(eGeometry[lpofa(i, iface)].Id());

                      found = SearchUtilities::FindCondition(cGeometry, element_face);
                      // found = SearchUtilities::FindCondition(cGeometry,eGeometry,lpofa,lnofa,iface);

                      if (found)
                      {
                        node = iface;
                        break;
                      }
                    }

                    if (found)
                    {

                      NodeWeakPtrVectorType &rMasterNodes = i_cond.GetValue(PRIMARY_NODES);

                      if (rMasterNodes.size())
                      {
                        this->CheckMasterNode(i_cond.Id(), rMasterNodes(0), eGeometry(lpofa(0, node)));
                        rMasterNodes.clear();
                      }
                      else
                      {
                        first_assignment_nodes = true;
                      }

                      rMasterNodes.push_back(eGeometry(lpofa(0, node)));
                    }
                    else
                    {
                      std::cout << " 3N Geometry MAIN_NODE not FOUND : something is wrong " << std::endl;
                    }
                  }
                  //}
                }
              }
              //}
            }
            //}
          }
        }

        ++total_conditions;
      }


      //********************************************************************

      if (mEchoLevel > 2)
      {
        std::cout << " SearchResult::Condition (" << i_cond.Id() << ")";
        ElementWeakPtrVectorType MasterElements = i_cond.GetValue(PRIMARY_ELEMENTS);
        if (MasterElements.size() != 0)
        {
          //if(!MasterElements(0).expired())
          std::cout << " ME=" << MasterElements.front().Id() << " (size:" << MasterElements.size() << ")";
        }
        NodeWeakPtrVectorType MasterNodes = i_cond.GetValue(PRIMARY_NODES);
        if (MasterNodes.size() != 0)
        {
          //if(!MasterNodes(0).expired())
          std::cout << " MN= " << MasterNodes.front().Id() << " (size:" << MasterNodes.size() << ")";
        }
        std::cout << std::endl;
      }

      if (found)
        ++counter;
    }

    //********************************************************************

    if (mEchoLevel >= 1)
    {
      if (first_assignment_elements)
        std::cout << " First Assignment of Master Elements " << std::endl;
      if (first_assignment_nodes)
        std::cout << " First Assignment of Master Nodes " << std::endl;
    }

    if (counter == total_conditions)
    {
      if (mEchoLevel >= 1)
        std::cout << "   Condition Masters (ModelPart " << mrModelPart.Name() << "): LOCATED [" << counter << "]" << std::endl;
      found = true;
    }
    else
    {
      if (mEchoLevel >= 1)
        std::cout << "   Condition Masters (ModelPart " << mrModelPart.Name() << "): not LOCATED [" << counter - total_conditions << "]" << std::endl;
      found = false;
    }

    if (counter != composite_conditions)
      if (mEchoLevel >= 1)
        std::cout << "   Condition Masters (ModelPart " << mrModelPart.Name() << "): LOCATED [" << counter << "] COMPOSITE [" << composite_conditions << "] NO MATCH" << std::endl;

    if (mEchoLevel > 1)
      std::cout << "   END SEARCH CONDITIONS MASTERS ] " << found << std::endl;

    return found;

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "BuildModelPartBoundaryProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "BuildModelPartBoundaryProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ModelPart &mrModelPart;

  std::string mModelPartName;

  int mEchoLevel;

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  bool ClearMasterEntities(ModelPart::ConditionsContainerType &rConditions)
  {
    KRATOS_TRY

    for (auto &i_cond : rConditions)
    {
      i_cond.GetValue(PRIMARY_ELEMENTS).clear();
      i_cond.GetValue(PRIMARY_NODES).clear();

      KRATOS_ERROR_IF(i_cond.GetValue(PRIMARY_ELEMENTS).size() || i_cond.GetValue(PRIMARY_NODES).size()) << " Master Entities not cleared " << std::endl;
    }

    return true;

    KRATOS_CATCH("")
  }

  bool UniqueSkinSearch(ModelPart &rModelPart)
  {

    KRATOS_TRY

    if (mEchoLevel > 0)
    {
      std::cout << " [ Initial Conditions : " << rModelPart.Conditions().size() << std::endl;
    }

    if (!rModelPart.Elements().size() || (rModelPart.Is(ACTIVE)))
    {
      if (mEchoLevel > 0)
      {
        std::cout << " [ Final Conditions   : " << rModelPart.Conditions().size() << std::endl;
      }
      return true;
    }

    BuiltinTimer time_conditions_counter;

    //check if a remesh process has been performed and there is any node to erase
    bool any_node_to_erase = false;
    for (auto &i_node : rModelPart.Nodes())
    {
      if (any_node_to_erase == false)
        if (i_node.Is(TO_ERASE))
          any_node_to_erase = true;
    }

    //swap conditions for a temporary use
    unsigned int id = 1;
    ModelPart::ConditionsContainerType TemporaryConditions;

    std::vector<int> PreservedConditions;

    //if there are no conditions check main modelpart mesh conditions
    if (rModelPart.Conditions().size() == 0)
    {
      ModelPart::ConditionsContainerType &rConditions = rModelPart.GetParentModelPart().Conditions();

      std::cout<<" conditions size 0 "<<rModelPart.GetParentModelPart().Name()<<" size "<<rConditions.size()<<std::endl;
      for (auto i_cond(rConditions.begin()); i_cond != rConditions.end(); ++i_cond)
      {
        TemporaryConditions.push_back(*i_cond.base());
        i_cond->SetId(id++);
      }

      //control the previous mesh conditions
      PreservedConditions.resize(TemporaryConditions.size() + 1);
      std::fill(PreservedConditions.begin(), PreservedConditions.end(), 1); // to not add to model part

    }
    else
    {
      //set consecutive ids in the mesh conditions
      if (any_node_to_erase)
      {
        ModelPart::ConditionsContainerType EmptyConditions; //empties modelpart conditions
        EmptyConditions.reserve(rModelPart.Conditions().size());
        EmptyConditions.swap(rModelPart.Conditions());

        TemporaryConditions.reserve(rModelPart.Conditions().size());

        for (auto i_cond(EmptyConditions.begin()); i_cond != EmptyConditions.end(); ++i_cond)
        {
          GeometryType &cGeometry = i_cond->GetGeometry();
          for (unsigned int i = 0; i < cGeometry.size(); ++i)
          {
            if (cGeometry[i].Is(TO_ERASE))
            {
              SearchUtilities::SetConditionToErase(*i_cond);
              std::cout<<" Set Condition ["<<i_cond->Id()<<"] TO_ERASE in build_model_part_boundary_process.hpp; node: "<<cGeometry[i].Id()<<std::endl;
              break;
            }
          }

          if (i_cond->IsNot(TO_ERASE))
          {
            i_cond->SetId(id++);
            TemporaryConditions.push_back(*i_cond.base());
          }
        }
      }
      else
      {
        if( mEchoLevel > 0 )
          std::cout<<"  ModelPart ["<<rModelPart.Name()<<"] conditions "<<rModelPart.Conditions().size()<<std::endl;
        TemporaryConditions.swap(rModelPart.Conditions());
        for (auto &i_cond : TemporaryConditions)
          i_cond.SetId(id++);
        if( mEchoLevel > 0 )
          std::cout<<"  Temporary conditions "<<TemporaryConditions.size()<<std::endl;
      }

      //control the previous mesh conditions
      PreservedConditions.resize(TemporaryConditions.size() + 1);
      std::fill(PreservedConditions.begin(), PreservedConditions.end(), 0);

    }
    if( mEchoLevel > 0 )
      std::cout<<"  PRESERVE conditions time = "<< time_conditions_counter.ElapsedSeconds() <<" "<<TemporaryConditions.size()<<std::endl;

    BuiltinTimer time_build_conditions;
    //build new skin for the Modelpart
    this->BuildCompositeConditions(rModelPart, TemporaryConditions, PreservedConditions, id);

    if( mEchoLevel > 0 )
      std::cout<<"  COMPOSITE conditions time = "<< time_build_conditions.ElapsedSeconds() <<" "<<TemporaryConditions.size()<<std::endl;

    BuiltinTimer time_add_conditions;
    //add other conditions out of the skin space dimension
    this->AddOtherConditions(rModelPart, TemporaryConditions, PreservedConditions, id);

    if( mEchoLevel > 0 )
      std::cout<<"  ADD conditions time = "<< time_add_conditions.ElapsedSeconds() <<" "<<TemporaryConditions.size()<<std::endl;

    return true;

    KRATOS_CATCH("")
  }


  virtual bool BuildCompositeConditions(ModelPart &rModelPart, ModelPart::ConditionsContainerType &rTemporaryConditions, std::vector<int> &rPreservedConditions, unsigned int &rId)
  {

    KRATOS_TRY

    //master conditions must be deleted and set them again in the build
    this->ClearMasterEntities(rTemporaryConditions);

    //properties to be used in the generation
    int number_properties = rModelPart.GetParentModelPart().NumberOfProperties();
    if (number_properties < 0)
      KRATOS_ERROR << " number of properties is " << number_properties << std::endl;

    Properties::Pointer properties = rModelPart.GetParentModelPart().GetMesh().pGetProperties(number_properties - 1);

    for (auto &i_cond : rTemporaryConditions)
    {
      if (i_cond.Is(BOUNDARY))
      { //composite condition
        if (i_cond.HasProperties())
        {
          properties = i_cond.pGetProperties();
          break;
        }
      }
    }

    //create faces vector
    std::vector<std::vector<unsigned int>> Faces;
    for (auto &i_cond : rTemporaryConditions)
    {
      std::vector<unsigned int> face;
      GeometryType &cGeometry = i_cond.GetGeometry();
      for (auto &i_node : cGeometry)
        face.push_back(i_node.Id());
      Faces.push_back(face);
    }

    ElementsContainerType &rElements = rModelPart.Elements();

    rId = 1;
    // double begin_time = OpenMPUtils::GetCurrentTime();

    // double max_time = 0;
    // double accumulated_time = 0;

    DenseMatrix<unsigned int> lpofa; //connectivities of points defining faces
    DenseVector<unsigned int> lnofa; //number of points defining faces

    for (auto i_elem(rElements.begin()); i_elem != rElements.end(); ++i_elem)
    {
      GeometryType &eGeometry = i_elem->GetGeometry();
      if (eGeometry.WorkingSpaceDimension() == eGeometry.LocalSpaceDimension())
      { //3 or 4
        //lpofa
        //********************************************************************
        /*each face is opposite to the corresponding node number so in 2D triangle
          0 ----- 1 2
          1 ----- 2 0
          2 ----- 0 1
        */

        /*each face is opposite to the corresponding node number so in 3D tetrahedron
          0 ----- 1 2 3
          1 ----- 2 0 3
          2 ----- 0 1 3
          3 ----- 0 2 1
        */
        //********************************************************************

        //finding boundaries and creating the "skin"
        ElementWeakPtrVectorType &nElements = i_elem->GetValue(NEIGHBOUR_ELEMENTS);

        //get matrix nodes in faces
        eGeometry.NodesInFaces(lpofa);
        eGeometry.NumberNodesInFaces(lnofa);

        //loop on neighbour elements of an element
        unsigned int iface = 0;
        for (auto &i_nelem : nElements)
        {
          if (i_nelem.Id() == i_elem->Id()) //means no neighbour element in that face
          {
            // double begin_itime = OpenMPUtils::GetCurrentTime();

            Condition::Pointer pCondition = this->GenerateCondition(rTemporaryConditions, rPreservedConditions, Faces, eGeometry, lpofa, lnofa, iface, properties, rId);

            // double end_itime = OpenMPUtils::GetCurrentTime();

            // accumulated_time += end_itime-begin_itime;

            // if( max_time<end_itime-begin_itime )
            //   max_time = end_itime-begin_itime;

            // usually one MasterElement and one MasterNode for 2D and 3D simplex
            // can be more than one in other geometries -> it has to be extended to that cases

            //std::cout<<" ID "<<pCondition->Id()<<" MASTER ELEMENT "<<i_elem->Id()<<std::endl;
            //std::cout<<" MASTER NODE "<<eGeometry[lpofa(0,iface)].Id()<<" or "<<eGeometry[lpofa(NumberNodesInFace,iface)].Id()<<std::endl;
            ElementWeakPtrVectorType &MasterElements = pCondition->GetValue(PRIMARY_ELEMENTS);
            MasterElements.push_back(*i_elem.base());

            NodeWeakPtrVectorType &MasterNodes = pCondition->GetValue(PRIMARY_NODES);
            MasterNodes.push_back(eGeometry(lpofa(0, iface)));

            rModelPart.Conditions().push_back(pCondition);
          } //end face condition

          ++iface;
        } //end loop neighbours
      }
    }
    if( mEchoLevel > 0 )
      std::cout << "  COMPOSITE CONDITIONS BUILD " << rId << std::endl;

    // if( mEchoLevel > 0 ){
    //   double end_time = OpenMPUtils::GetCurrentTime();
    //   std::cout<<"    -*- COMPOSITE ELEMENTS Time = "<<end_time-begin_time<<" accum_time "<<accumulated_time<<" max "<<max_time<<std::endl;
    // }

    return true;

    KRATOS_CATCH("")
  }

  virtual Condition::Pointer GenerateCondition(ModelPart::ConditionsContainerType &rTemporaryConditions, std::vector<int> &rPreservedConditions, std::vector<std::vector<unsigned int>> &rFaces, Geometry<Node<3>> &eGeometry, DenseMatrix<unsigned int> &lpofa, DenseVector<unsigned int> &lnofa, unsigned int &iface, Properties::Pointer &rProperties, unsigned int &rId)
  {
    KRATOS_TRY

    unsigned int NumberNodesInFace = lnofa[iface];

    //1.- create geometry: points array and geometry type
    Condition::NodesArrayType FaceNodes;
    Condition::GeometryType::Pointer ConditionVertices;

    FaceNodes.reserve(NumberNodesInFace);

    for (unsigned int j = 1; j <= NumberNodesInFace; ++j)
    {
      FaceNodes.push_back(eGeometry(lpofa(j, iface)));
    }

    const unsigned int dimension = eGeometry.WorkingSpaceDimension();

    if (NumberNodesInFace == 2)
    {
      if (dimension == 2)
        ConditionVertices = Kratos::make_shared<Line2D2<Node<3>>>(FaceNodes);
      else
        ConditionVertices = Kratos::make_shared<Line3D2<Node<3>>>(FaceNodes);
    }
    else if (NumberNodesInFace == 3)
    {
      if (dimension == 2)
        ConditionVertices = Kratos::make_shared<Line2D3<Node<3>>>(FaceNodes);
      else
        ConditionVertices = Kratos::make_shared<Triangle3D3<Node<3>>>(FaceNodes);
    }

    //Create a composite condition
    CompositeCondition::Pointer pCondition = Kratos::make_intrusive<CompositeCondition>(rId++, ConditionVertices, rProperties);

    bool condition_found = false;

    std::vector<unsigned int> element_face;
    for (unsigned int i = 1; i <= lnofa[iface]; ++i)
      element_face.push_back(eGeometry[lpofa(i, iface)].Id());

    // Search for existing conditions: start
    for (auto i_cond(rTemporaryConditions.begin()); i_cond != rTemporaryConditions.end(); ++i_cond)
    {
      GeometryType &cGeometry = i_cond->GetGeometry();
      if (cGeometry.PointsNumber() > 1)
      {
        condition_found = SearchUtilities::FindCondition(cGeometry, element_face);
        if (condition_found)
        {
          pCondition->AddChild(*i_cond.base());
          rPreservedConditions[i_cond->Id()] += 1;
        }
      }
    }
    // Search for existing conditions: end

    return pCondition;

    KRATOS_CATCH("")
  }

  bool FindConditionID(ModelPart &rModelPart, GeometryType &rGeometry)
  {
    KRATOS_TRY

    //check if the condition exists and belongs to the modelpart checking node Ids
    for (unsigned int i = 0; i < rGeometry.size(); ++i)
    {
      for (auto &i_node : rModelPart.Nodes())
      {
        if (rGeometry[i].Id() == i_node.Id())
          return true;
      }
    }

    return false;

    KRATOS_CATCH("")
  }

  void PrintSkin(ModelPart &rModelPart)
  {

    KRATOS_TRY

    //PRINT SKIN:
    std::cout << " CONDITIONS: geometry nodes (" << rModelPart.Conditions().size() << ")" << std::endl;

    for (auto &i_cond : rModelPart.Conditions())
    {
      GeometryType &cGeometry = i_cond.GetGeometry();
      std::cout << "[" << i_cond.Id() << "]:" << std::endl;
      //i_cond.PrintInfo(std::cout);
      std::cout << "( ";
      for (unsigned int i = 0; i < cGeometry.size(); ++i)
      {
        std::cout << cGeometry[i].Id() << ", ";
      }
      std::cout << " ): ";

      i_cond.GetValue(PRIMARY_ELEMENTS).front().PrintInfo(std::cout);

      std::cout << std::endl;
    }
    std::cout << std::endl;

    KRATOS_CATCH("")
  }

  bool AddOtherConditions(ModelPart &rModelPart, ModelPart::ConditionsContainerType &rTemporaryConditions, std::vector<int> &rPreservedConditions, unsigned int &rId)
  {

    KRATOS_TRY

    //unsigned int counter = 0;
    ModelPart::ConditionsContainerType new_condition_list;

    //add all previous conditions not found in the skin search:
    for (auto &i_cond : rTemporaryConditions)
    {
      if (i_cond.IsNot(CONTACT) && i_cond.IsNot(TO_ERASE) && rPreservedConditions[i_cond.Id()] == 0)
      { //I have not used the condition and any node of the condition
        if (this->CheckAcceptedCondition(rModelPart, i_cond))
        {
          GeometryType &cGeometry = i_cond.GetGeometry();

          Condition::NodesArrayType FaceNodes;

          FaceNodes.reserve(cGeometry.size());

          for (unsigned int j = 0; j < cGeometry.size(); ++j)
          {
            FaceNodes.push_back(cGeometry(j));
          }

          rPreservedConditions[i_cond.Id()] += 1;

          std::cout<<" Condition Added "<<i_cond.Id()<<std::endl;

          Condition::Pointer pCondition = i_cond.Clone(rId++, FaceNodes);

          //this->AddConditionToModelPart(rModelPart, pCondition);
          new_condition_list.push_back(pCondition);
          //counter++;
        }
      }
    }

    rModelPart.AddConditions(new_condition_list.begin(), new_condition_list.end());

    //control if all previous conditions have been added:
    bool all_assigned = true;
    unsigned int lost_conditions = 0;
    for (unsigned int i = 1; i < rPreservedConditions.size(); ++i)
    {
      if (rPreservedConditions[i] == 0)
      {
        //std::cout<<" Condition "<<i<<" lost "<<std::endl;
        all_assigned = false;
        lost_conditions++;
      }
    }

    if (mEchoLevel >= 1)
    {
      std::cout << "   Final Conditions   : " << rModelPart.NumberOfConditions() << std::endl;
      if (all_assigned == true)
        std::cout << "   ALL_PREVIOUS_CONDITIONS_RELOCATED " << std::endl;
      else
        std::cout << "   SOME_PREVIOUS_CONDITIONS_ARE_LOST [lost_conditions:" << lost_conditions << "]" << std::endl;
    }

    return all_assigned;

    KRATOS_CATCH("")
  }

  virtual bool CheckAcceptedCondition(ModelPart &rModelPart, Condition &rCondition)
  {
    KRATOS_TRY

    GeometryType &cGeometry = rCondition.GetGeometry();

    bool accepted = FindConditionID(rModelPart, cGeometry);

    return accepted;

    KRATOS_CATCH("")
  }

  virtual void AddConditionToModelPart(ModelPart &rModelPart, Condition::Pointer pCondition)
  {
    KRATOS_TRY

    rModelPart.AddCondition(pCondition);
    //rModelPart.Conditions().push_back(pCondition);

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{

  void SetComputingModelPart()
  {
    KRATOS_TRY

    std::string ComputingModelPartName;
    for (auto &i_mp : mrModelPart.SubModelParts())
    {
      if (i_mp.Is(ACTIVE) && i_mp.IsNot(THERMAL))
        ComputingModelPartName = i_mp.Name();
    }

    ModelPart &rModelPart = mrModelPart.GetSubModelPart(ComputingModelPartName);

    if (mEchoLevel >= 1)
    {
      std::cout << " [" << ComputingModelPartName << " :: CONDITIONS [OLD:" << rModelPart.NumberOfConditions();
    }

    ModelPart::ConditionsContainerType KeepConditions;

    for (auto &i_mp : mrModelPart.SubModelParts())
    {
      if (i_mp.NumberOfElements() && ComputingModelPartName != i_mp.Name())
      { //conditions of model_parts with elements only

        ModelPart::ConditionsContainerType &rConditions = i_mp.Conditions();

        for (auto i_cond(rConditions.begin()); i_cond != rConditions.end(); ++i_cond)
        {
          // i_cond->PrintInfo(std::cout);
          // std::cout<<" -- "<<std::endl;
          KeepConditions.push_back(*i_cond.base());
          // KeepConditions.back().PrintInfo(std::cout);
          // std::cout<<std::endl;
        }
      }
    }

    rModelPart.Conditions().swap(KeepConditions);

    if (mEchoLevel >= 1)
    {
      std::cout << " / NEW:" << rModelPart.NumberOfConditions() << "] " << std::endl;
    }

    KRATOS_CATCH("")
  }

  void SetMainModelPartConditions()
  {

    KRATOS_TRY

    if (mEchoLevel >= 1)
    {
      std::cout << " [" << mrModelPart.Name() << " :: CONDITIONS [OLD:" << mrModelPart.NumberOfConditions();
    }

    //contact conditions are located on Mesh_0
    ModelPart::ConditionsContainerType KeepConditions;

    unsigned int condId = 1;
    if (mModelPartName == mrModelPart.Name())
    {

      for (auto &i_mp : mrModelPart.SubModelParts())
      {
        if (!(i_mp.Is(ACTIVE)) && !(i_mp.Is(CONTACT)))
        {
          //std::cout<<" ModelPartName "<<i_mp.Name()<<" conditions "<<i_mp.NumberOfConditions()<<std::endl;
          ModelPart::ConditionsContainerType &rConditions = i_mp.Conditions();

          for (auto i_cond(rConditions.begin()); i_cond != rConditions.end(); ++i_cond)
          {
            // i_cond.PrintInfo(std::cout);
            // std::cout<<" -- "<<std::endl;
            KeepConditions.push_back(*(i_cond.base()));
            KeepConditions.back().SetId(condId);
            condId += 1;
            // KeepConditions.back().PrintInfo(std::cout);
            // std::cout<<std::endl;
          }
        }
      }
    }

    ModelPart::ConditionsContainerType &rConditions = mrModelPart.Conditions();

    for (auto i_cond(rConditions.begin()); i_cond != rConditions.end(); ++i_cond)
    {
      if (i_cond->Is(CONTACT))
      {
        KeepConditions.push_back(*i_cond.base());
        KeepConditions.back().SetId(condId);
        condId += 1;
        //std::cout<<" -- "<<std::endl;
        //KeepConditions.back().PrintInfo(std::cout);
        //std::cout<<std::endl;
      }
    }

    mrModelPart.Conditions().swap(KeepConditions);

    if (mEchoLevel >= 1)
    {
      std::cout << " / NEW:" << mrModelPart.NumberOfConditions() << "] " << std::endl;
    }

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{

  /// Assignment operator.
  BuildModelPartBoundaryProcess &operator=(BuildModelPartBoundaryProcess const &rOther);

  /// Copy constructor.
  //BuildModelPartBoundaryProcess(BuildModelPartBoundaryProcess const& rOther);

  ///@}

}; // Class BuildModelPartBoundaryProcess

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                BuildModelPartBoundaryProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const BuildModelPartBoundaryProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_BUILD_MODEL_PART_BOUNDARY_PROCESS_HPP_INCLUDED  defined
