//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:      October 2022 $
//
//

#if !defined(KRATOS_SET_FLAGS_TO_ENTITIES_PROCESS_HPP_INCLUDED)
#define KRATOS_SET_FLAGS_TO_ENTITIES_PROCESS_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_utilities/geometry_utilities.hpp"


namespace Kratos
{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

typedef Node<3> NodeType;
typedef array_1d<double,3> VectorType;
typedef Geometry<NodeType> GeometryType;
typedef GeometryType::PointsArrayType PointsArrayType;
typedef ModelPart::NodesContainerType NodesContainerType;
typedef ModelPart::ElementsContainerType ElementsContainerType;

typedef Node<3>::WeakPointer NodeWeakPtrType;
typedef Element::WeakPointer ElementWeakPtrType;
typedef Condition::WeakPointer ConditionWeakPtrType;

typedef GlobalPointersVector<Node<3>> NodeWeakPtrVectorType;
typedef GlobalPointersVector<Element> ElementWeakPtrVectorType;
typedef GlobalPointersVector<Condition> ConditionWeakPtrVectorType;

///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
 */
class SetFlagsToEntitiesProcess
    : public Process
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of SetFlagsToEntitiesProcess
  KRATOS_CLASS_POINTER_DEFINITION(SetFlagsToEntitiesProcess);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  SetFlagsToEntitiesProcess(ModelPart &rModelPart, int EchoLevel = 0)
      : mrModelPart(rModelPart)
  {
    mEchoLevel = EchoLevel;
  }

  /// Destructor.
  virtual ~SetFlagsToEntitiesProcess()
  {
  }

  ///@}
  ///@name Operators
  ///@{

  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  /// this function is designed for being called after the neighbour searches and boundary construction
  void Execute() override
  {
    KRATOS_TRY

    //Set flags to be taken into account during the computation and meshing

    //(Nodes) FLUID|SOLID|STRUCTURE|BOUNDARY|FREE_SURFACE|INTERACTION
    //(Elements) BOUNDARY|FREE_SURFACE|ISOLATED
    //(Conditions)

    //Assign boundary flags: (Nodes) BOUNDARY|FREE_SURFACE|INTERACTION (Element) BOUNDARY|FREE_SURFACE|ISOLATED
    this->SetBoundaryFlagsToDomainModelParts(mrModelPart);

    //SetFlagsUtilities::CountFlagsInEntities(mrModelPart,"Nodes",{BOUNDARY});
    //SetFlagsUtilities::CountFlagsInEntities(mrModelPart,"Nodes",{RIGID});

    KRATOS_CATCH("")
  }


  /// this function is designed for being called at the beginning of the computations
  /// right after reading the model and the groups
  void ExecuteInitialize() override
  {
    KRATOS_TRY

    //Set flags to be taken into account during the meshing

    //(Nodes) NEW_ENTITY|TO_ERASE|TO_REFINE (Elements) TO_ERASE|TO_REFINE (Conditions) TO_ERASE|TO_REFINE

    std::vector<Flags> FlagsToReset = {NEW_ENTITY.AsFalse(), MODIFIED.AsFalse()};
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{}}, FlagsToReset);
    SetFlagsUtilities::SetFlagsToElements(mrModelPart, {{}}, FlagsToReset);
    SetFlagsUtilities::SetFlagsToConditions(mrModelPart, {{}}, FlagsToReset);

    //Set flags : (Nodes) INTERFACE (includes INLET nodes¿?)
    SetFlagsUtilities::SetFlagsToFixedDofs(mrModelPart);


    KRATOS_CATCH("")
  }

  /// this function is designed for being called at the end of the computations
  /// right after reading the model and the groups
  void ExecuteFinalize() override
  {
    KRATOS_TRY

    //Set flags to be taken into account during the computation and next meshing

    //(Nodes) FLUID|SOLID|STRUCTURE|BOUNDARY|FREE_SURFACE|INTERACTION|OUTLET || ISOLATED|MARKER
    //(Elements) BOUNDARY|FREE_SURFACE|ISOLATED|OUTLET                       ||          MARKER
    //(Conditions)

    //Check and Clean meshing flags:
    std::vector<Flags> NodalFlagsToReset = {TO_ERASE.AsFalse(), TO_REFINE.AsFalse()};
    //SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{}}, NodalFlagsToReset);
    for(auto &i_flag : NodalFlagsToReset)
      SetFlagsUtilities::CheckAndResetNodeFlag(mrModelPart,i_flag);

    std::vector<Flags> FlagsToReset = {NEW_ENTITY.AsFalse()};
    SetFlagsUtilities::SetFlagsToConditions(mrModelPart, {{}}, FlagsToReset);
    SetFlagsUtilities::SetFlagsToElements(mrModelPart, {{}}, FlagsToReset);

    //Assign domain definition flags: (Nodes) SOLID FLUID
    SetFlagsUtilities::SetFlagsToDomainModelParts(mrModelPart);

    //Assign boundary flags: (Nodes) BOUNDARY|FREE_SURFACE|INTERACTION|OUTLET (Element) BOUNDARY|FREE_SURFACE|ISOLATED
    this->SetBoundaryFlagsToDomainModelParts(mrModelPart);

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "SetFlagsToEntitiesProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "SetFlagsToEntitiesProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{

  ModelPart &mrModelPart;

  int mEchoLevel;

  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{

  //*******************************************************************************************
  //*******************************************************************************************

  //set flag to domain nodes (the only with new_entities present)
  void SetBoundaryFlagsToDomainModelParts(ModelPart &rModelPart)
  {
    KRATOS_TRY

    for (auto &i_mp : rModelPart.SubModelParts())
    {
      if (i_mp.Is(SOLID)){
        SetFlagsUtilities::SetFlagsToBoundaryFaces(i_mp);
        //set node layer INTERACTION to STRUCTURE nodes
        //SetFlagsUtilities::SetFlagsToOtherElementNodes(i_mp,{STRUCTURE},{INTERACTION});
        //set as BOUNDARY elements with no face in the boundary
        //SetFlagsUtilities::SetFlagsToElementsIfExistInNodes(i_mp);
      }
      else if (i_mp.Is(RIGID)){
        this->SetFlagsToRigidBoundaryFaces(i_mp);
        //set as BOUNDARY elements with no face in the boundary
        //SetFlagsUtilities::SetFlagsToElementsIfExistInNodes(i_mp);
      }
    }

    for (auto &i_mp : rModelPart.SubModelParts())
    {
      if (i_mp.Is(FLUID)){
        this->SetFlagsToFluidBoundaryFaces(i_mp);
        //set node layer OUTLET to FREE_SURFACE nodes
        SetFlagsUtilities::SetFlagsToOtherElementNodes(i_mp,{FREE_SURFACE},{OUTLET});
        //set as BOUNDARY elements with no face in the boundary
        SetFlagsUtilities::SetFlagsToElementsIfExistInNodes(i_mp);
      }
    }

    //set STRUCTURE to ((RIGID || SOLID) && BOUNDARY) nodes
    SetFlagsUtilities::SetFlagsToNodes(rModelPart, {{RIGID,BOUNDARY},{SOLID,BOUNDARY}}, {STRUCTURE});

    for (auto &i_mp : rModelPart.SubModelParts())
    {
      if (i_mp.Is(FLUID)){
        //set wall layer nodes : set INTERACTION to non STRUCTURE nodes in a element with STRUCTURE nodes
        SetFlagsUtilities::SetFlagsToOtherElementNodes(i_mp,{STRUCTURE},{INTERACTION}); //still not structure at the begining
        //set node layer INTERACTION to STRUCTURE elements
        SetFlagsUtilities::SetFlagsToElementsIfExistInFaceNodes(i_mp,{STRUCTURE},{INTERACTION});
        //set node layer INTERACTION to STRUCTURE conditions
        SetFlagsUtilities::SetFlagsToConditions(i_mp,{{FREE_SURFACE}},{INTERACTION.AsFalse(),FREE_SURFACE.AsFalse()});
        SetFlagsUtilities::SetFlagsToConditionsIfExistInNodes(i_mp, {STRUCTURE.AsFalse(), FREE_SURFACE}, {FREE_SURFACE});
        SetFlagsUtilities::SetFlagsToConditionsIfExistInNodes(i_mp, {STRUCTURE, FREE_SURFACE}, {INTERACTION});
      }
      else if (i_mp.Is(RIGID) || i_mp.Is(SOLID)){
        //set node layer OUTLET to FREE_SURFACE STRUCTURE nodes
        // std::cout<<" Assigning OLD_ENTITY "<<i_mp.NumberOfElements()<<std::endl;
        // std::cout<<" Structure nodes "<<SetFlagsUtilities::CountFlagsInEntities(i_mp,"Nodes",{STRUCTURE})<<std::endl;
        // std::cout<<" FreeSurface nodes "<<SetFlagsUtilities::CountFlagsInEntities(i_mp,"Nodes",{FREE_SURFACE})<<std::endl;
        SetFlagsUtilities::SetFlagsToNodes(i_mp,{{STRUCTURE}},{OLD_ENTITY.AsFalse()});
        SetFlagsUtilities::SetFlagsToOtherElementNodes(i_mp,{FLUID},{OLD_ENTITY});
        SetFlagsUtilities::SetFlagsToNodes(i_mp,{{FLUID},{STRUCTURE.AsFalse()}},{OLD_ENTITY.AsFalse()});
        // std::cout<<" Old entity nodes "<<SetFlagsUtilities::CountFlagsInEntities(i_mp,"Nodes",{OLD_ENTITY})<<std::endl;

        //set FREE_SURFACE in STRUCTURE nodes
        SetFlagsUtilities::SetFlagsToOtherElementNodes(i_mp,{OLD_ENTITY},{FREE_SURFACE});
        SetFlagsUtilities::SetFlagsToNodes(i_mp,{{FLUID.AsFalse()}},{FREE_SURFACE.AsFalse()});

      }
    }

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void SetFlagsToRigidBoundaryFaces(ModelPart &rModelPart, std::vector<Flags> FlagsArray = {BOUNDARY})
  {
    KRATOS_TRY


    std::vector<Flags> FlagsToElements = {FREE_SURFACE.AsFalse(), INTERACTION.AsFalse()};
    std::vector<Flags> FlagsToNodes = {BOUNDARY.AsFalse(), FREE_SURFACE.AsFalse(), OUTLET.AsFalse()};

    SetFlagsUtilities::SetFlagsToElementsAndItsNodes(rModelPart,FlagsToElements,FlagsToNodes);

    // std::vector<Flags> FlagsToNodes = {BOUNDARY.AsFalse(), FREE_SURFACE.AsFalse(), OUTLET.AsFalse()};
    // SetFlagsUtilities::SetFlagsToElementNodes(rModelPart,{{}},FlagsToNodes);

    block_for_each(rModelPart.Elements(),[&](Element& i_elem){
      GeometryType &rGeometry = i_elem.GetGeometry();
      //elements with faces in the other working space dimension

      if (rGeometry.LocalSpaceDimension() != rGeometry.WorkingSpaceDimension())
        for (auto &i_node : rGeometry)
          i_node.Set(FlagsArray);
    });

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void SetFlagsToFluidBoundaryFaces(ModelPart &rModelPart, std::vector<Flags> FlagsArray = {BOUNDARY})
  {
    KRATOS_TRY

    std::vector<Flags> FlagsToElements = {BOUNDARY.AsFalse(), FREE_SURFACE.AsFalse(), INTERACTION.AsFalse(), ISOLATED.AsFalse(), OUTLET.AsFalse()};
    std::vector<Flags> FlagsToNodes = {BOUNDARY.AsFalse(), FREE_SURFACE.AsFalse(), INTERACTION.AsFalse(), OUTLET.AsFalse()};

    SetFlagsUtilities::SetFlagsToElementsAndItsNodes(rModelPart,FlagsToElements,FlagsToNodes);
    // ISOLATED particles
    SetFlagsUtilities::SetFlagsToNodes(rModelPart,{{ISOLATED}},{INTERACTION.AsFalse()});

    block_for_each(rModelPart.Elements(),[&](Element& i_elem){
      GeometryType &rGeometry = i_elem.GetGeometry();
      //elements with faces in the working space dimension
      if (rGeometry.WorkingSpaceDimension() == rGeometry.LocalSpaceDimension())
      {
        DenseMatrix<unsigned int> lpofa; //points that define the faces
        rGeometry.NodesInFaces(lpofa);
        ElementWeakPtrVectorType &nElements = i_elem.GetValue(NEIGHBOUR_ELEMENTS);
        unsigned int iface = 0;
        unsigned int boundary_faces = 0;
        unsigned int free_surface = 0;
        for (auto &i_nelem : nElements)
        {
          if (i_nelem.Id() == i_elem.Id()) //means no neighbour element in that face
          {
            //if no neighbour is present => the face is free surface
            unsigned int rigid_nodes = 0;
            unsigned int inlet_nodes = 0;
            unsigned int free_surface_nodes = 0;
            for (unsigned int j = 1; j < lpofa.size1(); ++j)
            {
              NodeType &rNode = rGeometry[lpofa(j, iface)];
              rNode.Set(BOUNDARY, true);

              if (rNode.Or({RIGID,SOLID}))
                ++rigid_nodes;
              else if (rNode.Is(INLET))
                ++inlet_nodes;
              else
                ++free_surface_nodes;
            }

            if ((rigid_nodes == 0 && inlet_nodes == 0) ||
                (free_surface_nodes > 0 && (rigid_nodes > 0 || inlet_nodes > 0))){
              ++free_surface;
              for (unsigned int j = 1; j < lpofa.size1(); ++j)
                rGeometry[lpofa(j, iface)].Set(FREE_SURFACE, true);
            }
            ++boundary_faces;
          }
          ++iface;
        }
        // set free surface elements and isolated elements (full free surface)
        if (free_surface > 0){
          i_elem.Set(FREE_SURFACE, true);
          if (free_surface == rGeometry.FacesNumber())
            i_elem.Set(ISOLATED, true);
        }
        // set blocked elements
        if(i_elem.Is(ISOLATED))
          if(SetFlagsUtilities::CheckNodeFlag(rGeometry,{RIGID}))
            i_elem.Set(OUTLET,GeometryUtilities::CheckBlocked(rGeometry,boundary_faces));
      }
      else
      {
        std::cout << " OTHER ELEMENTS DOMAIN " <<std::endl;
        //set nodes to BOUNDARY for elements outside of the working space dimension
        for (auto &i_node : rGeometry)
          i_node.Set(BOUNDARY, true);
      }
    });

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void SetFlagsToRigidWallNodes(ModelPart &rModelPart, std::vector<Flags> FlagsArray = {BOUNDARY})
  {
    KRATOS_TRY

    //boundary elements (different working dimension : walls)
    block_for_each(rModelPart.Elements(),[&](Element& i_elem){
      GeometryType &rGeometry = i_elem.GetGeometry();
      if (rGeometry.LocalSpaceDimension() != rGeometry.WorkingSpaceDimension())
        for (auto &i_node : rGeometry)
          if (i_node.Is(RIGID))
            i_node.Set(FlagsArray);
    });

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{

  /// Assignment operator.
  SetFlagsToEntitiesProcess &operator=(SetFlagsToEntitiesProcess const &rOther);

  /// Copy constructor.
  //SetFlagsToEntitiesProcess(SetFlagsToEntitiesProcess const& rOther);

  ///@}

}; // Class SetFlagsToEntitiesProcess

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                SetFlagsToEntitiesProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const SetFlagsToEntitiesProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_SET_FLAGS_TO_ENTITIES_PROCESS_HPP_INCLUDED  defined
