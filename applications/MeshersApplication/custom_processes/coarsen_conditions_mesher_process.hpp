//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:         July 2021 $
//
//

#if !defined(KRATOS_COARSEN_CONDITIONS_MESHER_PROCESS_HPP_INCLUDED)
#define KRATOS_COARSEN_CONDITIONS_MESHER_PROCESS_HPP_INCLUDED

// External includes

// System includes

// Project includes
#include "custom_processes/mesher_process.hpp"


namespace Kratos
{

///@name Kratos Classes
///@{

/// Remove Mesh Nodes Process for 2D and 3D cases
/**
    The process labels the nodes to be erased (TO_ERASE)
*/

class CoarsenConditionsMesherProcess
    : public MesherProcess
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of Process
  KRATOS_CLASS_POINTER_DEFINITION(CoarsenConditionsMesherProcess);

  typedef Node<3> NodeType;
  typedef array_1d<double,3> VectorType;
  typedef ModelPart::MeshType::GeometryType GeometryType;
  typedef GeometryType::PointsArrayType PointsArrayType;

  typedef GlobalPointersVector<NodeType> NodeWeakPtrVectorType;
  typedef GlobalPointersVector<Element> ElementWeakPtrVectorType;
  typedef GlobalPointersVector<Condition> ConditionWeakPtrVectorType;

  typedef Bucket<3, NodeType, std::vector<NodeType::Pointer>, NodeType::Pointer, std::vector<NodeType::Pointer>::iterator, std::vector<double>::iterator> BucketType;
  typedef Tree<KDTreePartition<BucketType>> KdtreeType; //Kdtree

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  CoarsenConditionsMesherProcess(ModelPart &rModelPart,
                           MesherData::MeshingParameters &rRemeshingParameters,
                           int EchoLevel)
      : mrModelPart(rModelPart),
        mrRemesh(rRemeshingParameters)
  {
    mEchoLevel = EchoLevel;
  }

  /// Destructor.
  virtual ~CoarsenConditionsMesherProcess() {}

  ///@}
  ///@name Operators
  ///@{

  /// This operator is provided to call the process as a function and simply calls the Execute method.
  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  /// Execute method is used to execute the Process algorithms.
  void Execute() override
  {
    KRATOS_TRY

    mInfoData.Initialize(mEchoLevel);
    RemoveBoundaryNodes(mrModelPart);
    mInfoData.Finalize(mEchoLevel);

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "CoarsenConditionsMesherProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "CoarsenConditionsMesherProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{

  ModelPart &mrModelPart;

  MesherData::MeshingParameters &mrRemesh;

  int mEchoLevel;

  // struct for monitoring the process information
  struct ProcessInfoData
  {
    unsigned int initial_conditions;
    unsigned int initial_nodes;
    unsigned int final_conditions;
    unsigned int final_nodes;

    void Initialize(int EchoLevel)
    {
      initial_conditions = 0;
      initial_nodes = 0;
      final_conditions = 0;
      final_nodes = 0;

      if (EchoLevel > 0)
        std::cout << " [ REMOVE BOUNDARY NODES: " << std::endl;
    }

    void Finalize(int EchoLevel)
    {

      if (EchoLevel > 0)
      {
        std::cout << "   [ NODES      ( removed : " << int(initial_nodes-final_nodes) << " total: " << final_nodes << " ) ]" << std::endl;
        std::cout << "   [ CONDITIONS ( removed : " << int(initial_conditions-final_conditions) << " total: " << final_conditions << " ) ]" << std::endl;
        std::cout << "   REMOVE BOUNDARY NODES ]; " << std::endl;
      }

    }
  };

  ProcessInfoData mInfoData;

  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{

  void RemoveBoundaryNodes(ModelPart &rModelPart)
  {
    KRATOS_TRY

    mrRemesh.Refine->Interior.Info.Initialize();
    mrRemesh.Refine->Boundary.Info.Initialize();

    mInfoData.initial_conditions = rModelPart.NumberOfConditions();
    mInfoData.initial_nodes = rModelPart.NumberOfNodes();


    //---- check mesh ----//

    //check existing nodes TO_ERASE
    this->CheckNodesToErase(rModelPart);

    // for (auto &i_cond : rModelPart.Conditions())
    // {
    //   std::vector<int> v1;
    //   for (auto &i_node : i_cond.GetGeometry())
    //     v1.push_back(i_node.Id());
    //   std::cout<<" INITIAL CONDITION ["<<i_cond.Id()<<"] "<<v1<<std::endl;
    // }
    // std::cout<<" initial facets check "<<std::endl;
    // ModelPartUtilities::CheckConditionFacets(rModelPart);

    // reset existing TO_ERASE flag...
    // SetFlagsUtilities::SetFlagsToNodes(rModelPart, {{TO_ERASE}}, {TO_ERASE.AsFalse()});

    //if the remove_node switch is activated, we check if the nodes got too close
    if (mrRemesh.IsBoundary(RefineData::COARSEN))
    {
      if (rModelPart.Is(SOLID) && rModelPart.GetProcessInfo()[SPACE_DIMENSION] == 2)
        this->RemoveNonConvexBoundary(rModelPart, mrRemesh.Refine->Boundary.Info.on_distance); //2D only

      if (mrRemesh.IsBoundary(RefineData::COARSEN_ON_DISTANCE))
      {
        //reset flags for the local process execution
        SetFlagsUtilities::SetFlagsToConditions(rModelPart, {{VISITED}}, {VISITED.AsFalse()});

        //this->RemoveConditionsOnDistanceAndAngle(rModelPart, mrRemesh.Refine->Boundary.Info.on_distance);
        this->RemoveConditionsOnDistance(rModelPart, mrRemesh.Refine->Boundary.Info.on_distance);
        this->RemoveConditionsOnAngle(rModelPart, mrRemesh.Refine->Boundary.Info.on_distance);

        //this->RemoveNodesOnDistance(rModelPart, mrRemesh.Refine->Boundary.Info.on_distance);
      }
    }

    //this is not the correct place to remove rigid contact conditions
    //this->RemoveRigidContactConditions(rModelPart);

    //reset flags for the local process execution
    SetFlagsUtilities::SetFlagsToNodes(rModelPart, {{BLOCKED}}, {BLOCKED.AsFalse()});
    SetFlagsUtilities::SetFlagsToConditions(rModelPart, {{VISITED}}, {VISITED.AsFalse()});

    mInfoData.final_conditions = rModelPart.NumberOfConditions();
    mInfoData.final_nodes = rModelPart.NumberOfNodes();

    // for (auto &i_cond : rModelPart.Conditions())
    // {
    //   std::vector<int> v1;
    //   for (auto &i_node : i_cond.GetGeometry())
    //     v1.push_back(i_node.Id());
    //   std::cout<<" CONDITION ["<<i_cond.Id()<<"] "<<v1<<std::endl;
    // }
    // std::cout<<" final facets check"<<std::endl;
    // Check duplicated facets
    // ModelPartUtilities::CheckConditionFacets(rModelPart);
    // Check close nodes
    // ModelPartUtilities::CheckCloseNodes(mrModelPart, 1e-7);
    // Check small domain conditions
    // ModelPartUtilities::CheckConditionsSize(mrModelPart, 1e-14);

    // check nodal normals (possible meshing problems if they are not defined correctly)
    ModelPartUtilities::CheckNodalNormal(rModelPart);

    // set neighbour conditions
    ModelPartUtilities::SetNodeNeighbourConditions(rModelPart);

    if (rModelPart.GetProcessInfo()[SPACE_DIMENSION] == 3)
    {
      PrintModelPartMeshProcess print_output(rModelPart,"coarse_conditions",1);
      print_output.Execute();
    }

    KRATOS_CATCH(" ")
  }

  //**************************************************************************
  //**************************************************************************

  virtual void SetLocalFlagsForSearches(ModelPart &rModelPart)
  {
    KRATOS_TRY

    //reset flags for the local process execution
    SetFlagsUtilities::SetFlagsToNodes(rModelPart, {{BLOCKED}}, {BLOCKED.AsFalse()});
    SetFlagsUtilities::SetFlagsToNodes(rModelPart, {{RIGID}, {INLET}}, {BLOCKED});
    //set neighbour conditions
    ModelPartUtilities::SetNodeNeighbourConditions(rModelPart);

    KRATOS_CATCH(" ")
  }

  //**************************************************************************
  //**************************************************************************

  virtual void CheckNodesToErase(ModelPart &rModelPart)
  {
    KRATOS_TRY

    for (auto& i_node: rModelPart.Nodes())
      if (i_node.Is(TO_ERASE))
        std::cout<<" Node ["<<i_node.Id()<<"] is TO_ERASE (before coarsen elements) "<<std::endl;

    KRATOS_CATCH(" ")
  }

  //**************************************************************************
  //**************************************************************************

  void RemoveRigidContactConditions(ModelPart &rModelPart)
  {
    KRATOS_TRY

    ModelPart &rParentModelPart = rModelPart.GetParentModelPart();

    for (auto &i_mp : rParentModelPart.SubModelParts())
    {
      const std::vector<Flags> NotFlags = {ACTIVE, BOUNDARY, CONTACT};
      if (i_mp.IsNot(NotFlags) && i_mp.NumberOfElements() == 0)
      {
        ModelPart::ConditionsContainerType PreservedConditions;
        PreservedConditions.swap(i_mp.Conditions());

        for (auto i_cond(PreservedConditions.begin()); i_cond != PreservedConditions.end(); ++i_cond)
        {
          GeometryType &rGeometry = i_cond->GetGeometry();
          if (rGeometry.PointsNumber() > 1)
          {
            i_mp.Conditions().push_back(*(i_cond.base()));
          }
          else
          {
            if (rGeometry[0].IsNot(TO_ERASE))
            {
              i_mp.Conditions().push_back(*(i_cond.base()));
            }
          }
        }
      }
    }

    KRATOS_CATCH("")
  }


  //**************************************************************************
  //**************************************************************************

  virtual void RemoveConditionsOnDistanceAndAngle(ModelPart &rModelPart, unsigned int &boundary_nodes_removed)
  {
    KRATOS_TRY

    this->SetLocalFlagsForSearches(rModelPart);

    unsigned int removed_conditions = 0;

    const ProcessInfo& rCurrentProcessInfo = rModelPart.GetProcessInfo();

    for (auto &i_cond : rModelPart.Conditions())
    {
      if (this->RemoveConditionOnDistance(i_cond,rCurrentProcessInfo))
        ++removed_conditions;
      if (this->RemoveConditionOnAngle(i_cond))
        ++removed_conditions;
    }

    if (rModelPart.Is(SOLID) && rModelPart.GetProcessInfo()[SPACE_DIMENSION] == 2)
      this->RemoveNodesOnContact(rModelPart, removed_conditions);

    if (removed_conditions > 0)
    {
      if (this->RebuildBoundary(rModelPart))
      {
        ModelPartUtilities::EraseDuplicatedFacets(rModelPart);
        ModelPartUtilities::EraseConditions(rModelPart);
        ModelPartUtilities::EraseNodes(rModelPart);
      }
    }

    boundary_nodes_removed += removed_conditions;

    KRATOS_CATCH(" ")
  }

  //**************************************************************************
  //**************************************************************************

  virtual void RemoveConditionsOnDistance(ModelPart &rModelPart, unsigned int &boundary_nodes_removed)
  {
    KRATOS_TRY

    this->SetLocalFlagsForSearches(rModelPart);

    unsigned int removed_conditions = 0;

    const ProcessInfo& rCurrentProcessInfo = rModelPart.GetProcessInfo();

    for (auto &i_cond : rModelPart.Conditions())
    {
      if (this->RemoveConditionOnDistance(i_cond,rCurrentProcessInfo))
        ++removed_conditions;
    }

    if (rModelPart.Is(SOLID) && rModelPart.GetProcessInfo()[SPACE_DIMENSION] == 2)
      this->RemoveNodesOnContact(rModelPart, boundary_nodes_removed);

    if (removed_conditions > 0)
    {
      if (this->RebuildBoundary(rModelPart))
      {
        ModelPartUtilities::EraseDuplicatedFacets(rModelPart);
        ModelPartUtilities::EraseConditions(rModelPart);
        ModelPartUtilities::EraseNodes(rModelPart);
      }
    }

    boundary_nodes_removed += removed_conditions;

    KRATOS_CATCH(" ")
  }

  //**************************************************************************
  //**************************************************************************

  virtual void RemoveConditionsOnAngle(ModelPart &rModelPart, unsigned int &boundary_nodes_removed)
  {
    KRATOS_TRY

    this->SetLocalFlagsForSearches(rModelPart);

    unsigned int removed_conditions = 0;

    for (auto &i_cond : rModelPart.Conditions())
    {
      if (this->RemoveConditionOnAngle(i_cond))
        ++removed_conditions;
    }

    if (removed_conditions > 0)
    {
      if (this->RebuildBoundary(rModelPart))
      {
        ModelPartUtilities::EraseDuplicatedFacets(rModelPart);
        ModelPartUtilities::EraseConditions(rModelPart);
        ModelPartUtilities::EraseNodes(rModelPart);
      }
    }

    KRATOS_CATCH(" ")
  }

  //**************************************************************************
  //**************************************************************************

  bool RemoveConditionOnDistance(Condition &rCondition, const ProcessInfo& rCurrentProcessInfo)
  {
    KRATOS_TRY

    std::vector<Flags> YesFlags = {TO_ERASE};
    if (rCondition.Is(YesFlags))
      return false;

    MesherData MeshData;
    RefineData& Refine = MeshData.GetRefineData(rCondition, mrRemesh, rCurrentProcessInfo);
    double size_for_distance_boundary = 0.30 * Refine.Boundary.DistanceMeshSize;

    GeometryType &rGeometry = rCondition.GetGeometry();

    if (rGeometry.size() == 2)
    {
      std::vector<bool> excluded_nodes(2,false);
      double edge = GeometryUtilities::CalculateMinEdgeSize(rGeometry);
      if (edge < size_for_distance_boundary){
        return this->SelectNodeToErase(rGeometry,excluded_nodes);
      }
    }
    else if (rGeometry.size() == 3)
    {
      std::vector<bool> excluded_nodes(3,false);
      //Check edge sizes
      std::vector<double> edges;
      GeometryUtilities::CalculateEdgesSize(rGeometry, edges);

      //Any TO_ERASE node at this point
      unsigned int i = 0;
      for (auto& edge : edges)
      {
        if (edge < size_for_distance_boundary){
          excluded_nodes[i] = true; //opposite side node : candidate to preserve
          //std::cout<<"["<<rCondition.Id()<<"] Distance coarsen "<<edge<<std::endl;
          return this->SelectNodeToErase(rGeometry, excluded_nodes);
        }
        ++i;
      }
    }

    return false;

    KRATOS_CATCH(" ")
  }

  //**************************************************************************
  //**************************************************************************

  bool RemoveConditionOnAngle(Condition &rCondition)
  {
    KRATOS_TRY

    std::vector<Flags> YesFlags = {TO_ERASE};
    if (rCondition.Is(YesFlags))
      return false;

    double critical_angle = 140;
    GeometryType &rGeometry = rCondition.GetGeometry();

    if (rGeometry.size() == 3)
    {
      //Check edge sizes
      std::vector<double> edges;
      GeometryUtilities::CalculateEdgesSize(rGeometry, edges);

      //Check edge angles
      VectorType angles = GeometryUtilities::CalculateAnglesSize(edges);

      //Any TO_ERASE node at this point
      unsigned int i = 0;
      std::vector<bool> excluded_nodes(3,true);

      //Nodes belonging to the condition with critical angle set TO_ERASE
      for (auto& angle : angles)
      {
        if (angle > critical_angle){
          rCondition.Set(VISITED, true); //VISITED, means flat condition
          excluded_nodes[i] = false; //candidate to erase -> VISITED, to split opposite side
          //std::cout<<"["<<rCondition.Id()<<"] Angle coarsen "<<angle<<" excluded nodes "<<excluded_nodes<<" i "<<i<<std::endl;
          return this->SelectNodeToErase(rGeometry, excluded_nodes);
        }
        ++i;
      }
    }

    return false;

    KRATOS_CATCH(" ")
  }

  //**************************************************************************
  //**************************************************************************

  virtual bool SelectNodeToErase(GeometryType &rGeometry, std::vector<bool> &excluded_nodes)
  {
    KRATOS_TRY

    bool select = true;
    for (auto& i_node : rGeometry)
    {
      if (i_node.Is(TO_ERASE))
      {
        select = false;
        break;
      }
    }

    if (select)
    {
      unsigned int marked = 0, i = 0, index = 0;
      for (const auto &i_bool : excluded_nodes)
      {
        if (!i_bool){
          ++marked;
          index = i;
        }
        ++i;
      }

      if (marked != 1)
      {
        std::vector<double> normals(rGeometry.size(),0);
        i = 0;
        for (auto& i_node : rGeometry)
        {
          //if (i_node.IsNot(NEW_ENTITY) && !excluded_nodes[i])
          if (!excluded_nodes[i])
          {
            ConditionWeakPtrVectorType &nConditions = i_node.GetValue(NEIGHBOUR_CONDITIONS);
            const VectorType& Normal = i_node.FastGetSolutionStepValue(NORMAL);
            //std::cout<<" node "<<i_node.Id()<<" normal "<<Normal<<" "<<nConditions.size()<<std::endl;
            for (unsigned int j=0; j<nConditions.size(); ++j)
            {
              VectorType FaceNormal = nConditions[j].GetValue(NORMAL);
              //std::cout<<" Face normal "<<nConditions[j].Id()<<" "<<FaceNormal<<" "<<normals[i]<<std::endl;
              normals[i] += inner_prod(Normal,FaceNormal);
              for (auto& i_nnode : nConditions[j].GetGeometry())
              {
                //if (i_nnode.Is(CONTACT) || i_nnode.Is(TO_ERASE))
                if (i_nnode.Is(TO_ERASE) || i_nnode.Is(BLOCKED))
                {
                  std::cout<<" node ["<<i_nnode.Id()<<"] is to erase / not selected"<<std::endl;
                  normals[i] = -1; //do not select this node
                  break;
                }
              }
            }
            normals[i]/=double(nConditions.size());
          }
          // else{
          //   std::cout<<" node ["<<i<<"]"<<" new entity "<<i_node.Is(NEW_ENTITY)<<std::endl;
          // }
          ++i;
        }


        //std::cout<<" normals "<<normals<<std::endl;
        // select node with less changing normals
        index = std::max_element(normals.begin(),normals.end()) - normals.begin();
        if (normals[index] >= 0 && !excluded_nodes[index])
        {
          std::cout<< "node to erase "<<index<<" :: "<<rGeometry[index].Id()<<" normals "<<normals<<" excluded "<<excluded_nodes<<" "<<rGeometry[0].Id()<<" "<<rGeometry[1].Id();
          if (rGeometry.size()>2)
            std::cout<<" "<<rGeometry[2].Id()<<std::endl;
          else
            std::cout<<std::endl;
          rGeometry[index].Set(TO_ERASE, true);
          //std::cout<<" node ["<<rGeometry[index].Id()<<"] is to erase "<<std::endl;
          //block replacement node for distance coarsening
          for (unsigned int i=0; i<rGeometry.size(); ++i)
            if (!excluded_nodes[i] && i!=index)
              rGeometry[i].Set(BLOCKED, true);
        }
      }
      else{
        rGeometry[index].Set(TO_ERASE);
        //std::cout<<" node ["<<rGeometry[index].Id()<<"] is to erase "<<std::endl;
      }
      return true;
    }
    else
      return false;
    // else{
    //   std::cout<<" node to erase not selected "<<std::endl;
    //   for (auto& i_node : rGeometry)
    //   {
    //     if (i_node.Is(TO_ERASE))
    //       std::cout<<" node ["<<i_node.Id()<<"] is to erase "<<std::endl;
    //     else
    //       std::cout<<" node ["<<i_node.Id()<<"] "<<std::endl;
    //   }
    // }

    KRATOS_CATCH(" ")
  }

  //**************************************************************************
  //**************************************************************************

  virtual void RemoveNodesOnDistance(ModelPart &rModelPart, unsigned int &boundary_nodes_removed)
  {
    KRATOS_TRY

    //create search tree
    KdtreeType::Pointer SearchTree;
    std::vector<NodeType::Pointer> list_of_nodes;
    ModelPartUtilities::CreateSearchTree(rModelPart, SearchTree, list_of_nodes, 20);

    //all of the nodes in this list will be preserved
    unsigned int num_neighbours = 100;
    std::vector<NodeType::Pointer> neighbours(num_neighbours);
    std::vector<double> neighbour_distances(num_neighbours);

    //radius means the distance, if the distance between two nodes is closer to radius -> mark for removing
    unsigned int n_points_in_radius;

    MesherData MeshData;
    for (auto &i_node : rModelPart.Nodes())
    {
      RefineData& Refine = MeshData.GetRefineData(i_node, mrRemesh, rModelPart.GetProcessInfo());

      //***SIZES :::: parameters do define the tolerance in mesh size:
      double size_for_distance_inside = 0.5 * Refine.Interior.DistanceMeshSize;
      double size_for_distance_boundary_squared = pow(0.25 * Refine.Boundary.DistanceMeshSize, 2);

      const std::vector<Flags> NotFlags = {BLOCKED, TO_ERASE, NEW_ENTITY, CONTACT};
      if (i_node.IsNot(NotFlags) && i_node.Is(BOUNDARY))
      {
        // the neighbour_distance returned by SearchInRadius is squared (distance to the power of two)
        n_points_in_radius = SearchTree->SearchInRadius(i_node, size_for_distance_inside, neighbours.begin(), neighbour_distances.begin(), num_neighbours);

        if (n_points_in_radius > 1)
        {
          //std::cout<<"  Remove close boundary nodes: Candidate ["<<i_node.Id()<<"]"<<std::endl;
          // check curvature and reduce size ... improvement

          //loop over the neighbouring BOUNDARY flag nodes and remove if they are REALLY close
          unsigned int k = 0;
          for (std::vector<NodeType::Pointer>::iterator i_nnode = neighbours.begin(); i_nnode != neighbours.begin() + n_points_in_radius; ++i_nnode)
          {
            if (neighbour_distances[k] < size_for_distance_boundary_squared && neighbour_distances[k] > 0.0){

              if ((*i_nnode)->IsNot(CONTACT) && (*i_nnode)->IsNot(TO_ERASE) && (*i_nnode)->Is(BOUNDARY)){
                i_node.Set(TO_ERASE);
                boundary_nodes_removed++;
                std::cout<<"     Set Node ["<<i_node.Id()<<"] TO_ERASE for boundary removal in coarsen_boundary_mesher_process.hpp"<<std::endl;
                break;
              }
            }
            k++;
          }
        }
      }
    }

    if (rModelPart.Is(SOLID))
       this->RemoveNodesOnContact(rModelPart, boundary_nodes_removed);

    KRATOS_CATCH(" ")
  }

  //**************************************************************************
  //**************************************************************************

  virtual void RemoveNodesOnContact(ModelPart &rModelPart, unsigned int &boundary_nodes_removed)
  {
    KRATOS_TRY

    if (mEchoLevel > 0)
    {
      std::cout << "   [ REMOVE NODES ON CONTACT AND BEYOND : " << std::endl;
      std::cout << "     Starting Conditions : " << rModelPart.Conditions().size() << std::endl;
    }

    //create search tree
    KdtreeType::Pointer SearchTree;
    std::vector<NodeType::Pointer> list_of_nodes;
    ModelPartUtilities::CreateSearchTree(rModelPart, SearchTree, list_of_nodes, 20);

    //all of the nodes in this list will be preserved
    unsigned int num_neighbours = 100;
    std::vector<NodeType::Pointer> neighbours(num_neighbours);
    std::vector<double> neighbour_distances(num_neighbours);

    //radius means the distance, if the distance between two nodes is closer to radius -> mark for removing
    unsigned int n_points_in_radius;
    MesherData MeshData;

    for (auto &i_node : rModelPart.Nodes())
    {
      RefineData& Refine = MeshData.GetRefineData(i_node, mrRemesh, rModelPart.GetProcessInfo());

      //***SIZES :::: parameters do define the tolerance in mesh size:
      double size_for_distance_inside = 0.5 * Refine.Interior.DistanceMeshSize;

      double size_for_distance_boundary_squared = pow(0.25 * Refine.Boundary.DistanceMeshSize, 2);
      double size_for_wall_tip_contact_side_squared = pow(0.15 * Refine.Boundary.DistanceMeshSize, 2);

      //

      if (i_node.IsNot(NEW_ENTITY) && i_node.IsNot(BLOCKED) && i_node.IsNot(TO_ERASE))
      {
        const NodeType &point = i_node;
        // the neighbour_distance returned by SearchInRadius is squared (distance to the power of two)
        n_points_in_radius = SearchTree->SearchInRadius(point, size_for_distance_inside, neighbours.begin(), neighbour_distances.begin(), num_neighbours);

        if (n_points_in_radius > 1)
        {
          if (i_node.IsNot(BOUNDARY))
          {
            if (Refine.Interior.Options.Is(RefineData::COARSEN_ON_DISTANCE))
            {
              //look if we are already erasing any of the other nodes
              unsigned int contact_nodes = 0;
              unsigned int erased_nodes = 0;
              unsigned int near_to_contact_nodes = 0;

              unsigned int kk = 0;
              for (std::vector<NodeType::Pointer>::iterator nn = neighbours.begin(); nn != neighbours.begin() + n_points_in_radius; ++nn)
              {
                if ((*nn)->Is(BOUNDARY) && (*nn)->Is(CONTACT))
                  contact_nodes += 1;

                if ((*nn)->Is(TO_ERASE))
                  erased_nodes += 1;

                // to remove a node that is very close to a contact node (two times the safety factor)
                if ((*nn)->Is(BOUNDARY) && (*nn)->Is(CONTACT) && (neighbour_distances[kk] < size_for_wall_tip_contact_side_squared))
                  near_to_contact_nodes += 1;

                kk++;
              } // end for neighbours

              if (erased_nodes < 1 && contact_nodes < 2 && near_to_contact_nodes == 1) // we release node if it is very very near a contact
              {
                // to remove an interior node to is to close to a contacting node
                i_node.Set(TO_ERASE);
                std::cout<<"     Set Node ["<<i_node.Id()<<"] TO_ERASE for interior contact removal in coarsen_boundary_mesher_process.hpp"<<std::endl;
                std::cout << "   RemovingC0, an interior node very very near to a (possibly) contacting node " << i_node.Id() << std::endl;
                std::cout << "      X: " << i_node.X() << " Y: " << i_node.Y() << std::endl;
              } // end else if
            }
          }
          else if ( Refine.Boundary.Options.Is(RefineData::COARSEN) && Refine.Boundary.Options.Is(RefineData::COARSEN_ON_DISTANCE))
          {
            //std::cout<<"  Remove close boundary nodes: Candidate ["<<i_node.Id()<<"]"<<std::endl;

            unsigned int k = 0;
            unsigned int counterC2 = 0, counterC3 = 0, counterC4 = 0;
            for (std::vector<NodeType::Pointer>::iterator nn = neighbours.begin(); nn != neighbours.begin() + n_points_in_radius; ++nn)
            {
              bool nn_active_contact = false;
              bool nn_on_contact_tip = CheckContactNode(**nn, nn_active_contact);

              if ((*nn)->Is(BOUNDARY) && neighbour_distances[k] > 0.0 && (*nn)->IsNot(TO_ERASE))
              {
                if (neighbour_distances[k] < size_for_wall_tip_contact_side_squared)
                {
                  if (nn_active_contact && (*nn)->IsNot(NEW_ENTITY))
                  {
                    counterC2 += 1;
                  }
                  if (nn_on_contact_tip && nn_active_contact && neighbour_distances[k] < size_for_distance_boundary_squared)
                  {
                    counterC3 += 1;
                  }
                  if (neighbour_distances[k] < size_for_distance_boundary_squared && (*nn)->IsNot(NEW_ENTITY))
                  {
                    counterC4 += 1;
                  }
                }
              }

              k++;
            } // end for each neighbour

            bool active_contact = false;
            bool on_contact_tip = CheckContactNode(i_node, active_contact);

            if (counterC2 > 1 && active_contact)
            {
              i_node.Set(TO_ERASE);
              std::cout << "     RemovingC2: three contacting nodes where close, removing the middle one [" << i_node.Id() << "]" << std::endl;
              boundary_nodes_removed++;
              std::cout << "      X: " << i_node.X() << " Y: " << i_node.Y() << std::endl;
            }
            else if (counterC3 > 0 && on_contact_tip && !active_contact)
            {
              i_node.Set(TO_ERASE);
              boundary_nodes_removed++;
              std::cout << "    RemovingC3: a non_contacting_node was to close to a contacting. removing the non_contacting " << i_node.Id() << std::endl;
              std::cout << "      X: " << i_node.X() << " Y: " << i_node.Y() << std::endl;
            }
            else if (counterC4 > 0 && !active_contact)
            {
              i_node.Set(TO_ERASE);
              std::cout << "    RemovingC4: two contacting nodes are very very close, removing one " << i_node.Id() << std::endl;
              std::cout << "      X: " << i_node.X() << " Y: " << i_node.Y() << std::endl;
              boundary_nodes_removed++;
              std::cout << "    RemovingC4: " << i_node.Id() << std::endl;
              std::cout << "      X: " << i_node.X() << " Y: " << i_node.Y() << std::endl;
            }
          }
        }
      }
    }

    // New loop to see if just two contacting nodes are very close. (it has to be done after the others to not to remove a pair)
    if (mrRemesh.IsBoundary(RefineData::COARSEN) && mrRemesh.IsBoundary(RefineData::COARSEN_ON_DISTANCE))
    {
      for (auto &i_node : rModelPart.Nodes())
      {
         RefineData& Refine = MeshData.GetRefineData(i_node, mrRemesh, rModelPart.GetProcessInfo());

        //***SIZES :::: parameters do define the tolerance in mesh size:
        double size_for_distance_inside = 0.5 * Refine.Interior.DistanceMeshSize;
        double size_for_wall_tip_contact_side_squared = pow(0.15 * Refine.Boundary.DistanceMeshSize, 2);

        bool active_contact = false;
        bool on_contact_tip = CheckContactNode(i_node, active_contact);

        if (i_node.IsNot(NEW_ENTITY) && i_node.IsNot(TO_ERASE) && i_node.Is(BOUNDARY) && on_contact_tip)
        {
          const NodeType &point = i_node;
          // the neighbour_distance returned by SearchInRadius is squared (distance to the power of two)
          n_points_in_radius = SearchTree->SearchInRadius(point, size_for_distance_inside, neighbours.begin(), neighbour_distances.begin(), num_neighbours);

          if (n_points_in_radius > 1)
          {
            unsigned int k = 0;
            unsigned int counterC4 = 0;
            for (std::vector<NodeType::Pointer>::iterator nn = neighbours.begin(); nn != neighbours.begin() + n_points_in_radius; ++nn)
            {
              bool nn_active_contact = false;
              bool nn_on_contact_tip = CheckContactNode(**nn,nn_active_contact);

              if ((*nn)->IsNot(NEW_ENTITY) && (*nn)->IsNot(TO_ERASE) && (*nn)->Is(BOUNDARY) && neighbour_distances[k] > 0.0)
              {
                if (nn_on_contact_tip && nn_active_contact && neighbour_distances[k] < size_for_wall_tip_contact_side_squared)
                {
                  counterC4 += 1;
                  std::cout << " THIS IS THE CONTRARY NODE: " << (*nn)->X() << " " << (*nn)->Y() << std::endl;
                }
              } // first if for C4 Condition
              k++;
            } // end for all Neighbours

            bool active_contact = false;
            bool on_contact_tip = CheckContactNode(i_node,active_contact);

            if (counterC4 > 0 && i_node.IsNot(NEW_ENTITY) && on_contact_tip && active_contact)
            {
              i_node.Set(TO_ERASE);
              boundary_nodes_removed++;
              std::cout << "    RemovingC4: two contacting nodes are very very close, removing one " << i_node.Id() << std::endl;
              std::cout << "      X: " << i_node.X() << " Y: " << i_node.Y() << std::endl;
            }
          }
        }
      }
    }

    if (mEchoLevel > 0)
    {
      std::cout << "     boundary_nodes_removed " << boundary_nodes_removed << std::endl;
      std::cout << "     REMOVE NODES ON CONTACT AND BEYOND ]; " << std::endl;
    }

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  bool RebuildBoundary(ModelPart &rModelPart)
  {
    KRATOS_TRY

    bool any_condition_removed = false;

    //set flags for the local process execution
    SetFlagsUtilities::SetFlagsToNodes(rModelPart, {{VISITED}}, {VISITED.AsFalse()});

    unsigned int id = ModelPartUtilities::GetMaxConditionId(rModelPart) + 1;

    const unsigned int &step_data_size = rModelPart.GetNodalSolutionStepDataSize();

    const std::vector<Flags> Flags = {BOUNDARY, TO_ERASE, VISITED.AsFalse()};

    //nodes
    unsigned int i = 0, j = 0;
    for (auto &i_node : rModelPart.Nodes())
    {
      if (i_node.Is(Flags))
      {
        ConditionWeakPtrVectorType &nConditions = i_node.GetValue(NEIGHBOUR_CONDITIONS);

        if (!this->CheckConditions(nConditions, {TO_ERASE})) // !nCondition TO_ERASE
        {
          if (nConditions[0].GetGeometry().size() == 2) //2D line
          {
            i = 0; j = 1;
            if (nConditions[0].GetGeometry()[0].Id() == i_node.Id()) // connectivity type: /--c0--/--c1--/ or /--c1--/--c0--/
              std::swap(i,j);

            //select condition nodes /---c0---/---c1---/
            std::vector<NodeType::Pointer> Nodes = {nConditions[i].GetGeometry()(0), nConditions[j].GetGeometry()(1)};

            (rModelPart.Conditions()).push_back(this->CreateCondition(nConditions[i],Nodes,id++));
            for(auto& i_ncond : nConditions)
              SearchUtilities::SetConditionToErase(i_ncond);

            //condition will be removed
            any_condition_removed = true;
          }
          else //3D triangle
          {
            //check neighbour conditions TO_ERASE

            //check VISITED
            if (!this->CheckConditions(nConditions, {VISITED})) //NOT_VISITED
            {
              // if (this->CheckFlagInConditionNodes(nConditions, {NEW_ENTITY})){
              //   i_node.Set(TO_ERASE, false);
              //   std::cout<<" NEW entity not erased "<<i_node.Id()<<std::endl;
              // }
              // else
              // {
                //detect closest node neighbour i_nnode (triangle minimum edge)
                NodeType::Pointer i_nnode;
                double min_edge = std::numeric_limits<double>::max();

                for (auto &i_ncond : nConditions)
                {
                  Condition::GeometryType &rGeometry = i_ncond.GetGeometry();
                  std::vector<unsigned int> indices;
                  SearchUtilities::GetConditionNodeIndices(rGeometry, i_node, indices);

                  double edge_0 = GeometryUtilities::CalculateSideLength(rGeometry[indices[0]], rGeometry[indices[1]]);
                  if(edge_0 < min_edge)
                  {
                    min_edge = edge_0;
                    i_nnode = rGeometry(indices[1]);
                  }
                  double edge_1 = GeometryUtilities::CalculateSideLength(rGeometry[indices[0]], rGeometry[indices[2]]);
                  if(edge_1 < min_edge)
                  {
                    min_edge = edge_1;
                    i_nnode = rGeometry(indices[2]);
                  }
                  //std::cout<<" edges "<<edge_0<<" "<<edge_1<<std::endl;
                }

                std::cout<<" non visited condition "<<i_node.Id()<<" -> "<<i_nnode->Id()<<std::endl;

                //replace conditions i_node for the i_nnode
                for (auto &i_ncond : nConditions)
                  this->ReplaceConditionNode(i_ncond, i_node, i_nnode);

                bool rebuild = true;
                if (!rebuild)
                {
                  //check new entity (splitted element) and move middle position in that case
                  for (auto &i_ncond : nConditions)
                  {
                    if (i_ncond.Is(NEW_ENTITY)){
                      for (auto &j_node : i_ncond.GetGeometry()){
                        if (j_node.Is(NEW_ENTITY) && j_node.IsNot(BLOCKED) && j_node.Id()!=i_nnode->Id() && j_node.Id()!=i_node.Id()) //exist a refined (splitted) element (node inserted)
                        {
                          i_nnode->Coordinates() = 0.5 * (i_node.Coordinates() + i_nnode->Coordinates());
                          i_nnode->GetInitialPosition() = Point{i_nnode->Coordinates() - i_nnode->FastGetSolutionStepValue(DISPLACEMENT)};
                          break;
                        }
                      }
                    }
                  }
                }
                else{

                  //check new entity (splitted element) and remove nodes
                  ConditionWeakPtrVectorType &nnConditions = i_nnode->GetValue(NEIGHBOUR_CONDITIONS);
                  for (auto &i_ncond : nnConditions)
                  {
                    if (i_ncond.Is(NEW_ENTITY)){
                      for (auto &j_node : i_ncond.GetGeometry()){
                        if (j_node.Is(NEW_ENTITY) && j_node.IsNot(BLOCKED) && j_node.Id()!=i_node.Id() && j_node.Id()!=i_nnode->Id()) //exist a refined (splitted) element (node inserted)
                        {
                          ConditionWeakPtrVectorType &jnConditions = j_node.GetValue(NEIGHBOUR_CONDITIONS);
                          //replace conditions j_node for the i_nnode
                          for (auto &j_ncond : jnConditions)
                            this->ReplaceConditionNode(j_ncond, j_node, i_nnode);

                          i_nnode->Coordinates() = 0.5 * (j_node.Coordinates() + i_nnode->Coordinates());
                          i_nnode->GetInitialPosition() = Point{i_nnode->Coordinates() - i_nnode->FastGetSolutionStepValue(DISPLACEMENT)};
                          std::cout<<" Rebuild splitted coarsen boundary "<<j_node.Id()<<" "<<i_nnode->Id()<<std::endl;

                          j_node.Set(TO_ERASE, true);
                          j_node.Set(VISITED, true); //to skip future selection to erase
                          j_node.Set(NEW_ENTITY, true); //to distinguish with other modified nodes (set to false later on)

                          break;
                        }
                      }
                    }
                  }
                }
                //conditions containing the edge i_node-i_nnode TO_ERASE
                any_condition_removed = true;
              // }
            }
            else //VISITED
            {
              //std::cout<<" Angle Edge Condition "<<i_node.Id()<<std::endl;

              //reset to erase node
              i_node.Set(TO_ERASE,false);

              //split opposite edge to the i_node modifying the i_node position
              for(auto& i_ncond : nConditions)
              {
                //std::cout<<" Condition ["<<i_ncond.Id()<<"] "<<i_ncond.GetGeometry()[0].Id()<<" "<<i_ncond.GetGeometry()[1].Id()<<" "<<i_ncond.GetGeometry()[2].Id()<<std::endl;

                if (i_ncond.Is(VISITED))
                {
                  //center
                  Geometry<Node<3>> &rGeometry = i_ncond.GetGeometry();
                  std::vector<unsigned int> indices;
                  SearchUtilities::GetConditionNodeIndices(rGeometry, i_node, indices);
                  //std::cout<<" node ["<<i_node.Id()<<"] indices "<<indices<<std::endl;

                  std::vector<NodeType::Pointer> Nodes = {rGeometry(indices[1]), rGeometry(indices[2])};
                  PointsArrayType Edge(Nodes.begin(),Nodes.end());
                  GeometryType Boundary = GeometryType(Edge);
                  //std::cout<<" Boundary "<<Boundary[0].Id()<<" "<<Boundary[1].Id()<<std::endl;

                  //search neighbour edge condition
                  NodeType::Pointer pEdge2;
                  Condition::Pointer pEdgeCondition;

                  //create new conditions on the splitted side
                  if(SearchUtilities::FindEdgeSharedCondition(i_ncond.Id(),rModelPart.Conditions(),Boundary,pEdgeCondition,pEdge2))
                  {
                    //set node variables
                    array_1d<double, 3> N1, N2;
                    noalias(N1) = i_ncond.GetValue(NORMAL);
                    noalias(N2) = pEdgeCondition->GetValue(NORMAL);

                    if (inner_prod(N1, N2) < 0.99){
                      GeometryUtilities::AddNodeFixedDofs(i_node, Boundary);
                      TransferUtilities::AddNodeStepData(i_node, Boundary, step_data_size);
                      TransferUtilities::SetBoundaryNodeVariables(rModelPart, rGeometry, Boundary, i_node);
                      //change node coordinates after set boundary nodes for correct rGeometry NORMAL calculation
                      i_node.Coordinates() = 0.5* (rGeometry[indices[1]].Coordinates() + rGeometry[indices[2]].Coordinates());
                      i_node.GetInitialPosition() = Point{i_node.Coordinates() - i_node.FastGetSolutionStepValue(DISPLACEMENT)};
                    }

                    if (pEdgeCondition->IsNot(TO_ERASE))
                    {
                      //new condition 1
                      Nodes = {Boundary(0), pEdge2, rGeometry(indices[0])};
                      if (!this->DuplicatedFace(i_node, Nodes))
                        (rModelPart.Conditions()).push_back(this->CreateCondition(nConditions[i],Nodes,id++));

                      //std::cout<<" Create Condition A ("<<id<<") ["<<Nodes[0]->Id()<<","<<Nodes[1]->Id()<<","<<Nodes[2]->Id()<<"]"<<std::endl;
                      if(rModelPart.Conditions().back().GetGeometry().Area()<1e-15)
                        std::cout<<" Area A "<<rModelPart.Conditions().back().GetGeometry().Area()<<"["<<Nodes[0]->Id()<<","<<Nodes[1]->Id()<<","<<Nodes[2]->Id()<<"]"<<std::endl;

                      //new condition 2
                      Nodes = {Boundary(1), rGeometry(indices[0]), pEdge2};
                      if (!this->DuplicatedFace(i_node, Nodes))
                        (rModelPart.Conditions()).push_back(this->CreateCondition(nConditions[i],Nodes,id++));
                      //std::cout<<" Create Condition B ("<<id<<") ["<<Nodes[0]->Id()<<","<<Nodes[1]->Id()<<","<<Nodes[2]->Id()<<"]"<<std::endl;
                      if(rModelPart.Conditions().back().GetGeometry().Area()<1e-15)
                        std::cout<<" Area B "<<rModelPart.Conditions().back().GetGeometry().Area()<<"["<<Nodes[0]->Id()<<","<<Nodes[1]->Id()<<","<<Nodes[2]->Id()<<"]"<<std::endl;

                      //flat condition TO_ERASE
                      SearchUtilities::SetConditionToErase(i_ncond);
                      SearchUtilities::SetConditionToErase(*pEdgeCondition);

                      //std::cout<<" erase conditions ["<<i_ncond.Id()<<"] "<<i_ncond.GetGeometry()[0].Id()<<" "<<i_ncond.GetGeometry()[1].Id()<<" "<<i_ncond.GetGeometry()[2].Id()<<" cond edge ["<<pEdgeCondition->Id()<<"] "<<pEdgeCondition->GetGeometry()[0].Id()<<" "<<pEdgeCondition->GetGeometry()[1].Id()<<" "<<pEdgeCondition->GetGeometry()[2].Id()<<std::endl;
                      //condition will be removed

                      any_condition_removed = true;
                    }
                    else
                      KRATOS_WARNING("") << "Edge Shared Condition TO_ERASE" << std::endl;
                  }
                  else
                    KRATOS_WARNING("") << "Edge Shared Condition not found" << std::endl;

                  break;
                }
              }
            }

          }

        }
        else
        {
          i_node.Set(TO_ERASE, false);
          std::cout << " FINALLY NOT Removing node [" << i_node.Id() << "] (BOUNDARY: " << i_node.Is(BOUNDARY) << ",BLOCKED :" << i_node.Is(BLOCKED) <<",NEW_ENTITY: " << i_node.Is(NEW_ENTITY) << ",TO_ERASE: " << i_node.Is(TO_ERASE) << ",VISITED: " << i_node.Is(VISITED) << ")"<< std::endl;
        }
      }
      else
      {
        if (i_node.Is(BOUNDARY) && i_node.Is(TO_ERASE)){
          if(i_node.Is(NEW_ENTITY))
            i_node.Set(NEW_ENTITY, false);
          else
            i_node.Set(TO_ERASE, false);
          std::cout<<" Restore modified node to not erase "<<i_node.Id()<<std::endl;
        }
      }

    }

    //reset flags for the local process execution
    SetFlagsUtilities::SetFlagsToNodes(rModelPart, {{VISITED}}, {VISITED.AsFalse()});

    return any_condition_removed;

    KRATOS_CATCH(" ")
  }


  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Private Static Member Variables
  ///@{
  ///@}
  ///@name Private Static Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{

  //*******************************************************************************************
  //*******************************************************************************************

  void ReplaceConditionNode(Condition &rCondition, NodeType &rNode, NodeType::Pointer &pNode)
  {
    GeometryType &rGeometry = rCondition.GetGeometry();

    for (const auto & i_node : rGeometry)
      if (i_node.Id() == pNode->Id())
        SearchUtilities::SetConditionToErase(rCondition);

    if (rCondition.IsNot(TO_ERASE))
      for (unsigned int i = 0; i < rGeometry.size(); ++i)
        if (rGeometry[i].Id() == rNode.Id())
          rGeometry(i) = pNode;

    this->EraseDuplicatedFace(rCondition,*pNode);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool DuplicatedFace(NodeType &rNode, std::vector<NodeType::Pointer> Nodes)
  {
    ConditionWeakPtrVectorType &nConditions = rNode.GetValue(NEIGHBOUR_CONDITIONS);
    std::vector<int> v1;
    for (auto &i_node : Nodes)
      v1.push_back(i_node->Id());

    for (auto &i_cond : nConditions)
    {
      std::vector<int> v2;
      for (auto &j_node : i_cond.GetGeometry())
        v2.push_back(j_node.Id());

      if (std::is_permutation(v1.begin(), v1.end(), v2.begin())){
        std::cout<<" Duplicated Face "<<std::endl;
        return true;
      }
    }
    return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void EraseDuplicatedFace(Condition &rCondition,NodeType &rNode)
  {
    ConditionWeakPtrVectorType &nConditions = rNode.GetValue(NEIGHBOUR_CONDITIONS);

    std::vector<int> v1;
    for (auto &i_node : rCondition.GetGeometry())
      v1.push_back(i_node.Id());

    for (auto &i_cond : nConditions)
    {
      if (i_cond.Id() != rCondition.Id() && i_cond.IsNot(TO_ERASE))
      {
        std::vector<int> v2;
        for (auto &j_node : i_cond.GetGeometry())
          v2.push_back(j_node.Id());

        if (std::is_permutation(v1.begin(), v1.end(), v2.begin())){
          std::cout<<" Set Condition ["<<rCondition.Id()<<"] to Erase "<<std::endl;
          SearchUtilities::SetConditionToErase(rCondition);
        }
      }
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void EraseDuplicatedFace(NodeType &iNode, NodeType &jNode)
  {
    ConditionWeakPtrVectorType &inConditions = iNode.GetValue(NEIGHBOUR_CONDITIONS);
    ConditionWeakPtrVectorType &jnConditions = jNode.GetValue(NEIGHBOUR_CONDITIONS);

    for (auto &i_cond : inConditions)
    {
      std::vector<int> v1;
      for (auto &i_node : i_cond.GetGeometry())
        v1.push_back(i_node.Id());

      for (auto &j_cond : jnConditions)
      {
        if (i_cond.Id() != j_cond.Id() && j_cond.IsNot(TO_ERASE))
        {
          std::vector<int> v2;
          for (auto &j_node : j_cond.GetGeometry())
            v2.push_back(j_node.Id());

          if (std::is_permutation(v1.begin(), v1.end(), v2.begin())){
            std::cout<<" Set Condition ["<<i_cond.Id()<<"] to Erase "<<std::endl;
            SearchUtilities::SetConditionToErase(i_cond);
          }
        }
      }
     }
  }

  //**************************************************************************
  //**************************************************************************

  bool CheckContactNode(NodeType& rNode, bool& active_contact)
  {
    active_contact = false;
    if(rNode.Is(CONTACT))
    {
      if (rNode.SolutionStepsDataHas(CONTACT_FORCE))
      {
        array_1d<double, 3> &ContactForceNormal = rNode.FastGetSolutionStepValue(CONTACT_FORCE);
        if (norm_2(ContactForceNormal) > 0)
        {
          active_contact = true;
          return true;
        }
      }
      return true;
    }

    return false;
  }

  //**************************************************************************
  //**************************************************************************

  Condition::Pointer CreateCondition(const ConditionType &rCondition, const std::vector<NodeType::Pointer> &rNodes, const unsigned int &id)
  {
    KRATOS_TRY

    //clone condition
    Condition::Pointer pCondition = rCondition.Clone(id, {rNodes.begin(),rNodes.end()});

    //transfer data
    TransferUtilities::AddConditionData(rCondition, *pCondition);
    //add flags
    SearchUtilities::SetConditionToErase(*pCondition,false);

    for (auto& i_node : rNodes){
      i_node->Set(VISITED,true);
      i_node->Set(TO_ERASE,false);
    }

    return pCondition;

    KRATOS_CATCH(" ")
  }

  //**************************************************************************
  //**************************************************************************

  bool CheckConditions(ConditionWeakPtrVectorType &nConditions, const std::vector<Flags> Flags = {TO_ERASE})
  {
    for(auto& i_ncond : nConditions)
      if (i_ncond.Is(Flags)){
        return true;
      }
    return false;
  }

  //**************************************************************************
  //**************************************************************************

  bool CheckFlagInConditionNodes(ConditionWeakPtrVectorType &nConditions, const std::vector<Flags> Flags = {VISITED})
  {
    for (auto& i_ncond :  nConditions)
      for (auto& i_node : i_ncond.GetGeometry())
        if (i_node.Is(Flags))
          return true;
    return false;
  }

  //**************************************************************************
  //**************************************************************************

  void RemoveNonConvexBoundary(ModelPart &rModelPart, unsigned int &boundary_nodes_removed)
  {
    KRATOS_TRY

    if (mEchoLevel > 0)
      std::cout << "   [ REMOVE NON CONVEX BOUNDARY : " << std::endl;

    this->SetLocalFlagsForSearches(rModelPart);

    // Sizes critical angle
    double critical_angle = -120;

    //set flags for the local process execution
    //SetFlagsUtilities::SetFlagsToNodes(rModelPart, {{VISITED}}, {VISITED.AsFalse()});

    bool any_condition_removed = false;
    double number_of_conditions = rModelPart.NumberOfConditions();

    unsigned int id = ModelPartUtilities::GetMaxConditionId(rModelPart) + 1;

    //nodes
    unsigned int i = 0, j = 0;

    const std::vector<Flags> Flags = {BOUNDARY, NEW_ENTITY.AsFalse(), RIGID.AsFalse(), CONTACT.AsFalse()};

    double factor = 0.1;
    MesherData MeshData;
    for (auto i_node(rModelPart.NodesBegin()); i_node != rModelPart.NodesEnd(); ++i_node)
    {
      RefineData& Refine = MeshData.GetRefineData(*i_node, mrRemesh, rModelPart.GetProcessInfo());
      if (i_node->Is(Flags))
      {
        ConditionWeakPtrVectorType &nConditions = i_node->GetValue(NEIGHBOUR_CONDITIONS);
        if (!this->CheckConditions(nConditions, {TO_ERASE}) && nConditions.size()>1)
        {
          i = 0; j = 1;
          if (nConditions[0].GetGeometry()[0].Id() == i_node->Id())
            std::swap(i,j);

          //neighbour conditions in 2D:   (Node0) ---[c1]--- (i_node) ---[c2]---(Node2)
          Geometry<NodeType> &c1Geometry = nConditions[i].GetGeometry();
          Geometry<NodeType> &c2Geometry = nConditions[j].GetGeometry();

          bool remove_condition = false;
          if (GeometryUtilities::CalculateFaceSize(c1Geometry) < factor * Refine.Boundary.DistanceMeshSize ||
              GeometryUtilities::CalculateFaceSize(c2Geometry) < factor * Refine.Boundary.DistanceMeshSize)
            remove_condition = true;

          if (!this->CheckFlagInConditionNodes(nConditions, {TO_ERASE}))
          {
            if(remove_condition)
            {
              //std::cout<<"     Condition ["<<nConditions[i].Id()<<"] TO_ERASE in remove non convex boundary coarsen_boundary_mesher_process.hpp"<<std::endl;
              //std::cout<<"     Condition ["<<nConditions[j].Id()<<"] TO_ERASE in remove non convex boundary coarsen_boundary_mesher_process.hpp"<<std::endl;
              //std::cout<<"     Node ["<<i_node->Id()<<"] TO_ERASE in remove non convex boundary coarsen_boundary_mesher_process.hpp"<<std::endl;

              //select condition nodes
              std::vector<NodeType::Pointer> Nodes = {c1Geometry(0), c2Geometry(1)};
              (rModelPart.Conditions()).push_back(this->CreateCondition(nConditions[i],Nodes,id++));

              //set flags
              //Old path:   (Node0) ---[c1]--- (i_node) ---[c2]---(Node2)
              SearchUtilities::SetConditionToErase(nConditions[i]);
              SearchUtilities::SetConditionToErase(nConditions[j]);
              i_node->Set(TO_ERASE);

              //New path:   (Node0) ---[cA]--- (Node2)
              c1Geometry[0].Set(TO_ERASE, false); // do not release Node0
              c2Geometry[1].Set(TO_ERASE, false); // do not release Node2

              any_condition_removed = true;
              boundary_nodes_removed++;
            }
            else
            {
              double side_projection; //[c1]-[c2]
              GeometryUtilities::CalculateEdgesProjection(*i_node, c1Geometry[0], c2Geometry[1], side_projection);

              double normal_projection; //[c1]-[c2]
              GeometryUtilities::CalculateNormalsProjection(nConditions[i], nConditions[j], normal_projection);

              //angles
              double condition_angle = 0;
              double relative_angle = 0;
              if (normal_projection != 0){
                relative_angle = side_projection;// / normal_projection;

                if (relative_angle <= 1 && relative_angle >= -1)
                  condition_angle = (180.0 / Globals::Pi) * std::acos(relative_angle);

                array_1d<double, 3> S1, N2;
                S1[0] = c1Geometry[1].X() - c1Geometry[0].X();
                S1[1] = c1Geometry[1].Y() - c1Geometry[0].Y();
                S1[2] = 0;
                noalias(N2) = nConditions[j].GetValue(NORMAL);

                //std::cout<<" S1 "<<S1<<" N2 "<<N2<<" angle "<<condition_angle<<std::endl;

                if (inner_prod(S1, N2) < 0)
                  condition_angle *= (-1);
              }

              //std::cout<<" side_projection "<<side_projection<<" normal_projection "<<normal_projection<<" condition_angle "<<condition_angle<<std::endl;

              if (condition_angle < critical_angle)
              {
                //Path of neighbour conditions in 2D:   (NodeA) ---[c0]--- (Node0) ---[c1]--- (i_node) ---[c2]--- (Node2) --- [c3]--- (NodeB)

                //std::cout<<"     Condition Critical Angle ["<<nConditions[i].Id()<<"] TO_ERASE in remove non convex boundary coarsen_boundary_mesher_process.hpp"<<std::endl;
                //std::cout<<"     Condition Critical Angle ["<<nConditions[j].Id()<<"] TO_ERASE in remove non convex boundary coarsen_boundary_mesher_process.hpp"<<std::endl;
                //std::cout<<"     Condition angle "<<condition_angle<<" Critial Angle "<<critical_angle<<std::endl;

                //set i_node to a new position (between 0 and 2)
                i_node->Coordinates() = 0.5 * (c1Geometry[0].Coordinates() + c2Geometry[1].Coordinates());

                //assign data to dofs
                const VariablesList &VariablesList = rModelPart.GetNodalSolutionStepVariablesList();
                const unsigned int &step_data_size = rModelPart.GetNodalSolutionStepDataSize();

                std::vector<NodeType::Pointer> Nodes = {c1Geometry(0),c2Geometry(1)};
                PointsArrayType face(Nodes.begin(),Nodes.end());
                Geometry<NodeType> cGeometry(face);
                Vector ShapeFunctionsN(2,0.5);

                TransferUtilities::InterpolateStepData(*i_node, cGeometry, ShapeFunctionsN, step_data_size, 1.0);

                //recover the original position of the node
                i_node->GetInitialPosition() = Point{i_node->Coordinates() - i_node->FastGetSolutionStepValue(DISPLACEMENT)};

                //New conditions profile in 2D:  (NodeA) ---[c0]--- (i_node) ---[c3]--- (NodeB)   where i_node has a new position

                //search shared condition of Node0 and Node A
                ConditionWeakPtrVectorType &n0Conditions = c1Geometry[0].GetValue(NEIGHBOUR_CONDITIONS);
                unsigned int k = 0;
                if (n0Conditions[0].Id() == c1Geometry[0].Id())
                  k = 1;

                Geometry<NodeType> &c0Geometry = n0Conditions[k].GetGeometry();
                //select condition nodes NodeA-i_node
                Nodes = {c0Geometry(0), c1Geometry(1)};
                (rModelPart.Conditions()).push_back(this->CreateCondition(n0Conditions[k],Nodes,id++));


                //search shared condition of Node2 and Node B
                ConditionWeakPtrVectorType &n2Conditions = c2Geometry[1].GetValue(NEIGHBOUR_CONDITIONS);
                unsigned int r = 1;
                if (n2Conditions[0].Id() == c2Geometry[1].Id())
                  r = 0;

                Geometry<NodeType> &c3Geometry = n2Conditions[r].GetGeometry();
                //select condition nodes i_node-NodeB
                Nodes = {c1Geometry(1), c3Geometry(1)};
                (rModelPart.Conditions()).push_back(this->CreateCondition(n2Conditions[r],Nodes,id++));

                //set flags
                //Old path:  (NodeA) ---[c0]--- (Node0) ---[c1]--- (i_node) ---[c2]--- (Node2) --- [c3]--- (NodeB)
                SearchUtilities::SetConditionToErase(n0Conditions[k]); //release condition [c0]
                SearchUtilities::SetConditionToErase(nConditions[i]);  //release condition [c1]
                SearchUtilities::SetConditionToErase(nConditions[j]);  //release condition [c2]
                SearchUtilities::SetConditionToErase(n2Conditions[r]); //release condition [c3]

                c1Geometry[0].Set(TO_ERASE,true);  //release Node0
                c2Geometry[1].Set(TO_ERASE,true);  //release Node2

                //New path:  (NodeA) ---[cA]--- (i_node) ---[cB]--- (NodeB)   where i_node has a new position
                i_node->Set(TO_ERASE,false); // do not release i_node
                c0Geometry[0].Set(TO_ERASE, false); // do not release NodeA
                c3Geometry[1].Set(TO_ERASE, false); // do not release NodeB

                any_condition_removed = true;
                boundary_nodes_removed++;
              }
            }
          }
          else{
            std::cout<<" Some neighbour conditions TO_ERASE "<< std::endl;
          }
        }
      }
    }

    //reset flags for the local process execution
    //SetFlagsUtilities::SetFlagsToNodes(rModelPart, {{VISITED}}, {VISITED.AsFalse()});

    if (any_condition_removed)
      ModelPartUtilities::EraseConditions(rModelPart);


    if (mEchoLevel > 0)
    {
      std::cout << "     [ CONDITIONS ( removed : " << number_of_conditions - rModelPart.Conditions().size() << " ) ]" << std::endl;
      std::cout << "     REMOVE NON CONVEX BOUNDARY ]; " << std::endl;
    }

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{

  /// Assignment operator.
  CoarsenConditionsMesherProcess &operator=(CoarsenConditionsMesherProcess const &rOther);

  ///@}

}; // Class Process

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                CoarsenConditionsMesherProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const CoarsenConditionsMesherProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_COARSEN_BOUNDARY_MESHER_PROCESS_HPP_INCLUDED  defined
