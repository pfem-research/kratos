//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

#if !defined(KRATOS_REFINE_ELEMENTS_IN_EDGES_MESHER_PROCESS_HPP_INCLUDED)
#define KRATOS_REFINE_ELEMENTS_IN_EDGES_MESHER_PROCESS_HPP_INCLUDED

// External includes

// System includes

// Project includes
#include "custom_processes/mesher_process.hpp"

namespace Kratos
{

///@name Kratos Classes
///@{

/// Refine Mesh Elements Process 2D and 3D
/** The process inserts nodes in the edge elements to be splitted in the remeshing process
*/

class RefineElementsInEdgesMesherProcess
    : public MesherProcess
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of Process
  KRATOS_CLASS_POINTER_DEFINITION(RefineElementsInEdgesMesherProcess);

  typedef ModelPart::ConditionType ConditionType;
  typedef ModelPart::PropertiesType PropertiesType;
  typedef ConditionType::GeometryType GeometryType;

  typedef GlobalPointersVector<Element> ElementWeakPtrVectorType;
  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  RefineElementsInEdgesMesherProcess(ModelPart &rModelPart,
                                     MesherData::MeshingParameters &rRemeshingParameters,
                                     int EchoLevel)
      : mrModelPart(rModelPart),
        mrRemesh(rRemeshingParameters)
  {
    mEchoLevel = EchoLevel;
  }

  /// Destructor.
  virtual ~RefineElementsInEdgesMesherProcess() {}

  ///@}
  ///@name Operators
  ///@{

  /// This operator is provided to call the process as a function and simply calls the Execute method.
  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  /// Execute method is used to execute the Process algorithms.
  void Execute() override
  {
    KRATOS_TRY

    if ((mrRemesh.Refine->Interior.Options.Is(RefineData::REFINE) ||
         mrRemesh.Refine->Boundary.Options.Is(RefineData::REFINE)))
    {
      if (mEchoLevel > 0)
      {
        std::cout << " [ SELECT EDGE ELEMENTS TO REFINE : " << std::endl;
      }

      //1.- Select Elements to split (edge elements with all nodes as boundary-free surface)
      std::vector<Element*> EdgedElements;
      this->SelectFullBoundaryEdgedElements(mrModelPart, EdgedElements);

      //2.- Select Inside Faces to refine
      std::vector<Geometry<Node<3>>> ListOfFacesToSplit;
      std::vector<Node<3>::Pointer> ListOfTipNodes;
      this->SelectFacesToSplit(EdgedElements, ListOfFacesToSplit, ListOfTipNodes, mrModelPart.GetProcessInfo());

      //3.- Create and insert new nodes
      std::vector<Node<3>::Pointer> ListOfNewNodes;
      this->GenerateNewNodes(mrModelPart, ListOfNewNodes, ListOfFacesToSplit, ListOfTipNodes);
      mrRemesh.Info->Inserted.Nodes += ListOfNewNodes.size();

      //4.- Insert new nodes to model part
      this->SetNodesToModelPart(mrModelPart, ListOfNewNodes);

      if (mEchoLevel > 0)
      {
        std::cout << "   Visited Elements: " << EdgedElements.size() << " new nodes " << ListOfNewNodes.size() << std::endl;
        std::cout << "   SELECT EDGE ELEMENTS TO REFINE ]; " << std::endl;
      }
    }

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "RefineElementsInEdgesMesherProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "RefineElementsInEdgesMesherProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{

  ModelPart &mrModelPart;

  MesherData::MeshingParameters &mrRemesh;

  int mEchoLevel;

  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{
  ///@}

  virtual void SelectFullBoundaryEdgedElements(ModelPart &rModelPart,
                                               std::vector<Element*> &rEdgedElements)
  {
    KRATOS_TRY

    bool full_boundary = false;
    bool corner_node = false;
    const double dimension = rModelPart.GetProcessInfo()[SPACE_DIMENSION];
    ModelPart::ElementsContainerType &rElements = rModelPart.Elements();
    // std::cout<<" block edges "<<std::endl;
    // block_for_each(
    //     rElements,
    //     [dimension,&full_boundary,&corner_node,&rEdgedElements](Element& i_elem)
    //     {

    for (auto& i_elem : rElements)
    {
      full_boundary = true;
      for (auto& i_node : i_elem.GetGeometry())
      {
        if (i_node.IsNot(BOUNDARY)){
          full_boundary = false;
          break;
        }
      }

      if (full_boundary)
      {
        corner_node = true;
        if (dimension == 3){
          corner_node = false;
          for (auto& i_node : i_elem.GetGeometry())
          {
            if (i_node.GetValue(NEIGHBOUR_ELEMENTS).size() == 1){
              corner_node = true;
              break;
            }
          }
        }
        if (corner_node)
          rEdgedElements.push_back(&i_elem);
      }
    }

    // );
    // std::cout<<" block edges end"<<std::endl;
    KRATOS_CATCH("")
  }

  void SelectFacesToSplit(std::vector<Element*> &rEdgedElements,
                          std::vector<Geometry<Node<3>>> &rListOfFacesToSplit,
                          std::vector<Node<3>::Pointer> &rListOfTipNodes,
                          const ProcessInfo &rCurrentProcessInfo)
  {
    KRATOS_TRY

    DenseMatrix<unsigned int> lpofa; //connectivities of points defining faces
    DenseVector<unsigned int> lnofa; //number of points defining faces

    for (auto& i_elem : rEdgedElements)
    {
      ElementWeakPtrVectorType &nElements = i_elem->GetValue(NEIGHBOUR_ELEMENTS);

      MesherData MeshData;
      RefineData& Refine = MeshData.GetRefineData(*i_elem, mrRemesh, rCurrentProcessInfo);

      unsigned int face = 0;
      bool accepted_face = false;
      for (auto &i_nelem : nElements)
      {
        if (i_nelem.Id() != i_elem->Id()) // If there is a shared element in face nf
        {
          accepted_face = true;

          Geometry<Node<3>> &rGeometry = i_elem->GetGeometry();

          rGeometry.NodesInFaces(lpofa);
          rGeometry.NumberNodesInFaces(lnofa);

          unsigned int NumberNodesInFace = lnofa[face];
          Condition::NodesArrayType FaceNodes;
          FaceNodes.reserve(NumberNodesInFace);

          unsigned int split_counter = 0;
          unsigned int counter = 0;
          for (unsigned int j = 1; j <= NumberNodesInFace; ++j)
          {
            if (rGeometry[lpofa(j, face)].Is(TO_SPLIT))
              ++split_counter;

            FaceNodes.push_back(rGeometry(lpofa(j, face)));

            //do not SPLIT small edges only big edges, else a lot of volume is added
            if (mrModelPart.Is(FLUID) && counter > 0)
            {
              if ((4.5 * Refine.Interior.DistanceMeshSize) > (norm_2(FaceNodes[counter].Coordinates() - FaceNodes[counter - 1].Coordinates())))
              {
                accepted_face = false;
                //std::cout<<" SMALL EDGE NOT SPLITED "<<(norm_2(FaceNodes[counter].Coordinates()-FaceNodes[counter-1].Coordinates()))<<std::endl;
              }
            }
            ++counter;
          }

          if (split_counter == NumberNodesInFace)
            accepted_face = false;

          unsigned int rigid_free_surface = 0;
          if (accepted_face)
          {

            if (mrModelPart.Is(FLUID))
            {
              for (unsigned int i = 0; i < FaceNodes.size(); ++i)
              {
                if (FaceNodes[i].Is(STRUCTURE) && FaceNodes[i].Is(FREE_SURFACE))
                {
                  ++rigid_free_surface;
                }
              }

              if (rigid_free_surface != 0)
              {
                accepted_face = false;
              }
            }
          }

          if (accepted_face)
          {
            Geometry<Node<3>> InsideFace(FaceNodes);
            rListOfFacesToSplit.push_back(InsideFace);
            rListOfTipNodes.push_back(rGeometry(lpofa(0, face)));

            //set TO_SPLIT to make the insertion unique
            for (unsigned int i = 0; i < FaceNodes.size(); ++i)
              FaceNodes[i].Set(TO_SPLIT);
            break;
          }
        }
        ++face;
      }
    }

    // reset TO_SPLIT
    for (auto &i_face : rListOfFacesToSplit)
    {
      for (unsigned int i = 0; i < i_face.size(); ++i)
      {
        i_face[i].Set(TO_SPLIT, false);
      }
    }

    // std::cout<<" rEdgeElements "<< rBoundaryEdgedElements.size()<<std::endl;
    // std::cout<<" FacesToSplit "<<rListOfFacesToSplit.size()<<std::endl;

    KRATOS_CATCH("")
  }

  void MoveCenterInside(const Geometry<Node<3>> &rFace, double &xc, double &yc, double &zc, const double &radius)
  {
    KRATOS_TRY

    array_1d<double, 3> Position;
    double projection = 0;
    for (auto &i_node : rFace)
    {
      if (i_node.Or({RIGID,SOLID}))
      {
        const array_1d<double, 3> &Normal = i_node.FastGetSolutionStepValue(NORMAL);
        Position = i_node.Coordinates();

        Position[0] -= xc;
        Position[1] -= yc;
        Position[2] -= zc;

        projection = inner_prod(Position, Normal);
        if (projection > 0 && (projection < 0.5 * radius))
        {
          Position = (0.5 * radius) * Normal;
          xc -= Position[0];
          yc -= Position[1];
          zc -= Position[2];
        }
      }
    }

    //std::cout<<" Modify position EGDE ("<<xc<<","<<yc<<","<<zc<<")"<<std::endl;

    KRATOS_CATCH("")
  }

  void MoveCenterInside(const Node<3>::Pointer &rTip, double &xc, double &yc, double &zc, const double &radius)
  {
    KRATOS_TRY

    array_1d<double, 3> Position;

    Position = rTip->Coordinates();

    Position[0] -= xc;
    Position[1] -= yc;
    Position[2] -= zc;

    Position /= 3;
    xc += Position[0];
    yc += Position[1];
    zc += Position[2];


    //std::cout<<" Modify position EGDE ("<<xc<<","<<yc<<","<<zc<<")"<<std::endl;

    KRATOS_CATCH("")
  }

  void GenerateNewNodes(ModelPart &rModelPart,
                        std::vector<Node<3>::Pointer> &rListOfNewNodes,
                        std::vector<Geometry<Node<3>>> &rListOfFacesToSplit,
                        std::vector<Node<3>::Pointer> &rListOfTipNodes)
  {
    KRATOS_TRY

    Node<3>::Pointer pNode;

    //center
    double xc = 0;
    double yc = 0;
    double zc = 0;

    //radius
    double radius = 0;

    //assign data to dofsv
    Node<3>::DofsContainerType &ReferenceDofs = rModelPart.Nodes().front().GetDofs();

    VariablesList &VariablesList = rModelPart.GetNodalSolutionStepVariablesList();
    const unsigned int &step_data_size = rModelPart.GetNodalSolutionStepDataSize();

    Vector ShapeFunctionsN;

    unsigned int id = ModelPartUtilities::GetMaxNodeId(rModelPart.GetParentModelPart()) + 1;

    unsigned int size = 0;
    unsigned int count = 0;

    for (auto &i_face : rListOfFacesToSplit)
    {
      size = i_face.size();

      ShapeFunctionsN.resize(size);

      if (size == 2)
        GeometryUtilities::CalculateCenterAndRadius(i_face[0].X(), i_face[0].Y(),
                                                    i_face[1].X(), i_face[1].Y(),
                                                    xc, yc, radius);

      if (size == 3)
      {
        GeometryUtilities::CalculateCenterAndRadius(i_face[0].X(), i_face[0].Y(), i_face[0].Z(),
                                                    i_face[1].X(), i_face[1].Y(), i_face[1].Z(),
                                                    i_face[2].X(), i_face[2].Y(), i_face[2].Z(),
                                                    xc, yc, zc, radius);

        //move new node to an inside safe position
        this->MoveCenterInside(rListOfTipNodes[count], xc, yc, zc, radius);
      }


      //create a new node
      pNode = Kratos::make_intrusive<Node<3>>(id, xc, yc, zc);

      //giving model part variables list to the node
      pNode->SetSolutionStepVariablesList(&VariablesList);

      //set buffer size
      pNode->SetBufferSize(rModelPart.GetBufferSize());


      //generating the dofs
      for (auto &i_dof : ReferenceDofs)
      {
        Node<3>::DofType &rDof = *i_dof;
        Node<3>::DofType::Pointer pNewDof = pNode->pAddDof(rDof);

        // in rigid edges set it fix has no sense:
        (pNewDof)->FreeDof();

        // count = 0;
        // for( unsigned int i = 0; i<size; ++i )
        // {
        //   if(i_face[i].IsFixed(rDof.GetVariable()))
        //     count++;
        // }

        // if( count == size )
        //   (pNewDof)->FixDof();
        // else
        //   (pNewDof)->FreeDof();
      }

      std::fill(ShapeFunctionsN.begin(), ShapeFunctionsN.end(), 1.0 / double(size));


      //TransferUtilities::InterpolateStepData(*pNode, i_face, ShapeFunctionsN, VariablesList, 1.0);
      //TransferUtilities::InterpolateStepData(*pNode, i_face, ShapeFunctionsN, step_data_size, 1.0);
      //set include stepdata from tip node
      double factor =  double(size)/double(size+1);
      TransferUtilities::InterpolateStepData(pNode->SolutionStepData(), *rListOfTipNodes[count], i_face, ShapeFunctionsN, step_data_size, factor);


      //set flags from one of the nodes
      pNode->AssignFlags(i_face[0]);

      //unset domain flags
      std::vector<Flags> FlagsArray = {RIGID,SOLID,FLUID};
      SetFlagsUtilities::SetFlags(*pNode, FlagsArray, false);

      //unset boundary flags
      FlagsArray = {BOUNDARY,FREE_SURFACE,STRUCTURE,INLET,SLIP};
      SetFlagsUtilities::SetFlags(*pNode, FlagsArray, false);

      //unset interior layer flags
      FlagsArray = {INTERACTION,OUTLET,INSIDE};
      SetFlagsUtilities::SetFlags(*pNode, FlagsArray, false);

      //set domain flag
      if (rModelPart.Is(FLUID))
        pNode->Set(FLUID);

      if (rModelPart.Is(SOLID))
        pNode->Set(SOLID);

      //set active flag
      pNode->Set(ACTIVE, true);

      //set it as new entity
      pNode->Set(NEW_ENTITY, true);

      //set variables
      this->SetNewNodeVariables(rModelPart, i_face, pNode);

      rListOfNewNodes.push_back(pNode);

      count++;
      id++;
    }

    KRATOS_CATCH("")
  }

  void SetNewNodeVariables(ModelPart &rModelPart, Geometry<Node<3>> &rGeometry, Node<3>::Pointer &pNode)
  {
    KRATOS_TRY

    //set model part
    pNode->SetValue(MODEL_PART_NAME, rModelPart.Name());

    GeometryType::Pointer Vertices;
    const unsigned int dimension = rGeometry.WorkingSpaceDimension();
    const unsigned int number_points = rGeometry.size();
    if (number_points == 2)
    {
      if (dimension == 2)
        Vertices = Kratos::make_shared<Line2D2<Node<3>>>(rGeometry);
      else
        Vertices = Kratos::make_shared<Line3D2<Node<3>>>(rGeometry);
    }
    else if (number_points == 3)
    {
      if (dimension == 2)
        Vertices = Kratos::make_shared<Line2D3<Node<3>>>(rGeometry);
      else
        Vertices = Kratos::make_shared<Triangle3D3<Node<3>>>(rGeometry);
    }

    //set nodal_h
    pNode->FastGetSolutionStepValue(NODAL_H) = Vertices->Length();
    //std::cout<<" Set domain size node["<<pNode->Id()<<"] nodal_h = "<<pNode->FastGetSolutionStepValue(NODAL_H)<<std::endl;
    //double &nodal_h = pNode->FastGetSolutionStepValue(NODAL_H);
    //if (pNode->FastGetSolutionStepValue(NODAL_H) < 2.0 * mrRemesh.Refine->Boundary.DistanceMeshSize)
    //  nodal_h = mrRemesh.Refine->Boundary.DistanceMeshSize * 2.0;

    // bool mesh_movement = false;
    // if(pNode->SolutionStepsDataHas(MESH_DISPLACEMENT)){
    //   mesh_movement = true;
    // }

    // if( rModelPart.Is(FLUID) ){
    //   //smooth variables
    //   NodeWeakPtrVectorType& nNodes = pNode->GetValue(NEIGHBOUR_NODES);

    //   for(auto& i_nnode : nNodes)
    //   {
    //     MesherUtilities::SmoothFluidVariables(*pNode,i_nnode);
    //     MesherUtilities::SmoothFluidVariables(*pNode,i_nnode,1);

    //     if(mesh_movement){
    //       MesherUtilities::SmoothFluidMeshVariables(*pNode,i_nnode);
    //       MesherUtilities::SmoothFluidMeshVariables(*pNode,i_nnode,1);
    //     }
    //   }
    //   double quotient = 1.0/double(nNodes.size()+1);

    //   MesherUtilities::MultiplyFluidVariables(*pNode,quotient);
    //   MesherUtilities::MultiplyFluidVariables(*pNode,quotient,1);

    //   if(mesh_movement){
    //     MesherUtilities::MultiplyFluidMeshVariables(*pNode,quotient);
    //     MesherUtilities::MultiplyFluidMeshVariables(*pNode,quotient,1);
    //   }

    // }

    //set original position
    pNode->GetInitialPosition() = Point(pNode->Coordinates() - pNode->FastGetSolutionStepValue(DISPLACEMENT));

    //reset contact force
    if (pNode->SolutionStepsDataHas(CONTACT_FORCE))
      pNode->FastGetSolutionStepValue(CONTACT_FORCE).clear();

    KRATOS_CATCH("")
  }

  void SetNodesToModelPart(ModelPart &rModelPart, std::vector<Node<3>::Pointer> &rListOfNewNodes)
  {
    KRATOS_TRY

    if (rListOfNewNodes.size())
    {
      //add new conditions: ( SOLID body model part )
      for (auto &i_node : rListOfNewNodes)
      {
        // std::cout<<" Node ["<<i_node->Id()<<"]"<<std::endl;
        // std::cout<<" eMD "<<i_node->FastGetSolutionStepValue(DISPLACEMENT)<<" eMD1 "<<i_node->FastGetSolutionStepValue(DISPLACEMENT,1)<<std::endl;
        // std::cout<<" eMD "<<i_node->FastGetSolutionStepValue(TEMPERATURE)<<" eMD1 "<<i_node->FastGetSolutionStepValue(TEMPERATURE,1)<<std::endl;

        // std::cout<<" eMD "<<i_node->FastGetSolutionStepValue(MESH_DISPLACEMENT)<<" eMD1 "<<i_node->FastGetSolutionStepValue(MESH_DISPLACEMENT,1)<<std::endl;
        // std::cout<<" eMV "<<i_node->FastGetSolutionStepValue(MESH_VELOCITY)<<" eMV1 "<<i_node->FastGetSolutionStepValue(MESH_VELOCITY,1)<<std::endl;
        // std::cout<<" eMA "<<i_node->FastGetSolutionStepValue(MESH_ACCELERATION)<<" eMA1 "<<i_node->FastGetSolutionStepValue(MESH_ACCELERATION,1)<<std::endl;

        rModelPart.Nodes().push_back(i_node);
      }
    }

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Private Static Member Variables
  ///@{
  ///@}
  ///@name Private Static Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{

  /// Assignment operator.
  RefineElementsInEdgesMesherProcess &operator=(RefineElementsInEdgesMesherProcess const &rOther);

  /// this function is a private function

  /// Copy constructor.
  //Process(Process const& rOther);

  ///@}

}; // Class Process

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                RefineElementsInEdgesMesherProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const RefineElementsInEdgesMesherProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_REFINE_ELEMENTS_IN_EDGES_MESHER_PROCESS_HPP_INCLUDED  defined
