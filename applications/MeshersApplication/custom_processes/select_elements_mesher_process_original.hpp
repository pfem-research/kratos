//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

#if !defined(KRATOS_SELECT_ELEMENTS_MESHER_PROCESS_HPP_INCLUDED)
#define KRATOS_SELECT_ELEMENTS_MESHER_PROCESS_HPP_INCLUDED

// External includes

// System includes

// Project includes
#include "custom_utilities/mesher_utilities.hpp"

#include "custom_processes/mesher_process.hpp"

namespace Kratos
{

///@name Kratos Classes
///@{

/// Refine Mesh Elements Process 2D and 3D
/** The process labels the elements to be refined in the mesher
    it applies a size constraint to elements that must be refined.

*/
class SelectElementsMesherProcess
    : public MesherProcess
{
public:
  ///@name Type Definitions
  ///@{

  typedef Node<3> NodeType;
  typedef Geometry<NodeType> GeometryType;

  /// Pointer definition of Process
  KRATOS_CLASS_POINTER_DEFINITION(SelectElementsMesherProcess);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  SelectElementsMesherProcess(ModelPart &rModelPart,
                              MesherData::MeshingParameters &rRemeshingParameters,
                              int EchoLevel)
      : mrModelPart(rModelPart),
        mrRemesh(rRemeshingParameters)
  {
    mEchoLevel = EchoLevel;
  }

  /// Destructor.
  virtual ~SelectElementsMesherProcess() {}

  ///@}
  ///@name Operators
  ///@{

  /// This operator is provided to call the process as a function and simply calls the Execute method.
  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  /// Execute method is used to execute the Process algorithms.
  void Execute() override
  {
    KRATOS_TRY

    BuiltinTimer time_counter;

    if (mEchoLevel > 0)
      std::cout << " [ SELECT MESH ELEMENTS: (" << mrRemesh.OutMesh.GetNumberOfElements() << ") " << std::endl;

    const int &OutNumberOfElements = mrRemesh.OutMesh.GetNumberOfElements();
    mrRemesh.PreservedElements.clear();
    mrRemesh.PreservedElements.resize(OutNumberOfElements);
    std::fill(mrRemesh.PreservedElements.begin(), mrRemesh.PreservedElements.end(), 0);
    mrRemesh.MeshElementsSelectedFlag = true;

    mrRemesh.Info->Current.Elements = 0;

    if (mEchoLevel > 0)
      std::cout << "   Start Element Selection " << OutNumberOfElements << std::endl;

    if (mrRemesh.ExecutionOptions.IsNot(MesherData::SELECT_TESSELLATION_ELEMENTS))
    {
      for (int el = 0; el < OutNumberOfElements; ++el)
      {
        mrRemesh.PreservedElements[el] = 1;
        mrRemesh.Info->Current.Elements += 1;
      }
    }
    else
    {
      //set flags for the local process execution marking slivers
      SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{SELECTED}}, {SELECTED.AsFalse()});

      this->SelectElements();
      if (mEchoLevel > 0)
        std::cout << "   Finished Element Selection " << std::endl;

      if (mrModelPart.Is(FLUID))
        this->CheckRigidElementNeighbours();

      if (mrModelPart.IsNot(CONTACT))
        this->SelectNodesToErase();

      //set flags for the local process execution marking slivers
      SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{SELECTED}}, {SELECTED.AsFalse()});
    }


    if (mEchoLevel > 0)
    {
      std::cout << "   SELECT MESH ELEMENTS (" << mrRemesh.Info->Current.Elements << ") ]; " << std::endl;

      if (mrRemesh.Options.Is(MesherData::CONSTRAINED))
      {
        int released_elements = mrRemesh.OutMesh.GetNumberOfElements() - mrRemesh.Info->Current.Elements;
        if (released_elements > 0)
          std::cout << "   RELEASED ELEMENTS (" << released_elements << ") IN CONSTRAINED MESH ]; " << std::endl;
      }
    }

    if( mEchoLevel >= 0 )
      std::cout<<"  SELECT ELEMENTS time = "<<time_counter.ElapsedSeconds()<<std::endl;

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "SelectElementsMesherProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "SelectElementsMesherProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{

  ModelPart &mrModelPart;

  MesherData::MeshingParameters &mrRemesh;

  MesherUtilities mMesherUtilities;

  int mEchoLevel;

  struct NodalFlags
  {

    unsigned int Solid;
    unsigned int Fluid;
    unsigned int Rigid;
    unsigned int Boundary;
    unsigned int FreeSurface;
    unsigned int NoWallFreeSurface;
    unsigned int Contact;
    unsigned int Inlet;
    unsigned int Isolated;
    unsigned int Sliver;
    unsigned int NewEntity;
    unsigned int Visited;
    unsigned int Slave;
    unsigned int Interaction;
    unsigned int Inside;

    double Radius;

    //constructor
    NodalFlags()
    {
      Solid = 0;
      Fluid = 0;
      Rigid = 0;
      Boundary = 0;
      FreeSurface = 0;
      NoWallFreeSurface = 0;
      Contact = 0;
      Inlet = 0;
      Isolated = 0;
      Sliver = 0;
      NewEntity = 0;
      Visited = 0;
      Radius = 0;
      Slave = 0;
      Interaction = 0;
      Inside = 0;
    }

    //counter method
    void CountFlags(const NodeType &rNode)
    {
      if (rNode.Is(SOLID))
        ++Solid;
      if (rNode.Is(FLUID))
        ++Fluid;
      if (rNode.Is(RIGID))
        ++Rigid;
      if (rNode.Is(BOUNDARY))
        ++Boundary;
      if (rNode.Is(CONTACT))
        ++Contact;
      if (rNode.Is(INLET))
        ++Inlet;
      if (rNode.Is(ISOLATED))
        ++Isolated;
      if (rNode.Is(FREE_SURFACE))
      {
        ++FreeSurface;
        if (rNode.IsNot({SOLID,RIGID}))
          ++NoWallFreeSurface;
      }
      if (rNode.Is(MARKER))
        ++Sliver;
      if (rNode.Is(NEW_ENTITY))
        ++NewEntity;
      if (rNode.Is(VISITED))
        ++Visited;
      if (rNode.Is(SLAVE))
        ++Slave;
      if (rNode.Is(INTERACTION))
        ++Interaction;
      if (rNode.Is(INSIDE))
        ++Inside;

      Radius += rNode.FastGetSolutionStepValue(NODAL_H);
    }
  };

  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{

  void SelectElements()
  {
    KRATOS_TRY

    const int &OutNumberOfElements = mrRemesh.OutMesh.GetNumberOfElements();

    if (OutNumberOfElements==0)
      KRATOS_ERROR << " 0 elements to select : NO ELEMENTS GENERATED " <<std::endl;

    unsigned int number_of_slivers = 0;
    unsigned int passed_boundary = 0;
    unsigned int passed_alpha_shape = 0;
    unsigned int passed_inner_outer = 0;

    //reassign fluid and rigid flags to eliminate full rigid elements
    //if( !IsFirstTimeStep() )
    this->LabelEdgeNodes(mrModelPart);

    unsigned int dimension = 0;
    unsigned int number_of_vertices = 0;

    this->GetElementDimension(dimension, number_of_vertices);

    const int *OutElementList = mrRemesh.OutMesh.GetElementList();

    ModelPart::NodesContainerType::iterator nodes_begin = mrModelPart.NodesBegin();

    int el = 0;
    int number = 0;

    //set flags for the local process execution marking slivers
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{SELECTED}}, {SELECTED.AsFalse()});

    ModelPartUtilities::CheckNodalH(mrModelPart);

    //#pragma omp parallel for reduction(+:number) //get same node from model part and flags set in vertices in parallel it is not threadsafe...
    for (el = 0; el < OutNumberOfElements; ++el)
    {
      GeometryType Vertices;
      NodalFlags VerticesFlags;

      bool accepted = this->GetVertices(el, nodes_begin, OutElementList, VerticesFlags, Vertices, number_of_vertices);

      if (!accepted)
        continue;

      // monitoring bools (for checking purposes)
      // bool boundary_accepted = false;
      // bool alpha_accepted = false;
      // bool subdomain_accepted = false;
      // bool center_accepted = false;
      // bool shape_accepted = false;

      const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();
      const double &TimeStep = rCurrentProcessInfo[DELTA_TIME];

      double Size = VerticesFlags.Radius;

      if (mrRemesh.Options.Is(MesherData::REFINE)) //defined Size for Interior and Boundary
      {
        MesherData MeshData;
        RefineData& Refine = MeshData.GetRefineData(Vertices, mrRemesh, rCurrentProcessInfo);
        Size = Refine.Interior.DistanceMeshSize;
      }

      // directly dismiss Fluid elements if criterion not fulfilled
      if (mrModelPart.Is(FLUID))
        accepted = this->CheckElementBoundaries(Vertices, VerticesFlags, Size, dimension, TimeStep);

      double Alpha = mrRemesh.AlphaParameter;
      this->GetAlphaParameter(Alpha, Vertices, VerticesFlags, Size, dimension, TimeStep);

      // if(VerticesFlags.Rigid == 4 && VerticesFlags.Fluid>0)
      // {
      //   std::cout<<" Alpha rigid "<<Alpha<<std::endl;
      //   accepted = true;
      // }

      //Alpha = 1.5;

      MesherUtilities MesherUtils;

      //2.- to control the alpha size
      if (accepted)
      {
        ++passed_boundary;
        //boundary_accepted = true;
        if (mrRemesh.Options.Is(MesherData::CONTACT_SEARCH))
        {
          accepted = GeometryUtilities::ShrankAlphaShape(Alpha, Vertices, mrRemesh.OffsetFactor, dimension);
        }
        else
        {
          if (mrModelPart.Is(FLUID))
          {
            accepted = GeometryUtilities::AlphaShape(Alpha, Vertices, dimension, 4.0 * Size);
          }
          else
          { //SOLID
            if (mrRemesh.Options.Is(MesherData::CONSTRAINED)) {
              accepted = true;
              if ( dimension == 2 && VerticesFlags.Boundary == Vertices.size() ) { //delete generated holes in constrained mesh
                if (CheckSharedBoundaryFace(Vertices))
                  accepted = false; //accepted = GeometryUtilities::AlphaShape(2.0, Vertices, dimension);
                else
                  accepted = GeometryUtilities::AlphaShape(3.0, Vertices, dimension);

              } //must be reviewed to check constrained faces consistency
            } else {
              accepted = GeometryUtilities::AlphaShape(Alpha, Vertices, dimension);
            }
          }
        }
      }

      //3.- to control all nodes from the same subdomain (problem, domain is not already set for new inserted particles on mesher)
      bool self_contact = false;
      if (accepted)
      {
        //alpha_accepted = true;
        ++passed_alpha_shape;

        if (mrRemesh.Options.Is(MesherData::CONTACT_SEARCH))
          self_contact = MesherUtils.SameSubdomain(Vertices);
      }

      //4.- to control that the element is inside of the domain boundaries
      if (accepted)
      {
        //subdomain_accepted = true;
        if (mrRemesh.Options.Is(MesherData::CONTACT_SEARCH))
        {
          //problems in 3D: be careful
          if (self_contact)
            accepted = GeometryUtilities::CheckOuterCentre(Vertices, mrRemesh.OffsetFactor, self_contact);
        }
        else
        {
          //accepted=GeometryUtilities::CheckInnerCentre(Vertices); //problems in 3D: when slivers are released, a boundary is created and the normals calculated, then elements that are inside suddently its center is calculated as outside... // some corrections are needded.
        }
      }

      //5.- to control that the element has a good shape
      if (accepted)
      {
        //center_accepted = true;
        ++passed_inner_outer;
        accepted = this->CheckElementShape(Vertices, VerticesFlags, Size, dimension, TimeStep, number_of_slivers);
      }

      // if all checks have been passed, accept the element
      if (accepted)
      {
        //shape_accepted = true;
        number += 1;
        mrRemesh.PreservedElements[el] = number;
      }
      // else{
      //   std::cout<<" Element ["<<el<<"] with alpha "<<mrRemesh.AlphaParameter<<"("<<Alpha<<")"<<std::endl;
      //   for( unsigned int n=0; n<number_of_vertices; ++n)
      //   {
      //     std::cout<<" ("<<n+1<<"): Id["<<Vertices[n].Id()<<"] PreID["<<mrRemesh.NodalPreIds[Vertices[n].Id()]<<"] "<<Vertices[n].Coordinates()<<std::endl;
      //   }
      //   std::cout<<" (alpha:"<<alpha_accepted<<" subdomain:"<<subdomain_accepted<<" center:"<<center_accepted<<" shape:"<<shape_accepted<<") "<< std::endl;

      // }
    }

    mrRemesh.Info->Current.Elements = number;

    if (mEchoLevel > 0)
    {
      std::cout << "  [Preserved Elements " << mrRemesh.Info->Current.Elements << "] (" << mrModelPart.NumberOfElements() << ") :: (slivers detected: " << number_of_slivers << ") " << std::endl;
      std::cout << "  (passed_boundary: "<<passed_boundary<<", passed_alpha_shape: " << passed_alpha_shape << ", passed_inner_outer: " << passed_inner_outer << ") Total out: " <<OutNumberOfElements<< std::endl;
    }

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  // void MesherData::SetSelectedMesh(MeshingParameters &rMeshingVariables, const unsigned int dimension, const unsigned int number_of_vertices)
  // {
  //   KRATOS_TRY

  //   MesherData::MeshContainer &OutMesh = rMeshingVariables.OutMesh;

  //   MeshContainer SelectedMesh;

  //   //delete isolated nodes in SOLID if not Keep flag

  //   //set nodes to selected mesh

  //   SelectedMesh.CreatePointList(rModelPart.Nodes().size(), dimension);

  //   double *PointList = SelectedMesh.GetPointList();
  //   int &NumberOfPoints = SelectedMesh.GetNumberOfPoints();


  //   //set elements to selected mesh

  //   SelectedMesh.CreateElementList(mrRemesh.Info->Current.Elements, number_of_vertices);
  //   int *ElementList = SelectedMesh.GetElementList();
  //   int &NumberOfElements = SelectedMesh.GetNumberOfElements();

  //   int base = 0;
  //   for (unsigned int el = 0; el < (unsigned int)NumberOfElements; ++el)
  //   {
  //     for (unsigned int pn = 0; pn < number_of_vertices; ++pn)
  //     {
  //       unsigned int id = el * number_of_vertices + pn;

  //       if (OutElementList[id] <= 0)

  //   }

  //   KRATOS_CATCH("")
  // }


  //*******************************************************************************************
  //*******************************************************************************************

  bool GetVertices(int &el, ModelPart::NodesContainerType::iterator nodes_begin, const int *OutElementList, NodalFlags &rVerticesFlags, GeometryType &rVertices, unsigned int &number_of_vertices)
  {
    KRATOS_TRY

    for (unsigned int pn = 0; pn < number_of_vertices; ++pn)
    {
      unsigned int id = el * number_of_vertices + pn;

      //std::cout<<" pn "<<pn<<" id "<<OutElementList[id]<<" pre_ids_size "<<mrRemesh.NodalPreIds.size()<<" preid "<<mrRemesh.NodalPreIds[OutElementList[id]]<<std::endl;

      if (OutElementList[id] <= 0)
        std::cout << " ERROR: something is wrong: nodal id < 0 " << el << std::endl;

      //check if the number of nodes are considered in the nodal pre ids
      if ((unsigned int)OutElementList[id] >= mrRemesh.NodalPreIds.size())
      {
        std::cout << " ERROR: something is wrong: node added by the mesher : " << (unsigned int)OutElementList[id] << std::endl;
        if (mrRemesh.Options.Is(MesherData::CONTACT_SEARCH))
          return false;
      }

      //check if the point is a vertex of an artificial external bounding box
      if (mrRemesh.NodalPreIds[OutElementList[id]] < 0)
      {
        if (mrRemesh.Options.IsNot(MesherData::CONTACT_SEARCH))
        {
          std::cout << " ERROR: something is wrong: nodal id < 0 " << std::endl;
        }
        return false;
      }

      //get node from model part and set it as vertex
      rVertices.push_back(*(nodes_begin + OutElementList[id] - 1).base());

      //vertices flags
      rVerticesFlags.CountFlags(rVertices.back());
    }

    // std::cout<<" selected vertices ["<<OutElementList[el*number_of_vertices];
    // for(unsigned int d=1; d<number_of_vertices; ++d)
    // {
    //   std::cout<<", "<<OutElementList[el*number_of_vertices+d];
    // }
    // std::cout<<"] "<<std::endl;

    // std::cout<<" model vertices ["<<rVertices[0].Id();
    // for(unsigned int d=1; d<number_of_vertices; ++d)
    // {
    //   std::cout<<", "<<rVertices[d].Id();
    // }
    // std::cout<<"] "<<std::endl;

    return true;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool IsFirstTimeStep()
  {
    const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();
    const double &CurrentTime = rCurrentProcessInfo[TIME];
    const double &TimeStep = rCurrentProcessInfo[DELTA_TIME];
    if (CurrentTime <= TimeStep)
      return true;
    else
      return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void LabelEdgeNodes(ModelPart &rModelPart)
  {

    //reset domain flags in nodes before new assignment
    if (rModelPart.Is(FLUID))
    {
      //set flags for the local process execution
      SetFlagsUtilities::SetFlagsToElements(mrModelPart, {{VISITED}}, {VISITED.AsFalse()});

      //set label to full rigid element nodes
      for (auto &i_elem : rModelPart.Elements())
      {
        GeometryType &rGeometry = i_elem.GetGeometry();

        bool rigid = true;
        for (auto &i_node : rGeometry)
        {
          if (i_node.IsNot(RIGID))
          {
            rigid = false;
            break;
          }
        }
        //to release full rigid elements with no fluid surrounding them
        if (rigid)
        {
          i_elem.Set(VISITED, true);
          for (auto &i_node : rGeometry)
          {
            i_node.Set(VISITED, true);
          }
        }
      }

      //unset label for nodes not fully rigid
      for (auto &i_elem : rModelPart.Elements())
      {
        GeometryType &rGeometry = i_elem.GetGeometry();

        //to release full rigid elements with no fluid surrounding them
        if (i_elem.IsNot(VISITED))
        {
          for (auto &i_node : rGeometry)
          {
            i_node.Set(VISITED, true);
          }
        }
        else
          i_elem.Set(VISITED, false);
      }

    }

  }

  //*******************************************************************************************
  //*******************************************************************************************

  void SelectNodesToErase()
  {
    // Set disconnected nodes to erase
    unsigned int dimension = 0;
    unsigned int number_of_vertices = 0;
    unsigned int isolated_nodes = 0;

    this->GetElementDimension(dimension, number_of_vertices);

    int *OutElementList = mrRemesh.OutMesh.GetElementList();

    ModelPart::NodesContainerType &rNodes = mrModelPart.Nodes();

    const int &OutNumberOfElements = mrRemesh.OutMesh.GetNumberOfElements();

    //set flags for the local process execution
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{BLOCKED}}, {BLOCKED.AsFalse()});

    //check engaged nodes
    for (int el = 0; el < OutNumberOfElements; ++el)
    {
      if (mrRemesh.PreservedElements[el])
      {
        for (unsigned int pn = 0; pn < number_of_vertices; ++pn)
        {
          //set vertices
          rNodes[OutElementList[el * number_of_vertices + pn]].Set(BLOCKED);
        }
      }
    }

    int count_release = 0;
    for (ModelPart::NodesContainerType::iterator i_node = rNodes.begin(); i_node != rNodes.end(); ++i_node)
    {
      if (i_node->IsNot(BLOCKED))
      {

        if (mrModelPart.Is(FLUID))
        {
          if (i_node->Is(TO_ERASE) && i_node->Or({RIGID,SOLID,INLET})
          {
            i_node->Set(TO_ERASE, false);
            std::cout << " WARNING TRYING TO DELETE A WALL NODE (fluid): " << i_node->Id() << std::endl;
          }
          else if (i_node->Or({RIGID,SOLID,INLET}))
          {
            if (mrRemesh.ExecutionOptions.Is(MesherData::KEEP_ISOLATED_NODES))
            {
              if (i_node->IsNot(TO_ERASE))
              {
                if (i_node->Is(FREE_SURFACE))
                {
                  i_node->Set(ISOLATED, true);
                  ++isolated_nodes;
                  //std::cout<<" ISOLATED FREE SURFACE FLUID "<<std::endl;
                }
                else
                {
                  // in fact can be a new free surface node -> check if it happens
                  i_node->Set(TO_ERASE, true);
                  //std::cout<<" ISOLATED inside FLUID node TO ERASE in select_elements_mesher_process.hpp"<<i_node->IsNot(BLOCKED)<<std::endl;
                }
              }
            }
            else
            {
              std::cout<<" ISOLATED FLUID NODES NOT KEPT in select_elements_mesher_process.hpp"<<std::endl;
              i_node->Set(TO_ERASE, true);
              if (mEchoLevel > 0)
              {
                if (i_node->Is(BOUNDARY))
                  std::cout << " NODE " << i_node->Id() << " IS BOUNDARY RELEASE " << std::endl;
                else
                  std::cout << " NODE " << i_node->Id() << " IS DOMAIN RELEASE " << std::endl;
              }
              ++count_release;
            }
          }
        }
        else
        {

          if (i_node->Is({RIGID,TO_ERASE}))
          {
            i_node->Set(TO_ERASE, false);
            std::cout << " WARNING TRYING TO DELETE A WALL NODE (solid): " << i_node->Id() << std::endl;
          }
          else if (i_node->IsNot(RIGID))
          {
            if (mrRemesh.ExecutionOptions.Is(MesherData::KEEP_ISOLATED_NODES))
            {
              if (i_node->IsNot(TO_ERASE))
              {
                i_node->Set(ISOLATED, true);
                ++isolated_nodes;
                std::cout << " ISOLATED SOLID TO ERASE in select_elements_mesher_process.hpp" << std::endl;
              }
            }
            else
            {
              std::cout<<" ISOLATED SOLID NODES NOT KEPT in select_elements_mesher_process.hpp"<<std::endl;
              i_node->Set(TO_ERASE, true);
              if (mEchoLevel > 0)
              {
                if (i_node->Is(BOUNDARY))
                  std::cout << " NODE " << i_node->Id() << " IS BOUNDARY RELEASE " << std::endl;
                else
                  std::cout << " NODE " << i_node->Id() << " IS DOMAIN RELEASE " << std::endl;
              }
              ++count_release;
            }
          }
        }
      }
      else  // BLOCKED NODE
      {
        i_node->Set(TO_ERASE, false);
        i_node->Set(ISOLATED, false);
      }

      i_node->Set(BLOCKED, false);

      i_node->Set(VISITED, false);

      if (i_node->Is(SELECTED)) // Belongs to a SLIVER
        i_node->Set(MARKER, true);
      else
        i_node->Set(MARKER, false);

      i_node->Set(SELECTED, false);
    }

    if (mEchoLevel > 0)
    {
      std::cout << "   NUMBER OF RELEASED NODES " << count_release << std::endl;
      std::cout << "   NUMBER OF ISOLATED NODES " << isolated_nodes << std::endl;
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void CheckIds(const int *OutElementList, const int &OutNumberOfElements, const unsigned int number_of_vertices)
  {
    unsigned int max_out_id = 0;
    for (int el = 0; el < OutNumberOfElements; ++el)
    {
      for (unsigned int pn = 0; pn < number_of_vertices; ++pn)
      {
        unsigned int id = el * number_of_vertices + pn;
        if (int(max_out_id) < OutElementList[id])
          max_out_id = OutElementList[id];
      }
    }

    if (max_out_id >= mrRemesh.NodalPreIds.size())
      std::cout << " ERROR ID PRE IDS " << max_out_id << " > " << mrRemesh.NodalPreIds.size() << " (nodes size:" << mrModelPart.Nodes().size() << ")" << std::endl;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void GetElementDimension(unsigned int &dimension, unsigned int &number_of_vertices)
  {
    if (mrModelPart.NumberOfElements())
    {
      ModelPart::ElementsContainerType::iterator element_begin = mrModelPart.ElementsBegin();
      dimension = element_begin->GetGeometry().WorkingSpaceDimension();
      number_of_vertices = element_begin->GetGeometry().size();
    }
    else if (mrModelPart.NumberOfConditions())
    {
      ModelPart::ConditionsContainerType::iterator condition_begin = mrModelPart.ConditionsBegin();
      dimension = condition_begin->GetGeometry().WorkingSpaceDimension();
      if (dimension == 3) //number of nodes of a tetrahedron
        number_of_vertices = 4;
      else if (dimension == 2) //number of nodes of a triangle
        number_of_vertices = 3;
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  //it can set TO_ERASE to new entities inserted in full rigid elements
  bool CheckElementBoundaries(GeometryType &rVertices, const NodalFlags &rVerticesFlags, const double &rSize, const unsigned int &dimension, const double &rTimeStep)
  {
    unsigned int NumberOfVertices = rVertices.size();

    //do not accept full solid elements
    if (rVerticesFlags.Solid == NumberOfVertices)
      return false;

    //do not accept full rigid-solid elements (no fluid)
    if ((rVerticesFlags.Solid + rVerticesFlags.Rigid) >= NumberOfVertices && rVerticesFlags.Fluid == 0)
      return false;

    if ((rVerticesFlags.Solid + rVerticesFlags.Rigid) >= NumberOfVertices)
      return GeometryUtilities::CheckInnerCentre(rVertices);

    //do not accept new fluid elements with walls (at free surface) which mesh velocity is higher than velocity
    if (rVerticesFlags.Rigid > 0 && rVerticesFlags.FreeSurface > 0 && rVerticesFlags.Fluid != NumberOfVertices && KinematicUtilities::CheckMeshVelocity(rVertices))
      return false;

    if (rVerticesFlags.Rigid > 0 && rVerticesFlags.NoWallFreeSurface > 0)
    {
      if (GeometryUtilities::CheckFluidMeniscus(rVertices, dimension))
      {
        //do not accept full rigid elements (no fluid)
        if (rVerticesFlags.Rigid == NumberOfVertices && rVerticesFlags.Fluid > 0)
        {
          //accept when it has more than two fluid nodes (2D) and more than 3 fluid nodes (3D)
          if (rVerticesFlags.Fluid < NumberOfVertices - 1)
          {
            return false;
          }
          else if (rVerticesFlags.Fluid == NumberOfVertices && rVerticesFlags.Visited > 2 && rVerticesFlags.FreeSurface > 0)
          {
            return false;
          }
        }

        //do not accept full rigid elements with a new inserted node (no fluid)
        if ((rVerticesFlags.Rigid + rVerticesFlags.NewEntity) == NumberOfVertices && rVerticesFlags.Fluid > 0)
        {
          if (rVerticesFlags.Fluid == NumberOfVertices && rVerticesFlags.Visited >= NumberOfVertices - 1)
          {
            std::cout << " OLD RIGID NEW ENTITY EDGE DISCARDED (old_entity: " << rVerticesFlags.Visited << " fluid: " << rVerticesFlags.Fluid << " rigid: " << rVerticesFlags.Rigid << " free_surface: " << rVerticesFlags.FreeSurface << ")" << std::endl;
            return false;
          }
        }

        //do not accept rigid wall elements with fluid node separating from walls
        if (((rVerticesFlags.Rigid + rVerticesFlags.Solid) >= NumberOfVertices - 1) && rVerticesFlags.NoWallFreeSurface > 0)
        {
          MesherUtilities MesherUtils;
          double CriticalDistance = 1e-1 * rSize ;
          return KinematicUtilities::CheckFreeSurfaceApproachingWall(rVertices, CriticalDistance, rTimeStep);
        }
      }
    }
    return true;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void GetAlphaParameter(double &rAlpha, GeometryType &rVertices, const NodalFlags &rVerticesFlags, const double &rSize, const unsigned int &rDimension, const double &rTimeStep)
  {
    unsigned int NumberOfVertices = rVertices.size();

    rAlpha = mrRemesh.AlphaParameter;

    if (mrModelPart.Is(SOLID))
    {
      if (rVerticesFlags.Boundary >= NumberOfVertices)
        rAlpha *= 1.2;
    }
    else if (mrModelPart.Is(FLUID))
    {
      MesherUtilities MesherUtils;

      //avoid penetrating/scaping isolated nodes at large speeds and full free surface fluid nodes at large speeds
      if ((rVerticesFlags.Isolated + rVerticesFlags.FreeSurface) == NumberOfVertices)
      {
        if (rVerticesFlags.Isolated > 0)
        {
          const double MaxRelativeVelocity = 1.5; //arbitrary value :: (try a criteria depending on time and size)
          if (KinematicUtilities::CheckRelativeApproach(rVertices, MaxRelativeVelocity, rTimeStep))
          {
            rAlpha = 0;
            //set isolated nodes to erase if they approach too fast
            for (unsigned int i = 0; i < rVertices.size(); ++i)
            {
              if (rVertices[i].Is(ISOLATED))
              {
                rVertices[i].Set(TO_ERASE);
                std::cout<<" ISOLATED FLUID node ["<<rVertices[i].Id()<<"] fast approaching NODE NOT KEPT in select_elements_mesher_process.hpp"<<std::endl;
              }

            }
          }
        }
        // else if (rVerticesFlags.NoWallFreeSurface == NumberOfVertices)
        // {
        //   rAlpha = 0;
        //   std::cout<<" FULL FREE SURFACE ELEMENT not considered "<<std::endl;
        // }
        // else if( rVerticesFlags.NoWallFreeSurface == NumberOfVertices ){
        //   const double MaxRelativeVelocity = 4.5; //arbitrary value, will depend on time step (AF)
        //   if(GeometryUtilities::CheckRelativeVelocities(rVertices, MaxRelativeVelocity) ){
        //     KRATOS_WARNING("")<<" Attention two free surfaces approaching at high speed "<<std::endl;
        //   }
        // }
      }

      if (rAlpha != 0)
      {
        if (rDimension == 2)
        {
          this->GetTriangleFluidElementAlpha(rAlpha, rVertices, rVerticesFlags, rSize, rDimension, rTimeStep);
        }
        else if (rDimension == 3)
        {
          //this->GetTetrahedronFluidElementAlpha(rAlpha, rVertices, rVerticesFlags, rSize, rDimension, rTimeStep);
          this->GetTetrahedronFluidElementSimplerAlpha(rAlpha,rVertices,rVerticesFlags, rSize, rDimension,rTimeStep);
        }
      }
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void GetTriangleFluidElementAlpha(double &rAlpha, GeometryType &rVertices, const NodalFlags &rVerticesFlags, const double &rSize, const unsigned int &rDimension, const double &rTimeStep)
  {

    MesherUtilities MesherUtils;

    double VolumeChange = 0;
    double VolumeTolerance = 1.15e-4 * pow(4.0 * rSize , rDimension);
    unsigned int NumberOfVertices = rVertices.size();

    //criterion for elements formed with new wall nodes
    if (rVerticesFlags.Fluid != NumberOfVertices)
    { //element formed with a wall
      //there are not fluid nodes
      if (rVerticesFlags.Fluid == 0)
      {
        rAlpha = 0;
      }
      else
      {
        //if the element is decreasing its volume:
        VolumeChange = 0;
        if (KinematicUtilities::CheckVolumeDecrease(rVertices, rDimension, rTimeStep, VolumeTolerance, VolumeChange))
        {
          //accept new elements formed with isolated nodes (here speedy test passed)
          if (rVerticesFlags.Isolated > 0)
          {
            rAlpha *= 0.80;
          }
          //one wall vertex
          else if (rVerticesFlags.Rigid == 1 || rVerticesFlags.Solid == 1)
          {
            //to avoid new approaching wall elements with two non-wall free-surface nodes
            if (rVerticesFlags.NoWallFreeSurface == 2)
            {
              //std::cout<<" Vertices NWFS 2["<<rVertices[0].Id()<<","<<rVertices[1].Id()<<","<<rVertices[2].Id()<<"] ["<<rVertices[0].Is(RIGID)<<","<<rVertices[1].Is(RIGID)<<","<<rVertices[2].Is(RIGID)<<"]"<<std::endl;
              rAlpha *= 0.50;
            }
            else
              rAlpha *= 0.80;
          }
          //two wall vertices
          else if (rVerticesFlags.Rigid == 2 || rVerticesFlags.Solid == 2 || (rVerticesFlags.Rigid + rVerticesFlags.Solid) == 2)
          {
            //to avoid new approaching wall elements with only one non-wall free-surface node
            if (rVerticesFlags.NoWallFreeSurface == 1)
            {
              //std::cout<<" Vertices NWFS 1["<<rVertices[0].Id()<<","<<rVertices[1].Id()<<","<<rVertices[2].Id()<<"] ["<<rVertices[0].Is(RIGID)<<","<<rVertices[1].Is(RIGID)<<","<<rVertices[2].Is(RIGID)<<"]"<<std::endl;
              rAlpha *= 0.50;
            }
            else
            {
              rAlpha *= 0.80;
            }
          }
          //there are no wall vertices and all nodes are not fluid (impossible/inserted nodes by the mesher)
          else
          {
            rAlpha *= 0.80;
            std::cout<<" WARNING: new element with non-fluid particles and non wall-particles (rigid: "<<rVerticesFlags.Rigid<<" solid: "<<rVerticesFlags.Solid<<" fluid: "<<rVerticesFlags.Fluid<<" free-surface: "<<rVerticesFlags.FreeSurface<<")"<<std::endl;
          }
        }
        //if the element is not decreasing its volume:
        else
        {
          //if the element does not change the volume (all rigid or moving tangentially to the wall)
          if (VolumeChange > 0 && VolumeChange < VolumeTolerance)
          {
            rAlpha *= 0.95;
            //std::cout<<" CONSIDERING VOLUME CHANGE "<<VolumeChange<<std::endl;
          }
          else
          {
            //std::cout<<" Vertices Volume ["<<rVertices[0].Id()<<","<<rVertices[1].Id()<<","<<rVertices[2].Id()<<"] ["<<rVertices[0].Is(RIGID)<<","<<rVertices[1].Is(RIGID)<<","<<rVertices[2].Is(RIGID)<<"]"<<VolumeChange<<std::endl;
            if (VolumeChange == 0)
              rAlpha *= 0.40;
            else
              rAlpha = 0;
          }
        }
      }
    }
    //all nodes are fluid (pre-existing elements) or new elements formed in the free-surface
    else
    { //fluid element
      //all nodes in the free-surface
      if (rVerticesFlags.FreeSurface == 3)
      {
        //all nodes in the non-wall free-surface
        if (rVerticesFlags.NoWallFreeSurface == 3)
        {
          //if the element is decreasing its volume:
          VolumeChange = 0;
          //to avoid closing fluid voids with new elements
          if (KinematicUtilities::CheckVolumeDecrease(rVertices, rDimension, rTimeStep, VolumeTolerance, VolumeChange))
          {
            //to avoid too avoid approaching free surfaces at very high speeds
            const double MaxRelativeVelocity = 1.5; //arbitrary value :: (try a criteria depending on time and size)
            if (KinematicUtilities::CheckRelativeVelocities(rVertices, MaxRelativeVelocity))
              rAlpha = 0;
            else
              rAlpha *= 0.80;
          }
          //to avoid increasing fluid surface with new elements
          else
          {
            rAlpha *= 0.60;
          }
        }
        //two nodes in non-wall free-surface
        else if (rVerticesFlags.NoWallFreeSurface == 2)
        {
          rAlpha *= 0.80;
        }
        //one node in non-wall free-surface
        else if (rVerticesFlags.NoWallFreeSurface == 1)
        {
          rAlpha *= 0.80;
        }
        //one node in non-wall free-surface
        else
        {
          rAlpha *= 1.20;
        }
      }
      //two nodes in the free-surface
      else if (rVerticesFlags.FreeSurface == 2)
      {

        //to avoid closing fluid voids with new elements with a wall fluid node
        if (rVerticesFlags.NoWallFreeSurface == 2 && (rVerticesFlags.Rigid == 1 || rVerticesFlags.Solid == 1))
          rAlpha *= 0.80;
        else
          rAlpha *= 1.20;
      }
      else
      {
        rAlpha *= 1.20;
      }
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void GetTetrahedronFluidElementSimplerAlpha(double &rAlpha, GeometryType &rVertices, const NodalFlags &rVerticesFlags, const double &rSize, const unsigned int &rDimension, const double &rTimeStep)
  {
    MesherUtilities MesherUtils;
    double VolumeChange = 0;
    double VolumeTolerance = 2.5e-3 * pow(4.0 * rSize , rDimension);
    unsigned int NumberOfVertices = rVertices.size();

    if (rVerticesFlags.Fluid == 0)
    {
      rAlpha = 0;
    }
    else if (rVerticesFlags.Fluid != NumberOfVertices)
    { //outer
      //if the element is decreasing its volume:
      //to avoid closing fluid voids with new elements
      if (!KinematicUtilities::CheckVolumeDecrease(rVertices, rDimension, rTimeStep, VolumeTolerance, VolumeChange))
        rAlpha *= 0.50;
      else
        rAlpha *= 0.70;
      if (VolumeChange == 0)
        rAlpha *= 0.30;
    }
    else if (rVerticesFlags.NoWallFreeSurface == NumberOfVertices)
    { //outer
      rAlpha *= 0.70;
    }
    else
    { //inner
      if (GeometryUtilities::CheckInnerCentre(rVertices))
      {
        rAlpha *= 2.0;
      }
      else
      {
        rAlpha *= 0.70;
      }
    }
    if (rVerticesFlags.Rigid > 0  && rVerticesFlags.FreeSurface > 0)
      if (KinemticUtilities::CheckMeshVelocity(rVertices) && !KinematicUtilities::CheckVolumeDecrease(rVertices, rDimension, rTimeStep, VolumeTolerance, VolumeChange))
        rAlpha *= 0.4;

  }

  //*******************************************************************************************
  //*******************************************************************************************

  void GetTetrahedronFluidElementAlpha(double &rAlpha, GeometryType &rVertices, const NodalFlags &rVerticesFlags, const double &rSize, const unsigned int &rDimension, const double &rTimeStep)
  {
    MesherUtilities MesherUtils;

    double VolumeChange = 0;
    double VolumeTolerance = 2.5e-3 * pow(4.0 * rSize , rDimension);
    unsigned int NumberOfVertices = rVertices.size();

    //criterion for elements formed with new wall nodes
    if (rVerticesFlags.Fluid != NumberOfVertices)
    { //element formed with a wall

      //there are not fluid nodes
      if (rVerticesFlags.Fluid == 0)
      {
        rAlpha = 0;
      }
      else
      {
        //if the element is decreasing its volume:
        VolumeChange = 0;
        if (KinematicUtilities::CheckVolumeDecrease(rVertices, rDimension, rTimeStep, VolumeTolerance, VolumeChange))
        {
          //accept new elements formed with isolated nodes (here speedy test passed)
          if (rVerticesFlags.Isolated > 0)
          {
            rAlpha *= 0.9;
          }
          //one wall vertex
          else if ((rVerticesFlags.Rigid == 1 || rVerticesFlags.Solid == 1))
          {
            //to avoid new approaching wall elements with three non-wall free-surface nodes
            if (rVerticesFlags.NoWallFreeSurface == 3)
              rAlpha *= 0.7;
            else
              rAlpha *= 0.8;
          }
          //two wall vertices
          else if (rVerticesFlags.Rigid == 2 || rVerticesFlags.Solid == 2 || (rVerticesFlags.Rigid + rVerticesFlags.Solid) == 2)
          {
            //to avoid new approaching wall elements with two non-wall free-surface nodes
            if (rVerticesFlags.NoWallFreeSurface == 2)
              rAlpha *= 0.6;
            else
              rAlpha *= 0.90;
          }
          //three wall vertices
          else if (rVerticesFlags.Rigid == 3 || rVerticesFlags.Solid == 3 || (rVerticesFlags.Rigid + rVerticesFlags.Solid) == 3)
          {
            //to avoid new approaching wall elements with only one non-wall free-surface node
            if (rVerticesFlags.NoWallFreeSurface == 1)
            {
              if (rVerticesFlags.Fluid == 1)
                rAlpha *= 0.5;
              else
                rAlpha *= 0.7;
            }
            else
            {
              rAlpha *= 0.9;
            }
          }
          //four wall vertices
          else if (rVerticesFlags.Rigid == 4 || rVerticesFlags.Solid == 4 || (rVerticesFlags.Rigid + rVerticesFlags.Solid) == 4)
          {
            rAlpha *= 1.2; //keep full rigid and fluid elements - will be released using other criterion if needed
          }
          //there are no wall vertices
          else
          {
            rAlpha *= 0.9;
            //std::cout<<" WARNING: new element with non-fluid particles and non wall-particles (rigid: "<<rVerticesFlags.Rigid<<" solid: "<<rVerticesFlags.Solid<<" fluid: "<<rVerticesFlags.Fluid<<" free-surface: "<<rVerticesFlags.FreeSurface<<" new_entity: "<<rVerticesFlags.NewEntity<<" isolated: "<<rVerticesFlags.Isolated<<" old_entity: "<<rVerticesFlags.Visited<<")"<<std::endl;
          }
        }
        //if the element is not decreasing its volume:
        else
        {
          //if the element does not change the volume (all rigid or moving tangentially to the wall)
          if (VolumeChange > 0 && VolumeChange < VolumeTolerance && rVerticesFlags.Rigid == NumberOfVertices)
          {
            rAlpha *= 0.90;
          }
          else
          {
            //if( !(VolumeChange == 0 && MesherUtils.CheckInnerCentre(rVertices)) ){//no moving elements VolumeChange == 0 are accepted if previously inside
            if (rVerticesFlags.Interaction + rVerticesFlags.Rigid >= 4)
            {

              bool accepted = false;

              if (rVerticesFlags.Rigid == 4)
              {
                accepted = true;
              }
              else
              {
                for (auto &i_node : rVertices)
                {
                  if (accepted)
                    break;
                  if (i_node.Is(INTERACTION))
                  {

                    for (auto &j_node : rVertices)
                    {
                      if (j_node.Is(RIGID))
                      {
                        double projection = inner_prod(i_node.FastGetSolutionStepValue(NORMAL), j_node.FastGetSolutionStepValue(NORMAL));
                        if (projection > 0.35)
                        {
                          accepted = true;
                          break;
                        }
                      }
                    }
                  }
                }
              }
              if (accepted)
                rAlpha *= 0.7;
              else
                rAlpha = 0;
            }
            else
            {
              rAlpha = 0;
            }
          }
        }
      }
    }
    //all nodes are fluid (pre-existing elements) or new elements formed inside or in the free-surface
    else
    { //fluid element
      VolumeChange = 0;
      //all nodes in the free-surface
      if (rVerticesFlags.FreeSurface == 4 && rVerticesFlags.Sliver == 0)
      {

        //all nodes in the non-wall free-surface (free surface flat element can be artificial)
        if (rVerticesFlags.NoWallFreeSurface == 4)
        {
          //if the element is decreasing its volume:
          //to avoid closing fluid voids with new elements
          if (KinematicUtilities::CheckVolumeDecrease(rVertices, rDimension, rTimeStep, VolumeTolerance, VolumeChange))
          {
            rAlpha *= 0.80;
          }
          else
          { //to avoid increasing fluid surface with new elements
            rAlpha *= 0.50;
          }
        }
        //three nodes in non-wall free-surface
        else if (rVerticesFlags.NoWallFreeSurface == 3)
        {
          if (KinematicUtilities::CheckVolumeDecrease(rVertices, rDimension, rTimeStep, VolumeTolerance, VolumeChange))
            rAlpha *= 0.80;
          else
            rAlpha *= 0.60;
        }
        //two nodes in non-wall free-surface
        else if (rVerticesFlags.NoWallFreeSurface == 2)
        {
          if (KinematicUtilities::CheckVolumeDecrease(rVertices, rDimension, rTimeStep, VolumeTolerance, VolumeChange))
            rAlpha *= 0.80;
          else
            rAlpha *= 0.60;
        }
        //one node in non-wall free-surface
        else
        {
          VolumeChange = 1;
          rAlpha *= 0.90;
        }

        if (VolumeChange == 0)
          rAlpha *= 2.2;
      }
      else
      {
        if (rVerticesFlags.FreeSurface > 0 && rVerticesFlags.Rigid > 0)
        {
          if (KinematicUtilities::CheckVolumeDecrease(rVertices, rDimension, rTimeStep, VolumeTolerance, VolumeChange))
            rAlpha *= 1.65;
          else
            rAlpha *= 1.25;

          if (VolumeChange == 0)
            rAlpha *= 2.2;
        }
        else if (rVerticesFlags.FreeSurface == 0 && rVerticesFlags.Rigid == 3)
        {
          rAlpha *= 2.2; //accept large elements
        }
        else
        {
          rAlpha *= 1.9;
        }
      }
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckElementShape(GeometryType &rVertices, const NodalFlags &rVerticesFlags,  const double &rSize, const unsigned int &rDimension, const double &rTimeStep, unsigned int &number_of_slivers)
  {
    bool accepted = true;
    unsigned int NumberOfVertices = rVertices.size();

    if (mrModelPart.Is(SOLID))
    {
      int sliver = 0;
      if (rDimension == 3 && NumberOfVertices == 4)
      {
        Tetrahedra3D4<NodeType> Tetrahedron(rVertices);

        MesherUtilities MesherUtils;
        accepted = MesherUtils.CheckGeometryShape(Tetrahedron, sliver);

        if (sliver)
        {
          if (mrRemesh.Options.Is(MesherData::CONTACT_SEARCH))
            accepted = true;
          else
          {
            accepted = false;
            //do not release sliver elements in solid domains in constrained tessellation
            if (mrRemesh.Options.Is(MesherData::CONSTRAINED)){
              accepted = true;
              if (rVerticesFlags.Boundary == NumberOfVertices && CheckSharedBoundaryFace(rVertices,2))
                accepted = false;
            }
            else if (rVerticesFlags.Boundary < NumberOfVertices) //do not release inside slivers in solids
              accepted = true;

          }
          ++number_of_slivers;
        }
        else
        {
          if (mrRemesh.Options.Is(MesherData::CONTACT_SEARCH))
            accepted = false;
          else
            accepted = true;
        }
      }
    }
    else if (mrModelPart.Is(FLUID))
    {
      if (rDimension == 2 && NumberOfVertices == 3)
      {
        //check 2D distorted elements with edges decreasing very fast
        if (rVerticesFlags.NoWallFreeSurface > 1 && (rVerticesFlags.Rigid + rVerticesFlags.Solid) == 0)
        {
          double MaxEdgeLength = std::numeric_limits<double>::min();
          double MinEdgeLength = std::numeric_limits<double>::max();

          for (unsigned int i = 0; i < NumberOfVertices - 1; ++i)
          {
            for (unsigned int j = i + 1; j < NumberOfVertices; ++j)
            {
              double Length = norm_2(rVertices[j].Coordinates() - rVertices[i].Coordinates());
              if (Length < MinEdgeLength)
              {
                MinEdgeLength = Length;
              }
              if (Length > MaxEdgeLength)
              {
                MaxEdgeLength = Length;
              }
            }
          }
          MesherUtilities MesherUtils;
          const double MaxRelativeVelocity = 1.5; //arbitrary value :: (try a criteria depending on time and size)
          if (MinEdgeLength * 5 < MaxEdgeLength)
          {
            if (KinematicUtilities::CheckRelativeVelocities(rVertices, MaxRelativeVelocity))
            {
              std::cout << " WARNING 2D sliver " << std::endl;
              //commented to no erase but labeled to not calculate them
              //accepted = false;
              //only erase full free surface sliver elements (visualization purposes)
              if (rVerticesFlags.FreeSurface == NumberOfVertices)
                accepted = false;
              ++number_of_slivers;
              for (unsigned int i = 0; i < NumberOfVertices; ++i)
              {
                if (rVertices[i].Is(SELECTED))
                  std::cout << " WARNING Second sliver in the same node bis " << std::endl;
                rVertices[i].Set(SELECTED);
              }
            }
          }
        }
      }
      else if (rDimension == 3 && NumberOfVertices == 4)
      {
        if (rVerticesFlags.FreeSurface >= 2 || (rVerticesFlags.Boundary == 4 && rVerticesFlags.NoWallFreeSurface != 4))
        {

          Tetrahedra3D4<NodeType> Tetrahedron(rVertices);
          double Volume = Tetrahedron.Volume();

          if (Volume < 0.01 * pow(4.0 * rSize , rDimension))
          {
            //KRATOS_INFO("SLIVER")<<" Volume="<<Volume<<" VS Critical Volume="<<0.01*pow(4.0*rSize ,rDimension)<<std::endl;
            //std::cout<<" SLIVER Volume="<<Volume<<" VS Critical Volume="<< 0.01*pow(4.0*rSize ,rDimension) <<std::endl;

            //commented to no erase but labeled to not calculate them
            //accepted = false;
            //only erase full free surface sliver elements (visualization purposes)
            if (rVerticesFlags.FreeSurface == NumberOfVertices)
            {
              accepted = false;
            }
            else if ((rVerticesFlags.FreeSurface == NumberOfVertices - 1) && rVerticesFlags.Interaction > 1)
            {
              accepted = false;
              //std::cout<<" Release interaction SLIVER "<<std::endl;
            }

            ++number_of_slivers;

            if (accepted)
            {
              for (unsigned int i = 0; i < NumberOfVertices; ++i)
              {
                // if( rVertices[i].Is(SELECTED) )
                //   std::cout<<" WARNING Second sliver in the same node "<<std::endl;
                rVertices[i].Set(SELECTED);
              }
            }
          }
          // else if( Volume < 0.02*pow(4.0*rSize ,rDimension) ){
          //   int sliver = 0;
          //   MesherUtilities MesherUtils;
          //   bool distorted = MesherUtils.CheckGeometryShape(Tetrahedron,sliver);

          //   //std::cout << " SLIVER " << sliver <<" DISTORTED "<< distorted << std::endl;

          //   if( sliver ){
          //     std::cout<<" SLIVER SHAPE Volume="<<Volume<<" VS Critical Volume="<< 0.02*pow(4.0*rSize ,rDimension) <<std::endl;
          //     accepted = false;
          //     ++number_of_slivers;

          //     for(unsigned int i=0; i<rVertices.size(); ++i)
          //     {
          //       rVertices[i].Set(SELECTED);
          //     }

          //   }

          // }
        }
        else if ((rVerticesFlags.Rigid == 1 || rVerticesFlags.Rigid == 2) && rVerticesFlags.Inside == 0)
        {
          MesherUtilities MesherUtils;

          double VolumeIncrement = KinematicUtilities::GetDeformationGradientDeterminant(rVertices, rDimension, rTimeStep);
          if (mEchoLevel > 0)
            if (VolumeIncrement < 0)
              std::cout << "  NEGATIVE Volume Increment (detF: " << VolumeIncrement << ")" << std::endl;

          double MovedVolume = KinematicUtilities::GetMovedVolume(rVertices, rDimension, rTimeStep, 1.0);

          //check if the element is going to invert
          if (VolumeIncrement < 0.0 || MovedVolume < 0.0)
          {

            // std::cout<<" SLIVER FOR INVERSION "<<VolumeIncrement<<" VS Critical Volume="<< 0.01*pow(4.0*rSize ,rDimension) <<"( rigid: "<<rVerticesFlags.Rigid<<" ) moved volume "<<MovedVolume<<std::endl;

            //commented to no erase but labeled to not calculate them
            //accepted = false;

            ++number_of_slivers;

            for (unsigned int i = 0; i < NumberOfVertices; ++i)
            {
              // if( rVertices[i].Is(SELECTED) )
              //   std::cout<<" WARNING Second sliver in the same node bis "<<std::endl;
              rVertices[i].Set(SELECTED);
            }
          }
        }
      }
    }
    return accepted;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckSharedBoundaryFace(GeometryType &rVertices, unsigned int number = 1)
  {
    unsigned int size = rVertices.size();
    std::vector<unsigned int> permuta;
    if (size == 4)
      permuta = {0, 1, 2, 3, 0, 1};
    else if (size == 3)
      permuta = {0, 1, 2, 0, 1};
    else
      KRATOS_ERROR << " element with not supported geometry vertices :"<< size <<std::endl;
    unsigned int j = 0;
    unsigned int faces = 0;
    for (auto &i_node : rVertices) // loop on the 4 faces
    {
      ConditionWeakPtrVectorType &nConditions = i_node.GetValue(NEIGHBOUR_CONDITIONS);
      std::vector<int> v1;
      for (unsigned int i=0; i<size-1; ++i)
        v1.push_back(rVertices[permuta[j+i]].Id());

      for (auto &i_cond : nConditions)
      {
        std::vector<int> v2;
        for (auto &j_node : i_cond.GetGeometry())
          v2.push_back(j_node.Id());

        if (std::is_permutation(v1.begin(), v1.end(), v2.begin())){
          ++faces;
          break;
        }
      }
      if (faces>=number)
        return true;
      ++j;
    }
    return false;
  }

  //**************************************************************************
  //**************************************************************************

  void CheckRigidElementNeighbours()
  {
    KRATOS_TRY

    unsigned int dimension = 0;
    unsigned int number_of_vertices = 0;

    this->GetElementDimension(dimension, number_of_vertices);

    if (dimension == 3)
    {
      const int &OutNumberOfElements = mrRemesh.OutMesh.GetNumberOfElements();
      const int *OutElementList = mrRemesh.OutMesh.GetElementList();
      const int *OutElementNeighbourList = mrRemesh.OutMesh.GetElementNeighbourList();

      //ModelPart::NodesContainerType &rNodes = mrModelPart.Nodes();
      ModelPart::NodesContainerType::iterator nodes_begin = mrModelPart.NodesBegin();

      int number_of_released_elements = -1; // start the while loop

      int max_iters = 4;
      int iter = 0;
      while (number_of_released_elements != 0 && iter <= max_iters)
      {
        number_of_released_elements = 0;
        ++iter;
        #pragma omp parallel for reduction(+: number_of_released_elements)
        for (int el = 0; el < OutNumberOfElements; ++el)
        {
          if (mrRemesh.PreservedElements[el]>0)
          {
            unsigned int element_faces = 0;
            unsigned int rigid_nodes = 0;
            unsigned int free_surface_nodes = 0;
            unsigned int selected_boundary_nodes = 0;
            unsigned int isolated_nodes = 0;

            int index = 0;
            const unsigned int id = el * number_of_vertices;
            //defined for triangles and tetrahedra (number of vertices = number of faces)
            for (unsigned int i = 0; i < number_of_vertices; ++i)
            {
              index = OutElementNeighbourList[id + i];
              if (index > 0)
                index = mrRemesh.PreservedElements[index - 1];

              if (index <= 0){
                ++element_faces;
              }
              else{

                ModelPart::NodesContainerType::iterator it_node = nodes_begin + OutElementList[id + i] - 1;
                if (it_node->Is(ISOLATED))
                  ++isolated_nodes;

                if (it_node->Is(RIGID))
                  ++rigid_nodes;
                else if (it_node->Is(FREE_SURFACE))
                  ++free_surface_nodes;

                if (it_node->Is(MARKER) && it_node->Is(BOUNDARY))
                  ++selected_boundary_nodes;
              }
            }

            //check full rigid isolated element
            if (element_faces == number_of_vertices || selected_boundary_nodes == number_of_vertices)
            {
              mrRemesh.PreservedElements[el] = 0;
              ++number_of_released_elements;

              //Restore deleted part December 2020
              if (rigid_nodes >= number_of_vertices - 1)
              {
                mrRemesh.PreservedElements[el] = 0;
                ++number_of_released_elements;
                //std::cout<<" RELEASE ISOLATED RIGID ELEMENT "<<std::endl;
              }
              else if (rigid_nodes > 0 && free_surface_nodes > 0 && (rigid_nodes + free_surface_nodes == number_of_vertices))
              {
                mrRemesh.PreservedElements[el] = 0;
                ++number_of_released_elements;
                //std::cout<<" RELEASE ISOLATED ELEMENT WITH RIGID NODES "<<std::endl;
              }
              //Restore deleted part December 2020
            }
            else if (element_faces > 0)
            {
              if (rigid_nodes == number_of_vertices)
              {
                unsigned int rigid_neighbours = 0;
                unsigned int almost_rigid_neighbours = 0;

                for (unsigned int i = 0; i < number_of_vertices; ++i)
                {
                  index = OutElementNeighbourList[id + i];
                  if (index > 0)
                  {
                    if (mrRemesh.PreservedElements[index - 1] > 0)
                    {
                      unsigned int rigid_nnodes = 0;
                      const unsigned int nid = (index - 1) * number_of_vertices;
                      for (unsigned int j = 0; j < number_of_vertices; ++j)
                      {
                        ModelPart::NodesContainerType::iterator it_node = nodes_begin + OutElementList[nid + j] - 1;
                        if (it_node->Is(RIGID))
                          ++rigid_nnodes;
                      }

                      if (rigid_nnodes == number_of_vertices)
                        ++rigid_neighbours;
                      else if (rigid_nnodes == number_of_vertices - 1)
                        ++almost_rigid_neighbours;
                    }
                  }
                }

                if (element_faces == number_of_vertices - 1)
                {
                  rigid_neighbours += almost_rigid_neighbours;
                  // if( rigid_neighbours == (number_of_vertices-element_faces) )
                  //   std::cout<<" Surrounded with almost rigid "<<std::endl;
                }

                //erase rigid elements surrounded by rigid elements
                if (rigid_neighbours == (number_of_vertices - element_faces) && free_surface_nodes > 1)
                {
                  mrRemesh.PreservedElements[el] = 0;
                  ++number_of_released_elements;
                  std::cout<<" RELEASE SURROUNDED ISOLATED RIGID ELEMENT "<<std::endl;
                }
              }
              else if (rigid_nodes + isolated_nodes == number_of_vertices)
              {
                mrRemesh.PreservedElements[el] = 0;
                ++number_of_released_elements;
                std::cout<<" RELEASE ELEMENT WITH ISOLATED NODE/S set selected node for ALE "<<std::endl;
                for (unsigned int i = 0; i < number_of_vertices; ++i)
                {
                  ModelPart::NodesContainerType::iterator it_node = nodes_begin + OutElementList[id + i] - 1;
                   if (it_node->Is(ISOLATED))
                    it_node->Set(MARKER);
                }

                // MesherUtilities MesherUtils;
                // GeometryType Vertices;
                // for(unsigned int i = 0; i<number_of_vertices; ++i)
                // {
                //   Vertices.push_back(rNodes(OutElementList[el*number_of_vertices+i]));
                // }

                // if(!KinematicUtilities::CheckFreeSurfaceApproachingWall(Vertices) )
                // {
                //   mrRemesh.PreservedElements[el] = 0;
                //   ++number_of_released_elements;
                //   std::cout<<" RELEASE ELEMENT WITH ISOLATED NODE/s non approaching "<<std::endl;
                // }
              }
              else if (element_faces == number_of_vertices - 1)
              {

                if (free_surface_nodes == number_of_vertices && rigid_nodes <= number_of_vertices - 1)
                {
                  mrRemesh.PreservedElements[el] = 0;
                  ++number_of_released_elements;
                  //std::cout<<" RELEASE SINGLE ELEMENT "<<std::endl;
                }
              }
            }
          }
        }

        mrRemesh.Info->Current.Elements -= number_of_released_elements;
      }
      //std::cout<<" ITERS "<<iter<<std::endl;
    }

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Private Static Member Variables
  ///@{
  ///@}
  ///@name Private Static Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{
  ///@}

}; // Class Process

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                SelectElementsMesherProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const SelectElementsMesherProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_SELECT_ELEMENTS_MESHER_PROCESS_HPP_INCLUDED defined
