//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

#if !defined(KRATOS_REFINE_ELEMENTS_ON_THRESHOLD_MESHER_PROCESS_HPP_INCLUDED)
#define KRATOS_REFINE_ELEMENTS_ON_THRESHOLD_MESHER_PROCESS_HPP_INCLUDED

// External includes

// System includes

// Project includes
#include "custom_utilities/mesher_utilities.hpp"

#include "custom_processes/mesher_process.hpp"


namespace Kratos
{

///@name Kratos Classes
///@{

/// Refine Mesh Elements Process 2D and 3D
/** The process labels the nodes to be refined (TO_REFINE)
    if the ThresholdVariable is larger than a ReferenceThreshold
*/

class RefineElementsOnThresholdMesherProcess
    : public MesherProcess
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of Process
  KRATOS_CLASS_POINTER_DEFINITION(RefineElementsOnThresholdMesherProcess);

  typedef ModelPart::ConditionType ConditionType;
  typedef ModelPart::PropertiesType PropertiesType;
  typedef ConditionType::GeometryType GeometryType;

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  RefineElementsOnThresholdMesherProcess(ModelPart &rModelPart,
                                         MesherData::MeshingParameters &rRemeshingParameters,
                                         int EchoLevel)
      : mrModelPart(rModelPart),
        mrRemesh(rRemeshingParameters)
  {
    mEchoLevel = EchoLevel;
  }

  /// Destructor.
  virtual ~RefineElementsOnThresholdMesherProcess() {}

  ///@}
  ///@name Operators
  ///@{

  /// This operator is provided to call the process as a function and simply calls the Execute method.
  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  /// Execute method is used to execute the Process algorithms.
  void Execute() override
  {
    KRATOS_TRY

    if (mrRemesh.IsInterior(RefineData::REFINE) || mrRemesh.IsBoundary(RefineData::REFINE))
      this->SetNodesToRefine();

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "RefineElementsOnThresholdMesherProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "RefineElementsOnThresholdMesherProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Static Member Variables
  ///@{

  ModelPart &mrModelPart;

  MesherData::MeshingParameters &mrRemesh;

  int mEchoLevel;

  ///@}
  ///@name Un accessible methods
  ///@{

  void SetNodesToRefine()
  {
    KRATOS_TRY

    const ProcessInfo& rCurrentProcessInfo = mrModelPart.GetProcessInfo();
    ModelPart::ElementsContainerType &rElements = mrModelPart.Elements();
    MesherData MeshData;
    MesherData::MeshingParameters &rRemesh = mrRemesh;

    block_for_each(rElements,[&](Element &i_elem){
      RefineData& Refine = MeshData.GetRefineData(i_elem, rRemesh, rCurrentProcessInfo);

      if (Refine.Interior.Options.Is(RefineData::REFINE_ON_THRESHOLD))
      {
        for (auto& variable : Refine.Interior.ThresholdVariables)
        {
          if (MesherUtilities::CheckThreshold(i_elem, *variable.first, variable.second, rCurrentProcessInfo, Refine.Interior.SpecificThreshold))
          {
            for (auto& i_node : i_elem.GetGeometry())
              if(i_node.IsNot(BOUNDARY))
                i_node.Set(TO_REFINE);
          }
        }
      }
    }
    );

    KRATOS_CATCH("")
  }

  /// Assignment operator.
  RefineElementsOnThresholdMesherProcess &operator=(RefineElementsOnThresholdMesherProcess const &rOther);

  /// this function is a private function

  ///@}

}; // Class Process

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                RefineElementsOnThresholdMesherProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const RefineElementsOnThresholdMesherProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_REFINE_ELEMENTS_ON_THRESHOLD_MESHER_PROCESS_HPP_INCLUDED  defined
