//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

#if !defined(KRATOS_SELECT_ELEMENTS_MESHER_PROCESS_HPP_INCLUDED)
#define KRATOS_SELECT_ELEMENTS_MESHER_PROCESS_HPP_INCLUDED

// External includes

// System includes

// Project includes
#include "custom_utilities/mesher_utilities.hpp"
#include "custom_utilities/level_set_utilities.hpp"
#include "custom_processes/mesher_process.hpp"

namespace Kratos
{

///@name Kratos Classes
///@{

/// Refine Mesh Elements Process 2D and 3D
/** The process labels the elements to be refined in the mesher
    it applies a size constraint to elements that must be refined.

*/
class SelectElementsMesherProcess
    : public MesherProcess
{
public:
  ///@name Type Definitions
  ///@{

  typedef Node<3> NodeType;
  typedef Geometry<NodeType> GeometryType;

  /// Pointer definition of Process
  KRATOS_CLASS_POINTER_DEFINITION(SelectElementsMesherProcess);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  SelectElementsMesherProcess(ModelPart &rModelPart,
                              MesherData::MeshingParameters &rRemeshingParameters,
                              int EchoLevel)
      : mrModelPart(rModelPart),
        mrRemesh(rRemeshingParameters)
  {
    mEchoLevel = EchoLevel;
  }

  /// Destructor.
  virtual ~SelectElementsMesherProcess() {}

  ///@}
  ///@name Operators
  ///@{

  /// This operator is provided to call the process as a function and simply calls the Execute method.
  void operator()()
  {
    this->Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  /// Execute method is used to execute the Process algorithms.
  void Execute() override
  {
    KRATOS_TRY

    BuiltinTimer time_counter;

    if (mEchoLevel > 0)
      std::cout << " [ SELECT MESH ELEMENTS: (" << mrRemesh.OutMesh.GetNumberOfElements() << ") " << std::endl;

    const int &OutNumberOfElements = mrRemesh.OutMesh.GetNumberOfElements();
    mrRemesh.PreservedElements.clear();
    mrRemesh.PreservedElements.resize(OutNumberOfElements);
    std::fill(mrRemesh.PreservedElements.begin(), mrRemesh.PreservedElements.end(), 0);
    mrRemesh.MeshElementsSelectedFlag = true;

    mrRemesh.Info->Current.Elements = 0;

    if (mEchoLevel > 0)
      std::cout << "   Start Element Selection " << OutNumberOfElements << std::endl;

    if (mrRemesh.ExecutionOptions.IsNot(MesherData::SELECT_TESSELLATION_ELEMENTS))
    {
      for (int el = 0; el < OutNumberOfElements; ++el)
      {
        mrRemesh.PreservedElements[el] = 1;
        mrRemesh.Info->Current.Elements += 1;
      }
    }
    else
    {
      this->SelectElements();
    }

    if (mEchoLevel > 0)
      std::cout << "   Finished Element Selection " << std::endl;

    if (mEchoLevel > 0)
    {
      std::cout << "   SELECT MESH ELEMENTS (" << mrRemesh.Info->Current.Elements << ") ]; " << std::endl;

      if (mrRemesh.Options.Is(MesherData::CONSTRAINED))
      {
        int released_elements = mrRemesh.OutMesh.GetNumberOfElements() - mrRemesh.Info->Current.Elements;
        if (released_elements > 0)
          std::cout << "   RELEASED ELEMENTS (" << released_elements << ") IN CONSTRAINED MESH ]; " << std::endl;
      }
    }

    if( mEchoLevel > 0 )
      std::cout<<"  SELECT ELEMENTS time = "<<time_counter.ElapsedSeconds()<<std::endl;

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "SelectElementsMesherProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "SelectElementsMesherProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{

  ModelPart &mrModelPart;

  MesherData::MeshingParameters &mrRemesh;

  MesherUtilities mMesherUtilities;

  int mEchoLevel;

  struct NodalFlags
  {

    unsigned int Solid;
    unsigned int Fluid;
    unsigned int NoBoundaryFluid;
    unsigned int Rigid;
    unsigned int Boundary;
    unsigned int FreeSurface;
    unsigned int NoWallFreeSurface;
    unsigned int Contact;
    unsigned int Inlet;
    unsigned int Isolated;
    unsigned int Sliver;
    unsigned int NewEntity;
    unsigned int Visited;
    unsigned int Slave;
    unsigned int Interaction;
    unsigned int Inside;
    unsigned int Structure;
    unsigned int OutletWall;
    unsigned int FluidWall;
    unsigned int FreeSurfaceWall;

    double Radius;

    //constructor
    NodalFlags()
    {
      Solid = 0;
      Fluid = 0;
      NoBoundaryFluid = 0;
      Rigid = 0;
      Boundary = 0;
      FreeSurface = 0;
      NoWallFreeSurface = 0;
      Contact = 0;
      Inlet = 0;
      Isolated = 0;
      Sliver = 0;
      NewEntity = 0;
      Visited = 0;
      Radius = 0;
      Slave = 0;
      Interaction = 0;
      Inside = 0;
      Structure = 0;
      OutletWall = 0;
      FluidWall = 0;
      FreeSurfaceWall = 0;
    }

    //counter method
    void CountFlags(const NodeType &rNode)
    {
      if (rNode.Is(SOLID))
        ++Solid;
      if (rNode.Is(FLUID))
      {
        ++Fluid;
        if (rNode.IsNot(BOUNDARY))
          ++NoBoundaryFluid;
      }
      if (rNode.Is(RIGID))
        ++Rigid;
      if (rNode.Is(STRUCTURE))
      {
        ++Structure;
        if (rNode.Is(FLUID)){
          if(rNode.Is(FREE_SURFACE))
            ++FreeSurfaceWall;
          else
            ++FluidWall;
        }
        if (rNode.Is(OLD_ENTITY))
          ++OutletWall;
      }
      if (rNode.Is(BOUNDARY))
        ++Boundary;
      if (rNode.Is(CONTACT))
        ++Contact;
      if (rNode.Is(INLET))
        ++Inlet;
      if (rNode.Is(ISOLATED))
        ++Isolated;
      if (rNode.Is(FREE_SURFACE))
      {
        ++FreeSurface;
        if (rNode.AndNot({SOLID,RIGID}))
          ++NoWallFreeSurface;
      }
      if (rNode.Is(MARKER))
        ++Sliver;
      if (rNode.Is(NEW_ENTITY))
        ++NewEntity;
      if (rNode.Is(VISITED))
        ++Visited;
      if (rNode.Is(SLAVE))
        ++Slave;
      if (rNode.Is(INTERACTION))
        ++Interaction;
      if (rNode.Is(INSIDE))
        ++Inside;

      Radius += rNode.FastGetSolutionStepValue(NODAL_H);
    }

    bool PrintFlags()
    {
      if(Rigid >0 && FreeSurface>0)
      {
        std::cout<< "(Solid:"<<Solid<<" Fluid:"<<Fluid<<" Rigid:"<<Rigid<<" Structure:"<<Structure<<")"<<std::endl;
        std::cout<< "(Boundary:"<<Boundary<<" FreeSurface:"<<FreeSurface<<" NoWallFreeSurface:"<<NoWallFreeSurface<<")"<<std::endl;
        std::cout<< "(Contact:"<<Contact<<" Inlet:"<<Inlet<<" Isolated:"<<Isolated<<" Sliver:"<<Sliver<<")"<<std::endl;
        std::cout<< "(NewEntity:"<<NewEntity<<" Visited:"<<Visited<<" Slave:"<<Slave<<" Radius:"<<Radius<<")"<<std::endl;
        std::cout<< "(Interaction:"<<Interaction<<" Inside:"<<Inside<<")"<<std::endl;
        return true;
      }
      return false;
    }

  };

  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{

  virtual void SelectElements()
  {
    KRATOS_TRY

    const int &OutNumberOfElements = mrRemesh.OutMesh.GetNumberOfElements();

    if (OutNumberOfElements==0)
      KRATOS_ERROR << " 0 elements to select : NO ELEMENTS GENERATED " <<std::endl;

    //set flags for the local process execution marking slivers
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{SELECTED}}, {SELECTED.AsFalse()});

    unsigned int number_of_slivers = 0;
    unsigned int passed_boundary = 0;
    unsigned int passed_alpha_shape = 0;
    unsigned int passed_inner_outer = 0;

    unsigned int dimension = 0;
    unsigned int number_of_vertices = 0;

    this->GetElementDimension(dimension, number_of_vertices);

    const int *OutElementList = mrRemesh.OutMesh.GetElementList();

    ModelPart::NodesContainerType::iterator nodes_begin = mrModelPart.NodesBegin();

    int el = 0;
    int number = 0;

    ModelPartUtilities::CheckNodalH(mrModelPart);

    //#pragma omp parallel for reduction(+:number) //get same node from model part and flags set in vertices in parallel it is not threadsafe...
    for (el = 0; el < OutNumberOfElements; ++el)
    {
      GeometryType Vertices;
      NodalFlags VerticesFlags;

      bool accepted = this->GetVertices(el, nodes_begin, OutElementList, VerticesFlags, Vertices, number_of_vertices);

      if (!accepted)
        continue;

      // monitoring bools (for checking purposes)
      bool boundary_accepted = false;
      bool alpha_accepted = false;
      bool subdomain_accepted = false;
      bool center_accepted = false;
      bool shape_accepted = false;

      const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();
      const double &TimeStep = rCurrentProcessInfo[DELTA_TIME];

      double Size = VerticesFlags.Radius;

      if (mrRemesh.Options.Is(MesherData::REFINE)) //defined Size for Interior and Boundary
      {
        MesherData MeshData;
        RefineData& Refine = MeshData.GetRefineData(Vertices, mrRemesh, rCurrentProcessInfo);
        Size = Refine.Interior.DistanceMeshSize;
      }

      double Alpha = this->GetAlphaParameter(Vertices, VerticesFlags, Size, dimension, TimeStep);

      MesherUtilities MesherUtils;

      //2.- to control the alpha size
      if (accepted)
      {
        ++passed_boundary;
        boundary_accepted = true;
        if (mrRemesh.Options.Is(MesherData::CONTACT_SEARCH))
        {
          accepted = GeometryUtilities::ShrankAlphaShape(Alpha, Vertices, mrRemesh.OffsetFactor, dimension);
        }
        else
        {
          if (mrRemesh.Options.Is(MesherData::CONSTRAINED)) {
            accepted = true;
            if ( dimension == 2 && VerticesFlags.Boundary == Vertices.size() ) { //delete generated holes in constrained mesh
              if (GeometryUtilities::CheckSharedFace(Vertices) && !GeometryUtilities::CheckInnerCentre(Vertices)){
		accepted = false;
	      }
              else
                accepted = GeometryUtilities::AlphaShape(3.0, Vertices, dimension);

            } //must be reviewed to check constrained faces consistency
          }
          else {
            accepted = GeometryUtilities::AlphaShape(Alpha, Vertices, dimension);
          }
        }
      }

      //3.- to control all nodes from the same subdomain (problem, domain is not already set for new inserted particles on mesher)
      bool self_contact = false;
      if (accepted)
      {
        alpha_accepted = true;
        ++passed_alpha_shape;

        if (mrRemesh.Options.Is(MesherData::CONTACT_SEARCH))
          self_contact = MesherUtils.SameSubdomain(Vertices);
      }

      //4.- to control that the element is inside of the domain boundaries
      if (accepted)
      {
        subdomain_accepted = true;
        if (mrRemesh.Options.Is(MesherData::CONTACT_SEARCH))
        {
          //problems in 3D: be careful
          if (self_contact)
            accepted = GeometryUtilities::CheckOuterCentre(Vertices, mrRemesh.OffsetFactor, self_contact);
        }
        else
        {
          //accepted=GeometryUtilities::CheckInnerCentre(Vertices); //problems in 3D: when slivers are released, a boundary is created and the normals calculated, then elements that are inside suddently its center is calculated as outside... // some corrections are needded.
        }
      }

      //5.- to control that the element has a good shape
      if (accepted)
      {
        center_accepted = true;
        ++passed_inner_outer;
        accepted = this->CheckElementShape(Vertices, VerticesFlags, Size, dimension, TimeStep, number_of_slivers);
      }

      // if all checks have been passed, accept the element
      if (accepted)
      {
        shape_accepted = true;
        number += 1;
        mrRemesh.PreservedElements[el] = number;
      }
      else{
         std::cout<<" Element ["<<el<<"] with alpha "<<mrRemesh.AlphaParameter<<"("<<Alpha<<")"<<std::endl;
         for( unsigned int n=0; n<number_of_vertices; ++n)
         {
           std::cout<<" ("<<n+1<<"): Id["<<Vertices[n].Id()<<"] PreID["<<mrRemesh.NodalPreIds[Vertices[n].Id()]<<"] "<<Vertices[n].Coordinates()<<std::endl;
         }
         std::cout<<" (boundary:"<<boundary_accepted<<" alpha:"<<alpha_accepted<<" subdomain:"<<subdomain_accepted<<" center:"<<center_accepted<<" shape:"<<shape_accepted<<") "<< std::endl;

      }
    }

    mrRemesh.Info->Current.Elements = number;

    if (mEchoLevel > 0)
    {
      std::cout << "  [Preserved Elements " << mrRemesh.Info->Current.Elements << "] (" << mrModelPart.NumberOfElements() << ") :: (slivers detected: " << number_of_slivers << ") " << std::endl;
      std::cout << "  (passed_boundary: "<<passed_boundary<<", passed_alpha_shape: " << passed_alpha_shape << ", passed_inner_outer: " << passed_inner_outer << ") Total out: " <<OutNumberOfElements<< std::endl;
    }

    if (mrModelPart.IsNot(CONTACT))
      this->SelectNodesToErase();


    //set flags for the local process ex ecution marking slivers
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{SELECTED}}, {SELECTED.AsFalse()});

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool GetVertices(int &el, ModelPart::NodesContainerType::iterator nodes_begin, const int *OutElementList, NodalFlags &rVerticesFlags, GeometryType &rVertices, unsigned int &number_of_vertices)
  {
    KRATOS_TRY

    for (unsigned int pn = 0; pn < number_of_vertices; ++pn)
    {
      unsigned int id = el * number_of_vertices + pn;

      //std::cout<<" pn "<<pn<<" id "<<OutElementList[id]<<" pre_ids_size "<<mrRemesh.NodalPreIds.size()<<" preid "<<mrRemesh.NodalPreIds[OutElementList[id]]<<std::endl;

      if (OutElementList[id] <= 0)
        std::cout << " ERROR: something is wrong: nodal id < 0 " << el << std::endl;

      //check if the number of nodes are considered in the nodal pre ids
      if ((unsigned int)OutElementList[id] >= mrRemesh.NodalPreIds.size())
      {
        std::cout << " ERROR: something is wrong: node added by the mesher : " << (unsigned int)OutElementList[id] << std::endl;
        if (mrRemesh.Options.Is(MesherData::CONTACT_SEARCH))
          return false;
      }

      //check if the point is a vertex of an artificial external bounding box
      if (mrRemesh.NodalPreIds[OutElementList[id]] < 0)
      {
        if (mrRemesh.Options.IsNot(MesherData::CONTACT_SEARCH))
        {
          std::cout << " ERROR: something is wrong: nodal id < 0 " << std::endl;
        }
        return false;
      }

      //get node from model part and set it as vertex
      rVertices.push_back(*(nodes_begin + OutElementList[id] - 1).base());

      //vertices flags
      rVerticesFlags.CountFlags(rVertices.back());
    }

    // std::cout<<" selected vertices ["<<OutElementList[el*number_of_vertices];
    // for(unsigned int d=1; d<number_of_vertices; ++d)
    // {
    //   std::cout<<", "<<OutElementList[el*number_of_vertices+d];
    // }
    // std::cout<<"] "<<std::endl;

    // if (rVerticesFlags.PrintFlags())
    // {
    //   std::cout<<" model vertices ["<<rVertices[0].Id();
    //   for(unsigned int d=1; d<number_of_vertices; ++d)
    //   {
    //     std::cout<<", "<<rVertices[d].Id();
    //   }
    //   std::cout<<"] "<<std::endl;
    // }

    return true;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  double GetReferenceSize(GeometryType &rVertices, const NodalFlags &rVerticesFlags, MesherData::MeshingParameters &rRemesh, const ProcessInfo &rCurrentProcessInfo)
  {
      if (mrRemesh.Options.Is(MesherData::REFINE)) //defined Size for Interior and Boundary
      {
        MesherData MeshData;
        RefineData& Refine = MeshData.GetRefineData(rVertices, rRemesh, rCurrentProcessInfo);
        return Refine.Interior.DistanceMeshSize;
      }
      else
	return rVerticesFlags.Radius;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  virtual void SelectNodesToErase()
  {
    // Set disconnected nodes to erase
    unsigned int dimension = 0;
    unsigned int number_of_vertices = 0;
    unsigned int isolated_nodes = 0;

    this->GetElementDimension(dimension, number_of_vertices);

    int *OutElementList = mrRemesh.OutMesh.GetElementList();

    ModelPart::NodesContainerType &rNodes = mrModelPart.Nodes();

    const int &OutNumberOfElements = mrRemesh.OutMesh.GetNumberOfElements();

    //set flags for the local process execution
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{BLOCKED}}, {BLOCKED.AsFalse()});

    //check engaged nodes
    for (int el = 0; el < OutNumberOfElements; ++el)
    {
      if (mrRemesh.PreservedElements[el])
      {
        for (unsigned int pn = 0; pn < number_of_vertices; ++pn)
        {
          //set vertices
          rNodes[OutElementList[el * number_of_vertices + pn]].Set(BLOCKED);
        }
      }
    }

    int count_release = 0;
    for (ModelPart::NodesContainerType::iterator i_node = rNodes.begin(); i_node != rNodes.end(); ++i_node)
    {
      if (i_node->IsNot(BLOCKED))
      {
        if (i_node->And({RIGID,TO_ERASE}))
        {
          i_node->Set(TO_ERASE, false);
          std::cout << " WARNING TRYING TO DELETE A WALL NODE (solid): " << i_node->Id() << std::endl;
        }
        else if (i_node->IsNot(RIGID))
        {
          if (mrRemesh.ExecutionOptions.Is(MesherData::KEEP_ISOLATED_NODES))
          {
            if (i_node->IsNot(TO_ERASE))
            {
              i_node->Set(ISOLATED, true);
              ++isolated_nodes;
              std::cout << " ISOLATED SOLID TO ERASE in select_elements_mesher_process.hpp" << std::endl;
            }
          }
          else
          {
            std::cout<<" ISOLATED SOLID NODES NOT KEPT in select_elements_mesher_process.hpp"<<std::endl;
            i_node->Set(TO_ERASE, true);
            if (mEchoLevel > 0)
            {
              if (i_node->Is(BOUNDARY))
                std::cout << " NODE " << i_node->Id() << " IS BOUNDARY RELEASE " << std::endl;
              else
                std::cout << " NODE " << i_node->Id() << " IS DOMAIN RELEASE " << std::endl;
            }
            ++count_release;
          }
        }

      }
      else  // BLOCKED NODE
      {
        i_node->Set(TO_ERASE, false);
        i_node->Set(ISOLATED, false);
      }

      i_node->Set(BLOCKED, false);

      i_node->Set(VISITED, false);

      if (i_node->Is(SELECTED)) // Belongs to a SLIVER
        i_node->Set(MARKER, true);
      else
        i_node->Set(MARKER, false);

      i_node->Set(SELECTED, false);
    }

    if (mEchoLevel > 0)
    {
      std::cout << "   NUMBER OF RELEASED NODES " << count_release << std::endl;
      std::cout << "   NUMBER OF ISOLATED NODES " << isolated_nodes << std::endl;
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void CheckIds(const int *OutElementList, const int &OutNumberOfElements, const unsigned int number_of_vertices)
  {
    unsigned int max_out_id = 0;
    for (int el = 0; el < OutNumberOfElements; ++el)
    {
      for (unsigned int pn = 0; pn < number_of_vertices; ++pn)
      {
        unsigned int id = el * number_of_vertices + pn;
        if (int(max_out_id) < OutElementList[id])
          max_out_id = OutElementList[id];
      }
    }

    if (max_out_id >= mrRemesh.NodalPreIds.size())
      std::cout << " ERROR ID PRE IDS " << max_out_id << " > " << mrRemesh.NodalPreIds.size() << " (nodes size:" << mrModelPart.Nodes().size() << ")" << std::endl;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void GetElementDimension(unsigned int &dimension, unsigned int &number_of_vertices)
  {
    if (mrModelPart.NumberOfElements())
    {
      ModelPart::ElementsContainerType::iterator element_begin = mrModelPart.ElementsBegin();
      dimension = element_begin->GetGeometry().WorkingSpaceDimension();
      number_of_vertices = element_begin->GetGeometry().size();
    }
    else if (mrModelPart.NumberOfConditions())
    {
      ModelPart::ConditionsContainerType::iterator condition_begin = mrModelPart.ConditionsBegin();
      dimension = condition_begin->GetGeometry().WorkingSpaceDimension();
      if (dimension == 3) //number of nodes of a tetrahedron
        number_of_vertices = 4;
      else if (dimension == 2) //number of nodes of a triangle
        number_of_vertices = 3;
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  virtual double GetAlphaParameter(GeometryType &rVertices, const NodalFlags &rVerticesFlags, const double &rSize, const unsigned int &rDimension, const double &rTimeStep)
  {
    double Alpha = mrRemesh.AlphaParameter;

    if (rVerticesFlags.Boundary >= rVertices.size())
      Alpha *= 1.2;

    return Alpha;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  virtual bool CheckElementShape(GeometryType &rVertices, const NodalFlags &rVerticesFlags,  const double &rSize, const unsigned int &rDimension, const double &rTimeStep, unsigned int &number_of_slivers)
  {
    bool accepted = true;
    unsigned int NumberOfVertices = rVertices.size();

    int sliver = 0;
    if (rDimension == 3 && NumberOfVertices == 4)
    {
      Tetrahedra3D4<NodeType> Tetrahedron(rVertices);

      MesherUtilities MesherUtils;
      accepted = MesherUtils.CheckGeometryShape(Tetrahedron, sliver);

      if (sliver)
      {
        if (mrRemesh.Options.Is(MesherData::CONTACT_SEARCH))
          accepted = true;
        else
        {
          accepted = false;
          //do not release sliver elements in solid domains in constrained tessellation
          if (mrRemesh.Options.Is(MesherData::CONSTRAINED)){
            accepted = true;
            if (rVerticesFlags.Boundary == NumberOfVertices && GeometryUtilities::CheckSharedFace(rVertices))
              accepted = false;
          }
          else if (rVerticesFlags.Boundary < NumberOfVertices) //do not release inside slivers in solids
            accepted = true;

        }
        ++number_of_slivers;
      }
      else
      {
        if (mrRemesh.Options.Is(MesherData::CONTACT_SEARCH))
          accepted = false;
        else
          accepted = true;
      }
    }
    return accepted;
  }

  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Private Static Member Variables
  ///@{
  ///@}
  ///@name Private Static Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{
  ///@}

}; // Class Process

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                SelectElementsMesherProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const SelectElementsMesherProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_SELECT_ELEMENTS_MESHER_PROCESS_HPP_INCLUDED defined
