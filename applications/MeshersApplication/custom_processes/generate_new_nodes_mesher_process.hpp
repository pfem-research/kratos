//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

#if !defined(KRATOS_GENERATE_NEW_NODES_MESHER_PROCESS_HPP_INCLUDED)
#define KRATOS_GENERATE_NEW_NODES_MESHER_PROCESS_HPP_INCLUDED

// External includes

// System includes

// Project includes
#include "custom_processes/mesher_process.hpp"

namespace Kratos
{

///@name Kratos Classes
///@{

/// Refine Mesh Elements Process 2D and 3D
/** The process labels the nodes to be refined
    if the ThresholdVariable  is larger than a ReferenceThreshold
*/

class GenerateNewNodesMesherProcess
    : public MesherProcess
{
public:
  ///@name Type Definitions
  ///@{

  typedef Node<3> NodeType;
  typedef array_1d<double,3> VectorType;
  typedef Geometry<NodeType> GeometryType;

  typedef Bucket<3, Node<3>, std::vector<Node<3>::Pointer>, Node<3>::Pointer, std::vector<Node<3>::Pointer>::iterator, std::vector<double>::iterator> BucketType;
  typedef Tree<KDTreePartition<BucketType>> KdtreeType; //Kdtree

  /// Pointer definition of Process
  KRATOS_CLASS_POINTER_DEFINITION(GenerateNewNodesMesherProcess);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  GenerateNewNodesMesherProcess(ModelPart &rModelPart,
                                MesherData::MeshingParameters &rRemeshingParameters,
                                int EchoLevel)
      : mrModelPart(rModelPart),
        mrRemesh(rRemeshingParameters)
  {
    mEchoLevel = EchoLevel;
  }

  /// Destructor.
  virtual ~GenerateNewNodesMesherProcess() {}

  ///@}
  ///@name Operators
  ///@{

  /// This operator is provided to call the process as a function and simply calls the Execute method.
  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  /// Execute method is used to execute the Process algorithms.
  void Execute() override
  {
    KRATOS_TRY

    BuiltinTimer time_counter;

    mInfoData.Initialize(mEchoLevel);
    this->GenerateNodes(mrModelPart);
    mInfoData.Finalize(mEchoLevel);

    if (mEchoLevel > 0)
      std::cout << "  GENERATE NODES time = " << time_counter.ElapsedSeconds() << std::endl;

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "GenerateNewNodesMesherProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "GenerateNewNodesMesherProcess";
  }

  /// Print object's data.s
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Static Member Variables
  ///@{
  ModelPart &mrModelPart;

  MesherData::MeshingParameters &mrRemesh;

  int mEchoLevel;

  // struct for monitoring the process information
  struct ProcessInfoData
  {
    unsigned int initial_nodes;
    unsigned int final_nodes;

    void Initialize(int EchoLevel)
    {
      initial_nodes = 0;
      final_nodes = 0;
      if (EchoLevel > 0)
        std::cout << "   [ GENERATED NODES: " << std::endl;
    }

    void Finalize(int EchoLevel)
    {
      if (EchoLevel > 0)
      {
        std::cout << "   [ NODES      ( added : " << final_nodes-initial_nodes << " total: " << final_nodes << " ) ]" << std::endl;
        std::cout << "   GENERATE NEW NODES ]; " << std::endl;
      }
    }
  };

  ProcessInfoData mInfoData;

  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{

  //*******************************************************************************************
  //*******************************************************************************************

  void GenerateNodes(ModelPart &rModelPart)
  {
    if (rModelPart.Name() != mrRemesh.SubModelPartName)
      std::cout << " ModelPart Supplied do not corresponds to the Meshing Domain: (" << mrModelPart.Name() << " != " << mrRemesh.SubModelPartName << ")" << std::endl;

    //Find out where the new nodes belong to:
    mInfoData.initial_nodes = rModelPart.NumberOfNodes();

    //creating an auxiliary list for the new nodes
    std::vector<NodeType::Pointer> list_of_new_nodes;
    this->GenerateNewNodes(mrModelPart, list_of_new_nodes);

    //project variables to new nodes from mesh elements
    this->ProjectVariablesToNewNodes(rModelPart, list_of_new_nodes);

    mrRemesh.Info->Inserted.Nodes += list_of_new_nodes.size();

    this->SetNodesToModelPart(rModelPart, list_of_new_nodes);

    mInfoData.final_nodes = rModelPart.NumberOfNodes();
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void GenerateNewNodes(ModelPart &rModelPart, std::vector<NodeType::Pointer> &list_of_nodes)
  {
    KRATOS_TRY

    const unsigned int &dimension = rModelPart.GetProcessInfo()[SPACE_DIMENSION];

    //center
    double x = 0;
    double y = 0;
    double z = 0;

    unsigned int id = ModelPartUtilities::GetMaxNodeId(rModelPart) + 1;

    double *OutPointList = mrRemesh.OutMesh.GetPointList();

    int &InNumberOfPoints = mrRemesh.InMesh.GetNumberOfPoints();
    int &OutNumberOfPoints = mrRemesh.OutMesh.GetNumberOfPoints();

    if (OutNumberOfPoints > InNumberOfPoints)
    {
      for (int i = InNumberOfPoints; i < OutNumberOfPoints; ++i)
      {
        int base = i * dimension;

        x = OutPointList[base] / mrRemesh.Scale;
        y = OutPointList[base + 1] / mrRemesh.Scale;
        z = 0;
        if (dimension == 3)
          z = OutPointList[base + 2] / mrRemesh.Scale;

        list_of_nodes.push_back(ModelPartUtilities::CreateFreeNode(rModelPart,id,x,y,z));

        //set new id
        if (mrRemesh.InputInitializedFlag)
        {
          mrRemesh.NodalPreIds.push_back(id);
          list_of_nodes.back()->SetId(i + 1);
          if (id > mrRemesh.NodeMaxId)
            mrRemesh.NodeMaxId = id;
        }

        id++;
      }
    }

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  void ProjectVariablesToNewNodes(ModelPart &rModelPart, std::vector<NodeType::Pointer> &list_of_new_nodes)
  {

    KRATOS_TRY

    if (list_of_new_nodes.size() > 0)
    {
      //create search tree
      KdtreeType::Pointer SearchTree;
      SearchUtilities::CreateSearchTree(SearchTree, list_of_new_nodes, 20);

      //Find out where the new nodes belong to:
      Vector ShapeFunctions;
      const unsigned int &step_data_size = rModelPart.GetNodalSolutionStepDataSize();

      //std::vector<VariablesListDataValueContainer> VariablesListVector(list_of_new_nodes.size());
      //const VariablesList &VariablesList = rModelPart.GetNodalSolutionStepVariablesList();

      //find the center and "radius" of the element
      double radius = 0;
      NodeType center(0, 0.0, 0.0, 0.0);

      //all of the nodes in this list will be preserved
      unsigned int num_neighbours = list_of_new_nodes.size();
      std::vector<Node<3>::Pointer> neighbours(num_neighbours);
      std::vector<double> neighbour_distances(num_neighbours);

      unsigned int counter = 0;
      unsigned int n_points_in_radius;
      for (ModelPart::ElementsContainerType::const_iterator ie = rModelPart.ElementsBegin();
           ie != rModelPart.ElementsEnd(); ++ie)
      {

        const GeometryType &rGeometry =  ie->GetGeometry();

        GeometryUtilities::CalculateCenterAndRadius(rGeometry, center.Coordinates(), radius);

        n_points_in_radius = SearchTree->SearchInRadius(center, radius*1.01, neighbours.begin(), neighbour_distances.begin(), num_neighbours);

        //check if inside and eventually interpolate
        for (std::vector<Node<3>::Pointer>::iterator i_nnode = neighbours.begin(); i_nnode != neighbours.begin() + n_points_in_radius; ++i_nnode)
        {
          bool is_inside = GeometryUtilities::CalculatePosition(rGeometry, (*i_nnode)->Coordinates(), ShapeFunctions);

          if (is_inside == true)
          {
            double alpha = 1; //1 to interpolate, 0 to leave the original data
            //TransferUtilities::InterpolateStepData(**i_nnode, ie->GetGeometry(), ShapeFunctionsN, rVariablesList, alpha);
            TransferUtilities::InterpolateStepData(**i_nnode, rGeometry, ShapeFunctions, step_data_size, alpha);

            TransferUtilities::SetNodeVariables(rModelPart, rGeometry, **i_nnode);

            ++counter;
          }
        }
      }
      std::cout << " PROJECT to new nodes [New nodes: " << list_of_new_nodes.size() << " Projections:" << counter << "]" << std::endl;
    }

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void SetNodesToModelPart(ModelPart &rModelPart, std::vector<NodeType::Pointer> &list_of_nodes)
  {
    KRATOS_TRY

    //sort needed because in the search projection list of nodes order changed
    std::sort(list_of_nodes.begin(), list_of_nodes.end(), [](NodeType::Pointer const &i, NodeType::Pointer const &j) { return i->Id() < j->Id(); });

    if (list_of_nodes.size())
    {
      //std::cout<<" last node "<<rModelPart.Nodes().back().Id()<<std::endl;
      //add new conditions: ( SOLID body model part )
      for (std::vector<NodeType::Pointer>::iterator i_node = list_of_nodes.begin(); i_node != list_of_nodes.end(); ++i_node)
      {
        rModelPart.Nodes().push_back(*(i_node));
        //std::cout<<" node id "<<rModelPart.Nodes().back().Id()<<std::endl;
      }
    }

    KRATOS_CATCH("")
  }
  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{

  /// Assignment operator.
  GenerateNewNodesMesherProcess &operator=(GenerateNewNodesMesherProcess const &rOther);

  ///@}

}; // Class Process

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                GenerateNewNodesMesherProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const GenerateNewNodesMesherProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_GENERATE_NEW_NODES_MESHER_PROCESS_HPP_INCLUDED  defined
