//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

#if !defined(KRATOS_REFINE_CONDITIONS_MESHER_PROCESS_HPP_INCLUDED)
#define KRATOS_REFINE_CONDITIONS_MESHER_PROCESS_HPP_INCLUDED

// External includes

// System includes
#include<numeric>

// Project includes
#include "custom_utilities/mesher_utilities.hpp"

#include "custom_processes/mesher_process.hpp"
#include "custom_processes/print_model_part_mesh_process.hpp"
#include "custom_processes/smooth_conditions_mesher_process.hpp"

namespace Kratos
{

///@name Kratos Classes
///@{

/// Refine Mesh Boundary Process
/** The process labels the boundary conditions
    Dependencies: RemoveMeshNodesProcess.Execute()  is needed as a previous step

    Determines if new conditions must be inserted in boundary.
    If boundary must to be kept (CONSTRAINED),
    New conditions will be rebuild (splitting the old ones and inserting new nodes)
    Old conditions will be erased at the end.
*/

class RefineConditionsMesherProcess
    : public MesherProcess
{
public:
  ///@name Type Definitions
  ///@{
  typedef ModelPart::NodeType NodeType;
  typedef ModelPart::ElementType ElementType;
  typedef ModelPart::ConditionType ConditionType;
  typedef ModelPart::PropertiesType PropertiesType;
  typedef ConditionType::GeometryType GeometryType;

  typedef PointerVector<NodeType> PointsArrayType;
  typedef GlobalPointersVector<NodeType> NodeWeakPtrVectorType;
  typedef GlobalPointersVector<ElementType> ElementWeakPtrVectorType;
  typedef GlobalPointersVector<ConditionType> ConditionWeakPtrVectorType;

  typedef Kratos::weak_ptr<ModelPart> ModelPartWeakPtrType;
  typedef GlobalPointersVector<ModelPart> ModelPartWeakPtrVectorType;

  /// Pointer definition of Process
  KRATOS_CLASS_POINTER_DEFINITION(RefineConditionsMesherProcess);


  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  RefineConditionsMesherProcess(ModelPart &rModelPart,
                                MesherData::MeshingParameters &rRemeshingParameters,
                                int EchoLevel)
      : mrModelPart(rModelPart),
        mrRemesh(rRemeshingParameters)
  {
    mEchoLevel = EchoLevel;
  }

  /// Destructor.
  virtual ~RefineConditionsMesherProcess() {}

  ///@}
  ///@name Operators
  ///@{

  /// This operator is provided to call the process as a function and simply calls the Execute method.
  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  /// Execute method is used to execute the Process algorithms.
  void Execute() override
  {
    KRATOS_TRY

    mInfoData.Initialize(mEchoLevel);
    this->RefineConditions(mrModelPart);
    mInfoData.Finalize(mEchoLevel);

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "RefineConditionsMesherProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "RefineConditionsMesherProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{

  ModelPart &mrModelPart;

  MesherData::MeshingParameters &mrRemesh;

  MesherUtilities mMesherUtilities;

  int mEchoLevel;

  // struct for monitoring the process information
  struct ProcessInfoData
  {
    unsigned int initial_conditions;
    unsigned int initial_nodes;
    unsigned int final_conditions;
    unsigned int final_nodes;

    void Initialize(int EchoLevel)
    {
      initial_conditions = 0;
      initial_nodes = 0;
      final_conditions = 0;
      final_nodes = 0;

      if (EchoLevel > 0)
        std::cout << " [ REFINE CONDITIONS : " << std::endl;
    }

    void Finalize(int EchoLevel)
    {
      if (EchoLevel > 0)
      {
        std::cout << "   [ CONDITIONS ( inserted : " << int(final_conditions-initial_conditions) << " total: " << final_conditions << " ) ]" << std::endl;
        std::cout << "   [ NODES      ( inserted : " << final_nodes-initial_nodes << " total: " << final_nodes << " ) ]" << std::endl;

        std::cout << "   REFINE CONDITIONS ]; " << std::endl;
      }

    }
  };

  ProcessInfoData mInfoData;

  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{

  void RefineConditions(ModelPart &rModelPart)
  {
    KRATOS_TRY

    const unsigned int &conditions_size = rModelPart.NumberOfConditions();
    mInfoData.initial_conditions = conditions_size;
    mInfoData.initial_nodes = rModelPart.NumberOfNodes();

    //if the insert switches are activated
    //we check if the boundaries got too coarse
    if (mrRemesh.IsBoundary(RefineData::REFINE))
    {

      SmoothConditionsMesherProcess SmoothProcess(rModelPart,mrRemesh,mEchoLevel);
      //SmoothProcess.Execute();

      //print surface mesh
      // if (rModelPart.GetProcessInfo()[SPACE_DIMENSION] == 3)
      // {
      //   PrintModelPartMeshProcess print_output(rModelPart,"in_conditions",1);
      //   print_output.Execute();
      // }

      // Select boundary to refine : conditions (TO_REFINE)
      this->SelectBoundaryToRefine(rModelPart);

      std::vector<NodeType::Pointer> list_of_nodes;
      list_of_nodes.reserve(conditions_size);
      std::vector<ConditionType::Pointer> list_of_conditions;
      list_of_conditions.reserve(conditions_size);

      // Generate Nodes : new nodes(NEW_ENTITY)
      this->GenerateNewNodes(rModelPart, list_of_nodes, list_of_conditions); //points (NEW_ENTITY)

      // Generate Conditions : new conditions(NEW_ENTITY)  / old conditions (TO_ERASE)
      this->GenerateNewConditions(rModelPart, list_of_nodes, list_of_conditions);

      // if (rModelPart.GetProcessInfo()[SPACE_DIMENSION] == 3 && list_of_conditions.size() > 0)
      //   this->SwapConditionEdges(rModelPart, list_of_conditions);
      mrRemesh.Info->Inserted.Nodes += list_of_nodes.size();

      // Entities to model part
      ModelPartUtilities::AddNodes(rModelPart, list_of_nodes);
      ModelPartUtilities::AddConditions(rModelPart, list_of_conditions);

      mInfoData.final_conditions = rModelPart.NumberOfConditions();
      mInfoData.final_nodes = rModelPart.NumberOfNodes();

      // Check nodal normals (possible meshing problems if they are not defined correctly)
      ModelPartUtilities::CheckNodalNormal(rModelPart);

      //print surface mesh
      if (rModelPart.GetProcessInfo()[SPACE_DIMENSION] == 3)
      {
        PrintModelPartMeshProcess print_output(rModelPart,"refine_conditions",1);
        print_output.Execute();
      }
      //new conditions are added to model part and later added to condition model parts via (NEW_ENTITY) and (MODEL_PART_NAME)
    }

    KRATOS_CATCH(" ")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void GenerateNewNodes(ModelPart &rModelPart, std::vector<NodeType::Pointer> &list_of_nodes, std::vector<ConditionType::Pointer> &list_of_conditions)
  {
    KRATOS_TRY

    const ProcessInfo &rCurrentProcessInfo = rModelPart.GetProcessInfo();
    MesherData MeshData;
    //assign data to dofs
    const unsigned int &step_data_size = rModelPart.GetNodalSolutionStepDataSize();

    //std::vector<double> list_of_distances;

    unsigned int id = ModelPartUtilities::GetMaxNodeId(rModelPart) + 1;

    for (ModelPart::ConditionsContainerType::iterator i_cond = rModelPart.ConditionsBegin(); i_cond != rModelPart.ConditionsEnd(); ++i_cond)
    {
      if (i_cond->Is(TO_REFINE))
      {
        //center
        NodeType center(id, 0.0, 0.0, 0.0);

        Geometry<Node<3>> &rGeometry = i_cond->GetGeometry();
        GeometryUtilities::CalculateCircumCenter(rGeometry, center.Coordinates());

        //get boundary edge to add fixity and nodal step data
        GeometryType Boundary = GeometryUtilities::GetBoundaryGeometry(center, rGeometry);

        if (Boundary.size() < rGeometry.size())
          i_cond->Set(TO_SPLIT,true);

        //double distance = 0;

        //position curvature correction
        this->SetNodalPosition(Boundary, rCurrentProcessInfo, center.X(), center.Y(), center.Z());

        RefineData& Refine = MeshData.GetRefineData(*i_cond, mrRemesh, rCurrentProcessInfo);

        if(CheckClosestAddedNode(list_of_nodes, center, Refine.Interior.DistanceMeshSize))
        {
          //distance = GeometryUtilities::CalculateMaxEdgeSize(Boundary);

          NodeType::Pointer pNode = ModelPartUtilities::CreateFreeNode(rModelPart, id, center.X(), center.Y(), center.Z());

          //set variables
          GeometryUtilities::AddNodeFixedDofs(*pNode, Boundary);
          TransferUtilities::AddNodeStepData(*pNode, Boundary, step_data_size);
          TransferUtilities::SetBoundaryNodeVariables(rModelPart, rGeometry, Boundary, *pNode);

          pNode->Set(NEW_ENTITY);

          //list_of_distances.push_back(distance);
          list_of_nodes.push_back(pNode);
          list_of_conditions.push_back(*(i_cond.base()));

          id++;
        }
      }
    }

    // //Sort conditions and inserted nodes from large edge to short edge
    // std::vector<size_t> indices(list_of_nodes.size());
    // std::iota(indices.begin(), indices.end(), 0);
    // std::sort(indices.begin(), indices.end(),[&](size_t A, size_t B) -> bool {return list_of_distances[A] > list_of_distances[B];});

    // // SearchUtilities::ReorderWithIndices(list_of_nodes,indices);
    // // SearchUtilities::ReorderWithIndices(list_of_conditions,indices);
    // SearchUtilities::ReorderWithIndices(list_of_nodes, list_of_conditions,indices);

    // unsigned int cond_id = ModelPartUtilities::GetMaxConditionId(rModelPart) + 1;
    // for (auto &i_cond : list_of_conditions)
    //   i_cond->SetId(cond_id++);

    // unsigned int node_id = ModelPartUtilities::GetMaxNodeId(rModelPart) + 1;
    // for (auto &i_node : list_of_nodes)
    //   i_node->SetId(node_id++);


    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckClosestAddedNode(const std::vector<NodeType::Pointer>& list_of_nodes, const NodeType& center, const double& distance)
  {
    KRATOS_TRY

    for (auto &i_node : list_of_nodes)
    {
      double d = norm_2(i_node->Coordinates()-center);
      if (d <= 0.25*distance)
        return false;
    }

    return true;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void SelectBoundaryToRefine(ModelPart &rModelPart)
  {
    KRATOS_TRY

    const ProcessInfo &rCurrentProcessInfo = rModelPart.GetProcessInfo();
    MesherData MeshData;

    block_for_each(rModelPart.Conditions(),[&](Condition &i_cond){
    //for (auto &i_cond : rModelPart.Conditions()){
      i_cond.Set(TO_REFINE, false);
      i_cond.Set(TO_SPLIT, false);

       bool refine_candidate = true;
       if (mrRemesh.Options.Is(MesherData::CONSTRAINED))
          if (i_cond.IsNot(BOUNDARY)) //Only CompositeCondition refined in constrained mesh
             refine_candidate = false;
       if ( i_cond.Is(TO_ERASE) ) {
          std::cout << " DANGER !!! , we have a condition to remove, why? " << std::endl;
          std::cout << " is this " << i_cond.Id() << std::endl;
          i_cond.Set(TO_ERASE, false);
          refine_candidate = false;
       }

      if (refine_candidate)
      {
        if (i_cond.IsNot(TO_ERASE))
        {
          RefineData& Refine = MeshData.GetRefineData(i_cond, mrRemesh, rCurrentProcessInfo);
          if (this->RefineBoundaryCondition(i_cond, Refine.Boundary, rCurrentProcessInfo))
            i_cond.Set(TO_REFINE, true);
          else if (this->RefineContactCondition(i_cond, Refine.Boundary, rCurrentProcessInfo))
            i_cond.Set(TO_REFINE, true);
        }
        else
          std::cout<<" Condition ["<<i_cond.Id()<<"] to refined LEGACY "<<std::endl;
      }
    });

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool RefineBoundaryCondition(ConditionType &rCondition, RefineData::RefineParameters &rRefine, const ProcessInfo &rCurrentProcessInfo)
  {
    KRATOS_TRY

    double factor = 1.2;
    if (rRefine.Options.Is(RefineData::REFINE_ON_THRESHOLD) || rRefine.Options.Is(RefineData::REFINE_ON_DISTANCE))
      if (GeometryUtilities::CheckCurvature(rCondition.GetGeometry(), 0.74, 0.99)) //curved but not conditions on TIP
          factor = 0.8;

    return this->RefineBoundaryCondition(rCondition, rRefine, rCurrentProcessInfo, factor);

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool RefineContactCondition(ConditionType &rCondition, RefineData::RefineParameters &rRefine, const ProcessInfo &rCurrentProcessInfo)
  {
    KRATOS_TRY

    if (rRefine.Options.Is(RefineData::REFINE_ON_DISTANCE) || (rRefine.Options.Is(RefineData::REFINE_ON_THRESHOLD)))
    {
      GeometryType rGeometry = rCondition.GetGeometry();

      bool contact_semi_active = false;
      std::vector<bool> semi_active_nodes;
      bool contact_active = mMesherUtilities.CheckContactActive(rGeometry, contact_semi_active, semi_active_nodes);

      bool inactive_contact = true; // all nodes with contact flag
      for (auto &i_node: rGeometry)
        if(i_node.IsNot(CONTACT))
          inactive_contact = false;

      double factor = 1.2;
      if (contact_active || contact_semi_active)
      {
        std::vector<array_1d<double, 3>> contact_normals;
        bool curved_contact = GeometryUtilities::CheckContactCurvature(rGeometry, contact_normals);
        if (contact_active)
        {
          //FACTOR VALUE INSERT plane contact
          factor = 1.0;
          //FACTOR VALUE INSERT curved contact
          if (curved_contact)
            factor = 0.75;
        }
        else{
          //FACTOR VALUE INSERT plane contact transition
          factor = 1.2;
          //FACTOR VALUE INSERT curved contact transition
          if (curved_contact)
            factor = 1.0;
        }
      }

      if (contact_active || contact_semi_active || inactive_contact)
        return this->RefineBoundaryCondition(rCondition, rRefine, rCurrentProcessInfo, factor);
    }

    return false;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool RefineBoundaryCondition(ConditionType &rCondition, RefineData::RefineParameters &rRefine, const ProcessInfo &rCurrentProcessInfo, double factor)
  {
    KRATOS_TRY

    if (rRefine.Options.Is(RefineData::REFINE_ON_THRESHOLD))
      if (this->RefineOnThreshold(rCondition, rRefine, rCurrentProcessInfo, factor))
        return true;

    if (rRefine.Options.Is(RefineData::REFINE_ON_DISTANCE))
      if (this->RefineOnDistance(rCondition, rRefine, factor))
        return true;

    return false;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool RefineOnThreshold(ConditionType &rCondition, RefineData::RefineParameters &rRefine, const ProcessInfo &rCurrentProcessInfo, double factor)
  {
    KRATOS_TRY

    if (rCondition.GetValue(PRIMARY_ELEMENTS).size() > 0)
    {
      Element::ElementType &rMasterElement = rCondition.GetValue(PRIMARY_ELEMENTS).back();

      for(auto& variable : rRefine.ThresholdVariables)
      {
        if (MesherUtilities::CheckThreshold(rMasterElement, *variable.first, variable.second, rCurrentProcessInfo, rRefine.SpecificThreshold))
          return this->RefineOnDistance(rCondition, rRefine, factor);
      }
    }

    return false;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool RefineOnDistance(ConditionType &rCondition, RefineData::RefineParameters &rRefine, double factor)
  {
    KRATOS_TRY

    //DISTANCE VALUE INSERT
    double critical_size = factor * rRefine.DistanceMeshSize;

    //calculate condition length
    double face_size = GeometryUtilities::CalculateMaxEdgeSize(rCondition.GetGeometry());

    if (face_size > critical_size)
      return true;

    return false;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void SetNodalPosition(const GeometryType &rGeometry, const ProcessInfo &rCurrentProcessInfo, double &xc, double &yc, double &zc)
  {
    KRATOS_TRY

    bool contact_semi_active = false;
    std::vector<bool> semi_active_nodes;

    bool contact_active = mMesherUtilities.CheckContactActive(rGeometry, contact_semi_active, semi_active_nodes);

    //std::cout << " Contact active: " << contact_active << " semi-active: " << contact_semi_active << std::endl;

    double correction_factor = 0.9;  //rigid contact 0.95 //deformable contact 0.85

    if (contact_semi_active || contact_active)
    { // if contact is semi_ative or active
      array_1d<double, 3> position_correction;
      noalias(position_correction) = ZeroVector(3);

      if (rGeometry.size() == 2 && rGeometry.WorkingSpaceDimension() == 2)
      { //line
        //circle interpolation
        //GeometryUtilities::CircleInterpolation(pCondition, position_correction);
        //hermite interpolation
        GeometryUtilities::HermiteInterpolation(rGeometry, position_correction);
        std::cout << " Hermite 2D Line Interpolation " << position_correction << std::endl;
      }

      // if (rGeometry.size() == 2 && rGeometry.WorkingSpaceDimension() == 3)
      // { //line
      //   GeometryUtilities::HermiteInterpolation(rGeometry, position_correction);
      //   std::cout << " Hermite 3D Line Interpolation " << position_correction << std::endl;
      // }

      if (rGeometry.size() == 3 && rGeometry.WorkingSpaceDimension() == 3)
      { //triangle
        //hermite interpolation
        GeometryUtilities::HermiteTriangleInterpolation(rGeometry, position_correction);
        std::cout << " Hermite 3D Trinagle Interpolation " << std::endl;
        correction_factor = 0.5;//0.2 //0.5
      }

      bool add_correction = true;
      if ( std::fabs(xc) < 1.0E-9 && rCurrentProcessInfo[IS_AXISYMMETRIC] )
         add_correction = false; // do not move nodes at r = 0 in axisymmetric conditions (it is a trick)

      // if (rGeometry.size() != rGeometry.WorkingSpaceDimension())
      // { //contact domain element
      //   //correct only if not self contact
      //   if (mMesherUtilities.SameSubdomain(rGeometry)) //return true if self contact (same domain)
      //     add_correction = false;
      // }

      position_correction *= correction_factor;
      if (add_correction)
      {
        xc += position_correction[0];
        yc += position_correction[1];
        zc += position_correction[2];
      }
    }

    KRATOS_CATCH("")
  }


  //*******************************************************************************************
  //*******************************************************************************************

  void GenerateNewConditions(ModelPart &rModelPart, std::vector<NodeType::Pointer> &list_of_nodes, std::vector<ConditionType::Pointer> &list_of_conditions)
  {
    KRATOS_TRY

    std::vector<ConditionType::Pointer> list_of_new_conditions;
    std::vector<NodeType::Pointer> list_of_new_nodes;

    unsigned int id = ModelPartUtilities::GetMaxConditionId(rModelPart) + 1;

    ConditionType::Pointer pCondition;

    int size = 0;

    unsigned int counter = 0;
    unsigned int splitted = 0;
    unsigned int to_split = 0;
    for (std::vector<ConditionType::Pointer>::iterator i_cond = list_of_conditions.begin(); i_cond != list_of_conditions.end(); ++i_cond)
    {
      if((*i_cond)->IsNot(TO_ERASE))
      {
        //Reset TO_REFINE
        (*i_cond)->Set(TO_REFINE, false);

        Geometry<Node<3>> &rGeometry = (*i_cond)->GetGeometry();
        size = rGeometry.size();

        PointsArrayType Nodes(size);

        if (size == 2)
        {
          //new condition 1
          Nodes(0) = rGeometry(0);
          Nodes(1) = list_of_nodes[counter];

          pCondition = (*i_cond)->Clone(id++, Nodes);

          //set flags
          pCondition->Set(NEW_ENTITY);

          TransferUtilities::AddConditionData(**i_cond, *pCondition);

          list_of_new_conditions.push_back(pCondition);

          //new condition 2
          Nodes(0) = list_of_nodes[counter];
          Nodes(1) = rGeometry(1);

          pCondition = (*i_cond)->Clone(id++, Nodes);

          //set flags
          pCondition->Set(NEW_ENTITY);

          //set variables
          TransferUtilities::AddConditionData(**i_cond, *pCondition);

          list_of_new_conditions.push_back(pCondition);
          list_of_new_nodes.push_back(list_of_nodes[counter]);

          // once the condition is refined set to erase
          SearchUtilities::SetConditionToErase(**i_cond);
        }

        if (size == 3)
        {
          if ((*i_cond)->Is(TO_SPLIT))
          {
            ++to_split;
            //search edge to split
            NodeType::Pointer pCenter = list_of_nodes[counter];
            NodeType::Pointer pEdge1;
            GeometryType Boundary = GeometryUtilities::GetBoundaryEdge(*pCenter, rGeometry, pEdge1);

            if (Boundary.size()==3)
               KRATOS_ERROR << "Edge not found" << std::endl;

            //search neighbour edge condition
            NodeType::Pointer pEdge2;
            Condition::Pointer pEdgeCondition;

            if (!SearchUtilities::FindEdgeSharedCondition((*i_cond)->Id(),list_of_conditions,Boundary,pEdgeCondition,pEdge2))
              if(!SearchUtilities::FindEdgeSharedCondition((*i_cond)->Id(),rModelPart.Conditions(),Boundary,pEdgeCondition,pEdge2))
                KRATOS_ERROR << "Edge Shared Condition not found" << std::endl;

            bool split = false;
            //split larger boundaries
            if (pEdgeCondition->IsNot(TO_ERASE)){
              split = true;
              if (pEdgeCondition->Is(TO_REFINE)){
                double edge = norm_2(Boundary[1].Coordinates()-Boundary[0].Coordinates());
                double side_1 = norm_2(Boundary[0].Coordinates()-pEdge2->Coordinates());
                double side_2 = norm_2(Boundary[1].Coordinates()-pEdge2->Coordinates());
                if (side_1 > edge || side_2 > edge){
                  // std::cout<<" NO SPLIT yet ["<<pEdgeCondition->Id()<<"] edge "<<edge<<" side1 "<<side_1<<" side2 "<<side_2<<std::endl;
                  split = false;
                }
                //   else{
                //     // std::cout<<" SPLIT A ["<<pEdgeCondition->Id()<<"] edge "<<edge<<" side1 "<<side_1<<" side2 "<<side_2<<std::endl;
                //     // std::cout<<" center ["<<pCenter->Id()<<"] "<<pCenter->Coordinates()<<std::endl;
                //   }
                // }
                // else{
                //   // double edge = norm_2(Boundary[1].Coordinates()-Boundary[0].Coordinates());
                //   // double side_1 = norm_2(Boundary[0].Coordinates()-pEdge2->Coordinates());
                //   // double side_2 = norm_2(Boundary[1].Coordinates()-pEdge2->Coordinates());
                //   // std::cout<<" SPLIT B ["<<pEdgeCondition->Id()<<"] edge "<<edge<<" side1 "<<side_1<<" side2 "<<side_2<<std::endl;
                //   // std::cout<<" center ["<<pCenter->Id()<<"] "<<pCenter->Coordinates()<<std::endl;
              }

            }
            else{
              split = false;
              std::cout<<" found edge condition is to erase "<<std::endl;
            }

            //rebuild boundary
            if (split){
              //std::cout<<" split refine : generate 4 remove 2 (1 node added: "<<pCenter->Id()<<")"<<std::endl;
              ++splitted;
              (*i_cond)->Set(TO_SPLIT,false);

              //new condition 1
              Nodes(0) = pEdge1;
              Nodes(1) = Boundary(0);
              Nodes(2) = pCenter;

              pCondition = (*i_cond)->Clone(id++, Nodes);

              //set flags
              pCondition->Set(NEW_ENTITY);

              TransferUtilities::AddConditionData(**i_cond, *pCondition);

              if(pCondition->GetGeometry().Area()<1e-15)
                std::cout<<" Area 1 "<<pCondition->GetGeometry().Area()<<"["<<Nodes[0].Id()<<","<<Nodes[1].Id()<<","<<Nodes[2].Id()<<"]"<<std::endl;
              list_of_new_conditions.push_back(pCondition);

              //new condition 2
              Nodes(0) = Boundary(1);
              Nodes(1) = pEdge1;
              Nodes(2) = pCenter;

              pCondition = (*i_cond)->Clone(id++, Nodes);

              //set flags
              pCondition->Set(NEW_ENTITY);

              TransferUtilities::AddConditionData(**i_cond, *pCondition);

              if(pCondition->GetGeometry().Area()<1e-15)
                std::cout<<" Area 2 "<<pCondition->GetGeometry().Area()<<"["<<Nodes[0].Id()<<","<<Nodes[1].Id()<<","<<Nodes[2].Id()<<"]"<<std::endl;
              list_of_new_conditions.push_back(pCondition);

              //new condition 3
              Nodes(0) = Boundary(0);
              Nodes(1) = pEdge2;
              Nodes(2) = pCenter;

              //now clone and set data using pEdgeCondition to have the correct condition NORMAL variable
              (pEdgeCondition)->Set(TO_REFINE, false);

              pCondition = (pEdgeCondition)->Clone(id++, Nodes);

              //set flags
              pCondition->Set(NEW_ENTITY);

              TransferUtilities::AddConditionData(*pEdgeCondition, *pCondition);

              if(pCondition->GetGeometry().Area()<1e-15)
                std::cout<<" Area 3 "<<pCondition->GetGeometry().Area()<<"["<<Nodes[0].Id()<<","<<Nodes[1].Id()<<","<<Nodes[2].Id()<<"]"<<std::endl;
              list_of_new_conditions.push_back(pCondition);

              //new condition 4
              Nodes(0) = pEdge2;
              Nodes(1) = Boundary(1);
              Nodes(2) = pCenter;

              pCondition = (pEdgeCondition)->Clone(id++, Nodes);

              //set flags
              pCondition->Set(NEW_ENTITY);

              TransferUtilities::AddConditionData(*pEdgeCondition, *pCondition);

              if(pCondition->GetGeometry().Area()<1e-15)
                std::cout<<" Area 4 "<<pCondition->GetGeometry().Area()<<"["<<Nodes[0].Id()<<","<<Nodes[1].Id()<<","<<Nodes[2].Id()<<"]"<<std::endl;
              list_of_new_conditions.push_back(pCondition);

              //set neighbour edge condition TO_ERASE
              SearchUtilities::SetConditionToErase(*pEdgeCondition);

              // once the condition is refined set to erase
              SearchUtilities::SetConditionToErase(**i_cond);

              list_of_new_nodes.push_back(list_of_nodes[counter]);
            }
            else{
              std::cout<<" no split edge found "<<std::endl;
            }
          }
          else{
            std::cout<<" insert refine : generate 3 remove 1 (1 node added : "<<list_of_nodes[counter]->Id()<<")"<<std::endl;
            //new condition 1
            Nodes(0) = rGeometry(0);
            Nodes(1) = rGeometry(1);
            Nodes(2) = list_of_nodes[counter];

            pCondition = (*i_cond)->Clone(id++, Nodes);

            //set flags
            pCondition->Set(NEW_ENTITY);

            TransferUtilities::AddConditionData(**i_cond, *pCondition);

            if(pCondition->GetGeometry().Area()<1e-15)
              std::cout<<" Area 1 "<<pCondition->GetGeometry().Area()<<"["<<Nodes[0].Id()<<","<<Nodes[1].Id()<<","<<Nodes[2].Id()<<"]"<<std::endl;
            list_of_new_conditions.push_back(pCondition);

            //new condition 2
            Nodes(0) = rGeometry(1);
            Nodes(1) = rGeometry(2);
            Nodes(2) = list_of_nodes[counter];

            pCondition = (*i_cond)->Clone(id++, Nodes);

            //set flags
            pCondition->Set(NEW_ENTITY);

            //set variables
            TransferUtilities::AddConditionData(**i_cond, *pCondition);

            if(pCondition->GetGeometry().Area()<1e-15)
              std::cout<<" Area 2 "<<pCondition->GetGeometry().Area()<<"["<<Nodes[0].Id()<<","<<Nodes[1].Id()<<","<<Nodes[2].Id()<<"]"<<std::endl;
            list_of_new_conditions.push_back(pCondition);

            //new condition 3
            Nodes(0) = rGeometry(2);
            Nodes(1) = rGeometry(0);
            Nodes(2) = list_of_nodes[counter];

            pCondition = (*i_cond)->Clone(id++, Nodes);

            //set flags
            pCondition->Set(NEW_ENTITY);

            //set variables
            TransferUtilities::AddConditionData(**i_cond, *pCondition);

            if(pCondition->GetGeometry().Area()<1e-15)
              std::cout<<" Area 3 "<<pCondition->GetGeometry().Area()<<"["<<Nodes[0].Id()<<","<<Nodes[1].Id()<<","<<Nodes[2].Id()<<"]"<<std::endl;
            list_of_new_conditions.push_back(pCondition);

            // once the condition is refined set to erase
            SearchUtilities::SetConditionToErase(**i_cond);

            list_of_new_nodes.push_back(list_of_nodes[counter]);
          }
        }
      }
      else{
        std::cout<<" condition ["<<(*i_cond)->Id()<<"] not to erase"<<std::endl;
      }
      counter++;
    }

    //std::cout<<" SPLIT conditions "<<splitted<<" of "<<to_split<<" (pre: "<<list_of_conditions.size()<<", "<<list_of_nodes.size()<<")"<<" (post: "<<list_of_new_conditions.size()<<", "<<list_of_new_nodes.size()<<")"<<std::endl;
    //update the list of old conditions and nodes  with the list of new conditions and nodes
    list_of_conditions.swap(list_of_new_conditions);
    unsigned int cond_id = ModelPartUtilities::GetMaxConditionId(rModelPart) + 1;
    for (auto &i_cond : list_of_conditions)
      i_cond->SetId(cond_id++);

    list_of_nodes.swap(list_of_new_nodes);
    unsigned int node_id = ModelPartUtilities::GetMaxNodeId(rModelPart) + 1;
    for (auto &i_node : list_of_nodes){
      i_node->SetId(node_id++);
    }

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void SwapConditionEdges(ModelPart &rModelPart, std::vector<ConditionType::Pointer> &list_of_conditions)
  {
    KRATOS_TRY

    std::cout << " Conditions to SWAP (" << list_of_conditions.size() <<")"<< std::endl;

    std::vector<ConditionType::Pointer> list_of_new_conditions;
    std::vector<NodeType::Pointer> list_of_bounding_nodes;

    unsigned int swapped_conditions = 0;

    // Clean node neighbour conditions and create list bounding nodes
    for (auto &i_cond : list_of_conditions)
    {
      Condition::GeometryType &rGeometry = i_cond->GetGeometry();
      for (unsigned int i = 0; i < rGeometry.size(); ++i)
      {
        ConditionWeakPtrVectorType &nConditions = rGeometry[i].GetValue(NEIGHBOUR_CONDITIONS);
        nConditions.clear();
        if (rGeometry[i].IsNot(NEW_ENTITY))
          SearchUtilities::AddUniqueSharedPointer<Node<3>>(list_of_bounding_nodes, rGeometry(i));
      }
    }

    //std::cout<<" Bounding nodes "<<list_of_bounding_nodes.size()<<std::endl;
    SearchUtilities::AddNodeNeighbourConditions(list_of_conditions);
    // Set node neighbour conditions

    // for (auto i_cond(list_of_conditions.begin()); i_cond != list_of_conditions.end(); ++i_cond)
    // {
    //   for (auto &i_node : (*i_cond)->GetGeometry())
    //     i_node.GetValue(NEIGHBOUR_CONDITIONS).push_back(*i_cond);
    // }

    // Add node neighbour previous conditions
    for (auto i_cond(rModelPart.ConditionsBegin()); i_cond != rModelPart.ConditionsEnd(); ++i_cond)
    {
      if (i_cond->Is(TO_REFINE)){
        i_cond->Set(TO_REFINE,false);
        for (auto &i_node : i_cond->GetGeometry())
          i_node.GetValue(NEIGHBOUR_CONDITIONS).push_back(*i_cond.base());
      }
    }

    // Add neighbour nodes (optional)
    // for(auto& i_cond : list_of_conditions)
    // {
    //   for(auto& i_node : i_cond->GetGeometry())
    //   {
    //     ConditionWeakPtrVectorType& nConditions = i_node.GetValue(NEIGHBOUR_CONDITIONS);
    //     for(auto& i_ncond : nConditions)
    //     {
    //       Condition::GeometryType& rGeometry = i_ncond.GetGeometry();
    //       for(unsigned int i = 0; i < rGeometry.size(); ++i)
    //       {
    //         if( rGeometry[i].Id() != i_node.Id() )
    //         {
    //           NodeWeakPtrVectorType& nNodes = i_node.GetValue(NEIGHBOUR_NODES);
    //           SearchUtilities::AddUniqueWeakPointer<Node<3> >(nNodes, rGeometry(i));
    //         }
    //       }
    //     }
    //   }
    // }

    unsigned int Id = ModelPartUtilities::GetMaxConditionId(rModelPart) + list_of_conditions.size() + 1;

    //std::cout<<" Id "<<list_of_conditions.back()->Id()<<" id "<<Id<<std::endl;

    ConditionType::Pointer pCondition;

    std::vector<unsigned int> permuta = {0, 1, 2, 0, 1};
    std::vector<unsigned int> edges(2);

    double critical_edge_projection = 0.7071; //cosinus of the angle between edges

    // Build Edges
    for (auto &i_node : list_of_bounding_nodes)
    {

      ConditionWeakPtrVectorType &nConditions = i_node->GetValue(NEIGHBOUR_CONDITIONS);

      //std::cout<<" Bounding Node "<<i_node->Id()<<" nConditions "<<nConditions.size()<<std::endl;

      for (auto &i_ncond : nConditions)
      {
        if (i_ncond.IsNot(TO_ERASE) && i_ncond.Is(NEW_ENTITY))
        {
          Condition::GeometryType &iGeometry = i_ncond.GetGeometry();
          // Check angle
          unsigned int id = 0;
          for (unsigned int i = 0; i < iGeometry.size(); ++i)
          {
            if (i_node->Id() == iGeometry[i].Id())
            {
              id = i;
              break;
            }
            //std::cout<<" id "<<id<<std::endl;
          }

          double edge_projection;
          GeometryUtilities::CalculateEdgesProjection(iGeometry[id], iGeometry[permuta[id + 1]], iGeometry[permuta[id + 2]], edge_projection);

          //std::cout<<" edge "<<edge_projection<<std::endl;

          if (edge_projection > critical_edge_projection)
          {

            double edge_0 = GeometryUtilities::CalculateSideLength(iGeometry[id], iGeometry[permuta[id + 1]]);
            double edge_1 = GeometryUtilities::CalculateSideLength(iGeometry[id], iGeometry[permuta[id + 2]]);

            //std::cout<<" edge_0 "<<edge_0<<" edge_1 "<<edge_1<<std::endl;
            unsigned int node_id;
            unsigned int other_id;
            bool right_condition;
            if (edge_1 > edge_0)
            {
              node_id = permuta[id + 1];
              other_id = permuta[id + 2];
              right_condition = false;
            }
            else
            {
              node_id = permuta[id + 2];
              other_id = permuta[id + 1];
              right_condition = true;
            }

            // Find condition sharing edge
            unsigned int j;
            unsigned int jd;
            bool found = false;

            //std::cout<<" nConditions "<<nConditions.size()<<" id "<<iGeometry[id].Id()<<" other id "<<iGeometry[other_id].Id()<<std::endl;
            for (unsigned int i = 0; i < nConditions.size(); ++i)
            {
              if (nConditions[i].IsNot(TO_ERASE))
              {
                if (i_ncond.Id() != nConditions[i].Id())
                {
                  Condition::GeometryType &jGeometry = nConditions[i].GetGeometry();
                  for (unsigned int k = 0; k < jGeometry.size(); ++k)
                  {
                    //std::cout<<" ids "<<jGeometry[k].Id()<<std::endl;
                    if (jGeometry[k].Id() == iGeometry[other_id].Id())
                    {
                      jd = k;
                      j = i;
                      found = true;
                      break;
                    }
                  }
                }
              }
              if (found){
                //Do not combine if almost coplanar // and swap worse than previous (negative or zero resultant triangle)
                Condition::GeometryType &jGeometry = nConditions[j].GetGeometry();
                double coplanar = 1;
                if (right_condition){
                  array_1d<double, 3> &rNormal1 = jGeometry[permuta[jd+2]].FastGetSolutionStepValue(NORMAL);
                  array_1d<double, 3> &rNormal2 = iGeometry[node_id].FastGetSolutionStepValue(NORMAL);
                  coplanar = inner_prod(rNormal1,rNormal2);

                  GeometryUtilities::CalculateEdgesProjection(jGeometry[permuta[jd + 2]],iGeometry[other_id],iGeometry[id], edge_0);
                  GeometryUtilities::CalculateEdgesProjection(jGeometry[permuta[jd + 2]],iGeometry[node_id],iGeometry[other_id], edge_1);
                }
                else{
                  array_1d<double, 3> &rNormal1 = jGeometry[permuta[jd+1]].FastGetSolutionStepValue(NORMAL);
                  array_1d<double, 3> &rNormal2 = iGeometry[node_id].FastGetSolutionStepValue(NORMAL);
                  coplanar = inner_prod(rNormal1,rNormal2);

                  GeometryUtilities::CalculateEdgesProjection(jGeometry[permuta[jd + 1]],iGeometry[other_id],iGeometry[id], edge_0);
                  GeometryUtilities::CalculateEdgesProjection(jGeometry[permuta[jd + 1]],iGeometry[node_id],iGeometry[other_id], edge_1);
                }
                //std::cout<<" coplanar "<<coplanar<<" edge projection "<<edge_0<<","<<edge_1<<std::endl;
                if (coplanar<0.75 || edge_0 > critical_edge_projection ||  edge_1 > critical_edge_projection || edge_0<0 || edge_1<0 )
                  found = false;
                break;
              }
            }

            //edge_0(right_condition) a=iGeometry[id], b=iGeometry[permuta[id+1]], c=iGeometry[permuta[id+2]]  //other condition d=jGeometry[permuta[jd+2]]
            //edge_1(left_condition)  a=iGeometry[id], b=iGeometry[permuta[id+2]], d=iGeometry[permuta[id+1]]  //other condition c=jGeometry[permuta[jd+1]]

            //std::cout<<" found "<<found<<" jd "<<jd<<" j "<<j<<std::endl;

            // Swap diagonals and create new conditions (mark old conditions TO_ERASE)
            if (found)
            {

              Condition::GeometryType &jGeometry = nConditions[j].GetGeometry();

              std::cout<<" Swap "<<edge_projection<<" ("<<i_ncond.Id()<<" "<< nConditions[j].Id()<<")"<<" Area i "<<iGeometry.Area()<<" ("<<iGeometry[0].Id()<<" "<<iGeometry[1].Id()<<" "<<iGeometry[2].Id()<<") "<<" Area j "<<jGeometry.Area()<<" ("<<jGeometry[0].Id()<<" "<<jGeometry[1].Id()<<" "<<jGeometry[2].Id()<<") "<<std::endl;

              // now nConditions[j] and i_ncond are the conditions pair
              // Condition 1 (a,d,c)
              // Condition 2 (b,c,d)

              PointsArrayType iNodes(3);
              PointsArrayType jNodes(3);

              if (right_condition)
              {
                iNodes(0) = iGeometry(id); //reference bounding node
                iNodes(1) = jGeometry(permuta[jd + 2]);
                iNodes(2) = iGeometry(permuta[id + 2]);

                jNodes(0) = iGeometry(permuta[id + 1]);
                jNodes(1) = iGeometry(permuta[id + 2]);
                jNodes(2) = jGeometry(permuta[jd + 2]);
              }
              else
              { //left_condition
                iNodes(0) = iGeometry(id); //reference bounding node
                iNodes(1) = iGeometry(permuta[id + 1]);
                iNodes(2) = jGeometry(permuta[jd + 1]);

                jNodes(0) = iGeometry(permuta[id + 2]);
                jNodes(1) = jGeometry(permuta[jd + 1]);
                jNodes(2) = iGeometry(permuta[id + 1]);
              }

              //right condition
              pCondition = i_ncond.Clone(Id++, iNodes);

              //set flags
              pCondition->Set(NEW_ENTITY);

              //set variables
              TransferUtilities::AddConditionData(i_ncond, *pCondition);


              if (pCondition->GetGeometry().Area()<1e-12)
                KRATOS_WARNING("")<<" Zero Area  ("<<iNodes[0].Id()<<" "<<iNodes[1].Id()<<" "<<iNodes[2].Id()<<") "<<Id<<" Area a "<<pCondition->GetGeometry().Area()<<std::endl;

              list_of_new_conditions.push_back(pCondition);

              //left condition
              pCondition = nConditions[j].Clone(Id++, jNodes);

              //set flags
              pCondition->Set(NEW_ENTITY);

              //set variables
              TransferUtilities::AddConditionData(nConditions[j], *pCondition);

              if (pCondition->GetGeometry().Area()<1e-12)
                KRATOS_WARNING("")<<" Zero Area  ("<<iNodes[0].Id()<<" "<<iNodes[1].Id()<<" "<<iNodes[2].Id()<<") "<<Id<<" Area b "<<pCondition->GetGeometry().Area()<<std::endl;

              list_of_new_conditions.push_back(pCondition);

              //i condition
              SearchUtilities::SetConditionToErase(i_ncond);

              //j condition
              SearchUtilities::SetConditionToErase(nConditions[j]);

              swapped_conditions += 2;
            }
          }
        }
      }
    }

    for (std::vector<ConditionType::Pointer>::iterator i_cond = list_of_conditions.begin(); i_cond != list_of_conditions.end(); ++i_cond)
    {
      if ((*i_cond)->IsNot(TO_ERASE))
        list_of_new_conditions.push_back(*i_cond);
    }

    //update the list of old conditions with the list of new conditions
    list_of_conditions.swap(list_of_new_conditions);
    unsigned int cond_id = ModelPartUtilities::GetMaxConditionId(rModelPart) + 1;
    for (auto &i_cond : list_of_conditions)
      i_cond->SetId(cond_id++);

    std::cout << " Swapped Conditions (" << swapped_conditions <<")"<< std::endl;

    KRATOS_CATCH("")
  }


  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Private Static Member Variables
  ///@{
  ///@}
  ///@name Private Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{

  /// Assignment operator.
  RefineConditionsMesherProcess &operator=(RefineConditionsMesherProcess const &rOther);

  ///@}

}; // Class Process

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                RefineConditionsMesherProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const RefineConditionsMesherProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_REFINE_CONDITIONS_MESHER_PROCESS_HPP_INCLUDED  defined
