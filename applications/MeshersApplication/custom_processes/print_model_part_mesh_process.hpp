//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

#if !defined(KRATOS_PRINT_MODEL_PART_MESH_PROCESS_HPP_INCLUDED)
#define KRATOS_PRINT_MODEL_PART_MESH_PROCESS_HPP_INCLUDED

// External includes
#include <fstream>

// System includes

// Project includes
#include "custom_processes/mesher_process.hpp"

namespace Kratos
{

///@name Kratos Classes
///@{

class PrintModelPartMeshProcess
    : public MesherProcess
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of Process
  KRATOS_CLASS_POINTER_DEFINITION(PrintModelPartMeshProcess);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  PrintModelPartMeshProcess(ModelPart &rModelPart,
                            std::string FileName,
                            int EchoLevel)
      : mrModelPart(rModelPart)
  {
    mFileName = FileName;
    mEchoLevel = EchoLevel;
  }

  /// Destructor.
  virtual ~PrintModelPartMeshProcess() {}

  ///@}
  ///@name Operators
  ///@{

  /// This operator is provided to call the process as a function and simply calls the Execute method.
  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  /// Execute method is used to execute the Process algorithms.
  void Execute() override
  {
    KRATOS_TRY

    if (mEchoLevel > 0)
    {
      std::cout << " [ PRINT MODELPART MESHER: (" << mFileName << ") " << std::endl;
      //std::cout<<"   Nodes before erasing : "<<mrModelPart.Nodes().size()<<std::endl;
    }

    //PrintPointsXYZ();

    PrintSurfaceMesh();

    std::cout << "   PRINT MODELPART MESHER ]; step :(" << mrModelPart.GetProcessInfo()[STEP]<<")"<< std::endl;

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "PrintModelPartMeshProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "PrintModelPartMeshProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Static Member Variables
  ///@{
  ModelPart &mrModelPart;

  std::string mFileName;

  int mEchoLevel;

  ///@}
  ///@name Un accessible methods
  ///@{

  void PrintPointsXYZ()
  {
    KRATOS_TRY

    const int &step = mrModelPart.GetProcessInfo()[STEP];

    const unsigned int &dimension = mrModelPart.GetProcessInfo()[SPACE_DIMENSION];

    std::string FileName;
    FileName += mrModelPart.Name();
    FileName += "_points_";
    FileName += mFileName;
    FileName += "_";
    FileName += std::to_string(step);
    FileName += ".txt";

    std::ofstream File;

    File.open(FileName);

    for (auto &i_node : mrModelPart.Nodes())
    {
      std::string Point(std::to_string(i_node.Id()));
      for (unsigned int i = 0; i < dimension; i++)
      {
        Point += " ";
        Point += to_string_precision(i_node.Coordinates()[i]);
      }

      Point += " \n";

      File << Point;
    }

    File.close();

    KRATOS_CATCH(" ")
  }

  void PrintSurfaceMesh()
  {
    KRATOS_TRY

    const int &step = mrModelPart.GetProcessInfo()[STEP];

    std::string FileName;
    FileName += mrModelPart.Name();
    FileName += "_mesh_";
    FileName += mFileName;
    FileName += "_";
    FileName += std::to_string(step);
    FileName += ".msh";

    std::ofstream File;

    File.open(FileName);

    const int &dimension = mrModelPart.GetProcessInfo()[SPACE_DIMENSION];

    if (dimension == 3) //number of nodes of a triangle
      File << "mesh dimension 3 elemtype triangle nnode 3 \n";
    else if (dimension == 2) //number of nodes of a triangle
      File << "mesh dimension 2 elemtype line nnode 2 \n";

    // write node coordinates
    PrintNodes(File);

    // write element connectivities
    PrintConditions(File);

    File.close();

    KRATOS_CATCH(" ")
  }

  void PrintMesh()
  {
    KRATOS_TRY

    const int &step = mrModelPart.GetProcessInfo()[STEP];

    std::string FileName;
    FileName += mrModelPart.Name();
    FileName += "_mesh_";
    FileName += mFileName;
    FileName += "_";
    FileName += std::to_string(step);
    FileName += ".msh";

    std::ofstream File;

    File.open(FileName);

    const int &dimension = mrModelPart.GetProcessInfo()[SPACE_DIMENSION];

    if (dimension == 3) //number of nodes of a tetrahedron
      File << "mesh dimension 3 elemtype tetrahedra nnode 4 \n";
    else if (dimension == 2) //number of nodes of a triangle
      File << "mesh dimension 2 elemtype triangle nnode 3 \n";

    // write node coordinates
    PrintNodes(File);

    // write element connectivities
    PrintElements(File);

    File.close();

    KRATOS_CATCH(" ")
  }

  void PrintNodes(std::ofstream &File)
  {
    KRATOS_TRY

    File << "\n";
    File << "coordinates  \n";
    File << "\n";

    const unsigned int &dimension = mrModelPart.GetProcessInfo()[SPACE_DIMENSION];
    for (auto &i_node : mrModelPart.Nodes())
    {
      std::string Point(std::to_string(i_node.Id()));

      for (unsigned int i = 0; i < dimension; i++)
      {
        Point += " ";
        Point += to_string_precision(i_node.Coordinates()[i]);
      }

      Point += " \n";

      File << Point;
    }

    File << "\n";
    File << "end coordinates  \n";
    File << "\n";

    KRATOS_CATCH(" ")
  }

  void PrintConditions(std::ofstream &File)
  {
    KRATOS_TRY

    File << "\n";
    File << "elements  \n";
    File << "\n";

    // const unsigned int &dimension = mrModelPart.GetProcessInfo()[SPACE_DIMENSION];

    // unsigned int nds = 2; //number of nodes of a line
    // if (dimension == 3)   //number of nodes of a triangle
    //   nds = 3;

    unsigned int id = 1;
    for (auto &i_cond : mrModelPart.Conditions())
    {
      std::string Element(std::to_string(id++));

      for (auto &i_node : i_cond.GetGeometry())
      {
        Element += " ";
        Element += std::to_string(i_node.Id());
      }

      Element += " \n";

      File << Element;
    }

    File << "\n";
    File << "end elements  \n";
    File << "\n";

    KRATOS_CATCH(" ")
  }

  void PrintElements(std::ofstream &File)
  {
    KRATOS_TRY

    File << "\n";
    File << "elements  \n";
    File << "\n";

    // const unsigned int &dimension = mrModelPart.GetProcessInfo()[SPACE_DIMENSION];

    // unsigned int nds = 3; //number of nodes of a triangle
    // if (dimension == 3)   //number of nodes of a tetrahedron
    //   nds = 4;

    for (auto &i_elem : mrModelPart.Elements())
    {
      std::string Element(std::to_string(i_elem.Id()));

      for (auto &i_node : i_elem.GetGeometry())
      {
        Element += " ";
        Element += std::to_string(i_node.Id());
      }

      Element += " \n";

      File << Element;
    }

    File << "\n";
    File << "end elements  \n";
    File << "\n";

    KRATOS_CATCH(" ")
  }

  template <typename T>
  std::string to_string_precision(const T a_value, const int n = 12)
  {
    std::ostringstream out;
    out.precision(n);
    out << std::fixed << a_value;
    return out.str();
  }

  /// Assignment operator.
  PrintModelPartMeshProcess &operator=(PrintModelPartMeshProcess const &rOther);

  /// this function is a private function

  /// Copy constructor.
  //Process(Process const& rOther);

  ///@}

}; // Class Process

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                PrintModelPartMeshProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const PrintModelPartMeshProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_PRINT_MODEL_PART_MESH_PROCESS_HPP_INCLUDED  defined
