//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:       August 2021 $
//
//

#if !defined(KRATOS_SMOOTH_CONDITIONS_MESHER_PROCESS_HPP_INCLUDED)
#define KRATOS_SMOOTH_CONDITIONS_MESHER_PROCESS_HPP_INCLUDED

// External includes

// System includes

// Project includes
#include "custom_utilities/mesher_utilities.hpp"

#include "custom_processes/mesher_process.hpp"

namespace Kratos
{

///@name Kratos Classes
///@{

/// Smooth Nodes Process for 2D and 3D cases

class SmoothConditionsMesherProcess
    : public MesherProcess
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of Process
  KRATOS_CLASS_POINTER_DEFINITION(SmoothConditionsMesherProcess);

  typedef ModelPart::ConditionType ConditionType;
  typedef ModelPart::PropertiesType PropertiesType;
  typedef ConditionType::GeometryType GeometryType;

  typedef GlobalPointersVector<Node<3>> NodeWeakPtrVectorType;
  typedef GlobalPointersVector<Element> ElementWeakPtrVectorType;
  typedef GlobalPointersVector<Condition> ConditionWeakPtrVectorType;
  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  SmoothConditionsMesherProcess(ModelPart &rModelPart,
                                 MesherData::MeshingParameters &rRemeshingParameters,
                                 int EchoLevel)
      : mrModelPart(rModelPart),
        mrRemesh(rRemeshingParameters),
        mEchoLevel(EchoLevel)
  {
  }

  /// Destructor.
  virtual ~SmoothConditionsMesherProcess() {}

  ///@}
  ///@name Operators
  ///@{

  /// This operator is provided to call the process as a function and simply calls the Execute method.
  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  /// Execute method is used to execute the Process algorithms.
  void Execute() override
  {
    KRATOS_TRY

    //set flags for the local process execution
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{VISITED}}, {VISITED.AsFalse()});

    this->MoveBoundaries(mrModelPart);

    //set flags for the local process execution
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{VISITED}}, {VISITED.AsFalse()});

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "SmoothConditionsMesherProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "SmoothConditionsMesherProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{

  ModelPart &mrModelPart;

  MesherData::MeshingParameters &mrRemesh;

  int mEchoLevel;

  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{
  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Private Static Member Variables
  ///@{
  ///@}
  ///@name Private Static Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{

  //**************************************************************************
  //**************************************************************************

  void LabelContactBoundaries(ModelPart &rModelPart)
  {
    KRATOS_TRY

    //Check active contact nodes
    std::vector<Flags> YesFlags = {BOUNDARY, CONTACT};
    for (auto &i_node : rModelPart.Nodes())
    {
      if (i_node.Is(YesFlags))
        if (i_node.SolutionStepsDataHas(CONTACT_FORCE) && norm_2(i_node.FastGetSolutionStepValue(CONTACT_FORCE)) > 0)
          i_node.Set(VISITED);
    }

    KRATOS_CATCH("")
  }


  //**************************************************************************
  //**************************************************************************

  void MoveBoundaries(ModelPart &rModelPart)
  {

    KRATOS_TRY

    this->LabelContactBoundaries(rModelPart);

    //set neighbour conditions
    ModelPartUtilities::SetNodeNeighbourConditions(rModelPart);

    //smooth surface interaction nodes
    double smooth_factor = 0.20;
    std::vector<Flags> PositionFlags = {BOUNDARY};
    std::vector<Flags> VariableFlags = {BOUNDARY};
    std::vector<Flags> YesFlags = {VISITED};
    for (auto &i_node : rModelPart.Nodes())
    {
      if (i_node.Is(YesFlags))
      {
        NodeWeakPtrVectorType &nNodes = i_node.GetValue(NEIGHBOUR_NODES);

        for (auto &i_nnode : nNodes)
        {
          if (i_nnode.Is(YesFlags))
          {
            this->MoveBoundaryNode(i_node, PositionFlags, VariableFlags, smooth_factor);
            break; //??
          }
        }
      }
    }

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  bool MoveBoundaryNode(Node<3> &rNode, const std::vector<Flags> &rPositionFlags, const std::vector<Flags> &rVariableFlags, const double &rsmooth_factor)
  {
    KRATOS_TRY

    if (rsmooth_factor == 0.0)
      return true;

    bool moved_node = false;
    //std::cout<<" Boundary to Move Pre ["<<rNode.Id()<<"] "<<rNode.Coordinates()<<std::endl;
    unsigned int BoundaryNodes = 0;
    ConditionWeakPtrVectorType &nConditions = rNode.GetValue(NEIGHBOUR_CONDITIONS);
    NodeWeakPtrVectorType nNodes;
    for (auto i_ncond : nConditions)
    {
      GeometryType &rGeometry = i_ncond.GetGeometry();
      for (unsigned int i=0; i<rGeometry.size(); ++i)
        SearchUtilities::AddUniqueWeakPointer<Node<3>>(nNodes, rGeometry(i));
    }

    //std::cout<<" nNodes build "<<nNodes.size()<<std::endl;

    NodeWeakPtrVectorType BoundaryNeighbours;
    const array_1d<double, 3> &Normal = rNode.FastGetSolutionStepValue(NORMAL);
    bool accepted = true;
    bool edge_node = false;
    for (auto i_nnode(nNodes.begin()); i_nnode != nNodes.end(); ++i_nnode)
    {
      if(rNode.Id() != i_nnode->Id())
      {
        accepted = true;
        for (auto &i_flag : rPositionFlags)
          if (i_nnode->IsNot(i_flag))
            accepted = false;

        if (accepted) //limited curvature accepted
        {
          const array_1d<double, 3> &nNormal = i_nnode->FastGetSolutionStepValue(NORMAL);
          double projection = inner_prod(Normal,nNormal);
          if (projection<0.86)
            edge_node = true;

          BoundaryNeighbours.push_back(*i_nnode.base());
          ++BoundaryNodes;
        }
      }
    }

    if (!edge_node){
      double Distance = 0;
      if (BoundaryNodes == 2)
      {
        array_1d<double, 3> MidPoint = 0.5 * (BoundaryNeighbours.front().Coordinates() + BoundaryNeighbours.back().Coordinates());
        array_1d<double, 3> Direction = (BoundaryNeighbours.front().Coordinates() - BoundaryNeighbours.back().Coordinates());

        if (norm_2(Direction))
          Direction /= norm_2(Direction);

        array_1d<double, 3> Displacement = inner_prod((MidPoint - rNode.Coordinates()), Direction) * Direction * rsmooth_factor;
        noalias(rNode.Coordinates()) += Displacement;
        noalias(rNode.GetInitialPosition()) += Displacement;
        // noalias(rNode.FastGetSolutionStepValue(DISPLACEMENT))   += Displacement;
        // noalias(rNode.FastGetSolutionStepValue(DISPLACEMENT,1)) += Displacement;
        // rNode.GetInitialPosition() = Point(rNode.Coordinates() - rNode.FastGetSolutionStepValue(DISPLACEMENT));

        Distance = norm_2(Displacement);

        //std::cout<<" Boundary 2 Move Post ["<<rNode.Id()<<"] "<<rNode.Coordinates()<<" Displacement "<<Displacement<<std::endl;
        moved_node = true;
      }
      else if (BoundaryNodes > 3)
      {

        array_1d<double, 3> MidPoint;
        noalias(MidPoint) = ZeroVector(3);
        double quotient = 1.0 / double(BoundaryNodes);

        for (auto &i_fnnode : BoundaryNeighbours)
        {
          MidPoint += i_fnnode.Coordinates();
        }

        MidPoint *= quotient;

        array_1d<double, 3> Displacement = (MidPoint - rNode.Coordinates());
        Displacement *= rsmooth_factor;

        noalias(rNode.Coordinates()) += Displacement;
        noalias(rNode.GetInitialPosition()) += Displacement;
        // noalias(rNode.FastGetSolutionStepValue(DISPLACEMENT)) += Displacement;
        // noalias(rNode.FastGetSolutionStepValue(DISPLACEMENT,1)) += Displacement;
        //rNode.GetInitialPosition() = Point(rNode.Coordinates() - rNode.FastGetSolutionStepValue(DISPLACEMENT));

        Distance = norm_2(Displacement);

        //std::cout<<" Boundary X Move Post ["<<rNode.Id()<<"] "<<rNode.Coordinates()<<" Displacement "<<Displacement<<std::endl;
        moved_node = true;
      }
      else
      {
        std::cout << " Boundary node with only "<<BoundaryNodes<<" VISITED neighbours " << std::endl;
      }

      if (moved_node)
      {

        BoundaryNeighbours.clear();
        BoundaryNeighbours.resize(0);
        for (auto i_nnodes(nNodes.begin()); i_nnodes != nNodes.end(); ++i_nnodes)
        {
          accepted = true;
          for (auto &i_flag : rVariableFlags)
            if (i_nnodes->IsNot(i_flag))
              accepted = false;

          if (accepted)
          {
            BoundaryNeighbours.push_back(*i_nnodes.base());
            ++BoundaryNodes;
          }
        }

        //To interpolate....
        if (Distance != 0)
          rNode.Set(VISITED, true);
      }
    }

    return moved_node;

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{

  /// Assignment operator.
  SmoothConditionsMesherProcess &operator=(SmoothConditionsMesherProcess const &rOther);

  /// this function is a private function

  /// Copy constructor.
  //Process(Process const& rOther);

  ///@}

}; // Class Process

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                SmoothConditionsMesherProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const SmoothConditionsMesherProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_SMOOTH_CONDITIONS_MESHER_PROCESS_HPP_INCLUDED  defined
