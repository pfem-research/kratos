//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

#if !defined(KRATOS_GENERATE_NEW_ELEMENTS_MESHER_PROCESS_HPP_INCLUDED)
#define KRATOS_GENERATE_NEW_ELEMENTS_MESHER_PROCESS_HPP_INCLUDED

// System includes

// External includes

// Project includes
//#include "custom_processes/nodal_neighbours_search_process.hpp"
#include "custom_meshers/laplacian_smoothing.hpp"
#include "custom_processes/mesher_process.hpp"

namespace Kratos
{

///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
class GenerateNewElementsMesherProcess
    : public MesherProcess
{
public:
  ///@name Type Definitions
  ///@{

  typedef Node<3> NodeType;
  typedef array_1d<double,3> VectorType;
  typedef Geometry<NodeType> GeometryType;
  typedef GeometryType::PointsArrayType PointsArrayType;

  typedef GlobalPointersVector<Element> ElementWeakPtrVectorType;

  /// Pointer definition of GenerateNewElementsMesherProcess
  KRATOS_CLASS_POINTER_DEFINITION(GenerateNewElementsMesherProcess);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  GenerateNewElementsMesherProcess(ModelPart &rModelPart,
                                   MesherData::MeshingParameters &rRemeshingParameters,
                                   int EchoLevel)
      : mrModelPart(rModelPart),
        mrRemesh(rRemeshingParameters)
  {
    mEchoLevel = EchoLevel;
  }

  /// Destructor.
  virtual ~GenerateNewElementsMesherProcess()
  {
  }

  ///@}
  ///@name Operators
  ///@{

  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  void Execute() override
  {
    KRATOS_TRY

    if (mEchoLevel > 0)
      std::cout << " [ GENERATE NEW ELEMENTS: " << std::endl;

    GenerateElements(mrModelPart);

    //ModelPartUtilities::CheckElements(mrModelPart);

    if (mEchoLevel > 0)
      std::cout << "   GENERATE NEW ELEMENTS ]; " << std::endl;

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "GenerateNewElementsMesherProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "GenerateNewElementsMesherProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{
  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  ModelPart &mrModelPart;

  MesherData::MeshingParameters &mrRemesh;

  int mEchoLevel;

  ///@}
  ///@name Private Operators
  ///@{

  //**************************************************************************
  //**************************************************************************

  void GenerateElements(ModelPart &rModelPart)
  {
    BuiltinTimer time_counter;

    if (rModelPart.Name() != mrRemesh.SubModelPartName)
      std::cout << " ModelPart Supplied do not corresponds to the Meshing Domain: (" << rModelPart.Name() << " != " << mrRemesh.SubModelPartName << ")" << std::endl;


    //Select Mesh Elements in case of previously made
    if (!mrRemesh.MeshElementsSelectedFlag)
    {
      KRATOS_WARNING("") << " ATTENTION : no selection of elements performed before building the elements " << std::endl;
      SelectElementsMesherProcess SelectElements(rModelPart, mrRemesh, mEchoLevel);
      SelectElements.Execute();
    }

    const Element &rReferenceElement = mrRemesh.GetReferenceElement();

    // const unsigned int number_of_vertices = rModelPart.Elements().front().GetGeometry().size();
    // const unsigned int number_of_faces = rModelPart.Elements().front().GetGeometry().FacesNumber();

    int &OutNumberOfElements = mrRemesh.OutMesh.GetNumberOfElements();
    int *OutElementList = mrRemesh.OutMesh.GetElementList();
    int *OutElementNeighbourList = mrRemesh.OutMesh.GetElementNeighbourList();

    //generate kratos elements (conditions are not touched)
    int number_of_preserved_elements = 0;
    for (int el = 0; el < OutNumberOfElements; ++el)
    {
      if (mrRemesh.PreservedElements[el]){
        ++number_of_preserved_elements;
        mrRemesh.PreservedElements[el] = number_of_preserved_elements;
      }
      else
        mrRemesh.PreservedElements[el] = -1;
    }

    if( mEchoLevel > 0 )
      std::cout<<"  Number of preserved elements "<<number_of_preserved_elements<<std::endl;
    std::vector<GeometryType> list_of_element_vertices(number_of_preserved_elements);
    GetVerticesList(rModelPart, OutElementList, mrRemesh.PreservedElements, OutNumberOfElements, list_of_element_vertices);

    std::vector<std::vector<int>> list_of_element_neighbours(number_of_preserved_elements);
    GetNeighboursList(rModelPart, OutElementNeighbourList, mrRemesh.PreservedElements, OutNumberOfElements, list_of_element_neighbours);

    //print out time
    if( mEchoLevel > 0 )
      std::cout<<"  GENERATE NEW ELEMENTS lists time = "<< time_counter.ElapsedSeconds() <<std::endl;

    BuiltinTimer time_smoothing_counter;

    //*******************************************************************
    //5) Laplacian Smoothing

    // check geometrical smoothing is better for solid cutting problems commented July 2018 for fluid testing

    //Check Mesh Info to perform smoothing:
    // bool smoothing = false;
    // if (mrRemesh.Options.Is(MesherData::REFINE))
    //   smoothing = mrRemesh.Info->CheckGeometricalSmoothing();
    // else
    //   smoothing = true;
    //if( mrRemesh.Options.Is(MesherData::MESH_SMOOTHING) && smoothing ){

    if (mrRemesh.Options.Is(MesherData::MESH_SMOOTHING))
    {

      const unsigned int &OutNumberOfPoints = mrRemesh.OutMesh.GetNumberOfPoints();
      if (mrRemesh.GetSmoothingControl().Execute()){
	LaplacianSmoothing::ApplyMeshSmoothing(rModelPart, OutElementList, mrRemesh.PreservedElements, OutNumberOfPoints, mrRemesh.OffsetFactor);
	std::cout<<"   Laplacian smoothing executed "<<std::endl;
      }
    }
    //*******************************************************************

    //print out time
    if( mEchoLevel > 0 )
      std::cout<<"  GENERATE NEW ELEMENTS smoothing time = "<< time_smoothing_counter.ElapsedSeconds() <<std::endl;

    BuiltinTimer time_transfer_counter;

    //*******************************************************************
    //6) Pass transfer variables
    SetFlagsUtilities::SetFlagsToElements(rModelPart, {{VISITED}}, {VISITED.AsFalse()});
    if(rModelPart.Is(FLUID) || mrRemesh.Options.IsNot(MesherData::SPR_TRANSFER))
       TransferUtilities::CreateElementsCPP(rModelPart, list_of_element_vertices);
    else{
      SetNodeMissingNeighbours(list_of_element_vertices);
      TransferUtilities::CreateElementsSPR(rModelPart, *mrRemesh.GetTransferVariables(), list_of_element_vertices);
    }
    SetFlagsUtilities::SetFlagsToElements(rModelPart, {{VISITED}}, {VISITED.AsFalse()});
    //*******************************************************************

    //print out time
    if( mEchoLevel > 0 )
      std::cout<<"  GENERATE NEW ELEMENTS transfer time = "<< time_transfer_counter.ElapsedSeconds() <<std::endl;


    if (mrRemesh.Options.Is(MesherData::MESH_SMOOTHING))
    {
      BuiltinTimer time_project_counter;
      // set initial coordinates from current smoothed position and displacements
      ModelPartUtilities::RecoverInitialPosition(rModelPart);
      std::cout<<"    POSITION RECOVER time = "<< time_project_counter.ElapsedSeconds() <<std::endl;
    }


    //*******************************************************************w
    //std::cout<<" Number of Nodes "<<rModelPart.Nodes().size()<<" Number Of Ids "<<mrRemesh.NodalPreIds.size()<<std::endl;

    //7) Restore global ID's
    block_for_each(rModelPart.Nodes(),[&](NodeType &i_node){
      i_node.SetId(mrRemesh.NodalPreIds[i_node.Id()]);
    }
    );

    //*******************************************************************

    BuiltinTimer time_neighbours_counter;

    //8) Filling element neighbour list
    SetElementNeighbours(rModelPart,list_of_element_neighbours);


    // 9) Filling nodal neighbours list (needed in smoothing, now out of meshing process)
    // NodalNeighboursSearchProcess FindNeighbours(rModelPart);
    // FindNeighbours.Execute();

    //print out time
    if( mEchoLevel > 0 )
      std::cout<<"  GENERATE NEW ELEMENTS neighbours time = "<< time_neighbours_counter.ElapsedSeconds() <<std::endl;

    //print out the mesh generation time
    if( mEchoLevel > 0 )
      std::cout<<"  GENERATE NEW ELEMENTS time = "<< time_counter.ElapsedSeconds() <<std::endl;
  }

  //**************************************************************************
  //**************************************************************************

  static inline void SetNodeMissingNeighbours(std::vector<GeometryType> &list_of_element_vertices)
  {
    //std::cout<<" Set Missing Neighbours in"<<std::endl;
    for (auto &i_geo : list_of_element_vertices)
    {
      for (auto &i_node : i_geo)
      {
        ElementWeakPtrVectorType &nElements = i_node.GetValue(NEIGHBOUR_ELEMENTS);
        if (nElements.size() == 0)
        {
          //std::cout<<" Node["<<i_node.Id()<<"] Neighbours: "<<nElements.size()<<std::endl;
          for (auto &i_nnode : i_geo)
          {
            if(i_nnode.Id() != i_node.Id())
            {
              ElementWeakPtrVectorType &nExtraElements = i_nnode.GetValue(NEIGHBOUR_ELEMENTS);
              for (unsigned int i=0; i<nExtraElements.size(); ++i)
                SearchUtilities::AddUniqueWeakPointer<Element>(nElements, nExtraElements(i));
            }
          }
          //std::cout<<" Node["<<i_node.Id()<<"] Neighbours extended: "<<nElements.size()<<std::endl;
        }
      }
    }
    //std::cout<<" Set Missing Neighbours out"<<std::endl;
  }

  //**************************************************************************
  //**************************************************************************

  static inline void GetVerticesList(ModelPart &rModelPart, const int *OutElementList, const std::vector<int> &rPreservedElements, const int &OutNumberOfElements, std::vector<GeometryType> &list_of_element_vertices)
  {
    ModelPart::NodesContainerType::iterator nodes_begin = rModelPart.NodesBegin();
    const unsigned int &number_of_vertices = rModelPart.Elements().front().GetGeometry().size();

    IndexPartition<int>(OutNumberOfElements).for_each(
        [&](int el){
          if (rPreservedElements[el]>0)
          {
            GeometryType vertices;
            GetVertices(el, nodes_begin, OutElementList, vertices, number_of_vertices);
            list_of_element_vertices[rPreservedElements[el]-1] = vertices;
          }
        }
    );
  }

  //**************************************************************************
  //**************************************************************************

  static inline void GetVertices(int &el, ModelPart::NodesContainerType::iterator nodes_begin, const int *OutElementList, GeometryType &rVertices, const unsigned int &number_of_vertices)
  {
    unsigned int id = el * number_of_vertices;
    for (unsigned int pn = 0; pn < number_of_vertices; ++pn)
      rVertices.push_back(*(nodes_begin + OutElementList[id + pn] - 1).base());
  }

  //**************************************************************************
  //**************************************************************************

  static inline void GetNeighboursList(const ModelPart &rModelPart, const int *OutElementNeighbourList, const std::vector<int> &rPreservedElements, const int &OutNumberOfElements, std::vector<std::vector<int>> &list_of_element_neighbours)
  {
    const unsigned int &number_of_faces = rModelPart.Elements().front().GetGeometry().FacesNumber();
    IndexPartition<int>(OutNumberOfElements).for_each([&](int el){
      if (rPreservedElements[el]>0)
      {
        std::vector<int> neighbours(number_of_faces);
        GetNeighbours(el, OutElementNeighbourList, rPreservedElements, neighbours, number_of_faces);
        list_of_element_neighbours[rPreservedElements[el]-1] = neighbours;
      }
    });
  }

  //**************************************************************************
  //**************************************************************************

  static inline void GetNeighbours(int &el, const int *OutElementNeighbourList, const std::vector<int> &rPreservedElements, std::vector<int> &rNeighbours, const unsigned int &number_of_faces)
  {
    int index = 0;
    const unsigned int id = el * number_of_faces;
    for (unsigned int iface = 0; iface < number_of_faces; ++iface)
    {
      index = OutElementNeighbourList[id + iface];
      if (index > 0)
        index = rPreservedElements[index - 1];

      if (index > 0)
        rNeighbours[iface] = index - 1;
      else
        rNeighbours[iface] = rPreservedElements[el]-1;
    }
  }

  //**************************************************************************
  //**************************************************************************

  void SetElementNeighboursClassical(ModelPart &rModelPart)
  {
    std::cout << " [ SET ELEMENT NEIGHBOURS : " << std::endl;
    std::cout << "   Initial Faces : " << rModelPart.Conditions().size() << std::endl;

    ModelPart::ElementsContainerType::iterator element_begin = rModelPart.ElementsBegin();

    const unsigned int nds = element_begin->GetGeometry().size();

    int *OutElementNeighbourList = mrRemesh.OutMesh.GetElementNeighbourList();

    unsigned int total_faces = 0;
    unsigned int Id = 0;
    for (auto i_elem(rModelPart.ElementsBegin()); i_elem != rModelPart.ElementsEnd(); ++i_elem)
    {
      for (unsigned int i = 0; i < mrRemesh.PreservedElements.size(); ++i)
      {
        if (mrRemesh.PreservedElements[Id] == -1)
          ++Id;
        else
          break;
      }

      //Id = i_elem->Id()-1;

      unsigned int number_of_faces = i_elem->GetGeometry().FacesNumber(); //defined for triangles and tetrahedra

      ElementWeakPtrVectorType &nElements = i_elem->GetValue(NEIGHBOUR_ELEMENTS);
      nElements.resize(number_of_faces);

      int index = 0;
      unsigned int element_faces = 0;
      for (unsigned int iface = 0; iface < number_of_faces; ++iface)
      {

        index = OutElementNeighbourList[Id * nds + iface];

        if (index > 0)
        {
          //std::cout<<" Element "<<Id<<" size "<<mrRemesh.PreservedElements.size()<<std::endl;
          //std::cout<<" Index pre "<<index<<" size "<<mrRemesh.PreservedElements.size()<<std::endl;
          index = mrRemesh.PreservedElements[index - 1];
          //std::cout<<" Index post "<<index<<std::endl;
        }

        if (index > 0)
        {
          nElements(iface) = *(element_begin + index - 1).base();
        }
        else
        {
          nElements(iface) = *i_elem.base();
          ++total_faces;
          ++element_faces;
        }
      }

      //check full rigid isolated element
      if (element_faces == number_of_faces)
      {
        GeometryType &eGeometry = i_elem->GetGeometry();
        unsigned int rigid_nodes = 0;
        for (auto &i_node : eGeometry)
        {
          if (i_node.Is(RIGID))
            ++rigid_nodes;
        }
        if (rigid_nodes == eGeometry.size())
        {
          std::cout << " WARNING :: ISOLATED RIGID ELEMENT [" << i_elem->Id() << "]" << std::endl;
        }
      }
      // std::cout<<" Element ["<<i_elem->Id()<<"] ";
      // for (unsigned int iface = 0; iface < number_of_faces; ++iface)
      //   std::cout<<" f["<<iface<<"]"<<nElements[iface].Id()<<" ";
      // std::cout<<std::endl;
      ++Id;
    }


    std::cout << "   Final Faces : " << total_faces << std::endl;
    std::cout << "   SET ELEMENT NEIGHBORS ]; " << std::endl;

  }

  //**************************************************************************
  //**************************************************************************

  void SetElementNeighbours(ModelPart &rModelPart)
  {
    if (mEchoLevel > 0)
      std::cout << " [ SET ELEMENT NEIGHBOURS : " << std::endl;

    std::cout << "   Initial Faces : " << rModelPart.Conditions().size() << std::endl;

    ModelPart::ElementsContainerType::iterator element_begin = rModelPart.ElementsBegin();

    const unsigned int nds = element_begin->GetGeometry().size();

    int *OutElementNeighbourList = mrRemesh.OutMesh.GetElementNeighbourList();

    block_for_each(rModelPart.Elements(),[&](Element &i_elem){
      const unsigned int &id = i_elem.Id()-1;
      if (mrRemesh.PreservedElements[id]>0){
        const unsigned int &number_of_faces = i_elem.GetGeometry().FacesNumber(); //defined for triangles and tetrahedra
        ElementWeakPtrVectorType &nElements = i_elem.GetValue(NEIGHBOUR_ELEMENTS);
        nElements.resize(number_of_faces);

        int index = 0;
        for (unsigned int iface = 0; iface < number_of_faces; ++iface)
        {
          index = OutElementNeighbourList[id * nds + iface];
          if (index > 0)
            index = mrRemesh.PreservedElements[index - 1];

          if (index > 0)
            nElements(iface) = *(element_begin + index - 1).base();
          else
            nElements(iface) = *(element_begin + id).base();
        }
      }
    }
    );

    if (mEchoLevel > 0)
      std::cout << "   SET ELEMENT NEIGHBORS ]; " << std::endl;
  }


  //**************************************************************************
  //**************************************************************************

  void SetElementNeighbours(ModelPart &rModelPart, std::vector<std::vector<int>> &rNeighbourIds)
  {
    ModelPart::ElementsContainerType::iterator element_begin = rModelPart.ElementsBegin();

    block_for_each(rModelPart.Elements(),[&](Element &i_elem){
      ElementWeakPtrVectorType &nElements = i_elem.GetValue(NEIGHBOUR_ELEMENTS);
      const unsigned int id = i_elem.Id()-1;
      for (unsigned int iface = 0; iface < rNeighbourIds[id].size(); ++iface)
        nElements(iface) = *(element_begin + rNeighbourIds[id][iface]).base();
    });
  }

  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{

  /// Assignment operator.
  GenerateNewElementsMesherProcess &operator=(GenerateNewElementsMesherProcess const &rOther);

  ///@}

}; // Class GenerateNewElementsMesherProcess

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                GenerateNewElementsMesherProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const GenerateNewElementsMesherProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_GENERATE_NEW_ELEMENTS_MESHER_PROCESS_HPP_INCLUDED  defined
