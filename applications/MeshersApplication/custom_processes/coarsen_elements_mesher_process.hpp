//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:         July 2021 $
//
//

#if !defined(KRATOS_COARSEN_ELEMENTS_MESHER_PROCESS_HPP_INCLUDED)
#define KRATOS_COARSEN_ELEMENTS_MESHER_PROCESS_HPP_INCLUDED

// External includes

// System includes

// Project includes
#include "custom_processes/mesher_process.hpp"


namespace Kratos
{

///@name Kratos Classes
///@{

/// Remove Mesh Nodes Process for 2D and 3D cases
/**
    The process labels the nodes to be erased (TO_ERASE)
*/

class CoarsenElementsMesherProcess
    : public MesherProcess
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of Process
  KRATOS_CLASS_POINTER_DEFINITION(CoarsenElementsMesherProcess);

  typedef ModelPart::MeshType::GeometryType::PointsArrayType PointsArrayType;

  typedef GlobalPointersVector<Node<3>> NodeWeakPtrVectorType;
  typedef GlobalPointersVector<Element> ElementWeakPtrVectorType;
  typedef GlobalPointersVector<Condition> ConditionWeakPtrVectorType;

  typedef Bucket<3, Node<3>, std::vector<Node<3>::Pointer>, Node<3>::Pointer, std::vector<Node<3>::Pointer>::iterator, std::vector<double>::iterator> BucketType;
  typedef Tree<KDTreePartition<BucketType>> KdtreeType; //Kdtree

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  CoarsenElementsMesherProcess(ModelPart &rModelPart,
                                MesherData::MeshingParameters &rRemeshingParameters,
                                int EchoLevel)
      : mrModelPart(rModelPart),
        mrRemesh(rRemeshingParameters)
  {
    mEchoLevel = EchoLevel;
  }

  /// Destructor.
  virtual ~CoarsenElementsMesherProcess() {}

  ///@}
  ///@name Operators
  ///@{

  /// This operator is provided to call the process as a function and simply calls the Execute method.
  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  /// Execute method is used to execute the Process algorithms.
  void Execute() override
  {
    KRATOS_TRY

    mInfoData.Initialize(mEchoLevel);
    this->RemoveInteriorNodes(mrModelPart);
    mInfoData.Finalize(mEchoLevel);

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "CoarsenElementsMesherProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "CoarsenElementsMesherProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{

  ModelPart &mrModelPart;

  MesherData::MeshingParameters &mrRemesh;

  int mEchoLevel;

  // struct for monitoring the process information
  struct ProcessInfoData
  {
    unsigned int initial_nodes;
    unsigned int final_nodes;

    void Initialize(int EchoLevel)
    {
      initial_nodes = 0;
      final_nodes = 0;

      if (EchoLevel > 0)
        std::cout << " [ REMOVE INSIDE NODES: " << std::endl;
    }

    void Finalize(int EchoLevel)
    {

      if (EchoLevel > 0)
      {
        std::cout << "   [ NODES      ( removed : " << int(initial_nodes-final_nodes) << " total: " << final_nodes << " ) ]" << std::endl;
        std::cout << "   REMOVE INSIDE NODES ]; " << std::endl;
      }

    }
  };

  ProcessInfoData mInfoData;

  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{

  void RemoveInteriorNodes(ModelPart &rModelPart)
  {
    KRATOS_TRY

    mrRemesh.Refine->Interior.Info.Initialize();

    mInfoData.initial_nodes = rModelPart.NumberOfNodes();

    //set flags for the local process execution
    SetFlagsUtilities::SetFlagsToNodes(rModelPart, {{RIGID}, {INLET}}, {BLOCKED});

    //check existing nodes TO_ERASE
    CheckNodesToErase(rModelPart);

    //if the remove_node switch is activated, we check if the nodes got too close
    if (mrRemesh.IsInterior(RefineData::COARSEN))
    {
      if (mrRemesh.IsInterior(RefineData::COARSEN_ON_ERROR))
        this->SelectNodesOnError(rModelPart, mrRemesh.Refine->Interior.Info.on_error);

      if (mrRemesh.IsInterior(RefineData::COARSEN_ON_DISTANCE))
        this->SelectNodesOnDistance(rModelPart, mrRemesh.Refine->Interior.Info.on_distance);
    }

    if (mrRemesh.Refine->Interior.Info.on_error > 0 || mrRemesh.Refine->Interior.Info.on_distance > 0)
       ModelPartUtilities::EraseNodes(rModelPart);

    //reset flags for the local process execution
    SetFlagsUtilities::SetFlagsToNodes(rModelPart, {{BLOCKED}}, {BLOCKED.AsFalse()});

    mInfoData.final_nodes = rModelPart.NumberOfNodes();

    KRATOS_CATCH(" ")
  }

  //**************************************************************************
  //**************************************************************************

  virtual void CheckNodesToErase(ModelPart &rModelPart)
  {
    KRATOS_TRY

    for (auto& i_node: rModelPart.Nodes())
      if (i_node.Is(TO_ERASE))
        std::cout<<" Node ["<<i_node.Id()<<"] is TO_ERASE (before coarsen elements) "<<std::endl;

    KRATOS_CATCH(" ")
  }

  //**************************************************************************
  //**************************************************************************

  virtual void SelectNodesOnError(ModelPart &rModelPart, unsigned int& error_removed_nodes, double size_factor=0.2)
  {
    KRATOS_TRY

    // number of variables
    std::vector<std::pair<const Variable<double>*, double> > ErrorVariables = mrRemesh.Refine->Interior.ErrorVariables;
    for (auto& refine : mrRemesh.RefineVector)
      for (auto& variable : refine->Interior.ErrorVariables)
        ErrorVariables.push_back(variable);

    // number of nodes
    unsigned int number_of_nodes = 0;
    if (mrRemesh.InputInitializedFlag)
      number_of_nodes = mrRemesh.NodeMaxId + 1;
    else
      number_of_nodes = ModelPartUtilities::GetMaxNodeId(rModelPart) + 1;
    std::vector<int> nodes_ids(number_of_nodes);

    // check mesh error
    MesherData MeshData;

    for(auto& error_variable : ErrorVariables)
    {
      std::vector<double> NodalError(rModelPart.NumberOfNodes() + 1);
      ModelPartUtilities::CalculateNodalError(rModelPart, NodalError, nodes_ids, *error_variable.first);

      // can not be parallel because I write and check TO_ERASE to nodes
      for (auto& i_node: rModelPart.Nodes())
      {
        if (i_node.IsNot(TO_REFINE) && i_node.IsNot(BOUNDARY))
        {
          RefineData& Refine = MeshData.GetRefineData(i_node, mrRemesh, rModelPart.GetProcessInfo());

          NodeWeakPtrVectorType &nNodes = i_node.GetValue(NEIGHBOUR_NODES);
          bool erased_nodes = false;
          for (auto &i_nnode : nNodes)
          {
            if (i_nnode.Is(TO_ERASE))
            {
              erased_nodes = true;
              break;
            }
          }

          if (!erased_nodes)
          {
            double &MeanError = i_node.FastGetSolutionStepValue(MEAN_ERROR);
            MeanError = NodalError[nodes_ids[i_node.Id()]];

            if (MeanError < error_variable.second)
            {
              ElementWeakPtrVectorType &nElements = i_node.GetValue(NEIGHBOUR_ELEMENTS);
              double mean_node_radius = 0;
              for (auto &i_nelem : nElements)
                mean_node_radius += GeometryUtilities::CalculateElementRadius(i_nelem.GetGeometry()); //Triangle 2D, Tetrahedron 3D
              mean_node_radius /= double(nElements.size());

              if (mean_node_radius < size_factor * Refine.Interior.DistanceMeshSize)
              {
                i_node.Set(TO_ERASE);
                error_removed_nodes++;
              }
            }
          }
        }
      }
    }

    KRATOS_CATCH(" ")
  }

  //**************************************************************************
  //**************************************************************************

  virtual void SelectNodesOnDistance(ModelPart &rModelPart, unsigned int &inside_nodes_removed, double size_factor=0.15)
  {
    KRATOS_TRY

    if (rModelPart.Is(FLUID))
      KRATOS_WARNING("") << " Remove nodes on distance for a FLUID domain, you are not in the correct place " << std::endl;

    //create search tree
    KdtreeType::Pointer SearchTree;
    std::vector<Node<3>::Pointer> list_of_nodes;
    ModelPartUtilities::CreateSearchTree(rModelPart, SearchTree, list_of_nodes, 20);

    //all of the nodes in this list will be preserved
    unsigned int num_neighbours = 100;
    std::vector<Node<3>::Pointer> neighbours(num_neighbours);
    std::vector<double> neighbour_distances(num_neighbours);

    //radius means the distance, if the distance between two nodes is closer to radius -> mark for removing
    unsigned int n_points_in_radius;

    MesherData MeshData;
    for (auto &i_node : rModelPart.Nodes())
    {
      RefineData& Refine = MeshData.GetRefineData(i_node, mrRemesh, rModelPart.GetProcessInfo());

      double size_for_distance_inside = size_factor * Refine.Interior.DistanceMeshSize;
      //double size_for_distance_boundary_squared = pow(0.25 * Refine.Boundary.DistanceMeshSize, 2);

      const std::vector<Flags> NotFlags = {BLOCKED, TO_ERASE, NEW_ENTITY, BOUNDARY, CONTACT};
      if (i_node.IsNot(NotFlags))
      {
        // the neighbour_distance returned by SearchInRadius is squared (distance to the power of two)
        n_points_in_radius = SearchTree->SearchInRadius(i_node, size_for_distance_inside, neighbours.begin(), neighbour_distances.begin(), num_neighbours);

        if (n_points_in_radius > 1)
        {
          if (!this->CheckEngagedNode(i_node, neighbours, neighbour_distances, n_points_in_radius))
          {
            i_node.Set(TO_ERASE);
            inside_nodes_removed++;
          }
        }
      }
    }

    KRATOS_CATCH(" ")
  }

  //**************************************************************************
  //**************************************************************************

  virtual bool CheckEngagedNode(Node<3> &rNode, std::vector<Node<3>::Pointer> &rNeighbours, std::vector<double> &rNeighbourDistances, unsigned int &rn_points_in_radius)
  {
    KRATOS_TRY

    //look if we are already erasing any of the other nodes
    bool engaged_node = false;

    //trying to remove the node in the shorter distance anyway
    double min_distance = std::numeric_limits<double>::max();
    unsigned int counter = 0;
    unsigned int closest_node = 0;

    for (std::vector<double>::iterator nd = rNeighbourDistances.begin(); nd != rNeighbourDistances.begin() + rn_points_in_radius; ++nd)
    {
      if ((*nd) < min_distance && (*nd) > 0.0)
      {
        min_distance = (*nd);
        closest_node = counter;
      }
      //std::cout<<" Distances "<<counter<<" "<< (*nd) <<std::endl;
      ++counter;
    }

    if (rNeighbours[closest_node]->Is(TO_ERASE) && rNeighbours[closest_node]->IsNot(BOUNDARY))
    {
      engaged_node = true;
    }
    else
    {
      counter = 0;
      unsigned int erased_node = 0;
      for (std::vector<Node<3>::Pointer>::iterator i_nnode = rNeighbours.begin(); i_nnode != rNeighbours.begin() + rn_points_in_radius; ++i_nnode)
      {
        if ((*i_nnode)->IsNot(BOUNDARY) && (*i_nnode)->Is(TO_ERASE))
        {
          erased_node = counter;
          engaged_node = true;
          break;
        }
        ++counter;
      }
      if (engaged_node)
      {
        // if the distance is 5 times smaller remove anyway
        if (rNeighbourDistances[closest_node] * 5 < rNeighbourDistances[erased_node])
        {
          engaged_node = false;
          //std::cout<<"     Distance Criterion Node ["<<i_node.Id()<<"] TO_ERASE "<<closest_node<<" "<<erased_node<<std::endl;
        }
      }
    }

    return engaged_node;

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Private Static Member Variables
  ///@{
  ///@}
  ///@name Private Static Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{

  /// Assignment operator.
  CoarsenElementsMesherProcess &operator=(CoarsenElementsMesherProcess const &rOther);

  ///@}

}; // Class Process

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                CoarsenElementsMesherProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const CoarsenElementsMesherProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_COARSEN_ELEMENTS_MESHER_PROCESS_HPP_INCLUDED  defined
