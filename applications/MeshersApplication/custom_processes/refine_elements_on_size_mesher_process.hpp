//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

#if !defined(KRATOS_REFINE_ELEMENTS_ON_SIZE_MESHER_PROCESS_HPP_INCLUDED)
#define KRATOS_REFINE_ELEMENTS_ON_SIZE_MESHER_PROCESS_HPP_INCLUDED

// External includes

// System includes

// Project includes
#include "custom_processes/mesher_process.hpp"

namespace Kratos
{

  ///@name Kratos Classes
  ///@{

  /// Refine Mesh Elements Process 2D and 3D
  /** The process labels the elements to be refined in the mesher
      it applies a size constraint to elements that must be refined.

  */
  class RefineElementsOnSizeMesherProcess
    : public MesherProcess
  {
  public:
    ///@name Type Definitions
    ///@{

    /// Pointer definition of Process
    KRATOS_CLASS_POINTER_DEFINITION(RefineElementsOnSizeMesherProcess);

    typedef ModelPart::ConditionType ConditionType;
    typedef ModelPart::PropertiesType PropertiesType;
    typedef ConditionType::GeometryType GeometryType;

    ///@}
    ///@name Life Cycle
    ///@{

    /// Default constructor.
    RefineElementsOnSizeMesherProcess(ModelPart &rModelPart,
                                      MesherData::MeshingParameters &rRemeshingParameters,
                                      int EchoLevel)
      : mrModelPart(rModelPart),
        mrRemesh(rRemeshingParameters)
    {
      mEchoLevel = EchoLevel;
    }

    /// Destructor.
    virtual ~RefineElementsOnSizeMesherProcess() {}

    ///@}
    ///@name Operators
    ///@{

    /// This operator is provided to call the process as a function and simply calls the Execute method.
    void operator()()
    {
      Execute();
    }

    ///@}
    ///@name Operations
    ///@{

    /// Execute method is used to execute the Process algorithms.
    void Execute() override
    {
      KRATOS_TRY

      mInfoData.Initialize(mEchoLevel);
      this->SetElementToRefine();
      mInfoData.Finalize(mEchoLevel);

      KRATOS_CATCH(" ")
    }

    ///@}
    ///@name Access
    ///@{
    ///@}
    ///@name Inquiry
    ///@{
    ///@}
    ///@name Input and output
    ///@{

    /// Turn back information as a string.
    std::string Info() const override
    {
      return "RefineElementsOnSizeMesherProcess";
    }

    /// Print information about this object.
    void PrintInfo(std::ostream &rOStream) const override
    {
      rOStream << "RefineElementsOnSizeMesherProcess";
    }

    /// Print object's data.
    void PrintData(std::ostream &rOStream) const override
    {
    }

    ///@}
    ///@name Friends
    ///@{
    ///@}

  private:
    ///@name Static Member Variables
    ///@{
    ///@}
    ///@name Static Member Variables
    ///@{

    ModelPart &mrModelPart;

    MesherData::MeshingParameters &mrRemesh;

    int mEchoLevel;

    // struct for monitoring the process information
    struct ProcessInfoData
    {
      unsigned int refine_on_size;
      unsigned int refine_on_threshold;
      unsigned int visited_elements;

      void Initialize(int EchoLevel)
      {
        refine_on_size = 0;
        refine_on_threshold = 0;
        visited_elements = 0;

        if (EchoLevel > 0)
          std::cout << " [ SELECT ELEMENTS TO REFINE : " << std::endl;

      }

      void Finalize(int EchoLevel)
      {
        if (EchoLevel > 0)
        {
          std::cout << "   Visited Elements: " << visited_elements << " [threshold:" << refine_on_threshold << "/size:" << refine_on_size << "]" << std::endl;
          std::cout << "   SELECT ELEMENTS TO REFINE ]; " << std::endl;
        }
      }
    };

    ProcessInfoData mInfoData;

    ///@}
    ///@name Private Operators
    ///@{
    ///@}
    ///@name Private Operations
    ///@{

    /// Execute method is used to execute the Process algorithms.
    void SetElementToRefine()
    {
      KRATOS_TRY

      //***SIZES :::: parameters do define the tolerance in mesh size:
      double factor_for_distance_inside = 1.0;
      double factor_for_distance_boundary= 1.0;

      const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();

      int id = 0;
      if (mrRemesh.IsInterior(RefineData::REFINE))
      {
        ModelPart::ElementsContainerType::iterator element_begin = mrModelPart.ElementsBegin();

        unsigned int nds = (*element_begin).GetGeometry().size();

        MesherData::MeshContainer &InMesh = mrRemesh.InMesh;

        unsigned int NumberOfElements = mrRemesh.Info->Current.Elements;

        InMesh.CreateElementList(NumberOfElements, nds); //number of preserved elements
        InMesh.CreateElementSizeList(NumberOfElements);

        int &OutNumberOfElements = mrRemesh.OutMesh.GetNumberOfElements();

        int *InElementList = mrRemesh.InMesh.GetElementList();
        double *InElementSizeList = mrRemesh.InMesh.GetElementSizeList();

        int *OutElementList = mrRemesh.OutMesh.GetElementList();

        ModelPart::NodesContainerType::iterator nodes_begin = mrModelPart.NodesBegin();

        //PREPARE THE NODAL_H as a variable to control the automatic point insertion
        //**************************************************************************

        if (!mrRemesh.IsBoundary(RefineData::REFINE))
        {
          for (unsigned int i = 0; i < mrModelPart.Nodes().size(); i++)
          {
            //Assign a huge NODAL_H to the Boundary nodes, so that there no nodes will be added
            if ((nodes_begin + i)->Is(BOUNDARY))
            {
              double &nodal_h = (nodes_begin + i)->FastGetSolutionStepValue(NODAL_H);
              nodal_h *= 2.0;
            }
          }
        }

        //SET THE REFINED ELEMENTS AND THE AREA (NODAL_H)
        //*********************************************************************
        mrRemesh.Info->ExceededThreshold = false;

        for (int el = 0; el < OutNumberOfElements; el++)
        {
          if (mrRemesh.PreservedElements[el]>0)
          {

            double prescribed_h = 0;
            bool dissipative = false; //dissipative means reference threshold is overwhelmed
            bool refine_size = false;

            unsigned int count_dissipative = 0;
            unsigned int count_boundary_inserted = 0;
            unsigned int count_boundary = 0;
            unsigned int count_contact_boundary = 0;

            Geometry<Node<3>> vertices;

            for (unsigned int pn = 0; pn < nds; pn++)
            {
              InElementList[id * nds + pn] = OutElementList[el * nds + pn];

              vertices.push_back(*(nodes_begin + OutElementList[el * nds + pn] - 1).base());

              prescribed_h += (nodes_begin + OutElementList[el * nds + pn] - 1)->FastGetSolutionStepValue(NODAL_H);

              if ((nodes_begin + OutElementList[el * nds + pn] - 1)->Is(TO_REFINE))
                count_dissipative += 1;

              if ((nodes_begin + OutElementList[el * nds + pn] - 1)->Is(BOUNDARY))
              {
                count_boundary += 1;

                if ((nodes_begin + OutElementList[el * nds + pn] - 1)->Is(NEW_ENTITY))
                  count_boundary_inserted += 1;

                if ((nodes_begin + OutElementList[el * nds + pn] - 1)->SolutionStepsDataHas(CONTACT_FORCE))
                {
                  array_1d<double, 3> &ContactForceNormal = (nodes_begin + OutElementList[el * nds + pn] - 1)->FastGetSolutionStepValue(CONTACT_FORCE);
                  if (norm_2(ContactForceNormal))
                    count_contact_boundary += 1;
                }
              }
            }

            MesherData MeshData;
            RefineData& Refine = MeshData.GetRefineData(vertices, mrRemesh, rCurrentProcessInfo);

            bool refine_candidate = false;
            if (Refine.Interior.Options.Is(RefineData::REFINE))
              refine_candidate = true;

            double element_size = 0;
            double element_radius = GeometryUtilities::CalculateElementRadius(vertices, element_size);

            //calculate the prescribed h
            prescribed_h *= (1.0 / double(nds));

            double h = mrRemesh.AlphaParameter * prescribed_h;
            double element_ideal_size = 0;
            double scale_factor = 0;
            if (nds == 3)
            { //if h is the height of a equilateral triangle, the area is sqrt(3)*h*h/4
              element_ideal_size = sqrt(3.0) * 0.25 * (h * h);
              scale_factor = mrRemesh.Scale * mrRemesh.Scale;
            }

            if (nds == 4)
            { //if h is the height of a regular tetrahedron, the volume is h*h*h/(6*sqrt(2))
              element_ideal_size = (h * h * h) / (6.0 * sqrt(2.0));
              scale_factor = mrRemesh.Scale * mrRemesh.Scale * mrRemesh.Scale;
            }

            if (refine_candidate)
            {
              //********* PLASTIC POWER ENERGY REFINEMENT CRITERION (A)
              if (count_dissipative >= nds - 1)
              {
                dissipative = true;
                //Set Critical Elements
                mrRemesh.Info->ExceededThreshold = true;
              }

              //********* SIZE REFINEMENT CRITERION (B)

              double critical_size = 0.5 * factor_for_distance_inside * Refine.Interior.DistanceMeshSize;
              if (count_boundary >= nds - 1)
                critical_size = 0.5 * factor_for_distance_boundary * Refine.Boundary.DistanceMeshSize;

              if (element_radius > critical_size)
                refine_size = true;

              //std::cout<<" Element Radius "<<element_radius<<" > critial size "<<critical_size<<" refine "<<refine_size<<" (element_size: "<<element_size<<" element_ideal_size "<< element_ideal_size <<")"<<std::endl;


              //Also a criteria for the CriticalDissipation (set in nodes)
              if (Refine.Interior.Options.Is(RefineData::REFINE_ON_THRESHOLD) && Refine.Interior.Options.Is(RefineData::REFINE_ON_DISTANCE))
              {
                //********* THRESHOLD REFINEMENT CRITERION (A)
                if (dissipative == true && refine_size == true)
                {
                  InElementSizeList[id] = 0.5 * element_size * scale_factor;

                  mInfoData.refine_on_threshold += 1;
                  //std::cout<<" Area Factor Refine DISSIPATIVE :"<<InElementSizeList[id]<<std::endl;
                }
                else if (refine_size == true)
                {
                  InElementSizeList[id] = 0.5 * element_ideal_size * scale_factor;

                  if (count_boundary_inserted && count_contact_boundary)
                  {
                    InElementSizeList[id] = 0.75 * element_ideal_size * scale_factor;

                    std::cout << " count boundary inserted-contact on " << std::endl;
                  }

                  mInfoData.refine_on_size += 1;
                }
                else
                {
                  InElementSizeList[id] = 2.0 * element_size * scale_factor;
                }

              }
              else if (Refine.Interior.Options.Is(RefineData::REFINE_ON_DISTANCE))
              {
                //********* SIZE REFINEMENT CRITERION (B)
                if (refine_size == true)
                {
                  InElementSizeList[id] = 0.5 * element_size * scale_factor;

                  mInfoData.refine_on_size += 1;
                }
                else
                {
                  InElementSizeList[id] = 0.5 * element_ideal_size * scale_factor;
                }
              }
              else
              {
                InElementSizeList[id] = 2.0 * element_size * scale_factor;
              }
            }
            else
            {
              InElementSizeList[id] = 2.0 * element_size * scale_factor;
            }

            id += 1;
          }
        }

        //RESTORE THE NODAL_H ON BOUNDARY
        //*********************************************************************
        if (!mrRemesh.IsBoundary(RefineData::REFINE))
        {

          for (unsigned int i = 0; i < mrModelPart.Nodes().size(); i++)
          {
            //Unassign the NODAL_H of the Boundary nodes
            if ((nodes_begin + i)->Is(BOUNDARY))
            {
              double &nodal_h = (nodes_begin + i)->FastGetSolutionStepValue(NODAL_H);
              nodal_h *= 0.5;
            }
          }
        }
      }
      else
      {

        ModelPart::ElementsContainerType::iterator element_begin = mrModelPart.ElementsBegin();

        unsigned int nds = (*element_begin).GetGeometry().size();

        MesherData::MeshContainer &InMesh = mrRemesh.InMesh;

        unsigned int NumberOfElements = mrRemesh.Info->Current.Elements;
        InMesh.CreateElementList(NumberOfElements, nds); //number of preserved elements
        InMesh.CreateElementSizeList(NumberOfElements);

        int &OutNumberOfElements = mrRemesh.OutMesh.GetNumberOfElements();

        int *InElementList = mrRemesh.InMesh.GetElementList();
        double *InElementSizeList = mrRemesh.InMesh.GetElementSizeList();

        int *OutElementList = mrRemesh.OutMesh.GetElementList();

        ModelPart::NodesContainerType::iterator nodes_begin = mrModelPart.NodesBegin();

        double scale_factor = 0;
        if (nds == 3)
        { //if h is the height of a equilateral triangle, the area is sqrt(3)*h*h/4
          scale_factor = mrRemesh.Scale * mrRemesh.Scale;
        }
        if (nds == 4)
        { //if h is the height of a regular tetrahedron, the volume is h*h*h/(6*sqrt(2))
          scale_factor = mrRemesh.Scale * mrRemesh.Scale * mrRemesh.Scale;
        }

        for (int el = 0; el < OutNumberOfElements; el++)
        {
          if (mrRemesh.PreservedElements[el]>0)
          {
            Geometry<Node<3>> vertices;
            for (unsigned int pn = 0; pn < nds; pn++)
            {
              InElementList[id * nds + pn] = OutElementList[el * nds + pn];
              vertices.push_back(*(nodes_begin + OutElementList[el * nds + pn] - 1).base());
            }

            double element_size = 0;
            GeometryUtilities::CalculateElementRadius(vertices, element_size);

            InElementSizeList[id] = 2.0 * element_size * scale_factor;

            id++;
          }
        }
      }

      mInfoData.visited_elements = id;

    KRATOS_CATCH(" ")
    }

    ///@}
    ///@name Un accessible methods
    ///@{
    ///@}

    /// Assignment operator.
    RefineElementsOnSizeMesherProcess &operator=(RefineElementsOnSizeMesherProcess const &rOther);

    ///@}

  }; // Class Process

  ///@}
  ///@name Type Definitions
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// input stream function
  inline std::istream &operator>>(std::istream &rIStream,
                                  RefineElementsOnSizeMesherProcess &rThis);

  /// output stream function
  inline std::ostream &operator<<(std::ostream &rOStream,
                                  const RefineElementsOnSizeMesherProcess &rThis)
  {
    rThis.PrintInfo(rOStream);
    rOStream << std::endl;
    rThis.PrintData(rOStream);

    return rOStream;
  }
  ///@}

} // namespace Kratos.

#endif // KRATOS_REFINE_ELEMENTS_ON_SIZE_MEHSER_PROCESS_HPP_INCLUDED defined
