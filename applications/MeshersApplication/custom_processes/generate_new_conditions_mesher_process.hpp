//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

#if !defined(KRATOS_GENERATE_NEW_CONDITIONS_MESHER_PROCESS_HPP_INCLUDED)
#define KRATOS_GENERATE_NEW_CONDITIONS_MESHER_PROCESS_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_processes/build_model_part_boundary_process.hpp"

namespace Kratos
{

///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
 */
class GenerateNewConditionsMesherProcess
    : public BuildModelPartBoundaryProcess
{
public:
  ///@name Type Definitions
  ///@{

  typedef ModelPart::NodesContainerType NodesContainerType;
  typedef ModelPart::ElementsContainerType ElementsContainerType;
  typedef ModelPart::ConditionsContainerType ConditionsContainerType;

  typedef Node<3>::WeakPointer NodeWeakPtrType;
  typedef Element::WeakPointer ElementWeakPtrType;
  typedef Condition::WeakPointer ConditionWeakPtrType;

  typedef GlobalPointersVector<Node<3>> NodeWeakPtrVectorType;
  typedef GlobalPointersVector<Element> ElementWeakPtrVectorType;
  typedef GlobalPointersVector<Condition> ConditionWeakPtrVectorType;

  /// Pointer definition of GenerateNewConditionsMesherProcess
  KRATOS_CLASS_POINTER_DEFINITION(GenerateNewConditionsMesherProcess);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  GenerateNewConditionsMesherProcess(ModelPart &rModelPart,
                                     MesherData::MeshingParameters &rRemeshingParameters,
                                     int EchoLevel)
      : BuildModelPartBoundaryProcess(rModelPart, rModelPart.Name(), EchoLevel),
        mrRemesh(rRemeshingParameters)
  {
  }

  /// Destructor.
  virtual ~GenerateNewConditionsMesherProcess()
  {
  }

  ///@}
  ///@name Operators
  ///@{

  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  void Execute() override
  {
    KRATOS_TRY

    bool success = false;

    BuiltinTimer time_counter;

    if (mEchoLevel > 0)
      std::cout << " [ Build Boundary on ModelPart [" << mrModelPart.Name() << "] ]" << std::endl;

    success = this->UniqueSkinSearch(mrModelPart);

    //ModelPartUtilities::CheckConditions(mrModelPart);

    if (!success)
    {
      std::cout << "  ERROR:  BOUNDARY BUILD FAILED ModelPart : [" << mrModelPart << "] " << std::endl;
    }
    else
    {
      if( mEchoLevel > 0 )
        std::cout<<"  GENERATE CONDITIONS time = "<< time_counter.ElapsedSeconds() <<std::endl;
      //PrintSkin(mrModelPart);
    }

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "GenerateNewConditionsMesherProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "GenerateNewConditionsMesherProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{

  Condition::Pointer GenerateCondition(ModelPart::ConditionsContainerType &rTemporaryConditions, std::vector<int> &rPreservedConditions, std::vector<std::vector<unsigned int>> &rConditionFaces, Geometry<Node<3>> &eGeometry, DenseMatrix<unsigned int> &lpofa, DenseVector<unsigned int> &lnofa, unsigned int &iface, Properties::Pointer &rProperties, unsigned int &rConditionId) override
  {
    KRATOS_TRY

    //Get the correct ReferenceCondition
    Condition::Pointer pBoundaryCondition;
    volatile bool condition_found = false;

    // double begin_time = OpenMPUtils::GetCurrentTime();

    std::vector<unsigned int> element_face;
    for (unsigned int i = 1; i <= lnofa[iface]; ++i)
      element_face.push_back(eGeometry[lpofa(i, iface)].Id());

    // Search for existing conditions: start
    const int nconditions = static_cast<int>(rTemporaryConditions.size());
    ModelPart::ConditionsContainerType::iterator cond_begin = rTemporaryConditions.begin();
    std::vector<std::vector<unsigned int>>::iterator face_begin = rConditionFaces.begin();

#pragma omp parallel for shared(condition_found)
    for (int k = 0; k < nconditions; k++)
    {
      if (condition_found)
        continue;

      ModelPart::ConditionsContainerType::iterator i_cond = cond_begin + k;

      if (i_cond->IsNot(CONTACT) && i_cond->IsNot(TO_ERASE))
      {

        Geometry<Node<3>> &cGeometry = i_cond->GetGeometry();
        if (cGeometry.size() > 1 && rPreservedConditions[i_cond->Id()] == 0)
        {

          std::vector<std::vector<unsigned int>>::iterator condition_face = face_begin + k;

          //if(SearchUtilities::FindCondition(cGeometry,element_face)){
          if (SearchUtilities::FindCondition(*condition_face, element_face))
          {
            pBoundaryCondition = (*i_cond.base());       //accessing shared_ptr  get() to obtain the raw pointer
            rPreservedConditions[i_cond->Id()] += 1; //add each time is used
            condition_found = true;
          }
        }
      }
    }

    // Search for existing erased conditions: start
    if (!condition_found)
    {

#pragma omp parallel for shared(condition_found)
      for (int k = 0; k < nconditions; k++)
      {
        if (condition_found)
          continue;

        ModelPart::ConditionsContainerType::iterator i_cond = cond_begin + k;

        if (i_cond->IsNot(CONTACT) && i_cond->Is(TO_ERASE))
        {

          Geometry<Node<3>> &cGeometry = i_cond->GetGeometry();
          unsigned int PointsNumber = cGeometry.PointsNumber();
          if (PointsNumber > 1 && rPreservedConditions[i_cond->Id()] < PointsNumber)
          {

            std::vector<std::vector<unsigned int>>::iterator condition_face = face_begin + k;

            //if(SearchUtilities::FindNodeInCondition(cGeometry,element_face)){
            if (SearchUtilities::FindNodeInCondition(*condition_face, element_face))
            {
              pBoundaryCondition = (*i_cond.base());       //accessing shared_ptr  get() to obtain the raw pointer
              rPreservedConditions[i_cond->Id()] += 1; //add each time is used
              condition_found = true;
            }
          }
        }
      }
    }
    // Search for existing erased conditions: end

    // if( mEchoLevel >= 0 ){
    //   double end_time = OpenMPUtils::GetCurrentTime();
    //   std::cout<<"    -**- SEARCH CONDITION Time = "<<end_time-begin_time<<std::endl;
    // }

    // begin_time = OpenMPUtils::GetCurrentTime();

    //1.- create geometry: points array and geometry type
    unsigned int NumberNodesInFace = lnofa[iface];

    Condition::NodesArrayType FaceNodes;

    FaceNodes.reserve(NumberNodesInFace);

    for (unsigned int j = 1; j <= NumberNodesInFace; ++j)
    {
      FaceNodes.push_back(eGeometry(lpofa(j, iface)));
    }

    Condition::Pointer pCondition;

    //Create a condition
    if (condition_found)
    {

      pCondition = pBoundaryCondition->Clone(rConditionId, FaceNodes);

      //std::cout<<" _IDa_ "<<pCondition->Id()<<" MASTER ELEMENT "<<i_elem->Id()<<" MASTER NODE "<<eGeometry[lpofa(0,iface)].Id()<<" or "<<eGeometry[lpofa(NumberNodesInFace,iface)].Id()<<std::endl;
    }
    else
    {

      if (mEchoLevel > 1)
      {
        std::cout << "   NOT FOUND CONDITION :: CREATED-> [" << rConditionId << "] (";
        std::cout << FaceNodes[0].Id();
        for (unsigned int f = 1; f < FaceNodes.size(); ++f)
          std::cout << ", " << FaceNodes[f].Id();
        std::cout << ")" << std::endl;
      }

      //Get the standard ReferenceCondition
      const Condition &rReferenceCondition = mrRemesh.GetReferenceCondition();
      // something not implemented in geometry or condition PrintData
      //std::cout<<" ReferenceCondition "<<rReferenceCondition<<std::endl;

      pCondition = rReferenceCondition.Create(rConditionId, FaceNodes, rProperties);

      //if a condition is created new nodes must be labeled TO_REFINE
      for (unsigned int j = 0; j < FaceNodes.size(); ++j)
      {
        FaceNodes[j].Set(TO_REFINE);
      }

      // const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();
      // TransferUtilities::InitializeBoundaryData(pCondition.get(), *(mrRemesh.Transfer), rCurrentProcessInfo);

      //std::cout<<" _IDb_ "<<pCondition->Id()<<" MASTER ELEMENT "<<i_elem->Id()<<" MASTER NODE "<<eGeometry[lpofa(0,iface)].Id()<<" or "<<eGeometry[lpofa(NumberNodesInFace,iface)].Id()<<std::endl;
    }

    // if( mEchoLevel > 0 ){
    //   double end_time = OpenMPUtils::GetCurrentTime();
    //   std::cout<<"    -**- CREATE CONDITION Time = "<<end_time-begin_time<<std::endl;
    // }

    ++rConditionId;

    return pCondition;

    KRATOS_CATCH("")
  }

  bool CheckAcceptedCondition(ModelPart &rModelPart, Condition &rCondition) override
  {
    KRATOS_TRY

    bool node_not_preserved = false;
    bool condition_not_preserved = false;

    Geometry<Node<3>> &cGeometry = rCondition.GetGeometry();

    for (unsigned int j = 0; j < cGeometry.size(); ++j)
    {
      if (cGeometry[j].Is(TO_ERASE) || cGeometry[j].Is(TO_REFINE))
        node_not_preserved = true;

      if (cGeometry[j].Is(ISOLATED) || cGeometry[j].IsNot(BOUNDARY))
        condition_not_preserved = true;
    }

    if (rCondition.Is(TO_ERASE))
      condition_not_preserved = true;

    if (rCondition.Is(BOUNDARY)) //flag for composite condition
      condition_not_preserved = true;

    if (node_not_preserved == true || condition_not_preserved == true)
      return false;
    else
      return true;

    KRATOS_CATCH("")
  }

  void AddConditionToModelPart(ModelPart &rModelPart, Condition::Pointer pCondition) override
  {
    KRATOS_TRY

    //rModelPart.AddCondition(pCondition); //if a Local Id corresponds to a Global Id not added
    rModelPart.Conditions().push_back(pCondition);

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Protected Operations
  ///@{
  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{

  MesherData::MeshingParameters &mrRemesh;

  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{

  /// Assignment operator.
  GenerateNewConditionsMesherProcess &operator=(GenerateNewConditionsMesherProcess const &rOther);

  /// Copy constructor.
  //GenerateNewConditionsMesherProcess(GenerateNewConditionsMesherProcess const& rOther);

  ///@}

}; // Class GenerateNewConditionsMesherProcess

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                GenerateNewConditionsMesherProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const GenerateNewConditionsMesherProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_GENERATE_NEW_CONDITIONS_MESHER_PROCESS_HPP_INCLUDED  defined
