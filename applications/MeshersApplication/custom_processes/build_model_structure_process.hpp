//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

#if !defined(KRATOS_BUILD_MODEL_STRUCTURE_PROCESS_HPP_INCLUDED)
#define KRATOS_BUILD_MODEL_STRUCTURE_PROCESS_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_processes/nodal_neighbours_search_process.hpp"
#include "custom_processes/build_model_part_boundary_process.hpp"
#include "custom_processes/set_flags_to_entities_process.hpp"
#include "utilities/entities_utilities.h"

namespace Kratos
{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{
typedef ModelPart::ConditionType ConditionType;
typedef ModelPart::NodesContainerType NodesContainerType;
typedef ModelPart::ElementsContainerType ElementsContainerType;
typedef ModelPart::ConditionsContainerType ConditionsContainerType;
typedef ConditionType::GeometryType GeometryType;
typedef GeometryType::PointsArrayType PointsArrayType;

typedef PointerVectorSet<ConditionType, IndexedObject> ConditionsContainerType;
typedef ConditionsContainerType::iterator ConditionIterator;
typedef ConditionsContainerType::const_iterator ConditionConstantIterator;

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
 */
class BuildModelStructureProcess
    : public Process
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of BuildModelStructureProcess
  KRATOS_CLASS_POINTER_DEFINITION(BuildModelStructureProcess);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  BuildModelStructureProcess(ModelPart &rModelPart, Flags Options, int EchoLevel = 0)
      : mrModelPart(rModelPart)
  {
    mOptions = Options;
    mEchoLevel = EchoLevel;
  }

  /// Destructor.
  virtual ~BuildModelStructureProcess()
  {
  }

  ///@}
  ///@name Operators
  ///@{

  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  /// this function is designed for being called at the beginning of the computations
  /// right after reading the model and the groups
  void ExecuteInitialize() override
  {
    KRATOS_TRY

    // Sort Conditions (contact conditions where renumerated to not coincide with element ids)
    ModelPartUtilities::SortConditions(mrModelPart);

    // Initialize Flags
    SetFlagsToEntitiesProcess AssignFlags(mrModelPart, mEchoLevel);
    AssignFlags.ExecuteInitialize();

    KRATOS_CATCH("")
  }

  /// this function is designed for being called at the end of the computations
  /// right after reading the model and the groups
  void ExecuteFinalize() override
  {
    KRATOS_TRY

    BuiltinTimer time_counter;

    // Build all modelparts
    this->BuildModelPartsStructure(mrModelPart, mEchoLevel);

    // Set boundary normals, neighbours, masters and slave entities
    this->SearchNeighbours(mrModelPart, mEchoLevel);

    // Assign Flags to nodes and elements
    SetFlagsToEntitiesProcess AssignFlags(mrModelPart, mEchoLevel);
    AssignFlags.ExecuteFinalize();

    this->AssignModelPartVariables(mrModelPart, mEchoLevel);

    if (mEchoLevel > 0)
      std::cout<<"  BUILD MODEL STRUCTURE time = "<< time_counter.ElapsedSeconds() <<std::endl;

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "BuildModelStructureProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "BuildModelStructureProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{

  ModelPart &mrModelPart;

  Flags mOptions;

  int mEchoLevel;

  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{

  //*******************************************************************************************
  //*******************************************************************************************

  virtual void SearchNeighbours(ModelPart &rModelPart, int EchoLevel)
  {
    KRATOS_TRY

    //NODAL NEIGHBOURS SEARCH
    NodalNeighboursSearchProcess FindNeighbours(rModelPart);
    FindNeighbours.Execute();

    //CONDITIONS PRIMARY_ELEMENTS and PRIMARY_NODES SEARCH
    BuildModelPartBoundaryProcess BuildBoundaryProcess(rModelPart, rModelPart.Name(), EchoLevel);
    BuildBoundaryProcess.SearchConditionMasters();


    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  virtual void AssignModelPartVariables(ModelPart &rModelPart, int EchoLevel)
  {
    KRATOS_TRY

    //BOUNDARY NORMALS SEARCH and SHRINKAGE FACTOR
    BoundaryNormalsUtilities BoundaryNormals;
    BoundaryNormals.CalculateWeightedBoundaryNormals(rModelPart, EchoLevel);

    //NODAL_H SEARCH (commented to not update NODAL_H)
    //FindNodalHProcess FindNodalH(mrModelPart);
    //FindNodalH.Execute();

    // Set modelpart names to entities
    ModelPartUtilities::SetModelPartNameToEntities(mrModelPart,"Nodes");

    // Renumerate contact conditions to not coincide with element ids
    ModelPartUtilities::SetUniqueIdContactConditions(mrModelPart);

    // Clean contact forces inside domain nodes
    this->CleanContactForces(mrModelPart);

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  virtual void BuildModelPartsStructure(ModelPart &rModelPart, int EchoLevel)
  {

    KRATOS_TRY

    if (EchoLevel > 0)
      std::cout << "   [ START MODEL PART [" << rModelPart.Name() << "] [Elems=:" << rModelPart.NumberOfElements() << "|Nodes=" << rModelPart.NumberOfNodes() << "|Conds=" << rModelPart.NumberOfConditions() << "] ] " << std::endl;

    //0.- clean elements and conditions (they remain in submodelparts)
    rModelPart.Nodes().clear();
    rModelPart.Elements().clear();

    ModelPart::ConditionsContainerType temporal_conditions;

    unsigned int node_id = 1, element_id = 1, condition_id = 1;

    //1.- build domain model parts
    this->BuildDomainModelParts(rModelPart, temporal_conditions, node_id, element_id, condition_id);

    //2.- build boundary model parts
    this->BuildBoundaryModelParts(rModelPart, temporal_conditions, node_id, element_id, condition_id);

    //3.- build contact model part
    this->BuildContactModelParts(rModelPart, temporal_conditions, node_id, element_id, condition_id);

    //4.- set preserved conditions by swapping
    rModelPart.Conditions().swap(temporal_conditions);

    //5.- build solving domain (get from other submodelparts)
    this->BuildComputingDomain(rModelPart, EchoLevel);

    //6.- reorder and optimize model part
    ModelPartUtilities::ReorderAndOptimizeModelPart(rModelPart);

    //7.- finalize solution step (no implex active, set internal variables implicitly after remeshing) it makes solid computation with IMPLEX estable but introduces smoothing to the variables.
    // for (auto &i_mp : rModelPart.SubModelParts())
    // {
    //   if (i_mp.Is(SOLID))
    // 	EntitiesUtilities::FinalizeSolutionStepEntities<Element>(i_mp);
    // }

    // for (auto &i_node : rModelPart.Nodes())
    //   if (i_node.Is(ISOLATED))
    // 	std::cout<<" ISOLATED node ID "<<i_node.Id()<<" of "<<rModelPart.NumberOfNodes()<<std::endl;

    if (EchoLevel > 0)
      std::cout << "   [ END MODEL PART [" << rModelPart.Name() << "] [Elems=:" << rModelPart.NumberOfElements() << "|Nodes=" << rModelPart.NumberOfNodes() << "|Conds=" << rModelPart.NumberOfConditions() << "] ] " << std::endl;

    KRATOS_CATCH("")
  }


  //*******************************************************************************************
  //*******************************************************************************************
  //this is not in the correct place
  void CleanContactForces(ModelPart &rModelPart)
  {
    const array_1d<double, 3> ZeroNormal(3, 0.0);

    for (auto &i_mp : rModelPart.SubModelParts())
    {
      if (this->IsDomainModelPart(i_mp))
      {
        block_for_each(rModelPart.Nodes(),[&](NodeType &i_node){
          if (i_node.IsNot(BOUNDARY))
            if (i_node.SolutionStepsDataHas(CONTACT_FORCE))
              noalias(i_node.GetSolutionStepValue(CONTACT_FORCE)) = ZeroNormal;
        });
      }
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool IsDomainModelPart(ModelPart &rModelPart)
  {
    //only the domains (no computing, no boundary)
    bool domain = false;
    if (rModelPart.AndNot({ACTIVE,BOUNDARY}))
    {
      if (rModelPart.Or({SOLID,FLUID,RIGID,CONTACT.AsFalse()}))
        domain = true;
    }
    return domain;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void BuildDomainModelParts(ModelPart &rModelPart, ModelPart::ConditionsContainerType &rConditions, unsigned int &rNodeId, unsigned int &rElemId, unsigned int &rCondId)
  {
    KRATOS_TRY

    for (auto &i_mp : rModelPart.SubModelParts())
    {
      if (this->IsDomainModelPart(i_mp))
      {
        if (mEchoLevel > 0)
          std::cout << "    [ SUBMODEL PART [" << i_mp.Name() << "] [Elems=" << i_mp.NumberOfElements() << "|Nodes=" << i_mp.NumberOfNodes() << "|Conds=" << i_mp.NumberOfConditions() << "] ";

        //Clean Nodes when redefining the main model part:
        ModelPart::NodesContainerType temporal_nodes;
        temporal_nodes.reserve(i_mp.Nodes().size());
        temporal_nodes.swap(i_mp.Nodes());

        for (ModelPart::ElementsContainerType::iterator i_elem = i_mp.ElementsBegin(); i_elem != i_mp.ElementsEnd(); ++i_elem)
        {
          if (i_elem->IsNot(TO_ERASE))
          {
            (rModelPart.Elements()).push_back(*(i_elem.base()));
            rModelPart.Elements().back().SetId(rElemId);
            ++rElemId;
          }
        }

        for (ModelPart::NodesContainerType::iterator i_node = temporal_nodes.begin(); i_node != temporal_nodes.end(); ++i_node)
        {
          if (i_node->IsNot(TO_ERASE))
          {
            (i_mp.Nodes()).push_back(*(i_node.base()));
            (rModelPart.Nodes()).push_back(*(i_node.base()));
            rModelPart.Nodes().back().SetId(rNodeId);
            ++rNodeId;
          }
        }
        for (ModelPart::ConditionsContainerType::iterator i_cond = i_mp.ConditionsBegin(); i_cond != i_mp.ConditionsEnd(); ++i_cond)
        {
          if (i_cond->IsNot(TO_ERASE))
          {
            rConditions.push_back(*(i_cond.base()));
            rConditions.back().SetId(rCondId);
            rCondId += 1;
          }
        }


        if (mEchoLevel > 0)
          std::cout << " / [Elems=" << i_mp.NumberOfElements() << "|Nodes=" << i_mp.NumberOfNodes() << "|Conds=" << i_mp.NumberOfConditions() << "] ] " << std::endl;
      }
    }

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void SetConditionsToBoundaryModelParts(ModelPart &rModelPart, ModelPart::ConditionsContainerType &rConditions)
  {
    // Check conditions size and add only child conditions to boundary model parts
    if (rConditions.size() > 0)
    {
      //Clear conditions
      for (auto& i_mp : rModelPart.SubModelParts())
      {
        if (i_mp.Is(BOUNDARY)) // Neumann or Dirichled sub model parts
        {
          if (mEchoLevel > 0)
            std::cout << "    [ SUBMODEL PART [" << i_mp.Name() << "]  initial [Elems=" << i_mp.NumberOfElements() << "|Nodes=" << i_mp.NumberOfNodes() << "|Conds=" << i_mp.NumberOfConditions() << "] ]" << std::endl;

          i_mp.Conditions().clear();
        }
      }

      //Add child conditions using the stored MODEL_PART pointer variable
      for (auto& i_cond : rConditions)
      {
        ConditionsContainerType &ChildrenConditions = i_cond.GetValue(CHILDREN_CONDITIONS);

        //this conditions are cloned, then the id has no coherence, must be renumbered at the end of the assignation
        for (ConditionConstantIterator cn = ChildrenConditions.begin(); cn != ChildrenConditions.end(); ++cn)
        {
          ModelPart& i_mp = rModelPart.GetSubModelPart(cn->GetValue(MODEL_PART_NAME));
          i_mp.Conditions().push_back(*(cn.base()));

          if (i_cond.Is(NEW_ENTITY))
          {
            GeometryType& rGeometry = i_cond.GetGeometry();
            for (unsigned int i=0; i<rGeometry.size(); ++i)
            {
              if (rGeometry[i].Is(NEW_ENTITY))
                (i_mp.Nodes()).push_back(rGeometry(i));
            }

          }

        }
      }
    }

  }


  //*******************************************************************************************
  //*******************************************************************************************

  void SetNodesToBoundaryModelParts(ModelPart &rModelPart)
  {
    for (auto &i_mp : rModelPart.SubModelParts())
    {
      if (i_mp.IsNot({BOUNDARY,ACTIVE,RIGID})) // domain sub model parts
      {
        for (ModelPart::NodesContainerType::iterator i_node = i_mp.NodesBegin(); i_node != i_mp.NodesEnd(); ++i_node)
        {
          if (i_node->Is(BOUNDARY) && i_node->Is(NEW_ENTITY))
          {
            for (auto& i_name : i_node->GetValue(MODEL_PART_NAMES))
            {
              ModelPart& j_mp = rModelPart.GetSubModelPart(i_name);
              j_mp.Nodes().push_back(*(i_node.base()));
            }
          }
        }
      }
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void RemoveNodes(ModelPart &rModelPart, Flags Flag)
  {
    //clean erased nodes from boundary model parts
    for (auto& i_mp : rModelPart.SubModelParts())
    {
      if (i_mp.Is(BOUNDARY))  //boundary model part
      {
        ModelPart::NodesContainerType swap_nodes;
        swap_nodes.reserve(i_mp.Nodes().size());
        swap_nodes.swap(i_mp.Nodes());

        for (ModelPart::NodesContainerType::iterator i_node = swap_nodes.begin(); i_node != swap_nodes.end(); ++i_node)
          if (i_node->IsNot(Flag))
            (i_mp.Nodes()).push_back(*(i_node.base()));
      }
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void BuildBoundaryModelParts(ModelPart &rModelPart, ModelPart::ConditionsContainerType &rConditions, unsigned int &rNodeId, unsigned int &rElemId, unsigned int &rCondId)
  {

    KRATOS_TRY

    // Set Preserved conditions (and new nodes) to Boundary Sub Model Parts // Neumann sub model parts
    this->SetConditionsToBoundaryModelParts(rModelPart, rConditions);

    // Set New Nodes (no conditions) to Boundary Sub Model Parts  // Dirichlet sub model parts
    this->SetNodesToBoundaryModelParts(rModelPart);

    //reset NEW_ENTITY in conditions
    // for (auto& i_cond : rConditions)
    //   i_cond.Set(NEW_ENTITY, false);

    //clean erased nodes from boundary model parts
    this->RemoveNodes(rModelPart,TO_ERASE);

    //add boundary model part conditions to preserved conditions (child conditions)
    for (auto& i_mp : rModelPart.SubModelParts())
    {
      if (i_mp.Is(BOUNDARY)) //boundary model part
      {
        for (ModelPart::ConditionsContainerType::iterator i_cond = i_mp.ConditionsBegin(); i_cond != i_mp.ConditionsEnd(); ++i_cond)
        {
          if (i_cond->IsNot(TO_ERASE))
          {
            // i_cond->Set(NEW_ENTITY, false); //reset here if the condition is inserted
            rConditions.push_back(*(i_cond.base()));
            rConditions.back().SetId(rCondId);
            rCondId += 1;
          }
        }

        if (mEchoLevel > 0)
          std::cout << "    [ SUBMODEL PART [" << i_mp.Name() << "]  final [Elems=" << i_mp.NumberOfElements() << "|Nodes=" << i_mp.NumberOfNodes() << "|Conds=" << i_mp.NumberOfConditions() << "] ] " << std::endl;
      }
    }

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void BuildContactModelParts(ModelPart &rModelPart, ModelPart::ConditionsContainerType &rConditions, unsigned int &rNodeId, unsigned int &rElemId, unsigned int &rCondId)
  {
    KRATOS_TRY

    //Add Contact modelparts to main modelpart  flags: ( CONTACT ) in contact model parts keep only nodes and contact conditions
    // after that a contact search will be needed

    //if contact condition has the same geometry size as an elements printing ids will coincide,
    //renumber conditions with rElemId instead of rCondId :: in order to ensure it check maximun and apply it
    unsigned int rContactId = rCondId;
    if (rElemId > rCondId)
      rContactId = rElemId;

    for (auto &i_mp : rModelPart.SubModelParts())
    {
      if (i_mp.Is(CONTACT))
      { //keep only contact conditions

        if (mEchoLevel > 0)
          std::cout << "    [ SUBMODEL PART [" << i_mp.Name() << "] [Elems=" << i_mp.NumberOfElements() << "|Nodes=" << i_mp.NumberOfNodes() << "|Conds=" << i_mp.NumberOfConditions() << "] ";

        i_mp.Elements().clear();

        //Clean Nodes when redefining the main model part:
        ModelPart::NodesContainerType temporal_nodes;
        temporal_nodes.reserve(i_mp.Nodes().size());
        temporal_nodes.swap(i_mp.Nodes());

        for (ModelPart::NodesContainerType::iterator i_node = temporal_nodes.begin(); i_node != temporal_nodes.end(); ++i_node)
        {
          if (i_node->IsNot(TO_ERASE))
          {
            (i_mp.Nodes()).push_back(*(i_node.base()));
          }
        }

        ModelPart::ConditionsContainerType temporal_conditions;
        temporal_conditions.reserve(i_mp.Conditions().size());
        temporal_conditions.swap(i_mp.Conditions());

        for (ModelPart::ConditionsContainerType::iterator i_cond = temporal_conditions.begin(); i_cond != temporal_conditions.end(); ++i_cond)
        {
          if (i_cond->Is(CONTACT))
          { //keep only contact conditions
            if (i_cond->IsNot(TO_ERASE))
            { //it can not be to erase
              (i_mp.Conditions()).push_back(*(i_cond.base()));
              rConditions.push_back(*(i_cond.base()));
              rConditions.back().SetId(rContactId);
              rContactId += 1;
            }
          }
        }

        if (mEchoLevel > 0)
          std::cout << " / [Elems=" << i_mp.NumberOfElements() << "|Nodes=" << i_mp.NumberOfNodes() << "|Conds=" << i_mp.NumberOfConditions() << "] ] " << std::endl;
      }
    }

    if (rElemId > rCondId)
      rElemId = rContactId;
    else
      rCondId = rContactId;

    KRATOS_CATCH("")
  }


  //*******************************************************************************************
  //*******************************************************************************************

  virtual void BuildComputingDomain(ModelPart &rModelPart, int EchoLevel)
  {
    KRATOS_TRY

    std::string ComputingModelPartName;

    for (auto &i_mp : rModelPart.SubModelParts())
    {
      if (i_mp.Is(ACTIVE) && i_mp.IsNot(THERMAL))
      { //computing_domain
        ComputingModelPartName = i_mp.Name();
      }
    }

    ModelPart &rComputingModelPart = rModelPart.GetSubModelPart(ComputingModelPartName);

    rComputingModelPart.Nodes().clear();
    rComputingModelPart.Elements().clear();
    rComputingModelPart.Conditions().clear();

    //std::cout<<" Computing Domain (Nodes:"<<rComputingModelPart.Nodes().size()<<",Elements:"<<rComputingModelPart.Elements().size()<<",Conditions:"<<rComputingModelPart.Conditions().size()<<")"<<std::endl;

    //add all needed computing entities (elements, nodes, conditions)

    for (auto &i_mp : rModelPart.SubModelParts())
    {
      if ((i_mp.AndNot({BOUNDARY,ACTIVE}) || i_mp.Is(RIGID)) && i_mp.IsNot(CONTACT))
      {
        //std::cout<<" Adding model part entities to ComputingDomain "<<i_mp.Name()<<"(Rigid:"<<i_mp.Is(RIGID)<<", Boundary:"<<i_mp.Is(BOUNDARY)<<", Active: "<<i_mp.Is(ACTIVE)<<", Contact: "<<i_mp.Is(CONTACT)<<")"<<std::endl;

        for (ModelPart::NodesContainerType::iterator i_node = i_mp.NodesBegin(); i_node != i_mp.NodesEnd(); ++i_node)
        {
          (rComputingModelPart.Nodes()).push_back(*(i_node.base()));
        }

        for (ModelPart::ConditionsContainerType::iterator i_cond = i_mp.ConditionsBegin(); i_cond != i_mp.ConditionsEnd(); ++i_cond)
        {
          (rComputingModelPart.Conditions()).push_back(*(i_cond.base()));
        }

        if (i_mp.Is(RIGID))
        {
          for (ModelPart::ElementsContainerType::iterator i_elem = i_mp.ElementsBegin(); i_elem != i_mp.ElementsEnd(); ++i_elem)
          {
            // check distorted element
            if (this->CheckRigidElementDistortion(*i_elem))
              (rComputingModelPart.Elements()).push_back(*(i_elem.base()));
          }
        }
        else
        {
          for (ModelPart::ElementsContainerType::iterator i_elem = i_mp.ElementsBegin(); i_elem != i_mp.ElementsEnd(); ++i_elem)
          {
            (rComputingModelPart.Elements()).push_back(*(i_elem.base()));
          }
        }
      }
    }

    //add all contact conditions
    for (auto &i_mp : rModelPart.SubModelParts())
    {
      if (i_mp.Is(CONTACT))
      {
        for (ModelPart::ConditionsContainerType::iterator i_cond = i_mp.ConditionsBegin(); i_cond != i_mp.ConditionsEnd(); ++i_cond)
        {
          if (i_cond->Is(CONTACT))
            (rComputingModelPart.Conditions()).push_back(*(i_cond.base()));
        }
      }
    }

    // Unique (sort included)
    rComputingModelPart.Nodes().Unique();

    //std::cout<<" Computing Domain Build (Nodes:"<<rComputingModelPart.Nodes().size()<<",Elements:"<<rComputingModelPart.Elements().size()<<",Conditions:"<<rComputingModelPart.Conditions().size()<<")"<<std::endl;

    if (EchoLevel > 0)
      std::cout << "    [ SUBMODEL PART [" << rComputingModelPart.Name() << "] [Elems=" << rComputingModelPart.NumberOfElements() << "|Nodes=" << rComputingModelPart.NumberOfNodes() << "|Conds=" << rComputingModelPart.NumberOfConditions() << "] ] " << std::endl;

    KRATOS_CATCH("")
  }


  //**************************************************************************
  //**************************************************************************

  virtual bool CheckRigidElementDistortion(Element &rElement)
  {
    KRATOS_TRY

    GeometryType &rElementGeometry = rElement.GetGeometry();

    unsigned int rigid = 0;
    unsigned int moving = 0;
    for (unsigned int i = 0; i < rElementGeometry.size(); ++i)
    {
      if (rElementGeometry[i].Is(RIGID))
      {
        ++rigid;
        const array_1d<double, 3> &movement = rElementGeometry[i].FastGetSolutionStepValue(DISPLACEMENT);
        for (unsigned int j = 0; j < 3; ++j)
        {
          if (movement[j] != 0)
            ++moving;
        }
      }
    }

    if (moving > 0 && rigid == rElementGeometry.size())
    {
      double CurrentSize = rElementGeometry.DomainSize();
      std::vector<array_1d<double, 3>> CurrentCoordinates;
      for (unsigned int i = 0; i < rigid; ++i)
      {
        CurrentCoordinates.push_back(rElementGeometry[i].Coordinates());
        rElementGeometry[i].Coordinates() = rElementGeometry[i].GetInitialPosition();
      }

      double OriginalSize = rElementGeometry.DomainSize();
      for (unsigned int i = 0; i < rigid; ++i)
      {
        rElementGeometry[i].Coordinates() = CurrentCoordinates[i];
      }

      if ((CurrentSize > OriginalSize + 1e-3 * OriginalSize) || (CurrentSize < OriginalSize - 1e-3 * OriginalSize))
      {
        KRATOS_WARNING("")<<" Rigid Element motion -> Discarted "<<std::endl;
        return false;
      }
    }

    return true;

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{

  /// Assignment operator.
  BuildModelStructureProcess &operator=(BuildModelStructureProcess const &rOther);

  /// Copy constructor.
  //BuildModelStructureProcess(BuildModelStructureProcess const& rOther);

  ///@}

}; // Class BuildModelStructureProcess

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                BuildModelStructureProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const BuildModelStructureProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_BUILD_MODEL_STRUCTURE_PROCESS_HPP_INCLUDED  defined
