# Tetgen automatic download
# Variables:  TRIANGLE_DIR

unset(TRIANGLE_FOUND CACHE)
find_path(TRIANGLE_FOUND NAMES triangle.h HINTS ${TRIANGLE_DIR})

if(TRIANGLE_FOUND)
  message(STATUS "Triangle found in ${TRIANGLE_DIR}")
else(TRIANGLE_FOUND)
  message(STATUS "TRIANGLE_DIR not defined: ${TRIANGLE_DIR}")

  # Default version
  set(triangle_install_dir "${EXTERNAL_LIBRARIES_DIR}")

  # Default TRIANGLE_DIR
  set(TRIANGLE_DIR "${triangle_install_dir}/triangle")
  message(STATUS "set TRIANGLE_DIR: ${TRIANGLE_DIR}")

  set(triangle_file_name "triangle-1.6.0-kratos")
  set(triangle_packed_file "${triangle_file_name}.zip")
  set(triangle_packed_dir "${triangle_install_dir}/${triangle_packed_file}")

  set(md5_triangle 848957c8ba0de1c1a37d72b96770a92c)

  if(EXISTS ${triangle_packed_dir})
    file(MD5 ${triangle_packed_dir} md5_number)
    message(STATUS "MD5: ${md5_number} vs ${md5_triangle}")
  endif(EXISTS ${triangle_packed_dir})

  if(NOT ${md5_number} STREQUAL ${md5_triangle})
    message(STATUS "${triangle_packed_file} invalid")
    file(REMOVE ${triangle_packed_dir})
  endif(NOT ${md5_number} STREQUAL ${md5_triangle})

  if(NOT EXISTS ${triangle_packed_dir})
    message(STATUS "${triangle_packed_file} not found in ${triangle_install_dir}")
    message(STATUS "Downloading ${triangle_packed_file} from https://github.com/PFEM/triangle-1.6.0/archive/kratos.zip to ${triangle_install_dir} ...")
    file(DOWNLOAD https://github.com/PFEM/triangle-1.6.0/archive/kratos.zip ${triangle_packed_dir} SHOW_PROGRESS EXPECTED_MD5 ${md5_triangle})
    message(STATUS "${triangle_packed_file} downloaded")
  endif(NOT EXISTS ${triangle_packed_dir})

  find_path(TRIANGLE_FOUND NAMES triangle.h HINTS ${TRIANGLE_DIR})

  if((NOT EXISTS ${TRIANGLE_DIR}) OR (NOT TRIANGLE_FOUND))
    message(STATUS "Unpacking ${triangle_packed_file} in ${triangle_install_dir} ...")
    execute_process(COMMAND ${CMAKE_COMMAND} -E tar xzf ${triangle_packed_file}
      WORKING_DIRECTORY ${triangle_install_dir})
    execute_process(COMMAND ${CMAKE_COMMAND} -E copy_directory ${triangle_install_dir}/${triangle_file_name} ${TRIANGLE_DIR}
      WORKING_DIRECTORY ${triangle_install_dir})
    execute_process(COMMAND ${CMAKE_COMMAND} -E remove_directory ${triangle_install_dir}/${triangle_file_name}
      WORKING_DIRECTORY ${triangle_install_dir})
  endif((NOT EXISTS ${TRIANGLE_DIR}) OR (NOT TRIANGLE_FOUND))
endif(TRIANGLE_FOUND)
