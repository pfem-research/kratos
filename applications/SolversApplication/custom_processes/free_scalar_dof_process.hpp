//
//   Project Name:        KratosSolversApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:       August 2016 $
//
//

#if !defined(KRATOS_FREE_SCALAR_DOF_PROCESS_HPP_INCLUDED)
#define KRATOS_FREE_SCALAR_DOF_PROCESS_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_processes/fix_scalar_dof_process.hpp"

namespace Kratos
{

///@name Kratos Classes
///@{

/// The base class for freeing scalar variable Dof or array_1d component Dof processes in Kratos.
/** This function free the variable dof belonging to all of the nodes in a given mesh
*/
class FreeScalarDofProcess : public FixScalarDofProcess
{
public:
    ///@name Type Definitions
    ///@{

    /// Pointer definition of FreeScalarDofProcess
    KRATOS_CLASS_POINTER_DEFINITION(FreeScalarDofProcess);

    ///@}
    ///@name Life Cycle
    ///@{
    FreeScalarDofProcess(ModelPart &model_part,
                         Parameters rParameters) : FixScalarDofProcess(model_part, rParameters)
    {
    }

    FreeScalarDofProcess(ModelPart &model_part,
                         const Variable<double> &rVariable,
                         std::vector<Flags> FlagsList = std::vector<Flags>()) : FixScalarDofProcess(model_part, rVariable, FlagsList)
    {
    }

    FreeScalarDofProcess(ModelPart &model_part,
                         const Variable<int> &rVariable,
                         std::vector<Flags> FlagsList = std::vector<Flags>()) : FixScalarDofProcess(model_part, rVariable, FlagsList)
    {
    }

    FreeScalarDofProcess(ModelPart &model_part,
                         const Variable<bool> &rVariable,
                         std::vector<Flags> FlagsList = std::vector<Flags>()) : FixScalarDofProcess(model_part, rVariable, FlagsList)
    {
    }

    /// Copy constructor.
    FreeScalarDofProcess(FreeScalarDofProcess const &rOther)
        : FixScalarDofProcess(rOther)
    {
    }

    /// Destructor.
    ~FreeScalarDofProcess() override {}

    ///@}
    ///@name Operators
    ///@{

    /// This operator is provided to call the process as a function and simply calls the Execute method.
    void operator()()
    {
        Execute();
    }

    ///@}
    ///@name Operations
    ///@{

    /// Execute method is used to execute the FreeScalarDofProcess algorithms.
    void Execute() override
    {

        KRATOS_TRY;

        if (KratosComponents<Variable<double>>::Has(mvariable_name)) //case of double variable
        {
          this->InternalFreeDof<>(KratosComponents<Variable<double>>::Get(mvariable_name));
        }
        else
        {
            KRATOS_THROW_ERROR(std::logic_error, "Not able to set the variable. Attempting to set variable:", mvariable_name);
        }

        KRATOS_CATCH("");
    }

    /// this function is designed for being called at the beginning of the computations
    /// right after reading the model and the groups
    void ExecuteInitialize() override
    {
    }

    /// this function is designed for being execute once before the solution loop but after all of the
    /// solvers where built
    void ExecuteBeforeSolutionLoop() override
    {
    }

    /// this function will be executed at every time step BEFORE performing the solve phase
    void ExecuteInitializeSolutionStep() override
    {
    }

    /// this function will be executed at every time step AFTER performing the solve phase
    void ExecuteFinalizeSolutionStep() override
    {
    }

    /// this function will be executed at every time step BEFORE  writing the output
    void ExecuteBeforeOutputStep() override
    {
    }

    /// this function will be executed at every time step AFTER writing the output
    void ExecuteAfterOutputStep() override
    {
    }

    /// this function is designed for being called at the end of the computations
    /// right after reading the model and the groups
    void ExecuteFinalize() override
    {
    }

    ///@}
    ///@name Access
    ///@{

    ///@}
    ///@name Inquiry
    ///@{

    ///@}
    ///@name Input and output
    ///@{

    /// Turn back information as a string.
    std::string Info() const override
    {
        return "FreeScalarDofProcess";
    }

    /// Print information about this object.
    void PrintInfo(std::ostream &rOStream) const override
    {
        rOStream << "FreeScalarDofProcess";
    }

    /// Print object's data.
    void PrintData(std::ostream &rOStream) const override
    {
    }

    ///@}
    ///@name Friends
    ///@{
    ///@}

protected:
    ///@name Protected static Member Variables
    ///@{
    ///@}
    ///@name Protected member Variables
    ///@{
    ///@}
    ///@name Protected Operators
    ///@{
    ///@}
    ///@name Protected Operations
    ///@{
    ///@}
    ///@name Protected  Access
    ///@{
    ///@}
    ///@name Protected Inquiry
    ///@{
    ///@}
    ///@name Protected LifeCycle
    ///@{
    ///@}

private:
    ///@name Static Member Variables
    ///@{
    ///@}
    ///@name Member Variables
    ///@{
    ///@}
    ///@name Private Operators
    ///@{
    ///@}
    ///@name Private Operations
    ///@{
    ///@}
    ///@name Private  Access
    ///@{

    /// Assignment operator.
    FreeScalarDofProcess &operator=(FreeScalarDofProcess const &rOther);

    ///@}
    ///@name Serialization
    ///@{
    ///@name Private Inquiry
    ///@{
    ///@}
    ///@name Un accessible methods
    ///@{
    ///@}

}; // Class FreeScalarDofProcess

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                FreeScalarDofProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const FreeScalarDofProcess &rThis)
{
    rThis.PrintInfo(rOStream);
    rOStream << std::endl;
    rThis.PrintData(rOStream);

    return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_FREE_SCALAR_DOF_PROCESS_HPP_INCLUDED  defined
