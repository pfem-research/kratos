//
//   Project Name:        KratosSolversApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:      January 2021 $
//
//

#if !defined(KRATOS_ASSIGN_LOCAL_CONSTRAINT_SOLVER_PROCESS_HPP_INCLUDED)
#define KRATOS_ASSIGN_LOCAL_CONSTRAINT_SOLVER_PROCESS_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_processes/solver_process.hpp"
#include "custom_processes/free_scalar_dof_process.hpp"
#include "utilities/beam_math_utilities.hpp"
#include "custom_utilities/time_interval.hpp"
#include "custom_utilities/function_value.hpp"
#include "custom_constraints/local_constraint.hpp"
#include "custom_solvers/time_integration_methods/time_integration_method.hpp"

namespace Kratos
{

///@name Kratos Classes
///@{

/// The base class for assigning a value to scalar variables or array_1d components processes in Kratos.
/** This function assigns a value to a variable belonging to all of the nodes in a given mesh
*/
class AssignLocalConstraintSolverProcess : public SolverProcess
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of AssignLocalConstraintSolverProcess
  KRATOS_CLASS_POINTER_DEFINITION(AssignLocalConstraintSolverProcess);

  /// KratosVariables
  typedef array_1d<double, 3> ValueData;
  typedef const Variable<ValueData> *VariablePointer;
  typedef Quaternion<double> QuaternionType;

  typedef ModelPart::NodesContainerType NodesContainerType;
  typedef ModelPart::ElementsContainerType ElementsContainerType;
  typedef ModelPart::ConditionsContainerType ConditionsContainerType;

  typedef Element::WeakPointer ElementWeakPtrType;

  typedef GlobalPointersVector<Node<3>> NodeWeakPtrVectorType;
  typedef GlobalPointersVector<Element> ElementWeakPtrVectorType;

  typedef Variable<double> VariableScalarType;

  typedef TimeIntegrationMethod<VariableScalarType, double> TimeIntegrationMethodScalarType;

  typedef TimeIntegrationMethodScalarType::Pointer ScalarIntegrationPointer;

public:

  ///@}
  ///@name Life Cycle
  ///@{

  AssignLocalConstraintSolverProcess(ModelPart &model_part) : SolverProcess(Flags()), mrModelPart(model_part), mLocalConstraint(mConstraintData) {}

  AssignLocalConstraintSolverProcess(ModelPart &model_part,
                                     Parameters rParameters) : SolverProcess(Flags()), mrModelPart(model_part), mLocalConstraint(mConstraintData)
  {
    KRATOS_TRY

    Parameters default_parameters(R"(
            {
                "model_part_name":"MODEL_PART_NAME",
                "variable_name": "VARIABLE_NAME",
                "value" : [0.0, 0.0, 0.0],
                "compound_assignment": "direct",
                "constrained": false,
                "interval": [0.0, "End"],
                "local_axes" : {
                    "support": "Axes",
                    "axes"   : [ [1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0] ]
                },
                "flags_list": []
            }  )");


    // Validate against defaults -- this ensures no type mismatch
    rParameters.ValidateAndAssignDefaults(default_parameters);

    if (rParameters["local_axes"].Has("axes"))
    {
      Matrix TransformationMatrix(3, 3);
      noalias(TransformationMatrix) = ZeroMatrix(3, 3);
      for (unsigned int i = 0; i < 3; i++)
        for (unsigned int j = 0; j < 3; j++)
          TransformationMatrix(j, i) = rParameters["local_axes"]["axes"][i][j].GetDouble();
      mConstraintData.LocalBase = QuaternionType::FromRotationMatrix(TransformationMatrix);
    }

    mConstraintData.SupportName =rParameters["local_axes"]["support"].GetString();

    mVariableName = rParameters["variable_name"].GetString();
    this->SetVariableNames();
    mConstrained = rParameters["constrained"].GetBool();

    Parameters time_parameters(R"({})");
    time_parameters.AddValue("interval",rParameters["interval"]);
    mTimeInterval = TimeInterval(time_parameters, mrModelPart.GetProcessInfo());

    if (KratosComponents<Variable<ValueData>>::Has(mVariableName))
    {
      //std::array<std::string, 3> Component = {"_X","_Y","_Z"};
      for (unsigned int i = 0; i < rParameters["value"].size(); ++i)
      {
        if(!rParameters["value"][i].IsNull())
        {
          mConstraintData.Dofs.push_back(i);

          Parameters value_parameters(R"({})");
          value_parameters.AddValue("value",rParameters["value"][i]);
          mConstraintValues[i] = FunctionValue(value_parameters);
          // mpVariables.push_back(static_cast<VariablePointer>(KratosComponents<VariableData>::pGet(mVariableNames[0]+Component[i])));
          // mpFirstDerivatives.push_back(static_cast<VariablePointer>(KratosComponents<VariableData>::pGet(mVariableNames[1]+Component[i])));
          // mpSecondDerivative.push_back(static_cast<VariablePointer>(KratosComponents<VariableData>::pGet(mVariableNames[2]+Component[i])));
        }
      }

    }
    else //case of bool variable
    {
      KRATOS_ERROR << "trying to set a variable that is not in the model_part - variable name is " << mConstraintData.VariableName << std::endl;
    }

    mConstraintData.SetAssignmentMethod(rParameters["compound_assignment"].GetString());

    for (unsigned int i = 0; i < rParameters["flags_list"].size(); ++i)
    {
      std::string flag_name = rParameters["flags_list"][i].GetString();
      std::size_t found = flag_name.find("NOT_");
      if (found==0)
        mflags_list.push_back(KratosComponents<Flags>::Get(flag_name.substr(4)).AsFalse());
      else
        mflags_list.push_back(KratosComponents<Flags>::Get(flag_name));
    }

    KRATOS_CATCH("")
  }

  /// Destructor.
  ~AssignLocalConstraintSolverProcess() override {}

  ///@}
  ///@name Operators
  ///@{

  /// This operator is provided to call the process as a function and simply calls the Execute method.
  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  /// Execute method is used to execute the AssignLocalConstraintSolverProcess algorithms.
  void Execute() override
  {
  }

  /// this function is designed for being called at the beginning of the computations
  /// right after reading the model and the groups
  void ExecuteInitialize() override
  {
    InitializeLocalConstraints();

    mConstraintData.SetVariable(this->GetPrimaryVariableName(mVariableName));

    if (mConstraintData.SupportName == "Element")
      SearchNeighbours(mrModelPart.GetParentModelPart());

    SetTimeIntegrationMethod();

    //std::cout<<" Local Constraint "<<" initialized "<<mConstraintData.VariableName<<std::endl;
    // set initial values
    if (mConstraintData.SupportName != "Node")
      AssignGlobalValueToNodes();
  }

  /// this function will be executed at every time step BEFORE performing the solve phase
  void ExecuteInitializeSolutionStep() override
  {
    ClearLocalConstraints();
    ClearLocalValues();
    //std::cout<<" Local Constraint "<<" initialize solution step "<<mConstraintData.VariableName<<std::endl;
  }

  /// this function will be executed at every time step BEFORE predict
  void ExecuteBeforePredict() override
  {
    if (mTimeInterval.IsInside())
    {
      AssignLocalConstraints();
      //AssignGlobalValueToNodes();

      //std::cout<<" Local Constraint "<<" execute before predict "<<mConstraintData.VariableName<<std::endl;

      if (mConstraintData.SupportName != "Node"){
        AssignGlobalValueToNodes();
      }
    }
  }

  /// this function will be executed at every time step AFTER predict
  void ExecuteAfterPredict() override
  {
    if (mTimeInterval.IsInside())
    {

      //std::cout<<" Local Constraint "<<" execute after predict "<<mConstraintData.VariableName<<std::endl;
      //update geometry normals or local base for better accuracy
      if (mConstraintData.SupportName == "Node"){
        CalculateUnitBoundaryNormals(mrModelPart.GetParentModelPart());
        AssignConstraintValues();
      }
      else
        AssignConstraintCorrectionValues();

    }
  }

  /// this function will be executed at every time step BEFORE update
  void ExecuteBeforeUpdate() override
  {
    if (mTimeInterval.IsInside())
    {
      //std::cout<<" Local Constraint "<<" execute before update "<<mConstraintData.VariableName<<std::endl;
      if (mConstraintData.SupportName != "Node")
        AssignGlobalValueToNodes();
    }
  }

  /// this function will be executed at every time step AFTER update
  void ExecuteAfterUpdate() override
  {
    if (mTimeInterval.IsInside())
    {
      //std::cout<<" Local Constraint "<<" execute after update "<<mConstraintData.VariableName<<std::endl;
      //update geometry normals or local base for better accuracy
      if (mConstraintData.SupportName == "Node")
      {
        CalculateUnitBoundaryNormals(mrModelPart.GetParentModelPart());
        ClearLocalValues();
        AssignConstraintValues();
      }

    }
  }

  /// this function will be executed at every time step AFTER performing the solve phase
  void ExecuteFinalizeSolutionStep() override
  {
  }

  /// this function will be executed at every time step BEFORE  writing the output
  void ExecuteBeforeOutputStep() override
  {
  }

  /// this function will be executed at every time step AFTER writing the output
  void ExecuteAfterOutputStep() override
  {
  }

  /// this function is designed for being called at the end of the computations
  void ExecuteFinalize() override
  {
  }

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "AssignLocalConstraintSolverProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "AssignLocalConstraintSolverProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{

  ModelPart &mrModelPart;

  bool mConstrained;

  TimeInterval mTimeInterval;

  std::vector<Flags> mflags_list;

  std::string mVariableName;

  std::vector<std::string> mVariableNames;

  std::array<FunctionValue, 3> mConstraintValues;

  LocalConstraint::ConstraintData mConstraintData;

  LocalConstraint mLocalConstraint;

  // std::vector<VariablePointer> mpVariables;
  // std::vector<VariablePointer> mpFirstDerivatives;
  // std::vector<VariablePointer> mpSecondDerivatives;

  std::vector<FixScalarDofProcess> mFixDofsProcesses;
  std::vector<FreeScalarDofProcess> mFreeDofsProcesses;

  std::vector<ScalarIntegrationPointer> mTimeIntegration;

  ///@}
  ///@name Protected Operators
  ///@{

  void InitializeLocalConstraints()
  {
    const int nnodes = this->mrModelPart.GetMesh().Nodes().size();

    std::vector<LocalConstraint*> LocalConstraintsVector;
    std::vector<array_1d<double,3> > LocalValuesVector;

    if (nnodes != 0)
    {
      ModelPart::NodesContainerType::iterator it_begin = mrModelPart.GetMesh().NodesBegin();

#pragma omp parallel for firstprivate(it_begin) //blames in parallel
      for (int i = 0; i < nnodes; i++)
      {
        ModelPart::NodesContainerType::iterator it = it_begin + i;
        if (this->MatchTransferFlags(*(it.base()))){
          if (!it->Has(LOCAL_CONSTRAINTS))
            it->SetValue(LOCAL_CONSTRAINTS,LocalConstraintsVector);
          if (!it->Has(LOCAL_VALUES))
            it->SetValue(LOCAL_VALUES,LocalValuesVector);
        }
      }
    }
  }

  void AssignLocalConstraints()
  {
    const int nnodes = this->mrModelPart.GetMesh().Nodes().size();

     if (nnodes != 0)
     {
       ModelPart::NodesContainerType::iterator it_begin = mrModelPart.GetMesh().NodesBegin();

#pragma omp parallel for firstprivate(it_begin) //blames in parallel not threadsafe if not initialized variable
       for (int i = 0; i < nnodes; i++)
       {
         ModelPart::NodesContainerType::iterator it = it_begin + i;
         if (this->MatchTransferFlags(*(it.base()))){
           it->GetValue(LOCAL_CONSTRAINTS).push_back(&mLocalConstraint);
         }
       }
     }
  }


  void AssignGlobalValueToNodes()
  {
    const int nnodes = this->mrModelPart.GetMesh().Nodes().size();
    const unsigned int dimension = this->mrModelPart.GetProcessInfo()[SPACE_DIMENSION];

    if (nnodes != 0)
    {
      const double& Time = mrModelPart.GetProcessInfo()[TIME];

      ModelPart::NodesContainerType::iterator it_begin = mrModelPart.GetMesh().NodesBegin();

      if (mConstraintData.SupportName != "Node")
      {
#pragma omp parallel for firstprivate(it_begin)
        for (int i = 0; i < nnodes; i++)
        {
          ModelPart::NodesContainerType::iterator it = it_begin + i;
          if (this->MatchTransferFlags(*(it.base())))
          {
            array_1d<double,3> Value;
            noalias(Value) = ZeroVector(3);
            for (const auto& Direction: mConstraintData.Dofs)
            {
              this->CallFunction(*(it.base()), Time, Value, Direction);
            }

            mLocalConstraint.MapToGlobalAndAssign(*(*(it.base())), dimension, Value, mVariableName, mTimeIntegration, mVariableNames);

          }
        }
      }
    }
  }


  void AssignConstraintValues()
  {
    const int nnodes = this->mrModelPart.GetMesh().Nodes().size();

    if (nnodes != 0)
    {
      const double& Time = mrModelPart.GetProcessInfo()[TIME];

      ModelPart::NodesContainerType::iterator it_begin = mrModelPart.GetMesh().NodesBegin();

      if (mConstraintData.SupportName == "Node")
      {
#pragma omp parallel for firstprivate(it_begin)
        for (int i = 0; i < nnodes; i++)
        {
          ModelPart::NodesContainerType::iterator it = it_begin + i;
          if (this->MatchTransferFlags(*(it.base())))
          {
            array_1d<double,3> Value;
            noalias(Value) = ZeroVector(3);
            const Variable<array_1d<double, 3>>& VariableToAssign = KratosComponents<Variable<array_1d<double, 3>>>::Get(mConstraintData.VariableName);
            array_1d<double,3> VariableValue = it->FastGetSolutionStepValue(VariableToAssign);
            if (it->SolutionStepsDataHas(MESH_VELOCITY)){
              VariableValue = it->FastGetSolutionStepValue(MESH_VELOCITY);
              VariableValue -= it->FastGetSolutionStepValue(VELOCITY);
            }
            array_1d<double,3>& rNormal = it->FastGetSolutionStepValue(NORMAL);
            Value[0] = inner_prod(rNormal, VariableValue);
            it->GetValue(LOCAL_VALUES).push_back(Value);
          }
        }
      }
      else{
#pragma omp parallel for firstprivate(it_begin)
        for (int i = 0; i < nnodes; i++)
        {
          ModelPart::NodesContainerType::iterator it = it_begin + i;
          if (this->MatchTransferFlags(*(it.base())))
          {
            array_1d<double,3> Value;
            noalias(Value) = ZeroVector(3);
            for (const auto& Direction: mConstraintData.Dofs)
            {
              this->CallFunction(*(it.base()), Time, Value, Direction);
            }
            it->GetValue(LOCAL_VALUES).push_back(Value);
          }
        }

      }
    }
  }

  void AssignConstraintCorrectionValues()
  {
    const int nnodes = this->mrModelPart.GetMesh().Nodes().size();

    if (nnodes != 0)
    {
      const double& Time = mrModelPart.GetProcessInfo()[TIME];
      const unsigned int dimension = this->mrModelPart.GetProcessInfo()[SPACE_DIMENSION];

      ModelPart::NodesContainerType::iterator it_begin = mrModelPart.GetMesh().NodesBegin();

#pragma omp parallel for firstprivate(it_begin)
        for (int i = 0; i < nnodes; i++)
        {
          ModelPart::NodesContainerType::iterator it = it_begin + i;
          if (this->MatchTransferFlags(*(it.base())))
          {
            array_1d<double,3> Value;
            noalias(Value) = ZeroVector(3);
            // trying to add the correction
            // array_1d<double,3> VariableValue = mLocalConstraint.GetLocalValue(*(*(it.base())), dimension);
            // for (const auto& Direction: mConstraintData.Dofs)
            // {
            //   this->CallFunction(*(it.base()), Time, Value, Direction);
            //   Value[Direction] -= VariableValue[Direction];
            // }
            it->GetValue(LOCAL_VALUES).push_back(Value);
          }
        }
    }
  }

  void ClearLocalConstraints()
  {
    const int nnodes = this->mrModelPart.GetMesh().Nodes().size();

    if (nnodes != 0)
    {
      ModelPart::NodesContainerType::iterator it_begin = mrModelPart.GetMesh().NodesBegin();

#pragma omp parallel for firstprivate(it_begin) //blames in parallel
      for (int i = 0; i < nnodes; i++)
      {
        ModelPart::NodesContainerType::iterator it = it_begin + i;
        if (this->MatchTransferFlags(*(it.base()))){
          it->GetValue(LOCAL_CONSTRAINTS).resize(0);
          it->GetValue(LOCAL_VALUES).resize(0);
        }
      }
    }
  }

  void ClearLocalValues()
  {
    const int nnodes = this->mrModelPart.GetMesh().Nodes().size();

    if (nnodes != 0)
    {
      ModelPart::NodesContainerType::iterator it_begin = mrModelPart.GetMesh().NodesBegin();

#pragma omp parallel for firstprivate(it_begin) //blames in parallel
      for (int i = 0; i < nnodes; i++)
      {
        ModelPart::NodesContainerType::iterator it = it_begin + i;
        if (this->MatchTransferFlags(*(it.base()))){
          it->GetValue(LOCAL_VALUES).resize(0);
        }
      }
    }
  }

  void CallFunction(const Node<3>::Pointer &pNode,
                    const double &time,
                    array_1d<double,3>& rValue,
                    const unsigned int &rDirection) const
  {
    const FunctionValue& Function = mConstraintValues[rDirection];

    if (Function.IsNumeric())
    {
      rValue[rDirection] = Function.GetValue();
    }
    else{

      if (Function.IsSpatialFunction())
      {
        //double x = 0.0, y = 0.0, z = 0.0;
        //LocalAxesTransform(pNode->X(), pNode->Y(), pNode->Z(), x, y, z);
        //rValue[rDirection] = Function.CallFunction(x, y, z, time);
        rValue[rDirection] = Function.CallFunction(pNode->X(), pNode->Y(), pNode->Z(), time);
      }
      else
      {
        rValue[rDirection] = Function.CallFunction(0, 0, 0, time);
      }
    }
  }


  bool MatchTransferFlags(const Node<3>::Pointer &pNode)
  {

    for (unsigned int i = 0; i < mflags_list.size(); i++)
    {
      if (pNode->IsNot(mflags_list[i]))
        return false;
    }

    return true;
  }

  // Add time integration of variable
  void SetTimeIntegrationMethod()
  {
    std::array<std::string, 3> Component = {"_X","_Y","_Z"};
    const ProcessInfo& rCurrentProcessInfo = mrModelPart.GetProcessInfo();
    if (rCurrentProcessInfo.Has(COMPONENT_TIME_INTEGRATION_METHODS))
    {
      for (const auto& Direction: mConstraintData.Dofs)
      {
        std::string variable_name = mVariableName+Component[Direction];

        ScalarIntegrationPointer pdouble_integration = nullptr;

        if (rCurrentProcessInfo[COMPONENT_TIME_INTEGRATION_METHODS]->Has(variable_name))
        {
          pdouble_integration = rCurrentProcessInfo[COMPONENT_TIME_INTEGRATION_METHODS]->Get(variable_name)->Clone();
          pdouble_integration->SetInputVariable(KratosComponents<Variable<double>>::Get(variable_name));
        }
        else
        {
          std::string method_variable_name =rCurrentProcessInfo[COMPONENT_TIME_INTEGRATION_METHODS]->GetMethodVariableName(variable_name);
          if (method_variable_name != variable_name)
          {
            pdouble_integration = rCurrentProcessInfo[COMPONENT_TIME_INTEGRATION_METHODS]->Get(method_variable_name)->Clone();
            pdouble_integration->SetInputVariable(KratosComponents<Variable<double>>::Get(variable_name));
          }
        }
        std::cout<<" integration "<<*pdouble_integration<<std::endl;
        mTimeIntegration.push_back(pdouble_integration);
      }

    }

  }

  // Integrate assigned variables (only nodal)
  void IntegrateAssignment()
  {
    const int nnodes = mrModelPart.Nodes().size();

    if (nnodes != 0)
    {
      ModelPart::NodesContainerType::iterator it_begin = mrModelPart.GetMesh().NodesBegin();

#pragma omp parallel for firstprivate(it_begin)//blames in parallel
      for (int i = 0; i < nnodes; i++)
      {
        ModelPart::NodesContainerType::iterator it = it_begin + i;

        if (this->MatchTransferFlags(*(it.base())))
          for (const auto& integrate: mTimeIntegration)
            integrate->Assign(*it);
      }
    }
  }

  std::string GetPrimaryVariableName(std::string variable_name)
  {
    std::string name = variable_name;
    name.append("_X");

    const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();

    if (rCurrentProcessInfo.Has(COMPONENT_TIME_INTEGRATION_METHODS))
    {
      //std::cout<<" name "<<name<<std::endl;
      std::string variable;
      if (rCurrentProcessInfo[COMPONENT_TIME_INTEGRATION_METHODS]->Has(name))
      {
        variable = rCurrentProcessInfo[COMPONENT_TIME_INTEGRATION_METHODS]->Get(name)->GetPrimaryVariableName();
      }
      else
      {
        variable = rCurrentProcessInfo[COMPONENT_TIME_INTEGRATION_METHODS]->GetMethodVariableName(name);
        variable = rCurrentProcessInfo[COMPONENT_TIME_INTEGRATION_METHODS]->Get(variable)->GetPrimaryVariableName();
      }

      variable.erase(variable.length()-2);

      return variable;
    }

    return variable_name;
  }


  void SetFixAndFreeProcesses(std::string variable_name)
  {
    std::string variable_name_x = variable_name;
    variable_name_x.append("_X");
    std::string variable_name_y = variable_name;
    variable_name_y.append("_Y");
    std::string variable_name_z = variable_name;
    variable_name_z.append("_Z");

    std::vector<std::string> components = {variable_name_x, variable_name_y, variable_name_z};

    const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();

    if (rCurrentProcessInfo.Has(COMPONENT_TIME_INTEGRATION_METHODS))
    {
      for (auto &name : components)
      {
        //std::cout<<" name "<<name<<std::endl;
        std::string variable;
        if (rCurrentProcessInfo[COMPONENT_TIME_INTEGRATION_METHODS]->Has(name))
        {
          variable = rCurrentProcessInfo[COMPONENT_TIME_INTEGRATION_METHODS]->Get(name)->GetPrimaryVariableName();
        }
        else
        {
          variable = rCurrentProcessInfo[COMPONENT_TIME_INTEGRATION_METHODS]->GetMethodVariableName(name);
          variable = rCurrentProcessInfo[COMPONENT_TIME_INTEGRATION_METHODS]->Get(variable)->GetPrimaryVariableName();
        }
        Parameters settings(R"( {"variable_name" : "NAME"} )");
        settings["variable_name"].SetString(variable);
        mFixDofsProcesses.push_back(FixScalarDofProcess(mrModelPart, settings));
        mFreeDofsProcesses.push_back(FreeScalarDofProcess(mrModelPart, settings));
      }
    }
  }

  /// Copy constructor.
  AssignLocalConstraintSolverProcess(AssignLocalConstraintSolverProcess const &rOther);

  ///@}
  ///@name Protected Operations
  ///@{
  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  void SetVariableNames()
  {
    if (mVariableName == "DISPLACEMENT" || mVariableName == "VELOCITY" || mVariableName == "ACCELERATION"){
      mVariableNames = {"DISPLACEMENT", "VELOCITY", "ACCELERATION"};
      std::string mesh_prefix = "MESH_";
      if (mVariableName.find(mesh_prefix) != std::string::npos)
      {
        mVariableNames = {"MESH_DISPLACEMENT", "MESH_VELOCITY", "MESH_ACCELERATION"};
      }
    }
    else if (mVariableName == "ROTATION" || mVariableName == "ANGULAR_VELOCITY" || mVariableName == "ANGULAR_ACCELERATION")
      mVariableNames = {"ROTATION", "ANGULAR_VELOCITY", "ANGULAR_ACCELERATION", "STEP_ROTATION"};
  }

  ///@}
  ///@name Private Operations
  ///@{


  //************************************************************************************
  //************************************************************************************

  void SearchNeighbours(ModelPart &rModelPart)
  {
    KRATOS_TRY

    NodesContainerType &rNodes = rModelPart.Nodes();
    ElementsContainerType &rElements = rModelPart.Elements();

    //Check if the neigbours search is already done and set
    bool neighsearch = false;
    unsigned int number_of_nodes = rElements.begin()->GetGeometry().PointsNumber();
    for (unsigned int i = 0; i < number_of_nodes; ++i)
      if ((rElements.begin()->GetGeometry()[i].GetValue(NEIGHBOUR_ELEMENTS)).size() >= 1)
        neighsearch = true;

    if (!neighsearch){

      KRATOS_WARNING("") << " Neighbour Search Not PERFORMED : search in local constraint process " << std::endl;

      //first of all the neighbour nodes and neighbour elements arrays are initialized to the guessed size
      //this cleans the old entries:

      //*****  Erase old node and element neighbours  *********//
      CleanElementNeighbours(rNodes, rElements);

      //*************  Neigbours of nodes  ************//
      //add the neighbour elements to all the nodes in the mesh
      for (auto i_elem(rElements.begin()); i_elem != rElements.end(); ++i_elem)
      {
        for (auto &i_node : i_elem->GetGeometry())
          i_node.GetValue(NEIGHBOUR_ELEMENTS).push_back(*i_elem.base());
      }

      //adding the neighbouring nodes to all nodes in the mesh
      // for (auto &i_node : rNodes)
      // {
      //   ElementWeakPtrVectorType &nElements = i_node.GetValue(NEIGHBOUR_ELEMENTS);
      //   for (auto &i_nelem : nElements)
      //   {
      //     Element::GeometryType &rGeometry = i_nelem.GetGeometry();
      //     for (unsigned int i = 0; i < rGeometry.size(); i++)
      //     {
      //       if (rGeometry[i].Id() != i_node.Id())
      //       {
      //         NodeWeakPtrVectorType &nNodes = i_nelem.GetValue(NEIGHBOUR_NODES);
      //         AddUniqueWeakPointer<Node<3>>(nNodes, rGeometry(i));
      //       }
      //     }
      //   }
      // }

      //*************  Neigbours of elements  *********//
      //add the neighbour elements to all the elements in the mesh

      //loop over faces
      for (auto i_elem(rElements.begin()); i_elem != rElements.end(); ++i_elem)
      {
        //face nodes
        Geometry<Node<3>> &rGeometry = i_elem->GetGeometry();
        if (rGeometry.FacesNumber() == 2)
        {

          ElementWeakPtrVectorType &nElements = i_elem->GetValue(NEIGHBOUR_ELEMENTS);

          //vector of the 2 faces around the given face
          if (nElements.size() != 2)
            nElements.resize(2);

          // neighbour element over edge 0 of element ic;
          nElements(0) = CheckForNeighbourElems1D(rGeometry[0].Id(), rGeometry[0].GetValue(NEIGHBOUR_ELEMENTS), i_elem);
          // neighbour element over edge 1 of element ic;
          nElements(1) = CheckForNeighbourElems1D(rGeometry[1].Id(), rGeometry[1].GetValue(NEIGHBOUR_ELEMENTS), i_elem);
        }
      }
    }

    KRATOS_CATCH("")
  }

  //************************************************************************************
  //************************************************************************************

  void CleanElementNeighbours(NodesContainerType &rNodes, ElementsContainerType &rElements)
  {

    KRATOS_TRY

    //first of all the neighbour nodes and neighbour elements arrays are initialized to the guessed size
    //this cleans the old entries:

    unsigned int AverageNodes = 2;
    unsigned int AverageElements = 2;

    //*************  Erase old node neighbours  *************//
    for (auto &i_node : rNodes)
    {
      // NodeWeakPtrVectorType &nNodes = i_node.GetValue(NEIGHBOUR_NODES);
      // nNodes.clear();
      // nNodes.reserve(AverageNodes);

      ElementWeakPtrVectorType &nElements = i_node.GetValue(NEIGHBOUR_ELEMENTS);
      nElements.clear();
      nElements.reserve(AverageElements);
    }

    //************* Erase old element neighbours ************//
    for (auto &i_elem : rElements)
    {
      ElementWeakPtrVectorType &nElements = i_elem.GetValue(NEIGHBOUR_ELEMENTS);
      nElements.clear();
      nElements.reserve(i_elem.GetGeometry().FacesNumber());
    }

    KRATOS_CATCH("")
  }

  //************************************************************************************
  //************************************************************************************
  template <class TDataType>
  void AddUniqueWeakPointer(GlobalPointersVector<TDataType> &v, const typename TDataType::WeakPointer candidate)
  {
    typename GlobalPointersVector<TDataType>::iterator i = v.begin();
    typename GlobalPointersVector<TDataType>::iterator endit = v.end();
    while (i != endit && (i)->Id() != (candidate)->Id())
      ++i;
    if (i == endit)
      v.push_back(candidate);
  }

  //************************************************************************************
  //************************************************************************************

  ElementWeakPtrType CheckForNeighbourElems1D(unsigned int Id_1, ElementWeakPtrVectorType &nElements, ElementsContainerType::iterator i_elem)
  {
    //look for the faces around node Id_1
    for (auto i_nelem(nElements.begin()); i_nelem != nElements.end(); ++i_nelem)
    {
      //look for the nodes of the neighbour faces
      Geometry<Node<3>> &nGeometry = i_nelem->GetGeometry();
      if (nGeometry.LocalSpaceDimension() == 1)
      {
        for (unsigned int node_i = 0; node_i < nGeometry.size(); ++node_i)
        {
          if (nGeometry[node_i].Id() == Id_1)
          {
            if (i_nelem->Id() != i_elem->Id())
            {
              return *i_nelem.base();
            }
          }
        }
      }
    }
    return *i_elem.base();
  }

  /// Calculates the area normal (unitary vector oriented as the normal) and weight the normal to shrink
  void CalculateUnitBoundaryNormals(ModelPart &rModelPart)
  {

    if (!rModelPart.IsSubModelPart())
    {
      this->ResetBodyNormals(rModelPart); //clear boundary normals

      //FLUID Domains
      for (auto &i_mp : rModelPart.SubModelParts())
      {
        if (i_mp.Is(FLUID) && i_mp.IsNot(ACTIVE) && i_mp.IsNot(BOUNDARY) && i_mp.IsNot(CONTACT))
        {
          //std::cout<<" Fluid Normals compute : "<<i_mp.Name()<<std::endl;
          CalculateBoundaryNormals(i_mp);

          //assignation for solid boundaries : Unity Normals on nodes and Shrink_Factor on nodes
          AddNormalsToNodes(i_mp);
        }
      }
      //SOLID Domains
      for (auto &i_mp : rModelPart.SubModelParts())
      {
        if (i_mp.Is(SOLID) && i_mp.IsNot(ACTIVE) && i_mp.IsNot(BOUNDARY) && i_mp.IsNot(CONTACT))
        {
          //std::cout<<" Solid Normals compute : "<<i_mp.Name()<<std::endl;
          CalculateBoundaryNormals(i_mp);

          //assignation for solid boundaries : Unity Normals on nodes and Shrink_Factor on nodes
          AddNormalsToNodes(i_mp);
        }
      }
      //RIGID Domains
      for (auto &i_mp : rModelPart.SubModelParts())
      {
        if (i_mp.Is(RIGID) && i_mp.IsNot(ACTIVE) && i_mp.IsNot(BOUNDARY) && i_mp.IsNot(CONTACT))
        {
          //std::cout<<" Rigid Normals compute : "<<i_mp.Name()<<std::endl;
          CalculateBoundaryNormals(i_mp);

          //assignation for solid boundaries : Unity Normals on nodes and Shrink_Factor on nodes
          AddNormalsToNodes(i_mp);
        }
      }
    }
    else
    {

      this->ResetBodyNormals(rModelPart); //clear boundary normals

      CalculateBoundaryNormals(rModelPart);

      //assignation for solid boundaries: Unity Normals on nodes and Shrink_Factor on nodes
      AddNormalsToNodes(rModelPart);
    }

    NormalizeBodyNormals(rModelPart);
  }


  void AddNormalsToNodes(ModelPart &rModelPart)
  {
    KRATOS_TRY

    const unsigned int dimension = rModelPart.GetProcessInfo()[SPACE_DIMENSION];

    this->ResetBodyNormals(rModelPart); //clear boundary normals

    if (rModelPart.NumberOfConditions() && this->CheckConditionsLocalSpace(rModelPart, dimension - 1))
    {
      //adding the normals to the nodes
      for (auto &i_cond : rModelPart.Conditions())
      {
        Geometry<Node<3>> &rGeometry = i_cond.GetGeometry();
        double coeff = 1.00 / rGeometry.size();
        const array_1d<double, 3> &An = i_cond.GetValue(NORMAL);

        for (unsigned int i = 0; i < rGeometry.size(); ++i)
        {
          noalias(rGeometry[i].FastGetSolutionStepValue(NORMAL)) += coeff * An;
        }
      }
    }
    else if (rModelPart.NumberOfElements() && this->CheckElementsLocalSpace(rModelPart, dimension - 1))
    {
      //adding the normals to the nodes
      for (auto &i_elem : rModelPart.Elements())
      {
        Geometry<Node<3>> &rGeometry = i_elem.GetGeometry();
        double coeff = 1.00 / rGeometry.size();
        const array_1d<double, 3> &An = i_elem.GetValue(NORMAL);

        for (unsigned int i = 0; i < rGeometry.size(); ++i)
        {
          noalias(rGeometry[i].FastGetSolutionStepValue(NORMAL)) += coeff * An;
        }
      }
    }

    KRATOS_CATCH("")
  }

  void NormalizeBodyNormals(ModelPart &rModelPart)
  {
    const int nnodes = static_cast<int>(rModelPart.NumberOfNodes());

    double norm = 0.0;
#pragma omp parallel for private(norm)
    for (int k = 0; k < nnodes; k++)
    {
      ModelPart::NodesContainerType::iterator it = rModelPart.NodesBegin() + k;

      array_1d<double,3>& rNormal = it->FastGetSolutionStepValue(NORMAL);
      norm = norm_2(rNormal);
      if (norm != 0.0)
        rNormal/=norm;
    }
  }

  void ResetBodyNormals(ModelPart &rModelPart)
  {
    const int nnodes = static_cast<int>(rModelPart.NumberOfNodes());

    double norm = 0.0;
#pragma omp parallel for private(norm)
    for (int k = 0; k < nnodes; k++)
    {
      ModelPart::NodesContainerType::iterator it = rModelPart.NodesBegin() + k;
      it->FastGetSolutionStepValue(NORMAL).clear();
    }
  }

  //Check if the elements local space is equal to the spatial dimension
  bool CheckElementsLocalSpace(ModelPart &rModelPart, unsigned int dimension)
  {
    KRATOS_TRY

    if (rModelPart.Elements().front().GetGeometry().LocalSpaceDimension() == dimension)
      return true;
    else
      return false;

    KRATOS_CATCH("")
  }

  //Check if the conditions local space is equal to the spatial dimension
  bool CheckConditionsLocalSpace(ModelPart &rModelPart, unsigned int dimension)
  {
    KRATOS_TRY

    if (rModelPart.Conditions().front().GetGeometry().LocalSpaceDimension() == dimension)
      return true;
    else
      return false;

    KRATOS_CATCH("")
  }

  /// Calculates the normals of the BOUNDARY for a given model part
  void CalculateBoundaryNormals(ModelPart &rModelPart)
  {
    KRATOS_TRY

    const unsigned int dimension = rModelPart.GetProcessInfo()[SPACE_DIMENSION];

    if (rModelPart.NumberOfConditions() && this->CheckConditionsLocalSpace(rModelPart, dimension - 1))
    {
      this->CalculateBoundaryNormals(rModelPart.Conditions());
    }
    else if (rModelPart.NumberOfElements())
    {

      if (this->CheckElementsLocalSpace(rModelPart, dimension))
      {
        this->CalculateVolumeBoundaryNormals(rModelPart);
      }
      else if (this->CheckElementsLocalSpace(rModelPart, dimension - 1))
      {
        ElementsContainerType &rElements = rModelPart.Elements();

        this->CalculateBoundaryNormals(rElements);
      }
    }

    KRATOS_CATCH("")
  }

  void CalculateBoundaryNormals(ConditionsContainerType &rConditions)
  {
    KRATOS_TRY

    const unsigned int dimension = rConditions.begin()->GetGeometry().WorkingSpaceDimension();
    const int nconditions = static_cast<int>(rConditions.size());

    //calculating the normals and storing on the conditions
    array_1d<double, 3> An;
    if (dimension == 2)
    {
#pragma omp parallel for private(An)
      for (int k = 0; k < nconditions; k++)
      {
        ModelPart::ConditionsContainerType::iterator it = rConditions.begin() + k;
        //calculate the normal on the given surface element
        if (it->IsNot(CONTACT) && it->Is(BOUNDARY))
        {
          CalculateUnitNormal2D(*(*(it.base())), An);
        }
      }
    }
    else if (dimension == 3)
    {
      array_1d<double, 3> v1;
      array_1d<double, 3> v2;
#pragma omp parallel for private(An,v1,v2)
      for (int k = 0; k < nconditions; k++)
      {
        ModelPart::ConditionsContainerType::iterator it = rConditions.begin() + k;
        //calculate the normal on the given surface condition
        if (it->IsNot(CONTACT) && it->Is(BOUNDARY))
        {
          CalculateUnitNormal3D(*(*(it.base())), An, v1, v2);
        }
      }
    }

    KRATOS_CATCH("")
  }

  void CalculateBoundaryNormals(ElementsContainerType &rElements)

  {
    KRATOS_TRY

    const unsigned int dimension = (rElements.begin())->GetGeometry().WorkingSpaceDimension();
    const int nelements = static_cast<int>(rElements.size());

    //calculating the normals and storing on elements
    array_1d<double, 3> An;
    if (dimension == 2)
    {
#pragma omp parallel for private(An)
      for (int k = 0; k < nelements; k++)
      {
        ModelPart::ElementsContainerType::iterator it = rElements.begin() + k;
        //calculate the normal on the given surface element
        if (it->IsNot(CONTACT))
        {
          it->Set(BOUNDARY); //give an error in set flags (for the created rigid body)
          CalculateUnitNormal2D(*(*(it.base())), An);
        }
      }
    }
    else if (dimension == 3)
    {
      array_1d<double, 3> v1;
      array_1d<double, 3> v2;
#pragma omp parallel for private(An,v1,v2)
      for (int k = 0; k < nelements; k++)
      {
        ModelPart::ElementsContainerType::iterator it = rElements.begin() + k;
        //calculate the normal on the given surface element
        if (it->IsNot(CONTACT))
        {
          it->Set(BOUNDARY); //give an error in set flags (for the created rigid body)
          CalculateUnitNormal3D(*(*(it.base())), An, v1, v2);
        }
      }
    }

    KRATOS_CATCH("")
  }


  //this function adds the Contribution of one of the geometries
  //to the corresponding nodes
  static void CalculateUnitNormal2D(Condition &rCondition, array_1d<double, 3> &An)
  {
    const Geometry<Node<3>> &rGeometry = rCondition.GetGeometry();

    //Attention: normal criterion changed for line conditions (vs line elements)
    An[0] = rGeometry[1].Y() - rGeometry[0].Y();
    An[1] = -(rGeometry[1].X() - rGeometry[0].X());
    An[2] = 0.00;

    array_1d<double, 3> &normal = rCondition.GetValue(NORMAL);
    noalias(normal) = An / norm_2(An);
  }

  static void CalculateUnitNormal3D(Condition &rCondition, array_1d<double, 3> &An,
                                    array_1d<double, 3> &v1, array_1d<double, 3> &v2)
  {
    const Geometry<Node<3>> &rGeometry = rCondition.GetGeometry();

    v1[0] = rGeometry[1].X() - rGeometry[0].X();
    v1[1] = rGeometry[1].Y() - rGeometry[0].Y();
    v1[2] = rGeometry[1].Z() - rGeometry[0].Z();

    v2[0] = rGeometry[2].X() - rGeometry[0].X();
    v2[1] = rGeometry[2].Y() - rGeometry[0].Y();
    v2[2] = rGeometry[2].Z() - rGeometry[0].Z();

    MathUtils<double>::CrossProduct(An, v1, v2);

    array_1d<double, 3> &normal = rCondition.GetValue(NORMAL);

    noalias(normal) = An / norm_2(An);
  }

  //this function adds the Contribution of one of the geometries
  //to the corresponding nodes
  static void CalculateUnitNormal2D(Element &rElement, array_1d<double, 3> &An)
  {
    const Geometry<Node<3>> &rGeometry = rElement.GetGeometry();

    if (rGeometry.size() < 2)
    {
      rElement.GetValue(NORMAL).clear();
    }
    else
    {
      An[0] = -(rGeometry[1].Y() - rGeometry[0].Y());
      An[1] = rGeometry[1].X() - rGeometry[0].X();
      An[2] = 0.00;

      array_1d<double, 3> &normal = rElement.GetValue(NORMAL);
      noalias(normal) = An / norm_2(An);
    }
  }

  static void CalculateUnitNormal3D(Element &rElement, array_1d<double, 3> &An,
                                    array_1d<double, 3> &v1, array_1d<double, 3> &v2)
  {
    const Geometry<Node<3>> &rGeometry = rElement.GetGeometry();

    if (rGeometry.size() < 3)
    {
      rElement.GetValue(NORMAL).clear();
    }
    else
    {
      v1[0] = rGeometry[1].X() - rGeometry[0].X();
      v1[1] = rGeometry[1].Y() - rGeometry[0].Y();
      v1[2] = rGeometry[1].Z() - rGeometry[0].Z();

      v2[0] = rGeometry[2].X() - rGeometry[0].X();
      v2[1] = rGeometry[2].Y() - rGeometry[0].Y();
      v2[2] = rGeometry[2].Z() - rGeometry[0].Z();

      MathUtils<double>::CrossProduct(An, v1, v2);
      An *= 0.5;

      array_1d<double, 3> &normal = rElement.GetValue(NORMAL);

      noalias(normal) = An / norm_2(An);
    }
  }

  void CalculateVolumeBoundaryNormals(ModelPart &rModelPart)
  {
    KRATOS_TRY

    const unsigned int dimension = rModelPart.GetProcessInfo()[SPACE_DIMENSION];

    //Reset normals
    ModelPart::NodesContainerType &rNodes = rModelPart.Nodes();
    ModelPart::ElementsContainerType &rElements = rModelPart.Elements();

    //Check if the neigbours search is already done and set
    bool neighsearch = false;
    unsigned int number_of_nodes = rElements.begin()->GetGeometry().PointsNumber();
    for (unsigned int i = 0; i < number_of_nodes; ++i)
      if ((rElements.begin()->GetGeometry()[i].GetValue(NEIGHBOUR_ELEMENTS)).size() >= 1)
        neighsearch = true;
    if (!neighsearch)
      KRATOS_ERROR << " Neighbour Search Not PERFORMED " << std::endl;

    //calculating the normals and storing it on nodes
    Vector An(3);
    Element::IntegrationMethod mIntegrationMethod = Element::GeometryDataType::IntegrationMethod::GI_GAUSS_1; //one gauss point
    int PointNumber = 0; //one gauss point
    Matrix J;
    Matrix InvJ;
    double detJ;
    Matrix DN_DX;

    //int boundarycounter=0;
    for (auto &i_node : rNodes)
    {
      if(i_node.Is(BOUNDARY)){

        noalias(An) = ZeroVector(3);

        ElementWeakPtrVectorType &nElements = i_node.GetValue(NEIGHBOUR_ELEMENTS);
        for (auto &i_nelem : nElements)
        {
          Element::GeometryType &rGeometry = i_nelem.GetGeometry();

          if (rGeometry.EdgesNumber() > 1 && rGeometry.LocalSpaceDimension() == dimension)
          {
            //********** Compute the element integral ******//
            const Element::GeometryType::IntegrationPointsArrayType &integration_points = rGeometry.IntegrationPoints(mIntegrationMethod);
            const Element::GeometryType::ShapeFunctionsGradientsType &DN_De = rGeometry.ShapeFunctionsLocalGradients(mIntegrationMethod);

            J.resize(dimension, dimension, false);
            J = rGeometry.Jacobian(J, PointNumber, mIntegrationMethod);

            InvJ.clear();
            detJ = 0;
            //Calculating the inverse of the jacobian and the parameters needed
            MathUtils<double>::InvertMatrix(J, InvJ, detJ, 0.0);

            //Compute cartesian derivatives for one gauss point
            DN_DX = prod(DN_De[PointNumber], InvJ);

            //Use detJ as integration weight
            detJ *= integration_points[PointNumber].Weight();

            for (unsigned int i = 0; i < rGeometry.size(); ++i)
              if (i_node.Id() == rGeometry[i].Id())
                for (unsigned int d = 0; d < dimension; ++d)
                  An[d] += DN_DX(i, d) * detJ;
            //********** Compute the element integral ******//
          }

          if (norm_2(An) > 1e-12)
            noalias(i_node.FastGetSolutionStepValue(NORMAL)) = An / norm_2(An);
          else
            i_node.FastGetSolutionStepValue(NORMAL).clear();
        }
      }
    }

    std::cout << "  [ Boundary_Normals  (Mesh Nodes:" << rNodes.size() << ")]" << std::endl;
    KRATOS_CATCH("")
  }

  ///@}
  ///@name Private  Access
  ///@{

  /// Assignment operator.
  AssignLocalConstraintSolverProcess &operator=(AssignLocalConstraintSolverProcess const &rOther);

  ///@}
  ///@name Serialization
  ///@{
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{
  ///@}

}; // Class AssignLocalConstraintSolverProcess

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                AssignLocalConstraintSolverProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const AssignLocalConstraintSolverProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_ASSIGN_LOCAL_CONSTRAINT_SOLVER_PROCESS_HPP_INCLUDED  defined
