//
//   Project Name:        KratosSolversApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:    September 2022 $
//
//

#if !defined(KRATOS_CALCULATE_OSS_PROJECTION_STEP_SOLVER_PROCESS_HPP_INCLUDED)
#define KRATOS_CALCULATE_OSS_PROJECTION_STEP_SOLVER_PROCESS_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_processes/solver_process.hpp"
#include "custom_processes/free_scalar_dof_process.hpp"
#include "utilities/beam_math_utilities.hpp"
#include "custom_utilities/time_interval.hpp"
#include "custom_utilities/function_value.hpp"

namespace Kratos
{

///@name Kratos Classes
///@{

/// The base class for assigning a value to scalar variables or array_1d components processes in Kratos.
/** This function assigns a value to a variable belonging to all of the nodes in a given mesh
*/
class CalulcateOSSProjectionStepSolverProcess : public SolverProcess
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of CalulcateOSSProjectionStepSolverProcess
  KRATOS_CLASS_POINTER_DEFINITION(CalulcateOSSProjectionStepSolverProcess);

  ///@}
  ///@name Life Cycle
  ///@{

  CalulcateOSSProjectionStepSolverProcess(ModelPart &model_part) : SolverProcess(Flags()), mrModelPart(model_part) {}

  CalulcateOSSProjectionStepSolverProcess(ModelPart &model_part,
                               Parameters rParameters) : SolverProcess(Flags()), mrModelPart(model_part)
  {
    KRATOS_TRY

    Parameters default_parameters(R"(
            {
                "model_part_name":"MODEL_PART_NAME",
                "variable_name": "VARIABLE_NAME",
                "modulus" : 0.0,
                "direction" : [],
                "center" : [],
                "compound_assignment": "direct",
                "constrained": true,
                "interval": [0.0, "End"],
                "flags_list": []
            }  )");

    if (rParameters.Has("modulus"))
      if (rParameters["modulus"].IsString())
        default_parameters["modulus"].SetString("0.0");

    // Validate against defaults -- this ensures no type mismatch
    rParameters.ValidateAndAssignDefaults(default_parameters);

    mvariable_name = rParameters["variable_name"].GetString();

    mconstrained = rParameters["constrained"].GetBool();

    std::string mesh_prefix = "MESH_";
    mvariable_names = {"DISPLACEMENT", "VELOCITY", "ACCELERATION"};
    if (mvariable_name.find(mesh_prefix) != std::string::npos)
    {
      mvariable_names = {"MESH_DISPLACEMENT", "MESH_VELOCITY", "MESH_ACCELERATION"};
    }

    Parameters time_parameters(R"({})");
    time_parameters.AddValue("interval",rParameters["interval"]);
    mTimeInterval = TimeInterval(time_parameters, mrModelPart.GetProcessInfo());

    mvalue = 0;

    if (KratosComponents<Variable<ValueData>>::Has(mvariable_name)) //case of array_1d variable
    {

      mFunctionValue = FunctionValue(rParameters);

      if (mFunctionValue.IsNumeric())
        mvalue = mFunctionValue.GetValue();
      else
      {
        if (mFunctionValue.IsSpatialFunction())
          KRATOS_ERROR << "only non-spatial functions: f(t) allowed for an axial turn" << std::endl;
      }

      for (unsigned int i = 0; i < 3; ++i)
      {
        mcenter[i] = rParameters["center"][i].GetDouble();
      }

      mpVariable = static_cast<VariablePointer>(KratosComponents<VariableData>::pGet(mvariable_names[0]));
      mpFirstDerivative = static_cast<VariablePointer>(KratosComponents<VariableData>::pGet(mvariable_names[1]));
      mpSecondDerivative = static_cast<VariablePointer>(KratosComponents<VariableData>::pGet(mvariable_names[2]));
    }
    else //case of bool variable
    {
      KRATOS_ERROR << "trying to set a variable that is not in the model_part - variable name is " << mvariable_name << std::endl;
    }

    // set type of imposed variable value
    for (unsigned int i = 0; i < mvariable_names.size(); ++i)
      if (mvariable_name == mvariable_names[i])
        mtype = i;

    AssignmentUtils::SetAssignmentType(rParameters["compound_assignment"].GetString(), mAssignment);
    AssignmentUtils::SetUnAssignmentType(rParameters["compound_assignment"].GetString(), mUnAssignment);

    for (unsigned int i = 0; i < rParameters["flags_list"].size(); ++i)
    {
      std::string flag_name = rParameters["flags_list"][i].GetString();
      std::size_t found = flag_name.find("NOT_");
      if (found==0)
        mflags_list.push_back(KratosComponents<Flags>::Get(flag_name.substr(4)).AsFalse());
      else
        mflags_list.push_back(KratosComponents<Flags>::Get(flag_name));

      //mflags_list.push_back(KratosComponents<Flags>::Get(rParameters["flags_list"][i].GetString()));
    }

    mtotal_rotation = false;

    if (mtype == 0 && (mAssignment == AssignmentUtils::AssignmentType::DIRECT || mAssignment == AssignmentUtils::AssignmentType::UNDIRECT))
    {
      mtotal_rotation = true;
    }

    KRATOS_CATCH("")
  }

  /// Destructor.
  ~CalulcateOSSProjectionStepSolverProcess() override {}

  ///@}
  ///@name Operators
  ///@{

  /// This operator is provided to call the process as a function and simply calls the Execute method.
  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  /// Execute method is used to execute the CalulcateOSSProjectionStepSolverProcess algorithms.
  void Execute() override
  {

    KRATOS_TRY

    this->AssignRotationAboutAnAxis();

    KRATOS_CATCH("")
  }

  /// this function is designed for being called at the beginning of the computations
  /// right after reading the model and the groups
  void ExecuteInitialize() override
  {
  }

  /// this function will be executed at every time step BEFORE performing the solve phase
  void ExecuteInitializeSolutionStep() override
  {
    mrModelPart.GetProcessInfo().SetValue(FRACTIONAL_STEP,4);
    this->CalculateSplitOssProjections();
    mrModelPart.GetProcessInfo().SetValue(FRACTIONAL_STEP,5);
    this->SetOldPressuse();
  }

  /// this function will be executed at every time step AFTER predict
  void ExecuteAfterPredict() override
  {
  }

  /// this function will be executed at every time step AFTER update
  void ExecuteAfterUpdate() override
  {
  }

  /// this function will be executed at every time step AFTER performing the solve phase
  void ExecuteFinalizeSolutionStep() override
  {

    this->SetNewPressuse();
    mrModelPart.GetProcessInfo().SetValue(FRACTIONAL_STEP,6);
    this->CalculateEndOfStepVelocity();
  }

  /// this function will be executed at every time step BEFORE  writing the output
  void ExecuteBeforeOutputStep() override
  {
  }

  /// this function will be executed at every time step AFTER writing the output
  void ExecuteAfterOutputStep() override
  {
  }

  /// this function is designed for being called at the end of the computations
  /// right after reading the model and the groups
  void ExecuteFinalize() override
  {
  }

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "CalulcateOSSProjectionStepSolverProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "CalulcateOSSProjectionStepSolverProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{

  ModelPart &mrModelPart;

  ///@}
  ///@name Protected Operators
  ///@{


  /// Copy constructor.
  CalulcateOSSProjectionStepSolverProcess(CalulcateOSSProjectionStepSolverProcess const &rOther);

  ///@}
  ///@name Protected Operations
  ///@{
  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  void CalculateSplitOssProjections()
  {
    KRATOS_TRY

    array_1d<double,3> Out = ZeroVector(3);
    const int n_nodes = mrModelPart.NumberOfNodes();
    const int n_elems = mrModelPart.NumberOfElements();

#pragma omp parallel for
    for (int i_node = 0; i_node < n_nodes; ++i_node) {
      auto it_node = mrModelPart.NodesBegin() + i_node;
      it_node->FastGetSolutionStepValue(CONV_PROJ) = CONV_PROJ.Zero();
      it_node->FastGetSolutionStepValue(PRESS_PROJ) = PRESS_PROJ.Zero();
      it_node->FastGetSolutionStepValue(DIVPROJ) = 0.0;
      it_node->FastGetSolutionStepValue(NODAL_AREA) = 0.0;
    }

#pragma omp parallel for
    for (int i_elem = 0; i_elem < n_elems; ++i_elem) {
      const auto it_elem = mrModelPart.ElementsBegin() + i_elem;
      it_elem->Calculate(CONV_PROJ, Out, rModelPart.GetProcessInfo());
    }

    mrModelPart.GetCommunicator().AssembleCurrentData(CONV_PROJ);
    mrModelPart.GetCommunicator().AssembleCurrentData(PRESS_PROJ);
    mrModelPart.GetCommunicator().AssembleCurrentData(DIVPROJ);
    mrModelPart.GetCommunicator().AssembleCurrentData(NODAL_AREA);

    // If there are periodic conditions, add contributions from both sides to the periodic nodes
    // this->PeriodicConditionProjectionCorrection(rModelPart);

#pragma omp parallel for
    for (int i_node = 0; i_node < n_nodes; ++i_node) {
      auto it_node = mrModelPart.NodesBegin() + i_node;
      const double NodalArea = it_node->FastGetSolutionStepValue(NODAL_AREA);
      it_node->FastGetSolutionStepValue(CONV_PROJ) /= NodalArea;
      it_node->FastGetSolutionStepValue(PRESS_PROJ) /= NodalArea;
      it_node->FastGetSolutionStepValue(DIVPROJ) /= NodalArea;
    }

    KRATOS_CATCH("")
  }

  void SetOldPressure()
  {
    KRATOS_TRY

#pragma omp parallel for
    for (int i_node = 0; i_node < n_nodes; ++i_node) {
      auto it_node = mrModelPart.NodesBegin() + i_node;
      const double old_press = it_node->FastGetSolutionStepValue(PRESSURE);
      it_node->FastGetSolutionStepValue(PRESSURE_OLD_IT) = -mPressureGradientRelaxationFactor * old_press;
    }

    KRATOS_CATCH("")
  }


  void SetNewPressure()
  {
    KRATOS_TRY

#pragma omp parallel for
    for (int i_node = 0; i_node < n_nodes; ++i_node) {
      auto it_node = mrModelPart.NodesBegin() + i_node;
      it_node->FastGetSolutionStepValue(PRESSURE_OLD_IT) += it_node->FastGetSolutionStepValue(PRESSURE);
    }

    KRATOS_CATCH("")
  }

  void CalculateEndOfStepVelocity()
  {
    KRATOS_TRY

    const int n_nodes = mrModelPart.NumberOfNodes();
    const int n_elems = mrModelPart.NumberOfElements();

    array_1d<double,3> Out = ZeroVector(3);
    VariableUtils().SetHistoricalVariableToZero(FRACT_VEL, mrModelPart.Nodes());

#pragma omp parallel for
    for (int i_elem = 0; i_elem < n_elems; ++i_elem) {
      const auto it_elem = mrModelPart.ElementsBegin() + i_elem;
      it_elem->Calculate(VELOCITY, Out, mrModelPart.GetProcessInfo());
    }

    mrModelPart.GetCommunicator().AssembleCurrentData(FRACT_VEL);
    //this->PeriodicConditionVelocityCorrection(mrModelPart);

    // Force the end of step velocity to verify slip conditions in the model
    // if (mUseSlipConditions)
    //   this->EnforceSlipCondition(SLIP);

    if (mDomainSize > 2)
    {
#pragma omp parallel for
      for (int i_node = 0; i_node < n_nodes; ++i_node) {
        auto it_node = mrModelPart.NodesBegin() + i_node;
        const double NodalArea = it_node->FastGetSolutionStepValue(NODAL_AREA);
        if ( ! it_node->IsFixed(VELOCITY_X) )
          it_node->FastGetSolutionStepValue(VELOCITY_X) += it_node->FastGetSolutionStepValue(FRACT_VEL_X) / NodalArea;
        if ( ! it_node->IsFixed(VELOCITY_Y) )
          it_node->FastGetSolutionStepValue(VELOCITY_Y) += it_node->FastGetSolutionStepValue(FRACT_VEL_Y) / NodalArea;
        if ( ! it_node->IsFixed(VELOCITY_Z) )
          it_node->FastGetSolutionStepValue(VELOCITY_Z) += it_node->FastGetSolutionStepValue(FRACT_VEL_Z) / NodalArea;
      }
    }
    else
    {
#pragma omp parallel for
      for (int i_node = 0; i_node < n_nodes; ++i_node) {
        auto it_node = mrModelPart.NodesBegin() + i_node;
        const double NodalArea = it_node->FastGetSolutionStepValue(NODAL_AREA);
        if ( ! it_node->IsFixed(VELOCITY_X) )
          it_node->FastGetSolutionStepValue(VELOCITY_X) += it_node->FastGetSolutionStepValue(FRACT_VEL_X) / NodalArea;
        if ( ! it_node->IsFixed(VELOCITY_Y) )
          it_node->FastGetSolutionStepValue(VELOCITY_Y) += it_node->FastGetSolutionStepValue(FRACT_VEL_Y) / NodalArea;
      }
    }


    KRATOS_CATCH("")
  }

  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{

  /// Assignment operator.
  CalulcateOSSProjectionStepSolverProcess &operator=(CalulcateOSSProjectionStepSolverProcess const &rOther);

  ///@}
  ///@name Serialization
  ///@{
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{
  ///@}

}; // Class CalulcateOSSProjectionStepSolverProcess

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                CalulcateOSSProjectionStepSolverProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const CalulcateOSSProjectionStepSolverProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_CALCULATE_OSS_PROJECTION_STEP_SOLVER_PROCESS_HPP_INCLUDED  defined
