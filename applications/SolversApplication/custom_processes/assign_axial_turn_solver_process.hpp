//
//   Project Name:        KratosSolversApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:        JMC  $
//   Date:                $Date:        April 2019 $
//
//

#if !defined(KRATOS_ASSIGN_AXIAL_TURN_SOLVER_PROCESS_HPP_INCLUDED)
#define KRATOS_ASSIGN_AXIAL_TURN_SOLVER_PROCESS_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_processes/solver_process.hpp"
#include "custom_processes/free_scalar_dof_process.hpp"
#include "utilities/beam_math_utilities.hpp"
#include "custom_utilities/time_interval.hpp"
#include "custom_utilities/function_value.hpp"

namespace Kratos
{

///@name Kratos Classes
///@{

/// The base class for assigning a value to scalar variables or array_1d components processes in Kratos.
/** This function assigns a value to a variable belonging to all of the nodes in a given mesh
*/
class AssignAxialTurnSolverProcess : public SolverProcess
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of AssignAxialTurnSolverProcess
  KRATOS_CLASS_POINTER_DEFINITION(AssignAxialTurnSolverProcess);

  /// KratosVariables
  typedef array_1d<double, 3> ValueData;
  typedef const Variable<ValueData> *VariablePointer;

  ///@}
  ///@name Life Cycle
  ///@{

  AssignAxialTurnSolverProcess(ModelPart &model_part) : SolverProcess(Flags()), mrModelPart(model_part) {}

  AssignAxialTurnSolverProcess(ModelPart &model_part,
                               Parameters rParameters) : SolverProcess(Flags()), mrModelPart(model_part)
  {
    KRATOS_TRY

    Parameters default_parameters(R"(
            {
                "model_part_name":"MODEL_PART_NAME",
                "variable_name": "VARIABLE_NAME",
                "modulus" : 0.0,
                "direction" : [],
                "center" : [],
                "compound_assignment": "direct",
                "constrained": true,
                "interval": [0.0, "End"],
                "flags_list": []
            }  )");

    if (rParameters.Has("modulus"))
      if (rParameters["modulus"].IsString())
        default_parameters["modulus"].SetString("0.0");

    // Validate against defaults -- this ensures no type mismatch
    rParameters.ValidateAndAssignDefaults(default_parameters);

    mvariable_name = rParameters["variable_name"].GetString();

    mconstrained = rParameters["constrained"].GetBool();

    std::string mesh_prefix = "MESH_";
    mvariable_names = {"DISPLACEMENT", "VELOCITY", "ACCELERATION"};
    if (mvariable_name.find(mesh_prefix) != std::string::npos)
    {
      mvariable_names = {"MESH_DISPLACEMENT", "MESH_VELOCITY", "MESH_ACCELERATION"};
    }

    Parameters time_parameters(R"({})");
    time_parameters.AddValue("interval",rParameters["interval"]);
    mTimeInterval = TimeInterval(time_parameters, mrModelPart.GetProcessInfo());

    mvalue = 0;

    if (KratosComponents<Variable<ValueData>>::Has(mvariable_name)) //case of array_1d variable
    {

      mFunctionValue = FunctionValue(rParameters);

      if (mFunctionValue.IsNumeric())
        mvalue = mFunctionValue.GetValue();
      else
      {
        if (mFunctionValue.IsSpatialFunction())
          KRATOS_ERROR << "only non-spatial functions: f(t) allowed for an axial turn" << std::endl;
      }

      for (unsigned int i = 0; i < 3; ++i)
      {
        mcenter[i] = rParameters["center"][i].GetDouble();
      }

      mpVariable = static_cast<VariablePointer>(KratosComponents<VariableData>::pGet(mvariable_names[0]));
      mpFirstDerivative = static_cast<VariablePointer>(KratosComponents<VariableData>::pGet(mvariable_names[1]));
      mpSecondDerivative = static_cast<VariablePointer>(KratosComponents<VariableData>::pGet(mvariable_names[2]));
    }
    else //case of bool variable
    {
      KRATOS_ERROR << "trying to set a variable that is not in the model_part - variable name is " << mvariable_name << std::endl;
    }

    // set type of imposed variable value
    for (unsigned int i = 0; i < mvariable_names.size(); ++i)
      if (mvariable_name == mvariable_names[i])
        mtype = i;

    AssignmentUtils::SetAssignmentType(rParameters["compound_assignment"].GetString(), mAssignment);
    AssignmentUtils::SetUnAssignmentType(rParameters["compound_assignment"].GetString(), mUnAssignment);

    for (unsigned int i = 0; i < rParameters["flags_list"].size(); ++i)
    {
      std::string flag_name = rParameters["flags_list"][i].GetString();
      std::size_t found = flag_name.find("NOT_");
      if (found==0)
        mflags_list.push_back(KratosComponents<Flags>::Get(flag_name.substr(4)).AsFalse());
      else
        mflags_list.push_back(KratosComponents<Flags>::Get(flag_name));

      //mflags_list.push_back(KratosComponents<Flags>::Get(rParameters["flags_list"][i].GetString()));
    }

    mtotal_rotation = false;

    if (mtype == 0 && (mAssignment == AssignmentUtils::AssignmentType::DIRECT || mAssignment == AssignmentUtils::AssignmentType::UNDIRECT))
    {
      mtotal_rotation = true;
    }

    KRATOS_CATCH("")
  }

  /// Destructor.
  ~AssignAxialTurnSolverProcess() override {}

  ///@}
  ///@name Operators
  ///@{

  /// This operator is provided to call the process as a function and simply calls the Execute method.
  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  /// Execute method is used to execute the AssignAxialTurnSolverProcess algorithms.
  void Execute() override
  {

    KRATOS_TRY;

    this->AssignRotationAboutAnAxis();

    KRATOS_CATCH("");
  }

  /// this function is designed for being called at the beginning of the computations
  /// right after reading the model and the groups
  void ExecuteInitialize() override
  {
    ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();

    if (!rCurrentProcessInfo[IS_RESTARTED])
      rCurrentProcessInfo[INTERVAL_END_TIME] = mTimeInterval.GetEndTime();

    if (!mTimeInterval.IsInitial() && mconstrained)
      this->SetFixAndFreeProcesses(mvariable_name);

    if (mTimeInterval.IsInside())
      if (mTimeInterval.IsFixingStep())
        //std::cout<<" FIX DOFS "<<std::endl;
        for (auto &i : mFixDofsProcesses)
          i.Execute();
  }

  /// this function will be executed at every time step BEFORE performing the solve phase
  void ExecuteInitializeSolutionStep() override
  {
    if (mTimeInterval.IsRecoverStep())
    {
      if (mTimeInterval.WasInside())
        this->UnAssignRotationAboutAnAxis();
    }

    if (mTimeInterval.IsInside())
      if (mTimeInterval.IsFixingStep())
        //std::cout<<" FIX DOFS "<<std::endl;
        for (auto &i : mFixDofsProcesses)
          i.Execute();
  }

  /// this function will be executed at every time step AFTER predict
  void ExecuteAfterPredict() override
  {
    if (mTimeInterval.IsInside())
      this->AssignRotationAboutAnAxis();
  }

  /// this function will be executed at every time step AFTER update
  void ExecuteAfterUpdate() override
  {
    if (mTimeInterval.IsInside())
      this->AssignRotationAboutAnAxis();
  }

  /// this function will be executed at every time step AFTER performing the solve phase
  void ExecuteFinalizeSolutionStep() override
  {
    if (mTimeInterval.IsUnfixingStep())
    {
      //std::cout<<" FREE DOFS "<<std::endl;
      for (auto &i : mFreeDofsProcesses)
        i.Execute();
    }
  }

  /// this function will be executed at every time step BEFORE  writing the output
  void ExecuteBeforeOutputStep() override
  {
  }

  /// this function will be executed at every time step AFTER writing the output
  void ExecuteAfterOutputStep() override
  {
  }

  /// this function is designed for being called at the end of the computations
  /// right after reading the model and the groups
  void ExecuteFinalize() override
  {
  }

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "AssignAxialTurnSolverProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "AssignAxialTurnSolverProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{

  ModelPart &mrModelPart;

  bool mconstrained;

  std::string mvariable_name;

  TimeInterval mTimeInterval;

  FunctionValue mFunctionValue;

  unsigned int mtype;
  bool mtotal_rotation;

  double mvalue; //n+1

  array_1d<double, 3> mcenter;

  AssignmentUtils::AssignmentType mAssignment;
  AssignmentUtils::AssignmentType mUnAssignment;

  std::vector<Flags> mflags_list;

  std::vector<std::string> mvariable_names;
  VariablePointer mpVariable;
  VariablePointer mpFirstDerivative;
  VariablePointer mpSecondDerivative;
  std::vector<FixScalarDofProcess> mFixDofsProcesses;
  std::vector<FreeScalarDofProcess> mFreeDofsProcesses;

  ///@}
  ///@name Protected Operators
  ///@{

  void GetValues(double &rvalue, double &rfirst_derivative, double &rsecond_derivative)
  {
    const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();
    const double &rDeltaTime = rCurrentProcessInfo[DELTA_TIME];
    const double &rPreviousDeltaTime = rCurrentProcessInfo.GetPreviousSolutionStepInfo()[DELTA_TIME];
    const double &rTime = rCurrentProcessInfo[TIME];
    const double &rPreviousTime = rCurrentProcessInfo.GetPreviousSolutionStepInfo()[TIME];

    double pre_value = mvalue;

    if (!mFunctionValue.IsNumeric())
    {
      if (mFunctionValue.IsSpatialFunction())
        KRATOS_ERROR << "only non-spatial functions: f(t) allowed for an axial turn" << std::endl;
      else{
        mvalue = mFunctionValue.CallFunction(0, 0, 0, rTime);
        pre_value = mFunctionValue.CallFunction(0, 0, 0, rPreviousTime);
      }
    }

    //std::cout<<" mvalue "<<mvalue<<std::endl;
    double current_value = 0;
    double previous_value = 0;
    double delta_value = 0;
    switch (mtype)
    {
    case 0: //rotation
      this->GetTimeValues(current_value, previous_value, delta_value);
      if (mtotal_rotation)
        rvalue = mvalue;
      else
        rvalue = delta_value;
      rfirst_derivative = (mvalue - current_value) / rDeltaTime;
      rsecond_derivative = (rfirst_derivative - (current_value - previous_value) / rPreviousDeltaTime) / rDeltaTime;
      break;
    case 1: //angular velocity
      this->GetTimeValues(current_value, previous_value, delta_value);
      rvalue = delta_value;
      rfirst_derivative = mvalue;
      rsecond_derivative = (mvalue - pre_value) / rDeltaTime;
      break;
    case 2: //angular_acceleration
      this->GetTimeValues(current_value, previous_value, delta_value);
      rvalue = delta_value;
      this->GetTimeDerivativeValues(current_value, previous_value, delta_value);
      rfirst_derivative = current_value;
      rsecond_derivative = mvalue;
    default:
      KRATOS_ERROR << " Assignment type is not defined " << std::endl;
      break;
    }

    //std::cout<<"["<<mrModelPart.GetProcessInfo()[TIME]<<"] rotation "<<rvalue<<" angular_velocity "<<rfirst_derivative<<" angular_acceleration "<<rsecond_derivative<<" ("<<current_value<<" "<<previous_value<<") delta "<<delta_value<<std::endl;
  }

  void GetTimeDerivativeValues(double &rcurrent_value, double &rprevious_value, double &rdelta_value)
  {
    const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();
    double StartTime = mTimeInterval.GetStartTime();
    const double &rTime = rCurrentProcessInfo[TIME];
    const double &rPreviousTime = rCurrentProcessInfo.GetPreviousSolutionStepInfo()[TIME];

    rcurrent_value = mvalue;
    rprevious_value = mvalue;
    rdelta_value = 0;

    //std::cout<<" mvalue "<<mvalue<<std::endl;
    if (mFunctionValue.IsSpatialFunction())
      KRATOS_ERROR << "only non-spatial functions: f(t) allowed for an axial turn" << std::endl;

    switch (mtype)
    {
    case 0: //rotation
      break;
    case 1: //angular velocity
      break;
    case 2: //angular_acceleration
      //integration of a quadratic function
      if (!mFunctionValue.IsNumeric())
      {
        rcurrent_value = mFunctionValue.CallTimeFunctionIntegral(0, 0, 0, StartTime, rTime, 2);
        rprevious_value = mFunctionValue.CallTimeFunctionIntegral(0, 0, 0, StartTime, rPreviousTime, 2);
        rdelta_value = mFunctionValue.CallTimeFunctionIntegral(0, 0, 0, rPreviousTime, rTime, 2);
      }
      else
      {
        rcurrent_value *= (rTime - StartTime);
        rprevious_value *= (rPreviousTime - StartTime);
        rdelta_value = rcurrent_value - rprevious_value;
      }
    default:
      KRATOS_ERROR << " Assignment type is not defined " << std::endl;
      break;
    }
  }

  void GetTimeValues(double &rcurrent_value, double &rprevious_value, double &rdelta_value)
  {
    const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();
    double StartTime = mTimeInterval.GetStartTime();
    const double &rTime = rCurrentProcessInfo[TIME];
    const double &rPreviousTime = rCurrentProcessInfo.GetPreviousSolutionStepInfo()[TIME];

    rcurrent_value = mvalue;
    rprevious_value = mvalue;
    rdelta_value = 0;

    //std::cout<<" mvalue "<<mvalue<<std::endl;
    if (mFunctionValue.IsSpatialFunction())
      KRATOS_ERROR << "only non-spatial functions: f(t) allowed for an axial turn" << std::endl;

    switch (mtype)
    {
    case 0: //rotation
      break;
    case 1: //angular velocity
      //integration of a quadratic function
      if (!mFunctionValue.IsNumeric())
      {
        rcurrent_value = mFunctionValue.CallTimeFunctionIntegral(0, 0, 0, StartTime, rTime, 2);
        rprevious_value = mFunctionValue.CallTimeFunctionIntegral(0, 0, 0, StartTime, rPreviousTime, 2);
        rdelta_value = mFunctionValue.CallTimeFunctionIntegral(0, 0, 0, rPreviousTime, rTime, 2);
      }
      else
      {
        rcurrent_value = mvalue * (rTime - StartTime);
        rprevious_value = mvalue * (rPreviousTime - StartTime);
        rdelta_value = rcurrent_value - rprevious_value;
      }

      break;
    case 2: //angular_acceleration
      //integration of a quadratic function
      if (!mFunctionValue.IsNumeric())
      {
        rcurrent_value = mFunctionValue.CallTimeFunctionDoubleIntegral(0, 0, 0, StartTime, rTime, 2);
        rprevious_value = mFunctionValue.CallTimeFunctionDoubleIntegral(0, 0, 0, StartTime, rPreviousTime, 2);
        rdelta_value = mFunctionValue.CallTimeFunctionDoubleIntegral(0, 0, 0, rPreviousTime, rTime, 2);
      }
      else
      {
        rcurrent_value *= (rTime - StartTime) * (rTime - StartTime);
        rprevious_value *= (rPreviousTime - StartTime) * (rPreviousTime - StartTime);
        rdelta_value = rcurrent_value - rprevious_value;
      }
    default:
      KRATOS_ERROR << " Assignment type is not defined " << std::endl;
      break;
    }
  }

  // set assignment method
  void GetAssignmentValues(double &rvalue, double &rfirst_derivative, double &rsecond_derivative)
  {
    this->GetValues(rvalue, rfirst_derivative, rsecond_derivative);

    switch (mAssignment)
    {
    case AssignmentUtils::AssignmentType::DIRECT:
      break;
    case AssignmentUtils::AssignmentType::UNDIRECT:
      rvalue *= (-1);
      rfirst_derivative *= (-1);
      rsecond_derivative *= (-1);
      break;
    case AssignmentUtils::AssignmentType::ADDITION:
      break;
    case AssignmentUtils::AssignmentType::SUBTRACTION:
      rvalue *= (-1);
      rfirst_derivative *= (-1);
      rsecond_derivative *= (-1);
      break;
    default:
      KRATOS_ERROR << " Assignment type is not supported " << std::endl;
      break;
    }
  }

  template <class TMethodPointerType>
  TMethodPointerType GetRotationAssignmentMethod()
  {
    TMethodPointerType AssignmentMethod = nullptr;
    //return this->GetAssignmentMethod<TMethodPointerType>();
    switch (mtype)
    {
    case 0: //rotation
      AssignmentMethod = this->GetAssignmentMethod<TMethodPointerType>();
      break;
    default:
      AssignmentMethod = &AssignAxialTurnSolverProcess::AddAssignRotation;
      break;
    }
    return AssignmentMethod;
  }

  template <class TMethodPointerType>
  TMethodPointerType GetVelocityAssignmentMethod()
  {
    TMethodPointerType AssignmentMethod = nullptr;

    switch (mtype)
    {
    case 0: //rotation
      AssignmentMethod = this->GetAssignmentMethod<TMethodPointerType>();
      break;
    case 1: //velocity
      AssignmentMethod = this->GetAssignmentMethod<TMethodPointerType>();
      break;
    default:
      AssignmentMethod = &AssignAxialTurnSolverProcess::AddAssignValue;
      break;
    }
    return AssignmentMethod;
  }

  template <class TMethodPointerType>
  TMethodPointerType GetAccelerationAssignmentMethod()
  {
    return this->GetAssignmentMethod<TMethodPointerType>();
  }

  template <class TMethodPointerType>
  TMethodPointerType GetAssignmentMethod()
  {
    TMethodPointerType AssignmentMethod = nullptr;

    switch (mAssignment)
    {
    case AssignmentUtils::AssignmentType::DIRECT:
      AssignmentMethod = &AssignAxialTurnSolverProcess::DirectAssignValue;
      break;
    case AssignmentUtils::AssignmentType::UNDIRECT:
      AssignmentMethod = &AssignAxialTurnSolverProcess::DirectAssignValue;
      break;
    case AssignmentUtils::AssignmentType::ADDITION:
      AssignmentMethod = &AssignAxialTurnSolverProcess::AddAssignValue;
      break;
    case AssignmentUtils::AssignmentType::SUBTRACTION:
      AssignmentMethod = &AssignAxialTurnSolverProcess::AddAssignValue;
      break;
    default:
      KRATOS_ERROR << "Unexpected value for Assignment method " << std::endl;
    }
    return AssignmentMethod;
  }

  template <class TVarType, class TDataType>
  void AddAssignRotation(ModelPart::NodeType &rNode, const TVarType &rVariable, const TDataType &value, const Matrix &rotation_matrix)
  {
    rNode.FastGetSolutionStepValue(rVariable) = rNode.FastGetSolutionStepValue(rVariable, 1) + value;
  }

  template <class TVarType, class TDataType>
  void DirectAssignValue(ModelPart::NodeType &rNode, const TVarType &rVariable, const TDataType &value, const Matrix &rotation_matrix)
  {
    rNode.FastGetSolutionStepValue(rVariable) = value;
  }

  template <class TVarType, class TDataType>
  void AddAssignValue(ModelPart::NodeType &rNode, const TVarType &rVariable, const TDataType &value, const Matrix &rotation_matrix)
  {
    rNode.FastGetSolutionStepValue(rVariable) = prod(rotation_matrix, rNode.FastGetSolutionStepValue(rVariable, 1)) + value;
  }

  bool MatchTransferFlags(const Node<3>::Pointer &pNode)
  {

    for (unsigned int i = 0; i < mflags_list.size(); i++)
    {
      if (pNode->IsNot(mflags_list[i]))
        return false;
    }

    return true;
  }

  void SetFixAndFreeProcesses(std::string variable_name)
  {
    std::string variable_name_x = variable_name;
    variable_name_x.append("_X");
    std::string variable_name_y = variable_name;
    variable_name_y.append("_Y");
    std::string variable_name_z = variable_name;
    variable_name_z.append("_Z");

    std::vector<std::string> components = {variable_name_x, variable_name_y, variable_name_z};

    const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();

    if (rCurrentProcessInfo.Has(COMPONENT_TIME_INTEGRATION_METHODS))
    {
      for (auto &name : components)
      {
        //std::cout<<" name "<<name<<std::endl;
        std::string variable;
        if (rCurrentProcessInfo[COMPONENT_TIME_INTEGRATION_METHODS]->Has(name))
        {
          variable = rCurrentProcessInfo[COMPONENT_TIME_INTEGRATION_METHODS]->Get(name)->GetPrimaryVariableName();
        }
        else
        {
          variable = rCurrentProcessInfo[COMPONENT_TIME_INTEGRATION_METHODS]->GetMethodVariableName(name);
          variable = rCurrentProcessInfo[COMPONENT_TIME_INTEGRATION_METHODS]->Get(variable)->GetPrimaryVariableName();
        }
        Parameters settings(R"( {"variable_name" : "NAME"} )");
        settings["variable_name"].SetString(variable);
        mFixDofsProcesses.push_back(FixScalarDofProcess(mrModelPart, settings));
        mFreeDofsProcesses.push_back(FreeScalarDofProcess(mrModelPart, settings));
      }
    }
  }

  /// Copy constructor.
  AssignAxialTurnSolverProcess(AssignAxialTurnSolverProcess const &rOther);

  ///@}
  ///@name Protected Operations
  ///@{
  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  void UnAssignRotationAboutAnAxis()
  {
    KRATOS_TRY

    AssignmentUtils::AssignmentType Assignment = mAssignment;

    mAssignment = mUnAssignment;

    AssignRotationAboutAnAxis();

    mAssignment = Assignment;

    KRATOS_CATCH("")
  }

  void AssignRotationAboutAnAxis()
  {
    KRATOS_TRY

    const int nnodes = mrModelPart.GetMesh().Nodes().size();

    typedef void (AssignAxialTurnSolverProcess::*AssignmentMethodPointer)(ModelPart::NodeType &, const Variable<array_1d<double, 3>> &, const array_1d<double, 3> &, const Matrix &);

    AssignmentMethodPointer RotationAssignmentMethod = this->GetRotationAssignmentMethod<AssignmentMethodPointer>();
    //AssignmentMethodPointer RotationAssignmentMethod     = this->GetAssignmentMethod<AssignmentMethodPointer>();
    AssignmentMethodPointer VelocityAssignmentMethod = this->GetVelocityAssignmentMethod<AssignmentMethodPointer>();
    AssignmentMethodPointer AccelerationAssignmentMethod = this->GetAccelerationAssignmentMethod<AssignmentMethodPointer>();

    if (nnodes != 0)
    {
      ModelPart::NodesContainerType::iterator it_begin = mrModelPart.NodesBegin();

      Matrix rotation_matrix;
      Matrix skewsym_matrix;
      array_1d<double, 3> radius;
      array_1d<double, 3> distance;
      array_1d<double, 3> delta_displacement;

      //Possible prescribed variables: DISPLACEMENT(rotation) / VELOCITY(angular_velocity)/ ACCELERATION(angular_acceleration)
      array_1d<double, 3> assigned_value;

      double rotation_modulus = 0, angular_velocity_modulus = 0, angular_acceleration_modulus = 0;
      this->GetAssignmentValues(rotation_modulus, angular_velocity_modulus, angular_acceleration_modulus);

      array_1d<double, 3> direction = mFunctionValue.GetDirection();

      array_1d<double, 3> rotation = rotation_modulus * direction;
      array_1d<double, 3> angular_velocity = angular_velocity_modulus * direction;
      array_1d<double, 3> angular_acceleration = angular_acceleration_modulus * direction;

      //std::cout<<" ASSIGN rotation: "<<rotation<<" velocity: "<<angular_velocity<<" acceleration: "<<angular_acceleration<<std::endl;

      bool assign_angular_velocity = false;
      bool assign_angular_acceleration = false;

      if (it_begin->SolutionStepsDataHas(*mpFirstDerivative))
      {
        assign_angular_velocity = true;
      }
      if (it_begin->SolutionStepsDataHas(*mpSecondDerivative))
      {
        assign_angular_acceleration = true;
      }

      //Get rotation matrix
      Quaternion<double> total_quaternion = Quaternion<double>::FromRotationVector<array_1d<double, 3>>(rotation);

#pragma omp parallel for private(distance, radius, delta_displacement, rotation_matrix, assigned_value, skewsym_matrix)
      for (int i = 0; i < nnodes; i++)
      {
        ModelPart::NodesContainerType::iterator it = it_begin + i;

        if (this->MatchTransferFlags(*(it.base())))
        {

          if (mtotal_rotation)
          {
            noalias(delta_displacement) = ZeroVector(3);
            noalias(distance) = it->GetInitialPosition() - mcenter;
          }
          else
          {
            //scheme integrated variables corrupts the mpVariable value
            noalias(delta_displacement) = (it->Coordinates() - it->GetInitialPosition()) - it->FastGetSolutionStepValue(*mpVariable, 1);
            noalias(distance) = (it->Coordinates() - delta_displacement) - mcenter;
          }

          total_quaternion.ToRotationMatrix(rotation_matrix);

          noalias(radius) = prod(rotation_matrix, distance);

          assigned_value = radius - distance;

          //std::cout<<" Node ["<<it->Id()<<"] radius "<<radius<<" displacement "<<(it->Coordinates()-it->GetInitialPosition())<<" displacement_1 "<<it->FastGetSolutionStepValue(*mpVariable,1)<<" distance "<<distance<<" coordinates "<<it->Coordinates()<<" coordinates n "<<it->Coordinates() - delta_displacement<<" initial "<<it->GetInitialPosition()<<std::endl;

          (this->*RotationAssignmentMethod)(*it, *mpVariable, assigned_value, rotation_matrix);

          //std::cout<<" Node ["<<it->Id()<<"] assigned displacement "<<it->FastGetSolutionStepValue(*mpVariable)<<std::endl;

          if (assign_angular_velocity)
          {

            //compute the skewsymmmetric tensor of the angular velocity
            BeamMathUtils<double>::VectorToSkewSymmetricTensor(angular_velocity, skewsym_matrix);

            //compute the contribution of the angular velocity to the velocity v = Wxr
            assigned_value = prod(skewsym_matrix, radius);
            (this->*VelocityAssignmentMethod)(*it, *mpFirstDerivative, assigned_value, rotation_matrix);

            if (assign_angular_acceleration)
            {
              //compute the contribution of the centripetal acceleration ac = Wx(Wxr)
              assigned_value = prod(skewsym_matrix, assigned_value);

              //compute the contribution of the angular acceleration to the acceleration a = Axr
              BeamMathUtils<double>::VectorToSkewSymmetricTensor(angular_acceleration, skewsym_matrix);
              assigned_value += prod(skewsym_matrix, radius);
              (this->*AccelerationAssignmentMethod)(*it, *mpSecondDerivative, assigned_value, rotation_matrix);
            }
          }
        }
      }
    }

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{

  /// Assignment operator.
  AssignAxialTurnSolverProcess &operator=(AssignAxialTurnSolverProcess const &rOther);

  ///@}
  ///@name Serialization
  ///@{
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{
  ///@}

}; // Class AssignAxialTurnSolverProcess

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                AssignAxialTurnSolverProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const AssignAxialTurnSolverProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_ASSIGN_AXIAL_TURN_SOLVER_PROCESS_HPP_INCLUDED  defined
