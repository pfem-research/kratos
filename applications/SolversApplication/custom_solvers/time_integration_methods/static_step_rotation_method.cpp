//
//   Project Name:        KratosSolversApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:        JMC  $
//   Date:                $Date:      January 2019 $
//
//

// System includes

// External includes

// Project includes
#include "custom_solvers/time_integration_methods/static_step_rotation_method.hpp"
#include "utilities/beam_math_utilities.hpp"

namespace Kratos
{

// other types
template <class TVariableType, class TValueType>
void StaticStepRotationMethod<TVariableType, TValueType>::Update(NodeType &rNode)
{
  KRATOS_TRY

  KRATOS_ERROR << " Calling a non compatible type update for ROTATIONS in StaticStepRotationMethod " << std::endl;

  KRATOS_CATCH("")
}

// specilization to array_1d
template <>
void StaticStepRotationMethod<Variable<array_1d<double, 3>>, array_1d<double, 3>>::Update(NodeType &rNode)
{
  KRATOS_TRY

  // predict step variable from previous and current values
  array_1d<double, 3> &CurrentStepVariable = rNode.FastGetSolutionStepValue(*this->mpStepVariable, 0);

  array_1d<double, 3> &CurrentVariable = rNode.FastGetSolutionStepValue(*this->mpVariable, 0);
  array_1d<double, 3> &PreviousStepVariable = rNode.FastGetSolutionStepValue(*this->mpStepVariable, 1);

  // update delta variable
  array_1d<double, 3> DeltaVariable;
  noalias(DeltaVariable) = CurrentVariable - PreviousStepVariable;

  Quaternion<double> DeltaVariableQuaternion = Quaternion<double>::FromRotationVector(DeltaVariable);

  // linear delta variable
  array_1d<double, 3> LinearDeltaVariable;
  noalias(LinearDeltaVariable) = -CurrentStepVariable;

  // update step variable
  Quaternion<double> StepVariableQuaternion = Quaternion<double>::FromRotationVector(CurrentStepVariable);

  StepVariableQuaternion = DeltaVariableQuaternion * StepVariableQuaternion;

  StepVariableQuaternion.ToRotationVector(CurrentStepVariable);

  LinearDeltaVariable += CurrentStepVariable;

  // update variable:
  Quaternion<double> VariableQuaternion = Quaternion<double>::FromRotationVector(PreviousStepVariable);

  VariableQuaternion = DeltaVariableQuaternion * VariableQuaternion;

  VariableQuaternion.ToRotationVector(CurrentVariable);

  // update variable previous iteration instead of previous step
  PreviousStepVariable = CurrentVariable;

  // update linear delta variable:
  VariableQuaternion = StepVariableQuaternion.conjugate() * VariableQuaternion;
  LinearDeltaVariable = BeamMathUtils<double>::MapToCurrentLocalFrame(VariableQuaternion, LinearDeltaVariable);

  KRATOS_CATCH("")
}

template class StaticStepRotationMethod<Variable<array_1d<double, 3>>, array_1d<double, 3>>;
template class StaticStepRotationMethod<Variable<double>, double>;

} // namespace Kratos.
