//
//   Project Name:        KratosSolversApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:        JMC  $
//   Date:                $Date:      January 2019 $
//
//

#if !defined(KRATOS_STATIC_METHOD_HPP_INCLUDED)
#define KRATOS_STATIC_METHOD_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_solvers/time_integration_methods/time_integration_method.hpp"

namespace Kratos
{
///@addtogroup SolversApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   * This class performs predict and update of dofs variables, their time derivatives and time integrals
   */
template <class TVariableType, class TValueType>
class KRATOS_API(SOLVERS_APPLICATION) StaticMethod : public TimeIntegrationMethod<TVariableType, TValueType>
{
public:
  ///@name Type Definitions
  ///@{

  /// BaseType
  typedef TimeIntegrationMethod<TVariableType, TValueType> BaseType;

  /// BasePointerType
  typedef typename BaseType::Pointer BasePointerType;

  /// NodeType
  typedef typename BaseType::NodeType NodeType;

  /// KratosVariable or KratosVariableComponent
  typedef typename BaseType::VariablePointer VariablePointer;

  KRATOS_CLASS_POINTER_DEFINITION(StaticMethod);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default Constructor.
  StaticMethod() : BaseType() {}

  /// Constructor.
  StaticMethod(const TVariableType &rVariable) : BaseType(rVariable) {}

  /// Constructor.
  StaticMethod(const TVariableType &rVariable, const TVariableType &rFirstDerivative, const TVariableType &rSecondDerivative) : BaseType(rVariable, rFirstDerivative, rSecondDerivative) {}

  /// Constructor.
  StaticMethod(const TVariableType &rVariable, const TVariableType &rFirstDerivative, const TVariableType &rSecondDerivative, const TVariableType &rPrimaryVariable) : BaseType(rVariable, rFirstDerivative, rSecondDerivative, rPrimaryVariable) {}

  /// Copy Constructor.
  StaticMethod(StaticMethod &rOther) : BaseType(rOther) {}

  /// Clone.
  BasePointerType Clone() override
  {
    return BasePointerType(new StaticMethod(*this));
  }

  /// Destructor.
  ~StaticMethod() override {}

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  // assign
  void Assign(NodeType &rNode) override
  {
  }

  // predict
  void Predict(NodeType &rNode) override
  {
    //it accelerates convergence by using a better initial prediction
    //but it can generate problems when zero prediction is better
    //buffer 3 requiered 15/09/2021
    if(this->Is(TimeIntegrationLocalFlags::PREDICT_PRIMARY_VARIABLE))
      this->PredictVariable(rNode);
  }

  // update
  void Update(NodeType &rNode) override
  {
  }

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "StaticMethod";
    return buffer.str();
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "StaticMethod";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "StaticMethod Data";
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  void PredictVariable(NodeType &rNode) override
  {
    KRATOS_TRY

    TValueType &CurrentVariable = rNode.FastGetSolutionStepValue(*this->mpVariable, 0);
    const TValueType &PreviousVariable = rNode.FastGetSolutionStepValue(*this->mpVariable, 1);
    const TValueType &PreviousStepVariable = rNode.FastGetSolutionStepValue(*this->mpVariable, 2);
    CurrentVariable += (PreviousVariable-PreviousStepVariable);

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  void save(Serializer &rSerializer) const override{
      KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BaseType)};

  void load(Serializer &rSerializer) override{
      KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BaseType)};

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class StaticMethod

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

template <class TVariableType, class TValueType>
inline std::istream &operator>>(std::istream &rIStream, StaticMethod<TVariableType, TValueType> &rThis)
{
  return rIStream;
}

template <class TVariableType, class TValueType>
inline std::ostream &operator<<(std::ostream &rOStream, const StaticMethod<TVariableType, TValueType> &rThis)
{
  return rOStream << rThis.Info();
}

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_STATIC_METHOD_HPP_INCLUDED defined
