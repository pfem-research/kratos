//
//   Project Name:        KratosSolversApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:        JMC  $
//   Date:                $Date:      January 2019 $
//
//

// System includes

// External includes

// Project includes
#include "custom_solvers/time_integration_methods/simo_step_rotation_method.hpp"
#include "utilities/beam_math_utilities.hpp"

namespace Kratos
{

// specilization to array_1d
template <class TVariableType, class TValueType>
void SimoStepRotationMethod<TVariableType, TValueType>::Predict(NodeType &rNode)
{
  KRATOS_TRY

  KRATOS_ERROR << " Calling a non compatible type update for ROTATIONS in SimoStepRotationScheme " << std::endl;

  KRATOS_CATCH("")
}

// specilization to array_1d
template <class TVariableType, class TValueType>
void SimoStepRotationMethod<TVariableType, TValueType>::Update(NodeType &rNode)
{
  KRATOS_TRY

  KRATOS_ERROR << " Calling a non compatible type update for ROTATIONS in SimoStepRotationScheme " << std::endl;

  KRATOS_CATCH("")
}

template <>
void SimoStepRotationMethod<Variable<array_1d<double, 3>>, array_1d<double, 3>>::Predict(NodeType &rNode)
{
  KRATOS_TRY

  // predict step variable from previous and current values
  array_1d<double, 3> &CurrentStepVariable = rNode.FastGetSolutionStepValue(*this->mpStepVariable, 0);
  array_1d<double, 3> &PreviousStepVariable = rNode.FastGetSolutionStepValue(*this->mpStepVariable, 1);

  array_1d<double, 3> &CurrentVariable = rNode.FastGetSolutionStepValue(*this->mpVariable, 0);
  const array_1d<double, 3> &PreviousVariable = rNode.FastGetSolutionStepValue(*this->mpVariable, 1);

  array_1d<double, 3> &CurrentFirstDerivative = rNode.FastGetSolutionStepValue(*this->mpFirstDerivative, 0);

  // predict step variable
  Quaternion<double> PreviousVariableQuaternion = Quaternion<double>::FromRotationVector(PreviousVariable);

  //CurrentVariable = PreviousVariable + CurrentFirstDerivative * (1.0 / this->mNewmark.c1);

  Quaternion<double> CurrentVariableQuaternion = Quaternion<double>::FromRotationVector(CurrentVariable);
  CurrentVariableQuaternion.ToRotationVector(CurrentVariable);

  Quaternion<double> StepVariableQuaternion = CurrentVariableQuaternion * PreviousVariableQuaternion.conjugate();
  StepVariableQuaternion.ToRotationVector(CurrentStepVariable);

  PreviousStepVariable = CurrentVariable;

  CurrentFirstDerivative = this->mNewmark.c1 * (CurrentStepVariable);

  array_1d<double, 3> &CurrentSecondDerivative = rNode.FastGetSolutionStepValue(*this->mpSecondDerivative, 0);
  //const array_1d<double, 3> &PreviousFirstDerivative = rNode.FastGetSolutionStepValue(*this->mpFirstDerivative, 1);

  //CurrentSecondDerivative = (this->mNewmark.c0 / this->mNewmark.c1) * (CurrentFirstDerivative - PreviousFirstDerivative);
  CurrentSecondDerivative = this->mNewmark.c0 * (CurrentStepVariable);

  KRATOS_CATCH("")
}

template <>
void SimoStepRotationMethod<Variable<array_1d<double, 3>>, array_1d<double, 3>>::Update(NodeType &rNode)
{
  KRATOS_TRY

  // predict step variable from previous and current values
  array_1d<double, 3> &CurrentStepVariable = rNode.FastGetSolutionStepValue(*this->mpStepVariable, 0);

  array_1d<double, 3> &CurrentVariable = rNode.FastGetSolutionStepValue(*this->mpVariable, 0);
  array_1d<double, 3> &PreviousStepVariable = rNode.FastGetSolutionStepValue(*this->mpStepVariable, 1);

  //std::cout<<this->mpVariable->Name()<<" Node["<<rNode.Id()<<"] "<<CurrentVariable<<" "<<CurrentStepVariable<<" "<<PreviousStepVariable<<std::endl;
  //std::cout<<this->mpVariable->Name()<<" Update Node["<<rNode.Id()<<"] "<<CurrentVariable<<" "<<std::endl;
  // update delta variable
  array_1d<double, 3> DeltaVariable;
  noalias(DeltaVariable) = CurrentVariable - PreviousStepVariable;

  Quaternion<double> DeltaVariableQuaternion = Quaternion<double>::FromRotationVector(DeltaVariable);

  // linear delta variable
  array_1d<double, 3> LinearDeltaVariable;
  noalias(LinearDeltaVariable) = -CurrentStepVariable;

  // update step variable
  Quaternion<double> StepVariableQuaternion = Quaternion<double>::FromRotationVector(CurrentStepVariable);

  StepVariableQuaternion = DeltaVariableQuaternion * StepVariableQuaternion;

  StepVariableQuaternion.ToRotationVector(CurrentStepVariable);

  LinearDeltaVariable += CurrentStepVariable;

  // update variable:
  Quaternion<double> VariableQuaternion = Quaternion<double>::FromRotationVector(PreviousStepVariable);

  VariableQuaternion = DeltaVariableQuaternion * VariableQuaternion;

  VariableQuaternion.ToRotationVector(CurrentVariable);

  // update variable previous iteration instead of previous step
  PreviousStepVariable = CurrentVariable;

  // update linear delta variable:
  VariableQuaternion = StepVariableQuaternion.conjugate() * VariableQuaternion;
  LinearDeltaVariable = BeamMathUtils<double>::MapToCurrentLocalFrame(VariableQuaternion, LinearDeltaVariable);

  // if (norm_2(CurrentVariable)!=0)
  //   std::cout<<this->mpVariable->Name()<<" Previous Node["<<rNode.Id()<<"] "<<rNode.FastGetSolutionStepValue(*this->mpFirstDerivative, 0)<<" "<<rNode.FastGetSolutionStepValue(*this->mpSecondDerivative, 0)<<std::endl;

  // update first derivative
  array_1d<double, 3> &CurrentFirstDerivative = rNode.FastGetSolutionStepValue(*this->mpFirstDerivative, 0);
  noalias(CurrentFirstDerivative) += this->mNewmark.c1 * LinearDeltaVariable;

  // update second derivative
  array_1d<double, 3> &CurrentSecondDerivative = rNode.FastGetSolutionStepValue(*this->mpSecondDerivative, 0);
  noalias(CurrentSecondDerivative) += this->mNewmark.c0 * LinearDeltaVariable;

  //std::cout<<this->mpVariable->Name()<<" Update Node["<<rNode.Id()<<"] "<<CurrentVariable<<" "<<std::endl;
  // if (norm_2(CurrentFirstDerivative)!=0)
  //   std::cout<<this->mpVariable->Name()<<" Update Node["<<rNode.Id()<<"] "<<CurrentFirstDerivative<<" "<<CurrentSecondDerivative<<" Linear Delta Variable "<<LinearDeltaVariable<<std::endl;

  KRATOS_CATCH("")
}

template class SimoStepRotationMethod<Variable<array_1d<double, 3>>, array_1d<double, 3>>;
template class SimoStepRotationMethod<Variable<double>, double>;

} // namespace Kratos.
