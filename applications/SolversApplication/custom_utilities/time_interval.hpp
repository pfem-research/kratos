//
//   Project Name:        KratosSolversApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:      January 2021 $
//
//

#if !defined(KRATOS_TIME_INTERVAL_HPP_INCLUDED)
#define KRATOS_TIME_INTERVAL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "includes/kratos_parameters.h"
#include "solvers_application_variables.h"

namespace Kratos
{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Methods to give support to time interval
 */
class TimeInterval
{
  ///@name Type Definitions
  ///@{

  /// Pointer definition of TimeInterval
  KRATOS_CLASS_POINTER_DEFINITION(TimeInterval);

protected:

  struct IntervalData
  {
    double begin, end, delta_time, current_time, previous_time, tolerance;
    bool initial, started, finished;
  };


public:

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  TimeInterval() : mpProcessInfo(nullptr){};

  /// Constructor.
  TimeInterval(Parameters& rParameters, ProcessInfo &rCurrentProcessInfo) : mpProcessInfo(&rCurrentProcessInfo)
  {
    if (!rParameters.Has("interval"))
    {
      Parameters default_parameters(R"( {"interval": [0.0, "End"]} )");
      rParameters.AddValue("interval", default_parameters["interval"]);
    }

    mInterval.begin = 0.0;
    if (rParameters["interval"][0].IsDouble())
      mInterval.begin = rParameters["interval"][0].GetDouble();
    else if (rParameters["interval"][0].IsInt())
      mInterval.begin = rParameters["interval"][0].GetInt();
    else
      KRATOS_ERROR << "first value for a time interval must be a number, supplied interval : \n" + rParameters["interval"].PrettyPrintJsonString();

    mInterval.end = 0.0;
    if (rParameters["interval"][1].IsString())
    {
      if (rParameters["interval"][1].GetString() == "End")
        mInterval.end = std::numeric_limits<double>::max();
      else
        KRATOS_ERROR << "second value for a time interval can be \"End\" or a number, supplied interval : \n" + rParameters["interval"].PrettyPrintJsonString();
    }
    else
    {
      if (rParameters["interval"][1].IsDouble())
        mInterval.end = rParameters["interval"][1].GetDouble();
      else if (rParameters["interval"][1].IsInt())
        mInterval.end = rParameters["interval"][1].GetInt();
      else
        KRATOS_ERROR << "second value for a time interval can be \"End\" or a number, supplied interval : \n" + rParameters["interval"].PrettyPrintJsonString();
    }

    mInterval.initial = false;
    if ((mInterval.begin == 0.0 && mInterval.end == 0.0) || mInterval.begin < 0.0)
    {
      mInterval.initial = true;
      mInterval.begin = 0.0;
    }

    mInterval.started = IsStarted();
    mInterval.finished = IsEnded();

  }

  /// Destructor.
  virtual ~TimeInterval(){}

  ///@}
  ///@name Operators
  ///@{
  ///@}
  ///@name Operations
  ///@{
  ///@}
  ///@name Access
  ///@{

  double GetStartTime() const
  {
    return mInterval.begin;
  }

  double GetEndTime() const
  {
    return mInterval.end;
  }

  double GetDeltaTime() const
  {
    return (*mpProcessInfo)[DELTA_TIME];
  }

  ///@}
  ///@name Inquiry
  ///@{

  bool IsInitial() const
  {
    return mInterval.initial;
  }

  bool IsInside()
  {
    SetCurrentTime();
    if (mInterval.current_time > mInterval.begin - mInterval.tolerance && mInterval.current_time <= mInterval.end + mInterval.tolerance){
      mInterval.finished = false;
      return true;
    }
    else
      return false;
  }

  bool WasInside()
  {
    SetPreviousTime();
    if (mInterval.current_time > mInterval.begin - mInterval.tolerance && mInterval.current_time <= mInterval.end + mInterval.tolerance){
      mInterval.finished = false;
      return true;
    }
    else
      return false;
  }

  bool IsEnded()
  {
    SetCurrentTime();
    if (mInterval.current_time + mInterval.delta_time >= mInterval.end + mInterval.tolerance)
      return true;
    else
      return false;
  }

  bool IsFixingStep()
  {
    if (!mInterval.started){
      mInterval.started = true;
      return true;
    }
    else{
      if (mInterval.previous_time == (*mpProcessInfo)[INTERVAL_END_TIME])
        return true;
      else
        return false;
    }
  }

  bool IsUnfixingStep()
  {
    SetCurrentTime();
    if (!mInterval.finished){
      if (mInterval.current_time + mInterval.delta_time > mInterval.end + mInterval.tolerance){
        mInterval.finished = true;
        (*mpProcessInfo)[INTERVAL_END_TIME] = mInterval.current_time;
        return true;
      }
      else
        return false;
    }
    else
        return false;

  }

  bool IsRecoverStep() const
  {
    if ((*mpProcessInfo).Has(DELTA_TIME_CHANGED)){
      if ((*mpProcessInfo)[DELTA_TIME_CHANGED])
        return true;
      else
        return false;
    }
    else
        return false;
  }

  ///@}
  ///@name Input and output
  ///@{
  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{

  bool IsStarted()
  {
    SetCurrentTime();
    if (mInterval.current_time >= mInterval.begin + mInterval.tolerance)
      return true;
    else
      return false;
  }

  inline void SetCurrentTime()
  {
    mInterval.delta_time = (*mpProcessInfo)[DELTA_TIME];
    mInterval.current_time = (*mpProcessInfo)[TIME];
    mInterval.previous_time = mInterval.current_time-mInterval.delta_time;
    mInterval.tolerance = mInterval.delta_time * 1e-4;
  }

  inline void SetPreviousTime()
  {
    const ProcessInfo& rPreviousProcessInfo = (*mpProcessInfo).GetPreviousSolutionStepInfo();
    mInterval.delta_time = rPreviousProcessInfo[DELTA_TIME];
    mInterval.current_time = rPreviousProcessInfo[TIME];
    mInterval.previous_time = mInterval.current_time-mInterval.delta_time;
    mInterval.tolerance = mInterval.delta_time * 1e-4;
  }


  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}
private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{

  ProcessInfo* mpProcessInfo;

  IntervalData mInterval;

  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Unaccessible methods
  ///@{
  ///@}

}; // Class TimeInterval


} // namespace Kratos.

#endif // KRATOS_TIME_INTERVAL_HPP_INCLUDED
