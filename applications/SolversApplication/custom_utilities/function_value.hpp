//
//   Project Name:        KratosSolversApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:      January 2020 $
//
//

#if !defined(KRATOS_FUNCTION_VALUE_HPP_INCLUDED)
#define KRATOS_FUNCTION_VALUE_HPP_INCLUDED

// System includes
#include <pybind11/pybind11.h>
#include <pybind11/eval.h>
#include <cmath>

// External includes

// Project includes
#include "includes/kratos_parameters.h"

namespace Kratos
{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
 */
class FunctionValue
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of FunctionValue
  KRATOS_CLASS_POINTER_DEFINITION(FunctionValue);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  FunctionValue(){};

  /// Constructor.
  FunctionValue(Parameters rParameters)
  {
    KRATOS_TRY

    if (!rParameters.Has("value") && !rParameters.Has("modulus"))
    {
      KRATOS_ERROR << "no modulus or value in the supplied parameters for FunctionValue: \n" + rParameters.PrettyPrintJsonString();
    }
    else
    {
      mNumericValue = false;

      std::string value_name;
      if (rParameters.Has("value"))
      {
        value_name = "value";
      }
      else if (rParameters.Has("modulus"))
      {
        value_name = "modulus";
      }

      if (rParameters[value_name].IsNumber())
      {
        mNumericValue = true;
        mSpatialFunction = false;
        mValue = rParameters[value_name].GetDouble();
      }
      else
      {
        mFunctionExpression = rParameters[value_name].GetString();

        try
        {
          mScope = pybind11::module::import("__main__").attr("__dict__");
          pybind11::exec("from math import *", mScope);
        }
        catch (pybind11::error_already_set const &)
        {
          PyErr_Print();
        }

        mCurrentValue = false;
        if (mFunctionExpression.find(std::string("current")) == std::string::npos)
          mCurrentValue = true;

        mSpatialFunction = true;
        if (mFunctionExpression.find(std::string("x")) == std::string::npos &&
            mFunctionExpression.find(std::string("y")) == std::string::npos &&
            mFunctionExpression.find(std::string("z")) == std::string::npos)
        {
          mSpatialFunction = false;
        }
      }
    }

    if (rParameters.Has("direction"))
    {
      for (unsigned int i = 0; i < 3; ++i)
      {
        mDirection[i] = rParameters["direction"][i].GetDouble();
      }
      double norm = norm_2(mDirection);
      if (norm != 0)
        mDirection /= norm;
    }
    else
    {
      noalias(mDirection) = ZeroVector(3);
    }

    mSetCompiledFunction = false;

    KRATOS_CATCH("");
  }

  /// Destructor.
  virtual ~FunctionValue() {}

  ///@}
  ///@name Operations
  ///@{

  bool IsNumeric() const
  {
    return mNumericValue;
  }

  double GetValue() const
  {
    return mValue;
  }

  double IsCurrentValue()
  {
    return mCurrentValue;
  }

  pybind11::object GetFunction()
  {
    return mScope;
  }

  std::string GetFunctionExpression()
  {
    return mFunctionExpression;
  }

  void SetCompiledFunction(pybind11::object &pPyObject)
  {
    mPyObject = pPyObject;
    mSetCompiledFunction = true;
  }

  double IsSpatialFunction() const
  {
    if (IsNumeric())
      return false;

    return mSpatialFunction;
  }

  void GetGaussPoint(const unsigned int order, const double ipoint, double &point, double &weight)
  {
    switch (order)
    {
    case 1:
      weight = 2.0;
      point = 0.0;
      break;
    case 2:
      weight = 1.0;
      if (ipoint == 1)
        point = -sqrt(1.0 / 3.0);
      if (ipoint == 2)
        point = sqrt(1.0 / 3.0);
      break;
    case 3:
      weight = 5.0 / 9.0;
      if (ipoint == 1)
        point = -sqrt(3.0 / 5.0);
      if (ipoint == 2)
      {
        point = 0.0;
        weight = 8.0 / 9.0;
      }
      if (ipoint == 3)
        point = sqrt(3.0 / 5.0);
      break;
    default:
      KRATOS_ERROR << " Integration Order " << order << " not avaliable " << std::endl;
    }
  }

  double CallTimeFunctionIntegral(const double x, const double y, const double z, const double t0, const double t1, const unsigned int order)
  {
    double integral = 0;
    double a = 0.5 * (t1 - t0);
    double b = 0.5 * (t0 + t1);

    double point = 0;
    double weight = 0;
    for (unsigned int i = 1; i <= order; ++i)
    {
      this->GetGaussPoint(order, i, point, weight);
      integral += a * weight * this->CallFunction(x, y, z, a * point + b);
    }
    return integral;
  }

  double CallTimeFunctionDoubleIntegral(const double x, const double y, const double z, const double t0, const double t1, const unsigned int order)
  {
    double integral = 0;
    double a = 0.5 * (t1 - t0);
    double b = 0.5 * (t0 + t1);

    double point = 0;
    double weight = 0;
    for (unsigned int i = 1; i <= order; ++i)
    {
      this->GetGaussPoint(order, i, point, weight);
      integral += a * weight * this->CallTimeFunctionIntegral(x, y, z, t0, a * point + b, order);
    }
    return integral;
  }

  double CallFunction(const double x, const double y, const double z, const double t) const
  {
    pybind11::object Scope = mScope;
    Scope["x"] = x;
    Scope["y"] = y;
    Scope["z"] = z;
    Scope["t"] = t;

    if (mSetCompiledFunction)
      return mPyObject.attr(mFunctionExpression.c_str())(x, y, z, t).cast<double>();
    else
      return pybind11::eval(mFunctionExpression, Scope).cast<double>();
  }

  void LocalAxesTransform(const BoundedMatrix<double, 3, 3> &rRotationMatrix, const array_1d<double, 3> &rLocalOrigin, const double &x_global, const double y_global, const double &z_global, double &x_local, double &y_local, double &z_local)
  {
    array_1d<double, 3> xglobal;
    xglobal[0] = x_global;
    xglobal[1] = y_global;
    xglobal[2] = z_global;
    array_1d<double, 3> xlocal = prod(rRotationMatrix, (xglobal - rLocalOrigin));
    x_local = xlocal[0];
    y_local = xlocal[1];
    z_local = xlocal[2];
  }

  array_1d<double, 3> GetVectorValue(double &rValue)
  {
    return (mDirection * rValue);
  }

  array_1d<double, 3> GetDirection()
  {
    return mDirection;
  }

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{
  double mValue;

  std::string mFunctionExpression;
  pybind11::object mScope;

  bool mSetCompiledFunction;
  pybind11::object mPyObject;

  bool mNumericValue;
  bool mSpatialFunction;
  bool mCurrentValue;

  array_1d<double, 3> mDirection;

  ///@}
  ///@name Unaccessible methods
  ///@{
  ///@}

}; // Class FunctionValue

} // namespace Kratos.

#endif // KRATOS_FUNCTION_VALUE__HPP_INCLUDED
