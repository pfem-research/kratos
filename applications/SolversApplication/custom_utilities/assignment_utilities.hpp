//
//   Project Name:        KratosSolversApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:      January 2020 $
//
//

#if !defined(KRATOS_ASSIGNMENT_UTILITIES_HPP_INCLUDED)
#define KRATOS_ASSIGNMENT_UTILITIES_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "includes/kratos_parameters.h"

namespace Kratos
{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
 */
class AssignmentUtils
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of AssignmentUtils
  KRATOS_CLASS_POINTER_DEFINITION(AssignmentUtils);

  enum class AssignmentType
  {
    DIRECT,
    UNDIRECT,
    ADDITION,
    SUBTRACTION,
    MULTIPLICATION,
    DIVISION
  };

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  AssignmentUtils(){};

  /// Destructor.
  virtual ~AssignmentUtils() {}

  ///@}
  ///@name Operations
  ///@{

  static inline void SetAssignmentType(std::string method, AssignmentType &rAssignment)
  {
    //compound_assignment:

    //implemented:
    //  = direct
    // -= undirect
    // += addition
    // -= subtraction
    // *= multiplication
    // /= division

    if (method == "direct")
    {
      rAssignment = AssignmentType::DIRECT;
    }
    else if (method == "undirect")
    {
      rAssignment = AssignmentType::DIRECT;
    }
    else if (method == "addition")
    {
      rAssignment = AssignmentType::ADDITION;
    }
    else if (method == "subtraction")
    {
      rAssignment = AssignmentType::SUBTRACTION;
    }
    else if (method == "multiplication")
    {
      rAssignment = AssignmentType::MULTIPLICATION;
    }
    else if (method == "division")
    {
      rAssignment = AssignmentType::DIVISION;
    }
    else
    {
      KRATOS_ERROR << " Assignment type " << method << " is not supported " << std::endl;
    }
  }

  static inline void SetUnAssignmentType(std::string method, AssignmentType &rAssignment)
  {

    if (method == "direct")
    {
      rAssignment = AssignmentType::UNDIRECT;
    }
    else if (method == "addition")
    {
      rAssignment = AssignmentType::SUBTRACTION;
    }
    else if (method == "subtraction")
    {
      rAssignment = AssignmentType::ADDITION;
    }
    else if (method == "multiplication")
    {
      rAssignment = AssignmentType::DIVISION;
    }
    else if (method == "division")
    {
      rAssignment = AssignmentType::MULTIPLICATION;
    }
    else
    {
      KRATOS_ERROR << " Assignment type " << method << " is not supported " << std::endl;
    }
  }

  static inline std::string GetInverseAssigment(std::string compound_assignment)
  {
    if (compound_assignment == "direct")
      return std::string("undirect");
    if (compound_assignment == "addition")
      return std::string("subtraction");
    if (compound_assignment == "subtraction")
      return std::string("addition");
    if (compound_assignment == "multiplication")
      return std::string("division");
    if (compound_assignment == "division")
      return std::string("multiplication");

    return "";
  }

  template <class TMethodPointerType>
  static inline TMethodPointerType GetAssignmentMethod(AssignmentType& rAssignment)
  {
    TMethodPointerType AssignmentMethod = nullptr;
    switch (rAssignment)
    {
    case AssignmentType::DIRECT:
      AssignmentMethod = &DirectAssignValue;
      break;
    case AssignmentType::ADDITION:
      AssignmentMethod = &AddAssignValue;
      break;
    case AssignmentType::SUBTRACTION:
      AssignmentMethod = &SubtractAssignValue;
      break;
    case AssignmentType::MULTIPLICATION:
      AssignmentMethod = &MultiplyAssignValue;
      break;
    case AssignmentType::DIVISION:
      AssignmentMethod = &DivideAssignValue;
      break;
    default:
      KRATOS_ERROR << "Unexpected value for Assignment method " << std::endl;
    }
    return AssignmentMethod;
  }

  static inline void DirectAssignValue(double & rVariable, const double& rValue)
  {
    rVariable = rValue;
  }

  static inline void AddAssignValue(double & rVariable, const double& rValue)
  {
    rVariable += rValue;
  }

  static inline void SubtractAssignValue(double & rVariable, const double& rValue)
  {
    rVariable -= rValue;
  }

  static inline void MultiplyAssignValue(double & rVariable, const double& rValue)
  {
    rVariable *= rValue;
  }

  static inline void DivideAssignValue(double & rVariable, const double& rValue)
  {
    if (rValue != 0)
      rVariable /= rValue;
  }

}; // Class AssignmentUtils

} // namespace Kratos.

#endif // KRATOS_ASSIGNMENT_UTILITIES__HPP_INCLUDED
