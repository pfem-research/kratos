//
//   Project Name:        KratosSolversApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:        JMC  $
//   Date:                $Date:      January 2019 $
//
//

#if !defined(KRATOS_REDUCTION_BUILDER_AND_SOLVER_HPP_INCLUDED)
#define KRATOS_REDUCTION_BUILDER_AND_SOLVER_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "linear_system/system_builders/system_builder_and_solver.hpp"


namespace Kratos
{

///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

template <class TSparseSpace,
          class TDenseSpace,  //= DenseSpace<double>,
          class TLinearSolver //= LinearSolver<TSparseSpace,TDenseSpace>
          >
class ReductionBuilderAndSolver : public SystemBuilderAndSolver<TSparseSpace, TDenseSpace, TLinearSolver>
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of ReductionBuilderAndSolver
  KRATOS_CLASS_POINTER_DEFINITION(ReductionBuilderAndSolver);

  typedef SystemBuilderAndSolver<TSparseSpace, TDenseSpace, TLinearSolver> BaseType;

  typedef typename BaseType::SizeType SizeType;
  typedef typename BaseType::IndexType IndexType;

  typedef typename BaseType::Pointer BasePointerType;

  typedef typename BaseType::LocalFlagType LocalFlagType;
  typedef typename BaseType::DofsArrayType DofsArrayType;

  typedef typename BaseType::SystemMatrixType SystemMatrixType;
  typedef typename BaseType::SystemVectorType SystemVectorType;
  typedef typename BaseType::SystemMatrixPointerType SystemMatrixPointerType;
  typedef typename BaseType::SystemVectorPointerType SystemVectorPointerType;
  typedef typename BaseType::LocalSystemVectorType LocalSystemVectorType;
  typedef typename BaseType::LocalSystemMatrixType LocalSystemMatrixType;

  typedef typename ModelPart::NodesContainerType NodesContainerType;
  typedef typename ModelPart::ElementsContainerType ElementsContainerType;
  typedef typename ModelPart::ConditionsContainerType ConditionsContainerType;

  typedef typename BaseType::SchemePointerType SchemePointerType;
  typedef typename BaseType::LinearSolverPointerType LinearSolverPointerType;


  ///@}
  ///@name Life Cycle
  ///@{

  /// Default Constructor.
  ReductionBuilderAndSolver(LinearSolverPointerType pLinearSystemSolver)
      : BaseType(pLinearSystemSolver)
  {
  }

  /// Destructor.
  ~ReductionBuilderAndSolver() override
  {
  }

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /**
   * @brief Function to perform the build of the RHS.
   * @details The vector could be sized as the total number of dofs or as the number of non constrained ones
   */
  void BuildRHS(SchemePointerType pScheme,
                ModelPart &rModelPart,
                SystemVectorType &rb) override
  {
    KRATOS_TRY

    //resetting to zero the vector of reactions
    if (this->mOptions.Is(LocalFlagType::COMPUTE_REACTIONS))
      TSparseSpace::SetToZero(*(this->mpReactionsVector));

    BaseType::BuildRHS(pScheme, rModelPart, rb);

    KRATOS_CATCH("")
  }


  /**
   * @brief applies the dirichlet conditions.
   * @details This operation may be very heavy or completely
   * @details unexpensive depending on the implementation choosen and on how the System Matrix
   * @details is built. For explanation of how it works for a particular implementation the user
   * @details should refer to the particular Builder And Solver choosen
   */
  void ApplyDirichletConditions(SchemePointerType pScheme,
                                ModelPart &rModelPart,
                                SystemMatrixType &rA,
                                SystemVectorType &rDx,
                                SystemVectorType &rb) override
  {
  }

  /**
   * @brief Builds the list of the DofSets involved in the problem by "asking" to each element and condition its Dofs.
   * @details The list of dofs is stores insde the BuilderAndSolver as it is closely connected to the way the matrix and RHS are built
  */
  void SetUpDofSet(SchemePointerType pScheme,
                   ModelPart &rModelPart) override
  {
    KRATOS_TRY

    if (this->mEchoLevel > 1 && rModelPart.GetCommunicator().MyPID() == 0)
    {
      KRATOS_INFO("setting_dofs") << "SetUpDofSet starts" << std::endl;
    }

    //Gets the array of elements from the modeler
    ElementsContainerType &rElements = rModelPart.Elements();
    const int nelements = static_cast<int>(rElements.size());

    Element::DofsVectorType ElementalDofList;

    const ProcessInfo &rCurrentProcessInfo = rModelPart.GetProcessInfo();

    unsigned int nthreads = ParallelUtilities::GetNumThreads();

    typedef std::unordered_set<Node<3>::DofType::Pointer, DofPointerHasher> set_type;

    std::vector<set_type> dofs_aux_list(nthreads);

    if (this->mEchoLevel > 2)
    {
      KRATOS_INFO("setting_dofs") << "Number of threads:" << nthreads << std::endl;
    }

    for (int i = 0; i < static_cast<int>(nthreads); i++)
    {
      dofs_aux_list[i].reserve(nelements);
    }

    if (this->mEchoLevel > 2)
    {
      KRATOS_INFO("setting_dofs") << "initialize_elements" << std::endl;
    }

    #pragma omp parallel for firstprivate(nelements, ElementalDofList)
    for (int i = 0; i < static_cast<int>(nelements); i++)
    {
      typename ElementsContainerType::iterator it = rElements.begin() + i;

      const unsigned int this_thread_id = OpenMPUtils::ThisThread();
      // detect if the element is active or not (no set : active by default)
      if (((it->IsDefined(ACTIVE)) ? it->Is(ACTIVE) : true))
      {
        // gets list of Dof involved on every element
        pScheme->GetDofList(*(it.base()), ElementalDofList, rCurrentProcessInfo);

        dofs_aux_list[this_thread_id].insert(ElementalDofList.begin(), ElementalDofList.end());
      }
    }

    if (this->mEchoLevel > 2)
    {
      KRATOS_INFO("setting_dofs") << "initialize_conditions" << std::endl;
    }

    ConditionsContainerType &rConditions = rModelPart.Conditions();
    const int nconditions = static_cast<int>(rConditions.size());
    #pragma omp parallel for firstprivate(nconditions, ElementalDofList)
    for (int i = 0; i < nconditions; i++)
    {
      typename ConditionsContainerType::iterator it = rConditions.begin() + i;
      const unsigned int this_thread_id = OpenMPUtils::ThisThread();
      // detect if the element is active or not (no set : active by default)
      if (((it->IsDefined(ACTIVE)) ? it->Is(ACTIVE) : true))
      {
        // gets list of Dof involved on every condition
        pScheme->GetDofList(*(it.base()), ElementalDofList, rCurrentProcessInfo);
        dofs_aux_list[this_thread_id].insert(ElementalDofList.begin(), ElementalDofList.end());
      }
    }

    if (this->mEchoLevel > 2)
    {
      KRATOS_INFO("setting_dofs") << "initialize tree reduction" << std::endl;
    }

    //here we do a reduction in a tree so to have everything on thread 0
    unsigned int old_max = nthreads;
    unsigned int new_max = ceil(0.5 * static_cast<double>(old_max));
    while (new_max >= 1 && new_max != old_max)
    {
      if (this->mEchoLevel > 2)
      {
        //just for debugging
        KRATOS_INFO("setting_dofs") << "old_max" << old_max << " new_max:" << new_max << std::endl;
        for (int i = 0; i < static_cast<int>(new_max); i++)
        {
          if (i + new_max < old_max)
          {
            KRATOS_INFO("setting_dofs") << i << " - " << i + new_max << std::endl;
          }
        }
      }

      #pragma omp parallel for
      for (int i = 0; i < static_cast<int>(new_max); i++)
      {
        if (i + new_max < old_max)
        {
          dofs_aux_list[i].insert(dofs_aux_list[i + new_max].begin(), dofs_aux_list[i + new_max].end());
          dofs_aux_list[i + new_max].clear();
        }
      }

      old_max = new_max;
      new_max = ceil(0.5 * static_cast<double>(old_max));
    }

    if (this->mEchoLevel > 2)
    {
      KRATOS_INFO("setting_dofs") << "initializing ordered array filling" << std::endl;
    }

    DofsArrayType Doftemp;
    this->mDofSet = DofsArrayType();

    Doftemp.reserve(dofs_aux_list[0].size());
    for (auto it = dofs_aux_list[0].begin(); it != dofs_aux_list[0].end(); it++)
    {
      Doftemp.push_back(*it);
    }
    Doftemp.Sort();

    this->mDofSet = Doftemp;

    //Throws an exception if there are no Degrees Of Freedom involved in the analysis
    if (this->mDofSet.size() == 0)
    {
      KRATOS_ERROR << "No degrees of freedom!" << std::endl;
    }

    if (this->mEchoLevel > 2)
    {
      KRATOS_INFO("Dofs size") << this->mDofSet.size() << std::endl;
    }

    if (this->mEchoLevel > 2)
    {
      KRATOS_INFO("setting_dofs") << "End of setupdofset" << std::endl;
    }

    this->Set(LocalFlagType::DOFS_INITIALIZED, true);

    KRATOS_CATCH("");
  }

  /**
   * @brief organises the dofset in order to speed up the building phase
   */
  void SetUpSystem() override
  {
    // Set equation id for degrees of freedom
    // the free degrees of freedom are positioned at the beginning of the system,
    // while the fixed one are at the end (in opposite order).
    //
    // that means that if the EquationId is greater than "mEquationSystemSize"
    // the pointed degree of freedom is restrained
    //
    int free_id = 0;
    int fix_id = this->mDofSet.size();

    for (typename DofsArrayType::iterator dof_iterator = this->mDofSet.begin(); dof_iterator != this->mDofSet.end(); ++dof_iterator)
      if (dof_iterator->IsFixed())
        dof_iterator->SetEquationId(--fix_id);
      else
        dof_iterator->SetEquationId(free_id++);

    this->mEquationSystemSize = free_id;
  }

  /**
   * @brief Resizes and Initializes the system vectors and matrices after SetUpDofSet and SetUpSytem has been called
   */
  void SetUpSystemMatrices(SchemePointerType pScheme,
                           ModelPart &rModelPart,
                           SystemMatrixPointerType &pA,
                           SystemVectorPointerType &pDx,
                           SystemVectorPointerType &pb) override
  {
    KRATOS_TRY

    BaseType::SetUpSystemMatrices(pScheme, rModelPart, pA, pDx, pb);

    if (this->mpReactionsVector == nullptr) //if the pointer is not initialized initialize it to an empty matrix
    {
      SystemVectorPointerType pNewReactionsVector = Kratos::make_shared<SystemVectorType>(0);
      this->mpReactionsVector.swap(pNewReactionsVector);
    }

    //if needed resize the vector for the calculation of reactions
    if (this->mOptions.Is(LocalFlagType::COMPUTE_REACTIONS))
    {
      const SizeType ReactionsVectorSize = this->mDofSet.size() - this->mEquationSystemSize;
      if (this->mpReactionsVector->size() != ReactionsVectorSize)
        this->mpReactionsVector->resize(ReactionsVectorSize, false);
    }

    KRATOS_CATCH("")
  }

  /**
   * @brief Calculates system reactions
   * @details A flag controls if reactions must be calculated
   * @details An internal variable to store the reactions vector is needed
   */
  void CalculateReactions(SchemePointerType pScheme,
                          ModelPart &rModelPart,
                          SystemMatrixType &rA,
                          SystemVectorType &rDx,
                          SystemVectorType &rb) override
  {
    //refresh RHS to have the correct reactions
    this->BuildRHS(pScheme, rModelPart, rb);

    const int ndofs = static_cast<int>(this->mDofSet.size());

    SystemVectorType &ReactionsVector = *this->mpReactionsVector;

    #pragma omp parallel for firstprivate(ndofs)
    for (int k = 0; k < ndofs; ++k)
    {
      typename DofsArrayType::iterator dof_iterator = this->mDofSet.begin() + k;

      if ((dof_iterator)->IsFixed())
      {
        IndexType i = (dof_iterator)->EquationId();
        if (i >= this->mEquationSystemSize)
        {
          i -= this->mEquationSystemSize;
          (dof_iterator)->GetSolutionStepReactionValue() = -ReactionsVector[i];
        }
      }
      else
      {
        (dof_iterator)->GetSolutionStepReactionValue() = 0.0;
      }
    }
  }

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{


  void ConstructMatrixStructure(SchemePointerType pScheme,
                                SystemMatrixType &rA,
                                ElementsContainerType &rElements,
                                ConditionsContainerType &rConditions,
                                const ProcessInfo &rCurrentProcessInfo) override
  {
    //filling with zero the matrix (creating the structure)
    BuiltinTimer construct_time;

    const SizeType equation_size = this->mEquationSystemSize;

    std::vector<std::unordered_set<IndexType>> indices(equation_size);

    #pragma omp parallel for firstprivate(equation_size)
    for (int iii = 0; iii < static_cast<int>(equation_size); iii++)
    {
      indices[iii].reserve(40);
    }

    Element::EquationIdVectorType ids(3, 0);
    #pragma omp parallel firstprivate(ids)
    {
      // We repeat the same declaration for each thead
      std::vector<std::unordered_set<IndexType> > temp_indexes(equation_size);

      #pragma omp for
      for (int index = 0; index < static_cast<int>(equation_size); ++index)
        temp_indexes[index].reserve(30);

      // Getting the size of the array of elements from the model
      const int nelements = static_cast<int>(rElements.size());
      // We iterate over the elements
      #pragma omp for schedule(guided, 512) nowait
      for (int iii = 0; iii < nelements; iii++){
        typename ElementsContainerType::iterator it = rElements.begin() + iii;
        // detect if the element is active or not (no set : active by default)
        if (((it->IsDefined(ACTIVE)) ? it->Is(ACTIVE) : true))
        {
          pScheme->EquationId(*(it.base()), ids, rCurrentProcessInfo);

          for (auto& id_i : ids) {
            if (id_i < this->mEquationSystemSize) {
              auto& row_indices = temp_indexes[id_i];
              for (auto& id_j : ids)
                if (id_j < this->mEquationSystemSize)
                  row_indices.insert(id_j);
            }
          }
        }
      }

      // Getting the size of the array of the conditions
      const int nconditions = static_cast<int>(rConditions.size());
      // We iterate over the conditions
      #pragma omp for schedule(guided, 512) nowait
      for (int iii = 0; iii < nconditions; iii++){
        typename ConditionsContainerType::iterator it = rConditions.begin() + iii;
        // detect if the element is active or not (no set : active by default)
        if (((it->IsDefined(ACTIVE)) ? it->Is(ACTIVE) : true))
        {
          pScheme->EquationId(*(it.base()), ids, rCurrentProcessInfo);

          for (auto& id_i : ids) {
            if (id_i < BaseType::mEquationSystemSize) {
              auto& row_indices = temp_indexes[id_i];
              for (auto& id_j : ids)
                if (id_j < BaseType::mEquationSystemSize)
                  row_indices.insert(id_j);
            }
          }
        }
      }

      // Merging all the temporal indexes
      #pragma omp critical
      {
        for (int i = 0; i < static_cast<int>(temp_indexes.size()); ++i) {
          indices[i].insert(temp_indexes[i].begin(), temp_indexes[i].end());
        }
      }
    }

    //count the row sizes
    unsigned int nnz = 0;
    for (unsigned int i = 0; i < indices.size(); i++)
      nnz += indices[i].size();

    rA = SystemMatrixType(indices.size(), indices.size(), nnz);

    double *Avalues = rA.value_data().begin();
    IndexType *Arow_indices = rA.index1_data().begin();
    IndexType *Acol_indices = rA.index2_data().begin();

    //filling the index1 vector - DO NOT MAKE PARALLEL THE FOLLOWING LOOP!
    Arow_indices[0] = 0;
    for (int i = 0; i < rA.size1(); ++i)
      Arow_indices[i + 1] = Arow_indices[i] + indices[i].size();

    #pragma omp parallel for
    for (int i = 0; i < static_cast<int>(rA.size1()); i++)
    {
      const unsigned int row_begin = Arow_indices[i];
      const unsigned int row_end = Arow_indices[i + 1];
      unsigned int k = row_begin;
      for (auto it = indices[i].begin(); it != indices[i].end(); it++)
      {
        Acol_indices[k] = *it;
        Avalues[k] = 0.0;
        k++;
      }

      indices[i].clear(); //deallocating the memory

      std::sort(&Acol_indices[row_begin], &Acol_indices[row_end]);
    }

    rA.set_filled(indices.size() + 1, nnz);

    if (this->mEchoLevel >= 2)
      KRATOS_INFO("BlockBuilderAndSolver") << "construct matrix structure time:" << construct_time.ElapsedSeconds() << "\n"
                                           << LoggerMessage::Category::STATISTICS;
  }

  bool CheckFreeDof(unsigned int &i_global) override
  {
    if (i_global < this->mEquationSystemSize)
      return true;
    return false;
  }

  void AssembleRHS(SystemVectorType &rb,
                   const LocalSystemVectorType &rRHS_Contribution,
                   const Element::EquationIdVectorType &rEquationId) override
  {
    if (this->mOptions.IsNot(LocalFlagType::COMPUTE_REACTIONS))
    {
      BaseType::AssembleRHS(rb, rRHS_Contribution, rEquationId);
    }
    else
    {
      unsigned int local_size = rRHS_Contribution.size();

      SystemVectorType &ReactionsVector = *this->mpReactionsVector;
      for (unsigned int i_local = 0; i_local < local_size; i_local++)
      {
        const unsigned int i_global = rEquationId[i_local];

        if (i_global < this->mEquationSystemSize) //free dof
        {
          #pragma omp atomic
          rb[i_global] += rRHS_Contribution[i_local];
        }
        else //fixed dof
        {
          #pragma omp atomic
          ReactionsVector[i_global - this->mEquationSystemSize] += rRHS_Contribution[i_local];
        }
      }
    }
  }

  /**
   * @brief This function is equivalent to the AssembleRowContribution of the block builder and solver
   * @note The main difference respect the block builder and solver is the fact that the fixed DoFs are skipped
   */
  inline virtual void AssembleRowContribution(SystemMatrixType &rA,
                                              const Matrix &rLHS_Contribution,
                                              const unsigned int i,
                                              const unsigned int i_local,
                                              Element::EquationIdVectorType &rEquationId) final
  {
    double* values_vector = rA.value_data().begin();
    IndexType* index1_vector = rA.index1_data().begin();
    IndexType* index2_vector = rA.index2_data().begin();

    const IndexType left_limit = index1_vector[i];

    // Find the first entry
    // We iterate over the equation ids until we find the first equation id to be considered
    // We count in which component we find an ID
    IndexType last_pos = 0;
    IndexType last_found = 0;
    IndexType counter = 0;
    for(IndexType j=0; j < rEquationId.size(); ++j) {
      ++counter;
      const IndexType j_global = rEquationId[j];
      if (j_global < this->mEquationSystemSize) {
        last_pos = this->ForwardFind(j_global,left_limit,index2_vector);
        last_found = j_global;
        break;
      }
    }

    // If the counter is equal to the size of the EquationID vector that means that only one dof will be considered, if the number is greater means that all the dofs are fixed. If the number is below means that at we have several dofs free to be considered
    if (counter <= rEquationId.size()) {
      #pragma omp atomic
      values_vector[last_pos] += rLHS_Contribution(i_local,counter - 1);

      // Now find all of the other entries
      IndexType pos = 0;
      for(IndexType j = counter; j < rEquationId.size(); ++j) {
        IndexType id_to_find = rEquationId[j];
        if (id_to_find < this->mEquationSystemSize) {
          if(id_to_find > last_found)
            pos = this->ForwardFind(id_to_find,last_pos+1,index2_vector);
          else if(id_to_find < last_found)
            pos = this->BackwardFind(id_to_find,last_pos-1,index2_vector);
          else
            pos = last_pos;

          #pragma omp atomic
          values_vector[pos] += rLHS_Contribution(i_local,j);

          last_found = id_to_find;
          last_pos = pos;
        }
      }
    }
  }
  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Serialization
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{
  ///@}

}; // Class ReductionBuilderAndSolver
///@}

///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
///@}

///@} addtogroup block

} // namespace Kratos

#endif // KRATOS_REDUCTION_BUILDER_AND_SOLVER_HPP_INCLUDED  defined
