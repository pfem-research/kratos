//
//   Project Name:        KratosSolversApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:        JMC  $
//   Date:                $Date:      January 2019 $
//
//

#if !defined(KRATOS_BLOCK_BUILDER_AND_SOLVER_HPP_INCLUDED)
#define KRATOS_BLOCK_BUILDER_AND_SOLVER_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "linear_system/system_builders/system_builder_and_solver.hpp"

namespace Kratos
{

///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/** @brief System Buider and Solver based on block matrix
 *  @details This is builder class the system is not reduced, 1 is set in the diagonal
 */
template <class TSparseSpace,
          class TDenseSpace,  //= DenseSpace<double>,
          class TLinearSolver //= LinearSolver<TSparseSpace,TDenseSpace>
          >
class BlockBuilderAndSolver : public SystemBuilderAndSolver<TSparseSpace, TDenseSpace, TLinearSolver>
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of BlockBuilderAndSolver
  KRATOS_CLASS_POINTER_DEFINITION(BlockBuilderAndSolver);

  /// Definition of the base class
  typedef SystemBuilderAndSolver<TSparseSpace, TDenseSpace, TLinearSolver> BaseType;

  /// Definition of the classes from the base class
  typedef typename BaseType::SizeType SizeType;
  typedef typename BaseType::IndexType IndexType;

  typedef typename BaseType::Pointer BasePointerType;

  typedef typename BaseType::LocalFlagType LocalFlagType;
  typedef typename BaseType::DofsArrayType DofsArrayType;

  typedef typename BaseType::SystemMatrixType SystemMatrixType;
  typedef typename BaseType::SystemVectorType SystemVectorType;
  typedef typename BaseType::SystemMatrixPointerType SystemMatrixPointerType;
  typedef typename BaseType::SystemVectorPointerType SystemVectorPointerType;
  typedef typename BaseType::LocalSystemVectorType LocalSystemVectorType;
  typedef typename BaseType::LocalSystemMatrixType LocalSystemMatrixType;

  typedef typename ModelPart::NodesContainerType NodesContainerType;
  typedef typename ModelPart::ElementsContainerType ElementsContainerType;
  typedef typename ModelPart::ConditionsContainerType ConditionsContainerType;

  typedef typename BaseType::SchemePointerType SchemePointerType;
  typedef typename BaseType::LinearSolverPointerType LinearSolverPointerType;

  /// DoF types definition
  typedef Node<3> NodeType;
  typedef typename NodeType::DofType DofType;
  typedef typename DofType::Pointer DofPointerType;

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default Constructor.
  BlockBuilderAndSolver(LinearSolverPointerType pLinearSystemSolver)
      : BaseType(pLinearSystemSolver)
  {
  }

  /// Destructor.
  ~BlockBuilderAndSolver() override
  {
  }

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{


  /**
   * @brief Function to perform the build of the RHS.
   * @details The vector could be sized as the total number of dofs or as the number of non constrained ones
   */
  void BuildRHS(SchemePointerType pScheme,
                ModelPart &rModelPart,
                SystemVectorType &rb) override
  {
    KRATOS_TRY

    BaseType::BuildRHS(pScheme, rModelPart, rb);

    const int ndofs = static_cast<int>(this->mDofSet.size());

    //NOTE: dofs are assumed to be numbered consecutively in the BlockBuilderAndSolver
#pragma omp parallel for firstprivate(ndofs)
    for (int k = 0; k < ndofs; k++)
    {
      typename DofsArrayType::iterator dof_iterator = this->mDofSet.begin() + k;
      const IndexType i = dof_iterator->EquationId();

      if (dof_iterator->IsFixed())
        rb[i] = 0.0;
    }

    KRATOS_CATCH("")
  }

  /**
   * @brief applies the dirichlet conditions.
   * @details This operation may be very heavy or completely
   * @details unexpensive depending on the implementation choosen and on how the System Matrix
   * @details is built. For explanation of how it works for a particular implementation the user
   * @details should refer to the particular Builder And Solver choosen
   */
  void ApplyDirichletConditions(SchemePointerType pScheme,
                                ModelPart &rModelPart,
                                SystemMatrixType &rA,
                                SystemVectorType &rDx,
                                SystemVectorType &rb) override
  {
    const std::size_t system_size = rA.size1();
    Vector scaling_factors (system_size);

    const auto it_dof_iterator_begin = BaseType::mDofSet.begin();
    const int ndofs = static_cast<int>(BaseType::mDofSet.size());

    // NOTE: dofs are assumed to be numbered consecutively in the BlockBuilderAndSolver
    #pragma omp parallel for firstprivate(ndofs)
    for (int k = 0; k<ndofs; k++) {
      auto it_dof_iterator = it_dof_iterator_begin + k;
      if (it_dof_iterator->IsFixed()) {
        scaling_factors[k] = 0.0;
      } else {
        scaling_factors[k] = 1.0;
      }
    }

    double* Avalues = rA.value_data().begin();
    std::size_t* Arow_indices = rA.index1_data().begin();
    std::size_t* Acol_indices = rA.index2_data().begin();

    // Detect if there is a line of all zeros and set the diagonal to a 1 if this happens
    #pragma omp parallel firstprivate(system_size)
    {
      std::size_t col_begin = 0, col_end  = 0;
      bool empty = true;

      #pragma omp for
      for (int k = 0; k < static_cast<int>(system_size); ++k) {
        col_begin = Arow_indices[k];
        col_end = Arow_indices[k + 1];
        empty = true;
        for (std::size_t j = col_begin; j < col_end; ++j) {
          if(Avalues[j] != 0.0) {
            empty = false;
            break;
          }
        }

        if(empty) {
          rA(k, k) = 1.0;
          rb[k] = 0.0;
        }
      }
    }

    #pragma omp parallel for firstprivate(system_size)
    for (int k = 0; k < static_cast<int>(system_size); ++k) {
      std::size_t col_begin = Arow_indices[k];
      std::size_t col_end = Arow_indices[k+1];
      const double k_factor = scaling_factors[k];
      if (k_factor == 0.0) {
        // Zero out the whole row, except the diagonal
        for (std::size_t j = col_begin; j < col_end; ++j)
          if (static_cast<int>(Acol_indices[j]) != k )
            Avalues[j] = 0.0;

        // Zero out the RHS
        rb[k] = 0.0;
      } else {
        // Zero out the column which is associated with the zero'ed row
        for (std::size_t j = col_begin; j < col_end; ++j)
          if(scaling_factors[ Acol_indices[j] ] == 0 )
            Avalues[j] = 0.0;
      }
    }
  }

  /**
   * @brief Builds the list of the DofSets involved in the problem by "asking" to each element and condition its Dofs.
   * @details The list of dofs is stores insde the BuilderAndSolver as it is closely connected to the way the matrix and RHS are built
  */
  void SetUpDofSet(SchemePointerType pScheme,
                   ModelPart &rModelPart) override
  {
    KRATOS_TRY

    if (this->mEchoLevel > 1 && rModelPart.GetCommunicator().MyPID() == 0)
    {
      KRATOS_INFO("setting_dofs") << "SetUpDofSet starts" << std::endl;
    }

    //Gets the array of elements from the modeler
    ElementsContainerType &rElements = rModelPart.Elements();
    const int nelements = static_cast<int>(rElements.size());

    Element::DofsVectorType dof_list;

    const ProcessInfo &rCurrentProcessInfo = rModelPart.GetProcessInfo();

    unsigned int nthreads = ParallelUtilities::GetNumThreads();

    typedef std::unordered_set<Node<3>::DofType::Pointer, DofPointerHasher> set_type;

    if (this->mEchoLevel > 2)
    {
      KRATOS_INFO("setting_dofs") << "Number of threads:" << nthreads << std::endl;
    }
    /**
     * Here we declare:
     * - The global set: Contains all the DoF of the system
     */
    set_type dof_global_set;
    dof_global_set.reserve(nelements*20);

    #pragma omp parallel firstprivate(nelements, dof_list)
    {
      const ProcessInfo& rCurrentProcessInfo = rModelPart.GetProcessInfo();

      // We create the temporal set and we reserve some space for them
      set_type dofs_tmp_set;
      dofs_tmp_set.reserve(20000);

      // Gets the array of elements from the modeler
      #pragma omp for schedule(guided, 512) nowait
      for (int i = 0; i < nelements; ++i) {
        typename ElementsContainerType::iterator it = rElements.begin() + i;
        // detect if the element is active or not (no set : active by default)
        if (((it->IsDefined(ACTIVE)) ? it->Is(ACTIVE) : true))
        {
          // Gets list of Dof involved on every element
          pScheme->GetDofList(*(it.base()), dof_list, rCurrentProcessInfo);
          dofs_tmp_set.insert(dof_list.begin(), dof_list.end());
        }
      }
      // Gets the array of conditions from the modeler
      ConditionsContainerType &rConditions = rModelPart.Conditions();
      const int nconditions = static_cast<int>(rConditions.size());
      #pragma omp for  schedule(guided, 512) nowait
      for (int i = 0; i < nconditions; ++i) {
        typename ConditionsContainerType::iterator it = rConditions.begin() + i;
        // detect if the element is active or not (no set : active by default)
        if (((it->IsDefined(ACTIVE)) ? it->Is(ACTIVE) : true))
        {
          // Gets list of Dof involved on every element
          pScheme->GetDofList(*(it.base()), dof_list, rCurrentProcessInfo);
          dofs_tmp_set.insert(dof_list.begin(), dof_list.end());
        }
      }

      // We merge all the sets in one thread
      #pragma omp critical
      {
        dof_global_set.insert(dofs_tmp_set.begin(), dofs_tmp_set.end());
      }
    }

    if (this->mEchoLevel > 2)
    {
      KRATOS_INFO("setting_dofs") << "initializing ordered array filling" << std::endl;
    }

    DofsArrayType Doftemp;
    this->mDofSet = DofsArrayType();

    Doftemp.reserve(dof_global_set.size());
    for (auto it= dof_global_set.begin(); it!= dof_global_set.end(); it++)
    {
      Doftemp.push_back( *it );
    }
    Doftemp.Sort();

    this->mDofSet = Doftemp;

    //Throws an exception if there are no Degrees Of Freedom involved in the analysis
    if (this->mDofSet.size() == 0)
    {
      KRATOS_ERROR << "No degrees of freedom!" << std::endl;
    }
    if (this->mEchoLevel > 2)
    {
      KRATOS_INFO("Dofs size") << this->mDofSet.size() << std::endl;
    }

    if (this->mEchoLevel > 2 && rModelPart.GetCommunicator().MyPID() == 0)
    {
      KRATOS_INFO("setting_dofs") << "Finished setting up the dofs" << std::endl;
    }


    if (this->mEchoLevel > 2)
    {
      KRATOS_INFO("setting_dofs") << "End of setupdofset" << std::endl;
    }

    this->Set(LocalFlagType::DOFS_INITIALIZED, true);

    KRATOS_CATCH("")
  }

  /**
   * @brief organises the dofset in order to speed up the building phase
   */
  void SetUpSystem() override
  {

    //int free_id = 0;
    this->mEquationSystemSize = this->mDofSet.size();
    int ndofs = static_cast<int>(this->mDofSet.size());

    #pragma omp parallel for firstprivate(ndofs)
    for (int i = 0; i < static_cast<int>(ndofs); i++)
    {
      typename DofsArrayType::iterator dof_iterator = this->mDofSet.begin() + i;
      dof_iterator->SetEquationId(i);
    }
  }

  /**
   * @brief Calculates system reactions
   * @details A flag controls if reactions must be calculated
   * @details An internal variable to store the reactions vector is needed
   */
  void CalculateReactions(SchemePointerType pScheme,
                          ModelPart &rModelPart,
                          SystemMatrixType &rA,
                          SystemVectorType &rDx,
                          SystemVectorType &rb) override
  {
    TSparseSpace::SetToZero(rb);

    //refresh RHS to have the correct reactions
    BaseType::BuildRHS(pScheme, rModelPart, rb);

    const int ndofs = static_cast<int>(this->mDofSet.size());

    //NOTE: dofs are assumed to be numbered consecutively in the BlockBuilderAndSolver
    #pragma omp parallel for firstprivate(ndofs)
    for (int k = 0; k < ndofs; ++k)
    {
      typename DofsArrayType::iterator dof_iterator = this->mDofSet.begin() + k;

      if ((dof_iterator)->IsFixed())
      {
        const IndexType i = (dof_iterator)->EquationId();
        (dof_iterator)->GetSolutionStepReactionValue() = -rb[i];
      }
      else
      {
        (dof_iterator)->GetSolutionStepReactionValue() = 0.0;
      }
    }
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{

  void ConstructMatrixStructure(SchemePointerType pScheme,
                                SystemMatrixType &rA,
                                ElementsContainerType &rElements,
                                ConditionsContainerType &rConditions,
                                const ProcessInfo &rCurrentProcessInfo) override
  {
    //filling with zero the matrix (creating the structure)
    BuiltinTimer construct_time;

    const SizeType equation_size = this->mEquationSystemSize;
    std::vector< LockObject > lock_array(equation_size);

    std::vector<std::unordered_set<IndexType>> indices(equation_size);
    #pragma omp parallel for firstprivate(equation_size)
    for (int iii = 0; iii < static_cast<int>(equation_size); iii++)
      indices[iii].reserve(40);

    Element::EquationIdVectorType ids(3, 0);

    const int nelements = static_cast<int>(rElements.size());
    #pragma omp parallel for firstprivate(nelements, ids)
    for (int iii = 0; iii < nelements; iii++)
    {
      typename ElementsContainerType::iterator it = rElements.begin() + iii;
      // detect if the element is active or not (no set : active by default)
      if (((it->IsDefined(ACTIVE)) ? it->Is(ACTIVE) : true))
      {
        pScheme->EquationId(*(it.base()), ids, rCurrentProcessInfo);
        for (IndexType i = 0; i < ids.size(); i++)
        {
          lock_array[ids[i]].lock();
          auto &row_indices = indices[ids[i]];
          row_indices.insert(ids.begin(), ids.end());
          lock_array[ids[i]].unlock();
        }
      }
    }
    const int nconditions = static_cast<int>(rConditions.size());
    #pragma omp parallel for firstprivate(nconditions, ids)
    for (int iii = 0; iii < nconditions; iii++)
    {
      typename ConditionsContainerType::iterator it = rConditions.begin() + iii;
      // detect if the element is active or not (no set : active by default)
      if (((it->IsDefined(ACTIVE)) ? it->Is(ACTIVE) : true))
      {
        pScheme->EquationId(*(it.base()), ids, rCurrentProcessInfo);
        for (IndexType i = 0; i < ids.size(); i++)
        {
          lock_array[ids[i]].lock();
          auto &row_indices = indices[ids[i]];
          row_indices.insert(ids.begin(), ids.end());
          lock_array[ids[i]].unlock();
        }
      }
    }

    //destroy locks
    lock_array = std::vector< LockObject >();

    //count the row sizes
    unsigned int nnz = 0;
    for (unsigned int i = 0; i < indices.size(); i++)
      nnz += indices[i].size();

    rA = CompressedMatrix(indices.size(), indices.size(), nnz);

    double *Avalues = rA.value_data().begin();
    IndexType *Arow_indices = rA.index1_data().begin();
    IndexType *Acol_indices = rA.index2_data().begin();

    //filling the index1 vector - DO NOT MAKE PARALLEL THE FOLLOWING LOOP!
    Arow_indices[0] = 0;
    for (int i = 0; i < static_cast<int>(rA.size1()); i++)
      Arow_indices[i + 1] = Arow_indices[i] + indices[i].size();

    #pragma omp parallel for
    for (int i = 0; i < static_cast<int>(rA.size1()); i++)
    {
      const unsigned int row_begin = Arow_indices[i];
      const unsigned int row_end = Arow_indices[i + 1];
      unsigned int k = row_begin;
      for (auto it = indices[i].begin(); it != indices[i].end(); it++)
      {
        Acol_indices[k] = *it;
        Avalues[k] = 0.0;
        k++;
      }

      indices[i].clear(); //deallocating the memory

      std::sort(&Acol_indices[row_begin], &Acol_indices[row_end]);
    }

    rA.set_filled(indices.size() + 1, nnz);

    if (this->mEchoLevel >= 2)
      KRATOS_INFO("BlockBuilderAndSolver") << "construct matrix structure time:" << construct_time.ElapsedSeconds() << std::endl;
  }

  inline virtual void AssembleRowContribution(SystemMatrixType &rA,
                                              const Matrix &rLHS_Contribution,
                                              const unsigned int i,
                                              const unsigned int i_local,
                                              Element::EquationIdVectorType &rEquationId) final
  {
    double *values_vector = rA.value_data().begin();
    IndexType *index1_vector = rA.index1_data().begin();
    IndexType *index2_vector = rA.index2_data().begin();

    size_t left_limit = index1_vector[i];
    //	size_t right_limit = index1_vector[i+1];

    //find the first entry
    size_t last_pos = this->ForwardFind(rEquationId[0], left_limit, index2_vector);
    size_t last_found = rEquationId[0];

    //#pragma omp parallel reduction (+:values_vector[last_pos])
    #pragma omp atomic
    values_vector[last_pos] += rLHS_Contribution(i_local, 0);

    //now find all of the other entries
    size_t pos = 0;
    for (unsigned int j = 1; j < rEquationId.size(); j++)
    {
      unsigned int id_to_find = rEquationId[j];
      if (id_to_find > last_found)
        pos = this->ForwardFind(id_to_find, last_pos + 1, index2_vector);
      else if(id_to_find < last_found)
        pos = this->BackwardFind(id_to_find, last_pos - 1, index2_vector);
      else
        pos = last_pos;

      //#pragma omp parallel reduction (+:values_vector[pos])
      #pragma omp atomic
      values_vector[pos] += rLHS_Contribution(i_local, j);

      last_found = id_to_find;
      last_pos = pos;
    }
  }

  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Serialization
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{
  ///@}

}; // Class BlockBuilderAndSolver
///@}

///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
///@}

///@} addtogroup block

} // namespace Kratos

#endif // KRATOS_BLOCK_BUILDER_AND_SOLVER_HPP_INCLUDED  defined
