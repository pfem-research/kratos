//
//   Project Name:        KratosSolversApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:        JMC  $
//   Date:                $Date:      January 2019 $
//
//

#if !defined(KRATOS_SYSTEM_BUILDER_AND_SOLVER_HPP_INCLUDED)
#define KRATOS_SYSTEM_BUILDER_AND_SOLVER_HPP_INCLUDED

// System includes
#include <unordered_set>

// External includes

// Project includes
#include "custom_solvers/solution_schemes/solution_scheme.hpp"
#include "includes/key_hash.h"


namespace Kratos
{

///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/** Short class definition.
    Current class provides an implementation for standard builder and solving operations.
    the RHS is constituted by the unbalanced loads (residual)

    Degrees of freedom are reordered putting the restrained degrees of freedom at
    the end of the system ordered in reverse order with respect to the DofSet.

    Imposition of the dirichlet conditions is naturally dealt with as the residual already contains this information.
 */

/** @brief System Buider and Solver base class
 *  @details This is the base class for the building and solving the system
 */
template <class TSparseSpace,
          class TDenseSpace,  // = DenseSpace<double>,
          class TLinearSolver //= LinearSolver<TSparseSpace,TDenseSpace>
          >
class SystemBuilderAndSolver : public Flags
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of SystemBuilderAndSolver
  KRATOS_CLASS_POINTER_DEFINITION(SystemBuilderAndSolver);

  // The size_t types
  typedef std::size_t SizeType;
  typedef std::size_t IndexType;

  typedef SolverLocalFlags LocalFlagType;

  typedef ModelPart::DofsArrayType DofsArrayType;
  typedef typename ModelPart::NodesContainerType NodesContainerType;
  typedef typename ModelPart::ElementsContainerType ElementsContainerType;
  typedef typename ModelPart::ConditionsContainerType ConditionsContainerType;

  typedef typename TSparseSpace::MatrixType SystemMatrixType;
  typedef typename TSparseSpace::VectorType SystemVectorType;

  typedef typename TSparseSpace::MatrixPointerType SystemMatrixPointerType;
  typedef typename TSparseSpace::VectorPointerType SystemVectorPointerType;

  typedef typename TDenseSpace::MatrixType LocalSystemMatrixType;
  typedef typename TDenseSpace::VectorType LocalSystemVectorType;

  typedef SolutionScheme<TSparseSpace, TDenseSpace> SchemeType;
  typedef typename SchemeType::Pointer SchemePointerType;

  typedef typename TLinearSolver::Pointer LinearSolverPointerType;

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default Constructor.
  SystemBuilderAndSolver() : Flags()
  {
    this->Set(LocalFlagType::DOFS_INITIALIZED, false);

    mpLinearSystemSolver = nullptr;
    mEchoLevel = 0;
  }

  /// Constructor.
  SystemBuilderAndSolver(LinearSolverPointerType pLinearSystemSolver) : Flags()
  {
    this->Set(LocalFlagType::DOFS_INITIALIZED, false);

    mpLinearSystemSolver = pLinearSystemSolver;
    mEchoLevel = 0;
  }

  /// Destructor.
  ~SystemBuilderAndSolver() override {}

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /**
   * @brief Function to perform the building of the LHS,
   * @details Depending on the implementation choosen the size of the matrix could be
   * @details equal to the total number of Dofs or to the number of non constrained dofs
   */
  virtual void BuildLHS(SchemePointerType pScheme,
                        ModelPart &rModelPart,
                        SystemMatrixType &rA)
  {
    KRATOS_TRY

    if (!pScheme)
      KRATOS_ERROR << "No scheme provided!" << std::endl;

    //getting the number of elements
    const int nelements = static_cast<int>(rModelPart.Elements().size());

    //getting the number of conditions
    const int nconditions = static_cast<int>(rModelPart.Conditions().size());

    const ProcessInfo &rCurrentProcessInfo = rModelPart.GetProcessInfo();
    ModelPart::ElementsContainerType::iterator el_begin = rModelPart.ElementsBegin();
    ModelPart::ConditionsContainerType::iterator cond_begin = rModelPart.ConditionsBegin();

    //contributions to the system
    LocalSystemMatrixType LHS_Contribution = LocalSystemMatrixType(0, 0);

    //vector containing the localization in the system of the different
    //terms
    Element::EquationIdVectorType EquationId;

    // assemble all elements
    BuiltinTimer build_time;

     #pragma omp parallel firstprivate(nelements, nconditions, LHS_Contribution, EquationId)
    {
      #pragma omp for schedule(guided, 512) nowait
      for (int k = 0; k < nelements; ++k)
      {
        typename ElementsContainerType::iterator it = el_begin + k;
        // detect if the element is active or not (no set : active by default)
        if (((it->IsDefined(ACTIVE)) ? it->Is(ACTIVE) : true))
        {
          //calculate elemental contribution
          pScheme->Calculate_LHS_Contribution(*(it.base()), LHS_Contribution, EquationId, rCurrentProcessInfo);

          this->AssembleLHS(rA, LHS_Contribution, EquationId);
        }
      }

      #pragma omp for schedule(guided, 512)
      for (int k = 0; k < nconditions; ++k)
      {
        typename ConditionsContainerType::iterator it = cond_begin + k;
        // detect if the element is active or not (no set : active by default)
        if (((it->IsDefined(ACTIVE)) ? it->Is(ACTIVE) : true))
        {
          //calculate elemental contribution
          pScheme->Calculate_LHS_Contribution(*(it.base()), LHS_Contribution, EquationId, rCurrentProcessInfo);

          this->AssembleLHS(rA, LHS_Contribution, EquationId);
        }
      }
    }

    KRATOS_CATCH("")
  }

  /**
   * @brief Function to perform the build of the RHS.
   * @details The vector could be sized as the total number of dofs or as the number of non constrained ones
   */
  virtual void BuildRHS(SchemePointerType pScheme,
                        ModelPart &rModelPart,
                        SystemVectorType &rb)
  {
    KRATOS_TRY

    if (!pScheme)
      KRATOS_ERROR << "No scheme provided!" << std::endl;

    const ProcessInfo &rCurrentProcessInfo = rModelPart.GetProcessInfo();

    //getting the number of elements
    const int nelements = static_cast<int>(rModelPart.Elements().size());

    //getting the number of conditions
    const int nconditions = static_cast<int>(rModelPart.Conditions().size());

    ModelPart::ElementsContainerType::iterator elem_begin = rModelPart.ElementsBegin();
    ModelPart::ConditionsContainerType::iterator cond_begin = rModelPart.ConditionsBegin();

    //contributions to the rhs
    LocalSystemVectorType RHS_Contribution = LocalSystemVectorType(0);

    //vector containing the localization in the system of the different terms
    Element::EquationIdVectorType EquationId;

    // assemble all elements
    #pragma omp parallel firstprivate(nelements, nconditions, RHS_Contribution, EquationId)
    {
      #pragma omp for schedule(guided, 512) nowait
      for (int k = 0; k < nelements; ++k)
      {
        typename ElementsContainerType::iterator it = elem_begin + k;
        // detect if the element is active or not (no set : active by default)
        if (((it->IsDefined(ACTIVE)) ? it->Is(ACTIVE) : true))
        {
          //calculate elemental Right Hand Side Contribution
          pScheme->Calculate_RHS_Contribution(*(it.base()), RHS_Contribution, EquationId, rCurrentProcessInfo);

          //assemble the elemental contribution
          this->AssembleRHS(rb, RHS_Contribution, EquationId);
        }
      }

      // assemble all conditions
      #pragma omp for schedule(guided, 512)
      for (int k = 0; k < nconditions; ++k)
      {
        typename ConditionsContainerType::iterator it = cond_begin + k;
        // detect if the element is active or not (no set : active by default)
        if (((it->IsDefined(ACTIVE)) ? it->Is(ACTIVE) : true))
        {
          //calculate elemental contribution
          pScheme->Calculate_RHS_Contribution(*(it.base()), RHS_Contribution, EquationId, rCurrentProcessInfo);

          this->AssembleRHS(rb, RHS_Contribution, EquationId);
        }
      }
    }

    KRATOS_CATCH("")
  }


  /**
   * @brief Function to perform the building of the LHS and RHS
   * @details Equivalent (but generally faster) then performing BuildLHS and BuildRHS
   */
  // virtual void Build(SchemePointerType pScheme,
  //                    ModelPart &rModelPart,
  //                    SystemMatrixType &rA,
  //                    SystemVectorType &rb)
  // {
  //   KRATOS_TRY

  //   // int threads = ParallelUtilities::GetNumThreads();
  //   // ParallelUtilities::SetNumThreads(1);
  //   this->BuildLHS(pScheme, rModelPart, rA);
  //   this->BuildRHS(pScheme, rModelPart, rb);
  //   // ParallelUtilities::SetNumThreads(threads);

  //   KRATOS_CATCH("")
  // }

  /**
   * @brief Function to perform the building of the LHS and RHS
   * @details Equivalent (but generally faster) then performing BuildLHS and BuildRHS
   */
  virtual void Build(SchemePointerType pScheme,
                     ModelPart &rModelPart,
                     SystemMatrixType &rA,
                     SystemVectorType &rb)
  {
    KRATOS_TRY

    if (!pScheme)
      KRATOS_ERROR << "No scheme provided!" << std::endl;

    //getting the elements from the model
    const int nelements = static_cast<int>(rModelPart.Elements().size());

    //getting the array of the conditions
    const int nconditions = static_cast<int>(rModelPart.Conditions().size());

    const ProcessInfo &rCurrentProcessInfo = rModelPart.GetProcessInfo();
    ModelPart::ElementsContainerType::iterator el_begin = rModelPart.ElementsBegin();
    ModelPart::ConditionsContainerType::iterator cond_begin = rModelPart.ConditionsBegin();

    //contributions to the system
    LocalSystemMatrixType LHS_Contribution = LocalSystemMatrixType(0, 0);
    LocalSystemVectorType RHS_Contribution = LocalSystemVectorType(0);

    //vector containing the localization in the system of the different terms
    Element::EquationIdVectorType EquationId;

    // assemble all elements
    BuiltinTimer build_time;

    #pragma omp parallel firstprivate(LHS_Contribution, RHS_Contribution, EquationId)
    {
      #pragma omp for schedule(guided, 512) nowait
      for (int k = 0; k < nelements; k++)
      {
        typename ElementsContainerType::iterator it = el_begin + k;
        // detect if the element is active or not (no set : active by default)
        if (((it->IsDefined(ACTIVE)) ? it->Is(ACTIVE) : true))
        {
          //calculate elemental contribution
          pScheme->CalculateSystemContributions(*(it.base()), LHS_Contribution, RHS_Contribution, EquationId, rCurrentProcessInfo);

          //assemble the elemental contribution
          this->Assemble(rA, rb, LHS_Contribution, RHS_Contribution, EquationId);
        }

      }

      #pragma omp for schedule(guided, 512)
      for (int k = 0; k < nconditions; k++)
      {
        typename ConditionsContainerType::iterator it = cond_begin + k;
        // detect if the element is active or not (no set : active by default)
        if (((it->IsDefined(ACTIVE)) ? it->Is(ACTIVE) : true))
        {
          //calculate elemental contribution
          pScheme->CalculateSystemContributions(*(it.base()), LHS_Contribution, RHS_Contribution, EquationId, rCurrentProcessInfo);

          //assemble the elemental contribution
          this->Assemble(rA, rb, LHS_Contribution, RHS_Contribution, EquationId);
        }
      }
    }

    if (this->mEchoLevel > 2 && rModelPart.GetCommunicator().MyPID() == 0)
      KRATOS_INFO("parallel_build_time") << build_time.ElapsedSeconds() << std::endl;

    if (this->mEchoLevel > 2 && rModelPart.GetCommunicator().MyPID() == 0)
    {
      KRATOS_INFO("parallel_build") << "finished" << std::endl;
    }

    KRATOS_CATCH("")
  }

  /**
   * @brief This is a call to the linear system solver
   */
  virtual void SystemSolve(SystemMatrixType &rA,
                           SystemVectorType &rDx,
                           SystemVectorType &rb,
                           ModelPart &rModelPart)
  {
    KRATOS_TRY

    double norm_b;
    if (TSparseSpace::Size(rb) != 0)
      norm_b = TSparseSpace::TwoNorm(rb);
    else
      norm_b = 0.00;

    if (norm_b != 0.00)
    {
      //do solve
      rModelPart.GetProcessInfo()[DIVERGENCE_ACHIEVED] = !this->mpLinearSystemSolver->Solve(rA, rDx, rb);
    }
    else
      TSparseSpace::SetToZero(rDx);

    //prints informations about the current time
    if (this->mEchoLevel > 1)
    {
      KRATOS_INFO("linear_solver") << *(this->mpLinearSystemSolver) << std::endl;
    }

    KRATOS_CATCH("")
  }

  /**
   * @brief Function to perform the building and solving phase at the same time.
   * @details It is ideally the fastest and safer function to use when it is possible to solve just after building
   */
  virtual void BuildAndSolve(SchemePointerType pScheme,
                             ModelPart &rModelPart,
                             SystemMatrixType &rA,
                             SystemVectorType &rDx,
                             SystemVectorType &rb)
  {
    KRATOS_TRY

    BuiltinTimer build_time;
    this->Build(pScheme, rModelPart, rA, rb);

    if (this->mEchoLevel > 1 && rModelPart.GetCommunicator().MyPID() == 0)
      KRATOS_INFO("system_build_time") << build_time.ElapsedSeconds() << std::endl;

    this->ApplyDirichletConditions(pScheme, rModelPart, rA, rDx, rb);

    if (this->mEchoLevel == 3)
    {
      KRATOS_INFO("LHS before solve") << "Matrix = " << rA << std::endl;
      KRATOS_INFO("Dx before solve") << "Solution = " << rDx << std::endl;
      KRATOS_INFO("RHS before solve") << "Vector = " << rb << std::endl;
    }

    BuiltinTimer solve_time;
    this->SystemSolveWithPhysics(rA, rDx, rb, rModelPart);

    if (this->mEchoLevel > 1 && rModelPart.GetCommunicator().MyPID() == 0)
      KRATOS_INFO("system_solve_time") << solve_time.ElapsedSeconds() << std::endl;

    if (this->mEchoLevel == 3)
    {
      KRATOS_INFO("LHS after solve") << "Matrix = " << rA << std::endl;
      KRATOS_INFO("Dx after solve") << "Solution = " << rDx << std::endl;
      KRATOS_INFO("RHS after solve") << "Vector = " << rb << std::endl;
    }

    KRATOS_CATCH("")
  }

  /**
   * @brief Function to perform the building of the RHS and solving phase at the same time.
   * @details  It corresponds to the previews, but the System's matrix is considered already built and only the RHS is built again
   */
  virtual void BuildRHSAndSolve(SchemePointerType pScheme,
                                ModelPart &rModelPart,
                                SystemMatrixType &rA,
                                SystemVectorType &rDx,
                                SystemVectorType &rb)
  {
    KRATOS_TRY

    this->BuildRHS(pScheme, rModelPart, rb);
    this->SystemSolve(rA, rDx, rb, rModelPart);

    KRATOS_CATCH("")
  }

  /**
   * @brief applies the dirichlet conditions.
   * @details This operation may be very heavy or completely
   * @details unexpensive depending on the implementation choosen and on how the System Matrix
   * @details is built. For explanation of how it works for a particular implementation the user
   * @details should refer to the particular Builder And Solver choosen
   */
  virtual void ApplyDirichletConditions(SchemePointerType pScheme,
                                        ModelPart &rModelPart,
                                        SystemMatrixType &rA,
                                        SystemVectorType &rDx,
                                        SystemVectorType &rb)
  {
  }

  /**
   * @brief Builds the list of the DofSets involved in the problem by "asking" to each element and condition its Dofs.
   * @details The list of dofs is stores insde the SystemBuilderAndSolver as it is closely connected to the way the matrix and RHS are built
  */
  virtual void SetUpDofSet(SchemePointerType pScheme,
                           ModelPart &rModelPart)
  {
  }

  /**
   * @brief organises the dofset in order to speed up the building phase
  */
  virtual void SetUpSystem()
  {
  }

  /**
   * @brief Resizes and Initializes the system vectors and matrices after SetUpDofSet and SetUpSytem has been called
  */
  virtual void SetUpSystemMatrices(SchemePointerType pScheme,
                                   ModelPart &rModelPart,
                                   SystemMatrixPointerType &pA,
                                   SystemVectorPointerType &pDx,
                                   SystemVectorPointerType &pb)
  {
    KRATOS_TRY

    if (pA == nullptr) //if the pointer is not initialized initialize it to an empty matrix
    {
      SystemMatrixPointerType pNewA = Kratos::make_shared<SystemMatrixType>(0, 0);
      pA.swap(pNewA);
    }
    if (pDx == nullptr) //if the pointer is not initialized initialize it to an empty matrix
    {
      SystemVectorPointerType pNewDx = Kratos::make_shared<SystemVectorType>(0);
      pDx.swap(pNewDx);
    }
    if (pb == nullptr) //if the pointer is not initialized initialize it to an empty matrix
    {
      SystemVectorPointerType pNewb = Kratos::make_shared<SystemVectorType>(0);
      pb.swap(pNewb);
    }

    SystemMatrixType &rA = *pA;
    SystemVectorType &rDx = *pDx;
    SystemVectorType &rb = *pb;

    //resizing the system vectors and matrix
    if (rA.size1() == 0 || this->mOptions.Is(LocalFlagType::REFORM_DOFS)) //if the matrix is not initialized
    {
      rA.resize(this->mEquationSystemSize, this->mEquationSystemSize, false);
      ConstructMatrixStructure(pScheme, rA, rModelPart.Elements(), rModelPart.Conditions(), rModelPart.GetProcessInfo());
    }
    else
    {
      if (rA.size1() != this->mEquationSystemSize || rA.size2() != this->mEquationSystemSize)
      {
        KRATOS_WARNING("system builder resize") << "it should not come here -> this is SLOW" << std::endl;
        rA.resize(this->mEquationSystemSize, this->mEquationSystemSize, true);
        ConstructMatrixStructure(pScheme, rA, rModelPart.Elements(), rModelPart.Conditions(), rModelPart.GetProcessInfo());
      }
    }
    if (rDx.size() != this->mEquationSystemSize)
      rDx.resize(this->mEquationSystemSize, false);
    TSparseSpace::SetToZero(rDx);
    if (rb.size() != this->mEquationSystemSize)
      rb.resize(this->mEquationSystemSize, false);
    TSparseSpace::SetToZero(rb);

    KRATOS_CATCH("")
  }

  /**
   * @brief Performs all the required operations that should be done (for each step) before solving the solution step.
   * @details this function must be called only once per step.
   */
  virtual void InitializeSolutionStep(SchemePointerType pScheme,
                                      ModelPart &rModelPart,
                                      SystemMatrixPointerType &pA,
                                      SystemVectorPointerType &pDx,
                                      SystemVectorPointerType &pb)
  {
  }

  /**
   * @brief Performs all the required operations that should be done (for each step) after solving the solution step.
   * @details this function must be called only once per step.
   */
  virtual void FinalizeSolutionStep(SchemePointerType pScheme,
                                    ModelPart &rModelPart,
                                    SystemMatrixPointerType &pA,
                                    SystemVectorPointerType &pDx,
                                    SystemVectorPointerType &pb)
  {
  }

  /**
   * @brief Calculates system reactions
   * @details A flag controls if reactions must be calculated
   * @details An internal variable to store the reactions vector is needed
   */
  virtual void CalculateReactions(SchemePointerType pScheme,
                                  ModelPart &rModelPart,
                                  SystemMatrixType &rA,
                                  SystemVectorType &rDx,
                                  SystemVectorType &rb)
  {
  }

  /**
   * @brief This function is intended to be called at the end of the solution step to clean up memory storage not needed
  */
  virtual void Clear()
  {
    this->mDofSet.clear();

    if (this->mpReactionsVector != nullptr)
      TSparseSpace::Clear(this->mpReactionsVector);

    if (this->mpLinearSystemSolver != nullptr)
      this->mpLinearSystemSolver->Clear();

    if (this->mEchoLevel > 1)
    {
      KRATOS_INFO("Builder and Solver Cleared") << "Clear Function called" << std::endl;
    }
  }

  /**
   * This function is designed to be called once to perform all the checks needed
   * on the input provided. Checks can be "expensive" as the function is designed
   * to catch user's errors.
   * @param rModelPart
   * @return 0 all ok
   */
  virtual int Check(ModelPart &rModelPart)
  {
    KRATOS_TRY

    return 0;

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Access
  ///@{

  /**
   * @brief Get size of the system
   */
  unsigned int GetEquationSystemSize()
  {
    return mEquationSystemSize;
  }

  /**
   * @brief Get linear solver
   */
  LinearSolverPointerType GetLinearSystemSolver()
  {
    return mpLinearSystemSolver;
  }

  /**
   * @brief Sets strategy options
   */
  void SetOptions(Flags &rOptions)
  {
    mOptions = rOptions;
  }

  /**
   * @brief Get strategy options
   @return mOptions: options member variable
   */
  Flags &GetOptions()
  {
    return mOptions;
  }

  /**
   * @brief This sets the level of echo for the builder and solver
   * @param Level of echo for the builder and solver
   * @details
   * {
   * 0 -> Mute... no echo at all
   * 1 -> Printing time and basic informations
   * 2 -> Printing linear solver data
   * 3 -> Print of debug informations: Echo of stiffness matrix, Dx, b...
   * }
   */
  virtual void SetEchoLevel(const int Level)
  {
    mEchoLevel = Level;
  }

  virtual int GetEchoLevel()
  {
    return mEchoLevel;
  }

  /**
   * @brief This method returns the components of the system of equations depending of the echo level
   */
  virtual void EchoInfo(ModelPart &rModelPart,
                        SystemMatrixType &rA,
                        SystemVectorType &rDx,
                        SystemVectorType &rb)
  {
    KRATOS_TRY

    if (this->mEchoLevel == 2) //if it is needed to print the debug info
    {
      this->WriteInfo(rA,rDx,rb);
    }
    else if (this->mEchoLevel == 3) //if it is needed to print the debug info
    {
      KRATOS_INFO("LHS") << "Matrix = " << rA << std::endl;
      KRATOS_INFO("Dx") << "Solution = " << rDx << std::endl;
      KRATOS_INFO("RHS") << "Vector = " << rb << std::endl;
    }
    else if (this->mEchoLevel == 4) //print to matrix market file
    {
      unsigned int iteration_number = 0;
      if (rModelPart.GetProcessInfo().Has(NL_ITERATION_NUMBER))
        iteration_number = rModelPart.GetProcessInfo()[NL_ITERATION_NUMBER];

      std::stringstream matrix_market_name;
      matrix_market_name << "A_" << rModelPart.GetProcessInfo()[TIME] << "_" << iteration_number << ".mm";
      TSparseSpace::WriteMatrixMarketMatrix((char *)(matrix_market_name.str()).c_str(), rA, false);

      std::stringstream matrix_market_vectname;
      matrix_market_vectname << "b_" << rModelPart.GetProcessInfo()[TIME] << "_" << iteration_number << ".mm.rhs";
      TSparseSpace::WriteMatrixMarketVector((char *)(matrix_market_vectname.str()).c_str(), rb);
    }

    KRATOS_CATCH("")
  }

  /**
   * @brief Allows to get the list of system Dofs
   */
  virtual DofsArrayType &GetDofSet()
  {
    return mDofSet;
  }

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  // Pointer to the LinearSolver.
  LinearSolverPointerType mpLinearSystemSolver;

  // Container for de system dofs set
  DofsArrayType mDofSet;

  // Flags to set options
  Flags mOptions;

  // Number of degrees of freedom of the problem to be solved
  unsigned int mEquationSystemSize;

  // Level of echo for the builder and solver
  int mEchoLevel;

  // Reactions vector
  SystemVectorPointerType mpReactionsVector;

  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{

  virtual bool CheckFreeDof(unsigned int &i_global)
  {
    return true;
  }

  virtual void SystemSolveWithPhysics(SystemMatrixType &rA,
                                      SystemVectorType &rDx,
                                      SystemVectorType &rb,
                                      ModelPart &rModelPart)
  {
    KRATOS_TRY

    double norm_b;
    if (TSparseSpace::Size(rb) != 0)
      norm_b = TSparseSpace::TwoNorm(rb);
    else
      norm_b = 0.00;

    if (norm_b != 0.00)
    {
      //provide physical data as needed
      if (this->mpLinearSystemSolver->AdditionalPhysicalDataIsNeeded())
        this->mpLinearSystemSolver->ProvideAdditionalData(rA, rDx, rb, this->mDofSet, rModelPart);

      //do solve
      rModelPart.GetProcessInfo()[DIVERGENCE_ACHIEVED] = !this->mpLinearSystemSolver->Solve(rA, rDx, rb);
    }
    else
    {
      TSparseSpace::SetToZero(rDx);
      KRATOS_WARNING("RHS") << "ATTENTION! setting the RHS to zero!" << std::endl;
    }

    //prints informations about the current time
    if (this->mEchoLevel > 1)
    {
      KRATOS_INFO("LinearSolver") << *(this->mpLinearSystemSolver) << std::endl;
    }

    KRATOS_CATCH("")
  }

  // Note: Parallel run will give different results than serial run:
  // Floating-point addition is a non-associative operation due to round-off errors.
  // Therefore the order of operations matters.
  // Having your parallel program give different result(s) than the serial version is something normal.
  // Understanding and dealing with it is part of the art of writing (portable) parallel codes.
  void Assemble(SystemMatrixType &rA,
                SystemVectorType &rb,
                const LocalSystemMatrixType &rLHS_Contribution,
                const LocalSystemVectorType &rRHS_Contribution,
                Element::EquationIdVectorType &rEquationId)
  {
    unsigned int local_size = rLHS_Contribution.size1();

    for (unsigned int i_local = 0; i_local < local_size; i_local++)
    {
      unsigned int i_global = rEquationId[i_local];

      if (CheckFreeDof(i_global)){
        #pragma omp atomic
        rb[i_global] += rRHS_Contribution[i_local];

        this->AssembleRowContribution(rA, rLHS_Contribution, i_global, i_local, rEquationId);
      }

    }
  }

  virtual void AssembleRHS(SystemVectorType &rb,
                           const LocalSystemVectorType &rRHS_Contribution,
                           const Element::EquationIdVectorType &rEquationId)
  {
    unsigned int local_size = rRHS_Contribution.size();

    for (unsigned int i_local = 0; i_local < local_size; i_local++)
    {
      unsigned int i_global = rEquationId[i_local];

      if (CheckFreeDof(i_global)){
        #pragma omp atomic
        rb[i_global] += rRHS_Contribution[i_local];
      }
    }
  }

  void AssembleLHS(SystemMatrixType &rA,
                   const LocalSystemMatrixType &rLHS_Contribution,
                   Element::EquationIdVectorType &rEquationId)
  {
    unsigned int local_size = rLHS_Contribution.size1();

    for (unsigned int i_local = 0; i_local < local_size; i_local++)
    {
      unsigned int i_global = rEquationId[i_local];
      if (CheckFreeDof(i_global))
        this->AssembleRowContribution(rA, rLHS_Contribution, i_global, i_local, rEquationId);
    }
  }

  inline void AddUnique(std::vector<IndexType> &v, const IndexType &candidate)
  {
    typename std::vector<IndexType>::iterator i = v.begin();
    typename std::vector<IndexType>::iterator endit = v.end();
    while (i != endit && (*i) != candidate)
    {
      ++i;
    }
    if (i == endit)
    {
      v.push_back(candidate);
    }
  }

  inline virtual void AssembleRowContribution(SystemMatrixType &rA,
                                              const Matrix &rLHS_Contribution,
                                              const unsigned int i,
                                              const unsigned int i_local,
                                              Element::EquationIdVectorType &rEquationId)
  {
    KRATOS_ERROR << " Calling base system builder and solver AssembleRowContribution "<<std::endl;
  }

  inline unsigned int ForwardFind(const unsigned int id_to_find,
                                  const unsigned int start,
                                  const size_t *index_vector)
  {
    unsigned int pos = start;
    while (id_to_find != index_vector[pos])
      pos++;
    return pos;
  }

  inline unsigned int BackwardFind(const unsigned int id_to_find,
                                   const unsigned int start,
                                   const size_t *index_vector)
  {
    unsigned int pos = start;
    while (id_to_find != index_vector[pos])
      pos--;
    return pos;
  }

  virtual void ConstructMatrixStructure(SchemePointerType pScheme,
                                        SystemMatrixType &rA,
                                        ElementsContainerType &rElements,
                                        ConditionsContainerType &rConditions,
                                        const ProcessInfo &rCurrentProcessInfo)
  {
    KRATOS_ERROR << " Calling base system builder and solver ConstructMatrixStructure "<<std::endl;
  }


  /**
   * @brief This method writes the components of the system of equations
   */
  virtual void WriteInfo(const SystemMatrixType &rA,
                         const SystemVectorType &rDx,
                         const SystemVectorType &rb,
                         std::string Prefix = "")
  {
      std::cout.precision(17);
      std::cout<<Prefix<<" LHS: Matrix   = " <<std::endl;
      for (unsigned int i=0; i<rA.size1(); ++i)
      {
        std::cout<<" line "<<i<<": (";
        for (unsigned int j=0; j<rA.size2(); ++j)
        {
          if (rA(i,j)!=0)
          {
            std::cout<<rA(i,j)<<", ";
          }
        }
        std::cout<<")"<<std::endl;
      }
      //std::cout<<Prefix<<" LHS: Matrix   = " << rA << std::endl;
      std::cout<<Prefix<<" Dx : Solution = " << rDx << std::endl;
      std::cout<<Prefix<<" RHS: Vector   = " << rb << std::endl;
  }

  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{

private:
  ///@}
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{
  ///@}

}; /// Class SystemBuilderAndSolver

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_SYSTEM_BUILDER_AND_SOLVER_HPP_INCLUDED defined
