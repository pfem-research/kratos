//
//   Project Name:        KratosSolversApplication    $
//   Created by:          $Author:        JMCarbonell $
//   Last modified by:    $Co-Author:                 $
//   Date:                $Date:         January 2019 $
//   Revision:            $Revision:              0.0 $
//
//

// System includes

// External includes

// Project includes
#include "custom_python/add_custom_utilities_to_python.h"

// Utilities
#include "custom_utilities/eigenvector_to_solution_step_variable_transfer_utility.hpp"
#include "custom_utilities/assignment_utilities.hpp"
#include "custom_utilities/time_interval.hpp"
#include "custom_utilities/function_value.hpp"


namespace Kratos
{

namespace Python
{

inline void TransferEigenvector1(EigenvectorToSolutionStepVariableTransferUtility &rThisUtil,
                                 ModelPart &rModelPart,
                                 int iEigenMode)
{
  rThisUtil.Transfer(rModelPart, iEigenMode);
}

inline void TransferEigenvector2(EigenvectorToSolutionStepVariableTransferUtility &rThisUtil,
                                 ModelPart &rModelPart,
                                 int iEigenMode,
                                 int step)
{
  rThisUtil.Transfer(rModelPart, iEigenMode, step);
}

void AddCustomUtilitiesToPython(pybind11::module &m)
{

  namespace py = pybind11;

  py::class_<EigenvectorToSolutionStepVariableTransferUtility>(m, "EigenvectorToSolutionStepVariableTransferUtility")
      .def("Transfer", TransferEigenvector1)
      .def("Transfer", TransferEigenvector2);

  py::class_<TimeInterval>(m, "TimeInterval")
      .def(py::init<>())
      .def(py::init<Parameters&, ProcessInfo&>())
      .def("GetStartTime", &TimeInterval::GetStartTime)
      .def("GetEndTime", &TimeInterval::GetEndTime)
      .def("GetDeltaTime", &TimeInterval::GetDeltaTime)
      .def("IsInitial", &TimeInterval::IsInitial)
      .def("IsInside", &TimeInterval::IsInside)
      .def("WasInside", &TimeInterval::WasInside)
      .def("IsEnded", &TimeInterval::IsEnded)
      .def("IsFixingStep", &TimeInterval::IsFixingStep)
      .def("IsUnfixingStep", &TimeInterval::IsUnfixingStep)
      .def("IsRecoverStep", &TimeInterval::IsRecoverStep);

  py::class_<FunctionValue>(m, "FunctionValue")
      .def(py::init<Parameters>())
      .def("IsNumeric", &FunctionValue::IsNumeric)
      .def("GetValue", &FunctionValue::GetValue)
      .def("IsCurrentValue", &FunctionValue::IsCurrentValue)
      .def("GetFunction", &FunctionValue::GetFunction)
      .def("GetFunctionExpresion", &FunctionValue::GetFunctionExpression)
      .def("SetCompiledFunction", &FunctionValue::SetCompiledFunction)
      .def("IsSpatialFunction", &FunctionValue::IsSpatialFunction)
      .def("CallFunction", &FunctionValue::CallFunction)
      .def("LocalAxesTransform", &FunctionValue::LocalAxesTransform)
      .def("GetVectorValue", &FunctionValue::GetVectorValue)
      .def("GetDirection", &FunctionValue::GetDirection);

  py::class_<AssignmentUtils>(m, "AssignmentUtils")
      .def("GetInverseAssignment", &AssignmentUtils::GetInverseAssigment);
}

} // namespace Python.

} // Namespace Kratos
