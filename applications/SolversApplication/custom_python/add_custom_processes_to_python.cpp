//
//   Project Name:        KratosSolversApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:        JMC  $
//   Date:                $Date:      January 2019 $
//
//

// System includes

// External includes

// Project includes
#include "custom_python/add_custom_processes_to_python.h"

// Processes
#include "custom_processes/add_dofs_process.hpp"

// Solver Processes
#include "custom_processes/solver_process.hpp"
#include "custom_processes/assign_axial_turn_solver_process.hpp"
#include "custom_processes/assign_local_constraint_solver_process.hpp"

namespace Kratos
{

namespace Python
{

void AddCustomProcessesToPython(pybind11::module &m)
{

    namespace py = pybind11;

    py::class_<FixScalarDofProcess, FixScalarDofProcess::Pointer, Process>(m, "FixScalarDofProcess")
        .def(py::init<ModelPart &, Parameters>())
        .def(py::init<ModelPart &, Parameters &>())
        .def(py::init<ModelPart &, const Variable<double> &, std::vector<Flags> >())
        .def(py::init<ModelPart &, const Variable<int> &, std::vector<Flags> >())
        .def(py::init<ModelPart &, const Variable<bool> &, std::vector<Flags> >())
        .def("Execute", &FixScalarDofProcess::Execute)

        ;

    py::class_<FreeScalarDofProcess, FreeScalarDofProcess::Pointer, Process>(m, "FreeScalarDofProcess")
        .def(py::init<ModelPart &, Parameters>())
        .def(py::init<ModelPart &, Parameters &>())
        .def(py::init<ModelPart &, const Variable<double> &, std::vector<Flags> >())
        .def(py::init<ModelPart &, const Variable<int> &, std::vector<Flags> >())
        .def(py::init<ModelPart &, const Variable<bool> &, std::vector<Flags> >())
        .def("Execute", &FreeScalarDofProcess::Execute)

        ;

    py::class_<AddDofsProcess, AddDofsProcess::Pointer, Process>(m, "AddDofsProcess")
        .def(py::init<ModelPart &, Parameters>())
        .def(py::init<ModelPart &, Parameters &>())
        .def(py::init<ModelPart &, const pybind11::list &, const pybind11::list &>())
        .def("Execute", &AddDofsProcess::Execute);

    py::class_<SolverProcess, SolverProcess::Pointer, Process>(m, "SolverProcess")
        .def(py::init<>());

    py::class_<AssignAxialTurnSolverProcess, AssignAxialTurnSolverProcess::Pointer, SolverProcess>(m, "AssignAxialTurnSolverProcess")
        .def(py::init<ModelPart &, Parameters>());

    py::class_<AssignLocalConstraintSolverProcess, AssignLocalConstraintSolverProcess::Pointer, SolverProcess>(m, "AssignLocalConstraintSolverProcess")
        .def(py::init<ModelPart &, Parameters>());

}

} // namespace Python.

} // Namespace Kratos
