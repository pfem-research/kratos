""" Project: SolversApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Kratos Imports
from KratosMultiphysics import _ImportApplication
import KratosMultiphysics.LinearSolversApplication

from KratosSolversApplication import *
application = KratosSolversApplication()
application_name = "KratosSolversApplication"

_ImportApplication(application, application_name)
