# Solvers Application

In this application the algorithms for time integration are implemented. They are classified in Solution Strategies, Solution Schemes and Time Integration Methods. 
The system builders, convergence criteria and some external linear solvers are also included as a part of the application.

## License

The Solvers application is OPEN SOURCE. The main code and program structure is available and aimed to grow with the need of any user willing to expand it. The BSD (Berkeley Software Distribution) licence allows to use and distribute the existing code without any restriction, but with the possibility to develop new parts of the code on an open or close source depending on the developers.

## Contact

* **Josep Maria Carbonell** - *Core Development* - [cpuigbo@cimne.upc.edu](mailto:cpuigbo@cimne.upc.edu)
