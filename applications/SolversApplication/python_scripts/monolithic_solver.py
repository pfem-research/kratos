""" Project: SolversApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""


# Built-in/Generic Imports
import sys
import os

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolversApplication as KratosSolver


def CreateSolver(custom_settings, Model):
    return MonolithicSolver(Model, custom_settings)

# Base class to develop other solvers


class MonolithicSolver(object):
    """The base class for solvers

    This class provides functions for importing and exporting models,
    adding nodal variables and dofs and solving each solution step.

    Derived classes must override the function _create_mechanical_solver which
    constructs and returns a valid solving strategy. Depending on the type of
    solver, derived classes may also need to override the following functions:

    _create_solution_scheme
    _create_convergence_criterion
    _create_linear_solver
    _create_builder_and_solver
    _create_mechanical_solver

    The mechanical_solver, builder_and_solver, etc. should alway be retrieved
    using the getter functions _get_mechanical_solver, _get_builder_and_solver,
    etc. from this base class.

    Only the member variables listed below should be accessed directly.

    Public member variables:
    settings -- Kratos parameters containing solver settings.
    model_part -- the model part used to construct the solver (computing_model_part).
    """

    def __init__(self, Model, custom_settings):

        default_settings = KratosMultiphysics.Parameters("""
        {
            "solving_model_part": "computing_domain",
            "dofs": [],
            "time_integration_settings":{
                "solution_type": "Dynamic",
                "analysis_type": "Non-linear",
                "formulation_type": "Lagrangian",
                "time_integration": "Implicit",
                "integration_method": "Newmark",
                "prediction_level": 0,
                "time_integration_order": 1,
                "buffer_size": 2,
                "move_mesh_flag": true,
                "incremental_update": true,
                "integration_variables": []
            },
            "convergence_criterion_settings":{
                "convergence_criterion": "Residual_criterion",
                "variable_relative_tolerance": 1.0e-4,
                "variable_absolute_tolerance": 1.0e-9,
                "residual_relative_tolerance": 1.0e-4,
                "residual_absolute_tolerance": 1.0e-9,
                "separate_dofs": true
            },
            "solving_strategy_settings":{
                "builder_type": "block_builder",
                "line_search": false,
                "line_search_type": 0,
                "implex": false,
                "compute_reactions": true,
                "move_mesh_flag": true,
                "iterative_update": true,
                "clear_storage": false,
                "reform_dofs_at_each_step": true,
                "max_iteration": 10
            },
            "linear_solver_settings":{
                "solver_type": "ks_superlu_direct",
                "max_iteration": 500,
                "tolerance": 1e-9,
                "scaling": false
            },
            "processes": []
        }
        """)

        # check existing default linear solver exit
        if not hasattr(KratosMultiphysics.SolversApplication, "ks_superlu_direct"):
            default_settings["linear_solver_settings"]["solver_type"].SetString(
                "skyline_lu_factorization")

        # Linear solver settings can have different number of settings
        if(custom_settings.Has("linear_solver_settings")):
            default_settings.RemoveValue("linear_solver_settings")
            default_settings.AddValue(
                "linear_solver_settings", custom_settings["linear_solver_settings"])

        # Check and fix supplied settings compatibility (experimental)
        from KratosMultiphysics.SolversApplication.json_settings_utility import JsonSettingsUtility
        JsonSettingsUtility.CheckAndFixNotMatchingSettings(
            custom_settings, default_settings)

        # Overwrite the default settings with user-provided parameters
        self.settings = custom_settings
        self.settings.ValidateAndAssignDefaults(default_settings)

        # Validate and assign other values
        self.settings["time_integration_settings"].ValidateAndAssignDefaults(
            default_settings["time_integration_settings"])  # default values in factory
        self.settings["solving_strategy_settings"].ValidateAndAssignDefaults(
            default_settings["solving_strategy_settings"])
        # self.settings["convergence_criterion_settings"].ValidateAndAssignDefaults(default_settings["convergence_criterion_settings"]) #default values in factory
        self.settings["linear_solver_settings"].ValidateAndAssignDefaults(
            default_settings["linear_solver_settings"])
        #print("Monolithic Solver Settings",self.settings.PrettyPrintJsonString())

        print(self._class_prefix(), "Linear Solver: (" +
              self.settings["linear_solver_settings"]["solver_type"].GetString()+")")

        # Model
        self.model = Model

        # Echo level
        self.echo_level = 0

        # Adaptive solution
        self.adaptive = 0

    def ExecuteInitialize(self):

        # Set model and info
        self._set_model_info()

        # Configure model and solver
        self._set_integration_parameters()

    def ExecuteBeforeSolutionLoop(self):

        # The mechanical solver is created here if it does not already exist.
        mechanical_solver = self._get_mechanical_solver()
        mechanical_solver.SetEchoLevel(self.echo_level)

        self.Clear()

        self._check_initialized()

        self.Check()

        print(self._class_prefix(
        ), self.settings["time_integration_settings"]["integration_method"].GetString()+"_Scheme Ready")

    def GetVariables(self):

        import KratosMultiphysics.SolversApplication.schemes_factory as schemes_factory
        Schemes = schemes_factory.SolutionScheme(
            self.settings["time_integration_settings"], self.settings["dofs"])
        return Schemes.GetVariables()

    def GetOutputVariables(self):
        pass

    def ComputeDeltaTime(self):
        pass

    def SetEchoLevel(self, level):
        self.echo_level = level

    def SetAdaptiveTime(self, adaptive):
        self.adaptive = adaptive

    def Clear(self):
        if self.settings["solving_strategy_settings"]["clear_storage"].GetBool():
            self._get_mechanical_solver().Clear()

    def Check(self):
        self._get_mechanical_solver().Check()

    #### Solve loop methods ####

    def Solve(self):
        self.Clear()
        self._check_reform_dofs()
        return self._get_mechanical_solver().Solve()

    # step by step:

    def InitializeSolutionStep(self):
        self.Clear()
        self._check_reform_dofs()
        self._get_mechanical_solver().InitializeSolutionStep()

    def SolveSolutionStep(self):
        return self._get_mechanical_solver().SolveSolutionStep()

    def FinalizeSolutionStep(self):
        self._get_mechanical_solver().FinalizeSolutionStep()

    #### Solver internal methods ####

    def _check_adaptive_solution(self):
        return self.adaptive

    def _check_reform_dofs(self):
        if self._domain_parts_updated():
            if not self._get_mechanical_solver().GetOptions().Is(KratosSolver.SolverLocalFlags.REFORM_DOFS):
                self._get_mechanical_solver().GetOptions().Set(
                    KratosSolver.SolverLocalFlags.REFORM_DOFS, True)
                KratosMultiphysics.Logger.PrintInfo("", self._class_prefix()+" Set flag (REFORM_DOFS:true)")
        else:
            if self._get_mechanical_solver().GetOptions().Is(KratosSolver.SolverLocalFlags.REFORM_DOFS):
                self._get_mechanical_solver().GetOptions().Set(
                    KratosSolver.SolverLocalFlags.REFORM_DOFS, False)
                KratosMultiphysics.Logger.PrintInfo("", self._class_prefix()+" Set flag (REFORM_DOFS:false)")
        KratosMultiphysics.Logger.PrintInfo("", self._class_prefix()+" Updated Domains (REFORM_DOFS:"+str(self._get_mechanical_solver().GetOptions().Is(KratosSolver.SolverLocalFlags.REFORM_DOFS))+")")


    def _domain_parts_updated(self):
        update_time = False
        if self._is_restarted():
            if self.process_info.Has(KratosSolver.RESTART_STEP_TIME):
                update_time = self._check_previous_time_step(
                    self.process_info[KratosSolver.RESTART_STEP_TIME])

        if not update_time and self.process_info.Has(KratosSolver.MESHING_STEP_TIME):
            update_time = self._check_previous_time_step(
                self.process_info[KratosSolver.MESHING_STEP_TIME])

        if not update_time and self.process_info.Has(KratosSolver.CONTACT_STEP_TIME):
            update_time = self._check_previous_time_step(
                self.process_info[KratosSolver.CONTACT_STEP_TIME])

        return update_time

    def _check_current_time_step(self, step_time):
        current_time = self.process_info[KratosMultiphysics.TIME]
        delta_time = self.process_info[KratosMultiphysics.DELTA_TIME]
        # arithmetic floating point tolerance
        tolerance = delta_time * 0.001

        if(step_time > current_time-tolerance and step_time < current_time+tolerance):
            return True
        else:
            return False

    def _check_previous_time_step(self, step_time):
        current_time = self.process_info[KratosMultiphysics.TIME]
        delta_time = self.process_info[KratosMultiphysics.DELTA_TIME]
        previous_time = current_time - delta_time

        # arithmetic floating point tolerance
        tolerance = delta_time * 0.001

        if(step_time > previous_time-tolerance and step_time < previous_time+tolerance):
            return True
        else:
            return False

    def _check_initialized(self):
        mechanical_solver = self._get_mechanical_solver()
        #this is legacy if kratos core solvers are used
        if self._is_restarted():
            if hasattr(mechanical_solver, 'SetInitializePerformedFlag'):
                mechanical_solver.SetInitializePerformedFlag(True)
        mechanical_solver.Initialize()

    def _is_restarted(self):
        if self.process_info[KratosMultiphysics.IS_RESTARTED]:
            return True
        else:
            return False

    def _is_not_restarted(self):
        return not self._is_restarted()

    def _set_model_info(self):

        #print("Monolithic Solver Settings",self.settings.PrettyPrintJsonString())

        # Get solving model part
        self.model_part = self.model[self.settings["solving_model_part"].GetString(
        )]

        # Main model part from computing model part
        self.main_model_part = self.model_part.GetRootModelPart()

        # Process information
        self.process_info = self.main_model_part.ProcessInfo

    def _set_integration_parameters(self):
        # Add dofs
        if self._is_not_restarted():
            self._add_dofs()

        # Create integration information (needed in other processes)
        self._set_time_integration_methods()

        # Set buffer
        if self._is_not_restarted():
            self._set_and_fill_buffer()

    def _get_solution_scheme(self):
        if not hasattr(self, '_solution_scheme'):
            self._solution_scheme = self._create_solution_scheme()
        return self._solution_scheme

    def _get_convergence_criterion(self):
        if not hasattr(self, '_convergence_criterion'):
            self._convergence_criterion = self._create_convergence_criterion()
        return self._convergence_criterion

    def _get_linear_solver(self):
        if not hasattr(self, '_linear_solver'):
            self._linear_solver = self._create_linear_solver()
        return self._linear_solver

    def _get_builder_and_solver(self):
        if not hasattr(self, '_builder_and_solver'):
            self._builder_and_solver = self._create_builder_and_solver()
        return self._builder_and_solver

    def _get_mechanical_solver(self):
        if not hasattr(self, '_mechanical_solver'):
            self._mechanical_solver = self._create_mechanical_solver()
        return self._mechanical_solver

    def _get_minimum_buffer_size(self):
        buffer_size = self.settings["time_integration_settings"]["buffer_size"].GetInt(
        )
        time_integration_order = self.settings["time_integration_settings"]["time_integration_order"].GetInt(
        )
        if(buffer_size <= time_integration_order):
            buffer_size = time_integration_order + 1
        return buffer_size

    def _set_and_fill_buffer(self):
        """Prepare nodal solution step data containers and time step information. """
        # Set the buffer size for the nodal solution steps data. Existing nodal
        # solution step data may be lost.
        buffer_size = self._get_minimum_buffer_size()
        self.main_model_part.SetBufferSize(buffer_size)
        # Cycle the buffer. This sets all historical nodal solution step data to
        # the current value and initializes the time stepping in the process info.
        delta_time = self.process_info[KratosMultiphysics.DELTA_TIME]
        time = self.process_info[KratosMultiphysics.TIME]
        step = -buffer_size
        time = time - delta_time * buffer_size
        self.process_info.SetValue(KratosMultiphysics.TIME, time)
        for i in range(0, buffer_size):
            step = step + 1
            time = time + delta_time
            self.process_info.SetValue(KratosMultiphysics.STEP, step)
            self.main_model_part.CloneTimeStep(time)

    def _create_solution_scheme(self):
        import KratosMultiphysics.SolversApplication.schemes_factory as schemes_factory
        Schemes = schemes_factory.SolutionScheme(
            self.settings["time_integration_settings"], self.settings["dofs"])
        solution_scheme = Schemes.GetSolutionScheme()
        solution_scheme.SetProcessVector(self._get_scheme_custom_processes())
        return solution_scheme

    def _create_convergence_criterion(self):
        criterion_parameters = self.settings["convergence_criterion_settings"]
        criterion_parameters.AddEmptyValue(
            "echo_level").SetInt(self.echo_level)
        # Construction of the class convergence_criterion
        import KratosMultiphysics.SolversApplication.convergence_criteria_factory as convergence_criteria_factory
        convergence_criterion = convergence_criteria_factory.ConvergenceCriterion(
            criterion_parameters, self.settings["dofs"])
        return convergence_criterion.GetConvergenceCriterion()

    def _create_linear_solver(self):
        linear_solver_settings = self.settings["linear_solver_settings"]
        if linear_solver_settings.Has("solver_type"):
            from KratosMultiphysics import python_linear_solver_factory as linear_solver_factory
            return linear_solver_factory.ConstructSolver(linear_solver_settings)
        else:
            raise Exception("Linear-Solver not defined")

    def _create_builder_and_solver(self):
        linear_solver = self._get_linear_solver()
        if(self.settings["solving_strategy_settings"]["builder_type"].GetString() == "block_builder"):
            # To keep matrix blocks in builder
            builder_and_solver = KratosSolver.BlockBuilderAndSolver(
                linear_solver)
        else:
            builder_and_solver = KratosSolver.ReductionBuilderAndSolver(
                linear_solver)

        return builder_and_solver

    def _create_mechanical_solver(self):
        raise Exception(
            "please implement the Custom Choice of your Mechanical Solver (_create_mechanical_solver) in your solver")

    def _get_dofs(self):
        import KratosMultiphysics.SolversApplication.schemes_factory as schemes_factory
        Schemes = schemes_factory.SolutionScheme(
            self.settings["time_integration_settings"], self.settings["dofs"])
        return Schemes.GetDofsAndReactions()

    def _add_dofs(self):
        dof_variables, dof_reactions = self._get_dofs()
        AddDofsProcess = KratosSolver.AddDofsProcess(
            self.main_model_part, dof_variables, dof_reactions)
        AddDofsProcess.Execute()
        if(self.echo_level > 1):
            print(dof_variables + dof_reactions)
            print(self._class_prefix()+" DOF's ADDED")

    def _get_scheme_custom_processes(self):
        processes = self.settings["processes"]
        processes = self._set_ale_mesh_movement_constraints(processes)
        processes_list = []
        for process in processes.values():
            processes_list.append(self._construct_process(process))
        return processes_list

    def _construct_process(self, process):
        #kratos_module = __import__(process["kratos_module"].GetString())
        import importlib
        process_module_name = None
        if process.Has("kratos_module") and process.Has("python_module"):
            process_module_name = process["kratos_module"].GetString()+"."+process["python_module"].GetString()
        if process.Has("process_module"):
            process_module_name = "KratosMultiphysics."+process["process_module"].GetString()
        process_module = importlib.import_module(process_module_name)
        return(process_module.Factory(process, self.model))

    def _set_scheme_process_info_parameters(self):
        pass

    def _get_time_integration_methods(self):
        # set solution scheme
        import KratosMultiphysics.SolversApplication.schemes_factory as schemes_factory
        Schemes = schemes_factory.SolutionScheme(
            self.settings["time_integration_settings"], self.settings["dofs"])

        # set integration method dictionary for scalars
        scalar_integration_methods = Schemes.GetScalarIntegrationMethods()

        # set integration method dictionary for components
        component_integration_methods = Schemes.GetComponentIntegrationMethods()

        # print(scalar_integration_methods)
        # print(component_integration_methods)

        # set time order
        self.process_info[KratosSolver.TIME_INTEGRATION_ORDER] = Schemes.GetTimeIntegrationOrder()

        # set scheme parameters to process_info
        self._set_scheme_process_info_parameters()

        # first: calculate parameters (only once permitted for each monolithic dof set "components + scalar")
        dofs_list = self.settings["dofs"]

        dofs = []
        for i in range(0, dofs_list.size()):
            dofs.append(dofs_list[i].GetString())

        # add default DISPLACEMENT dof
        if(len(dofs) == 0 or (len(dofs) == 1 and dofs[0] == "ROTATION")):
            dofs.append('DISPLACEMENT')

        #print(" DOFS ",dofs)

        scalar_dof_method_set = False
        vector_dof_method_set = False
        method_type = None

        for dof in dofs:
            kratos_variable = KratosMultiphysics.KratosGlobals.GetVariable(dof)
            if(isinstance(kratos_variable, KratosMultiphysics.DoubleVariable) and (not scalar_dof_method_set)):
                if(method_type != str(scalar_integration_methods[dof])):
                    scalar_integration_methods[dof].CalculateParameters(
                        self.process_info)
                    scalar_dof_method_set = True
                    # print("::[----Integration----]::",scalar_integration_methods[dof],"("+dof+")", set parameters)
                    method_type = str(scalar_integration_methods[dof])
            if(isinstance(kratos_variable, KratosMultiphysics.Array1DVariable3) and (not vector_dof_method_set)):
                if(method_type != str(component_integration_methods[dof+"_X"])):
                    component_integration_methods[dof +
                                                  "_X"].CalculateParameters(self.process_info)
                    vector_dof_method_set = True
                    # print("::[----Integration----]::",component_integration_methods[dof+"_X"],"("+dof+")", set parameters)
                    method_type = str(component_integration_methods[dof+"_X"])

        return scalar_integration_methods, component_integration_methods

    def _set_time_integration_methods(self):

        scalar_integration_methods, component_integration_methods = self._get_time_integration_methods()

        # second: for the same method the parameters (already calculated)
        scalar_integration_methods_container = KratosSolver.ScalarTimeIntegrationMethods()
        for dof, method in scalar_integration_methods.items():
            # set same parameters to all methods from process_info values
            method.SetParameters(self.process_info)
            scalar_integration_methods_container.Set(dof, method)

        component_integration_methods_container = KratosSolver.ScalarTimeIntegrationMethods()
        for dof, method in component_integration_methods.items():
            # set same parameters to all methods from process_info values
            method.SetParameters(self.process_info)
            component_integration_methods_container.Set(dof, method)

        # set time integration methods (for scalars and components) to process_info for processes access
        if(len(scalar_integration_methods)):
            scalar_integration_methods_container.AddToProcessInfo(
                KratosSolver.SCALAR_TIME_INTEGRATION_METHODS, scalar_integration_methods_container, self.process_info)

        component_integration_methods_container.AddToProcessInfo(
            KratosSolver.COMPONENT_TIME_INTEGRATION_METHODS, component_integration_methods_container, self.process_info)

        # print(scalar_integration_methods)
        # print(component_integration_methods)

    def _set_ale_mesh_movement_constraints(self, processes_list):
        if self.settings["time_integration_settings"]["formulation_type"].GetString() == "ALE":
            print(self._class_prefix(),"Analysis type ALE")
            local_list = KratosMultiphysics.Parameters("""{ "processes_list":[] }""")
            local = []
            for i in range(processes_list.size()):
                module = processes_list[i]["process_module"].GetString()
                if "local_constraint" in module:
                    local.append(i)
                    continue
                if processes_list[i].Has("Parameters"):
                    if processes_list[i]["Parameters"].Has("variable_name"):
                        variable_name = processes_list[i]["Parameters"]["variable_name"].GetString(
                        )
                        if variable_name in ("DISPLACEMENT", "VELOCITY", "ACCELERATION"):
                            ale_settings = processes_list[i].Clone()
                            ale_settings["Parameters"]["variable_name"].SetString(
                                "MESH"+"_"+variable_name)
                            ale_settings["Parameters"].AddEmptyList(
                                "flags_list")
                            ale_settings["Parameters"]["flags_list"].Append(
                                "SLIP")
                            if processes_list[i].Has("constrained"):
                                ale_settings["Parameters"]["constrained"].SetBool(
                                    False)
                                processes_list[i]["Parameters"]["constrained"].SetBool(
                                    False)
                            else:
                                ale_settings["Parameters"].AddEmptyValue(
                                    "constrained").SetBool(False)
                                processes_list[i]["Parameters"].AddEmptyValue(
                                    "constrained").SetBool(False)
                            processes_list[i]["Parameters"].AddEmptyList(
                                "flags_list")
                            processes_list[i]["Parameters"]["flags_list"].Append(
                                "NOT_SLIP")
                            local_list["processes_list"].Append(processes_list[i])
                            local_list["processes_list"].Append(ale_settings)
                            #processes_list.Append(ale_settings)
            # local processes at end
            for i in local:
                local_list["processes_list"].Append(processes_list[i])

            # set new ordered list
            processes_list = local_list["processes_list"]

        print(" EXTENDED_SOLVER_PROCESSES ", processes_list.PrettyPrintJsonString())

        return processes_list

    #
    @classmethod
    def _class_prefix(self):
        header = "::[-Monolithic_Solver-]::"
        return header
