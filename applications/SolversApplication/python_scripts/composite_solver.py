""" Project: SolversApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""


# Built-in/Generic Imports
import sys
import os

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolversApplication as KratosSolver
import KratosMultiphysics.SolversApplication.monolithic_solver as BaseSolver


def CreateSolver(custom_settings, Model):
    return CompositeSolver(Model, custom_settings)

# Base class to develop other solvers


class CompositeSolver(BaseSolver.MonolithicSolver):
    """The composite solver

    This class creates the a list of monolithic solvers for the configuration of a composite strategy

    See monolithic_solver.py for more information.
    """

    def __init__(self, Model, custom_settings):

        default_settings = KratosMultiphysics.Parameters("""
        {
            "solving_model_part": "computing_domain",
            "solvers":[],
            "processes":[]
        }
        """)

        # Overwrite the default settings with user-provided parameters
        self.settings = custom_settings
        self.settings.ValidateAndAssignDefaults(default_settings)

        # Create solvers list
        self.solvers = []
        solvers_list = self.settings["solvers"]
        import importlib
        for i in range(solvers_list.size()):
            name = solvers_list[i]["solver_type"].GetString()
            if "solid_mechanics_" in name:
                name = solvers_list[i]["solver_type"].GetString().split("solid_mechanics_", 1)[1]

            solver_module = importlib.import_module("KratosMultiphysics.SolversApplication."+name)
            self.solvers.append(solver_module.CreateSolver(
                solvers_list[i]["Parameters"], Model))

        # Model
        self.model = Model

        # Echo level
        self.echo_level = 0

        # Solver processes
        self.processes = []

    def ExecuteInitialize(self):
        for solver in self.solvers:
            solver._set_model_info()

        super(CompositeSolver, self).ExecuteInitialize()

        composite_variable = KratosMultiphysics.KratosGlobals.GetVariable(
            "SOLVER_STEP")
        self.process_info[composite_variable] = 0  # initialize

    def ExecuteBeforeSolutionLoop(self):
        mechanical_solver = self._get_mechanical_solver()
        mechanical_solver.SetEchoLevel(self.echo_level)

        for solver in self.solvers:
            solver.Clear()

        self._check_initialized()

        for solver in self.solvers:
            solver.Check()

    def GetVariables(self):
        nodal_variables = []
        for solver in self.solvers:
            nodal_variables = nodal_variables + solver.GetVariables()
        return nodal_variables

    def SetEchoLevel(self, level):
        for solver in self.solvers:
            solver.SetEchoLevel(level)
        self.echo_level = level

    #
    def SetAdaptiveTime(self, adaptive):
        for solver in self.solvers:
            solver.SetAdaptiveTime(adaptive)

    def Clear(self):
        for solver in self.solvers:
            solver.Clear()

    #### Solver internal methods ####
    def _check_reform_dofs(self):
        for solver in self.solvers:
            solver._check_reform_dofs()

    def _check_initialized(self):
        mechanical_solver = self._get_mechanical_solver()
        mechanical_solver.Initialize()

    def _create_mechanical_solver(self):
        strategies = []
        for solver in self.solvers:
            strategies.append(solver._get_mechanical_solver())
        options = KratosMultiphysics.Flags()
        mechanical_solver = KratosSolver.CompositeStrategy(
            self.model_part, options, strategies)
        mechanical_solver.Set(
            KratosSolver.SolverLocalFlags.ADAPTIVE_SOLUTION, self._check_adaptive_solution())
        return mechanical_solver

    #
    def _check_adaptive_solution(self):
        for solver in self.solvers:
            if solver._check_adaptive_solution():
                return True
        return False
    #

    def _get_dofs(self):
        dof_variables = []
        dof_reactions = []
        for solver in self.solvers:
            solver_dof_variables, solver_dof_reactions = solver._get_dofs()
            dof_variables = dof_variables + solver_dof_variables
            dof_reactions = dof_reactions + solver_dof_reactions

        return dof_variables, dof_reactions

    #

    def _get_time_integration_methods(self):
        scalar_integration_methods = {}
        component_integration_methods = {}
        for solver in self.solvers:
            solver_scalar_integration_methods, solver_component_integration_methods = solver._get_time_integration_methods()
            scalar_integration_methods.update(
                solver_scalar_integration_methods)
            component_integration_methods.update(
                solver_component_integration_methods)

        return scalar_integration_methods, component_integration_methods

    #
    def _get_minimum_buffer_size(self):
        buffer_size = 2
        for solver in self.solvers:
            size = solver._get_minimum_buffer_size()
            if(size > buffer_size):
                buffer_size = size
        return buffer_size
