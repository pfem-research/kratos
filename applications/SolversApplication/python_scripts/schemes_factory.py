""" Project: SolversApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""


# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolversApplication as KratosSolver

# SolutionScheme class


class SolutionScheme:

    def __init__(self, custom_settings, dofs_list):

        default_settings = KratosMultiphysics.Parameters("""
        {
           "solution_type": "Dynamic",
           "analysis_type": "Non-linear",
           "formulation_type": "Lagrangian",
           "time_integration": "Implicit",
           "integration_method": "Newmark",
           "prediction_level": 0,
           "time_integration_order": 1,
           "buffer_size": 2,
           "move_mesh_flag": true,
           "incremental_update": true,
           "integration_variables": []
        }
        """)

        # Overwrite the default settings with user-provided parameters
        self.settings = custom_settings
        self.settings.ValidateAndAssignDefaults(default_settings)

        self.dofs = []
        for i in range(0, dofs_list.size()):
            self.dofs.append(dofs_list[i].GetString())

        # add default DISPLACEMENT dof
        if(len(self.dofs) == 0 or (len(self.dofs) == 1 and self.dofs[0] == "ROTATION")):
            self.dofs.append('DISPLACEMENT')

        self.integration_variables = []
        for i in range(0, self.settings["integration_variables"].size()):
            self.integration_variables.append(
                self.settings["integration_variables"][i].GetString())

    def GetVariables(self):

        self._set_variables_and_dofs()

        nodal_variables = self.nodal_variables + self.dof_variables + \
            self.dof_reactions + self.dof_derivatives

        return nodal_variables

    def GetDofsAndReactions(self):

        if not hasattr(self, '_dof_variables'):
            self._set_variables_and_dofs()

        return self.dof_variables, self.dof_reactions

    def GetSolutionScheme(self):

        vector_integration_methods = []
        scalar_integration_methods = []

        for dof in self.dofs:

            # check if variable type is a vector
            kratos_variable = KratosMultiphysics.KratosGlobals.GetVariable(dof)
            if(isinstance(kratos_variable, KratosMultiphysics.Array1DVariable3)):

                integration_method_name = self._get_integration_method_name(
                    dof)
                vector_integration_method = None

                variable_names = self._get_integration_method_variables(dof)
                variables = []
                for variable in variable_names:
                    variables = variables + \
                        [KratosMultiphysics.KratosGlobals.GetVariable(
                            variable)]

                integration_method = None
                vector_integration_method = getattr(
                    KratosSolver, integration_method_name+'VectorIntegration')
                if len(variables) == 4:
                    integration_method = vector_integration_method(
                        variables[0], variables[1], variables[2], variables[3])
                    print(self._class_prefix(),
                          "Primary Vector Variable ["+variables[3].Name()+"]")
                elif len(variables) == 1:
                    integration_method = vector_integration_method(
                        variables[0])
                    print(self._class_prefix(),
                          "Primary Vector Variable ["+variables[0].Name()+"]")
                else:
                    raise Exception('len(variables) = ' + str(len(variables)))

                if(integration_method_name.find("Step") != -1):
                    step_variable_name = 'STEP_'+variables[0].Name()
                    integration_method.SetStepVariable(
                        KratosMultiphysics.KratosGlobals.GetVariable(step_variable_name))
                    print(self._class_prefix(),
                          "Step Variable ["+step_variable_name+"]")

                vector_integration_methods.append(integration_method)

            elif(isinstance(kratos_variable, KratosMultiphysics.DoubleVariable)):

                integration_method_name = self._get_integration_method_name(
                    dof)
                scalar_integration_method = None

                variable_names = self._get_integration_method_variables(dof)
                variables = []
                for variable in variable_names:
                    variables = variables + \
                        [KratosMultiphysics.KratosGlobals.GetVariable(
                            variable)]

                integration_method = None
                if len(variables) == 4:
                    scalar_integration_method = getattr(
                        KratosSolver, integration_method_name+'ScalarIntegration')
                    integration_method = scalar_integration_method(
                        variables[0], variables[1], variables[2], variables[3])
                    print(self._class_prefix(),
                          "Primary Scalar Variable ["+variables[3].Name()+"]")
                elif len(variables) == 1:
                    if(integration_method_name.find("Step") != -1):
                        scalar_integration_method = getattr(
                            KratosSolver, 'StaticStepScalarIntegration')
                    else:
                        scalar_integration_method = getattr(
                            KratosSolver, 'StaticScalarIntegration')
                    integration_method = scalar_integration_method(
                        variables[0])
                    print(self._class_prefix(),
                          "Primary Scalar Variable ["+variables[0].Name()+"]")
                else:
                    raise Exception('len(variables) = ' + str(len(variables)))

                if(integration_method_name.find("Step") != -1):
                    step_variable_name = 'STEP_'+variables[0].Name()
                    integration_method.SetStepVariable(
                        KratosMultiphysics.KratosGlobals.GetVariable(step_variable_name))

                scalar_integration_methods.append(integration_method)

            else:
                raise Exception(
                    "DOF is incorrect. Must be a three-component vector or a scalar.")

        solution_scheme = None
        options = KratosMultiphysics.Flags()
        options.Set(KratosSolver.SolverLocalFlags.INCREMENTAL_SOLUTION,self.settings["incremental_update"].GetBool())
        if(self.settings["solution_type"].GetString() == "Dynamic"):
            if(self.settings["time_integration"].GetString() == "Implicit"):
                if(self.settings["formulation_type"].GetString() == "ALE"):
                    if(len(vector_integration_methods) and len(scalar_integration_methods)):
                        import KratosMultiphysics.PfemApplication as KratosPfem
                        # number of lagrangian layers (-1 if all lagrangian, 0 is one layer ...)
                        lagrangian_layers = 3
                        # interaction layer eulerian (True) or lagrangian (False)
                        eulerian_interaction_layer = True
                        options.Set(KratosSolver.SolverLocalFlags.MOVE_MESH,
                                    self.settings["move_mesh_flag"].GetBool())
                        solution_scheme = KratosPfem.AleDynamicScheme(
                            vector_integration_methods, scalar_integration_methods, options, lagrangian_layers, eulerian_interaction_layer)
                        print(self._class_prefix(),
                              "Dynamic ALE Scheme with vectors and scalars")
                    else:
                        raise Exception(
                            "ALE scheme needs vector and scalar integration methods.")
                else:
                    if(len(vector_integration_methods)):
                        if(len(scalar_integration_methods)):
                            solution_scheme = KratosSolver.DynamicScheme(
                                vector_integration_methods, scalar_integration_methods, options)
                            print(self._class_prefix(),
                                  "Dynamic Scheme with vectors and scalars")
                        else:
                            solution_scheme = KratosSolver.DynamicScheme(
                                vector_integration_methods, options)
                            print(self._class_prefix(),
                                  "Dynamic Scheme with vectors only")
                    elif(len(scalar_integration_methods)):
                        solution_scheme = KratosSolver.DynamicScheme(
                            vector_integration_methods, scalar_integration_methods, options)
                        print(self._class_prefix(),
                              "Dynamic Scheme with scalars only")
                    else:
                        print("WARNING: no integration methods")

        elif(self.settings["solution_type"].GetString() == "Static" or self.settings["solution_type"].GetString() == "Quasi-static"):
            if(len(vector_integration_methods)):
                if self.settings["prediction_level"].GetInt() != 0:
                    options.Set(KratosSolver.TimeIntegrationLocalFlags.PREDICT_PRIMARY_VARIABLE,True)
                if(len(scalar_integration_methods)):
                    solution_scheme = KratosSolver.StaticScheme(
                        vector_integration_methods, scalar_integration_methods, options)
                    print(self._class_prefix(),
                          "Static Scheme with vectors and scalars")
                else:
                    solution_scheme = KratosSolver.StaticScheme(
                        vector_integration_methods, options)
                    print(self._class_prefix(),
                          "Static Scheme with vectors only")
            elif(len(scalar_integration_methods)):
                solution_scheme = KratosSolver.StaticScheme(
                    vector_integration_methods, scalar_integration_methods, options)
                print(self._class_prefix(), "Static Scheme with scalars only")
            else:
                print("WARNING: no integration methods")

        print(self._class_prefix(), "Variables", self.dofs)
        if len(vector_integration_methods) != 0:
            print(self._class_prefix(), "Vector Integration",
                  vector_integration_methods)
        if len(scalar_integration_methods) != 0:
            print(self._class_prefix(), "Scalar Integration",
                  scalar_integration_methods)

        return solution_scheme

    #
    def GetComponentIntegrationMethods(self):

        # set solution scheme and integration method dictionary
        integration_methods = {}

        all_integration_variables = self.dofs + self.integration_variables

        for var in all_integration_variables:

            integration_method_name = self._get_integration_method_name(var)
            variable_names = self._get_integration_method_variables(var)

            # check if variable type is a vector
            kratos_variable = KratosMultiphysics.KratosGlobals.GetVariable(var)
            if(isinstance(kratos_variable, KratosMultiphysics.Array1DVariable3)):

                component_integration_method = None

                variable_components = ['_X', '_Y', '_Z']
                for component in variable_components:
                    variables = []
                    for variable in variable_names:
                        variables = variables + \
                            [KratosMultiphysics.KratosGlobals.GetVariable(
                                variable+component)]

                    integration_method = None
                    if(len(variables) == 4):
                        component_integration_method = getattr(
                            KratosSolver, integration_method_name+'ScalarIntegration')
                        integration_method = component_integration_method(
                            variables[0], variables[1], variables[2], variables[3])
                    elif(len(variables) == 1):
                        if(integration_method_name.find("Step") != -1):
                            component_integration_method = getattr(
                                KratosSolver, 'StaticStepScalarIntegration')
                        else:
                            component_integration_method = getattr(
                                KratosSolver, 'StaticScalarIntegration')
                        integration_method = component_integration_method(
                            variables[0])
                    else:
                        raise Exception('len(variables) = ' +
                                        str(len(variables)))

                    if(integration_method_name.find("Step") != -1):
                        step_variable_name = 'STEP_'+variables[0].Name()
                        integration_method.SetStepVariable(
                            KratosMultiphysics.KratosGlobals.GetVariable(step_variable_name))

                    integration_methods.update(
                        {variables[0].Name(): integration_method})
                    if(len(variables) == 4):
                        integration_methods.update(
                            {variables[1].Name(): integration_method})
                        integration_methods.update(
                            {variables[2].Name(): integration_method})

        #print("Component time integration methods ", integration_methods)
        if len(integration_methods) != 0:
            print(self._class_prefix(), "Component Integration", integration_methods)

        return integration_methods

    #
    def GetScalarIntegrationMethods(self):

        # set solution scheme and integration method dictionary
        integration_methods = {}

        all_integration_variables = self.dofs + self.integration_variables

        for var in all_integration_variables:

            integration_method_name = self._get_integration_method_name(var)
            variable_names = self._get_integration_method_variables(var)

            # check if variable type is a scalar
            kratos_variable = KratosMultiphysics.KratosGlobals.GetVariable(var)
            if(isinstance(kratos_variable, KratosMultiphysics.DoubleVariable)):

                scalar_integration_method = None

                variables = []
                for variable in variable_names:
                    variables = variables + \
                        [KratosMultiphysics.KratosGlobals.GetVariable(
                            variable)]

                integration_method = None
                if(len(variables) == 4):
                    scalar_integration_method = getattr(
                        KratosSolver, integration_method_name+'ScalarIntegration')
                    integration_method = scalar_integration_method(
                        variables[0], variables[1], variables[2], variables[3])
                elif(len(variables) == 1):
                    if(integration_method_name.find("Step") != -1):
                        scalar_integration_method = getattr(
                            KratosSolver, 'StaticStepScalarIntegration')
                    else:
                        scalar_integration_method = getattr(
                            KratosSolver, 'StaticScalarIntegration')
                    integration_method = scalar_integration_method(
                        variables[0])
                else:
                    raise Exception('len(variables) = ' + str(len(variables)))

                if(integration_method_name.find("Step") != -1):
                    step_variable_name = 'STEP_'+var
                    integration_method.SetStepVariable(
                        KratosMultiphysics.KratosGlobals.GetVariable(step_variable_name))

                integration_methods.update(
                    {variables[0].Name(): integration_method})

        return integration_methods

    #
    def GetBufferSize(self):
        buffer_size = self.settings["buffer_size"].GetInt()
        time_integration_order = self.settings["time_integration_order"].GetInt(
        )
        if(buffer_size <= time_integration_order):
            buffer_size = time_integration_order + 1
        return buffer_size

    #
    def GetTimeIntegrationOrder(self):
        time_integration_order = self.settings["time_integration_order"].GetInt(
        )
        return time_integration_order

    #
    def _get_integration_method_name(self, dof):

        integration_method_name = self.settings["integration_method"].GetString(
        )

        if integration_method_name.find("Step") != -1:
            if dof == 'ROTATION':
                integration_method_name = integration_method_name+'Rotation'
            elif dof != 'DISPLACEMENT' and dof != 'VELOCITY':
                start = 0
                end = integration_method_name.find("Step")
                integration_method_name = integration_method_name[start:end]
                # print(print(self._class_prefix(),Step Method Changed ("+integration_method_name+":"+dof+")")

        return integration_method_name

    #
    def _get_integration_method_variables(self, var):

        variables = []
        if(self.settings["solution_type"].GetString() == "Dynamic"):
            if(var == 'DISPLACEMENT' or var == 'VELOCITY' or var == 'ACCELERATION'):
                variables = variables + \
                    ['DISPLACEMENT', 'VELOCITY', 'ACCELERATION', var]
            elif(var == 'ROTATION' or var == 'ANGULAR_VELOCITY' or var == 'ANGULAR_ACCELERATION'):
                variables = variables + \
                    ['ROTATION', 'ANGULAR_VELOCITY', 'ANGULAR_ACCELERATION', var]
            elif(var == 'WATER_DISPLACEMENT'):
                variables = variables + \
                    ['WATER_DISPLACEMENT', 'WATER_VELOCITY',
                        'WATER_ACCELERATION', var]
            elif(var == 'WATER_PRESSURE'):
                variables = variables + \
                    ['WATER_PRESSURE', 'WATER_PRESSURE_VELOCITY',
                        'WATER_PRESSURE_ACCELERATION', var]
            elif(var == 'PRESSURE'):
                variables = variables + \
                    ['PRESSURE', 'PRESSURE_VELOCITY', 'PRESSURE_ACCELERATION', var]
            elif(var == 'FLUID_PRESSURE'):
                variables = variables + \
                    ['FLUID_PRESSURE', 'FLUID_PRESSURE_VELOCITY',
                        'FLUID_PRESSURE_ACCELERATION', var]
            elif(var == 'MESH_DISPLACEMENT' or var == 'MESH_VELOCITY' or var == 'MESH_ACCELERATION'):
                variables = variables + \
                    ['MESH_DISPLACEMENT', 'MESH_VELOCITY', 'MESH_ACCELERATION', var]
            else:
                variables = variables + [var]
        else:
            variables = variables + [var]

        return variables

    #
    def _set_variables_and_dofs(self):

        # Variables and Dofs settings
        self.nodal_variables = []
        self.dof_variables = []
        self.dof_reactions = []
        self.dof_derivatives = []

        # Add displacement variables
        if self._check_input_dof("DISPLACEMENT"):
            self.dof_variables = self.dof_variables + ['DISPLACEMENT']
            self.dof_reactions = self.dof_reactions + ['DISPLACEMENT_REACTION']

            # Add dynamic variables
            if(self.settings["solution_type"].GetString() == "Dynamic"):
                self.dof_derivatives = self.dof_derivatives + \
                    ['VELOCITY', 'ACCELERATION']

            if self.settings["integration_method"].GetString().find("Step") != -1:
                self.nodal_variables = self.nodal_variables + \
                    ['STEP_DISPLACEMENT']

        if self._check_input_dof("VELOCITY"):
            # Add specific variables for the problem (velocity dofs)
            self.dof_variables = self.dof_variables + ['VELOCITY']
            self.dof_reactions = self.dof_reactions + ['VELOCITY_REACTION']

            # Add dynamic variables
            self.dof_derivatives = self.dof_derivatives + \
                ['DISPLACEMENT', 'ACCELERATION']

            if self.settings["integration_method"].GetString().find("Step") != -1:
                self.nodal_variables = self.nodal_variables + \
                    ['STEP_DISPLACEMENT']

        # Add rotational variables
        if self._check_input_dof("ROTATION"):
            # Add specific variables for the problem (rotation dofs)
            self.dof_variables = self.dof_variables + ['ROTATION']
            self.dof_reactions = self.dof_reactions + ['ROTATION_REACTION']

            if(self.settings["solution_type"].GetString() == "Dynamic"):
                self.dof_derivatives = self.dof_derivatives + \
                    ['ANGULAR_VELOCITY', 'ANGULAR_ACCELERATION']
            # Add large rotation variables
            if self.settings["integration_method"].GetString().find("Step") != -1:
                self.nodal_variables = self.nodal_variables + ['STEP_ROTATION']

        # Add pressure variables
        if self._check_input_dof("PRESSURE"):
            # Add specific variables for the problem (pressure dofs)
            self.dof_variables = self.dof_variables + ['PRESSURE']
            self.dof_reactions = self.dof_reactions + ['PRESSURE_REACTION']

            if(self.settings["solution_type"].GetString() == "Dynamic"):
                self.dof_derivatives = self.dof_derivatives + \
                    ['PRESSURE_VELOCITY', 'PRESSURE_ACCELERATION']

        # Add fluid pressure variables
        if self._check_input_dof("FLUID_PRESSURE"):
            # Add specific variables for the problem (pressure dofs)
            self.dof_variables = self.dof_variables + ['FLUID_PRESSURE']
            self.dof_reactions = self.dof_reactions + \
                ['FLUID_PRESSURE_REACTION']

            self.dof_derivatives = self.dof_derivatives + \
                ['FLUID_PRESSURE_VELOCITY', 'FLUID_PRESSURE_ACCELERATION']

        # Ass thermal variables
        if self._check_input_dof("TEMPERATURE"):
            self.dof_variables = self.dof_variables + ['TEMPERATURE']
            self.dof_reactions = self.dof_reactions + ['TEMPERATURE_REACTION']

        # Add contat variables
        if self._check_input_dof("LAGRANGE_MULTIPLIER_NORMAL"):
            # Add specific variables for the problem (contact dofs)
            self.dof_variables = self.dof_variables + \
                ['LAGRANGE_MULTIPLIER_NORMAL']
            self.dof_reactions = self.dof_reactions + \
                ['LAGRANGE_MULTIPLIER_NORMAL_REACTION']

        # Add water displacement variables
        if self._check_input_dof("WATER_DISPLACEMENT"):
            self.dof_variables = self.dof_variables + \
                ['WATER_DISPLACEMENT']
            self.dof_reactions = self.dof_reactions + \
                ['WATER_DISPLACEMENT_REACTION']
            self.dof_derivatives = self.dof_derivatives + ['WATER_VELOCITY', 'WATER_ACCELERATION']

        # Add water pressure variables
        if self._check_input_dof("WATER_PRESSURE"):
            self.dof_variables = self.dof_variables + \
                ['WATER_PRESSURE']
            self.dof_reactions = self.dof_reactions + \
                ['REACTION_WATER_PRESSURE']
            if(self.settings["solution_type"].GetString() == "Dynamic"):
                self.dof_derivatives = self.dof_derivatives + ['WATER_PRESSURE_VELOCITY', 'WATER_PRESSURE_ACCELERATION']

        # Add jacobian variables
        if self._check_input_dof("JACOBIAN"):
            self.dof_variables = self.dof_variables + ['JACOBIAN']
            self.dof_reactions = self.dof_reactions + ['REACTION_JACOBIAN']

        # Add ale mesh variables
        if self._check_input_variable("MESH_VELOCITY"):
            self.nodal_variables = self.nodal_variables + \
                ['MESH_DISPLACEMENT', 'MESH_VELOCITY', 'MESH_ACCELERATION']

    def _check_input_variable(self, variable):

        for i in self.integration_variables:
            if i == variable:
                return True

        return False

    def _check_input_dof(self, variable):

        for i in self.dofs:
            if i == variable:
                return True

        # temporary default
        if(variable == 'DISPLACEMENT' and len(self.dofs) == 0):
            return True

        return False

    #
    @classmethod
    def _class_prefix(self):
        header = "::[---Scheme Factory--]::"
        return header
