//
//   Project Name:        KratosSolversApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:     December 2020 $
//
//

// System includes

// External includes

// Project includes
#include "custom_constraints/local_constraint.hpp"
#include "solvers_application_variables.h"

namespace Kratos
{

const std::array<const VariableData, 3> LocalConstraint::msDisplacementDofs = {DISPLACEMENT_X, DISPLACEMENT_Y, DISPLACEMENT_Z};
const std::array<const VariableData, 3> LocalConstraint::msVelocityDofs = {VELOCITY_X, VELOCITY_Y, VELOCITY_Z};
const std::array<const VariableData, 3> LocalConstraint::msRotationDofs = {ROTATION_X, ROTATION_Y, ROTATION_Z};

//***********************************************************************************
//***********************************************************************************
LocalConstraint::LocalConstraint()
{
}

//***********************************************************************************
//***********************************************************************************
LocalConstraint::LocalConstraint(ConstraintData& rConstraint) : mpConstraint(&rConstraint)
{

}


//***********************************************************************************
//***********************************************************************************
LocalConstraint::LocalConstraint(LocalConstraint const &rOther)
  :mpConstraint(rOther.mpConstraint)
{
}

//***********************************************************************************
//***********************************************************************************
LocalConstraint::~LocalConstraint()
{
}

//************************************************************************************
//************************************************************************************

void LocalConstraint::Rotate(MatrixType &rLeftHandSideMatrix,
                             VectorType &rRightHandSideVector,
                             const DofsVectorType &rLocalDofList,
                             const GeometryType &rGeometry,
                             const unsigned int &rNodeId) const
{
  KRATOS_TRY

  //get constrained dofs start position in matrix blocks
  SizeType BlockStart;
  if (this->GetBlockStart(rLocalDofList, BlockStart)){

    //std::cout<<" BlockStart "<<BlockStart<<std::endl;
    const SizeType NumBlocks = rGeometry.size();
    const SizeType BlockSize = static_cast<SizeType>(rLocalDofList.size()/double(NumBlocks));

    Matrix RotationMatrix;
    this->GetLocalRotationMatrix(rGeometry[rNodeId], RotationMatrix, BlockSize, rGeometry.WorkingSpaceDimension(), BlockStart);
    //std::cout<<" RotationMatrix  "<<RotationMatrix<<std::endl;
    this->RotateMatrix(rLeftHandSideMatrix, BlockSize, NumBlocks, RotationMatrix, rGeometry, rNodeId);
    this->RotateVector(rRightHandSideVector, BlockSize, NumBlocks, RotationMatrix, rGeometry, rNodeId);
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void LocalConstraint::Rotate(MatrixType &rLeftHandSideMatrix,
                             const DofsVectorType &rLocalDofList,
                             const GeometryType &rGeometry,
                             const unsigned int &rNodeId) const
{
  KRATOS_TRY

  //get constrained dofs start position in matrix blocks
  SizeType BlockStart;
  if (this->GetBlockStart(rLocalDofList, BlockStart)){

    const SizeType NumBlocks = rGeometry.size();
    const SizeType BlockSize = static_cast<SizeType>(rLocalDofList.size()/double(NumBlocks));

    Matrix RotationMatrix;
    this->GetLocalRotationMatrix(rGeometry[rNodeId], RotationMatrix, BlockSize, rGeometry.WorkingSpaceDimension(), BlockStart);

    this->RotateMatrix(rLeftHandSideMatrix, BlockSize, NumBlocks, RotationMatrix, rGeometry, rNodeId);
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void LocalConstraint::Rotate(VectorType &rRightHandSideVector,
                             const DofsVectorType &rLocalDofList,
                             const GeometryType &rGeometry,
                             const unsigned int &rNodeId) const
{
  KRATOS_TRY

  //get constrained dofs start position in matrix blocks
  SizeType BlockStart;
  if (this->GetBlockStart(rLocalDofList, BlockStart)){

    const SizeType NumBlocks = rGeometry.size();
    const SizeType BlockSize = static_cast<SizeType>(rLocalDofList.size()/double(NumBlocks));

    Matrix RotationMatrix;
    this->GetLocalRotationMatrix(rGeometry[rNodeId], RotationMatrix, BlockSize, rGeometry.WorkingSpaceDimension(), BlockStart);

    this->RotateVector(rRightHandSideVector, BlockSize, NumBlocks, RotationMatrix, rGeometry, rNodeId);
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void LocalConstraint::Assign(MatrixType &rLeftHandSideMatrix,
                             VectorType &rRightHandSideVector,
                             const DofsVectorType &rLocalDofList,
                             const array_1d<double,3> &rValues,
                             const GeometryType &rGeometry,
                             const unsigned int &rNodeId) const
{
  KRATOS_TRY

  //get constrained dofs start position in matrix blocks
  SizeType BlockStart;
  if (this->GetBlockStart(rLocalDofList, BlockStart)){

    const SizeType NumBlocks = rGeometry.size();
    const SizeType BlockSize = static_cast<SizeType>(rLocalDofList.size()/double(NumBlocks));

    this->AssignMatrix(rLeftHandSideMatrix, BlockStart, BlockSize, NumBlocks, rNodeId);
    this->AssignVector(rRightHandSideVector, BlockStart, BlockSize, NumBlocks, rValues, rGeometry, rNodeId);
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void LocalConstraint::Assign(MatrixType &rLeftHandSideMatrix,
                             const DofsVectorType &rLocalDofList,
                             const GeometryType &rGeometry,
                             const unsigned int &rNodeId) const
{
  KRATOS_TRY

  //get constrained dofs start position in matrix blocks
  SizeType BlockStart;
  if (this->GetBlockStart(rLocalDofList, BlockStart)){

    const SizeType NumBlocks = rGeometry.size();
    const SizeType BlockSize = static_cast<SizeType>(rLocalDofList.size()/double(NumBlocks));

    this->AssignMatrix(rLeftHandSideMatrix, BlockStart, BlockSize, NumBlocks, rNodeId);
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void LocalConstraint::Assign(VectorType &rRightHandSideVector,
                             const DofsVectorType &rLocalDofList,
                             const array_1d<double,3> &rValues,
                             const GeometryType &rGeometry,
                             const unsigned int &rNodeId) const
{
  KRATOS_TRY

  //get constrained dofs start position in matrix blocks
  SizeType BlockStart;
  if (this->GetBlockStart(rLocalDofList, BlockStart)){

    const SizeType NumBlocks = rGeometry.size();
    const SizeType BlockSize = static_cast<SizeType>(rLocalDofList.size()/double(NumBlocks));

    this->AssignVector(rRightHandSideVector, BlockStart, BlockSize, NumBlocks, rValues, rGeometry, rNodeId);
  }

  KRATOS_CATCH("")
}


//************************************************************************************
//************************************************************************************

void LocalConstraint::MapToLocal(NodeType& rNode, const SizeType& rDimension, std::string ExtraVariableName) const
{
  KRATOS_TRY

  Matrix RotationMatrix;
  this->GetLocalRotationMatrix(rNode, RotationMatrix, 3, rDimension);

  const Variable<array_1d<double, 3>>& VariableToRotate = KratosComponents<Variable<array_1d<double, 3>>>::Get(mpConstraint->VariableName);
  array_1d<double, 3> &rVariable = rNode.FastGetSolutionStepValue(VariableToRotate);

  //std::cout<<" Node Global "<<rNode.Id()<<" "<<VariableToRotate.Name()<<" "<<rVariable;

  rVariable = prod(RotationMatrix, rVariable);

  //std::cout<<" To Local "<<rVariable<<std::endl;

  //std::cout<<" Rotation Matrix "<<RotationMatrix<<std::endl;

  // if (VariableToRotate.Name() == "ROTATION")
  //   ExtraVariableName = "STEP_ROTATION";

  if (ExtraVariableName != "None")
  {
    const Variable<array_1d<double, 3>>& ExtraVariableToRotate = KratosComponents<Variable<array_1d<double, 3>>>::Get(ExtraVariableName);
    array_1d<double, 3> &rExtraVariable = rNode.FastGetSolutionStepValue(ExtraVariableToRotate);
    rExtraVariable = prod(RotationMatrix, rExtraVariable);
    //std::cout<<" Node td "<<rNode.Id()<<" Rot "<<RotationMatrix<<" "<<ExtraVariableToRotate.Name()<<" "<<rExtraVariable<<std::endl;
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void LocalConstraint::MapToGlobal(NodeType& rNode, const SizeType& rDimension, std::string ExtraVariableName) const
{
  KRATOS_TRY

  Matrix RotationMatrix;
  this->GetLocalRotationMatrix(rNode, RotationMatrix, 3, rDimension);

  const Variable<array_1d<double, 3>>& VariableToRotate = KratosComponents<Variable<array_1d<double, 3>>>::Get(mpConstraint->VariableName);
  array_1d<double, 3>& rVariable = rNode.FastGetSolutionStepValue(VariableToRotate);

  //std::cout<<" Node Local "<<rNode.Id()<<" "<<VariableToRotate.Name()<<" "<<rVariable;

  rVariable = prod(trans(RotationMatrix), rVariable);

  //std::cout<<" To Global "<<rVariable<<std::endl;

  //std::cout<<" Rotation Matrix "<<RotationMatrix<<std::endl;

  // if (VariableToRotate.Name() == "ROTATION")
  //   ExtraVariableName = "STEP_ROTATION";

  if (ExtraVariableName != "None")
  {
    const Variable<array_1d<double, 3>>& ExtraVariableToRotate = KratosComponents<Variable<array_1d<double, 3>>>::Get(ExtraVariableName);
    array_1d<double, 3> &rExtraVariable = rNode.FastGetSolutionStepValue(ExtraVariableToRotate);
    rExtraVariable = prod(trans(RotationMatrix), rExtraVariable);
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void LocalConstraint::MapToLocal(NodeType& rNode,
                                 const SizeType& rDimension,
                                 const std::vector<std::string> &rVariableNames) const
{
  KRATOS_TRY

  Matrix RotationMatrix;
  this->GetLocalRotationMatrix(rNode, RotationMatrix, 3, rDimension);

  for (const auto& name: rVariableNames)
  {
    const Variable<array_1d<double, 3>>& VariableToRotate = KratosComponents<Variable<array_1d<double, 3>>>::Get(name);
    array_1d<double, 3>& rVariable = rNode.FastGetSolutionStepValue(VariableToRotate);
    //std::cout<<name<<" MAP global node["<<rNode.Id()<<"] "<<rVariable<<" ";
    rVariable = prod(RotationMatrix, rVariable);
    array_1d<double, 3>& rPreVariable = rNode.FastGetSolutionStepValue(VariableToRotate,1);
    //std::cout<<rPreVariable<<std::endl;
    rPreVariable = prod(RotationMatrix, rPreVariable);
    //std::cout<<name<<" to local node["<<rNode.Id()<<"] "<<rVariable<<" "<<rPreVariable<<std::endl;
  }

  // const Variable<array_1d<double, 3>>& VariableToRotate = KratosComponents<Variable<array_1d<double, 3>>>::Get(mpConstraint->VariableName);
  // array_1d<double, 3>& rVariable = rNode.FastGetSolutionStepValue(VariableToRotate);
  // // QuaternionType VariableQuaternion = QuaternionType::FromRotationVector(rVariable);
  // // VariableQuaternion.ToRotationVector(rVariable);
  // std::cout<<mpConstraint->VariableName<<" Quaternion Node["<<rNode.Id()<<"] "<<rVariable<<" "<<std::endl;

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void LocalConstraint::MapToGlobal(NodeType& rNode,
                                 const SizeType& rDimension,
                                 const std::vector<std::string> &rVariableNames) const
{
  KRATOS_TRY

  Matrix RotationMatrix;
  this->GetLocalRotationMatrix(rNode, RotationMatrix, 3, rDimension);

  for (const auto& name: rVariableNames)
  {
    const Variable<array_1d<double, 3>>& VariableToRotate = KratosComponents<Variable<array_1d<double, 3>>>::Get(name);
    array_1d<double, 3>& rVariable = rNode.FastGetSolutionStepValue(VariableToRotate);
    //std::cout<<name<<" MAP local node["<<rNode.Id()<<"] "<<rVariable<<" ";
    rVariable = prod(trans(RotationMatrix), rVariable);
    array_1d<double, 3>& rPreVariable = rNode.FastGetSolutionStepValue(VariableToRotate,1);
    //std::cout<<rPreVariable<<std::endl;
    rPreVariable = prod(trans(RotationMatrix), rPreVariable);

    //std::cout<<name<<" to global node["<<rNode.Id()<<"] "<<rVariable<<" "<<rPreVariable<<std::endl;
  }

  // const Variable<array_1d<double, 3>>& VariableToRotate = KratosComponents<Variable<array_1d<double, 3>>>::Get(mpConstraint->VariableName);
  // array_1d<double, 3>& rVariable = rNode.FastGetSolutionStepValue(VariableToRotate);
  // // QuaternionType VariableQuaternion = QuaternionType::FromRotationVector(rVariable);
  // // VariableQuaternion.ToRotationVector(rVariable);
  // std::cout<<mpConstraint->VariableName<<" Quaternion Node["<<rNode.Id()<<"] "<<rVariable<<" "<<std::endl;

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void LocalConstraint::MapToGlobalAndAssign(NodeType& rNode,
                                           const SizeType &rDimension,
                                           array_1d<double,3> &rValues,
                                           std::string VariableName,
                                           std::vector<ScalarIntegrationPointer> &rTimeIntegration,
                                           const std::vector<std::string> &rVariableNames) const
{
  KRATOS_TRY

  this->MapToLocal(rNode, rDimension, rVariableNames);

  const Variable<array_1d<double, 3>>& VariableToAssign = KratosComponents<Variable<array_1d<double, 3>>>::Get(VariableName);
  array_1d<double, 3>& rVariable = rNode.FastGetSolutionStepValue(VariableToAssign);

  for (const auto& Direction: mpConstraint->Dofs)
  {
    (*(mpConstraint->AssignmentMethod))(rVariable[Direction], rValues[Direction]);
  }

  for (const auto& integrate: rTimeIntegration)
    integrate->Assign(rNode);

  this->MapToGlobal(rNode, rDimension, rVariableNames);

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

array_1d<double,3> LocalConstraint::GetLocalValue(NodeType& rNode,
                                                  const SizeType& rDimension) const
{
  KRATOS_TRY

  Matrix RotationMatrix;
  this->GetLocalRotationMatrix(rNode, RotationMatrix, 3, rDimension);

  const Variable<array_1d<double, 3>>& VariableToRotate = KratosComponents<Variable<array_1d<double, 3>>>::Get(mpConstraint->VariableName);
  array_1d<double, 3> rVariable = rNode.FastGetSolutionStepValue(VariableToRotate);
  rVariable = prod(RotationMatrix, rVariable);

  return rVariable;

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

bool LocalConstraint::CheckDofs(const DofsVectorType& rLocalDofList) const
{
  if (mpConstraint->Variables == nullptr)
    return false;

  for (const auto &local_dof : rLocalDofList)
  {
    for (const auto &variable : *mpConstraint->Variables)
    {
      if (local_dof->GetVariable() == variable){
        return true;
      }
    }
  }
  return false;
}

//************************************************************************************
//************************************************************************************

bool LocalConstraint::GetBlockStart(const DofsVectorType& rLocalDofList, SizeType& rStart) const
{
  if (mpConstraint->Variables == nullptr)
    return false;

  rStart = 0;
  bool dof_set = false;
  for (const auto &local_dof : rLocalDofList)
  {
    for (const auto &variable : *mpConstraint->Variables)
    {
      if (local_dof->GetVariable() == variable){
        dof_set = true;
        break;
      }
    }
    if (dof_set)
      break;
    ++rStart;
  }
  return dof_set;
}

//************************************************************************************
//************************************************************************************

void LocalConstraint::GetBlockMatrix(MatrixType &BlockMatrix,
                                     const MatrixType &SourceMatrix,
                                     const SizeType i_begin,
                                     const SizeType j_begin,
                                     const SizeType& size) const
{
  for (unsigned int i = 0; i < size; i++)
  {
    for (unsigned int j = 0; j < size; j++)
    {
      BlockMatrix(i, j) = SourceMatrix(i_begin + i, j_begin + j);
    }
  }
}

//************************************************************************************
//************************************************************************************

void LocalConstraint::SetBlockMatrix(const MatrixType &BlockMatrix,
                                     MatrixType &DestinationMatrix,
                                     const SizeType i_begin,
                                     const SizeType j_begin,
                                     const SizeType& size) const
{
  for (unsigned int i = 0; i < size; i++)
  {
    for (unsigned int j = 0; j < size; j++)
    {
      DestinationMatrix(i_begin + i, j_begin + j) = BlockMatrix(i, j);
    }
  }
}

//************************************************************************************
//************************************************************************************

void LocalConstraint::RotateMatrix(MatrixType &rLeftHandSideMatrix,
                                   const SizeType& rBlockSize,
                                   const SizeType& rNumBlocks,
                                   const Matrix &rRotationMatrix,
                                   const GeometryType &rGeometry,
                                   const unsigned int &rNodeId) const
{
  KRATOS_TRY

  MatrixType BlockMatrix(rBlockSize,rBlockSize);
  MatrixType SemiBlockMatrix(rBlockSize,rBlockSize);;

  SizeType i_start,j_start;
  for (SizeType i = 0; i < rNumBlocks; ++i)
  {
    i_start = i * rBlockSize;
    if (i == rNodeId)
    {
      for (SizeType j = 0; j < rNumBlocks; ++j)
      {
        j_start = j * rBlockSize;
        if (j == rNodeId)
        {
          //std::cout<<" Rotate ["<<i_start<<", "<<j_start<<"] "<<rBlockSize<<std::endl;
          GetBlockMatrix(BlockMatrix, rLeftHandSideMatrix, i_start, j_start, rBlockSize);
          noalias(SemiBlockMatrix) = prod(BlockMatrix, trans(rRotationMatrix));
          noalias(BlockMatrix) = prod(rRotationMatrix, SemiBlockMatrix);
          SetBlockMatrix(BlockMatrix, rLeftHandSideMatrix, i_start, j_start, rBlockSize);
        }
        else
        {
          //std::cout<<" Semi Rotate ["<<i_start<<", "<<j_start<<"] "<<rBlockSize<<std::endl;
          GetBlockMatrix(BlockMatrix, rLeftHandSideMatrix, i_start, j_start, rBlockSize);
          noalias(SemiBlockMatrix) = prod(rRotationMatrix, BlockMatrix);
          SetBlockMatrix(SemiBlockMatrix, rLeftHandSideMatrix, i_start, j_start, rBlockSize);
        }
      }
    }
    else
    {
      for (SizeType j = 0; j < rNumBlocks; ++j)
      {
        j_start = j * rBlockSize;
        if (j == rNodeId)
        {
          //std::cout<<" Semi Rotate ["<<i_start<<", "<<j_start<<"] "<<rBlockSize<<std::endl;
          GetBlockMatrix(BlockMatrix, rLeftHandSideMatrix, i_start, j_start, rBlockSize);
          noalias(SemiBlockMatrix) = prod(BlockMatrix, trans(rRotationMatrix));
          SetBlockMatrix(SemiBlockMatrix, rLeftHandSideMatrix, i_start, j_start, rBlockSize);
        }
      }
    }
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void LocalConstraint::RotateVector(VectorType &rRightHandSideVector,
                                   const SizeType& rBlockSize,
                                   const SizeType& rNumBlocks,
                                   const Matrix &rRotationMatrix,
                                   const GeometryType &rGeometry,
                                   const unsigned int &rNodeId) const
{
  KRATOS_TRY

  Vector BlockVector(rBlockSize), SemiBlockVector(rBlockSize);

  SizeType i_start;
  for (SizeType i = 0; i < rNumBlocks; ++i)
  {
    if (i == rNodeId)
    {
      i_start = i * rBlockSize;

      for (unsigned int k = 0; k < rBlockSize; k++)
        BlockVector[k] = rRightHandSideVector[i_start + k];

      noalias(SemiBlockVector) = prod(rRotationMatrix, BlockVector);

      for (unsigned int k = 0; k < rBlockSize; k++)
        rRightHandSideVector[i_start + k] = SemiBlockVector[k];
    }
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void LocalConstraint::AssignMatrix(MatrixType &rLeftHandSideMatrix,
                                   const SizeType& rBlockStart,
                                   const SizeType& rBlockSize,
                                   const SizeType& rNumBlocks,
                                   const unsigned int &rNodeId) const
{
  KRATOS_TRY

  //Locate the constrained direction in order to set zero row and column
  SizeType j;
  for (SizeType i = 0; i < rNumBlocks; ++i)
  {
    if (i == rNodeId)
    {
      for (const auto& Direction: mpConstraint->Dofs)
      {
        j = (Direction + rBlockStart) + i * rBlockSize;
        for (SizeType k = 0; k < j; ++k) // Skip term (i,i)
        {
          rLeftHandSideMatrix(k, j) = 0.0;
          rLeftHandSideMatrix(j, k) = 0.0;
        }
        for (SizeType k = j + 1; k < rBlockSize*rNumBlocks; ++k)
        {
          rLeftHandSideMatrix(k, j) = 0.0;
          rLeftHandSideMatrix(j, k) = 0.0;
        }
        rLeftHandSideMatrix(j, j) = 1.0;
      }
    }
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void LocalConstraint::AssignVector(VectorType &rRightHandSideVector,
                                   const SizeType& rBlockStart,
                                   const SizeType& rBlockSize,
                                   const SizeType& rNumBlocks,
                                   const array_1d<double,3> &rValues,
                                   const GeometryType &rGeometry,
                                   const unsigned int &rNodeId) const
{
  KRATOS_TRY

  //Locate the constrained direction in order to set the imposed value
  double Value = 0;
  SizeType i_start;
  for (SizeType i = 0; i < rNumBlocks; ++i)
  {
    if (i == rNodeId)
    {
      for (const auto& Direction: mpConstraint->Dofs)
      {
        SizeType j = (Direction + rBlockStart) + i * rBlockSize;

        //rRightHandSideVector[j] = rValues[Direction];
        (*(mpConstraint->AssignmentMethod))(rRightHandSideVector[j], rValues[Direction]);
        //std::cout<<" Local Vector constraint "<<rRightHandSideVector[j]<<std::endl;
      }
    }
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void LocalConstraint::GetLocalRotationMatrix(const NodeType &rNode,
                                             Matrix& rRotationMatrix,
                                             const SizeType& rBlockSize,
                                             const SizeType dimension,
                                             const SizeType BlockStart) const
{
  KRATOS_TRY

  // Note: we consider that the vector Dofs are at the begining and others at end.
  Matrix LocalMatrix;

  if (mpConstraint->SupportName == "Axes"){
    // given local base
    mpConstraint->LocalBase.ToRotationMatrix(LocalMatrix);
    if (dimension!=3)
      LocalMatrix.resize(dimension,dimension,true);
  }
  else if (mpConstraint->SupportName == "Node"){
    // node normal evaluated local base (slip conditions)
    this->GetBoundaryRotationMatrix(rNode, LocalMatrix, dimension);
  }
  else if (mpConstraint->SupportName == "Element"){
    // element evaluated local base (beam conditions)
    this->GetElementRotationMatrix(rNode, LocalMatrix, dimension);
  }
  else{
    KRATOS_ERROR << " Defined support for local axes not valid " << mpConstraint->SupportName <<std::endl;
  }

  //consider two possible vector fields diplacements and rotations
  SizeType start = 0;
  if (BlockStart >= dimension)
    start = dimension;

  //resize and assign rotation matrix in block position
  rRotationMatrix.resize(rBlockSize,rBlockSize,false);
  noalias(rRotationMatrix) = IdentityMatrix(rBlockSize);
  for (SizeType i=0; i < dimension; ++i )
    for (SizeType j=0; j < dimension; ++j )
      rRotationMatrix(start+i,start+j) = LocalMatrix(i,j);


  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void LocalConstraint::GetBoundaryRotationMatrix(const NodeType &rNode,
                                                Matrix& rRotationMatrix,
                                                const SizeType& rDimension) const
{
  KRATOS_TRY

  // Get the normal evaluated at the node :: we suppose the normal is normalized
  const array_1d<double, 3> &rNormal = rNode.FastGetSolutionStepValue(NORMAL);

  rRotationMatrix.resize(rDimension,rDimension,false);

  if (rDimension == 2){
    // Define the new coordinate system, where the first vector is aligned with the normal
    rRotationMatrix(0, 0) = rNormal[0];
    rRotationMatrix(0, 1) = rNormal[1];
    rRotationMatrix(1, 0) = -rNormal[1];
    rRotationMatrix(1, 1) = rNormal[0];
  }
  else if (rDimension == 3){
    // Suppose the normal is normalized
    // Define the new coordinate system, where the first vector is aligned with the normal
    rRotationMatrix(0, 0) = rNormal[0];
    rRotationMatrix(0, 1) = rNormal[1];
    rRotationMatrix(0, 2) = rNormal[2];

    // To choose the remaining two vectors, we project the first component of the cartesian base to the tangent plane
    array_1d<double, 3> Tangent;
    Tangent(0) = 1.0;
    Tangent(1) = 0.0;
    Tangent(2) = 0.0;
    double norm = rRotationMatrix(0, 0); //inner_prod(rNormal,Tangent);

    // It is possible that the normal is aligned with (1,0,0), resulting in norm(Tangent) = 0
    // If this is the case, repeat the procedure using (0,1,0)
    if (fabs(norm) > 0.99)
    {
      Tangent(0) = 0.0;
      Tangent(1) = 1.0;
      Tangent(2) = 0.0;

      norm = rRotationMatrix(0, 1); //inner_prod(rNormal,Tangent);
    }

    // calculate projection and normalize
    Tangent[0] -= norm * rRotationMatrix(0, 0);
    Tangent[1] -= norm * rRotationMatrix(0, 1);
    Tangent[2] -= norm * rRotationMatrix(0, 2);

    norm = norm_2(Tangent);
    if (norm!=0)
      Tangent/=norm;

    rRotationMatrix(1, 0) = Tangent[0];
    rRotationMatrix(1, 1) = Tangent[1];
    rRotationMatrix(1, 2) = Tangent[2];

    // The third base component is choosen as N x T1, which is normalized by construction
    rRotationMatrix(2, 0) = rRotationMatrix(0, 1) * Tangent[2] - rRotationMatrix(0, 2) * Tangent[1];
    rRotationMatrix(2, 1) = rRotationMatrix(0, 2) * Tangent[0] - rRotationMatrix(0, 0) * Tangent[2];
    rRotationMatrix(2, 2) = rRotationMatrix(0, 0) * Tangent[1] - rRotationMatrix(0, 1) * Tangent[0];
  }
  else{
    KRATOS_ERROR << " Wrong size for the Rotation Matrix " << rDimension  << std::endl;
  }

  KRATOS_CATCH("")
}


//************************************************************************************
//************************************************************************************

void LocalConstraint::GetElementRotationMatrix(const NodeType &rNode,
                                               Matrix& rRotationMatrix,
                                               const SizeType& rDimension) const
{
  KRATOS_TRY

  // Get the local base evaluated at the elements
  const ElementWeakPtrVectorType &nElements = rNode.GetValue(NEIGHBOUR_ELEMENTS);

  Matrix LocalMatrix(3,3);
  noalias(LocalMatrix) = IdentityMatrix(3);
  QuaternionType LocalQuaternion = QuaternionType::FromRotationMatrix(LocalMatrix);

  for (SizeType i=0; i < nElements.size(); ++i)
  {
    const GeometryType &nGeometry = nElements[i].GetGeometry();

    //check beam elements
    if (nGeometry.LocalSpaceDimension() == 1 && nGeometry.size()>1)
    {
      noalias(LocalMatrix) = ZeroMatrix(3,3);
      CalculateCurrentLocalAxesMatrix(nElements[i], LocalMatrix, rDimension);
      //std::cout<<" Current Matrix "<<LocalMatrix<<std::endl;
      // if (mpConstraint->VariableName == "ROTATION"){
      //   CalculateInitialLocalAxesMatrix(nElements[i], LocalMatrix, rDimension);
      // }
      LocalQuaternion = LocalQuaternion * QuaternionType::FromRotationMatrix(LocalMatrix);
    }
    else{
      KRATOS_ERROR << " Wrong element support :: only line geometries accepted " << std::endl;
    }

  }

  // LocalQuaternion.ToRotationMatrix(LocalMatrix);
  // std::cout<<" Rotation Matrix "<<LocalMatrix<<std::endl;
  // if (mpConstraint->VariableName == "ROTATION"){
  //   const Variable<array_1d<double, 3>>& Rotation = KratosComponents<Variable<array_1d<double, 3>>>::Get(mpConstraint->VariableName);
  //   QuaternionType RotationQuaternion = QuaternionType::FromRotationVector(rNode.FastGetSolutionStepValue(Rotation,1));
  //   LocalQuaternion = RotationQuaternion * LocalQuaternion;
  // }

  noalias(LocalMatrix) = ZeroMatrix(3,3);
  LocalQuaternion.ToRotationMatrix(LocalMatrix);
  //std::cout<<" Rotation + Matrix "<<LocalMatrix<<std::endl;

  rRotationMatrix.resize(rDimension,rDimension,false);
  noalias(rRotationMatrix) = IdentityMatrix(rDimension);
  //**local axis gives column local axes location but row local axes is needed for transformation
  for (SizeType i=0; i < rDimension; ++i )
      for (SizeType j=0; j < rDimension; ++j )
        rRotationMatrix(i,j) = LocalMatrix(j,i); //** transpose

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

//Local E1 beam axis
void LocalConstraint::CalculateInitialLocalAxesMatrix(const Element& rElement,
                                                      Matrix &rRotationMatrix,
                                                      const SizeType& rDimension) const
{
  KRATOS_TRY

  const GeometryType &rGeometry = rElement.GetGeometry();
  const SizeType number_of_nodes = rGeometry.size();
  const SizeType dimension = rGeometry.WorkingSpaceDimension();

  Vector LocalX(3);
  noalias(LocalX) = ZeroVector(3);
  Vector ReferenceCoordinates(6);
  noalias(ReferenceCoordinates) = ZeroVector(6);

  //use initial configuration
  ReferenceCoordinates[0] = rGeometry[0].X0();
  ReferenceCoordinates[1] = rGeometry[0].Y0();
  ReferenceCoordinates[2] = rGeometry[0].Z0();

  int k = number_of_nodes - 1;

  ReferenceCoordinates[3] = rGeometry[k].X0();
  ReferenceCoordinates[4] = rGeometry[k].Y0();
  ReferenceCoordinates[5] = rGeometry[k].Z0();

  for (SizeType i = 0; i < dimension; i++)
  {
    LocalX[i] = (ReferenceCoordinates[i + 3] - ReferenceCoordinates[i]);
  }

  if (rDimension == 2){
    // Define the new coordinate system, where the first vector is aligned with LocalX
    double norm = norm_2(LocalX);
    if (norm!=0)
      LocalX /= norm;
    noalias(rRotationMatrix) = IdentityMatrix(3);
    rRotationMatrix(0, 0) = LocalX[0];
    rRotationMatrix(1, 0) = LocalX[1];
    rRotationMatrix(0, 1) = -LocalX[1];
    rRotationMatrix(1, 1) = LocalX[0];
  }
  else if (rDimension == 3){

    if (rElement.Has(LOCAL_AXIS_2))
    {
      Vector LocalY(3);
      noalias(LocalY) = rElement.GetValue(LOCAL_AXIS_2);
      BeamMathUtilsType::CalculateLocalAxesMatrix(LocalX, LocalY, rRotationMatrix);
    }
    else
    {
      BeamMathUtilsType::CalculateLocalAxesMatrix(LocalX, rRotationMatrix);
    }

  }
  else{
    KRATOS_ERROR << " Wrong size for the Rotation Matrix " << rDimension  << std::endl;
  }

  KRATOS_CATCH("")
}


//***********************************************************************************
//***********************************************************************************

//Local E1 beam axis
void LocalConstraint::CalculatePreviousLocalAxesMatrix(const Element& rElement,
                                                       Matrix &rRotationMatrix,
                                                       const SizeType& rDimension) const
{
  KRATOS_TRY

  const GeometryType &rGeometry = rElement.GetGeometry();
  const SizeType number_of_nodes = rGeometry.size();
  const SizeType dimension = rGeometry.WorkingSpaceDimension();

  Vector LocalX(3);
  noalias(LocalX) = ZeroVector(3);
  Vector ReferenceCoordinates(6);
  noalias(ReferenceCoordinates) = ZeroVector(6);

  //use initial configuration
  const array_1d<double, 3> &rDisplacement0 = rGeometry[0].FastGetSolutionStepValue(DISPLACEMENT,1);
  ReferenceCoordinates[0] = rGeometry[0].X0() + rDisplacement0[0];
  ReferenceCoordinates[1] = rGeometry[0].Y0() + rDisplacement0[1];
  ReferenceCoordinates[2] = rGeometry[0].Z0() + rDisplacement0[2];

  int k = number_of_nodes - 1;

  const array_1d<double, 3> &rDisplacement1 = rGeometry[1].FastGetSolutionStepValue(DISPLACEMENT,1);
  ReferenceCoordinates[3] = rGeometry[k].X0() + rDisplacement1[0];
  ReferenceCoordinates[4] = rGeometry[k].Y0() + rDisplacement1[1];
  ReferenceCoordinates[5] = rGeometry[k].Z0() + rDisplacement1[2];

  for (SizeType i = 0; i < dimension; i++)
  {
    LocalX[i] = (ReferenceCoordinates[i + 3] - ReferenceCoordinates[i]);
  }

  if (rDimension == 2){
    // Define the new coordinate system, where the first vector is aligned with LocalX
    double norm = norm_2(LocalX);
    if (norm!=0)
      LocalX /= norm;
    noalias(rRotationMatrix) = IdentityMatrix(3);
    rRotationMatrix(0, 0) = LocalX[0];
    rRotationMatrix(1, 0) = LocalX[1];
    rRotationMatrix(0, 1) = -LocalX[1];
    rRotationMatrix(1, 1) = LocalX[0];
  }
  else if (rDimension == 3){
    BeamMathUtilsType::CalculateLocalAxesMatrix(LocalX, rRotationMatrix);
  }
  else{
    KRATOS_ERROR << " Wrong size for the Rotation Matrix " << rDimension  << std::endl;
  }

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

//Local E1 beam axis
void LocalConstraint::CalculateCurrentLocalAxesMatrix(const Element& rElement,
                                                      Matrix &rRotationMatrix,
                                                      const SizeType& rDimension) const
{
  KRATOS_TRY

  const GeometryType &rGeometry = rElement.GetGeometry();
  const SizeType number_of_nodes = rGeometry.size();
  const SizeType dimension = rGeometry.WorkingSpaceDimension();

  Vector LocalX(3);
  noalias(LocalX) = ZeroVector(3);
  Vector ReferenceCoordinates(6);
  noalias(ReferenceCoordinates) = ZeroVector(6);

  //use current configuration
  ReferenceCoordinates[0] = rGeometry[0].X();
  ReferenceCoordinates[1] = rGeometry[0].Y();
  ReferenceCoordinates[2] = rGeometry[0].Z();

  int k = number_of_nodes - 1;

  ReferenceCoordinates[3] = rGeometry[k].X();
  ReferenceCoordinates[4] = rGeometry[k].Y();
  ReferenceCoordinates[5] = rGeometry[k].Z();

  for (SizeType i = 0; i < dimension; i++)
  {
    LocalX[i] = (ReferenceCoordinates[i + 3] - ReferenceCoordinates[i]);
  }

  if (rDimension == 2){
    // Define the new coordinate system, where the first vector is aligned with LocalX
    double norm = norm_2(LocalX);
    if (norm!=0)
      LocalX /= norm;
    noalias(rRotationMatrix) = IdentityMatrix(3);
    rRotationMatrix(0, 0) = LocalX[0];
    rRotationMatrix(1, 0) = LocalX[1];
    rRotationMatrix(0, 1) = -LocalX[1];
    rRotationMatrix(1, 1) = LocalX[0];
  }
  else if (rDimension == 3){
    BeamMathUtilsType::CalculateLocalAxesMatrix(LocalX, rRotationMatrix);
  }
  else{
    KRATOS_ERROR << " Wrong size for the Rotation Matrix " << rDimension  << std::endl;
  }

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void LocalConstraint::save(Serializer &rSerializer) const
{
  //rSerializer.save("ConstraintData", mpConstraint);
}

void LocalConstraint::load(Serializer &rSerializer)
{
  //rSerializer.load("ConstraintData", mpConstraint);
}

} // Namespace Kratos.
