//
//   Project Name:        KratosSolversApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:     December 2020 $
//
//

#ifndef KRATOS_LOCAL_CONSTRAINT_TRANSFORMATION_HPP_INCLUDED
#define KRATOS_LOCAL_CONSTRAINT_TRANSFORMATION_HPP_INCLUDED

// system includes

// external includes

// kratos includes
#include "includes/dof.h"
#include "containers/variable.h"
#include "custom_constraints/local_constraint.hpp"
#include "solvers_application_variables.h"

namespace Kratos
{

///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// A utility to rotate the local contributions of certain nodes to the system matrix, which is required to apply local conditions in arbitrary directions.
class LocalConstraintTransformation
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of LocalConstraintTransformation
  KRATOS_CLASS_POINTER_DEFINITION(LocalConstraintTransformation);

  typedef Vector VectorType;
  typedef Matrix MatrixType;
  typedef Node<3> NodeType;
  typedef Geometry<NodeType> GeometryType;
  typedef GeometryType::PointType PointType;
  typedef GeometryType::SizeType SizeType;
  typedef Quaternion<double> QuaternionType;
  typedef std::vector< Dof<double>::Pointer > DofsVectorType;

  ///@}
  ///@name Life Cycle
  ///@{

  /// Constructor.
  LocalConstraintTransformation() {}

  /// Copy Constructor.
  LocalConstraintTransformation(LocalConstraintTransformation &rOther) {}

  /// Destructor.
  virtual ~LocalConstraintTransformation() {}

  ///@}
  ///@name Operators
  ///@{
  ///@}
  ///@name Operations
  ///@{

  /// SetConstraint the local system contributions
  /**
     @param rLocalMatrix Local system matrix
     @param rLocalVector Local RHS vector
     @param rLocalDofsList Local Dofs vector
     @param rGeometry A reference to the element's (or condition's) geometry
  */
  void SetConstraint(MatrixType &rLocalMatrix,
                     VectorType &rLocalVector,
                     const DofsVectorType &rLocalDofsList,
                     const GeometryType &rGeometry) const
  {
    KRATOS_TRY

    if (this->CheckConstraint(rGeometry))
    {
      //std::cout<<" Global Matrix "<<rLocalMatrix<<" Global Vector "<<rLocalVector<<std::endl;

      std::vector<unsigned int> node_ids;
      for (unsigned int i=0; i < rGeometry.size(); ++i)
      {
        if (rGeometry[i].Has(LOCAL_CONSTRAINTS))
          node_ids.push_back(i);
      }
      //Rotate the system components
      if (rLocalMatrix.size1() != 0 && rLocalMatrix.size2() != 0)
      {
        for (const auto& id : node_ids)
        {
          const std::vector<LocalConstraint*>& rLocalConstraint = rGeometry[id].GetValue(LOCAL_CONSTRAINTS);
          for (const auto& constraint : rLocalConstraint)
            constraint->Rotate(rLocalMatrix, rLocalVector, rLocalDofsList, rGeometry, id);
        }
      }
      //Set constraint once everything is rotated
      if (rLocalVector.size() != 0)
      {
        for (const auto& id : node_ids)
        {
          const std::vector<LocalConstraint*>& rLocalConstraint = rGeometry[id].GetValue(LOCAL_CONSTRAINTS);
          const std::vector<array_1d<double,3> >& rLocalValues = rGeometry[id].GetValue(LOCAL_VALUES);
          for (unsigned int i=0; i < rLocalConstraint.size(); ++i)
            rLocalConstraint[i]->Assign(rLocalMatrix, rLocalVector, rLocalDofsList, rLocalValues[i], rGeometry, id);
        }
      }

      //std::cout<<" Local Matrix "<<rLocalMatrix<<" Local Vector "<<rLocalVector<<std::endl;
    }

    KRATOS_CATCH("")
  }

  /// LHS only version of SetConstraint
  /**
     @param rLocalMatrix Local system matrix
     @param rLocalDofsList Local Dofs vector
     @param rGeometry A reference to the element's (or condition's) geometry
  */
  void SetConstraint(MatrixType &rLocalMatrix,
                     const DofsVectorType &rLocalDofsList,
                     const GeometryType &rGeometry) const
  {
    KRATOS_TRY

    if (rLocalMatrix.size1() != 0 && rLocalMatrix.size2() != 0)
    {
      if (this->CheckConstraint(rGeometry))
      {
        std::vector<unsigned int> node_ids;
        for (unsigned int i=0; i < rGeometry.size(); ++i)
        {
          if (rGeometry[i].Has(LOCAL_CONSTRAINTS))
            node_ids.push_back(i);
        }

        //Rotate the system components
        for (const auto& id : node_ids)
        {
          const std::vector<LocalConstraint*>& rLocalConstraint = rGeometry[id].GetValue(LOCAL_CONSTRAINTS);
          for (const auto& constraint : rLocalConstraint)
            constraint->Rotate(rLocalMatrix, rLocalDofsList, rGeometry, id);
        }
        //Set constraint once everything is rotated
        for (const auto& id : node_ids)
        {
          const std::vector<LocalConstraint*>& rLocalConstraint = rGeometry[id].GetValue(LOCAL_CONSTRAINTS);
          for (const auto& constraint : rLocalConstraint)
            constraint->Assign(rLocalMatrix, rLocalDofsList, rGeometry, id);
        }
      }
    }
    KRATOS_CATCH("")
  }

  /// RHS only version of SetConstraint
  /**
     @param rLocalVector Local RHS vector
     @param rLocalDofsList Local Dofs vector
     @param rGeometry A reference to the element's (or condition's) geometry
  */
  void SetConstraint(VectorType &rLocalVector,
                     const DofsVectorType &rLocalDofsList,
                     const GeometryType &rGeometry) const
  {
    KRATOS_TRY

    if (rLocalVector.size() != 0)
    {
      if (this->CheckConstraint(rGeometry))
      {
        std::vector<unsigned int> node_ids;
        for (unsigned int i=0; i < rGeometry.size(); ++i)
        {
          if (rGeometry[i].Has(LOCAL_CONSTRAINTS))
            node_ids.push_back(i);
        }

        //Rotate the system components
        for (const auto& id : node_ids)
        {
          const std::vector<LocalConstraint*>& rLocalConstraint = rGeometry[id].GetValue(LOCAL_CONSTRAINTS);
          for (const auto& constraint : rLocalConstraint)
            constraint->Rotate(rLocalVector, rLocalDofsList, rGeometry, id);
        }
        //Set constraint once everything is rotated
        for (const auto& id : node_ids)
        {
          const std::vector<LocalConstraint*>& rLocalConstraint = rGeometry[id].GetValue(LOCAL_CONSTRAINTS);
          const std::vector<array_1d<double,3> >& rLocalValues = rGeometry[id].GetValue(LOCAL_VALUES);
          for (unsigned int i=0; i < rLocalConstraint.size(); ++i)
            rLocalConstraint[i]->Assign(rLocalVector, rLocalDofsList, rLocalValues[i], rGeometry, id);
        }
      }
    }
    KRATOS_CATCH("")
  }

  /**
     Transform nodal variable (e.g. DISPLACEMENT or VELOCITY) to the local coordinates
     @param rModelPart Local system matrix
     @param ExtraVariableName A related variable i.e. MESH_VELOCITY which is not a dof and constrained
  */
  void MapToLocal(ModelPart &rModelPart, const std::string ExtraVariableName = "None") const
  {
    const SizeType dimension = rModelPart.GetProcessInfo()[SPACE_DIMENSION];
    ModelPart::NodeIterator it_begin = rModelPart.NodesBegin();
#pragma omp parallel for
    for (int i = 0; i < static_cast<int>(rModelPart.Nodes().size()); ++i)
    {
      ModelPart::NodeIterator it_node = it_begin + i;
      if (it_node->Has(LOCAL_CONSTRAINTS))
      {
        const std::vector<LocalConstraint*>& rLocalConstraint = it_node->GetValue(LOCAL_CONSTRAINTS);
        for (const auto& constraint : rLocalConstraint)
          constraint->MapToLocal(*(*(it_node.base())), dimension, ExtraVariableName);
      }
    }
  }

  /**
     Transform nodal variable (e.g. DISPLACEMENT or VELOCITY) to the global coordinates
     @param rModelPart Local system matrix
     @param ExtraVariableName A related variable i.e. MESH_VELOCITY which is not a dof and constrained
  */
  void MapToGlobal(ModelPart &rModelPart, const std::string ExtraVariableName = "None") const
  {
    const SizeType dimension = rModelPart.GetProcessInfo()[SPACE_DIMENSION];
    ModelPart::NodeIterator it_begin = rModelPart.NodesBegin();
#pragma omp parallel for
    for (int i = 0; i < static_cast<int>(rModelPart.Nodes().size()); ++i)
    {
      ModelPart::NodeIterator it_node = it_begin + i;
      if (it_node->Has(LOCAL_CONSTRAINTS))
      {
        const std::vector<LocalConstraint*>& rLocalConstraint = it_node->GetValue(LOCAL_CONSTRAINTS);
        for (const auto& constraint : rLocalConstraint)
          constraint->MapToGlobal(*(*(it_node.base())), dimension, ExtraVariableName);
      }
    }
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  virtual std::string Info() const
  {
    std::stringstream buffer;
    buffer << "LocalConstraintTransformation";
    return buffer.str();
  }

  /// Print information about this object.
  virtual void PrintInfo(std::ostream &rOStream) const
  {
    rOStream << "LocalConstraintTransformation";
  }

  /// Print object's data.
  virtual void PrintData(std::ostream &rOStream) const {}

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{

  bool CheckConstraint(const GeometryType &rGeometry) const
  {
    //Check if has local constrained nodes
    for (const auto& inode : rGeometry)
    {
      if (inode.Has(LOCAL_CONSTRAINTS))
        return true;
    }
    return false;
  }

  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{

  /// Assignment operator.
  LocalConstraintTransformation &operator=(LocalConstraintTransformation const &rOther);

  /// Copy constructor.
  LocalConstraintTransformation(LocalConstraintTransformation const &rOther);

  ///@}
};

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                LocalConstraintTransformation &rThis)
{
  return rIStream;
}

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const LocalConstraintTransformation &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}

///@}

///@} addtogroup block

} // namespace Kratos

#endif // KRATOS_LOCAL_CONSTRAINT_TRANSFORMATION_HPP_INCLUDED
