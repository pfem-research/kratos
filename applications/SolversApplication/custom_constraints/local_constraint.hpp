//
//   Project Name:        KratosSolversApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:     December 2020 $
//
//

#if !defined(KRATOS_LOCAL_CONSTRAINT_HPP_INCLUDED)
#define KRATOS_LOCAL_CONSTRAINT_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "includes/variables.h"
#include "includes/geometrical_object.h"
#include "includes/kratos_parameters.h"
#include "includes/process_info.h"
#include "utilities/beam_math_utilities.hpp"
#include "custom_utilities/assignment_utilities.hpp"
#include "custom_solvers/time_integration_methods/time_integration_method.hpp"

namespace Kratos
{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Local Dirichlet condition for point geometries. (base class)

/**
 * Implements a General Dirichlet Condition in local axes.
 * This works for point geometries only.
 */
class KRATOS_API(SOLVERS_APPLICATION) LocalConstraint
{
public:
    ///@name Type Definitions
    ///@{

    typedef Vector VectorType;

    typedef Matrix MatrixType;

    typedef GeometryData::SizeType SizeType;

    typedef std::vector< Dof<double>::Pointer > DofsVectorType;

    typedef Node<3> NodeType;

    typedef Geometry<NodeType> GeometryType;

    typedef Quaternion<double> QuaternionType;

    typedef BeamMathUtils<double> BeamMathUtilsType;

    typedef GlobalPointersVector<Element> ElementWeakPtrVectorType;

    typedef Variable<double> VariableScalarType;

    typedef TimeIntegrationMethod<VariableScalarType, double> TimeIntegrationMethodScalarType;

    typedef TimeIntegrationMethodScalarType::Pointer ScalarIntegrationPointer;

    // Counted pointer of LocalConstraint
    KRATOS_CLASS_POINTER_DEFINITION(LocalConstraint);

    struct ConstraintData
    {
    public:

      //Type Definitions
      typedef AssignmentUtils::AssignmentType AssignmentType;
      typedef void (*AssignmentMethodPointer)(double &, const double &);

      //Variables
      AssignmentMethodPointer AssignmentMethod = nullptr;
      const std::array<const VariableData, 3>* Variables = nullptr;
      ProcessInfo* CurrentProcessInfo = nullptr; //not const because of serializer load
      std::vector<unsigned int> Dofs;
      std::string VariableName;
      std::string MethodName;
      std::string SupportName;
      Quaternion<double> LocalBase;

      //Method Definitions
      void SetVariable(std::string Name)
      {
        VariableName = Name;
        SetDofVariables();
      }

      void SetDofVariables()
      {
        if (VariableName == "DISPLACEMENT")
          Variables = &msDisplacementDofs;
        else if (VariableName == "VELOCITY")
          Variables = &msVelocityDofs;
        else if (VariableName == "ROTATION")
          Variables = &msRotationDofs;
      }

      void SetAssignmentMethod(std::string method)
      {
        MethodName = method;
        AssignmentUtils::AssignmentType Assignment;
        AssignmentUtils::SetAssignmentType(MethodName, Assignment);
        AssignmentMethod = AssignmentUtils::GetAssignmentMethod<AssignmentMethodPointer>(Assignment);
      }

      void SetProcessInfo(ProcessInfo& rProcessInfo)
      {
        CurrentProcessInfo = &rProcessInfo;
      }

    // private:

    //   friend class Serializer;

    //   void save(Serializer& rSerializer) const
    //   {
    //     rSerializer.save("Dofs", Dofs);
    //     rSerializer.save("VariableName", VariableName);
    //     rSerializer.save("MethodName", MethodName);
    //     rSerializer.save("SupportName", SupportName);
    //     rSerializer.save("LocalBase", LocalBase);
    //   }

    //   void load(Serializer& rSerializer)
    //   {
    //     rSerializer.load("Dofs", Dofs);
    //     rSerializer.load("VariableName", VariableName);
    //     rSerializer.load("MethodName", MethodName);
    //     rSerializer.load("SupportName", SupportName);
    //     rSerializer.load("LocalBase", LocalBase);

    //     SetDofVariables();
    //     SetAssignmentMethod(MethodName);
    //   }

    };

    ///@}
    ///@name Life Cycle
    ///@{

    /// Empty constructor needed for serialization
    LocalConstraint();

    /// Default constructor.
    LocalConstraint(ConstraintData& rConstraint);

    /// Copy constructor
    LocalConstraint(LocalConstraint const &rOther);

    /// Destructor
    ~LocalConstraint();

    ///@}
    ///@name Operators
    ///@{

    ///@}
    ///@name Operations
    ///@{

    /**
     * Transform local system matrix and vector to local base
     */
    virtual void Rotate(MatrixType &rLeftHandSideMatrix,
                        VectorType &rRightHandSideVector,
                        const DofsVectorType &rLocalDofList,
                        const GeometryType &rGeometry,
                        const unsigned int &rNodeId) const;


    /**
     * Transform local system matrix to local base
     */
    virtual void Rotate(MatrixType &rLeftHandSideMatrix,
                        const DofsVectorType &rLocalDofList,
                        const GeometryType &rGeometry,
                        const unsigned int &rNodeId) const;


    /**
     * Transform local system vector to local base
     */
    virtual void Rotate(VectorType &rRightHandSideVector,
                        const DofsVectorType &rLocalDofList,
                        const GeometryType &rGeometry,
                        const unsigned int &rNodeId) const;


    /**
     * Transform local system matrix and vector to assign constraint
     */
    virtual void Assign(MatrixType &rLeftHandSideMatrix,
                        VectorType &rRightHandSideVector,
                        const DofsVectorType &rLocalDofList,
                        const array_1d<double,3> &rValues,
                        const GeometryType &rGeometry,
                        const unsigned int &rNodeId) const;


    /**
     * Transform local system matrix to assign constraint
     */
    virtual void Assign(MatrixType &rLeftHandSideMatrix,
                        const DofsVectorType &rLocalDofList,
                        const GeometryType &rGeometry,
                        const unsigned int &rNodeId) const;


    /**
     * Transform local system vector to assign constraint
     */
    virtual void Assign(VectorType &rRightHandSideVector,
                        const DofsVectorType &rLocalDofList,
                        const array_1d<double,3> &rValues,
                        const GeometryType &rGeometry,
                        const unsigned int &rNodeId) const;


    /**
     * Transform variables to local base
     */
    virtual void MapToLocal(NodeType& rNode,
                            const SizeType& rDimension,
                            std::string ExtraVariableName) const;


    /**
     * Transform variables to global base
     */
    virtual void MapToGlobal(NodeType& rNode,
                             const SizeType& rDimension,
                             std::string ExtraVariableName) const;

    /**
     * Transform variables to local base variables defined in VariableNames
     */
    virtual void MapToLocal(NodeType& rNode,
                            const SizeType& rDimension,
                            const std::vector<std::string> &rVariableNames) const;


    /**
     * Transform variables to global base variables defined in VariableNames
     */
    virtual void MapToGlobal(NodeType& rNode,
                             const SizeType& rDimension,
                             const std::vector<std::string> &rVariableNames) const;
    /**
     * Transform variables to global base and assign
     */
    virtual void MapToGlobalAndAssign(NodeType& rNode,
                                      const SizeType &rDimension,
                                      array_1d<double,3> &rValues,
                                      std::string VariableName,
                                      std::vector<ScalarIntegrationPointer> &rTimeIntegration,
                                      const std::vector<std::string> &rVariableNames) const;

    /**
     * Transform variables to global base and assign
     */
    virtual array_1d<double,3> GetLocalValue(NodeType& rNode,
                                             const SizeType& rDimension) const;


    ///@}
    ///@name Access
    ///@{
    ///@}
    ///@name Inquiry
    ///@{
    ///@}
    ///@name Input and output
    ///@{

    /// Turn back information as a string.
    virtual std::string Info() const
    {
      std::stringstream buffer;
      buffer << "LocalConstraint";
      return buffer.str();
    }

    /// Print information about this object.
    virtual void PrintInfo(std::ostream &rOStream) const
    {
      rOStream << "LocalConstraint";
    }

    /// Print object's data.
    virtual void PrintData(std::ostream &rOStream) const {}


    ///@}
    ///@name Friends
    ///@{
    ///@}

protected:
    ///@name Protected static Member Variables
    ///@{
    ///@}
    ///@name Protected member Variables
    ///@{

    ConstraintData* mpConstraint;

    ///@}
    ///@name Protected Operators
    ///@{
    ///@}
    ///@name Protected Operations
    ///@{

    void RotateMatrix(MatrixType &rLeftHandSideMatrix,
                      const SizeType& rBlockSize,
                      const SizeType& rNumBlocks,
                      const Matrix &rRotationMatrix,
                      const GeometryType &rGeometry,
                      const unsigned int &rNodeId) const;



    void RotateVector(VectorType &rRightHandSideVector,
                      const SizeType& rBlockSize,
                      const SizeType& rNumBlocks,
                      const Matrix &rRotationMatrix,
                      const GeometryType &rGeometry,
                      const unsigned int &rNodeId) const;


    void AssignMatrix(MatrixType &rLeftHandSideMatrix,
                      const SizeType& rBlockStart,
                      const SizeType& rBlockSize,
                      const SizeType& rNumBlocks,
                      const unsigned int &rNodeId) const;


    void AssignVector(VectorType &rRightHandSideVector,
                      const SizeType& rBlockStart,
                      const SizeType& rBlockSize,
                      const SizeType& rNumBlocks,
                      const array_1d<double,3> &rValues,
                      const GeometryType &rGeometry,
                      const unsigned int &rNodeId) const;


    bool CheckDofs(const DofsVectorType& rLocalDofList) const;

    bool GetBlockStart(const DofsVectorType& rLocalDofList, SizeType& rStart) const;


    void GetBlockMatrix(MatrixType &BlockMatrix,
                        const MatrixType &SourceMatrix,
                        const SizeType i_begin,
                        const SizeType j_begin,
                        const SizeType& size) const;


    void SetBlockMatrix(const MatrixType &BlockMatrix,
                        MatrixType &DestinationMatrix,
                        const SizeType i_begin,
                        const SizeType j_begin,
                        const SizeType& size) const;


    void GetLocalRotationMatrix(const NodeType &rNode,
                                Matrix& rRotationMatrix,
                                const SizeType& rBlockSize,
                                const SizeType dimension = 3,
                                const SizeType BlockStart = 0) const;


    void GetBoundaryRotationMatrix(const NodeType &rNode,
                                   Matrix& rRotationMatrix,
                                   const SizeType& rBlockSize) const;

    void GetElementRotationMatrix(const NodeType &rNode,
                                  Matrix& rRotationMatrix,
                                  const SizeType& rBlockSize) const;

    void CalculateInitialLocalAxesMatrix(const Element& rElement,
                                         Matrix &rRotationMatrix,
                                         const SizeType& rDimension) const;

    void CalculatePreviousLocalAxesMatrix(const Element& rElement,
                                         Matrix &rRotationMatrix,
                                         const SizeType& rDimension) const;

    void CalculateCurrentLocalAxesMatrix(const Element& rElement,
                                         Matrix &rRotationMatrix,
                                         const SizeType& rDimension) const;

    ///@}
    ///@name Protected  Access
    ///@{
    ///@}
    ///@name Protected Inquiry
    ///@{
    ///@}
    ///@name Protected LifeCycle
    ///@{
    ///@}

private:
    ///@name Static Member Variables
    ///@{

    static const std::array<const VariableData, 3> msDisplacementDofs;
    static const std::array<const VariableData, 3> msVelocityDofs;
    static const std::array<const VariableData, 3> msRotationDofs;

    ///@}
    ///@name Member Variables
    ///@{
    ///@}
    ///@name Private Operators
    ///@{
    ///@}
    ///@name Private Operations
    ///@{
    ///@}
    ///@name Private  Access
    ///@{
    ///@}
    ///@name Private Inquiry
    ///@{
    ///@}
    ///@name Serialization
    ///@{

    friend class Serializer;

    void save(Serializer &rSerializer) const;

    void load(Serializer &rSerializer);


}; // class LocalConstraint.

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{

/// input stream function
inline std::istream & operator >>(std::istream& rIStream,
                                  LocalConstraint& rThis);

/// output stream function

inline std::ostream & operator <<(std::ostream& rOStream,
                                  const LocalConstraint& rThis)
{
    rThis.PrintInfo(rOStream);
    rOStream << " : " << std::endl;
    rThis.PrintData(rOStream);

    return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_LOCAL_CONSTRAINT_HPP_INCLUDED defined
