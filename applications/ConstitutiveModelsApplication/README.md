# Constitutive Models Application

In this application the material models for the continuum media are implemented.

## License

The Constitutive Models application is OPEN SOURCE. The main code and program structure is available and aimed to grow with the need of any user willing to expand it. The BSD (Berkeley Software Distribution) licence allows to use and distribute the existing code without any restriction, but with the possibility to develop new parts of the code on an open or close source depending on the developers.

## Contact

* **Josep Maria Carbonell** - *Core Development* - [cpuigbo@cimne.upc.edu](mailto:cpuigbo@cimne.upc.edu)
* **Lluís Monforte Vila** - *Core Development* - [lluis.monforte@upc.edu](mailto:lluis.monforte@upc.edu)