//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:                        $
//   Maintained by:       $Maintainer:                       $
//   Date:                $Date:                January 2020 $
//
//

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/hardening_rules/mohr_coulomb_hardening_rule.hpp"

namespace Kratos
{

//*******************************CONSTRUCTOR******************************************
//************************************************************************************

MohrCoulombHardeningRule::MohrCoulombHardeningRule()
    : HardeningRule()
{
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

MohrCoulombHardeningRule &MohrCoulombHardeningRule::operator=(MohrCoulombHardeningRule const &rOther)
{
   HardeningRule::operator=(rOther);
   return *this;
}

//*******************************COPY CONSTRUCTOR*************************************
//************************************************************************************

MohrCoulombHardeningRule::MohrCoulombHardeningRule(MohrCoulombHardeningRule const &rOther)
    : HardeningRule(rOther)
{
}

//********************************CLONE***********************************************
//************************************************************************************

HardeningRule::Pointer MohrCoulombHardeningRule::Clone() const
{
   return Kratos::make_shared<MohrCoulombHardeningRule>(*this);
}

//********************************DESTRUCTOR******************************************
//************************************************************************************

MohrCoulombHardeningRule::~MohrCoulombHardeningRule()
{
}

/// Operations.

//*******************************CALCULATE TOTAL HARDENING****************************
//************************************************************************************

double &MohrCoulombHardeningRule::CalculateHardening(const PlasticDataType &rVariables, double &rHardening)
{
   KRATOS_TRY

   rHardening = 0;
   return rHardening;

   KRATOS_CATCH(" ")
}

//*******************************CALCULATE HARDENING DERIVATIVE***********************
//************************************************************************************

double &MohrCoulombHardeningRule::CalculateDeltaHardening(const PlasticDataType &rVariables, double &rDeltaHardening)
{
   KRATOS_TRY

   rDeltaHardening = 0;
   return rDeltaHardening;

   KRATOS_CATCH(" ")
}

//*******************************CALCULATE HARDENING DERIVATIVE***********************
//************************************************************************************
double &MohrCoulombHardeningRule::CalculateDeltaHardening(const PlasticDataType &rVariables, double &rDeltaHardening, const MatrixType &rPlasticPotentialDerivative)
{
   KRATOS_TRY

   rDeltaHardening = 0;
   return rDeltaHardening;
   KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void MohrCoulombHardeningRule::GetRequiredProperties(Properties &rProperties)
{
  KRATOS_TRY

  // Set required properties

  KRATOS_CATCH(" ")
}

} // namespace Kratos.
