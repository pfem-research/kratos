//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:                        $
//   Maintained by:       $Maintainer:                       $
//   Date:                $Date:                January 2020 $
//
//

#if !defined(KRATOS_MOHR_COULOMB_HARDENING_RULE_HPP_INCLUDED)
#define      KRATOS_MOHR_COULOMB_HARDENING_RULE_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/hardening_rules/hardening_rule.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) MohrCoulombHardeningRule : public HardeningRule
{
protected:
  constexpr static std::size_t VarSize = 10;

public:
  enum class NameType : std::size_t {PLASTIC_STRAIN = 0,
     PLASTIC_VOL_DEF = 1,
     PLASTIC_DEV_DEF = 2,
     PS = 3,
     PT = 4,
     PC = 5,
     PLASTIC_VOL_DEF_ABS = 6,
     NONLOCAL_PLASTIC_VOL_DEF = 7,
     NONLOCAL_PLASTIC_DEV_DEF = 8,
     NONLOCAL_PLASTIC_VOL_DEF_ABS = 9,
  };


  typedef ConstitutiveModelData::InternalData<VarSize,NameType> InternalDataType;
  typedef ConstitutiveModelData::InternalBuffer<InternalDataType,2> InternalVariablesType;
  typedef ConstitutiveModelData::PlasticityData<VarSize,NameType> PlasticityDataType;
  typedef ConstitutiveModelData::ModelDataVariables<PlasticityDataType> PlasticDataType;

  /// Pointer definition of MohrCoulombHardeningRule
  KRATOS_CLASS_POINTER_DEFINITION(MohrCoulombHardeningRule);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  MohrCoulombHardeningRule();

  /// Copy constructor.
  MohrCoulombHardeningRule(MohrCoulombHardeningRule const &rOther);

  /// Assignment operator.
  MohrCoulombHardeningRule &operator=(MohrCoulombHardeningRule const &rOther);

  /// Clone.
  HardeningRule::Pointer Clone() const override;

  /// Destructor.
  ~MohrCoulombHardeningRule() override;

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /**
     * Calculate Hardening functions
     */

  virtual double &CalculateHardening(const PlasticDataType &rVariables, double &rHardening);

  /**
     * Calculate Hardening function derivatives
     */

  virtual double &CalculateDeltaHardening(const PlasticDataType &rVariables, double &rDeltaHardening);

  virtual double &CalculateDeltaHardening(const PlasticDataType &rVariables, double &rDeltaHardening, const MatrixType &rPlasticPotentialDerivative); //do not override -> it must hide the method

  /**
     * Get required properties
     */
  void GetRequiredProperties(Properties &rProperties) override;

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "MohrCoulombHardeningRule";
    return buffer.str();
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "MohrCoulombHardeningRule";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "MohrCoulombHardeningRule Data";
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  using HardeningRule::CalculateDeltaHardening;
  using HardeningRule::CalculateHardening;

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, HardeningRule)
  }

  void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, HardeningRule)
  }

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class MohrCoulombHardeningRule

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_MOHR_COULOMB_HARDENING_RULE_HPP_INCLUDED  defined
