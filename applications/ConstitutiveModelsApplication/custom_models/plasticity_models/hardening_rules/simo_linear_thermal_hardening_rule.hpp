//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                  April 2017 $
//
//

#if !defined(KRATOS_SIMO_LINEAR_THERMAL_HARDENING_RULE_HPP_INCLUDED)
#define KRATOS_SIMO_LINEAR_THERMAL_HARDENING_RULE_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/hardening_rules/simo_linear_hardening_rule.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) SimoLinearThermalHardeningRule
    : public SimoLinearHardeningRule
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of SimoLinearThermalHardeningRule
  KRATOS_CLASS_POINTER_DEFINITION(SimoLinearThermalHardeningRule);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  SimoLinearThermalHardeningRule();

  /// Copy constructor.
  SimoLinearThermalHardeningRule(SimoLinearThermalHardeningRule const &rOther);

  /// Assignment operator.
  SimoLinearThermalHardeningRule &operator=(SimoLinearThermalHardeningRule const &rOther);

  /// Clone.
  HardeningRule::Pointer Clone() const override;

  /// Destructor.
  ~SimoLinearThermalHardeningRule() override;

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /**
     * Calculate Hardening function derivatives
     */

  double &CalculateDeltaThermalHardening(const PlasticDataType &rVariables, double &rDeltaThermalHardening) override;

  /**
     * Get required properties
     */
  void GetRequiredProperties(Properties &rProperties) override;

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "SimoLinearThermalHardeningRule";
    return buffer.str();
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "SimoLinearThermalHardeningRule";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "SimoLinearThermalHardeningRule Data";
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  double &CalculateThermalReferenceEffect(const PlasticDataType &rVariables, double &rThermalFactor) override;

  double &CalculateThermalCurrentEffect(const PlasticDataType &rVariables, double &rThermalFactor) override;

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, SimoExponentialHardeningRule)
  }

  void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, SimoExponentialHardeningRule)
  }

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class SimoLinearThermalHardeningRule

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_SIMO_LINEAR_THERMAL_HARDENING_RULE_HPP_INCLUDED  defined
