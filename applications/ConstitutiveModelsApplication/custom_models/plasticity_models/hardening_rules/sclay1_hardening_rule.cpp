//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:                October 2021 $
//
//

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/hardening_rules/sclay1_hardening_rule.hpp"
#include "custom_utilities/constitutive_model_utilities.hpp"
#include "custom_utilities/stress_invariants_utilities.hpp"
#include "custom_utilities/shape_deviatoric_plane_utilities.hpp"

namespace Kratos
{

//*******************************CONSTRUCTOR******************************************
//************************************************************************************

SClay1HardeningRule::SClay1HardeningRule()
    : HardeningRule()
{
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

SClay1HardeningRule &SClay1HardeningRule::operator=(SClay1HardeningRule const &rOther)
{
  HardeningRule::operator=(rOther);
  return *this;
}

//*******************************COPY CONSTRUCTOR*************************************
//************************************************************************************

SClay1HardeningRule::SClay1HardeningRule(SClay1HardeningRule const &rOther)
    : HardeningRule(rOther)
{
}

//********************************CLONE***********************************************
//************************************************************************************

HardeningRule::Pointer SClay1HardeningRule::Clone() const
{
  return (HardeningRule::Pointer(new SClay1HardeningRule(*this)));
}

//********************************DESTRUCTOR******************************************
//************************************************************************************

SClay1HardeningRule::~SClay1HardeningRule()
{
}

/// Operations.

//*******************************CALCULATE TOTAL HARDENING****************************
//************************************************************************************

double &SClay1HardeningRule::CalculateHardening(const PlasticDataType &rVariables, double &rHardening)
{
  KRATOS_TRY

  KRATOS_ERROR << " you should not be here " << std::endl;
  return rHardening;

  KRATOS_CATCH(" ")
}

//*******************************CALCULATE HARDENING DERIVATIVE***********************
//************************************************************************************
double &SClay1HardeningRule::CalculateDeltaHardening(const PlasticDataType &rVariables, double &rDeltaHardening)
{
  KRATOS_ERROR << " not implemented. should not be here " << std::endl;
}

//*******************************CALCULATE HARDENING DERIVATIVE***********************
//************************************************************************************
double &SClay1HardeningRule::CalculateDeltaHardening(const PlasticDataType &rVariables, double &rDeltaHardening, const MatrixType &rPlasticPotentialDerivative)
{
  KRATOS_TRY

  const ModelDataType &rModelData = rVariables.GetModelData();
  const Properties &rMaterialProperties = rModelData.GetProperties();
  const double &rShearM = rMaterialProperties[CRITICAL_STATE_LINE];
  const double &rFriction = rMaterialProperties[INTERNAL_FRICTION_ANGLE];
  const double &rLambda = rMaterialProperties[NORMAL_COMPRESSION_SLOPE];
  const double &rKappa = rMaterialProperties[SWELLING_SLOPE];
  const double &rCalfa = rMaterialProperties[FABRIC_RATE];
  const double &rChi = rMaterialProperties[FABRIC_RATE_DEVIATORIC];
  const MatrixType &rStressMatrix = rModelData.GetStressMatrix();

  const double &rP0 = rVariables.Data.Internal(3);


  VectorType FabricVector;
  for (unsigned int i = 0; i < 6; i++)
     FabricVector(i) = rVariables.Data.Internal(10+i);

  MatrixType FabricTensor; 
  ConstitutiveModelUtilities::StressVectorToTensor( FabricVector, FabricTensor);


  double TracePlasticPotDerivative = 0.0;
  for (unsigned int i = 0; i < 3; i++)
    TracePlasticPotDerivative += rPlasticPotentialDerivative(i, i);

  MatrixType DevPlasticPotDerivative = rPlasticPotentialDerivative;
  for (unsigned int i = 0; i < 3; i++)
    DevPlasticPotDerivative(i, i) -= TracePlasticPotDerivative / 3.0;

  double NormPlasticPotDerivative = 0.0;
  for (unsigned int i = 0; i < 3; i++)
  {
    for (unsigned int j = 0; j < 3; j++)
    {
      NormPlasticPotDerivative += pow(DevPlasticPotDerivative(i, j), 2.0);
    }
  }
  NormPlasticPotDerivative = sqrt(NormPlasticPotDerivative);



  double MeanStress(0.0);
  MatrixType StressDev = rStressMatrix;
  for (unsigned int i = 0; i < 3; i++)
     MeanStress += rStressMatrix(i,i)/3.0;
  for (unsigned int i = 0; i < 3; i++)
     StressDev(i,i) -= MeanStress;

  MatrixType StressMinusAP;
  noalias( StressMinusAP ) = StressDev - MeanStress*FabricTensor;

  double NormFabricTensor(0.0);
  for (unsigned int i = 0; i < 3; i++) {
     for (unsigned int j = 0; j < 3; j++) {
        NormFabricTensor += pow( FabricTensor(i,j), 2);
     }
  }

  double ThirdInvariantEffect(1.0);
  if ( rFriction > 0.0) {
      double dummy1(1.0), dummy2(2.0), LodeAngle;
      StressInvariantsUtilities::CalculateStressInvariants(StressDev, dummy1, dummy2, LodeAngle);
      ThirdInvariantEffect = ShapeAtDeviatoricPlaneUtility::EvaluateEffectDueToThirdInvariant(ThirdInvariantEffect, LodeAngle, rFriction);
  }


  rDeltaHardening = -(pow(rShearM/ThirdInvariantEffect, 2) - 3.0/2.0*NormFabricTensor) * MeanStress *  rP0/(rLambda-rKappa) * TracePlasticPotDerivative;


  MatrixType dFdFabric;
  noalias( dFdFabric ) = 3.0 * StressMinusAP * (- MeanStress) + 3.0 * FabricTensor * MeanStress * ( rP0-MeanStress);

  MatrixType Matrix1; 
  noalias( Matrix1 ) = rCalfa * ( 3.0/4.0*StressDev/MeanStress - FabricTensor);

  MatrixType Matrix2;
  noalias( Matrix2 ) = rCalfa * rChi * (1.0/3.0*StressDev/MeanStress - FabricTensor);

  double product1(0.0), product2(0.0);
  for (unsigned int i = 0; i < 3; i++) {
     for (unsigned int j = 0; j < 3; j++) {
        product1 += dFdFabric(i,j)*Matrix1(i,j);
        product2 += dFdFabric(i,j)*Matrix2(i,j);
     }
  }

  TracePlasticPotDerivative = std::max(-TracePlasticPotDerivative, 0.0);
  rDeltaHardening -= product1 * TracePlasticPotDerivative + product2 * NormPlasticPotDerivative;

  return rDeltaHardening;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void SClay1HardeningRule::GetRequiredProperties(Properties &rProperties)
{
  KRATOS_TRY

  // Set required properties

  KRATOS_CATCH(" ")
}

} // namespace Kratos.
