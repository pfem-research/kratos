//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                  April 2017 $
//
//

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/hardening_rules/baker_johnson_cook_thermal_hardening_rule.hpp"

namespace Kratos
{

//*******************************CONSTRUCTOR******************************************
//************************************************************************************

BakerJohnsonCookThermalHardeningRule::BakerJohnsonCookThermalHardeningRule()
    : HardeningRule()
{
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

BakerJohnsonCookThermalHardeningRule &BakerJohnsonCookThermalHardeningRule::operator=(BakerJohnsonCookThermalHardeningRule const &rOther)
{
  HardeningRule::operator=(rOther);
  return *this;
}

//*******************************COPY CONSTRUCTOR*************************************
//************************************************************************************

BakerJohnsonCookThermalHardeningRule::BakerJohnsonCookThermalHardeningRule(BakerJohnsonCookThermalHardeningRule const &rOther)
    : HardeningRule(rOther)
{
}

//********************************CLONE***********************************************
//************************************************************************************

HardeningRule::Pointer BakerJohnsonCookThermalHardeningRule::Clone() const
{
  return Kratos::make_shared<BakerJohnsonCookThermalHardeningRule>(*this);
}

//********************************DESTRUCTOR******************************************
//************************************************************************************

BakerJohnsonCookThermalHardeningRule::~BakerJohnsonCookThermalHardeningRule()
{
}

/// Operations.

//*******************************CALCULATE TOTAL HARDENING****************************
//************************************************************************************

double &BakerJohnsonCookThermalHardeningRule::CalculateHardening(const PlasticDataType &rVariables, double &rHardening)
{
  KRATOS_TRY

  const ModelDataType &rModelData = rVariables.GetModelData();

  //get values
  const double &rRateFactor = rVariables.Data.GetRateFactor();
  const double &rCurrentPlasticStrain = rVariables.Data.GetInternalVariables()[0];
  const double &rDeltaGamma = rVariables.Data.GetDeltaInternalVariables()[0];

  const double &rTemperature = rModelData.GetTemperature();
  const double &rDeltaTime = rModelData.GetProcessInfo()[DELTA_TIME];

  //Constant Parameters of the -- Johnson and Cook --:
  const Properties &rProperties = rModelData.GetProperties();
  const double &K = rProperties[BJC_PARAMETER_K];
  const double &C = rProperties[BJC_PARAMETER_C];

  const double &n = rProperties[BJC_PARAMETER_n];
  const double &m = rProperties[BJC_PARAMETER_m];

  const double &rReferenceTemperature = rProperties[REFERENCE_TEMPERATURE];
  const double &rMeldTemperature = rProperties[MELD_TEMPERATURE];
  const double &rCurrentPlasticStrainRate = rProperties[REFERENCE_PLASTIC_STRAIN_RATE];

  if (rTemperature - rReferenceTemperature < 0)
  {
    KRATOS_WARNING_ONCE("Thermal law") << " Reference Temperature Higher than Current Temperature (" << rTemperature << " < " << rReferenceTemperature << ") :" << (rTemperature - rReferenceTemperature) << std::endl;
  }

  double NormalizedTemperature = pow((rTemperature) / (rMeldTemperature), m);

  double Kv = K * exp((-1) * NormalizedTemperature);
  double nv = n * exp((-1) * NormalizedTemperature);

  rHardening = (Kv * pow(rCurrentPlasticStrain, nv));

  if (rRateFactor != 0)
  {

    if (rDeltaGamma == 0)
      std::cout << " Something is wrong in the Baker_Johnson_Cook_hardening variables supplied " << std::endl;

    rHardening *= (1.0 + rRateFactor * C * std::log((rDeltaGamma * sqrt(2.0 / 3.0)) / (rCurrentPlasticStrainRate * rDeltaTime)));
  }

  return rHardening;

  KRATOS_CATCH(" ")
}

//*******************************CALCULATE HARDENING DERIVATIVE***********************
//************************************************************************************

double &BakerJohnsonCookThermalHardeningRule::CalculateDeltaHardening(const PlasticDataType &rVariables, double &rDeltaHardening)
{
  KRATOS_TRY

  const ModelDataType &rModelData = rVariables.GetModelData();

  //get values
  const double &rRateFactor = rVariables.Data.GetRateFactor();
  const double &rCurrentPlasticStrain = rVariables.Data.GetInternalVariables()[0];
  const double &rDeltaGamma = rVariables.Data.GetDeltaInternalVariables()[0];

  const double &rTemperature = rModelData.GetTemperature();
  const double &rDeltaTime = rModelData.GetProcessInfo()[DELTA_TIME];

  //Constant Parameters of the -- Johnson and Cook --:
  const Properties &rProperties = rModelData.GetProperties();
  const double &K = rProperties[BJC_PARAMETER_K];
  const double &C = rProperties[BJC_PARAMETER_C];

  const double &n = rProperties[BJC_PARAMETER_n];
  const double &m = rProperties[BJC_PARAMETER_m];

  const double &rReferenceTemperature = rProperties[REFERENCE_TEMPERATURE];
  const double &rMeldTemperature = rProperties[MELD_TEMPERATURE];
  const double &rCurrentPlasticStrainRate = rProperties[REFERENCE_PLASTIC_STRAIN_RATE];

  if (rTemperature - rReferenceTemperature < 0)
  {
    KRATOS_WARNING_ONCE("Thermal law") << " Reference Temperature Higher than Current Temperature (" << rTemperature << " < " << rReferenceTemperature << ") :" << (rTemperature - rReferenceTemperature) << std::endl;
  }

  double NormalizedTemperature = pow((rTemperature) / (rMeldTemperature), m);

  double Kv = K * exp((-1) * NormalizedTemperature);
  double nv = n * exp((-1) * NormalizedTemperature);

  rDeltaHardening = (nv * Kv * pow(rCurrentPlasticStrain, nv - 1));

  if (rRateFactor != 0)
  {

    if (rDeltaGamma == 0)
      std::cout << " Something is wrong in the Baker_Johnson_Cook_hardening variables supplied " << std::endl;

    rDeltaHardening *= (1.0 + rRateFactor * C * std::log((rDeltaGamma * sqrt(2.0 / 3.0)) / (rCurrentPlasticStrainRate * rDeltaTime)));

    rDeltaHardening += rRateFactor * (sqrt(3.0 / 2.0) * (Kv * pow(rCurrentPlasticStrain, nv)) * C / rDeltaGamma);
  }

  return rDeltaHardening;

  KRATOS_CATCH(" ")
}

//***************************CALCULATE HARDENING DERIVATIVE TEMPERATURE***************
//************************************************************************************

double &BakerJohnsonCookThermalHardeningRule::CalculateDeltaThermalHardening(const PlasticDataType &rVariables, double &rDeltaThermalHardening)
{
  KRATOS_TRY

  const ModelDataType &rModelData = rVariables.GetModelData();

  //get values
  const double &rRateFactor = rVariables.Data.GetRateFactor();
  const double &rCurrentPlasticStrain = rVariables.Data.GetInternalVariables()[0];
  const double &rDeltaGamma = rVariables.Data.GetDeltaInternalVariables()[0];

  const double &rTemperature = rModelData.GetTemperature();
  const double &rDeltaTime = rModelData.GetProcessInfo()[DELTA_TIME];

  //Constant Parameters of the -- Baker Johnson and Cook --:
  const Properties &rProperties = rModelData.GetProperties();
  const double &K = rProperties[BJC_PARAMETER_K];
  const double &C = rProperties[BJC_PARAMETER_C];

  const double &n = rProperties[BJC_PARAMETER_n];
  const double &m = rProperties[BJC_PARAMETER_m];

  const double &rReferenceTemperature = rProperties[REFERENCE_TEMPERATURE];
  const double &rMeldTemperature = rProperties[MELD_TEMPERATURE];
  const double &rCurrentPlasticStrainRate = rProperties[REFERENCE_PLASTIC_STRAIN_RATE];

  if (rTemperature - rReferenceTemperature < 0)
  {
      KRATOS_WARNING_ONCE("Thermal law") << " Reference Temperature Higher than Current Temperature (" << rTemperature << " < " << rReferenceTemperature << ") :" << (rTemperature - rReferenceTemperature) << std::endl;
  }

  double NormalizedTemperature = pow((rTemperature) / (rMeldTemperature), m);

  double Kv = K * exp((-1) * NormalizedTemperature);
  double nv = n * exp((-1) * NormalizedTemperature);

  double DeltaNormalizedTemperature = (m / rMeldTemperature) * pow((rTemperature / rMeldTemperature), m - 1);

  rDeltaThermalHardening = (1 + nv * std::log(rCurrentPlasticStrain));

  rDeltaThermalHardening *= (Kv * pow(rCurrentPlasticStrain, nv) * DeltaNormalizedTemperature);

  if (rRateFactor != 0)
  {

    if (rDeltaGamma == 0)
      std::cout << " Something is wrong in the Baker_Johnson_Cook_hardening variables supplied " << std::endl;

    rDeltaThermalHardening *= (1.0 + rRateFactor * C * std::log((rDeltaGamma * sqrt(2.0 / 3.0)) / (rCurrentPlasticStrainRate * rDeltaTime)));
  }

  return rDeltaThermalHardening;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void BakerJohnsonCookThermalHardeningRule::GetRequiredProperties(Properties &rProperties)
{
  KRATOS_TRY

  // Set required properties
  double value = 0.0; //dummy
  rProperties.SetValue(BJC_PARAMETER_K,value);
  rProperties.SetValue(BJC_PARAMETER_C,value);
  rProperties.SetValue(BJC_PARAMETER_n,value);
  rProperties.SetValue(BJC_PARAMETER_m,value);
  rProperties.SetValue(REFERENCE_TEMPERATURE,value);
  rProperties.SetValue(MELD_TEMPERATURE,value);
  rProperties.SetValue(REFERENCE_PLASTIC_STRAIN_RATE,value);

  rProperties.SetValue(THERMAL_EXPANSION_COEFFICIENT, value);

  KRATOS_CATCH(" ")
}

} // namespace Kratos.
