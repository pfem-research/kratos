//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                   July 2017 $
//
//

#if !defined(KRATOS_HARDENING_RULE_HPP_INCLUDED)
#define KRATOS_HARDENING_RULE_HPP_INCLUDED

// System includes
#include <string>
#include <iostream>

// External includes

// Project includes
#include "includes/define.h"
#include "includes/properties.h"

#include "constitutive_models_application_variables.h"

#include "custom_models/constitutive_model_data.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
class HardeningRule
{
protected:
  //warning::this variable is going to be shadowed by they derived classes
  //if any problem is detected an alternative method must be used instead
  constexpr static std::size_t VarSize = 1;

public:
  ///@name Type Definitions
  ///@{
  enum class NameType : std::size_t { PLASTIC_STRAIN = 0 };

  typedef ConstitutiveModelData::MatrixType MatrixType;
  typedef ConstitutiveModelData::VectorType VectorType;
  typedef ConstitutiveModelData::ModelData ModelDataType;
  typedef ConstitutiveModelData::MaterialData MaterialDataType;

  // typedef ConstitutiveModelData::InternalVariables<VarSize> InternalVariablesType;
  // typedef ConstitutiveModelData::PlasticModelData<VarSize> PlasticDataType;

  typedef ConstitutiveModelData::InternalData<VarSize,NameType> InternalDataType;
  typedef ConstitutiveModelData::InternalBuffer<InternalDataType,2> InternalVariablesType;
  typedef ConstitutiveModelData::PlasticityData<VarSize,NameType> PlasticityDataType;
  typedef ConstitutiveModelData::ModelDataVariables<PlasticityDataType> PlasticDataType;

  /// Pointer definition of HardeningRule
  KRATOS_CLASS_POINTER_DEFINITION(HardeningRule);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  HardeningRule() {}

  /// Copy constructor.
  HardeningRule(HardeningRule const &rOther) {}

  /// Assignment operator.
  HardeningRule &operator=(HardeningRule const &rOther)
  {
    return *this;
  }

  /// Clone.
  virtual HardeningRule::Pointer Clone() const
  {
    return Kratos::make_shared<HardeningRule>(*this);
  }

  /// Destructor.
  virtual ~HardeningRule() {}

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /**
     * Calculate Hardening functions
     */

  virtual double &CalculateHardening(const PlasticDataType &rVariables, double &rHardening)
  {
    KRATOS_TRY

    KRATOS_ERROR << "calling the HardeningRule base class ... illegal operation" << std::endl;

    return rHardening;

    KRATOS_CATCH(" ")
  }

  /**
     * Calculate Hardening function derivatives
     */

  virtual double &CalculateDeltaHardening(const PlasticDataType &rVariables, double &rDeltaHardening)
  {
    KRATOS_TRY

    KRATOS_ERROR << "calling the HardeningRule base class ... illegal operation" << std::endl;

    return rDeltaHardening;

    KRATOS_CATCH(" ")
  }

  virtual double &CalculateDeltaHardening(const PlasticDataType &rVariables, double &rDeltaHardening, const MatrixType &rPlasticPotentialDerivative)
  {
    KRATOS_TRY

    KRATOS_ERROR << "calling the HardeningRule base class ... illegal operation" << std::endl;

    return rDeltaHardening;

    KRATOS_CATCH(" ")
  }

  virtual double &CalculateDeltaHardeningUnsat(const PlasticDataType &rVariables, double &rDeltaHardening, const MatrixType &rPlasticPotentialDerivative, const double & rSuction)
  {
    KRATOS_TRY

    KRATOS_ERROR << "calling the HardeningRule base class ... illegal operation" << std::endl;

    return rDeltaHardening;

    KRATOS_CATCH(" ")
  }

  virtual double &CalculateDeltaThermalHardening(const PlasticDataType &rVariables, double &rDeltaThermalHardening)
  {
    KRATOS_TRY

    KRATOS_ERROR << "calling the HardeningRule base class ... illegal operation" << std::endl;

    return rDeltaThermalHardening;

    KRATOS_CATCH(" ")
  }

  /**
     * Get required properties
     */
  virtual void GetRequiredProperties(Properties &rProperties)
  {
    KRATOS_TRY

    KRATOS_ERROR << "calling the HardeningRule base class ... illegal operation" << std::endl;

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  virtual std::string Info() const
  {
    std::stringstream buffer;
    buffer << "HardeningRule";
    return buffer.str();
  }

  /// Print information about this object.
  virtual void PrintInfo(std::ostream &rOStream) const
  {
    rOStream << "HardeningRule";
  }

  /// Print object's data.
  virtual void PrintData(std::ostream &rOStream) const
  {
    rOStream << "HardeningRule Data";
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  virtual void save(Serializer &rSerializer) const
  {
  }

  virtual void load(Serializer &rSerializer)
  {
  }

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class HardeningRule

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_HARDENING_RULE_HPP_INCLUDED  defined
