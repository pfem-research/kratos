//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                       $
//   Date:                $Date:                  March 2021 $
//
//

#if !defined(KRATOS_CASM_STRUCTURE_HARDENING_RULE_HPP_INCLUDED)
#define KRATOS_CASM_STRUCTURE_HARDENING_RULE_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/hardening_rules/hardening_rule.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) CasmStructureHardeningRule : public HardeningRule
{
protected:
  constexpr static std::size_t VarSize = 14;

public:
  enum class NameType : std::size_t {PLASTIC_STRAIN = 0,
     PLASTIC_VOL_DEF = 1,
     PLASTIC_DEV_DEF = 2,
     PS = 3,
     PT = 4,
     PC = 5,
     PLASTIC_VOL_DEF_ABS = 6,
     NONLOCAL_PLASTIC_VOL_DEF = 7,
     NONLOCAL_PLASTIC_DEV_DEF = 8,
     NONLOCAL_PLASTIC_VOL_DEF_ABS = 9,
  };


  typedef ConstitutiveModelData::InternalData<VarSize,NameType> InternalDataType;
  typedef ConstitutiveModelData::InternalBuffer<InternalDataType,2> InternalVariablesType;
  typedef ConstitutiveModelData::PlasticityData<VarSize,NameType> PlasticityDataType;
  typedef ConstitutiveModelData::ModelDataVariables<PlasticityDataType> PlasticDataType;


  /// Pointer definition of CasmStructureHardeningRule
  KRATOS_CLASS_POINTER_DEFINITION(CasmStructureHardeningRule);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  CasmStructureHardeningRule();

  /// Copy constructor.
  CasmStructureHardeningRule(CasmStructureHardeningRule const &rOther);

  /// Assignment operator.
  CasmStructureHardeningRule &operator=(CasmStructureHardeningRule const &rOther);

  /// Clone.
  virtual HardeningRule::Pointer Clone() const override;

  /// Destructor.
  ~CasmStructureHardeningRule();

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /**
     * Calculate Hardening functions
     */

  virtual double &CalculateHardening(const PlasticDataType &rVariables, double &rHardening); //do not override -> it must hide the method

  /**
     * Calculate Hardening function derivatives
     */

  virtual double &CalculateDeltaHardening(const PlasticDataType &rVariables, double &rDeltaHardening); //do not override -> it must hide the method

  virtual double &CalculateDeltaHardening(const PlasticDataType &rVariables, double &rDeltaHardening, const MatrixType &rPlasticPotentialDerivative); //do not override -> it must hide the method

  /**
     * Get required properties
     */
  void GetRequiredProperties(Properties &rProperties) override;

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  virtual std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "CasmStructureHardeningRule";
    return buffer.str();
  }

  /// Print information about this object.
  virtual void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "CasmStructureHardeningRule";
  }

  /// Print object's data.
  virtual void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "CasmStructureHardeningRule Data";
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  using HardeningRule::CalculateDeltaHardening;
  using HardeningRule::CalculateHardening;

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  virtual void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, HardeningRule)
  }

  virtual void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, HardeningRule)
  }

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class CasmStructureHardeningRule

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_CASM_STRUCTURE_HARDENING_RULE_HPP_INCLUDED  defined
