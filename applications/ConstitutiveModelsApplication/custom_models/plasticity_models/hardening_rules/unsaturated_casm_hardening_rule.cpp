//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:               December 2021 $
//
//

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/hardening_rules/unsaturated_casm_hardening_rule.hpp"
#include "custom_utilities/constitutive_model_utilities.hpp"

namespace Kratos
{

//*******************************CONSTRUCTOR******************************************
//************************************************************************************

UnsaturatedCasmHardeningRule::UnsaturatedCasmHardeningRule()
    : HardeningRule()
{
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

UnsaturatedCasmHardeningRule &UnsaturatedCasmHardeningRule::operator=(UnsaturatedCasmHardeningRule const &rOther)
{
  HardeningRule::operator=(rOther);
  return *this;
}

//*******************************COPY CONSTRUCTOR*************************************
//************************************************************************************

UnsaturatedCasmHardeningRule::UnsaturatedCasmHardeningRule(UnsaturatedCasmHardeningRule const &rOther)
    : HardeningRule(rOther)
{
}

//********************************CLONE***********************************************
//************************************************************************************

HardeningRule::Pointer UnsaturatedCasmHardeningRule::Clone() const
{
  return (HardeningRule::Pointer(new UnsaturatedCasmHardeningRule(*this)));
}

//********************************DESTRUCTOR******************************************
//************************************************************************************

UnsaturatedCasmHardeningRule::~UnsaturatedCasmHardeningRule()
{
}

/// Operations.

//*******************************CALCULATE TOTAL HARDENING****************************
//************************************************************************************

double &UnsaturatedCasmHardeningRule::CalculateHardening(const PlasticDataType &rVariables, double &rHardening)
{
  KRATOS_TRY

  KRATOS_ERROR << " you should not be here " << std::endl;

  KRATOS_CATCH(" ")
}

//*******************************CALCULATE HARDENING DERIVATIVE***********************
//************************************************************************************
double &UnsaturatedCasmHardeningRule::CalculateDeltaHardening(const PlasticDataType &rVariables, double &rDeltaHardening)
{
  KRATOS_ERROR << " not implemented. should not be here " << std::endl;
}


//*******************************CALCULATE HARDENING DERIVATIVE***********************
//************************************************************************************
double &UnsaturatedCasmHardeningRule::CalculateDeltaHardening(const PlasticDataType &rVariables, double &rDeltaHardeningi, const MatrixType & rPlasticPotentialDerivative)
{
  KRATOS_ERROR << " not implemented. should not be here " << std::endl;
}

//*******************************CALCULATE HARDENING DERIVATIVE***********************
//************************************************************************************
double & UnsaturatedCasmHardeningRule::CalculateDeltaHardeningUnsat(const PlasticDataType &rVariables, double &rDeltaHardening, const MatrixType &rPlasticPotentialDerivative, const double & rSuction)
{
  KRATOS_TRY

  const ModelDataType &rModelData = rVariables.GetModelData();
  const Properties &rMaterialProperties = rModelData.GetProperties();
  // const MatrixType    & rStressMatrix = rModelData.GetStressMatrix();

  const double &rPC    = rVariables.Data.Internal(5);
  const double &rPCSat = rVariables.Data.Internal(6);

  const double &rSpacingR = rMaterialProperties[SPACING_RATIO];
  const double &rLambda = rMaterialProperties[NORMAL_COMPRESSION_SLOPE];
  const double &rKappa = rMaterialProperties[SWELLING_SLOPE];
  const double & rPref     = rMaterialProperties[REFERENCE_PRESSURE];
  const double & rR      = rMaterialProperties[R_BBM];
  const double & rBeta   = rMaterialProperties[BETA_BBM];
       
  double lambdaS = rLambda * (  (1.0-rR)*exp(-rBeta*rSuction) + rR);

  Vector m = ZeroVector(6);
  for (unsigned int i = 0; i < 3; ++i)
    m(i) = 1.0;

  double df_dPC = 1.0 / (rPC * std::log(rSpacingR));
  double dPC_dPCSat = pow( -rPCSat/rPref, (rLambda-rKappa)/(lambdaS-rKappa) -1.0) * ( rLambda-rKappa)/(lambdaS-rKappa);
  double dPCSat_dEVolPlastic = -rPCSat / (rLambda - rKappa);

  rDeltaHardening = df_dPC * dPC_dPCSat * dPCSat_dEVolPlastic;
  Vector PlasticPot(6);
  PlasticPot = ConstitutiveModelUtilities::StrainTensorToVector(rPlasticPotentialDerivative, PlasticPot);
  rDeltaHardening *= MathUtils<double>::Dot(m, PlasticPot);

  return rDeltaHardening;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void UnsaturatedCasmHardeningRule::GetRequiredProperties(Properties &rProperties)
{
  KRATOS_TRY

  // Set required properties
  double value = 0.0; //dummy
  rProperties.SetValue(SWELLING_SLOPE,value);
  rProperties.SetValue(NORMAL_COMPRESSION_SLOPE,value);
  rProperties.SetValue(SPACING_RATIO,value);
  rProperties.SetValue(PRE_CONSOLIDATION_STRESS,value);

  KRATOS_CATCH(" ")
}

} // namespace Kratos.
