//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              IPouplana $
//   Maintained by:       $Maintainer:           JMCarbonell $
//   Date:                $Date:                  April 2017 $
//
//

#if !defined(KRATOS_EXPONENTIAL_DAMAGE_HARDENING_RULE_HPP_INCLUDED)
#define KRATOS_EXPONENTIAL_DAMAGE_HARDENING_RULE_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/hardening_rules/hardening_rule.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) ExponentialDamageHardeningRule
    : public HardeningRule
{
protected:
  //warning::this variable is going to be shadowed by they derived classes
  //if any problem is detected an alternative method must be used instead
  constexpr static std::size_t VarSize = 2;

public:
  ///@name Type Definitions
  ///@{
  enum class NameType : std::size_t { DAMAGE_STATE = 0, DAMAGE = 1 };

  //typedef ConstitutiveModelData::InternalVariables<VarSize> InternalVariablesType;
  //typedef ConstitutiveModelData::PlasticModelData<VarSize> PlasticDataType;

  typedef ConstitutiveModelData::InternalData<VarSize,NameType> InternalDataType;
  typedef ConstitutiveModelData::InternalBuffer<InternalDataType,2> InternalVariablesType;
  typedef ConstitutiveModelData::PlasticityData<VarSize,NameType> PlasticityDataType;
  typedef ConstitutiveModelData::ModelDataVariables<PlasticityDataType> PlasticDataType;

  /// Pointer definition of ExponentialDamageHardeningRule
  KRATOS_CLASS_POINTER_DEFINITION(ExponentialDamageHardeningRule);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  ExponentialDamageHardeningRule();

  /// Copy constructor.
  ExponentialDamageHardeningRule(ExponentialDamageHardeningRule const &rOther);

  /// Assignment operator.
  ExponentialDamageHardeningRule &operator=(ExponentialDamageHardeningRule const &rOther);

  /// Clone.
  HardeningRule::Pointer Clone() const override;

  /// Destructor.
  ~ExponentialDamageHardeningRule() override;

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /**
     * Calculate Hardening functions
     */

  virtual double &CalculateHardening(const PlasticDataType &rVariables, double &rHardening); //do not override -> it must hide the method

  /**
     * Calculate Hardening function derivatives
     */

  virtual double &CalculateDeltaHardening(const PlasticDataType &rVariables, double &rDeltaHardening); //do not override -> it must hide the method

  /**
     * Get required properties
     */
  void GetRequiredProperties(Properties &rProperties) override;

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "ExponentialDamageHardeningRule";
    return buffer.str();
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "ExponentialDamageHardeningRule";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "ExponentialDamageHardeningRule Data";
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  using HardeningRule::CalculateDeltaHardening;
  using HardeningRule::CalculateHardening;

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, HardeningRule)
  }

  void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, HardeningRule)
  }

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class ExponentialDamageHardeningRule

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_EXPONENTIAL_DAMAGE_HARDENING_RULE_HPP_INCLUDED  defined
