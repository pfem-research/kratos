//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                  April 2017 $
//
//

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/hardening_rules/simo_linear_thermal_hardening_rule.hpp"

namespace Kratos
{

//*******************************CONSTRUCTOR******************************************
//************************************************************************************

SimoLinearThermalHardeningRule::SimoLinearThermalHardeningRule()
    : SimoLinearHardeningRule()
{
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

SimoLinearThermalHardeningRule &SimoLinearThermalHardeningRule::operator=(SimoLinearThermalHardeningRule const &rOther)
{
  SimoLinearHardeningRule::operator=(rOther);
  return *this;
}

//*******************************COPY CONSTRUCTOR*************************************
//************************************************************************************

SimoLinearThermalHardeningRule::SimoLinearThermalHardeningRule(SimoLinearThermalHardeningRule const &rOther)
    : SimoLinearHardeningRule(rOther)
{
}

//********************************CLONE***********************************************
//************************************************************************************

HardeningRule::Pointer SimoLinearThermalHardeningRule::Clone() const
{
  return Kratos::make_shared<SimoLinearThermalHardeningRule>(*this);
}

//********************************DESTRUCTOR******************************************
//************************************************************************************

SimoLinearThermalHardeningRule::~SimoLinearThermalHardeningRule()
{
}

/// Operations.

//***************************CALCULATE HARDENING DERIVATIVE TEMPERATURE***************
//************************************************************************************

double &SimoLinearThermalHardeningRule::CalculateDeltaThermalHardening(const PlasticDataType &rVariables, double &rDeltaThermalHardening)
{
  KRATOS_TRY

  const ModelDataType &rModelData = rVariables.GetModelData();

  //get values
  const double &rCurrentPlasticStrain = rVariables.Data.GetInternal()[NameType::PLASTIC_STRAIN];

  //linear hardening properties
  const Properties &rProperties = rModelData.GetProperties();
  double YieldStress = rProperties[YIELD_STRESS];
  double KinematicHardeningConstant = rProperties[KINEMATIC_HARDENING_MODULUS];

  //parameters for the thermal solution
  const double &YieldStressThermalSoftening = rProperties[YIELD_STRESS_THERMAL_SOFTENING];
  const double &HardeningThermalSoftening = rProperties[HARDENING_THERMAL_SOFTENING];

  double ThermalFactor = this->CalculateThermalReferenceEffect(rVariables, ThermalFactor);
  YieldStress *= ThermalFactor;

  ThermalFactor = this->CalculateThermalCurrentEffect(rVariables, ThermalFactor);
  KinematicHardeningConstant *= ThermalFactor;

  //Linear Hardening rule: (mTheta = 1)
  rDeltaThermalHardening = (YieldStress * YieldStressThermalSoftening + SimoExponentialHardeningRule::mTheta * KinematicHardeningConstant * HardeningThermalSoftening * rCurrentPlasticStrain);

  return rDeltaThermalHardening;

  KRATOS_CATCH(" ")
}

//***************************CALCULATE TEMPERATURE EVOLUTION PROPERTIES***************
//************************************************************************************

double &SimoLinearThermalHardeningRule::CalculateThermalReferenceEffect(const PlasticDataType &rVariables, double &rThermalFactor)
{
  KRATOS_TRY

  const ModelDataType &rModelData = rVariables.GetModelData();

  //get values
  const Properties &rProperties = rModelData.GetProperties();
  const double &rTemperature = rModelData.GetTemperature();

  //parameters for the thermal solution
  const double &YieldStressThermalSoftening = rProperties[YIELD_STRESS_THERMAL_SOFTENING];
  const double &ReferenceTemperature = rProperties[REFERENCE_TEMPERATURE];

  //thermal effect in the initial parameters
  rThermalFactor = (1.0 - YieldStressThermalSoftening * (rTemperature - ReferenceTemperature));

  return rThermalFactor;

  KRATOS_CATCH(" ")
}

//***************************CALCULATE TEMPERATURE EVOLUTION PROPERTIES***************
//************************************************************************************

double &SimoLinearThermalHardeningRule::CalculateThermalCurrentEffect(const PlasticDataType &rVariables, double &rThermalFactor)
{
  KRATOS_TRY

  const ModelDataType &rModelData = rVariables.GetModelData();

  //get values
  const Properties &rProperties = rModelData.GetProperties();
  const double &rTemperature = rModelData.GetTemperature();

  //parameters for the thermal solution
  const double &HardeningThermalSoftening = rProperties[HARDENING_THERMAL_SOFTENING];
  const double &ReferenceTemperature = rProperties[REFERENCE_TEMPERATURE];

  //thermal effect in the final parameters
  rThermalFactor = (1.0 - HardeningThermalSoftening * (rTemperature - ReferenceTemperature));

  return rThermalFactor;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void SimoLinearThermalHardeningRule::GetRequiredProperties(Properties &rProperties)
{
  KRATOS_TRY

  // Set required properties
  double value = 0.0; //dummy
  rProperties.SetValue(YIELD_STRESS,value);
  rProperties.SetValue(KINEMATIC_HARDENING_MODULUS,value);
  rProperties.SetValue(HARDENING_THERMAL_SOFTENING,value);
  rProperties.SetValue(REFERENCE_TEMPERATURE,value);
  rProperties.SetValue(YIELD_STRESS_THERMAL_SOFTENING,value);

  rProperties.SetValue(THERMAL_EXPANSION_COEFFICIENT, value);

  KRATOS_CATCH(" ")
}

} // namespace Kratos.
