//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                  April 2017 $
//
//

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/hardening_rules/simo_exponential_thermal_hardening_rule.hpp"

namespace Kratos
{

//*******************************CONSTRUCTOR******************************************
//************************************************************************************

SimoExponentialThermalHardeningRule::SimoExponentialThermalHardeningRule()
    : SimoExponentialHardeningRule()
{
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

SimoExponentialThermalHardeningRule &SimoExponentialThermalHardeningRule::operator=(SimoExponentialThermalHardeningRule const &rOther)
{
  SimoExponentialHardeningRule::operator=(rOther);
  return *this;
}

//*******************************COPY CONSTRUCTOR*************************************
//************************************************************************************

SimoExponentialThermalHardeningRule::SimoExponentialThermalHardeningRule(SimoExponentialThermalHardeningRule const &rOther)
    : SimoExponentialHardeningRule(rOther)
{
}

//********************************CLONE***********************************************
//************************************************************************************

HardeningRule::Pointer SimoExponentialThermalHardeningRule::Clone() const
{
  return Kratos::make_shared<SimoExponentialThermalHardeningRule>(*this);
}

//********************************DESTRUCTOR******************************************
//************************************************************************************

SimoExponentialThermalHardeningRule::~SimoExponentialThermalHardeningRule()
{
}

/// Operations.

//***************************CALCULATE HARDENING DERIVATIVE TEMPERATURE***************
//************************************************************************************

double &SimoExponentialThermalHardeningRule::CalculateDeltaThermalHardening(const PlasticDataType &rVariables, double &rDeltaThermalHardening)
{
  KRATOS_TRY

  const ModelDataType &rModelData = rVariables.GetModelData();

  //get values
  const double &rCurrentPlasticStrain = rVariables.Data.GetInternal()[NameType::PLASTIC_STRAIN];

  //linear hardening properties
  const Properties &rProperties = rModelData.GetProperties();
  double YieldStress = rProperties[YIELD_STRESS];
  double KinematicHardeningConstant = rProperties[KINEMATIC_HARDENING_MODULUS];

  //exponential saturation properties
  double K_reference = rProperties[REFERENCE_HARDENING_MODULUS];
  double K_infinity = rProperties[INFINITY_HARDENING_MODULUS];
  const double &Delta = rProperties[HARDENING_EXPONENT];

  //parameters for the thermal solution
  const double &YieldStressThermalSoftening = rProperties[YIELD_STRESS_THERMAL_SOFTENING];
  const double &HardeningThermalSoftening = rProperties[HARDENING_THERMAL_SOFTENING];

  double ThermalFactor = this->CalculateThermalReferenceEffect(rVariables, ThermalFactor);
  YieldStress *= ThermalFactor;
  K_reference *= ThermalFactor;

  ThermalFactor = this->CalculateThermalCurrentEffect(rVariables, ThermalFactor);
  K_infinity *= ThermalFactor;
  KinematicHardeningConstant *= ThermalFactor;

  //Linear Hardening rule: (mTheta = 1)
  rDeltaThermalHardening = (YieldStress * YieldStressThermalSoftening + SimoExponentialHardeningRule::mTheta * KinematicHardeningConstant * HardeningThermalSoftening * rCurrentPlasticStrain);

  //Exponential Saturation:
  rDeltaThermalHardening += (K_infinity * HardeningThermalSoftening - K_reference * YieldStressThermalSoftening) * (1.0 - exp((-1.0) * Delta * rCurrentPlasticStrain));

  return rDeltaThermalHardening;

  KRATOS_CATCH(" ")
}

//***************************CALCULATE TEMPERATURE EVOLUTION PROPERTIES***************
//************************************************************************************

double &SimoExponentialThermalHardeningRule::CalculateThermalReferenceEffect(const PlasticDataType &rVariables, double &rThermalFactor)
{
  KRATOS_TRY

  const ModelDataType &rModelData = rVariables.GetModelData();

  //get values
  const Properties &rProperties = rModelData.GetProperties();
  const double &rTemperature = rModelData.GetTemperature();

  //parameters for the thermal solution
  const double &YieldStressThermalSoftening = rProperties[YIELD_STRESS_THERMAL_SOFTENING];
  const double &ReferenceTemperature = rProperties[REFERENCE_TEMPERATURE];

  //thermal effect in the initial parameters
  rThermalFactor = (1.0 - YieldStressThermalSoftening * (rTemperature - ReferenceTemperature));

  return rThermalFactor;

  KRATOS_CATCH(" ")
}

//***************************CALCULATE TEMPERATURE EVOLUTION PROPERTIES***************
//************************************************************************************

double &SimoExponentialThermalHardeningRule::CalculateThermalCurrentEffect(const PlasticDataType &rVariables, double &rThermalFactor)
{
  KRATOS_TRY

  const ModelDataType &rModelData = rVariables.GetModelData();

  //get values
  const Properties &rProperties = rModelData.GetProperties();
  const double &rTemperature = rModelData.GetTemperature();

  //parameters for the thermal solution
  const double &HardeningThermalSoftening = rProperties[HARDENING_THERMAL_SOFTENING];
  const double &ReferenceTemperature = rProperties[REFERENCE_TEMPERATURE];

  //thermal effect in the final parameters
  rThermalFactor = (1.0 - HardeningThermalSoftening * (rTemperature - ReferenceTemperature));

  return rThermalFactor;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void SimoExponentialThermalHardeningRule::GetRequiredProperties(Properties &rProperties)
{
  KRATOS_TRY

  // Set required properties
  double value = 0.0; //dummy
  rProperties.SetValue(YIELD_STRESS,value);
  rProperties.SetValue(KINEMATIC_HARDENING_MODULUS,value);
  rProperties.SetValue(REFERENCE_HARDENING_MODULUS,value);
  rProperties.SetValue(INFINITY_HARDENING_MODULUS,value);
  rProperties.SetValue(HARDENING_EXPONENT,value);
  rProperties.SetValue(HARDENING_THERMAL_SOFTENING,value);
  rProperties.SetValue(REFERENCE_TEMPERATURE,value);
  rProperties.SetValue(YIELD_STRESS_THERMAL_SOFTENING,value);

  rProperties.SetValue(THERMAL_EXPANSION_COEFFICIENT, value);

  KRATOS_CATCH(" ")
}

} // namespace Kratos.
