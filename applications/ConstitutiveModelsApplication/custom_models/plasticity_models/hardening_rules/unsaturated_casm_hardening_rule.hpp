//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:               December 2021 $
//
//

#if !defined(KRATOS_UNSAT_CASM_HARDENING_RULE_HPP_INCLUDED)
#define KRATOS_UNSAT_CASM_HARDENING_RULE_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/hardening_rules/hardening_rule.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) UnsaturatedCasmHardeningRule : public HardeningRule
{
protected:
  constexpr static std::size_t VarSize = 12;

public:
  enum class NameType : std::size_t {PLASTIC_STRAIN = 0,
     PLASTIC_VOL_DEF = 1,
     PLASTIC_DEV_DEF = 2,
     PLASTIC_VOL_DEF_ABS = 3,
     BONDING = 4,
     PC = 5,
     NONLOCAL_PLASTIC_VOL_DEF = 8,
     HENCKY_ELASTIC_VOLUMETRIC_STRAIN = 10,
     NONLOCAL_HENCKY_ELASTIC_VOLUMETRIC_STRAIN = 10,
  };

  typedef ConstitutiveModelData::InternalData<VarSize,NameType> InternalDataType;
  typedef ConstitutiveModelData::InternalBuffer<InternalDataType,2> InternalVariablesType;
  typedef ConstitutiveModelData::PlasticityData<VarSize,NameType> PlasticityDataType;
  typedef ConstitutiveModelData::ModelDataVariables<PlasticityDataType> PlasticDataType;

  /// Pointer definition of UnsaturatedCasmHardeningRule
  KRATOS_CLASS_POINTER_DEFINITION(UnsaturatedCasmHardeningRule);

  ///@}
   ///@name Life Cycle
  ///@{

  /// Default constructor.
  UnsaturatedCasmHardeningRule();

  /// Copy constructor.
  UnsaturatedCasmHardeningRule(UnsaturatedCasmHardeningRule const &rOther);

  /// Assignment operator.
  UnsaturatedCasmHardeningRule &operator=(UnsaturatedCasmHardeningRule const &rOther);

  /// Clone.
  virtual HardeningRule::Pointer Clone() const override;

  /// Destructor.
  ~UnsaturatedCasmHardeningRule();

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /**
     * Calculate Hardening functions
     */

  virtual double &CalculateHardening(const PlasticDataType &rVariables, double &rHardening); //do not override -> it must hide the method

  /**
     * Calculate Hardening function derivatives
     */

  virtual double &CalculateDeltaHardening(const PlasticDataType &rVariables, double &rDeltaHardening); //do not override -> it must hide the method

  virtual double &CalculateDeltaHardening(const PlasticDataType &rVariables, double &rDeltaHardening, const MatrixType &rPlasticPotentialDerivative); //do not override -> it must hide the method
  
  virtual double &CalculateDeltaHardeningUnsat(const PlasticDataType &rVariables, double &rDeltaHardening, const MatrixType &rPlasticPotentialDerivative, const double & rSuction); //do not override -> it must hide the method

  /**
     * Get required properties
     */
  void GetRequiredProperties(Properties &rProperties) override;

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  virtual std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "UnsaturatedCasmHardeningRule";
    return buffer.str();
  }

  /// Print information about this object.
  virtual void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "UnsaturatedCasmHardeningRule";
  }

  /// Print object's data.
  virtual void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "UnsaturatedCasmHardeningRule Data";
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  using HardeningRule::CalculateDeltaHardeningUnsat;
  using HardeningRule::CalculateDeltaHardening;
  using HardeningRule::CalculateHardening;

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  virtual void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, HardeningRule)
  }

  virtual void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, HardeningRule)
  }

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class UnsaturatedCasmHardeningRule

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_UNSAT_CASM_HARDENING_RULE_HPP_INCLUDED  defined
