//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                       $
//   Date:                $Date:                  March 2021 $
//
//

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/hardening_rules/casm_structure_hardening_rule.hpp"
#include "custom_utilities/constitutive_model_utilities.hpp"
#include "custom_utilities/stress_invariants_utilities.hpp"
#include "custom_utilities/shape_deviatoric_plane_utilities.hpp"

namespace Kratos
{

//*******************************CONSTRUCTOR******************************************
//************************************************************************************

CasmStructureHardeningRule::CasmStructureHardeningRule()
    : HardeningRule()
{
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

CasmStructureHardeningRule &CasmStructureHardeningRule::operator=(CasmStructureHardeningRule const &rOther)
{
  HardeningRule::operator=(rOther);
  return *this;
}

//*******************************COPY CONSTRUCTOR*************************************
//************************************************************************************

CasmStructureHardeningRule::CasmStructureHardeningRule(CasmStructureHardeningRule const &rOther)
    : HardeningRule(rOther)
{
}

//********************************CLONE***********************************************
//************************************************************************************

HardeningRule::Pointer CasmStructureHardeningRule::Clone() const
{
  return (HardeningRule::Pointer(new CasmStructureHardeningRule(*this)));
}

//********************************DESTRUCTOR******************************************
//************************************************************************************

CasmStructureHardeningRule::~CasmStructureHardeningRule()
{
}

/// Operations.

//*******************************CALCULATE TOTAL HARDENING****************************
//************************************************************************************

double &CasmStructureHardeningRule::CalculateHardening(const PlasticDataType &rVariables, double &rHardening)
{
  KRATOS_TRY


  KRATOS_ERROR << " you should not be here " << std::endl;
  return rHardening;

  KRATOS_CATCH(" ")
}

//*******************************CALCULATE HARDENING DERIVATIVE***********************
//************************************************************************************
double &CasmStructureHardeningRule::CalculateDeltaHardening(const PlasticDataType &rVariables, double &rDeltaHardening)
{
  KRATOS_ERROR << " not implemented. should not be here " << std::endl;
}

//*******************************CALCULATE HARDENING DERIVATIVE***********************
//************************************************************************************
double &CasmStructureHardeningRule::CalculateDeltaHardening(const PlasticDataType &rVariables, double &rDeltaHardening, const MatrixType &rPlasticPotentialDerivative)
{
  KRATOS_TRY

  const ModelDataType &rModelData = rVariables.GetModelData();
  const Properties &rMaterialProperties = rModelData.GetProperties();
  const MatrixType    & rStressMatrix = rModelData.GetStressMatrix();

  const double &rP0 = rVariables.Data.Internal(4);
  const double &rPM = rVariables.Data.Internal(5);
  const double &rPT = rVariables.Data.Internal(6);

  const double & rShearM = rMaterialProperties[CRITICAL_STATE_LINE];
  const double & rH1 = rMaterialProperties[DEGRADATION_RATE_COMPRESSION];
  const double & rH2 = rMaterialProperties[DEGRADATION_RATE_SHEAR];
  const double & rSpacingR = rMaterialProperties[SPACING_RATIO];
  const double & rShapeN   = rMaterialProperties[SHAPE_PARAMETER];
  const double & rSwellingSlope = rMaterialProperties[SWELLING_SLOPE];
  const double & rOtherSlope = rMaterialProperties[NORMAL_COMPRESSION_SLOPE];
  const double &rFriction = rMaterialProperties[INTERNAL_FRICTION_ANGLE];


  double TracePlasticPotDerivative = 0.0;
  for (unsigned int i = 0; i < 3; i++)
    TracePlasticPotDerivative += rPlasticPotentialDerivative(i, i);

  double AbsTracePlasticPotDerivative = std::fabs(TracePlasticPotDerivative);

  MatrixType DevPlasticPotDerivative = rPlasticPotentialDerivative;
  for (unsigned int i = 0; i < 3; i++)
    DevPlasticPotDerivative(i, i) -= TracePlasticPotDerivative / 3.0;

  double NormPlasticPotDerivative = 0.0;
  for (unsigned int i = 0; i < 3; i++)
  {
    for (unsigned int j = 0; j < 3; j++)
    {
      NormPlasticPotDerivative += pow(DevPlasticPotDerivative(i, j), 2.0);
    }
  }
  NormPlasticPotDerivative = sqrt(NormPlasticPotDerivative);

  //calculate stress invariants
  double MeanStress, LodeAngle, J2sqrt;
  StressInvariantsUtilities::CalculateStressInvariants(rStressMatrix, MeanStress, J2sqrt, LodeAngle);
  double ThirdInvEffect = 1.0;
  ShapeAtDeviatoricPlaneUtility::EvaluateEffectDueToThirdInvariant(ThirdInvEffect, LodeAngle, rFriction);

  double dFdP0 = -1.0/( std::log(rSpacingR) * (rP0 + rPT+rPM) );
  double dFdPM = dFdP0;
  double dFdPT = (rP0 + rPM -MeanStress)/ ( std::log(rSpacingR)* (MeanStress+rP0) * (rPT+rP0+rPM) );
  dFdPT += dFdPT + rShapeN * std::pow( std::sqrt(3.0)*J2sqrt/(rShearM/ThirdInvEffect), rShapeN) / std::pow( -MeanStress-rPT, rShapeN+1.0);

  double dP0dEpsVol = -rP0/(rOtherSlope-rSwellingSlope);

  rDeltaHardening = -dFdP0*dP0dEpsVol*TracePlasticPotDerivative;
  rDeltaHardening -= dFdPT*rPT*( TracePlasticPotDerivative/(rOtherSlope-rSwellingSlope ) + rH1 * AbsTracePlasticPotDerivative + rH2 * std::sqrt(2.0/3.0) * NormPlasticPotDerivative );
  rDeltaHardening -= dFdPM*rPM*( TracePlasticPotDerivative/(rOtherSlope-rSwellingSlope ) + rH1 * AbsTracePlasticPotDerivative + rH2 * std::sqrt(2.0/3.0) * NormPlasticPotDerivative );

  return rDeltaHardening;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void CasmStructureHardeningRule::GetRequiredProperties(Properties &rProperties)
{
  KRATOS_TRY

  // Set required properties
  double value = 0.0; //dummy
  rProperties.SetValue(SWELLING_SLOPE,value);
  rProperties.SetValue(NORMAL_COMPRESSION_SLOPE,value);
  rProperties.SetValue(SPACING_RATIO,value);

  KRATOS_CATCH(" ")
}


} // namespace Kratos.
