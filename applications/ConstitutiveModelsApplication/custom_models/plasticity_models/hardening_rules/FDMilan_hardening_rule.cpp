//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:       Kateryna Oliynyk $
//   Maintained by:       $Maintainer:                    KO $
//   Date:                $Date:               November 2020 $
//
//

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/hardening_rules/FDMilan_hardening_rule.hpp"
#include "custom_utilities/stress_invariants_utilities.hpp"
#include "custom_utilities/shape_deviatoric_plane_utilities.hpp"

namespace Kratos
{

//*******************************CONSTRUCTOR******************************************
//************************************************************************************

FDMilanHardeningRule::FDMilanHardeningRule()
    : HardeningRule()
{
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

FDMilanHardeningRule &FDMilanHardeningRule::operator=(FDMilanHardeningRule const &rOther)
{
  HardeningRule::operator=(rOther);
  return *this;
}

//*******************************COPY CONSTRUCTOR*************************************
//************************************************************************************

FDMilanHardeningRule::FDMilanHardeningRule(FDMilanHardeningRule const &rOther)
    : HardeningRule(rOther)
{
}

//********************************CLONE***********************************************
//************************************************************************************

HardeningRule::Pointer FDMilanHardeningRule::Clone() const
{
  return (HardeningRule::Pointer(new FDMilanHardeningRule(*this)));
}

//********************************DESTRUCTOR******************************************
//************************************************************************************

FDMilanHardeningRule::~FDMilanHardeningRule()
{
}

/// Operations.

//*******************************CALCULATE TOTAL HARDENING****************************
//************************************************************************************

double &FDMilanHardeningRule::CalculateHardening(const PlasticDataType &rVariables, double &rHardening)
{
  KRATOS_TRY

  KRATOS_ERROR << " you should not be here " << std::endl;
  return rHardening;

  KRATOS_CATCH(" ")
}

//*******************************CALCULATE HARDENING DERIVATIVE***********************
//************************************************************************************
double &FDMilanHardeningRule::CalculateDeltaHardening(const PlasticDataType &rVariables, double &rDeltaHardening)
{
  KRATOS_ERROR << " not implemented. should not be here " << std::endl;
}

//*******************************CALCULATE HARDENING DERIVATIVE***********************
//************************************************************************************
double &FDMilanHardeningRule::CalculateDeltaHardening(const PlasticDataType &rVariables, double &rDeltaHardening, const MatrixType &rPlasticPotentialDerivative)
{
  KRATOS_TRY

  const ModelDataType &rModelData = rVariables.GetModelData();
  const Properties &rMaterialProperties = rModelData.GetProperties();
  const MatrixType &rStressMatrix = rModelData.GetStressMatrix();

  const double &rPS = rVariables.Data.Internal(3);
  const double &rPT = rVariables.Data.Internal(4);
  const double &rPCstar = rVariables.Data.Internal(5);
  const double &rhos = rMaterialProperties[RHOS];
  const double &rhot = rMaterialProperties[RHOT];
  const double &k = rMaterialProperties[KSIM];
  const double &rShearM = rMaterialProperties[CRITICAL_STATE_LINE];
  const double &ralpha_f = rMaterialProperties[ALPHA_F];
  const double &rmu_f = rMaterialProperties[MU_F];

  double Friction = 0;
  if (rMaterialProperties.Has(INTERNAL_FRICTION_ANGLE))
   Friction = rMaterialProperties[INTERNAL_FRICTION_ANGLE];

  // calculate sig* = sig + pt

  MatrixType StressTranslated;
  StressTranslated.clear();
  StressTranslated = rStressMatrix;
  for (unsigned int i = 0; i < 3; i++)
    StressTranslated(i, i) += rPT;

  // compute stress invariants p* = MeanStressT, J = J2sqrt, theta = LodeAngle (pstarSM = -MeanStressT positive in compression)

  double MeanStressT, J2sqrt, LodeAngle;
  StressInvariantsUtilities::CalculateStressInvariants(StressTranslated, MeanStressT, J2sqrt, LodeAngle);

  // compute function M(theta)

  double Mth, Mth3;
  double ThirdInvariantEffect = 1.0;
  if (Friction > 0.0)
    ThirdInvariantEffect = ShapeAtDeviatoricPlaneUtility::EvaluateEffectDueToThirdInvariant(ThirdInvariantEffect, LodeAngle, Friction);

  Mth = rShearM/ThirdInvariantEffect;
  Mth3 = Mth/sqrt(3.0);

  // compute functions K1f, K2f, Cf

  double tmp1, tmp2, K1f, K2f, CCf, Pow1, Pow2;

  tmp1 = rmu_f*(1.0-ralpha_f)/(2.0*(1.0-rmu_f));
  tmp2 = 1.0 - 4.0*ralpha_f*(1.0-rmu_f)/(rmu_f*(1.0-ralpha_f)*(1.0-ralpha_f));

  K1f = tmp1*(1.0 + sqrt(tmp2));
  K2f = tmp1*(1.0 - sqrt(tmp2));
  CCf  = (1.0-rmu_f)*(K1f-K2f);

  Pow1  = -K1f/CCf;
  Pow2  = K2f/CCf;

  // compute eta_max if it exists - Note: etadash = (1/Mth)*(-J/p*)

  bool ExistsEtaJMax;
  double EtaJMax = 1.0e6;

  if (ralpha_f > 1.0){
    ExistsEtaJMax = true;
    EtaJMax = -K2f*Mth3;
  }
  else if (rmu_f > 1.0){
    ExistsEtaJMax = true;
    EtaJMax = -K1f*Mth3;
  }
  else{
    ExistsEtaJMax = false;
  }

// compute derivatives of f w.r. to ps, pt

  double EtaJ, Af, Bf;
  double Aux1, Aux2, Aux3;
  double dfdps, dfdpt;

  if(abs(MeanStressT)>1.0e-6){
    EtaJ = -J2sqrt/MeanStressT;
  }
  else{
    EtaJ = J2sqrt/1.0e-6;
  }

  if(ExistsEtaJMax) {
    if ((MeanStressT < 0) && (EtaJ < EtaJMax)) {
      // Zone 1 - Lagioia et al. surface
      Af = 1.0 + EtaJ/(K1f*Mth3);
      Bf = 1.0 + EtaJ/(K2f*Mth3);
      Aux1 = pow(Af,Pow1)*pow(Bf,Pow2);
      Aux2 = 1.0/(Mth3*CCf);
      Aux3 = 1.0/Af - 1.0/Bf;
      dfdps = Aux1;
      dfdpt = -1.0 + Aux1*(rPCstar/MeanStressT)*Aux2*Aux3*EtaJ;
      dfdpt += (1.0+k)*Aux1;
    }
    else if((MeanStressT >= 0) && (J2sqrt < MeanStressT/Mth3)) {
      // Zone 3 - circular surface
      double DeltaEta = EtaJMax - Mth3;
      double Alpha = std::atan(Mth3);
      double Phi = std::cos(Alpha);
      double radius = sqrt(pow(MeanStressT,2)+pow(J2sqrt,2));
      dfdps = 0.0;
      dfdpt = MeanStressT/(Phi*DeltaEta*radius);
    }
    else{
      // Zone 2 - Linear surface
      double DeltaEta = EtaJMax - Mth3;
      dfdps = 0.0;
      dfdpt = Mth3/DeltaEta;
    }
  }
  else{
    if (MeanStressT < 0) {
      // Zone 1 - Lagioia et al. surface
      Af = 1.0 + EtaJ/(K1f*Mth3);
      Bf = 1.0 + EtaJ/(K2f*Mth3);
      Aux1 = pow(Af,Pow1)*pow(Bf,Pow2);
      Aux2 = 1.0/(Mth3*CCf);
      Aux3 = 1.0/Af - 1.0/Bf;
      dfdps = Aux1;
      dfdpt = -1.0 + Aux1*(rPCstar/MeanStressT)*Aux2*Aux3*EtaJ;
      dfdpt += (1.0+k)*Aux1;
    }
    else{
      // Zona 2 - p* > 0 region
      double Slope = 1.0;
      double EtaJlim = 1.0e6;
      double Aflim = 1.0 + EtaJlim/(K1f*Mth3);
      double Bflim = 1.0 + EtaJlim/(K2f*Mth3);
      dfdps = pow(Aflim,Pow1)*pow(Bflim,Pow2);
      dfdpt = pow(Aflim,Pow1)*pow(Bflim,Pow2)*(1.0+k) + Slope;
    }
  }

// compute hardening functions

  double TracePlasticPotDerivative = 0.0;
  for (unsigned int i = 0; i < 3; i++)
    TracePlasticPotDerivative += rPlasticPotentialDerivative(i, i);

  MatrixType DevPlasticPotDerivative = rPlasticPotentialDerivative;
  for (unsigned int i = 0; i < 3; i++)
    DevPlasticPotDerivative(i, i) -= TracePlasticPotDerivative / 3.0;

  double NormPlasticPotDerivative = 0.0;
  for (unsigned int i = 0; i < 3; i++)
  {
    for (unsigned int j = 0; j < 3; j++)
    {
      NormPlasticPotDerivative += pow(DevPlasticPotDerivative(i, j), 2.0);
    }
  }
  NormPlasticPotDerivative = sqrt(NormPlasticPotDerivative);

  const double &chis = rMaterialProperties[CHIS];
  const double &chit = rMaterialProperties[CHIT];

  double hstil, httil;

  hstil = -rhos* rPS * (TracePlasticPotDerivative - chis*sqrt(2.0/3.0)*NormPlasticPotDerivative);
  httil = -rhot* rPT * (fabs(TracePlasticPotDerivative) + chit*sqrt(2.0/3.0)*NormPlasticPotDerivative);

  // compute plastic modulus Hp = -df/dq*h

  rDeltaHardening = -(dfdps*hstil + dfdpt*httil);

  return rDeltaHardening;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************
void FDMilanHardeningRule::GetRequiredProperties(Properties &rProperties)
{
  KRATOS_TRY

  // Set required properties
  double value = 0.0; //dummy
  rProperties.SetValue(RHOS,value);
  rProperties.SetValue(RHOT,value);
  rProperties.SetValue(KSIM,value);
  rProperties.SetValue(CRITICAL_STATE_LINE,value);
  rProperties.SetValue(ALPHA_F,value);
  rProperties.SetValue(MU_F,value);
  rProperties.SetValue(INTERNAL_FRICTION_ANGLE,value);
  rProperties.SetValue(CHIS,value);
  rProperties.SetValue(CHIT,value);

  KRATOS_CATCH(" ")
}


} // namespace Kratos.
