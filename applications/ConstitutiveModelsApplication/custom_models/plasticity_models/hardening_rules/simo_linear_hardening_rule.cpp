//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                  April 2017 $
//
//

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/hardening_rules/simo_linear_hardening_rule.hpp"

namespace Kratos
{

//*******************************CONSTRUCTOR******************************************
//************************************************************************************

SimoLinearHardeningRule::SimoLinearHardeningRule()
    : SimoExponentialHardeningRule(0.0)
{
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

SimoLinearHardeningRule &SimoLinearHardeningRule::operator=(SimoLinearHardeningRule const &rOther)
{
  SimoExponentialHardeningRule::operator=(rOther);
  return *this;
}

//*******************************COPY CONSTRUCTOR*************************************
//************************************************************************************

SimoLinearHardeningRule::SimoLinearHardeningRule(SimoLinearHardeningRule const &rOther)
    : SimoExponentialHardeningRule(rOther)
{
}

//********************************CLONE***********************************************
//************************************************************************************

HardeningRule::Pointer SimoLinearHardeningRule::Clone() const
{
  return Kratos::make_shared<SimoLinearHardeningRule>(*this);
}

//********************************DESTRUCTOR******************************************
//************************************************************************************

SimoLinearHardeningRule::~SimoLinearHardeningRule()
{
}

/// Operations.

//*******************************CALCULATE ISOTROPIC HARDENING************************
//************************************************************************************

double &SimoLinearHardeningRule::CalculateAndAddIsotropicHardening(const PlasticDataType &rVariables, double &rIsotropicHardening)
{
  KRATOS_TRY

  const ModelDataType &rModelData = rVariables.GetModelData();

  //get values
  const double &rCurrentPlasticStrain = rVariables.Data.GetInternalVariables()[0];

  //linear hardening properties
  const Properties &rProperties = rModelData.GetProperties();
  double YieldStress = rProperties[YIELD_STRESS];
  double KinematicHardeningConstant = rProperties[KINEMATIC_HARDENING_MODULUS];

  double ThermalFactor = this->CalculateThermalReferenceEffect(rVariables, ThermalFactor);
  YieldStress *= ThermalFactor;

  ThermalFactor = this->CalculateThermalCurrentEffect(rVariables, ThermalFactor);
  KinematicHardeningConstant *= ThermalFactor;

  //Linear Hardening rule: (mTheta = 0)
  rIsotropicHardening += YieldStress + (1.0 - SimoExponentialHardeningRule::mTheta) * KinematicHardeningConstant * rCurrentPlasticStrain;

  return rIsotropicHardening;

  KRATOS_CATCH(" ")
}

//*******************************CALCULATE KINEMATIC HARDENING************************
//************************************************************************************

double &SimoLinearHardeningRule::CalculateAndAddKinematicHardening(const PlasticDataType &rVariables, double &rKinematicHardening)
{
  KRATOS_TRY

  return rKinematicHardening;

  KRATOS_CATCH(" ")
}

//*******************************CALCULATE HARDENING DERIVATIVE***********************
//************************************************************************************

double &SimoLinearHardeningRule::CalculateDeltaHardening(const PlasticDataType &rVariables, double &rDeltaHardening)
{
  KRATOS_TRY

  const ModelDataType &rModelData = rVariables.GetModelData();

  //linear hardening properties
  double KinematicHardeningConstant = rModelData.GetProperties()[KINEMATIC_HARDENING_MODULUS];

  double ThermalFactor = this->CalculateThermalCurrentEffect(rVariables, ThermalFactor);
  KinematicHardeningConstant *= ThermalFactor;

  //Linear Hardening rule: (mTheta = 0)
  rDeltaHardening = SimoExponentialHardeningRule::mTheta * KinematicHardeningConstant;

  return rDeltaHardening;

  KRATOS_CATCH(" ")
}

//***************************CALCULATE ISOTROPIC HARDENING DERIVATIVE*****************
//************************************************************************************

double &SimoLinearHardeningRule::CalculateAndAddDeltaIsotropicHardening(const PlasticDataType &rVariables, double &rDeltaIsotropicHardening)
{
  KRATOS_TRY

  const ModelDataType &rModelData = rVariables.GetModelData();

  //linear hardening properties
  double KinematicHardeningConstant = rModelData.GetProperties()[KINEMATIC_HARDENING_MODULUS];

  double ThermalFactor = this->CalculateThermalCurrentEffect(rVariables, ThermalFactor);
  KinematicHardeningConstant *= ThermalFactor;

  //Linear Hardening rule: (mTheta = 0)
  rDeltaIsotropicHardening += SimoExponentialHardeningRule::mTheta * KinematicHardeningConstant;

  return rDeltaIsotropicHardening;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void SimoLinearHardeningRule::GetRequiredProperties(Properties &rProperties)
{
  KRATOS_TRY

  // Set required properties
  double value = 0.0; //dummy
  rProperties.SetValue(YIELD_STRESS,value);
  rProperties.SetValue(KINEMATIC_HARDENING_MODULUS,value);

  KRATOS_CATCH(" ")
}

} // namespace Kratos.
