//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:                  April 2017 $
//
//

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/hardening_rules/cam_clay_hardening_rule.hpp"

namespace Kratos
{

//*******************************CONSTRUCTOR******************************************
//************************************************************************************

CamClayHardeningRule::CamClayHardeningRule()
    : HardeningRule()
{
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

CamClayHardeningRule &CamClayHardeningRule::operator=(CamClayHardeningRule const &rOther)
{
  HardeningRule::operator=(rOther);
  return *this;
}

//*******************************COPY CONSTRUCTOR*************************************
//************************************************************************************

CamClayHardeningRule::CamClayHardeningRule(CamClayHardeningRule const &rOther)
    : HardeningRule(rOther)
{
}

//********************************CLONE***********************************************
//************************************************************************************

HardeningRule::Pointer CamClayHardeningRule::Clone() const
{
  return Kratos::make_shared<CamClayHardeningRule>(*this);
}

//********************************DESTRUCTOR******************************************
//************************************************************************************

CamClayHardeningRule::~CamClayHardeningRule()
{
}

/// Operations.

//*******************************CALCULATE TOTAL HARDENING****************************
//************************************************************************************

double &CamClayHardeningRule::CalculateHardening(const PlasticDataType &rVariables, double &rHardening)
{
  KRATOS_TRY

  const ModelDataType &rModelData = rVariables.GetModelData();
  const Properties &rProperties = rModelData.GetProperties();

  // get values
  const double &rVolumetricPlasticDeformation = rVariables.Data.GetInternalVariables()[1];

  // Set constitutive parameters
  const double &rFirstPreconsolidationPressure = rProperties[PRE_CONSOLIDATION_STRESS];
  const double &rSwellingSlope = rProperties[SWELLING_SLOPE];
  const double &rOtherSlope = rProperties[NORMAL_COMPRESSION_SLOPE];

  rHardening = -rFirstPreconsolidationPressure * (std::exp(-rVolumetricPlasticDeformation / (rOtherSlope - rSwellingSlope)));

  return rHardening;

  KRATOS_CATCH(" ")
}

//*******************************CALCULATE HARDENING DERIVATIVE***********************
//************************************************************************************

double &CamClayHardeningRule::CalculateDeltaHardening(const PlasticDataType &rVariables, double &rDeltaHardening)
{
  KRATOS_TRY

  const ModelDataType &rModelData = rVariables.GetModelData();
  const Properties &rProperties = rModelData.GetProperties();
  const MatrixType &rStressMatrix = rModelData.GetStressMatrix();

  const double &rSwellingSlope = rProperties[SWELLING_SLOPE];
  const double &rOtherSlope = rProperties[NORMAL_COMPRESSION_SLOPE];

  double MeanStress = 0.0;
  for (unsigned int i = 0; i < 3; i++)
    MeanStress += rStressMatrix(i, i) / 3.0;

  double PreconsolidationStress = CalculateHardening(rVariables, PreconsolidationStress);

  rDeltaHardening = (2.0 * MeanStress - PreconsolidationStress);
  rDeltaHardening *= (-MeanStress);
  rDeltaHardening *= PreconsolidationStress / (rOtherSlope - rSwellingSlope);
  return rDeltaHardening;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void CamClayHardeningRule::GetRequiredProperties(Properties &rProperties)
{
  KRATOS_TRY

  // Set required properties
  double value = 0.0; //dummy
  rProperties.SetValue(SWELLING_SLOPE,value);
  rProperties.SetValue(NORMAL_COMPRESSION_SLOPE,value);
  rProperties.SetValue(PRE_CONSOLIDATION_STRESS,value);

  KRATOS_CATCH(" ")
}

} // namespace Kratos.
