//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:                  March 2021 $
//
//

#if !defined(KRATOS_STRUCTURED_CASM_MCC_SOIL_MODEL_HPP_INCLUDED)
#define      KRATOS_STRUCTURED_CASM_MCC_SOIL_MODEL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/structured_casm_base_soil_model.hpp"
#include "custom_models/plasticity_models/hardening_rules/casm_structure_hardening_rule.hpp"
#include "custom_models/plasticity_models/yield_surfaces/casm_structure_yield_surface.hpp"
#include "custom_models/elasticity_models/borja_model.hpp"

#include "custom_models/plasticity_models/yield_surfaces/plastic_potential/modified_cam_clay_structure_plastic_potential.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
    */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) StructuredCasmMCCSoilModel : public StructuredCasmBaseSoilModel<BorjaModel, CasmStructureYieldSurface<CasmStructureHardeningRule>>
{
public:
   ///@name Type Definitions
   ///@{

   //elasticity model
   typedef BorjaModel ElasticityModelType;
   typedef ElasticityModelType::Pointer ElasticityModelPointer;

   //yield surface
   typedef CasmStructureHardeningRule HardeningRuleType;
   typedef CasmStructureYieldSurface<HardeningRuleType> YieldSurfaceType;
   typedef YieldSurfaceType::Pointer YieldSurfacePointer;

   //base type
   typedef StructuredCasmBaseSoilModel<ElasticityModelType, YieldSurfaceType> BaseType;


   //common types
   typedef BaseType::Pointer BaseTypePointer;
   typedef BaseType::SizeType SizeType;
   typedef BaseType::VoigtIndexType VoigtIndexType;
   typedef BaseType::MatrixType MatrixType;
   typedef BaseType::ModelDataType ModelDataType;
   typedef BaseType::MaterialDataType MaterialDataType;
   typedef BaseType::PlasticDataType PlasticDataType;
   typedef BaseType::InternalVariablesType InternalVariablesType;

   /// Pointer definition of StructuredCasmMCCSoilModel
   KRATOS_CLASS_POINTER_DEFINITION(StructuredCasmMCCSoilModel);

   ///@}
   ///@name Life Cycle
   ///@{

   /// Default constructor.
   StructuredCasmMCCSoilModel() : BaseType()
   {
      //ModifiedCamClayPlasticPotential<CasmStructureHardeningRule> Object;
      ModifiedCamClayStructurePlasticPotential<CasmStructureHardeningRule> Object;
      YieldSurface<CasmStructureHardeningRule>::Pointer pPlasticPotential = Object.Clone();
      mYieldSurface = CasmStructureYieldSurface<CasmStructureHardeningRule>(pPlasticPotential);
   }

   /// Copy constructor.
   StructuredCasmMCCSoilModel(StructuredCasmMCCSoilModel const &rOther) : BaseType(rOther) {}

   /// Assignment operator.
   StructuredCasmMCCSoilModel &operator=(StructuredCasmMCCSoilModel const &rOther)
   {
      BaseType::operator=(rOther);
      return *this;
   }

   /// Clone.
   ConstitutiveModel::Pointer Clone() const override
   {
      return Kratos::make_shared<StructuredCasmMCCSoilModel>(*this);
   }

   /// Destructor.
   ~StructuredCasmMCCSoilModel() override {}

   ///@}
   ///@name Operators
   ///@{

   ///@}
   ///@name Operations
   ///@{

   /**
          * Check
          */
   int Check(const Properties &rProperties, const ProcessInfo &rCurrentProcessInfo) override
   {
      KRATOS_TRY

      //LMV: to be implemented. but should not enter in the base one

      return 0;

      KRATOS_CATCH("")
   }

   ///@}
   ///@name Access
   ///@{

   /**
          * Has Values
          */



   ///@}
   ///@name Inquiry
   ///@{

   ///@}
   ///@name Input and output
   ///@{

   /// Turn back information as a string.
   std::string Info() const override
   {
      std::stringstream buffer;
      buffer << "StructuredCasmMCCSoilModel";
      return buffer.str();
   }

   /// Print information about this object.
   void PrintInfo(std::ostream &rOStream) const override
   {
      rOStream << "StructuredCasmMCCSoilModel";
   }

   /// Print object's data.
   void PrintData(std::ostream &rOStream) const override
   {
      rOStream << "StructuredCasmMCCSoilModel Data";
   }

   ///@}
   ///@name Friends
   ///@{

   ///@}

protected:
   ///@name Protected static Member Variables
   ///@{

   ///@}
   ///@name Protected member Variables
   ///@{

   ///@}
   ///@name Protected Operators
   ///@{

   ///@}
   ///@name Protected Operations
   ///@{

   ///@}
   ///@name Protected  Access
   ///@{

   ///@}
   ///@name Protected Inquiry
   ///@{

   ///@}
   ///@name Protected LifeCycle
   ///@{

   ///@}

private:
   ///@name Static Member Variables
   ///@{

   ///@}
   ///@name Member Variables
   ///@{

   ///@}
   ///@name Private Operators
   ///@{

   ///@}
   ///@name Private Operations
   ///@{

   ///@}
   ///@name Private  Access
   ///@{

   ///@}
   ///@name Private Inquiry
   ///@{

   ///@}
   ///@name Serialization
   ///@{
   friend class Serializer;

   void save(Serializer &rSerializer) const override
   {
      KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BaseType)
   }

   void load(Serializer &rSerializer) override
   {
      KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BaseType)
   }

   ///@}
   ///@name Un accessible methods
   ///@{

   ///@}

}; // Class StructuredCasmMCCSoilModel

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_STRUCTURED_CASM_MCC_SOIL_MODEL_HPP_INCLUDED  defined
