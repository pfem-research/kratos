//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:                  April 2017 $
//
//

#if !defined(KRATOS_NONLOCAL_CAM_CLAY_MODEL_HPP_INCLUDED)
#define KRATOS_NONLOCAL_CAM_CLAY_MODEL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/soil_base_model.hpp"
#include "custom_models/plasticity_models/hardening_rules/cam_clay_hardening_rule.hpp"
#include "custom_models/plasticity_models/yield_surfaces/modified_cam_clay_yield_surface.hpp"
#include "custom_models/elasticity_models/borja_model.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
    */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) NonlocalCamClayModel : public SoilBaseModel<BorjaModel, ModifiedCamClayYieldSurface<CamClayHardeningRule>>
{
public:
   ///@name Type Definitions
   ///@{

   //elasticity model
   typedef BorjaModel ElasticityModelType;
   typedef ElasticityModelType::Pointer ElasticityModelPointer;

   //yield surface
   typedef CamClayHardeningRule HardeningRuleType;
   typedef ModifiedCamClayYieldSurface<HardeningRuleType> YieldSurfaceType;
   typedef YieldSurfaceType::Pointer YieldSurfacePointer;

   //base type
   typedef SoilBaseModel<ElasticityModelType, YieldSurfaceType> BaseType;

   //common types
   typedef BaseType::Pointer BaseTypePointer;
   typedef BaseType::SizeType SizeType;
   typedef BaseType::VoigtIndexType VoigtIndexType;
   typedef BaseType::MatrixType MatrixType;
   typedef BaseType::ModelDataType ModelDataType;
   typedef BaseType::MaterialDataType MaterialDataType;
   typedef BaseType::PlasticDataType PlasticDataType;
   typedef BaseType::InternalVariablesType InternalVariablesType;

   /// Pointer definition of NonlocalCamClayModel
   KRATOS_CLASS_POINTER_DEFINITION(NonlocalCamClayModel);

   ///@}
   ///@name Life Cycle
   ///@{

   /// Default constructor.
   NonlocalCamClayModel() : BaseType()  { }

   /// Copy constructor.
   NonlocalCamClayModel(NonlocalCamClayModel const &rOther) : BaseType(rOther), 
   mShearModulus(rOther.mShearModulus), mBulkModulus(rOther.mBulkModulus) {}

   /// Assignment operator.
   NonlocalCamClayModel &operator=(NonlocalCamClayModel const &rOther)
   {
      BaseType::operator=(rOther);
      this->mShearModulus = rOther.mShearModulus;
      this->mBulkModulus = rOther.mBulkModulus;
      return *this;
   }

   /// Clone.
   ConstitutiveModel::Pointer Clone() const override
   {
      return Kratos::make_shared<NonlocalCamClayModel>(*this);
   }

   /// Destructor.
   ~NonlocalCamClayModel() override {}

   ///@}
   ///@name Operators
   ///@{

   ///@}
   ///@name Operations
   ///@{
   /**
          * Initialize member data
          */
   void InitializeModel(ModelDataType &rValues) override
   {
      KRATOS_TRY

      if (this->mInitialized == false)
      {
         PlasticDataType Variables;
         this->InitializeVariables(rValues, Variables);

         const ModelDataType &rModelData = Variables.GetModelData();
         const Properties &rMaterialProperties = rModelData.GetProperties();

         double ElasticModulus = rMaterialProperties[PRE_CONSOLIDATION_STRESS];
         ElasticModulus /= rMaterialProperties[SWELLING_SLOPE];

         MatrixType Stress;
         this->UpdateInternalVariables(rValues, Variables, Stress);

         this->mInitialized = true;

         mBulkModulus = ElasticModulus;
         mShearModulus = ElasticModulus;
      }
      this->mElasticityModel.InitializeModel(rValues);

      KRATOS_CATCH("")
   }

   //*******************************************************************************
   //*******************************************************************************
   // Calculate Stress and constitutive tensor
   void CalculateStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix) override
   {
      KRATOS_TRY

      double LocalCurrentPlasticStrain = this->mInternal(1);
      double NonLocalCurrentPlasticStrain = this->mInternal(4);
      this->mInternal(1) = this->mInternal(4);

      SoilBaseModel::CalculateStressAndConstitutiveTensors(rValues, rStressMatrix, rConstitutiveMatrix);

      if (rValues.State.Is(ConstitutiveModelData::UPDATE_INTERNAL_VARIABLES))
      {
         mInternal(1) = LocalCurrentPlasticStrain + (mInternal(1) - NonLocalCurrentPlasticStrain);
      }
      else
      {
         mInternal(1) = LocalCurrentPlasticStrain;
      }

      KRATOS_CATCH("")
   }

   /**
          * Check
          */
   int Check(const Properties &rProperties, const ProcessInfo &rCurrentProcessInfo) override
   {
      KRATOS_TRY

      //LMV: to be implemented. but should not enter in the base one

      return 0;

      KRATOS_CATCH("")
   }

   /**
       * Get required properties
       */
   void GetRequiredProperties(Properties &rProperties) override
   {
      KRATOS_TRY

      // Set required properties to check keys
      BaseType::GetRequiredProperties(rProperties);

      KRATOS_CATCH(" ")
   }
   ///@}
   ///@name Access
   ///@{

   /**
          * Has Values
          */
   bool Has(const Variable<double> &rVariable) override
   {
      if (rVariable == PLASTIC_STRAIN || rVariable == DELTA_PLASTIC_STRAIN)
         return true;

      return false;
   }

   /**
          * Get Values
          */
   double &GetValue(const Variable<double> &rVariable, double &rValue) override
   {

      rValue = 0;

      if (rVariable == M_MODULUS)
      {
         rValue = 1e10;
         if (this->mInitialized)
         {
            rValue = mBulkModulus + 4.0/3.0*mShearModulus;
         }
      }
      else if (rVariable == YOUNG_MODULUS)
      {
         rValue = 1e10;
         if (this->mInitialized)
         {
            rValue = 9.0*mShearModulus*mBulkModulus/(3.0*mBulkModulus+mShearModulus);
         }
      }
      else if (rVariable == SHEAR_MODULUS)
      {
         rValue = 1000.0; 
         if ( this->mInitialized) {
            rValue = mShearModulus;
         }
      }
      else if (rVariable == BULK_MODULUS)
      {
         rValue = 1000.0; 
         if ( this->mInitialized) {
              rValue = mBulkModulus;
         }
      }
      else if (rVariable == PLASTIC_STRAIN)
      {
         rValue = this->mInternal(0);
      }
      else if (rVariable == DELTA_PLASTIC_STRAIN)
      {
         rValue = this->mInternal(0) - this->mInternal(0,1);
      }
      else if (rVariable == PRE_CONSOLIDATION_STRESS)
      {
         rValue = this->mInternal(3);
      }
      else if (rVariable == NONLOCAL_PLASTIC_VOL_DEF) 
      {
         rValue = this->mInternal(4);
      }

      else
      {
         rValue = SoilBaseModel::GetValue(rVariable, rValue);
      }
      return rValue;
   }

   /**
          * Set Values
          */
   void SetValue(const Variable<double> &rVariable,
                 const double &rValue,
                 const ProcessInfo &rCurrentProcessInfo) override
   {
      KRATOS_TRY

      if (rVariable == NONLOCAL_PLASTIC_VOL_DEF)
      {
         this->mInternal(4) = rValue;
      } else {
         BaseType::SetValue(rVariable, rValue, rCurrentProcessInfo);
}

      KRATOS_CATCH("")
   }
   ///@}
   ///@name Inquiry
   ///@{

   ///@}
   ///@name Input and output
   ///@{

   /// Turn back information as a string.
   std::string Info() const override
   {
      std::stringstream buffer;
      buffer << "NonlocalCamClayModel";
      return buffer.str();
   }

   /// Print information about this object.
   void PrintInfo(std::ostream &rOStream) const override
   {
      rOStream << "NonlocalCamClayModel";
   }

   /// Print object's data.
   void PrintData(std::ostream &rOStream) const override
   {
      rOStream << "NonlocalCamClayModel Data";
   }

   ///@}
   ///@name Friends
   ///@{

   ///@}

protected:
   ///@name Protected static Member Variables
   ///@{

   ///@}
   ///@name Protected member Variables
   ///@{

   double mShearModulus;
   double mBulkModulus;

   ///@}
   ///@name Protected Operators
   ///@{

   ///@}
   ///@name Protected Operations
   ///@{

   ///@}
   ///@name Protected  Access
   ///@{

   ///@}
   ///@name Protected Inquiry
   ///@{

   ///@}
   ///@name Protected LifeCycle
   ///@{

   ///@}

private:
   ///@name Static Member Variables
   ///@{

   ///@}
   ///@name Member Variables
   ///@{

   ///@}
   ///@name Private Operators
   ///@{

   ///@}
   ///@name Private Operations
   ///@{

   ///@}
   ///@name Private  Access
   ///@{

   ///@}
   ///@name Private Inquiry
   ///@{

   ///@}
   ///@name Serialization
   ///@{
   friend class Serializer;

   void save(Serializer &rSerializer) const override
   {
      KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BaseType)
      rSerializer.save("BulkModulus", mBulkModulus);
      rSerializer.save("ShearModulus", mShearModulus);
   }

   void load(Serializer &rSerializer) override
   {
      KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BaseType)
      rSerializer.load("BulkModulus", mBulkModulus);
      rSerializer.load("ShearModulus", mShearModulus);
   }

   ///@}
   ///@name Un accessible methods
   ///@{

   ///@}

}; // Class NonlocalCamClayModel

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_CAM_CLAY_MODEL_HPP_INCLUDED  defined
