//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:                January 2022 $
//
//

#if !defined(KRATOS_UNSAT_CASM_MM_SOIL_MODEL_HPP_INCLUDED)
#define KRATOS_UNSAT_CASM_MM_SOIL_MODEL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/unsaturated_casm_base_soil_model.hpp"
#include "custom_models/plasticity_models/hardening_rules/unsaturated_casm_hardening_rule.hpp"
#include "custom_models/plasticity_models/yield_surfaces/unsaturated_casm_yield_surface.hpp"
#include "custom_models/elasticity_models/borja_model.hpp"

#include "custom_models/plasticity_models/yield_surfaces/plastic_potential/mm_plastic_potential.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
    */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) UnsaturatedCasmMMSoilModel : public UnsaturatedCasmBaseSoilModel<BorjaModel, UnsaturatedCasmYieldSurface<UnsaturatedCasmHardeningRule>>
{
public:
   ///@name Type Definitions
   ///@{

   //elasticity model
   typedef BorjaModel ElasticityModelType;
   typedef ElasticityModelType::Pointer ElasticityModelPointer;

   //yield surface
   typedef UnsaturatedCasmHardeningRule HardeningRuleType;
   typedef UnsaturatedCasmYieldSurface<HardeningRuleType> YieldSurfaceType;
   typedef YieldSurfaceType::Pointer YieldSurfacePointer;

   //base type
   typedef UnsaturatedCasmBaseSoilModel<ElasticityModelType, YieldSurfaceType> BaseType;

   //common types
   typedef BaseType::Pointer BaseTypePointer;
   typedef BaseType::SizeType SizeType;
   typedef BaseType::VoigtIndexType VoigtIndexType;
   typedef BaseType::MatrixType MatrixType;
   typedef BaseType::ModelDataType ModelDataType;
   typedef BaseType::MaterialDataType MaterialDataType;
   typedef BaseType::PlasticDataType PlasticDataType;
   typedef BaseType::InternalVariablesType InternalVariablesType;

   /// Pointer definition of UnsaturatedCasmMMSoilModel
   KRATOS_CLASS_POINTER_DEFINITION(UnsaturatedCasmMMSoilModel);

   ///@}
   ///@name Life Cycle
   ///@{

   /// Default constructor.
   UnsaturatedCasmMMSoilModel() : BaseType()
   {
      MMPlasticPotential<UnsaturatedCasmHardeningRule> Object;
      YieldSurface<UnsaturatedCasmHardeningRule>::Pointer pPlasticPotential = Object.Clone();
      mYieldSurface = UnsaturatedCasmYieldSurface<UnsaturatedCasmHardeningRule>(pPlasticPotential);
   }

   /// Copy constructor.
   UnsaturatedCasmMMSoilModel(UnsaturatedCasmMMSoilModel const &rOther) : BaseType(rOther) {}

   /// Assignment operator.
   UnsaturatedCasmMMSoilModel &operator=(UnsaturatedCasmMMSoilModel const &rOther)
   {
      BaseType::operator=(rOther);
      return *this;
   }

   /// Clone.
   ConstitutiveModel::Pointer Clone() const override
   {
      return Kratos::make_shared<UnsaturatedCasmMMSoilModel>(*this);
   }

   /// Destructor.
   ~UnsaturatedCasmMMSoilModel() override {}

   ///@}
   ///@name Operators
   ///@{

   ///@}
   ///@name Operations
   ///@{

   /**
          * Check
          */
   int Check(const Properties &rProperties, const ProcessInfo &rCurrentProcessInfo) override
   {
      KRATOS_TRY

      //LMV: to be implemented. but should not enter in the base one

      return 0;

      KRATOS_CATCH("")
   }

   /**
       * Get required properties
       */
   void GetRequiredProperties(Properties &rProperties) override
   {
      KRATOS_TRY

      // Set required properties to check keys
      UnsaturatedCasmBaseSoilModel<ElasticityModelType, YieldSurfaceType>::GetRequiredProperties(rProperties);

      KRATOS_CATCH(" ")
   }

   ///@}
   ///@name Access
   ///@{

   /**
          * Has Values
          */
   bool Has(const Variable<double> &rVariable) override
   {
      if (rVariable == PLASTIC_STRAIN || rVariable == DELTA_PLASTIC_STRAIN)
         return true;

      return false;
   }

   /**
          * Set Values
          */
   void SetValue(const Variable<double> &rVariable,
                 const double &rValue,
                 const ProcessInfo &rCurrentProcessInfo) override
   {
      KRATOS_TRY

      if (rVariable == NONLOCAL_PLASTIC_VOL_DEF)
      {
         mInternal(4) = rValue;
      }
      else
      {
         BaseType::SetValue(rVariable, rValue, rCurrentProcessInfo);
      }

      KRATOS_CATCH("")
   }

   /**
          * Get Values
          */
   double &GetValue(const Variable<double> &rVariable, double &rValue) override
   {

      KRATOS_TRY

      rValue = 0;

      if (rVariable == PLASTIC_VOL_DEF)
      {
         rValue = this->mInternal(1);
      }
      else
      {
         rValue = BaseType::GetValue(rVariable, rValue);
      }
      return rValue;

      KRATOS_CATCH("")
   }

   ///@}
   ///@name Inquiry
   ///@{

   ///@}
   ///@name Input and output
   ///@{

   /// Turn back information as a string.
   std::string Info() const override
   {
      std::stringstream buffer;
      buffer << "UnsaturatedCasmMMSoilModel";
      return buffer.str();
   }

   /// Print information about this object.
   void PrintInfo(std::ostream &rOStream) const override
   {
      rOStream << "UnsaturatedCasmMMSoilModel";
   }

   /// Print object's data.
   void PrintData(std::ostream &rOStream) const override
   {
      rOStream << "UnsaturatedCasmMMSoilModel Data";
   }

   ///@}
   ///@name Friends
   ///@{

   ///@}

protected:
   ///@name Protected static Member Variables
   ///@{

   ///@}
   ///@name Protected member Variables
   ///@{

   ///@}
   ///@name Protected Operators
   ///@{

   ///@}
   ///@name Protected Operations
   ///@{

   ///@}
   ///@name Protected  Access
   ///@{

   ///@}
   ///@name Protected Inquiry
   ///@{

   ///@}
   ///@name Protected LifeCycle
   ///@{

   ///@}

private:
   ///@name Static Member Variables
   ///@{

   ///@}
   ///@name Member Variables
   ///@{

   ///@}
   ///@name Private Operators
   ///@{

   ///@}
   ///@name Private Operations
   ///@{

   ///@}
   ///@name Private  Access
   ///@{

   ///@}
   ///@name Private Inquiry
   ///@{

   ///@}
   ///@name Serialization
   ///@{
   friend class Serializer;

   void save(Serializer &rSerializer) const override
   {
      KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BaseType)
   }

   void load(Serializer &rSerializer) override
   {
      KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BaseType)
   }

   ///@}
   ///@name Un accessible methods
   ///@{

   ///@}

}; // Class UnsaturatedCasmMMSoilModel

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_UNSAT_CASM_MM_SOIL_MODEL_HPP_INCLUDED  defined
