//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                  April 2017 $
//
//

#if !defined(KRATOS_DAMAGE_MODEL_HPP_INCLUDED)
#define KRATOS_DAMAGE_MODEL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/plasticity_model.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
template <class TElasticityModel, class TYieldSurface>
class DamageModel : public PlasticityModel<TElasticityModel, TYieldSurface>
{
public:
  ///@name Type Definitions
  ///@{

  //elasticity model
  typedef TElasticityModel ElasticityModelType;

  //yield surface
  typedef TYieldSurface YieldSurfaceType;

  //base type
  typedef PlasticityModel<ElasticityModelType, YieldSurfaceType> BaseType;

  //common types
  typedef typename BaseType::Pointer BaseTypePointer;
  typedef typename BaseType::SizeType SizeType;
  typedef typename BaseType::VoigtIndexType VoigtIndexType;
  typedef typename BaseType::VectorType VectorType;
  typedef typename BaseType::MatrixType MatrixType;
  typedef typename BaseType::ModelDataType ModelDataType;
  typedef typename BaseType::MaterialDataType MaterialDataType;
  typedef typename BaseType::PlasticDataType PlasticDataType;
  typedef typename BaseType::InternalVariablesType InternalVariablesType;

  typedef typename BaseType::NameType NameType;

  typedef ConstitutiveModelData::StrainMeasureType StrainMeasureType;
  typedef ConstitutiveModelData::StressMeasureType StressMeasureType;

  /// Pointer definition of DamageModel
  KRATOS_CLASS_POINTER_DEFINITION(DamageModel);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  DamageModel() : BaseType() {}

  /// Copy constructor.
  DamageModel(DamageModel const &rOther) : BaseType(rOther), mInternal(rOther.mInternal) {}

  /// Assignment operator.
  DamageModel &operator=(DamageModel const &rOther)
  {
    BaseType::operator=(rOther);
    mInternal = rOther.mInternal;
    return *this;
  }

  /// Clone.
  ConstitutiveModel::Pointer Clone() const override
  {
    return Kratos::make_shared<DamageModel>(*this);
  }

  /// Destructor.
  ~DamageModel() override {}

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /**
     * Initialize member data
     */
  void InitializeMaterial(const Properties &rProperties) override
  {
    KRATOS_TRY

    double &rDamageThreshold = mInternal(NameType::DAMAGE_STATE);

    //damage threshold properties
    rDamageThreshold = rProperties[DAMAGE_THRESHOLD];

    KRATOS_CATCH(" ")
  }

  /**
     * Initialize member data
     */
  void InitializeModel(ModelDataType &rValues) override
  {
    KRATOS_TRY

    BaseType::InitializeModel(rValues);

    KRATOS_CATCH(" ")
  }

  /**
     * Calculate Stresses
     */

  void CalculateStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix) override
  {
    KRATOS_TRY

    PlasticDataType Variables;
    this->InitializeVariables(rValues, Variables);

    // calculate elastic stress
    this->mElasticityModel.CalculateStressTensor(rValues, rStressMatrix);

    rValues.StressMatrix = rStressMatrix; //store stress to ModelData StressMatrix

    // calculate damaged stress
    this->CalculateAndAddStressTensor(Variables, rStressMatrix);

    // set requested internal variables
    this->SetInternalVariables(rValues, Variables);

    if (rValues.State.Is(ConstitutiveModelData::UPDATE_INTERNAL_VARIABLES))
      this->UpdateInternalVariables(rValues, Variables, rStressMatrix);

    KRATOS_CATCH(" ")
  }

  /**
     * Calculate Constitutive Tensor
     */
  void CalculateConstitutiveTensor(ModelDataType &rValues, Matrix &rConstitutiveMatrix) override
  {
    KRATOS_TRY

    //Initialize ConstitutiveMatrix
    rConstitutiveMatrix.clear();

    PlasticDataType Variables;
    this->InitializeVariables(rValues, Variables);

    //Calculate Stress Matrix

    MatrixType StressMatrix;
    // calculate elastic stress
    this->mElasticityModel.CalculateStressTensor(rValues, StressMatrix);

    rValues.StressMatrix = StressMatrix; //store stress to ModelData StressMatrix

    // calculate damaged stress
    this->CalculateAndAddStressTensor(Variables, StressMatrix);

    //Calculate Constitutive Matrix

    // calculate elastic constitutive tensor
    this->mElasticityModel.CalculateConstitutiveTensor(rValues, rConstitutiveMatrix);

    // calculate plastic constitutive tensor
    if (Variables.State().Is(ConstitutiveModelData::PLASTIC_REGION))
      this->CalculateAndAddPlasticConstitutiveTensor(Variables, rConstitutiveMatrix);

    Variables.State().Set(ConstitutiveModelData::CONSTITUTIVE_MATRIX_COMPUTED, true);

    KRATOS_CATCH(" ")
  }

  /**
     * Calculate Stress and Constitutive Tensor
     */
  void CalculateStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix) override
  {
    KRATOS_TRY

    PlasticDataType Variables;
    this->InitializeVariables(rValues, Variables);

    //Calculate Stress Matrix

    // calculate elastic stress
    this->mElasticityModel.CalculateStressTensor(rValues, rStressMatrix);

    rValues.StressMatrix = rStressMatrix; //store stress to ModelData StressMatrix

    // calculate damaged stress
    this->CalculateAndAddStressTensor(Variables, rStressMatrix);

    //Calculate Constitutive Matrix

    // calculate elastic constitutive tensor
    this->mElasticityModel.CalculateConstitutiveTensor(rValues, rConstitutiveMatrix);

    // calculate plastic constitutive tensor
    if (Variables.State().Is(ConstitutiveModelData::PLASTIC_REGION))
    {
      this->CalculateAndAddPlasticConstitutiveTensor(Variables, rConstitutiveMatrix);
    }

    // set requested internal variables
    this->SetInternalVariables(rValues, Variables);

    Variables.State().Set(ConstitutiveModelData::CONSTITUTIVE_MATRIX_COMPUTED, true);

    if (rValues.State.Is(ConstitutiveModelData::UPDATE_INTERNAL_VARIABLES))
      this->UpdateInternalVariables(rValues, Variables, rStressMatrix);

    KRATOS_CATCH(" ")
  }

  // calculate requested internal variables
  void CalculateInternalVariables(ModelDataType &rValues) override
  {
    KRATOS_TRY

    PlasticDataType Variables;
    this->InitializeVariables(rValues, Variables);

    // calculate elastic stress
    this->mElasticityModel.CalculateStressTensor(rValues, rValues.StressMatrix);

    // calculate damaged stress
    this->CalculateAndAddStressTensor(Variables, rValues.StressMatrix);

    // set requested internal variables
    this->SetInternalVariables(rValues, Variables);

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{

  /**
     * Has Values
     */
  bool Has(const Variable<double> &rVariable) override { return false; }

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "DamageModel";
    return buffer.str();
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "DamageModel";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "DamageModel Data";
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  // internal variables:
  InternalVariablesType mInternal;

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  /**
     * Calculate Stresses
     */
  virtual void CalculateAndAddStressTensor(PlasticDataType &rVariables, MatrixType &rStressMatrix)
  {
    KRATOS_TRY

    double &rDamageThreshold = rVariables.Data.Internal[NameType::DAMAGE_STATE];

    //1.- Stress norm and Strengh factor for damage (StressNorm and RateFactor variables used)
    this->CalculateStressNorm(rVariables, rStressMatrix);

    //2.-Check yield condition
    rVariables.Data.TrialStateFunction = this->mYieldSurface.CalculateYieldCondition(rVariables, rVariables.Data.TrialStateFunction);

    if (rVariables.State().Is(ConstitutiveModelData::IMPLEX_ACTIVE))
    {
      //3.- Calculate the implex radial return
      this->CalculateImplexReturnMapping(rVariables, rStressMatrix);
    }
    else
    {

      if (rVariables.Data.TrialStateFunction < rDamageThreshold)
        {
          rVariables.State().Set(ConstitutiveModelData::PLASTIC_REGION, false);
        }
      else
        {

          //3.- Calculate the radial return
          bool converged = this->CalculateReturnMapping(rVariables, rStressMatrix);

          if (!converged)
            std::cout << " ConstitutiveLaw did not converge " << std::endl;

          //4.- Update back stress, plastic strain and stress
          this->UpdateStressConfiguration(rVariables, rStressMatrix);

          rVariables.State().Set(ConstitutiveModelData::PLASTIC_REGION, true);
        }
    }

    rVariables.State().Set(ConstitutiveModelData::RETURN_MAPPING_COMPUTED, true);

    KRATOS_CATCH(" ")
  }

  /**
     * Calculate Constitutive Tensor
     */
  virtual void CalculateAndAddPlasticConstitutiveTensor(PlasticDataType &rVariables, Matrix &rConstitutiveMatrix)
  {
    KRATOS_TRY

    //Compute radial return check
    if (rVariables.State().IsNot(ConstitutiveModelData::RETURN_MAPPING_COMPUTED))
      KRATOS_ERROR << "ReturnMapping has to be computed to perform the calculation" << std::endl;

    const SizeType &VoigtSize = rVariables.GetModelData().GetVoigtSize();

    //double& rDamage          = rVariables.Data.Internal(NameType::DAMAGE);
    //alternative way, compute damage if not computed
    //double rDamage = 0;
    //rDamage = this->mYieldSurface.CalculateStateFunction( rVariables, rDamage );

    double DeltaStateFunction = 0;
    DeltaStateFunction = this->mYieldSurface.CalculateDeltaStateFunction(rVariables, DeltaStateFunction);

    //tangent OPTION 1:

    // double& rDamageThreshold = rVariables.Data.Internal(NameType::DAMAGE_STATE);
    // const MatrixType& rStrainMatrix  = rVariables.Data.GetStrainMatrix();

    // VectorType StrainVector;
    // ConstitutiveModelUtilities::StrainTensorToVector(rStrainMatrix, StrainVector);

    // VectorType EffectiveStressVector;
    // noalias(EffectiveStressVector) = prod(rConstitutiveMatrix,StrainVector);

    // rConstitutiveMatrix *= (1-rDamage);
    // rConstitutiveMatrix += DeltaStateFunction * rDamageThreshold * outer_prod(EffectiveStressVector,EffectiveStressVector);

    //alternative tangent OPTION 2:

    const ModelDataType &rModelData = rVariables.GetModelData();
    const MatrixType &rStressMatrix = rModelData.GetStressMatrix();

    Vector EffectiveStressVector(VoigtSize);
    EffectiveStressVector = ConstitutiveModelUtilities::StressTensorToVector(rStressMatrix, EffectiveStressVector);

    Vector EquivalentStrainVector(VoigtSize);
    EquivalentStrainVector = this->CalculateEquivalentStrainDerivative(rVariables, rConstitutiveMatrix, EquivalentStrainVector);

    rVariables.State().Set(ConstitutiveModelData::CONSTITUTIVE_MATRIX_COMPUTED, true);

    KRATOS_CATCH(" ")
  }

  // calculate return mapping

  virtual bool CalculateReturnMapping(PlasticDataType &rVariables, MatrixType &rStressMatrix)
  {
    KRATOS_TRY

    double &rDamageThreshold = rVariables.Data.Internal[NameType::DAMAGE_STATE];
    double &rDamage = rVariables.Data.Internal[NameType::DAMAGE];

    double &StateFunction = rVariables.Data.TrialStateFunction;

    if (StateFunction >= rDamageThreshold)
    {
      rDamageThreshold = StateFunction;

      //Calculate State Function: (damage)
      StateFunction = this->mYieldSurface.CalculateStateFunction(rVariables, StateFunction);

      rDamage = StateFunction;
    }

    return true;

    KRATOS_CATCH(" ")
  }

  // implex protected methods

  virtual void CalculateImplexReturnMapping(PlasticDataType &rVariables, MatrixType &rStressMatrix)
  {
    KRATOS_TRY

    //double &rDamageOld = rVariables.Data.Internal(NameType::DAMAGE);
    double &rDamageOld = this->mInternal(NameType::DAMAGE,1);
    double &rDamage = rVariables.Data.Internal[NameType::DAMAGE];

    rDamage *= 2.0;
    rDamage -= rDamageOld;

    //2.- Update back stress, plastic strain and stress
    this->UpdateStressConfiguration(rVariables, rStressMatrix);

    KRATOS_CATCH(" ")
  }

  // auxiliar methods

  void InitializeVariables(ModelDataType &rValues, PlasticDataType &rVariables) override
  {
    KRATOS_TRY

    //set model data pointer
    rVariables.SetModelData(rValues);

    rValues.State.Set(ConstitutiveModelData::PLASTIC_REGION, false);

    // rValues.State.Set(ConstitutiveModelData::IMPLEX_ACTIVE, false);
    // if (rValues.GetProcessInfo()[IMPLEX] == 1)
    //   rValues.State.Set(ConstitutiveModelData::IMPLEX_ACTIVE, true);
    rValues.State.Set(ConstitutiveModelData::IMPLEX_ACTIVE, rValues.GetProcessInfo()[IMPLEX]);

    rVariables.SetState(rValues.State);

    // RateFactor
    rVariables.Data.RateFactor = 0;

    // Damage threshold variable [0] and damage variable [1]
    rVariables.Data.Internal = mInternal.Current();

    // Flow Rule local variables
    rVariables.Data.TrialStateFunction = 0;
    rVariables.Data.StressNorm = 0;

    // Set Strain
    rVariables.Data.StrainMatrix = rValues.StrainMatrix;

    KRATOS_CATCH(" ")
  }

  virtual void UpdateStressConfiguration(PlasticDataType &rVariables, MatrixType &rStressMatrix)
  {
    KRATOS_TRY

    double &rDamage = rVariables.Data.Internal[NameType::DAMAGE];

    //Stress Update:
    rStressMatrix *= (1.0 - rDamage);

    KRATOS_CATCH(" ")
  }

  virtual void UpdateInternalVariables(ModelDataType &rValues, PlasticDataType &rVariables, const MatrixType &rStressMatrix)
  {
    KRATOS_TRY

    //update mechanical variables
    mInternal.Update(rVariables.Data.Internal);

    KRATOS_CATCH(" ")
  }

  // calculate stress norm

  void CalculateStressNorm(PlasticDataType &rVariables, MatrixType &rStressMatrix)
  {
    KRATOS_TRY

    // Compute strenght type parameter
    rVariables.Data.RateFactor = 0.0;

    const SizeType &VoigtSize = rVariables.GetModelData().GetVoigtSize();

    Vector PrincipalStresses;
    if (VoigtSize == 3)
    {
      PrincipalStresses.resize(2);
      PrincipalStresses[0] = 0.5 * (rStressMatrix(0, 0) + rStressMatrix(1, 1)) +
                             sqrt(0.25 * (rStressMatrix(0, 0) - rStressMatrix(1, 1)) * (rStressMatrix(0, 0) - rStressMatrix(1, 1)) +
                                  rStressMatrix(0, 1) * rStressMatrix(0, 1));
      PrincipalStresses[1] = 0.5 * (rStressMatrix(0, 0) + rStressMatrix(1, 1)) -
                             sqrt(0.25 * (rStressMatrix(0, 0) - rStressMatrix(1, 1)) * (rStressMatrix(0, 0) - rStressMatrix(1, 1)) +
                                  rStressMatrix(0, 1) * rStressMatrix(0, 1));
    }
    else
    {
      PrincipalStresses.resize(3);
      noalias(PrincipalStresses) = ConstitutiveModelUtilities::EigenValuesDirectMethod(rStressMatrix);
    }

    double Macaulay_PrincipalStress = 0.0;
    double Absolute_PrincipalStress = 0.0;

    for (unsigned int i = 0; i < PrincipalStresses.size(); i++)
    {
      if (PrincipalStresses[i] > 0.0)
      {
        Macaulay_PrincipalStress += PrincipalStresses[i];
        Absolute_PrincipalStress += PrincipalStresses[i];
      }
      else
      {
        Absolute_PrincipalStress -= PrincipalStresses[i];
      }
    }

    if (Absolute_PrincipalStress > 1.0e-20)
    {
      rVariables.Data.RateFactor = Macaulay_PrincipalStress / Absolute_PrincipalStress;
    }
    else
    {
      rVariables.Data.RateFactor = 0.5;
    }

    // Compute Equivalent Strain (rYieldCondition)
    const Matrix &rStrainMatrix = rVariables.Data.GetStrainMatrix();
    MatrixType Auxiliar;
    noalias(Auxiliar) = prod(rStrainMatrix, rStressMatrix);

    rVariables.Data.StressNorm = 0.0;

    for (unsigned int i = 0; i < 3; i++)
    {
      rVariables.Data.StressNorm += Auxiliar(i, i);
    }

    KRATOS_CATCH(" ")
  }

  // calculate equivalent strain derivative

  Vector &CalculateEquivalentStrainDerivative(PlasticDataType &rVariables, const Matrix &rConstitutiveMatrix, Vector &rEquivalentStrainDerivative)
  {
    KRATOS_TRY

    //The derivative of the equivalent strain with respect to the strain vector is obtained through the perturbation method

    const SizeType &VoigtSize = rVariables.GetModelData().GetVoigtSize();

    Vector StressVector(VoigtSize);
    MatrixType StressMatrix;
    double EquivalentStrainForward = 0.0;
    double EquivalentStrainBackward = 0.0;

    //Compute the strains perturbations in each direction of the vector
    const MatrixType &rStrainMatrix = rVariables.Data.GetStrainMatrix();
    Vector StrainVector(VoigtSize);
    StrainVector = ConstitutiveModelUtilities::StrainTensorToVector(rStrainMatrix, StrainVector);
    Vector PerturbatedStrainVector;
    ConstitutiveModelUtilities::ComputePerturbationVector(PerturbatedStrainVector, StrainVector);

    for (unsigned int i = 0; i < VoigtSize; i++)
    {
      //Forward perturbed equivalent strain
      StrainVector[i] += PerturbatedStrainVector[i];

      rVariables.Data.StrainMatrix = ConstitutiveModelUtilities::StrainVectorToTensor(StrainVector, rVariables.Data.StrainMatrix);
      noalias(StressVector) = prod(rConstitutiveMatrix, StrainVector);
      StressMatrix = ConstitutiveModelUtilities::StressVectorToTensor(StressVector, StressMatrix);

      this->CalculateStressNorm(rVariables, StressMatrix);
      EquivalentStrainForward = this->mYieldSurface.CalculateYieldCondition(rVariables, EquivalentStrainForward);

      StrainVector[i] -= PerturbatedStrainVector[i];

      //Backward perturbed equivalent strain
      StrainVector[i] -= PerturbatedStrainVector[i];

      rVariables.Data.StrainMatrix = ConstitutiveModelUtilities::StrainVectorToTensor(StrainVector, rVariables.Data.StrainMatrix);
      noalias(StressVector) = prod(rConstitutiveMatrix, StrainVector);
      StressMatrix = ConstitutiveModelUtilities::StressVectorToTensor(StressVector, StressMatrix);

      this->CalculateStressNorm(rVariables, StressMatrix);
      EquivalentStrainBackward = this->mYieldSurface.CalculateYieldCondition(rVariables, EquivalentStrainBackward);

      StrainVector[i] += PerturbatedStrainVector[i];

      rEquivalentStrainDerivative[i] = (EquivalentStrainForward - EquivalentStrainBackward) / (2.0 * PerturbatedStrainVector[i]);
    }

    return rEquivalentStrainDerivative;

    KRATOS_CATCH(" ")
  }

  // set requested internal variables
  void SetInternalVariables(ModelDataType &rValues, PlasticDataType &rVariables) override
  {
    KRATOS_TRY

    //supply internal variable
    rValues.InternalVariable.SetValue(DAMAGE_VARIABLE, rVariables.Data.Internal[NameType::DAMAGE]);

    KRATOS_CATCH(" ")
  }

  /**
     * Get required properties
     */
  void GetRequiredProperties(Properties &rProperties) override
  {
    KRATOS_TRY

    // Set required properties
    double value = 0.0; //dummy
    rProperties.SetValue(DAMAGE_THRESHOLD,value);

    BaseType::GetRequiredProperties(rProperties);

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  using ConstitutiveModel::CalculateAndAddStressTensor;

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BaseType)
    rSerializer.save("InternalVariables", mInternal);
  }

  void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BaseType)
    rSerializer.load("InternalVariables", mInternal);
  }

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class DamageModel

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_DAMAGE_MODEL_HPP_INCLUDED  defined
