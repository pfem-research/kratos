#include "custom_models/plasticity_models/unsaturated_casm_base_soil_model.hpp"
#include "custom_models/plasticity_models/hardening_rules/unsaturated_casm_hardening_rule.hpp"
#include "custom_models/plasticity_models/yield_surfaces/unsaturated_casm_yield_surface.hpp"
#include "custom_models/elasticity_models/borja_model.hpp"
#include "custom_models/elasticity_models/dv2_borja_model.hpp"
#include "custom_utilities/stress_invariants_utilities.hpp"


// VERY IMPORTANT: Suction is always positive: i.e. suction = max( waterPressure, 0.0) this way I don't have to check it everytime in the loading collapse...


namespace Kratos
{

   template<typename TElasticityModel, typename TYieldSurface>
   double & UnsaturatedCasmBaseSoilModel<TElasticityModel, TYieldSurface>::GetValue(const Variable<double> &rVariable, double &rValue)
    {
      KRATOS_TRY

        if (rVariable == PLASTIC_VOL_DEF)
        {
          rValue = this->mInternal(1);
        }
        else if (rVariable == PLASTIC_DEV_DEF)
        {
          rValue = this->mInternal(2);
        }
        else if (rVariable == NONLOCAL_PLASTIC_VOL_DEF)
        {
          rValue = this->mInternal(3);
        }
        else if (rVariable == PRE_CONSOLIDATION_STRESS)
        {
          rValue = this->mInternal(5);
        }
        else if (rVariable == PRE_CONSOLIDATION_STRESS_SAT)
        {
          rValue = this->mInternal(6);
        } else if ( rVariable == DELTA_GAMMA_MECHANIC) {
           rValue = this->mInternal(7) - this->mInternal(7,1);
        } else if ( rVariable == DELTA_GAMMA_SUCTION) {
           rValue = this->mInternal(8) - this->mInternal(8,1);
        } else if (rVariable == HENCKY_ELASTIC_VOLUMETRIC_STRAIN) {
           rValue = this->mInternal(10);
        } else if (rVariable == NONLOCAL_HENCKY_ELASTIC_VOLUMETRIC_STRAIN) {
           rValue = this->mInternal(11);
        } else if (rVariable == M_MODULUS) {
           rValue = 1e4;
           if (this->mInitialized)
              rValue = mBulkModulus + 4.0/3.0*mShearModulus;
        } else if (rVariable == YOUNG_MODULUS) {
           rValue = 1e4;
           if (this->mInitialized)
              rValue = 9.0*mShearModulus*mBulkModulus/(3.0*mBulkModulus+mShearModulus);
        } else if (rVariable == SHEAR_MODULUS) {
           rValue = 1e4;
           if (this->mInitialized)
              rValue = mShearModulus;
        } else if (rVariable == BULK_MODULUS){
           rValue = 1e4;
           if (this->mInitialized)
              rValue = mBulkModulus;
        } else if ( rVariable == PLASTIC_STRAIN) {
           rValue = this->mInternal(0);
        } else if ( rVariable == DELTA_PLASTIC_STRAIN) {
           rValue = this->mInternal(0) - this->mInternal(0,1);
        } else {
           rValue = SoilBaseModel<TElasticityModel, TYieldSurface>::GetValue(rVariable, rValue);
        }

        return rValue;

      KRATOS_CATCH("")
        }

   template<typename TElasticityModel, typename TYieldSurface>
   array_1d<double, 3>& UnsaturatedCasmBaseSoilModel<TElasticityModel, TYieldSurface>::GetValue(const Variable< array_1d<double, 3> >& rVariable, array_1d<double, 3>& rValue)
    {
      KRATOS_TRY

      return rValue;

      KRATOS_CATCH("")
    }

   template<typename TElasticityModel, typename TYieldSurface>
   Vector & UnsaturatedCasmBaseSoilModel<TElasticityModel, TYieldSurface>::GetValue(const Variable<Vector> &rVariable, Vector &rValue)
    {
       KRATOS_TRY
      rValue = this->mElasticityModel.GetValue(rVariable, rValue);
      return rValue;
      KRATOS_CATCH("")
    }

   template<typename TElasticityModel, typename TYieldSurface>
   Matrix & UnsaturatedCasmBaseSoilModel<TElasticityModel, TYieldSurface>::GetValue(const Variable<Matrix> &rVariable, Matrix &rValue)
    {
      KRATOS_TRY

      rValue = this->mElasticityModel.GetValue(rVariable, rValue);
      return rValue;

      KRATOS_CATCH("")
    }

   template<typename TElasticityModel, typename TYieldSurface>
   void UnsaturatedCasmBaseSoilModel<TElasticityModel, TYieldSurface>::SetValue(const Variable<double> &rVariable, const double &rValue, const ProcessInfo &rCurrentProcessInfo)
    {

      KRATOS_TRY

      if ( rVariable == NONLOCAL_PLASTIC_VOL_DEF) {
         this->mInternal(3) = rValue;
      } else if ( rVariable == HENCKY_ELASTIC_VOLUMETRIC_STRAIN) {
         this->mInternal(10) = rValue; // dangerous
      } else if ( rVariable == NONLOCAL_HENCKY_ELASTIC_VOLUMETRIC_STRAIN) {
         this->mInternal(11) = rValue;
      } else {
         this->mElasticityModel.SetValue(rVariable, rValue, rCurrentProcessInfo);
      }

       KRATOS_CATCH("")
    }

   template<typename TElasticityModel, typename TYieldSurface>
   void UnsaturatedCasmBaseSoilModel<TElasticityModel, TYieldSurface>::SetValue(const Variable<Vector> &rVariable, const Vector &rValue, const ProcessInfo &rCurrentProcessInfo)
    {
      KRATOS_TRY

      this->mElasticityModel.SetValue(rVariable, rValue, rCurrentProcessInfo);

      KRATOS_CATCH("")
    }

   template<typename TElasticityModel, typename TYieldSurface>
   Matrix & UnsaturatedCasmBaseSoilModel<TElasticityModel, TYieldSurface>::SetConstitutiveMatrixToTheAppropriateSizeUnsat(Matrix & rConstitutiveMatrix, Matrix & rConstMatrixBig, Vector & rConstVector, const MatrixType & rStressMatrix)
   {
      KRATOS_TRY

      unsigned int thisSize = rConstitutiveMatrix.size1();
      rConstitutiveMatrix.resize(thisSize, thisSize+1); // dangerous??
      if ( thisSize+1 != rConstitutiveMatrix.size2()) {
         std::cout << " CONSTITUTIVE MATRIX " << rConstitutiveMatrix << std::endl;
         std::cout << " the sizes " << rConstitutiveMatrix.size1() << " and " << rConstitutiveMatrix.size2() << std::endl;
         KRATOS_ERROR << " The constitutive matrix does not have the appropriate size " << std::endl;
      }

      Matrix AuxiliarMatrix( thisSize, thisSize);
      noalias( AuxiliarMatrix ) = ZeroMatrix(thisSize, thisSize);

      AuxiliarMatrix = SoilBaseModel<TElasticityModel, TYieldSurface>::SetConstitutiveMatrixToTheAppropriateSize( AuxiliarMatrix, rConstMatrixBig, rStressMatrix);

      for (unsigned int i = 0; i < thisSize; i++) {
         for (unsigned int j = 0; j < thisSize; j++) {
            rConstitutiveMatrix(i,j) = AuxiliarMatrix(i,j);
         }
      }


      if ( thisSize == 3) {
         rConstitutiveMatrix(0,3) = rConstVector(0);
         rConstitutiveMatrix(1,3) = rConstVector(1);
         rConstitutiveMatrix(2,3) = rConstVector(3);
      } else if  ( thisSize == 4) {
         rConstitutiveMatrix(0,4) = rConstVector(0);
         rConstitutiveMatrix(1,4) = rConstVector(1);
         rConstitutiveMatrix(2,4) = rConstVector(2);
         rConstitutiveMatrix(3,4) = rConstVector(3);
      } else if (thisSize == 6) {
         for (unsigned int i = 0; i < thisSize; i++)
            rConstitutiveMatrix(i,6) = rConstVector(i);
      } else {
         KRATOS_ERROR << " Setting the appropriate size. Incorrect input " << std::endl;
      }

      return rConstitutiveMatrix;


      KRATOS_CATCH("")
   }

   template<typename TElasticityModel, typename TYieldSurface>
    double & UnsaturatedCasmBaseSoilModel<TElasticityModel, TYieldSurface>::EvaluateLoadingCollapse( PlasticDataType & rVariables, double & rPC, const double & rPCSat, const double & rSuction)
    {
       KRATOS_TRY

       const ModelDataType & rModelData = rVariables.GetModelData();
       const Properties & rMaterialProperties = rModelData.GetProperties();

       const double & rLambda = rMaterialProperties[NORMAL_COMPRESSION_SLOPE];
       const double & rKappa  = rMaterialProperties[SWELLING_SLOPE];
       const double & rR      = rMaterialProperties[R_BBM];
       const double & rBeta   = rMaterialProperties[BETA_BBM];
       const double & rP0     = rMaterialProperties[REFERENCE_PRESSURE];

       double lambdaS = rLambda * (  (1.0-rR)*exp( -rBeta*rSuction) + rR);

       rPC = pow(-rPCSat/rP0, (rLambda-rKappa)/(lambdaS-rKappa) );
       rPC *= -rP0;

       return rPC;

       KRATOS_CATCH("")
    }


   template<typename TElasticityModel, typename TYieldSurface>
   void UnsaturatedCasmBaseSoilModel<TElasticityModel, TYieldSurface>::CalculateImplexPlasticStepUnsat(ModelDataType &rValues, PlasticDataType &rVariables, MatrixType &rStressMatrix, const MatrixType &rDeltaDeformationMatrix, Vector & rConstitutiveVector)
    {
      KRATOS_TRY

      const double &rPlasticMultiplierOld = this->mInternal(NameType::PLASTIC_STRAIN, 1);
      const double &rPlasticMultiplier = rVariables.Data.Internal(0);
      double DeltaPlasticMultiplier = (rPlasticMultiplier - rPlasticMultiplierOld);

      double DeltaGammaM = rVariables.Data.Internal(7)-this->mInternal(7,1);


      const ModelDataType &rModelData = rVariables.GetModelData();
      const Properties &rMatProperties = rModelData.GetProperties();

      if (rMatProperties.Has(MAX_IMPLEX_PLASTIC_MULTIPLIER))
      {
        const double &rMaxPlasticMultiplier = rMatProperties[MAX_IMPLEX_PLASTIC_MULTIPLIER];
        if (rMaxPlasticMultiplier > 0)
        {
          if (DeltaPlasticMultiplier > 100.0* rMaxPlasticMultiplier)
          {
            DeltaPlasticMultiplier = rMaxPlasticMultiplier;
            std::cout << " IMPLEX: reducing plastic multiplier " << std::endl;
          }
        }
      }

      const ProcessInfo &rCurrentProcessInfo = rModelData.GetProcessInfo();
      const double &rCurrentDeltaTime = rCurrentProcessInfo[DELTA_TIME];
      const double &rPreviousDeltaTime = rCurrentProcessInfo.GetPreviousTimeStepInfo(1)[DELTA_TIME];
      if (rCurrentDeltaTime != rPreviousDeltaTime)
      {
        DeltaPlasticMultiplier *= rCurrentDeltaTime / rPreviousDeltaTime;
        DeltaGammaM *= rCurrentDeltaTime / rPreviousDeltaTime;
      }


      if ( DeltaPlasticMultiplier <= 0.0) {

         rValues.StrainMatrix = prod(rDeltaDeformationMatrix, rValues.StrainMatrix);
         rValues.StrainMatrix = prod(rValues.StrainMatrix, trans(rDeltaDeformationMatrix));

         this->mElasticityModel.CalculateStressTensor(rValues, rStressMatrix);
         return;
      } else {

         const double & rSuctionNew = rValues.GetSuctionNew();
         const double & rSuctionOld = rValues.GetSuctionOld();
         double DeltaSuction = rSuctionNew-rSuctionOld;

         this->mElasticityModel.CalculateStressTensor(rValues, rStressMatrix);
         Matrix ElasticMatrix(6, 6);
         noalias(ElasticMatrix) = ZeroMatrix(6, 6);
         this->mElasticityModel.CalculateConstitutiveTensor(rValues, ElasticMatrix);

         VectorType DeltaStressYieldCondition = this->mYieldSurface.CalculateDeltaStressYieldCondition(rVariables, DeltaStressYieldCondition);
         VectorType PlasticPotentialDerivative = this->mYieldSurface.CalculateDeltaPlasticPotential(rVariables, PlasticPotentialDerivative);
         double DeltaSuctionYieldCondition = this->mYieldSurface.CalculateDeltaSuctionYieldCondition(rVariables, rSuctionOld, DeltaSuctionYieldCondition);
         MatrixType PlasticPotDerTensor;
         PlasticPotDerTensor = ConstitutiveModelUtilities::StrainVectorToTensor(PlasticPotentialDerivative, PlasticPotDerTensor);
         double H = this->mYieldSurface.GetHardeningRule().CalculateDeltaHardeningUnsat(rVariables, H, PlasticPotDerTensor, rSuctionOld);
         double denominador = H + MathUtils<double>::Dot(DeltaStressYieldCondition, prod(ElasticMatrix, PlasticPotentialDerivative));
         double DeltaGammaS = -DeltaSuctionYieldCondition*DeltaSuction/denominador;

         double DeltaGammaTotal = DeltaGammaM + DeltaGammaS;
         MatrixType UpdateMatrix;
         this->ConvertHenckyVectorToCauchyGreenTensor(-DeltaGammaTotal * PlasticPotentialDerivative / 2.0, UpdateMatrix);
         UpdateMatrix = prod(rDeltaDeformationMatrix, UpdateMatrix);
         rValues.StrainMatrix = prod(UpdateMatrix, rValues.StrainMatrix);
         rValues.StrainMatrix = prod(rValues.StrainMatrix, trans(UpdateMatrix));

         /*std::cout << " PREVIOUS " << DeltaPlasticMultiplier << " mechanical " << DeltaGammaM << " suction " << DeltaGammaS << std::endl;
         std::cout << " totla " << DeltaGammaTotal << " and was " << rVariables.Data.Internal(8) -this->mInternal(8,1)<< std::endl;*/

         this->mElasticityModel.CalculateStressTensor(rValues, rStressMatrix);

         if ( rSuctionNew > 0.0) {
            rConstitutiveVector = -prod(ElasticMatrix, PlasticPotentialDerivative) * DeltaSuctionYieldCondition / denominador;
         }
      }

      KRATOS_CATCH("")
        }

   template<typename TElasticityModel, typename TYieldSurface>
   void UnsaturatedCasmBaseSoilModel<TElasticityModel, TYieldSurface>::InitializeVariables(ModelDataType &rValues, PlasticDataType &rVariables)
    {
      KRATOS_TRY

        //set model data pointer
        rVariables.SetModelData(rValues);

      rValues.State.Set(ConstitutiveModelData::PLASTIC_REGION, false);

      rValues.State.Set(ConstitutiveModelData::IMPLEX_ACTIVE, false);
      if (rValues.GetProcessInfo()[IMPLEX] == 1)
      {
        rValues.State.Set(ConstitutiveModelData::IMPLEX_ACTIVE, true);
      }

      rVariables.SetState(rValues.State);

      // RateFactor
      rVariables.Data.RateFactor = 0;

      // CurrentPlasticStrain
      rVariables.Data.Internal = this->mInternal.Current();

      // DeltaGamma / DeltaPlasticStrain (asociative plasticity)
      rVariables.Data.DeltaInternal.clear();

      // Flow Rule local variables
      rVariables.Data.TrialStateFunction = 0;
      rVariables.Data.StressNorm = 0;

      KRATOS_CATCH(" ")
    }

   template<typename TElasticityModel, typename TYieldSurface>
   void UnsaturatedCasmBaseSoilModel<TElasticityModel, TYieldSurface>::ComputeOneStepElastoPlasticProblemUnsat(ModelDataType &rValues, PlasticDataType &rVariables, const MatrixType &rDeltaDeformationMatrix, const double & rInitialSuction, const double & rDeltaSuction)
    {
      KRATOS_TRY

      const ModelDataType & rModelData = rVariables.GetModelData();
      const Properties & rMaterialProperties = rModelData.GetProperties();

      const double & rLambda = rMaterialProperties[NORMAL_COMPRESSION_SLOPE];
      const double & rKappa  = rMaterialProperties[SWELLING_SLOPE];
      const double &rFirstPreconsolidationPressure = rMaterialProperties[PRE_CONSOLIDATION_STRESS];

      //const double & rSuctionNew = rValues.GetSuctionNew();

      MatrixType StressMatrix;
      // evaluate constitutive matrix and plastic flow
      double &rPlasticMultiplier = rVariables.Data.Internal(0);
      double &rPlasticVolDef = rVariables.Data.Internal(1);
      double &rPlasticDevDef = rVariables.Data.Internal(2);
      double &rPlasticMultiplier_Mechanical = rVariables.Data.Internal(7);
      double &rPlasticMultiplier_Suction = rVariables.Data.Internal(8);

      double & rPreconsolidationSat = rVariables.Data.Internal(6);
      rPreconsolidationSat = -rFirstPreconsolidationPressure * (std::exp(-rPlasticVolDef / (rLambda - rKappa)));
      double & rPreconsolidation = rVariables.Data.Internal(5);
      rPreconsolidation = this->EvaluateLoadingCollapse( rVariables, rPreconsolidation, rPreconsolidationSat, rInitialSuction);

      Matrix ElasticMatrix(6, 6);
      noalias(ElasticMatrix) = ZeroMatrix(6, 6);
      this->mElasticityModel.CalculateConstitutiveTensor(rValues, ElasticMatrix);

      VectorType DeltaStressYieldCondition = this->mYieldSurface.CalculateDeltaStressYieldCondition(rVariables, DeltaStressYieldCondition);
      double DeltaSuctionYieldCondition = this->mYieldSurface.CalculateDeltaSuctionYieldCondition(rVariables, rInitialSuction, DeltaSuctionYieldCondition);
      VectorType PlasticPotentialDerivative = this->mYieldSurface.CalculateDeltaPlasticPotential(rVariables, PlasticPotentialDerivative);
      MatrixType PlasticPotDerTensor;
      PlasticPotDerTensor = ConstitutiveModelUtilities::StrainVectorToTensor(PlasticPotentialDerivative, PlasticPotDerTensor);
      double H = this->mYieldSurface.GetHardeningRule().CalculateDeltaHardeningUnsat(rVariables, H, PlasticPotDerTensor, rInitialSuction);

      MatrixType StrainMatrix = prod(rDeltaDeformationMatrix, trans(rDeltaDeformationMatrix));
      VectorType StrainVector;
      this->ConvertCauchyGreenTensorToHenckyVector(StrainMatrix, StrainVector);

      VectorType AuxVector;
      AuxVector = prod(ElasticMatrix, StrainVector);
      double DeltaGamma(0.0), DeltaGammaM(0.0), DeltaGammaS(0.0);
      DeltaGammaM = MathUtils<double>::Dot(AuxVector, DeltaStressYieldCondition);
      DeltaGammaS = DeltaSuctionYieldCondition * rDeltaSuction;

      double Denominador = H + MathUtils<double>::Dot(DeltaStressYieldCondition, prod(ElasticMatrix, PlasticPotentialDerivative));
      DeltaGammaM /= Denominador;
      DeltaGammaS /= Denominador;

      DeltaGamma = DeltaGammaM + DeltaGammaS;

      DeltaGamma = 0;
      if ( DeltaGammaM > 0.0)
         DeltaGamma += DeltaGammaM;
      if ( DeltaGammaS > 0.0)
         DeltaGamma += DeltaGammaS;

      if (DeltaGamma < 0) {
         DeltaGamma = 0.0;
      }

      MatrixType UpdateMatrix;
      this->ConvertHenckyVectorToCauchyGreenTensor(-DeltaGamma * PlasticPotentialDerivative / 2.0, UpdateMatrix);
      UpdateMatrix = prod(rDeltaDeformationMatrix, UpdateMatrix);

      rValues.StrainMatrix = prod(UpdateMatrix, rValues.StrainMatrix);
      rValues.StrainMatrix = prod(rValues.StrainMatrix, trans(UpdateMatrix));

      this->mElasticityModel.CalculateStressTensor(rValues, StressMatrix);

      rPlasticMultiplier += DeltaGamma;
      rPlasticMultiplier_Mechanical += DeltaGammaM;
      rPlasticMultiplier_Suction += DeltaGammaS;

      double VolPlasticIncr = 0.0;
      for (unsigned int i = 0; i < 3; i++)
        VolPlasticIncr += DeltaGamma * PlasticPotentialDerivative(i);
      rPlasticVolDef += VolPlasticIncr;

      double DevPlasticIncr = 0.0;
      for (unsigned int i = 0; i < 3; i++)
        DevPlasticIncr += pow(DeltaGamma * PlasticPotentialDerivative(i) - VolPlasticIncr / 3.0, 2.0);
      for (unsigned int i = 3; i < 6; i++)
        DevPlasticIncr += 2.0 * pow(DeltaGamma * PlasticPotentialDerivative(i) / 2.0, 2.0);
      DevPlasticIncr = sqrt(DevPlasticIncr);
      rPlasticDevDef += DevPlasticIncr;


      //double & rPreconsolidationSat = rVariables.Data.Internal(6);
      rPreconsolidationSat = -rFirstPreconsolidationPressure * (std::exp(-rPlasticVolDef / (rLambda - rKappa)));

      //ouble & rPreconsolidation = rVariables.Data.Internal(5);
      rPreconsolidation = this->EvaluateLoadingCollapse( rVariables, rPreconsolidation, rPreconsolidationSat, rInitialSuction+rDeltaSuction);

      //std::cout << " stress " << StressMatrix << " rP " << rPreconsolidation << " , " << rPreconsolidationSat << " H: " << H << " gamma " << DeltaGamma << " and " << VolPlasticIncr <<  " , " << DeltaGammaM << " and " << DeltaGammaS << " GRAD " << DeltaStressYieldCondition << " and " << DeltaSuctionYieldCondition << std::endl;
      KRATOS_CATCH("")
    }


    //***********************************************************************************
    //***********************************************************************************
    // Compute one elasto-plastic problem with two discretizations and then compute some
    // sort of error measure
   template<typename TElasticityModel, typename TYieldSurface>
    double UnsaturatedCasmBaseSoilModel<TElasticityModel, TYieldSurface>::ComputeElastoPlasticProblem(ModelDataType &rValues, PlasticDataType &rVariables, const MatrixType &rSubstepDeformationGradient, const double & rInitialSuction, const double & rDeltaSuction)
    {
      KRATOS_TRY

      MatrixType InitialStrain = rValues.StrainMatrix;
      MatrixType Stress1;
      MatrixType Stress2;
      InternalDataType InitialInternalVariables = rVariables.Data.Internal;

      // 1. Compute with one discretization
      this->mElasticityModel.CalculateStressTensor(rValues, Stress1);
      this->ComputeOneStepElastoPlasticProblemUnsat(rValues, rVariables, rSubstepDeformationGradient, rInitialSuction, rDeltaSuction);
      Stress1 = rValues.StressMatrix;

      // 2. Compute with nSteps steps
      unsigned int nSteps = 3;
      rValues.StrainMatrix = InitialStrain;
      rVariables.Data.Internal = InitialInternalVariables;
      this->mElasticityModel.CalculateStressTensor(rValues, Stress2);

      MatrixType IncrementalDefGradient;

      for (unsigned int i = 0; i < nSteps; i++)
      {
        double tBegin = double(i) / double(nSteps);
        double tEnd = double(i + 1) / double(nSteps);
        this->ComputeSubstepIncrementalDeformationGradient(rSubstepDeformationGradient, tBegin, tEnd, IncrementalDefGradient);
        this->ComputeOneStepElastoPlasticProblemUnsat(rValues, rVariables, IncrementalDefGradient, rInitialSuction+tBegin*rDeltaSuction, rDeltaSuction/double(nSteps) );
      }

      double ErrorMeasure = 0;
      double Denom = 0;
      for (unsigned int i = 0; i < 3; i++)
      {
        for (unsigned int j = 0; j < 3; j++)
        {
          ErrorMeasure += pow(Stress1(i, j) - rValues.StressMatrix(i, j), 2);
          Denom += pow(rValues.StressMatrix(i, j), 2);
        }
      }

      //std::cout << "                              " << ErrorMeasure << std::endl;
      if (fabs(Denom) > 1.0E-5)
        ErrorMeasure /= Denom;
      ErrorMeasure = sqrt(ErrorMeasure);

      return ErrorMeasure;

      KRATOS_CATCH("")
        }




   template<typename TElasticityModel, typename TYieldSurface>
    void UnsaturatedCasmBaseSoilModel<TElasticityModel, TYieldSurface>::ComputeSubsteppingElastoPlasticProblem(ModelDataType &rValues, PlasticDataType &rVariables, const MatrixType &rDeltaDeformationMatrix, const double & rInitialSuction, const double & rFinalSuction)
    {
      KRATOS_TRY

      double Tolerance = 1e-5;
      double TimeStep = 0.25;
      double MinTimeStep = 1.0e-4;
      double DoneTimeStep = 0.0;
      double MaxTimeStep = 0.5;

      MatrixType SubstepDeformationGradient;

      double ErrorMeasure;
      double DeltaSuction;
      double CurrentSuction;


      while (DoneTimeStep < 1.0)
      {

        MatrixType InitialStress = rValues.StrainMatrix;
        InternalDataType InitialInternalVariables = rVariables.Data.Internal;

        if (DoneTimeStep + TimeStep >= 1.0)
        {
          TimeStep = 1.0 - DoneTimeStep;
        }

        this->ComputeSubstepIncrementalDeformationGradient(rDeltaDeformationMatrix, DoneTimeStep, DoneTimeStep + TimeStep, SubstepDeformationGradient);

        CurrentSuction = rInitialSuction + DoneTimeStep*(rFinalSuction-rInitialSuction);
        DeltaSuction = TimeStep * ( rFinalSuction-rInitialSuction);

        ErrorMeasure = ComputeElastoPlasticProblem(rValues, rVariables, SubstepDeformationGradient, CurrentSuction, DeltaSuction);
        //std::cout << "                     TIME STEP " << DoneTimeStep << " " << TimeStep << " ERROR " << ErrorMeasure << std::endl;
        if (ErrorMeasure < Tolerance)
        {
          DoneTimeStep += TimeStep;
        }
        else if (TimeStep <= MinTimeStep)
        {
          if (ErrorMeasure > 50.0 * Tolerance)
          {
            if (rValues.State.Is(ConstitutiveModelData::UPDATE_INTERNAL_VARIABLES))
            {
              std::cout << " ExplicitStressIntegrationDidNotConvege: StressError: " << ErrorMeasure << std::endl;
            }
          }
          DoneTimeStep += TimeStep;
        }
        else
        {
          rValues.StrainMatrix = InitialStress;
          rVariables.Data.Internal = InitialInternalVariables;
        }


        TimeStep *= sqrt(Tolerance / (ErrorMeasure + 1e-8));
        TimeStep = std::max(TimeStep, MinTimeStep);
        TimeStep = std::min(TimeStep, MaxTimeStep);
      }

      KRATOS_CATCH("")
    }

   template<typename TElasticityModel, typename TYieldSurface>
   void UnsaturatedCasmBaseSoilModel<TElasticityModel, TYieldSurface>::ComputeSolutionWithChange(ModelDataType &rValues, PlasticDataType &rVariables, const MatrixType &rDeltaDeformationMatrix)
    {
      KRATOS_TRY


      const double & rSuctionNew = rValues.GetSuctionNew();
      const double & rSuctionOld = rValues.GetSuctionOld();

      //std::cout << " COMPUTE SOLUTION WITH CHANGE " << std::endl;
      double Tolerance = 1e-6;

      double InitialTime = 0;
      double EndTime = 1;
      double HalfTime;
      double InitialStateFunction(-1), EndStateFunction(1), HalfTimeStateFunction;
      double InitialSuction(rSuctionOld), EndSuction(rSuctionNew);
      double HalfTimeSuction;

      MatrixType HalfTimeDeformationGradient;
      MatrixType StressMatrix;
      MatrixType InitialLeftCauchyGreen = rValues.StrainMatrix;

      const double & rPreconsolidationSat = rVariables.Data.Internal(6);
      double & rPreconsolidation = rVariables.Data.Internal(5);

      for (unsigned int i = 0; i < 150; i++)
      {
        HalfTime = 0.5 * (InitialTime + EndTime);
        HalfTimeSuction = 0.5*(InitialSuction+EndSuction);
      

        this->ComputeSubstepIncrementalDeformationGradient(rDeltaDeformationMatrix, 0.0, HalfTime, HalfTimeDeformationGradient);
        MatrixType AuxMatrix;
        AuxMatrix = prod(InitialLeftCauchyGreen, trans(HalfTimeDeformationGradient));
        AuxMatrix = prod(HalfTimeDeformationGradient, AuxMatrix);
        rValues.StrainMatrix = AuxMatrix;

        this->mElasticityModel.CalculateStressTensor(rValues, StressMatrix);
        rPreconsolidation = this->EvaluateLoadingCollapse(rVariables, rPreconsolidation, rPreconsolidationSat, HalfTimeSuction);
        HalfTimeStateFunction = this->mYieldSurface.CalculateYieldCondition(rVariables, HalfTimeStateFunction);

        if (HalfTimeStateFunction < 0.0)
        {
          InitialStateFunction = HalfTimeStateFunction;
          InitialTime = HalfTime;
          InitialSuction = HalfTimeSuction;
        }
        else
        {
          EndStateFunction = HalfTimeStateFunction;
          EndTime = HalfTime;
          EndSuction = HalfTimeSuction;
        }

        double ErrorMeasure1 = fabs(InitialStateFunction - EndStateFunction);
        double ErrorMeasure2 = fabs(InitialTime - EndTime);

        /*std::cout << "         " << InitialTime << " , " << HalfTime << " , " << EndTime << std::endl;
        std::cout << "         " << InitialSuction << " , " << HalfTimeSuction << " , " << EndSuction << std::endl;
        std::cout << "                       " << ErrorMeasure1 << " and " << ErrorMeasure2 << std::endl;*/

        if ((ErrorMeasure1 < Tolerance) && (ErrorMeasure2 < Tolerance))
          break;
      }

      // continue with plasticity
      MatrixType RemainingDeformationGradient;
      this->ComputeSubstepIncrementalDeformationGradient(rDeltaDeformationMatrix, HalfTime, 1.0, RemainingDeformationGradient);

      this->ComputeSubsteppingElastoPlasticProblem(rValues, rVariables, RemainingDeformationGradient, HalfTimeSuction, EndSuction);

      KRATOS_CATCH("")
    }


   template<typename TElasticityModel, typename TYieldSurface>
   void UnsaturatedCasmBaseSoilModel<TElasticityModel, TYieldSurface>::ComputeElastoPlasticTangentMatrixAndVector(ModelDataType &rValues, PlasticDataType &rVariables, Matrix &rEPMatrix, Vector & rEPVector)
   {
      KRATOS_TRY

      const double & rSuctionNew = rValues.GetSuctionNew();

      // evaluate constitutive matrix and plastic flow
      Matrix ElasticMatrix(6, 6);
      noalias(ElasticMatrix) = ZeroMatrix(6, 6);
      this->mElasticityModel.CalculateConstitutiveTensor(rValues, ElasticMatrix);

      VectorType DeltaStressYieldCondition = this->mYieldSurface.CalculateDeltaStressYieldCondition(rVariables, DeltaStressYieldCondition);
      double DeltaSuctionYieldCondition = this->mYieldSurface.CalculateDeltaSuctionYieldCondition(rVariables, rSuctionNew, DeltaSuctionYieldCondition);
      VectorType PlasticPotentialDerivative = this->mYieldSurface.CalculateDeltaPlasticPotential(rVariables, PlasticPotentialDerivative);

      MatrixType PlasticPotDerTensor;
      PlasticPotDerTensor = ConstitutiveModelUtilities::StrainVectorToTensor(PlasticPotentialDerivative, PlasticPotDerTensor);
      double H = this->mYieldSurface.GetHardeningRule().CalculateDeltaHardeningUnsat(rVariables, H, PlasticPotDerTensor, rSuctionNew);

      VectorType AuxF = prod(trans(DeltaStressYieldCondition), rEPMatrix);
      VectorType AuxG = prod(rEPMatrix, PlasticPotentialDerivative);

      Matrix PlasticUpdateMatrix(6, 6);
      noalias(PlasticUpdateMatrix) = ZeroMatrix(6, 6);
      double denom = 0;
      for (unsigned int i = 0; i < 6; i++)
      {
         denom += AuxF(i) * PlasticPotentialDerivative(i);
         for (unsigned int j = 0; j < 6; j++)
         {
            PlasticUpdateMatrix(i, j) = AuxF(i) * AuxG(j);
         }
      }

      rEPMatrix -= PlasticUpdateMatrix / (H + denom);

      if ( rSuctionNew > 0.0) {
         rEPVector = prod(ElasticMatrix, PlasticPotentialDerivative);
         rEPVector *= -DeltaSuctionYieldCondition/(H + denom);
      }

      KRATOS_CATCH("")
   }

   template<typename TElasticityModel, typename TYieldSurface>
   void UnsaturatedCasmBaseSoilModel<TElasticityModel, TYieldSurface>::ReturnStressToYieldSurface(ModelDataType &rValues, PlasticDataType &rVariables)
   {
      KRATOS_TRY
      const double & rSuctionNew = rValues.GetSuctionNew();

      const ModelDataType & rModelData = rVariables.GetModelData();
      const Properties & rMaterialProperties = rModelData.GetProperties();

      const double & rLambda = rMaterialProperties[NORMAL_COMPRESSION_SLOPE];
      const double & rKappa  = rMaterialProperties[SWELLING_SLOPE];
      const double &rFirstPreconsolidationPressure = rMaterialProperties[PRE_CONSOLIDATION_STRESS];

      MatrixType StressMatrix;
      // evaluate constitutive matrix and plastic flow
      double &rPlasticVolDef = rVariables.Data.Internal(1);
      double & rPreconsolidationSat = rVariables.Data.Internal(6);
      double & rPreconsolidation = rVariables.Data.Internal(5);

      double Tolerance = 1e-6;

      double YieldSurface = this->mYieldSurface.CalculateYieldCondition(rVariables, YieldSurface);

      if (fabs(YieldSurface) < Tolerance)
         return;

      for (unsigned int i = 0; i < 150; i++)
      {
         //std::cout << " ITER " << i << " YieldSurfaceValue " << YieldSurface << std::endl;

         Matrix ElasticMatrix(6, 6);
         noalias(ElasticMatrix) = ZeroMatrix(6, 6);
         this->mElasticityModel.CalculateConstitutiveTensor(rValues, ElasticMatrix);

         VectorType DeltaStressYieldCondition = this->mYieldSurface.CalculateDeltaStressYieldCondition(rVariables, DeltaStressYieldCondition);

         VectorType PlasticPotentialDerivative = this->mYieldSurface.CalculateDeltaPlasticPotential(rVariables, PlasticPotentialDerivative);

         MatrixType PlasticPotDerTensor;
         PlasticPotDerTensor = ConstitutiveModelUtilities::StrainVectorToTensor(PlasticPotentialDerivative, PlasticPotDerTensor);
         double H = this->mYieldSurface.GetHardeningRule().CalculateDeltaHardeningUnsat(rVariables, H, PlasticPotDerTensor, rSuctionNew);

         double DeltaGamma = YieldSurface;
         DeltaGamma /= (H + MathUtils<double>::Dot(DeltaStressYieldCondition, prod(ElasticMatrix, PlasticPotentialDerivative)));

         MatrixType UpdateMatrix;
         this->ConvertHenckyVectorToCauchyGreenTensor(-DeltaGamma * PlasticPotentialDerivative / 2.0, UpdateMatrix);

         rValues.StrainMatrix = prod(UpdateMatrix, rValues.StrainMatrix);
         rValues.StrainMatrix = prod(rValues.StrainMatrix, trans(UpdateMatrix));

         MatrixType StressMatrix;
         this->mElasticityModel.CalculateStressTensor(rValues, StressMatrix);

         for (unsigned int i = 0; i < 3; i++)
            rPlasticVolDef += DeltaGamma * DeltaStressYieldCondition(i);

         rPreconsolidationSat = -rFirstPreconsolidationPressure * (std::exp(-rPlasticVolDef / (rLambda - rKappa)));
         rPreconsolidation = this->EvaluateLoadingCollapse( rVariables, rPreconsolidation, rPreconsolidationSat, rSuctionNew);

         YieldSurface = this->mYieldSurface.CalculateYieldCondition(rVariables, YieldSurface);

         if (fabs(YieldSurface) < Tolerance)
         {
            return;
         }
      }

      KRATOS_CATCH("")
   }

   template<typename TElasticityModel, typename TYieldSurface>
   void UnsaturatedCasmBaseSoilModel<TElasticityModel, TYieldSurface>::CalculateStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix)
   {
      KRATOS_TRY

      const double & rSuctionNew = rValues.GetSuctionNew();
      const double & rSuctionOld = rValues.GetSuctionOld();


      { // modify the internal variables to make it nonlocal
      }
      const double LocalPlasticVolStrain = this->mInternal(1);
      const double NonLocalPlasticVolStrain = this->mInternal(3);
      this->mInternal(1) = this->mInternal(3);

      const double LocalHenckyVolumetricStrain = this->mInternal(10);
      const double NonLocalHenckyVolumetricStrain = this->mInternal(11);

      ProcessInfo SomeProcessInfo;
      if ( NonLocalHenckyVolumetricStrain!= LocalHenckyVolumetricStrain) {
         this->mElasticityModel.SetValue( HENCKY_ELASTIC_VOLUMETRIC_STRAIN, NonLocalHenckyVolumetricStrain, SomeProcessInfo);
      }
      this->mInternal(10) = this->mInternal(11);

      double Tolerance = 1e-6;

      PlasticDataType Variables;
      this->InitializeVariables(rValues, Variables);

      const ModelDataType &rModelData = Variables.GetModelData();
      const Properties &rMaterialProperties = rModelData.GetProperties();

      const double &rPlasticVolDef = Variables.Data.Internal(1);

      const double & rLambda = rMaterialProperties[NORMAL_COMPRESSION_SLOPE];
      const double & rKappa  = rMaterialProperties[SWELLING_SLOPE];
      const double &rFirstPreconsolidationPressure = rMaterialProperties[PRE_CONSOLIDATION_STRESS];


      //Calculate trial stress Matrix
      this->mElasticityModel.CalculateStressTensor(rValues, rStressMatrix);

      double & rPreconsolidationSat = Variables.Data.Internal(6);
      rPreconsolidationSat = -rFirstPreconsolidationPressure * (std::exp(-rPlasticVolDef / (rLambda - rKappa)));

      double & rPreconsolidation = Variables.Data.Internal(5);
      rPreconsolidation = this->EvaluateLoadingCollapse( Variables, rPreconsolidation, rPreconsolidationSat, rSuctionNew);

      Variables.Data.TrialStateFunction = this->mYieldSurface.CalculateYieldCondition(Variables, Variables.Data.TrialStateFunction);

      Matrix ConstitutiveMatrix(6, 6);
      noalias(ConstitutiveMatrix) = ZeroMatrix(6, 6);
      Vector ConstitutiveVector(6);
      noalias(ConstitutiveVector) = ZeroVector(6);
      Matrix ElasticTensor(6, 6);
      noalias(ElasticTensor) = ZeroMatrix(6, 6);
      Matrix ElastoPlasticTensor(6, 6);
      noalias(ElastoPlasticTensor) = ZeroMatrix(6, 6);


      if (Variables.State().Is(ConstitutiveModelData::IMPLEX_ACTIVE))
      {
         const MatrixType &rDeltaDeformationMatrix = rValues.GetDeltaDeformationMatrix();
         this->RecoverPreviousElasticLeftCauchyGreen(rDeltaDeformationMatrix, rValues.StrainMatrix);
         // Calculate with implex
         this->CalculateImplexPlasticStepUnsat(rValues, Variables, rStressMatrix, rDeltaDeformationMatrix, ConstitutiveVector);

         rConstitutiveMatrix.clear();
         this->mElasticityModel.CalculateConstitutiveTensor(rValues, ConstitutiveMatrix);
         rConstitutiveMatrix = this->SetConstitutiveMatrixToTheAppropriateSizeUnsat(rConstitutiveMatrix, ConstitutiveMatrix, ConstitutiveVector, rStressMatrix);
      }
      else if (Variables.Data.TrialStateFunction < Tolerance)
      {

         // elastic loading step
         rConstitutiveMatrix.clear();
         this->mElasticityModel.CalculateConstitutiveTensor(rValues, ConstitutiveMatrix);
         ElasticTensor = ConstitutiveMatrix;
         rConstitutiveMatrix = this->SetConstitutiveMatrixToTheAppropriateSizeUnsat(rConstitutiveMatrix, ConstitutiveMatrix, ConstitutiveVector, rStressMatrix);
      }
      else
      {

         // elasto-plastic step. Recover Initial be
         const MatrixType &rDeltaDeformationMatrix = rValues.GetDeltaDeformationMatrix();
         this->RecoverPreviousElasticLeftCauchyGreen(rDeltaDeformationMatrix, rValues.StrainMatrix);
         rPreconsolidation = this->EvaluateLoadingCollapse( Variables, rPreconsolidation, rPreconsolidationSat, rSuctionOld);

         double InitialYieldFunction;
         this->mElasticityModel.CalculateStressTensor(rValues, rStressMatrix);
         //std::cout << " PREVIOUS STRESS MATRIX " << rStressMatrix << std::endl;
         //std::cout << " PREVIOUS PC and PCSAT " << rPreconsolidation << " SAT " << rPreconsolidationSat << std::endl;
         InitialYieldFunction = this->mYieldSurface.CalculateYieldCondition(Variables, InitialYieldFunction);

         if (InitialYieldFunction > Tolerance)
         {
            //std::cout << " CORRECTING DRIFT PREVIOUS TO THAT " << std::endl;
            //std::cout << "  f0 and f " <<InitialYieldFunction << " and " << Variables.Data.TrialStateFunction << std::endl;
            // correct the initial drift (nonlocal, transfer...)
            this->ReturnStressToYieldSurface(rValues, Variables);
         }

         if ((InitialYieldFunction < -Tolerance) && (Variables.Data.TrialStateFunction > Tolerance))
         {
            //std::cout << " SOLUTION WITH CHANGE " << std::endl;
            // compute solution with change
            ComputeSolutionWithChange(rValues, Variables, rDeltaDeformationMatrix);
         }
         else
         {
            bool UnloadingCondition = false;

            UnloadingCondition = this->EvaluateUnloadingCondition(rValues, Variables, rDeltaDeformationMatrix);
            if (UnloadingCondition)
            {
               //std::cout << " SOLUTION WITH CHANGE b " << std::endl;
               // compute solution with change
               ComputeSolutionWithChange(rValues, Variables, rDeltaDeformationMatrix);
            }
            else
            {
               // compute plastic problem
               // compute unloading condition
               //std::cout << " SOLUTION ElastoPlastic " << std::endl;
               ComputeSubsteppingElastoPlasticProblem(rValues, Variables, rDeltaDeformationMatrix, rSuctionOld, rSuctionNew);
            }
         }

         this->ReturnStressToYieldSurface(rValues, Variables);

         noalias(rStressMatrix) = rValues.StressMatrix;

         this->mElasticityModel.CalculateConstitutiveTensor(rValues, ConstitutiveMatrix);
         ElasticTensor = ConstitutiveMatrix;
         this->ComputeElastoPlasticTangentMatrixAndVector(rValues, Variables, ConstitutiveMatrix, ConstitutiveVector);

         ElastoPlasticTensor = ConstitutiveMatrix;
         rConstitutiveMatrix = this->SetConstitutiveMatrixToTheAppropriateSizeUnsat(rConstitutiveMatrix, ConstitutiveMatrix, ConstitutiveVector, rStressMatrix);
      }

      if (rValues.State.Is(ConstitutiveModelData::UPDATE_INTERNAL_VARIABLES))
      {

         this->UpdateInternalVariables(rValues, Variables, rStressMatrix);
         this->mStressMatrix = rStressMatrix / rValues.GetTotalDeformationDet();

         // is this my error?
         const ModelDataType &rModelData = Variables.GetModelData();
         const Properties &rMaterialProperties = rModelData.GetProperties();
         double MeanStress = 0;
         for (unsigned int i = 0; i < 3; i++)
            MeanStress += rValues.StressMatrix(i, i) / 3.0;
         const double &rSwellingSlope = rMaterialProperties[SWELLING_SLOPE];
         const double &rAlpha = rMaterialProperties[ALPHA_SHEAR];
         double K = (-MeanStress) / rSwellingSlope;
         double G = rMaterialProperties[INITIAL_SHEAR_MODULUS];
         G += rAlpha * (-MeanStress);
         mBulkModulus = K;
         mShearModulus = G;

         this->mInternal(3) = this->mInternal(1);
         this->mInternal(1) = LocalPlasticVolStrain + (this->mInternal(1) -NonLocalPlasticVolStrain);

         this->mInternal(10,1) = this->mInternal(10);

         double CurrentHenckyVol(0);
         /// not updated
         MatrixType HenckyStrain;
         noalias( HenckyStrain) = ZeroMatrix(3,3);
         this->ConvertCauchyGreenTensorToHenckyTensor( rValues.StrainMatrix, HenckyStrain);
         for (unsigned int i = 0; i < 3; i++)
            CurrentHenckyVol += HenckyStrain(i,i);


         this->mInternal(10,1) = LocalHenckyVolumetricStrain;
         this->mInternal(11,1) = NonLocalHenckyVolumetricStrain;
         this->mInternal(11) = CurrentHenckyVol; // in case it is local....
         this->mInternal(10) = LocalHenckyVolumetricStrain + ( CurrentHenckyVol - NonLocalHenckyVolumetricStrain);
      } else {
         this->mInternal(1) = LocalPlasticVolStrain;
         this->mInternal(3) = NonLocalPlasticVolStrain;
         this->mInternal(10) = LocalHenckyVolumetricStrain;
         this->mInternal(11) = NonLocalHenckyVolumetricStrain;
      }

      /*std::cout << " STRESS " << rStressMatrix << " " << rPreconsolidationSat << " " << rPreconsolidation << std::endl;
      std::cout << std::endl;
      std::cout << std::endl;*/

      KRATOS_CATCH(" ")
   }


   template<typename TElasticityModel, typename TYieldSurface>
   void UnsaturatedCasmBaseSoilModel<TElasticityModel, TYieldSurface>::CalculateStressTensor( ModelDataType & rValues, MatrixType & rStressMatrix)
   {
      KRATOS_TRY

      Matrix ConstitutiveMatrix(6, 6);
      noalias(ConstitutiveMatrix) = ZeroMatrix(6, 6);
      this->CalculateStressAndConstitutiveTensors(rValues, rStressMatrix, ConstitutiveMatrix);
      rValues.StressMatrix = rStressMatrix;

      KRATOS_CATCH("")
   }

   template<typename TElasticityModel, typename TYieldSurface>
   void UnsaturatedCasmBaseSoilModel<TElasticityModel, TYieldSurface>::InitializeModel(ModelDataType & rValues)
   {
      KRATOS_TRY
      if ( this->mInitialized == false)
      {
         PlasticDataType Variables;
         this->InitializeVariables(rValues, Variables);

         const ModelDataType &rModelData = Variables.GetModelData();
         const Properties &rMaterialProperties = rModelData.GetProperties();

         Variables.Data.Internal(5) = -rMaterialProperties[PRE_CONSOLIDATION_STRESS];
         Variables.Data.Internal(6) = -rMaterialProperties[PRE_CONSOLIDATION_STRESS];

         MatrixType Stress;
         this->UpdateInternalVariables(rValues, Variables, Stress);

         double &rPC = Variables.Data.Internal(5);

         rPC = -rMaterialProperties[PRE_CONSOLIDATION_STRESS];

         double ElasticModulus = rMaterialProperties[PRE_CONSOLIDATION_STRESS];
         ElasticModulus /= rMaterialProperties[SWELLING_SLOPE];

         mBulkModulus = ElasticModulus;
         mShearModulus = ElasticModulus;

         this->mInitialized = true;
      }
      this->mElasticityModel.InitializeModel(rValues);

      KRATOS_CATCH("")
   }
   

   template class UnsaturatedCasmBaseSoilModel<BorjaModel, UnsaturatedCasmYieldSurface<UnsaturatedCasmHardeningRule>>;
   template class UnsaturatedCasmBaseSoilModel<DV2BorjaModel, UnsaturatedCasmYieldSurface<UnsaturatedCasmHardeningRule>>;


}
