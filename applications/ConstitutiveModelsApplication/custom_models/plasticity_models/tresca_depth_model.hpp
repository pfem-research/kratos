//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:                  April 2017 $
//
//

#if !defined(KRATOS_TRESCA_DEPTH_MODEL_HPP_INCLUDED)
#define KRATOS_TRESCA_DEPTH_MODEL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/soil_base_model.hpp"
#include "custom_models/plasticity_models/hardening_rules/mohr_coulomb_v1_hardening_rule.hpp"
#include "custom_models/plasticity_models/yield_surfaces/tresca_depth_yield_surface.hpp"
#include "custom_models/elasticity_models/hencky_depth_model.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
    */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) TrescaDepthModel : public SoilBaseModel<HenckyDepthModel, TrescaDepthYieldSurface<MohrCoulombV1HardeningRule>>
{
public:
   ///@name Type Definitions
   ///@{

   //elasticity model
   typedef HenckyDepthModel ElasticityModelType;
   typedef ElasticityModelType::Pointer ElasticityModelPointer;

   //yield surface
   typedef MohrCoulombV1HardeningRule HardeningRuleType;
   typedef TrescaDepthYieldSurface<HardeningRuleType> YieldSurfaceType;
   typedef YieldSurfaceType::Pointer YieldSurfacePointer;

   //base type
   typedef SoilBaseModel<ElasticityModelType, YieldSurfaceType> BaseType;

   //common types
   typedef BaseType::Pointer BaseTypePointer;
   typedef BaseType::SizeType SizeType;
   typedef BaseType::VoigtIndexType VoigtIndexType;
   typedef BaseType::MatrixType MatrixType;
   typedef BaseType::ModelDataType ModelDataType;
   typedef BaseType::MaterialDataType MaterialDataType;
   typedef BaseType::PlasticDataType PlasticDataType;
   typedef BaseType::InternalVariablesType InternalVariablesType;

   /// Pointer definition of TrescaDepthModel
   KRATOS_CLASS_POINTER_DEFINITION(TrescaDepthModel);

   ///@}
   ///@name Life Cycle
   ///@{

   /// Default constructor.
   TrescaDepthModel() : BaseType() { }

   /// Copy constructor.
   TrescaDepthModel(TrescaDepthModel const &rOther) : BaseType(rOther) {}

   /// Assignment operator.
   TrescaDepthModel &operator=(TrescaDepthModel const &rOther)
   {
      BaseType::operator=(rOther);
      return *this;
   }

   /// Clone.
   ConstitutiveModel::Pointer Clone() const override
   {
      return Kratos::make_shared<TrescaDepthModel>(*this);
   }

   /// Destructor.
   ~TrescaDepthModel() override {}

   ///@}
   ///@name Operators
   ///@{

   ///@}
   ///@name Operations
   ///@{

   /**
          * Initialize member data
          */
   void InitializeModel(ModelDataType &rValues) override
   {
      KRATOS_TRY

      if (this->mInitialized == false)
      {
         PlasticDataType Variables;
         this->InitializeVariables(rValues, Variables);

         double &rYield0 = Variables.Data.Internal(5);

         rYield0 = 1.0E88;

         MatrixType Stress;
         this->UpdateInternalVariables(rValues, Variables, Stress);

         this->mInitialized = true;
      }
      this->mElasticityModel.InitializeModel(rValues);

      KRATOS_CATCH("")
   }

   /**
          * Check
          */
   int Check(const Properties &rProperties, const ProcessInfo &rCurrentProcessInfo) override
   {
      KRATOS_TRY

      //LMV: to be implemented. but should not enter in the base one

      return 0;

      KRATOS_CATCH("")
   }

   ///@}
   ///@name Access
   ///@{

   /**
          * Has Values
          */
   bool Has(const Variable<double> &rVariable) override
   {
      if (rVariable == PLASTIC_STRAIN || rVariable == DELTA_PLASTIC_STRAIN)
         return true;

      return false;
   }

   /**
          * Set Values
          */
   void SetValue(const Variable<double> &rVariable,
                 const double &rValue,
                 const ProcessInfo &rCurrentProcessInfo) override
   {
      KRATOS_TRY

      if ( rVariable == YIELD_STRESS) {
         mInternal(5) = rValue;
      } else {
         SoilBaseModel::SetValue( rVariable, rValue, rCurrentProcessInfo);
      }

      KRATOS_CATCH("")
   }

   /**
          * Get Values
          */
   double &GetValue(const Variable<double> &rVariable, double &rValue) override
   {

      rValue = 0;

      if (rVariable == PLASTIC_STRAIN)
      {
         rValue = this->mInternal(0);
      }
      else if (rVariable == DELTA_PLASTIC_STRAIN)
      {
         rValue = this->mInternal(0) - this->mInternal(0,1);
      }
       else if ( rVariable == YIELD_STRESS) {
         rValue = mInternal(5);
       }
      else
      {
         rValue = SoilBaseModel::GetValue(rVariable, rValue);
      }
      return rValue;
   }

   ///@}
   ///@name Inquiry
   ///@{

   ///@}
   ///@name Input and output
   ///@{

   /// Turn back information as a string.
   std::string Info() const override
   {
      std::stringstream buffer;
      buffer << "TrescaDepthModel";
      return buffer.str();
   }

   /// Print information about this object.
   void PrintInfo(std::ostream &rOStream) const override
   {
      rOStream << "TrescaDepthModel";
   }

   /// Print object's data.
   void PrintData(std::ostream &rOStream) const override
   {
      rOStream << "TrescaDepthModel Data";
   }

   ///@}
   ///@name Friends
   ///@{

   ///@}

protected:
   ///@name Protected static Member Variables
   ///@{

   ///@}
   ///@name Protected member Variables
   ///@{


   ///@}
   ///@name Protected Operators
   ///@{

   ///@}
   ///@name Protected Operations
   ///@{

   ///@}
   ///@name Protected  Access
   ///@{

   ///@}
   ///@name Protected Inquiry
   ///@{

   ///@}
   ///@name Protected LifeCycle
   ///@{

   ///@}

private:
   ///@name Static Member Variables
   ///@{

   ///@}
   ///@name Member Variables
   ///@{

   ///@}
   ///@name Private Operators
   ///@{

   ///@}
   ///@name Private Operations
   ///@{

   ///@}
   ///@name Private  Access
   ///@{

   ///@}
   ///@name Private Inquiry
   ///@{

   ///@}
   ///@name Serialization
   ///@{
   friend class Serializer;

   void save(Serializer &rSerializer) const override
   {
      KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BaseType)
   }

   void load(Serializer &rSerializer) override
   {
      KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BaseType)
   }

   ///@}
   ///@name Un accessible methods
   ///@{

   ///@}

}; // Class TrescaDepthModel

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_TRESCA_DEPTH_MODEL_HPP_INCLUDED  defined
