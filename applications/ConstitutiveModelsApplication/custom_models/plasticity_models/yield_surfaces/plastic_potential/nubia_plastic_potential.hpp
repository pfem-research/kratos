//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:      LMonforte-LHauser $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:                  April 2017 $
//
//

#if !defined(KRATOS_NUBIA_PLASTIC_POTENTIAL_HPP_INCLUDED)
#define KRATOS_NUBIA_PLASTIC_POTENTIAL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/yield_surfaces/yield_surface.hpp"
#include "custom_utilities/stress_invariants_utilities.hpp"
#include "custom_utilities/shape_deviatoric_plane_utilities.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
    */
template <class THardeningRule>
class NubiaPlasticPotential : public YieldSurface<THardeningRule>
{
public:
   ///@name Type Definitions
   ///@{

   typedef ConstitutiveModelData::MatrixType MatrixType;
   typedef ConstitutiveModelData::VectorType VectorType;
   typedef ConstitutiveModelData::ModelData ModelDataType;
   typedef ConstitutiveModelData::MaterialData MaterialDataType;

   typedef YieldSurface<THardeningRule> BaseType;
   typedef typename YieldSurface<THardeningRule>::Pointer BaseTypePointer;
   typedef typename BaseType::PlasticDataType PlasticDataType;

   /// Pointer definition of NubiaPlasticPotential
   KRATOS_CLASS_POINTER_DEFINITION(NubiaPlasticPotential);

   ///@}
   ///@name Life Cycle
   ///@{

   /// Default constructor.
   NubiaPlasticPotential() : BaseType() {}

   /// Copy constructor.
   NubiaPlasticPotential(NubiaPlasticPotential const &rOther) : BaseType(rOther) {}

   /// Assignment operator.
   NubiaPlasticPotential &operator=(NubiaPlasticPotential const &rOther)
   {
      BaseType::operator=(rOther);
      return *this;
   }

   /// Clone.
   BaseTypePointer Clone() const override
   {
      return Kratos::make_shared<NubiaPlasticPotential>(*this);
   }

   /// Destructor.
   ~NubiaPlasticPotential() override {}

   ///@}
   ///@name Operators
   ///@{

   ///@}
   ///@name Operations
   ///@{

   //*************************************************************************************
   //*************************************************************************************
   // evaluation of the derivative of the yield surface respect the stresses
   VectorType &CalculateDeltaStressYieldCondition(const PlasticDataType &rVariables, VectorType &rDeltaStressYieldCondition) override
   {
      KRATOS_TRY

      const ModelDataType &rModelData = rVariables.GetModelData();
      const MatrixType &rStressMatrix = rModelData.GetStressMatrix();

      // Material Parameters
      const Properties &rProperties = rModelData.GetProperties();
      const double &rShearM = rProperties[CRITICAL_STATE_LINE];
      //const double& rSpacingR = rProperties[SPACING_RATIO];
      const double &rShapeN = rProperties[SHAPE_PARAMETER];
      const double &rSwellingSlope = rProperties[SWELLING_SLOPE];
      const double &rOtherSlope = rProperties[NORMAL_COMPRESSION_SLOPE];
      double Friction = 0;
      if (rProperties.Has(INTERNAL_FRICTION_ANGLE))
          Friction = rProperties[INTERNAL_FRICTION_ANGLE];

      //1- stress invariants and deviative vectors

      double MeanStress, J2sqrt, LodeAngle;

      VectorType V1, V2, V3;
      // more work is requiered
      StressInvariantsUtilities::CalculateStressInvariants(rStressMatrix, MeanStress, J2sqrt, LodeAngle);
      StressInvariantsUtilities::CalculateDerivativeVectors(rStressMatrix, V1, V2, V3);

      // 2- Evaluate m of equation 2-12
      double m(0);
      if (rProperties.Has(CASM_M))
      {
         m = rProperties[CASM_M];
      }
      if (m <= 1.0)
      {
         double BigLambda = 1.0 - rSwellingSlope / rOtherSlope;

         m = pow(rShearM * (6.0 - rShearM), rShapeN) - pow(3.0 * rShearM, rShapeN);
         m /= BigLambda * (6.0 - rShearM) * pow(3.0 * rShearM, rShapeN - 1);
         m *= 2.0 / 3.0;
      }

      if (m <= 1.0)
      {
         KRATOS_ERROR << " the given parameters are problematic with Nubia//Casm plastic flow " << std::endl;
      }
     
      double ThirdInvariantEffect(1), DerivativeEffect(0);
      bool MultiplySinInstead = false;
      if (Friction > 0.0)
      {
         ShapeAtDeviatoricPlaneUtility::CalculateKLodeCoefficients(ThirdInvariantEffect, DerivativeEffect, LodeAngle, Friction, true, MultiplySinInstead);
         //std::cout << " LodeAngle " << LodeAngle*180.0/Globals::Pi << " effect " << ThirdInvariantEffect << std::endl;
      }

      double StressRatio = sqrt(3.0) * J2sqrt / (-MeanStress);
      double ShearM = rShearM/ThirdInvariantEffect;

      double C1 = m * (m - 1.0) * rShapeN * pow(StressRatio / ShearM, rShapeN - 1.0) * (-1.0) * (-1.0) * sqrt(3.0) * J2sqrt / pow(MeanStress, 2) / ShearM;
      C1 /= 1.0 + (m - 1.0) * pow(StressRatio / ShearM, rShapeN);

      C1 += rShapeN * (m - 1.0) / MeanStress;

      double C2 = m * (m - 1.0) * rShapeN * pow(StressRatio / ShearM, rShapeN - 1.0) * sqrt(3.0) / (-MeanStress) / ShearM;
      C2 /= 1.0 + (m - 1.0) * pow(StressRatio / ShearM, rShapeN);

      rDeltaStressYieldCondition = C1 * V1 + C2 * V2;
    
      if (Friction > 0.0 && J2sqrt > 1E-6)
      {
         
         double Term = m * (m-1.0) * rShapeN * pow( StressRatio/rShearM, rShapeN) * pow(ThirdInvariantEffect, rShapeN-1.0) * DerivativeEffect ;
         Term /= 1.0 + (m-1.0)* pow( StressRatio / ShearM, rShapeN);

         double C2(0);
         double C3(0);
         if ( MultiplySinInstead){
            C2 = -std::sin(3.0 * LodeAngle) / J2sqrt * Term;
            C3 = -sqrt(3.0) / 2.0 / pow(J2sqrt, 3) * Term;
         }
         else{
            C2 = -std::tan(3.0 * LodeAngle) / J2sqrt * Term;
            C3 = -sqrt(3.0) / 2.0 / pow(J2sqrt, 3) / std::cos(3.0 * LodeAngle) * Term;
         }

         rDeltaStressYieldCondition += C2 * V2 + C3 * V3;
      }

      //std::cout << " P " << MeanStress << " J2sqrt " << J2sqrt << " stressRatio " << StressRatio << " m " << m << " C1 " << C1 << " C2 " << C2 << std::endl;

      if (false)
      {
         //double StressRatio = sqrt(3.0)*J2sqrt/(-MeanStress);
         double Ratio = (pow(rShearM, rShapeN) - pow(StressRatio, rShapeN)) / m / pow(StressRatio, rShapeN - 1.0);
         double C22 = 1;
         double C12 = -sqrt(3.0) / 3.0 * Ratio * C22;

         if (fabs(StressRatio) < 1e-12)
         {
            C12 = -1.0;
            C22 = 0.0;
         }
         std::cout << " P " << MeanStress << " J2sqrt " << J2sqrt << " stressRatio " << StressRatio << " m " << m << " C1 " << C1 << " C2 " << C2 << " C12 " << C12 << " C22 " << C22 << std::endl;
         std::cout << "                 ratio1 " << C1 / C2 << " ratio2 " << C12 / C22 << " FINAL RATIO " << C1 * C22 / C12 / C2 << std::endl;
         rDeltaStressYieldCondition = C12 * V1 + C22 * V2;
      }

      if (rProperties.Has(NORMALIZE_FLOW_RULE))
      {
         if (rProperties[NORMALIZE_FLOW_RULE] > 0)
         {
            double norm = 0.0;
            for (unsigned int ii = 0; ii < 3; ii++)
               norm += pow(rDeltaStressYieldCondition(ii), 2);
            for (unsigned int ii = 3; ii < 6; ii++)
               norm += 2.0 * pow(rDeltaStressYieldCondition(ii) / 2.0, 2);
            norm = sqrt(norm);
            if (norm > 1e-1)
               rDeltaStressYieldCondition /= norm;
         }
      }

      return rDeltaStressYieldCondition;

      KRATOS_CATCH(" ")
   }

   /**
       * Get required properties
       */
   void GetRequiredProperties(Properties &rProperties) override
   {
      KRATOS_TRY

      // Set required properties
      double value = 0.0; //dummy
      rProperties.SetValue(CRITICAL_STATE_LINE,value);
      rProperties.SetValue(SWELLING_SLOPE,value);
      //rProperties.SetValue(SPACING_RATIO,value);
      rProperties.SetValue(SHAPE_PARAMETER,value);
      rProperties.SetValue(NORMAL_COMPRESSION_SLOPE,value);

      BaseType::GetRequiredProperties(rProperties);

      KRATOS_CATCH(" ")
   }

   ///@}
   ///@name Access
   ///@{

   ///@}
   ///@name Inquiry
   ///@{

   ///@}
   ///@name Input and output
   ///@{

   /// Turn back information as a string.
   std::string Info() const override
   {
      std::stringstream buffer;
      buffer << "NubiaPlasticPotential";
      return buffer.str();
   }

   /// Print information about this object.
   void PrintInfo(std::ostream &rOStream) const override
   {
      rOStream << "NubiaPlasticPotential";
   }

   /// Print object's data.
   void PrintData(std::ostream &rOStream) const override
   {
      rOStream << "NubiaPlasticPotential Data";
   }

   ///@}
   ///@name Friends
   ///@{

   ///@}

protected:
   ///@name Protected static Member Variables
   ///@{

   ///@}
   ///@name Protected member Variables
   ///@{

   ///@}
   ///@name Protected Operators
   ///@{

   ///@}
   ///@name Protected Operations
   ///@{

   ///@}
   ///@name Protected  Access
   ///@{

   ///@}
   ///@name Protected Inquiry
   ///@{

   ///@}
   ///@name Protected LifeCycle
   ///@{

   ///@}

private:
   ///@name Static Member Variables
   ///@{

   ///@}
   ///@name Member Variables
   ///@{

   ///@}
   ///@name Private Operators
   ///@{

   ///@}
   ///@name Private Operations
   ///@{

   ///@}
   ///@name Private  Access
   ///@{

   ///@}
   ///@name Serialization
   ///@{
   friend class Serializer;

   void save(Serializer &rSerializer) const override
   {
      KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BaseType)
   }

   void load(Serializer &rSerializer) override
   {
      KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BaseType)
   }

   ///@}
   ///@name Private Inquiry
   ///@{

   ///@}
   ///@name Un accessible methods
   ///@{

   ///@}

}; // Class NubiaPlasticPotential

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_NUBIA_PLASTIC_POTENTIAL_HPP_INCLUDED  defined
