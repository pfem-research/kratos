//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:      LMonforte-LHauser $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:                  April 2021 $
//
//

#if !defined(KRATOS_MODIFIED_CAM_CLAY_STRUCTURE_PLASTIC_POTENTIAL_HPP_INCLUDED)
#define      KRATOS_MODIFIED_CAM_CLAY_STRUCTURE_PLASTIC_POTENTIAL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/yield_surfaces/yield_surface.hpp"
#include "custom_utilities/stress_invariants_utilities.hpp"
#include "custom_utilities/shape_deviatoric_plane_utilities.hpp"

namespace Kratos
{
   ///@addtogroup ConstitutiveModelsApplication
   ///@{

   ///@name Kratos Globals
   ///@{

   ///@}
   ///@name Type Definitions
   ///@{

   ///@}
   ///@name  Enum's
   ///@{

   ///@}
   ///@name  Functions
   ///@{

   ///@}
   ///@name Kratos Classes
   ///@{

   /// Short class definition.
   /** Detail class definition.
    */
   template <class THardeningRule>
      class ModifiedCamClayStructurePlasticPotential : public YieldSurface<THardeningRule>
   {
      public:
         ///@name Type Definitions
         ///@{

         typedef ConstitutiveModelData::MatrixType MatrixType;
         typedef ConstitutiveModelData::VectorType VectorType;
         typedef ConstitutiveModelData::ModelData ModelDataType;
         typedef ConstitutiveModelData::MaterialData MaterialDataType;

         typedef YieldSurface<THardeningRule> BaseType;
         typedef typename YieldSurface<THardeningRule>::Pointer BaseTypePointer;
         typedef typename BaseType::PlasticDataType PlasticDataType;

         /// Pointer definition of ModifiedCamClayStructurePlasticPotential
         KRATOS_CLASS_POINTER_DEFINITION(ModifiedCamClayStructurePlasticPotential);

         ///@}
         ///@name Life Cycle
         ///@{

         /// Default constructor.
         ModifiedCamClayStructurePlasticPotential() : BaseType() {}

         /// Copy constructor.
         ModifiedCamClayStructurePlasticPotential(ModifiedCamClayStructurePlasticPotential const &rOther) : BaseType(rOther) {}

         /// Assignment operator.
         ModifiedCamClayStructurePlasticPotential &operator=(ModifiedCamClayStructurePlasticPotential const &rOther)
         {
            BaseType::operator=(rOther);
            return *this;
         }

         /// Clone.
         BaseTypePointer Clone() const override
         {
            return Kratos::make_shared<ModifiedCamClayStructurePlasticPotential>(*this);
         }

         /// Destructor.
         ~ModifiedCamClayStructurePlasticPotential() override {}

         ///@}
         ///@name Operators
         ///@{

         ///@}
         ///@name Operations
         ///@{

         //*************************************************************************************
         //*************************************************************************************
         // evaluation of the derivative of the yield surface respect the stresses
         VectorType &CalculateDeltaStressYieldCondition(const PlasticDataType &rVariables, VectorType &rDeltaStressYieldCondition) override
         {
            KRATOS_TRY

            const ModelDataType &rModelData = rVariables.GetModelData();
            const MatrixType &rStressMatrix = rModelData.GetStressMatrix();

            // Material Parameters
            const Properties &rProperties = rModelData.GetProperties();
            const double &rShearM = rProperties[CRITICAL_STATE_LINE];
            const double &rFriction = rProperties[INTERNAL_FRICTION_ANGLE];

            // compute something with the hardening rule

            const double &rPT = rVariables.Data.Internal(6);

            double MeanStress, J2sqrt, LodeAngle;

            VectorType V1, V2, V3;
            // more work is requiered
            StressInvariantsUtilities::CalculateStressInvariants(rStressMatrix, MeanStress, J2sqrt, LodeAngle);
            StressInvariantsUtilities::CalculateDerivativeVectors(rStressMatrix, V1, V2, V3);
            MeanStress += rPT;

            double ThirdInvariantEffect(1), DerivativeEffect(0);
            bool MultiplySinInstead = false;
            if (rFriction > 0.0)
               ShapeAtDeviatoricPlaneUtility::CalculateKLodeCoefficients(ThirdInvariantEffect, DerivativeEffect, LodeAngle, rFriction, true, MultiplySinInstead);

            double PreconsolidationStress;
            PreconsolidationStress = MeanStress + 3.0 / MeanStress * pow(J2sqrt / (rShearM / ThirdInvariantEffect), 2);

            rDeltaStressYieldCondition = (2.0 * MeanStress - PreconsolidationStress) * V1 + 2.0 * 3.0 * pow(ThirdInvariantEffect / rShearM, 2) * J2sqrt * V2;

            if (rFriction > 0.0 && J2sqrt > 1E-6)
            {
               double Term = pow(sqrt(3.0) * J2sqrt / rShearM, 2) * ThirdInvariantEffect * 2.0 * DerivativeEffect;
               double C2(0);
               double C3(0);
               if ( MultiplySinInstead){
                  C2 = -std::sin(3.0 * LodeAngle) / J2sqrt * Term;
                  C3 = -sqrt(3.0) / 2.0 / pow(J2sqrt, 3) * Term;
               }
               else{
                  C2 = -std::tan(3.0 * LodeAngle) / J2sqrt * Term;
                  C3 = -sqrt(3.0) / 2.0 / pow(J2sqrt, 3) / std::cos(3.0 * LodeAngle) * Term;
               }

               rDeltaStressYieldCondition += C2 * V2 + C3 * V3;
            }

            return rDeltaStressYieldCondition;

            KRATOS_CATCH(" ")
         }

         ///@}
         ///@name Access
         ///@{

         ///@}
         ///@name Inquiry
         ///@{

         ///@}
         ///@name Input and output
         ///@{

         /// Turn back information as a string.
         std::string Info() const override
         {
            std::stringstream buffer;
            buffer << "ModifiedCamClayStructurePlasticPotential";
            return buffer.str();
         }

         /// Print information about this object.
         void PrintInfo(std::ostream &rOStream) const override
         {
            rOStream << "ModifiedCamClayStructurePlasticPotential";
         }

         /// Print object's data.
         void PrintData(std::ostream &rOStream) const override
         {
            rOStream << "ModifiedCamClayStructurePlasticPotential Data";
         }

         ///@}
         ///@name Friends
         ///@{

         ///@}

      protected:
         ///@name Protected static Member Variables
         ///@{

         ///@}
         ///@name Protected member Variables
         ///@{

         ///@}
         ///@name Protected Operators
         ///@{

         ///@}
         ///@name Protected Operations
         ///@{

         ///@}
         ///@name Protected  Access
         ///@{

         ///@}
         ///@name Protected Inquiry
         ///@{

         ///@}
         ///@name Protected LifeCycle
         ///@{

         ///@}

      private:
         ///@name Static Member Variables
         ///@{

         ///@}
         ///@name Member Variables
         ///@{

         ///@}
         ///@name Private Operators
         ///@{

         ///@}
         ///@name Private Operations
         ///@{

         ///@}
         ///@name Private  Access
         ///@{

         ///@}
         ///@name Serialization
         ///@{
         friend class Serializer;

         void save(Serializer &rSerializer) const override
         {
            KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BaseType)
         }

         void load(Serializer &rSerializer) override
         {
            KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BaseType)
         }

         ///@}
         ///@name Private Inquiry
         ///@{

         ///@}
         ///@name Un accessible methods
         ///@{

         ///@}

   }; // Class ModifiedCamClayStructurePlasticPotential

   ///@}

   ///@name Type Definitions
   ///@{

   ///@}
   ///@name Input and output
   ///@{

   ///@}

   ///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_MODIFIED_CAM_CLAY_STRUCTURE_PLASTIC_POTENTIAL_HPP_INCLUDED  defined
