//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:      LMonforte-LHauser $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:                  April 2020 $
//
//

#if !defined(KRATOS_MM_PLASTIC_POTENTIAL_HPP_INCLUDED)
#define KRATOS_MM_PLASTIC_POTENTIAL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/yield_surfaces/yield_surface.hpp"
#include "custom_utilities/stress_invariants_utilities.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
    */
template <class THardeningRule>
class MMPlasticPotential : public YieldSurface<THardeningRule>
{
public:
   ///@name Type Definitions
   ///@{

   typedef ConstitutiveModelData::MatrixType MatrixType;
   typedef ConstitutiveModelData::VectorType VectorType;
   typedef ConstitutiveModelData::ModelData ModelDataType;
   typedef ConstitutiveModelData::MaterialData MaterialDataType;

   typedef YieldSurface<THardeningRule> BaseType;
   typedef typename YieldSurface<THardeningRule>::Pointer BaseTypePointer;
   typedef typename BaseType::PlasticDataType PlasticDataType;

   /// Pointer definition of MMPlasticPotential
   KRATOS_CLASS_POINTER_DEFINITION(MMPlasticPotential);

   ///@}
   ///@name Life Cycle
   ///@{

   /// Default constructor.
   MMPlasticPotential() : BaseType() {}

   /// Copy constructor.
   MMPlasticPotential(MMPlasticPotential const &rOther) : BaseType(rOther) {}

   /// Assignment operator.
   MMPlasticPotential &operator=(MMPlasticPotential const &rOther)
   {
      BaseType::operator=(rOther);
      return *this;
   }

   /// Clone.
   BaseTypePointer Clone() const override
   {
      return Kratos::make_shared<MMPlasticPotential>(*this);
   }

   /// Destructor.
   ~MMPlasticPotential() override {}

   ///@}
   ///@name Operators
   ///@{

   ///@}
   ///@name Operations
   ///@{

   //*************************************************************************************
   //*************************************************************************************
   // evaluation of the derivative of the yield surface respect the stresses
   VectorType &CalculateDeltaStressYieldCondition(const PlasticDataType &rVariables, VectorType &rDeltaStressYieldCondition) override
   {
      KRATOS_TRY

      const ModelDataType &rModelData = rVariables.GetModelData();
      const MatrixType &rStressMatrix = rModelData.GetStressMatrix();

      // Material Parameters
      const Properties &rProperties = rModelData.GetProperties();
      const double &rShearM = rProperties[CRITICAL_STATE_LINE];
      const double &rM = rProperties[CASM_M];
      const double &rFriction = rProperties[INTERNAL_FRICTION_ANGLE];

      //1- stress invariants and deviative vectors

      double MeanStress, J2sqrt, LodeAngle;

      // more work is requiered
      StressInvariantsUtilities::CalculateStressInvariants(rStressMatrix, MeanStress, J2sqrt, LodeAngle);

      rDeltaStressYieldCondition.clear();

      if ( rFriction <= 0.0) {
         VectorType V1, V2;
         StressInvariantsUtilities::CalculateDerivativeVectors(rStressMatrix, V1, V2);

         // 2- Evaluate m of equation 2-12

         double StressRatio = sqrt(3.0) * J2sqrt / (-MeanStress);


         double dummy = pow( StressRatio/rShearM, rM) + rM -1.0;
         dummy *= -MeanStress/(rM-1.0);

         double C1 = rM * pow( StressRatio/rShearM, rM-1.0) * sqrt(3.0)*J2sqrt / rShearM /pow( MeanStress, 2.0) - dummy*(rM-1.0)/pow(MeanStress, 2.0);


         double C2 = rM * pow( StressRatio/rShearM, rM-1.0)* sqrt(3.0)/(-MeanStress)/rShearM;

         rDeltaStressYieldCondition = C1 * V1 + C2 * V2;

         //std::cout << " P " << MeanStress << " J2sqrt " << J2sqrt << " stressRatio " << StressRatio << " m " << m << " C1 " << C1 << " C2 " << C2 << std::endl;
      } 

      {
         double ThirdInvariantEffect = 1.0;
         ThirdInvariantEffect = ShapeAtDeviatoricPlaneUtility::EvaluateEffectDueToThirdInvariant(ThirdInvariantEffect, LodeAngle, rFriction);

         double StressRatio = sqrt(3.0) * J2sqrt / (-MeanStress);

         double dummy = pow( StressRatio*ThirdInvariantEffect/rShearM, rM) + rM -1.0;
         dummy *= -MeanStress/(rM-1.0);

         Vector s1(6); 
         noalias( s1 ) = ZeroVector(6);
         Vector s2(6);
         noalias( s2 ) = ZeroVector(6);
         Vector dg(6);
         noalias( dg ) = ZeroVector(6);

         double g1, g2;
         double delta = 1E-8;

         for (unsigned int i = 0; i < 6; i++) {
            ConstitutiveModelUtilities::StressTensorToVector ( rStressMatrix, s1);
            ConstitutiveModelUtilities::StressTensorToVector ( rStressMatrix, s2);
            s1(i) += delta;
            s2(i) -= delta;

            g1 = EvaluateGFunction(g1, s1, rShearM, rM, rFriction, dummy);
            g2 = EvaluateGFunction(g2, s2, rShearM, rM, rFriction, dummy);

            dg(i) = (g1-g2)/(2.0*delta);
         }

         VectorType DeltaStressYieldCondition;
         DeltaStressYieldCondition = dg;
    
         /*std::cout << std::endl;
         Vector StressVector(6);
         noalias( StressVector) = ZeroVector(6);
         ConstitutiveModelUtilities::StressTensorToVector( rStressMatrix, StressVector);
         std::cout << " STRESS " << StressVector << std::endl;

         std::cout << " PP 1 : " << rDeltaStressYieldCondition << std::endl;
         std::cout << " PP 2 : " << DeltaStressYieldCondition << std::endl;
         std::cout << std::endl;
         std::cout << std::endl;
         std::cout << std::endl;
         std::cout << std::endl;*/
         if ( rFriction > 0.0) {
            rDeltaStressYieldCondition = DeltaStressYieldCondition;
         }

      }
      if (rProperties.Has(NORMALIZE_FLOW_RULE))
      {
         if (rProperties[NORMALIZE_FLOW_RULE] > 0)
         {
            double norm = 0.0;
            for (unsigned int ii = 0; ii < 3; ii++)
               norm += pow(rDeltaStressYieldCondition(ii), 2);
            for (unsigned int ii = 3; ii < 6; ii++)
               norm += 2.0 * pow(rDeltaStressYieldCondition(ii) / 2.0, 2);
            norm = sqrt(norm);
            if (norm > 1e-1)
               rDeltaStressYieldCondition /= norm;
         }
      }

      return rDeltaStressYieldCondition;

      KRATOS_CATCH(" ")
   }

   ///@}
   ///@name Access
   ///@{

   ///@}
   ///@name Inquiry
   ///@{

   ///@}
   ///@name Input and output
   ///@{

   /// Turn back information as a string.
   std::string Info() const override
   {
      std::stringstream buffer;
      buffer << "MMPlasticPotential";
      return buffer.str();
   }

   /// Print information about this object.
   void PrintInfo(std::ostream &rOStream) const override
   {
      rOStream << "MMPlasticPotential";
   }

   /// Print object's data.
   void PrintData(std::ostream &rOStream) const override
   {
      rOStream << "MMPlasticPotential Data";
   }

   ///@}
   ///@name Friends
   ///@{

   ///@}

protected:
   ///@name Protected static Member Variables
   ///@{

   ///@}
   ///@name Protected member Variables
   ///@{

   ///@}
   ///@name Protected Operators
   ///@{

   ///@}
   ///@name Protected Operations
   ///@{

   double & EvaluateGFunction(double & rG, const Vector & rStressVector, const double & rShearM, const double & rM, const double & rFriction, const double & dummy)
   {
      KRATOS_TRY



      double MeanStress, J2sqrt, LodeAngle;
      StressInvariantsUtilities::CalculateStressInvariants(rStressVector, MeanStress, J2sqrt, LodeAngle);
      double StressRatio = sqrt(3.0) * J2sqrt / (-MeanStress);

      double ThirdInvariantEffect = 1.0;
      ThirdInvariantEffect = ShapeAtDeviatoricPlaneUtility::EvaluateEffectDueToThirdInvariant(ThirdInvariantEffect, LodeAngle, rFriction);

      double ShearM = rShearM/ThirdInvariantEffect;

      rG = pow( StressRatio/ ShearM, rM) + rM - (dummy)*(rM-1.0)/(-MeanStress) - 1.0;

      return rG;

      KRATOS_CATCH("")
   }

   ///@}
   ///@name Protected  Access
   ///@{

   ///@}
   ///@name Protected Inquiry
   ///@{

   ///@}
   ///@name Protected LifeCycle
   ///@{

   ///@}

private:
   ///@name Static Member Variables
   ///@{

   ///@}
   ///@name Member Variables
   ///@{

   ///@}
   ///@name Private Operators
   ///@{

   ///@}
   ///@name Private Operations
   ///@{

   ///@}
   ///@name Private  Access
   ///@{

   ///@}
   ///@name Serialization
   ///@{
   friend class Serializer;

   void save(Serializer &rSerializer) const override
   {
      KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BaseType)
   }

   void load(Serializer &rSerializer) override
   {
      KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BaseType)
   }

   ///@}
   ///@name Private Inquiry
   ///@{

   ///@}
   ///@name Un accessible methods
   ///@{

   ///@}

}; // Class MMPlasticPotential

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_MM_PLASTIC_POTENTIAL_HPP_INCLUDED  defined
