//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:       Kateryna Oliynyk $
//   Maintained by:       $Maintainer:                    KO $
//   Date:                $Date:                    Nov 2020 $
//
//

#if !defined(KRATOS_FDMILAN_PLASTIC_POTENTIAL_HPP_INCLUDED)
#define KRATOS_FDMILAN_PLASTIC_POTENTIAL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/yield_surfaces/yield_surface.hpp"
#include "custom_utilities/stress_invariants_utilities.hpp"
#include "custom_utilities/shape_deviatoric_plane_utilities.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
    */
template <class THardeningRule>
class FDMilanPlasticPotential : public YieldSurface<THardeningRule>
{
public:
   ///@name Type Definitions
   ///@{

   typedef ConstitutiveModelData::MatrixType MatrixType;
   typedef ConstitutiveModelData::VectorType VectorType;
   typedef ConstitutiveModelData::ModelData ModelDataType;
   typedef ConstitutiveModelData::MaterialData MaterialDataType;

   typedef YieldSurface<THardeningRule> BaseType;
   typedef typename YieldSurface<THardeningRule>::Pointer BaseTypePointer;
   typedef typename BaseType::PlasticDataType PlasticDataType;

   /// Pointer definition of FDMilanPlasticPotential
   KRATOS_CLASS_POINTER_DEFINITION(FDMilanPlasticPotential);

   ///@}
   ///@name Life Cycle
   ///@{

   /// Default constructor.
   FDMilanPlasticPotential() : BaseType() {}

   /// Copy constructor.
   FDMilanPlasticPotential(FDMilanPlasticPotential const &rOther) : BaseType(rOther) {}

   /// Assignment operator.
   FDMilanPlasticPotential &operator=(FDMilanPlasticPotential const &rOther)
   {
      BaseType::operator=(rOther);
      return *this;
   }

   /// Clone.
   BaseTypePointer Clone() const override
   {
      return Kratos::make_shared<FDMilanPlasticPotential>(*this);
   }

   /// Destructor.
   ~FDMilanPlasticPotential() override {}

   ///@}
   ///@name Operators
   ///@{

   ///@}
   ///@name Operations
   ///@{

   //*************************************************************************************
   //*************************************************************************************

   // evaluation of the derivative of the plastic potential surface respect the stresses

   virtual VectorType &CalculateDeltaStressYieldCondition(const PlasticDataType &rVariables, VectorType &rDeltaStressYieldCondition) override
  {
    KRATOS_TRY

    const ModelDataType &rModelData = rVariables.GetModelData();
    const MatrixType &rStressMatrix = rModelData.GetStressMatrix();

    const double &rPT = rVariables.Data.Internal(4);

//    std::cout << " --- enter CalculateDeltaStressYieldCondition (g) --- " << std::endl;
//    std::cout << " PT     :" << rPT << std::endl;

    // calculate sig* = sig + pt

    MatrixType StressTranslated;
    PerformStressTranslation(rStressMatrix, StressTranslated, rPT);

    // Material Parameters Mgc and phi_g = 3Mgc/(6+Mgc)

    const Properties &rMaterialProperties = rModelData.GetProperties();
    const double &rShearMPP = rMaterialProperties[CRITICAL_STATE_LINE_PP];

    // recover alpha_g and mu_g

    const double &ralpha_g = rMaterialProperties[ALPHA_G];
    const double &rmu_g = rMaterialProperties[MU_G];

//    std::cout << " alpha_g :" << ralpha_g << std::endl;
//    std::cout << " mu_g    :" << rmu_g << std::endl;

    // recover equivalent "friction angle"

    double FrictionPP = 0;
    if (rMaterialProperties.Has(INTERNAL_FRICTION_ANGLE))
    {
      double SinPhi = 3.0*rShearMPP/(6.0+rShearMPP);
      FrictionPP = std::asin(SinPhi);
    }

    // compute stress invariants p* = MeanStress, J = J2sqrt, theta = LodeAngle (MeanStressSM = -MeanStress positive in compression)
    // and derivatives of invariants w.r. to sigma: V1 = dp/dsig, V2 = dJ/dsig, V3 = s^2 - (2/3)*J^2*I

    double MeanStress, J2sqrt, LodeAngle;
    VectorType V1, V2, V3;

    StressInvariantsUtilities::CalculateStressInvariants(StressTranslated, MeanStress, J2sqrt, LodeAngle);
    StressInvariantsUtilities::CalculateDerivativeVectors(StressTranslated, V1, V2, V3);

//    std::cout << " p_star :" << MeanStress << ", J :" << J2sqrt << ", theta :" << LodeAngle << std::endl;

    // calculate Lode angle function and its derivative

    double ThirdInvariantEffect(1), DerivativeEffect(0), Mth, Mth3;
    if (FrictionPP > 0.0)
    {
      ShapeAtDeviatoricPlaneUtility::CalculateKLodeCoefficients(ThirdInvariantEffect, DerivativeEffect, LodeAngle, FrictionPP, true);
      //std::cout << " LodeAngle " << LodeAngle*180.0/Globals::Pi << " effect " << ThirdInvariantEffect << std::endl;
    }

    Mth = rShearMPP/ThirdInvariantEffect;
    Mth3 = Mth/sqrt(3.0);

//    std::cout << " Mth :" << Mth << std::endl;

    // compute functions K1g, K2g, Cg

    double tmp1, tmp2, K1g, K2g, CCg;

    tmp1 = rmu_g*(1.0-ralpha_g)/(2.0*(1.0-rmu_g));
    tmp2 = 1.0 - 4.0*ralpha_g*(1.0-rmu_g)/(rmu_g*(1.0-ralpha_g)*(1.0-ralpha_g));

    K1g = tmp1*(1.0 + sqrt(tmp2));
    K2g = tmp1*(1.0 - sqrt(tmp2));
    CCg  = (1.0-rmu_g)*(K1g-K2g);

    // compute eta_max if it exists - Note: etadash = (1/Mth)*(-J/p*)

    bool ExistsEtaJMax;
    double EtaJMax = 1.0e6;

    if (ralpha_g > 1.0){
      ExistsEtaJMax = true;
      EtaJMax = -K2g*Mth3;
    }
    else if (rmu_g > 1.0){
      ExistsEtaJMax = true;
      EtaJMax = -K1g*Mth3;
    }
    else{
      ExistsEtaJMax = false;
    }

//    std::cout << " ExistsEtaJMax (g) :" << ExistsEtaJMax << std::endl;
//    std::cout << " EtaJMax (g):" << EtaJMax << std::endl;

    // compute derivatives of g wr to p, J, theta

    double EtaJ, Ag, Bg;
    double Aux1,Aux2;
    double dPhidth, dDeltadth;
    double dgdp, dgdJ, dgdtheta;

    if(abs(MeanStress)>1.0e-6){
      EtaJ = -J2sqrt/MeanStress;
    }
    else{
      EtaJ = J2sqrt/1.0e-6;
    }

    // Compute dgdp, dgdJ, dgdtheta values for 3 different regions in p* : J space

    if(ExistsEtaJMax) {
      if ((MeanStress < 0) && (EtaJ < EtaJMax)) {
        // Zone 1 - Lagioia et al. surface
        Ag = 1.0 + EtaJ/(K1g*Mth3);
        Bg = 1.0 + EtaJ/(K2g*Mth3);
        Aux1 = (1.0/Ag - 1.0/Bg)/CCg;
        dgdp = -1.0 + Aux1*EtaJ/Mth3;
        dgdJ = Aux1/Mth3;
        dgdtheta = Aux1*sqrt(3.0)*J2sqrt*DerivativeEffect/rShearMPP;
//        std::cout << " Zone 1 " << std::endl;
//        std::cout << "   Ag :" << Ag << std::endl;
//        std::cout << "   Bg :" << Bg << std::endl;
//        std::cout << " Aux1 :" << Aux1 << std::endl;
      }
      else if((MeanStress >= 0) && (J2sqrt < sqrt(3)*MeanStress/Mth)) {
        // Zone 3 - circular surface
        double DeltaEta = EtaJMax - Mth3;
        double Alpha = std::atan(Mth3);
        double Phi = std::cos(Alpha);
        double radius = sqrt(pow(MeanStress,2)+pow(J2sqrt,2));
        Aux1 = DerivativeEffect/ThirdInvariantEffect;
        Aux2 = Mth3/(1.0+pow(Mth3,2));
        dDeltadth = Mth3*Aux1;
        dPhidth = std::sin(Alpha)*Aux1*Aux2;
        dgdp = MeanStress/(Phi*DeltaEta*radius);
        dgdJ = J2sqrt/(Phi*DeltaEta*radius);
        dgdtheta = -radius*(dDeltadth/DeltaEta + dPhidth/Phi);
        dgdtheta /= (Phi*DeltaEta);
//        std::cout << " Zone 2 " << std::endl;
      }
      else{
        // Zone 2 - Linear surface
        double DeltaEta = EtaJMax - Mth3;
        dgdp = Mth3/DeltaEta;
        dgdJ = 1.0/DeltaEta;
        dgdtheta = -Mth3*(J2sqrt+EtaJMax*MeanStress)/pow(DeltaEta,2);
        dgdtheta *= DerivativeEffect/ThirdInvariantEffect;
//        std::cout << " Zone 3 " << std::endl;
//        std::cout << "     EtaJ :" << EtaJ << std::endl;
//        std::cout << " DeltaEta :" << DeltaEta << std::endl;
      }
    }
    else{
      if (MeanStress < 0) {
        // Zone 1 - Lagioia et al. surface
        Ag = 1.0 + EtaJ/(K1g*Mth3);
        Bg = 1.0 + EtaJ/(K2g*Mth3);
        Aux1 = (1.0/Ag - 1.0/Bg)/CCg;
        dgdp = -1.0 + Aux1*EtaJ/Mth3;
        dgdJ = Aux1/Mth3;
        dgdtheta = Aux1*sqrt(3.0)*J2sqrt*DerivativeEffect/rShearMPP;
//        std::cout << " Zone 1 " << std::endl;
//        std::cout << "   Ag :" << Ag << std::endl;
//        std::cout << "   Bg :" << Bg << std::endl;
//        std::cout << " Aux1 :" << Aux1 << std::endl;
      }
      else{
        // Zona 2 - p* > 0 region
        double Slope = 1.0;
        dgdp = Slope;
        dgdJ = 0.0;
        dgdtheta = 0.0;
      }
    }

//    std::cout << "     dgdp :" << dgdp << std::endl;
//    std::cout << "     dgdJ :" << dgdJ << std::endl;
//    std::cout << " dgdtheta :" << dgdtheta << std::endl;

    // calculate dg/dsig = dg/dp*dp/dsig + dg/dJ*dJ/dsig + dg/dtheta*dtheta/dsig

    rDeltaStressYieldCondition = dgdp * V1 + dgdJ * V2;

    if (FrictionPP > 0.0 && J2sqrt > 1E-6){
       double C2, C3;
       C2 = -std::tan(3.0 * LodeAngle) / J2sqrt * dgdtheta;
       C3 = -sqrt(3.0) / 2.0 / pow(J2sqrt, 3) / std::cos(3.0 * LodeAngle) * dgdtheta;
       rDeltaStressYieldCondition += C2 * V2 + C3 * V3;
    }

//    std::cout << " --- exit  CalculateDeltaStressYieldCondition (g) --- " << std::endl;

    return rDeltaStressYieldCondition;

    KRATOS_CATCH(" ")
  }

  /**
     * Get required properties
     */
  void GetRequiredProperties(Properties &rProperties) override
  {
    KRATOS_TRY

    BaseType::GetRequiredProperties(rProperties);

    // Set required properties
    double value = 0.0; //dummy
    rProperties.SetValue(CRITICAL_STATE_LINE_PP,value);
    rProperties.SetValue(ALPHA_G,value);
    rProperties.SetValue(MU_G,value);

    KRATOS_CATCH(" ")
  }

   ///@}
   ///@name Access
   ///@{

   ///@}
   ///@name Inquiry
   ///@{

   ///@}
   ///@name Input and output
   ///@{

   /// Turn back information as a string.
   std::string Info() const override
   {
      std::stringstream buffer;
      buffer << "FDMilanPlasticPotential";
      return buffer.str();
   }

   /// Print information about this object.
   void PrintInfo(std::ostream &rOStream) const override
   {
      rOStream << "FDMilanPlasticPotential";
   }

   /// Print object's data.
   void PrintData(std::ostream &rOStream) const override
   {
      rOStream << "FDMilanPlasticPotential Data";
   }

   ///@}
   ///@name Friends
   ///@{

   ///@}

protected:
   ///@name Protected static Member Variables
   ///@{

   ///@}
   ///@name Protected member Variables
   ///@{

   ///@}
   ///@name Protected Operators
   ///@{

   ///@}
   ///@name Protected Operations
   ///@{

   void PerformStressTranslation(const MatrixType &rStressMatrix, MatrixType &rStressTranslated, const double &rTranslation)
  {
    KRATOS_TRY

    rStressTranslated.clear();
    rStressTranslated = rStressMatrix;
    for (unsigned int i = 0; i < 3; i++)
      rStressTranslated(i, i) += rTranslation;

    KRATOS_CATCH("")
  }

   ///@}
   ///@name Protected  Access
   ///@{

   ///@}
   ///@name Protected Inquiry
   ///@{

   ///@}
   ///@name Protected LifeCycle
   ///@{

   ///@}

private:
   ///@name Static Member Variables
   ///@{

   ///@}
   ///@name Member Variables
   ///@{

   ///@}
   ///@name Private Operators
   ///@{

   ///@}
   ///@name Private Operations
   ///@{

   ///@}
   ///@name Private  Access
   ///@{

   ///@}
   ///@name Serialization
   ///@{
   friend class Serializer;

   void save(Serializer &rSerializer) const override
   {
      KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BaseType)
   }

   void load(Serializer &rSerializer) override
   {
      KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BaseType)
   }

   ///@}
   ///@name Private Inquiry
   ///@{

   ///@}
   ///@name Un accessible methods
   ///@{

   ///@}

}; // Class FDMilanPlasticPotential

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_FDMILAN_PLASTIC_POTENTIAL_HPP_INCLUDED  defined
