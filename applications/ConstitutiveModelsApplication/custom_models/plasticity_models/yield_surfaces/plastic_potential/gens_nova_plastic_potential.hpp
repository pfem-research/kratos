//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:               MCiantia $
//   Maintained by:       $Maintainer:                    MC $
//   Date:                $Date:                    Jan 2020 $
//
//

#if !defined(KRATOS_GENS_NOVA_PLASTIC_POTENTIAL_HPP_INCLUDED)
#define KRATOS_GENS_NOVA_PLASTIC_POTENTIAL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/yield_surfaces/yield_surface.hpp"
#include "custom_utilities/stress_invariants_utilities.hpp"
#include "custom_utilities/shape_deviatoric_plane_utilities.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
    */
template <class THardeningRule>
class GensNovaPlasticPotential : public YieldSurface<THardeningRule>
{
public:
   ///@name Type Definitions
   ///@{

   typedef ConstitutiveModelData::MatrixType MatrixType;
   typedef ConstitutiveModelData::VectorType VectorType;
   typedef ConstitutiveModelData::ModelData ModelDataType;
   typedef ConstitutiveModelData::MaterialData MaterialDataType;

   typedef YieldSurface<THardeningRule> BaseType;
   typedef typename YieldSurface<THardeningRule>::Pointer BaseTypePointer;
   typedef typename BaseType::PlasticDataType PlasticDataType;

   /// Pointer definition of GensNovaPlasticPotential
   KRATOS_CLASS_POINTER_DEFINITION(GensNovaPlasticPotential);

   ///@}
   ///@name Life Cycle
   ///@{

   /// Default constructor.
   GensNovaPlasticPotential() : BaseType() {}

   /// Copy constructor.
   GensNovaPlasticPotential(GensNovaPlasticPotential const &rOther) : BaseType(rOther) {}

   /// Assignment operator.
   GensNovaPlasticPotential &operator=(GensNovaPlasticPotential const &rOther)
   {
      BaseType::operator=(rOther);
      return *this;
   }

   /// Clone.
   BaseTypePointer Clone() const override
   {
      return Kratos::make_shared<GensNovaPlasticPotential>(*this);
   }

   /// Destructor.
   ~GensNovaPlasticPotential() override {}

   ///@}
   ///@name Operators
   ///@{

   ///@}
   ///@name Operations
   ///@{

   //*************************************************************************************
   //*************************************************************************************
   // evaluation of the derivative of the yield surface respect the stresses
   virtual VectorType &CalculateDeltaStressYieldCondition(const PlasticDataType &rVariables, VectorType &rDeltaStressYieldCondition) override
  {
    KRATOS_TRY

    const ModelDataType &rModelData = rVariables.GetModelData();
    const MatrixType &rStressMatrix = rModelData.GetStressMatrix();

    const double &rPT = rVariables.Data.Internal(4);

    // Perform the stress translation
    MatrixType StressTranslated;
    PerformStressTranslation(rStressMatrix, StressTranslated, rPT);

    // Material Parameters
    const Properties &rMaterialProperties = rModelData.GetProperties();
    const double &rShearMPP = rMaterialProperties[CRITICAL_STATE_LINE_PP];
    double Friction = 0;
    if (rMaterialProperties.Has(INTERNAL_FRICTION_ANGLE))
      Friction = rMaterialProperties[INTERNAL_FRICTION_ANGLE];

    double MeanStress, J2sqrt, LodeAngle;

    VectorType V1, V2, V3;
    // more work is requiered
    StressInvariantsUtilities::CalculateStressInvariants(StressTranslated, MeanStress, J2sqrt, LodeAngle);
    StressInvariantsUtilities::CalculateDerivativeVectors(StressTranslated, V1, V2, V3);

    double ThirdInvariantEffect(1), DerivativeEffect(0);
    bool MultiplySinInstead = false;
    if (Friction > 0.0)
    {
      ShapeAtDeviatoricPlaneUtility::CalculateKLodeCoefficients(ThirdInvariantEffect, DerivativeEffect, LodeAngle, Friction, true, MultiplySinInstead);
      //std::cout << " LodeAngle " << LodeAngle*180.0/Globals::Pi << " effect " << ThirdInvariantEffect << std::endl;
    }

    double PreconsolidationStress;
      PreconsolidationStress = MeanStress + 3.0 / MeanStress * pow(J2sqrt / (rShearMPP / ThirdInvariantEffect), 2);

    rDeltaStressYieldCondition = (2.0 * MeanStress - PreconsolidationStress) * V1 + 2.0 * 3.0 * pow(ThirdInvariantEffect / rShearMPP, 2) * J2sqrt * V2;

    if (Friction > 0.0 && J2sqrt > 1E-6)
    {
               double Term = pow(sqrt(3.0) * J2sqrt / rShearMPP, 2) * ThirdInvariantEffect * 2.0 * DerivativeEffect;
               double C2(0);
               double C3(0);
               if ( MultiplySinInstead){
                  C2 = -std::sin(3.0 * LodeAngle) / J2sqrt * Term;
                  C3 = -sqrt(3.0) / 2.0 / pow(J2sqrt, 3) * Term;
               }
               else{
                  C2 = -std::tan(3.0 * LodeAngle) / J2sqrt * Term;
                  C3 = -sqrt(3.0) / 2.0 / pow(J2sqrt, 3) / std::cos(3.0 * LodeAngle) * Term;
               }

      rDeltaStressYieldCondition += C2 * V2 + C3 * V3;
    }

    return rDeltaStressYieldCondition;

    KRATOS_CATCH(" ")
  }

   ///@}
   ///@name Access
   ///@{

   ///@}
   ///@name Inquiry
   ///@{

   ///@}
   ///@name Input and output
   ///@{

   /// Turn back information as a string.
   std::string Info() const override
   {
      std::stringstream buffer;
      buffer << "GensNovaPlasticPotential";
      return buffer.str();
   }

   /// Print information about this object.
   void PrintInfo(std::ostream &rOStream) const override
   {
      rOStream << "GensNovaPlasticPotential";
   }

   /// Print object's data.
   void PrintData(std::ostream &rOStream) const override
   {
      rOStream << "GensNovaPlasticPotential Data";
   }

   ///@}
   ///@name Friends
   ///@{

   ///@}

protected:
   ///@name Protected static Member Variables
   ///@{

   ///@}
   ///@name Protected member Variables
   ///@{

   ///@}
   ///@name Protected Operators
   ///@{

   ///@}
   ///@name Protected Operations
   ///@{

   void PerformStressTranslation(const MatrixType &rStressMatrix, MatrixType &rStressTranslated, const double &rTranslation)
  {
    KRATOS_TRY

    rStressTranslated.clear();
    rStressTranslated = rStressMatrix;
    for (unsigned int i = 0; i < 3; i++)
      rStressTranslated(i, i) += rTranslation;

    KRATOS_CATCH("")
  }

   ///@}
   ///@name Protected  Access
   ///@{

   ///@}
   ///@name Protected Inquiry
   ///@{

   ///@}
   ///@name Protected LifeCycle
   ///@{

   ///@}

private:
   ///@name Static Member Variables
   ///@{

   ///@}
   ///@name Member Variables
   ///@{

   ///@}
   ///@name Private Operators
   ///@{

   ///@}
   ///@name Private Operations
   ///@{

   ///@}
   ///@name Private  Access
   ///@{

   ///@}
   ///@name Serialization
   ///@{
   friend class Serializer;

   void save(Serializer &rSerializer) const override
   {
      KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BaseType)
   }

   void load(Serializer &rSerializer) override
   {
      KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BaseType)
   }

   ///@}
   ///@name Private Inquiry
   ///@{

   ///@}
   ///@name Un accessible methods
   ///@{

   ///@}

}; // Class GensNovaPlasticPotential

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_GENS_NOVA_PLASTIC_POTENTIAL_HPP_INCLUDED  defined
