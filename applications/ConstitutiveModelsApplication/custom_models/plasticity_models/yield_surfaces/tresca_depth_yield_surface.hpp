//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:              September 2020 $
//
//

#if !defined(KRATOS_TRESCA_DEPTH_YIELD_SURFACE_HPP_INCLUDED)
#define KRATOS_TRESCA_DEPTH_YIELD_SURFACE_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/yield_surfaces/tresca_yield_surface.hpp"
#include "custom_utilities/stress_invariants_utilities.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
template <class THardeningRule>
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) TrescaDepthYieldSurface : public TrescaYieldSurface<THardeningRule>
{
public:
  ///@name Type Definitions
  ///@{

  typedef ConstitutiveModelData::MatrixType MatrixType;
  typedef ConstitutiveModelData::VectorType VectorType;
  typedef ConstitutiveModelData::ModelData ModelDataType;
  typedef ConstitutiveModelData::MaterialData MaterialDataType;

  typedef TrescaYieldSurface<THardeningRule> BaseType;
  typedef typename BaseType::Pointer BaseTypePointer;
  typedef typename YieldSurface<THardeningRule>::Pointer YieldSurfacePointer;
  typedef typename BaseType::PlasticDataType PlasticDataType;

  /// Pointer definition of TrescaDepthYieldSurface
  KRATOS_CLASS_POINTER_DEFINITION(TrescaDepthYieldSurface);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  TrescaDepthYieldSurface() : BaseType() {}

  /// Copy constructor.
  TrescaDepthYieldSurface(TrescaDepthYieldSurface const &rOther) : BaseType(rOther) {}

  /// Assignment operator.
  TrescaDepthYieldSurface &operator=(TrescaDepthYieldSurface const &rOther)
  {
    BaseType::operator=(rOther);
    return *this;
  }

  /// Clone.
  virtual YieldSurfacePointer Clone() const override
  {
    return YieldSurfacePointer(new TrescaDepthYieldSurface(*this));
  }

  /// Destructor.
  ~TrescaDepthYieldSurface() override {}

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /**
     * Calculate Yield Condition
     */

  virtual double &CalculateYieldCondition(const PlasticDataType &rVariables, double &rYieldCondition) override
  {
    KRATOS_TRY

    const ModelDataType &rModelData = rVariables.GetModelData();

    const MatrixType &rStressMatrix = rModelData.GetStressMatrix();

    // Material Parameters
    // const double &rYieldStress = rMaterialProperties[YIELD_STRESS];
    const double & rYieldStress = rVariables.Data.Internal(5);

    double MeanStress, LodeAngle, J2sqrt;
    // more work is requiered
    StressInvariantsUtilities::CalculateStressInvariants(rStressMatrix, MeanStress, J2sqrt, LodeAngle);

    double LodeCut = this->GetSmoothingLodeAngle();

    if (fabs(LodeAngle) < LodeCut)
    {

      rYieldCondition = J2sqrt * std::cos(LodeAngle);
    }
    else
    {

      double ASmoothing, BSmoothing, CSmoothing;
      this->CalculateSmoothingInvariants(ASmoothing, BSmoothing, CSmoothing, LodeAngle);

      rYieldCondition = J2sqrt * (ASmoothing + BSmoothing * std::sin(3.0 * LodeAngle) + CSmoothing * pow(std::sin(3.0 * LodeAngle), 2));
    }

    rYieldCondition = rYieldCondition - rYieldStress;

    return rYieldCondition;

    KRATOS_CATCH(" ")
  }


  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  virtual std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "TrescaDepthYieldSurface";
    return buffer.str();
  }

  /// Print information about this object.
  virtual void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "TrescaDepthYieldSurface";
  }

  /// Print object's data.
  virtual void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "TrescaDepthYieldSurface Data";
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  virtual void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BaseType)
  }

  virtual void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BaseType)
  }

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class TrescaDepthYieldSurface

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_TRECA_DEPTH_YIELD_SURFACE_HPP_INCLUDED  defined
