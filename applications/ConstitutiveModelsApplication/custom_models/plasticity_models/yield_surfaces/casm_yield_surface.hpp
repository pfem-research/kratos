//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:                LHauser $
//   Maintained by:       $Maintainer:                    LH $
//   Date:                $Date:                  April 2017 $
//
//

#if !defined(KRATOS_CASM_YIELD_SURFACE_HPP_INCLUDED)
#define KRATOS_CASM_YIELD_SURFACE_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/yield_surfaces/non_associative_yield_surface.hpp"
#include "custom_utilities/stress_invariants_utilities.hpp"
#include "custom_utilities/shape_deviatoric_plane_utilities.hpp"

// Variables
// 0 Plastic Multiplier
// 1 Volumetric Plastic Strain
// 2 Dev Plastic Strain
// 3 Abs Value Volumetric Plastic Strain
// 4 B (bounding)
// 5 pc preconsolidation
// 6 pt
// 7 ps

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
template <class THardeningRule>
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) CasmYieldSurface : public NonAssociativeYieldSurface<THardeningRule>
{
public:
  ///@name Type Definitions
  ///@{

  typedef ConstitutiveModelData::MatrixType MatrixType;
  typedef ConstitutiveModelData::VectorType VectorType;
  typedef ConstitutiveModelData::ModelData ModelDataType;
  typedef ConstitutiveModelData::MaterialData MaterialDataType;

  typedef NonAssociativeYieldSurface<THardeningRule> BaseType;
  typedef typename YieldSurface<THardeningRule>::Pointer BaseTypePointer;
  typedef typename BaseType::PlasticDataType PlasticDataType;

  /// Pointer definition of CasmYieldSurface
  KRATOS_CLASS_POINTER_DEFINITION(CasmYieldSurface);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  CasmYieldSurface() : BaseType() {}

  /// Default constructor.
  CasmYieldSurface(BaseTypePointer const &rpPlasticPotential) : BaseType(rpPlasticPotential) {}

  /// Copy constructor.
  CasmYieldSurface(CasmYieldSurface const &rOther) : BaseType(rOther) {}

  /// Assignment operator.
  CasmYieldSurface &operator=(CasmYieldSurface const &rOther)
  {
    BaseType::operator=(rOther);
    return *this;
  }

  /// Clone.
  virtual BaseTypePointer Clone() const override
  {
    return BaseTypePointer(new CasmYieldSurface(*this));
  }

  /// Destructor.
  ~CasmYieldSurface() override {}

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /**
     * Calculate Yield Condition
     */

  virtual double &CalculateYieldCondition(const PlasticDataType &rVariables, double &rYieldCondition) override
  {
    KRATOS_TRY

    const ModelDataType &rModelData = rVariables.GetModelData();
    const Properties &rMaterialProperties = rModelData.GetProperties();
    const MatrixType &rStressMatrix = rModelData.GetStressMatrix();

    //get constants
    const double &rShearM = rMaterialProperties[CRITICAL_STATE_LINE];
    const double &rFriction = rMaterialProperties[INTERNAL_FRICTION_ANGLE];
    const double &rSpacingR = rMaterialProperties[SPACING_RATIO];
    const double &rShapeN = rMaterialProperties[SHAPE_PARAMETER];

    //get internal variables
    const double &rP0 = rVariables.Data.Internal(5);

    //calculate stress invariants
    double MeanStress, J2sqrt, LodeAngle;
    StressInvariantsUtilities::CalculateStressInvariants(rStressMatrix, MeanStress, J2sqrt, LodeAngle);

    //calcualte third invariant effect
    double ThirdInvEffect = 1.0;
    ShapeAtDeviatoricPlaneUtility::EvaluateEffectDueToThirdInvariant(ThirdInvEffect, LodeAngle, rFriction);

    //evaluate yield function
    rYieldCondition = std::pow(-std::sqrt(3.0) * J2sqrt / ( (rShearM / ThirdInvEffect) * MeanStress), rShapeN);
    rYieldCondition += 1.0 / std::log(rSpacingR) * std::log(MeanStress / rP0);

    return rYieldCondition;

    KRATOS_CATCH(" ")
  }

  //*************************************************************************************
  //*************************************************************************************
  // evaluation of the derivative of the yield surface respect the stresses
  virtual VectorType &CalculateDeltaStressYieldCondition(const PlasticDataType &rVariables, VectorType &rDeltaStressYieldCondition) override
  {
    KRATOS_TRY

    const ModelDataType &rModelData = rVariables.GetModelData();
    const Properties &rMaterialProperties = rModelData.GetProperties();
    const MatrixType &rStressMatrix = rModelData.GetStressMatrix();

    //get constants
    const double &rShearM = rMaterialProperties[CRITICAL_STATE_LINE];
    const double &rFriction = rMaterialProperties[INTERNAL_FRICTION_ANGLE];
    const double &rSpacingR = rMaterialProperties[SPACING_RATIO];
    const double &rShapeN = rMaterialProperties[SHAPE_PARAMETER];

    //get internal variables

    //calculate stress invariants and derivatives
    double MeanStress, J2sqrt, LodeAngle;
    VectorType V1, V2, V3;
    StressInvariantsUtilities::CalculateStressInvariants(rStressMatrix, MeanStress, J2sqrt, LodeAngle);

    //V1.clear(); V2.clear(); V3.clear();
    StressInvariantsUtilities::CalculateDerivativeVectors(rStressMatrix, V1, V2, V3);

    //calculate third invariant effect on M
    double ThirdInvEffect = 1.0;

    ShapeAtDeviatoricPlaneUtility::EvaluateEffectDueToThirdInvariant(ThirdInvEffect, LodeAngle, rFriction);

    //evaluate yield surface derivative df/dp*dp/dsig + df/dJ2sqrt*dJ2sqrt/dsig
    rDeltaStressYieldCondition = (1.0 / (MeanStress * std::log(rSpacingR)) + (rShapeN * std::pow(std::sqrt(3.0) * J2sqrt, rShapeN)) / (std::pow(rShearM / ThirdInvEffect, rShapeN) * pow(-MeanStress, rShapeN + 1.0))) * V1;
    rDeltaStressYieldCondition += ((rShapeN * std::pow(3.0, rShapeN / 2.0) * std::pow(J2sqrt, rShapeN - 1.0)) / (std::pow(rShearM / ThirdInvEffect, rShapeN) * std::pow(-MeanStress, rShapeN))) * V2;

    //add contribution of third inv derivative
    if (rFriction > 1.0E-3 && J2sqrt > 1.0E-5)
    {
      double DerivativeEffect;
      bool MultiplySinInstead = false; 
      ShapeAtDeviatoricPlaneUtility::CalculateKLodeCoefficients(ThirdInvEffect, DerivativeEffect, LodeAngle, rFriction, true, MultiplySinInstead);

      double Term = std::pow(-std::sqrt(3.0) * J2sqrt / (rShearM * MeanStress), rShapeN) * rShapeN * pow( ThirdInvEffect, rShapeN-1.0) * DerivativeEffect;
       double C2(0);
       double C3(0);
       if ( MultiplySinInstead){
          C2 = -std::sin(3.0 * LodeAngle) / J2sqrt * Term;
          C3 = -sqrt(3.0) / 2.0 / pow(J2sqrt, 3) * Term;
       }
       else{
         C2 = -std::tan(3.0 * LodeAngle) / J2sqrt * Term;
         C3 = -sqrt(3.0) / 2.0 / pow(J2sqrt, 3) / std::cos(3.0 * LodeAngle) * Term;
       }



      /*std::cout << " C2 and C3 " << C2 << " : " << C3 << std::endl;
      std::cout << " tan 3 " << std::tan(3.0*LodeAngle) << std::endl;
      std::cout << " inverse 3Cos " << std::cos(3.0*LodeAngle) << " . " << 1.0/std::cos(3.0*LodeAngle) << std::endl;
      std::cout << V2 << std::endl;
      std::cout << V3 << std::endl;
      std::cout << DerivativeEffect << " lode " << LodeAngle << " Third " << ThirdInvEffect <<  std::endl;
      std::cout << " TERM " << Term << std::endl;*/

      rDeltaStressYieldCondition += C2 * V2 + C3 * V3;
    }


    /*
    // now lets calculate the numerical derivative.... just in case....
    VectorType DeltaStressYieldCondition;
    MatrixType s1;
    MatrixType s2;
    MatrixType dg;


    double g1, g2;
    double delta = 1E-9;

    const double& rP0   = rVariables.Data.Internal(5);

    for (unsigned int i = 0; i < 3; i++) {
       for (unsigned int j = 0; j < 3; j++) {
          s1 = rStressMatrix;
          s2 = rStressMatrix;
          s1(i,j) += delta;
          s2(i,j) -= delta;

          g1 = EvaluateFFunction(g1, s1, rShearM, rFriction, rSpacingR, rShapeN, rP0);
          g2 = EvaluateFFunction(g2, s2, rShearM, rFriction, rSpacingR, rShapeN, rP0);

          dg(i,j) = (g1-g2)/(2.0*delta);
       }
    }

    ConstitutiveModelUtilities::StrainTensorToVector( dg, DeltaStressYieldCondition);

    std::cout << std::endl;
    Vector StressVector(6);
    noalias( StressVector) = ZeroVector(6);
    ConstitutiveModelUtilities::StressTensorToVector( rStressMatrix, StressVector);
    std::cout << " STRESS " << StressVector << std::endl;
    std::cout << " rP0 " << rP0 << std::endl;
    std::cout << " YieldFunction   Real " << rDeltaStressYieldCondition << std::endl;
    std::cout << " numerical derivative " << DeltaStressYieldCondition << std::endl;
    std::cout << std::endl;
    std::cout << " Lode " << LodeAngle*180.0/Globals::Pi << " ThirdIva " << ThirdInvEffect << std::endl;
    std::cout << " dKdL " <<  " fri " << rFriction << std::endl;
    std::cout << std::endl;*/

    return rDeltaStressYieldCondition;

    KRATOS_CATCH(" ")
  }

  /**
    * Get required properties
    */
  void GetRequiredProperties(Properties &rProperties) override
  {
    KRATOS_TRY

    // Set required properties
    double value = 0.0; //dummy
    rProperties.SetValue(CRITICAL_STATE_LINE,value);
    rProperties.SetValue(INTERNAL_FRICTION_ANGLE,value);
    rProperties.SetValue(SPACING_RATIO,value);
    rProperties.SetValue(SHAPE_PARAMETER,value);

    BaseType::GetRequiredProperties(rProperties);

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  virtual std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "CasmYieldSurface";
    return buffer.str();
  }

  /// Print information about this object.
  virtual void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "CasmYieldSurface";
  }

  /// Print object's data.
  virtual void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "CasmYieldSurface Data";
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  double & EvaluateFFunction( double & rF, const MatrixType & rStressMatrix, const double & rShearM, const double & rFriction, const double & rSpacingR, const double & rShapeN, const double & rP0)
  {
     KRATOS_TRY

    //calculate stress invariants
    double MeanStress, J2sqrt, LodeAngle;
    StressInvariantsUtilities::CalculateStressInvariants(rStressMatrix, MeanStress, J2sqrt, LodeAngle);

    //calcualte third invariant effect
    double ThirdInvEffect = 1.0;
    ShapeAtDeviatoricPlaneUtility::EvaluateEffectDueToThirdInvariant(ThirdInvEffect, LodeAngle, rFriction);

    //evaluate yield function
    rF = std::pow(-std::sqrt(3.0) * J2sqrt  * ThirdInvEffect / (rShearM  * MeanStress), rShapeN);
    rF += 1.0 / std::log(rSpacingR) * std::log(MeanStress / rP0);

    return rF;


     KRATOS_CATCH("")
  }
  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  virtual void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BaseType)
  }

  virtual void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BaseType)
  }

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class CasmYieldSurface

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_CASM_YIELD_SURFACE_HPP_INCLUDED  defined
