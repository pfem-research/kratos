//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                       $
//   Date:                $Date:                  March 2021 $
//
//

#if !defined(KRATOS_CASM_STRUCTURE_YIELD_SURFACE_HPP_INCLUDED)
#define KRATOS_CASM_STRUCTURE_YIELD_SURFACE_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/yield_surfaces/non_associative_yield_surface.hpp"
#include "custom_utilities/stress_invariants_utilities.hpp"
#include "custom_utilities/shape_deviatoric_plane_utilities.hpp"

// Variables
// 0 Plastic Multiplier
// 1 Volumetric Plastic Strain
// 2 Dev Plastic Strain
// 3 Abs Value Volumetric Plastic Strain
// 4 B (bounding)
// 5 pc preconsolidation
// 6 pt
// 7 ps

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
template <class THardeningRule>
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) CasmStructureYieldSurface : public NonAssociativeYieldSurface<THardeningRule>
{
public:
  ///@name Type Definitions
  ///@{

  typedef ConstitutiveModelData::MatrixType MatrixType;
  typedef ConstitutiveModelData::VectorType VectorType;
  typedef ConstitutiveModelData::ModelData ModelDataType;
  typedef ConstitutiveModelData::MaterialData MaterialDataType;

  typedef NonAssociativeYieldSurface<THardeningRule> BaseType;
  typedef typename YieldSurface<THardeningRule>::Pointer BaseTypePointer;
  typedef typename BaseType::PlasticDataType PlasticDataType;

  /// Pointer definition of CasmStructureYieldSurface
  KRATOS_CLASS_POINTER_DEFINITION(CasmStructureYieldSurface);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  CasmStructureYieldSurface() : BaseType() {}

  /// Default constructor.
  CasmStructureYieldSurface(BaseTypePointer const &rpPlasticPotential) : BaseType(rpPlasticPotential) {}

  /// Copy constructor.
  CasmStructureYieldSurface(CasmStructureYieldSurface const &rOther) : BaseType(rOther) {}

  /// Assignment operator.
  CasmStructureYieldSurface &operator=(CasmStructureYieldSurface const &rOther)
  {
    BaseType::operator=(rOther);
    return *this;
  }

  /// Clone.
  virtual BaseTypePointer Clone() const override
  {
    return BaseTypePointer(new CasmStructureYieldSurface(*this));
  }

  /// Destructor.
  virtual ~CasmStructureYieldSurface() {}

  ///@}
  ///@name Operators
  ///@{
  ///@}
  ///@name Operations
  ///@{

  /**
     * Calculate Yield Condition
     */

  virtual double &CalculateYieldCondition(const PlasticDataType &rVariables, double &rYieldCondition) override
  {
     KRATOS_TRY

     const ModelDataType &rModelData = rVariables.GetModelData();
     const Properties &rMaterialProperties = rModelData.GetProperties();
     const MatrixType &rStressMatrix = rModelData.GetStressMatrix();

     //get constants
     const double &rShearM = rMaterialProperties[CRITICAL_STATE_LINE];
     const double &rFriction = rMaterialProperties[INTERNAL_FRICTION_ANGLE];
     const double &rSpacingR = rMaterialProperties[SPACING_RATIO];
     const double &rShapeN = rMaterialProperties[SHAPE_PARAMETER];

     //get internal variables
     const double &rP0 = rVariables.Data.Internal(4);
     const double &rPM = rVariables.Data.Internal(5);
     const double &rPT = rVariables.Data.Internal(6);

     //calculate stress invariants
     double MeanStress, J2sqrt, LodeAngle;
     StressInvariantsUtilities::CalculateStressInvariants(rStressMatrix, MeanStress, J2sqrt, LodeAngle);

     //calcualte third invariant effect
     double ThirdInvEffect = 1.0;
     ShapeAtDeviatoricPlaneUtility::EvaluateEffectDueToThirdInvariant(ThirdInvEffect, LodeAngle, rFriction);

     //evaluate yield function
     rYieldCondition = std::pow(-std::sqrt(3.0) * J2sqrt / ( (rShearM / ThirdInvEffect) * (MeanStress+ rPT)), rShapeN);
     rYieldCondition += 1.0 / std::log(rSpacingR) * std::log( (MeanStress + rPT) / (rP0+rPM + rPT) );

     return rYieldCondition;

     KRATOS_CATCH(" ")
  }

  //*************************************************************************************
  //*************************************************************************************
  // evaluation of the derivative of the yield surface respect the stresses
  virtual VectorType &CalculateDeltaStressYieldCondition(const PlasticDataType &rVariables, VectorType &rDeltaStressYieldCondition) override
  {
    KRATOS_TRY

    const ModelDataType &rModelData = rVariables.GetModelData();
    const Properties &rMaterialProperties = rModelData.GetProperties();
    const MatrixType &rStressMatrix = rModelData.GetStressMatrix();

    //get constants
    const double &rShearM = rMaterialProperties[CRITICAL_STATE_LINE];
    const double &rFriction = rMaterialProperties[INTERNAL_FRICTION_ANGLE];
    const double &rSpacingR = rMaterialProperties[SPACING_RATIO];
    const double &rShapeN = rMaterialProperties[SHAPE_PARAMETER];

    //get internal variables
    const double &rPT = rVariables.Data.Internal(6);

    //calculate stress invariants and derivatives
    double MeanStress, J2sqrt, LodeAngle;
    VectorType V1, V2, V3;
    StressInvariantsUtilities::CalculateStressInvariants(rStressMatrix, MeanStress, J2sqrt, LodeAngle);

    //V1.clear(); V2.clear(); V3.clear();
    StressInvariantsUtilities::CalculateDerivativeVectors(rStressMatrix, V1, V2, V3);

    //calculate third invariant effect on M
    double ThirdInvariantEffect(1), DerivativeEffect(0);
    bool MultiplySinInstead = false;
    if (rFriction > 0.0)
    {
      ShapeAtDeviatoricPlaneUtility::CalculateKLodeCoefficients(ThirdInvariantEffect, DerivativeEffect, LodeAngle, rFriction, true, MultiplySinInstead);
      //std::cout << " LodeAngle " << LodeAngle*180.0/Globals::Pi << " effect " << ThirdInvariantEffect << std::endl;
    }

    //evaluate yield surface derivative
    double C1, C2;

    //evaluate yield surface derivative df/dp*dp/dsig + df/dJ2sqrt*dJ2sqrt/dsig
    C1 = 1.0/std::log(rSpacingR) / ( MeanStress + rPT);
    C1 += rShapeN * std::pow( std::sqrt(3.0) *J2sqrt / ( rShearM/ThirdInvariantEffect) , rShapeN)  / std::pow( -MeanStress-rPT, rShapeN+1.0);

    C2 = rShapeN * std::pow(3.0, rShapeN/2.0) * std::pow( J2sqrt, rShapeN-1.0) / std::pow(  - rShearM/ThirdInvariantEffect * ( MeanStress + rPT), rShapeN);


    rDeltaStressYieldCondition  = C1*V1 + C2*V2;

    if ( rFriction > 0.0 && J2sqrt > 1E-6)
    {
      double Term = std::pow(-std::sqrt(3.0) * J2sqrt / (rShearM * (MeanStress+rPT) ), rShapeN) * rShapeN * pow( ThirdInvariantEffect, rShapeN-1.0) * DerivativeEffect;

    double C2(0);
    double C3(0);
    if ( MultiplySinInstead){
          C2 = -std::sin(3.0 * LodeAngle) / J2sqrt * Term;
          C3 = -sqrt(3.0) / 2.0 / pow(J2sqrt, 3) * Term;
    }
    else{
         C2 = -std::tan(3.0 * LodeAngle) / J2sqrt * Term;
         C3 = -sqrt(3.0) / 2.0 / pow(J2sqrt, 3) / std::cos(3.0 * LodeAngle) * Term;
    }

      rDeltaStressYieldCondition += C2 * V2 + C3 * V3;
    }

    return rDeltaStressYieldCondition;

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  virtual std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "CasmStructureYieldSurface";
    return buffer.str();
  }

  /// Print information about this object.
  virtual void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "CasmStructureYieldSurface";
  }

  /// Print object's data.
  virtual void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "CasmStructureYieldSurface Data";
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{
  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  virtual void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BaseType)
  }

  virtual void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BaseType)
  }

  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{
  ///@}

}; // Class CasmStructureYieldSurface

///@}

///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_CASM_STRUCTURE_YIELD_SURFACE_HPP_INCLUDED  defined
