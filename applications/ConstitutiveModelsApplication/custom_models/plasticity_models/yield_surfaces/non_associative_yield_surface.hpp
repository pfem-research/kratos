//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:      LMonforte-LHauser $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:                  April 2017 $
//
//

#if !defined(KRATOS_NON_ASSOCIATIVE_YIELD_SURFACE_HPP_INCLUDED)
#define KRATOS_NON_ASSOCIATIVE_YIELD_SURFACE_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/yield_surfaces/yield_surface.hpp"
namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
 */
  template <class THardeningRule>
  class NonAssociativeYieldSurface : public YieldSurface<THardeningRule>
  {
  public:
    ///@name Type Definitions
    ///@{

    typedef ConstitutiveModelData::MatrixType MatrixType;
    typedef ConstitutiveModelData::VectorType VectorType;
    typedef ConstitutiveModelData::ModelData ModelDataType;
    typedef ConstitutiveModelData::MaterialData MaterialDataType;

    typedef YieldSurface<THardeningRule> BaseType;
    typedef typename YieldSurface<THardeningRule>::Pointer BaseTypePointer;
    typedef THardeningRule HardeningRuleType;
    typedef typename THardeningRule::PlasticDataType PlasticDataType;
    typedef typename THardeningRule::InternalVariablesType InternalVariablesType;

    /// Pointer definition of NonAssociativeYieldSurface
    KRATOS_CLASS_POINTER_DEFINITION(NonAssociativeYieldSurface);

    ///@}
    ///@name Life Cycle
    ///@{

    /// Default constructor.
    NonAssociativeYieldSurface() : BaseType()
    {
      mpPlasticPotential = this;
    }

    NonAssociativeYieldSurface(BaseTypePointer const &rpPlasticPotential)
      : BaseType()
    {
      mpSharedPotential = rpPlasticPotential; //stored become owner shared pointer
      mpPlasticPotential = mpSharedPotential.get();
    }

    /// Copy constructor.
    NonAssociativeYieldSurface(NonAssociativeYieldSurface const &rOther)
      : BaseType(rOther)
      , mpPlasticPotential(rOther.mpPlasticPotential)
      , mpSharedPotential(rOther.mpSharedPotential)
    {

    }

    /// Assignment operator.
    NonAssociativeYieldSurface &operator=(NonAssociativeYieldSurface const &rOther)
    {
      BaseType::operator=(rOther);
      mpSharedPotential = rOther.mpSharedPotential;
      mpPlasticPotential = rOther.mpPlasticPotential;

      return *this;
    }

    /// Clone.
    virtual BaseTypePointer Clone() const override
    {
      return BaseTypePointer(new NonAssociativeYieldSurface(*this));
    }

    /// Destructor.
    ~NonAssociativeYieldSurface() override {}

    ///@}
    ///@name Operators
    ///@{

    ///@}
    ///@name Operations
    ///@{

    /**
     * Calculate Plastic Potential Condition Stresses derivative
     */

    virtual VectorType &CalculateDeltaPlasticPotential(const PlasticDataType &rVariables, VectorType &rDeltaPlasticPotential) override
    {
      KRATOS_TRY

      if ( mpPlasticPotential == NULL) {
         rDeltaPlasticPotential = this->CalculateDeltaStressYieldCondition(rVariables, rDeltaPlasticPotential);
      } else if ( mpPlasticPotential == nullptr) {
         rDeltaPlasticPotential = this->CalculateDeltaStressYieldCondition(rVariables, rDeltaPlasticPotential);
      } else if (mpPlasticPotential == this) {
         rDeltaPlasticPotential = this->CalculateDeltaStressYieldCondition(rVariables, rDeltaPlasticPotential);
      } else {
        rDeltaPlasticPotential = mpPlasticPotential->CalculateDeltaStressYieldCondition(rVariables, rDeltaPlasticPotential);
      }
      
      return rDeltaPlasticPotential;

      KRATOS_CATCH(" ")
        }

    /**
     * Calculate Yield Condition Stresses derivative
     */

    virtual VectorType &CalculateDeltaStressInvPlasticPotential(const PlasticDataType &rVariables, VectorType &rDeltaStressInvPlasticPotential) override
    {
      KRATOS_TRY

        // if (mpPlasticPotential)
        // {
        rDeltaStressInvPlasticPotential = mpPlasticPotential->CalculateDeltaStressInvYieldCondition(rVariables, rDeltaStressInvPlasticPotential);
      // }
      // else
      // {
      //   rDeltaStressInvPlasticPotential = this->CalculateDeltaStressInvYieldCondition(rVariables, rDeltaStressInvPlasticPotential);
      // }
      return rDeltaStressInvPlasticPotential;

      KRATOS_CATCH(" ")
        }

    /**
     * Get required properties
     */
    void GetRequiredProperties(Properties &rProperties) override
    {
      KRATOS_TRY

      // Set required properties
      BaseType::GetRequiredProperties(rProperties);

      if (mpPlasticPotential != nullptr){
        std::cout<<" Non-associated plasticity :: PlasticPotential -> "<<mpPlasticPotential->Info()<<std::endl;
        if (mpPlasticPotential != this)
          mpPlasticPotential->GetRequiredProperties(rProperties);
      }

      KRATOS_CATCH(" ")
    }

    ///@}
    ///@name Access
    ///@{

    ///@}
    ///@name Inquiry
    ///@{

    ///@}
    ///@name Input and output
    ///@{

    /// Turn back information as a string.
    virtual std::string Info() const override
    {
      std::stringstream buffer;
      buffer << "NonAssociativeYieldSurface";
      return buffer.str();
    }

    /// Print information about this object.
    virtual void PrintInfo(std::ostream &rOStream) const override
    {
      rOStream << "NonAssociativeYieldSurface";
    }

    /// Print object's data.
    virtual void PrintData(std::ostream &rOStream) const override
    {
      rOStream << "NonAssociativeYieldSurface Data";
    }

    ///@}
    ///@name Friends
    ///@{

    ///@}

  protected:
    ///@name Protected static Member Variables
    ///@{

    ///@}
    ///@name Protected member Variables
    ///@{

    BaseType* mpPlasticPotential = nullptr;

    ///@}
    ///@name Protected Operators
    ///@{

    ///@}
    ///@name Protected Operations
    ///@{

    ///@}
    ///@name Protected  Access
    ///@{

    ///@}
    ///@name Protected Inquiry
    ///@{

    ///@}
    ///@name Protected LifeCycle
    ///@{

    ///@}

  private:
    ///@name Static Member Variables
    ///@{

    ///@}
    ///@name Member Variables
    ///@{

    BaseTypePointer mpSharedPotential = nullptr;

    ///@}
    ///@name Private Operators
    ///@{

    ///@}
    ///@name Private Operations
    ///@{

    ///@}
    ///@name Private  Access
    ///@{

    ///@}
    ///@name Serialization
    ///@{
    friend class Serializer;

    virtual void save(Serializer &rSerializer) const override
    {
      KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BaseType)
      //rSerializer.save("mpSharedPotential", mpSharedPotential);
    }

    virtual void load(Serializer &rSerializer) override
    {
      KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BaseType)
      //rSerializer.load("mpSharedPotential", mpSharedPotential);
      //mpPlasticPotential = mpSharedPotential.get();
    }

    ///@}
    ///@name Private Inquiry
    ///@{

    ///@}
    ///@name Un accessible methods
    ///@{

    ///@}

  }; // Class NonAssociativeYieldSurface

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_YIELD_SURFACE_HPP_INCLUDED  defined
