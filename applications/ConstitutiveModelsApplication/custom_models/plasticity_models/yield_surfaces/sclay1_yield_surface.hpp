//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:               November 2021 $
//
//

#if !defined(KRATOS_SCLAY1_YIELD_SURFACE_HPP_INCLUDED)
#define KRATOS_SCLAY1_YIELD_SURFACE_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/yield_surfaces/yield_surface.hpp"
#include "custom_utilities/stress_invariants_utilities.hpp"
#include "custom_utilities/shape_deviatoric_plane_utilities.hpp"

namespace Kratos
{
   ///@addtogroup ConstitutiveModelsApplication
   ///@{

   ///@name Kratos Globals
   ///@{

   ///@}
   ///@name Type Definitions
   ///@{

   ///@}
   ///@name  Enum's
   ///@{

   ///@}
   ///@name  Functions
   ///@{

   ///@}
   ///@name Kratos Classes
   ///@{

   /// Short class definition.
   /** Detail class definition.
    */
   template <class THardeningRule>
      class SClay1YieldSurface : public YieldSurface<THardeningRule>
   {
      public:
         ///@name Type Definitions
         ///@{

         typedef ConstitutiveModelData::MatrixType MatrixType;
         typedef ConstitutiveModelData::VectorType VectorType;
         typedef ConstitutiveModelData::ModelData ModelDataType;
         typedef ConstitutiveModelData::MaterialData MaterialDataType;

         typedef YieldSurface<THardeningRule> BaseType;
         typedef typename BaseType::Pointer BaseTypePointer;
         typedef typename BaseType::PlasticDataType PlasticDataType;

         /// Pointer definition of SClay1YieldSurface
         KRATOS_CLASS_POINTER_DEFINITION(SClay1YieldSurface);

         ///@}
         ///@name Life Cycle
         ///@{

         /// Default constructor.
         SClay1YieldSurface() : BaseType() {}

         /// Copy constructor.
         SClay1YieldSurface(SClay1YieldSurface const &rOther) : BaseType(rOther) {}

         /// Assignment operator.
         SClay1YieldSurface &operator=(SClay1YieldSurface const &rOther)
         {
            BaseType::operator=(rOther);
            return *this;
         }

         /// Clone.
         BaseTypePointer Clone() const override
         {
            return Kratos::make_shared<SClay1YieldSurface>(*this);
         }

         /// Destructor.
         ~SClay1YieldSurface() override {}

         ///@}
         ///@name Operators
         ///@{

         ///@}
         ///@name Operations
         ///@{

         /**
          * Calculate Yield Condition
          */

         double &CalculateYieldCondition(const PlasticDataType &rVariables, double &rYieldCondition) override
         {
            KRATOS_TRY

            const ModelDataType &rModelData = rVariables.GetModelData();
            const MatrixType &rStressMatrix = rModelData.GetStressMatrix();

            // Material Parameters
            const Properties &rProperties = rModelData.GetProperties();
            const double &rShearM = rProperties[CRITICAL_STATE_LINE];
            const double &rFriction = rProperties[INTERNAL_FRICTION_ANGLE];

            // compute something with the hardening rule
            const double & rPreconsolidationStress = rVariables.Data.Internal(3);


            VectorType FabricVector;
            for (unsigned int i = 0; i < 6; i++)
               FabricVector(i) = rVariables.Data.Internal(10+i);

            MatrixType FabricTensor; 
            ConstitutiveModelUtilities::StressVectorToTensor( FabricVector, FabricTensor);

            double MeanStress(0.0);
            MatrixType StressDev = rStressMatrix;
            for (unsigned int i = 0; i < 3; i++)
               MeanStress += rStressMatrix(i,i)/3.0;
            for (unsigned int i = 0; i < 3; i++)
               StressDev(i,i) -= MeanStress;

            StressDev = StressDev - MeanStress*FabricTensor;

            double TensorProduct1(0.0), TensorProduct2(0.0);
            for (unsigned int i = 0; i < 3; i++) {
               for (unsigned int j = 0; j < 3; j++) {
                  TensorProduct1 += pow( StressDev(i,j), 2);
                  TensorProduct2 += pow( FabricTensor(i,j), 2);
               }
            }

            double ThirdInvariantEffect(1.0);
            if ( rFriction > 0.0)  {
               double dummy1(1.0), dummy2(2.0), LodeAngle;
               StressInvariantsUtilities::CalculateStressInvariants(StressDev, dummy1, dummy2, LodeAngle);
               ThirdInvariantEffect = ShapeAtDeviatoricPlaneUtility::EvaluateEffectDueToThirdInvariant(ThirdInvariantEffect, LodeAngle, rFriction);
               rYieldCondition = 1.5*TensorProduct1 * pow(ThirdInvariantEffect, 2) - ( pow( rShearM, 2) - 3.0/2.0*TensorProduct2) * MeanStress*(rPreconsolidationStress-MeanStress);
               return rYieldCondition;
            }  // new idea

            if ( rFriction > 0.0)  {
              double dummy1(1.0), dummy2(2.0), LodeAngle;
              StressInvariantsUtilities::CalculateStressInvariants(StressDev, dummy1, dummy2, LodeAngle);
              ThirdInvariantEffect = ShapeAtDeviatoricPlaneUtility::EvaluateEffectDueToThirdInvariant(ThirdInvariantEffect, LodeAngle, rFriction);
              }  // new idea
            /*if ( rFriction > 0.0) {
               double p(0), J2(0), LodeAngle(0);
               StressInvariantsUtilities::CalculateStressInvariants(rStressMatrix, p, J2, LodeAngle);
               ThirdInvariantEffect = ShapeAtDeviatoricPlaneUtility::EvaluateEffectDueToThirdInvariant(ThirdInvariantEffect, LodeAngle, rFriction);
            }*/


            rYieldCondition = 1.5*TensorProduct1 - ( pow( rShearM/ThirdInvariantEffect, 2) - 3.0/2.0*TensorProduct2) * MeanStress*(rPreconsolidationStress-MeanStress);

            /*std::cout << " p " << MeanStress << " p0 " << rPreconsolidationStress << " M " << rShearM  << " cauchy " << rStressMatrix <<  " fabric " << FabricTensor << " and " << TensorProduct2 << " sDev-pF" << TensorProduct1 << std::endl;
              std::cout << " YIELD " << rYieldCondition << std::endl;*/

            return rYieldCondition;

            KRATOS_CATCH(" ")
         }

         //*************************************************************************************
         //*************************************************************************************
         // evaluation of the derivative of the yield surface respect the stresses
         VectorType &CalculateDeltaStressYieldCondition(const PlasticDataType &rVariables, VectorType &rDeltaStressYieldCondition) override
         {
            KRATOS_TRY

      const ModelDataType &rModelData = rVariables.GetModelData();
            const MatrixType &rStressMatrix = rModelData.GetStressMatrix();

            // Material Parameters
            const Properties &rProperties = rModelData.GetProperties();
            const double &rShearM = rProperties[CRITICAL_STATE_LINE];
            const double &rFriction = rProperties[INTERNAL_FRICTION_ANGLE];

            // compute something with the hardening rule
            const double & rPreconsolidationStress = rVariables.Data.Internal(3);

            VectorType FabricVector;
            for (unsigned int i = 0; i < 6; i++)
               FabricVector(i) = rVariables.Data.Internal(10+i);

            MatrixType FabricTensor;
            FabricTensor = ZeroMatrix(3,3);
            ConstitutiveModelUtilities::StressVectorToTensor( FabricVector, FabricTensor);

            if ( rFriction > 0.0 && true ) {
               // now let's check the derivative.... just in case....
               MatrixType s1; 
               MatrixType s2;
               MatrixType dg;

               double g1, g2;
               double delta = 1E-8;

               const double& rP0   = rPreconsolidationStress;

               for (unsigned int i = 0; i < 3; i++) {
                  for (unsigned int j = 0; j < 3; j++) {
                     s1 = rStressMatrix;
                     s2 = rStressMatrix;
                     s1(i,j) += delta;
                     s2(i,j) -= delta;

                     g1 = EvaluateFFunction(g1, s1, rShearM, rFriction, FabricTensor, rP0);
                     g2 = EvaluateFFunction(g2, s2, rShearM, rFriction, FabricTensor, rP0);

                     dg(i,j) = (g1-g2)/(2.0*delta);
                  }
               }
               ConstitutiveModelUtilities::StrainTensorToVector( dg, rDeltaStressYieldCondition);
               //std::cout << " Numerical approximation " << rDeltaStressYieldCondition << std::endl;
               return rDeltaStressYieldCondition;
            }

            double MeanStress(0.0);
            MatrixType StressDev = rStressMatrix;
            for (unsigned int i = 0; i < 3; i++)
               MeanStress += rStressMatrix(i,i)/3.0;
            for (unsigned int i = 0; i < 3; i++)
               StressDev(i,i) -= MeanStress;

            StressDev = StressDev - MeanStress*FabricTensor;

            double TensorProduct1(0.0), TensorProduct2(0.0);
            for (unsigned int i = 0; i < 3; i++) {
               for (unsigned int j = 0; j < 3; j++) {
                  TensorProduct1 += StressDev(i,j) * FabricTensor(i,j);
                  TensorProduct2 += pow( FabricTensor(i,j), 2);
               }
            }


            MatrixType EyeMatrix = ZeroMatrix(3,3);
            for (unsigned int i = 0; i < 3; i++)
               EyeMatrix(i,i) = 1.0;

            double ThirdInvariantEffect(1.0);
            if ( rFriction > 0.0)  {
               double dummy1(1.0), dummy2(2.0), LodeAngle;
               StressInvariantsUtilities::CalculateStressInvariants(StressDev, dummy1, dummy2, LodeAngle);
               ThirdInvariantEffect = ShapeAtDeviatoricPlaneUtility::EvaluateEffectDueToThirdInvariant(ThirdInvariantEffect, LodeAngle, rFriction);
            }


            MatrixType Gradient; 
            Gradient = 3.0*StressDev;
            Gradient -= TensorProduct1 * EyeMatrix;
            Gradient -= 1.0/3.0 * ( pow(rShearM/ThirdInvariantEffect, 2) - 3.0/2.0 * TensorProduct2) * ( rPreconsolidationStress - 2.0*MeanStress) * EyeMatrix; 

            ConstitutiveModelUtilities::StrainTensorToVector( Gradient, rDeltaStressYieldCondition);

            if ( rFriction > 0.0) {

              bool MultiplySinInstead = false;
              double auxMeanStress, J2sqrt, LodeAngle;
              double DerivativeEffect(0);

              VectorType V1, V2, V3;
            // more work is requiered
            StressInvariantsUtilities::CalculateStressInvariants(StressDev, auxMeanStress, J2sqrt, LodeAngle);
            StressInvariantsUtilities::CalculateDerivativeVectors(StressDev, V1, V2, V3);
            ShapeAtDeviatoricPlaneUtility::CalculateKLodeCoefficients(ThirdInvariantEffect, DerivativeEffect, LodeAngle, rFriction, true, MultiplySinInstead);
            double Term = 2.0 * pow( rShearM, 2)/pow(ThirdInvariantEffect, 3) * MeanStress * (rPreconsolidationStress - MeanStress) * DerivativeEffect;

            double C2(0);
            double C3(0);


            if (J2sqrt > 1E-6) {
            if ( MultiplySinInstead){
            C2 = -std::sin(3.0 * LodeAngle) / J2sqrt * Term;
            C3 = -sqrt(3.0) / 2.0 / pow(J2sqrt, 3) * Term;
            }
            else{
            C2 = -std::tan(3.0 * LodeAngle) / J2sqrt * Term;
            C3 = -sqrt(3.0) / 2.0 / pow(J2sqrt, 3) / std::cos(3.0 * LodeAngle) * Term;
            }

            V1 = C2*V2 + C3*V3;

            Matrix AuxMatrix(6,6);
            noalias (AuxMatrix) = ZeroMatrix(6,6);

            VectorType IdentityVector;
            for (unsigned int i = 0; i < 3; i++) {
            IdentityVector(i) = 1.0;
            IdentityVector(i+3) = 0.0;
            }

            for (unsigned int i = 0; i < 6; i++) {
            AuxMatrix(i,i) += 1.0;
            for (unsigned int j = 0; j < 6; j++) {
            AuxMatrix(i,j) -= 1.0/3.0*FabricVector(i)*IdentityVector(j);
            }
            }

            rDeltaStressYieldCondition += prod( V1, AuxMatrix);
            //std::cout << " Analytical approximation " << rDeltaStressYieldCondition << std::endl;
            //std::cout << std::endl;
            //std::cout << " C2 " << C2 << " C3 " << C3 << std::endl;
            //std::cout << V2 << std::endl;
            //std::cout << V3 << std::endl;
            //std::cout << "inv " << auxMeanStress << " , " << J2sqrt << " , " << LodeAngle << std::endl;
            //std::cout << "FabricVector " << FabricVector << std::endl;
            //std::cout << V1 << std::endl;
            //std::cout << prod(V1, AuxMatrix) << std::endl;
            //std::cout << " AuxMatrix " << AuxMatrix << std::endl;
            //std::cout << std::endl;
            //std::cout << std::endl;
            } 
            return rDeltaStressYieldCondition;
            }  // new idea

            if ( rFriction > 0.0) {

               bool MultiplySinInstead = false;
               double auxMeanStress, J2sqrt, LodeAngle;
               double DerivativeEffect(0);

               VectorType V1, V2, V3;
               // more work is requiered
               StressInvariantsUtilities::CalculateStressInvariants(rStressMatrix, auxMeanStress, J2sqrt, LodeAngle);
               StressInvariantsUtilities::CalculateDerivativeVectors(rStressMatrix, V1, V2, V3);
               ShapeAtDeviatoricPlaneUtility::CalculateKLodeCoefficients(ThirdInvariantEffect, DerivativeEffect, LodeAngle, rFriction, true, MultiplySinInstead);
               double Term = 2.0 * pow( rShearM, 2)/pow(ThirdInvariantEffect, 3) * MeanStress * (rPreconsolidationStress - MeanStress) * DerivativeEffect;

               double C2(0);
               double C3(0);


               if (J2sqrt > 1E-6) {
                  if ( MultiplySinInstead){
                     C2 = -std::sin(3.0 * LodeAngle) / J2sqrt * Term;
                     C3 = -sqrt(3.0) / 2.0 / pow(J2sqrt, 3) * Term;
                  }
                  else{
                     C2 = -std::tan(3.0 * LodeAngle) / J2sqrt * Term;
                     C3 = -sqrt(3.0) / 2.0 / pow(J2sqrt, 3) / std::cos(3.0 * LodeAngle) * Term;
                  }

                  V1 = C2*V2 + C3*V3;

                  /*Matrix AuxMatrix(6,6);
                    noalias (AuxMatrix) = ZeroMatrix(6,6);

                    VectorType IdentityVector;
                    for (unsigned int i = 0; i < 3; i++) {
                    IdentityVector(i) = 1.0;
                    IdentityVector(i+3) = 0.0;
                    }

                    for (unsigned int i = 0; i < 6; i++) {
                    AuxMatrix(i,i) += 1.0;
                    for (unsigned int j = 0; j < 6; j++) {
                    AuxMatrix(i,j) -= 1.0/3.0*FabricVector(i)*IdentityVector(j);
                    }
                    }*/

                  rDeltaStressYieldCondition += V1;
                  /*std::cout << " Analytical approximation " << rDeltaStressYieldCondition << std::endl;
                    std::cout << std::endl;
                    std::cout << " C2 " << C2 << " C3 " << C3 << std::endl;
                    std::cout << V2 << std::endl;
                    std::cout << V3 << std::endl;
                    std::cout << "inv " << auxMeanStress << " , " << J2sqrt << " , " << LodeAngle << std::endl;
                    std::cout << "FabricVector " << FabricVector << std::endl;
                    std::cout << V1 << std::endl;
                    std::cout << prod(V1, AuxMatrix) << std::endl;
                    std::cout << " AuxMatrix " << AuxMatrix << std::endl;
                    std::cout << std::endl;
                    std::cout << std::endl;*/
               } 
            }

            return rDeltaStressYieldCondition;

            KRATOS_CATCH(" ")
         }



         double & EvaluateFFunction( double & rF, const MatrixType & rStressMatrix, const double & rShearM,  const double & rFriction, const MatrixType & FabricTensor, const double & rPreconsolidationStress)
         {
            KRATOS_TRY

      double rYieldCondition;
      //calculate stress invariants
      double MeanStress(0.0);
            MatrixType StressDev = rStressMatrix;
            for (unsigned int i = 0; i < 3; i++)
               MeanStress += rStressMatrix(i,i)/3.0;
            for (unsigned int i = 0; i < 3; i++)
               StressDev(i,i) -= MeanStress;

            StressDev = StressDev - MeanStress*FabricTensor;


            double TensorProduct1(0.0), TensorProduct2(0.0);
            for (unsigned int i = 0; i < 3; i++) {
               for (unsigned int j = 0; j < 3; j++) {
                  TensorProduct1 += pow( StressDev(i,j), 2);
                  TensorProduct2 += pow( FabricTensor(i,j), 2);
               }
            }

            double ThirdInvariantEffect(1.0);
            if ( rFriction > 0.0)  {
               double dummy1(1.0), dummy2(2.0), LodeAngle;
               StressInvariantsUtilities::CalculateStressInvariants(StressDev, dummy1, dummy2, LodeAngle);
               ThirdInvariantEffect = ShapeAtDeviatoricPlaneUtility::EvaluateEffectDueToThirdInvariant(ThirdInvariantEffect, LodeAngle, rFriction);
            }  // new idea
            rYieldCondition = 1.5*TensorProduct1 * pow(ThirdInvariantEffect, 2) - ( pow( rShearM, 2) - 3.0/2.0*TensorProduct2) * MeanStress*(rPreconsolidationStress-MeanStress);
            rF = rYieldCondition;
            return rF;


            KRATOS_CATCH("")
         }

         /**
          * Get required properties
          */
         void GetRequiredProperties(Properties &rProperties) override
         {
            KRATOS_TRY

      // Set required properties
      double value = 0.0; //dummy
            rProperties.SetValue(CRITICAL_STATE_LINE,value);

            BaseType::GetRequiredProperties(rProperties);

            KRATOS_CATCH(" ")
         }

         ///@}
         ///@name Access
         ///@{

         ///@}
         ///@name Inquiry
         ///@{

         ///@}
         ///@name Input and output
         ///@{

         /// Turn back information as a string.
         std::string Info() const override
         {
            std::stringstream buffer;
            buffer << "SClay1YieldSurface";
            return buffer.str();
         }

         /// Print information about this object.
         void PrintInfo(std::ostream &rOStream) const override
         {
            rOStream << "SClay1YieldSurface";
         }

         /// Print object's data.
         void PrintData(std::ostream &rOStream) const override
         {
            rOStream << "SClay1YieldSurface Data";
         }

         ///@}
         ///@name Friends
         ///@{

         ///@}

      protected:
         ///@name Protected static Member Variables
         ///@{

         ///@}
         ///@name Protected member Variables
         ///@{

         ///@}
         ///@name Protected Operators
         ///@{

         ///@}
         ///@name Protected Operations
         ///@{

         ///@}
         ///@name Protected  Access
         ///@{

         ///@}
         ///@name Protected Inquiry
         ///@{

         ///@}
         ///@name Protected LifeCycle
         ///@{

         ///@}

      private:
         ///@name Static Member Variables
         ///@{

         ///@}
         ///@name Member Variables
         ///@{

         ///@}
         ///@name Private Operators
         ///@{

         ///@}
         ///@name Private Operations
         ///@{

         ///@}
         ///@name Private  Access
         ///@{

         ///@}
         ///@name Serialization
         ///@{
         friend class Serializer;

         void save(Serializer &rSerializer) const override
         {
            KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BaseType)
         }

         void load(Serializer &rSerializer) override
         {
            KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BaseType)
         }

         ///@}
         ///@name Private Inquiry
         ///@{

         ///@}
         ///@name Un accessible methods
         ///@{

         ///@}

   }; // Class SClay1YieldSurface

   ///@}

   ///@name Type Definitions
   ///@{

   ///@}
   ///@name Input and output
   ///@{

   ///@}

   ///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_SCLAY1_YIELD_SURFACE_HPP_INCLUDED  defined
