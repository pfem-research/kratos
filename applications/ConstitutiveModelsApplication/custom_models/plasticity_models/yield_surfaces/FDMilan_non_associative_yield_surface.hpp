//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:             K. Oliynyk $
//   Maintained by:       $Maintainer:                    KO $
//   Date:                $Date:                   Sept 2020 $
//
//   Created by Kateryna Oliynyk - September 2020
//

#if !defined(KRATOS_FDMILAN_NON_ASSOCIATIVE_YIELD_SURFACE_HPP_INCLUDED)
#define KRATOS_FDMILAN_NON_ASSOCIATIVE_YIELD_SURFACE_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/yield_surfaces/non_associative_yield_surface.hpp"
#include "custom_utilities/stress_invariants_utilities.hpp"
#include "custom_utilities/shape_deviatoric_plane_utilities.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
template <class THardeningRule>
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) FDMilanNonAssociativeYieldSurface : public NonAssociativeYieldSurface<THardeningRule>
{
public:
  ///@name Type Definitions
  ///@{

  typedef ConstitutiveModelData::MatrixType MatrixType;
  typedef ConstitutiveModelData::VectorType VectorType;
  typedef ConstitutiveModelData::ModelData ModelDataType;
  typedef ConstitutiveModelData::MaterialData MaterialDataType;

  typedef NonAssociativeYieldSurface<THardeningRule> BaseType;
  typedef typename YieldSurface<THardeningRule>::Pointer BaseTypePointer;
  typedef typename BaseType::PlasticDataType PlasticDataType;

  /// Pointer definition of FDMilanNonAssociativeYieldSurface
  KRATOS_CLASS_POINTER_DEFINITION(FDMilanNonAssociativeYieldSurface);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  FDMilanNonAssociativeYieldSurface() : BaseType() {}

  /// Default constructor.
  FDMilanNonAssociativeYieldSurface(BaseTypePointer const &rpPlasticPotential) : BaseType(rpPlasticPotential) {}

  /// Copy constructor.
  FDMilanNonAssociativeYieldSurface(FDMilanNonAssociativeYieldSurface const &rOther) : BaseType(rOther) {}

  /// Assignment operator.
  FDMilanNonAssociativeYieldSurface &operator=(FDMilanNonAssociativeYieldSurface const &rOther)
  {
    BaseType::operator=(rOther);
    return *this;
  }

  /// Clone.
  virtual BaseTypePointer Clone() const override
  {
    return BaseTypePointer(new FDMilanNonAssociativeYieldSurface(*this));
  }

  /// Destructor.
  ~FDMilanNonAssociativeYieldSurface() override {}

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /**
     * Calculate Yield Condition f = f(p*,J,theta) (Continuum Mechanics stress convention)
     */

  virtual double &CalculateYieldCondition(const PlasticDataType &rVariables, double &rYieldCondition) override
  {
    KRATOS_TRY

    const ModelDataType &rModelData = rVariables.GetModelData();
    const MatrixType &rStressMatrix = rModelData.GetStressMatrix();

    const double &rPT = rVariables.Data.Internal(4);
    const double &rPCstar = rVariables.Data.Internal(5);

    // calculate sig* = sig + pt

    MatrixType StressTranslated;
    PerformStressTranslation(rStressMatrix, StressTranslated, rPT);

    // std::cout << "sig_11 :" << rStressMatrix(0,0) << std::endl;
    // std::cout << "sig_22 :" << rStressMatrix(1,1) << std::endl;
    // std::cout << "sig_33 :" << rStressMatrix(2,2) << std::endl;
    // std::cout << "sigstar_11 :" << StressTranslated(0,0) << std::endl;
    // std::cout << "sigstar_22 :" << StressTranslated(1,1) << std::endl;
    // std::cout << "sigstar_33 :" << StressTranslated(2,2) << std::endl;

    // recover Mf constant

    const Properties &rMaterialProperties = rModelData.GetProperties();
    const double &rShearM = rMaterialProperties[CRITICAL_STATE_LINE];

    // recover alpha_f and mu_f

    const double &ralpha_f = rMaterialProperties[ALPHA_F];
    const double &rmu_f = rMaterialProperties[MU_F];

    // recover friction angle

    double Friction = 0.0;
    if (rMaterialProperties.Has(INTERNAL_FRICTION_ANGLE))
    {
      double SinPhi = 3.0*rShearM/(6.0+rShearM);
      Friction = std::asin(SinPhi);
    }

    // compute stress invariants p* = MeanStress, J = J2sqrt, theta = LodeAngle (p* = MeanStress negative in compression)

    double MeanStress, J2sqrt, LodeAngle;

    StressInvariantsUtilities::CalculateStressInvariants(StressTranslated, MeanStress, J2sqrt, LodeAngle);

    // compute function M(theta)

    double Mth, Mth3;
    double ThirdInvariantEffect = 1.0;
    if (Friction > 0.0)
      ThirdInvariantEffect = ShapeAtDeviatoricPlaneUtility::EvaluateEffectDueToThirdInvariant(ThirdInvariantEffect, LodeAngle, Friction);

    Mth = rShearM/ThirdInvariantEffect;
    Mth3 = Mth/sqrt(3.0);

    // compute functions K1f, K2f, Cf

    double tmp1, tmp2, K1f, K2f, CCf, Pow1, Pow2;

    tmp1 = rmu_f*(1.0-ralpha_f)/(2.0*(1.0-rmu_f));
    tmp2 = 1.0 - 4.0*ralpha_f*(1.0-rmu_f)/(rmu_f*(1.0-ralpha_f)*(1.0-ralpha_f));

    K1f = tmp1*(1.0 + sqrt(tmp2));
    K2f = tmp1*(1.0 - sqrt(tmp2));
    CCf  = (1.0-rmu_f)*(K1f-K2f);

    Pow1  = -K1f/CCf;
    Pow2  = K2f/CCf;

    // compute eta_max if it exists - Note: etadash = (1/Mth)*(-J/p*)

    bool ExistsEtaJMax;
    double EtaJMax = 1.0e6;

    if (ralpha_f > 1.0){
      ExistsEtaJMax = true;
      EtaJMax = -K2f*Mth3;
    }
    else if (rmu_f > 1.0){
      ExistsEtaJMax = true;
      EtaJMax = -K1f*Mth3;
    }
    else{
      ExistsEtaJMax = false;
    }

    // compute yield function; regularization adopted for cases when ExistsEtaJMax = true and etaJ > EtaJMax

    double EtaJ;

    if(abs(MeanStress)>1.0e-6){
      EtaJ = -J2sqrt/MeanStress;
      }
    else{
      EtaJ = J2sqrt/1.0e-6;
      }

    // Compute f values for 3 different regions in p* : J space

    if(ExistsEtaJMax){
      if ((MeanStress < 0) && (EtaJ < EtaJMax)) {
        // Zone 1 - Lagioia et al. surface
        double Af = 1.0 + EtaJ/(K1f*Mth3);
        double Bf = 1.0 + EtaJ/(K2f*Mth3);
        rYieldCondition = -MeanStress + pow(Af,Pow1)*pow(Bf,Pow2)*rPCstar;
      }
      else if((MeanStress >= 0) && (J2sqrt < MeanStress/Mth3)) {
        // Zone 3 - circular surface
        double DeltaEta = EtaJMax - Mth3;
        double Alpha = std::atan(Mth3);
        double Phi = std::cos(Alpha);
        double radius = sqrt(pow(MeanStress,2)+pow(J2sqrt,2));
        rYieldCondition = radius/(Phi*DeltaEta);
      }
      else{
        // Zone 2 - Linear surface
        double DeltaEta = EtaJMax - Mth3;
        rYieldCondition = (J2sqrt + Mth3*MeanStress)/DeltaEta;
      }
    }
    else{
      if(MeanStress < 0){
        // Zone 1 - Lagioia et al. surface
        double Af = 1.0 + EtaJ/(K1f*Mth3);
        double Bf = 1.0 + EtaJ/(K2f*Mth3);
        rYieldCondition = -MeanStress + pow(Af,Pow1)*pow(Bf,Pow2)*rPCstar;
      }
      else{
        // Zona 2 - p* > 0 region
        double Slope = 1.0;
        double EtaJlim = 1.0e6;
        double Aflim = 1.0 + EtaJlim/(K1f*Mth3);
        double Bflim = 1.0 + EtaJlim/(K2f*Mth3);
        rYieldCondition = pow(Aflim,Pow1)*pow(Bflim,Pow2)*rPCstar + Slope*MeanStress;
      }
    }

    return rYieldCondition;

    KRATOS_CATCH(" ")
  }

  //*************************************************************************************
  //*************************************************************************************

  // evaluation of the derivative of the yield surface respect the stresses

  virtual VectorType &CalculateDeltaStressYieldCondition(const PlasticDataType &rVariables, VectorType &rDeltaStressYieldCondition) override
  {
    KRATOS_TRY

    const ModelDataType &rModelData = rVariables.GetModelData();
    const MatrixType &rStressMatrix = rModelData.GetStressMatrix();

    const double &rPT = rVariables.Data.Internal(4);
    const double &rPCstar = rVariables.Data.Internal(5);

//    std::cout << " --- enter CalculateDeltaStressYieldCondition --- " << std::endl;
//    std::cout << " PCstar :" << rPCstar << std::endl;
//    std::cout << " PT     :" << rPT << std::endl;

    // calculate sig* = sig + pt

    MatrixType StressTranslated;
    PerformStressTranslation(rStressMatrix, StressTranslated, rPT);

    // recover Mf constant

    const Properties &rMaterialProperties = rModelData.GetProperties();
    const double &rShearM = rMaterialProperties[CRITICAL_STATE_LINE];

    // recover alpha_f and mu_f

    const double &ralpha_f = rMaterialProperties[ALPHA_F];
    const double &rmu_f = rMaterialProperties[MU_F];

//    std::cout << " alpha_f :" << ralpha_f << std::endl;
//    std::cout << " mu_f    :" << rmu_f << std::endl;

    // recover friction angle

    double Friction = 0;
    if (rMaterialProperties.Has(INTERNAL_FRICTION_ANGLE))
    {
      double SinPhi = 3.0*rShearM/(6.0+rShearM);
      Friction = std::asin(SinPhi);
    }

    // compute stress invariants p* = MeanStress, J = J2sqrt, theta = LodeAngle (pstarSM = -MeanStress positive in compression)

    double MeanStress, J2sqrt, LodeAngle;
    StressInvariantsUtilities::CalculateStressInvariants(StressTranslated, MeanStress, J2sqrt, LodeAngle);

//    std::cout << " p_star :" << MeanStress << ", J :" << J2sqrt << ", theta :" << LodeAngle << std::endl;

    // compute derivatives of invariants w.r. to sigma: V1 = dp/dsig, V2 = dJ/dsig, V3 = s^2 - (2/3)*J^2*I

    VectorType V1, V2, V3;
    StressInvariantsUtilities::CalculateDerivativeVectors(StressTranslated, V1, V2, V3);

    // calculate Lode angle function and its derivative

    double ThirdInvariantEffect(1), DerivativeEffect(0), Mth, Mth3;
    if (Friction > 0.0)
    {
      ShapeAtDeviatoricPlaneUtility::CalculateKLodeCoefficients(ThirdInvariantEffect, DerivativeEffect, LodeAngle, Friction, true);
      //std::cout << " LodeAngle " << LodeAngle*180.0/Globals::Pi << " effect " << ThirdInvariantEffect << std::endl;
    }

    Mth = rShearM/ThirdInvariantEffect;
    Mth3 = Mth/sqrt(3.0);

//    std::cout << " Mth :" << Mth << std::endl;

    // compute functions K1f, K2f, Cf

    double tmp1, tmp2, K1f, K2f, CCf, Pow1, Pow2;

    tmp1 = rmu_f*(1.0-ralpha_f)/(2.0*(1.0-rmu_f));
    tmp2 = 1.0 - 4.0*ralpha_f*(1.0-rmu_f)/(rmu_f*(1.0-ralpha_f)*(1.0-ralpha_f));

    K1f = tmp1*(1.0 + sqrt(tmp2));
    K2f = tmp1*(1.0 - sqrt(tmp2));
    CCf  = (1.0-rmu_f)*(K1f-K2f);

    Pow1  = -K1f/CCf;
    Pow2  = K2f/CCf;

    // compute eta_max if it exists - Note: etadash = (1/Mth)*(-J/p*)

    bool ExistsEtaJMax;
    double EtaJMax = 1.0e6;

    if (ralpha_f > 1.0){
      ExistsEtaJMax = true;
      EtaJMax = -K2f*Mth3;
    }
    else if (rmu_f > 1.0){
      ExistsEtaJMax = true;
      EtaJMax = -K1f*Mth3;
    }
    else{
      ExistsEtaJMax = false;
    }

//    std::cout << " ExistsEtaJMax :" << ExistsEtaJMax << std::endl;
//    std::cout << " EtaJMax :" << EtaJMax << std::endl;

    // compute derivatives of f wr to p, J, theta

    double EtaJ, Af, Bf;
    double Aux1, Aux2, Aux3, Aux4;
    double dfdp, dfdJ, dfdtheta;

    if(abs(MeanStress)>1.0e-6){
      EtaJ = -J2sqrt/MeanStress;
    }
    else{
      EtaJ = J2sqrt/1.0e-6;
    }

    // Compute dfdp, dfdJ, dfdtheta values for 3 different regions in p* : J space

    if(ExistsEtaJMax) {
      if ((MeanStress < 0) && (EtaJ < EtaJMax)) {
      // Zone 1 - Lagioia et al. surface
        Af = 1.0 + EtaJ/(K1f*Mth3);
        Bf = 1.0 + EtaJ/(K2f*Mth3);
        Aux1 = pow(Af,Pow1)*pow(Bf,Pow2)*rPCstar/MeanStress;
        Aux2 = 1.0/(Mth3*CCf);
        Aux3 = sqrt(3.0)/(rShearM*CCf);
        Aux4 = 1.0/Af - 1.0/Bf;
        dfdp = -1.0 + Aux1*Aux2*Aux4*EtaJ;
        dfdJ = Aux1*Aux2*Aux4;
        dfdtheta = Aux1*Aux3*J2sqrt*Aux4*DerivativeEffect;
//        std::cout << " Zone 1 " << std::endl;
//        std::cout << "   Af :" << Af << std::endl;
//        std::cout << "   Bf :" << Bf << std::endl;
//        std::cout << " Aux1 :" << Aux1 << std::endl;
//        std::cout << " Aux4 :" << Aux4 << std::endl;
      }
      else if((MeanStress >= 0) && (J2sqrt < MeanStress/Mth3)) {
        // Zone 3 - circular surface
        double DeltaEta = EtaJMax - Mth3;
        double Alpha = std::atan(Mth3);
        double Phi = std::cos(Alpha);
        double radius = sqrt(pow(MeanStress,2)+pow(J2sqrt,2));
        Aux1 = DerivativeEffect/ThirdInvariantEffect;
        Aux2 = Mth3/(1.0+pow(Mth3,2));
        double dDeltadth = Mth3*Aux1;
        double dPhidth = std::sin(Alpha)*Aux1*Aux2;
        dfdp = MeanStress/(Phi*DeltaEta*radius);
        dfdJ = J2sqrt/(Phi*DeltaEta*radius);
        dfdtheta = -radius*(dDeltadth/DeltaEta + dPhidth/Phi);
        dfdtheta /= (Phi*DeltaEta);
//        std::cout << " Zone 3 " << std::endl;
      }
      else{
        // Zone 2 - Linear surface
        double DeltaEta = EtaJMax - Mth3;
        dfdp = Mth3/DeltaEta;
        dfdJ = 1.0/DeltaEta;
        dfdtheta = -Mth3*(J2sqrt+EtaJMax*MeanStress)/pow(DeltaEta,2);
        dfdtheta *= DerivativeEffect/ThirdInvariantEffect;
//        std::cout << " Zone 2 " << std::endl;
//        std::cout << " regularized f function " << std::endl;
//        std::cout << "     EtaJ :" << EtaJ << std::endl;
//        std::cout << " DeltaEta :" << DeltaEta << std::endl;
      }
    }
    else{
      if (MeanStress < 0) {
        // Zone 1 - Lagioia et al. surface
        Af = 1.0 + EtaJ/(K1f*Mth3);
        Bf = 1.0 + EtaJ/(K2f*Mth3);
        Aux1 = pow(Af,Pow1)*pow(Bf,Pow2)*rPCstar/MeanStress;
        Aux2 = 1.0/(Mth3*CCf);
        Aux3 = sqrt(3.0)/(rShearM*CCf);
        Aux4 = 1.0/Af - 1.0/Bf;
        dfdp = -1.0 + Aux1*Aux2*Aux4*EtaJ;
        dfdJ = Aux1*Aux2*Aux4;
        dfdtheta = Aux1*Aux3*J2sqrt*Aux4*DerivativeEffect;
//        std::cout << "   Af :" << Af << std::endl;
//        std::cout << "   Bf :" << Bf << std::endl;
//        std::cout << " Aux1 :" << Aux1 << std::endl;
//        std::cout << " Aux4 :" << Aux4 << std::endl;
      }
      else{
        // Zona 2 - p* > 0 region
        double Slope = 1.0;
        dfdp = Slope;
        dfdJ = 0.0;
        dfdtheta = 0.0;
      }
    }

//    std::cout << "     dfdp :" << dfdp << std::endl;
//    std::cout << "     dfdJ :" << dfdJ << std::endl;
//    std::cout << " dfdtheta :" << dfdtheta << std::endl;

    // calculate df/dsig = df/dp*dp/dsig + df/dJ*dJ/dsig + df/dtheta*dtheta/dsig

    rDeltaStressYieldCondition = dfdp * V1 + dfdJ * V2;

    if (Friction > 0.0 && J2sqrt > 1E-6){
       double C2, C3;
       C2 = -std::tan(3.0 * LodeAngle) / J2sqrt * dfdtheta;
       C3 = -sqrt(3.0) / 2.0 / pow(J2sqrt, 3) / std::cos(3.0 * LodeAngle) * dfdtheta;
       rDeltaStressYieldCondition += C2 * V2 + C3 * V3;
    }

//    std::cout << " --- exit  CalculateDeltaStressYieldCondition --- " << std::endl;

    return rDeltaStressYieldCondition;

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  virtual std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "FDMilanNonAssociativeYieldSurface";
    return buffer.str();
  }

  /// Print information about this object.
  virtual void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "FDMilanNonAssociativeYieldSurface";
  }

  /// Print object's data.
  virtual void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "FDMilanNonAssociativeYieldSurface Data";
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  void PerformStressTranslation(const MatrixType &rStressMatrix, MatrixType &rStressTranslated, const double &rTranslation)
  {
    KRATOS_TRY

    rStressTranslated.clear();
    rStressTranslated = rStressMatrix;
    for (unsigned int i = 0; i < 3; i++)
      rStressTranslated(i, i) += rTranslation;

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  virtual void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BaseType)
  }

  virtual void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BaseType)
  }

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class FDMilanNonAssociativeYieldSurface

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_FDMILAN_NON_ASSOCIATIVE_YIELD_SURFACE_HPP_INCLUDED  defined
