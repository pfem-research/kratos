//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:                  April 2017 $
//
//

#if !defined(KRATOS_MODIFIED_CAM_CLAY_YIELD_SURFACE_HPP_INCLUDED)
#define KRATOS_MODIFIED_CAM_CLAY_YIELD_SURFACE_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/yield_surfaces/yield_surface.hpp"
#include "custom_utilities/stress_invariants_utilities.hpp"
#include "custom_utilities/shape_deviatoric_plane_utilities.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
template <class THardeningRule>
class ModifiedCamClayYieldSurface : public YieldSurface<THardeningRule>
{
public:
  ///@name Type Definitions
  ///@{

  typedef ConstitutiveModelData::MatrixType MatrixType;
  typedef ConstitutiveModelData::VectorType VectorType;
  typedef ConstitutiveModelData::ModelData ModelDataType;
  typedef ConstitutiveModelData::MaterialData MaterialDataType;

  typedef YieldSurface<THardeningRule> BaseType;
  typedef typename BaseType::Pointer BaseTypePointer;
  typedef typename BaseType::PlasticDataType PlasticDataType;

  /// Pointer definition of ModifiedCamClayYieldSurface
  KRATOS_CLASS_POINTER_DEFINITION(ModifiedCamClayYieldSurface);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  ModifiedCamClayYieldSurface() : BaseType() {}

  /// Copy constructor.
  ModifiedCamClayYieldSurface(ModifiedCamClayYieldSurface const &rOther) : BaseType(rOther) {}

  /// Assignment operator.
  ModifiedCamClayYieldSurface &operator=(ModifiedCamClayYieldSurface const &rOther)
  {
    BaseType::operator=(rOther);
    return *this;
  }

  /// Clone.
  BaseTypePointer Clone() const override
  {
    return Kratos::make_shared<ModifiedCamClayYieldSurface>(*this);
  }

  /// Destructor.
  ~ModifiedCamClayYieldSurface() override {}

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /**
     * Calculate Yield Condition
     */

  double &CalculateYieldCondition(const PlasticDataType &rVariables, double &rYieldCondition) override
  {
    KRATOS_TRY

    const ModelDataType &rModelData = rVariables.GetModelData();
    const MatrixType &rStressMatrix = rModelData.GetStressMatrix();

    // Material Parameters
    const Properties &rProperties = rModelData.GetProperties();
    const double &rShearM = rProperties[CRITICAL_STATE_LINE];
    const double &rFriction = rProperties[INTERNAL_FRICTION_ANGLE];

    // compute something with the hardening rule
    double PreconsolidationStress;
    PreconsolidationStress = this->mHardeningRule.CalculateHardening(rVariables, PreconsolidationStress);

    double MeanStress, LodeAngle;
    double DeviatoricQ; // == sqrt(3)*J2sqrt

    // more work is requiered
    StressInvariantsUtilities::CalculateStressInvariants(rStressMatrix, MeanStress, DeviatoricQ, LodeAngle);
    DeviatoricQ *= sqrt(3.0);

    double ThirdInvariantEffect = 1.0;
    if (rFriction > 0.0)
      ThirdInvariantEffect = ShapeAtDeviatoricPlaneUtility::EvaluateEffectDueToThirdInvariant(ThirdInvariantEffect, LodeAngle, rFriction);

    rYieldCondition = pow(ThirdInvariantEffect * DeviatoricQ / rShearM, 2);
    rYieldCondition += (MeanStress * (MeanStress - PreconsolidationStress));

    //std::cout << " yield funciton: p " << MeanStress << " q: " << DeviatoricQ << "  pc " << PreconsolidationStress << " yield value " << rYieldCondition << std::endl;
    return rYieldCondition;

    KRATOS_CATCH(" ")
  }

  //*************************************************************************************
  //*************************************************************************************
  // evaluation of the derivative of the yield surface respect the stresses
  VectorType &CalculateDeltaStressYieldCondition(const PlasticDataType &rVariables, VectorType &rDeltaStressYieldCondition) override
  {
    KRATOS_TRY

    const ModelDataType &rModelData = rVariables.GetModelData();
    const MatrixType &rStressMatrix = rModelData.GetStressMatrix();

    // Material Parameters
    const Properties &rProperties = rModelData.GetProperties();
    const double &rShearM = rProperties[CRITICAL_STATE_LINE];
    const double &rFriction = rProperties[INTERNAL_FRICTION_ANGLE];

    // compute something with the hardening rule
    double PreconsolidationStress;
    PreconsolidationStress = this->mHardeningRule.CalculateHardening(rVariables, PreconsolidationStress);

    double MeanStress, J2sqrt, LodeAngle;

    VectorType V1, V2, V3;
    // more work is requiered
    StressInvariantsUtilities::CalculateStressInvariants(rStressMatrix, MeanStress, J2sqrt, LodeAngle);
    StressInvariantsUtilities::CalculateDerivativeVectors(rStressMatrix, V1, V2, V3);

    double ThirdInvariantEffect(1), DerivativeEffect(0);
    bool MultiplySinInstead = false; 
    if (rFriction > 0.0)
      ShapeAtDeviatoricPlaneUtility::CalculateKLodeCoefficients(ThirdInvariantEffect, DerivativeEffect, LodeAngle, rFriction, true, MultiplySinInstead);

    rDeltaStressYieldCondition = (2.0 * MeanStress - PreconsolidationStress) * V1 + 2.0 * 3.0 * pow(ThirdInvariantEffect / rShearM, 2) * J2sqrt * V2;

    if (rFriction > 0.0 && J2sqrt > 1E-6)
    {
      double Term = pow(sqrt(3.0) * J2sqrt / rShearM, 2) * ThirdInvariantEffect * 2.0 * DerivativeEffect;
      double C2(0);
      double C3(0);
      if ( MultiplySinInstead){
           C2 = -std::sin(3.0 * LodeAngle) / J2sqrt * Term;
           C3 = -sqrt(3.0) / 2.0 / pow(J2sqrt, 3) * Term;
       }
       else{
           C2 = -std::tan(3.0 * LodeAngle) / J2sqrt * Term;
           C3 = -sqrt(3.0) / 2.0 / pow(J2sqrt, 3) / std::cos(3.0 * LodeAngle) * Term;
       }

      rDeltaStressYieldCondition += C2 * V2 + C3 * V3;
    }


    /*// now let's check the derivative.... just in case....
    VectorType DeltaStressYieldCondition;
    MatrixType s1; 
    MatrixType s2;
    MatrixType dg;


    double g1, g2;
    double delta = 1E-8;
    
    const double& rP0   = PreconsolidationStress;

    for (unsigned int i = 0; i < 3; i++) {
       for (unsigned int j = 0; j < 3; j++) {
          s1 = rStressMatrix;
          s2 = rStressMatrix;
          s1(i,j) += delta;
          s2(i,j) -= delta;

          g1 = EvaluateFFunction(g1, s1, rShearM, rFriction, rP0);
          g2 = EvaluateFFunction(g2, s2, rShearM, rFriction, rP0);

          dg(i,j) = (g1-g2)/(2.0*delta);
       }
    }

    ConstitutiveModelUtilities::StrainTensorToVector( dg, DeltaStressYieldCondition);

    std::cout << std::endl;
    Vector StressVector(6);
    noalias( StressVector) = ZeroVector(6);
    ConstitutiveModelUtilities::StressTensorToVector( rStressMatrix, StressVector);
    std::cout << " STRESS " << StressVector << std::endl;
    std::cout << " rP0 " << rP0 << std::endl;
    std::cout << " YieldFunction   Real " << rDeltaStressYieldCondition << std::endl;
    std::cout << " numerical derivative " << DeltaStressYieldCondition << std::endl;
    std::cout << std::endl;
    std::cout << " Lode " << LodeAngle*180.0/3.1415 << " ThirdIva " << ThirdInvariantEffect << std::endl;
    std::cout << " dKdL " << DerivativeEffect <<  " fri " << rFriction << std::endl;
    std::cout << std::endl;
    std::cout << std::endl;*/



    return rDeltaStressYieldCondition;

    KRATOS_CATCH(" ")
  }

  /**
    * Get required properties
    */
  void GetRequiredProperties(Properties &rProperties) override
  {
    KRATOS_TRY

    // Set required properties
    double value = 0.0; //dummy
    rProperties.SetValue(CRITICAL_STATE_LINE,value);
    rProperties.SetValue(INTERNAL_FRICTION_ANGLE,value);

    BaseType::GetRequiredProperties(rProperties);

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "ModifiedCamClayYieldSurface";
    return buffer.str();
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "ModifiedCamClayYieldSurface";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "ModifiedCamClayYieldSurface Data";
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{
  double & EvaluateFFunction( double & rF, const MatrixType & rStressMatrix, const double & rShearM, const double & rFriction, const double & PreconsolidationStress)
  {
     KRATOS_TRY

    //calculate stress invariants
    double MeanStress, DeviatoricQ, LodeAngle;
    StressInvariantsUtilities::CalculateStressInvariants(rStressMatrix, MeanStress, DeviatoricQ, LodeAngle);
    DeviatoricQ *= sqrt(3.0);

    double ThirdInvariantEffect = 1.0;
    if (rFriction > 0.0)
      ThirdInvariantEffect = ShapeAtDeviatoricPlaneUtility::EvaluateEffectDueToThirdInvariant(ThirdInvariantEffect, LodeAngle, rFriction);

    rF = pow(ThirdInvariantEffect * DeviatoricQ / rShearM, 2);
    rF += (MeanStress * (MeanStress - PreconsolidationStress));

    return rF;


     KRATOS_CATCH("")
  }

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BaseType)
  }

  void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BaseType)
  }

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class ModifiedCamClayYieldSurface

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_MODIFIED_CAM_CLAY_YIELD_SURFACE_HPP_INCLUDED  defined
