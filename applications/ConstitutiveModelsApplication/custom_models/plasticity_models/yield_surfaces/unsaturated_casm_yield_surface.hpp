//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:               December 2021 $
//
//

#if !defined(KRATOS_UNSAT_CASM_YIELD_SURFACE_HPP_INCLUDED)
#define KRATOS_UNSAT_CASM_YIELD_SURFACE_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/yield_surfaces/casm_yield_surface.hpp"

// Variables
// 0 Plastic Multiplier
// 1 Volumetric Plastic Strain
// 2 Dev Plastic Strain
// 3 Abs Value Volumetric Plastic Strain
// 4 B (bounding)
// 5 pc preconsolidation
// 6 pt
// 7 ps

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
template <class THardeningRule>
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) UnsaturatedCasmYieldSurface : public CasmYieldSurface<THardeningRule>
{
public:
  ///@name Type Definitions
  ///@{

  typedef ConstitutiveModelData::MatrixType MatrixType;
  typedef ConstitutiveModelData::VectorType VectorType;
  typedef ConstitutiveModelData::ModelData ModelDataType;
  typedef ConstitutiveModelData::MaterialData MaterialDataType;

  typedef CasmYieldSurface<THardeningRule> BaseType;
  typedef typename YieldSurface<THardeningRule>::Pointer BaseTypePointer;
  typedef typename BaseType::PlasticDataType PlasticDataType;

  /// Pointer definition of UnsaturatedCasmYieldSurface
  KRATOS_CLASS_POINTER_DEFINITION(UnsaturatedCasmYieldSurface);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  UnsaturatedCasmYieldSurface() : BaseType() {}

  /// Default constructor.
  UnsaturatedCasmYieldSurface(BaseTypePointer const &rpPlasticPotential) : BaseType(rpPlasticPotential) {}

  /// Copy constructor.
  UnsaturatedCasmYieldSurface(UnsaturatedCasmYieldSurface const &rOther) : BaseType(rOther) {}

  /// Assignment operator.
  UnsaturatedCasmYieldSurface &operator=(UnsaturatedCasmYieldSurface const &rOther)
  {
    BaseType::operator=(rOther);
    return *this;
  }

  /// Clone.
  virtual BaseTypePointer Clone() const override
  {
    return BaseTypePointer(new UnsaturatedCasmYieldSurface(*this));
  }

  /// Destructor.
  ~UnsaturatedCasmYieldSurface() override {}

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /**
     * Calculate Yield Condition
     */

  
  //*************************************************************************************
  //*************************************************************************************
  // evaluation of the derivative of the yield surface respect suction
  virtual double & CalculateDeltaSuctionYieldCondition(const PlasticDataType & rVariables, const double & rSuction, double & rDeltaSuctionYieldCondition) override
  {

     KRATOS_TRY

     const ModelDataType &rModelData = rVariables.GetModelData();
     const Properties &rMaterialProperties = rModelData.GetProperties();

     //get constants
     const double &rSpacingR = rMaterialProperties[SPACING_RATIO];
     const double & rLambda = rMaterialProperties[NORMAL_COMPRESSION_SLOPE];
     const double & rKappa = rMaterialProperties[SWELLING_SLOPE];
     const double & rPref = rMaterialProperties[REFERENCE_PRESSURE];

     const double & rR    = rMaterialProperties[R_BBM];
     const double & rBeta = rMaterialProperties[BETA_BBM];

     const double & rPC = rVariables.Data.Internal(5);
     const double & rPCSat = rVariables.Data.Internal(6);
       
     double lambdaS = rLambda * (  (1.0-rR)*exp(-rBeta*rSuction) + rR);
     
     
     double dF_dpC = -1.0/(std::log(rSpacingR)*rPC);
     double dpC_dLambdaS = (rPref) * std::log( -rPCSat/rPref) * pow( -rPCSat/rPref, (rLambda-rKappa)/(lambdaS - rKappa) ) * (rLambda-rKappa) / pow( lambdaS-rKappa, 2);

     double dLambdaS_dS = rLambda * ( 1.0-rR) * ( -rBeta) * std::exp( - rBeta * rSuction);
     rDeltaSuctionYieldCondition = dF_dpC*dpC_dLambdaS*dLambdaS_dS;

     return rDeltaSuctionYieldCondition;
     
     KRATOS_CATCH("")

  }



  /**
    * Get required properties
    */
  void GetRequiredProperties(Properties &rProperties) override
  {
    KRATOS_TRY

    // Set required properties
    double value = 0.0; //dummy
    rProperties.SetValue(CRITICAL_STATE_LINE,value);
    rProperties.SetValue(INTERNAL_FRICTION_ANGLE,value);
    rProperties.SetValue(SPACING_RATIO,value);
    rProperties.SetValue(SHAPE_PARAMETER,value);

    BaseType::GetRequiredProperties(rProperties);

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  virtual std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "UnsaturatedCasmYieldSurface";
    return buffer.str();
  }

  /// Print information about this object.
  virtual void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "UnsaturatedCasmYieldSurface";
  }

  /// Print object's data.
  virtual void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "UnsaturatedCasmYieldSurface Data";
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  virtual void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BaseType)
  }

  virtual void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BaseType)
  }

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class UnsaturatedCasmYieldSurface

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_UNSAT_CASM_YIELD_SURFACE_HPP_INCLUDED  defined
