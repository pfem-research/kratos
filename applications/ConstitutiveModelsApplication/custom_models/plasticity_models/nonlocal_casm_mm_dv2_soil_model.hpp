//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:      LMonforte-LHauser $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:                  April 2017 $
//
//

#if !defined(KRATOS_NONLOCAL_CASM_MM_DV2_SOIL_MODEL_HPP_INCLUDED)
#define KRATOS_NONLOCAL_CASM_MM_DV2_SOIL_MODEL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/casm_base_soil_model.hpp"
#include "custom_models/plasticity_models/hardening_rules/casm_hardening_rule.hpp"
#include "custom_models/plasticity_models/yield_surfaces/casm_yield_surface.hpp"
#include "custom_models/elasticity_models/dv2_borja_model.hpp"

#include "custom_models/plasticity_models/yield_surfaces/plastic_potential/mm_plastic_potential.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
    */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) NonlocalCasmMMdv2SoilModel : public CasmBaseSoilModel<DV2BorjaModel, CasmYieldSurface<CasmHardeningRule>>
{
public:
   ///@name Type Definitions
   ///@{

   //elasticity model
   typedef DV2BorjaModel ElasticityModelType;
   typedef ElasticityModelType::Pointer ElasticityModelPointer;

   //yield surface
   typedef CasmHardeningRule HardeningRuleType;
   typedef CasmYieldSurface<HardeningRuleType> YieldSurfaceType;
   typedef YieldSurfaceType::Pointer YieldSurfacePointer;

   //base type
   typedef CasmBaseSoilModel<ElasticityModelType, YieldSurfaceType> BaseType;

   //common types
   typedef BaseType::Pointer BaseTypePointer;
   typedef BaseType::SizeType SizeType;
   typedef BaseType::VoigtIndexType VoigtIndexType;
   typedef BaseType::MatrixType MatrixType;
   typedef BaseType::ModelDataType ModelDataType;
   typedef BaseType::MaterialDataType MaterialDataType;
   typedef BaseType::PlasticDataType PlasticDataType;
   typedef BaseType::InternalVariablesType InternalVariablesType;

   /// Pointer definition of NonlocalCasmMMdv2SoilModel
   KRATOS_CLASS_POINTER_DEFINITION(NonlocalCasmMMdv2SoilModel);

   ///@}
   ///@name Life Cycle
   ///@{

   /// Default constructor.
   NonlocalCasmMMdv2SoilModel() : BaseType()
   {
      MMPlasticPotential<CasmHardeningRule> Object;
      YieldSurface<CasmHardeningRule>::Pointer pPlasticPotential = Object.Clone();
      mYieldSurface = CasmYieldSurface<CasmHardeningRule>(pPlasticPotential);
   }

   /// Copy constructor.
   NonlocalCasmMMdv2SoilModel(NonlocalCasmMMdv2SoilModel const &rOther) : BaseType(rOther) {}

   /// Assignment operator.
   NonlocalCasmMMdv2SoilModel &operator=(NonlocalCasmMMdv2SoilModel const &rOther)
   {
      BaseType::operator=(rOther);
      return *this;
   }

   /// Clone.
   ConstitutiveModel::Pointer Clone() const override
   {
      return Kratos::make_shared<NonlocalCasmMMdv2SoilModel>(*this);
   }

   /// Destructor.
   ~NonlocalCasmMMdv2SoilModel() override {}

   ///@}
   ///@name Operators
   ///@{

   ///@}
   ///@name Operations
   ///@{

   /**
          * Check
          */
   int Check(const Properties &rProperties, const ProcessInfo &rCurrentProcessInfo) override
   {
      KRATOS_TRY

      //LMV: to be implemented. but should not enter in the base one

      return 0;

      KRATOS_CATCH("")
   }

   ///@}
   ///@name Access
   ///@{

   /**
          * Has Values
          */
   bool Has(const Variable<double> &rVariable) override
   {
      if (rVariable == PLASTIC_STRAIN || rVariable == DELTA_PLASTIC_STRAIN)
         return true;

      return false;
   }

   /**
          * Get Values
          */
   double &GetValue(const Variable<double> &rVariable, double &rValue) override
   {

      KRATOS_TRY

      rValue = 0;

      if (rVariable == PLASTIC_VOL_DEF)
      {
         rValue = this->mInternal(1);
      }
      else if (rVariable == NONLOCAL_PLASTIC_VOL_DEF)
      {
         rValue = this->mInternal(8);
      }
      else if (rVariable == NONLOCAL_PLASTIC_VOL_DEF_ABS)
      {
         rValue = std::fabs(this->mInternal(8));
      }
      else if ( rVariable == NONLOCAL_HENCKY_ELASTIC_VOLUMETRIC_STRAIN) {
         rValue = this->mInternal(11);
      }
      else if ( rVariable == HENCKY_ELASTIC_VOLUMETRIC_STRAIN) {
         rValue = this->mInternal(10);
      }
      else if (rVariable == SHEAR_MODULUS) {
        rValue = this->mElasticityModel.GetValue(SHEAR_MODULUS, rValue);
      } else if (rVariable == M_MODULUS) {
         rValue = 1000.0; 
         if ( this->mInitialized) {
            double G = this->mElasticityModel.GetValue(SHEAR_MODULUS, rValue);
            rValue = mBulkModulus + 4.0/3.0*G;
         }
      } else if (rVariable == YOUNG_MODULUS) {
         rValue = 1000.0; 
         if ( this->mInitialized) {
            double G = this->mElasticityModel.GetValue(SHEAR_MODULUS, rValue);
            rValue = 9.0*G*mBulkModulus/(3.0*mBulkModulus + G);
         }
      } else {
         rValue = CasmBaseSoilModel::GetValue(rVariable, rValue);
      }

      return rValue;

      KRATOS_CATCH("")
   }

   /**
          * Set Values
          */
   void SetValue(const Variable<double> &rVariable,
                 const double &rValue,
                 const ProcessInfo &rCurrentProcessInfo) override
   {
      KRATOS_TRY

      if (rVariable == NONLOCAL_PLASTIC_VOL_DEF)
      {
        mInternal(8) = rValue;
      }
      else if ( rVariable == NONLOCAL_HENCKY_ELASTIC_VOLUMETRIC_STRAIN) {
        mInternal(11) = rValue;
      }
      else if ( rVariable == HENCKY_ELASTIC_VOLUMETRIC_STRAIN) {
        mInternal(10) = rValue;
      }
      else
      {
        BaseType::SetValue(rVariable, rValue, rCurrentProcessInfo);
      }

      KRATOS_CATCH("")
   }

   // Calculate Stress and constitutive tensor
   void CalculateStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix) override
   {
      KRATOS_TRY

      { // modify the internal variables to make it nonlocal
      }

      const double LocalPlasticVolStrain = mInternal(1);
      const double NonLocalPlasticVolStrain = mInternal(8);
      mInternal(1) = mInternal(8);

      double LocalHenckyVolumetricStrain = mInternal(10);
      double NonLocalHenckyVolumetricStrain = mInternal(11);
      ProcessInfo SomeProcessInfo;
      if ( NonLocalHenckyVolumetricStrain!= LocalHenckyVolumetricStrain) {
         this->mElasticityModel.SetValue( HENCKY_ELASTIC_VOLUMETRIC_STRAIN, NonLocalHenckyVolumetricStrain, SomeProcessInfo);
      }
      mInternal(10) = mInternal(11);

      // integrate "analytically" ps and pt from plastic variables. Then update the internal variables.

      PlasticDataType Variables;
      this->InitializeVariables(rValues, Variables);

      const ModelDataType &rModelData = Variables.GetModelData();
      const Properties &rMaterialProperties = rModelData.GetProperties();

      const double &rInitialPrecon = rMaterialProperties[PRE_CONSOLIDATION_STRESS];
      const double &rSwellingSlope = rMaterialProperties[SWELLING_SLOPE];
      const double &rOtherSlope = rMaterialProperties[NORMAL_COMPRESSION_SLOPE];

      const double &rPlasticVolDef = Variables.Data.Internal(1);
      double &rPreconsolidation = mInternal(5);

      rPreconsolidation = -rInitialPrecon * std::exp(-rPlasticVolDef / (rOtherSlope - rSwellingSlope));
      CasmBaseSoilModel::CalculateStressAndConstitutiveTensors(rValues, rStressMatrix, rConstitutiveMatrix);

      if (rValues.State.Is(ConstitutiveModelData::UPDATE_INTERNAL_VARIABLES))
      {
         mInternal(8,1) = mInternal(8);
         mInternal(8) = mInternal(1);
         mInternal(1) = LocalPlasticVolStrain + (mInternal(1) - NonLocalPlasticVolStrain);

         mInternal(11,1) = mInternal(11);
         double CurrentHenckyVol(0);
         /// not updated
         MatrixType HenckyStrain;
         noalias( HenckyStrain) = ZeroMatrix(3,3);
         ConvertCauchyGreenTensorToHenckyTensor( rValues.StrainMatrix, HenckyStrain);
         for (unsigned int i = 0; i < 3; i++)
            CurrentHenckyVol += HenckyStrain(i,i);

         this->mInternal(10,1) = LocalHenckyVolumetricStrain;
         this->mInternal(11,1) = NonLocalHenckyVolumetricStrain;
         this->mInternal(11) = CurrentHenckyVol; // in case it is local....
         this->mInternal(10) = LocalHenckyVolumetricStrain + ( CurrentHenckyVol - NonLocalHenckyVolumetricStrain);
      }
      else
      {
        mInternal(1) = LocalPlasticVolStrain;
        mInternal(8) = NonLocalPlasticVolStrain;
        mInternal(10) = LocalHenckyVolumetricStrain;
        mInternal(11) = NonLocalHenckyVolumetricStrain;
      }

      KRATOS_CATCH("")
   }
   ///@}
   ///@name Inquiry
   ///@{

   ///@}
   ///@name Input and output
   ///@{

   /// Turn back information as a string.
   std::string Info() const override
   {
      std::stringstream buffer;
      buffer << "NonlocalCasmMMdv2SoilModel";
      return buffer.str();
   }

   /// Print information about this object.
   void PrintInfo(std::ostream &rOStream) const override
   {
      rOStream << "NonlocalCasmMMdv2SoilModel";
   }

   /// Print object's data.
   void PrintData(std::ostream &rOStream) const override
   {
      rOStream << "NonlocalCasmMMdv2SoilModel Data";
   }

   ///@}
   ///@name Friends
   ///@{

   ///@}

protected:
   ///@name Protected static Member Variables
   ///@{

   ///@}
   ///@name Protected member Variables
   ///@{

   ///@}
   ///@name Protected Operators
   ///@{

   ///@}
   ///@name Protected Operations
   ///@{
   

   ///@}
   ///@name Protected  Access
   ///@{

   ///@}
   ///@name Protected Inquiry
   ///@{

   ///@}
   ///@name Protected LifeCycle
   ///@{

   ///@}

private:
   ///@name Static Member Variables
   ///@{

   ///@}
   ///@name Member Variables
   ///@{

   ///@}
   ///@name Private Operators
   ///@{

   ///@}
   ///@name Private Operations
   ///@{

   ///@}
   ///@name Private  Access
   ///@{

   ///@}
   ///@name Private Inquiry
   ///@{

   ///@}
   ///@name Serialization
   ///@{
   friend class Serializer;

   void save(Serializer &rSerializer) const override
   {
      KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BaseType)
   }

   void load(Serializer &rSerializer) override
   {
      KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BaseType)
   }

   ///@}
   ///@name Un accessible methods
   ///@{

   ///@}

}; // Class NonlocalCasmMMdv2SoilModel

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_NONLOCAL_CASM_dv2_MM_SOIL_MODEL_HPP_INCLUDED  defined
