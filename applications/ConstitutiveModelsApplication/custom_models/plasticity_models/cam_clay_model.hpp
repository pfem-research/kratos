//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:                  April 2017 $
//
//

#if !defined(KRATOS_CAM_CLAY_MODEL_HPP_INCLUDED)
#define KRATOS_CAM_CLAY_MODEL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/soil_base_model.hpp"
#include "custom_models/plasticity_models/hardening_rules/cam_clay_hardening_rule.hpp"
#include "custom_models/plasticity_models/yield_surfaces/modified_cam_clay_yield_surface.hpp"
#include "custom_models/elasticity_models/borja_model.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
    */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) CamClayModel : public SoilBaseModel<BorjaModel, ModifiedCamClayYieldSurface<CamClayHardeningRule>>
{
public:
   ///@name Type Definitions
   ///@{

   //elasticity model
   typedef BorjaModel ElasticityModelType;
   typedef ElasticityModelType::Pointer ElasticityModelPointer;

   //yield surface
   typedef CamClayHardeningRule HardeningRuleType;
   typedef ModifiedCamClayYieldSurface<HardeningRuleType> YieldSurfaceType;
   typedef YieldSurfaceType::Pointer YieldSurfacePointer;

   //base type
   typedef SoilBaseModel<ElasticityModelType, YieldSurfaceType> BaseType;
    
   typedef typename BaseType::NameType NameType;

   //common types
   typedef BaseType::Pointer BaseTypePointer;
   typedef BaseType::SizeType SizeType;
   typedef BaseType::VoigtIndexType VoigtIndexType;
   typedef BaseType::MatrixType MatrixType;
   typedef BaseType::ModelDataType ModelDataType;
   typedef BaseType::MaterialDataType MaterialDataType;
   typedef BaseType::PlasticDataType PlasticDataType;
   typedef BaseType::InternalVariablesType InternalVariablesType;

   /// Pointer definition of CamClayModel
   KRATOS_CLASS_POINTER_DEFINITION(CamClayModel);

   ///@}
   ///@name Life Cycle
   ///@{

   /// Default constructor.
   CamClayModel() : BaseType()  {  }

   /// Copy constructor.
   CamClayModel(CamClayModel const &rOther) : BaseType(rOther),
   mShearModulus(rOther.mShearModulus), mBulkModulus(rOther.mBulkModulus) {}

   /// Assignment operator.
   CamClayModel &operator=(CamClayModel const &rOther)
   {
      BaseType::operator=(rOther);
      this->mShearModulus = rOther.mShearModulus;
      this->mBulkModulus = rOther.mBulkModulus;
      return *this;
   }

   /// Clone.
   ConstitutiveModel::Pointer Clone() const override
   {
      return Kratos::make_shared<CamClayModel>(*this);
   }

   /// Destructor.
   ~CamClayModel() override {}

   ///@}
   ///@name Operators
   ///@{

   ///@}
   ///@name Operations
   ///@{
   /**
          * Initialize member data
          */
   void InitializeModel(ModelDataType &rValues) override
   {
      KRATOS_TRY

      if (this->mInitialized == false)
      {
         PlasticDataType Variables;
         this->InitializeVariables(rValues, Variables);

         const ModelDataType &rModelData = Variables.GetModelData();
         const Properties &rMaterialProperties = rModelData.GetProperties();

         double ElasticModulus = rMaterialProperties[PRE_CONSOLIDATION_STRESS];
         ElasticModulus /= rMaterialProperties[SWELLING_SLOPE];

         MatrixType Stress;
         this->UpdateInternalVariables(rValues, Variables, Stress);

         this->mInitialized = true;

         mBulkModulus = ElasticModulus;
         mShearModulus = ElasticModulus;
      }
      this->mElasticityModel.InitializeModel(rValues);

      KRATOS_CATCH("")
   }

   /**
          * Check
          */
   int Check(const Properties &rProperties, const ProcessInfo &rCurrentProcessInfo) override
   {
      KRATOS_TRY

      //LMV: to be implemented. but should not enter in the base one

      return 0;

      KRATOS_CATCH("")
   }

   /**
       * Get required properties
       */
   void GetRequiredProperties(Properties &rProperties) override
   {
      KRATOS_TRY

      // Set required properties to check keys
      BaseType::GetRequiredProperties(rProperties);

      KRATOS_CATCH(" ")
   }
   ///@}
   ///@name Access
   ///@{

   /**
          * Has Values
          */
   bool Has(const Variable<double> &rVariable) override
   {
      if (rVariable == PLASTIC_STRAIN || rVariable == DELTA_PLASTIC_STRAIN)
         return true;

      return false;
   }

 

   /**
          * Get Values
          */
   double &GetValue(const Variable<double> &rVariable, double &rValue) override
   {

      rValue = 0;

      if (rVariable == M_MODULUS)
      {
         rValue = 1e3;
         if (this->mInitialized)
         {
            rValue = mBulkModulus + 4.0/3.0*mShearModulus;
         }
      }
      else if (rVariable == YOUNG_MODULUS)
      {
         rValue = 1e3;
         if (this->mInitialized)
         {
            rValue = 9.0*mShearModulus*mBulkModulus/(3.0*mBulkModulus+mShearModulus);
         }
      }
      else if (rVariable == SHEAR_MODULUS)
      {
         rValue = 1e3;
         if (this->mInitialized)
         {
            rValue = mShearModulus;
         }
      }
      else if (rVariable == BULK_MODULUS)
      {
         rValue = 1e3;
         if (this->mInitialized)
         {
            rValue = mBulkModulus;
         }
      }
      else if (rVariable == PLASTIC_STRAIN)
      {
         rValue = this->mInternal(NameType::PLASTIC_STRAIN);
      }
      else if (rVariable == DELTA_PLASTIC_STRAIN)
      {
         rValue = this->mInternal(0) - this->mInternal(0, 1);
      }
      else if (rVariable == PRE_CONSOLIDATION_STRESS)
      {
         rValue = this->mInternal(NameType::PRE_CONSOLIDATION_STRESS);
      }

      else
      {
         rValue = SoilBaseModel::GetValue(rVariable, rValue);
      }
      return rValue;
   }

   ///@}
   ///@name Inquiry
   ///@{

   ///@}
   ///@name Input and output
   ///@{

   /// Turn back information as a string.
   std::string Info() const override
   {
      std::stringstream buffer;
      buffer << "CamClayModel";
      return buffer.str();
   }

   /// Print information about this object.
   void PrintInfo(std::ostream &rOStream) const override
   {
      rOStream << "CamClayModel";
   }

   /// Print object's data.
   void PrintData(std::ostream &rOStream) const override
   {
      rOStream << "CamClayModel Data";
   }

   ///@}
   ///@name Friends
   ///@{

   ///@}

protected:
   ///@name Protected static Member Variables
   ///@{

   ///@}
   ///@name Protected member Variables
   ///@{


   double mShearModulus;
   double mBulkModulus;

   ///@}
   ///@name Protected Operators
   ///@{

   ///@}
   ///@name Protected Operations
   ///@{
   //********************************************************************
   //********************************************************************
   // UpdateInternalVariables
   virtual void UpdateInternalVariables(ModelDataType &rValues, PlasticDataType &rVariables, const MatrixType &rStressMatrix) override
   {
      KRATOS_TRY

         BaseType::UpdateInternalVariables(rValues, rVariables, rStressMatrix);
         const ModelDataType &rModelData = rVariables.GetModelData();
         const Properties &rMaterialProperties = rModelData.GetProperties();
         const double &rSwellingSlope = rMaterialProperties[SWELLING_SLOPE];
         const double &rAlpha = rMaterialProperties[ALPHA_SHEAR];

         double MeanStress = 0;
         for (unsigned int i = 0; i < 3; i++)
            MeanStress += rValues.StressMatrix(i, i) / 3.0;
         double K = (-MeanStress) / rSwellingSlope;
         double G = rMaterialProperties[INITIAL_SHEAR_MODULUS];
         G += rAlpha * (-MeanStress);
         mBulkModulus = K;
         mShearModulus = G;



      KRATOS_CATCH("")
   }

   ///@}
   ///@name Protected  Access
   ///@{

   ///@}
   ///@name Protected Inquiry
   ///@{

   ///@}
   ///@name Protected LifeCycle
   ///@{

   ///@}

private:
   ///@name Static Member Variables
   ///@{

   ///@}
   ///@name Member Variables
   ///@{

   ///@}
   ///@name Private Operators
   ///@{

   ///@}
   ///@name Private Operations
   ///@{

   ///@}
   ///@name Private  Access
   ///@{

   ///@}
   ///@name Private Inquiry
   ///@{

   ///@}
   ///@name Serialization
   ///@{
   friend class Serializer;

   void save(Serializer &rSerializer) const override
   {
      KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BaseType)
      rSerializer.save("BulkModulus", mBulkModulus);
      rSerializer.save("ShearModulus", mShearModulus);
   }

   void load(Serializer &rSerializer) override
   {
      KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BaseType)
      rSerializer.load("BulkModulus", mBulkModulus);
      rSerializer.load("ShearModulus", mShearModulus);
   }

   ///@}
   ///@name Un accessible methods
   ///@{

   ///@}

}; // Class CamClayModel

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_CAM_CLAY_MODEL_HPP_INCLUDED  defined
