//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:                  March 2021 $
//
//

#if !defined(KRATOS_DV_STRUCTURED_CASM_MCC_SOIL_MODEL_HPP_INCLUDED)
#define      KRATOS_DV_STRUCTURED_CASM_MCC_SOIL_MODEL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/structured_casm_base_soil_model.hpp"
#include "custom_models/plasticity_models/hardening_rules/casm_structure_hardening_rule.hpp"
#include "custom_models/plasticity_models/yield_surfaces/casm_structure_yield_surface.hpp"
#include "custom_models/elasticity_models/dv_borja_model.hpp"

#include "custom_models/plasticity_models/yield_surfaces/plastic_potential/modified_cam_clay_structure_plastic_potential.hpp"


// TRYING TO DO A CASM WITH non-constant shear modulus

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
    */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) DVStructuredCasmMCCSoilModel : public StructuredCasmBaseSoilModel<DVBorjaModel, CasmStructureYieldSurface<CasmStructureHardeningRule>>
{
public:
   ///@name Type Definitions
   ///@{

   //elasticity model
   typedef DVBorjaModel ElasticityModelType;
   typedef ElasticityModelType::Pointer ElasticityModelPointer;

   //yield surface
   typedef CasmStructureHardeningRule HardeningRuleType;
   typedef CasmStructureYieldSurface<HardeningRuleType> YieldSurfaceType;
   typedef YieldSurfaceType::Pointer YieldSurfacePointer;

   //base type
   typedef StructuredCasmBaseSoilModel<ElasticityModelType, YieldSurfaceType> BaseType;


   //common types
   typedef BaseType::Pointer BaseTypePointer;
   typedef BaseType::SizeType SizeType;
   typedef BaseType::VoigtIndexType VoigtIndexType;
   typedef BaseType::MatrixType MatrixType;
   typedef BaseType::ModelDataType ModelDataType;
   typedef BaseType::MaterialDataType MaterialDataType;
   typedef BaseType::PlasticDataType PlasticDataType;
   typedef BaseType::InternalVariablesType InternalVariablesType;

   /// Pointer definition of DVStructuredCasmMCCSoilModel
   KRATOS_CLASS_POINTER_DEFINITION(DVStructuredCasmMCCSoilModel);

   ///@}
   ///@name Life Cycle
   ///@{

   /// Default constructor.
   DVStructuredCasmMCCSoilModel() : BaseType()
   {
      //ModifiedCamClayPlasticPotential<CasmStructureHardeningRule> Object;
      ModifiedCamClayStructurePlasticPotential<CasmStructureHardeningRule> Object;
      YieldSurface<CasmStructureHardeningRule>::Pointer pPlasticPotential = Object.Clone();
      mYieldSurface = CasmStructureYieldSurface<CasmStructureHardeningRule>(pPlasticPotential);
   }

   /// Copy constructor.
   DVStructuredCasmMCCSoilModel(DVStructuredCasmMCCSoilModel const &rOther) : BaseType(rOther) {}

   /// Assignment operator.
   DVStructuredCasmMCCSoilModel &operator=(DVStructuredCasmMCCSoilModel const &rOther)
   {
      BaseType::operator=(rOther);
      return *this;
   }

   /// Clone.
   ConstitutiveModel::Pointer Clone() const override
   {
      return Kratos::make_shared<DVStructuredCasmMCCSoilModel>(*this);
   }

   /// Destructor.
   ~DVStructuredCasmMCCSoilModel() override {}

   ///@}
   ///@name Operators
   ///@{

   ///@}
   ///@name Operations
   ///@{

   /**
          * Check
          */
   int Check(const Properties &rProperties, const ProcessInfo &rCurrentProcessInfo) override
   {
      KRATOS_TRY

      //LMV: to be implemented. but should not enter in the base one

      return 0;

      KRATOS_CATCH("")
   }

   ///@}
   ///@name Access
   ///@{

   /**
          * Has Values
          */

   virtual double &GetValue(const Variable<double> &rThisVariable, double &rValue) override
   {
      KRATOS_TRY

      rValue = 0;
      if (rThisVariable == SHEAR_MODULUS)
      {
        rValue = this->mElasticityModel.GetValue(rThisVariable, rValue);
      }
      else if (rThisVariable == BULK_MODULUS)
      {
         rValue = 1000.0; 
         if ( this->mInitialized) {
            rValue = mBulkModulus;
         }
      }
      else if (rThisVariable == M_MODULUS)
      {
         rValue = 1000.0; 
         if ( this->mInitialized) {
            double G = this->mElasticityModel.GetValue(SHEAR_MODULUS, rValue);
            rValue = mBulkModulus + 4.0/3.0*G;
         }
      }
      else if (rThisVariable == YOUNG_MODULUS)
      {
         rValue = 1000.0; 
         if ( this->mInitialized) {
            double G = this->mElasticityModel.GetValue(SHEAR_MODULUS, rValue);
            rValue = 9.0*G*mBulkModulus/(3.0*mBulkModulus + G);
         }
      }
      else
      {
         rValue = StructuredCasmBaseSoilModel<ElasticityModelType, YieldSurfaceType>::GetValue(rThisVariable, rValue);
      }
      return rValue;

      KRATOS_CATCH("")
   }


   ///@}
   ///@name Inquiry
   ///@{

   ///@}
   ///@name Input and output
   ///@{

   /// Turn back information as a string.
   std::string Info() const override
   {
      std::stringstream buffer;
      buffer << "DVStructuredCasmMCCSoilModel";
      return buffer.str();
   }

   /// Print information about this object.
   void PrintInfo(std::ostream &rOStream) const override
   {
      rOStream << "DVStructuredCasmMCCSoilModel";
   }

   /// Print object's data.
   void PrintData(std::ostream &rOStream) const override
   {
      rOStream << "DVStructuredCasmMCCSoilModel Data";
   }

   ///@}
   ///@name Friends
   ///@{

   ///@}

protected:
   ///@name Protected static Member Variables
   ///@{

   ///@}
   ///@name Protected member Variables
   ///@{

   ///@}
   ///@name Protected Operators
   ///@{

   ///@}
   ///@name Protected Operations
   ///@{

   ///@}
   ///@name Protected  Access
   ///@{

   ///@}
   ///@name Protected Inquiry
   ///@{

   ///@}
   ///@name Protected LifeCycle
   ///@{

   ///@}

private:
   ///@name Static Member Variables
   ///@{

   ///@}
   ///@name Member Variables
   ///@{

   ///@}
   ///@name Private Operators
   ///@{

   ///@}
   ///@name Private Operations
   ///@{

   ///@}
   ///@name Private  Access
   ///@{

   ///@}
   ///@name Private Inquiry
   ///@{

   ///@}
   ///@name Serialization
   ///@{
   friend class Serializer;

   void save(Serializer &rSerializer) const override
   {
      KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BaseType)
   }

   void load(Serializer &rSerializer) override
   {
      KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BaseType)
   }

   ///@}
   ///@name Un accessible methods
   ///@{

   ///@}

}; // Class DVStructuredCasmMCCSoilModel

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_DV_STRUCTURED_CASM_MCC_SOIL_MODEL_HPP_INCLUDED  defined
