//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:                  March 2021 $
//
//

#if !defined(KRATOS_STRUCTURED_CASM_BASE_SOIL_MODEL_HPP_INCLUDED)
#define      KRATOS_STRUCTURED_CASM_BASE_SOIL_MODEL_HPP_INCLUDED

// System includes

// External includes
#include <iostream>
#include <fstream>

// Project includes
#include "custom_models/plasticity_models/soil_non_associative_model.hpp"

//***** the hardening law associated to this Model has ... variables
// 0. Plastic multiplier
// 1. Plastic Volumetric deformation
// 2. Plastic Deviatoric deformation
// 3. Plastic Volumetric deformation Absolut Value
// 4. p0 (mechanical)
// 5. pm (structure compression)
// 6. pt (structure tensile)
// 7. NonLocal Plastic Vol Def
// 8. NonLocal Plastic Dev Def
// 9. NonLocal Plastic Vol Def ABS
//10. Bonding
//11. HenkcyVolStrain
//12. Nonlocal HenckyVolStrain
// ... (the number now is then..., xD)

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
    */
template <class TElasticityModel, class TYieldSurface>
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) StructuredCasmBaseSoilModel : public SoilNonAssociativeModel<TElasticityModel, TYieldSurface>
{
public:
   ///@name Type Definitions
   ///@{

   //elasticity model
   typedef TElasticityModel ElasticityModelType;

   //yield surface
   typedef TYieldSurface YieldSurfaceType;

   // derived type
   typedef SoilNonAssociativeModel<ElasticityModelType, YieldSurfaceType> DerivedType;

   //base type
   typedef PlasticityModel<ElasticityModelType, YieldSurfaceType> BaseType;

   //common types
   typedef typename BaseType::Pointer BaseTypePointer;
   typedef typename BaseType::SizeType SizeType;
   typedef typename BaseType::VoigtIndexType VoigtIndexType;
   typedef typename BaseType::MatrixType MatrixType;
   typedef typename BaseType::VectorType VectorType;
   typedef typename BaseType::ModelDataType ModelDataType;
   typedef typename BaseType::MaterialDataType MaterialDataType;
   typedef typename BaseType::PlasticDataType PlasticDataType;
   typedef typename BaseType::InternalVariablesType InternalVariablesType;

   /// Pointer definition of StructuredCasmBaseSoilModel
   KRATOS_CLASS_POINTER_DEFINITION(StructuredCasmBaseSoilModel);

   ///@}
   ///@name Life Cycle
   ///@{

   /// Default constructor.
   StructuredCasmBaseSoilModel() : DerivedType() {  }

   /// Copy constructor.
   StructuredCasmBaseSoilModel(StructuredCasmBaseSoilModel const &rOther) : DerivedType(rOther), mShearModulus(rOther.mShearModulus), mBulkModulus(rOther.mBulkModulus)  {}

   /// Assignment operator.
   StructuredCasmBaseSoilModel &operator=(StructuredCasmBaseSoilModel const &rOther)
   {
      DerivedType::operator=(rOther);
      this->mShearModulus = rOther.mShearModulus;
      this->mBulkModulus = rOther.mBulkModulus;
      return *this;
   }

   /// Clone.
   ConstitutiveModel::Pointer Clone() const override
   {
      return (StructuredCasmBaseSoilModel::Pointer(new StructuredCasmBaseSoilModel(*this)));
   }

   /// Destructor.
   ~StructuredCasmBaseSoilModel() override {}

   ///@}
   ///@name Operators
   ///@{

   ///@}
   ///@name Operations
   ///@{

   /**
          * Initialize member data
          */
   void InitializeModel(ModelDataType &rValues) override
   {
      KRATOS_TRY

      if ( this->mInitialized == false)
      {
         PlasticDataType Variables;
         this->InitializeVariables(rValues, Variables);

         const ModelDataType &rModelData = Variables.GetModelData();
         const Properties &rMaterialProperties = rModelData.GetProperties();

         double bonding = rMaterialProperties[BONDING];
         double Alpha_T = rMaterialProperties[ALPHA_TENSILE];

         double &rP0 = Variables.Data.Internal(4);
         double &rPM = Variables.Data.Internal(5);
         double &rPT = Variables.Data.Internal(6);
         double &rBonding = Variables.Data.Internal(10);

         rP0 = -rMaterialProperties[P0];
         rPM = -rMaterialProperties[P0]*bonding;
         rPT = -rMaterialProperties[P0]*bonding*Alpha_T;
         rBonding = bonding;

         MatrixType Stress;
         this->UpdateInternalVariables(rValues, Variables, Stress);

         mShearModulus = rMaterialProperties[INITIAL_SHEAR_MODULUS];
         mBulkModulus = rMaterialProperties[P0]/rMaterialProperties[SWELLING_SLOPE];

         this->mInitialized = true;
      }
      this->mElasticityModel.InitializeModel(rValues);

      KRATOS_CATCH("")
   }

   /**
          * Check
          */
   virtual int Check(const Properties &rMaterialProperties, const ProcessInfo &rCurrentProcessInfo) override
   {
      KRATOS_TRY

      //LMV: to be implemented. but should not enter in the base one

      return 0;

      KRATOS_CATCH("")
   }

   ///@}
   ///@name Access
   ///@{

   /**
          * Has Values
          */


   /**
          * Get Values
          */
   void SetValue(const Variable<double> &rVariable,
                 const double &rValue,
                 const ProcessInfo &rCurrentProcessInfo) override
   {
      KRATOS_TRY

      if (rVariable == NONLOCAL_PLASTIC_VOL_DEF)
      {
         this->mInternal(7) = rValue;
      }
      else if (rVariable == NONLOCAL_PLASTIC_DEV_DEF)
      {
         this->mInternal(8) = rValue;
      }
      else if (rVariable == NONLOCAL_PLASTIC_VOL_DEF_ABS)
      {
         this->mInternal(9) = rValue;
      }
      else if (rVariable == NONLOCAL_HENCKY_ELASTIC_VOLUMETRIC_STRAIN)
      {
         this->mInternal(12) = rValue;
      }
      else
      {
         SoilNonAssociativeModel<TElasticityModel, TYieldSurface>::SetValue(rVariable, rValue, rCurrentProcessInfo);
      }

      KRATOS_CATCH("")
   }

   /**
          * Get Values
          */
   virtual double &GetValue(const Variable<double> &rThisVariable, double &rValue) override
   {
      KRATOS_TRY

      rValue = 0;

      if (rThisVariable == PLASTIC_STRAIN)
      {
         rValue = this->mInternal(0);
      }
      else if (rThisVariable == DELTA_PLASTIC_STRAIN)
      {
         rValue = this->mInternal(0) - this->mInternal(0,1);
      }
      else if (rThisVariable == PLASTIC_VOL_DEF)
      {
         rValue = this->mInternal(1);
      }
      else if (rThisVariable == PLASTIC_DEV_DEF)
      {
         rValue = this->mInternal(2);
      }
      else if (rThisVariable == PLASTIC_VOL_DEF_ABS)
      {
         rValue = this->mInternal(3);
      }
      else if (rThisVariable == P0)
      {
         rValue = this->mInternal(4);
      }
      else if (rThisVariable == PM)
      {
         rValue = this->mInternal(5);
      }
      else if (rThisVariable == PT)
      {
         rValue = this->mInternal(6);
      }
      else if (rThisVariable == PS) {
         rValue = this->mInternal(4)+this->mInternal(5)+this->mInternal(6);
      }
      else if (rThisVariable == NONLOCAL_PLASTIC_VOL_DEF)
      {
         rValue = this->mInternal(7, 1);
      }
      else if (rThisVariable == NONLOCAL_PLASTIC_DEV_DEF)
      {
         rValue = this->mInternal(8, 1);
      }
      else if (rThisVariable == NONLOCAL_PLASTIC_VOL_DEF_ABS)
      {
         rValue = this->mInternal(9, 1);
      }
      else if (rThisVariable == BONDING)
      {
         rValue = this->mInternal(10);
      }
      else if (rThisVariable == HENCKY_ELASTIC_VOLUMETRIC_STRAIN)
      {
         rValue = this->mInternal(11);
      }
      else if (rThisVariable == NONLOCAL_HENCKY_ELASTIC_VOLUMETRIC_STRAIN)
      {
         rValue = this->mInternal(12, 1);
      }
      else if (rThisVariable == SHEAR_MODULUS)
      {
         rValue = 1000.0; 
         if ( this->mInitialized) {
            rValue = mShearModulus;
         }
      }
      else if (rThisVariable == BULK_MODULUS)
      {
         rValue = 1000.0; 
         if ( this->mInitialized) {
            rValue = mBulkModulus;
         }
      }
      else if (rThisVariable == M_MODULUS)
      {
         rValue = 1000.0; 
         if ( this->mInitialized) {
            rValue = mBulkModulus + 4.0/3.0*mShearModulus;
         }
      }
      else if (rThisVariable == YOUNG_MODULUS)
      {
         rValue = 1000.0; 
         if ( this->mInitialized) {
            rValue = 9.0*mShearModulus*mBulkModulus/(3.0*mBulkModulus + mShearModulus);
         }
      }
      else
      {
         rValue = SoilNonAssociativeModel<TElasticityModel, TYieldSurface>::GetValue(rThisVariable, rValue);
      }
      return rValue;

      KRATOS_CATCH("")
   }

   /////////////////////////////////////////////////////////////////////////////////
   // Calculate Stress and constitutive tensor
   void CalculateStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix) override
   {
      KRATOS_TRY

      { // modify the internal variables to make it nonlocal
      }

      const double LocalPlasticVolStrain = this->mInternal(1);
      const double NonLocalPlasticVolStrain = this->mInternal(7);
      this->mInternal(1) = this->mInternal(7);

      const double LocalPlasticDevStrain = this->mInternal(2);
      const double NonLocalPlasticDevStrain = this->mInternal(8);
      this->mInternal(2) = this->mInternal(8);

      double LocalPlasticVolStrainAbs = this->mInternal(3);
      double NonLocalPlasticVolStrainAbs = this->mInternal(9);
      /*{ // LMV
         if ( LocalPlasticVolStrainAbs > 2.0*fabs(LocalPlasticVolStrain) )
            LocalPlasticVolStrainAbs = 2.0*LocalPlasticVolStrain;
         if ( NonLocalPlasticVolStrainAbs > 2.0*fabs(NonLocalPlasticVolStrain) )
            NonLocalPlasticVolStrainAbs = 2.0*NonLocalPlasticVolStrain;

      }*/
      this->mInternal(3) = NonLocalPlasticVolStrainAbs;

      const double LocalHenckyVolumetricStrain = this->mInternal(11);
      const double NonLocalHenckyVolumetricStrain = this->mInternal(12);

      ProcessInfo SomeProcessInfo;
      if ( NonLocalHenckyVolumetricStrain!= LocalHenckyVolumetricStrain) {
         this->mElasticityModel.SetValue( HENCKY_ELASTIC_VOLUMETRIC_STRAIN, NonLocalHenckyVolumetricStrain, SomeProcessInfo);
      }
      this->mInternal(11) = this->mInternal(12);

      // integrate "analytically" ps and pt from plastic variables. Then update the internal variables.

      PlasticDataType Variables;
      this->InitializeVariables(rValues, Variables);

      const ModelDataType &rModelData = Variables.GetModelData();
      const Properties &rMaterialProperties = rModelData.GetProperties();

      const double & rBonding0 = rMaterialProperties[BONDING];
      const double & rP0Ref = rMaterialProperties[P0];
      const double & rH1 = rMaterialProperties[DEGRADATION_RATE_COMPRESSION];
      const double & rH2 = rMaterialProperties[DEGRADATION_RATE_SHEAR];
      const double & rAlphaT = rMaterialProperties[ALPHA_TENSILE];
      const double & rChis = rMaterialProperties[CHIS];
      const double & rSwellingSlope = rMaterialProperties[SWELLING_SLOPE];
      const double & rOtherSlope = rMaterialProperties[NORMAL_COMPRESSION_SLOPE];

      const double &rPlasticVolDef = Variables.Data.Internal(1);
      const double &rPlasticDevDef = Variables.Data.Internal(2);
      const double &rPlasticVolDefAbs = Variables.Data.Internal(3);

      double &rBonding = this->mInternal(10);
      double &rP0 = this->mInternal(4);
      double &rPM = this->mInternal(5);
      double &rPT = this->mInternal(6);

      rP0 = -rP0Ref * std::exp(-(rPlasticVolDef+rChis*sqrt(2.0/3.0)*rPlasticDevDef )/ (rOtherSlope - rSwellingSlope));

      rBonding = rBonding0 * exp( -(rH1*rPlasticVolDefAbs + rH2*sqrt(2.0/3.0)*rPlasticDevDef) );

      rPM = rBonding*rP0;
      rPT = rBonding*rAlphaT*rP0;


      // The computation itself... the previous is changing things from places
      DerivedType::CalculateStressAndConstitutiveTensors(rValues, rStressMatrix, rConstitutiveMatrix);


      if (rValues.State.Is(ConstitutiveModelData::UPDATE_INTERNAL_VARIABLES))
      {

         this->mInternal(7) = this->mInternal(1); 
         this->mInternal(8) = this->mInternal(2); 
         this->mInternal(9) = this->mInternal(3);

         this->mInternal(1) = LocalPlasticVolStrain + (this->mInternal(1) - NonLocalPlasticVolStrain);
         this->mInternal(2) = LocalPlasticDevStrain + (this->mInternal(2) - NonLocalPlasticDevStrain);
         this->mInternal(3) = LocalPlasticVolStrainAbs + (this->mInternal(3) - NonLocalPlasticVolStrainAbs);


         this->mInternal(11,1) = this->mInternal(11);
         double CurrentHenckyVol(0);
         /// not updated
         MatrixType HenckyStrain;
         noalias( HenckyStrain) = ZeroMatrix(3,3);
         this->ConvertCauchyGreenTensorToHenckyTensor( rValues.StrainMatrix, HenckyStrain);
         for (unsigned int i = 0; i < 3; i++)
            CurrentHenckyVol += HenckyStrain(i,i);


         this->mInternal(11,1) = LocalHenckyVolumetricStrain;
         this->mInternal(12,1) = NonLocalHenckyVolumetricStrain;
         this->mInternal(12) = CurrentHenckyVol; // in case it is local....
         this->mInternal(11) = LocalHenckyVolumetricStrain + ( CurrentHenckyVol - NonLocalHenckyVolumetricStrain);
      }
      else
      {
         this->mInternal(1)  = LocalPlasticVolStrain;
         this->mInternal(2)  = LocalPlasticDevStrain;
         this->mInternal(3)  = LocalPlasticVolStrainAbs;
         this->mInternal(11) = LocalHenckyVolumetricStrain;
      }


      KRATOS_CATCH("")
   }

   ///@}
   ///@name Inquiry
   ///@{

   ///@}
   ///@name Input and output
   ///@{

   /// Turn back information as a string.
   virtual std::string Info() const override
   {
      std::stringstream buffer;
      buffer << "StructuredCasmBaseSoilModel";
      return buffer.str();
   }

   /// Print information about this object.
   virtual void PrintInfo(std::ostream &rOStream) const override
   {
      rOStream << "StructuredCasmBaseSoilModel";
   }

   /// Print object's data.
   virtual void PrintData(std::ostream &rOStream) const override
   {
      rOStream << "StructuredCasmBaseSoilModel Data";
   }

   ///@}
   ///@name Friends
   ///@{

   ///@}

protected:
   ///@name Protected static Member Variables
   ///@{

   ///@}
   ///@name Protected member Variables
   ///@{

   double mShearModulus;
   double mBulkModulus;

   ///@}
   ///@name Protected Operators
   ///@{

   ///@}
   ///@name Protected Operations
   ///@{

   //***************************************************************************************
   //***************************************************************************************
   // Compute Elasto Plastic Matrix
   void ComputeElastoPlasticTangentMatrix(ModelDataType &rValues, PlasticDataType &rVariables, Matrix &rEPMatrix) override
   {

      KRATOS_TRY

      // evaluate constitutive matrix and plastic flow

      VectorType DeltaStressYieldCondition = this->mYieldSurface.CalculateDeltaStressYieldCondition(rVariables, DeltaStressYieldCondition);
      VectorType PlasticPotentialDerivative = this->mYieldSurface.CalculateDeltaPlasticPotential(rVariables, PlasticPotentialDerivative);

      MatrixType PlasticPotDerTensor;
      PlasticPotDerTensor = ConstitutiveModelUtilities::StrainVectorToTensor(PlasticPotentialDerivative, PlasticPotDerTensor);
      double H = this->mYieldSurface.GetHardeningRule().CalculateDeltaHardening(rVariables, H, PlasticPotDerTensor);

      VectorType AuxF = prod(trans(DeltaStressYieldCondition), rEPMatrix);
      VectorType AuxG = prod(rEPMatrix, PlasticPotentialDerivative);

      Matrix PlasticUpdateMatrix(6, 6);
      noalias(PlasticUpdateMatrix) = ZeroMatrix(6, 6);
      double denom = 0;
      for (unsigned int i = 0; i < 6; i++)
      {
         denom += AuxF(i) * PlasticPotentialDerivative(i);
         for (unsigned int j = 0; j < 6; j++)
         {
            PlasticUpdateMatrix(i, j) = AuxG(i) * AuxF(j);
         }
      }

      rEPMatrix -= PlasticUpdateMatrix / (H + denom);

      KRATOS_CATCH("")
   }

   //***********************************************************************************
   //***********************************************************************************
   // Compute one step of the elasto-plastic problem
   void ComputeOneStepElastoPlasticProblem(ModelDataType &rValues, PlasticDataType &rVariables, const MatrixType &rDeltaDeformationMatrix) override
   {
      KRATOS_TRY

      const ModelDataType &rModelData = rVariables.GetModelData();
      const Properties &rMaterialProperties = rModelData.GetProperties();

      const double & rBonding0 = rMaterialProperties[BONDING];
      const double & rP0Ref = rMaterialProperties[P0];

      const double & rH1 = rMaterialProperties[DEGRADATION_RATE_COMPRESSION];
      const double & rH2 = rMaterialProperties[DEGRADATION_RATE_SHEAR];
      const double & rAlphaT = rMaterialProperties[ALPHA_TENSILE];
      const double & rChis = rMaterialProperties[CHIS];
      const double & rSwellingSlope = rMaterialProperties[SWELLING_SLOPE];
      const double & rOtherSlope = rMaterialProperties[NORMAL_COMPRESSION_SLOPE];

      MatrixType StressMatrix;
      // evaluate constitutive matrix and plastic flow
      double &rPlasticMultiplier = rVariables.Data.Internal(0);
      double &rPlasticVolDef = rVariables.Data.Internal(1);
      double &rPlasticDevDef = rVariables.Data.Internal(2);
      double &rPlasticVolDefAbs = rVariables.Data.Internal(3);
      double &rP0 = rVariables.Data.Internal(4);
      double &rPM = rVariables.Data.Internal(5);
      double &rPT = rVariables.Data.Internal(6);
      double &rBonding = rVariables.Data.Internal(10);

      Matrix ElasticMatrix(6, 6);
      noalias(ElasticMatrix) = ZeroMatrix(6, 6);
      this->mElasticityModel.CalculateConstitutiveTensor(rValues, ElasticMatrix);

      VectorType DeltaStressYieldCondition = this->mYieldSurface.CalculateDeltaStressYieldCondition(rVariables, DeltaStressYieldCondition);
      VectorType PlasticPotentialDerivative = this->mYieldSurface.CalculateDeltaPlasticPotential(rVariables, PlasticPotentialDerivative);



      MatrixType PlasticPotDerTensor;
      PlasticPotDerTensor = ConstitutiveModelUtilities::StrainVectorToTensor(PlasticPotentialDerivative, PlasticPotDerTensor);
      double H = this->mYieldSurface.GetHardeningRule().CalculateDeltaHardening(rVariables, H, PlasticPotDerTensor);

      MatrixType StrainMatrix = prod(rDeltaDeformationMatrix, trans(rDeltaDeformationMatrix));
      VectorType StrainVector;
      this->ConvertCauchyGreenTensorToHenckyVector(StrainMatrix, StrainVector);

      VectorType AuxVector;
      AuxVector = prod(ElasticMatrix, StrainVector);
      double DeltaGamma;
      DeltaGamma = MathUtils<double>::Dot(AuxVector, DeltaStressYieldCondition);

      double Denominador = H + MathUtils<double>::Dot(DeltaStressYieldCondition, prod(ElasticMatrix, PlasticPotentialDerivative));

      DeltaGamma /= Denominador;

      if (DeltaGamma < 0)
         DeltaGamma = 0;

      MatrixType UpdateMatrix;
      this->ConvertHenckyVectorToCauchyGreenTensor(-DeltaGamma * PlasticPotentialDerivative / 2.0, UpdateMatrix);
      UpdateMatrix = prod(rDeltaDeformationMatrix, UpdateMatrix);

      rValues.StrainMatrix = prod(UpdateMatrix, rValues.StrainMatrix);
      rValues.StrainMatrix = prod(rValues.StrainMatrix, trans(UpdateMatrix));

      this->mElasticityModel.CalculateStressTensor(rValues, StressMatrix);

      rPlasticMultiplier += DeltaGamma;
      double VolPlasticIncr = 0.0;
      for (unsigned int i = 0; i < 3; i++)
         VolPlasticIncr += DeltaGamma * PlasticPotentialDerivative(i);
      rPlasticVolDef += VolPlasticIncr;
      rPlasticVolDefAbs += fabs(VolPlasticIncr);

      double DevPlasticIncr = 0.0;
      for (unsigned int i = 0; i < 3; i++)
         DevPlasticIncr += pow(DeltaGamma * PlasticPotentialDerivative(i) - VolPlasticIncr / 3.0, 2.0);
      for (unsigned int i = 3; i < 6; i++)
         DevPlasticIncr += 2.0 * pow(DeltaGamma * PlasticPotentialDerivative(i) / 2.0, 2.0);
      DevPlasticIncr = sqrt(DevPlasticIncr);
      rPlasticDevDef += DevPlasticIncr;

      /*{ // LMV
         if ( rPlasticVolDefAbs > 2.0*fabs(rPlasticVolDef) )
            rPlasticVolDefAbs = 2.0*rPlasticVolDef;
      }*/

      rP0 = -rP0Ref * std::exp(-(rPlasticVolDef+rChis*sqrt(2.0/3.0)*rPlasticDevDef )/ (rOtherSlope - rSwellingSlope));

      rBonding = rBonding0 * exp( -(rH1*rPlasticVolDefAbs + rH2*sqrt(2.0/3.0)*rPlasticDevDef) );

      rPM = rBonding*rP0;
      rPT = rBonding*rAlphaT*rP0;

      if ( false) {
         std::cout << " HOLA " << std::endl;
         std::cout << " hardening " << H << std::endl;
         std::cout << " ps " << rPT << " . " << rPM << " . " << rP0 << std::endl;
         std::cout << " rBonding " << rBonding << std::endl;
         std::cout << " rPlasticDevDef " << rPlasticDevDef << std::endl;
      }

      KRATOS_CATCH("")
   }

   //********************************************************************
   //********************************************************************
   // UpdateInternalVariables
   virtual void UpdateInternalVariables(ModelDataType &rValues, PlasticDataType &rVariables, const MatrixType &rStressMatrix) override
   {
      KRATOS_TRY


      for (unsigned int i = 0; i < 14; i++)
      {
         if ( i == 11 || i == 12) {
            continue;
         }
         double &rCurrentPlasticVariable = rVariables.Data.Internal(i);
         double &rPreviousPlasticVariable = this->mInternal(i);

         this->mInternal(i,1) = rPreviousPlasticVariable;
         rPreviousPlasticVariable = rCurrentPlasticVariable;
      }

      const ModelDataType &rModelData = rVariables.GetModelData();
      const Properties &rMaterialProperties = rModelData.GetProperties();
      const double &rSwellingSlope = rMaterialProperties[SWELLING_SLOPE];
      const double &rAlpha = rMaterialProperties[ALPHA_SHEAR];

      double MeanStress = 0;
      for (unsigned int i = 0; i < 3; i++)
         MeanStress += rValues.StressMatrix(i, i) / 3.0;
      double K = (-MeanStress) / rSwellingSlope;
      double G = rMaterialProperties[INITIAL_SHEAR_MODULUS];
      G += rAlpha * (-MeanStress);
      mBulkModulus = K;
      mShearModulus = G;

      KRATOS_CATCH("")
   }

   //***************************************************************************************
   //***************************************************************************************
   // Correct Yield Surface Drift According to
   virtual void ReturnStressToYieldSurface(ModelDataType &rValues, PlasticDataType &rVariables) override
   {
      KRATOS_TRY

      double Tolerance = 1e-6;

      MatrixType StressMatrix;
      this->mElasticityModel.CalculateStressTensor(rValues, StressMatrix);
      double YieldSurface = this->mYieldSurface.CalculateYieldCondition(rVariables, YieldSurface);

      bool Returning = false;
      
      double &rPlasticVolDef = rVariables.Data.Internal(1);
      double &rPlasticDevDef = rVariables.Data.Internal(2);
      double &rPlasticVolDefAbs = rVariables.Data.Internal(3);

      if (fabs(YieldSurface) < Tolerance)
         Returning = true;
      //if ( rPlasticVolDefAbs > 2.0*fabs(rPlasticVolDef) )
      //   Returning = false;

      if ( Returning )
         return;

      double SignYieldSurface = 1.0;
      const ModelDataType &rModelData = rVariables.GetModelData();
      const Properties &rMaterialProperties = rModelData.GetProperties();
      const double & rBonding0 = rMaterialProperties[BONDING];
      const double & rP0Ref = rMaterialProperties[P0];

      const double & rH1 = rMaterialProperties[DEGRADATION_RATE_COMPRESSION];
      const double & rH2 = rMaterialProperties[DEGRADATION_RATE_SHEAR];
      const double & rAlphaT = rMaterialProperties[ALPHA_TENSILE];
      const double & rChis = rMaterialProperties[CHIS];
      const double & rSwellingSlope = rMaterialProperties[SWELLING_SLOPE];
      const double & rOtherSlope = rMaterialProperties[NORMAL_COMPRESSION_SLOPE];

      // evaluate constitutive matrix and plastic flow
      //double &rPlasticMultiplier = rVariables.Data.Internal(0);
      /*double &rPlasticVolDef = rVariables.Data.Internal(1);
      double &rPlasticDevDef = rVariables.Data.Internal(2);
      double &rPlasticVolDefAbs = rVariables.Data.Internal(3);*/
      double &rP0 = rVariables.Data.Internal(4);
      double &rPM = rVariables.Data.Internal(5);
      double &rPT = rVariables.Data.Internal(6);
      double &rBonding = rVariables.Data.Internal(10);

      /*std::cout << " ------------------------------------------------------------------ " << std::endl;
      std::cout << " INITIAL. Current Yield " << YieldSurface << std::endl;
      if ( YieldSurface > 0) {
         std::cout << " positive " << std::endl;
      } else {
         std::cout << " negative " << std::endl;
      }
      std::cout << " EPSI VOL DEF " << rPlasticVolDef << " incr " << rPlasticVolDef-this->mInternal(1,1) << std::endl;
      std::cout << " EPSI DEV DEF " << rPlasticDevDef << " incr " << rPlasticDevDef-this->mInternal(2,1) << std::endl;
      std::cout << "Absep VOL DEF " << rPlasticVolDefAbs << " incr " << rPlasticVolDefAbs-this->mInternal(3,1) << std::endl;
      std::cout << std::endl;*/

      for (unsigned int iter = 0; iter < 150; iter++)
      {

         Matrix ElasticMatrix(6, 6);
         noalias(ElasticMatrix) = ZeroMatrix(6, 6);
         this->mElasticityModel.CalculateConstitutiveTensor(rValues, ElasticMatrix);

         VectorType DeltaStressYieldCondition = this->mYieldSurface.CalculateDeltaStressYieldCondition(rVariables, DeltaStressYieldCondition);
         VectorType PlasticPotentialDerivative = this->mYieldSurface.CalculateDeltaPlasticPotential(rVariables, PlasticPotentialDerivative);

         MatrixType PlasticPotDerTensor;
         PlasticPotDerTensor = ConstitutiveModelUtilities::StrainVectorToTensor(PlasticPotentialDerivative, PlasticPotDerTensor);
         double H = this->mYieldSurface.GetHardeningRule().CalculateDeltaHardening(rVariables, H, PlasticPotDerTensor);

         double DeltaGamma = YieldSurface;
         DeltaGamma /= (H + MathUtils<double>::Dot(DeltaStressYieldCondition, prod(ElasticMatrix, PlasticPotentialDerivative)));

         MatrixType UpdateMatrix;
         this->ConvertHenckyVectorToCauchyGreenTensor(-DeltaGamma * PlasticPotentialDerivative / 2.0, UpdateMatrix);

         rValues.StrainMatrix = prod(UpdateMatrix, rValues.StrainMatrix);
         rValues.StrainMatrix = prod(rValues.StrainMatrix, trans(UpdateMatrix));

         this->mElasticityModel.CalculateStressTensor(rValues, StressMatrix);
      
         SignYieldSurface = 1.0;
         if ( YieldSurface < 0.0)
            SignYieldSurface = -1.0;

         double VolPlasticIncr = 0.0;
         for (unsigned int i = 0; i < 3; i++)
            VolPlasticIncr += DeltaGamma * PlasticPotentialDerivative(i);
         rPlasticVolDef += VolPlasticIncr;
         rPlasticVolDefAbs += SignYieldSurface * fabs(VolPlasticIncr);

         double DevPlasticIncr = 0.0;
         for (unsigned int i = 0; i < 3; i++)
            DevPlasticIncr += pow(DeltaGamma * PlasticPotentialDerivative(i) - VolPlasticIncr / 3.0, 2.0);
         for (unsigned int i = 3; i < 6; i++)
            DevPlasticIncr += 2.0 * pow(DeltaGamma * DeltaStressYieldCondition(i) / 2.0, 2.0);
         DevPlasticIncr = SignYieldSurface * sqrt(DevPlasticIncr);
         rPlasticDevDef += DevPlasticIncr;

      
         /*if ( rPlasticVolDefAbs > 2.0*fabs(rPlasticVolDef) ) {
            rPlasticVolDefAbs = 2.0*fabs(rPlasticVolDef);
         } else if ( rPlasticVolDefAbs < 0.0) {
            rPlasticVolDefAbs = 0.0;
         }*/

         rP0 = -rP0Ref * std::exp(-(rPlasticVolDef+rChis*sqrt(2.0/3.0)*rPlasticDevDef )/ (rOtherSlope - rSwellingSlope));

         rBonding = rBonding0 * exp( -(rH1*rPlasticVolDefAbs + rH2*sqrt(2.0/3.0)*rPlasticDevDef) );

         rPM = rBonding*rP0;
         rPT = rBonding*rAlphaT*rP0;

         YieldSurface = this->mYieldSurface.CalculateYieldCondition(rVariables, YieldSurface);

         if (false) {
            std::cout << std::endl;
            std::cout << " this Iter " << iter << " f " << YieldSurface << std::endl;
            std::cout << " gammma " << DeltaGamma << std::endl;
            std::cout << " ps " << rPT << " . " << rPM << " . " << rP0 << std::endl;
            std::cout << " rBonding " << rBonding << std::endl;
            std::cout << " rPlasticDevDef " << rPlasticDevDef << std::endl;
            std::cout << std::endl;
            std::cout << std::endl;
         
            std::cout << " ------------------------------------------------------------------ " << std::endl;
            std::cout << " iter." << iter <<" Current Yield " << YieldSurface << std::endl;
            std::cout << " Delta Gamma " << DeltaGamma << std::endl;
            std::cout << " EPSI VOL DEF " << rPlasticVolDef << " incr " << rPlasticVolDef-this->mInternal(1,1) << " incr " << VolPlasticIncr << std::endl;
            std::cout << " EPSI DEV DEF " << rPlasticDevDef << " incr " << rPlasticDevDef-this->mInternal(2,1) << " incr " << DevPlasticIncr << std::endl;
            std::cout << "Absep VOL DEF " << rPlasticVolDefAbs << " incr " << rPlasticVolDefAbs-this->mInternal(3,1) << " incr "<< DeltaGamma/fabs(DeltaGamma)*fabs(VolPlasticIncr) << std::endl;
            std::cout << std::endl;
         }

         if (fabs(YieldSurface) < Tolerance)
         {
            return;
         }
      }
      std::cout << " theStressPointDidNotReturnedCorrectly " << YieldSurface << std::endl;

      KRATOS_CATCH("")
   }
   ///@}
   ///@name Protected  Access
   ///@{

   ///@}
   ///@name Protected Inquiry
   ///@{

   ///@}
   ///@name Protected LifeCycle
   ///@{

   ///@}

private:
   ///@name Static Member Variables
   ///@{

   ///@}
   ///@name Member Variables
   ///@{

   ///@}
   ///@name Private Operators
   ///@{

   ///@}
   ///@name Private Operations
   ///@{

   ///@}
   ///@name Private  Access
   ///@{

   ///@}
   ///@name Private Inquiry
   ///@{

   ///@}
   ///@name Serialization
   ///@{
   friend class Serializer;

   virtual void save(Serializer &rSerializer) const override
   {
      KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, DerivedType)
      rSerializer.save("BulkModulus", mBulkModulus);
      rSerializer.save("ShearModulus", mShearModulus);
   }

   virtual void load(Serializer &rSerializer) override
   {
      KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, DerivedType)
      rSerializer.load("BulkModulus", mBulkModulus);
      rSerializer.load("ShearModulus", mShearModulus);
   }

   ///@}
   ///@name Un accessible methods
   ///@{

   ///@}

}; // Class StructuredCasmBaseSoilModel

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_STRUCTURED_CASM_BASE_SOIL_MODEL_HPP_INCLUDED  defined
