//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:                        $
//   Maintained by:       $Maintainer:                       $
//   Date:                $Date:                January 2020 $
//
//

#if !defined(KRATOS_MOHR_COULOMB_MODEL_HPP_INCLUDED)
#define      KRATOS_MOHR_COULOMB_MODEL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/soil_base_model.hpp"
#include "custom_models/plasticity_models/hardening_rules/mohr_coulomb_hardening_rule.hpp"
#include "custom_models/plasticity_models/yield_surfaces/mohr_coulomb_yield_surface.hpp"
#include "custom_models/elasticity_models/hencky_linear_model.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
    */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) MohrCoulombModel : public SoilBaseModel<HenckyLinearModel, MohrCoulombYieldSurface<MohrCoulombHardeningRule>>
{
public:
   ///@name Type Definitions
   ///@{

   //elasticity model
   typedef HenckyLinearModel ElasticityModelType;
   typedef ElasticityModelType::Pointer ElasticityModelPointer;

   //yield surface
   typedef MohrCoulombHardeningRule HardeningRuleType;
   typedef MohrCoulombYieldSurface<HardeningRuleType> YieldSurfaceType;
   typedef YieldSurfaceType::Pointer YieldSurfacePointer;

   //base type
   typedef SoilBaseModel<ElasticityModelType, YieldSurfaceType> BaseType;

   //common types
   typedef BaseType::Pointer BaseTypePointer;
   typedef BaseType::SizeType SizeType;
   typedef BaseType::VoigtIndexType VoigtIndexType;
   typedef BaseType::MatrixType MatrixType;
   typedef BaseType::ModelDataType ModelDataType;
   typedef BaseType::MaterialDataType MaterialDataType;
   typedef BaseType::PlasticDataType PlasticDataType;
   typedef BaseType::InternalVariablesType InternalVariablesType;

   /// Pointer definition of MohrCoulombModel
   KRATOS_CLASS_POINTER_DEFINITION(MohrCoulombModel);

   ///@}
   ///@name Life Cycle
   ///@{

   /// Default constructor.
   MohrCoulombModel() : BaseType() {}

   /// Copy constructor.
   MohrCoulombModel(MohrCoulombModel const &rOther) : BaseType(rOther) {}

   /// Assignment operator.
   MohrCoulombModel &operator=(MohrCoulombModel const &rOther)
   {
      BaseType::operator=(rOther);
      return *this;
   }

   /// Clone.
   ConstitutiveModel::Pointer Clone() const override
   {
      return Kratos::make_shared<MohrCoulombModel>(*this);
   }

   /// Destructor.
   ~MohrCoulombModel() override {}

   ///@}
   ///@name Operators
   ///@{

   ///@}
   ///@name Operations
   ///@{

   /**
          * Check
          */
   int Check(const Properties &rProperties, const ProcessInfo &rCurrentProcessInfo) override
   {
      KRATOS_TRY

      //LMV: to be implemented. but should not enter in the base one

      return 0;

      KRATOS_CATCH("")
   }

   /**
       * Get required properties
       */
   void GetRequiredProperties(Properties &rProperties) override
   {
      KRATOS_TRY

      // Set required properties to check keys
      BaseType::GetRequiredProperties(rProperties);

      KRATOS_CATCH(" ")
   }

   ///@}
   ///@name Access
   ///@{

   /**
          * Has Values
          */
   bool Has(const Variable<double> &rVariable) override
   {
      if (rVariable == PLASTIC_STRAIN || rVariable == DELTA_PLASTIC_STRAIN)
         return true;

      return false;
   }

   /**
          * Set Values
          */
   void SetValue(const Variable<double> &rVariable,
                 const double &rValue,
                 const ProcessInfo &rCurrentProcessInfo) override
   {
      KRATOS_TRY

      if (rVariable == NONLOCAL_PLASTIC_VOL_DEF)
      {
         mInternal(4) = rValue;
      }

      KRATOS_CATCH("")
   }

   /**
          * Get Values
          */
   double &GetValue(const Variable<double> &rVariable, double &rValue) override
   {

      rValue = 0;

      if (rVariable == PLASTIC_STRAIN)
      {
         rValue = this->mInternal(0);
      }
      else if (rVariable == DELTA_PLASTIC_STRAIN)
      {
         rValue = this->mInternal(0) - this->mInternal(0,1);
      }
      else if (rVariable == PRE_CONSOLIDATION_STRESS)
      {
         rValue = this->mInternal(3);
      }
      else
      {
         rValue = SoilBaseModel::GetValue(rVariable, rValue);
      }
      return rValue;
   }

   ///@}
   ///@name Inquiry
   ///@{

   ///@}
   ///@name Input and output
   ///@{

   /// Turn back information as a string.
   std::string Info() const override
   {
      std::stringstream buffer;
      buffer << "MohrCoulombModel";
      return buffer.str();
   }

   /// Print information about this object.
   void PrintInfo(std::ostream &rOStream) const override
   {
      rOStream << "MohrCoulombModel";
   }

   /// Print object's data.
   void PrintData(std::ostream &rOStream) const override
   {
      rOStream << "MohrCoulombModel Data";
   }

   ///@}
   ///@name Friends
   ///@{

   ///@}

protected:
   ///@name Protected static Member Variables
   ///@{

   ///@}
   ///@name Protected member Variables
   ///@{

   ///@}
   ///@name Protected Operators
   ///@{

   ///@}
   ///@name Protected Operations
   ///@{

   ///@}
   ///@name Protected  Access
   ///@{

   ///@}
   ///@name Protected Inquiry
   ///@{

   ///@}
   ///@name Protected LifeCycle
   ///@{

   ///@}

private:
   ///@name Static Member Variables
   ///@{

   ///@}
   ///@name Member Variables
   ///@{

   ///@}
   ///@name Private Operators
   ///@{

   ///@}
   ///@name Private Operations
   ///@{

   ///@}
   ///@name Private  Access
   ///@{

   ///@}
   ///@name Private Inquiry
   ///@{

   ///@}
   ///@name Serialization
   ///@{
   friend class Serializer;

   void save(Serializer &rSerializer) const override
   {
      KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BaseType)
   }

   void load(Serializer &rSerializer) override
   {
      KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BaseType)
   }

   ///@}
   ///@name Un accessible methods
   ///@{

   ///@}

}; // Class MohrCoulombModel

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_MOHR_COULOMB_MODEL_HPP_INCLUDED  defined
