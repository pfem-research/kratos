//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                  April 2017 $
//
//

#if !defined(KRATOS_NON_LINEAR_ASSOCIATIVE_PLASTICITY_MODEL_HPP_INCLUDED)
#define KRATOS_NON_LINEAR_ASSOCIATIVE_PLASTICITY_MODEL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/plasticity_model.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
template <class TElasticityModel, class TYieldSurface>
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) NonLinearAssociativePlasticityModel : public PlasticityModel<TElasticityModel, TYieldSurface>
{
public:
  ///@name Type Definitions
  ///@{

  //elasticity model
  typedef TElasticityModel ElasticityModelType;

  //yield surface
  typedef TYieldSurface YieldSurfaceType;

  //base type
  typedef PlasticityModel<ElasticityModelType, YieldSurfaceType> BaseType;

  //common types
  typedef typename BaseType::Pointer BaseTypePointer;
  typedef typename BaseType::SizeType SizeType;
  typedef typename BaseType::VoigtIndexType VoigtIndexType;
  typedef typename BaseType::MatrixType MatrixType;
  typedef typename BaseType::ModelDataType ModelDataType;
  typedef typename BaseType::MaterialDataType MaterialDataType;
  typedef typename BaseType::PlasticDataType PlasticDataType;
  typedef typename BaseType::InternalVariablesType InternalVariablesType;

  typedef typename BaseType::NameType NameType;

  typedef typename TElasticityModel::ElasticDataType ElasticDataType;

  typedef ConstitutiveModelData::StrainMeasureType StrainMeasureType;
  typedef ConstitutiveModelData::StressMeasureType StressMeasureType;

  enum class ThermalType : std::size_t { PLASTIC_DISSIPATION = 0, DELTA_PLASTIC_DISSIPATION = 1 };
  typedef ConstitutiveModelData::InternalData<2,NameType> ThermalDataType;
  typedef ConstitutiveModelData::InternalBuffer<ThermalDataType,2> ThermalVariablesType;
  //typedef ConstitutiveModelData::ThermalVariables ThermalVariablesType;

protected:

  struct PlasticFactors
  {
    double Beta0;
    double Beta1;
    double Beta2;
    double Beta3;
    double Beta4;

    MatrixType Normal;
    MatrixType Dev_Normal;
  };

public:
  /// Pointer definition of NonLinearAssociativePlasticityModel
  KRATOS_CLASS_POINTER_DEFINITION(NonLinearAssociativePlasticityModel);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  NonLinearAssociativePlasticityModel() : BaseType() {}

  /// Copy constructor.
  NonLinearAssociativePlasticityModel(NonLinearAssociativePlasticityModel const &rOther) : BaseType(rOther), mInternal(rOther.mInternal), mThermal(rOther.mThermal) {}

  /// Assignment operator.
  NonLinearAssociativePlasticityModel &operator=(NonLinearAssociativePlasticityModel const &rOther)
  {
    BaseType::operator=(rOther);
    mInternal = rOther.mInternal;
    mThermal = rOther.mThermal;
    return *this;
  }

  /// Clone.
  ConstitutiveModel::Pointer Clone() const override
  {
    return Kratos::make_shared<NonLinearAssociativePlasticityModel>(*this);
  }

  /// Destructor.
  ~NonLinearAssociativePlasticityModel() override {}

  ///@}
  ///@name Operators
  ///@{
  ///@}
  ///@name Operations
  ///@{

  /**
     * Calculate Stresses
     */

  void CalculateStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix) override
  {
    KRATOS_TRY

    PlasticDataType Variables;
    this->InitializeVariables(rValues, Variables);

    ElasticDataType ElasticVariables;
    this->mElasticityModel.InitializeVariables(rValues, ElasticVariables);

    // calculate elastic isochoric stress
    this->mElasticityModel.CalculateAndAddIsochoricStressTensor(ElasticVariables, rStressMatrix);

    rValues.StressMatrix = rStressMatrix; //store trial isochoric stress to ModelData StressMatrix

    // calculate plastic isochoric stress
    this->CalculateAndAddIsochoricStressTensor(Variables, rStressMatrix);

    if (rValues.State.Is(ConstitutiveModelData::UPDATE_INTERNAL_VARIABLES))
      this->UpdateInternalVariables(rValues, Variables, rStressMatrix);

    // calculate volumetric stress
    this->mElasticityModel.CalculateAndAddVolumetricStressTensor(ElasticVariables, rStressMatrix);

    KRATOS_CATCH(" ")
  }

  void CalculateIsochoricStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix) override
  {
    KRATOS_TRY

    PlasticDataType Variables;
    this->InitializeVariables(rValues, Variables);

    // calculate elastic isochoric stress
    this->mElasticityModel.CalculateIsochoricStressTensor(rValues, rStressMatrix);

    rValues.StressMatrix = rStressMatrix; //store trial isochoric stress to ModelData StressMatrix

    // calculate plastic isochoric stress
    this->CalculateAndAddIsochoricStressTensor(Variables, rStressMatrix);

    if (rValues.State.Is(ConstitutiveModelData::UPDATE_INTERNAL_VARIABLES))
      this->UpdateInternalVariables(rValues, Variables, rStressMatrix);

    KRATOS_CATCH(" ")
  }

  /**
     * Calculate Constitutive Tensor
     */
  void CalculateConstitutiveTensor(ModelDataType &rValues, Matrix &rConstitutiveMatrix) override
  {
    KRATOS_TRY

    PlasticDataType Variables;
    this->InitializeVariables(rValues, Variables);

    // calculate elastic constitutive tensor
    this->mElasticityModel.CalculateConstitutiveTensor(rValues, rConstitutiveMatrix);

    MatrixType StressMatrix = rValues.StressMatrix; //elastic isochoric stress computed here
    // calculate and Add Plastic Isochoric Stress Matrix
    this->CalculateAndAddIsochoricStressTensor(Variables, StressMatrix);

    // calculate plastic constitutive tensor
    if (Variables.State().Is(ConstitutiveModelData::PLASTIC_REGION))
      this->CalculateAndAddPlasticConstitutiveTensor(Variables, rConstitutiveMatrix);

    Variables.State().Set(ConstitutiveModelData::CONSTITUTIVE_MATRIX_COMPUTED, true);

    KRATOS_CATCH(" ")
  }

  /**
     * Calculate Stress and Constitutive Tensor
     */
  void CalculateStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix) override
  {
    KRATOS_TRY

    PlasticDataType Variables;
    this->InitializeVariables(rValues, Variables);

    ElasticDataType ElasticVariables;
    this->mElasticityModel.InitializeVariables(rValues, ElasticVariables);

    // calculate elastic isochoric stress
    this->mElasticityModel.CalculateAndAddIsochoricStressTensor(ElasticVariables, rStressMatrix);

    rValues.StressMatrix = rStressMatrix; //store trial isochoric stress to ModelData StressMatrix

    //Set constitutive matrix to zero before adding
    const SizeType &rVoigtSize = Variables.GetModelData().GetVoigtSize();
    noalias(rConstitutiveMatrix) = ZeroMatrix(rVoigtSize,rVoigtSize);

    // calculate elastic constitutive tensor
    this->mElasticityModel.CalculateAndAddConstitutiveTensor(ElasticVariables, rConstitutiveMatrix);

    // calculate plastic isochoric stress
    this->CalculateAndAddIsochoricStressTensor(Variables, rStressMatrix);

    // calculate plastic constitutive tensor
    if (Variables.State().Is(ConstitutiveModelData::PLASTIC_REGION))
      this->CalculateAndAddPlasticConstitutiveTensor(Variables, rConstitutiveMatrix);

    Variables.State().Set(ConstitutiveModelData::CONSTITUTIVE_MATRIX_COMPUTED, true);

    if (rValues.State.Is(ConstitutiveModelData::UPDATE_INTERNAL_VARIABLES))
      this->UpdateInternalVariables(rValues, Variables, rStressMatrix);

    // calculate volumetric stress
    this->mElasticityModel.CalculateAndAddVolumetricStressTensor(ElasticVariables, rStressMatrix);

    KRATOS_CATCH(" ")
  }

  void CalculateInternalVariables(ModelDataType &rValues) override
  {
    KRATOS_TRY

    BaseType::CalculateInternalVariables(rValues);

    PlasticDataType Variables;
    this->InitializeVariables(rValues, Variables);

    // calculate elastic stress
    this->mElasticityModel.CalculateIsochoricStressTensor(rValues, rValues.StressMatrix);

    // calculate damaged stress
    this->CalculateAndAddIsochoricStressTensor(Variables, rValues.StressMatrix);

    // set requested internal variables
    this->SetInternalVariables(rValues, Variables);

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{

  /**
     * Has Values
     */
  bool Has(const Variable<double> &rVariable) override
  {
    return BaseType::Has(rVariable);
  }

  bool Has(const Variable<array_1d<double,3>> &rVariable) override
  {
    return BaseType::Has(rVariable);
  }

  bool Has(const Variable<Vector> &rVariable) override
  {
    return BaseType::Has(rVariable);
  }

  bool Has(const Variable<Matrix> &rVariable) override
  {
    return BaseType::Has(rVariable);
  }

  /**
     * Set Values
     */
  void SetValue(const Variable<double> &rVariable,
                const double &rValue,
                const ProcessInfo &rCurrentProcessInfo) override
  {
    BaseType::SetValue(rVariable, rValue, rCurrentProcessInfo);
  }

  void SetValue(const Variable<array_1d<double,3>> &rVariable,
                const array_1d<double,3> &rValue,
                const ProcessInfo &rCurrentProcessInfo) override
  {
    BaseType::SetValue(rVariable, rValue, rCurrentProcessInfo);
  }

  void SetValue(const Variable<Vector> &rVariable, const Vector &rValue,
                const ProcessInfo &rCurrentProcessInfo) override
  {
    BaseType::SetValue(rVariable, rValue, rCurrentProcessInfo);
  }

  void SetValue(const Variable<Matrix> &rVariable, const Matrix &rValue,
                const ProcessInfo &rCurrentProcessInfo) override
  {
    BaseType::SetValue(rVariable, rValue, rCurrentProcessInfo);
  }


  /**
     * Get Values
     */
  double &GetValue(const Variable<double> &rVariable, double &rValue) override
  {
    rValue = BaseType::GetValue(rVariable, rValue);
    return rValue;
  }

  array_1d<double, 3> &GetValue(const Variable< array_1d<double, 3> > & rVariable,
                                array_1d<double, 3> & rValue) override
  {
    rValue = BaseType::GetValue(rVariable, rValue);
    return rValue;
  }

  Vector &GetValue(const Variable<Vector> &rVariable, Vector &rValue) override
  {
    rValue = BaseType::GetValue(rVariable, rValue);
    return rValue;
  }

  Matrix &GetValue(const Variable<Matrix> &rVariable, Matrix &rValue) override
  {
    rValue = BaseType::GetValue(rVariable, rValue);
    return rValue;
  }

  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "NonLinearAssociativePlasticityModel";
    return buffer.str();
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "NonLinearAssociativePlasticityModel";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "NonLinearAssociativePlasticityModel Data";
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{

  // internal variables:
  InternalVariablesType mInternal;

  ThermalVariablesType  mThermal;

  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{

  /**
     * Set Working Measures
     */
  virtual void SetWorkingMeasures(PlasticDataType &rVariables, MatrixType &rStressMatrix)
  {
    KRATOS_TRY

    const ModelDataType &rModelData = rVariables.GetModelData();

    //working stress is Kirchhoff by default : transform stresses is working stress is PK2
    const StressMeasureType &rStressMeasure = rModelData.GetStressMeasure();
    const StrainMeasureType &rStrainMeasure = rModelData.GetStrainMeasure();

    if (rStressMeasure == ConstitutiveModelData::StressMeasureType::StressMeasure_PK2)
    {

      const MatrixType &rTotalDeformationMatrix = rModelData.GetTotalDeformationMatrix();
      MatrixType StressMatrixPart;
      noalias(StressMatrixPart) = prod(rTotalDeformationMatrix, rStressMatrix);
      noalias(rStressMatrix) = prod(StressMatrixPart, trans(rTotalDeformationMatrix));

      if (rStrainMeasure == ConstitutiveModelData::StrainMeasureType::StrainMeasure_Right_CauchyGreen || rStrainMeasure == ConstitutiveModelData::StrainMeasureType::StrainMeasure_None)
      {
        double I3 = 0;
        ConstitutiveModelUtilities::InvertMatrix3(rModelData.GetStrainMatrix(), rVariables.Data.StrainMatrix, I3);
      }
      else
      {
        KRATOS_ERROR << "calling initialize PlasticityModel .. StrainMeasure provided is inconsistent" << std::endl;
      }
    }
    else if (rStressMeasure == ConstitutiveModelData::StressMeasureType::StressMeasure_Kirchhoff)
    {

      if (rStrainMeasure == ConstitutiveModelData::StrainMeasureType::StrainMeasure_Left_CauchyGreen || rStrainMeasure == ConstitutiveModelData::StrainMeasureType::StrainMeasure_None)
      {
        rVariables.Data.StrainMatrix = IdentityMatrix(3);
      }
      else
      {
        KRATOS_ERROR << "calling initialize PlasticityModel .. StrainMeasure provided is inconsistent" << std::endl;
      }
    }
    else
    {
      KRATOS_ERROR << "calling initialize PlasticityModel .. StressMeasure provided is inconsistent" << std::endl;
    }

    //initialize thermal variables
    mThermal(ThermalType::PLASTIC_DISSIPATION) = 0;
    mThermal(ThermalType::DELTA_PLASTIC_DISSIPATION) = 0;

    KRATOS_CATCH(" ")
  }

  /**
     * Get Working Measures
     */
  virtual void GetWorkingMeasures(PlasticDataType &rVariables, MatrixType &rStressMatrix)
  {
    KRATOS_TRY

    const ModelDataType &rModelData = rVariables.GetModelData();

    //working stress is Kirchhoff by default : transform stresses if working stress is PK2
    const StressMeasureType &rStressMeasure = rModelData.GetStressMeasure();

    if (rStressMeasure == ConstitutiveModelData::StressMeasureType::StressMeasure_PK2)
    {

      const MatrixType &rTotalDeformationMatrix = rModelData.GetTotalDeformationMatrix();
      MatrixType StressMatrixPart;

      MatrixType InverseTotalDeformationMatrix;
      noalias(InverseTotalDeformationMatrix) = ZeroMatrix(3,3);
      double TotalDeformationDet;
      ConstitutiveModelUtilities::InvertMatrix3(rTotalDeformationMatrix, InverseTotalDeformationMatrix, TotalDeformationDet);
      noalias(StressMatrixPart) = prod(InverseTotalDeformationMatrix, rStressMatrix);
      noalias(rStressMatrix) = prod(StressMatrixPart, trans(InverseTotalDeformationMatrix));
    }

    KRATOS_CATCH(" ")
  }

  /**
     * Calculate Stresses
     */
  virtual void CalculateAndAddIsochoricStressTensor(PlasticDataType &rVariables, MatrixType &rStressMatrix)
  {
    KRATOS_TRY

    //0.-Check working stress
    this->SetWorkingMeasures(rVariables, rStressMatrix);

    //1.-Isochoric stress norm   (rStressMatrix := Elastic Isochoric Stress Matrix)
    rVariables.Data.StressNorm = ConstitutiveModelUtilities::CalculateStressNorm(rStressMatrix, rVariables.Data.StressNorm);

    //2.-Check yield condition
    rVariables.Data.TrialStateFunction = this->mYieldSurface.CalculateYieldCondition(rVariables, rVariables.Data.TrialStateFunction);

    if (rVariables.State().Is(ConstitutiveModelData::IMPLEX_ACTIVE))
    {
      //3.- Calculate the implex radial return
      this->CalculateImplexReturnMapping(rVariables, rStressMatrix);
    }
    else
    {
      if (rVariables.Data.TrialStateFunction <= 0)
      {
        rVariables.State().Set(ConstitutiveModelData::PLASTIC_REGION, false);
      }
      else
      {
        //3.- Calculate the return mapping
        bool converged = this->CalculateReturnMapping(rVariables, rStressMatrix);

        //4.- Update back stress, plastic strain and stress
        this->UpdateStressConfiguration(rVariables, rStressMatrix);

        if (!converged)
          KRATOS_WARNING("NON-LINEAR PLASTICITY") << "Not converged" << std::endl;

        //5.- Calculate thermal dissipation and delta thermal dissipation
        this->CalculateThermalDissipation(rVariables);

        rVariables.State().Set(ConstitutiveModelData::PLASTIC_REGION, true);
      }
    }

    //6.- Recover working stress
    this->GetWorkingMeasures(rVariables, rStressMatrix);

    rVariables.State().Set(ConstitutiveModelData::RETURN_MAPPING_COMPUTED, true);

    KRATOS_CATCH(" ")
  }

  /**
     * Calculate Constitutive Tensor
     */
  virtual void CalculateAndAddPlasticConstitutiveTensor(PlasticDataType &rVariables, Matrix &rConstitutiveMatrix)
  {
    KRATOS_TRY

    //Compute radial return
    if (rVariables.State().IsNot(ConstitutiveModelData::RETURN_MAPPING_COMPUTED))
      KRATOS_ERROR << "ReturnMapping has to be computed to perform the calculation" << std::endl;

    //Algorithmic moduli factors
    PlasticFactors Factors;
    this->CalculateScalingFactors(rVariables, Factors);

    //Calculate HyperElastic ConstitutiveMatrix
    const ModelDataType &rModelData = rVariables.GetModelData();
    const SizeType &rVoigtSize = rModelData.GetVoigtSize();
    const VoigtIndexType &rIndexVoigtTensor = rModelData.GetVoigtIndexTensor();

    for (SizeType i = 0; i < rVoigtSize; i++)
    {
      for (SizeType j = 0; j < rVoigtSize; j++)
      {

        rConstitutiveMatrix(i, j) = this->AddPlasticConstitutiveComponent(rVariables, Factors, rConstitutiveMatrix(i, j),
                                                                          rIndexVoigtTensor[i][0], rIndexVoigtTensor[i][1],
                                                                          rIndexVoigtTensor[j][0], rIndexVoigtTensor[j][1]);
      }
    }

    rVariables.State().Set(ConstitutiveModelData::CONSTITUTIVE_MATRIX_COMPUTED, true);

    KRATOS_CATCH(" ")
  }

  /**
     * Calculate Constitutive Components
     */
  virtual double &AddPlasticConstitutiveComponent(PlasticDataType &rVariables,
                                                  PlasticFactors &rFactors, double &rCabcd,
                                                  const unsigned int &a, const unsigned int &b,
                                                  const unsigned int &c, const unsigned int &d)
  {
    KRATOS_TRY

    const ModelDataType &rModelData = rVariables.GetModelData();
    const MaterialDataType &rMaterial = rVariables.GetMaterialParameters();

    const MatrixType &rIsochoricStressMatrix = rModelData.GetStressMatrix(); //isochoric stress stored as StressMatrix
    const MatrixType &rStrainMatrix = rVariables.Data.GetStrainMatrix();          // C^-1 or Id

    //TODO: this constitutive part must be revised, depending on the working strain/stress measure C^-1 or I must be supplied

    double Cabcd = 0;

    Cabcd = (1.0 / 3.0) * (rStrainMatrix(a, b) * rStrainMatrix(c, d));

    Cabcd -= (0.5 * (rStrainMatrix(a, c) * rStrainMatrix(b, d) + rStrainMatrix(a, d) * rStrainMatrix(b, c)));

    Cabcd *= 3.0 * rMaterial.GetLameMuBar();

    Cabcd += (rStrainMatrix(c, d) * rIsochoricStressMatrix(a, b) + rIsochoricStressMatrix(c, d) * rStrainMatrix(a, b));

    Cabcd *= (-2.0 / 3.0) * ((-1) * rFactors.Beta1);

    Cabcd -= rFactors.Beta3 * 2.0 * rMaterial.GetLameMuBar() * (rFactors.Normal(a, b) * rFactors.Normal(c, d));

    Cabcd -= rFactors.Beta4 * 2.0 * rMaterial.GetLameMuBar() * (rFactors.Normal(a, b) * rFactors.Dev_Normal(c, d));

    rCabcd += Cabcd;

    return rCabcd;

    KRATOS_CATCH(" ")
  }

  // calculate return mapping

  virtual bool CalculateReturnMapping(PlasticDataType &rVariables, MatrixType &rStressMatrix)
  {
    KRATOS_TRY

    //Set convergence parameters
    unsigned int iter = 0;
    double Tolerance = 1e-5;
    double MaxIterations = 20;

    //start
    double DeltaDeltaGamma = 0;
    double DeltaStateFunction = 0;
    double DeltaPlasticStrain = 0;

    const double &rPreviousPlasticStrain = mInternal(NameType::PLASTIC_STRAIN);
    double &rCurrentPlasticStrain = rVariables.Data.Internal[NameType::PLASTIC_STRAIN];
    double &rDeltaGamma = rVariables.Data.DeltaInternal[NameType::PLASTIC_STRAIN];

    double &StateFunction = rVariables.Data.TrialStateFunction;

    // rCurrentPlasticStrain = 0;
    rDeltaGamma = 0;

    // constant zero increment
    bool zero_increment = false;

    double alpha = 1;
    while (fabs(StateFunction) >= Tolerance && iter <= MaxIterations && !zero_increment)
    {
      //std::cout<<" iter: "<<iter<<" StateFunction "<<StateFunction<<std::endl;

      //Calculate Delta State Function:
      DeltaStateFunction = this->mYieldSurface.CalculateDeltaStateFunction(rVariables, DeltaStateFunction);

      //Calculate DeltaGamma:
      DeltaDeltaGamma = StateFunction / DeltaStateFunction;
      rDeltaGamma += DeltaDeltaGamma;

      //Update Equivalent Plastic Strain:
      DeltaPlasticStrain = sqrt(2.0 / 3.0) * rDeltaGamma;

      //Linesearch to improve convergence (improvement not clear)
      //alpha = CalculateLineSearch( rVariables, alpha );

      rCurrentPlasticStrain = rPreviousPlasticStrain + alpha * DeltaPlasticStrain;

      //Calculate State Function:
      StateFunction = this->mYieldSurface.CalculateStateFunction(rVariables, StateFunction);

      if(fabs(DeltaDeltaGamma)<1e-16 && iter>3)
        zero_increment = true;
      //std::cout<<" End StateFunction "<<StateFunction<<" Ep "<<rCurrentPlasticStrain<<" Ep_old "<<rPreviousPlasticStrain<<" d_Ep "<<rDeltaGamma<<" dd_Ep "<<DeltaDeltaGamma<<" d_StateFunction "<<DeltaStateFunction<<std::endl;

      iter++;
    }

    if (iter > MaxIterations)
    {
      std::cout << " ConstitutiveLaw did not converge [Stress: " << rStressMatrix << " DeltaGamma: " << rDeltaGamma << " StateFunction: " << StateFunction << " DeltaDeltaGamma: " << DeltaDeltaGamma << "]" << std::endl;
      if (fabs(StateFunction)>1e-3){ // correction : get previous state
        rCurrentPlasticStrain = rPreviousPlasticStrain;
        rDeltaGamma = 0;
      }
      return false;
    }

    return true;

    KRATOS_CATCH(" ")
  }

  // implex protected methods
  virtual void CalculateImplexReturnMapping(PlasticDataType &rVariables, MatrixType &rStressMatrix)
  {
    KRATOS_TRY

    double &rPreviousPlasticStrain = mInternal(NameType::PLASTIC_STRAIN,1);
    double &rCurrentPlasticStrain = rVariables.Data.Internal[NameType::PLASTIC_STRAIN];
    double &rDeltaGamma = rVariables.Data.DeltaInternal[NameType::PLASTIC_STRAIN];

    //1.-Computation of the plastic Multiplier
    rDeltaGamma = sqrt(3.0 * 0.5) * (rCurrentPlasticStrain - rPreviousPlasticStrain);

    //2.- Update back stress, plastic strain and stress
    this->UpdateStressConfiguration(rVariables, rStressMatrix);

    //3.- Calculate thermal dissipation and delta thermal dissipation
    if (rDeltaGamma > 0)
    {
      this->CalculateImplexThermalDissipation(rVariables);
      rVariables.State().Set(ConstitutiveModelData::PLASTIC_REGION, true);
    }
    else
    {
      //set thermal variables
      mThermal(ThermalType::PLASTIC_DISSIPATION) = 0;
      mThermal(ThermalType::DELTA_PLASTIC_DISSIPATION) = 0;
    }

    KRATOS_CATCH(" ")
  }

  // auxiliar methods

  void InitializeVariables(ModelDataType &rValues, PlasticDataType &rVariables) override
  {
    KRATOS_TRY

    //set model data pointer
    rVariables.SetModelData(rValues);

    rValues.State.Set(ConstitutiveModelData::PLASTIC_REGION, false);

    rValues.State.Set(ConstitutiveModelData::IMPLEX_ACTIVE, rValues.GetProcessInfo()[IMPLEX]);

    rVariables.SetState(rValues.State);

    // RateFactor
    rVariables.Data.RateFactor = 0;

    // CurrentPlasticStrain
    rVariables.Data.Internal = mInternal.Current();

    // DeltaGamma / DeltaPlasticStrain (associative plasticity)
    rVariables.Data.DeltaInternal.clear();

    // Flow Rule local variables
    rVariables.Data.TrialStateFunction = 0;
    rVariables.Data.StressNorm = 0;

    KRATOS_CATCH(" ")
  }

  virtual void UpdateStressConfiguration(PlasticDataType &rVariables, MatrixType &rStressMatrix)
  {
    KRATOS_TRY

    //Plastic Strain and Back Stress update
    if (rVariables.Data.StressNorm > 0)
    {
      double &DeltaGamma = rVariables.Data.DeltaInternal[NameType::PLASTIC_STRAIN];
      const MaterialDataType &rMaterial = rVariables.GetMaterialParameters();

      //Stress Update:
      rStressMatrix -= (rStressMatrix * ((2.0 * rMaterial.GetLameMuBar() * DeltaGamma) / rVariables.Data.StressNorm));
    }

    KRATOS_CATCH(" ")
  }

  virtual void UpdateInternalVariables(ModelDataType &rValues, PlasticDataType &rVariables, const MatrixType &rStressMatrix)
  {
    KRATOS_TRY

    mThermal.Update();

    mInternal.Update(rVariables.Data.Internal);

    const MaterialDataType &rMaterial = rVariables.GetMaterialParameters();

    //update total strain measure
    double VolumetricPart = (rValues.StrainMatrix(0, 0) + rValues.StrainMatrix(1, 1) + rValues.StrainMatrix(2, 2)) / (3.0);

    rValues.StrainMatrix = rStressMatrix;
    rValues.StrainMatrix *= ((1.0 / rMaterial.GetLameMu()) * pow(rValues.GetTotalDeformationDet(), (2.0 / 3.0)));

    rValues.StrainMatrix(0, 0) += VolumetricPart;
    rValues.StrainMatrix(1, 1) += VolumetricPart;
    rValues.StrainMatrix(2, 2) += VolumetricPart;

    KRATOS_CATCH(" ")
  }

  virtual void CalculateScalingFactors(PlasticDataType &rVariables, PlasticFactors &rFactors)
  {
    KRATOS_TRY

    //0.-Get needed parameters
    const ModelDataType &rModelData = rVariables.GetModelData();
    const MaterialDataType &rMaterial = rVariables.GetMaterialParameters();

    const double &rDeltaGamma = rVariables.Data.DeltaInternal[NameType::PLASTIC_STRAIN];
    const MatrixType &rIsochoricStressMatrix = rModelData.GetStressMatrix(); //isochoric stress stored as StressMatrix

    //1.-Identity build
    MatrixType Identity = IdentityMatrix(3);

    //2.-Auxiliar matrices
    rFactors.Normal = rIsochoricStressMatrix * (1.0 / rVariables.Data.StressNorm);

    MatrixType Norm_Normal = ZeroMatrix(3);
    noalias(Norm_Normal) = prod(rFactors.Normal, trans(rFactors.Normal));

    double Trace_Norm_Normal = Norm_Normal(0, 0) + Norm_Normal(1, 1) + Norm_Normal(2, 2);

    rFactors.Dev_Normal = Norm_Normal;

    rFactors.Dev_Normal -= (1.0 / 3.0) * Trace_Norm_Normal * Identity;

    //3.-Auxiliar constants
    if (rVariables.State().Is(ConstitutiveModelData::IMPLEX_ACTIVE))
    {

      rFactors.Beta0 = 0;

      rFactors.Beta1 = 2.0 * rMaterial.GetLameMuBar() * rDeltaGamma / rVariables.Data.StressNorm;

      rFactors.Beta2 = (2.0 / 3.0) * rVariables.Data.StressNorm * rDeltaGamma / (rMaterial.GetLameMuBar());

      rFactors.Beta3 = (-rFactors.Beta1 + rFactors.Beta2);

      rFactors.Beta4 = (-rFactors.Beta1) * rVariables.Data.StressNorm / (rMaterial.GetLameMuBar());
    }
    else
    {

      if (rVariables.State().Is(ConstitutiveModelData::PLASTIC_RATE_REGION))
        rVariables.Data.RateFactor = 1;
      else if (rVariables.State().IsNot(ConstitutiveModelData::PLASTIC_RATE_REGION))
        rVariables.Data.RateFactor = 0;

      double DeltaHardening = 0;
      DeltaHardening = this->mYieldSurface.GetHardeningRule().CalculateDeltaHardening(rVariables, DeltaHardening);

      rFactors.Beta0 = 1.0 + DeltaHardening / (3.0 * rMaterial.GetLameMuBar());

      rFactors.Beta1 = 2.0 * rMaterial.GetLameMuBar() * rDeltaGamma / rVariables.Data.StressNorm;

      rFactors.Beta2 = ((1.0 - (1.0 / rFactors.Beta0)) * (2.0 / 3.0) * rVariables.Data.StressNorm * rDeltaGamma) / (rMaterial.GetLameMuBar());

      rFactors.Beta3 = ((1.0 / rFactors.Beta0) - rFactors.Beta1 + rFactors.Beta2);

      rFactors.Beta4 = ((1.0 / rFactors.Beta0) - rFactors.Beta1) * rVariables.Data.StressNorm / (rMaterial.GetLameMuBar());
    }

    // std::cout<<"FACTORS:: Beta0 "<<rFactors.Beta0<<std::endl;
    // std::cout<<" Beta1 "<<rFactors.Beta1<<std::endl;
    // std::cout<<" Beta2 "<<rFactors.Beta2<<std::endl;
    // std::cout<<" Beta3 "<<rFactors.Beta3<<std::endl;
    // std::cout<<" Beta4 "<<rFactors.Beta4<<std::endl;

    // std::cout<<" Normal "<<rFactors.Normal<<std::endl;
    // std::cout<<" Dev_Normal "<<rFactors.Dev_Normal<<std::endl;

    KRATOS_CATCH(" ")
  }

  // energy calculation methods
  void CalculateThermalDissipation(PlasticDataType &rVariables)
  {
    KRATOS_TRY

    //1.- Thermal Dissipation:
    double &PlasticDissipation =  mThermal(ThermalType::PLASTIC_DISSIPATION);
    PlasticDissipation = this->mYieldSurface.CalculatePlasticDissipation(rVariables, PlasticDissipation);

    //2.- Thermal Dissipation Increment:
    double &DeltaPlasticDissipation = mThermal(ThermalType::DELTA_PLASTIC_DISSIPATION);
    DeltaPlasticDissipation = this->mYieldSurface.CalculateDeltaPlasticDissipation(rVariables, DeltaPlasticDissipation);

    // if (PlasticDissipation- mThermal(ThermalType::PLASTIC_DISSIPATION,1) != DeltaPlasticDissipation)
    //   std::cout<<" DeltaPS "<<DeltaPlasticDissipation<<" IncrPS "<<PlasticDissipation- mThermal(ThermalType::PLASTIC_DISSIPATION,1)<<" PS "<<PlasticDissipation<<std::endl;

    KRATOS_CATCH(" ")
  }

  // implex protected methods
  void CalculateImplexThermalDissipation(PlasticDataType &rVariables)
  {
    KRATOS_TRY

    //1.- Thermal Dissipation:
    double &PlasticDissipation =  mThermal(ThermalType::PLASTIC_DISSIPATION);
    PlasticDissipation = this->mYieldSurface.CalculateImplexPlasticDissipation(rVariables, PlasticDissipation);

    //2.- Thermal Dissipation Increment:
    double &DeltaPlasticDissipation = mThermal(ThermalType::DELTA_PLASTIC_DISSIPATION);
    DeltaPlasticDissipation = this->mYieldSurface.CalculateImplexDeltaPlasticDissipation(rVariables, DeltaPlasticDissipation);

    KRATOS_CATCH(" ")
  }

  // set requested internal variables
  void SetInternalVariables(ModelDataType &rValues, PlasticDataType &rVariables) override
  {
    KRATOS_TRY

    //supply internal variable
    rValues.InternalVariable.SetValue(PLASTIC_STRAIN, rVariables.Data.Internal[NameType::PLASTIC_STRAIN]);

    rValues.InternalVariable.SetValue(PLASTIC_DISSIPATION, mThermal(ThermalType::PLASTIC_DISSIPATION));

    rValues.InternalVariable.SetValue(DELTA_PLASTIC_DISSIPATION, mThermal(ThermalType::DELTA_PLASTIC_DISSIPATION));

    KRATOS_CATCH(" ")
  }

  // calculate line search
  double &CalculateLineSearch(PlasticDataType &rVariables, double &rAlpha)
  {
    KRATOS_TRY

    //Set convergence parameters
    unsigned int iter = 0;
    double MaxIterations = 10;

    //start preserve initial variables
    const double &rPreviousPlasticStrain = mInternal(NameType::PLASTIC_STRAIN);
    double PreviousDeltaGamma = rVariables.Data.DeltaInternal[NameType::PLASTIC_STRAIN];
    double PreviousPlasticStrain = rVariables.Data.Internal[NameType::PLASTIC_STRAIN];

    double &rDeltaGamma = rVariables.Data.DeltaInternal[NameType::PLASTIC_STRAIN];
    double &rCurrentPlasticStrain = rVariables.Data.Internal[NameType::PLASTIC_STRAIN];

    double StateFunction = this->mYieldSurface.CalculateStateFunction(rVariables, StateFunction);
    double s0 = rDeltaGamma * StateFunction;

    double R0 = StateFunction;

    rCurrentPlasticStrain = rPreviousPlasticStrain + sqrt(2.0 / 3.0) * rDeltaGamma;
    StateFunction = this->mYieldSurface.CalculateStateFunction(rVariables, StateFunction);

    double s1 = rDeltaGamma * StateFunction;

    //std::cout<<" s0 "<<s0<<" s1 "<<s1<<std::endl;

    rAlpha = 1.0;

    if (s0 * s1 < 0)
      if(R0 < StateFunction)
      {
        double s2 = s1;

        if (fabs(s1) < fabs(s0))
          s2 = s0;
        double s0_0 = s0;

        double nabla = 0;
        double delta = 1;

        while (fabs(s2 / s0_0) > 0.3 && iter < MaxIterations && (s1 * s0) < 0 && fabs(s1) > 1e-7 && fabs(s0) > 1e-7)
        {
          rAlpha = 0.5 * (nabla + delta);

          rCurrentPlasticStrain = rPreviousPlasticStrain + rAlpha * sqrt(2.0 / 3.0) * rDeltaGamma;

          StateFunction = this->mYieldSurface.CalculateStateFunction(rVariables, StateFunction);

          s2 = rDeltaGamma * StateFunction;

          if (s2 * s1 < 0)
          {
            nabla = rAlpha;
            s0 = s2;
          }
          else if (s2 * s0 < 0)
          {
            delta = rAlpha;
            s1 = s2;
          }
          else
          {
            break;
          }
          iter++;
        }
      }

    rDeltaGamma = PreviousDeltaGamma;
    rCurrentPlasticStrain = PreviousPlasticStrain;

    if (rAlpha != 1)
      std::cout << " [ LINE SEARCH: (Iterations: " << iter << ", rAlpha: " << rAlpha << ") ] " << std::endl;

    if (rAlpha > 1)
      rAlpha = 1;

    if (rAlpha <= 0)
      rAlpha = 0.001;

    return rAlpha;

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Protected  Access
  ///@{

  using ConstitutiveModel::InitializeVariables;

  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{

  using ConstitutiveModel::CalculateAndAddIsochoricStressTensor;

  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BaseType)
    rSerializer.save("InternalVariables", mInternal);
    rSerializer.save("ThermalVariables", mThermal);
  }

  void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BaseType)
    rSerializer.load("InternalVariables", mInternal);
    rSerializer.load("ThermalVariables", mThermal);
  }

  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{
  ///@}

}; // Class NonLinearAssociativePlasticityModel

///@}

///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
///@}
///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_NON_LINEAR_ASSOCIATIVE_PLASTICITY_MODEL_HPP_INCLUDED  defined
