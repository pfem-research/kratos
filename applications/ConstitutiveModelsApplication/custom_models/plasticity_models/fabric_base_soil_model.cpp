#include "custom_models/plasticity_models/fabric_base_soil_model.hpp"
#include "custom_models/plasticity_models/hardening_rules/sclay1_hardening_rule.hpp"
#include "custom_models/plasticity_models/yield_surfaces/sclay1_yield_surface.hpp"
#include "custom_models/elasticity_models/borja_model.hpp"
#include "custom_models/elasticity_models/dv2_borja_model.hpp"
#include "custom_utilities/stress_invariants_utilities.hpp"

namespace Kratos
{



   template<typename TElasticityModel, typename TYieldSurface>
   void FabricBaseSoilModel<TElasticityModel, TYieldSurface>::PolarDecomposition( const MatrixType & rF, MatrixType & rR, MatrixType & rU)
   {
      KRATOS_TRY

      MatrixType C = prod( trans(rF), rF);

      MatrixType EigenVectors;
      MatrixType EigenValues;
      MatrixType InvEigenValues;
      EigenVectors.clear();
      EigenValues.clear();
      InvEigenValues.clear();

      MathUtils<double>::GaussSeidelEigenSystem<MatrixType, MatrixType>(C, EigenVectors, EigenValues, 1.0E-15, 200);
      for (unsigned int i = 0; i < 3; i++) {
         EigenValues(i,i) = std::sqrt(EigenValues(i,i));
         InvEigenValues(i,i) = 1.0/EigenValues(i,i);
      }

      noalias( rU ) = prod(EigenVectors, MatrixType(prod(EigenValues, trans(EigenVectors))));

      double detU;
      MathUtils<double>::InvertMatrix(rU, rR, detU);
      rR = prod(rF, rR);
      //noalias( rR ) = prod(EigenVectors, MatrixType(prod(InvEigenValues, trans(EigenVectors))));
      //noalias( rR) = prod( rF, rR);


      KRATOS_CATCH("")
   }


   //***************************************************************************************
   //***************************************************************************************
   // Compute Elasto Plastic Matrix
   template<typename TElasticityModel, typename TYieldSurface>
   void FabricBaseSoilModel<TElasticityModel, TYieldSurface>::ComputeElastoPlasticTangentMatrix(ModelDataType &rValues, PlasticDataType &rVariables, Matrix &rEPMatrix)
   {
      KRATOS_TRY

      // evaluate constitutive matrix and plastic flow

      VectorType DeltaStressYieldCondition = this->mYieldSurface.CalculateDeltaStressYieldCondition(rVariables, DeltaStressYieldCondition);
      VectorType PlasticPotentialDerivative;
      PlasticPotentialDerivative = DeltaStressYieldCondition; // LMV

      MatrixType PlasticPotDerTensor;
      PlasticPotDerTensor = ConstitutiveModelUtilities::StrainVectorToTensor(PlasticPotentialDerivative, PlasticPotDerTensor);
      double H = this->mYieldSurface.GetHardeningRule().CalculateDeltaHardening(rVariables, H, PlasticPotDerTensor);

      VectorType AuxF = prod(trans(DeltaStressYieldCondition), rEPMatrix);
      VectorType AuxG = prod(rEPMatrix, PlasticPotentialDerivative);

      Matrix PlasticUpdateMatrix(6, 6);
      noalias(PlasticUpdateMatrix) = ZeroMatrix(6, 6);
      double denom = 0;
      for (unsigned int i = 0; i < 6; i++)
      {
         denom += AuxF(i) * PlasticPotentialDerivative(i);
         for (unsigned int j = 0; j < 6; j++)
         {
            PlasticUpdateMatrix(i, j) = AuxG(i) * AuxF(j);
         }
      }


      rEPMatrix -= PlasticUpdateMatrix / (H + denom);


      KRATOS_CATCH("")
   }

   //***********************************************************************************
   //***********************************************************************************
   // Compute one step of the elasto-plastic problem
   template<typename TElasticityModel, typename TYieldSurface>
   void FabricBaseSoilModel<TElasticityModel, TYieldSurface>::ComputeOneStepElastoPlasticProblem(ModelDataType &rValues, PlasticDataType &rVariables, const MatrixType &rDeltaDeformationMatrix)
   {
      KRATOS_TRY

      const ModelDataType &rModelData = rVariables.GetModelData();
      const Properties &rMaterialProperties = rModelData.GetProperties();
      const double &rInitialPrecon = rMaterialProperties[P0];
      const double &rLambda = rMaterialProperties[NORMAL_COMPRESSION_SLOPE];
      const double &rKappa = rMaterialProperties[SWELLING_SLOPE];
      const double &rCalfa = rMaterialProperties[FABRIC_RATE];
      const double &rChi = rMaterialProperties[FABRIC_RATE_DEVIATORIC];

      MatrixType StressMatrix;
      // evaluate constitutive matrix and plastic flow
      double &rPlasticVolDef = rVariables.Data.Internal(1);
      double &rPlasticMultiplier = rVariables.Data.Internal(0);
      double &rPlasticDevDef = rVariables.Data.Internal(2);
      double &rP0 = rVariables.Data.Internal(3);


      VectorType FabricVector;
      for (unsigned int i = 0; i < 6; i++)
         FabricVector(i) = rVariables.Data.Internal(10+i);


      MatrixType FabricTensor = ZeroMatrix(3,3);
      FabricTensor = ConstitutiveModelUtilities::StressVectorToTensor( FabricVector, FabricTensor);

      Matrix ElasticMatrix(6, 6);
      noalias(ElasticMatrix) = ZeroMatrix(6, 6);
      this->mElasticityModel.CalculateConstitutiveTensor(rValues, ElasticMatrix);

      VectorType DeltaStressYieldCondition = this->mYieldSurface.CalculateDeltaStressYieldCondition(rVariables, DeltaStressYieldCondition);
      VectorType PlasticPotentialDerivative;
      PlasticPotentialDerivative = DeltaStressYieldCondition; // LMV

      MatrixType PlasticPotDerTensor;
      PlasticPotDerTensor = ConstitutiveModelUtilities::StrainVectorToTensor(PlasticPotentialDerivative, PlasticPotDerTensor);
      double H = this->mYieldSurface.GetHardeningRule().CalculateDeltaHardening(rVariables, H, PlasticPotDerTensor);

      MatrixType StrainMatrix = prod(rDeltaDeformationMatrix, trans(rDeltaDeformationMatrix));
      VectorType StrainVector;
      this->ConvertCauchyGreenTensorToHenckyVector(StrainMatrix, StrainVector);

      VectorType AuxVector;
      AuxVector = prod(ElasticMatrix, StrainVector);
      double DeltaGamma;
      DeltaGamma = MathUtils<double>::Dot(AuxVector, DeltaStressYieldCondition);

      double Denominador = H + MathUtils<double>::Dot(DeltaStressYieldCondition, prod(ElasticMatrix, PlasticPotentialDerivative));

      DeltaGamma /= Denominador;

      if (DeltaGamma < 0)
         DeltaGamma = 0;

      MatrixType UpdateMatrix;
      this->ConvertHenckyVectorToCauchyGreenTensor(-DeltaGamma * PlasticPotentialDerivative / 2.0, UpdateMatrix);
      UpdateMatrix = prod(rDeltaDeformationMatrix, UpdateMatrix);

      this->mElasticityModel.CalculateStressTensor(rValues, StressMatrix);

      rValues.StrainMatrix = prod(UpdateMatrix, rValues.StrainMatrix);
      rValues.StrainMatrix = prod(rValues.StrainMatrix, trans(UpdateMatrix));




      rPlasticMultiplier += DeltaGamma;
      double VolPlasticIncr = 0.0;
      for (unsigned int i = 0; i < 3; i++)
         VolPlasticIncr += DeltaGamma * DeltaStressYieldCondition(i);
      rPlasticVolDef += VolPlasticIncr;

      double DevPlasticIncr = 0.0;
      for (unsigned int i = 0; i < 3; i++)
         DevPlasticIncr += pow(DeltaGamma * DeltaStressYieldCondition(i) - VolPlasticIncr / 3.0, 2);
      for (unsigned int i = 3; i < 6; i++)
         DevPlasticIncr += 2.0 * pow(DeltaGamma * DeltaStressYieldCondition(i) / 2.0, 2.0);
      DevPlasticIncr = sqrt(DevPlasticIncr);
      rPlasticDevDef += DevPlasticIncr;



      // Update the fabric !
      double MeanStress(0.0);
      MatrixType StressDev = StressMatrix;
      for (unsigned int i = 0; i < 3; i++)
         MeanStress += StressMatrix(i,i)/3.0;
      for (unsigned int i = 0; i < 3; i++)
         StressDev(i,i) -= MeanStress;

      StressDev = StressDev - MeanStress*FabricTensor;


      double VolPlasticIncrAbs = std::max(-VolPlasticIncr, 0.0);
      FabricTensor += rCalfa*( (3.0/4.0*StressDev/MeanStress - FabricTensor) * VolPlasticIncrAbs + rChi*(1.0/3.0*StressDev/MeanStress-FabricTensor) * DevPlasticIncr );
      //FabricTensor = prod( rDeltaDeformationMatrix, MatrixType(prod( FabricTensor, trans(rDeltaDeformationMatrix) ) ) );


      /*MatrixType R, U;
      this->PolarDecomposition(rDeltaDeformationMatrix, R, U);*/

      //std::cout << " POLAR DECOMPOSITION " << rDeltaDeformationMatrix << " R : " << R << " U : " << U << std::endl;
      //std::cout << " FABRIC TENSOR " << FabricTensor << std::endl;
      //FabricTensor = prod( R, MatrixType( prod(FabricTensor, trans(R))));
      //std::cout << " FABRIC TENSOR rotated " << FabricTensor <<  std::endl;

      //ConstitutiveModelUtilities::StressTensorToVector( FabricTensor, FabricVector);
      Vector FabricVector2 = ZeroVector(6);
      ConstitutiveModelUtilities::StressTensorToVector( FabricTensor, FabricVector2);

      for (unsigned int i = 0; i < 6; i++)
         rVariables.Data.Internal(10+i) = FabricVector2(i);

      rP0 = -rInitialPrecon * std::exp(-rPlasticVolDef / (rLambda - rKappa));

      this->mElasticityModel.CalculateStressTensor(rValues, StressMatrix);

      KRATOS_CATCH("")
   }

   //********************************************************************
   //********************************************************************
   // UpdateInternalVariables
   template<typename TElasticityModel, typename TYieldSurface>
   void FabricBaseSoilModel<TElasticityModel, TYieldSurface>::UpdateInternalVariables(ModelDataType &rValues, PlasticDataType &rVariables, const MatrixType &rStressMatrix)
   {
      KRATOS_TRY


      for (unsigned int i = 0; i < 10; i++)
      {
         double &rCurrentPlasticVariable = rVariables.Data.Internal(i);
         double &rPreviousPlasticVariable = this->mInternal(i);

         this->mInternal(i,1) = rPreviousPlasticVariable;
         rPreviousPlasticVariable = rCurrentPlasticVariable;

      }

      for (unsigned int i = 10; i < 16; i++) {
         double &rCurrentPlasticVariable = rVariables.Data.Internal(i);
         double &rPreviousPlasticVariable = this->mInternal(i);

         this->mInternal(i,1) = rPreviousPlasticVariable;
         rPreviousPlasticVariable = rCurrentPlasticVariable;
      }


      // temporal solution. Store the Constrianed modulus as variable 9 and compute it here
      const ModelDataType &rModelData = rVariables.GetModelData();
      const Properties &rMaterialProperties = rModelData.GetProperties();
      const double &rSwellingSlope = rMaterialProperties[SWELLING_SLOPE];
      const double &rAlpha = rMaterialProperties[ALPHA_SHEAR];
      double MeanStress = 0;

      for (unsigned int i = 0; i < 3; i++)
         MeanStress += rValues.StressMatrix(i, i) / 3.0;
      double K = (-MeanStress) / rSwellingSlope;
      double G = rMaterialProperties[INITIAL_SHEAR_MODULUS];
      G += rAlpha * (-MeanStress);
      mBulkModulus = K;
      mShearModulus = G;



      KRATOS_CATCH("")
   }

   //***************************************************************************************
   //***************************************************************************************
   // Correct Yield Surface Drift According to
   template<typename TElasticityModel, typename TYieldSurface>
   void FabricBaseSoilModel<TElasticityModel, TYieldSurface>::ReturnStressToYieldSurface(ModelDataType &rValues, PlasticDataType &rVariables)
   {
      KRATOS_TRY


      double Tolerance = 1e-6;

      MatrixType StressMatrix;
      this->mElasticityModel.CalculateStressTensor(rValues, StressMatrix);
      double YieldSurface = this->mYieldSurface.CalculateYieldCondition(rVariables, YieldSurface);

      if (fabs(YieldSurface) < Tolerance)
         return;

      const ModelDataType &rModelData = rVariables.GetModelData();
      const Properties &rMaterialProperties = rModelData.GetProperties();
      const double &rInitialPrecon = rMaterialProperties[P0];
      const double &rLambda = rMaterialProperties[NORMAL_COMPRESSION_SLOPE];
      const double &rKappa = rMaterialProperties[SWELLING_SLOPE];
      const double &rCalfa = rMaterialProperties[FABRIC_RATE];
      const double &rChi = rMaterialProperties[FABRIC_RATE_DEVIATORIC];


      // evaluate constitutive matrix and plastic flow
      double &rPlasticVolDef = rVariables.Data.Internal(1);
      double &rPlasticDevDef = rVariables.Data.Internal(2);
      double &rP0 = rVariables.Data.Internal(3);

      VectorType FabricVector;
      for (unsigned int i = 0; i < 6; i++)
         FabricVector(i) = rVariables.Data.Internal(10+i);


      MatrixType FabricTensor = ZeroMatrix(3,3);
      FabricTensor = ConstitutiveModelUtilities::StressVectorToTensor( FabricVector, FabricTensor);


      for (unsigned int i = 0; i < 150; i++)
      {

         /*std::cout << " ---------------------------------------- " << std::endl;
         std::cout << " ITERACIO " << i << " YIELD " << YieldSurface << std::endl;
         std::cout << " STRESS MATRIX " << StressMatrix << std::endl;*/

         Matrix ElasticMatrix(6, 6);
         noalias(ElasticMatrix) = ZeroMatrix(6, 6);
         this->mElasticityModel.CalculateConstitutiveTensor(rValues, ElasticMatrix);

         VectorType DeltaStressYieldCondition = this->mYieldSurface.CalculateDeltaStressYieldCondition(rVariables, DeltaStressYieldCondition);
         VectorType PlasticPotentialDerivative;
         PlasticPotentialDerivative = DeltaStressYieldCondition; // LMV

         MatrixType PlasticPotDerTensor;
         PlasticPotDerTensor = ConstitutiveModelUtilities::StrainVectorToTensor(PlasticPotentialDerivative, PlasticPotDerTensor);
         double H = this->mYieldSurface.GetHardeningRule().CalculateDeltaHardening(rVariables, H, PlasticPotDerTensor);

         //std::cout << " hardening modulus " << H << std::endl;
         //std::cout << " plastic potential " << PlasticPotentialDerivative << std::endl;

         double DeltaGamma = YieldSurface;
         DeltaGamma /= (H + MathUtils<double>::Dot(DeltaStressYieldCondition, prod(ElasticMatrix, PlasticPotentialDerivative)));

         //std::cout << " DELTA GAMMA " << DeltaGamma << std::endl;

         MatrixType UpdateMatrix;
         this->ConvertHenckyVectorToCauchyGreenTensor(-DeltaGamma * PlasticPotentialDerivative / 2.0, UpdateMatrix);

         rValues.StrainMatrix = prod(UpdateMatrix, rValues.StrainMatrix);
         rValues.StrainMatrix = prod(rValues.StrainMatrix, trans(UpdateMatrix));

         this->mElasticityModel.CalculateStressTensor(rValues, StressMatrix);

         double VolPlasticIncr = 0.0;
         for (unsigned int i = 0; i < 3; i++)
            VolPlasticIncr += DeltaGamma * DeltaStressYieldCondition(i);
         rPlasticVolDef += VolPlasticIncr;

         double DevPlasticIncr = 0.0;
         for (unsigned int i = 0; i < 3; i++)
            DevPlasticIncr += pow(DeltaGamma * DeltaStressYieldCondition(i) - VolPlasticIncr / 3.0, 2);
         for (unsigned int i = 3; i < 6; i++)
            DevPlasticIncr += 2.0 * pow(DeltaGamma * DeltaStressYieldCondition(i) / 2.0, 2.0);
         DevPlasticIncr = DeltaGamma / fabs(DeltaGamma) * sqrt(DevPlasticIncr);
         rPlasticDevDef += DevPlasticIncr;

         // Update the fabric !
         double MeanStress(0.0);
         MatrixType StressDev = StressMatrix;
         for (unsigned int i = 0; i < 3; i++)
            MeanStress += StressMatrix(i,i)/3.0;
         for (unsigned int i = 0; i < 3; i++)
            StressDev(i,i) -= MeanStress;

         StressDev = StressDev - MeanStress*FabricTensor;

         FabricTensor += rCalfa*( (3.0/4.0*StressDev/MeanStress - FabricTensor) * VolPlasticIncr + rChi*(1.0/3.0*StressDev/MeanStress-FabricTensor) * DevPlasticIncr );

         rP0 = -rInitialPrecon * std::exp(-rPlasticVolDef / (rLambda - rKappa));

         //std::cout << " P0 " << rP0 << " update " << h0 << std::endl;

         YieldSurface = this->mYieldSurface.CalculateYieldCondition(rVariables, YieldSurface);

         //std::cout << " ITERACIO " << i << " YIELD " << YieldSurface << std::endl;

         if (fabs(YieldSurface) < Tolerance)
         {
            Vector FabricVector2 = ZeroVector(6);
            ConstitutiveModelUtilities::StressTensorToVector( FabricTensor, FabricVector2);

            for (unsigned int i = 0; i < 6; i++)
               rVariables.Data.Internal(10+i) = FabricVector2(i);
            return;
         }
      }
      std::cout << " theStressPointDidNotReturnedCorrectly " << YieldSurface << std::endl;

      KRATOS_CATCH("")
   }


    //*******************************************************************************
    //*******************************************************************************
    // Calculate Stress and constitutive tensor
  template<typename TElasticityModel, typename TYieldSurface>
  void FabricBaseSoilModel<TElasticityModel, TYieldSurface>::CalculateStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix)
  {
     KRATOS_TRY
      { // modify the internal variables to make it nonlocal
      }

      const double LocalPlasticVolStrain = this->mInternal(1);
      const double NonLocalPlasticVolStrain = this->mInternal(4);
      this->mInternal(1) = this->mInternal(4);

      double LocalHenckyVolumetricStrain = this->mInternal(6);
      double NonLocalHenckyVolumetricStrain = this->mInternal(7);
      ProcessInfo SomeProcessInfo;
      if ( NonLocalHenckyVolumetricStrain!= LocalHenckyVolumetricStrain) {
         this->mElasticityModel.SetValue( HENCKY_ELASTIC_VOLUMETRIC_STRAIN, NonLocalHenckyVolumetricStrain, SomeProcessInfo);
      }
      this->mInternal(6) = this->mInternal(7);

      // integrate "analytically" ps and pt from plastic variables. Then update the internal variables.

      PlasticDataType Variables;
      this->InitializeVariables(rValues, Variables);

      const ModelDataType &rModelData = Variables.GetModelData();
      const Properties &rMaterialProperties = rModelData.GetProperties();

      const double &rInitialPrecon = rMaterialProperties[P0];
      const double &rKappa = rMaterialProperties[SWELLING_SLOPE];
      const double &rLambda = rMaterialProperties[NORMAL_COMPRESSION_SLOPE];

      const double &rPlasticVolDef = Variables.Data.Internal(1);
      double &rPC = this->mInternal(3);

      rPC = -rInitialPrecon * std::exp(-rPlasticVolDef / (rLambda - rKappa));

      { // end of the modifications
      }

     double Tolerance = 1e-6;

    /*PlasticDataType Variables;
     this->InitializeVariables(rValues, Variables);*/ // already done

     //Calculate trial stress Matrix
     this->mElasticityModel.CalculateStressTensor(rValues, rStressMatrix);

     VectorType FabricVector;
     for (unsigned int i = 0; i < 6; i++)
        FabricVector(i) = Variables.Data.Internal(10+i);


     MatrixType FabricTensor = ZeroMatrix(3,3);
     FabricTensor = ConstitutiveModelUtilities::StressVectorToTensor( FabricVector, FabricTensor);

     const MatrixType &rDeltaDeformationMatrix = rValues.GetDeltaDeformationMatrix();
     MatrixType R, U;
     this->PolarDecomposition(rDeltaDeformationMatrix, R, U);

     FabricTensor = prod( R, MatrixType( prod(FabricTensor, trans(R))));
     Vector FabricVectorRotated = ZeroVector(6);
     ConstitutiveModelUtilities::StressTensorToVector( FabricTensor, FabricVectorRotated);

     for (unsigned int i = 0; i < 6; i++)
        Variables.Data.Internal(10+i) = FabricVectorRotated(i);



     Variables.Data.TrialStateFunction = this->mYieldSurface.CalculateYieldCondition(Variables, Variables.Data.TrialStateFunction);

     Matrix ConstitutiveMatrix(6, 6);
     noalias(ConstitutiveMatrix) = ZeroMatrix(6, 6);
     Matrix ElasticTensor(6, 6);
     noalias(ElasticTensor) = ZeroMatrix(6, 6);
     Matrix ElastoPlasticTensor(6, 6);
     noalias(ElastoPlasticTensor) = ZeroMatrix(6, 6);

     //bool Plasticity = false;

     if (Variables.State().Is(ConstitutiveModelData::IMPLEX_ACTIVE))
     {
        // 1. Recover previous fabric
        for (unsigned int i = 0; i < 6; i++)
           Variables.Data.Internal(10+i) = FabricVector(i);

        this->RecoverPreviousElasticLeftCauchyGreen(rDeltaDeformationMatrix, rValues.StrainMatrix);
        // Calculate with implex
        this->CalculateImplexPlasticStep(rValues, Variables, rStressMatrix, rDeltaDeformationMatrix);

        rConstitutiveMatrix.clear();
        this->mElasticityModel.CalculateConstitutiveTensor(rValues, ConstitutiveMatrix);
        rConstitutiveMatrix = this->SetConstitutiveMatrixToTheAppropriateSize(rConstitutiveMatrix, ConstitutiveMatrix, rStressMatrix);
     }
     else if (Variables.Data.TrialStateFunction < Tolerance)
     {

        // elastic loading step
        rConstitutiveMatrix.clear();
        this->mElasticityModel.CalculateConstitutiveTensor(rValues, ConstitutiveMatrix);
        ElasticTensor = ConstitutiveMatrix;
        rConstitutiveMatrix = this->SetConstitutiveMatrixToTheAppropriateSize(rConstitutiveMatrix, ConstitutiveMatrix, rStressMatrix);
     }
     else
     {
       //Plasticity = true;

        // 1. Recover previous fabric
        for (unsigned int i = 0; i < 6; i++)
           Variables.Data.Internal(10+i) = FabricVector(i);


        // elasto-plastic step. Recover Initial be
        const MatrixType &rDeltaDeformationMatrixR = rValues.GetDeltaDeformationMatrix();
        this->RecoverPreviousElasticLeftCauchyGreen(rDeltaDeformationMatrixR, rValues.StrainMatrix);

        MatrixType R, U;
        this->PolarDecomposition(rDeltaDeformationMatrixR, R, U);
        const MatrixType DeltaDeformationMatrix = U;

        double InitialYieldFunction;
        this->mElasticityModel.CalculateStressTensor(rValues, rStressMatrix);
        InitialYieldFunction = this->mYieldSurface.CalculateYieldCondition(Variables, InitialYieldFunction);

        if (InitialYieldFunction > Tolerance)
        {
           // correct the initial drift (nonlocal, transfer...)
           this->ReturnStressToYieldSurface(rValues, Variables);
        }

        if ((InitialYieldFunction < -Tolerance) && (Variables.Data.TrialStateFunction > Tolerance))
        {
           // compute solution with change
           this->ComputeSolutionWithChange(rValues, Variables, DeltaDeformationMatrix);
        }
        else
        {
           bool UnloadingCondition = false;

           UnloadingCondition = this->EvaluateUnloadingCondition(rValues, Variables, DeltaDeformationMatrix);
           if (UnloadingCondition)
           {
              // compute solution with change
              this->ComputeSolutionWithChange(rValues, Variables, DeltaDeformationMatrix);
           }
           else
           {
              // compute plastic problem
              // compute unloading condition
              this->ComputeSubsteppingElastoPlasticProblem(rValues, Variables, DeltaDeformationMatrix);
           }
        }

        this->ReturnStressToYieldSurface(rValues, Variables);


       /*{ // doing an exercise ]
          MatrixType StrainMatrix = rValues.StrainMatrix;
          MatrixType Q;
          Q = ZeroMatrix(3,3);
          for (unsigned int i = 0; i < 360; i++) {
             std::cout << " ----------------i "  << i << std::endl;
             std::cout << " StrainMatrix " << StrainMatrix << std::endl;
             Q(0,0) = cos(2.0*3.14159*float(i)/360.0);
             Q(0,1) = sin(2.0*3.14159*float(i)/360.0);
             Q(1,0) = -Q(0,1);
             Q(1,1) = Q(0,0);
             Q(2,2) = 1.0;
             if ( i > 100)
                Q = R;
             std::cout << " Q !" << Q << std::endl;
             rValues.StrainMatrix = prod( Q, MatrixType( prod(StrainMatrix, trans(Q))));
             std::cout << " so " << rValues.StrainMatrix << std::endl;
            this->mElasticityModel.CalculateStressTensor(rValues, rStressMatrix);
            double p, J2, angle;
            StressInvariantsUtilities::CalculateStressInvariants(rStressMatrix, p, J2, angle);
            std::cout << rValues.StressMatrix << std::endl;
            std::cout << rStressMatrix << std::endl;
            std::cout << " XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXx " << p << " , " << J2 << " , " << angle << std::endl;

          }


       rValues.StrainMatrix = StrainMatrix;

       }*/



        rValues.StrainMatrix = prod(R, MatrixType(prod(rValues.StrainMatrix, trans(R))));
        this->mElasticityModel.CalculateStressTensor(rValues, rStressMatrix);
        noalias(rStressMatrix) = rValues.StressMatrix;

        for (unsigned int i = 0; i < 6; i++)
           FabricVector(i) = Variables.Data.Internal(10+i);
        FabricTensor = ConstitutiveModelUtilities::StressVectorToTensor( FabricVector, FabricTensor);
        FabricTensor = prod( R, MatrixType( prod(FabricTensor, trans(R))));
        Vector FabricVectorRotated = ZeroVector(6);
        ConstitutiveModelUtilities::StressTensorToVector( FabricTensor, FabricVectorRotated);
        for (unsigned int i = 0; i < 6; i++)
           Variables.Data.Internal(10+i) = FabricVectorRotated(i);

        this->mElasticityModel.CalculateConstitutiveTensor(rValues, ConstitutiveMatrix);
        ElasticTensor = ConstitutiveMatrix;
        this->ComputeElastoPlasticTangentMatrix(rValues, Variables, ConstitutiveMatrix);
        ElastoPlasticTensor = ConstitutiveMatrix;
        rConstitutiveMatrix = this->SetConstitutiveMatrixToTheAppropriateSize(rConstitutiveMatrix, ConstitutiveMatrix, rStressMatrix);
     }

     if (rValues.State.Is(ConstitutiveModelData::UPDATE_INTERNAL_VARIABLES))
     {
        this->UpdateInternalVariables(rValues, Variables, rStressMatrix);
        this->mStressMatrix = rStressMatrix / rValues.GetTotalDeformationDet();

        /*if ( Plasticity ) {
           mpAcousticTensorUtilities->SetConstitutiveTensor( ElastoPlasticTensor, ElasticTensor, Plasticity);
        } else {
           mpAcousticTensorUtilities->SetConstitutiveTensor( ElasticTensor, ElasticTensor, Plasticity);
        } // no need to save more data. Not using it*/
     }

     // Modifications for the nonlocal part
     if (rValues.State.Is(ConstitutiveModelData::UPDATE_INTERNAL_VARIABLES))
      {
         this->mInternal(4,1) = this->mInternal(4);
         this->mInternal(4) = this->mInternal(1);
         this->mInternal(1) = LocalPlasticVolStrain + (this->mInternal(1) - NonLocalPlasticVolStrain);

         this->mInternal(7,1) = this->mInternal(7);
         double CurrentHenckyVol(0);
         /// not updated
         MatrixType HenckyStrain;
         noalias( HenckyStrain) = ZeroMatrix(3,3);
         this->ConvertCauchyGreenTensorToHenckyTensor( rValues.StrainMatrix, HenckyStrain);
         for (unsigned int i = 0; i < 3; i++)
            CurrentHenckyVol += HenckyStrain(i,i);

         this->mInternal(6,1) = LocalHenckyVolumetricStrain;
         this->mInternal(7,1) = NonLocalHenckyVolumetricStrain;
         this->mInternal(7) = CurrentHenckyVol; // in case it is local....
         this->mInternal(6) = LocalHenckyVolumetricStrain + ( CurrentHenckyVol - NonLocalHenckyVolumetricStrain);
      }
      else
      {
        this->mInternal(1) = LocalPlasticVolStrain;
        this->mInternal(4) = NonLocalPlasticVolStrain;
        this->mInternal(6) = LocalHenckyVolumetricStrain;
        this->mInternal(7) = NonLocalHenckyVolumetricStrain;
      }
      // end modifications of the nonlocal part


     KRATOS_CATCH(" ")
  }

  template<typename TElasticityModel, typename TYieldSurface>
  void FabricBaseSoilModel<TElasticityModel, TYieldSurface>::SetValue(const Variable<Matrix> &rVariable,
                 const Matrix &rValue,
                 const ProcessInfo &rCurrentProcessInfo)
   {
      KRATOS_TRY

      if (rVariable == FABRIC_TENSOR)
      {
         Vector FabricVector(6);
         FabricVector = ZeroVector(6);
         ConstitutiveModelUtilities::StressTensorToVector( rValue, FabricVector);
         for (unsigned int i = 0; i < 6; i++)
            this->mInternal(10+i) = FabricVector(i);
      }
      else
      {
         PlasticityModel<TElasticityModel, TYieldSurface>::SetValue(rVariable, rValue, rCurrentProcessInfo);
      }

      KRATOS_CATCH("")
   }

  template<typename TElasticityModel, typename TYieldSurface>
  void FabricBaseSoilModel<TElasticityModel, TYieldSurface>::SetValue(const Variable<Vector> &rVariable,
                 const Vector &rValue,
                 const ProcessInfo &rCurrentProcessInfo)
   {
      KRATOS_TRY

      SoilBaseModel<TElasticityModel, TYieldSurface>::SetValue(rVariable, rValue, rCurrentProcessInfo);

      KRATOS_CATCH("")
   }


  template<typename TElasticityModel, typename TYieldSurface>
  void FabricBaseSoilModel<TElasticityModel, TYieldSurface>::SetValue(const Variable<double> &rVariable,
                 const double &rValue,
                 const ProcessInfo &rCurrentProcessInfo)
   {
      KRATOS_TRY

      if (rVariable == NONLOCAL_PLASTIC_VOL_DEF)
      {
         this->mInternal(4) = rValue;
      }
      else if (rVariable == NONLOCAL_PLASTIC_DEV_DEF)
      {
         this->mInternal(5) = rValue;
      }
      else if ( rVariable == NONLOCAL_HENCKY_ELASTIC_VOLUMETRIC_STRAIN) {
         this->mInternal(7) = rValue;
      }
      else
      {
         SoilBaseModel<TElasticityModel, TYieldSurface>::SetValue(rVariable, rValue, rCurrentProcessInfo);
      }

      KRATOS_CATCH("")
   }

  template<typename TElasticityModel, typename TYieldSurface>
  Matrix & FabricBaseSoilModel<TElasticityModel, TYieldSurface>::GetValue(const Variable<Matrix> &rVariable, Matrix &rValue)
   {
      KRATOS_TRY

      if (rVariable == FABRIC_TENSOR)
      {
         VectorType FabricVector;
         for (unsigned int i = 0; i < 6; i++)
            FabricVector(i) = this->mInternal(10+i);

         rValue = ZeroMatrix(3,3);
         MatrixType FabricTensor = ZeroMatrix(3,3);
         FabricTensor = ConstitutiveModelUtilities::StressVectorToTensor( FabricVector, FabricTensor);
         for (unsigned int i = 0; i < 3; i++)
            for (unsigned int j = 0; j < 3; j++)
               rValue(i,j) = FabricTensor(i,j);
      }
      else
      {
         rValue = FabricBaseSoilModel::GetValue(rVariable, rValue);
      }
      return rValue;

      KRATOS_CATCH("")
   }

  template<typename TElasticityModel, typename TYieldSurface>
  double & FabricBaseSoilModel<TElasticityModel, TYieldSurface>::GetValue(const Variable<double> &rVariable, double &rValue)
   {
      KRATOS_TRY

      rValue = 0;

      if (rVariable == M_MODULUS)
      {
         rValue = 1e10;
         if (this->mInitialized)
         {
            rValue = mBulkModulus + 4.0/3.0*mShearModulus;
         }
      }
      else if (rVariable == YOUNG_MODULUS)
      {
         rValue = 1e10;
         if (this->mInitialized)
         {
            rValue = 9.0*mShearModulus*mBulkModulus/(3.0*mBulkModulus+mShearModulus);
         }
      }
      else if (rVariable == SHEAR_MODULUS)
      {
         rValue = 1e10;
         if (this->mInitialized)
         {
            rValue = mShearModulus;
         }
      }
      else if (rVariable == BULK_MODULUS)
      {
         rValue = 1e10;
         if (this->mInitialized)
         {
            rValue = mBulkModulus;
         }
      }
      else if (rVariable == PLASTIC_STRAIN)
      {
         rValue = this->mInternal(0);
      }
      else if (rVariable == DELTA_PLASTIC_STRAIN)
      {
         rValue = this->mInternal(0) - this->mInternal(0,1);
      }
      else if (rVariable == PLASTIC_VOL_DEF)
      {
         rValue = this->mInternal(1);
      }
      else if (rVariable == PLASTIC_DEV_DEF)
      {
         rValue = this->mInternal(2);
      }
      else if (rVariable == P0)
      {
         rValue = this->mInternal(3);
      }
      else if (rVariable == NONLOCAL_PLASTIC_VOL_DEF)
      {
         rValue = this->mInternal(4,1);
      }
      else if (rVariable == NONLOCAL_PLASTIC_DEV_DEF)
      {
         rValue = this->mInternal(5,1);
      }
      else if ( rVariable == HENCKY_ELASTIC_VOLUMETRIC_STRAIN) {
         rValue = this->mInternal(6);
      }
      else if ( rVariable == NONLOCAL_HENCKY_ELASTIC_VOLUMETRIC_STRAIN) {
         rValue = this->mInternal(7);
      } else if ( rVariable == FABRIC) {

         VectorType FabricVector;
         for (unsigned int i = 0; i < 6; i++) {
            FabricVector(i) = this->mInternal(10+i);
         }
         std::cout << std::endl;
         rValue = 0.0;
         double number = 1.0;
         for (unsigned int i = 0; i < 6; i++) {
            number = 1.0; 
            if ( i > 2) {
               number = 2.0;
            }
            rValue += number * pow( FabricVector(i), 2);
         }
         rValue = sqrt(1.5*rValue);

      } else
      {
         rValue = SoilBaseModel<TElasticityModel, TYieldSurface>::GetValue(rVariable, rValue);
      }
      return rValue;

      KRATOS_CATCH("")
   }

   template class FabricBaseSoilModel<BorjaModel, SClay1YieldSurface<SClay1HardeningRule>>;
   template class FabricBaseSoilModel<DV2BorjaModel, SClay1YieldSurface<SClay1HardeningRule>>;
}
