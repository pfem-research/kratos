//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:                October 2021 $
//
//

#if !defined(KRATOS_SCLAY1_SOIL_MODEL_HPP_INCLUDED)
#define KRATOS_SCLAY1_SOIL_MODEL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/fabric_base_soil_model.hpp"
#include "custom_models/plasticity_models/hardening_rules/sclay1_hardening_rule.hpp"
#include "custom_models/plasticity_models/yield_surfaces/sclay1_yield_surface.hpp"
#include "custom_models/elasticity_models/borja_model.hpp"


namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
    */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) SClay1SoilModel : public FabricBaseSoilModel<BorjaModel, SClay1YieldSurface<SClay1HardeningRule>>
{
public:
   ///@name Type Definitions
   ///@{

   //elasticity model
   typedef BorjaModel ElasticityModelType;
   typedef ElasticityModelType::Pointer ElasticityModelPointer;

   //yield surface
   typedef SClay1HardeningRule HardeningRuleType;
   typedef SClay1YieldSurface<HardeningRuleType> YieldSurfaceType;
   typedef YieldSurfaceType::Pointer YieldSurfacePointer;

   //base type
   typedef FabricBaseSoilModel<ElasticityModelType, YieldSurfaceType> BaseType;

   //common types
   typedef BaseType::Pointer BaseTypePointer;
   typedef BaseType::SizeType SizeType;
   typedef BaseType::VoigtIndexType VoigtIndexType;
   typedef BaseType::MatrixType MatrixType;
   typedef BaseType::ModelDataType ModelDataType;
   typedef BaseType::MaterialDataType MaterialDataType;
   typedef BaseType::PlasticDataType PlasticDataType;
   typedef BaseType::InternalVariablesType InternalVariablesType;

   /// Pointer definition of SClay1SoilModel
   KRATOS_CLASS_POINTER_DEFINITION(SClay1SoilModel);

   ///@}
   ///@name Life Cycle
   ///@{

   /// Default constructor.
   SClay1SoilModel() : BaseType()
   {
   }

   /// Copy constructor.
   SClay1SoilModel(SClay1SoilModel const &rOther) : BaseType(rOther) {}

   /// Assignment operator.
   SClay1SoilModel &operator=(SClay1SoilModel const &rOther)
   {
      BaseType::operator=(rOther);
      return *this;
   }

   /// Clone.
   ConstitutiveModel::Pointer Clone() const override
   {
      return Kratos::make_shared<SClay1SoilModel>(*this);
   }

   /// Destructor.
   ~SClay1SoilModel() override {}

   ///@}
   ///@name Operators
   ///@{

   ///@}
   ///@name Operations
   ///@{

   /**
          * Check
          */
   int Check(const Properties &rProperties, const ProcessInfo &rCurrentProcessInfo) override
   {
      KRATOS_TRY

      //LMV: to be implemented. but should not enter in the base one

      return 0;

      KRATOS_CATCH("")
   }

   /**
       * Get required properties
       */
   void GetRequiredProperties(Properties &rProperties) override
   {
      KRATOS_TRY

      // Set required properties to check keys
      DerivedType::GetRequiredProperties(rProperties);

      KRATOS_CATCH(" ")
   }

   ///@}
   ///@name Access
   ///@{

   /**
          * Has Values
          */
   bool Has(const Variable<double> &rVariable) override
   {
      if (rVariable == PLASTIC_STRAIN || rVariable == DELTA_PLASTIC_STRAIN)
         return true;

      return false;
   }

   /**
          * Get Values
          */

   ///@}
   ///@name Inquiry
   ///@{

   ///@}
   ///@name Input and output
   ///@{

   /// Turn back information as a string.
   std::string Info() const override
   {
      std::stringstream buffer;
      buffer << "SClay1SoilModel";
      return buffer.str();
   }

   /// Print information about this object.
   void PrintInfo(std::ostream &rOStream) const override
   {
      rOStream << "SClay1SoilModel";
   }

   /// Print object's data.
   void PrintData(std::ostream &rOStream) const override
   {
      rOStream << "SClay1SoilModel Data";
   }

   ///@}
   ///@name Friends
   ///@{

   ///@}

protected:
   ///@name Protected static Member Variables
   ///@{

   ///@}
   ///@name Protected member Variables
   ///@{

   ///@}
   ///@name Protected Operators
   ///@{

   ///@}
   ///@name Protected Operations
   ///@{

   ///@}
   ///@name Protected  Access
   ///@{

   ///@}
   ///@name Protected Inquiry
   ///@{

   ///@}
   ///@name Protected LifeCycle
   ///@{

   ///@}

private:
   ///@name Static Member Variables
   ///@{

   ///@}
   ///@name Member Variables
   ///@{

   ///@}
   ///@name Private Operators
   ///@{

   ///@}
   ///@name Private Operations
   ///@{

   ///@}
   ///@name Private  Access
   ///@{

   ///@}
   ///@name Private Inquiry
   ///@{

   ///@}
   ///@name Serialization
   ///@{
   friend class Serializer;

   void save(Serializer &rSerializer) const override
   {
      KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BaseType)
   }

   void load(Serializer &rSerializer) override
   {
      KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BaseType)
   }

   ///@}
   ///@name Un accessible methods
   ///@{

   ///@}

}; // Class SClay1SoilModel

///@}

///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
///@}
///@name Input and output
///@{
///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_SCLAY1_SOIL_MODEL_HPP_INCLUDED  defined
