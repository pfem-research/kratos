//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                  April 2017 $
//
//

#if !defined(KRATOS_PLASTICITY_MODEL_HPP_INCLUDED)
#define KRATOS_PLASTICITY_MODEL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/constitutive_model.hpp"
#include "custom_models/plasticity_models/yield_surfaces/yield_surface.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplicationes
///@{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
template <class TElasticityModel, class TYieldSurface>
class PlasticityModel : public ConstitutiveModel
{
public:
  ///@name Type Definitions
  ///@{

  //elasticity model
  typedef TElasticityModel ElasticityModelType;
  typedef typename TElasticityModel::ElasticDataType ElasticDataType;

  //yield surface
  typedef TYieldSurface YieldSurfaceType;

  //common types
  typedef ConstitutiveModelData::SizeType SizeType;
  typedef ConstitutiveModelData::VoigtIndexType VoigtIndexType;
  typedef ConstitutiveModelData::MatrixType MatrixType;
  typedef ConstitutiveModelData::VectorType VectorType;
  typedef ConstitutiveModelData::ModelData ModelDataType;

  typedef typename TYieldSurface::NameType NameType;
  typedef typename TYieldSurface::PlasticDataType PlasticDataType;
  typedef typename TYieldSurface::InternalVariablesType InternalVariablesType;
  typedef typename TYieldSurface::InternalDataType InternalDataType;

  /// Pointer definition of PlasticityModel
  KRATOS_CLASS_POINTER_DEFINITION(PlasticityModel);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  PlasticityModel() : mElasticityModel(), mYieldSurface()
  {
  }

  /// Copy constructor.
  PlasticityModel(PlasticityModel const &rOther) : mElasticityModel(rOther.mElasticityModel), mYieldSurface(rOther.mYieldSurface)
  {
    this->mOptions = rOther.mOptions;
  }

  /// Assignment operator.
  PlasticityModel &operator=(PlasticityModel const &rOther)
  {
    this->mOptions = rOther.mOptions;
    mElasticityModel = rOther.mElasticityModel;
    mYieldSurface = rOther.mYieldSurface;
    return *this;
  }

  /// Clone.
  ConstitutiveModel::Pointer Clone() const override
  {
    return Kratos::make_shared<PlasticityModel>(*this);
  }

  /// Destructor.
  ~PlasticityModel() override {}

  ///@}
  ///@name Operators
  ///@{
  ///@}
  ///@name Operations
  ///@{

  /**
     * Initialize member data
     */
  void InitializeMaterial(const Properties &rProperties) override
  {
    KRATOS_TRY

    mElasticityModel.InitializeMaterial(rProperties);

    KRATOS_CATCH(" ")
  }

  /**
     * Initialize member data
     */
  void InitializeModel(ModelDataType &rValues) override
  {
    KRATOS_TRY

    mElasticityModel.InitializeModel(rValues);

    KRATOS_CATCH(" ")
  }

  /**
     * Finalize member data
     */
  void FinalizeModel(ModelDataType &rValues) override
  {
    KRATOS_TRY

    //attention updated mpHistoryVector from ElasticityModel
    mElasticityModel.FinalizeModel(rValues);

    KRATOS_CATCH(" ")
  }

  /**
     * Calculate Stresses
     */
  void CalculateStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix) override
  {
    KRATOS_TRY

    KRATOS_ERROR << "calling the PlasticityModel base class ... illegal operation" << std::endl;

    KRATOS_CATCH(" ")
  }

  void CalculateIsochoricStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix) override
  {
    KRATOS_TRY

    KRATOS_ERROR << "calling the PlasticityModel base class ... illegal operation" << std::endl;

    KRATOS_CATCH(" ")
  }

  void CalculateVolumetricStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix) override
  {
    KRATOS_TRY

    mElasticityModel.CalculateVolumetricStressTensor(rValues, rStressMatrix);

    KRATOS_CATCH(" ")
  }

  /**
     * Calculate Constitutive Tensor
     */
  void CalculateConstitutiveTensor(ModelDataType &rValues, Matrix &rConstitutiveMatrix) override
  {
    KRATOS_ERROR << "calling PlasticityModel base class " << std::endl;
  }

  void CalculateIsochoricConstitutiveTensor(ModelDataType &rValues, Matrix &rConstitutiveMatrix) override
  {
    KRATOS_ERROR << "calling PlasticityModel base class " << std::endl;
  }

  void CalculateVolumetricConstitutiveTensor(ModelDataType &rValues, Matrix &rConstitutiveMatrix) override
  {
    KRATOS_TRY

    mElasticityModel.CalculateVolumetricConstitutiveTensor(rValues, rConstitutiveMatrix);

    KRATOS_CATCH(" ")
  }

  /**
     * Calculate Stress and Constitutive Tensor
     */
  void CalculateStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix) override
  {
    KRATOS_ERROR << "calling PlasticityModel base class " << std::endl;
  }

  void CalculateIsochoricStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix) override
  {
    KRATOS_ERROR << "calling PlasticityModel base class " << std::endl;
  }

  void CalculateVolumetricStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix) override
  {
    KRATOS_ERROR << "calling PlasticityModel base class " << std::endl;
  }

  // calculate requested internal variables
  void CalculateInternalVariables(ModelDataType &rValues) override
  {
    KRATOS_TRY

    this->mElasticityModel.CalculateInternalVariables(rValues);

    KRATOS_CATCH(" ")
  }

  /**
     * Check
     */
  int Check(const Properties &rProperties, const ProcessInfo &rCurrentProcessInfo) override
  {
    KRATOS_TRY

    if (DENSITY.Key() == 0 || rProperties[DENSITY] < 0.00)
      KRATOS_ERROR << "DENSITY has Key zero or invalid value" << std::endl;

    mElasticityModel.Check(rProperties, rCurrentProcessInfo);

    return 0;

    KRATOS_CATCH(" ")
  }

  /**
      * Get required properties
      */
  void GetRequiredProperties(Properties &rProperties) override
  {
    KRATOS_TRY

    // Set required properties
    double value = 0.0; //dummy
    rProperties.SetValue(DENSITY,value);

    mElasticityModel.GetRequiredProperties(rProperties);
    mYieldSurface.GetRequiredProperties(rProperties);

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{

  /**
     * method to ask the plasticity model the list of variables (dofs) needed from the domain
     * @param rScalarVariables : list of scalar dofs
     * @param rComponentVariables :  list of vector dofs
     */
  void GetDomainVariablesList(std::vector<Variable<double>*> &rScalarVariables,
                              std::vector<Variable<array_1d<double, 3>>*> &rComponentVariables) override
  {
    KRATOS_TRY

    mElasticityModel.GetDomainVariablesList(rScalarVariables, rComponentVariables);

    KRATOS_CATCH(" ")
  }

  void GetStrainMeasures(std::vector<ConstitutiveModelData::StrainMeasureType> &rStrainMeasures) override
  {
    KRATOS_TRY

    mElasticityModel.GetStrainMeasures(rStrainMeasures);

    KRATOS_CATCH(" ")
  }

  /**
     * Has Values
     */
  bool Has(const Variable<double> &rVariable) override { return false; }

  bool Has(const Variable<array_1d<double,3>> &rVariable) override { return false; }

  bool Has(const Variable<Vector> &rVariable) override { return false; }

  bool Has(const Variable<Matrix> &rVariable) override { return false; }


  /**
     * Set Values
     */
  void SetValue(const Variable<double> &rVariable,
                const double &rValue,
                const ProcessInfo &rCurrentProcessInfo) override
  {
    mElasticityModel.SetValue(rVariable, rValue, rCurrentProcessInfo);
  }

  void SetValue(const Variable<array_1d<double,3>> &rVariable,
                const array_1d<double,3> &rValue,
                const ProcessInfo &rCurrentProcessInfo) override
  {
    mElasticityModel.SetValue(rVariable, rValue, rCurrentProcessInfo);
  }

  void SetValue(const Variable<Vector> &rVariable, const Vector &rValue,
                const ProcessInfo &rCurrentProcessInfo) override
  {
    mElasticityModel.SetValue(rVariable, rValue, rCurrentProcessInfo);
  }

  void SetValue(const Variable<Matrix> &rVariable, const Matrix &rValue,
                const ProcessInfo &rCurrentProcessInfo) override
  {
    mElasticityModel.SetValue(rVariable, rValue, rCurrentProcessInfo);
  }

  /**
     * Get Values
     */
  double &GetValue(const Variable<double> &rVariable, double &rValue) override
  {
    rValue = mElasticityModel.GetValue(rVariable, rValue);
    return rValue;
  }

  array_1d<double, 3> &GetValue(const Variable<array_1d<double,3>> & rVariable,
                                 array_1d<double, 3> & rValue) override
  {
    rValue = mElasticityModel.GetValue(rVariable, rValue);
    return rValue;
  }

  Vector &GetValue(const Variable<Vector> &rVariable, Vector &rValue) override
  {
    rValue = mElasticityModel.GetValue(rVariable, rValue);
    return rValue;
  }

  Matrix &GetValue(const Variable<Matrix> &rVariable, Matrix &rValue) override
  {
    rValue = mElasticityModel.GetValue(rVariable, rValue);
    return rValue;
  }


  ElasticityModelType &GetElasticityModel() { return mElasticityModel; };


  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "PlasticityModel";
    return buffer.str();
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "PlasticityModel";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "PlasticityModel Data";
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{

  ElasticityModelType mElasticityModel;
  YieldSurfaceType mYieldSurface;

  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{

  virtual void InitializeVariables(ModelDataType &rValues, PlasticDataType &rVariables)
  {
    KRATOS_TRY

    KRATOS_ERROR << "calling PlasticityModel base class " << std::endl;

    KRATOS_CATCH(" ")
  }

  //set calculated internal variables when requested
  virtual void SetInternalVariables(ModelDataType &rValues, PlasticDataType &rVariables)
  {
    KRATOS_TRY

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Protected  Access
  ///@{

  using ConstitutiveModel::InitializeVariables;

  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Serialization
  ///@{

  friend class Serializer;

  void save(Serializer &rSerializer) const override
  {
    //KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, ConstitutiveModel)
    rSerializer.save("mOptions", this->mOptions);
    rSerializer.save("mElasticityModel", mElasticityModel);
    rSerializer.save("mYieldSurface", mYieldSurface);
  }

  void load(Serializer &rSerializer) override
  {
    //KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, ConstitutiveModel)
    rSerializer.load("mOptions", this->mOptions);
    rSerializer.load("mElasticityModel", mElasticityModel);
    rSerializer.load("mYieldSurface", mYieldSurface);
  }

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class PlasticityModel

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
///@}
///@name Input and output
///@{
///@}
///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_PLASTICITY_MODEL_HPP_INCLUDED  defined
