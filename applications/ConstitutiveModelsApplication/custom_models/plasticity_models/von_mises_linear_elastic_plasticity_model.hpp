//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                  April 2017 $
//
//

#if !defined(KRATOS_VON_MISES_LINEAR_ELASTIC_PLASTICITY_MODEL_HPP_INCLUDED)
#define KRATOS_VON_MISES_LINEAR_ELASTIC_PLASTICITY_MODEL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/non_linear_associative_plasticity_model.hpp"
#include "custom_models/plasticity_models/yield_surfaces/mises_huber_yield_surface.hpp"
#include "custom_models/plasticity_models/hardening_rules/simo_exponential_hardening_rule.hpp"
#include "custom_models/elasticity_models/linear_elastic_model.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) VonMisesLinearElasticPlasticityModel : public NonLinearAssociativePlasticityModel<LinearElasticModel, MisesHuberYieldSurface<SimoExponentialHardeningRule>>
{
public:
  ///@name Type Definitions
  ///@{

  //elasticity model
  typedef LinearElasticModel ElasticityModelType;
  typedef ElasticityModelType::Pointer ElasticityModelPointer;

  //yield surface
  typedef SimoExponentialHardeningRule HardeningRuleType;
  typedef MisesHuberYieldSurface<HardeningRuleType> YieldSurfaceType;
  typedef YieldSurfaceType::Pointer YieldSurfacePointer;

  //derived type
  typedef NonLinearAssociativePlasticityModel<ElasticityModelType, YieldSurfaceType> DerivedType;

  //base type
  typedef PlasticityModel<ElasticityModelType, YieldSurfaceType> BaseType;

  //common types
  typedef BaseType::Pointer BaseTypePointer;
  typedef BaseType::SizeType SizeType;
  typedef BaseType::VoigtIndexType VoigtIndexType;
  typedef BaseType::MatrixType MatrixType;
  typedef BaseType::ModelDataType ModelDataType;
  typedef BaseType::MaterialDataType MaterialDataType;
  typedef BaseType::PlasticDataType PlasticDataType;
  typedef BaseType::InternalVariablesType InternalVariablesType;

  typedef DerivedType::NameType NameType;
  typedef DerivedType::ElasticDataType ElasticDataType;

  /// Pointer definition of VonMisesLinearElasticPlasticityModel
  KRATOS_CLASS_POINTER_DEFINITION(VonMisesLinearElasticPlasticityModel);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  VonMisesLinearElasticPlasticityModel()
    : DerivedType()
  {
    mpHistoryVector = new VectorType(ZeroVector(6));
  }

  /// Copy constructor.
  VonMisesLinearElasticPlasticityModel(VonMisesLinearElasticPlasticityModel const &rOther)
      : DerivedType(rOther)
  {
    mpHistoryVector = new VectorType(*rOther.mpHistoryVector);
  }

  /// Assignment operator.
  VonMisesLinearElasticPlasticityModel &operator=(VonMisesLinearElasticPlasticityModel const &rOther)
  {
    DerivedType::operator=(rOther);
    mpHistoryVector = new VectorType(*rOther.mpHistoryVector);
    return *this;
  }

  /// Clone.
  ConstitutiveModel::Pointer Clone() const override
  {
    return Kratos::make_shared<VonMisesLinearElasticPlasticityModel>(*this);
  }

  /// Destructor.
  ~VonMisesLinearElasticPlasticityModel() override {}

  ///@}
  ///@name Operators
  ///@{
  ///@}
  ///@name Operations
  ///@{
  ///@}
  ///@name Access
  ///@{

  /**
     * Has Values
     */
  bool Has(const Variable<double> &rVariable) override
  {
    if (rVariable == PLASTIC_STRAIN || rVariable == DELTA_PLASTIC_STRAIN)
      return true;
    else
      return BaseType::Has(rVariable);
  }

  bool Has(const Variable<Vector> &rVariable) override
  {
    if (rVariable == INTERNAL_PLASTIC_STRAIN_VECTOR)
      return true;
    else if (rVariable == INTERNAL_VARIABLES)
      return true;
    else
      return BaseType::Has(rVariable);
  }

  /**
     * Set Values
     */
  void SetValue(const Variable<double> &rVariable,
                const double &rValue,
                const ProcessInfo &rCurrentProcessInfo) override
  {
    if (rVariable == PLASTIC_STRAIN)
    {
      this->mInternal(NameType::PLASTIC_STRAIN) = rValue;
    }
  }

  void SetValue(const Variable<Vector> &rVariable, const Vector &rValue,
                const ProcessInfo &rCurrentProcessInfo) override
  {
    if (rVariable == INTERNAL_PLASTIC_STRAIN_VECTOR)
    {
      (*this->mpHistoryVector) = rValue;
    }
    else if (rVariable == INTERNAL_VARIABLES)
    {
      this->mInternal.SetData(rValue);
    }
    else{
      BaseType::SetValue(rVariable, rValue, rCurrentProcessInfo);
    }
  }


  /**
     * Get Values
     */
  double &GetValue(const Variable<double> &rVariable, double &rValue) override
  {

    rValue = 0;

    if (rVariable == PLASTIC_STRAIN)
    {
      rValue = this->mInternal(NameType::PLASTIC_STRAIN);
    }
    else if (rVariable == DELTA_PLASTIC_STRAIN)
    {
      rValue = this->mInternal(NameType::PLASTIC_STRAIN) - mInternal(NameType::PLASTIC_STRAIN,1);
    }
    else{
      rValue = BaseType::GetValue(rVariable, rValue);
    }

    return rValue;
  }

  Vector &GetValue(const Variable<Vector> &rVariable, Vector &rValue) override
  {
    if (rVariable == INTERNAL_PLASTIC_STRAIN_VECTOR)
    {
      rValue = *this->mpHistoryVector;
    }
    else if (rVariable == INTERNAL_VARIABLES)
    {
      this->mInternal.GetData(rValue);
    }
    else{
      rValue = BaseType::GetValue(rVariable, rValue);
    }

    return rValue;
  }

  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "VonMisesLinearElasticPlasticityModel";
    return buffer.str();
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "VonMisesLinearElasticPlasticityModel";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "VonMisesLinearElasticPlasticityModel Data";
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{

  /**
     * Initialize variables
     */
  void InitializeVariables(ModelDataType &rValues, PlasticDataType &rVariables) override
  {
    KRATOS_TRY

    DerivedType::InitializeVariables(rValues, rVariables);

    //elastic strain
    VectorType StrainVector;
    ConstitutiveModelUtilities::StrainTensorToVector(rValues.StrainMatrix, StrainVector);

    StrainVector -= *this->mpHistoryVector;

    rValues.StrainMatrix = ConstitutiveModelUtilities::StrainVectorToTensor(StrainVector, rValues.StrainMatrix);

    KRATOS_CATCH(" ")
  }

  /**
     * Update internal variables
     */
  void UpdateInternalVariables(ModelDataType &rValues, PlasticDataType &rVariables, const MatrixType &rStressMatrix) override
  {
    KRATOS_TRY

    double &rDeltaGamma = rVariables.Data.DeltaInternal[NameType::PLASTIC_STRAIN];

    mThermal.Update();

    mInternal.Update(rVariables.Data.Internal);

    const MaterialDataType &rMaterial = rVariables.GetMaterialParameters();

    //update plastic strain measure
    rValues.StrainMatrix = ConstitutiveModelUtilities::StrainVectorToTensor(*this->mpHistoryVector, rValues.StrainMatrix);
    rValues.StrainMatrix += rDeltaGamma * rStressMatrix / (rVariables.Data.StressNorm - 2.0 * rMaterial.GetLameMuBar() * rDeltaGamma);
    ConstitutiveModelUtilities::StrainTensorToVector(rValues.StrainMatrix, *this->mpHistoryVector);

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, DerivedType)
    rSerializer.save("mpHistoryVector", *mpHistoryVector);
  }

  void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, DerivedType)
    rSerializer.load("mpHistoryVector", *mpHistoryVector);
  }

  ///@}
  ///@name Un accessible methods
  ///@{
  ///@}

}; // Class VonMisesLinearElasticPlasticityModel

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
///@}
///@name Input and output
///@{
///@}
///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_VON_MISES_LINEAR_ELASTIC_PLASTICITY_MODEL_HPP_INCLUDED  defined
