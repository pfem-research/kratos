//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:                October 2021 $
//
//

#if !defined(KRATOS_FABRIC_BASE_SOIL_MODEL_HPP_INCLUDED)
#define KRATOS_FABRIC_BASE_SOIL_MODEL_HPP_INCLUDED

// System includes

// External includes
#include <iostream>
#include <fstream>

// Project includes
#include "custom_models/plasticity_models/soil_base_model.hpp"

//***** the hardening law associated to this Model has ... variables
// 0. Plastic multiplier
// 1. Plastic Volumetric deformation
// 2. Plastic Deviatoric deformation
// 3. p0 (mechanical)
// 4. NonLocal Plastic Vol Def
// 5. NonLocal Plastic Dev Def
// 6. ElasticHenckyStrain
// 7. Nonlocal ElasticHenckyStrain
// ... (the number now is then..., xD)
// 10-15 FabricTensor

// since in the whole formulation I only need the deviatoric part of the fabric tensor, the "variable" is the deviatoric part and then, for postprocess, I will add the "volumetric" part which is one

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
    */
template <class TElasticityModel, class TYieldSurface>
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) FabricBaseSoilModel : public SoilBaseModel<TElasticityModel, TYieldSurface>
{
public:
   ///@name Type Definitions
   ///@{

   //elasticity model
   typedef TElasticityModel ElasticityModelType;

   //yield surface
   typedef TYieldSurface YieldSurfaceType;

   // derived type
   typedef SoilBaseModel<ElasticityModelType, YieldSurfaceType> DerivedType;

   //base type
   typedef PlasticityModel<ElasticityModelType, YieldSurfaceType> BaseType;

   //common types
   typedef typename BaseType::Pointer BaseTypePointer;
   typedef typename BaseType::SizeType SizeType;
   typedef typename BaseType::VoigtIndexType VoigtIndexType;
   typedef typename BaseType::MatrixType MatrixType;
   typedef typename BaseType::VectorType VectorType;
   typedef typename BaseType::ModelDataType ModelDataType;
   typedef typename BaseType::MaterialDataType MaterialDataType;
   typedef typename BaseType::PlasticDataType PlasticDataType;
   typedef typename BaseType::InternalVariablesType InternalVariablesType;

   /// Pointer definition of FabricBaseSoilModel
   KRATOS_CLASS_POINTER_DEFINITION(FabricBaseSoilModel);

   ///@}
   ///@name Life Cycle
   ///@{

   /// Default constructor.
   FabricBaseSoilModel() : DerivedType() { }

   /// Copy constructor.
   FabricBaseSoilModel(FabricBaseSoilModel const &rOther) : DerivedType(rOther),
   mShearModulus(rOther.mShearModulus), mBulkModulus(rOther.mBulkModulus) {}

   /// Assignment operator.
   FabricBaseSoilModel &operator=(FabricBaseSoilModel const &rOther)
   {
      DerivedType::operator=(rOther);
      this->mShearModulus = rOther.mShearModulus;
      this->mBulkModulus = rOther.mBulkModulus;

      return *this;
   }

   /// Clone.
   ConstitutiveModel::Pointer Clone() const override
   {
      return (FabricBaseSoilModel::Pointer(new FabricBaseSoilModel(*this)));
   }

   /// Destructor.
   ~FabricBaseSoilModel() override {}

   ///@}
   ///@name Operators
   ///@{

   ///@}
   ///@name Operations
   ///@{

   /**
          * Initialize member data
          */
   void InitializeModel(ModelDataType &rValues) override
   {
      KRATOS_TRY

      if (this->mInitialized == false)
      {
         PlasticDataType Variables;
         this->InitializeVariables(rValues, Variables);

         const ModelDataType &rModelData = Variables.GetModelData();
         const Properties &rMaterialProperties = rModelData.GetProperties();

         double &rP0 = Variables.Data.Internal(3);
         rP0 = -rMaterialProperties[P0];

         // FABRIC TENSOR INITIALIZATION
         for (unsigned int i = 0; i < 3; i++)
            Variables.Data.Internal(10+i) = 0.0;
         for (unsigned int i = 3; i < 6; i++)
            Variables.Data.Internal(10+i) = 0.0;

         Variables.Data.Internal(10) = rMaterialProperties[FABRIC_TENSOR_X];
         Variables.Data.Internal(11) = rMaterialProperties[FABRIC_TENSOR_Y];
         Variables.Data.Internal(12) = rMaterialProperties[FABRIC_TENSOR_Z];

         double ElasticModulus = rMaterialProperties[P0];
         ElasticModulus /= rMaterialProperties[SWELLING_SLOPE];


         MatrixType Stress;
         this->UpdateInternalVariables(rValues, Variables, Stress);

         this->mInitialized = true;

         mBulkModulus = ElasticModulus;
         mShearModulus = ElasticModulus;
      }
      this->mElasticityModel.InitializeModel(rValues);

      KRATOS_CATCH("")
   }

   /**
          * Check
          */
   virtual int Check(const Properties &rMaterialProperties, const ProcessInfo &rCurrentProcessInfo) override
   {
      KRATOS_TRY

      //LMV: to be implemented. but should not enter in the base one

      return 0;

      KRATOS_CATCH("")
   }

   /**
       * Get required properties
       */
   void GetRequiredProperties(Properties &rProperties) override
   {
      KRATOS_TRY

      // Set required properties to check keys
      DerivedType::GetRequiredProperties(rProperties);

      KRATOS_CATCH(" ")
   }

   ///@}
   ///@name Access
   ///@{

   /**
          * Has Values
          */
   virtual bool Has(const Variable<double> &rVariable) override
   {
      if (rVariable == PLASTIC_STRAIN || rVariable == DELTA_PLASTIC_STRAIN)
         return true;

      return false;
   }

   /**
          * Get Values
          */
   void SetValue(const Variable<double> &rVariable,
                 const double &rValue,
                 const ProcessInfo &rCurrentProcessInfo) override;
   
   void SetValue(const Variable<Vector> &rVariable,
                 const Vector &rValue,
                 const ProcessInfo &rCurrentProcessInfo) override;
   
   void SetValue(const Variable<Matrix> &rVariable,
                 const Matrix &rValue,
                 const ProcessInfo &rCurrentProcessInfo) override;

   /**
          * Get Values
          */
   virtual double &GetValue(const Variable<double> &rVariable, double &rValue) override;
   
   virtual Matrix &GetValue(const Variable<Matrix> &rVariable, Matrix &rValue) override;
    
   
   //*******************************************************************************
    //*******************************************************************************
    // Calculate Stress and constitutive tensor
    void CalculateStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix) override;
    

   ///@}
   ///@name Inquiry
   ///@{

   ///@}
   ///@name Input and output
   ///@{

   /// Turn back information as a string.
   virtual std::string Info() const override
   {
      std::stringstream buffer;
      buffer << "FabricBaseSoilModel";
      return buffer.str();
   }

   /// Print information about this object.
   virtual void PrintInfo(std::ostream &rOStream) const override
   {
      rOStream << "FabricBaseSoilModel";
   }

   /// Print object's data.
   virtual void PrintData(std::ostream &rOStream) const override
   {
      rOStream << "FabricBaseSoilModel Data";
   }

   ///@}
   ///@name Friends
   ///@{

   ///@}

protected:
   ///@name Protected static Member Variables
   ///@{

   ///@}
   ///@name Protected member Variables
   ///@{


   double mShearModulus;
   double mBulkModulus;

   ///@}
   ///@name Protected Operators
   ///@{

   ///@}
   ///@name Protected Operations
   ///@{


   void PolarDecomposition( const MatrixType & rF, MatrixType & rR, MatrixType & rU);

   //***************************************************************************************
   //***************************************************************************************
   // Compute Elasto Plastic Matrix
   void ComputeElastoPlasticTangentMatrix(ModelDataType &rValues, PlasticDataType &rVariables, Matrix &rEPMatrix) override;

   //***********************************************************************************
   //***********************************************************************************
   // Compute one step of the elasto-plastic problem
   void ComputeOneStepElastoPlasticProblem(ModelDataType &rValues, PlasticDataType &rVariables, const MatrixType &rDeltaDeformationMatrix) override;

   //********************************************************************
   //********************************************************************
   // UpdateInternalVariables
   virtual void UpdateInternalVariables(ModelDataType &rValues, PlasticDataType &rVariables, const MatrixType &rStressMatrix) override;

   //***************************************************************************************
   //***************************************************************************************
   // Correct Yield Surface Drift According to
   virtual void ReturnStressToYieldSurface(ModelDataType &rValues, PlasticDataType &rVariables) override;
   ///@}
   ///@name Protected  Access
   ///@{

   ///@}
   ///@name Protected Inquiry
   ///@{

   ///@}
   ///@name Protected LifeCycle
   ///@{

   ///@}

private:
   ///@name Static Member Variables
   ///@{

   ///@}
   ///@name Member Variables
   ///@{

   ///@}
   ///@name Private Operators
   ///@{

   ///@}
   ///@name Private Operations
   ///@{

   ///@}
   ///@name Private  Access
   ///@{

   ///@}
   ///@name Private Inquiry
   ///@{

   ///@}
   ///@name Serialization
   ///@{
   friend class Serializer;

   virtual void save(Serializer &rSerializer) const override
   {
      KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, DerivedType)
      rSerializer.save("BulkModulus", mBulkModulus);
      rSerializer.save("ShearModulus", mShearModulus);
   }

   virtual void load(Serializer &rSerializer) override
   {
      KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, DerivedType)
      rSerializer.load("BulkModulus", mBulkModulus);
      rSerializer.load("ShearModulus", mShearModulus);
   }

   ///@}
   ///@name Un accessible methods
   ///@{

   ///@}

}; // Class FabricBaseSoilModel

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_FABRIC_BASE_SOIL_MODEL_HPP_INCLUDED  defined
