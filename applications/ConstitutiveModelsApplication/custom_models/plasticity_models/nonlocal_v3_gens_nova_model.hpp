//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:     LMonforte-MCiantia $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:                   July 2018 $
//
//

#if !defined(KRATOS_NONLOCAL_V3_GENS_NOVA_MODEL_HPP_INCLUDED)
#define KRATOS_NONLOCAL_V3_GENS_NOVA_MODEL_HPP_INCLUDED

// System includes

// External includes
#include <iostream>
#include <fstream>

// Project includes
#include "custom_models/plasticity_models/structured_non_associative_soil_model.hpp"
#include "custom_models/plasticity_models/hardening_rules/gens_nova_hardening_rule.hpp"
#include "custom_models/plasticity_models/yield_surfaces/gens_nova_non_associative_yield_surface.hpp"
#include "custom_models/plasticity_models/yield_surfaces/plastic_potential/gens_nova_plastic_potential.hpp"
#include "custom_models/elasticity_models/borja_model.hpp"
#include "custom_models/elasticity_models/tamagnini_model.hpp"

//***** the hardening law associated to this Model has ... variables
// 0. Plastic multiplier
// 1. Plastic Volumetric deformation
// 2. Plastic Deviatoric deformation
// 3. ps (mechanical)
// 4. pt (ageing)
// 5. pcSTAR = ps + (1+k) p_t
// 6. Plastic Volumetric deformation Absolut Value
// 7. NonLocal Plastic Vol Def
// 8. NonLocal Plastic Dev Def
// 9. NonLocal Plastic Vol Def ABS
// ... (the number now is then..., xD)

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
    */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) NonlocalV3GensNovaModel : public StructuredNonAssociativeSoilModel<TamagniniModel, GensNovaNonAssociativeYieldSurface<GensNovaHardeningRule>>
{
public:
   ///@name Type Definitions
   ///@{

   //elasticity model
   //typedef BorjaModel                                     ElasticityModelType;
   typedef TamagniniModel ElasticityModelType;
   typedef ElasticityModelType::Pointer ElasticityModelPointer;

   //yield surface
   typedef GensNovaHardeningRule HardeningRuleType;
   typedef GensNovaNonAssociativeYieldSurface<HardeningRuleType> YieldSurfaceType;
   typedef YieldSurfaceType::Pointer YieldSurfacePointer;

   //base type
   typedef StructuredNonAssociativeSoilModel<ElasticityModelType, YieldSurfaceType> BaseType;

   //common types
   typedef BaseType::Pointer BaseTypePointer;
   typedef BaseType::SizeType SizeType;
   typedef BaseType::VoigtIndexType VoigtIndexType;
   typedef BaseType::MatrixType MatrixType;
   typedef BaseType::ModelDataType ModelDataType;
   typedef BaseType::MaterialDataType MaterialDataType;
   typedef BaseType::PlasticDataType PlasticDataType;
   typedef BaseType::InternalVariablesType InternalVariablesType;

   /// Pointer definition of NonlocalV3GensNovaModel
   KRATOS_CLASS_POINTER_DEFINITION(NonlocalV3GensNovaModel);

   ///@}
   ///@name Life Cycle
   ///@{

   /// Default constructor.
   NonlocalV3GensNovaModel() : BaseType()
   {
      GensNovaPlasticPotential<GensNovaHardeningRule> Object;
      YieldSurface<GensNovaHardeningRule>::Pointer pPlasticPotential = Object.Clone();
      mYieldSurface = GensNovaNonAssociativeYieldSurface<GensNovaHardeningRule>(pPlasticPotential);
   }

   /// Copy constructor.
   NonlocalV3GensNovaModel(NonlocalV3GensNovaModel const &rOther) : BaseType(rOther) {}

   /// Assignment operator.
   NonlocalV3GensNovaModel &operator=(NonlocalV3GensNovaModel const &rOther)
   {
      BaseType::operator=(rOther);
      return *this;
   }

   /// Clone.
   ConstitutiveModel::Pointer Clone() const override
   {
      return (NonlocalV3GensNovaModel::Pointer(new NonlocalV3GensNovaModel(*this)));
   }

   /// Destructor.
   ~NonlocalV3GensNovaModel() override {}

   ///@}
   ///@name Operators
   ///@{

   ///@}
   ///@name Operations
   ///@{

   // Calculate Stress and constitutive tensor
   void CalculateStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix) override
   {
      KRATOS_TRY

      { // modify the internal variables to make it nonlocal
      }

      double LocalPlasticVolStrain = mInternal(1);
      double NonLocalPlasticVolStrain = mInternal(7);
      mInternal(1) = mInternal(7);

      double LocalPlasticDevStrain = mInternal(2);
      double NonLocalPlasticDevStrain = mInternal(8);
      mInternal(2) = mInternal(8);

      double LocalPlasticVolStrainAbs = mInternal(6);
      double NonLocalPlasticVolStrainAbs = mInternal(9);
      mInternal(6) = mInternal(9);

      // integrate "analytically" ps and pt from plastic variables. Then update the internal variables.

      PlasticDataType Variables;
      this->InitializeVariables(rValues, Variables);

      const ModelDataType &rModelData = Variables.GetModelData();
      const Properties &rMaterialProperties = rModelData.GetProperties();

      const double &rPs0 = rMaterialProperties[PS];
      const double &rPt0 = rMaterialProperties[PT];
      const double &rChis = rMaterialProperties[CHIS];
      const double &rChit = rMaterialProperties[CHIT];
      const double &rhos = rMaterialProperties[RHOS];
      const double &rhot = rMaterialProperties[RHOT];
      const double &k = rMaterialProperties[KSIM];

      const double &rPlasticVolDef = Variables.Data.Internal(1);
      const double &rPlasticDevDef = Variables.Data.Internal(2);
      const double &rPlasticVolDefAbs = Variables.Data.Internal(6);

      double sq2_3 = sqrt(2.0 / 3.0);

      double ps;
      ps = rPlasticVolDef + sq2_3 * rChis * rPlasticDevDef;
      ps = (-rPs0) * std::exp(-rhos * ps);

      double pt;
      pt = rPlasticVolDefAbs + sq2_3 * rChit * rPlasticDevDef;
      pt = (-rPt0) * std::exp(rhot * pt);

      double pm;
      pm = ps + (1.0 + k) * pt;

      mInternal(3) = ps;
      mInternal(4) = pt;
      mInternal(5) = pm;

      StructuredNonAssociativeSoilModel::CalculateStressAndConstitutiveTensors(rValues, rStressMatrix, rConstitutiveMatrix);

      if (rValues.State.Is(ConstitutiveModelData::UPDATE_INTERNAL_VARIABLES))
      {

         mInternal(7,1) = mInternal(7);
         mInternal(8,1) = mInternal(8);
         mInternal(9,1) = mInternal(9);

         mInternal(7) = mInternal(1);
         mInternal(8) = mInternal(2);
         mInternal(9) = mInternal(6);

         mInternal(1) = LocalPlasticVolStrain + (mInternal(1) - NonLocalPlasticVolStrain);
         mInternal(2) = LocalPlasticDevStrain + (mInternal(2) - NonLocalPlasticDevStrain);
         mInternal(6) = LocalPlasticVolStrainAbs + (mInternal(6) - NonLocalPlasticVolStrainAbs);
      }
      else
      {
         mInternal(1) = LocalPlasticVolStrain;
         mInternal(2) = LocalPlasticDevStrain;
         mInternal(6) = LocalPlasticVolStrainAbs;
      }

      KRATOS_CATCH("")
   }
   ///@}
   ///@name Access
   ///@{

   ///@}
   ///@name Inquiry
   ///@{

   ///@}
   ///@name Input and output
   ///@{

   /// Turn back information as a string.
   std::string Info() const override
   {
      std::stringstream buffer;
      buffer << "NonlocalV3GensNovaModel";
      return buffer.str();
   }

   /// Print information about this object.
   void PrintInfo(std::ostream &rOStream) const override
   {
      rOStream << "NonlocalV3GensNovaModel";
   }

   /// Print object's data.
   void PrintData(std::ostream &rOStream) const override
   {
      rOStream << "NonlocalV3GensNovaModel Data";
   }

   ///@}
   ///@name Friends
   ///@{

   ///@}

protected:
   ///@name Protected static Member Variables
   ///@{

   ///@}
   ///@name Protected member Variables
   ///@{

   ///@}
   ///@name Protected Operators
   ///@{

   ///@}
   ///@name Protected Operations
   ///@{

   ///@}
   ///@name Protected  Access
   ///@{

   ///@}
   ///@name Protected Inquiry
   ///@{

   ///@}
   ///@name Protected LifeCycle
   ///@{

   ///@}

private:
   ///@name Static Member Variables
   ///@{

   ///@}
   ///@name Member Variables
   ///@{

   ///@}
   ///@name Private Operators
   ///@{

   ///@}
   ///@name Private Operations
   ///@{

   ///@}
   ///@name Private  Access
   ///@{

   ///@}
   ///@name Private Inquiry
   ///@{

   ///@}
   ///@name Serialization
   ///@{
   friend class Serializer;

   virtual void save(Serializer &rSerializer) const override
   {
      KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BaseType)
   }

   virtual void load(Serializer &rSerializer) override
   {
      KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BaseType)
   }

   ///@}
   ///@name Un accessible methods
   ///@{

   ///@}

}; // Class NonlocalV3GensNovaModel

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_NONLOCAL_V3_GENS_NOVA_MODEL_HPP_INCLUDED  defined
