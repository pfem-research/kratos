//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:               December 2021 $
//
//

#if !defined(KRATOS_UNSATURATED_CASM_BASE_MODEL_HPP_INCLUDED)
#define      KRATOS_UNSATURATED_CASM_BASE_MODEL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/plasticity_models/soil_base_model.hpp"


//***** the hardening law associated to this Model has ... variables
// Variables
// 0 Plastic Multiplier
// 1 Volumetric Plastic Strain
// 2 Dev Plastic Strain
// 3 NonLocal Plastic VOlumetric Strain 
// 4  
// 5 pc preconsolidation
// 6 pcSat Saturated Preconsolidation

// 7 Plastic Multiplier Mechanical part
// 8 Plastic Multiplier Suction part

// 10 Hencky Elastic plastic strain
// 11 nonlocal hencky elastic plastic strain

// Implementation of CASM for partially saturated soils considering the BBM LC
// Constitutive variables: 1. Bishop's Stress 2. Suction

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
 */
  template <class TElasticityModel, class TYieldSurface>
  class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) UnsaturatedCasmBaseSoilModel : public SoilBaseModel<TElasticityModel, TYieldSurface>
  {
  public:
    ///@name Type Definitions
    ///@{

    //elasticity model
    typedef TElasticityModel ElasticityModelType;

    //yield surface
    typedef TYieldSurface YieldSurfaceType;

    //base type
    typedef SoilBaseModel<ElasticityModelType, YieldSurfaceType> BaseType;

    //common types
    typedef typename BaseType::Pointer BaseTypePointer;
    typedef typename BaseType::SizeType SizeType;
    typedef typename BaseType::VoigtIndexType VoigtIndexType;
    typedef typename BaseType::MatrixType MatrixType;
    typedef typename BaseType::VectorType VectorType;
    typedef typename BaseType::ModelDataType ModelDataType;
    typedef typename BaseType::MaterialDataType MaterialDataType;
    typedef typename BaseType::PlasticDataType PlasticDataType;
    typedef typename BaseType::InternalVariablesType InternalVariablesType;
    typedef typename BaseType::InternalDataType InternalDataType;

    typedef typename BaseType::NameType NameType;

    typedef ConstitutiveModelData::StrainMeasureType StrainMeasureType;
    typedef ConstitutiveModelData::StressMeasureType StressMeasureType;

    /// Pointer definition of UnsaturatedCasmBaseSoilModel
    KRATOS_CLASS_POINTER_DEFINITION(UnsaturatedCasmBaseSoilModel);
    ///@}
    ///@name Life Cycle
    ///@{

    /// Default constructor.
    UnsaturatedCasmBaseSoilModel() : BaseType() { }

    /// Copy constructor.
    UnsaturatedCasmBaseSoilModel(UnsaturatedCasmBaseSoilModel const &rOther) : BaseType(rOther), 
   mShearModulus(rOther.mShearModulus), mBulkModulus(rOther.mBulkModulus) 
    {
    }

    /// Assignment operator.
    UnsaturatedCasmBaseSoilModel &operator=(UnsaturatedCasmBaseSoilModel const &rOther)
    {
      BaseType::operator=(rOther);

      this->mShearModulus = rOther.mShearModulus;
      this->mBulkModulus = rOther.mBulkModulus;
      
      return *this;
    }

    /// Clone.
    ConstitutiveModel::Pointer Clone() const override
    {
      return Kratos::make_shared<UnsaturatedCasmBaseSoilModel>(*this);
    }

    /// Destructor.
    ~UnsaturatedCasmBaseSoilModel() override {}

    ///@}
    ///@name Operators
    ///@{

    ///@}
    ///@name Operations
    ///@{


    void InitializeModel(ModelDataType & rValues) override;

    /**
     * Get Values
     */
    virtual double &GetValue(const Variable<double> &rVariable, double &rValue) override;

    virtual array_1d<double, 3>& GetValue(const Variable< array_1d<double, 3> >& rVariable, array_1d<double, 3>& rValue) override;

    Vector &GetValue(const Variable<Vector> &rVariable, Vector &rValue) override;

    Matrix &GetValue(const Variable<Matrix> &rVariable, Matrix &rValue) override;

    /**
     * Set Values
     */
    void SetValue(const Variable<double> &rVariable,
                  const double &rValue,
                  const ProcessInfo &rCurrentProcessInfo) override;

    /**
     * Set Values
     */
    void SetValue(const Variable<Vector> &rVariable,
                  const Vector &rValue,
                  const ProcessInfo &rCurrentProcessInfo) override;
    /**
     * Calculate Stresses
     */



    //*******************************************************************************
    //*******************************************************************************
    // Calculate Stress and constitutive tensor
    void CalculateStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix) override;


    void CalculateStressTensor(ModelDataType &rValues, MatrixType & rStressMatrix) override;

    ///@}
    ///@name Access
    ///@{

    ///@}
    ///@name Inquiry
    ///@{

    ///@}
    ///@name Input and output
    ///@{

    /// Turn back information as a string.
    std::string Info() const override
    {
      std::stringstream buffer;
      buffer << "UnsaturatedCasmBaseSoilModel";
      return buffer.str();
    }

    /// Print information about this object.
    void PrintInfo(std::ostream &rOStream) const override
    {
      rOStream << "UnsaturatedCasmBaseSoilModel";
    }

    /// Print object's data.
    void PrintData(std::ostream &rOStream) const override
    {
      rOStream << "UnsaturatedCasmBaseSoilModel Data";
    }

    ///@}
    ///@name Friends
    ///@{

    ///@}

  protected:
    ///@name Protected static Member Variables
    ///@{

    ///@}
    ///@name Protected member Variables
    ///@{


   double mShearModulus;
   double mBulkModulus;

    ///@}
    ///@name Protected Operators
    ///@{

    ///@}
    ///@name Protected Operations
    ///@{


    //***************************************************************************************
    //***************************************************************************************
    // Correct Yield Surface Drift According to
    virtual void ReturnStressToYieldSurface(ModelDataType &rValues, PlasticDataType &rVariables) override;

    //***************************************************************************************
    //***************************************************************************************
    // Compute Elasto Plastic Matrix and Vector ( derivative of stresses respect suctions)
    virtual void ComputeElastoPlasticTangentMatrixAndVector(ModelDataType &rValues, PlasticDataType &rVariables, Matrix &rEPMatrix, Vector & rEPVector);


    //***************************************************************************************
    //***************************************************************************************
    // Advance the solution first in elastic regime and then in elastoplastic
    virtual void ComputeSolutionWithChange(ModelDataType &rValues, PlasticDataType &rVariables, const MatrixType &rDeltaDeformationMatrix) override;


    //***********************************************************************************
    //***********************************************************************************
    // Compute the elasto-plastic problem
    void ComputeSubsteppingElastoPlasticProblem(ModelDataType &rValues, PlasticDataType &rVariables, const MatrixType &rDeltaDeformationMatrix, const double & rInitialSuction, const double & rFinalSuction);


    //***********************************************************************************
    //***********************************************************************************
    // Compute one elasto-plastic problem with two discretizations and then compute some
    // sort of error measure
    double ComputeElastoPlasticProblem(ModelDataType &rValues, PlasticDataType &rVariables, const MatrixType &rSubstepDeformationGradient, const double & rInitialSuction, const double & rDeltaSuction);

    //***********************************************************************************
    //***********************************************************************************
    // Compute one step of the elasto-plastic problem
    void ComputeOneStepElastoPlasticProblemUnsat(ModelDataType &rValues, PlasticDataType &rVariables, const MatrixType &rDeltaDeformationMatrix, const double & rInitialSuction, const double & rDeltaSuction);

    Matrix & SetConstitutiveMatrixToTheAppropriateSizeUnsat(Matrix &rConstitutiveMatrix, Matrix &rConstMatrixBig, Vector & rConstVector, const MatrixType &rStressMatrix);


    /**
     * Calculate Stresses
     */
    virtual void SetWorkingMeasures(PlasticDataType &rVariables, MatrixType &rStressMatrix) override
    {
      KRATOS_TRY

        const ModelDataType &rModelData = rVariables.GetModelData();

      //working stress is Kirchhoff by default : transform stresses is working stress is PK2
      const StressMeasureType &rStressMeasure = rModelData.GetStressMeasure();
      const StrainMeasureType &rStrainMeasure = rModelData.GetStrainMeasure();

      if (rStressMeasure == ConstitutiveModelData::StressMeasureType::StressMeasure_PK2)
      {
        KRATOS_ERROR << "calling initialize PlasticityModel .. StrainMeasure provided is inconsistent" << std::endl;
      }
      else if (rStressMeasure == ConstitutiveModelData::StressMeasureType::StressMeasure_Kirchhoff)
      {

        if (rStrainMeasure == ConstitutiveModelData::StrainMeasureType::StrainMeasure_Left_CauchyGreen)
        {
          rVariables.Data.StrainMatrix = IdentityMatrix(3);
        }
        else
        {
          KRATOS_ERROR << "calling initialize PlasticityModel .. StrainMeasure provided is inconsistent" << std::endl;
        }
      }
      else
      {
        KRATOS_ERROR << "calling initialize PlasticityModel .. StressMeasure provided is inconsistent" << std::endl;
      }

      KRATOS_CATCH(" ")
    }

    //********************************************************************
    //********************************************************************
    // Initialize Plastic Variables ( a copy from elsewhere:'P)
    void InitializeVariables(ModelDataType &rValues, PlasticDataType &rVariables) override;

    //********************************************************************
    //********************************************************************
    // UpdateInternalVariables
    virtual void UpdateInternalVariables(ModelDataType &rValues, PlasticDataType &rVariables, const MatrixType &rStressMatrix) override
    {
      KRATOS_TRY

      this->mInternal.Update(rVariables.Data.Internal);
      
      
      const ModelDataType &rModelData = rVariables.GetModelData();
      const Properties &rMaterialProperties = rModelData.GetProperties();

      const double &rSwellingSlope = rMaterialProperties[SWELLING_SLOPE];
      const double &rAlpha = rMaterialProperties[ALPHA_SHEAR];

      double MeanStress = 0;
      for (unsigned int i = 0; i < 3; i++)
         MeanStress += rValues.StressMatrix(i, i) / 3.0;
      
      double K = (-MeanStress) / rSwellingSlope;
      double G = rMaterialProperties[INITIAL_SHEAR_MODULUS];
      
      G += rAlpha * (-MeanStress);
      mBulkModulus = K;
      mShearModulus = G;

      KRATOS_CATCH("")

     }

    // ****************************************************************************
    //  compute the stress state by using implex
    virtual void  CalculateImplexPlasticStepUnsat(ModelDataType &rValues, PlasticDataType &rVariables, MatrixType &rStressMatrix, const MatrixType &rDeltaDeformationMatrix, Vector & rConstitutiveVector);


    double & EvaluateLoadingCollapse( PlasticDataType & rVariables, double & rPC, const double & rPCSat, const double & rSuction);

    ///@}
    ///@name Protected  Access
    ///@{

    ///@}
    ///@name Protected Inquiry
    ///@{

    ///@}
    ///@name Protected LifeCycle
    ///@{

    ///@}

  private:
    ///@name Static Member Variables
    ///@{

    ///@}
    ///@name Member Variables
    ///@{

    ///@}
    ///@name Private Operators
    ///@{

    ///@}
    ///@name Private Operations
    ///@{

    ///@}
    ///@name Private  Access
    ///@{

    ///@}
    ///@name Serialization
    ///@{
    friend class Serializer;

    void save(Serializer &rSerializer) const override
    {
      KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BaseType)
      rSerializer.save("BulkModulus", mBulkModulus);
      rSerializer.save("ShearModulus", mShearModulus);
    }

    void load(Serializer &rSerializer) override
    {
      KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BaseType)
      rSerializer.load("BulkModulus", mBulkModulus);
      rSerializer.load("ShearModulus", mShearModulus);
    }

    ///@}
    ///@name Private Inquiry
    ///@{

    ///@}
    ///@name Un accessible methods
    ///@{

    ///@}

  }; // Class UnsaturatedCasmBaseSoilModel

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos

#endif // KRATOS_UNSATURATED_CASM_BASE_MODEL_HPP_INCLUDED
