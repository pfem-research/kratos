//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                  April 2017 $
//
//

// System includes

// External includes

// Project includes
#include "custom_models/constitutive_model.hpp"

namespace Kratos
{

KRATOS_CREATE_LOCAL_FLAG(ConstitutiveModel, ADD_HISTORY_VECTOR, 0);
KRATOS_CREATE_LOCAL_FLAG(ConstitutiveModel, HISTORY_STRAIN_MEASURE, 1);
KRATOS_CREATE_LOCAL_FLAG(ConstitutiveModel, HISTORY_STRESS_MEASURE, 2);

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

ConstitutiveModel::ConstitutiveModel()
    : mpHistoryVector(new VectorType(ZeroVector(6)))
{
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

ConstitutiveModel::ConstitutiveModel(const ConstitutiveModel &rOther)
    : mOptions(rOther.mOptions)
{
  mpHistoryVector = new VectorType(*rOther.mpHistoryVector);
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

ConstitutiveModel &ConstitutiveModel::operator=(const ConstitutiveModel &rOther)
{
  mOptions = rOther.mOptions;
  mpHistoryVector = new VectorType(*rOther.mpHistoryVector);
  return *this;
}

//********************************CLONE***********************************************
//************************************************************************************

ConstitutiveModel::Pointer ConstitutiveModel::Clone() const
{
  return Kratos::make_shared<ConstitutiveModel>(*this);
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

ConstitutiveModel::~ConstitutiveModel()
{
  if (mpHistoryVector != nullptr)
    delete mpHistoryVector;
}

//***********************HAS : DOUBLE - VECTOR - MATRIX*******************************
//************************************************************************************

bool ConstitutiveModel::Has(const Variable<double> &rVariable)
{
  return false;
}

bool ConstitutiveModel::Has(const Variable<Vector> &rVariable)
{
  return false;
}

bool ConstitutiveModel::Has(const Variable<array_1d<double,3>> &rVariable)
{
  return false;
}

bool ConstitutiveModel::Has(const Variable<Matrix> &rVariable)
{
  return false;
}

//***********************SET VALUE: DOUBLE - VECTOR - MATRIX**************************
//************************************************************************************
void ConstitutiveModel::SetValue(const Variable<double> &rVariable, const double &rValue,
                                 const ProcessInfo &rCurrentProcessInfo)

{
  KRATOS_TRY

  KRATOS_CATCH(" ")
}

void ConstitutiveModel::SetValue(const Variable<array_1d<double,3>> &rVariable, const array_1d<double,3> &rValue,
                                 const ProcessInfo &rCurrentProcessInfo)

{
  KRATOS_TRY

  KRATOS_CATCH(" ")
}

void ConstitutiveModel::SetValue(const Variable<Vector> &rVariable, const Vector &rValue,
                                 const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  if(rVariable == INITIAL_STRESS_VECTOR || rVariable == INITIAL_STRAIN_VECTOR || rVariable == INTERNAL_STRENGTH_VECTOR)
    *this->mpHistoryVector = rValue;

  KRATOS_CATCH(" ")
}

void ConstitutiveModel::SetValue(const Variable<Matrix> &rVariable, const Matrix &rValue,
                                 const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  KRATOS_CATCH(" ")
}

//***********************GET VALUE: DOUBLE - VECTOR - MATRIX**************************
//************************************************************************************

double &ConstitutiveModel::GetValue(const Variable<double> &rVariable, double &rValue)
{
  KRATOS_TRY

  rValue = 0;

  return rValue;

  KRATOS_CATCH(" ")
}

array_1d<double, 3> &ConstitutiveModel::GetValue(const Variable<array_1d<double, 3> > &rVariable,
                                                 array_1d<double, 3> &rValue)
{
  KRATOS_TRY

  return rValue;

  KRATOS_CATCH(" ")
}

Vector &ConstitutiveModel::GetValue(const Variable<Vector> &rVariable, Vector &rValue)
{
  KRATOS_TRY

  if(rVariable == INITIAL_STRESS_VECTOR || rVariable == INITIAL_STRAIN_VECTOR || rVariable == INTERNAL_STRENGTH_VECTOR)
    rValue = *this->mpHistoryVector;

  return rValue;

  KRATOS_CATCH(" ")
}

Matrix &ConstitutiveModel::GetValue(const Variable<Matrix> &rVariable, Matrix &rValue)
{
  KRATOS_TRY

  return rValue;

  KRATOS_CATCH(" ")
}


void ConstitutiveModel::GetDomainVariablesList(std::vector<Variable<double>*> &rScalarVariables,
                                               std::vector<Variable<array_1d<double, 3>>*> &rComponentVariables)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling the Constitutive Model base class Variables List... illegal operation" << std::endl;

  KRATOS_CATCH(" ")
}

void ConstitutiveModel::GetStrainMeasures(std::vector<ConstitutiveModelData::StrainMeasureType> &rStrainMeasures)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling the Constitutive Model base class to get the compatible strain Measures... illegal operation" << std::endl;

  KRATOS_CATCH(" ")
}


//************* STARTING - ENDING  METHODS
//************************************************************************************
//************************************************************************************

void ConstitutiveModel::InitializeMaterial(const Properties &rProperties)
{
  KRATOS_TRY

  //KRATOS_ERROR << "calling ConstitutiveModel Initialize base class " << std::endl;

  KRATOS_CATCH(" ")
}

void ConstitutiveModel::InitializeModel(ModelDataType &rValues)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling ConstitutiveModel Initialize base class " << std::endl;

  KRATOS_CATCH(" ")
}

void ConstitutiveModel::FinalizeModel(ModelDataType &rValues)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling ConstitutiveModel Finalize base class " << std::endl;

  KRATOS_CATCH(" ")
}

//************* COMPUTING  METHODS
//************************************************************************************
//************************************************************************************

/**
 *  Calculate Material properties and parameters
 */
void ConstitutiveModel::CalculateMaterialParameters(ModelDataType &rValues)
{
  KRATOS_TRY

  ConstitutiveModelData::CalculateSolidMaterialParameters(rValues);

  KRATOS_CATCH(" ")
}


/**
   * Calculate Strain Energy Density Functions
   */
void ConstitutiveModel::CalculateStrainEnergy(ModelDataType &rValues, double &rDensityFunction)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling ConstitutiveModel base class " << std::endl;

  KRATOS_CATCH(" ")
}

/**
   * Calculate Stresses
   */
void ConstitutiveModel::CalculateStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling ConstitutiveModel base class " << std::endl;

  KRATOS_CATCH(" ")
}

void ConstitutiveModel::CalculateIsochoricStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling ConstitutiveModel base class " << std::endl;

  KRATOS_CATCH(" ")
}

void ConstitutiveModel::CalculateVolumetricStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling ConstitutiveModel base class " << std::endl;

  KRATOS_CATCH(" ")
}

/**
   * Calculate Constitutive Tensor
   */
void ConstitutiveModel::CalculateConstitutiveTensor(ModelDataType &rValues, Matrix &rConstitutive)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling ConstitutiveModel base class " << std::endl;

  KRATOS_CATCH(" ")
}

void ConstitutiveModel::CalculateIsochoricConstitutiveTensor(ModelDataType &rValues, Matrix &rConstitutive)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling ConstitutiveModel base class " << std::endl;

  KRATOS_CATCH(" ")
}

void ConstitutiveModel::CalculateVolumetricConstitutiveTensor(ModelDataType &rValues, Matrix &rConstitutive)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling ConstitutiveModel base class " << std::endl;

  KRATOS_CATCH(" ")
}

/**
   * Calculate Stress and Constitutive Tensor
   */
void ConstitutiveModel::CalculateStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutive)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling ConstitutiveModel base class " << std::endl;

  KRATOS_CATCH(" ")
}

void ConstitutiveModel::CalculateIsochoricStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutive)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling ConstitutiveModel base class " << std::endl;

  KRATOS_CATCH(" ")
}

void ConstitutiveModel::CalculateVolumetricStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutive)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling ConstitutiveModel base class " << std::endl;

  KRATOS_CATCH(" ")
}

void ConstitutiveModel::CalculateInternalVariables(ModelDataType &rValues)
{
  KRATOS_TRY

  //KRATOS_ERROR << "calling ConstitutiveModel base class " << std::endl;

  KRATOS_CATCH(" ")
}

int ConstitutiveModel::Check(const Properties &rProperties,
                             const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling ConstitutiveModel base class " << std::endl;

  return 0;

  KRATOS_CATCH(" ")
}

void ConstitutiveModel::GetRequiredProperties(Properties &rProperties)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling ConstitutiveModel base class to get model requiered properties" << std::endl;

  KRATOS_CATCH(" ")
}

/**
   * Initialize variables for calculations
   */
void ConstitutiveModel::InitializeVariables(ModelDataType &rValues, DataType &rVariables)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling ConstitutiveModel base class " << std::endl;

  KRATOS_CATCH(" ")
}

/**
   * Calculate Stresses
   */
void ConstitutiveModel::CalculateAndAddStressTensor(DataType &rVariables, MatrixType &rStressMatrix)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling ConstitutiveModel base class " << std::endl;

  KRATOS_CATCH(" ")
}

void ConstitutiveModel::CalculateAndAddIsochoricStressTensor(DataType &rVariables, MatrixType &rStressMatrix)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling ConstitutiveModel base class " << std::endl;

  KRATOS_CATCH(" ")
}

void ConstitutiveModel::CalculateAndAddVolumetricStressTensor(DataType &rVariables, MatrixType &rStressMatrix)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling ConstitutiveModel base class " << std::endl;

  KRATOS_CATCH(" ")
}

/**
   * Calculate Constitutive Tensor
   */
void ConstitutiveModel::CalculateAndAddConstitutiveTensor(DataType &rVariables, Matrix &rConstitutive)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling ConstitutiveModel base class " << std::endl;

  KRATOS_CATCH(" ")
}

void ConstitutiveModel::CalculateAndAddIsochoricConstitutiveTensor(DataType &rVariables, Matrix &rConstitutive)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling ConstitutiveModel base class " << std::endl;

  KRATOS_CATCH(" ")
}

void ConstitutiveModel::CalculateAndAddVolumetricConstitutiveTensor(DataType &rVariables, Matrix &rConstitutive)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling ConstitutiveModel base class " << std::endl;

  KRATOS_CATCH(" ")
}


} // Namespace Kratos
