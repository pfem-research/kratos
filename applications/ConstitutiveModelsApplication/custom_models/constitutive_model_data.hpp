//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                  April 2017 $
//
//

#if !defined(KRATOS_CONSTITUTIVE_MODEL_DATA_HPP_INCLUDED)
#define KRATOS_CONSTITUTIVE_MODEL_DATA_HPP_INCLUDED

// System includes
#include <string>
#include <iostream>

// External includes

// Project includes
#include "includes/define.h"
#include "includes/serializer.h"
#include "includes/properties.h"

#include "constitutive_models_application_variables.h"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) ConstitutiveModelData
{
public:
  ///@name Type Definitions
  ///@{
  using VoigtIndexType = const unsigned int (*)[2];
  using SizeType = std::size_t;
  using MatrixType = BoundedMatrix<double, 3, 3>;
  using VectorType = array_1d<double, 6>;

  //state flags
  KRATOS_DEFINE_LOCAL_FLAG(IMPLEX_ACTIVE);
  KRATOS_DEFINE_LOCAL_FLAG(STRAIN_COMPUTED);
  KRATOS_DEFINE_LOCAL_FLAG(STRESS_COMPUTED);
  KRATOS_DEFINE_LOCAL_FLAG(CONSTITUTIVE_MATRIX_COMPUTED);
  KRATOS_DEFINE_LOCAL_FLAG(PLASTIC_REGION);
  KRATOS_DEFINE_LOCAL_FLAG(PLASTIC_RATE_REGION);
  KRATOS_DEFINE_LOCAL_FLAG(RETURN_MAPPING_COMPUTED);
  KRATOS_DEFINE_LOCAL_FLAG(UPDATE_INTERNAL_VARIABLES);

  /**
   * Defines Strain Measure types for the constitutive models
   */
  enum class StrainMeasureType //supplied cauchy green strain measure
  {
    StrainMeasure_Infinitesimal,   //strain measure small displacements
    StrainMeasure_GreenLagrange,   //strain measure reference configuration
    StrainMeasure_Almansi,         //strain measure current configuration

    // True strain:
    StrainMeasure_Hencky_Material, //strain measure reference configuration
    StrainMeasure_Hencky_Spatial,  //strain measure current   configuration

    // Deformation measures:
    StrainMeasure_Deformation_Gradient, //material deformation gradient as a strain mea
    StrainMeasure_Right_CauchyGreen,    //right cauchy-green tensor as a strain measure
    StrainMeasure_Left_CauchyGreen,     //left  cauchy-green tensor as a strain measure
    StrainMeasure_Velocity_Gradient,    //spatial velocity gradient as a strain measure

    // No strain measure supplied
    StrainMeasure_None  //no strain measure supplied
  };


  /**
   * Defines Stress Measure types for the constitutive models
   */
  enum class StressMeasureType //required stress measure
  {
    StressMeasure_PK1,       //stress related to reference configuration non-symmetric
    StressMeasure_PK2,       //stress related to reference configuration
    StressMeasure_Kirchhoff, //stress related to current   configuration
    StressMeasure_Cauchy     //stress related to current   configuration
  };

  /**
   * Defines Specific material properties for the constitutive models
   */
  struct MaterialData
  {
   private:

    //custom material properties container
    DataValueContainer mData;

    const Properties *mpProperties;
    PropertiesLayout::Pointer mpPropertiesLayout;

   public:

    //general elastic material properties
    // double PoissonCoefficient;
    // double YoungModulus;
    // double LameMu;
    // double LameMuBar;
    // double LameLambda;
    // double BulkModulus;
    // double Density;

    //Set Data
    void SetPoissonCoefficient(const double &rV)  { mData.SetValue(POISSON_RATIO, rV); };
    void SetYoungModulus(const double &rV)  { mData.SetValue(YOUNG_MODULUS, rV); };
    void SetLameMu(const double &rV)  { mData.SetValue(LAME_MU, rV); };
    void SetLameMuBar(const double &rV)  { mData.SetValue(LAME_MU_BAR, rV); };
    void SetLameLambda(const double &rV)  { mData.SetValue(LAME_LAMBDA, rV); };
    void SetBulkModulus(const double &rV)  { mData.SetValue(BULK_MODULUS, rV); };
    void SetDensity(const double &rV)  { mData.SetValue(DENSITY, rV); };
    void SetModelParameters(const Vector &rV)  { mData.SetValue(MATERIAL_PARAMETERS, rV); };

    template<class TVariableType>
    void SetValue(TVariableType const& rV, typename TVariableType::Type const& rValue)
    {
        mData.SetValue(rV, rValue);
    }

    //Get const Data
    const double &GetPoissonCoefficient() const { return this->operator[](POISSON_RATIO); };
    const double &GetYoungModulus() const { return this->operator[](YOUNG_MODULUS); };
    const double &GetLameMu() const { return this->operator[](LAME_MU); };
    const double &GetLameMuBar() const { return this->operator[](LAME_MU_BAR); };
    const double &GetLameLambda() const { return this->operator[](LAME_LAMBDA); };
    const double &GetBulkModulus() const { return this->operator[](BULK_MODULUS); };
    const double &GetDensity() const { return this->operator[](DENSITY); };
    const Vector &GetModelParameters() const { return this->operator[](MATERIAL_PARAMETERS); };

    void SetProperties(const Properties &rProperties) { mpProperties = &rProperties; };
    void SetPropertiesLayout(PropertiesLayout::Pointer pPropertiesLayout) { mpPropertiesLayout = pPropertiesLayout; };

    const Properties &GetProperties() const { return *mpProperties; };
    const PropertiesLayout &GetPropertiesLayout() const { return *mpPropertiesLayout.get(); };
    //Has Data Pointers
    bool HasPropertiesLayout() const { return (!mpPropertiesLayout) ? false : true; };


    template<class TVariableType>
    typename TVariableType::Type& GetIfHasValue(const TVariableType& rV, typename TVariableType::Type &rValue)
    {
      if(mData.Has(rV))
        return mData.GetValue(rV);
      else if(mpProperties->Has(rV))
        return mpProperties->GetValue(rV);
      else
        return rValue;
    }

    template<class TVariableType>
    typename TVariableType::Type const& GetIfHasValue(const TVariableType& rV, typename TVariableType::Type & rValue) const
    {
      if(mData.Has(rV))
        return mData.GetValue(rV);
      else if(mpProperties->Has(rV))
        return mpProperties->GetValue(rV);
      else
        return rValue;
    }

    template<class TVariableType>
    typename TVariableType::Type& operator[](const TVariableType& rV)
    {
      if(mData.Has(rV))
        return mData.GetValue(rV);

      return mpProperties->GetValue(rV);
    }

    template<class TVariableType>
    typename TVariableType::Type const& operator[](const TVariableType& rV) const
    {
      if(mData.Has(rV))
        return mData.GetValue(rV);

      return mpProperties->GetValue(rV);
    }

  };

  /**
   * Defines Variable Template (with the Value for this variable) to be used in VariableValueData
   */
  template <class T>
  struct VariableValue
  {
  private:
    const Variable<T> *mpVariable;
    T *mpValue;

  public:
    //constructors
    VariableValue(){};
    VariableValue(const Variable<T> &rVariable, T &rValue)
    {
      mpVariable = &rVariable;
      mpValue = &rValue;
    };

    //Set Data Pointers
    void SetVariableValue(const Variable<T> &rVariable, T &rValue)
    {
      mpVariable = &rVariable;
      mpValue = &rValue;
    };
    void SetVariable(const Variable<T> &rVariable) { mpVariable = &rVariable; };

    //Get-Set Data
    bool Is(const Variable<T> &rVariable) const { return (rVariable == *mpVariable); };

    //Get Data
    const Variable<T>& GetVariable() const {return *mpVariable;};

    void SetValue(T &rValue) { *mpValue = rValue; };
  };

  /**
   * Defines Variables to recover when called in Calculate (kind of Tuple)
   */
  struct VariableValueData
  {
   private:
    enum class VarType
    {
      INTEGER,
      DOUBLE,
      VECTOR,
      MATRIX,
      ARRAY3,
      ARRAY6,
      NONE
    };

    VarType mType;
    VariableValue<int> *mpIntVariable;
    VariableValue<double> *mpDoubleVariable;
    VariableValue<Vector> *mpVectorVariable;
    VariableValue<Matrix> *mpMatrixVariable;
    VariableValue<array_1d<double, 3>> *mpArray3Variable;
    VariableValue<array_1d<double, 6>> *mpArray6Variable;

   public:
    //Constructor
    VariableValueData()
    {
      mType = VarType::NONE;
      mpIntVariable = nullptr;
      mpDoubleVariable = nullptr;
      mpVectorVariable = nullptr;
      mpMatrixVariable = nullptr;
      mpArray3Variable = nullptr;
      mpArray6Variable = nullptr;
    }

    // Destructor
    ~VariableValueData()
    {
      switch (mType)
      {
        case VarType::INTEGER:
          delete mpIntVariable;
          break;
        case VarType::DOUBLE:
          delete mpDoubleVariable;
          break;
        case VarType::VECTOR:
          delete mpVectorVariable;
          break;
        case VarType::MATRIX:
          delete mpMatrixVariable;
          break;
        case VarType::ARRAY3:
          delete mpArray3Variable;
          break;
        case VarType::ARRAY6:
          delete mpArray6Variable;
          break;
        case VarType::NONE:
          break;
        default:
          break;
      }
    }

    //Set Data
    void SetIntVariableValue(const Variable<int> &rVariable, int &rValue)
    {
      mType = VarType::INTEGER;
      typedef VariableValue<int> VariableValueType;
      mpIntVariable = new VariableValueType(rVariable, rValue);
    }

    void SetDoubleVariableValue(const Variable<double> &rVariable, double &rValue)
    {
      mType = VarType::DOUBLE;
      typedef VariableValue<double> VariableValueType;
      mpDoubleVariable = new VariableValueType(rVariable, rValue);

    }

    void SetVectorVariableValue(const Variable<Vector> &rVariable, Vector &rValue)
    {
      mType = VarType::VECTOR;
      typedef VariableValue<Vector> VariableValueType;
      mpVectorVariable = new VariableValueType(rVariable, rValue);
    }

    void SetMatrixVariableValue(const Variable<Matrix> &rVariable, Matrix &rValue)
    {
      mType = VarType::MATRIX;
      typedef VariableValue<Matrix> VariableValueType;
      mpMatrixVariable = new VariableValueType(rVariable, rValue);
    }

    void SetArray3VariableValue(const Variable<array_1d<double, 3>> &rVariable, array_1d<double, 3> &rValue)
    {
      mType = VarType::ARRAY3;
      typedef VariableValue<array_1d<double, 3>> VariableValueType;
      mpArray3Variable = new VariableValueType(rVariable, rValue);
    }

    void SetArray6VariableValue(const Variable<array_1d<double, 6>> &rVariable, array_1d<double, 6> &rValue)
    {
      mType = VarType::ARRAY6;
      typedef VariableValue<array_1d<double, 6>> VariableValueType;
      mpArray6Variable = new VariableValueType(rVariable, rValue);
    }

    //Get Data
    template <class T>
    bool GetVariableValue(VariableValue<T> &rVariableValue)
    {
      if (std::is_same<T, int>::value)
      {
        if (mpIntVariable != nullptr)
        {
          rVariableValue = &mpIntVariable;
          return true;
        }
        else
        {
          return false;
        }
      }
      else if (std::is_same<T, double>::value)
      {
        if (mpDoubleVariable != nullptr)
        {
          rVariableValue = &mpDoubleVariable;
          return true;
        }
        else
        {
          return false;
        }
      }
      else if (std::is_same<T, Vector>::value)
      {
        if (mpVectorVariable != nullptr)
        {
          rVariableValue = &mpVectorVariable;
          return true;
        }
        else
        {
          return false;
        }
      }
      else if (std::is_same<T, Matrix>::value)
      {
        if (mpMatrixVariable != nullptr)
        {
          rVariableValue = &mpMatrixVariable;
          return true;
        }
        else
        {
          return false;
        }
      }
      else if (std::is_same<T, array_1d<double, 3>>::value)
      {
        if (mpArray3Variable != nullptr)
        {
          rVariableValue = &mpArray3Variable;
          return true;
        }
        else
        {
          return false;
        }
      }
      else if (std::is_same<T, array_1d<double, 6>>::value)
      {
        if (mpArray6Variable != nullptr)
        {
          rVariableValue = &mpArray6Variable;
          return true;
        }
        else
        {
          return false;
        }
      }
      else
      {
        return false;
      }
    }

    //Has Variable
    bool Is(const Variable<int> &rVariable)
    {
      if (mpIntVariable != nullptr)
        if (mpIntVariable->Is(rVariable))
          return true;
      return false;
    }

    bool Is(const Variable<double> &rVariable)
    {
      if (mpDoubleVariable != nullptr)
        if (mpDoubleVariable->Is(rVariable))
          return true;
      return false;
    }

    bool Is(const Variable<Vector> &rVariable)
    {
      if (mpVectorVariable != nullptr)
        if (mpVectorVariable->Is(rVariable))
          return true;
      return false;
    }

    bool Is(const Variable<Matrix> &rVariable)
    {
      if (mpMatrixVariable != nullptr)
        if (mpMatrixVariable->Is(rVariable))
          return true;
      return false;
    }

    bool Is(const Variable<array_1d<double, 3>> &rVariable)
    {
      if (mpArray3Variable != nullptr)
        if (mpArray3Variable->Is(rVariable))
          return true;
      return false;
    }

    bool Is(const Variable<array_1d<double, 6>> &rVariable)
    {
      if (mpArray6Variable != nullptr)
        if (mpArray6Variable->Is(rVariable))
          return true;
      return false;
    }

    //Set Value
    void SetValue(const Variable<int> &rVariable, int &rValue)
    {
      if (mpIntVariable != nullptr)
        if (mpIntVariable->Is(rVariable))
          mpIntVariable->SetValue(rValue);
    }

    void SetValue(const Variable<double> &rVariable, double &rValue)
    {
      if (mpDoubleVariable != nullptr)
        if (mpDoubleVariable->Is(rVariable))
          mpDoubleVariable->SetValue(rValue);
    }

    void SetValue(const Variable<Vector> &rVariable, Vector &rValue)
    {
      if (mpVectorVariable != nullptr)
        if (mpVectorVariable->Is(rVariable))
          mpVectorVariable->SetValue(rValue);
    }

    void SetValue(const Variable<Matrix> &rVariable, Matrix &rValue)
    {
      if (mpMatrixVariable != nullptr)
        if (mpMatrixVariable->Is(rVariable))
          mpMatrixVariable->SetValue(rValue);
    }

    void SetValue(const Variable<array_1d<double, 3>> &rVariable, array_1d<double, 3> &rValue)
    {
      if (mpArray3Variable != nullptr)
        if (mpArray3Variable->Is(rVariable))
          mpArray3Variable->SetValue(rValue);
    }

    void SetValue(const Variable<array_1d<double, 6>> &rVariable, array_1d<double, 6> &rValue)
    {
      if (mpArray6Variable != nullptr)
        if (mpArray6Variable->Is(rVariable))
          mpArray6Variable->SetValue(rValue);
    }
  };

  /**
   * Defines Operation Data variables of the Constitutive Law class for the ModelData
   */
  struct ConstitutiveLawData
  {
  public:
    //elemental data
    double Pressure;
    double Temperature;
    double CharacteristicSize;
    double DeltaDeformationDet; //wildcard for the determinant of the deformation increment (usually detF )
    double TotalDeformationDet; //wildcard for the determinant of the total deformation     (usually detF0)

    double SuctionOld;
    double SuctionNew;

    //model data
    StressMeasureType StressMeasure; //stress measure requested
    StrainMeasureType StrainMeasure; //strain measure provided

    //deformation
    MatrixType DeltaDeformationMatrix; //wildcard deformation increment (usually incremental F)
    MatrixType TotalDeformationMatrix; //wildcard total deformation     (usually total F := F0)
  };


  /**
   * Defines Operation Data variables for Constitutive Model calculations
   */
  struct ModelData
  {
  private:
    const Flags *mpOptions;
    const ProcessInfo *mpProcessInfo;

    SizeType mVoigtSize;
    VoigtIndexType mIndexVoigtTensor;

    ConstitutiveLawData mConstitutiveLawData;

  public:
    Flags State;

    MatrixType StressMatrix; //wildcard stress (isochoric stress tensor)
    MatrixType StrainMatrix; //wildcard strain (cauchy green tensors or infinitessimal tensor)
    MaterialData MaterialParameters;

    VariableValueData InternalVariable; //internal variable to compute and return

    //Set Data Pointers
    void SetOptions(const Flags &rOptions) { mpOptions = &rOptions; };
    void SetProperties(const Properties &rProperties) { MaterialParameters.SetProperties(rProperties); };
    void SetPropertiesLayout(PropertiesLayout::Pointer pPropertiesLayout) { MaterialParameters.SetPropertiesLayout(pPropertiesLayout); };

    void SetProcessInfo(const ProcessInfo &rProcessInfo) { mpProcessInfo = &rProcessInfo; };
    void SetVoigtSize(const SizeType &rVoigtSize) { mVoigtSize = rVoigtSize; };
    void SetVoigtIndexTensor(VoigtIndexType rIndexVoigtTensor) { mIndexVoigtTensor = rIndexVoigtTensor; };

    void SetIntVariableData(const Variable<int> &rVariable, int &rValue) { InternalVariable.SetIntVariableValue(rVariable, rValue); };
    void SetDoubleVariableData(const Variable<double> &rVariable, double &rValue) { InternalVariable.SetDoubleVariableValue(rVariable, rValue); };
    void SetVectorVariableData(const Variable<Vector> &rVariable, Vector &rValue) { InternalVariable.SetVectorVariableValue(rVariable, rValue); };
    void SetMatrixVariableData(const Variable<Matrix> &rVariable, Matrix &rValue) { InternalVariable.SetMatrixVariableValue(rVariable, rValue); };
    void SetArray3VariableData(const Variable<array_1d<double, 3>> &rVariable, array_1d<double, 3> &rValue) { InternalVariable.SetArray3VariableValue(rVariable, rValue); };
    void SetArray6VariableData(const Variable<array_1d<double, 6>> &rVariable, array_1d<double, 6> &rValue) { InternalVariable.SetArray6VariableValue(rVariable, rValue); };

    void SetStressMeasure(StressMeasureType Measure) { mConstitutiveLawData.StressMeasure = Measure; };
    void SetStrainMeasure(StrainMeasureType Measure) { mConstitutiveLawData.StrainMeasure = Measure; };

    //Has Data Pointers
    bool HasPropertiesLayout() const { return MaterialParameters.HasPropertiesLayout(); };

    //Get Data Pointers
    const Flags &GetOptions() const { return *mpOptions; };
    const Properties &GetProperties() const { return MaterialParameters.GetProperties(); };

    const PropertiesLayout &GetPropertiesLayout() const { return MaterialParameters.GetPropertiesLayout(); };

    const ProcessInfo &GetProcessInfo() const { return *mpProcessInfo; };
    const SizeType &GetVoigtSize() const { return mVoigtSize; };
    const VoigtIndexType &GetVoigtIndexTensor() const { return mIndexVoigtTensor; };

    //Acces non const Data
    ConstitutiveLawData &rConstitutiveLawData() { return mConstitutiveLawData; };
    MatrixType &rStrainMatrix() { return StrainMatrix; };
    MatrixType &rStressMatrix() { return StressMatrix; };
    MaterialData &rMaterialParameters() { return MaterialParameters; };

    //Get const Data
    const double &GetPressure() const { return mConstitutiveLawData.Pressure; };
    const double &GetTemperature() const { return mConstitutiveLawData.Temperature; };
    const double &GetSuctionOld() const { return mConstitutiveLawData.SuctionOld; };
    const double &GetSuctionNew() const { return mConstitutiveLawData.SuctionNew; };
    const double &GetDeltaDeformationDet() const { return mConstitutiveLawData.DeltaDeformationDet; };
    const double &GetTotalDeformationDet() const { return mConstitutiveLawData.TotalDeformationDet; };
    const double &GetCharacteristicSize() const { return mConstitutiveLawData.CharacteristicSize; };

    const StressMeasureType &GetStressMeasure() const { return mConstitutiveLawData.StressMeasure; };
    const StrainMeasureType &GetStrainMeasure() const { return mConstitutiveLawData.StrainMeasure; };

    const MatrixType &GetDeltaDeformationMatrix() const { return mConstitutiveLawData.DeltaDeformationMatrix; };
    const MatrixType &GetTotalDeformationMatrix() const { return mConstitutiveLawData.TotalDeformationMatrix; };

    const ConstitutiveLawData &GetConstitutiveLawData() const { return mConstitutiveLawData; };

    const MatrixType &GetStrainMatrix() const { return StrainMatrix; };
    const MatrixType &GetStressMatrix() const { return StressMatrix; };
    const MaterialData &GetMaterialParameters() const { return MaterialParameters; };
  };

  /**
   * Defines Operation Data variables for Model calculations
   */
  template <class TVariables>
  struct ModelDataVariables
  {
  private:
    Flags *mpState;
    const ModelData *mpModelData;

  public:
    TVariables Data;

    //Set Data Pointers
    void SetState(Flags &rState) { mpState = &rState; };
    void SetModelData(const ModelData &rModelData) { mpModelData = &rModelData; };

    //Get Data Pointers
    const ModelData &GetModelData() const { return *mpModelData; };
    const MaterialData &GetMaterialParameters() const { return mpModelData->GetMaterialParameters(); };

    //Get non const Data
    Flags &State() { return *mpState; };

    //Get const Data
    const Flags &GetState() const { return *mpState; };
  };


  /**
   * Defines Internal variables buffer type for the Plasticity Model
   */
  template <class TVariables, std::size_t TBufferSize = 1>
  struct InternalBuffer
  {
    //internal variables
   private:

    BoundedVector<TVariables, TBufferSize> Buffer;

   public:

    InternalBuffer() { for(auto i_var : Buffer) i_var.clear(); };

    //operator
    TVariables &operator[](unsigned int buffer){
#ifdef KRATOS_DEBUG
     KRATOS_DEBUG_ERROR_IF(buffer>=TBufferSize) << "Internal variables buffer size : " << TBufferSize << " asking for: " << buffer << std::endl;
#endif
     return Buffer[buffer];
    }

    //operator
    double &operator()(unsigned int index, unsigned int buffer=0){
#ifdef KRATOS_DEBUG
     KRATOS_DEBUG_ERROR_IF(buffer>=TBufferSize) << "Internal variables buffer size : " << TBufferSize << " asking for: " << buffer << std::endl;
#endif
     return Buffer[buffer][index];
    }

    //operator
    const double &operator()(unsigned int index, unsigned int buffer=0) const {
#ifdef KRATOS_DEBUG
     KRATOS_DEBUG_ERROR_IF(buffer>=TBufferSize) << "Internal variables buffer size : " << TBufferSize << " asking for: " << buffer << std::endl;
#endif
     return Buffer[buffer][index];
    }

    //operator
    template<class TVarName>
    double &operator()(TVarName index, unsigned int buffer=0){
#ifdef KRATOS_DEBUG
     KRATOS_DEBUG_ERROR_IF(buffer>=TBufferSize) << "Internal variables buffer size : " << TBufferSize << " asking for: " << buffer << std::endl;
#endif
     return Buffer[buffer][static_cast<size_t>(index)];
    }

    //operator
    template<class TVarName>
    const double &operator()(TVarName index, unsigned int buffer=0) const {
#ifdef KRATOS_DEBUG
     KRATOS_DEBUG_ERROR_IF(buffer>=TBufferSize) << "Internal variables buffer size : " << TBufferSize << " asking for: " << buffer << std::endl;
#endif
     return Buffer[buffer][static_cast<size_t>(index)];
    }

    TVariables &Current()
    {
      return Buffer[0];
    }

    template<class TVector>
    void GetData(TVector& rVector)
    {
      size_t k = 0;
      for (size_t i=0; i<TBufferSize; ++i)
      {
        k += Buffer[i].size();
      }
      rVector.resize(k,false);
      k = 0;
      for (size_t i=0; i<TBufferSize; ++i)
      {
        for (size_t j=0; j<Buffer[i].size(); ++j)
          rVector[k++] = Buffer[i][j];
      }
    }

    template<class TVector>
    void SetData(TVector& rVector)
    {
      size_t k = 0;
      for (size_t i=0; i<TBufferSize; ++i)
      {
        for (size_t j=0; j<Buffer[i].size(); ++j)
          Buffer[i][j] = rVector[k++];
      }
    }

    void Update()
    {
      for (size_t i=TBufferSize-1; i>=1; --i)
        Buffer[i]=Buffer[i-1];
    }

    void Update(TVariables& rVariables)
    {
      Update();
      Buffer[0] = rVariables;
    }

  private:
    friend class Serializer;

    void save(Serializer &rSerializer) const
    {
      rSerializer.save("Buffer", Buffer);
    };

    void load(Serializer &rSerializer)
    {
      rSerializer.load("Buffer", Buffer);
    };
  };


  /**
   * Defines Internal variables vector indexed by a NAME defined by an enum class (TVarName) for the Plasticity Model
   */
  template <std::size_t TVarSize, class TVarName>
  struct InternalData
  {
    //internal variables
   private:

    array_1d<double, TVarSize> Variables;

   public:

    //default constructor (to initialize Variables)
    InternalData() { clear(); };

    //operator with enum name
    double& operator[](TVarName index){
#ifdef KRATOS_DEBUG
     KRATOS_DEBUG_ERROR_IF(static_cast<size_t>(index)>=TVarSize) << "Internal variables size : " << TVarSize << " asking for: " << static_cast<size_t>(index) << std::endl;
#endif
     return Variables[static_cast<size_t>(index)];
    }

    //operator with enum name const
    const double& operator[](TVarName index) const {
#ifdef KRATOS_DEBUG
     KRATOS_DEBUG_ERROR_IF(static_cast<size_t>(index)>=TVarSize) << "Internal variables size : " << TVarSize << " asking for: " << static_cast<size_t>(index) << std::endl;
#endif
     return Variables[static_cast<size_t>(index)];
    }

    //operator with integer
    double& operator[](unsigned int index){
#ifdef KRATOS_DEBUG
     KRATOS_DEBUG_ERROR_IF(index>=TVarSize) << "Internal variables size : " << TVarSize << " asking for: " << index << std::endl;
#endif
     return Variables[index];
    }

    //operator
    double& operator()(unsigned int index){
#ifdef KRATOS_DEBUG
     KRATOS_DEBUG_ERROR_IF(index>=TVarSize) << "Internal variables size : " << TVarSize << " asking for: " << index << std::endl;
#endif
     return Variables[index];
    }

    //operator const
    const double& operator()(unsigned int index) const {
#ifdef KRATOS_DEBUG
     KRATOS_DEBUG_ERROR_IF(index>=TVarSize) << "Internal variables size : " << TVarSize << " asking for: " << index << std::endl;
#endif
     return Variables[index];
    }

    const array_1d<double, TVarSize> &GetVariables() const { return Variables; };

    unsigned int size() { return TVarSize; }

    void clear(){
      Variables.clear();
    }

  private:
    friend class Serializer;

    void save(Serializer &rSerializer) const
    {
      rSerializer.save("InternalData", Variables);
    };

    void load(Serializer &rSerializer)
    {
      rSerializer.load("InternalData", Variables);
    };
  };


  /**
   * Defines Operation Data variables for Plasticity Model calculations
   */
  template <std::size_t TVarSize, class TVarName>
  struct PlasticityData
  {
    //flow rule internal variables
    double TrialStateFunction;
    double StressNorm;

    //hardening rule internal variables
    double RateFactor;

    //internal variables
    InternalData<TVarSize, TVarName> Internal;
    InternalData<TVarSize, TVarName> DeltaInternal;

    //strain matrix
    MatrixType StrainMatrix; //wildcard strain (cauchy green tensors or infinitessimal tensor)

     //Get const Data
    const double &GetTrialStateFunction() const { return TrialStateFunction; };
    const double &GetStressNorm() const { return StressNorm; };
    const double &GetRateFactor() const { return RateFactor; };

    const InternalData<TVarSize, TVarName> &GetInternal() const { return Internal; };
    const InternalData<TVarSize, TVarName> &GetDeltaInternal() const { return DeltaInternal; };

    const MatrixType &GetStrainMatrix() const { return StrainMatrix; };

    const array_1d<double, TVarSize> &GetInternalVariables(unsigned int buffer=0) const { return Internal.GetVariables(); };
    const array_1d<double, TVarSize> &GetDeltaInternalVariables(unsigned int buffer=0) const { return DeltaInternal.GetVariables(); };
  };

  // TO DEPRECATE
  /**
   * Defines Internal variables vector indexed by an unsigned int for the Plasticity Model
   */
  template <std::size_t TVarSize>
  struct InternalVariables
  {
    //internal variables
    array_1d<double, TVarSize> Variables;

    //default constructor (to initialize Variables)
    InternalVariables() { clear(); };

    //operator with integer
    double& operator[](unsigned int index){
     return Variables[index];
    }

    //operator
    double& operator()(unsigned int index){
     return Variables[index];
    }

    //operator
    const double& operator()(unsigned int index) const {
     return Variables[index];
    }

    const array_1d<double, TVarSize> &GetVariables() { return Variables; };

    unsigned int size() { return TVarSize; }

    void clear(){
      noalias(Variables) = ZeroVector(TVarSize);
    }

  private:
    friend class Serializer;

    void save(Serializer &rSerializer) const
    {
      rSerializer.save("Variables", Variables);
    };

    void load(Serializer &rSerializer)
    {
      rSerializer.load("Variables", Variables);
    };
  };

  /**
   * Defines Operation Data variables for Plasticity Model calculations
   */
  template <std::size_t TVarSize>
  struct PlasticityDataOld
  {
    //flow rule internal variables
    double TrialStateFunction;
    double StressNorm;

    //hardening rule internal variables
    double RateFactor;

    //internal variables
    InternalVariables<TVarSize> Internal;
    InternalVariables<TVarSize> DeltaInternal;

    //strain matrix
    MatrixType StrainMatrix; //wildcard strain (cauchy green tensors or infinitessimal tensor)

    //Get const Data
    const double &GetTrialStateFunction() const { return TrialStateFunction; };
    const double &GetStressNorm() const { return StressNorm; };
    const double &GetRateFactor() const { return RateFactor; };

    const InternalVariables<TVarSize> &GetInternal() const { return Internal; };
    const InternalVariables<TVarSize> &GetDeltaInternal() const { return DeltaInternal; };

    const MatrixType &GetStrainMatrix() const { return StrainMatrix; };

    const array_1d<double, TVarSize> &GetInternalVariables() const { return Internal.Variables; };
    const array_1d<double, TVarSize> &GetDeltaInternalVariables() const { return DeltaInternal.Variables; };
  };
  // TO DEPRECATE


  /// Pointer definition of ConstitutiveModelData
  KRATOS_CLASS_POINTER_DEFINITION(ConstitutiveModelData);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  ConstitutiveModelData() {}

  /// Copy constructor.
  ConstitutiveModelData(ConstitutiveModelData const &rOther) {}

  /// Clone.
  ConstitutiveModelData::Pointer Clone() const
  {
    return Kratos::make_shared<ConstitutiveModelData>(*this);
  }

  /// Destructor.
  virtual ~ConstitutiveModelData() {}

  ///@}
  ///@name Operators
  ///@{
  ///@}
  ///@name Operations
  ///@{
  ///@}
  ///@name Access
  ///@{

  static inline void CalculateSolidMaterialParameters(ModelData &rValues)
  {
    KRATOS_TRY

    //material properties
    const Properties &rProperties = rValues.GetProperties();

    //if previously computed LameMu / LameLambda / BulkModulus
    // rValues.MaterialParameters.LameMu      = rProperties[LAME_MU];
    // rValues.MaterialParameters.LameLambda  = rProperties[LAME_LAMBDA];
    // rValues.MaterialParameters.BulkModulus = rProperties[BULK_MODULUS];

    // compute material properties

    // const double& YoungModulus       = rProperties[YOUNG_MODULUS];
    // const double& PoissonCoefficient = rProperties[POISSON_RATIO];

    // temperature dependent parameters:
    double value = 0;
    if (rValues.HasPropertiesLayout())
    {
      const PropertiesLayout &rPropertiesLayout = rValues.GetPropertiesLayout();

      rPropertiesLayout.GetValue(YOUNG_MODULUS, value);
      rValues.MaterialParameters.SetValue(YOUNG_MODULUS, value);
      rPropertiesLayout.GetValue(POISSON_RATIO, value);
      rValues.MaterialParameters.SetValue(POISSON_RATIO, value);
    }
    else
    {

      ConstitutiveLawData &rConstitutiveLawData = rValues.rConstitutiveLawData();
      if (rProperties.HasTable(TEMPERATURE, YOUNG_MODULUS))
      {
        const Table<double> &YoungModulusTable = rProperties.GetTable(TEMPERATURE, YOUNG_MODULUS);
        rValues.MaterialParameters.SetValue(YOUNG_MODULUS, YoungModulusTable[rConstitutiveLawData.Temperature]);
      }
      else
      {
        rValues.MaterialParameters.SetValue(YOUNG_MODULUS, rProperties[YOUNG_MODULUS]);
      }

      if (rProperties.HasTable(TEMPERATURE, POISSON_RATIO))
      {
        const Table<double> &PoissonCoefficientTable = rProperties.GetTable(TEMPERATURE, POISSON_RATIO);
        rValues.MaterialParameters.SetValue(POISSON_RATIO, PoissonCoefficientTable[rConstitutiveLawData.Temperature]);
      }
      else
      {
        rValues.MaterialParameters.SetValue(POISSON_RATIO,rProperties[POISSON_RATIO]);
      }
    }

    value = rValues.MaterialParameters.GetYoungModulus() / (2.0 * (1.0 + rValues.MaterialParameters.GetPoissonCoefficient()));
    rValues.MaterialParameters.SetLameMu(value);
    value = (rValues.MaterialParameters.GetYoungModulus() * rValues.MaterialParameters.GetPoissonCoefficient()) / ((1.0 + rValues.MaterialParameters.GetPoissonCoefficient()) * (1.0 - 2.0 * rValues.MaterialParameters.GetPoissonCoefficient()));
    rValues.MaterialParameters.SetLameLambda(value);


    //hyperelastic model parameters
    if (rProperties.Has(MATERIAL_PARAMETERS))
    {
      rValues.MaterialParameters.SetModelParameters(rProperties[MATERIAL_PARAMETERS]);
    }
    else
    {
      std::vector<double> ModelParameters;
      if (rProperties.Has(C10))
      {
        ModelParameters.push_back(rProperties[C10]);

        //make neo-hookean consistent with the parameters:
        rValues.MaterialParameters.SetLameMu(2.0 * rProperties[C10]);
      }

      if (rProperties.Has(C20))
        ModelParameters.push_back(rProperties[C20]);

      if (rProperties.Has(C30))
        ModelParameters.push_back(rProperties[C30]);

      Vector MaterialParameters(ModelParameters.size());
      for(unsigned int i=0; i<ModelParameters.size(); ++i)
        MaterialParameters[i] = ModelParameters[i];

      rValues.MaterialParameters.SetModelParameters(MaterialParameters);
    }


    if (rProperties.Has(BULK_MODULUS))
    {
      rValues.MaterialParameters.SetBulkModulus(rProperties[BULK_MODULUS]);

      //make neo-hookean consistent with the parameters:
      value = rValues.MaterialParameters.GetBulkModulus() - (2.0 / 3.0) * rValues.MaterialParameters.GetLameMu();
      rValues.MaterialParameters.SetLameLambda(value);
    }
    else
    {
      value = rValues.MaterialParameters.GetLameLambda() + (2.0 / 3.0) * rValues.MaterialParameters.GetLameMu();
      rValues.MaterialParameters.SetBulkModulus(value);
    }

    //std::cout<<" Mu "<<rValues.MaterialParameters.LameMu<<" Lambda "<<rValues.MaterialParameters.LameLambda<<" BulkModulus "<<rValues.MaterialParameters.BulkModulus<<std::endl;

    //infinitessimal strain (plasticity mu_bar := mu)
    rValues.MaterialParameters.SetLameMuBar(rValues.MaterialParameters.GetLameMu());

    //std::cout<<" B: Mu "<<rValues.MaterialParameters.LameMu<<" Lambda "<<rValues.MaterialParameters.LameLambda<<" BulkModulus "<<rValues.MaterialParameters.BulkModulus<<std::endl;

    KRATOS_CATCH(" ")
  }


  static inline void CalculateFluidMaterialParameters(ModelData &rValues)
  {
    KRATOS_TRY

    //material properties
    const Properties &rProperties = rValues.GetProperties();

    // temperature dependent parameters:
    if (rValues.HasPropertiesLayout())
    {
      const PropertiesLayout &rPropertiesLayout = rValues.GetPropertiesLayout();

      double value;
      rPropertiesLayout.GetValue(DYNAMIC_VISCOSITY, value);
      rValues.MaterialParameters.SetValue(DYNAMIC_VISCOSITY, value);
      rPropertiesLayout.GetValue(DENSITY, value);
      rValues.MaterialParameters.SetValue(DENSITY, value);
    }
    else
    {
      ConstitutiveLawData &rConstitutiveLawData = rValues.rConstitutiveLawData();
      if (rProperties.HasTable(TEMPERATURE, DYNAMIC_VISCOSITY))
      {
        const Table<double> &ViscosityTable = rProperties.GetTable(TEMPERATURE, DYNAMIC_VISCOSITY);
        rValues.MaterialParameters.SetLameMu(ViscosityTable[rConstitutiveLawData.Temperature]);
      }
      else
      {
        rValues.MaterialParameters.SetLameMu(rProperties[DYNAMIC_VISCOSITY]);
      }

      if (rProperties.HasTable(TEMPERATURE, DENSITY))
      {
        const Table<double> &DensityTable = rProperties.GetTable(TEMPERATURE, DENSITY);
        rValues.MaterialParameters.SetDensity(DensityTable[rConstitutiveLawData.Temperature]);
      }
      else
      {
        rValues.MaterialParameters.SetDensity(rProperties[DENSITY]);
      }
    }

    KRATOS_CATCH(" ")
  }


  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  virtual std::string Info() const
  {
    std::stringstream buffer;
    buffer << "ConstitutiveModelData";
    return buffer.str();
  }

  /// Print information about this object.
  virtual void PrintInfo(std::ostream &rOStream) const
  {
    rOStream << "ConstitutiveModelData";
  }

  /// Print object's data.
  virtual void PrintData(std::ostream &rOStream) const
  {
    rOStream << "ConstitutiveModelData Data";
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{
  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{

  /// Assignment operator.
  ConstitutiveModelData &operator=(ConstitutiveModelData const &rOther);

  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{
  ///@}

}; // Class ConstitutiveModelData

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                ConstitutiveModelData &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const ConstitutiveModelData &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << " : " << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}

///@}
///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_CONSTITUTIVE_MODEL_DATA_HPP_INCLUDED  defined
