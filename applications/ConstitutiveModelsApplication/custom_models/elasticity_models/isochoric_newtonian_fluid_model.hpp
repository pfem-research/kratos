//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                 August 2020 $
//
//

#if !defined(KRATOS_ISOCHORIC_NEWTONIAN_FLUID_MODEL_HPP_INCLUDED)
#define KRATOS_ISOCHORIC_NEWTONIAN_FLUID_MODEL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/elasticity_models/newtonian_fluid_model.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) IsochoricNewtonianFluidModel : public NewtonianFluidModel
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of IsochoricNewtonianFluidModel
  KRATOS_CLASS_POINTER_DEFINITION(IsochoricNewtonianFluidModel);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  IsochoricNewtonianFluidModel() : NewtonianFluidModel() {}

  /// Copy constructor.
  IsochoricNewtonianFluidModel(IsochoricNewtonianFluidModel const &rOther) : NewtonianFluidModel(rOther) {}

  /// Assignment operator.
  IsochoricNewtonianFluidModel &operator=(IsochoricNewtonianFluidModel const &rOther)
  {
    NewtonianFluidModel::operator=(rOther);
    return *this;
  }

  /// Clone.
  ConstitutiveModel::Pointer Clone() const override
  {
    return Kratos::make_shared<IsochoricNewtonianFluidModel>(*this);
  }

  /// Destructor.
  ~IsochoricNewtonianFluidModel() override {}

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  void CalculateStrainEnergy(ModelDataType &rValues, double &rDensityFunction) override
  {
    KRATOS_TRY

    ElasticDataType Variables;
    this->InitializeVariables(rValues, Variables);

    rDensityFunction = 0;
    this->CalculateAndAddIsochoricStrainEnergy(Variables, rDensityFunction);
    this->CalculateAndAddVolumetricStrainEnergy(Variables, rDensityFunction);

    KRATOS_CATCH(" ")
  }


  /**
     * Check
     */
  int Check(const Properties &rProperties, const ProcessInfo &rCurrentProcessInfo) override
  {
    KRATOS_TRY

    return NewtonianFluidModel::Check(rProperties, rCurrentProcessInfo);

    KRATOS_CATCH(" ")
  }

  /**
     * Get required properties
     */
  void GetRequiredProperties(Properties &rProperties) override
  {
    KRATOS_TRY

    NewtonianFluidModel::GetRequiredProperties(rProperties);

    KRATOS_CATCH(" ")
  }
  void CalculateAndAddVolumetricStressTensor(ElasticDataType &rVariables, VectorType &rStressVector) override
  {
    KRATOS_TRY

    double Factor = 0;
    this->CalculatePressureFactor(rVariables, Factor);

    // Cauchy Volumetric StressVector
    rStressVector[0] += Factor;
    rStressVector[1] += Factor;
    rStressVector[2] += Factor;

    KRATOS_CATCH(" ")
  }

  void CalculateAndAddIsochoricStressTensor(ElasticDataType &rVariables, VectorType &rStressVector) override
  {
    KRATOS_TRY

    const double &rViscosity = rVariables.GetModelData().GetMaterialParameters().GetLameMu();

    // Cauchy Isochoric StressVector (with shear and bulk viscosity)
    rStressVector[0] += 2.0 * rViscosity * (rVariables.Data.StrainVector[0]);
    rStressVector[1] += 2.0 * rViscosity * (rVariables.Data.StrainVector[1]);
    rStressVector[2] += 2.0 * rViscosity * (rVariables.Data.StrainVector[2]);
    rStressVector[3] += rViscosity * rVariables.Data.StrainVector[3];
    rStressVector[4] += rViscosity * rVariables.Data.StrainVector[4];
    rStressVector[5] += rViscosity * rVariables.Data.StrainVector[5];

    KRATOS_CATCH(" ")
  }

  void CalculateAndAddStressTensor(ElasticDataType &rVariables, VectorType &rStressVector) override
  {
    KRATOS_TRY

    noalias(rStressVector) = ZeroVector(6);

    this->CalculateAndAddIsochoricStressTensor(rVariables, rStressVector);

    this->CalculateAndAddVolumetricStressTensor(rVariables, rStressVector);

    KRATOS_CATCH(" ")
  }

  void CalculateAndAddConstitutiveTensor(ElasticDataType &rVariables, Matrix &rConstitutiveMatrix) override
  {
    KRATOS_TRY

    this->CalculateAndAddIsochoricConstitutiveTensor(rVariables);

    rConstitutiveMatrix = ConstitutiveModelUtilities::ConstitutiveTensorToMatrix(rVariables.Data.ConstitutiveTensor, rConstitutiveMatrix);

    this->CalculateAndAddVolumetricConstitutiveTensor(rVariables, rConstitutiveMatrix);

    rVariables.State().Set(ConstitutiveModelData::CONSTITUTIVE_MATRIX_COMPUTED);

    KRATOS_CATCH(" ")
  }


  void CalculateAndAddVolumetricConstitutiveTensor(ElasticDataType &rVariables, Matrix &rConstitutiveMatrix) override
  {
    KRATOS_TRY

    //Calculate HyperElastic ConstitutiveMatrix
    const ModelDataType &rModelData = rVariables.GetModelData();
    const SizeType &rVoigtSize = rModelData.GetVoigtSize();
    const VoigtIndexType &rIndexVoigtTensor = rModelData.GetVoigtIndexTensor();

    for (SizeType i = 0; i < rVoigtSize; i++)
      {
        for (SizeType j = 0; j < rVoigtSize; j++)
          {

            rConstitutiveMatrix(i, j) = this->AddVolumetricConstitutiveComponent(rVariables, rConstitutiveMatrix(i, j),
                                                                                 rIndexVoigtTensor[i][0], rIndexVoigtTensor[i][1],
                                                                                 rIndexVoigtTensor[j][0], rIndexVoigtTensor[j][1]);
          }
      }

    rVariables.State().Set(ConstitutiveModelData::CONSTITUTIVE_MATRIX_COMPUTED, true);

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "IsochoricNewtonianFluidModel";
    return buffer.str();
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "IsochoricNewtonianFluidModel";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "IsochoricNewtonianFluidModel Data";
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  //specialized methods:

  void CalculateAndAddIsochoricConstitutiveTensor(ElasticDataType &rVariables) override
  {
    KRATOS_TRY

    //Calculate Elastic ConstitutiveMatrix
    const ModelDataType &rModelData = rVariables.GetModelData();
    const MaterialDataType &rMaterial = rModelData.GetMaterialParameters();
    const double &rViscosity = rMaterial.GetLameMu();

    noalias(rVariables.Data.ConstitutiveTensor) = ZeroMatrix(6,6);

    // 3D linear elastic constitutive matrix
    const double diagonal_component = 2.0 * rViscosity;

    // 3D linear elastic constitutive matrix
    rVariables.Data.ConstitutiveTensor(0, 0) = diagonal_component;
    rVariables.Data.ConstitutiveTensor(1, 1) = diagonal_component;
    rVariables.Data.ConstitutiveTensor(2, 2) = diagonal_component;

    rVariables.Data.ConstitutiveTensor(3, 3) = rViscosity;
    rVariables.Data.ConstitutiveTensor(4, 4) = rViscosity;
    rVariables.Data.ConstitutiveTensor(5, 5) = rViscosity;

    rVariables.Data.ConstitutiveTensor(0, 1) = 0.0;
    rVariables.Data.ConstitutiveTensor(1, 0) = 0.0;

    rVariables.Data.ConstitutiveTensor(0, 2) = 0.0;
    rVariables.Data.ConstitutiveTensor(2, 0) = 0.0;

    rVariables.Data.ConstitutiveTensor(1, 2) = 0.0;
    rVariables.Data.ConstitutiveTensor(2, 1) = 0.0;

    //initialize to zero other values
    for (unsigned int i = 0; i < 3; ++i)
      {
        for (unsigned int j = 3; j < 6; ++j)
          {
            rVariables.Data.ConstitutiveTensor(i, j) = 0;
            rVariables.Data.ConstitutiveTensor(j, i) = 0;
          }
      }

    rVariables.Data.ConstitutiveTensor(3, 4) = 0.0;
    rVariables.Data.ConstitutiveTensor(3, 5) = 0.0;
    rVariables.Data.ConstitutiveTensor(4, 5) = 0.0;

    rVariables.Data.ConstitutiveTensor(4, 3) = 0.0;
    rVariables.Data.ConstitutiveTensor(5, 3) = 0.0;
    rVariables.Data.ConstitutiveTensor(5, 4) = 0.0;

    rVariables.State().Set(ConstitutiveModelData::CONSTITUTIVE_MATRIX_COMPUTED, true);

    KRATOS_CATCH(" ")
  }

  virtual void CalculatePressureFactor(ElasticDataType &rVariables, double &rFactor)
  {
    KRATOS_TRY

    const MatrixType &rStrainMatrix = rVariables.GetModelData().GetStrainMatrix();
    rFactor = (rStrainMatrix(0,0) + rStrainMatrix(1,1) + rStrainMatrix(2,2)) / 3.0;

    const double &rBulkModulus = rVariables.GetModelData().GetMaterialParameters().GetBulkModulus();
    rFactor *= rBulkModulus;

    KRATOS_CATCH(" ")
  }

  virtual void CalculateConstitutiveMatrixPressureFactor(ElasticDataType &rVariables, double &rFactor)
  {
    KRATOS_TRY

    const MatrixType &rStrainMatrix = rVariables.GetModelData().GetStrainMatrix();
    rFactor = (rStrainMatrix(0,0) + rStrainMatrix(1,1) + rStrainMatrix(2,2)) / 3.0;

    const double &rBulkModulus = rVariables.GetModelData().GetMaterialParameters().GetBulkModulus();
    rFactor *= rBulkModulus;

    KRATOS_CATCH(" ")
  }


  virtual double &AddVolumetricConstitutiveComponent(ElasticDataType &rVariables, double &rCabcd,
                                                     const unsigned int &a, const unsigned int &b,
                                                     const unsigned int &c, const unsigned int &d)
  {
    KRATOS_TRY


    double Factor = 0;
    this->CalculateConstitutiveMatrixPressureFactor(rVariables, Factor);

    double Cabcd = 0;

    //volumetric constitutive tensor component:
    Cabcd  = msIdentityMatrix(a, b) * msIdentityMatrix(c, d);
    Cabcd -= msIdentityMatrix(a, c) * msIdentityMatrix(b, d) + msIdentityMatrix(a, d) * msIdentityMatrix(b, c);
    Cabcd *= Factor;

    rCabcd += Cabcd;

    return rCabcd;

    KRATOS_CATCH(" ")
  }


  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, NewtonianFluidModel)
  }

  void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, NewtonianFluidModel)
  }

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class IsochoricNewtonianFluidModel

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_ISOCHORIC_NEWTONIAN_FLUID_MODEL_HPP_INCLUDED  defined
