//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:              September 2022 $
//
//

#if !defined(KRATOS_HYPO_ELASTIC_JAUMANN_MODEL_HPP_INCLUDED)
#define KRATOS_HYPO_ELASTIC_JAUMANN_MODEL_HPP_INCLUDED

// System includes
#include <string>
#include <iostream>

// External includes

// Project includes
#include "custom_models/elasticity_models/hypo_elastic_jaumann_model.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) HypoElasticJaumannModel : public HypoElasticModel
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of HypoElasticJaumannModel
  KRATOS_CLASS_POINTER_DEFINITION(HypoElasticJaumannModel);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  HypoElasticJaumannModel() : HypoElasticModel() {}

  /// Copy constructor.
  HypoElasticJaumannModel(HypoElasticJaumannModel const &rOther) : HypoElasticModel(rOther) {}

  /// Assignment operator.
  HypoElasticJaumannModel &operator=(HypoElasticJaumannModel const &rOther)
  {
    HypoElasticModel::operator=(rOther);
    return *this;
  }

  /// Clone.
  ConstitutiveModel::Pointer Clone() const override
  {
    return Kratos::make_shared<HypoElasticJaumannModel>(*this);
  }

  /// Destructor.
  ~HypoElasticJaumannModel() override {}

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  //************//

  void InitializeVariables(ModelDataType &rValues, ElasticDataType &rVariables) override
  {
    KRATOS_TRY

    //set model data pointer
    rVariables.SetModelData(rValues);
    rVariables.SetState(rValues.State);

    //deformation gradient
    const MatrixType &rDeltaDeformationMatrix = rValues.GetDeltaDeformationMatrix();

    // ConstitutiveModelData::StressMeasureType::StressMeasure_Kirchhoff  allowed only
    rValues.SetStrainMeasure(ConstitutiveModelData::StrainMeasureType::StrainMeasure_Hencky_Material);

    noalias(rValues.StrainMatrix) = prod(trans(rDeltaDeformationMatrix), rDeltaDeformationMatrix); // C=U²

    rVariables.Data.Strain.Eigen.Vectors.clear();
    rVariables.Data.Strain.Matrix.clear();
    MathUtils<double>::GaussSeidelEigenSystem<MatrixType, MatrixType>(rValues.StrainMatrix, rVariables.Data.Strain.Eigen.Vectors, rVariables.Data.Strain.Matrix);

    // Compute the natural strain (Hencky strain)
    rValues.StrainMatrix.clear(); //storage of the natural strain E = 0.5*ln(U²)
    for (unsigned int i = 0; i < 3; i++){
      rVariables.Data.Strain.Eigen.Values[i] = std::sqrt(rVariables.Data.Strain.Matrix(i, i));
      rValues.StrainMatrix(i, i) = std::log(rVariables.Data.Strain.Matrix(i, i)) / 2.0;
    }

    noalias(rVariables.Data.Strain.Matrix) = prod(rVariables.Data.Strain.Eigen.Vectors, rValues.StrainMatrix);
    noalias(rValues.StrainMatrix) = prod(rVariables.Data.Strain.Matrix, trans(rVariables.Data.Strain.Eigen.Vectors));

    ConstitutiveModelUtilities::StrainTensorToVector(rValues.StrainMatrix, rVariables.Data.StrainVector);

    // Compute Rotation Matrix (Store it in rValues.StrainMatrix)
    this->CalculateRotationMatrix(rValues, rVariables, rValues.StrainMatrix);

    rValues.State.Set(ConstitutiveModelData::STRAIN_COMPUTED);

    rValues.MaterialParameters.SetLameMuBar(rValues.MaterialParameters.GetLameMu());

    KRATOS_CATCH(" ")
  }

  /**
     * Check
     */
  int Check(const Properties &rProperties, const ProcessInfo &rCurrentProcessInfo) override
  {
    KRATOS_TRY

    return HypoElasticModel::Check(rProperties, rCurrentProcessInfo);

    KRATOS_CATCH(" ")
  }

  //************//

  ///@}
  ///@name Access
  ///@{

  /**
     * method to ask the plasticity model the list of variables (dofs)  needed from the domain
     * @param rScalarVariables : list of scalar dofs
     * @param rComponentVariables :  list of vector dofs
     */
  void GetDomainVariablesList(std::vector<Variable<double>*> &rScalarVariables,
                              std::vector<Variable<array_1d<double, 3>>*> &rComponentVariables) override
  {
    KRATOS_TRY

    rComponentVariables.push_back(&DISPLACEMENT);

    KRATOS_CATCH(" ")
  }

  /**
     * method to ask the constituitve model strain measures required
     * @param rStrainMeasures : list of strain measures
     */
  void GetStrainMeasures(std::vector<ConstitutiveModelData::StrainMeasureType> &rStrainMeasures) override
  {
    KRATOS_TRY

    rStrainMeasures.push_back(ConstitutiveModelData::StrainMeasureType::StrainMeasure_Deformation_Gradient);

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "HypoElasticJaumannModel";
    return buffer.str();
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "HypoElasticJaumannModel";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "HypoElasticJaumannModel Data";
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  /**
     * Rotation matrix calculation
     */
  virtual void CalculateRotationMatrix(ModelDataType &rValues, ElasticDataType &rVariables,MatrixType &rRotationMatrix)
  {
    KRATOS_TRY

    // NOTE: Just be careful because the rRotationMatrix reference use to be rValues.StrainMatrix (check noalias operations)

    // Jaumann Rotation Matrix R:

    const MatrixType &rDeltaDeformationMatrix = rValues.GetDeltaDeformationMatrix();

    // Compute the rotation matrix (F=R*U)  R=F*U^-1
    rVariables.Data.Strain.Matrix.clear(); // U^-1
    for (unsigned int i = 0; i < 3; i++)
      rVariables.Data.Strain.Matrix(i, i) = 1.0 / rVariables.Data.Strain.Eigen.Values[i];

    noalias(rRotationMatrix) = prod(rVariables.Data.Strain.Eigen.Vectors, rVariables.Data.Strain.Matrix);
    noalias(rVariables.Data.Strain.Matrix) = prod(rRotationMatrix, trans(rVariables.Data.Strain.Eigen.Vectors));

    // rVariables.Data.Strain.Matrix it the Jaumann R
    noalias(rRotationMatrix) = prod(rDeltaDeformationMatrix,rVariables.Data.Strain.Matrix);


    KRATOS_CATCH(" ")
  }

  /**
     * Stress update for a hypoelastic model
     */
  void AddHistoricalStress(ModelDataType &rValues, MatrixType &rStressMatrix) override
  {
    KRATOS_TRY

    MatrixType PreviousStressMatrix;
    PreviousStressMatrix = ConstitutiveModelUtilities::VectorToSymmetricTensor(*this->mpHistoryVector, PreviousStressMatrix);

    rStressMatrix += PreviousStressMatrix;

    // Update stress to the new reference position
    rStressMatrix = prod(rValues.StrainMatrix, rStressMatrix);
    rStressMatrix = prod(rStressMatrix, trans(rValues.StrainMatrix));

    // To store when FinalizeModel (can be the total or the isochoric stress)
    rValues.StressMatrix = rStressMatrix;

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, HypoElasticModel)
  }

  void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, HypoElasticModel)
  }

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class HypoElasticJaumannModel

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_HYPO_ELASTIC_JAUMANN_MODEL_HPP_INCLUDED  defined
