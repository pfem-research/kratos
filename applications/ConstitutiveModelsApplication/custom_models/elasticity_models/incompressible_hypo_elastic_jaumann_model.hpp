//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:              September 2022 $
//
//

#if !defined(KRATOS_INCOMPRESSIBLE_HYPO_ELASTIC_JAUMANN_MODEL_HPP_INCLUDED)
#define KRATOS_INCOMPRESSIBLE_HYPO_ELASTIC_JAUMANN_MODEL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/elasticity_models/isochoric_hypo_elastic_jaumann_model.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) IncompressibleHypoElasticJaumannModel : public IsochoricHypoElasticJaumannModel
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of IncompressibleHypoElasticJaumannModel
  KRATOS_CLASS_POINTER_DEFINITION(IncompressibleHypoElasticJaumannModel);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  IncompressibleHypoElasticJaumannModel() : IsochoricHypoElasticJaumannModel() {}

  /// Copy constructor.
  IncompressibleHypoElasticJaumannModel(IncompressibleHypoElasticJaumannModel const &rOther) : IsochoricHypoElasticJaumannModel(rOther) {}

  /// Assignment operator.
  IncompressibleHypoElasticJaumannModel &operator=(IncompressibleHypoElasticJaumannModel const &rOther)
  {
    HypoElasticJaumannModel::operator=(rOther);
    return *this;
  }

  /// Clone.
  ConstitutiveModel::Pointer Clone() const override
  {
    return Kratos::make_shared<IncompressibleHypoElasticJaumannModel>(*this);
  }

  /// Destructor.
  ~IncompressibleHypoElasticJaumannModel() override {}

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /**
     * Check
     */
  int Check(const Properties &rProperties, const ProcessInfo &rCurrentProcessInfo) override
  {
    KRATOS_TRY

    return IsochoricHypoElasticJaumannModel::Check(rProperties, rCurrentProcessInfo);

    KRATOS_CATCH(" ")
  }


  ///@}
  ///@name Access
  ///@{

  /**
     * method to ask the constitutive model the list of variables (dofs) needed from the domain
     * @param rScalarVariables : list of scalar dofs
     * @param rComponentVariables :  list of vector dofs
     */
  void GetDomainVariablesList(std::vector<Variable<double>*> &rScalarVariables,
                              std::vector<Variable<array_1d<double, 3>>*> &rComponentVariables) override
  {
    KRATOS_TRY

    HypoElasticJaumannModel::GetDomainVariablesList(rScalarVariables, rComponentVariables);

    rScalarVariables.push_back(&PRESSURE);

    KRATOS_CATCH(" ")
  }

  void CalculateAndAddVolumetricStressTensor(ElasticDataType &rVariables, VectorType &rStressVector) override
  {
    KRATOS_TRY

    const ModelDataType &rValues = rVariables.GetModelData();

    //volumetric stress
    const double &Pressure = rValues.GetPressure();
    for (unsigned int i = 0; i < 3; i++)
      rStressVector[i] += Pressure;

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "IncompressibleHypoElasticJaumannModel";
    return buffer.str();
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "IncompressibleHypoElasticJaumannModel";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "IncompressibleHypoElasticJaumannModel Data";
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  // set the default volumetric function for the incompressible case

  void CalculateAndAddVolumetricStrainEnergy(ElasticDataType &rVariables, double &rVolumetricDensityFunction) override
  {
    KRATOS_TRY

    KRATOS_ERROR << "calling the class function in IncompressibleHypoElasticJaumannModel ... illegal operation" << std::endl;

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, HypoElasticJaumannModel)
  }

  void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, HypoElasticJaumannModel)
  }

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class IncompressibleHypoElasticJaumannModel

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_INCOMPRESSIBLE_HYPO_ELASTIC_JAUMANN_MODEL_HPP_INCLUDED  defined
