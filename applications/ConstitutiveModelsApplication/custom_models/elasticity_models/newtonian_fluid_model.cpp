//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                  April 2018 $
//
//

// System includes

// External includes

// Project includes
#include "custom_models/elasticity_models/newtonian_fluid_model.hpp"

namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************
const NewtonianFluidModel::MatrixType NewtonianFluidModel::msIdentityMatrix = IdentityMatrix(3);

NewtonianFluidModel::NewtonianFluidModel()
    : ConstitutiveModel()
{
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

NewtonianFluidModel::NewtonianFluidModel(const NewtonianFluidModel &rOther)
    : ConstitutiveModel(rOther)
{
}

//********************************CLONE***********************************************
//************************************************************************************

ConstitutiveModel::Pointer NewtonianFluidModel::Clone() const
{
  return Kratos::make_shared<NewtonianFluidModel>(*this);
}

//********************************ASSIGNMENT******************************************
//************************************************************************************
NewtonianFluidModel &NewtonianFluidModel::operator=(NewtonianFluidModel const &rOther)
{
  ConstitutiveModel::operator=(rOther);
  return *this;
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

NewtonianFluidModel::~NewtonianFluidModel()
{
}

//***********************PUBLIC OPERATIONS FROM BASE CLASS****************************
//************************************************************************************

void NewtonianFluidModel::InitializeModel(ModelDataType &rValues)
{
  KRATOS_TRY

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void NewtonianFluidModel::FinalizeModel(ModelDataType &rValues)
{
  KRATOS_TRY

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void NewtonianFluidModel::CalculateMaterialParameters(ModelDataType &rValues)
{
  KRATOS_TRY

  ConstitutiveModelData::CalculateFluidMaterialParameters(rValues);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void NewtonianFluidModel::InitializeVariables(ModelDataType &rValues, ElasticDataType &rVariables)
{
  KRATOS_TRY

  //set model data pointer
  rVariables.SetModelData(rValues);
  rVariables.SetState(rValues.State);

  rValues.SetStrainMeasure(ConstitutiveModelData::StrainMeasureType::StrainMeasure_None);

  ConstitutiveModelUtilities::StrainTensorToVector(rValues.StrainMatrix, rVariables.Data.StrainVector);

  KRATOS_CATCH(" ")
}


//************************************************************************************
//************************************************************************************

void NewtonianFluidModel::CalculateStrainEnergy(ModelDataType &rValues, double &rDensityFunction)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling the base class function in NewtonianFluidModel ... illegal operation" << std::endl;

  KRATOS_CATCH(" ")
}

//************// W

void NewtonianFluidModel::CalculateAndAddIsochoricStrainEnergy(ElasticDataType &rVariables, double &rIsochoricDensityFunction)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling the base class function in NewtonianFluidModel ... illegal operation" << std::endl;

  KRATOS_CATCH(" ")
}

void NewtonianFluidModel::CalculateAndAddVolumetricStrainEnergy(ElasticDataType &rVariables, double &rVolumetricDensityFunction)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling the base class function in NewtonianFluidModel ... illegal operation" << std::endl;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void NewtonianFluidModel::CalculateStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix)
{
  KRATOS_TRY

  ElasticDataType Variables;
  this->InitializeVariables(rValues, Variables);

  VectorType StressVector;
  this->CalculateAndAddStressTensor(Variables, StressVector);

  rStressMatrix = ConstitutiveModelUtilities::StressVectorToTensor(StressVector, rStressMatrix);

  Variables.State().Set(ConstitutiveModelData::STRESS_COMPUTED);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void NewtonianFluidModel::CalculateAndAddStressTensor(ElasticDataType &rVariables, VectorType &rStressVector)
{
  KRATOS_TRY

  const ModelDataType &rModelData = rVariables.GetModelData();
  const MaterialDataType &rMaterial = rModelData.GetMaterialParameters();
  const double &rViscosity = rMaterial.GetLameMu();

  const double pressure = (rVariables.Data.StrainVector[0] + rVariables.Data.StrainVector[1] + rVariables.Data.StrainVector[2]) / 3.0;

  // Cauchy Isochoric StressVector
  rStressVector[0] = 2.0 * rViscosity * (rVariables.Data.StrainVector[0] - pressure);
  rStressVector[1] = 2.0 * rViscosity * (rVariables.Data.StrainVector[1] - pressure);
  rStressVector[2] = 2.0 * rViscosity * (rVariables.Data.StrainVector[2] - pressure);
  rStressVector[3] = rViscosity * rVariables.Data.StrainVector[3];
  rStressVector[4] = rViscosity * rVariables.Data.StrainVector[4];
  rStressVector[5] = rViscosity * rVariables.Data.StrainVector[5];

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void NewtonianFluidModel::CalculateIsochoricStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix)
{
  KRATOS_TRY

  ElasticDataType Variables;
  this->InitializeVariables(rValues, Variables);

  VectorType StressVector;
  noalias(StressVector) = ZeroVector(6);
  this->CalculateAndAddIsochoricStressTensor(Variables, StressVector);

  rStressMatrix = ConstitutiveModelUtilities::StressVectorToTensor(StressVector, rStressMatrix);

  Variables.State().Set(ConstitutiveModelData::STRESS_COMPUTED);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void NewtonianFluidModel::CalculateAndAddIsochoricStressTensor(ElasticDataType &rVariables, VectorType &rStressVector)
{
  KRATOS_TRY

  const ModelDataType &rModelData = rVariables.GetModelData();
  const MaterialDataType &rMaterial = rModelData.GetMaterialParameters();
  const double &rViscosity = rMaterial.GetLameMu();

  const double pressure = (rVariables.Data.StrainVector[0] + rVariables.Data.StrainVector[1] + rVariables.Data.StrainVector[2]) / 3.0;

  // Cauchy Isochoric StressVector
  rStressVector[0] += 2.0 * rViscosity * (rVariables.Data.StrainVector[0] - pressure);
  rStressVector[1] += 2.0 * rViscosity * (rVariables.Data.StrainVector[1] - pressure);
  rStressVector[2] += 2.0 * rViscosity * (rVariables.Data.StrainVector[2] - pressure);
  rStressVector[3] += rViscosity * rVariables.Data.StrainVector[3];
  rStressVector[4] += rViscosity * rVariables.Data.StrainVector[4];
  rStressVector[5] += rViscosity * rVariables.Data.StrainVector[5];

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void NewtonianFluidModel::CalculateVolumetricStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix)
{
  KRATOS_TRY

  ElasticDataType Variables;
  this->InitializeVariables(rValues, Variables);

  VectorType StressVector;
  noalias(StressVector) = ZeroVector(6);
  this->CalculateAndAddVolumetricStressTensor(Variables, StressVector);

  rStressMatrix = ConstitutiveModelUtilities::StressVectorToTensor(StressVector, rStressMatrix);

  Variables.State().Set(ConstitutiveModelData::STRESS_COMPUTED);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void NewtonianFluidModel::CalculateAndAddVolumetricStressTensor(ElasticDataType &rVariables, VectorType &rStressVector)
{
  KRATOS_TRY

  const ModelDataType &rModelData = rVariables.GetModelData();
  const MaterialDataType &rMaterial = rModelData.GetMaterialParameters();
  const double &rViscosity = rMaterial.GetLameMu();

  const double pressure = (rVariables.Data.StrainVector[0] + rVariables.Data.StrainVector[1] + rVariables.Data.StrainVector[2]) / 3.0;

  // Cauchy Volumetric StressVector
  rStressVector[0] += 2.0 * rViscosity * pressure;
  rStressVector[1] += 2.0 * rViscosity * pressure;
  rStressVector[2] += 2.0 * rViscosity * pressure;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void NewtonianFluidModel::CalculateConstitutiveTensor(ModelDataType &rValues, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  ElasticDataType Variables;
  this->InitializeVariables(rValues, Variables);

  //Set constitutive matrix to zero before adding
  const SizeType &rVoigtSize = Variables.GetModelData().GetVoigtSize();
  noalias(rConstitutiveMatrix) = ZeroMatrix(rVoigtSize,rVoigtSize);

  this->CalculateAndAddConstitutiveTensor(Variables, rConstitutiveMatrix);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void NewtonianFluidModel::CalculateAndAddConstitutiveTensor(ElasticDataType &rVariables, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  this->CalculateAndAddConstitutiveTensor(rVariables);

  rConstitutiveMatrix = ConstitutiveModelUtilities::ConstitutiveTensorToMatrix(rVariables.Data.ConstitutiveTensor, rConstitutiveMatrix);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void NewtonianFluidModel::CalculateAndAddConstitutiveTensor(ElasticDataType &rVariables)
{
  KRATOS_TRY

  //Calculate Elastic ConstitutiveMatrix
  const ModelDataType &rModelData = rVariables.GetModelData();
  const MaterialDataType &rMaterial = rModelData.GetMaterialParameters();
  const double &rViscosity = rMaterial.GetLameMu();

  rVariables.Data.ConstitutiveTensor.clear();

  // 3D linear elastic constitutive matrix
  const double diagonal_component = 4.0 * rViscosity / 3.0;
  const double side_component = -0.5 * diagonal_component;

  // 3D linear elastic constitutive matrix
  rVariables.Data.ConstitutiveTensor(0, 0) = diagonal_component;
  rVariables.Data.ConstitutiveTensor(1, 1) = diagonal_component;
  rVariables.Data.ConstitutiveTensor(2, 2) = diagonal_component;

  rVariables.Data.ConstitutiveTensor(3, 3) = rViscosity;
  rVariables.Data.ConstitutiveTensor(4, 4) = rViscosity;
  rVariables.Data.ConstitutiveTensor(5, 5) = rViscosity;

  rVariables.Data.ConstitutiveTensor(0, 1) = side_component;
  rVariables.Data.ConstitutiveTensor(1, 0) = side_component;

  rVariables.Data.ConstitutiveTensor(0, 2) = side_component;
  rVariables.Data.ConstitutiveTensor(2, 0) = side_component;

  rVariables.Data.ConstitutiveTensor(1, 2) = side_component;
  rVariables.Data.ConstitutiveTensor(2, 1) = side_component;

  //initialize to zero other values
  for (unsigned int i = 0; i < 3; ++i)
  {
    for (unsigned int j = 3; j < 6; ++j)
    {
      rVariables.Data.ConstitutiveTensor(i, j) = 0;
      rVariables.Data.ConstitutiveTensor(j, i) = 0;
    }
  }

  rVariables.Data.ConstitutiveTensor(3, 4) = 0.0;
  rVariables.Data.ConstitutiveTensor(3, 5) = 0.0;
  rVariables.Data.ConstitutiveTensor(4, 5) = 0.0;

  rVariables.Data.ConstitutiveTensor(4, 3) = 0.0;
  rVariables.Data.ConstitutiveTensor(5, 3) = 0.0;
  rVariables.Data.ConstitutiveTensor(5, 4) = 0.0;

  rVariables.State().Set(ConstitutiveModelData::CONSTITUTIVE_MATRIX_COMPUTED);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void NewtonianFluidModel::CalculateStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  ElasticDataType Variables;
  this->InitializeVariables(rValues, Variables);

  //Set constitutive matrix to zero before adding
  const SizeType &rVoigtSize = Variables.GetModelData().GetVoigtSize();
  noalias(rConstitutiveMatrix) = ZeroMatrix(rVoigtSize,rVoigtSize);

  this->CalculateAndAddConstitutiveTensor(Variables, rConstitutiveMatrix);

  VectorType StressVector;
  this->CalculateAndAddStressTensor(Variables, StressVector);

  rStressMatrix = ConstitutiveModelUtilities::StressVectorToTensor(StressVector, rStressMatrix);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void NewtonianFluidModel::CalculateIsochoricStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling the base class function in NewtonianFluidModel ... illegal operation" << std::endl;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void NewtonianFluidModel::CalculateVolumetricStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling the base class function in NewtonianFluidModel ... illegal operation" << std::endl;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void NewtonianFluidModel::CalculateIsochoricConstitutiveTensor(ModelDataType &rValues, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  ElasticDataType Variables;
  this->InitializeVariables(rValues, Variables);
  this->CalculateAndAddIsochoricConstitutiveTensor(Variables, rConstitutiveMatrix);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void NewtonianFluidModel::CalculateAndAddIsochoricConstitutiveTensor(ElasticDataType &rVariables, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  this->CalculateAndAddIsochoricConstitutiveTensor(rVariables);

  rConstitutiveMatrix = ConstitutiveModelUtilities::ConstitutiveTensorToMatrix(rVariables.Data.ConstitutiveTensor, rConstitutiveMatrix);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void NewtonianFluidModel::CalculateAndAddIsochoricConstitutiveTensor(ElasticDataType &rVariables)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling the base class function in NewtonianFluidModel ... illegal operation" << std::endl;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void NewtonianFluidModel::CalculateVolumetricConstitutiveTensor(ModelDataType &rValues, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  ElasticDataType Variables;
  this->InitializeVariables(rValues, Variables);
  this->CalculateAndAddVolumetricConstitutiveTensor(Variables, rConstitutiveMatrix);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void NewtonianFluidModel::CalculateAndAddVolumetricConstitutiveTensor(ElasticDataType &rVariables, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  this->CalculateAndAddVolumetricConstitutiveTensor(rVariables);

  rConstitutiveMatrix = ConstitutiveModelUtilities::ConstitutiveTensorToMatrix(rVariables.Data.ConstitutiveTensor, rConstitutiveMatrix);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void NewtonianFluidModel::CalculateAndAddVolumetricConstitutiveTensor(ElasticDataType &rVariables)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling the base class function in NewtonianFluidModel ... illegal operation" << std::endl;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void NewtonianFluidModel::CalculateInternalVariables(ModelDataType &rValues)
{
  KRATOS_TRY

  ElasticDataType Variables;
  this->InitializeVariables(rValues, Variables);

  // set requested internal variables
  this->SetInternalVariables(rValues, Variables);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

// set requested internal variables
void NewtonianFluidModel::SetInternalVariables(ModelDataType &rValues, ElasticDataType &rVariables)
{
  KRATOS_TRY

  if (rValues.InternalVariable.Is(EFFECTIVE_VISCOSITY))
  {
    //supply internal variable
    const ModelDataType &rModelData = rVariables.GetModelData();
    const MaterialDataType &rMaterial = rModelData.GetMaterialParameters();
    double EffectiveViscosity = rMaterial.GetLameMu();
    rValues.InternalVariable.SetValue(EFFECTIVE_VISCOSITY, EffectiveViscosity);
    //const Properties &rProperties = rModelData.GetProperties();
    //rValues.InternalVariable.SetValue(EFFECTIVE_VISTOSITY, rProperties[DYNAMIC_VISTOSITY]);
  }

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

int NewtonianFluidModel::Check(const Properties &rProperties, const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  if (!rProperties.Has(DYNAMIC_VISCOSITY) || rProperties[DYNAMIC_VISCOSITY] <= 0.00)
    KRATOS_ERROR << "Missing DYNAMIC_VISCOSITY property for NewtonianFluid3DLaw" << std::endl;
  else if (rProperties[DYNAMIC_VISCOSITY] <= 0.00)
    KRATOS_ERROR << "Incorrect DYNAMIC_VISCOSITY provided in process info for NewtonianFluid3DLaw: " << rProperties[DYNAMIC_VISCOSITY] << std::endl;

  return 0;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void NewtonianFluidModel::GetRequiredProperties(Properties &rProperties)
{
  KRATOS_TRY

  // Set required properties to check keys
  double value = 0.0; //dummy
  rProperties.SetValue(DYNAMIC_VISCOSITY, value);
  rProperties.SetValue(DENSITY, value);

  KRATOS_CATCH(" ")
}

} // Namespace Kratos
