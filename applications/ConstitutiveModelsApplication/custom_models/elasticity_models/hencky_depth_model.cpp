//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:                  April 2017 $
//
//

// System includes

// External includes

// Project includes
#include "custom_models/elasticity_models/hencky_depth_model.hpp"

namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

HenckyDepthModel::HenckyDepthModel()
    : HenckyHyperElasticModel()
{
   mShearModulus = 0.0;
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

HenckyDepthModel::HenckyDepthModel(const HenckyDepthModel &rOther)
    : HenckyHyperElasticModel(rOther), mShearModulus(rOther.mShearModulus)
{
}

//********************************CLONE***********************************************
//************************************************************************************

ConstitutiveModel::Pointer HenckyDepthModel::Clone() const
{
   return Kratos::make_shared<HenckyDepthModel>(*this);
}

//********************************ASSIGNMENT******************************************
//************************************************************************************
HenckyDepthModel &HenckyDepthModel::operator=(HenckyDepthModel const &rOther)
{
   HenckyHyperElasticModel::operator=(rOther);
   mShearModulus = rOther.mShearModulus;
   return *this;
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

HenckyDepthModel::~HenckyDepthModel()
{
}

double & HenckyDepthModel::GetValue( const Variable<double> & rThisVariable, double & rValue)
{
   KRATOS_TRY

   if ( rThisVariable == SHEAR_MODULUS) {
      rValue = mShearModulus;
   } else {
      rValue = HenckyHyperElasticModel::GetValue( rThisVariable, rValue);
   }
   return rValue;

   KRATOS_CATCH("")
}

// Set Get
void HenckyDepthModel::SetValue(const Variable<double> &rThisVariable, const double &rValue, const ProcessInfo &rCurrentProcessInfo) 
{
   KRATOS_TRY


   if ( rThisVariable == SHEAR_MODULUS)
   {
      mShearModulus = rValue;
   } else {
      HenckyHyperElasticModel::SetValue( rThisVariable, rValue, rCurrentProcessInfo);
   }

   KRATOS_CATCH("")
}

void HenckyDepthModel::SetValue(const Variable<Vector> &rThisVariable, const Vector &rValue, const ProcessInfo &rCurrentProcessInfo) 
{
   KRATOS_TRY
   
   HenckyHyperElasticModel::SetValue( rThisVariable, rValue, rCurrentProcessInfo);

   KRATOS_CATCH("")
}



// ****************************************************************
// ***************************************************************
// not in the correct place
void HenckyDepthModel::SeparateVolumetricAndDeviatoricPart(const MatrixType &rA, double &rVolumetric, MatrixType &rDev, double &devNorm)
{
   KRATOS_TRY

   rVolumetric = 0;
   for (unsigned int i = 0; i < 3; i++)
      rVolumetric += rA(i, i);

   noalias(rDev) = rA;
   for (unsigned int i = 0; i < 3; i++)
      rDev(i, i) -= rVolumetric / 3.0;

   devNorm = 0;
   for (unsigned int i = 0; i < 3; i++)
      for (unsigned int j = 0; j < 3; j++)
         devNorm += pow(rDev(i, j), 2);

   devNorm = sqrt(devNorm);

   KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************
// CalculateAndAddStressTensor
void HenckyDepthModel::CalculateAndAddStressTensor(ElasticDataType &rVariables, MatrixType &rStressMatrix)
{
   KRATOS_TRY

   // material parameters

   const ModelDataType &rModelData = rVariables.GetModelData();
   const Properties &rMaterialProperties = rModelData.GetProperties();

   const double &rPoissonRatio = rMaterialProperties[POISSON_RATIO];

   double ShearModulus = mShearModulus;
   if ( ShearModulus < 1E-8)
      ShearModulus = rMaterialProperties[SHEAR_MODULUS];
   double BulkModulus = 2.0*ShearModulus * ( 1.0+rPoissonRatio) / 3.0 / (1.0 - 2.0 * rPoissonRatio);

   MatrixType HenckyStrain(3, 3);
   HenckyStrain = rVariables.Data.Strain.Matrix;

   if (this->mSetStressState)
   {
      this->mSetStressState = false;
      const double YoungModulus = 2.0*ShearModulus*(1.0+rPoissonRatio);
      SetStressState(HenckyStrain, YoungModulus, rPoissonRatio);
   }

   // 2.a Separate Volumetric and deviatoric part
   double VolumetricHencky;
   MatrixType DeviatoricHencky(3, 3);
   noalias(DeviatoricHencky) = ZeroMatrix(3, 3);
   double deviatoricNorm;

   SeparateVolumetricAndDeviatoricPart(HenckyStrain, VolumetricHencky, DeviatoricHencky, deviatoricNorm);

   // 3.a Compute Deviatoric Part
   rStressMatrix.clear();
   rStressMatrix += DeviatoricHencky * 2.0 * ShearModulus;

   // 3.b Compute Volumetric Part
   double pressure = VolumetricHencky * BulkModulus;

   for (unsigned int i = 0; i < 3; i++)
      rStressMatrix(i, i) += pressure;

   KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************
// CalculateAndAddConstitutiveTensor
void HenckyDepthModel::CalculateAndAddConstitutiveTensor(ElasticDataType &rVariables, Matrix &rConstitutiveMatrix)
{
   KRATOS_TRY

   // material parameters
   const ModelDataType &rModelData = rVariables.GetModelData();
   const Properties &rMaterialProperties = rModelData.GetProperties();

   const double &rNu = rMaterialProperties[POISSON_RATIO];
   double ShearModulus = mShearModulus;
     
   const double Young = 2.0*ShearModulus*(1.0+rNu);
   if ( ShearModulus < 1E-8)
      ShearModulus = rMaterialProperties[SHEAR_MODULUS];

   double diagonal = Young / (1.0 + rNu) / (1.0 - 2.0 * rNu) * (1.0 - rNu);
   double nodiagonal = Young / (1.0 + rNu) / (1.0 - 2.0 * rNu) * (rNu);
   double corte = Young / (1.0 + rNu) / 2.0;

   rConstitutiveMatrix.clear();

   for (unsigned int i = 0; i < 3; ++i)
   {
      for (unsigned int j = 0; j < 3; ++j)
      {
         if (i == j)
         {
            rConstitutiveMatrix(i, i) = diagonal;
         }
         else
         {
            rConstitutiveMatrix(i, j) = nodiagonal;
         }
      }
   }

   for (unsigned int j = 3; j < 6; ++j)
      rConstitutiveMatrix(j, j) = corte;

   rVariables.State().Set(ConstitutiveModelData::CONSTITUTIVE_MATRIX_COMPUTED);

   KRATOS_CATCH("")
}

// *********************************************************************************
// Set Stress state
void HenckyDepthModel::SetStressState(MatrixType &rHenckyStrain, const double &rE, const double &rNu)
{

   KRATOS_TRY
   Matrix StressMat(3, 3);
   noalias(StressMat) = ZeroMatrix(3, 3);

   for (int i = 0; i < 3; ++i)
      StressMat(i, i) = this->mInitialStressState(i);

   Vector EigenStress(3);
   MatrixType EigenStressM;
   MatrixType EigenV;
   noalias(EigenStressM) = ZeroMatrix(3, 3);
   noalias(EigenV) = ZeroMatrix(3, 3);
   MatrixType OriginalHencky;
   noalias(OriginalHencky) = ZeroMatrix(3, 3);
   OriginalHencky = rHenckyStrain;

   //SolidMechanicsMathUtilities<double>::EigenVectors( StressMat, EigenV, EigenStress);
   MathUtils<double>::GaussSeidelEigenSystem(StressMat, EigenV, EigenStressM);
   for (unsigned int i = 0; i < 3; i++)
      EigenStress(i) = EigenStressM(i, i);

   Vector ElasticHenckyStrain(3);
   Matrix InverseElastic(3, 3);
   const double &YoungModulus = rE;
   const double &PoissonCoef = rNu;

   for (unsigned int i = 0; i < 3; ++i)
   {
      for (unsigned int j = 0; j < 3; ++j)
      {
         if (i == j)
         {
            InverseElastic(i, i) = 1.0 / YoungModulus;
         }
         else
         {
            InverseElastic(i, j) = -PoissonCoef / YoungModulus;
         }
      }
   }

   noalias(ElasticHenckyStrain) = prod(InverseElastic, EigenStress);

   MatrixType ElasticLeftCauchy(3, 3);
   noalias(ElasticLeftCauchy) = ZeroMatrix(3, 3);
   noalias(rHenckyStrain) = ZeroMatrix(3, 3);

   for (unsigned int i = 0; i < 3; ++i)
   {
      ElasticLeftCauchy(i, i) = std::exp(2.0 * ElasticHenckyStrain(i));
      rHenckyStrain(i, i) = ElasticHenckyStrain(i);
   }

   noalias(ElasticLeftCauchy) = prod(EigenV, ElasticLeftCauchy);
   noalias(ElasticLeftCauchy) = prod(ElasticLeftCauchy, trans(EigenV));

   noalias(rHenckyStrain) = prod(EigenV, rHenckyStrain);
   noalias(rHenckyStrain) = prod(rHenckyStrain, trans(EigenV));

   rHenckyStrain += OriginalHencky;

   Vector NewHistoryVector(6);
   NewHistoryVector = ConstitutiveModelUtilities::StrainTensorToVector(ElasticLeftCauchy, NewHistoryVector);
   *this->mpHistoryVector = NewHistoryVector;

   KRATOS_CATCH("")
}

} // Namespace Kratos
