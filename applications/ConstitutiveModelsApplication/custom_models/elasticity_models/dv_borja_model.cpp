//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:                  April 2017 $
//
//

// System includes

// External includes

// Project includes
#include "custom_models/elasticity_models/dv_borja_model.hpp"

namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

DVBorjaModel::DVBorjaModel()
    : BorjaModel()
{
   mShearModulus = 0.0;
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

DVBorjaModel::DVBorjaModel(const DVBorjaModel &rOther)
    : BorjaModel(rOther), mShearModulus(rOther.mShearModulus)
{
}

//********************************CLONE***********************************************
//************************************************************************************

ConstitutiveModel::Pointer DVBorjaModel::Clone() const
{
   return Kratos::make_shared<DVBorjaModel>(*this);
}

//********************************ASSIGNMENT******************************************
//************************************************************************************
DVBorjaModel &DVBorjaModel::operator=(DVBorjaModel const &rOther)
{
   BorjaModel::operator=(rOther);
   mShearModulus = rOther.mShearModulus;
   return *this;
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

DVBorjaModel::~DVBorjaModel()
{
}


void DVBorjaModel::SetValue(const Variable<double> &rThisVariable,
      const double &rValue,
      const ProcessInfo &rCurrentProcessInfo)
{
   KRATOS_TRY


   if ( rThisVariable == SHEAR_MODULUS)
   {
      mShearModulus = rValue;
   } else {
      HenckyHyperElasticModel::SetValue( rThisVariable, rValue, rCurrentProcessInfo);
   }

   KRATOS_CATCH("")
}

void DVBorjaModel::SetValue(const Variable<Vector> &rThisVariable,
      const Vector &rValue,
      const ProcessInfo &rCurrentProcessInfo)
{
   KRATOS_TRY
   
   HenckyHyperElasticModel::SetValue( rThisVariable, rValue, rCurrentProcessInfo);

   KRATOS_CATCH("")
}

double &DVBorjaModel::GetValue(const Variable<double> &rThisVariable,
      double &rValue)
{
   KRATOS_TRY

   if ( rThisVariable == SHEAR_MODULUS) {
      rValue = mShearModulus;
   } else {
      rValue = HenckyHyperElasticModel::GetValue( rThisVariable, rValue);
   }
   return rValue;

   KRATOS_CATCH("")
}



double DVBorjaModel::GetShearModulus(const Properties & rProperties)
{
   KRATOS_TRY

   return this->mShearModulus;

   KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

int DVBorjaModel::Check(const Properties &rProperties, const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  return 0;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void DVBorjaModel::GetRequiredProperties(Properties &rProperties)
{
  KRATOS_TRY

  // Set required properties to check keys
  double value = 0.0; //dummy
  rProperties.SetValue(SWELLING_SLOPE, value);
  rProperties.SetValue(ALPHA_SHEAR, value);
  //rProperties.SetValue(PRE_CONSOLIDATION_STRESS, value);
  rProperties.SetValue(INITIAL_SHEAR_MODULUS, value);

  KRATOS_CATCH(" ")
}

} // Namespace Kratos
