//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:              September 2022 $
//
//

#if !defined(KRATOS_ISOCHORIC_HYPO_ELASTIC_GREEN_NAGHDI_MODEL_HPP_INCLUDED)
#define KRATOS_ISOCHORIC_HYPO_ELASTIC_GREEN_NAGHDI_MODEL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/elasticity_models/hypo_elastic_green_naghdi_model.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) IsochoricHypoElasticGreenNaghdiModel : public HypoElasticGreenNaghdiModel
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of IsochoricHypoElasticGreenNaghdiModel
  KRATOS_CLASS_POINTER_DEFINITION(IsochoricHypoElasticGreenNaghdiModel);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  IsochoricHypoElasticGreenNaghdiModel() : HypoElasticGreenNaghdiModel() {}

  /// Copy constructor.
  IsochoricHypoElasticGreenNaghdiModel(IsochoricHypoElasticGreenNaghdiModel const &rOther) : HypoElasticGreenNaghdiModel(rOther) {}

  /// Assignment operator.
  IsochoricHypoElasticGreenNaghdiModel &operator=(IsochoricHypoElasticGreenNaghdiModel const &rOther)
  {
    HypoElasticGreenNaghdiModel::operator=(rOther);
    return *this;
  }

  /// Clone.
  ConstitutiveModel::Pointer Clone() const override
  {
    return Kratos::make_shared<IsochoricHypoElasticGreenNaghdiModel>(*this);
  }

  /// Destructor.
  ~IsochoricHypoElasticGreenNaghdiModel() override {}

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  void CalculateStrainEnergy(ModelDataType &rValues, double &rDensityFunction) override
  {
    KRATOS_TRY

    ElasticDataType Variables;
    this->InitializeVariables(rValues, Variables);

    rDensityFunction = 0;
    this->CalculateAndAddIsochoricStrainEnergy(Variables, rDensityFunction);
    this->CalculateAndAddVolumetricStrainEnergy(Variables, rDensityFunction);

    KRATOS_CATCH(" ")
  }

  void CalculateStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix) override
  {
    KRATOS_TRY

    ElasticDataType Variables;
    this->InitializeVariables(rValues, Variables);

    this->CalculateAndAddConstitutiveTensor(Variables);

    VectorType StressVector;
    noalias(StressVector) = ZeroVector(6);
    this->CalculateAndAddIsochoricStressTensor(Variables, StressVector);

    rValues.StressMatrix = ConstitutiveModelUtilities::VectorToSymmetricTensor(StressVector, rValues.StressMatrix); //store isochoric stress matrix as StressMatrix

    this->CalculateAndAddVolumetricStressTensor(Variables, StressVector);

    rStressMatrix = ConstitutiveModelUtilities::VectorToSymmetricTensor(StressVector, rStressMatrix);

    // Isochoric stress stored in the HistoryVector
    this->AddHistoricalStress(rValues, rStressMatrix);

    Variables.State().Set(ConstitutiveModelData::STRESS_COMPUTED);

    KRATOS_CATCH(" ")
  }

  void CalculateStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix) override
  {
    KRATOS_TRY

    ElasticDataType Variables;
    this->InitializeVariables(rValues, Variables);

    //Calculate Constitutive Matrix
    this->CalculateAndAddConstitutiveTensor(Variables, rConstitutiveMatrix);

    //Calculate Stress Matrix
    VectorType StressVector;
    noalias(StressVector) = ZeroVector(6);
    this->CalculateAndAddIsochoricStressTensor(Variables, StressVector);

    rValues.StressMatrix = ConstitutiveModelUtilities::VectorToSymmetricTensor(StressVector, rValues.StressMatrix); //store isochoric stress matrix as StressMatrix

    this->CalculateAndAddVolumetricStressTensor(Variables, StressVector);

    rStressMatrix = ConstitutiveModelUtilities::VectorToSymmetricTensor(StressVector, rStressMatrix);

    // Isochoric stress stored in the HistoryVector
    this->AddHistoricalStress(rValues, rStressMatrix);

    Variables.State().Set(ConstitutiveModelData::STRESS_COMPUTED);

    KRATOS_CATCH(" ")
  }

  /**
     * Check
     */
  int Check(const Properties &rProperties, const ProcessInfo &rCurrentProcessInfo) override
  {
    KRATOS_TRY

    return HypoElasticGreenNaghdiModel::Check(rProperties, rCurrentProcessInfo);

    KRATOS_CATCH(" ")
  }


  void CalculateAndAddIsochoricStressTensor(ElasticDataType &rVariables, VectorType &rStressVector) override
  {
    KRATOS_TRY

    //total stress
    VectorType StressVector;
    this->CalculateAndAddStressTensor(rVariables, StressVector);

    //deviatoric stress
    double MeanStress = (1.0 / 3.0) * (StressVector[0] + StressVector[1] + StressVector[2]);
    for (unsigned int i = 0; i < 3; i++)
      StressVector[i] -= MeanStress;

    rStressVector += StressVector;

    KRATOS_CATCH(" ")
  }

  void CalculateAndAddVolumetricStressTensor(ElasticDataType &rVariables, VectorType &rStressVector) override
  {
    KRATOS_TRY

    //total stress
    VectorType StressVector;
    this->CalculateAndAddStressTensor(rVariables,StressVector);

    //volumetric stress
    double MeanStress = (1.0 / 3.0) * (StressVector[0] + StressVector[1] + StressVector[2]);
    for (unsigned int i = 0; i < 3; i++)
      rStressVector[i] += MeanStress;

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "IsochoricHypoElasticGreenNaghdiModel";
    return buffer.str();
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "IsochoricHypoElasticGreenNaghdiModel";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "IsochoricHypoElasticGreenNaghdiModel Data";
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  void CalculateAndAddIsochoricStrainEnergy(ElasticDataType &rVariables, double &rIsochoricDensityFunction) override
  {
    KRATOS_TRY

    KRATOS_ERROR << "calling the class function in IsochoricHypoElasticGreenNaghdiModel ... illegal operation" << std::endl;

    KRATOS_CATCH(" ")
  }

  void CalculateAndAddVolumetricStrainEnergy(ElasticDataType &rVariables, double &rVolumetricDensityFunction) override
  {
    KRATOS_TRY

    KRATOS_ERROR << "calling the class function in IsochoricHypoElasticGreenNaghdiModel ... illegal operation" << std::endl;

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, HypoElasticGreenNaghdiModel)
  }

  void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, HypoElasticGreenNaghdiModel)
  }

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class IsochoricHypoElasticGreenNaghdiModel

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_ISOCHORIC_HYPO_ELASTIC_GREEN_NAGHDI_MODEL_HPP_INCLUDED  defined
