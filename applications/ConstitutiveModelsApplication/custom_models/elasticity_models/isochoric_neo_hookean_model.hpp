//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                  April 2017 $
//
//

#if !defined(KRATOS_ISOCHORIC_NEO_HOOKEAN_MODEL_HPP_INCLUDED)
#define KRATOS_ISOCHORIC_NEO_HOOKEAN_MODEL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/elasticity_models/isochoric_mooney_rivlin_model.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) IsochoricNeoHookeanModel : public IsochoricMooneyRivlinModel
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of IsochoricNeoHookeanModel
  KRATOS_CLASS_POINTER_DEFINITION(IsochoricNeoHookeanModel);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  IsochoricNeoHookeanModel() : IsochoricMooneyRivlinModel() {}

  /// Copy constructor.
  IsochoricNeoHookeanModel(IsochoricNeoHookeanModel const &rOther) : IsochoricMooneyRivlinModel(rOther) {}

  /// Assignment operator.
  IsochoricNeoHookeanModel &operator=(IsochoricNeoHookeanModel const &rOther)
  {
    IsochoricMooneyRivlinModel::operator=(rOther);
    return *this;
  }

  /// Clone.
  ConstitutiveModel::Pointer Clone() const override
  {
    return Kratos::make_shared<IsochoricNeoHookeanModel>(*this);
  }

  /// Destructor.
  ~IsochoricNeoHookeanModel() override {}

  ///@}
  ///@name Operators
  ///@{
  ///@}
  ///@name Operations
  ///@{

  /**
     * Check
     */

  int Check(const Properties &rProperties, const ProcessInfo &rCurrentProcessInfo) override
  {
    KRATOS_TRY

    HyperElasticModel::Check(rProperties, rCurrentProcessInfo);

    if (C10.Key() == 0 || rProperties[C10] <= 0.00)
      KRATOS_ERROR << "C10 has an invalid key or value" << std::endl;

    if (BULK_MODULUS.Key() == 0 || rProperties[BULK_MODULUS] <= 0.00)
      KRATOS_ERROR << "BULK_MODULUS has an invalid key or value" << std::endl;

    return 0;

    KRATOS_CATCH(" ")
  }

  /**
     * Get required properties
     */
  void GetRequiredProperties(Properties &rProperties) override
  {
    KRATOS_TRY

    // Set required properties to check keys
    double value = 0.0; //dummy
    rProperties.SetValue(C10,value);
    rProperties.SetValue(BULK_MODULUS,value);

    KRATOS_CATCH(" ")
  }

  // Specialized method instead of the general one (faster)
  virtual void CalculateAndAddIsochoricStressTensor(ElasticDataType &rVariables, MatrixType &rStressMatrix) override
  {
    KRATOS_TRY

    const ModelDataType &rModelData = rVariables.GetModelData();
    const StressMeasureType &rStressMeasure = rModelData.GetStressMeasure();

    MatrixType StressMatrix;
    const MaterialDataType &rMaterial = rVariables.GetMaterialParameters();

    double trace = -1.0 / 3.0 * (rVariables.Data.Strain.Matrix(0, 0) + rVariables.Data.Strain.Matrix(1, 1) + rVariables.Data.Strain.Matrix(2, 2));

    if (rStressMeasure == ConstitutiveModelData::StressMeasureType::StressMeasure_PK2)
    { //Variables.Data.Strain.Matrix = RightCauchyGreen (C)

      StressMatrix = msIdentityMatrix;
      StressMatrix += trace * rVariables.Data.Strain.InverseMatrix;
      StressMatrix *= rMaterial.GetLameMu() * rVariables.Data.Strain.Invariants.J_13 * rVariables.Data.Strain.Invariants.J_13;

      rStressMatrix += StressMatrix;
    }
    else if (rStressMeasure == ConstitutiveModelData::StressMeasureType::StressMeasure_Kirchhoff)
    { //Variables.Data.Strain.Matrix = LeftCauchyGreen (b)

      StressMatrix = rVariables.Data.Strain.Matrix;
      StressMatrix += trace * msIdentityMatrix;
      StressMatrix *= rMaterial.GetLameMu() * rVariables.Data.Strain.Invariants.J_13 * rVariables.Data.Strain.Invariants.J_13;

      rStressMatrix += StressMatrix;
    }

    KRATOS_CATCH(" ")
  }

  // Specialized method instead of the general one (faster) needs compatibility for incompressible law (BulkFactor Method)
  virtual void CalculateAndAddVolumetricStressTensor(ElasticDataType &rVariables, MatrixType &rStressMatrix) override
  {
    KRATOS_TRY

    const ModelDataType &rModelData = rVariables.GetModelData();
    const StressMeasureType &rStressMeasure = rModelData.GetStressMeasure();

    MatrixType StressMatrix;

    double Factor = 0;
    this->CalculatePressureFactor(rVariables, Factor);

    if (rStressMeasure == ConstitutiveModelData::StressMeasureType::StressMeasure_PK2)
    { //Variables.Data.Strain.Matrix = RightCauchyGreen (C)

      StressMatrix = Factor * rVariables.Data.Strain.InverseMatrix;

      rStressMatrix += StressMatrix;
    }
    else if (rStressMeasure == ConstitutiveModelData::StressMeasureType::StressMeasure_Kirchhoff)
    { //Variables.Data.Strain.Matrix = LeftCauchyGreen (b)

      StressMatrix = Factor * msIdentityMatrix;

      rStressMatrix += StressMatrix;
    }

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "IsochoricNeoHookeanModel";
    return buffer.str();
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "IsochoricNeoHookeanModel";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "IsochoricNeoHookeanModel Data";
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{

  //specialized methods:

  virtual void CalculateVolumetricFactor(ElasticDataType &rVariables, double &rFactor)
  {
    KRATOS_TRY

    rFactor = 0.5 * (rVariables.Data.Strain.Invariants.I3 - 1.0);

    KRATOS_CATCH(" ")
  }

  virtual void CalculatePressureFactor(ElasticDataType &rVariables, double &rFactor)
  {
    KRATOS_TRY

    this->CalculateVolumetricFactor(rVariables, rFactor);

    this->AddVolumetricThermalFactor(rVariables, rFactor);

    rFactor *= rVariables.GetMaterialParameters().GetBulkModulus();

    KRATOS_CATCH(" ")
  }

  virtual void AddVolumetricThermalFactor(ElasticDataType &rVariables, double &rFactor)
  {
    KRATOS_TRY

    const ModelDataType &rModelData = rVariables.GetModelData();
    const Properties &rProperties = rModelData.GetProperties();

    //Thermal constants
    double ThermalExpansionCoefficient = 0;
    double ReferenceTemperature = 0;

    if (rProperties.Has(THERMAL_EXPANSION_COEFFICIENT))
      ThermalExpansionCoefficient = rProperties[THERMAL_EXPANSION_COEFFICIENT];

    if (rProperties.Has(REFERENCE_TEMPERATURE))
      ReferenceTemperature = rProperties[REFERENCE_TEMPERATURE];

    const double &rTemperature = rModelData.GetTemperature();
    double DeltaTemperature = 0;
    if (rTemperature != 0)
      DeltaTemperature = rTemperature - ReferenceTemperature;

    //Thermal volumetric factor:
    rFactor -= 3.0 * ThermalExpansionCoefficient * rFactor * DeltaTemperature;

    KRATOS_CATCH(" ")
  }


  virtual void CalculateConstitutiveMatrixFactor(ElasticDataType &rVariables, double &rFactor)
  {
    KRATOS_TRY

    rFactor = rVariables.Data.Strain.Invariants.I3;

    KRATOS_CATCH(" ")
  }

  virtual void CalculateConstitutiveMatrixPressureFactor(ElasticDataType &rVariables, double &rFactor)
  {
    KRATOS_TRY

    rFactor = rVariables.GetMaterialParameters().GetBulkModulus();

    KRATOS_CATCH(" ")
  }

  // SPECIALIZED METHODS:
  // {

  // Specialized method instead of the general one (faster) this calculation is not needed
  void CalculateScalingFactors(ElasticDataType &rVariables) override
  {
    KRATOS_TRY

    // MooneyRivlinModel::CalculateScalingFactors(rVariables);

    // rVariables.Data.Factors.Alpha4 = this->GetVolumetricFunction1stJDerivative(rVariables,rVariables.Data.Factors.Alpha4);
    // rVariables.Data.Factors.Beta4  = this->GetVolumetricFunction2ndJDerivative(rVariables,rVariables.Data.Factors.Beta4);

    KRATOS_CATCH(" ")
  }


  // Specialized method instead of the general one (faster)
  virtual double &AddIsochoricConstitutiveComponent(ElasticDataType &rVariables, double &rCabcd,
                                                    const unsigned int &a, const unsigned int &b,
                                                    const unsigned int &c, const unsigned int &d) override
  {
    KRATOS_TRY

    double Cabcd = 0;

    const MaterialDataType &rMaterial = rVariables.GetMaterialParameters();

    const ModelDataType &rModelData = rVariables.GetModelData();
    const StressMeasureType &rStressMeasure = rModelData.GetStressMeasure();
    const MatrixType &rIsochoricStressMatrix = rModelData.GetStressMatrix();

    if (rStressMeasure == ConstitutiveModelData::StressMeasureType::StressMeasure_PK2)
    { //mStrainMatrix = RightCauchyGreen (C)

      Cabcd = (1.0 / 3.0) * (rVariables.Data.Strain.InverseMatrix(a, b) * rVariables.Data.Strain.InverseMatrix(d, c));

      Cabcd -= 0.5 * (rVariables.Data.Strain.InverseMatrix(a, c) * rVariables.Data.Strain.InverseMatrix(b, d) + rVariables.Data.Strain.InverseMatrix(a, d) * rVariables.Data.Strain.InverseMatrix(b, c));

      Cabcd *= rMaterial.GetLameMu() * (rVariables.Data.Strain.Matrix(0, 0) + rVariables.Data.Strain.Matrix(1, 1) + rVariables.Data.Strain.Matrix(2, 2)) * rVariables.Data.Strain.Invariants.J_13 * rVariables.Data.Strain.Invariants.J_13;

      Cabcd += (rVariables.Data.Strain.InverseMatrix(c, d) * rIsochoricStressMatrix(a, b) + rIsochoricStressMatrix(c, d) * rVariables.Data.Strain.InverseMatrix(a, b));

      Cabcd *= (-2.0 / 3.0);
    }
    else if (rStressMeasure == ConstitutiveModelData::StressMeasureType::StressMeasure_Kirchhoff)
    { //mStrainMatrix = LeftCauchyGreen (b)

      Cabcd = (1.0 / 3.0) * (msIdentityMatrix(a, b) * msIdentityMatrix(c, d));

      Cabcd -= 0.5 * (msIdentityMatrix(a, c) * msIdentityMatrix(b, d) + msIdentityMatrix(a, d) * msIdentityMatrix(b, c));

      Cabcd *= rMaterial.GetLameMu() * (rVariables.Data.Strain.Matrix(0, 0) + rVariables.Data.Strain.Matrix(1, 1) + rVariables.Data.Strain.Matrix(2, 2)) * rVariables.Data.Strain.Invariants.J_13 * rVariables.Data.Strain.Invariants.J_13;

      Cabcd += (msIdentityMatrix(c, d) * rIsochoricStressMatrix(a, b) + rIsochoricStressMatrix(c, d) * msIdentityMatrix(a, b));

      Cabcd *= (-2.0 / 3.0);
    }

    rCabcd += Cabcd;

    rVariables.State().Set(ConstitutiveModelData::CONSTITUTIVE_MATRIX_COMPUTED);

    return rCabcd;

    KRATOS_CATCH(" ")
  }

  // Specialized method instead of the general one (faster) needs compatibility for incompressible law (BulkFactor Method)
  virtual double &AddVolumetricConstitutiveComponent(ElasticDataType &rVariables, double &rCabcd,
                                                     const unsigned int &a, const unsigned int &b,
                                                     const unsigned int &c, const unsigned int &d) override
  {
    KRATOS_TRY

    double Cabcd = 0;

    const ModelDataType &rModelData = rVariables.GetModelData();
    const StressMeasureType &rStressMeasure = rModelData.GetStressMeasure();

    double FactorA = 0;
    this->CalculateConstitutiveMatrixFactor(rVariables, FactorA);

    double FactorB = 0;
    this->CalculateVolumetricFactor(rVariables, FactorB);

    double FactorC = 0;
    this->CalculateConstitutiveMatrixPressureFactor(rVariables, FactorC);

    if (rStressMeasure == ConstitutiveModelData::StressMeasureType::StressMeasure_PK2)
    { //mStrainMatrix = RightCauchyGreen (C)

      Cabcd = FactorA * (rVariables.Data.Strain.InverseMatrix(a, b) * rVariables.Data.Strain.InverseMatrix(c, d));

      Cabcd -= FactorB * (rVariables.Data.Strain.InverseMatrix(a, c) * rVariables.Data.Strain.InverseMatrix(b, d) + rVariables.Data.Strain.InverseMatrix(a, d) * rVariables.Data.Strain.InverseMatrix(b, c));

      Cabcd *= FactorC;
    }
    else if (rStressMeasure == ConstitutiveModelData::StressMeasureType::StressMeasure_Kirchhoff)
    { //mStrainMatrix = LeftCauchyGreen (b)

      Cabcd = FactorA * (msIdentityMatrix(a, b) * msIdentityMatrix(c, d));

      Cabcd -= FactorB * (msIdentityMatrix(a, c) * msIdentityMatrix(b, d) + msIdentityMatrix(a, d) * msIdentityMatrix(b, c));

      Cabcd *= FactorC;
    }

    rCabcd += Cabcd;

    rVariables.State().Set(ConstitutiveModelData::CONSTITUTIVE_MATRIX_COMPUTED);

    return rCabcd;

    KRATOS_CATCH(" ")
  }

  // } SPECIALIED METHODS END

  //************// W

  void CalculateAndAddIsochoricStrainEnergy(ElasticDataType &rVariables, double &rIsochoricDensityFunction) override
  {
    KRATOS_TRY

    const MaterialDataType &rMaterial = rVariables.GetMaterialParameters();

    rIsochoricDensityFunction += rMaterial.GetModelParameters()[0] * (rVariables.Data.Strain.Invariants.J_13 * rVariables.Data.Strain.Invariants.I1 - 3.0);

    KRATOS_CATCH(" ")
  }

  void CalculateAndAddVolumetricStrainEnergy(ElasticDataType &rVariables, double &rVolumetricDensityFunction) override
  {
    KRATOS_TRY

    const MaterialDataType &rMaterial = rVariables.GetMaterialParameters();

    //energy function "U(J) = (K/4)*(J²-1) - (K/2)*lnJ"
    rVolumetricDensityFunction += rMaterial.GetBulkModulus() * 0.25 * (rVariables.Data.Strain.Invariants.J * rVariables.Data.Strain.Invariants.J - 1.0);
    rVolumetricDensityFunction -= rMaterial.GetBulkModulus() * 0.5 * std::log(rVariables.Data.Strain.Invariants.J);

    KRATOS_CATCH(" ")
  }

  //************// dW

  double &GetFunction1stI1Derivative(ElasticDataType &rVariables, double &rDerivative) override //dW/dI1
  {
    KRATOS_TRY

    const MaterialDataType &rMaterial = rVariables.GetMaterialParameters();

    rDerivative = rMaterial.GetModelParameters()[0];

    return rDerivative;

    KRATOS_CATCH(" ")
  }

  double &GetFunction1stI2Derivative(ElasticDataType &rVariables, double &rDerivative) override //dW/dI2
  {
    KRATOS_TRY

    rDerivative = 0.0;

    return rDerivative;

    KRATOS_CATCH(" ")
  }

  double &GetFunction1stI3Derivative(ElasticDataType &rVariables, double &rDerivative) override //dW/dI3
  {
    KRATOS_TRY

    rDerivative = 0.0;

    return rDerivative;

    KRATOS_CATCH(" ")
  }

  double &GetVolumetricFunction1stJDerivative(ElasticDataType &rVariables, double &rDerivative) override //dU/dJ
  {
    KRATOS_TRY

    const MaterialDataType &rMaterial = rVariables.GetMaterialParameters();

    //derivative of "U(J) = (K/4)*(J²-1) - (K/2)*lnJ"
    //dU(J)/dJ = (K/2)*(J-1/J)
    rDerivative = 0.5 * rMaterial.GetBulkModulus() * (rVariables.Data.Strain.Invariants.J * rVariables.Data.Strain.Invariants.J - 1.0);

    rDerivative /= rVariables.Data.Strain.Invariants.J;

    return rDerivative;

    KRATOS_CATCH(" ")
  }

  double &GetFunction2ndI1Derivative(ElasticDataType &rVariables, double &rDerivative) override //ddW/dI1dI1
  {
    KRATOS_TRY

    rDerivative = 0.0;

    return rDerivative;

    KRATOS_CATCH(" ")
  }

  double &GetFunction2ndI2Derivative(ElasticDataType &rVariables, double &rDerivative) override //ddW/dI2dI2
  {
    KRATOS_TRY

    rDerivative = 0.0;

    return rDerivative;

    KRATOS_CATCH(" ")
  }

  double &GetFunction2ndI3Derivative(ElasticDataType &rVariables, double &rDerivative) override //ddW/dI3dI3
  {
    KRATOS_TRY

    rDerivative = 0.0;

    return rDerivative;

    KRATOS_CATCH(" ")
  }

  double &GetVolumetricFunction2ndJDerivative(ElasticDataType &rVariables, double &rDerivative) override //ddU/dJdJ
  {
    KRATOS_TRY

    const MaterialDataType &rMaterial = rVariables.GetMaterialParameters();

    //derivative of "dU(J)/dJ = (K/2)*(J-1/J)"
    //ddU(J)/dJdJ = (K/2)*(1-1/J²)
    rDerivative = 0.5 * rMaterial.GetBulkModulus() * (rVariables.Data.Strain.Invariants.J * rVariables.Data.Strain.Invariants.J + 1.0);

    rDerivative /= rVariables.Data.Strain.Invariants.J * rVariables.Data.Strain.Invariants.J;

    return rDerivative;

    KRATOS_CATCH(" ")
  }

  void GetVolumetricFunctionFactors(ElasticDataType &rVariables, Vector &rFactors) override
  {
    KRATOS_TRY

    if (rFactors.size() != 3)
      rFactors.resize(3, false);

    //derivative of "Uk(J) = (1/4)*(J²-1) - (1/2)*lnJ"
    //dUk(J)/dJ = (1/2)*(J-1/J)
    rFactors[0] = 0.5 * (rVariables.Data.Strain.Invariants.J * rVariables.Data.Strain.Invariants.J - 1.0) / rVariables.Data.Strain.Invariants.J;

    //derivative of "dUk(J)/dJ = (1/2)*(J-1/J)"
    //ddUk(J)/dJdJ = (1/2)*(1+1/J²)
    rFactors[1] = 0.5 * (rVariables.Data.Strain.Invariants.J * rVariables.Data.Strain.Invariants.J + 1.0) / (rVariables.Data.Strain.Invariants.J * rVariables.Data.Strain.Invariants.J);

    //derivative of "ddUk(J)/dJdJ = (1/2)*(1+1/J²)"
    //dddUk(J)/dJdJdJ = -1/J³
    rFactors[2] = (-1.0) / (rVariables.Data.Strain.Invariants.J * rVariables.Data.Strain.Invariants.J * rVariables.Data.Strain.Invariants.J);

    this->GetVolumetricFunctionThermalFactors(rVariables, rFactors);

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, IsochoricMooneyRivlinModel)
  }

  void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, IsochoricMooneyRivlinModel)
  }

  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{
  ///@}

}; // Class IsochoricNeoHookeanModel

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
///@}
///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_ISOCHORIC_NEO_HOOKEAN_MODEL_HPP_INCLUDED  defined
