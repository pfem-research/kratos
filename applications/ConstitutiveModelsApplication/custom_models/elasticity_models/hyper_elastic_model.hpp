//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                  April 2017 $
//
//

#if !defined(KRATOS_HYPER_ELASTIC_MODEL_HPP_INCLUDED)
#define KRATOS_HYPER_ELASTIC_MODEL_HPP_INCLUDED

// System includes
#include <string>
#include <iostream>

// External includes

// Project includes
#include "custom_models/constitutive_model.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) HyperElasticModel : public ConstitutiveModel
{
protected:
  struct EigenData
  {
    array_1d<double, 3> Values;
    MatrixType Vectors;
  };

  struct InvariantsData
  {
    double I1;
    double I2;
    double I3;

    double J;
    double J_13;
  };

  struct StrainData
  {
    InvariantsData Invariants;
    EigenData Eigen;

    MatrixType Matrix;        //left(b) or right(C) cauchy green
    MatrixType InverseMatrix; //insverse right(C) cauchy green
  };

  struct HyperElasticFactors
  {
    double Alpha1; //1st derivative I1
    double Alpha2; //1st derivative I2
    double Alpha3; //1st derivative I3
    double Alpha4; //1st derivative J

    double Beta1; //2nd derivative I1
    double Beta2; //2nd derivative I2
    double Beta3; //2nd derivative I3
    double Beta4; //2nd derivative J

    // the implementation of the crossed derivatives have to be added for a more general form (usually they are zero)
    // double Gamma21;  //2nd derivative ddW/dI2dI1
    // double Gamma31;  //2nd derivative ddW/dI3dI1
    // double Gamma12;  //2nd derivative ddW/dI1dI2
    // double Gamma32;  //2nd derivative ddW/dI3dI2
    // double Gamma13;  //2nd derivative ddW/dI1dI3
    // double Gamma23;  //2nd derivative ddW/dI2dI3
  };

  struct HyperElasticityData
  {
    HyperElasticFactors Factors;
    StrainData Strain;
  };


public:
  ///@name Type Definitions
  ///@{
  typedef ConstitutiveModelData::ModelDataVariables<HyperElasticityData> ElasticDataType;

  /// Pointer definition of HyperElasticModel
  KRATOS_CLASS_POINTER_DEFINITION(HyperElasticModel);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  HyperElasticModel();

  /// Copy constructor.
  HyperElasticModel(HyperElasticModel const &rOther);

  /// Assignment operator.
  HyperElasticModel &operator=(HyperElasticModel const &rOther);

  /// Clone.
  ConstitutiveModel::Pointer Clone() const override;

  /// Destructor.
  ~HyperElasticModel() override;

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /**
     * Initialize member data
     */
  void InitializeModel(ModelDataType &rValues) override;

  /**
     * Finalize member data
     */
  void FinalizeModel(ModelDataType &rValues) override;

  /**
     * Calculate Strain Energy Density Functions
     */
  void CalculateStrainEnergy(ModelDataType &rValues, double &rDensityFunction) override;

  /**
     * Calculate Stresses
     */
  void CalculateStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix) override;

  void CalculateIsochoricStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix) override;

  void CalculateVolumetricStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix) override;

  /**
     * Calculate Constitutive Tensor
     */
  void CalculateConstitutiveTensor(ModelDataType &rValues, Matrix &rConstitutiveMatrix) override;

  void CalculateIsochoricConstitutiveTensor(ModelDataType &rValues, Matrix &rConstitutiveMatrix) override;

  void CalculateVolumetricConstitutiveTensor(ModelDataType &rValues, Matrix &rConstitutiveMatrix) override;

  /**
     * Calculate Stress and Constitutive Tensor
     */
  void CalculateStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix) override;

  void CalculateIsochoricStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix) override;

  void CalculateVolumetricStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix) override;

  /**
     * Calculate internal variables
     */
  void CalculateInternalVariables(ModelDataType &rValues) override;

  /**
     * Check
     */
  int Check(const Properties &rProperties, const ProcessInfo &rCurrentProcessInfo) override;

  /**
     * Get required properties
     */
  void GetRequiredProperties(Properties &rProperties) override;

  /**
     * Calculate Stresses
     */
  virtual void CalculateAndAddStressTensor(ElasticDataType &rVariables, MatrixType &rStressMatrix);

  virtual void CalculateAndAddIsochoricStressTensor(ElasticDataType &rVariables, MatrixType &rStressMatrix);

  virtual void CalculateAndAddVolumetricStressTensor(ElasticDataType &rVariables, MatrixType &rStressMatrix);

  /**
     * Calculate Constitutive Tensor
     */
  virtual void CalculateAndAddConstitutiveTensor(ElasticDataType &rVariables, Matrix &rConstitutiveMatrix);

  virtual void CalculateAndAddIsochoricConstitutiveTensor(ElasticDataType &rVariables, Matrix &rConstitutiveMatrix);

  virtual void CalculateAndAddVolumetricConstitutiveTensor(ElasticDataType &rVariables, Matrix &rConstitutiveMatrix);

  ///@}
  ///@name Access
  ///@{

  /**
     * method to ask the model the list of variables (dofs) needed from the domain
     * @param rScalarVariables : list of scalar dofs
     * @param rComponentVariables :  list of vector dofs
     */
  void GetDomainVariablesList(std::vector<Variable<double>*> &rScalarVariables,
                              std::vector<Variable<array_1d<double, 3>>*> &rComponentVariables) override
  {
    KRATOS_TRY

    rComponentVariables.push_back(&DISPLACEMENT);

    KRATOS_CATCH(" ")
  }

  /**
     * method to ask the constituitve model strain measures required
     * @param rStrainMeasures : list of strain measures
     */
  void GetStrainMeasures(std::vector<ConstitutiveModelData::StrainMeasureType> &rStrainMeasures) override
  {
    KRATOS_TRY

    rStrainMeasures.push_back(ConstitutiveModelData::StrainMeasureType::StrainMeasure_Deformation_Gradient);

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "HyperElasticModel";
    return buffer.str();
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "HyperElasticModel";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "HyperElasticModel Data";
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  static const MatrixType msIdentityMatrix;

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  /**
     * Calculate Constitutive Components
     */

  virtual double &AddConstitutiveComponent(ElasticDataType &rVariables, double &rCabcd,
                                           const unsigned int &a, const unsigned int &b,
                                           const unsigned int &c, const unsigned int &d);

  virtual double &AddIsochoricConstitutiveComponent(ElasticDataType &rVariables, double &rCabcd,
                                                    const unsigned int &a, const unsigned int &b,
                                                    const unsigned int &c, const unsigned int &d);

  virtual double &AddVolumetricConstitutiveComponent(ElasticDataType &rVariables, double &rCabcd,
                                                     const unsigned int &a, const unsigned int &b,
                                                     const unsigned int &c, const unsigned int &d);

  //************// Strain Data

  virtual void InitializeVariables(ModelDataType &rValues, ElasticDataType &rVariables);

  virtual void CalculateInvariants(ElasticDataType &rVariables);

  virtual void CalculateScalingFactors(ElasticDataType &rVariables);

  void CalculateStrainInvariants(const MatrixType &rStrainMatrix, double &rI1, double &rI2, double &rI3);

  //************//W

  virtual void CalculateAndAddIsochoricStrainEnergy(ElasticDataType &rVariables, double &rIsochoricDensityFunction);

  virtual void CalculateAndAddVolumetricStrainEnergy(ElasticDataType &rVariables, double &rVolumetricDensityFunction);

  //************// dW

  virtual double &GetVolumetricFunction1stJDerivative(ElasticDataType &rVariables, double &rDerivative); //dU/dJ

  virtual double &GetVolumetricFunction2ndJDerivative(ElasticDataType &rVariables, double &rDerivative); //ddU/dJdJ

  virtual void GetVolumetricFunctionFactors(ElasticDataType &rVariables, Vector &rFactors);

  virtual void GetVolumetricFunctionThermalFactors(ElasticDataType &rVariables, Vector &rFactors);

  //************// right cauchy green: C
  MatrixType &GetJRightCauchyGreenDerivative(const StrainData &rStrain, MatrixType &rDerivative); //dJ/dC

  double &GetJRightCauchyGreen1stDerivative(const StrainData &rStrain,
                                            double &rDerivative,
                                            const double &a,
                                            const double &b); ///dJ/dC

  double &GetJRightCauchyGreenSquare1stDerivative(const StrainData &rStrain,
                                                  double &rDerivative,
                                                  const double &a,
                                                  const double &b,
                                                  const double &c,
                                                  const double &d); //dJ/dC * dJ/dC

  double &GetJRightCauchyGreen2ndDerivative(const StrainData &rStrain,
                                            double &rDerivative,
                                            const double &a,
                                            const double &b,
                                            const double &c,
                                            const double &d); //ddJ/dCdC

  //************// left cauchy green : b

  MatrixType &GetJLeftCauchyGreenDerivative(const StrainData &rStrain, MatrixType &rDerivative); //dJ/db

  double &GetJLeftCauchyGreen1stDerivative(const StrainData &rStrain,
                                           double &rDerivative,
                                           const double &a,
                                           const double &b); //dJ/db

  double &GetJLeftCauchyGreenSquare1stDerivative(const StrainData &rStrain,
                                                 double &rDerivative,
                                                 const double &a,
                                                 const double &b,
                                                 const double &c,
                                                 const double &d); //dJ/db * dJ/db

  double &GetJLeftCauchyGreen2ndDerivative(const StrainData &rStrain,
                                           double &rDerivative,
                                           const double &a,
                                           const double &b,
                                           const double &c,
                                           const double &d); //ddJ/dbdb

  // set requested internal variables
  virtual void SetInternalVariables(ModelDataType &rValues, ElasticDataType &rVariables);

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{
  using ConstitutiveModel::InitializeVariables;

  using ConstitutiveModel::CalculateAndAddStressTensor;
  using ConstitutiveModel::CalculateAndAddIsochoricStressTensor;
  using ConstitutiveModel::CalculateAndAddVolumetricStressTensor;

  using ConstitutiveModel::CalculateAndAddConstitutiveTensor;
  using ConstitutiveModel::CalculateAndAddIsochoricConstitutiveTensor;
  using ConstitutiveModel::CalculateAndAddVolumetricConstitutiveTensor;

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, ConstitutiveModel)
  }

  void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, ConstitutiveModel)
  }

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class HyperElasticModel

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_HYPER_ELASTIC_MODEL_HPP_INCLUDED  defined
