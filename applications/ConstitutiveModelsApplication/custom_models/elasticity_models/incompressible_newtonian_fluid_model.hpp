//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                 August 2020 $
//
//

#if !defined(KRATOS_INCOMPRESSIBLE_NEWTONIAN_FLUID_MODEL_HPP_INCLUDED)
#define KRATOS_INCOMPRESSIBLE_NEWTONIAN_FLUID_MODEL_HPP_INCLUDED

// System includes
#include <string>
#include <iostream>

// External includes

// Project includes
#include "custom_models/elasticity_models/isochoric_newtonian_fluid_model.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) IncompressibleNewtonianFluidModel : public IsochoricNewtonianFluidModel
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of IncompressibleNewtonianFluidModel
  KRATOS_CLASS_POINTER_DEFINITION(IncompressibleNewtonianFluidModel);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  IncompressibleNewtonianFluidModel() : IsochoricNewtonianFluidModel() {}

  /// Copy constructor.
  IncompressibleNewtonianFluidModel(IncompressibleNewtonianFluidModel const &rOther) : IsochoricNewtonianFluidModel(rOther) {}

  /// Assignment operator.
  IncompressibleNewtonianFluidModel &operator=(IncompressibleNewtonianFluidModel const &rOther)
  {
    IsochoricNewtonianFluidModel::operator=(rOther);
    return *this;
  }

  /// Clone.
  ConstitutiveModel::Pointer Clone() const override
  {
    return Kratos::make_shared<IncompressibleNewtonianFluidModel>(*this);
  }

  /// Destructor.
  ~IncompressibleNewtonianFluidModel() override {}

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  // Simplyfied methods must be implemented for performance purposes
  /**
     * Calculate Stresses
     */

  /**
     * Calculate Constitutive Components
     */

  /**
     * Check
     */
  int Check(const Properties &rProperties, const ProcessInfo &rCurrentProcessInfo) override
  {
    KRATOS_TRY

    IsochoricNewtonianFluidModel::Check(rProperties, rCurrentProcessInfo);

    return 0;

    KRATOS_CATCH(" ")
  }

  /**
     * Get required properties
     */
  void GetRequiredProperties(Properties &rProperties) override
  {
    KRATOS_TRY

    IsochoricNewtonianFluidModel::GetRequiredProperties(rProperties);

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{

  /**
     * method to ask the constitutive model the list of variables (dofs) needed from the domain
     * @param rScalarVariables : list of scalar dofs
     * @param rComponentVariables :  list of vector dofs
     */
  void GetDomainVariablesList(std::vector<Variable<double>*> &rScalarVariables,
                              std::vector<Variable<array_1d<double, 3>>*> &rComponentVariables) override
  {
    KRATOS_TRY

    NewtonianFluidModel::GetDomainVariablesList(rScalarVariables, rComponentVariables);

    rScalarVariables.push_back(&PRESSURE);

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "IncompressibleHyperElasticModel";
    return buffer.str();
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "IncompressibleHyperElasticModel";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "IncompressibleHyperElasticModel Data";
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  //specialized methods:

  void CalculatePressureFactor(ElasticDataType &rVariables, double &rFactor) override
  {
    KRATOS_TRY

    rFactor = rVariables.GetModelData().GetPressure();

    KRATOS_CATCH(" ")
  }


  void CalculateConstitutiveMatrixPressureFactor(ElasticDataType &rVariables, double &rFactor) override
  {
    KRATOS_TRY

    rFactor = rVariables.GetModelData().GetPressure();

    KRATOS_CATCH(" ")
  }


  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, IsochoricNewtonianFluidModel)
  }

  void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, IsochoricNewtonianFluidModel)
  }

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class IncompressibleNewtonianFluidModel

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_INCOMPRESSIBLE_NEWTONIAN_FLUID_MODEL_HPP_INCLUDED  defined
