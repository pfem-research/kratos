//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:                  April 2017 $
//
//

#if !defined(KRATOS_HENCKY_DEPTH_MODEL_HPP_INCLUDED)
#define KRATOS_HENCKY_DEPTH_MODEL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/elasticity_models/hencky_hyper_elastic_model.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) HenckyDepthModel : public HenckyHyperElasticModel
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of HenckyDepthModel
  KRATOS_CLASS_POINTER_DEFINITION(HenckyDepthModel);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  HenckyDepthModel();

  /// Copy constructor.
  HenckyDepthModel(HenckyDepthModel const &rOther);

  /// Assignment operator.
  HenckyDepthModel &operator=(HenckyDepthModel const &rOther);

  /// Clone.
  ConstitutiveModel::Pointer Clone() const override;

  /// Destructor.
  ~HenckyDepthModel() override;

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  double &GetValue(const Variable<double> &rThisVariable,
                    double &rValue) override;

   array_1d<double, 3> &GetValue(const Variable< array_1d<double, 3> > & rVariable,
                                 array_1d<double, 3> & rValue) override
   {
     rValue = BaseType::GetValue(rVariable, rValue);
     return rValue;
   }

   Vector &GetValue(const Variable<Vector> &rVariable, Vector &rValue) override
   {
     rValue = BaseType::GetValue(rVariable, rValue);
     return rValue;
   }

   Matrix &GetValue(const Variable<Matrix> &rVariable, Matrix &rValue) override
   {
     rValue = BaseType::GetValue(rVariable, rValue);
     return rValue;
   }
   
  void SetValue(const Variable<double> &rThisVariable,
                 const double &rValue,
                 const ProcessInfo &rCurrentProcessInfo) override;

   void SetValue(const Variable<array_1d<double,3>> &rVariable,
                const array_1d<double,3> &rValue,
                 const ProcessInfo &rCurrentProcessInfo) override
   {
     BaseType::SetValue(rVariable, rValue, rCurrentProcessInfo);
   }

  void SetValue(const Variable<Vector> &rThisVariable,
                 const Vector &rValue,
                 const ProcessInfo &rCurrentProcessInfo) override;

   void SetValue(const Variable<Matrix> &rVariable,
                const Matrix &rValue,
                 const ProcessInfo &rCurrentProcessInfo) override
   {
     BaseType::SetValue(rVariable, rValue, rCurrentProcessInfo);
   }


  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "HenckyDepthModel";
    return buffer.str();
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "HenckyDepthModel";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "HenckyDepthModel Data";
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  /**
     * Calculate Stresses
     */
  void CalculateAndAddStressTensor(ElasticDataType &rVariables, MatrixType &rStressMatrix) override;

  /**
     * Calculate Constitutive Tensor
     */
  void CalculateAndAddConstitutiveTensor(ElasticDataType &rVariables, Matrix &rConstitutiveMatrix) override;

  /**
      * Calculate some strain invariants
      */
  void SeparateVolumetricAndDeviatoricPart(const MatrixType &rA, double &rVolumetric, MatrixType &rDev, double &devNorm);

  void SetStressState(MatrixType &rHenckyStrain, const double &rE, const double &rNu);

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  double mShearModulus;

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, HenckyHyperElasticModel)
    rSerializer.save("mShearModulus", mShearModulus);
  }

  void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, HenckyHyperElasticModel)
    rSerializer.load("mShearModulus", mShearModulus);
  }

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class HenckyDepthModel

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_HENCKY_DEPTH_MODEL_HPP_INCLUDED  defined
