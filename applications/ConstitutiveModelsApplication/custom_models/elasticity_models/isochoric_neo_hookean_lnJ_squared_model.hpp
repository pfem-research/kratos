//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                  April 2017 $
//
//

#if !defined(KRATOS_ISOCHORIC_NEO_HOOKEAN_LNJ_SQUARED_MODEL_HPP_INCLUDED)
#define KRATOS_ISOCHORIC_NEO_HOOKEAN_LNJ_SQUARED_MODEL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/elasticity_models/isochoric_neo_hookean_model.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) IsochoricNeoHookeanLnJSquaredModel : public IsochoricNeoHookeanModel
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of IsochoricNeoHookeanLnJSquaredModel
  KRATOS_CLASS_POINTER_DEFINITION(IsochoricNeoHookeanLnJSquaredModel);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  IsochoricNeoHookeanLnJSquaredModel() : IsochoricNeoHookeanModel() {}

  /// Copy constructor.
  IsochoricNeoHookeanLnJSquaredModel(IsochoricNeoHookeanLnJSquaredModel const &rOther) : IsochoricNeoHookeanModel(rOther) {}

  /// Assignment operator.
  IsochoricNeoHookeanLnJSquaredModel &operator=(IsochoricNeoHookeanLnJSquaredModel const &rOther)
  {
    IsochoricNeoHookeanModel::operator=(rOther);
    return *this;
  }

  /// Clone.
  ConstitutiveModel::Pointer Clone() const override
  {
    return Kratos::make_shared<IsochoricNeoHookeanLnJSquaredModel>(*this);
  }

  /// Destructor.
  ~IsochoricNeoHookeanLnJSquaredModel() override {}

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  // Simplyfied methods must be implemented for performance purposes

  /**
     * Calculate Stresses
     */

  /**
     * Calculate Constitutive Components
     */

  /**
     * Check
     */

  int Check(const Properties &rProperties, const ProcessInfo &rCurrentProcessInfo) override
  {
    KRATOS_TRY

    HyperElasticModel::Check(rProperties, rCurrentProcessInfo);

    if (C10.Key() == 0 || rProperties[C10] <= 0.00)
      KRATOS_ERROR << "C10 has an invalid key or value" << std::endl;

    if (BULK_MODULUS.Key() == 0 || rProperties[BULK_MODULUS] <= 0.00)
      KRATOS_ERROR << "BULK_MODULUS has an invalid key or value" << std::endl;

    return 0;

    KRATOS_CATCH(" ")
  }

  /**
     * Get required properties
     */
  void GetRequiredProperties(Properties &rProperties) override
  {
    KRATOS_TRY

    // Set required properties to check keys
    double value = 0.0; //dummy
    rProperties.SetValue(C10,value);
    rProperties.SetValue(BULK_MODULUS,value);

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "IsochoricNeoHookeanLnJSquaredModel";
    return buffer.str();
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "IsochoricNeoHookeanLnJSquaredModel";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "IsochoricNeoHookeanLnJSquaredModel Data";
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  //specialized methods:

  void CalculateVolumetricFactor(ElasticDataType &rVariables, double &rFactor) override
  {
    KRATOS_TRY

    rFactor = std::log(rVariables.Data.Strain.Invariants.J);

    KRATOS_CATCH(" ")
  }

  void CalculateConstitutiveMatrixFactor(ElasticDataType &rVariables, double &rFactor) override
  {
    KRATOS_TRY

    rFactor = 1.0;

    KRATOS_CATCH(" ")
  }

  //************// W

  void CalculateAndAddIsochoricStrainEnergy(ElasticDataType &rVariables, double &rIsochoricDensityFunction) override
  {
    KRATOS_TRY

    const MaterialDataType &rMaterial = rVariables.GetMaterialParameters();

    rIsochoricDensityFunction += rMaterial.GetModelParameters()[0] * (rVariables.Data.Strain.Invariants.J_13 * rVariables.Data.Strain.Invariants.I1 - 3.0);

    KRATOS_CATCH(" ")
  }

  void CalculateAndAddVolumetricStrainEnergy(ElasticDataType &rVariables, double &rVolumetricDensityFunction) override
  {
    KRATOS_TRY

    const MaterialDataType &rMaterial = rVariables.GetMaterialParameters();

    //energy function "U(J) = (K/2)*(lnJ)²"
    rVolumetricDensityFunction += rMaterial.GetBulkModulus() * 0.5 * pow(std::log(rVariables.Data.Strain.Invariants.J), 2);

    KRATOS_CATCH(" ")
  }

  //************// dW

  double &GetFunction1stI1Derivative(ElasticDataType &rVariables, double &rDerivative) override //dW/dI1
  {
    KRATOS_TRY

    const MaterialDataType &rMaterial = rVariables.GetMaterialParameters();

    rDerivative = rMaterial.GetModelParameters()[0];

    return rDerivative;

    KRATOS_CATCH(" ")
  }

  double &GetFunction1stI2Derivative(ElasticDataType &rVariables, double &rDerivative) override //dW/dI2
  {
    KRATOS_TRY

    rDerivative = 0.0;

    return rDerivative;

    KRATOS_CATCH(" ")
  }

  double &GetFunction1stI3Derivative(ElasticDataType &rVariables, double &rDerivative) override //dW/dI3
  {
    KRATOS_TRY

    rDerivative = 0.0;

    return rDerivative;

    KRATOS_CATCH(" ")
  }

  double &GetVolumetricFunction1stJDerivative(ElasticDataType &rVariables, double &rDerivative) override //dU/dJ
  {
    KRATOS_TRY

    const MaterialDataType &rMaterial = rVariables.GetMaterialParameters();

    //derivative of "U(J) = (K/2)*ln(J)²"
    //dU(J)/dJ = (K)*(lnJ/J)
    rDerivative = rMaterial.GetBulkModulus() * std::log(rVariables.Data.Strain.Invariants.J);

    rDerivative /= rVariables.Data.Strain.Invariants.J;

    return rDerivative;

    KRATOS_CATCH(" ")
  }

  double &GetFunction2ndI1Derivative(ElasticDataType &rVariables, double &rDerivative) override //ddW/dI1dI1
  {
    KRATOS_TRY

    rDerivative = 0.0;

    return rDerivative;

    KRATOS_CATCH(" ")
  }

  double &GetFunction2ndI2Derivative(ElasticDataType &rVariables, double &rDerivative) override //ddW/dI2dI2
  {
    KRATOS_TRY

    rDerivative = 0.0;

    return rDerivative;

    KRATOS_CATCH(" ")
  }

  double &GetFunction2ndI3Derivative(ElasticDataType &rVariables, double &rDerivative) override //ddW/dI3dI3
  {
    KRATOS_TRY

    rDerivative = 0.0;

    return rDerivative;

    KRATOS_CATCH(" ")
  }

  double &GetVolumetricFunction2ndJDerivative(ElasticDataType &rVariables, double &rDerivative) override //ddU/dJdJ
  {
    KRATOS_TRY

    const MaterialDataType &rMaterial = rVariables.GetMaterialParameters();

    //derivative of "dU(J)/dJ = (K)*(lnJ/J)"
    //ddU(J)/dJdJ = (K)*(1-lnJ)/J²
    rDerivative = rMaterial.GetBulkModulus() * (1.0 - std::log(rVariables.Data.Strain.Invariants.J)) / (rVariables.Data.Strain.Invariants.J * rVariables.Data.Strain.Invariants.J);

    return rDerivative;

    KRATOS_CATCH(" ")
  }

  void GetVolumetricFunctionFactors(ElasticDataType &rVariables, Vector &rFactors) override
  {
    KRATOS_TRY

    if (rFactors.size() != 3)
      rFactors.resize(3, false);

    //derivative of "Uk(J) = (1/2)*ln(J)²"
    //dUk(J)/dJ = (lnJ/J)
    rFactors[0] = std::log(rVariables.Data.Strain.Invariants.J) / rVariables.Data.Strain.Invariants.J;

    //derivative of "dUk(J)/dJ = (lnJ/J)"
    //ddUk(J)/dJdJ = (1-lnJ)/J²
    rFactors[1] = (1.0 - std::log(rVariables.Data.Strain.Invariants.J)) / (rVariables.Data.Strain.Invariants.J * rVariables.Data.Strain.Invariants.J);

    //derivative of "ddUk(J)/dJdJ = (1-lnJ)/J²"
    //dddUk(J)/dJdJdJ = (2lnJ-3)/J³
    rFactors[2] = (2.0 * std::log(rVariables.Data.Strain.Invariants.J) - 3.0) / (rVariables.Data.Strain.Invariants.J * rVariables.Data.Strain.Invariants.J * rVariables.Data.Strain.Invariants.J);

    this->GetVolumetricFunctionThermalFactors(rVariables, rFactors);

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, IsochoricNeoHookeanModel)
  }

  void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, IsochoricNeoHookeanModel)
  }

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class IsochoricNeoHookeanLnJSquaredModel

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_ISOCHORIC_NEO_HOOKEAN_LNJ_SQUARED_MODEL_HPP_INCLUDED  defined
