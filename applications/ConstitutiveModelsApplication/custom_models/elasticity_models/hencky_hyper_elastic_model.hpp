//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:                  April 2017 $
//
//

#if !defined(KRATOS_HENCKY_HYPER_ELASTIC_MODEL_HPP_INCLUDED)
#define KRATOS_HENCKY_HYPER_ELASTIC_MODEL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/elasticity_models/hyper_elastic_model.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
    */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) HenckyHyperElasticModel : public HyperElasticModel
{
public:
   ///@name Type Definitions
   ///@{

   //base type
   typedef HyperElasticModel BaseType;

   /// Pointer definition of BorjaModel
   KRATOS_CLASS_POINTER_DEFINITION(HenckyHyperElasticModel);

   ///@}
   ///@name Life Cycle
   ///@{

   /// Default constructor.
   HenckyHyperElasticModel() : HyperElasticModel() { mSetStressState = false; };

   /// Copy constructor.
   HenckyHyperElasticModel(HenckyHyperElasticModel const &rOther) : HyperElasticModel(rOther), mSetStressState(rOther.mSetStressState){};

   /// Assignment operator.
   HenckyHyperElasticModel &operator=(HenckyHyperElasticModel const &rOther)
   {
      HyperElasticModel::operator=(rOther);
      mSetStressState = rOther.mSetStressState;
      return *this;
   };

   /// Clone.
   ConstitutiveModel::Pointer Clone() const override
   {
      return (HenckyHyperElasticModel::Pointer(new HenckyHyperElasticModel(*this)));
   };

   /// Destructor.
   ~HenckyHyperElasticModel() override {}

   ///@}
   ///@name Operators
   ///@{

   ///@}
   ///@name Operations
   ///@{

   /**
          * Initialize member data
          */
   void InitializeModel(ModelDataType &rValues) override
   {
      KRATOS_TRY

      HyperElasticModel::InitializeModel(rValues);

      // Compute trial strain
      //deformation gradient
      const MatrixType &rDeltaDeformationMatrix = rValues.GetDeltaDeformationMatrix();

      //historical strain matrix
      rValues.StrainMatrix = ConstitutiveModelUtilities::VectorToSymmetricTensor(*this->mpHistoryVector, rValues.StrainMatrix);
      rValues.StrainMatrix = prod(rDeltaDeformationMatrix, rValues.StrainMatrix);
      rValues.StrainMatrix = prod(rValues.StrainMatrix, trans(rDeltaDeformationMatrix));

      KRATOS_CATCH("")
   }

   /**
     * Check
     */
   int Check(const Properties &rProperties, const ProcessInfo &rCurrentProcessInfo) override
   {
      KRATOS_TRY

      if (YOUNG_MODULUS.Key() == 0 || rProperties[YOUNG_MODULUS] <= 0.00)
         KRATOS_ERROR << "YOUNG_MODULUS has Key zero or invalid value" << std::endl;

      if (POISSON_RATIO.Key() == 0)
      {
         KRATOS_ERROR << "POISSON_RATIO has Key zero invalid value" << std::endl;
      }
      else
      {
         const double &nu = rProperties[POISSON_RATIO];
         if ((nu > 0.499 && nu < 0.501) || (nu < -0.999 && nu > -1.01))
            KRATOS_ERROR << "POISSON_RATIO has an invalid value" << std::endl;
      }

      return 0;

      KRATOS_CATCH(" ")
   }
  
   void CalculateConstitutiveTensor(ModelDataType &rValues, Matrix &rConstitutiveMatrix) override
   {
      KRATOS_TRY

      //Initialize ConstitutiveMatrix
      ElasticDataType Variables;
      this->InitializeVariables(rValues, Variables);

      //Set constitutive matrix to zero before adding
      noalias(rConstitutiveMatrix) = ZeroMatrix(6,6);

      //Calculate Constitutive Matrix
      this->CalculateAndAddConstitutiveTensor(Variables, rConstitutiveMatrix);

      KRATOS_CATCH(" ")
   }

   /**
     * Get required properties
     */
   void GetRequiredProperties(Properties &rProperties) override
   {
      KRATOS_TRY

      // Set required properties to check keys
      double value = 0.0; //dummy
      rProperties.SetValue(YOUNG_MODULUS, value);
      rProperties.SetValue(POISSON_RATIO, value);

      KRATOS_CATCH(" ")
   }

   ///@}
   ///@name Access
   ///@{

   double &GetValue(const Variable<double> &rVariable,
                    double &rValue) override
   {
      KRATOS_TRY

      if (rVariable == ELASTIC_JACOBIAN)
      {
         MatrixType be;
         be.clear();
         be = ConstitutiveModelUtilities::VectorToSymmetricTensor(*this->mpHistoryVector, be);
         rValue = MathUtils<double>::Det3(be);
         rValue = sqrt(rValue);
      }
      else if ( rVariable == HENCKY_ELASTIC_VOLUMETRIC_STRAIN) {
         MatrixType be;
         be.clear();
         be = ConstitutiveModelUtilities::VectorToSymmetricTensor(*this->mpHistoryVector, be);
         rValue = MathUtils<double>::Det3(be);
         rValue = std::log(sqrt(rValue));
      }
      return rValue;

      KRATOS_CATCH("")
   }

   array_1d<double, 3> &GetValue(const Variable< array_1d<double, 3> > & rVariable,
                                 array_1d<double, 3> & rValue) override
   {
     rValue = BaseType::GetValue(rVariable, rValue);
     return rValue;
   }

   Vector &GetValue(const Variable<Vector> &rVariable, Vector &rValue) override
   {
     rValue = BaseType::GetValue(rVariable, rValue);
     return rValue;
   }

   Matrix &GetValue(const Variable<Matrix> &rVariable, Matrix &rValue) override
   {
     rValue = BaseType::GetValue(rVariable, rValue);
     return rValue;
   }

   void SetValue(const Variable<double> &rVariable,
                 const double &rValue,
                 const ProcessInfo &rCurrentProcessInfo) override
   {
      KRATOS_TRY

      if (rVariable == ELASTIC_JACOBIAN)
      {
         MatrixType be;
         be.clear();
         be = ConstitutiveModelUtilities::VectorToSymmetricTensor(*this->mpHistoryVector, be);
         double detBe = MathUtils<double>::Det3(be);
         detBe = sqrt(detBe);
         double JJ = pow(rValue / detBe, 2.0 / 3.0);
         be *= JJ;
         ConstitutiveModelUtilities::SymmetricTensorToVector(be, *this->mpHistoryVector);
      }
      else if ( rVariable == HENCKY_ELASTIC_VOLUMETRIC_STRAIN)
      {
         MatrixType be;
         be.clear();
         be = ConstitutiveModelUtilities::VectorToSymmetricTensor(*this->mpHistoryVector, be);
         double detBe = MathUtils<double>::Det3(be);
         detBe = sqrt(detBe);
         double newDeterminant = std::exp( rValue);
         double JJ = pow( newDeterminant / detBe, 2.0 / 3.0);
         be *= JJ;
         ConstitutiveModelUtilities::SymmetricTensorToVector(be, *this->mpHistoryVector);
      }

      KRATOS_CATCH("")
   }

   void SetValue(const Variable<array_1d<double,3>> &rVariable,
                const array_1d<double,3> &rValue,
                 const ProcessInfo &rCurrentProcessInfo) override
   {
     BaseType::SetValue(rVariable, rValue, rCurrentProcessInfo);
   }

   void SetValue(const Variable<Vector> &rVariable,
                 const Vector &rValue,
                 const ProcessInfo &rCurrentProcessInfo) override
   {
      KRATOS_TRY

      if (rVariable == ELASTIC_LEFT_CAUCHY_FROM_KIRCHHOFF_STRESS)
      {
         mInitialStressState.resize(6);
         noalias(mInitialStressState) = ZeroVector(6);
         mInitialStressState = rValue;
         mSetStressState = true;
      }
      else
        BaseType::SetValue(rVariable, rValue, rCurrentProcessInfo);

      KRATOS_CATCH("")
   }

   void SetValue(const Variable<Matrix> &rVariable, const Matrix &rValue,
                 const ProcessInfo &rCurrentProcessInfo) override
   {
     BaseType::SetValue(rVariable, rValue, rCurrentProcessInfo);
   }

   ///@}
   ///@name Inquiry
   ///@{

   ///@}
   ///@name Input and output
   ///@{

   /// Turn back information as a string.
   std::string Info() const override
   {
      std::stringstream buffer;
      buffer << "HenckyHyperElasticModel";
      return buffer.str();
   }

   /// Print information about this object.
   void PrintInfo(std::ostream &rOStream) const override
   {
      rOStream << "HenckyHyperElasticModel";
   }

   /// Print object's data.
   void PrintData(std::ostream &rOStream) const override
   {
      rOStream << "HenckyHyperElasticModel Data";
   }

   ///@}
   ///@name Friends
   ///@{

   ///@}

protected:
   ///@name Protected static Member Variables
   ///@{

   ///@}
   ///@name Protected member Variables
   ///@{

   bool mSetStressState;
   Vector mInitialStressState;

   ///@}
   ///@name Protected Operators
   ///@{

   ///@}
   ///@name Protected Operations
   ///@{

   void InitializeVariables(ModelDataType &rValues, ElasticDataType &rVariables) override
   {
      KRATOS_TRY

      rVariables.SetModelData(rValues);
      rVariables.SetState(rValues.State);

      const StressMeasureType &rStressMeasure = rValues.GetStressMeasure();

      if (rStressMeasure == ConstitutiveModelData::StressMeasureType::StressMeasure_PK2)
      { //mStrainMatrix = RightCauchyGreen (C=FT*F)  C^-1=(FT*F)^-1=F^-1*FT^-1

         KRATOS_ERROR << "calling HenckyHyperelastic based method with PK2 stress. not implemented" << std::endl;
      }
      else if (rStressMeasure == ConstitutiveModelData::StressMeasureType::StressMeasure_Kirchhoff)
      { //mStrainMatrix = LeftCauchyGreen (b=F*FT)

         //set working strain measure
         rValues.SetStrainMeasure(ConstitutiveModelData::StrainMeasureType::StrainMeasure_Left_CauchyGreen);

         MatrixType EigenVectors;
         MatrixType EigenValues;
         EigenVectors.clear();
         EigenValues.clear();

         MathUtils<double>::GaussSeidelEigenSystem<MatrixType, MatrixType>(rValues.StrainMatrix, EigenVectors, EigenValues);

         rVariables.Data.Strain.Matrix.clear();
         for (unsigned int i = 0; i < 3; i++)
            rVariables.Data.Strain.Matrix(i, i) = std::log(EigenValues(i, i)) / 2.0;

         rVariables.Data.Strain.Matrix = prod(rVariables.Data.Strain.Matrix, trans(EigenVectors));
         rVariables.Data.Strain.Matrix = prod(EigenVectors, rVariables.Data.Strain.Matrix);

         rValues.State.Set(ConstitutiveModelData::STRAIN_COMPUTED);
      }
      else
      {
         KRATOS_ERROR << "calling initialize HyperElasticModel .. StressMeasure required is inconsistent" << std::endl;
      }

      KRATOS_CATCH("")
   };

   ///@}
   ///@name Protected  Access
   ///@{

   ///@}
   ///@name Protected Inquiry
   ///@{

   ///@}
   ///@name Protected LifeCycle
   ///@{

   ///@}

private:
   ///@name Static Member Variables
   ///@{

   ///@}
   ///@name Member Variables
   ///@{

   ///@}
   ///@name Private Operators
   ///@{

   ///@}
   ///@name Private Operations
   ///@{

   ///@}
   ///@name Private  Access
   ///@{

   ///@}
   ///@name Serialization
   ///@{
   friend class Serializer;

   void save(Serializer &rSerializer) const override
   {
      KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, HyperElasticModel)
      rSerializer.save("mSetStressState", mSetStressState); // no fa falta...
   }

   void load(Serializer &rSerializer) override
   {
      KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, HyperElasticModel)
      rSerializer.load("mSetStressState", mSetStressState); // no fa falta...
   }

   ///@}
   ///@name Private Inquiry
   ///@{

   ///@}
   ///@name Un accessible methods
   ///@{

   ///@}

}; // Class BorjaModel

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_HENCKY_HYPER_ELASTIC_MODEL_HPP_INCLUDED  defined
