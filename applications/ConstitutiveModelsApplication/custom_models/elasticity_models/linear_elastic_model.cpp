//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                  April 2017 $
//
//

// System includes

// External includes

// Project includes
#include "custom_models/elasticity_models/linear_elastic_model.hpp"

namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

LinearElasticModel::LinearElasticModel()
    : ConstitutiveModel()
{
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

LinearElasticModel::LinearElasticModel(const LinearElasticModel &rOther)
    : ConstitutiveModel(rOther)
{
}

//********************************CLONE***********************************************
//************************************************************************************

ConstitutiveModel::Pointer LinearElasticModel::Clone() const
{
  return Kratos::make_shared<LinearElasticModel>(*this);
}

//********************************ASSIGNMENT******************************************
//************************************************************************************
LinearElasticModel &LinearElasticModel::operator=(LinearElasticModel const &rOther)
{
  ConstitutiveModel::operator=(rOther);
  return *this;
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

LinearElasticModel::~LinearElasticModel()
{
}

//***********************PUBLIC OPERATIONS FROM BASE CLASS****************************
//************************************************************************************

void LinearElasticModel::InitializeModel(ModelDataType &rValues)
{
  KRATOS_TRY

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LinearElasticModel::FinalizeModel(ModelDataType &rValues)
{
  KRATOS_TRY

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LinearElasticModel::InitializeVariables(ModelDataType &rValues, ElasticDataType &rVariables)
{
  KRATOS_TRY

  //set model data pointer
  rVariables.SetModelData(rValues);
  rVariables.SetState(rValues.State);

  ConstitutiveModelUtilities::StrainTensorToVector(rValues.StrainMatrix, rVariables.Data.StrainVector);

  //add initial strain
  if (this->mOptions.Is(ConstitutiveModel::ADD_HISTORY_VECTOR) && this->mOptions.Is(ConstitutiveModel::HISTORY_STRAIN_MEASURE))
  {
    for (unsigned int i = 0; i < rVariables.Data.StrainVector.size(); i++)
    {
      rVariables.Data.StrainVector[i] += (*this->mpHistoryVector)[i];
    }
    rValues.StrainMatrix = ConstitutiveModelUtilities::StrainVectorToTensor(rVariables.Data.StrainVector, rValues.StrainMatrix);
  }

  rValues.SetStrainMeasure(ConstitutiveModelData::StrainMeasureType::StrainMeasure_None);

  rValues.MaterialParameters.SetLameMuBar(rValues.MaterialParameters.GetLameMu());

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LinearElasticModel::CalculateStrainEnergy(ModelDataType &rValues, double &rDensityFunction)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling the base class function in LinearElasticModel ... illegal operation" << std::endl;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LinearElasticModel::CalculateStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix)
{
  KRATOS_TRY

  ElasticDataType Variables;
  this->InitializeVariables(rValues, Variables);

  this->CalculateAndAddStressTensor(Variables, rStressMatrix);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LinearElasticModel::CalculateAndAddStressTensor(ElasticDataType &rVariables, MatrixType &rStressMatrix)
{
  KRATOS_TRY

  this->CalculateConstitutiveTensor(rVariables);

  VectorType StressVector;
  noalias(StressVector) = prod(rVariables.Data.ConstitutiveTensor, rVariables.Data.StrainVector);

  rStressMatrix = ConstitutiveModelUtilities::VectorToSymmetricTensor(StressVector, rStressMatrix);

  rVariables.State().Set(ConstitutiveModelData::STRESS_COMPUTED);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LinearElasticModel::CalculateIsochoricStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix)
{
  KRATOS_TRY

  ElasticDataType Variables;
  this->InitializeVariables(rValues, Variables);

  this->CalculateAndAddIsochoricStressTensor(Variables, rStressMatrix);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LinearElasticModel::CalculateAndAddIsochoricStressTensor(ElasticDataType &rVariables, MatrixType &rStressMatrix)
{
  KRATOS_TRY

  //Set constitutive matrix to zero before adding
  this->CalculateConstitutiveTensor(rVariables);

  //total stress
  VectorType StressVector;
  noalias(StressVector) = prod(rVariables.Data.ConstitutiveTensor, rVariables.Data.StrainVector);

  //deviatoric stress
  double MeanStress = (1.0 / 3.0) * (StressVector[0] + StressVector[1] + StressVector[2]);
  StressVector[0] -= MeanStress;
  StressVector[1] -= MeanStress;
  StressVector[2] -= MeanStress;

  rStressMatrix = ConstitutiveModelUtilities::VectorToSymmetricTensor(StressVector, rStressMatrix);

  rVariables.State().Set(ConstitutiveModelData::STRESS_COMPUTED);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LinearElasticModel::CalculateVolumetricStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix)
{
  KRATOS_TRY

  this->CalculateStressTensor(rValues, rStressMatrix);
  double MeanPressure = 0;
  for (unsigned int i = 0; i < 3; i++)
    MeanPressure += rStressMatrix(i, i) / 3.0;
  rStressMatrix = IdentityMatrix(3);
  rStressMatrix *= MeanPressure;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LinearElasticModel::CalculateAndAddVolumetricStressTensor(ElasticDataType &rVariables, MatrixType &rStressMatrix)
{
  KRATOS_TRY

  MatrixType StressMatrix;
  noalias(StressMatrix) = ZeroMatrix(3,3);
  this->CalculateAndAddStressTensor(rVariables, StressMatrix);
  double MeanPressure = 0;
  for (unsigned int i = 0; i < 3; i++)
    MeanPressure += StressMatrix(i, i) / 3.0;
  for (unsigned int i = 0; i < 3; i++)
    rStressMatrix(i, i) += MeanPressure;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LinearElasticModel::CalculateConstitutiveTensor(ModelDataType &rValues, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  ElasticDataType Variables;
  this->InitializeVariables(rValues, Variables);

  //Set constitutive matrix to zero before adding
  const SizeType &rVoigtSize = Variables.GetModelData().GetVoigtSize();
  noalias(rConstitutiveMatrix) = ZeroMatrix(rVoigtSize,rVoigtSize);

  this->CalculateAndAddConstitutiveTensor(Variables, rConstitutiveMatrix);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LinearElasticModel::CalculateAndAddConstitutiveTensor(ElasticDataType &rVariables, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  this->CalculateConstitutiveTensor(rVariables);

  rConstitutiveMatrix = ConstitutiveModelUtilities::ConstitutiveTensorToMatrix(rVariables.Data.ConstitutiveTensor, rConstitutiveMatrix);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LinearElasticModel::CalculateConstitutiveTensor(ElasticDataType &rVariables)
{
  KRATOS_TRY

  //Calculate Elastic ConstitutiveMatrix
  const ModelDataType &rModelData = rVariables.GetModelData();
  const MaterialDataType &rMaterial = rModelData.GetMaterialParameters();

  noalias(rVariables.Data.ConstitutiveTensor) = ZeroMatrix(6,6);

  // Lame constants
  const double &rYoungModulus = rMaterial.GetYoungModulus();
  const double &rPoissonCoefficient = rMaterial.GetPoissonCoefficient();

  // 3D linear elastic constitutive matrix
  rVariables.Data.ConstitutiveTensor(0, 0) = (rYoungModulus * (1.0 - rPoissonCoefficient) / ((1.0 + rPoissonCoefficient) * (1.0 - 2.0 * rPoissonCoefficient)));
  rVariables.Data.ConstitutiveTensor(1, 1) = rVariables.Data.ConstitutiveTensor(0, 0);
  rVariables.Data.ConstitutiveTensor(2, 2) = rVariables.Data.ConstitutiveTensor(0, 0);

  rVariables.Data.ConstitutiveTensor(3, 3) = rVariables.Data.ConstitutiveTensor(0, 0) * (1.0 - 2.0 * rPoissonCoefficient) / (2.0 * (1.0 - rPoissonCoefficient));
  rVariables.Data.ConstitutiveTensor(4, 4) = rVariables.Data.ConstitutiveTensor(3, 3);
  rVariables.Data.ConstitutiveTensor(5, 5) = rVariables.Data.ConstitutiveTensor(3, 3);

  rVariables.Data.ConstitutiveTensor(0, 1) = rVariables.Data.ConstitutiveTensor(0, 0) * rPoissonCoefficient / (1.0 - rPoissonCoefficient);
  rVariables.Data.ConstitutiveTensor(1, 0) = rVariables.Data.ConstitutiveTensor(0, 1);

  rVariables.Data.ConstitutiveTensor(0, 2) = rVariables.Data.ConstitutiveTensor(0, 1);
  rVariables.Data.ConstitutiveTensor(2, 0) = rVariables.Data.ConstitutiveTensor(0, 1);

  rVariables.Data.ConstitutiveTensor(1, 2) = rVariables.Data.ConstitutiveTensor(0, 1);
  rVariables.Data.ConstitutiveTensor(2, 1) = rVariables.Data.ConstitutiveTensor(0, 1);

  rVariables.State().Set(ConstitutiveModelData::CONSTITUTIVE_MATRIX_COMPUTED);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LinearElasticModel::CalculateStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  ElasticDataType Variables;
  this->InitializeVariables(rValues, Variables);

  //Set constitutive matrix to zero before adding
  const SizeType &rVoigtSize = Variables.GetModelData().GetVoigtSize();
  noalias(rConstitutiveMatrix) = ZeroMatrix(rVoigtSize,rVoigtSize);

  this->CalculateAndAddConstitutiveTensor(Variables, rConstitutiveMatrix);

  this->CalculateAndAddStressTensor(Variables, rStressMatrix);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LinearElasticModel::CalculateIsochoricStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling the base class function in LinearElasticModel ... illegal operation" << std::endl;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LinearElasticModel::CalculateVolumetricStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling the base class function in LinearElasticModel ... illegal operation" << std::endl;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LinearElasticModel::CalculateIsochoricConstitutiveTensor(ModelDataType &rValues, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  ElasticDataType Variables;
  this->InitializeVariables(rValues, Variables);
  this->CalculateAndAddIsochoricConstitutiveTensor(Variables, rConstitutiveMatrix);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LinearElasticModel::CalculateAndAddIsochoricConstitutiveTensor(ElasticDataType &rVariables, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  this->CalculateIsochoricConstitutiveTensor(rVariables);

  rConstitutiveMatrix = ConstitutiveModelUtilities::ConstitutiveTensorToMatrix(rVariables.Data.ConstitutiveTensor, rConstitutiveMatrix);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LinearElasticModel::CalculateIsochoricConstitutiveTensor(ElasticDataType &rVariables)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling the base class function in LinearElasticModel ... illegal operation" << std::endl;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LinearElasticModel::CalculateVolumetricConstitutiveTensor(ModelDataType &rValues, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  ElasticDataType Variables;
  this->InitializeVariables(rValues, Variables);
  this->CalculateAndAddVolumetricConstitutiveTensor(Variables, rConstitutiveMatrix);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LinearElasticModel::CalculateAndAddVolumetricConstitutiveTensor(ElasticDataType &rVariables, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  this->CalculateVolumetricConstitutiveTensor(rVariables);

  rConstitutiveMatrix = ConstitutiveModelUtilities::ConstitutiveTensorToMatrix(rVariables.Data.ConstitutiveTensor, rConstitutiveMatrix);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LinearElasticModel::CalculateVolumetricConstitutiveTensor(ElasticDataType &rVariables)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling the base class function in LinearElasticModel ... illegal operation" << std::endl;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

int LinearElasticModel::Check(const Properties &rProperties, const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  if (YOUNG_MODULUS.Key() == 0 || rProperties[YOUNG_MODULUS] <= 0.00)
    KRATOS_ERROR << "YOUNG_MODULUS has Key zero or invalid value" << std::endl;

  if (POISSON_RATIO.Key() == 0)
  {
    KRATOS_ERROR << "POISSON_RATIO has Key zero invalid value" << std::endl;
  }
  else
  {
    const double &nu = rProperties[POISSON_RATIO];
    if ((nu > 0.499 && nu < 0.501) || (nu < -0.999 && nu > -1.01))
      KRATOS_ERROR << "POISSON_RATIO has an invalid value" << std::endl;
  }

  return 0;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LinearElasticModel::GetRequiredProperties(Properties &rProperties)
{
  KRATOS_TRY

  // Set required properties to check keys
  double value = 0.0; //dummy
  rProperties.SetValue(YOUNG_MODULUS, value);
  rProperties.SetValue(POISSON_RATIO, value);

  KRATOS_CATCH(" ")
}

} // Namespace Kratos
