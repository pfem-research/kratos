//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:                January 2022 $
//
//

#if !defined(KRATOS_DV2_BORJA_MODEL_HPP_INCLUDED)
#define KRATOS_DV2_BORJA_MODEL_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_models/elasticity_models/borja_model.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) DV2BorjaModel : public BorjaModel
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of DV2BorjaModel
  KRATOS_CLASS_POINTER_DEFINITION(DV2BorjaModel);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  DV2BorjaModel();

  /// Copy constructor.
  DV2BorjaModel(DV2BorjaModel const &rOther);

  /// Assignment operator.
  DV2BorjaModel &operator=(DV2BorjaModel const &rOther);

  /// Clone.
  ConstitutiveModel::Pointer Clone() const override;

  /// Destructor.
  ~DV2BorjaModel() override;

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

   /**
      * Finalize member data
     */
  void InitializeModel(ModelDataType &rValues) override;
  
  void FinalizeModel(ModelDataType &rValues) override;
  /**
     * Check
     */
  int Check(const Properties &rProperties, const ProcessInfo &rCurrentProcessInfo) override;

  /**
     * Get required properties
     */
  void GetRequiredProperties(Properties &rProperties) override;


  ///@}
  ///@name Access
  ///@{
   
  void SetValue(const Variable<double> &rVariable,
        const double &rValue,
        const ProcessInfo &rCurrentProcessInfo) override;

  void SetValue(const Variable<array_1d<double,3>> &rVariable,
        const array_1d<double,3> &rValue,
        const ProcessInfo &rCurrentProcessInfo) override
  {
     BaseType::SetValue(rVariable, rValue, rCurrentProcessInfo);
  }

  void SetValue(const Variable<Vector> &rVariable,
        const Vector &rValue,
        const ProcessInfo &rCurrentProcessInfo) override;

  void SetValue(const Variable<Matrix> &rVariable,
        const Matrix &rValue,
        const ProcessInfo &rCurrentProcessInfo) override
  {
     BaseType::SetValue(rVariable, rValue, rCurrentProcessInfo);
  }

  double &GetValue(const Variable<double> &rVariable,
        double &rValue) override;

  array_1d<double, 3> &GetValue(const Variable< array_1d<double, 3> > & rVariable,
        array_1d<double, 3> & rValue) override
  {
     rValue = BaseType::GetValue(rVariable, rValue);
     return rValue;
  }

  Vector &GetValue(const Variable<Vector> &rVariable, Vector &rValue) override
  {
     rValue = BaseType::GetValue(rVariable, rValue);
     return rValue;
  }

  Matrix &GetValue(const Variable<Matrix> &rVariable, Matrix &rValue) override
  {
     rValue = BaseType::GetValue(rVariable, rValue);
     return rValue;
  }
  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "DV2BorjaModel";
    return buffer.str();
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "DV2BorjaModel";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "DV2BorjaModel Data";
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  /**
      * Calculate some strain invariants
      */


  virtual double  GetShearModulus(const Properties & rProperties) override;

  void ConvertHenckyStrainToCauchyGreen( const MatrixType & rHenckyStrain, MatrixType & rCauchyGreen);

  void ConvertCauchyGreenToHenckyStrain( const MatrixType & rCauchyGreen, MatrixType & rHenckyStrain);


  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{
  
  double mPoissonRatio;
  double mShearModulus;

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BorjaModel)
    rSerializer.save("mPoissonRatio", mPoissonRatio);
    rSerializer.save("mShearModulus", mShearModulus);
  }

  void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BorjaModel)
    rSerializer.load("mPoissonRatio", mPoissonRatio);
    rSerializer.load("mShearModulus", mShearModulus);
  }

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class DV2BorjaModel

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_DV2_BORJA_MODEL_HPP_INCLUDED  defined
