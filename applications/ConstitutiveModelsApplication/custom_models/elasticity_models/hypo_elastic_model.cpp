//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                  April 2018 $
//
//

// System includes

// External includes

// Project includes
#include "custom_models/elasticity_models/hypo_elastic_model.hpp"

namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

HypoElasticModel::HypoElasticModel()
    : ConstitutiveModel()
{
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

HypoElasticModel::HypoElasticModel(const HypoElasticModel &rOther)
    : ConstitutiveModel(rOther)
{
}

//********************************CLONE***********************************************
//************************************************************************************

ConstitutiveModel::Pointer HypoElasticModel::Clone() const
{
  return Kratos::make_shared<HypoElasticModel>(*this);
}

//********************************ASSIGNMENT******************************************
//************************************************************************************
HypoElasticModel &HypoElasticModel::operator=(HypoElasticModel const &rOther)
{
  ConstitutiveModel::operator=(rOther);
  return *this;
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

HypoElasticModel::~HypoElasticModel()
{
}

//***********************PUBLIC OPERATIONS FROM BASE CLASS****************************
//************************************************************************************

void HypoElasticModel::InitializeModel(ModelDataType &rValues)
{
  KRATOS_TRY

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void HypoElasticModel::FinalizeModel(ModelDataType &rValues)
{
  KRATOS_TRY

  //update total stress measure
  ConstitutiveModelUtilities::SymmetricTensorToVector(rValues.StressMatrix, *this->mpHistoryVector);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void HypoElasticModel::InitializeVariables(ModelDataType &rValues, ElasticDataType &rVariables)
{
  KRATOS_TRY

  //set model data pointer
  rVariables.SetModelData(rValues);
  rVariables.SetState(rValues.State);

  // ConstitutiveModelData::StressMeasureType::StressMeasure_Kirchhoff  allowed only

  // symmetric spatial velocity gradient
  MatrixType StrainMatrix;
  noalias(StrainMatrix) = 0.5 * (rValues.StrainMatrix + trans(rValues.StrainMatrix)); // spatial velocity gradient is rValues.StrainMatrix

  rValues.SetStrainMeasure(ConstitutiveModelData::StrainMeasureType::StrainMeasure_None);

  rValues.MaterialParameters.SetLameMuBar(rValues.MaterialParameters.GetLameMu());

  ConstitutiveModelUtilities::StrainTensorToVector(StrainMatrix, rVariables.Data.StrainVector);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void HypoElasticModel::CalculateStrainEnergy(ModelDataType &rValues, double &rDensityFunction)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling the base class function in HypoElasticModel ... illegal operation" << std::endl;

  KRATOS_CATCH(" ")
}

//************// W

void HypoElasticModel::CalculateAndAddIsochoricStrainEnergy(ElasticDataType &rVariables, double &rIsochoricDensityFunction)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling the base class function in HypoElasticModel ... illegal operation" << std::endl;

  KRATOS_CATCH(" ")
}

void HypoElasticModel::CalculateAndAddVolumetricStrainEnergy(ElasticDataType &rVariables, double &rVolumetricDensityFunction)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling the base class function in HypoElasticModel ... illegal operation" << std::endl;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void HypoElasticModel::AddHistoricalStress(ModelDataType &rValues, MatrixType &rStressMatrix)
{
  KRATOS_TRY

  // Add previous stress (Jaumann rate)  :: not necessary, added in the Constitutive Matrix
  const double &rDeltaTime = rValues.GetProcessInfo()[DELTA_TIME];

  // Skewsymmetric spatial velocity gradient
  // const MatrixType &rSpatialVelocityGradient = rValues.GetStrainMatrix(); // spatial velocity gradient is rValues.StrainMatrix
  // Matrix W = 0.5 * rDeltaTime * (rSpatialVelocityGradient - trans(rSpatialVelocityGradient));

  MatrixType PreviousStressMatrix;
  PreviousStressMatrix = ConstitutiveModelUtilities::VectorToSymmetricTensor(*this->mpHistoryVector, PreviousStressMatrix);
  // rStressMatrix  = prod(W, PreviousStressMatrix);
  // rStressMatrix -= prod(PreviousStressMatrix, W);

  // Rate to stress value
  rStressMatrix *= rDeltaTime;
  rStressMatrix += PreviousStressMatrix;

  // To store when FinalizeModel (can be the total or the isochoric stress)
  rValues.StressMatrix = rStressMatrix;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void HypoElasticModel::CalculateStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix)
{
  KRATOS_TRY

  ElasticDataType Variables;
  this->InitializeVariables(rValues, Variables);

  this->CalculateAndAddConstitutiveTensor(Variables);

  VectorType StressVector;
  this->CalculateAndAddStressTensor(Variables, StressVector);

  rStressMatrix = ConstitutiveModelUtilities::VectorToSymmetricTensor(StressVector, rStressMatrix);

  // Total stress stored in the HistoryVector
  this->AddHistoricalStress(rValues, rStressMatrix);

  Variables.State().Set(ConstitutiveModelData::STRESS_COMPUTED);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void HypoElasticModel::CalculateAndAddStressTensor(ElasticDataType &rVariables, VectorType &rStressVector)
{
  KRATOS_TRY

  noalias(rStressVector) = prod(rVariables.Data.ConstitutiveTensor, rVariables.Data.StrainVector);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void HypoElasticModel::CalculateIsochoricStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix)
{
  KRATOS_TRY

  ElasticDataType Variables;
  this->InitializeVariables(rValues, Variables);

  this->CalculateAndAddConstitutiveTensor(Variables);

  VectorType StressVector;
  noalias(StressVector) = ZeroVector(6);
  this->CalculateAndAddIsochoricStressTensor(Variables, StressVector);

  rStressMatrix = ConstitutiveModelUtilities::VectorToSymmetricTensor(StressVector, rStressMatrix);

  // Isochoric stress stored in the HistoryVector
  this->AddHistoricalStress(rValues, rStressMatrix);

  Variables.State().Set(ConstitutiveModelData::STRESS_COMPUTED);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void HypoElasticModel::CalculateAndAddIsochoricStressTensor(ElasticDataType &rVariables, VectorType &rStressVector)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling the class function in HypoElasticModel ... illegal operation" << std::endl;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void HypoElasticModel::CalculateVolumetricStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling the class function in HypoElasticModel ... illegal operation" << std::endl;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void HypoElasticModel::CalculateAndAddVolumetricStressTensor(ElasticDataType &rVariables, VectorType &rStressVector)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling the base class function in HypoElasticModel ... illegal operation" << std::endl;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void HypoElasticModel::CalculateConstitutiveTensor(ModelDataType &rValues, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  ElasticDataType Variables;
  this->InitializeVariables(rValues, Variables);

  //Set constitutive matrix to zero before adding
  const SizeType &rVoigtSize = Variables.GetModelData().GetVoigtSize();
  noalias(rConstitutiveMatrix) = ZeroMatrix(rVoigtSize,rVoigtSize);

  this->CalculateAndAddConstitutiveTensor(Variables, rConstitutiveMatrix);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void HypoElasticModel::CalculateAndAddConstitutiveTensor(ElasticDataType &rVariables, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  this->CalculateAndAddConstitutiveTensor(rVariables);

  rConstitutiveMatrix = ConstitutiveModelUtilities::ConstitutiveTensorToMatrix(rVariables.Data.ConstitutiveTensor, rConstitutiveMatrix);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void HypoElasticModel::CalculateAndAddConstitutiveTensor(ElasticDataType &rVariables)
{
  KRATOS_TRY

  //Calculate Elastic ConstitutiveMatrix
  const ModelDataType &rModelData = rVariables.GetModelData();
  const MaterialDataType &rMaterial = rModelData.GetMaterialParameters();

  noalias(rVariables.Data.ConstitutiveTensor) = ZeroMatrix(6,6);

  // Lame constants
  const double &rYoungModulus = rMaterial.GetYoungModulus();
  const double &rPoissonCoefficient = rMaterial.GetPoissonCoefficient();

  // 3D linear elastic constitutive matrix
  rVariables.Data.ConstitutiveTensor(0, 0) = (rYoungModulus * (1.0 - rPoissonCoefficient) / ((1.0 + rPoissonCoefficient) * (1.0 - 2.0 * rPoissonCoefficient)));
  rVariables.Data.ConstitutiveTensor(1, 1) = rVariables.Data.ConstitutiveTensor(0, 0);
  rVariables.Data.ConstitutiveTensor(2, 2) = rVariables.Data.ConstitutiveTensor(0, 0);

  rVariables.Data.ConstitutiveTensor(3, 3) = rVariables.Data.ConstitutiveTensor(0, 0) * (1.0 - 2.0 * rPoissonCoefficient) / (2.0 * (1.0 - rPoissonCoefficient));
  rVariables.Data.ConstitutiveTensor(4, 4) = rVariables.Data.ConstitutiveTensor(3, 3);
  rVariables.Data.ConstitutiveTensor(5, 5) = rVariables.Data.ConstitutiveTensor(3, 3);

  rVariables.Data.ConstitutiveTensor(0, 1) = rVariables.Data.ConstitutiveTensor(0, 0) * rPoissonCoefficient / (1.0 - rPoissonCoefficient);
  rVariables.Data.ConstitutiveTensor(1, 0) = rVariables.Data.ConstitutiveTensor(0, 1);

  rVariables.Data.ConstitutiveTensor(0, 2) = rVariables.Data.ConstitutiveTensor(0, 1);
  rVariables.Data.ConstitutiveTensor(2, 0) = rVariables.Data.ConstitutiveTensor(0, 1);

  rVariables.Data.ConstitutiveTensor(1, 2) = rVariables.Data.ConstitutiveTensor(0, 1);
  rVariables.Data.ConstitutiveTensor(2, 1) = rVariables.Data.ConstitutiveTensor(0, 1);

  //Jaumann Rate
  for (unsigned int i = 0; i<6; ++i)
    for (unsigned int j = 0; j<3; ++j)
      rVariables.Data.ConstitutiveTensor(i, j) += (*this->mpHistoryVector)[i];


  rVariables.State().Set(ConstitutiveModelData::CONSTITUTIVE_MATRIX_COMPUTED);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void HypoElasticModel::CalculateStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  ElasticDataType Variables;
  this->InitializeVariables(rValues, Variables);

  //Set constitutive matrix to zero before adding
  const SizeType &rVoigtSize = Variables.GetModelData().GetVoigtSize();
  noalias(rConstitutiveMatrix) = ZeroMatrix(rVoigtSize,rVoigtSize);

  this->CalculateAndAddConstitutiveTensor(Variables, rConstitutiveMatrix);

  VectorType StressVector;
  this->CalculateAndAddStressTensor(Variables, StressVector);

  rStressMatrix = ConstitutiveModelUtilities::VectorToSymmetricTensor(StressVector, rStressMatrix);

  // Total stress stored in the HistoryVector
  this->AddHistoricalStress(rValues, rStressMatrix);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void HypoElasticModel::CalculateIsochoricStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling the base class function in HypoElasticModel ... illegal operation" << std::endl;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void HypoElasticModel::CalculateVolumetricStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling the base class function in HypoElasticModel ... illegal operation" << std::endl;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void HypoElasticModel::CalculateIsochoricConstitutiveTensor(ModelDataType &rValues, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  ElasticDataType Variables;
  this->InitializeVariables(rValues, Variables);
  this->CalculateAndAddIsochoricConstitutiveTensor(Variables, rConstitutiveMatrix);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void HypoElasticModel::CalculateAndAddIsochoricConstitutiveTensor(ElasticDataType &rVariables, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  this->CalculateAndAddIsochoricConstitutiveTensor(rVariables);

  rConstitutiveMatrix = ConstitutiveModelUtilities::ConstitutiveTensorToMatrix(rVariables.Data.ConstitutiveTensor, rConstitutiveMatrix);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void HypoElasticModel::CalculateAndAddIsochoricConstitutiveTensor(ElasticDataType &rVariables)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling the base class function in HypoElasticModel ... illegal operation" << std::endl;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void HypoElasticModel::CalculateVolumetricConstitutiveTensor(ModelDataType &rValues, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  ElasticDataType Variables;
  this->InitializeVariables(rValues, Variables);
  this->CalculateAndAddVolumetricConstitutiveTensor(Variables, rConstitutiveMatrix);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void HypoElasticModel::CalculateAndAddVolumetricConstitutiveTensor(ElasticDataType &rVariables, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  this->CalculateAndAddVolumetricConstitutiveTensor(rVariables);

  rConstitutiveMatrix = ConstitutiveModelUtilities::ConstitutiveTensorToMatrix(rVariables.Data.ConstitutiveTensor, rConstitutiveMatrix);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void HypoElasticModel::CalculateAndAddVolumetricConstitutiveTensor(ElasticDataType &rVariables)
{
  KRATOS_TRY

  KRATOS_ERROR << "calling the base class function in HypoElasticModel ... illegal operation" << std::endl;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void HypoElasticModel::CalculateInternalVariables(ModelDataType &rValues)
{
  KRATOS_TRY

  ElasticDataType Variables;
  this->InitializeVariables(rValues, Variables);

  // set requested internal variables
  this->SetInternalVariables(rValues, Variables);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

// set requested internal variables
void HypoElasticModel::SetInternalVariables(ModelDataType &rValues, ElasticDataType &rVariables)
{
  KRATOS_TRY

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

int HypoElasticModel::Check(const Properties &rProperties, const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  if (YOUNG_MODULUS.Key() == 0 || rProperties[YOUNG_MODULUS] <= 0.00)
    KRATOS_ERROR << "YOUNG_MODULUS has Key zero or invalid value" << std::endl;

  if (POISSON_RATIO.Key() == 0)
  {
    KRATOS_ERROR << "POISSON_RATIO has Key zero invalid value" << std::endl;
  }
  else
  {
    const double &nu = rProperties[POISSON_RATIO];
    if ((nu > 0.499 && nu < 0.501) || (nu < -0.999 && nu > -1.01))
      KRATOS_ERROR << "POISSON_RATIO has an invalid value" << std::endl;
  }

  return 0;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void HypoElasticModel::GetRequiredProperties(Properties &rProperties)
{
  KRATOS_TRY

  // Set required properties to check keys
  double value = 0.0; //dummy
  rProperties.SetValue(YOUNG_MODULUS, value);
  rProperties.SetValue(POISSON_RATIO, value);

  KRATOS_CATCH(" ")
}

} // Namespace Kratos
