//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                  April 2018 $
//
//

#if !defined(KRATOS_HYPO_ELASTIC_MODEL_HPP_INCLUDED)
#define KRATOS_HYPO_ELASTIC_MODEL_HPP_INCLUDED

// System includes
#include <string>
#include <iostream>

// External includes

// Project includes
#include "custom_models/constitutive_model.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) HypoElasticModel : public ConstitutiveModel
{
protected:
  struct EigenData
  {
    array_1d<double, 3> Values;
    MatrixType Vectors;
  };

  struct StrainData
  {
    EigenData Eigen;
    MatrixType Matrix;        //right(C)=U^2 or R tensor
  };
  struct HypoElasticityData
  {
    BoundedMatrix<double, 6, 6> ConstitutiveTensor;
    VectorType StrainVector;
    StrainData Strain;
  };

public:
  ///@name Type Definitions
  ///@{
  typedef ConstitutiveModelData::ModelDataVariables<HypoElasticityData> ElasticDataType;

  /// Pointer definition of HypoElasticModel
  KRATOS_CLASS_POINTER_DEFINITION(HypoElasticModel);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  HypoElasticModel();

  /// Copy constructor.
  HypoElasticModel(HypoElasticModel const &rOther);

  /// Clone.
  ConstitutiveModel::Pointer Clone() const override;

  /// Assignment operator.
  HypoElasticModel &operator=(HypoElasticModel const &rOther);

  /// Destructor.
  ~HypoElasticModel() override;

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /**
     * Initialize member data
     */
  void InitializeModel(ModelDataType &rValues) override;

  /**
     * Finalize member data
     */
  void FinalizeModel(ModelDataType &rValues) override;

  /**
     * Calculate Strain Energy Density Functions
     */
  void CalculateStrainEnergy(ModelDataType &rValues, double &rDensityFunction) override;

  /**
     * Calculate Stresses
     */
  void CalculateStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix) override;

  void CalculateIsochoricStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix) override;

  void CalculateVolumetricStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix) override;

  /**
     * Calculate Constitutive Tensor
     */
  void CalculateConstitutiveTensor(ModelDataType &rValues, Matrix &rConstitutiveMatrix) override;

  void CalculateIsochoricConstitutiveTensor(ModelDataType &rValues, Matrix &rConstitutiveMatrix) override;

  void CalculateVolumetricConstitutiveTensor(ModelDataType &rValues, Matrix &rConstitutiveMatrix) override;

  /**
     * Calculate Stress and Constitutive Tensor
     */
  void CalculateStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix) override;

  void CalculateIsochoricStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix) override;

  void CalculateVolumetricStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix) override;

  /**
     * Calculate internal variables
     */
  void CalculateInternalVariables(ModelDataType &rValues) override;

  /**
     * Check
     */
  int Check(const Properties &rProperties, const ProcessInfo &rCurrentProcessInfo) override;

  /**
     * Get required properties
     */
  void GetRequiredProperties(Properties &rProperties) override;

  //************//

  virtual void InitializeVariables(ModelDataType &rValues, ElasticDataType &rVariables);

  /**
     * Calculate Stresses
     */
  virtual void CalculateAndAddStressTensor(ElasticDataType &rVariables, VectorType &rStressVector);

  virtual void CalculateAndAddIsochoricStressTensor(ElasticDataType &rVariables, VectorType &rStressVector);

  virtual void CalculateAndAddVolumetricStressTensor(ElasticDataType &rVariables, VectorType &rStressVector);

  /**
     * Calculate Constitutive Tensor
     */
  virtual void CalculateAndAddConstitutiveTensor(ElasticDataType &rVariables, Matrix &rConstitutiveMatrix);

  virtual void CalculateAndAddIsochoricConstitutiveTensor(ElasticDataType &rVariables, Matrix &rConstitutiveMatrix);

  virtual void CalculateAndAddVolumetricConstitutiveTensor(ElasticDataType &rVariables, Matrix &rConstitutiveMatrix);

  ///@}
  ///@name Access
  ///@{

  /**
     * method to ask the plasticity model the list of variables (dofs)  needed from the domain
     * @param rScalarVariables : list of scalar dofs
     * @param rComponentVariables :  list of vector dofs
     */
  void GetDomainVariablesList(std::vector<Variable<double>*> &rScalarVariables,
                              std::vector<Variable<array_1d<double, 3>>*> &rComponentVariables) override
  {
    KRATOS_TRY

    rComponentVariables.push_back(&VELOCITY);

    KRATOS_CATCH(" ")
  }

  /**
     * method to ask the constituitve model strain measures required
     * @param rStrainMeasures : list of strain measures
     */
  void GetStrainMeasures(std::vector<ConstitutiveModelData::StrainMeasureType> &rStrainMeasures) override
  {
    KRATOS_TRY

    rStrainMeasures.push_back(ConstitutiveModelData::StrainMeasureType::StrainMeasure_Velocity_Gradient);

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "HypoElasticModel";
    return buffer.str();
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "HypoElasticModel";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "HypoElasticModel Data";
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  /**
     * Stress update for a hypoelastic model
     */
  virtual void AddHistoricalStress(ModelDataType &rValues, MatrixType &rStressMatrix);

  virtual void CalculateAndAddConstitutiveTensor(ElasticDataType &rVariables);

  virtual void CalculateAndAddIsochoricConstitutiveTensor(ElasticDataType &rVariables);

  virtual void CalculateAndAddVolumetricConstitutiveTensor(ElasticDataType &rVariables);

  //************//

  virtual void CalculateAndAddIsochoricStrainEnergy(ElasticDataType &rVariables, double &rIsochoricDensityFunction);

  virtual void CalculateAndAddVolumetricStrainEnergy(ElasticDataType &rVariables, double &rIsochoricDensityFunction);

  // set requested internal variables
  virtual void SetInternalVariables(ModelDataType &rValues, ElasticDataType &rVariables);

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{
  using ConstitutiveModel::InitializeVariables;

  using ConstitutiveModel::CalculateAndAddStressTensor;
  using ConstitutiveModel::CalculateAndAddIsochoricStressTensor;
  using ConstitutiveModel::CalculateAndAddVolumetricStressTensor;

  using ConstitutiveModel::CalculateAndAddConstitutiveTensor;
  using ConstitutiveModel::CalculateAndAddIsochoricConstitutiveTensor;
  using ConstitutiveModel::CalculateAndAddVolumetricConstitutiveTensor;

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, ConstitutiveModel)
  }

  void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, ConstitutiveModel)
  }

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class HypoElasticModel

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_HYPO_ELASTIC_MODEL_HPP_INCLUDED  defined
