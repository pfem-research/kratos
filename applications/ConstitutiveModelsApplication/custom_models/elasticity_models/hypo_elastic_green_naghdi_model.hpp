//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:              September 2022 $
//
//

#if !defined(KRATOS_HYPO_ELASTIC_GREEN_NAGHDI_MODEL_HPP_INCLUDED)
#define KRATOS_HYPO_ELASTIC_GREEN_NAGHDI_MODEL_HPP_INCLUDED

// System includes
#include <string>
#include <iostream>

// External includes

// Project includes
#include "custom_models/elasticity_models/hypo_elastic_jaumann_model.hpp"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) HypoElasticGreenNaghdiModel : public HypoElasticJaumannModel
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of HypoElasticGreenNaghdiModel
  KRATOS_CLASS_POINTER_DEFINITION(HypoElasticGreenNaghdiModel);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  HypoElasticGreenNaghdiModel() : HypoElasticJaumannModel() {}

  /// Copy constructor.
  HypoElasticGreenNaghdiModel(HypoElasticGreenNaghdiModel const &rOther) : HypoElasticJaumannModel(rOther) {}

  /// Assignment operator.
  HypoElasticGreenNaghdiModel &operator=(HypoElasticGreenNaghdiModel const &rOther)
  {
    HypoElasticJaumannModel::operator=(rOther);
    return *this;
  }

  /// Clone.
  ConstitutiveModel::Pointer Clone() const override
  {
    return Kratos::make_shared<HypoElasticGreenNaghdiModel>(*this);
  }

  /// Destructor.
  ~HypoElasticGreenNaghdiModel() override {}

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  //************//

  /**
     * Check
     */
  int Check(const Properties &rProperties, const ProcessInfo &rCurrentProcessInfo) override
  {
    KRATOS_TRY

    return HypoElasticJaumannModel::Check(rProperties, rCurrentProcessInfo);

    KRATOS_CATCH(" ")
  }

  //************//

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "HypoElasticGreenNaghdiModel";
    return buffer.str();
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "HypoElasticGreenNaghdiModel";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
    rOStream << "HypoElasticGreenNaghdiModel Data";
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  /**
     * Rotation matrix calculation
     */
  void CalculateRotationMatrix(ModelDataType &rValues, ElasticDataType &rVariables,MatrixType &rRotationMatrix) override
  {
    KRATOS_TRY

    // NOTE: Just be careful because the rRotationMatrix reference use to be rValues.StrainMatrix (check noalias operations)

    // Green-Naghdi Rotation Matrix R

    // Compute Current Total Rotation Rn+1  // (F=R*U)  R=F*U^-1
    const MatrixType &rTotalDeformationMatrix = rValues.GetTotalDeformationMatrix();
    noalias(rValues.StrainMatrix) = prod(trans(rTotalDeformationMatrix), rTotalDeformationMatrix); // C=U²

    rVariables.Data.Strain.Eigen.Vectors.clear();
    rVariables.Data.Strain.Eigen.Values.clear();
    MathUtils<double>::GaussSeidelEigenSystem<MatrixType, MatrixType>(rValues.StrainMatrix, rVariables.Data.Strain.Eigen.Vectors, rVariables.Data.Strain.Matrix);

    rValues.StrainMatrix.clear(); // U^-1
    for (unsigned int i = 0; i < 3; i++)
      rValues.StrainMatrix(i, i) = 1.0 / std::sqrt(rVariables.Data.Strain.Matrix(i, i));

    noalias(rVariables.Data.Strain.Matrix) = prod(rVariables.Data.Strain.Eigen.Vectors, rValues.StrainMatrix);
    noalias(rValues.StrainMatrix) = prod(rVariables.Data.Strain.Matrix, trans(rVariables.Data.Strain.Eigen.Vectors));

    // TotalRotationMatrix it the Current Total Rotation Rn+1
    MatrixType TotalRotationMatrix;
    noalias(TotalRotationMatrix) = prod(rTotalDeformationMatrix,rValues.StrainMatrix);


    // Compute Previous Total Rotation Rn  // (F=R*U)  R=F*U^-1
    const MatrixType &rDeltaDeformationMatrix = rValues.GetDeltaDeformationMatrix();

    double DetF;
    rValues.StrainMatrix.clear(); // F^-1
    ConstitutiveModelUtilities::InvertMatrix3(rDeltaDeformationMatrix, rValues.StrainMatrix, DetF);

    MatrixType PreviousDeformationMatrix;
    noalias(PreviousDeformationMatrix) = prod(rValues.StrainMatrix,rTotalDeformationMatrix); // Previous Total Deformation Gradient

    noalias(rValues.StrainMatrix) = prod(trans(PreviousDeformationMatrix), PreviousDeformationMatrix); // C=U²

    rVariables.Data.Strain.Eigen.Vectors.clear();
    rVariables.Data.Strain.Eigen.Values.clear();
    MathUtils<double>::GaussSeidelEigenSystem<MatrixType, MatrixType>(rValues.StrainMatrix, rVariables.Data.Strain.Eigen.Vectors, rVariables.Data.Strain.Matrix);

    rValues.StrainMatrix.clear(); // U^-1
    for (unsigned int i = 0; i < 3; i++)
      rValues.StrainMatrix(i, i) = 1.0 / std::sqrt(rVariables.Data.Strain.Matrix(i, i));

    noalias(rVariables.Data.Strain.Matrix) = prod(rVariables.Data.Strain.Eigen.Vectors, rValues.StrainMatrix);
    noalias(rValues.StrainMatrix) = prod(rVariables.Data.Strain.Matrix, trans(rVariables.Data.Strain.Eigen.Vectors));

    // rVariables.Data.Strain.Matrix it the Previous Total Rotation Rn
    noalias(rVariables.Data.Strain.Matrix) = prod(PreviousDeformationMatrix,rValues.StrainMatrix);

    // PreviousDeformationMatrix is Green-Naghdi R = Rn+1 * Rn^T  (dR)
    noalias(rRotationMatrix) = prod(TotalRotationMatrix, trans(rVariables.Data.Strain.Matrix));


    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, HypoElasticJaumannModel)
  }

  void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, HypoElasticJaumannModel)
  }

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class HypoElasticGreenNaghdiModel

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_HYPO_ELASTIC_GREEN_NAGHDI_MODEL_HPP_INCLUDED  defined
