//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:                January 2022 $
//
//

// System includes

// External includes

// Project includes
#include "custom_models/elasticity_models/dv2_borja_model.hpp"

namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

DV2BorjaModel::DV2BorjaModel()
    : BorjaModel()
{
   mPoissonRatio = 0.0;
   mShearModulus = 0.0;
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

DV2BorjaModel::DV2BorjaModel(const DV2BorjaModel &rOther)
    : BorjaModel(rOther), mPoissonRatio(rOther.mPoissonRatio), mShearModulus(rOther.mShearModulus)
{
}

//********************************CLONE***********************************************
//************************************************************************************

ConstitutiveModel::Pointer DV2BorjaModel::Clone() const
{
   return Kratos::make_shared<DV2BorjaModel>(*this);
}

//********************************ASSIGNMENT******************************************
//************************************************************************************
DV2BorjaModel &DV2BorjaModel::operator=(DV2BorjaModel const &rOther)
{
   BorjaModel::operator=(rOther);
   mPoissonRatio = rOther.mPoissonRatio;
   mShearModulus = rOther.mShearModulus;

   return *this;
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

DV2BorjaModel::~DV2BorjaModel()
{
}


void DV2BorjaModel::SetValue(const Variable<double> &rThisVariable,
      const double &rValue,
      const ProcessInfo &rCurrentProcessInfo)
{
   KRATOS_TRY


   if ( rThisVariable == SHEAR_MODULUS)
   {
      mShearModulus = rValue;
   } else {
      HenckyHyperElasticModel::SetValue( rThisVariable, rValue, rCurrentProcessInfo);
   }

   KRATOS_CATCH("")
}

void DV2BorjaModel::SetValue(const Variable<Vector> &rThisVariable,
      const Vector &rValue,
      const ProcessInfo &rCurrentProcessInfo)
{
   KRATOS_TRY
   
   HenckyHyperElasticModel::SetValue( rThisVariable, rValue, rCurrentProcessInfo);

   KRATOS_CATCH("")
}

double &DV2BorjaModel::GetValue(const Variable<double> &rThisVariable,
      double &rValue)
{
   KRATOS_TRY

   if ( rThisVariable == SHEAR_MODULUS) {
      rValue = mShearModulus;
   } else {
      rValue = HenckyHyperElasticModel::GetValue( rThisVariable, rValue);
   }
   return rValue;

   KRATOS_CATCH("")
}

void DV2BorjaModel::InitializeModel(ModelDataType &rValues)
{
   KRATOS_TRY

   if ( this->mSetStressState) {
      // compute initial shear modulus
      double p = 0;
      for (unsigned int i = 0; i < 3; i++)
         p += this->mInitialStressState(i)/3.0;

      const Properties &rProperties = rValues.GetProperties();
      const double &rSwellingSlope = rProperties[SWELLING_SLOPE];
      const double &rPoissonRatio =  rProperties[POISSON_RATIO];
      mPoissonRatio = rPoissonRatio;

      double K = (-p)/rSwellingSlope;
      mShearModulus = 3.0*K*(1.0-2.0*mPoissonRatio) / 2.0/(1.0+mPoissonRatio);
   }
   
   BorjaModel::InitializeModel(rValues);

   KRATOS_CATCH("")
}

void DV2BorjaModel::FinalizeModel(ModelDataType &rValues)
{
  KRATOS_TRY

  MatrixType StressMatrix;
  this->CalculateStressTensor(rValues, StressMatrix);

  MatrixType StressDev = StressMatrix;
  double p  = 0;
  for (unsigned int i = 0; i < 3; i++)
     p += StressMatrix(i,i)/3.0;
  for (unsigned int i = 0; i < 3; i++)
     StressDev(i,i) -= p;


  ElasticDataType Variables;
  this->InitializeVariables(rValues, Variables);
   
  const ModelDataType &rModelData = Variables.GetModelData();
  const Properties &rProperties = rModelData.GetProperties();

  const double &rSwellingSlope = rProperties[SWELLING_SLOPE];
  const double &rPoissonRatio =  rProperties[POISSON_RATIO];
  mPoissonRatio = rPoissonRatio;

  double K = (-p)/rSwellingSlope;

  double G0 = mShearModulus;
  double G = 3.0*K*(1.0-2.0*mPoissonRatio) / 2.0/(1.0+mPoissonRatio);

  MatrixType PreviousHenckyStrain;
  ConvertCauchyGreenToHenckyStrain( rValues.StrainMatrix, PreviousHenckyStrain);
  MatrixType NewHencky;
  NewHencky.clear();
  double eVol(0.0);
  for (unsigned int i = 0; i< 3; i++)
     eVol += PreviousHenckyStrain(i,i);
  for (unsigned int i = 0; i < 3; i++) {
     NewHencky(i,i) += eVol/3.0;
     PreviousHenckyStrain(i,i) -= eVol/3.0;
  }
  NewHencky += (G0/G)*PreviousHenckyStrain;
  ConvertHenckyStrainToCauchyGreen( NewHencky, rValues.StrainMatrix);

  mShearModulus = G;

  MatrixType StressMatrix2;
  this->CalculateStressTensor(rValues, StressMatrix2);

  /*std::cout << " " << std::endl;
  std::cout << " SM1 " << StressMatrix << std::endl;
  std::cout << " SM2 " << StressMatrix2 << std::endl;
  std::cout << "diff " << StressMatrix - StressMatrix2 << std::endl;*/

  //update total strain measure
  ConstitutiveModelUtilities::SymmetricTensorToVector(rValues.StrainMatrix, *this->mpHistoryVector);


  KRATOS_CATCH(" ")
}


void DV2BorjaModel::ConvertHenckyStrainToCauchyGreen( const MatrixType & rHenckyStrain, MatrixType & rCauchyGreen)
{
   KRATOS_TRY
      
   MatrixType EigenVectors;
   EigenVectors.clear();

   rCauchyGreen.clear();
   MathUtils<double>::GaussSeidelEigenSystem<MatrixType, MatrixType>(rHenckyStrain, EigenVectors, rCauchyGreen);

   for (unsigned int i = 0; i < 3; i++)
      rCauchyGreen(i, i) = std::exp(2.0 * rCauchyGreen(i, i));

   rCauchyGreen = prod(EigenVectors, MatrixType(prod(rCauchyGreen, trans(EigenVectors))));
   
   KRATOS_CATCH("")
}

void DV2BorjaModel::ConvertCauchyGreenToHenckyStrain( const MatrixType & rCauchyGreen, MatrixType & rHenckyStrain)
{
   KRATOS_TRY
        
   MatrixType EigenVectors;
   EigenVectors.clear();

   rHenckyStrain.clear();
   MathUtils<double>::GaussSeidelEigenSystem<MatrixType, MatrixType>(rCauchyGreen, EigenVectors, rHenckyStrain);

   for (unsigned int i = 0; i < 3; i++)
      rHenckyStrain(i, i) = std::log(rHenckyStrain(i, i)) / 2.0;

   rHenckyStrain = prod(EigenVectors, MatrixType(prod(rHenckyStrain, trans(EigenVectors))));
   
   KRATOS_CATCH("")
}
double DV2BorjaModel::GetShearModulus(const Properties & rProperties)
{
   KRATOS_TRY

   return this->mShearModulus;

   KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

int DV2BorjaModel::Check(const Properties &rProperties, const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  return 0;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void DV2BorjaModel::GetRequiredProperties(Properties &rProperties)
{
  KRATOS_TRY

  // Set required properties to check keys
  double value = 0.0; //dummy
  rProperties.SetValue(SWELLING_SLOPE, value);
  rProperties.SetValue(ALPHA_SHEAR, value);
  //rProperties.SetValue(PRE_CONSOLIDATION_STRESS, value);
  rProperties.SetValue(INITIAL_SHEAR_MODULUS, value);

  KRATOS_CATCH(" ")
}

} // Namespace Kratos
