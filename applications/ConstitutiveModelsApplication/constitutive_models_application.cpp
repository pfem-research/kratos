//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                  April 2017 $
//
//

// System includes

// External includes

// Project includes
#include "constitutive_models_application.h"

namespace Kratos
{

KratosConstitutiveModelsApplication::KratosConstitutiveModelsApplication()
    : KratosApplication("ConstitutiveModelsApplication")
{
}

void KratosConstitutiveModelsApplication::Register()
{
  std::stringstream banner;

  banner << "            __  __      _           _      _          \n"
         << "    KRATOS |  \\/  |__ _| |_ ___ _ _(_)__ _| |         \n"
         << "           | |\\/| / _` |  _/ -_) '_| / _` | |         \n"
         << "           |_|  |_\\__,_|\\__\\___|_| |_\\__,_|_| MODELS\n"
         << "Initialize KratosConstitutiveModelsApplication... " << std::endl;

  // mpi initialization
  int mpi_is_initialized = 0;
  int rank = -1;

#ifdef KRATOS_MPI

  MPI_Initialized(&mpi_is_initialized);

  if (mpi_is_initialized)
  {
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  }

#endif

  if (mpi_is_initialized)
  {
    if (rank == 0)
      KRATOS_INFO("") << banner.str();
  }
  else
  {
    KRATOS_INFO("") << banner.str();
  }

  //Register Variables (variables created in constitutive_models_application_variables.cpp)
  KRATOS_REGISTER_VARIABLE(TEMPERATURE_VARIABLE)
  KRATOS_REGISTER_VARIABLE(PRESSURE_VARIABLE)
  KRATOS_REGISTER_VARIABLE(PROPERTIES_LAYOUT)

  KRATOS_REGISTER_VARIABLE(STABILIZATION_FACTOR_P)
  KRATOS_REGISTER_VARIABLE(STABILIZATION_FACTOR_J)
  KRATOS_REGISTER_VARIABLE(STABILIZATION_FACTOR_WP)

  KRATOS_REGISTER_VARIABLE(LAME_MU_BAR)

  KRATOS_REGISTER_VARIABLE(CRITICAL_STATE_LINE_PP)
  KRATOS_REGISTER_VARIABLE(ALPHA_G)
  KRATOS_REGISTER_VARIABLE(ALPHA_F)
  KRATOS_REGISTER_VARIABLE(MU_G)
  KRATOS_REGISTER_VARIABLE(MU_F)
  KRATOS_REGISTER_VARIABLE(DILATANCY_ANGLE)
  KRATOS_REGISTER_VARIABLE(GEL_STRENGTH)

  KRATOS_REGISTER_VARIABLE(RHOS)
  KRATOS_REGISTER_VARIABLE(RHOT)
  KRATOS_REGISTER_VARIABLE(KSIM)
  KRATOS_REGISTER_VARIABLE(CHIS)
  KRATOS_REGISTER_VARIABLE(CHIT)
  KRATOS_REGISTER_VARIABLE(REFERENCE_PRESSURE)

  // adding Hoek-Brown
  //KRATOS_REGISTER_VARIABLE(HB_M)
  //KRATOS_REGISTER_VARIABLE(HB_S)
  //KRATOS_REGISTER_VARIABLE(HB_UNIAXIAL_COMPRESSIVE_STRENGTH)


  KRATOS_REGISTER_VARIABLE(PS)
  KRATOS_REGISTER_VARIABLE(PT)
  KRATOS_REGISTER_VARIABLE(PM)
  KRATOS_REGISTER_VARIABLE(P0)
  KRATOS_REGISTER_VARIABLE(BONDING)

  KRATOS_REGISTER_VARIABLE(PRE_CONSOLIDATION_STRESS_SAT)
  KRATOS_REGISTER_VARIABLE(R_BBM)
  KRATOS_REGISTER_VARIABLE(BETA_BBM)

  KRATOS_REGISTER_VARIABLE(PLASTIC_VOL_DEF)
  KRATOS_REGISTER_VARIABLE(NONLOCAL_PLASTIC_VOL_DEF)
  KRATOS_REGISTER_VARIABLE(PLASTIC_VOL_DEF_ABS)
  KRATOS_REGISTER_VARIABLE(NONLOCAL_PLASTIC_VOL_DEF_ABS)
  KRATOS_REGISTER_VARIABLE(PLASTIC_DEV_DEF)
  KRATOS_REGISTER_VARIABLE(NONLOCAL_PLASTIC_DEV_DEF)
  KRATOS_REGISTER_VARIABLE(HENCKY_ELASTIC_VOLUMETRIC_STRAIN)
  KRATOS_REGISTER_VARIABLE(NONLOCAL_HENCKY_ELASTIC_VOLUMETRIC_STRAIN)

  KRATOS_REGISTER_VARIABLE(DELTA_GAMMA_MECHANIC)
  KRATOS_REGISTER_VARIABLE(DELTA_GAMMA_SUCTION)

  KRATOS_REGISTER_VARIABLE(CASM_M)
  KRATOS_REGISTER_VARIABLE(ELASTIC_JACOBIAN)

  KRATOS_REGISTER_VARIABLE(SPACING_RATIO)
  KRATOS_REGISTER_VARIABLE(SHAPE_PARAMETER)

  KRATOS_REGISTER_VARIABLE(COHESION)

  KRATOS_REGISTER_VARIABLE(FRICTION_ANGLE)
  KRATOS_REGISTER_VARIABLE(DEGRADATION_RATE_COMPRESSION)
  KRATOS_REGISTER_VARIABLE(DEGRADATION_RATE_SHEAR)
  KRATOS_REGISTER_VARIABLE(PLASTIC_DEVIATORIC_STRAIN_HARDENING)
  KRATOS_REGISTER_VARIABLE(ALPHA_TENSILE)

   KRATOS_REGISTER_VARIABLE(MAX_IMPLEX_PLASTIC_MULTIPLIER)
   KRATOS_REGISTER_VARIABLE(NORMALIZE_FLOW_RULE)
   KRATOS_REGISTER_VARIABLE(LOCALIZATION_ANGLE)
   KRATOS_REGISTER_VARIABLE(DET_ACOUSTIC_TENSOR)
   KRATOS_REGISTER_VARIABLE(ACOUSTIC_DIRECTION_N)
   KRATOS_REGISTER_VARIABLE(ACOUSTIC_DIRECTION_M)
   KRATOS_REGISTER_VARIABLE(ACOUSTIC_DIRECTION_N2)
   KRATOS_REGISTER_VARIABLE(ACOUSTIC_DIRECTION_M2)

   KRATOS_REGISTER_VARIABLE(FABRIC_TENSOR)
   KRATOS_REGISTER_VARIABLE(FABRIC_RATE)
   KRATOS_REGISTER_VARIABLE(FABRIC_RATE_DEVIATORIC)
   KRATOS_REGISTER_VARIABLE(FABRIC_TENSOR_X)
   KRATOS_REGISTER_VARIABLE(FABRIC_TENSOR_Y)
   KRATOS_REGISTER_VARIABLE(FABRIC_TENSOR_Z)
   KRATOS_REGISTER_VARIABLE(FABRIC)
   
   KRATOS_REGISTER_VARIABLE(PERZYNA_N)
   KRATOS_REGISTER_VARIABLE(PERZYNA_ETA)
  //specific constitutive models variables must be REGISTERED here

  //Register Constitutive Laws

  //outfitted python laws
  //Serializer::Register( "PythonOutfittedConstitutiveLaw", mPythonOutfittedConstitutiveLaw );

  //general constitutive laws

  //elasticity laws

  //small strain laws
  KRATOS_REGISTER_CONSTITUTIVE_LAW("SmallStrain3DLaw", mSmallStrain3DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("SmallStrainOrthotropic3DLaw", mSmallStrainOrthotropic3DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("SmallStrainPlaneStrain2DLaw", mSmallStrainPlaneStrain2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("SmallStrainPlaneStress2DLaw", mSmallStrainPlaneStress2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("SmallStrainAxisymmetric2DLaw", mSmallStrainAxisymmetric2DLaw);

  //large strain laws
  KRATOS_REGISTER_CONSTITUTIVE_LAW("LargeStrain3DLaw", mLargeStrain3DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("LargeStrainPlaneStrain2DLaw", mLargeStrainPlaneStrain2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("LargeStrainAxisymmetric2DLaw", mLargeStrainAxisymmetric2DLaw);

  //strain rate laws
  KRATOS_REGISTER_CONSTITUTIVE_LAW("StrainRate3DLaw", mStrainRate3DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("StrainRatePlaneStrain2DLaw", mStrainRatePlaneStrain2DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("NewtonianFluid3DLaw", mNewtonianFluid3DLaw);
  KRATOS_REGISTER_CONSTITUTIVE_LAW("NewtonianFluidPlaneStrain2DLaw", mNewtonianFluidPlaneStrain2DLaw);

  //general constitutive models

  //elasticity models
  Serializer::Register("LinearElasticModel", mLinearElasticModel);
  Serializer::Register("SaintVenantKirchhoffModel", mSaintVenantKirchhoffModel);
  Serializer::Register("NeoHookeanModel", mNeoHookeanModel);
  Serializer::Register("NeoHookeanLnJSquaredModel", mNeoHookeanLnJSquaredModel);
  Serializer::Register("NeoHookeanJ_1SquaredModel", mNeoHookeanJ_1SquaredModel);
  Serializer::Register("IsochoricNeoHookeanModel", mIsochoricNeoHookeanModel);
  Serializer::Register("IsochoricNeoHookeanLnJSquaredModel", mIsochoricNeoHookeanLnJSquaredModel);
  Serializer::Register("IncompressibleNeoHookeanModel", mIncompressibleNeoHookeanModel);
  Serializer::Register("IncompressibleNeoHookeanLnJSquaredModel", mIncompressibleNeoHookeanLnJSquaredModel);
  Serializer::Register("BorjaModel", mBorjaModel);
  Serializer::Register("DVBorjaModel", mDVBorjaModel);
  Serializer::Register("DV2BorjaModel", mDV2BorjaModel);
  Serializer::Register("TamagniniModel", mTamagniniModel);
  Serializer::Register("OgdenModel", mOgdenModel);
  Serializer::Register("IsochoricOgdenModel", mIsochoricOgdenModel);
  Serializer::Register("HypoElasticModel", mHypoElasticModel);
  Serializer::Register("IsochoricHypoElasticModel", mIsochoricHypoElasticModel);
  Serializer::Register("IncompressibleHypoElasticModel", mIncompressibleHypoElasticModel);
  Serializer::Register("HypoElasticJaumannModel", mHypoElasticJaumannModel);
  Serializer::Register("IsochoricHypoElasticJaumannModel", mIsochoricHypoElasticJaumannModel);
  Serializer::Register("IncompressibleHypoElasticJaumannModel", mIncompressibleHypoElasticJaumannModel);
  Serializer::Register("HypoElasticGreenNaghdiModel", mHypoElasticGreenNaghdiModel);
  Serializer::Register("IsochoricHypoElasticGreenNaghdiModel", mIsochoricHypoElasticGreenNaghdiModel);
  Serializer::Register("IncompressibleHypoElasticGreenNaghdiModel", mIncompressibleHypoElasticGreenNaghdiModel);
  Serializer::Register("NewtonianFluidModel", mNewtonianFluidModel);
  Serializer::Register("IsochoricNewtonianFluidModel", mIsochoricNewtonianFluidModel);
  Serializer::Register("IncompressibleNewtonianFluidModel", mIncompressibleNewtonianFluidModel);

  //damage models
  Serializer::Register("SimoJuExponentialDamageModel", mSimoJuExponentialDamageModel);
  Serializer::Register("SimoJuModifiedExponentialDamageModel", mSimoJuModifiedExponentialDamageModel);


  //plasticity models
  Serializer::Register("VonMisesLinearElasticPlasticityModel", mVonMisesLinearElasticPlasticityModel);

  Serializer::Register("VonMisesNeoHookeanLinearPlasticityModel", mVonMisesNeoHookeanLinearPlasticityModel);
  Serializer::Register("VonMisesNeoHookeanPlasticityModel", mVonMisesNeoHookeanPlasticityModel);
  Serializer::Register("VonMisesNeoHookeanThermoPlasticityModel", mVonMisesNeoHookeanThermoPlasticityModel);

  Serializer::Register("SimoJ2LinearPlasticityModel", mSimoJ2LinearPlasticityModel);
  Serializer::Register("SimoJ2PlasticityModel", mSimoJ2PlasticityModel);
  Serializer::Register("SimoJ2LinearThermoPlasticityModel", mSimoJ2LinearThermoPlasticityModel);
  Serializer::Register("SimoJ2ThermoPlasticityModel", mSimoJ2ThermoPlasticityModel);
  Serializer::Register("JohnsonCookJ2ThermoPlasticityModel", mJohnsonCookJ2ThermoPlasticityModel);
  Serializer::Register("BakerJohnsonCookJ2ThermoPlasticityModel", mBakerJohnsonCookJ2ThermoPlasticityModel);


  Serializer::Register("CamClayModel", mCamClayModel);
  Serializer::Register("NonlocalCamClayModel", mNonlocalCamClayModel);
  Serializer::Register("GensNovaModel", mGensNovaModel);
  Serializer::Register("V2GensNovaModel", mV2GensNovaModel);
  Serializer::Register("NonlocalV2GensNovaModel", mNonlocalV2GensNovaModel);
  Serializer::Register("NonlocalV3GensNovaModel", mNonlocalV3GensNovaModel);
  //begin modification Oliynyk 08/09/2020
  //Serializer::Register("NonlocalV1FDMilanModel", mNonlocalV1FDMilanModel);
  //end modification
  Serializer::Register("NonlocalV2FDMilanModel", mNonlocalV2FDMilanModel);
  //Serializer::Register("NonlocalV1FDMilanMCCModel", mNonlocalV1FDMilanMCCModel);
  Serializer::Register("MohrCoulombModel", mMohrCoulombModel);
  //Serializer::Register("HoekBrownModel", mHoekBrownModel);
  Serializer::Register("MohrCoulombV1Model", mMohrCoulombV1Model);
  Serializer::Register("MohrCoulombNonAssociativeModel", mMohrCoulombNonAssociativeModel);
  Serializer::Register("TrescaModel", mTrescaModel);
  Serializer::Register("TrescaDepthModel", mTrescaDepthModel);
  Serializer::Register("CasmAssociatedSoilModel", mCasmAssociatedSoilModel);
  Serializer::Register("CasmMCCSoilModel", mCasmMCCSoilModel);
  Serializer::Register("CasmNubiaSoilModel", mCasmNubiaSoilModel);
  Serializer::Register("CasmMMSoilModel", mCasmMMSoilModel);
  Serializer::Register("NonlocalCasmMCCSoilModel", mNonlocalCasmMCCSoilModel);
  Serializer::Register("NonlocalCasmNubiaSoilModel", mNonlocalCasmNubiaSoilModel);
  Serializer::Register("NonlocalCasmMMSoilModel", mNonlocalCasmMMSoilModel);
  Serializer::Register("NonlocalCasmMMdv2SoilModel", mNonlocalCasmMMdv2SoilModel);
  Serializer::Register("StructuredCasmMCCSoilModel", mStructuredCasmMCCSoilModel);
  Serializer::Register("DVStructuredCasmMCCSoilModel", mDVStructuredCasmMCCSoilModel);
  Serializer::Register("DV2StructuredCasmMCCSoilModel", mDV2StructuredCasmMCCSoilModel);
  Serializer::Register("StructuredCasmNubiaSoilModel", mStructuredCasmNubiaSoilModel);
  Serializer::Register("UnsaturatedCasmMCCSoilModel", mUnsaturatedCasmMCCSoilModel);
  Serializer::Register("UnsaturatedCasmMMSoilModel", mUnsaturatedCasmMMSoilModel);
  Serializer::Register("UnsaturatedCasmMMdv2SoilModel", mUnsaturatedCasmMMdv2SoilModel);
  Serializer::Register("SClay1SoilModel", mSClay1SoilModel);
  Serializer::Register("SClay1dv2SoilModel", mSClay1dv2SoilModel);

  //yield criteria
  Serializer::Register("MisesHuberYieldSurface", mMisesHuberYieldSurface);
  Serializer::Register("MisesHuberThermalYieldSurface", mMisesHuberThermalYieldSurface);
  Serializer::Register("SimoJuYieldSurface", mSimoJuYieldSurface);
  Serializer::Register("ModifiedMisesYieldSurface", mModifiedMisesYieldSurface);
  Serializer::Register("ModifiedCamClayYieldSurface", mModifiedCamClayYieldSurface);
  //Serializer::Register("HoekBrownYieldSurface", mHoekBrownYieldSurface); // adding hoek brown MP
  Serializer::Register("MohrCoulombV1YieldSurface", mMohrCoulombV1YieldSurface);
  Serializer::Register("MohrCoulombNonAssociativeYieldSurface", mMohrCoulombNonAssociativeYieldSurface);
  Serializer::Register("TrescaYieldSurface", mTrescaYieldSurface);
  Serializer::Register("TrescaDepthYieldSurface", mTrescaDepthYieldSurface);
  //begin modification Oliynyk 08/09/2020
  Serializer::Register("FDMilanNonAssociativeYieldSurface", mFDMilanNonAssociativeYieldSurface);
  //end modification
  Serializer::Register("SClay1YieldSurface", mSClay1YieldSurface);

  //plastic potentials
  Serializer::Register("FDMilanPlasticPotential", mFDMilanPlasticPotential);
  Serializer::Register("GensNovaPlasticPotential", mGensNovaPlasticPotential);
  Serializer::Register("MMPlasticPotential", mMMPlasticPotential);
  Serializer::Register("ModifiedCamClayPlasticPotential", mModifiedCamClayPlasticPotential);
  Serializer::Register("ModifiedCamClayStructurePlasticPotential", mModifiedCamClayStructurePlasticPotential);
  Serializer::Register("MohrCoulombPlasticPotential", mMohrCoulombPlasticPotential);
  Serializer::Register("NubiaPlasticPotential", mNubiaPlasticPotential);
  Serializer::Register("NubiaStructurePlasticPotential", mNubiaStructurePlasticPotential);


  //hardening rules
  Serializer::Register("SimoExponentialHardeningRule", mSimoExponentialHardeningRule);
  Serializer::Register("SimoLinearHardeningRule", mSimoLinearHardeningRule);
  Serializer::Register("SimoExponentialThermalHardeningRule", mSimoExponentialThermalHardeningRule);
  Serializer::Register("SimoLinearThermalHardeningRule", mSimoLinearThermalHardeningRule);
  Serializer::Register("JohnsonCookThermalHardeningRule", mJohnsonCookThermalHardeningRule);
  Serializer::Register("BakerJohnsonCookThermalHardeningRule", mBakerJohnsonCookThermalHardeningRule);
  Serializer::Register("ExponentialDamageHardeningRule", mExponentialDamageHardeningRule);
  Serializer::Register("ModifiedExponentialDamageHardeningRule", mModifiedExponentialDamageHardeningRule);
  Serializer::Register("CamClayHardeningRule", mCamClayHardeningRule);
  Serializer::Register("GensNovaHardeningRule", mGensNovaHardeningRule);
  //Serializer::Register("HoekBrownHardeningRule", mHoekBrownHardeningRule);
  Serializer::Register("MohrCoulombV1HardeningRule", mMohrCoulombV1HardeningRule);

  //begin modification Oliynyk 11/11/2020
  Serializer::Register("FDMilanHardeningRule", mFDMilanHardeningRule);
  //end modification
  Serializer::Register("SClay1HardeningRule", mSClay1HardeningRule);
}
} // namespace Kratos.
