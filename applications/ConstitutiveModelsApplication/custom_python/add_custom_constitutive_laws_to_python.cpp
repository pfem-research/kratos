//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                  April 2017 $
//
//

// System includes
#include <pybind11/stl.h>

// External includes

// Project includes
#include "includes/constitutive_law.h"
//#include "python/pointer_vector_set_python_interface.h"
//#include "python/variable_indexing_python.h"
#include "custom_python/add_custom_constitutive_laws_to_python.h"

// Outfitted python laws
//#include "custom_python/python_outfitted_constitutive_law.hpp"

// Constitutive laws:

// Small strain laws
#include "custom_laws/small_strain_laws/small_strain_orthotropic_3D_law.hpp"
#include "custom_laws/small_strain_laws/small_strain_plane_strain_2D_law.hpp"
#include "custom_laws/small_strain_laws/small_strain_plane_stress_2D_law.hpp"
#include "custom_laws/small_strain_laws/small_strain_axisymmetric_2D_law.hpp"

// Large strain laws
#include "custom_laws/large_strain_laws/large_strain_plane_strain_2D_law.hpp"
#include "custom_laws/large_strain_laws/large_strain_axisymmetric_2D_law.hpp"

// Strain rate laws
#include "custom_laws/strain_rate_laws/strain_rate_plane_strain_2D_law.hpp"
#include "custom_laws/strain_rate_laws/newtonian_fluid_plane_strain_2D_law.hpp"

// Constitutive models:

// Elasticity models
#include "custom_models/elasticity_models/linear_elastic_model.hpp"
#include "custom_models/elasticity_models/saint_venant_kirchhoff_model.hpp"
#include "custom_models/elasticity_models/neo_hookean_model.hpp"
#include "custom_models/elasticity_models/neo_hookean_lnJ_squared_model.hpp"
#include "custom_models/elasticity_models/neo_hookean_J_1_squared_model.hpp"
#include "custom_models/elasticity_models/isochoric_neo_hookean_model.hpp"
#include "custom_models/elasticity_models/isochoric_neo_hookean_lnJ_squared_model.hpp"
#include "custom_models/elasticity_models/incompressible_neo_hookean_model.hpp"
#include "custom_models/elasticity_models/incompressible_neo_hookean_lnJ_squared_model.hpp"
#include "custom_models/elasticity_models/borja_model.hpp"
#include "custom_models/elasticity_models/dv_borja_model.hpp"
#include "custom_models/elasticity_models/dv2_borja_model.hpp"
#include "custom_models/elasticity_models/ogden_model.hpp"
#include "custom_models/elasticity_models/isochoric_ogden_model.hpp"
#include "custom_models/elasticity_models/incompressible_hypo_elastic_model.hpp"
#include "custom_models/elasticity_models/incompressible_hypo_elastic_jaumann_model.hpp"
#include "custom_models/elasticity_models/incompressible_hypo_elastic_green_naghdi_model.hpp"
#include "custom_models/elasticity_models/incompressible_newtonian_fluid_model.hpp"

// Plasticity models
#include "custom_models/plasticity_models/von_mises_linear_elastic_plasticity_model.hpp"
#include "custom_models/plasticity_models/von_mises_neo_hookean_linear_plasticity_model.hpp"
#include "custom_models/plasticity_models/von_mises_neo_hookean_plasticity_model.hpp"
#include "custom_models/plasticity_models/von_mises_neo_hookean_thermo_plasticity_model.hpp"
#include "custom_models/plasticity_models/simo_J2_linear_plasticity_model.hpp"
#include "custom_models/plasticity_models/simo_J2_plasticity_model.hpp"
#include "custom_models/plasticity_models/simo_J2_thermo_plasticity_model.hpp"
#include "custom_models/plasticity_models/simo_J2_linear_thermo_plasticity_model.hpp"
#include "custom_models/plasticity_models/johnson_cook_J2_thermo_plasticity_model.hpp"
#include "custom_models/plasticity_models/baker_johnson_cook_J2_thermo_plasticity_model.hpp"
#include "custom_models/plasticity_models/cam_clay_model.hpp"
#include "custom_models/plasticity_models/nonlocal_cam_clay_model.hpp"
#include "custom_models/plasticity_models/gens_nova_model.hpp"
#include "custom_models/plasticity_models/v2_gens_nova_model.hpp"
#include "custom_models/plasticity_models/nonlocal_v2_gens_nova_model.hpp"
#include "custom_models/plasticity_models/nonlocal_v3_gens_nova_model.hpp"
#include "custom_models/plasticity_models/simo_ju_exponential_damage_model.hpp"
#include "custom_models/plasticity_models/simo_ju_modified_exponential_damage_model.hpp"
#include "custom_models/plasticity_models/mohr_coulomb_v1_model.hpp"
//#include "custom_models/plasticity_models/hoek_brown_model.hpp"
#include "custom_models/plasticity_models/mohr_coulomb_model.hpp"
#include "custom_models/plasticity_models/mohr_coulomb_non_associative_model.hpp"
#include "custom_models/plasticity_models/tresca_model.hpp"
#include "custom_models/plasticity_models/tresca_depth_model.hpp"
#include "custom_models/plasticity_models/casm_associated_soil_model.hpp"
#include "custom_models/plasticity_models/casm_mcc_soil_model.hpp"
#include "custom_models/plasticity_models/casm_nubia_soil_model.hpp"
#include "custom_models/plasticity_models/casm_mm_soil_model.hpp"
#include "custom_models/plasticity_models/structured_casm_nubia_soil_model.hpp"
#include "custom_models/plasticity_models/structured_casm_mcc_soil_model.hpp"
#include "custom_models/plasticity_models/dv_structured_casm_mcc_soil_model.hpp"
#include "custom_models/plasticity_models/dv2_structured_casm_mcc_soil_model.hpp"
#include "custom_models/plasticity_models/nonlocal_casm_mcc_soil_model.hpp"
#include "custom_models/plasticity_models/nonlocal_casm_nubia_soil_model.hpp"
#include "custom_models/plasticity_models/nonlocal_casm_mm_soil_model.hpp"
#include "custom_models/plasticity_models/nonlocal_casm_mm_dv2_soil_model.hpp"
//#include "custom_models/plasticity_models/nonlocal_v1_FDMilan_model.hpp"
#include "custom_models/plasticity_models/nonlocal_v2_FDMilan_model.hpp"
//#include "custom_models/plasticity_models/nonlocal_v1_FDMilan_MCC_model.hpp"
#include "custom_models/plasticity_models/sclay1_soil_model.hpp"
#include "custom_models/plasticity_models/sclay1_dv2_soil_model.hpp"
#include "custom_models/plasticity_models/unsaturated_casm_mcc_soil_model.hpp"
#include "custom_models/plasticity_models/unsaturated_casm_mm_soil_model.hpp"
#include "custom_models/plasticity_models/unsaturated_casm_mm_dv2_soil_model.hpp"


namespace Kratos
{
namespace Python
{

namespace py = pybind11;

typedef ConstitutiveLaw ConstitutiveLawBaseType;
typedef ConstitutiveLaw::Pointer ConstitutiveLawPointer;
typedef std::vector<ConstitutiveLaw::Pointer> MaterialsContainer;

typedef ConstitutiveModel::Pointer ConstitutiveModelPointer;

//MECHANICAL plasticity base types (PlasticityType)
typedef PlasticityModel<LinearElasticModel, MisesHuberYieldSurface<SimoExponentialHardeningRule>> LinearVonMisesPlasticityBaseType;
typedef PlasticityModel<IsochoricNeoHookeanModel, MisesHuberYieldSurface<SimoLinearHardeningRule>> VonMisesLinearPlasticityBaseType;
typedef PlasticityModel<IsochoricNeoHookeanModel, MisesHuberYieldSurface<SimoExponentialHardeningRule>> VonMisesPlasticityBaseType;
typedef PlasticityModel<IsochoricNeoHookeanModel, MisesHuberThermalYieldSurface<SimoExponentialThermalHardeningRule>> VonMisesThermalPlasticityBaseType;
typedef PlasticityModel<IncompressibleNeoHookeanModel, MisesHuberYieldSurface<SimoLinearHardeningRule>> SimoLinearPlasticityBaseType;
typedef PlasticityModel<IncompressibleNeoHookeanModel, MisesHuberYieldSurface<SimoExponentialHardeningRule>> SimoPlasticityBaseType;
typedef PlasticityModel<IncompressibleNeoHookeanModel, MisesHuberThermalYieldSurface<SimoLinearThermalHardeningRule>> SimoThermalLinearPlasticityBaseType;
typedef PlasticityModel<IncompressibleNeoHookeanModel, MisesHuberThermalYieldSurface<SimoExponentialThermalHardeningRule>> SimoThermalPlasticityBaseType;
typedef PlasticityModel<IncompressibleNeoHookeanModel, MisesHuberThermalYieldSurface<JohnsonCookThermalHardeningRule>> JohnsonCookPlasticityBaseType;
typedef PlasticityModel<IncompressibleNeoHookeanModel, MisesHuberThermalYieldSurface<BakerJohnsonCookThermalHardeningRule>> BakerJohnsonCookPlasticityBaseType;

//NonLinearAssociativePlasticityModel::PlasticityModel (BaseType)
typedef NonLinearAssociativePlasticityModel<LinearElasticModel, MisesHuberYieldSurface<SimoExponentialHardeningRule>> LinearVonMisesBaseType;
typedef NonLinearAssociativePlasticityModel<IsochoricNeoHookeanModel, MisesHuberYieldSurface<SimoLinearHardeningRule>> VonMisesLinearBaseType;
typedef NonLinearAssociativePlasticityModel<IsochoricNeoHookeanModel, MisesHuberYieldSurface<SimoExponentialHardeningRule>> VonMisesBaseType;
typedef NonLinearAssociativePlasticityModel<IsochoricNeoHookeanModel, MisesHuberThermalYieldSurface<SimoExponentialThermalHardeningRule>> VonMisesThermalBaseType;
typedef NonLinearAssociativePlasticityModel<IncompressibleNeoHookeanModel, MisesHuberYieldSurface<SimoLinearHardeningRule>> SimoLinearBaseType;
typedef NonLinearAssociativePlasticityModel<IncompressibleNeoHookeanModel, MisesHuberYieldSurface<SimoExponentialHardeningRule>> SimoBaseType;
typedef NonLinearAssociativePlasticityModel<IncompressibleNeoHookeanModel, MisesHuberThermalYieldSurface<SimoLinearThermalHardeningRule>> SimoThermalLinearBaseType;
typedef NonLinearAssociativePlasticityModel<IncompressibleNeoHookeanModel, MisesHuberThermalYieldSurface<SimoExponentialThermalHardeningRule>> SimoThermalBaseType;
typedef NonLinearAssociativePlasticityModel<IncompressibleNeoHookeanModel, MisesHuberThermalYieldSurface<JohnsonCookThermalHardeningRule>> JohnsonCookBaseType;
typedef NonLinearAssociativePlasticityModel<IncompressibleNeoHookeanModel, MisesHuberThermalYieldSurface<BakerJohnsonCookThermalHardeningRule>> BakerJohnsonCookBaseType;

//LinearAssociativePlasticityModel::NonLinearAssociativePlasticityModel::PlasticityModel (DerivedType)
typedef LinearAssociativePlasticityModel<IsochoricNeoHookeanModel, MisesHuberYieldSurface<SimoLinearHardeningRule>> VonMisesLinearDerivedType;
typedef LinearAssociativePlasticityModel<IncompressibleNeoHookeanModel, MisesHuberYieldSurface<SimoLinearHardeningRule>> SimoLinearDerivedType;
typedef LinearAssociativePlasticityModel<IncompressibleNeoHookeanModel, MisesHuberThermalYieldSurface<SimoLinearThermalHardeningRule>> SimoThermalLinearDerivedType;

//NonLinearRateDependentPlasticityModel::NonLinearAssociativePlasticityModel::PlasticityModel (RateType)
typedef NonLinearRateDependentPlasticityModel<IncompressibleNeoHookeanModel, MisesHuberThermalYieldSurface<JohnsonCookThermalHardeningRule>> JohnsonCookRateType;
typedef NonLinearRateDependentPlasticityModel<IncompressibleNeoHookeanModel, MisesHuberThermalYieldSurface<BakerJohnsonCookThermalHardeningRule>> BakerJohnsonCookRateType;

//GEO-MECHANICAL plasticity base types (PlasticityType)
typedef PlasticityModel<BorjaModel, ModifiedCamClayYieldSurface<CamClayHardeningRule>> CamClayPlasticityBaseType;
//typedef PlasticityModel<HenckyLinearModel, HoekBrownYieldSurface<HoekBrownHardeningRule>> HoekBrownPlasticityBaseType;
typedef PlasticityModel<HenckyLinearModel, MohrCoulombYieldSurface<MohrCoulombHardeningRule>> MohrCoulombPlasticityBaseType;
typedef PlasticityModel<HenckyLinearModel, MohrCoulombV1YieldSurface<MohrCoulombV1HardeningRule>> MohrCoulombV1PlasticityBaseType;
typedef PlasticityModel<HenckyLinearModel, TrescaYieldSurface<MohrCoulombV1HardeningRule>> TrescaPlasticityBaseType;
typedef PlasticityModel<HenckyDepthModel, TrescaDepthYieldSurface<MohrCoulombV1HardeningRule>> TrescaDepthPlasticityBaseType;
typedef PlasticityModel<TamagniniModel, GensNovaYieldSurface<GensNovaHardeningRule>> GensNovaPlasticityBaseType;
typedef PlasticityModel<HenckyLinearModel, MohrCoulombNonAssociativeYieldSurface<MohrCoulombV1HardeningRule>> NonAssociativeMohrCoulombPlasticityBaseType;
typedef PlasticityModel<TamagniniModel, GensNovaNonAssociativeYieldSurface<GensNovaHardeningRule>> NonAssociativeGensNovaPlasticityBaseType;
typedef PlasticityModel<TamagniniModel, FDMilanNonAssociativeYieldSurface<FDMilanHardeningRule>> MilanModelPlasticityBaseType;
typedef PlasticityModel<TamagniniModel, FDMilanNonAssociativeYieldSurface<FDMilanHardeningRule>> MilanModelPlasticityBaseType2;
//typedef PlasticityModel<TamagniniModel, GensNovaNonAssociativeYieldSurface<FDMilanMCCHardeningRule>> MilanMCCModelPlasticityBaseType;
typedef PlasticityModel<BorjaModel, CasmYieldSurface<CasmHardeningRule>> CasmSoilPlasticityBaseType;
typedef PlasticityModel<DV2BorjaModel, CasmYieldSurface<CasmHardeningRule>> DV2CasmSoilPlasticityBaseType;
typedef PlasticityModel<BorjaModel, CasmStructureYieldSurface<CasmStructureHardeningRule>> StructuredCasmSoilPlasticityBaseType;
typedef PlasticityModel<DVBorjaModel, CasmStructureYieldSurface<CasmStructureHardeningRule>> DVStructuredCasmSoilPlasticityBaseType;
typedef PlasticityModel<DV2BorjaModel, CasmStructureYieldSurface<CasmStructureHardeningRule>> DV2StructuredCasmSoilPlasticityBaseType;
typedef PlasticityModel<BorjaModel, SClay1YieldSurface<SClay1HardeningRule>> FabricSoilPlasticityBaseType;
typedef PlasticityModel<DV2BorjaModel, SClay1YieldSurface<SClay1HardeningRule>> DV2FabricSoilPlasticityBaseType;
typedef PlasticityModel<BorjaModel, UnsaturatedCasmYieldSurface<UnsaturatedCasmHardeningRule>> UnsaturatedCasmSoilPlasticityBaseType;
typedef PlasticityModel<DV2BorjaModel, UnsaturatedCasmYieldSurface<UnsaturatedCasmHardeningRule>> DV2UnsaturatedCasmSoilPlasticityBaseType;

//SoilBaseModel::PlasticityModel (SoilBaseType)
typedef SoilBaseModel<BorjaModel, ModifiedCamClayYieldSurface<CamClayHardeningRule>> CamClaySoilBaseType;
//typedef SoilBaseModel<HenckyLinearModel, HoekBrownYieldSurface<HoekBrownHardeningRule>> HoekBrownSoilBaseType;
typedef SoilBaseModel<HenckyLinearModel, MohrCoulombYieldSurface<MohrCoulombHardeningRule>> MohrCoulombSoilBaseType;
typedef SoilBaseModel<HenckyLinearModel, MohrCoulombV1YieldSurface<MohrCoulombV1HardeningRule>> MohrCoulombV1SoilBaseType;
typedef SoilBaseModel<HenckyLinearModel, TrescaYieldSurface<MohrCoulombV1HardeningRule>> TrescaSoilBaseType;
typedef SoilBaseModel<HenckyDepthModel, TrescaDepthYieldSurface<MohrCoulombV1HardeningRule>> TrescaDepthSoilBaseType;
typedef SoilBaseModel<TamagniniModel, GensNovaYieldSurface<GensNovaHardeningRule>> GensNovaSoilBaseType;
typedef SoilBaseModel<HenckyLinearModel, MohrCoulombNonAssociativeYieldSurface<MohrCoulombV1HardeningRule>> NonAssociativeMohrCoulombSoilBaseType;
typedef SoilBaseModel<TamagniniModel, GensNovaNonAssociativeYieldSurface<GensNovaHardeningRule>> NonAssociativeGensNovaSoilBaseType;
typedef SoilBaseModel<TamagniniModel, FDMilanNonAssociativeYieldSurface<FDMilanHardeningRule>> MilanModelSoilBaseType;
typedef SoilBaseModel<TamagniniModel, FDMilanNonAssociativeYieldSurface<FDMilanHardeningRule>> MilanModelSoilBaseType2;
//typedef SoilBaseModel<TamagniniModel, GensNovaNonAssociativeYieldSurface<FDMilanMCCHardeningRule>> MilanMCCModelSoilBaseType;
typedef SoilBaseModel<BorjaModel, CasmYieldSurface<CasmHardeningRule>> CasmSoilSoilBaseType;
typedef SoilBaseModel<DV2BorjaModel, CasmYieldSurface<CasmHardeningRule>> DV2CasmSoilSoilBaseType;
typedef SoilNonAssociativeModel<BorjaModel, CasmStructureYieldSurface<CasmStructureHardeningRule>> StructuredCasmSoilSoilBaseType;
typedef SoilNonAssociativeModel<DVBorjaModel, CasmStructureYieldSurface<CasmStructureHardeningRule>> DVStructuredCasmSoilSoilBaseType;
typedef SoilNonAssociativeModel<DV2BorjaModel, CasmStructureYieldSurface<CasmStructureHardeningRule>> DV2StructuredCasmSoilSoilBaseType;
typedef SoilBaseModel<BorjaModel, SClay1YieldSurface<SClay1HardeningRule>> FabricSoilSoilBaseType;
typedef SoilBaseModel<DV2BorjaModel, SClay1YieldSurface<SClay1HardeningRule>> DV2FabricSoilSoilBaseType;
typedef SoilBaseModel<BorjaModel, UnsaturatedCasmYieldSurface<UnsaturatedCasmHardeningRule>> UnsaturatedCasmSoilSoilBaseType;
typedef SoilBaseModel<DV2BorjaModel, UnsaturatedCasmYieldSurface<UnsaturatedCasmHardeningRule>> DV2UnsaturatedCasmSoilSoilBaseType;

//StructuredSoilModel::SoilBaseModel::PlasticityModel (BaseType)
typedef StructuredSoilModel<TamagniniModel, GensNovaYieldSurface<GensNovaHardeningRule>> GensNovaBaseType;

//SoilNonAssociativeModel::SoilBaseModel::PlasticityModel (BaseType)
typedef SoilNonAssociativeModel<HenckyLinearModel, MohrCoulombNonAssociativeYieldSurface<MohrCoulombV1HardeningRule>> NonAssociativeMohrCoulombBaseType;
typedef SoilNonAssociativeModel<TamagniniModel, GensNovaNonAssociativeYieldSurface<GensNovaHardeningRule>> NonAssociativeGensNovaBaseType;
typedef SoilNonAssociativeModel<TamagniniModel, FDMilanNonAssociativeYieldSurface<FDMilanHardeningRule>> MilanModelBaseType;
typedef SoilNonAssociativeModel<TamagniniModel, FDMilanNonAssociativeYieldSurface<FDMilanHardeningRule>> MilanModelBaseType2;
//typedef SoilNonAssociativeModel<TamagniniModel, GensNovaNonAssociativeYieldSurface<FDMilanMCCHardeningRule>> MilanMCCModelBaseType;

//StructuredNonAssociativeSoilModel::SoilNonAssociativeModel::SoilBaseModel::PlasticityModel  (DerivedType)
typedef StructuredNonAssociativeSoilModel<TamagniniModel, GensNovaNonAssociativeYieldSurface<GensNovaHardeningRule>> NonAssociativeGensNovaDerivedType;
typedef StructuredFDMilanNonAssociativeSoilModel<TamagniniModel, FDMilanNonAssociativeYieldSurface<FDMilanHardeningRule>> MilanModelDerivedType;
typedef StructuredFDMilanNonAssociativeSoilModel<TamagniniModel, FDMilanNonAssociativeYieldSurface<FDMilanHardeningRule>> MilanModelDerivedType2;
//typedef StructuredFDMilanNonAssociativeSoilModel<TamagniniModel, GensNovaNonAssociativeYieldSurface<FDMilanMCCHardeningRule>> MilanMCCModelDerivedType;


//CasmBaseSoilModel::SoilBaseModel::PlasticityModel (BaseType)
typedef CasmBaseSoilModel<BorjaModel, CasmYieldSurface<CasmHardeningRule>> CasmSoilBaseType;
typedef CasmBaseSoilModel<DV2BorjaModel, CasmYieldSurface<CasmHardeningRule>> DV2CasmSoilBaseType;
typedef StructuredCasmBaseSoilModel<BorjaModel, CasmStructureYieldSurface<CasmStructureHardeningRule>> StructuredCasmSoilBaseType;
typedef StructuredCasmBaseSoilModel<DVBorjaModel, CasmStructureYieldSurface<CasmStructureHardeningRule>> DVStructuredCasmSoilBaseType;
typedef StructuredCasmBaseSoilModel<DV2BorjaModel, CasmStructureYieldSurface<CasmStructureHardeningRule>> DV2StructuredCasmSoilBaseType;
typedef FabricBaseSoilModel<BorjaModel, SClay1YieldSurface<SClay1HardeningRule>> FabricSoilBaseType;
typedef FabricBaseSoilModel<DV2BorjaModel, SClay1YieldSurface<SClay1HardeningRule>> DV2FabricSoilBaseType;
typedef UnsaturatedCasmBaseSoilModel<BorjaModel, UnsaturatedCasmYieldSurface<UnsaturatedCasmHardeningRule>> UnsaturatedCasmSoilBaseType;
typedef UnsaturatedCasmBaseSoilModel<DV2BorjaModel, UnsaturatedCasmYieldSurface<UnsaturatedCasmHardeningRule>> DV2UnsaturatedCasmSoilBaseType;

typedef SoilBaseModel<HenckyLinearModel, TrescaYieldSurface<MohrCoulombV1HardeningRule>> TrescaBaseType;
typedef SoilBaseModel<HenckyDepthModel, TrescaDepthYieldSurface<MohrCoulombV1HardeningRule>> TrescaDepthBaseType;
//DAMAGE plasticity base types (PlasticityType)
typedef PlasticityModel<LinearElasticModel, SimoJuYieldSurface<ExponentialDamageHardeningRule>> DamageModelPlasticityBaseType;
typedef PlasticityModel<LinearElasticModel, SimoJuYieldSurface<ModifiedExponentialDamageHardeningRule>> ModifiedDamageModelPlasticityBaseType;

//DamageModel::PlasticityModel (BaseType)
typedef DamageModel<LinearElasticModel, SimoJuYieldSurface<ExponentialDamageHardeningRule>> DamageModelBaseType;
typedef DamageModel<LinearElasticModel, SimoJuYieldSurface<ModifiedExponentialDamageHardeningRule>> ModifiedDamageModelBaseType;


void AddCustomConstitutiveLawsToPython(pybind11::module &m)
{

    //outfitted python laws
    // py::class_< PythonOutfittedConstitutiveLaw, typename PythonOutfittedConstitutiveLaw::Pointer, ConstitutiveLawBaseType >
    //  	(m, "PythonOutfittedConstitutiveLaw")
    //  	.def( py::init<>() )
    //  	.def(init<PyObject* >())
    //  	;

    //general constitutive 3D law
    py::class_<Constitutive3DLaw, typename Constitutive3DLaw::Pointer, ConstitutiveLawBaseType>(m, "Constitutive3DLaw")
        .def(py::init<>())
        .def("CheckRequiredProperties",&Constitutive3DLaw::CheckRequiredProperties);

    //small strain laws
    py::class_<SmallStrain3DLaw, typename SmallStrain3DLaw::Pointer, Constitutive3DLaw>(m, "SmallStrain3DLaw")
        .def(py::init<>())
        .def(py::init<ConstitutiveModelPointer>());

    py::class_<SmallStrainOrthotropic3DLaw, typename SmallStrainOrthotropic3DLaw::Pointer, Constitutive3DLaw>(m, "SmallStrainOrthotropic3DLaw")
        .def(py::init<>())
        .def(py::init<ConstitutiveModelPointer>());

    py::class_<SmallStrainPlaneStrain2DLaw, typename SmallStrainPlaneStrain2DLaw::Pointer, Constitutive3DLaw>(m, "SmallStrainPlaneStrain2DLaw")
        .def(py::init<>())
        .def(py::init<ConstitutiveModelPointer>());

    py::class_<SmallStrainPlaneStress2DLaw, typename SmallStrainPlaneStress2DLaw::Pointer, Constitutive3DLaw>(m, "SmallStrainPlaneStress2DLaw")
        .def(py::init<>())
        .def(py::init<ConstitutiveModelPointer>());

    py::class_<SmallStrainAxisymmetric2DLaw, typename SmallStrainAxisymmetric2DLaw::Pointer, Constitutive3DLaw>(m, "SmallStrainAxisymmetric2DLaw")
        .def(py::init<>())
        .def(py::init<ConstitutiveModelPointer>());

    //large strain laws
    py::class_<LargeStrain3DLaw, typename LargeStrain3DLaw::Pointer, Constitutive3DLaw>(m, "LargeStrain3DLaw")
        .def(py::init<ConstitutiveModelPointer>());

    py::class_<LargeStrainPlaneStrain2DLaw, typename LargeStrainPlaneStrain2DLaw::Pointer, Constitutive3DLaw>(m, "LargeStrainPlaneStrain2DLaw")
        .def(py::init<ConstitutiveModelPointer>());

    py::class_<LargeStrainAxisymmetric2DLaw, typename LargeStrainAxisymmetric2DLaw::Pointer, Constitutive3DLaw>(m, "LargeStrainAxisymmetric2DLaw")
        .def(py::init<ConstitutiveModelPointer>());

    //strain rate laws
    py::class_<StrainRate3DLaw, typename StrainRate3DLaw::Pointer, Constitutive3DLaw>(m, "StrainRate3DLaw")
        .def(py::init<ConstitutiveModelPointer>());

    py::class_<StrainRatePlaneStrain2DLaw, typename StrainRatePlaneStrain2DLaw::Pointer, Constitutive3DLaw>(m, "StrainRatePlaneStrain2DLaw")
        .def(py::init<ConstitutiveModelPointer>());

    py::class_<NewtonianFluid3DLaw, typename NewtonianFluid3DLaw::Pointer, ConstitutiveLawBaseType>(m, "NewtonianFluid3DLaw")
        .def(py::init<>());

    py::class_<NewtonianFluidPlaneStrain2DLaw, typename NewtonianFluidPlaneStrain2DLaw::Pointer, ConstitutiveLawBaseType>(m, "NewtonianFluidPlaneStrain2DLaw")
        .def(py::init<>());

    //general constitutive model
    py::class_<ConstitutiveModel, typename ConstitutiveModel::Pointer>(m, "ConstitutiveModel")
        .def(py::init<>());

    //general constitutive elasticity models
    py::class_<HyperElasticModel, typename HyperElasticModel::Pointer, ConstitutiveModel>(m, "HyperElasticModel")
        .def(py::init<>());
    py::class_<MooneyRivlinModel, typename MooneyRivlinModel::Pointer, HyperElasticModel>(m, "MooneyRivlinModel")
        .def(py::init<>());
    py::class_<IsochoricMooneyRivlinModel, typename IsochoricMooneyRivlinModel::Pointer, MooneyRivlinModel>(m, "IsochoricMooneyRivlinModel")
        .def(py::init<>());
    py::class_<HenckyHyperElasticModel, typename HenckyHyperElasticModel::Pointer, HyperElasticModel>(m, "HenckyHyperElasticModel")
        .def(py::init<>());





    //elasticity models
    py::class_<LinearElasticModel, typename LinearElasticModel::Pointer, ConstitutiveModel>(m, "LinearElasticModel")
        .def(py::init<>());

    py::class_<SaintVenantKirchhoffModel, typename SaintVenantKirchhoffModel::Pointer, HyperElasticModel>(m, "SaintVenantKirchhoffModel")
        .def(py::init<>());

    py::class_<NeoHookeanModel, typename NeoHookeanModel::Pointer, MooneyRivlinModel>(m, "NeoHookeanModel")
        .def(py::init<>());

    py::class_<NeoHookeanLnJSquaredModel, typename NeoHookeanLnJSquaredModel::Pointer, NeoHookeanModel>(m, "NeoHookeanLnJSquaredModel")
        .def(py::init<>());

    py::class_<NeoHookeanJ_1SquaredModel, typename NeoHookeanJ_1SquaredModel::Pointer, NeoHookeanModel>(m, "NeoHookeanJ_1SquaredModel")
        .def(py::init<>());

    py::class_<IsochoricNeoHookeanModel, typename IsochoricNeoHookeanModel::Pointer, IsochoricMooneyRivlinModel>(m, "IsochoricNeoHookeanModel")
        .def(py::init<>());

    py::class_<IsochoricNeoHookeanLnJSquaredModel, typename IsochoricNeoHookeanLnJSquaredModel::Pointer, IsochoricNeoHookeanModel>(m, "IsochoricNeoHookeanLnJSquaredModel")
        .def(py::init<>());

    py::class_<IncompressibleNeoHookeanModel, typename IncompressibleNeoHookeanModel::Pointer, IsochoricNeoHookeanModel>(m, "IncompressibleNeoHookeanModel")
        .def(py::init<>());

    py::class_<IncompressibleNeoHookeanLnJSquaredModel, typename IncompressibleNeoHookeanLnJSquaredModel::Pointer, IsochoricNeoHookeanLnJSquaredModel>(m, "IncompressibleNeoHookeanLnJSquaredModel")
        .def(py::init<>());

    py::class_<BorjaModel, typename BorjaModel::Pointer, HenckyHyperElasticModel>(m, "BorjaModel")
        .def(py::init<>());

    py::class_<OgdenModel, typename OgdenModel::Pointer, HyperElasticModel>(m, "OgdenModel")
        .def(py::init<>());

    py::class_<IsochoricOgdenModel, typename IsochoricOgdenModel::Pointer, OgdenModel>(m, "IsochoricOgdenModel")
        .def(py::init<>());

    py::class_<HypoElasticModel, typename HypoElasticModel::Pointer, ConstitutiveModel>(m, "HypoElasticModel")
        .def(py::init<>());

    py::class_<IsochoricHypoElasticModel, typename IsochoricHypoElasticModel::Pointer, HypoElasticModel>(m, "IsochoricHypoElasticModel")
        .def(py::init<>());

    py::class_<IncompressibleHypoElasticModel, typename IncompressibleHypoElasticModel::Pointer, IsochoricHypoElasticModel>(m, "IncompressibleHypoElasticModel")
        .def(py::init<>());

    py::class_<HypoElasticJaumannModel, typename HypoElasticJaumannModel::Pointer, ConstitutiveModel>(m, "HypoElasticJaumannModel")
        .def(py::init<>());

    py::class_<IsochoricHypoElasticJaumannModel, typename IsochoricHypoElasticJaumannModel::Pointer, HypoElasticJaumannModel>(m, "IsochoricHypoElasticJaumannModel")
        .def(py::init<>());

    py::class_<IncompressibleHypoElasticJaumannModel, typename IncompressibleHypoElasticJaumannModel::Pointer, IsochoricHypoElasticJaumannModel>(m, "IncompressibleHypoElasticJaumannModel")
        .def(py::init<>());

    py::class_<HypoElasticGreenNaghdiModel, typename HypoElasticGreenNaghdiModel::Pointer, ConstitutiveModel>(m, "HypoElasticGreenNaghdiModel")
        .def(py::init<>());

    py::class_<IsochoricHypoElasticGreenNaghdiModel, typename IsochoricHypoElasticGreenNaghdiModel::Pointer, HypoElasticGreenNaghdiModel>(m, "IsochoricHypoElasticGreenNaghdiModel")
        .def(py::init<>());

    py::class_<IncompressibleHypoElasticGreenNaghdiModel, typename IncompressibleHypoElasticGreenNaghdiModel::Pointer, IsochoricHypoElasticGreenNaghdiModel>(m, "IncompressibleHypoElasticGreenNaghdiModel")
        .def(py::init<>());

    py::class_<NewtonianFluidModel, typename NewtonianFluidModel::Pointer, ConstitutiveModel>(m, "NewtonianFluidModel")
        .def(py::init<>());

    py::class_<IsochoricNewtonianFluidModel, typename IsochoricNewtonianFluidModel::Pointer, NewtonianFluidModel>(m, "IsochoricNewtonianFluidModel")
        .def(py::init<>());

    py::class_<IncompressibleNewtonianFluidModel, typename IncompressibleNewtonianFluidModel::Pointer, IsochoricNewtonianFluidModel>(m, "IncompressibleNewtonianFluidModel")
        .def(py::init<>());

    //plasticity base types
    py::class_<LinearVonMisesPlasticityBaseType, typename LinearVonMisesPlasticityBaseType::Pointer, ConstitutiveModel>(m, "LinearVonMisesPlasticityBaseType");
    py::class_<VonMisesLinearPlasticityBaseType, typename VonMisesLinearPlasticityBaseType::Pointer, ConstitutiveModel>(m, "VonMisesLinearPlasticityBaseType");
    py::class_<VonMisesPlasticityBaseType, typename VonMisesPlasticityBaseType::Pointer, ConstitutiveModel>(m, "VonMisesPlasticityBaseType");
    py::class_<VonMisesThermalPlasticityBaseType, typename VonMisesThermalPlasticityBaseType::Pointer, ConstitutiveModel>(m, "VonMisesThermalPlasticityBaseType");
    py::class_<SimoLinearPlasticityBaseType, typename SimoLinearPlasticityBaseType::Pointer, ConstitutiveModel>(m, "SimoLinearPlasticityBaseType");
    py::class_<SimoPlasticityBaseType, typename SimoPlasticityBaseType::Pointer, ConstitutiveModel>(m, "SimoPlasticityBaseType");
    py::class_<SimoThermalLinearPlasticityBaseType, typename SimoThermalLinearPlasticityBaseType::Pointer, ConstitutiveModel>(m, "SimoThermalLinearPlasticityBaseType");
    py::class_<SimoThermalPlasticityBaseType, typename SimoThermalPlasticityBaseType::Pointer, ConstitutiveModel>(m, "SimoThermalPlasticityBaseType");
    py::class_<JohnsonCookPlasticityBaseType, typename JohnsonCookPlasticityBaseType::Pointer, ConstitutiveModel>(m, "JohnsonCookPlasticityBaseType");
    py::class_<BakerJohnsonCookPlasticityBaseType, typename BakerJohnsonCookPlasticityBaseType::Pointer, ConstitutiveModel>(m, "BakerJohnsonCookPlasticityBaseType");

    py::class_<CamClayPlasticityBaseType, typename CamClayPlasticityBaseType::Pointer, ConstitutiveModel>(m, "CamClayPlasticityBaseType");
    //py::class_<HoekBrownPlasticityBaseType, typename HoekBrownPlasticityBaseType::Pointer, ConstitutiveModel>(m, "HoekBrownPlasticityBaseType");
    py::class_<MohrCoulombPlasticityBaseType, typename MohrCoulombPlasticityBaseType::Pointer, ConstitutiveModel>(m, "MohrCoulombPlasticityBaseType");
    py::class_<MohrCoulombV1PlasticityBaseType, typename MohrCoulombV1PlasticityBaseType::Pointer, ConstitutiveModel>(m, "MohrCoulombV1PlasticityBaseType");
    py::class_<TrescaPlasticityBaseType, typename TrescaPlasticityBaseType::Pointer, ConstitutiveModel>(m, "TrescaPlasticityBaseType");
    py::class_<TrescaDepthPlasticityBaseType, typename TrescaDepthPlasticityBaseType::Pointer, ConstitutiveModel>(m, "TrescaDepthPlasticityBaseType");
    py::class_<GensNovaPlasticityBaseType, typename GensNovaPlasticityBaseType::Pointer, ConstitutiveModel>(m, "GensNovaPlasticityBaseType");
    py::class_<NonAssociativeMohrCoulombPlasticityBaseType, typename NonAssociativeMohrCoulombPlasticityBaseType::Pointer, ConstitutiveModel>(m, "NonAssociativeMohrCoulombPlasticityBaseType");
    py::class_<NonAssociativeGensNovaPlasticityBaseType, typename NonAssociativeGensNovaPlasticityBaseType::Pointer, ConstitutiveModel>(m, "NonAssociativeGensNovaPlasticityBaseType");
    py::class_<MilanModelPlasticityBaseType, typename MilanModelPlasticityBaseType::Pointer, ConstitutiveModel>(m, "MilanModelPlasticityBaseType");
    //py::class_<MilanMCCModelPlasticityBaseType, typename MilanMCCModelPlasticityBaseType::Pointer, ConstitutiveModel>(m, "MilanMCCModelPlasticityBaseType");
    py::class_<CasmSoilPlasticityBaseType, typename CasmSoilPlasticityBaseType::Pointer, ConstitutiveModel>(m, "CasmSoilPlasticityBaseType");
    py::class_<DV2CasmSoilPlasticityBaseType, typename DV2CasmSoilPlasticityBaseType::Pointer, ConstitutiveModel>(m, "DV2CasmSoilPlasticityBaseType");
    py::class_<StructuredCasmSoilPlasticityBaseType, typename StructuredCasmSoilPlasticityBaseType::Pointer, ConstitutiveModel>(m, "StructuredCasmSoilPlasticityBaseType");
    py::class_<DVStructuredCasmSoilPlasticityBaseType, typename DVStructuredCasmSoilPlasticityBaseType::Pointer, ConstitutiveModel>(m, "DVStructuredCasmSoilPlasticityBaseType");
    py::class_<DV2StructuredCasmSoilPlasticityBaseType, typename DV2StructuredCasmSoilPlasticityBaseType::Pointer, ConstitutiveModel>(m, "DV2StructuredCasmSoilPlasticityBaseType");
    py::class_<FabricSoilPlasticityBaseType, typename FabricSoilPlasticityBaseType::Pointer, ConstitutiveModel>(m, "FabricSoilPlasticityBaseType");
    py::class_<DV2FabricSoilPlasticityBaseType, typename DV2FabricSoilPlasticityBaseType::Pointer, ConstitutiveModel>(m, "DV2FabricSoilPlasticityBaseType");
    py::class_<UnsaturatedCasmSoilPlasticityBaseType, typename UnsaturatedCasmSoilPlasticityBaseType::Pointer, ConstitutiveModel>(m, "UnsaturatedCasmSoilPlasticityBaseType");
    py::class_<DV2UnsaturatedCasmSoilPlasticityBaseType, typename DV2UnsaturatedCasmSoilPlasticityBaseType::Pointer, ConstitutiveModel>(m, "DV2UnsaturatedCasmSoilPlasticityBaseType");
    py::class_<DamageModelPlasticityBaseType, typename DamageModelPlasticityBaseType::Pointer, ConstitutiveModel>(m, "DamageModelPlasticityBaseType");
    py::class_<ModifiedDamageModelPlasticityBaseType, typename ModifiedDamageModelPlasticityBaseType::Pointer, ConstitutiveModel>(m, "ModifiedDamageModelPlasticityBaseType");


    //nonlinear plasticity base types
    py::class_<LinearVonMisesBaseType, typename LinearVonMisesBaseType::Pointer, LinearVonMisesPlasticityBaseType>(m, "LinearVonMisesBaseType");
    py::class_<VonMisesLinearBaseType, typename VonMisesLinearBaseType::Pointer, VonMisesLinearPlasticityBaseType>(m, "VonMisesLinearBaseType");
    py::class_<VonMisesBaseType, typename VonMisesBaseType::Pointer, VonMisesPlasticityBaseType>(m, "VonMisesBaseType");
    py::class_<VonMisesThermalBaseType, typename VonMisesThermalBaseType::Pointer, VonMisesThermalPlasticityBaseType>(m, "VonMisesThermalBaseType");
    py::class_<SimoLinearBaseType, typename SimoLinearBaseType::Pointer, SimoLinearPlasticityBaseType>(m, "SimoLinearBaseType");
    py::class_<SimoBaseType, typename SimoBaseType::Pointer, SimoPlasticityBaseType>(m, "SimoBaseType");
    py::class_<SimoThermalLinearBaseType, typename SimoThermalLinearBaseType::Pointer, SimoThermalLinearPlasticityBaseType>(m, "SimoThermalLinearBaseType");
    py::class_<SimoThermalBaseType, typename SimoThermalBaseType::Pointer, SimoThermalPlasticityBaseType>(m, "SimoThermalBaseType");
    py::class_<JohnsonCookBaseType, typename JohnsonCookBaseType::Pointer, JohnsonCookPlasticityBaseType>(m, "JohnsonCookBaseType");
    py::class_<BakerJohnsonCookBaseType, typename BakerJohnsonCookBaseType::Pointer, BakerJohnsonCookPlasticityBaseType>(m, "BakerJohnsonCookBaseType");

    //linear plasticity base types
    py::class_<VonMisesLinearDerivedType, typename VonMisesLinearDerivedType::Pointer, VonMisesLinearBaseType>(m, "VonMisesLinearDerivedType");
    py::class_<SimoLinearDerivedType, typename SimoLinearDerivedType::Pointer, SimoLinearBaseType>(m, "SimoLinearDerivedType");
    py::class_<SimoThermalLinearDerivedType, typename SimoThermalLinearDerivedType::Pointer, SimoThermalLinearBaseType>(m, "SimoThermalLinearDerivedType");

    //rate dependent plasticity base types
    py::class_<JohnsonCookRateType, typename JohnsonCookRateType::Pointer, JohnsonCookBaseType>(m, "JohnsonCookRateType");
    py::class_<BakerJohnsonCookRateType, typename BakerJohnsonCookRateType::Pointer, BakerJohnsonCookBaseType>(m, "BakerJohnsonCookRateType");

    //soil base types
    py::class_<CamClaySoilBaseType, typename CamClaySoilBaseType::Pointer, CamClayPlasticityBaseType>(m, "CamClaySoilBaseType");
    //py::class_<HoekBrownSoilBaseType, typename HoekBrownSoilBaseType::Pointer, HoekBrownPlasticityBaseType>(m, "HoekBrownSoilBaseType");
    py::class_<MohrCoulombSoilBaseType, typename MohrCoulombSoilBaseType::Pointer, MohrCoulombPlasticityBaseType>(m, "MohrCoulombSoilBaseType");
    py::class_<MohrCoulombV1SoilBaseType, typename MohrCoulombV1SoilBaseType::Pointer, MohrCoulombV1PlasticityBaseType>(m, "MohrCoulombV1SoilBaseType");
    py::class_<TrescaSoilBaseType, typename TrescaSoilBaseType::Pointer, TrescaPlasticityBaseType>(m, "TrescaSoilBaseType");
    py::class_<TrescaDepthSoilBaseType, typename TrescaDepthSoilBaseType::Pointer, TrescaDepthPlasticityBaseType>(m, "TrescaDepthSoilBaseType");
    py::class_<GensNovaSoilBaseType, typename GensNovaSoilBaseType::Pointer, GensNovaPlasticityBaseType>(m, "GensNovaSoilBaseType");
    py::class_<NonAssociativeMohrCoulombSoilBaseType, typename NonAssociativeMohrCoulombSoilBaseType::Pointer, NonAssociativeMohrCoulombPlasticityBaseType>(m, "NonAssociativeMohrCoulombSoilBaseType");
    py::class_<NonAssociativeGensNovaSoilBaseType, typename NonAssociativeGensNovaSoilBaseType::Pointer, NonAssociativeGensNovaPlasticityBaseType>(m, "NonAssociativeGensNovaSoilBaseType");
    py::class_<MilanModelSoilBaseType, typename MilanModelSoilBaseType::Pointer, MilanModelPlasticityBaseType>(m, "MilanModelSoilBaseType");
    //py::class_<MilanMCCModelSoilBaseType, typename MilanMCCModelSoilBaseType::Pointer, MilanMCCModelPlasticityBaseType>(m, "MilanMCCModelSoilBaseType");
    py::class_<CasmSoilSoilBaseType, typename CasmSoilSoilBaseType::Pointer, CasmSoilPlasticityBaseType>(m, "CasmSoilSoilBaseType");
    py::class_<DV2CasmSoilSoilBaseType, typename DV2CasmSoilSoilBaseType::Pointer, DV2CasmSoilPlasticityBaseType>(m, "DV2CasmSoilSoilBaseType");
    py::class_<StructuredCasmSoilSoilBaseType, typename StructuredCasmSoilSoilBaseType::Pointer, StructuredCasmSoilPlasticityBaseType>(m, "StructuredCasmSoilSoilBaseType");
    py::class_<DVStructuredCasmSoilSoilBaseType, typename DVStructuredCasmSoilSoilBaseType::Pointer, DVStructuredCasmSoilPlasticityBaseType>(m, "DVStructuredCasmSoilSoilBaseType");
    py::class_<DV2StructuredCasmSoilSoilBaseType, typename DV2StructuredCasmSoilSoilBaseType::Pointer, DV2StructuredCasmSoilPlasticityBaseType>(m, "DV2StructuredCasmSoilSoilBaseType");
    py::class_<FabricSoilSoilBaseType, typename FabricSoilSoilBaseType::Pointer, FabricSoilPlasticityBaseType>(m, "FabricSoilSoilBaseType");
    py::class_<DV2FabricSoilSoilBaseType, typename DV2FabricSoilSoilBaseType::Pointer, DV2FabricSoilPlasticityBaseType>(m, "DV2FabricSoilSoilBaseType");
    py::class_<UnsaturatedCasmSoilSoilBaseType, typename UnsaturatedCasmSoilSoilBaseType::Pointer, UnsaturatedCasmSoilPlasticityBaseType>(m, "UnsaturatedCasmSoilSoilBaseType");
    py::class_<DV2UnsaturatedCasmSoilSoilBaseType, typename DV2UnsaturatedCasmSoilSoilBaseType::Pointer, DV2UnsaturatedCasmSoilPlasticityBaseType>(m, "DV2UnsaturatedCasmSoilSoilBaseType");

    //non associative soil base types
    py::class_<NonAssociativeMohrCoulombBaseType, typename NonAssociativeMohrCoulombBaseType::Pointer, NonAssociativeMohrCoulombSoilBaseType>(m, "NonAssociativeMohrCoulombBaseType");
    py::class_<NonAssociativeGensNovaBaseType, typename NonAssociativeGensNovaBaseType::Pointer, NonAssociativeGensNovaSoilBaseType>(m, "NonAssociativeGensNovaBaseType");
    py::class_<MilanModelBaseType, typename MilanModelBaseType::Pointer, MilanModelSoilBaseType>(m, "MilanModelBaseType");
    //py::class_<MilanMCCModelBaseType, typename MilanMCCModelBaseType::Pointer, MilanMCCModelSoilBaseType>(m, "MilanMCCModelBaseType");

    //structure soil base type
    py::class_<GensNovaBaseType, typename GensNovaBaseType::Pointer, GensNovaSoilBaseType>(m, "GensNovaBaseType");

    //structure non-associative soil base type
    py::class_<NonAssociativeGensNovaDerivedType, typename NonAssociativeGensNovaDerivedType::Pointer, NonAssociativeGensNovaBaseType>(m, "NonAssociativeGensNovaDerivedType");
    py::class_<MilanModelDerivedType, typename MilanModelDerivedType::Pointer, MilanModelBaseType>(m, "MilanModelDerivedType");
    //py::class_<MilanMCCModelDerivedType, typename MilanMCCModelDerivedType::Pointer, MilanMCCModelBaseType>(m, "MilanMCCModelDerivedType");

    //casm model base type
    py::class_<CasmSoilBaseType, typename CasmSoilBaseType::Pointer, CasmSoilSoilBaseType>(m, "CasmSoilBaseType");
    py::class_<DV2CasmSoilBaseType, typename DV2CasmSoilBaseType::Pointer, DV2CasmSoilSoilBaseType>(m, "DV2CasmSoilBaseType");
    py::class_<StructuredCasmSoilBaseType, typename StructuredCasmSoilBaseType::Pointer, StructuredCasmSoilSoilBaseType>(m, "StructuredCasmSoilBaseType");
    py::class_<DVStructuredCasmSoilBaseType, typename DVStructuredCasmSoilBaseType::Pointer, DVStructuredCasmSoilSoilBaseType>(m, "DVStructuredCasmSoilBaseType");
    py::class_<DV2StructuredCasmSoilBaseType, typename DV2StructuredCasmSoilBaseType::Pointer, DV2StructuredCasmSoilSoilBaseType>(m, "DV2StructuredCasmSoilBaseType");
    py::class_<FabricSoilBaseType, typename FabricSoilBaseType::Pointer, FabricSoilSoilBaseType>(m, "FabricSoilBaseType");
    py::class_<DV2FabricSoilBaseType, typename DV2FabricSoilBaseType::Pointer, DV2FabricSoilSoilBaseType>(m, "DV2FabricSoilBaseType");
    py::class_<UnsaturatedCasmSoilBaseType, typename UnsaturatedCasmSoilBaseType::Pointer, UnsaturatedCasmSoilSoilBaseType>(m, "UnsaturatedCasmSoilBaseType");
    py::class_<DV2UnsaturatedCasmSoilBaseType, typename DV2UnsaturatedCasmSoilBaseType::Pointer, DV2UnsaturatedCasmSoilSoilBaseType>(m, "DV2UnsaturatedCasmSoilBaseType");

    //damage model base types
    py::class_<DamageModelBaseType, typename DamageModelBaseType::Pointer, DamageModelPlasticityBaseType>(m, "DamageModelBaseType");
    py::class_<ModifiedDamageModelBaseType, typename ModifiedDamageModelBaseType::Pointer, ModifiedDamageModelPlasticityBaseType>(m, "ModifiedDamageModelBaseType");
    // py::class_<, typename ::Pointer, >(m, "");
    // py::class_<, typename ::Pointer, >(m, "");




    //callable MECHANICAL plasticity models
    py::class_<VonMisesLinearElasticPlasticityModel, typename VonMisesLinearElasticPlasticityModel::Pointer, LinearVonMisesBaseType>(m, "VonMisesLinearElasticPlasticityModel")
        .def(py::init<>());

    py::class_<VonMisesNeoHookeanLinearPlasticityModel, typename VonMisesNeoHookeanLinearPlasticityModel::Pointer, VonMisesLinearDerivedType>(m, "VonMisesNeoHookeanLinearPlasticityModel")
        .def(py::init<>());

    py::class_<VonMisesNeoHookeanPlasticityModel, typename VonMisesNeoHookeanPlasticityModel::Pointer, VonMisesBaseType>(m, "VonMisesNeoHookeanPlasticityModel")
        .def(py::init<>());

    py::class_<VonMisesNeoHookeanThermoPlasticityModel, typename VonMisesNeoHookeanThermoPlasticityModel::Pointer, VonMisesThermalBaseType>(m, "VonMisesNeoHookeanThermoPlasticityModel")
        .def(py::init<>());

    py::class_<SimoJ2LinearPlasticityModel, typename SimoJ2LinearPlasticityModel::Pointer, SimoLinearDerivedType>(m, "SimoJ2LinearPlasticityModel")
        .def(py::init<>());

    py::class_<SimoJ2PlasticityModel, typename SimoJ2PlasticityModel::Pointer, SimoBaseType>(m, "SimoJ2PlasticityModel")
        .def(py::init<>());

    py::class_<SimoJ2ThermoPlasticityModel, typename SimoJ2ThermoPlasticityModel::Pointer, SimoThermalBaseType>(m, "SimoJ2ThermoPlasticityModel")
        .def(py::init<>());

    py::class_<SimoJ2LinearThermoPlasticityModel, typename SimoJ2LinearThermoPlasticityModel::Pointer, SimoThermalLinearDerivedType>(m, "SimoJ2LinearThermoPlasticityModel")
        .def(py::init<>());

    py::class_<JohnsonCookJ2ThermoPlasticityModel, typename JohnsonCookJ2ThermoPlasticityModel::Pointer, JohnsonCookRateType>(m, "JohnsonCookJ2ThermoPlasticityModel")
        .def(py::init<>());

    py::class_<BakerJohnsonCookJ2ThermoPlasticityModel, typename BakerJohnsonCookJ2ThermoPlasticityModel::Pointer, BakerJohnsonCookRateType>(m, "BakerJohnsonCookJ2ThermoPlasticityModel")
        .def(py::init<>());


    //callable GEO-MECHANICAL plasticity models
    py::class_<CamClayModel, typename CamClayModel::Pointer, CamClaySoilBaseType>(m, "CamClayModel")
        .def(py::init<>());

    py::class_<NonlocalCamClayModel, typename NonlocalCamClayModel::Pointer, CamClaySoilBaseType>(m, "NonlocalCamClayModel")
        .def(py::init<>());

    py::class_<GensNovaModel, typename GensNovaModel::Pointer, GensNovaBaseType>(m, "GensNovaModel")
        .def(py::init<>());

    py::class_<V2GensNovaModel, typename V2GensNovaModel::Pointer, GensNovaBaseType>(m, "V2GensNovaModel")
        .def(py::init<>());

    py::class_<NonlocalV2GensNovaModel, typename NonlocalV2GensNovaModel::Pointer, GensNovaBaseType>(m, "NonlocalV2GensNovaModel")
        .def(py::init<>());

    py::class_<NonlocalV3GensNovaModel, typename NonlocalV3GensNovaModel::Pointer, NonAssociativeGensNovaDerivedType>(m, "NonlocalV3GensNovaModel")
        .def(py::init<>());

    /*py::class_<NonlocalV1FDMilanModel, typename NonlocalV1FDMilanModel::Pointer, MilanModelDerivedType>(m, "NonlocalV1FDMilanModel")
        .def(py::init<>());*/

    py::class_<NonlocalV2FDMilanModel, typename NonlocalV2FDMilanModel::Pointer, MilanModelDerivedType2>(m, "NonlocalV2FDMilanModel")
        .def(py::init<>());

    /*py::class_<NonlocalV1FDMilanMCCModel, typename NonlocalV1FDMilanMCCModel::Pointer, MilanMCCModelDerivedType>(m, "NonlocalV1FDMilanMCCModel")
        .def(py::init<>());*/

    /*py::class_<HoekBrownModel, typename HoekBrownModel::Pointer, HoekBrownSoilBaseType>(m, "HoekBrownModel")
        .def(py::init<>());*/

    py::class_<MohrCoulombModel, typename MohrCoulombModel::Pointer, MohrCoulombSoilBaseType>(m, "MohrCoulombModel")
        .def(py::init<>());

    py::class_<MohrCoulombV1Model, typename MohrCoulombV1Model::Pointer, MohrCoulombV1SoilBaseType>(m, "MohrCoulombV1Model")
        .def(py::init<>());

    py::class_<MohrCoulombNonAssociativeModel, typename MohrCoulombNonAssociativeModel::Pointer, NonAssociativeMohrCoulombBaseType>(m, "MohrCoulombNonAssociativeModel")
        .def(py::init<>());

    py::class_<CasmAssociatedSoilModel, typename CasmAssociatedSoilModel::Pointer, CasmSoilBaseType>(m, "CasmAssociatedSoilModel")
        .def(py::init<>());

    py::class_<CasmMCCSoilModel, typename CasmMCCSoilModel::Pointer, CasmSoilBaseType>(m, "CasmMCCSoilModel")
        .def(py::init<>());

    py::class_<CasmNubiaSoilModel, typename CasmNubiaSoilModel::Pointer, CasmSoilBaseType>(m, "CasmNubiaSoilModel")
        .def(py::init<>());

    py::class_<CasmMMSoilModel, typename CasmMMSoilModel::Pointer, CasmSoilBaseType>(m, "CasmMMSoilModel")
        .def(py::init<>());

    py::class_<StructuredCasmNubiaSoilModel, typename StructuredCasmNubiaSoilModel::Pointer, StructuredCasmSoilBaseType>(m, "StructuredCasmNubiaSoilModel")
        .def(py::init<>());

    py::class_<StructuredCasmMCCSoilModel, typename StructuredCasmMCCSoilModel::Pointer, StructuredCasmSoilBaseType>(m, "StructuredCasmMCCSoilModel")
        .def(py::init<>());

    py::class_<DVStructuredCasmMCCSoilModel, typename DVStructuredCasmMCCSoilModel::Pointer, DVStructuredCasmSoilBaseType>(m, "DVStructuredCasmMCCSoilModel")
        .def(py::init<>());

    py::class_<DV2StructuredCasmMCCSoilModel, typename DV2StructuredCasmMCCSoilModel::Pointer, DV2StructuredCasmSoilBaseType>(m, "DV2StructuredCasmMCCSoilModel")
        .def(py::init<>());

    py::class_<NonlocalCasmMCCSoilModel, typename NonlocalCasmMCCSoilModel::Pointer, CasmSoilBaseType>(m, "NonlocalCasmMCCSoilModel")
        .def(py::init<>());

    py::class_<NonlocalCasmNubiaSoilModel, typename NonlocalCasmNubiaSoilModel::Pointer, CasmSoilBaseType>(m, "NonlocalCasmNubiaSoilModel")
        .def(py::init<>());

    py::class_<NonlocalCasmMMSoilModel, typename NonlocalCasmMMSoilModel::Pointer, CasmSoilBaseType>(m, "NonlocalCasmMMSoilModel")
        .def(py::init<>());

    py::class_<NonlocalCasmMMdv2SoilModel, typename NonlocalCasmMMdv2SoilModel::Pointer, DV2CasmSoilBaseType>(m, "NonlocalCasmMMdv2SoilModel")
        .def(py::init<>());

    py::class_<TrescaModel, typename TrescaModel::Pointer, TrescaSoilBaseType>(m, "TrescaModel")
        .def(py::init<>());

    py::class_<TrescaDepthModel, typename TrescaDepthModel::Pointer, TrescaDepthSoilBaseType>(m, "TrescaDepthModel")
        .def(py::init<>());

    py::class_<SClay1SoilModel, typename SClay1SoilModel::Pointer, FabricSoilBaseType>(m, "SClay1SoilModel")
        .def(py::init<>());

    py::class_<SClay1dv2SoilModel, typename SClay1dv2SoilModel::Pointer, DV2FabricSoilBaseType>(m, "SClay1dv2SoilModel")
        .def(py::init<>());
    
    py::class_<UnsaturatedCasmMCCSoilModel, typename UnsaturatedCasmMCCSoilModel::Pointer, UnsaturatedCasmSoilBaseType>(m, "UnsaturatedCasmMCCSoilModel")
        .def(py::init<>());

    py::class_<UnsaturatedCasmMMSoilModel, typename UnsaturatedCasmMMSoilModel::Pointer, UnsaturatedCasmSoilBaseType>(m, "UnsaturatedCasmMMSoilModel")
        .def(py::init<>());

    py::class_<UnsaturatedCasmMMdv2SoilModel, typename UnsaturatedCasmMMdv2SoilModel::Pointer, DV2UnsaturatedCasmSoilBaseType>(m, "UnsaturatedCasmMMdv2SoilModel")
        .def(py::init<>());

    //callable DAMAGE models
    py::class_<SimoJuExponentialDamageModel, typename SimoJuExponentialDamageModel::Pointer, DamageModelBaseType>(m, "SimoJuExponentialDamageModel")
        .def(py::init<>());

    py::class_<SimoJuModifiedExponentialDamageModel, typename SimoJuModifiedExponentialDamageModel::Pointer, ModifiedDamageModelBaseType>(m, "SimoJuModifiedExponentialDamageModel")
        .def(py::init<>());
}

} // namespace Python.
} // namespace Kratos.
