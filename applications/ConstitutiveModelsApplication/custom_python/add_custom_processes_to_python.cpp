//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                   July 2016 $
//
//

// System includes

// External includes

// Project includes
#include "custom_python/add_custom_processes_to_python.h"

//Processes
#include "custom_processes/non_local_plasticity_process.hpp"
#include "custom_processes/depth_varying_yield_stress_process.hpp"
#include "custom_processes/convert_matrix_process.hpp"

namespace Kratos
{

namespace Python
{

void AddCustomProcessesToPython(pybind11::module &m)
{

    namespace py = pybind11;

    // Set initial mechanical state
    py::class_<NonLocalPlasticityProcess, NonLocalPlasticityProcess::Pointer, Process>(m, "NonLocalPlasticityProcess")
        .def(py::init<ModelPart &, Parameters>())
        .def("Execute", &NonLocalPlasticityProcess::Execute);
    // Set initial mechanical state
    py::class_<DepthVaryingYieldStressProcess, DepthVaryingYieldStressProcess::Pointer, Process>(m, "DepthVaryingYieldStressProcess")
        .def(py::init<ModelPart &, Parameters>())
        .def("Execute", &DepthVaryingYieldStressProcess::Execute);
    py::class_<ConvertMatrixProcess, ConvertMatrixProcess::Pointer, Process>(m, "ConvertMatrixProcess")
       .def(py::init<>())
       .def("ConvertMatrix", &ConvertMatrixProcess::ConvertMatrix)
       ;
}

} // namespace Python.

} // Namespace Kratos
