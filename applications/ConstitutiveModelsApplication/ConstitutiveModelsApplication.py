""" Project: ConstitutiveModelsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Kratos Imports
from KratosMultiphysics import _ImportApplication
from KratosConstitutiveModelsApplication import *
application = KratosConstitutiveModelsApplication()
application_name = "KratosConstitutiveModelsApplication"

_ImportApplication(application, application_name)