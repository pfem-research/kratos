""" Project: ConstitutiveModelsApplication
    Developer: LMonforte
    Maintainer: LM
"""


# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.ConstitutiveModelsApplication as KratosModels


def Factory(settings, Model):
    if(not isinstance(settings, KratosMultiphysics.Parameters)):
        raise Exception(
            "Expected input shall be a Parameters object, encapsulating a json string")
    return AssignDepthMaterialPropertiesProcess(Model, settings["Parameters"])


class AssignDepthMaterialPropertiesProcess(KratosMultiphysics.Process):
    #
    def __init__(self, Model, custom_settings):

        KratosMultiphysics.Process.__init__(self)

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
            "echo_level"            : 1,
            "model_part_name"       : "Main_Domain",
            "initial_shear_strength": 0.0,
            "rigidity_index"        : 0.0,
            "initial_depth"         : 0.0,
            "gradient"              : 0.0
        }
        """)

        self.settings = custom_settings
        self.settings.ValidateAndAssignDefaults(default_settings)

        self.model_part = Model
        self.model_part_name = self.settings["model_part_name"].GetString()

        self.echo_level = self.settings["echo_level"].GetInt()

        self.model = Model

        self.OnlyOnce = False;


        

    def ExecuteInitialize(self):

        self.main_model_part = self.model[self.model_part_name]

        self.non_local_cxx_process = KratosModels.DepthVaryingYieldStressProcess(
            self.main_model_part, self.settings)


    def ExecuteInitializeSolutionStep(self):

        if ( self.main_model_part.ProcessInfo[KratosMultiphysics.TIME] > 0.2):
            self.OnlyOnce = True

        print(  self.main_model_part.ProcessInfo[KratosMultiphysics.TIME])
        if (self.OnlyOnce == False):
            self.non_local_cxx_process.Execute()
            self.OnlyOnce = True


