""" Project: ConstitutiveModelsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""


# Built-in/Generic Imports
import importlib

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.ConstitutiveModelsApplication as KratosMaterials


def Factory(custom_settings, Model):
    if(type(custom_settings) != KratosMultiphysics.Parameters):
        raise Exception(
            "expected input shall be a Parameters object, encapsulating a json string")
    return AssignMaterialsProcess(Model, custom_settings["Parameters"])


class AssignMaterialsProcess(KratosMultiphysics.Process):
    def __init__(self, Model, custom_settings):

        KratosMultiphysics.Process.__init__(self)

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
            "model_part_name" : "no_name_supplied",
            "properties_id"   : 1,
                "material_name"   : "steel",
                    "constitutive_law": {
                        "name"   : "KratosMultiphysics.ConstitutiveModelsApplication.LargeStrain3DLaw.LinearElasticModel"
                    },
            "variables": {},
            "tables": {},
                "echo_level" : 0
        }
        """)

        # overwrite the default settings with user-provided parameters
        self.settings = custom_settings
        self.settings.ValidateAndAssignDefaults(default_settings)

        # get model part
        self.model_part = None
        if self.settings["model_part_name"].GetString() != "no_name_supplied":
            self.model_part = Model[self.settings["model_part_name"].GetString()]
            self.main_model_part = self.model_part.GetRootModelPart()
        else:
            self.main_model_part = Model[Model.GetModelPartNames()[0]].GetRootModelPart()

        self.echo_level = self.settings["echo_level"].GetInt()
        self.material_name = self.settings["material_name"].GetString()

        # material properties
        self.properties = self.main_model_part.Properties[self.settings["properties_id"].GetInt()]

        # read variables
        self._ReadVariables()

        # read tables
        self._ReadTables()

        # create constitutive law
        self.module_name, self.material_law = self._GetLawFromModule(
            self.settings["constitutive_law"]["name"].GetString())

        if self.module_name != None and "KratosMaterials" in self.module_name:
            print(" check law properties ")
            self.material_law.CheckRequiredProperties(self.properties)

        self.properties.SetValue(
            KratosMultiphysics.CONSTITUTIVE_LAW, self.material_law.Clone())

        if self.model_part != None:
            self.model_part.AddProperties(self.properties)
        else:
            model_part_names = Model.GetModelPartNames()
            for model_part_name in model_part_names:
                Model[model_part_name].AddProperties(self.properties)

        if self.echo_level > 0:
            for properties in self.main_model_part.Properties:
                print(" "+str(properties.Id)+":",properties)

        splitted_law_name = (self.settings["constitutive_law"]["name"].GetString()).split(".")

        print(self._class_prefix(), self.material_name +
              " [Model: "+splitted_law_name[len(splitted_law_name)-1]+"]")

    #
    def ExecuteInitialize(self):
        pass

    #
    def Execute(self):
        #self._CheckConstitutiveLawDimension()
        self._AssignMaterialProperties()

    #
    def ExecuteFinalize(self):
        pass

    #
    def _GetVariableName(self, name):
        legacy = {'HEAT_CAPACITY': 'SPECIFIC_HEAT_CAPACITY', 'HEAT_CONDUCTIVITY':
                  'THERMAL_CONDUCTIVITY', 'STABILIZATION_FACTOR': 'STABILIZATION_FACTOR_P',
                  'PLASTIC_STRAIN_RATE': 'REFERENCE_PLASTIC_STRAIN_RATE'}
        for key, value in legacy.items():
             if key == name:
                 print(" WARNING : Legacy Material Variable",key," must be replaced by", value)
                 return value
        return name
    #
    def _ReadVariables(self):
        variables = self.settings["variables"]
        name = self.settings["constitutive_law"]["name"].GetString()
        if not "Fluid" in name:
            self._CheckHyperElasticVariables(variables)
        for key, value in variables.items():
            name = self._GetVariableName(key)
            variable = KratosMultiphysics.KratosGlobals.GetVariable(name)
            if(value.IsDouble()):
                self.properties.SetValue(variable, value.GetDouble())
            elif(value.IsInt()):
                self.properties.SetValue(variable, value.GetInt())
            elif(value.IsArray()):
                vector_value = KratosMultiphysics.Vector(value.size())
                for i in range(0, value.size()):
                    vector_value[i] = value[i].GetDouble()
                self.properties.SetValue(variable, vector_value)

    #
    def _ReadTables(self):
        tables = self.settings["tables"]
        properties_layout = KratosMaterials.PropertiesLayout()
        number_of_tables = 0
        for key, table in tables.items():
            number_of_tables += 1
            print(" table", key)
            if table.Has("table_file_name"):
                import os
                problem_path = os.getcwd()
                table_path = os.path.join(
                    problem_path, table["table_file_name"].GetString())
                import csv
                with open(table_path, 'r') as table_file:
                    reader = csv.DictReader(table_file)
                    new_table = KratosMultiphysics.PiecewiseLinearTable()
                    input_variable_name = reader.fieldnames[0]
                    output_variable_name = reader.fieldnames[1]
                    input_variable = KratosMultiphysics.KratosGlobals.GetVariable(input_variable_name)
                    output_variable = KratosMultiphysics.KratosGlobals.GetVariable(output_variable_name)
                    '''
                    input_variable = self._GetItemFromModule(
                        "KratosMultiphysics."+str(input_variable_name))
                    output_variable = self._GetItemFromModule(
                        "KratosMultiphysics."+str(output_variable_name))
                    '''
                    for line in reader:
                        new_table.AddRow(float(line[input_variable_name]), float(
                            line[output_variable_name]))
                    self.properties.SetTable(
                        input_variable, output_variable, new_table)
                    properties_layout.RegisterTable(
                        input_variable, output_variable)
            else:
                input_variable = KratosMultiphysics.KratosGlobals.GetVariable(table["input_variable"].GetString())
                output_variable = KratosMultiphysics.KratosGlobals.GetVariable(table["output_variable"].GetString())
                '''
                input_variable = self._GetItemFromModule(
                    table["input_variable"].GetString())
                output_variable = self._GetItemFromModule(
                    table["output_variable"].GetString())
                '''
                new_table = KratosMultiphysics.PiecewiseLinearTable()
                for i in range(0, table["data"].size()):
                    new_table.AddRow(
                        table["data"][i][0].GetDouble(), table["data"][i][1].GetDouble())
                self.properties.SetTable(
                    input_variable, output_variable, new_table)
                properties_layout.RegisterTable(
                    input_variable, output_variable)

        # set properties layout and table keys and arguments
        if number_of_tables > 0:
            properties_layout.AddToProperties(
                KratosMaterials.PROPERTIES_LAYOUT, properties_layout, self.properties)

    #
    def _AssignMaterialProperties(self):
        if self.model_part != None:
            # Assign properties to the model_part elements
            for Element in self.model_part.Elements:
                Element.Properties = self.properties

            # Assign properties to the model_part conditions
            for Condition in self.model_part.Conditions:
                Condition.Properties = self.properties

    #
    def _CheckConstitutiveLawDimension(self):
        self.dimension = self.main_model_part.ProcessInfo[KratosMultiphysics.SPACE_DIMENSION]

        if(self.material_law.WorkingSpaceDimension() != self.dimension):
            # feature flags
            self.features = KratosMultiphysics.ConstitutiveLawFeatures()
            self.material_law.GetLawFeatures(self.features)
            self.feature_options = self.features.GetOptions()
            if(self.feature_options.IsNot(KratosMultiphysics.ConstitutiveLaw.PLANE_STRESS_LAW)):
                raise Exception(self._class_prefix(
                ), "mismatch between the ConstitutiveLaw dimension and the dimension of the space")

    #
    def _GetLawFromModule(self, my_string):
        module_name = None
        splitted = my_string.split(".")
        if(len(splitted) == 0):
            raise Exception("something wrong. Trying to split the string "+my_string)
        #name:'Law' (with no Model) supplied (Law module is the ConstitutiveModelsApplication)
        if(len(splitted) == 1):
            material_law = "KratosMaterials."+my_string+"()"
            return module_name,eval(material_law)
        #name:'Law.Model' supplied
        elif(len(splitted) == 2):
            law_module_name = "KratosMaterials"
            model_module_name = law_module_name
            if "Umat" in splitted[1]:
                model_module_name = "KratosMultiphysics.UmatApplication"
                module = importlib.import_module(model_module_name)
            material_law = law_module_name + "." +splitted[0]+"("+model_module_name+ "."+splitted[1]+"())"
            return law_module_name,eval(material_law)
        #name:'ConstitutiveModelsApplication.Law.Model' or name:'KratosMultiphysics.Module.Model' supplied
        elif(len(splitted) == 3):
            law_module_name = ""
            for i in range(len(splitted)-1):
                law_module_name += splitted[i]
                if i != len(splitted)-2:
                    law_module_name += "."
            #name:'ConstitutiveModelsApplication.Law.Model'
            if(splitted[0] != "KratosMultiphysics"):
                law_module_name="KratosMultiphysics."+splitted[0]
                module = importlib.import_module(law_module_name)
                model_module_name = law_module_name
                if "Umat" in splitted[-1]:
                    model_module_name = "KratosMultiphysics.UmatApplication"
                    module = importlib.import_module(model_module_name)
                material_law = law_module_name+"."+splitted[-2]+"("+model_module_name+"."+splitted[-1]+"())"
            #name:'KratosMultiphysics.Module.Model' (Law module is a standard Kratos Application CLW, not the ConsitutiveModelsApplication)
            else:
                module = importlib.import_module(law_module_name)
                material_law = law_module_name+"."+splitted[-1]+"()"
                print(" Material law: ", material_law)
            return law_module_name,eval(material_law)
        #name:'KratosMultiphysics.ConstitutiveModelsApplication.Law.Model'
        elif(len(splitted) == 4):
            law_module_name = ""
            for i in range(len(splitted)-2):
                law_module_name += splitted[i]
                if i != len(splitted)-3:
                    law_module_name += "."
            module = importlib.import_module(law_module_name)
            model_module_name = law_module_name
            if "Umat" in splitted[-1]:
                model_module_name = "KratosMultiphysics.UmatApplication"
                module = importlib.import_module(model_module_name)
            material_law = law_module_name+"." + \
                splitted[-2]+"("+model_module_name+"."+splitted[-1]+"())"
            return law_module_name,eval(material_law)
        else:
            raise Exception("something wrong .. Trying to split the string "+my_string)
    #
    def _GetItemFromModule(self, my_string):
        splitted = my_string.split(".")
        if(len(splitted) == 0):
            raise Exception(
                "something wrong. Trying to split the string "+my_string)
        if(len(splitted) == 1):
            return eval(my_string)
        else:
            module_name = ""
            for i in range(len(splitted)-1):
                module_name += splitted[i]
                if i != len(splitted)-2:
                    module_name += "."

            module = importlib.import_module(module_name)
            try:
                return getattr(module, str(splitted[-1]))
            except:
                raise

    def _CheckHyperElasticVariables(self, variables):
        C10_value = 0.0
        E_value = 0.0
        K_value = 0.0
        nu_value = -1.0
        for key, value in variables.items():
            if key == "C10":
                C10_value = value.GetDouble()
            if key == "YOUNG_MODULUS":
                E_value = value.GetDouble()
            if key == "BULK_MODULUS":
                K_value = value.GetDouble()
            if key == "POISSON_RATIO":
                nu_value = value.GetDouble()

        if C10_value != 0.0:
            if K_value != 0.0:
                print(self._class_prefix(),
                      "[ Elastic variables E,nu computed from C10,K ]")
                E_value = 9.0*K_value*2.0*C10_value/(3.0*K_value+2.0*C10_value)
                nu_value = (3.0*K_value-4.0*C10_value) / \
                    (2*(3.0*K_value+2.0*C10_value))
                print(self._class_prefix(),
                      "[ YOUNG_MODULUS:", E_value, "] added")
                print(self._class_prefix(),
                      "[ POISSON_RATIO:", nu_value, "] added")
            else:
                if E_value == 0 and nu_value >= 0:
                    K_value = 4.0*C10_value * \
                        (1.0+nu_value)/(3*(1.0-2.0*nu_value))
                    E_value = 4.0*C10_value*(1.0+nu_value)
                    print(self._class_prefix(),
                          "[ BULK_MODULUS:", K_value, "] added")
                    print(self._class_prefix(),
                          "[ YOUNG_MODULUS:", E_value, "] added")
                elif nu_value < 0 and E_value != 0:
                    K_value = 2.0*C10_value*E_value / \
                        (3.0*(6.0*C10_value-E_value))
                    nu_value = E_value/(4.0*C10_value)-1.0
                    print(self._class_prefix(),
                          "[ BULK_MODULUS:", K_value, "] added")
                    print(self._class_prefix(),
                          "[ POISSON_RATIO:", nu_value, "] added")
                elif nu_value >= 0 and E_value != 0:
                    print(" YOUNG_MODULUS, POISSON_RATIO and C10 supplied ")
                else:
                    print(" YOUNG_MODULUS or POISSON_RATIO needed but not supplied ")

        else:
            if K_value == 0:
                if E_value != 0 and nu_value >= 0:
                    print(self._class_prefix(),
                          "[ Elastic variables C10,K computed from E,nu ]")
                    C10_value = 0.25*E_value/(1.0+nu_value)
                    K_value = E_value/(3.0*(1.0-2.0*nu_value))
                    print(self._class_prefix(), "[ C10:", C10_value, "] added")
                    print(self._class_prefix(),
                          "[ BULK_MODULUS:", K_value, "] added")
                else:
                    print(" YOUNG_MODULUS and POISSON_RATIO needed but not supplied ")
            else:
                if E_value == 0 and nu_value >= 0:
                    C10_value = 0.25*3*K_value * \
                        (1.0-2.0*nu_value)/(1.0+nu_value)
                    E_value = 3.0*K_value*(1.0-2.0*nu_value)
                    print(self._class_prefix(), "[ C10:", C10_value, "] added")
                    print(self._class_prefix(),
                          "[ YOUNG_MODULUS:", E_value, "] added")
                elif nu_value < 0 and E_value != 0:
                    C10_value = 0.5*3*K_value*E_value/(9.0*K_value-E_value)
                    nu_value = 3.0*(K_value-E_value)/(6.0*K_value)
                    print(self._class_prefix(), "[ C10:", C10_value, "] added")
                    print(self._class_prefix(),
                          "[ POISSON_RATIO:", nu_value, "] added")
                else:
                    print(" YOUNG_MODULUS or POISSON_RATIO needed but not supplied ")

        self._add_double_value(variables, "YOUNG_MODULUS", E_value)
        self._add_double_value(variables, "POISSON_RATIO", nu_value)
        self._add_double_value(variables, "C10", C10_value)
        self._add_double_value(variables, "BULK_MODULUS", K_value)

    #
    def _add_double_value(self, variables, name, value):
        if not variables.Has(str(name)):
            variables.AddEmptyValue(str(name))
        variables[str(name)].SetDouble(value)

    #
    @classmethod
    def _class_prefix(self):
        header = "::[--Material_Models--]::"
        return header
