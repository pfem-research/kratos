//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                   July 2018 $
//
//

#include "includes/model_part.h"

/* System includes */

/* External includes */

/* Project includes */
#include "custom_processes/depth_varying_yield_stress_process.hpp"
#include "utilities/openmp_utils.h"

namespace Kratos
{

//*********************************************************************************************
// constructor
DepthVaryingYieldStressProcess::DepthVaryingYieldStressProcess(ModelPart &rModelPart, Parameters rParameters)
    : Process(Flags()), mrModelPart(rModelPart)
{
   KRATOS_TRY

   Parameters default_parameters(R"(
      {
         "echo_level": 1,
         "model_part_name": "Main_Domain",
         "rigidity_index": 100.0,
         "initial_shear_strength": 0.0,
         "initial_depth": 0.0,
         "gradient": 0.0
      } )");

   rParameters.ValidateAndAssignDefaults(default_parameters);
   mRigidityIndex = rParameters["rigidity_index"].GetDouble();
   mSu0 = rParameters["initial_shear_strength"].GetDouble();
   mY0 = rParameters["initial_depth"].GetDouble();
   mGradient = rParameters["gradient"].GetDouble();


   std::cout << " ASSIGN Su :: " << mSu0 << " . " << mY0 << " . " << mGradient << std::endl;
   std::cout << " ASSIGN Su :: Rigidity Index " << mRigidityIndex << " . " << mY0 << " . " << mGradient << std::endl;

   KRATOS_CATCH("")
}

//*********************************************************************************************
// destructor
DepthVaryingYieldStressProcess::~DepthVaryingYieldStressProcess()
{
}

//*********************************************************************************************
// Execute
void DepthVaryingYieldStressProcess::Execute()
{
   KRATOS_TRY

   const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();


   GeometryData::IntegrationMethod MyIntegrationMethod;
   array_1d<double, 3> AuxLocalCoordinates;
   array_1d<double, 3> AuxGlobalCoordinates;
   double YieldStress;

   for (ModelPart::ElementsContainerType::iterator ie = mrModelPart.ElementsBegin(); ie != mrModelPart.ElementsEnd(); ie++)
   {

      if (ie->IsNot(SOLID))
         continue;

      Element::GeometryType &rGeom = ie->GetGeometry();
      const unsigned int nNodes = rGeom.size();
      Matrix DeltaPosition(nNodes,3);
      noalias(DeltaPosition) = ZeroMatrix(nNodes,3);

      for (unsigned int ii = 0; ii < nNodes; ii++) {
         const array_1d< double, 3> &  NodeDelta = rGeom[ii].FastGetSolutionStepValue(DISPLACEMENT);
         for (unsigned int jj = 0; jj < 3; jj++) {
            DeltaPosition(jj,ii) = - NodeDelta(ii);
         }
      }

      MyIntegrationMethod = ie->GetIntegrationMethod();
      const Element::GeometryType::IntegrationPointsArrayType &IntegrationPoints = rGeom.IntegrationPoints(MyIntegrationMethod);
      unsigned int numberOfGP = IntegrationPoints.size();

      std::vector<ConstitutiveLaw::Pointer> ConstitutiveLawVector(numberOfGP);
      ie->CalculateOnIntegrationPoints(CONSTITUTIVE_LAW, ConstitutiveLawVector, rCurrentProcessInfo);


      for (unsigned int nGP = 0; nGP < numberOfGP; nGP++)
      {
         for (unsigned int i = 0; i < 3; i++)
            AuxLocalCoordinates[i] = IntegrationPoints[nGP][i];

         rGeom.GlobalCoordinates(AuxGlobalCoordinates, AuxLocalCoordinates, DeltaPosition);
         double DeltaY = mY0-AuxGlobalCoordinates(1);
         YieldStress = mSu0 + mGradient * DeltaY;
         if ( YieldStress < 1E-5)
            YieldStress = 1E9;
         ConstitutiveLawVector[nGP]->SetValue( YIELD_STRESS, YieldStress, rCurrentProcessInfo);
         ConstitutiveLawVector[nGP]->SetValue( SHEAR_MODULUS, mRigidityIndex*YieldStress, rCurrentProcessInfo);
         

      }
   }


   KRATOS_CATCH("")
}



} // end namespace Kratos
