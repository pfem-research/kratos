//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                   July 2018 $
//
//

#if !defined(KRATOS_DEPTH_VARYING_YIELD_STRESS_PROCESS_HPP_INCLUDED)
#define KRATOS_DEPTH_VARYING_YIELD_STRESS_PROCESS_HPP_INCLUDED

/* System includes */

/* External includes */

/* Project includes */
#include "processes/process.h"
#include "includes/model_part.h"
#include "includes/kratos_flags.h"
#include "utilities/math_utils.h"
#include "constitutive_models_application_variables.h"

#include "includes/kratos_parameters.h"

namespace Kratos
{

class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) DepthVaryingYieldStressProcess
    : public Process
{

protected:

public:
   /**@name Type Definitions */
   /*@{ */

   // Pointer definition of Process
   KRATOS_CLASS_POINTER_DEFINITION(DepthVaryingYieldStressProcess);

   typedef ModelPart::NodesContainerType NodesArrayType;
   typedef ModelPart::ConditionsContainerType ConditionsContainerType;
   typedef ModelPart::MeshType MeshType;
   /*@} */
   /**@name Life Cycle
             */
   /*@{ */

   // Constructor.

   DepthVaryingYieldStressProcess(ModelPart &rModelPart, Parameters rParameters);

   /** Destructor.
             */

   virtual ~DepthVaryingYieldStressProcess();

   /*@} */
   /**@name Operators
             */
   /*@{ */

   /*@} */
   /**@name Operations */
   /*@{ */

   void operator()()
   {
      Execute();
   }

   void Execute() override;

protected:

protected:
   // member variables

   ModelPart &mrModelPart;

   double mRigidityIndex;
   double mSu0;
   double mY0;
   double mGradient;

}; //end class DepthVaryingYieldStressProcess

} // END namespace Kratos

#endif //KRATOS_DEPTH_VARYING_YIELD_STRESS_PROCESS_HPP_INCLUDED
