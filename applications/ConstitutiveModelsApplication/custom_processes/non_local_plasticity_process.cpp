//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                   July 2018 $
//
//

#include "includes/model_part.h"

/* System includes */

/* External includes */

/* Project includes */
#include "custom_processes/non_local_plasticity_process.hpp"


namespace Kratos
{

//*********************************************************************************************
// constructor
NonLocalPlasticityProcess::NonLocalPlasticityProcess(ModelPart &rModelPart, Parameters rParameters)
    : Process(Flags()), mrModelPart(rModelPart)
{
   KRATOS_TRY

   Parameters default_parameters(R"(
      {
         "echo_level": 1,
         "model_part_name": "Main_Domain",
         "characteristic_length": 0.0,
         "local_variables": [],
         "alpha_factor": 1.0,
         "non_local_variables": []
      } )");

   rParameters.ValidateAndAssignDefaults(default_parameters);
   mCharacteristicLength = rParameters["characteristic_length"].GetDouble();
   if (mCharacteristicLength == 0.0)
   {
      std::cout << " NonLocalPlasticityProcess:: " << std::endl;
      std::cout << "       using the default value for the characteristic length: DANGEROUS! " << std::endl;
   }

   mAlphaFactor = rParameters["alpha_factor"].GetDouble();
   if (rParameters["non_local_variables"].size() != rParameters["local_variables"].size())
   {
      KRATOS_ERROR << " NonLocalPlasticityProcess :: the number of local and nonlocal variables is not correct " << std::endl;
   }

   for (unsigned int ii = 0; ii < rParameters["non_local_variables"].size(); ii++)
   {
      const std::string &rLocalVariable = rParameters["local_variables"][ii].GetString();
      const std::string &rNonLocalVariable = rParameters["non_local_variables"][ii].GetString();

      const Variable<double> &rThisLocalVariable = KratosComponents<Variable<double>>::Get(rLocalVariable);
      const Variable<double> &rThisNonLocalVariable = KratosComponents<Variable<double>>::Get(rNonLocalVariable);

      mLocalVariables.push_back(&rThisLocalVariable);
      mNonLocalVariables.push_back(&rThisNonLocalVariable);
   }

   if (mLocalVariables.size() != mNonLocalVariables.size())
   {
      KRATOS_ERROR << " NonLocalPlasticityProcess :: the number of local and nonlocal variables is not correct " << std::endl;
   }

   KRATOS_CATCH("")
}

//*********************************************************************************************
// destructor
NonLocalPlasticityProcess::~NonLocalPlasticityProcess()
{
}

//*********************************************************************************************
// Execute
void NonLocalPlasticityProcess::Execute()
{
   KRATOS_TRY

   if (mLocalVariables.size() == 0)
   {
      std::cout << " nonLocalModel:: Nothing to transfer. No variables defined" << std::endl;
      return;
   }

   std::vector<GaussPoint> GaussPointsVector;

   CreateSimplifiedStructure(GaussPointsVector);

   std::vector<std::vector<double>> AllLocalVariables;
   const unsigned int numberGaussPoints = GaussPointsVector.size();

   for (unsigned int variable = 0; variable < this->mLocalVariables.size(); variable++)
   {

      const Variable<double> &rLocalVariable = *mLocalVariables[variable];

      std::vector<double> ThisLocalVariable(numberGaussPoints);

      for (unsigned int gp = 0; gp < numberGaussPoints; gp++)
      {
         ThisLocalVariable[gp] = GaussPointsVector[gp].pConstitutiveLaw->GetValue(rLocalVariable, ThisLocalVariable[gp]);
      }
      AllLocalVariables.push_back(ThisLocalVariable);
   }

   //std::cout << "        SearchTime " << start.ElapsedSeconds() - start2.ElapsedSeconds() << " smooothing " << start2.ElapsedSeconds() << " totalTime " << start.ElapsedSeconds() << std::endl;

   GaussPointSearchAndTransfer(GaussPointsVector, AllLocalVariables);


   //std::cout << "        CreateStructure " << start2 - start << " copyLocalVariables " << start3 - start2 << " Search&Smooth (without saving data) " << start4 - start3 << " totalTime " << start4 - start << std::endl;

   KRATOS_CATCH("")
}

//*********************************************************************************************
// create  a list of GP
void NonLocalPlasticityProcess::CreateSimplifiedStructure(std::vector<GaussPoint> &rGPVector)
{
   KRATOS_TRY

   const ProcessInfo &rCurrentProcessInfo = this->mrModelPart.GetProcessInfo();

   GeometryData::IntegrationMethod MyIntegrationMethod;
   array_1d<double, 3> AuxLocalCoordinates;
   array_1d<double, 3> AuxGlobalCoordinates;

   for (ModelPart::ElementsContainerType::iterator ie = this->mrModelPart.ElementsBegin(); ie != this->mrModelPart.ElementsEnd(); ie++)
   {

      if (ie->IsNot(SOLID))
         continue;

      Element::GeometryType &rGeom = ie->GetGeometry();
      MyIntegrationMethod = ie->GetIntegrationMethod();
      const Element::GeometryType::IntegrationPointsArrayType &IntegrationPoints = rGeom.IntegrationPoints(MyIntegrationMethod);
      unsigned int numberOfGP = IntegrationPoints.size();
      unsigned int PropertiesId = ie->GetProperties().Id();

      std::vector<ConstitutiveLaw::Pointer> ConstitutiveLawVector(numberOfGP);
      ie->CalculateOnIntegrationPoints(CONSTITUTIVE_LAW, ConstitutiveLawVector, rCurrentProcessInfo);

      for (unsigned int nGP = 0; nGP < numberOfGP; nGP++)
      {

         for (unsigned int i = 0; i < 3; i++)
            AuxLocalCoordinates[i] = IntegrationPoints[nGP][i];

         rGeom.GlobalCoordinates(AuxGlobalCoordinates, AuxLocalCoordinates);

         rGPVector.push_back(GaussPoint(ConstitutiveLawVector[nGP], AuxGlobalCoordinates, PropertiesId ));
      }
   }

   KRATOS_CATCH("")
}

//*********************************************************************************************
// perform GP search and transfer, without saving lots of data
void NonLocalPlasticityProcess::GaussPointSearchAndTransfer(std::vector<GaussPoint> &rGPVector, const std::vector<std::vector<double>> &rAllLocalVariables)
{
   KRATOS_TRY

   const ProcessInfo &rCurrentProcessInfo = this->mrModelPart.GetProcessInfo();
   const double CharacteristicLength = 3.0 * (this->mCharacteristicLength);

   double numerator, denominator;

   const unsigned int NumberGP = rGPVector.size();
   for (unsigned int ii = 0; ii < NumberGP; ii++)
   {

      std::vector<int> NeighbourNodes;
      NeighbourNodes.reserve(NumberGP);
      std::vector<double> NeighbourDistances;
      NeighbourDistances.reserve(NumberGP);

      const array_1d<double, 3> &rCoordII = rGPVector[ii].Coordinates;
      unsigned int PropIdII = rGPVector[ii].PropertiesId;

      // search
      for (unsigned int jj = 0; jj < NumberGP; jj++)
      {
         unsigned int PropIdJJ = rGPVector[jj].PropertiesId;

         if ( PropIdII != PropIdJJ)
            continue;

         const array_1d<double, 3> &rCoordJJ = rGPVector[jj].Coordinates;
         double distance = 0;
         for (unsigned int i = 0; i < 3; i++)
            distance += pow(rCoordII[i] - rCoordJJ[i], 2);
         distance = pow(distance, 0.5);

         if (distance < CharacteristicLength)
         {
            NeighbourNodes.push_back(jj);
            NeighbourDistances.push_back(distance);
         }
      }

      // transfer
      if (NeighbourDistances.size() < 2)
      {

         for (unsigned int variable = 0; variable < this->mNonLocalVariables.size(); variable++)
         {
            const Variable<double> &rNonLocalVariable = *mNonLocalVariables[variable];
            rGPVector[ii].pConstitutiveLaw->SetValue(rNonLocalVariable, rAllLocalVariables[variable][ii], rCurrentProcessInfo);
         }
      }
      else
      {
         const unsigned int ContributionNumber = NeighbourNodes.size();
         std::vector<double> weights(ContributionNumber);

         for (unsigned int gp = 0; gp < ContributionNumber; gp++)
         {
            weights[gp] = this->ComputeWeightFunction(NeighbourDistances[gp], this->mCharacteristicLength, weights[gp]);
         }

         for (unsigned int variable = 0; variable < this->mNonLocalVariables.size(); variable++)
         {
            numerator = 0;
            denominator = 0;
            for (unsigned int gp = 0; gp < ContributionNumber; gp++)
            {
               numerator += weights[gp] * rAllLocalVariables[variable][NeighbourNodes[gp]];
               denominator += weights[gp];
            }
            numerator /= denominator;

            numerator = mAlphaFactor*numerator + (1.0-mAlphaFactor)*rAllLocalVariables[variable][ii];
            const Variable<double> &rNonLocalVariable = *mNonLocalVariables[variable];
            rGPVector[ii].pConstitutiveLaw->SetValue(rNonLocalVariable, numerator, rCurrentProcessInfo);
         }
      }
   }

   KRATOS_CATCH("")
}

//*********************************************************************************************
// weighting function
double &NonLocalPlasticityProcess::ComputeWeightFunction(const double &rDistance, const double &rCharacteristicLength, double &rAlpha)
{
   KRATOS_TRY

   // Galavi & Schweiger
   rAlpha = rDistance * exp(-pow(rDistance / rCharacteristicLength, 2));

   // Gaussian function
   //rAlpha =  exp( - pow(rDistance/rCharacteristicLength, 2) );
   return rAlpha;

   KRATOS_CATCH("")
}

} // end namespace Kratos
