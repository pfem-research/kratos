//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:         JMCarbonell $
//   Maintained by:       $Maintainer:                    $
//   Date:                $Date:                 May 2020 $
//
//

#if !defined(KRATOS_CONVERT_MATRIX_PROCESS_HPP_INCLUDED)
#define KRATOS_CONVERT_MATRIX_PROCESS_HPP_INCLUDED


// System includes

// External includes

// Project includes
#include "processes/process.h"
#include "includes/model_part.h"
#include "includes/kratos_flags.h"
#include "utilities/math_utils.h"
#include "constitutive_models_application_variables.h"

#include "includes/kratos_parameters.h"

namespace Kratos
{

//class KRATOS_API(CONSTITUTIVE_MODELS_APPLICATION) ConvertMatrixProcess
class ConvertMatrixProcess
   : public Process
   {
      public : 

         KRATOS_CLASS_POINTER_DEFINITION(ConvertMatrixProcess);

         ConvertMatrixProcess() 
            : Process() 
         { 
         }



         ~ConvertMatrixProcess() override 
         {
         }

         void ConvertMatrix( const Matrix & rA, CompressedMatrix & rB)
         {
            KRATOS_TRY

            for (unsigned int i = 0; i < rA.size1(); i++) {
               for (unsigned int j = 0; j < rA.size2(); j++) {
                  rB(i,j) = rA(i,j);
               }
            }
            KRATOS_CATCH("")
         }
   
         void operator()()
         {
            Execute();
         }

         void Execute() override
         {
         }

   };

}

#endif // KRATOS_CONVERT_MATRIX_PROCESS_HPP_INCLUDED

