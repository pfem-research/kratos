import KratosMultiphysics

import KratosMultiphysics.ConstitutiveModelsApplication as KratosMaterialModels
import KratosMultiphysics.KratosUnittest as KratosUnittest


class TestModifiedCamClayModel(KratosUnittest.TestCase):

    def __init__(self, *args, **kwargs):
        try:
            import importlib
            import math as math
            import KratosMultiphysics.LinearSolversApplication
            import KratosMultiphysics.SolidMechanicsApplication
        except ImportError as e:
            self.skipTest("Missing python libraries ( importlib and math)")

        super(KratosUnittest.TestCase, self).__init__(*args, **kwargs)
        
        settings = KratosMultiphysics.Parameters("""{"solver_type": "sparse_lu"}""")
        from KratosMultiphysics import python_linear_solver_factory as linear_solver_factory
        self.linear_solver = linear_solver_factory.ConstructSolver(settings)

    def test_UndrainedStressPath(self):
        
        NumberIncrements = 2500

        [Fincr, Fpresc, Sigmaincr, Sigmapresc, A, B] = self._get_problem_matrices_and_vectors()

        IncrF = 0.2
        Fincr[1] = IncrF
        Fincr[0] = Fincr[1]**(-0.5)
        Fincr[2] = Fincr[0]

        for i in range(3,6):
            A[i,i] = 1.0
        for i in range(0,3):
            B[i,i] = 1.0

        for pressure in range(40, 101, 10):
            self._create_material_model_and_law()

            self.parameters.SetMaterialProperties( self.properties )
            
            self._setInitialStressState( -float(pressure))

            Fpresc[0] = self.F[0,0]
            Fpresc[1] = self.F[1,1]
            Fpresc[2] = self.F[2,2]

            self._ComputeThisProblem( Fincr, Fpresc, Sigmaincr, Sigmapresc,  A, B, NumberIncrements)



            # Compute Analytical solution
            pc0 = self.properties.GetValue(
                KratosMultiphysics.PRE_CONSOLIDATION_STRESS)
            OCR = pc0/float(pressure)
            kappa = self.properties.GetValue(KratosMultiphysics.SWELLING_SLOPE)
            landa = self.properties.GetValue(
                KratosMultiphysics.NORMAL_COMPRESSION_SLOPE)
            M = self.properties.GetValue(KratosMultiphysics.CRITICAL_STATE_LINE)
            alphaS = self.properties.GetValue(KratosMultiphysics.ALPHA_SHEAR)
            if (abs(alphaS) > 1e-12):
                self.skipTest(
                    "The constitutive problem no longer has analytical solution")

            p0 = pc0 / OCR
            BigLambda = (landa - kappa) / landa
            pressureFailure = p0 * ((OCR / 2.0) ** BigLambda)
            UndrainedShearStrenght = 0.5*p0*M * ((OCR/2.0)**BigLambda)

            stress = self.parameters.GetStressVector()
            Pressure = stress[0]+stress[1]+stress[2]
            Pressure = -Pressure/3.0
            DeviatoricQ = -stress[1]+stress[0]

            self.assertAlmostEqual(Pressure, pressureFailure, 2)
            self.assertAlmostEqual(0.5*DeviatoricQ, UndrainedShearStrenght, 2)

    def test_IsotropicLoading(self):

        import math
        NumberIncrements = 100

        [Fincr, Fpresc, Sigmaincr, Sigmapresc, A, B] = self._get_problem_matrices_and_vectors()

        IncrF = 0.99
        Fincr[0] = IncrF
        Fincr[1] = IncrF
        Fincr[2] = IncrF

        B.fill_identity()

        self._create_material_model_and_law()

        self.parameters.SetMaterialProperties( self.properties )

        pc0 = self.properties.GetValue(
             KratosMultiphysics.PRE_CONSOLIDATION_STRESS)
        self._setInitialStressState( -pc0)

        Fpresc[0] = self.F[0,0]
        Fpresc[1] = self.F[1,1]
        Fpresc[2] = self.F[2,2]

        self._ComputeThisProblem( Fincr, Fpresc, Sigmaincr, Sigmapresc,  A, B, NumberIncrements)


        pc0 = self.properties.GetValue(
            KratosMultiphysics.PRE_CONSOLIDATION_STRESS)
        kappa = self.properties.GetValue(KratosMultiphysics.SWELLING_SLOPE)
        landa = self.properties.GetValue(
            KratosMultiphysics.NORMAL_COMPRESSION_SLOPE)
        M = self.properties.GetValue(KratosMultiphysics.CRITICAL_STATE_LINE)

        pc = -pc0 * math.exp( -math.log(IncrF**6)/2.0/landa)

        stress = self.parameters.GetStressVector()
        for i in range(0,3):
            self.assertAlmostEqual(stress[i], pc)
        for i in range(3,6):
            self.assertAlmostEqual(stress[i], 0.0)

    def _ComputeThisProblem(self, Fincr, Finitial, Sigmaincr, SigmaInitial, A, B, nSteps):
        
        XX = KratosMultiphysics.Vector(6)
        XX.fill(0.0)
        XX[0] = self.F[0,0]-1.0;
        XX[1] = self.F[1,1]-1.0;
        XX[2] = self.F[2,2]-1.0;

        dx = KratosMultiphysics.Vector(6)
        dx.fill(0.0)

        stress = self.parameters.GetStressVector()
        F_presc = KratosMultiphysics.Vector(6)


        for t in range(1, nSteps+1):

            sigma_presc = SigmaInitial + float(t)/float(nSteps)*Sigmaincr
            for i in range(0,6):
                if ( Fincr[i] == 0):
                    F_presc[i] = 0.0
                else:
                    F_presc[i] = ( Fincr[i]**( float(t)/float(nSteps)) ) * Finitial[i]
                    if ( i < 3):
                        F_presc[i] = F_presc[i]-1.0

            for iteracio in range(0, 500):

                residual = self.multiply(A, stress) - sigma_presc + self.multiply(B, XX) - F_presc
                norm = residual.norm_2()

                if ( norm < 1E-9 and iteracio > 1):
                    break
                if ( iteracio > 100 and norm < 1E-5):
                    break
                if ( iteracio > 195):
                    print('This Problem is not converting')
                    pNode

                C = self.parameters.GetConstitutiveMatrix()
                C2 = self.multiply_matrix(A, C)
                for i in range(0,3):
                    for j in range(0,3):
                        C2[i,j] = C2[i,j] / self.F[j,j] # whatever
                SystemMatrix = KratosMultiphysics.CompressedMatrix(6,6)
                for i in range(0,6):
                    for j in range(0,6):
                        SystemMatrix[i,j] = C2[i,j] + B[i,j]
                
                hello = self.linear_solver.Solve(SystemMatrix, dx, residual)
                
                for i in range(0,6):
                    XX[i] = XX[i] - dx[i];
                self.F.fill_identity()
                for j in range(0,3):
                    self.F[j,j] = self.F[j,j] + 1.0*XX[j]
                self.detF = self._compute_determinant(self.F)

                #Compute
                self.parameters.SetDeformationGradientF( self.F )
                self.parameters.SetDeterminantF( self.detF )
                self.material_law.CalculateMaterialResponseKirchhoff( self.parameters)


            self.material_law.FinalizeMaterialResponseKirchhoff( self.parameters )
            stress = self.parameters.GetStressVector();



    def multiply(self, A, x):
        y = KratosMultiphysics.Vector(x)
        y.fill(0.0)
        for i in range(0,y.Size() ):
            for j in range(0,y.Size() ):
                y[i] = y[i] + A[i,j]*x[j]
        return y

    def multiply_matrix(self, A, B):
        C = KratosMultiphysics.Matrix(A.Size1(), B.Size2())
        C.fill(0.0)

        for i in range(0, A.Size1()):
            for j in range(0, A.Size2()):
                for k in range(0, C.Size2() ):
                    C[i,k] = C[i,k] + A[i,j]*B[j,k]
        return C

    
    def _setInitialStressState(self, pressure):
    
        #self._create_material_model_and_law()
        
        self.F = KratosMultiphysics.Matrix(3,3)
        self.F.fill_identity()

        self.detF = 1.0
        self.parameters.SetDeformationGradientF( self.F )
        self.parameters.SetDeterminantF( self.detF )
       
        self.material_law.CalculateMaterialResponseKirchhoff( self.parameters )
        self.material_law.FinalizeMaterialResponseKirchhoff( self.parameters )

        
        XX = KratosMultiphysics.Vector(6)
        XX.fill(0.0)

        dx = KratosMultiphysics.Vector(6)
        dx.fill(0.0)

        sigmapresc = KratosMultiphysics.Vector(6)
        sigmapresc.fill(0.0)
        sigmapresc[0] = pressure
        sigmapresc[1] = pressure
        sigmapresc[2] = pressure

        
        for iteracio in range(0, 200):
            stress = self.parameters.GetStressVector()

            residual = stress-sigmapresc;
            
            #Compute the norm of the residual
            norm = residual.norm_2()

            if (norm < 1e-9):
                break
            if ( norm < 1e-5 and iteracio > 100):
                break
            if ( iteracio > 195):
                disp('The initial state is not converging')
                pNode


            C = self.parameters.GetConstitutiveMatrix()
            SystemMatrix = KratosMultiphysics.CompressedMatrix(6,6)
            for i in range(0,6):
                for j in range(0,6):
                    SystemMatrix[i,j] = C[i,j]
            hello = self.linear_solver.Solve(SystemMatrix, dx, residual)

            


            for i in range(0,6):
                XX[i] = XX[i] - dx[i];
            self.F.fill_identity()
            for j in range(0,3):
                self.F[j,j] = self.F[j,j] + 1.0*XX[j]
            self.detF = self._compute_determinant(self.F)

            #Compute
            self.parameters.SetDeformationGradientF( self.F )
            self.parameters.SetDeterminantF( self.detF )
            self.material_law.CalculateMaterialResponseKirchhoff( self.parameters)


        self.material_law.FinalizeMaterialResponseKirchhoff( self.parameters )
        stress = self.parameters.GetStressVector();

        

    def _get_problem_matrices_and_vectors(self):
    
        V1 = KratosMultiphysics.Vector(6)
        V1.fill(0.0)
        V2 = KratosMultiphysics.Vector(V1)
        V3 = KratosMultiphysics.Vector(V1)
        V4 = KratosMultiphysics.Vector(V1)

        A = KratosMultiphysics.Matrix(6,6)
        A.fill(0.0);
        B = KratosMultiphysics.Matrix(A)
        return [V1, V2, V3, V4, A, B]

    def _create_material_model_and_law(self):

        settings = KratosMultiphysics.Parameters("""
        {
            "model_part_name" : "MaterialDomain",
            "properties_id"   : 1,
            "material_name"   : "steel",
            "constitutive_law": {
                "law_name"   : "KratosMultiphysics.ConstitutiveModelsApplication.LargeStrain3DLaw",
                "model_name" : "KratosMultiphysics.ConstitutiveModelsApplication.CamClayModel"
            },
            "variables": {
            },
            "element_type": "Tetrahedra3D4",
            "nodes" : [ [0.0,0.0,0.0], [1.0,0.0,0.0], [0.0,1.0,0.0], [0.0,0.0,1.0] ],
            "strain": {
                "deformation_gradient" : [ [1.0,0.0,0.0], [0.0,1.0,0.0], [0.0,0.0,1.0] ],
                "jacobian": 1.0
            },
            "echo_level" : 0,
	    "parametric_analysis_variable": "PRESSURE",
	    "parametric_analysis_values": [-86.0, -80.0, -70.0, -60.0, -50.0, -40.0, -30.0, -20.0, -10.0, -5.0, -2.5],
	    "initial_stress_state": [-56, -56, -56]
        }""")
       
        material_settings = KratosMultiphysics.Parameters("""
        {
          "material_models_list": [
        {
          "process_module": "ConstitutiveModelsApplication.assign_materials_process",
          "Parameters": {
            "model_part_name": "Materials_Auto1",
            "material_name": "Casm",
            "constitutive_law": {
              "name": "LargeStrainAxisymmetric2DLaw.CamClayModel"
            },
            "properties_id": 1,
            "variables": {
              "SWELLING_SLOPE": 0.001,
              "NORMAL_COMPRESSION_SLOPE": 0.01,
              "INITIAL_SHEAR_MODULUS": 5000.0,
              "ALPHA_SHEAR": 0.0,
              "CRITICAL_STATE_LINE": 1.00,
              "INTERNAL_FRICTION_ANGLE": 0.0,
              "PRE_CONSOLIDATION_STRESS": 100.0
            }
          }
        }
      ]
    }""")
        
        model = KratosMultiphysics.Model()
        self.model_part = model.CreateModelPart(settings["model_part_name"].GetString())
        self.echo_level = settings["echo_level"].GetInt()

        #read nodes
        self.number_of_nodes = settings["nodes"].size()
        self.nodes = [] #self.model_part.GetNodes()
        for i in range(0, self.number_of_nodes):
            node = self.model_part.CreateNewNode(i,settings["nodes"][i][0].GetDouble(),settings["nodes"][i][1].GetDouble(),settings["nodes"][i][2].GetDouble())
            self.nodes.append(node)

        self.geometry  = KratosMultiphysics.Geometry()
        self.dimension = 3
        if( settings["element_type"].GetString() == "Tetrahedra3D4"):
            if( self.number_of_nodes != 4 ):
                print(" number of nodes:",self.number_of_nodes," do not matches geometry :", settings["element_type"].GetString() )
            else:
                self.geometry = KratosMultiphysics.Tetrahedra3D4(self.nodes[0],self.nodes[1],self.nodes[2],self.nodes[3])
                
        if( settings["element_type"].GetString() == "Triangle2D3"):
            if( self.number_of_nodes != 4 ):
                print(" number of nodes:",self.number_of_nodes," do not matches geometry :", settings["element_type"].GetString() )
            else:
                self.geometry  = KratosMultiphysics.Triangle2D3(self.nodes[0],self.nodes[1],self.nodes[2])
                self.dimension = 2
                
        #material properties
        self.properties = self.model_part.Properties[settings["properties_id"].GetInt()]

        #read variables
        parameters_material = material_settings["material_models_list"][0]["Parameters"]
        self.variables = material_settings["material_models_list"][0]["Parameters"]["variables"]
        this = parameters_material["constitutive_law"]["name"].GetString()
        
        for key, value in self.variables.items():
            variable = KratosMultiphysics.KratosGlobals.GetVariable( key )
            if( value.IsDouble() ):
                self.properties.SetValue(variable, value.GetDouble())
            elif( value.IsArray() ):
                vector_value = KratosMultiphysics.Vector(value.size())
                for i in range(0, value.size() ):
                    vector_value[i] = value[i].GetDouble()
                self.properties.SetValue(variable, vector_value)

        splitted = this.split(".")

        constitutive_law_name = "KratosMultiphysics.ConstitutiveModelsApplication.LargeStrain3DLaw" 
        constitutive_model_name = "KratosMultiphysics.ConstitutiveModelsApplication." + splitted[1]

        self.material_model = self._GetItemFromModule( constitutive_model_name)()
        self.material_law   = self._GetItemFromModule( constitutive_law_name)(self.material_model)

        #check dimension
        if(self.material_law.WorkingSpaceDimension() != self.dimension):
            raise Exception( "mismatch between the ConstitutiveLaw dimension and the dimension of the space")

        #set strain
        self.F = KratosMultiphysics.Matrix(3,3)
        self.strain_measure = settings["strain"]["deformation_gradient"]
       
        for i in range(0,3):
            for j in range(0,3):
                self.F[i,j] = self.strain_measure[i][j].GetDouble()
        
        self.detF = settings["strain"]["jacobian"].GetDouble()        
        
        #element parameters
        self.N     = KratosMultiphysics.Vector(self.number_of_nodes)
        self.DN_DX = KratosMultiphysics.Matrix(self.number_of_nodes, self.dimension)
        
        #set calculation flags
        self.options = KratosMultiphysics.Flags()
        self.options.Set(KratosMultiphysics.ConstitutiveLaw.USE_ELEMENT_PROVIDED_STRAIN, True)
        self.options.Set(KratosMultiphysics.ConstitutiveLaw.COMPUTE_STRESS, True)
        self.options.Set(KratosMultiphysics.ConstitutiveLaw.COMPUTE_CONSTITUTIVE_TENSOR, True)

        #set calculation variables
        self.stress_vector       = KratosMultiphysics.Vector(self.material_law.GetStrainSize())
        self.strain_vector       = KratosMultiphysics.Vector(self.material_law.GetStrainSize())
        self.constitutive_matrix = KratosMultiphysics.Matrix(self.material_law.GetStrainSize(),self.material_law.GetStrainSize())

                
        self.parameters = KratosMultiphysics.ConstitutiveLawParameters()
        
        self.parameters.SetOptions( self.options )
        self.parameters.SetDeformationGradientF( self.F )
        self.parameters.SetDeterminantF( self.detF )
        self.parameters.SetStrainVector( self.strain_vector )
        self.parameters.SetStressVector( self.stress_vector )
        self.parameters.SetConstitutiveMatrix( self.constitutive_matrix )
        self.parameters.SetShapeFunctionsValues( self.N )
        self.parameters.SetShapeFunctionsDerivatives( self.DN_DX )
        self.parameters.SetProcessInfo( self.model_part.ProcessInfo )
        self.parameters.SetMaterialProperties( self.properties )
        self.parameters.SetElementGeometry( self.geometry )



    def _GetItemFromModule(self,my_string):

        import importlib 

        splitted = my_string.split(".")
        if(len(splitted) == 0):
            raise Exception("something wrong. Trying to split the string "+my_string)
        if(len(splitted) == 1):
            return eval(my_string)
        else:
            module_name = ""
            for i in range(len(splitted)-1):
                module_name += splitted[i] 
                if i != len(splitted)-2:
                    module_name += "."

            module = importlib.import_module(module_name)
            return getattr(module,splitted[-1]) 
    
    def _OpenOutputFile(self, FileName):
        import os.path

        FileName = str(self.current_case)+ FileName
        problem_path = os.getcwd()
        self.csv_path = os.path.join(problem_path, FileName)

        self.csv_file = open(self.csv_path, "w")


        N = self.properties.GetValue( KratosMultiphysics.ConstitutiveModelsApplication.SHAPE_PARAMETER) 
        R = self.properties.GetValue( KratosMultiphysics.ConstitutiveModelsApplication.SPACING_RATIO)
        PC = self.properties.GetValue( KratosMultiphysics.ConstitutiveModelsApplication.PRE_CONSOLIDATION_STRESS )
        line = str(N) + ' , ' + str(R) + ' , ' + str(PC);

        for i in range(0, 13):
            line = line + ' 0 , '
        line = line + " \n"

        self.csv_file.write(line)

    def _WriteThisToFile(self, t, stress, F):

        line = str(t) + " , "
        for st in stress:
            line = line + str(st) + " , "


        line = line + str(F[0,0]) + " , "
        line = line + str(F[1,1]) + " , "
        line = line + str(F[2,2]) + " , "
        
        ps = 0;
        ps = self.material_law.GetValue( KratosMultiphysics.PRE_CONSOLIDATION_STRESS, ps)
        pt = 0;
        pt = self.material_law.GetValue( KratosMultiphysics.ConstitutiveModelsApplication.PT, pt)
        pm = 0;
        pm = self.material_law.GetValue( KratosMultiphysics.ConstitutiveModelsApplication.PM, pm)

        line = line + str(ps) + " , " + str(pt) + " , " + str(pm)

        line = line + " \n"
        self.csv_file.write(line)

    def _compute_determinant(self, A):

        det = 0

        det = det + A[0,0]*A[1,1]*A[2,2]
        det = det + A[1,0]*A[2,1]*A[0,2]
        det = det + A[2,0]*A[0,1]*A[1,2]

        det = det - A[0,2]*A[1,1]*A[2,0]
        det = det - A[1,2]*A[2,1]*A[0,0]
        det = det - A[2,2]*A[0,1]*A[1,0]

        return det


if __name__ == '__main__':
    KratosUnittest.main()
