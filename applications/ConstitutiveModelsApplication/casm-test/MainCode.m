function [] = MainCode()

for i = [1:3, 4]
    figure(i)
    hold off;
end

global M;
global ShapeN;
global SpacingR;

M = 0.98;
ShapeN = 10;
SpacingR = 12;

global number
number = 0;






for i = [0:0]

%      ThisExists = MakeThisFile([num2str(i), 'undrained_triaxial.csv']);
%     ThisExists = MakeThisFile([num2str(i),'drained_triaxial.csv']);
    ThisExists = MakeThisFile([num2str(i),'undrained_triaxial.csv']);
    if (ThisExists == false )
        break
    end
    pause(0.01)
    %      MakeThisFile([num2str(i),'oedometer.csv']);
%     %      MakeThisFile([num2str(i),'isotropic.csv']);
    
end


figure(2)
set(gca, 'FontSize', 15)
xlabel('$\| \epsilon^d \|$', 'interpreter', 'latex')
ylabel('$q$ (kPa)', 'interpreter', 'latex')
yy = ylim();

ylim([0, yy(2)]);

figure(1)
set(gca, 'FontSize', 15)
xlabel('$p^{\prime}$ (kPa)', 'interpreter', 'latex')
ylabel('$q$ (kPa)', 'interpreter', 'latex')
axis equal
xlim([0, 80])
ylim([0, 60])


figure(3)
set(gca, 'FontSize', 15)
xlabel('$p^{\prime}$ (kPa)', 'interpreter', 'latex')
ylabel('$\epsilon_v$', 'interpreter', 'latex')


figure(1)
drawnow
print('EffectiveStressPath-Casm', '-dpng')
figure(2)
drawnow
ylim([0, 60])
print('DevDef-DevStress-Casm', '-dpng')

function [ThisExists] = MakeThisFile(XFILE)
global number
number = number+1;

ThisExists = true;

if ( isfile(XFILE) == false)
    ThisExists = false;
    return
end
rawData = csvread(XFILE);

global ShapeN;
global SpacingR;

ShapeN = rawData(1,1);
SpacingR = rawData(1,2);
rawData = rawData(2:end,:);

Stress = rawData(:,2:7);
Strain = rawData(:,8:13);

ps = rawData(:,14);


[p, J] = ComputeStressInvariants( Stress);
[eVol, eDev] = ComputeStrainInvariants( Strain);


figure(1)
PlotYieldSurface( ps(1),  'k-.');
% PlotYieldSurface( ps(end), 'g-.');


SPEC = ':';
linew = 1;
if ( number == 1 || number == 5 || number == 8)
    SPEC = '-';
    linew = 3;
end

if ( number == 8)
    SPEC = ['r', SPEC];
end


% Triaxial plane
plot(p, J, SPEC, 'linewidth', linew);
xlabel('p')
ylabel('q')


figure(2)
plot( eDev-eDev(1), J,  SPEC, 'linewidth', linew);
xlabel('eDev')
ylabel('sDev')
hold on



figure(3)
semilogx( p, -eVol+eVol(1),  SPEC, 'linewidth', linew);
xlabel('p')
ylabel('eVol')
hold on

disp(max(J)/2)
disp(J(end)/2)

figure(4)
plot( eDev-eDev(1), -eVol+eVol(1),  SPEC, 'linewidth', linew);
xlabel('eDev')
ylabel('eVol')
hold on


function PlotYieldSurface( p0, SPEC)

global M;
global ShapeN;
global SpacingR;
n = ShapeN;
r = SpacingR;

p0 = -p0;


pp = linspace(0, p0, 1000);



plot([0, 1.5*p0], M*[0, 1.5*p0], 'r-.')
hold on

qq = M*pp .* ( -log(pp/p0)/log(r)).^(1/n);
% plot(pp, qq, SPEC, 'linewidth', 1.0)







function [p, J] = ComputeStressInvariants( Stress)

p = sum(Stress(:,1:3)')/3;

for i = 1:size(Stress,1)
    J(i) = 0;
    for e = 1:3
        J(i) = J(i) + (Stress(i,e)-p(i))^2;
    end
    for e = 4:6
        J(i) = J(i) + 2*(Stress(i,e))^2;
    end
    J(i) = sqrt(0.5*J(i))*sqrt(3);
end
p = - p;


function [eVol, eDev] = ComputeStrainInvariants( Strain)

eVol = sum(Strain(:,1:3)')/3;

for i = 1:size(Strain,1)
    eDev(i) = 0;
    for e = 1:3
        eDev(i) = eDev(i) + (Strain(i,e)-eVol(i))^2;
    end
    for e = 4:6
        eDev(i) = eDev(i) + 2*(Strain(i,e)/2.0)^2;
    end
    eDev(i) = sqrt(0.5*eDev(i))*sqrt(3);
end
eVol = - eVol*3;
