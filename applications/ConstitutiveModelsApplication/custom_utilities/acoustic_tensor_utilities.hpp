//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                    LM $
//   date:                $date:              September 2020 $
//
//

#if !defined(KRATOS_ACOUSTIC_TENSOR_UTILITIES)
#define KRATOS_ACOUSTIC_TENSOR_UTILITIES

// System includes
#include <cmath>
#include <set>

// External includes
#include "boost/smart_ptr.hpp"

// Project includes
#include "includes/define.h"
#include "includes/variables.h"

#include "constitutive_model_utilities.hpp"

namespace Kratos
{

   class AcousticTensorUtilities
   {

public:
    ///@name Type Definitions
    ///@{

    /// Pointer definition of AcousticTensorUtilities
    KRATOS_CLASS_POINTER_DEFINITION(AcousticTensorUtilities);

    ///@}
    ///@name Life Cycle
    ///@{

    /// Default constructor.
    AcousticTensorUtilities();

    /// Assignment operator.
    AcousticTensorUtilities& operator=(AcousticTensorUtilities const& rOther);

    /// Copy constructor.
    AcousticTensorUtilities(AcousticTensorUtilities const& rOther);

    /// Clone.
    virtual AcousticTensorUtilities::Pointer Clone() const;

    /// Destructor.
    virtual ~AcousticTensorUtilities();

    ///@}
    ///@name Operators
    ///@{


    ///@}
    ///@name Operations
    ///@{


    ///@}
    ///@name Access
    ///@{

    void SetConstitutiveTensor( const Matrix& rEP, const Matrix& rE, const bool & rPlastic);

    void GetLocalizationAngle( double & rValue);

    void GetDetAcousticTensor( double & rValue);

    void GetAcousticDirectionN( array_1d<double, 3> & rValue);

    void GetAcousticDirectionM( array_1d<double, 3> & rValue);

    void GetAcousticDirectionN2( array_1d<double, 3> & rValue);

    void GetAcousticDirectionM2( array_1d<double, 3> & rValue);

    ///@}
    ///@name Inquiry
    ///@{


    ///@}
    ///@name Input and output
    ///@{

    /// Turn back information as a string.
    virtual std::string Info() const {
      std::stringstream buffer;
      buffer << "AcousticTensorUtilities";
      return buffer.str();
    };

    // /// Print information about this object.
    // virtual void PrintInfo(std::ostream& rOStream) const {};

    // /// Print object's data.
    // virtual void PrintData(std::ostream& rOStream) const {};


    ///@}
    ///@name Friends
    ///@{


    ///@}

protected:
    ///@name Protected static Member Variables
    ///@{


    ///@}
    ///@name Protected member Variables
    ///@{


    ///@}
    ///@name Protected Operators
    ///@{


    ///@}
    ///@name Protected Operations
    ///@{


    ///@}
    ///@name Protected  Access
    ///@{



    ///@}
    ///@name Protected Inquiry
    ///@{


    ///@}
    ///@name Protected LifeCycle
    ///@{


    ///@}

private:
    ///@name Static Member Variables
    ///@{


    ///@}
    ///@name Member Variables
    ///@{

    Matrix mConstitutiveTensor;
    Matrix mElasticTensor;
    array_1d<double, 3>  mDirectionN;
    array_1d<double, 3>  mDirectionM;
    array_1d<double, 3>  mDirectionN2;
    array_1d<double, 3>  mDirectionM2;
    double mLowestEigenValue;
    double mDetAcousticTensor;
    bool   mPlastic;

    bool mInitialized;
    bool mComputed;


    ///@}
    ///@name Private Operators
    ///@{


    ///@}
    ///@name Private Operations
    ///@{

    void ComputeAcousticTensor();

    ///@}
    ///@name Private  Access
    ///@{


    ///@}
    ///@name Private Inquiry
    ///@{


    ///@}
    ///@name Un accessible methods
    ///@{



    ///@}


   }; // end class AcousticTensorUtilities


} // end namespace kratos

#endif // KRATOS_ACOUSTIC_TENSOR_UTILITIES
