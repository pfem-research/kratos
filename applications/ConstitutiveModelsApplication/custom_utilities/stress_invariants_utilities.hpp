//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:              LMonforte $
//   Maintained by:       $Maintainer:                    LM $
//   Date:                $Date:                  April 2017 $
//
//

#if !defined(KRATOS_STRESS_INVARIANTS_UTILITIES)
#define KRATOS_STRESS_INVARIANTS_UTILITIES

// System includes
#include <cmath>
#include <set>

// External includes
#include "boost/smart_ptr.hpp"

// Project includes
#include "includes/define.h"
#include "includes/variables.h"

#include "constitutive_model_utilities.hpp"

namespace Kratos
{

class StressInvariantsUtilities
{

public:
   typedef BoundedMatrix<double, 3, 3> MatrixType;

   typedef array_1d<double, 6> VectorType;

   typedef unsigned int IndexType;

   typedef unsigned int SizeType;

   static inline void CalculateStressInvariants(const MatrixType &rStressMatrix, double &rI1, double &rJ2sqrt)
   {
      Vector StressVector = ZeroVector(6);
      ConstitutiveModelUtilities::StressTensorToVector(rStressMatrix, StressVector);
      CalculateStressInvariants(StressVector, rI1, rJ2sqrt);
   }

   static inline void CalculateStressInvariants(const Vector &rStress, double &I1, double &J2sqrt)
   {

      I1 = 0;
      for (unsigned int i = 0; i < 3; ++i)
         I1 += rStress(i);
      I1 /= 3.0;

      J2sqrt = 0;
      for (unsigned int i = 0; i < 3; i++)
         J2sqrt += pow(rStress(i) - I1, 2);

      for (unsigned int i = 3; i < 6; i++)
         J2sqrt += 2.0 * pow(rStress(i), 2);

      J2sqrt = sqrt(0.5 * J2sqrt);
   }

   static inline void CalculateStressInvariants(const MatrixType &rStressMatrix, double &rI1, double &rJ2sqrt, double &rLode)
   {
      Vector StressVector(6);
      noalias(StressVector) = ZeroVector(6);
      ConstitutiveModelUtilities::StressTensorToVector(rStressMatrix, StressVector);
      CalculateStressInvariants(StressVector, rI1, rJ2sqrt, rLode);
   }
   static inline void CalculateStressInvariants(const Vector &rStress, double &I1, double &J2sqrt, double &Lode)
   {
      CalculateStressInvariants(rStress, I1, J2sqrt);
      Matrix StressTensor(3, 3);
      StressTensor = MathUtils<double>::StressVectorToTensor(rStress);

      for (unsigned int i = 0; i < 3; ++i)
         StressTensor(i, i) -= I1;

      Lode = MathUtils<double>::Det(StressTensor);
      Lode = 3.0 * sqrt(3.0) / 2.0 * Lode / pow(J2sqrt, 3);

      double epsi = 1.0e-6;
      if (fabs(Lode) > 1.0 - epsi)
      {
         Lode = -(30.0 - epsi) * Globals::Pi / 180.0 * Lode / fabs(Lode);
      }
      else if (J2sqrt < epsi)
      {
         Lode = (30.0 - epsi) * Globals::Pi / 180.0;
      }
      else
      {
         Lode = std::asin(-Lode) / 3.0;
      }
   }

   static inline void CalculateDerivativeVectors(const MatrixType &rStressMatrix, VectorType &C1, VectorType &C2)
   {
      Vector StressVector = ZeroVector(6);
      ConstitutiveModelUtilities::StressTensorToVector(rStressMatrix, StressVector);
      CalculateDerivativeVectors(StressVector, C1, C2);
   }

   static inline void CalculateDerivativeVectors(const Vector &rStress, VectorType &C1, VectorType &C2)
   {

      C1.clear();
      for (unsigned int i = 0; i < 3; i++)
         C1(i) = 1.0 / 3.0;

      double I1, J2sqrt;
      CalculateStressInvariants(rStress, I1, J2sqrt);

      C2.clear();

      for (unsigned int i = 0; i < 3; i++)
         C2(i) = rStress(i) - I1;

      for (unsigned int i = 3; i < 6; i++)
         C2(i) = 2.0 * rStress(i);

      if (J2sqrt > 1E-5)
      {
         C2 /= 2.0 * J2sqrt;
      }
      else
      {
         noalias( C2 ) = ZeroVector(6);
      }
   }

   static inline void CalculateDerivativeVectors(const MatrixType &rStressMatrix, VectorType &C1, VectorType &C2, VectorType &C3)
   {
      Vector StressVector = ZeroVector(6);
      ConstitutiveModelUtilities::StressTensorToVector(rStressMatrix, StressVector);
      CalculateDerivativeVectors(StressVector, C1, C2, C3);
   }

   static inline void CalculateDerivativeVectors(const Vector &rStress, VectorType &C1, VectorType &C2, VectorType &C3)
   {
      // COPY
      noalias( C1 ) = ZeroVector(6);
      for (unsigned int i = 0; i < 3; i++)
         C1(i) = 1.0 / 3.0;

      double I1, J2sqrt;
      CalculateStressInvariants(rStress, I1, J2sqrt);

      noalias( C2 ) = ZeroVector(6);

      for (unsigned int i = 0; i < 3; i++)
         C2(i) = rStress(i) - I1;

      for (unsigned int i = 3; i < 6; i++)
         C2(i) = 2.0 * rStress(i);

      if (J2sqrt > 1E-5)
      {
         C2 /= (2.0 * J2sqrt);
      }
      else
      {
         noalias( C2 ) = ZeroVector(6);
         noalias( C3 ) = ZeroVector(6);
         return;
      }

      Vector ShearStress = rStress;
      for (int i = 0; i < 3; i++)
         ShearStress(i) -= I1;

      MatrixType Aux, ShearStressM;
      ShearStressM = MathUtils<double>::StressVectorToTensor(ShearStress);
      Aux = prod(ShearStressM, ShearStressM);

      for (unsigned int i = 0; i < 3; i++)
         Aux(i, i) -= 1.0 / 3.0 * 2.0 * pow(J2sqrt, 2);

      noalias( C3 ) = MathUtils<double>::StrainTensorToVector(Aux, 6);
   }

}; // end Class StressInvariantsUtilities

} // end namespace Kratos

#endif // KRATOS_STRESS_INVARIANTS_UTILITIES
