//
//   Project Name:        KratosSolidMechanicsApplication $
//   Developed by:        $Developer:           LMonforte $
//   Maintained by:       $Maintainer:                 LM $
//   date:                $date:           September 2020 $
//
//

// System includes

// External includes

// Project includes
#include "custom_utilities/acoustic_tensor_utilities.hpp"
#include "constitutive_models_application_variables.h"

namespace Kratos
{

   AcousticTensorUtilities::AcousticTensorUtilities()
   {
      KRATOS_TRY

      mInitialized = false;
      mComputed = false;

      KRATOS_CATCH("")
   }

   AcousticTensorUtilities::AcousticTensorUtilities( AcousticTensorUtilities const & rOther):
      mConstitutiveTensor( rOther.mConstitutiveTensor),
      mElasticTensor( rOther.mElasticTensor),
      mDirectionN( rOther.mDirectionN), mDirectionM( rOther.mDirectionM),
      mDirectionN2( rOther.mDirectionN2), mDirectionM2( rOther.mDirectionM2),
      mLowestEigenValue( rOther.mLowestEigenValue),
      mDetAcousticTensor( rOther.mDetAcousticTensor),
      mPlastic( rOther.mPlastic),
      mInitialized( rOther.mInitialized),
      mComputed( rOther.mComputed)
   {
   }

   AcousticTensorUtilities::Pointer AcousticTensorUtilities::Clone() const
   {
      KRATOS_TRY

      return Kratos::make_shared<AcousticTensorUtilities>(*this);

      KRATOS_CATCH("")
   }

   AcousticTensorUtilities::~AcousticTensorUtilities()
   {
   }

   void AcousticTensorUtilities::SetConstitutiveTensor( const Matrix & rEP, const Matrix & rE, const bool & rPlastic)
   {

      KRATOS_TRY

      if ( mConstitutiveTensor.size1() != rEP.size1() )
      {
         mConstitutiveTensor.resize( rEP.size1(), rEP.size2(), false);
      }
      mConstitutiveTensor =  rEP;
      if ( mElasticTensor.size1() != rE.size1() )
      {
         mElasticTensor.resize( rE.size1(), rE.size2(), false);
      }
      mElasticTensor = rE;

      mPlastic = rPlastic;

      mInitialized = true;
      mComputed = false;


      KRATOS_CATCH("")
   }


   void AcousticTensorUtilities::ComputeAcousticTensor()
   {
      KRATOS_TRY

      if ( mComputed ) {
         return;
      }
      mComputed = true;


      mLowestEigenValue = 1.0;
      mDetAcousticTensor = 1.0;
      noalias( mDirectionM ) = ZeroVector(3);
      noalias( mDirectionN ) = ZeroVector(3);
      noalias( mDirectionM2 ) = ZeroVector(3);
      noalias( mDirectionN2 ) = ZeroVector(3);

      if ( mPlastic == false)
      {
         return;
      }


      double MinRatio = 20;
      double n1, n2, n3;
      Matrix NMatrix(6,3);
      noalias( NMatrix ) = ZeroMatrix(6,3);
      Matrix Q0 = Matrix(3,3);
      Matrix Q = Matrix(3,3);
      double ratio = 0;
      double phiBest = 0;

      // 1. Compute the minimum of the determinant of the acoustic tensor


      //for (double theta = 0; theta < 90; theta +=1.0) {
      double theta = 90;
      for (double phi = 0; phi <= 90; phi +=1.0) {
         n1 = std::sin(theta*Globals::Pi/180.0)*std::cos(phi*Globals::Pi/180.0);
         n2 = std::sin(theta*Globals::Pi/180.0)*std::sin(phi*Globals::Pi/180.0);
         n3 = std::cos(theta*Globals::Pi/180.0);
         NMatrix(0,0) = n1; NMatrix(1,1) = n2; NMatrix(2,2) = n3;
         NMatrix(3,0) = n2; NMatrix(3,1) = n1;
         NMatrix(4,0) = n3; NMatrix(4,2) = n1;
         NMatrix(5,1) = n3; NMatrix(5,2) = n2;

         noalias( Q )  = prod( trans(NMatrix), Matrix( prod( mConstitutiveTensor, NMatrix)));
         noalias( Q0 ) = prod( trans(NMatrix), Matrix( prod( mElasticTensor, NMatrix)));
         ratio = MathUtils<double>::Det3(Q) / MathUtils<double>::Det3(Q0);

         /*std::cout << " phi " << phi << std::endl;
         std::cout << " const1 " << mConstitutiveTensor << std::endl;
         std::cout << " const2 " << mElasticTensor << std::endl;
         std::cout << " Q " << Q << std::endl;
         std::cout << " Q0 " << Q << std::endl;
         std::cout << " N " << NMatrix << std::endl;
         std::cout << " detQ " <<   MathUtils<double>::Det3(Q) << " det Q0 " <<  MathUtils<double>::Det3(Q0) << std::endl;
         std::cout << " ratio " << ratio << std::endl;
         std::cout << std::endl;
         std::cout << std::endl;
         std::cout << std::endl;
         std::cout << std::endl;*/

         if ( ratio < MinRatio)
         {
            phiBest = phi;
            MinRatio = ratio;
            mDirectionN(0) = n1;
            mDirectionN(1) = n2;
            mDirectionN(2) = n3;
         }

      }
      //}
      mDetAcousticTensor = MinRatio;

      // 2. Compute direction M
      n1 = mDirectionN(0);
      n2 = mDirectionN(1);
      n3 = mDirectionN(2);
      NMatrix(0,0) = n1; NMatrix(1,1) = n2; NMatrix(2,2) = n3;
      NMatrix(3,0) = n2; NMatrix(3,1) = n1;
      NMatrix(4,0) = n3; NMatrix(4,2) = n1;
      NMatrix(5,1) = n3; NMatrix(5,2) = n2;

      noalias( Q )  = prod( trans(NMatrix), Matrix( prod( mConstitutiveTensor, NMatrix)));


      Matrix EigenValues(3,3);
      Matrix EigenVectors(3,3);
      MathUtils<double>::GaussSeidelEigenSystem<Matrix, Matrix>(Q, EigenVectors, EigenValues);

      std::vector<double> aux(3);
      for (unsigned int i = 0; i < 3; i++)
         aux[i] = EigenValues(i,i);
      std::sort( aux.begin(), aux.end());
      unsigned int index = 0;
      for (unsigned int i = 0; i < 3; i++) {
         if ( aux[0] == EigenValues(i,i)) {
            index = i;
         }
      }


      // the direction can not be the out of the plane...
      if ( abs(EigenVectors(index, 2)) > 0.8) {
         unsigned int prevIndex(index);
         index = prevIndex;
         for (unsigned int i = 0; i < 3; i++) {
            if ( aux[1] == EigenValues(i,i) && prevIndex != i) {
               index = i;
            }
         }
      }

      for (unsigned int i = 0; i<3; i++) {
         mDirectionM(i) = EigenVectors( i, index);
      }


      /*std::cout << " -- " << std::endl;
      std::cout << " -- " << std::endl;
      std::cout << " -- " << std::endl;
      std::cout << " -- " << std::endl;
      std::cout << " -- " << std::endl;
      std::cout << " Q " << Q << std::endl;
      std::cout << mDirectionN << std::endl;
      std::cout << mDirectionM << std::endl;
      std::cout << EigenVectors << std::endl;
      std::cout << EigenValues << std::endl;
      std::cout << aux << std::endl;
      std::cout << std::endl;
      std::cout << std::endl;
      std::cout << std::endl;
      std::cout << std::endl;*/

      MinRatio = 20.0;

      //for (double theta = 0; theta < 90; theta +=1.0) {
      theta = 90;
      for (double phi = phiBest + 2.0; phi <= 180; phi +=1.0) {
         n1 = std::sin(theta*Globals::Pi/180.0)*std::cos(phi*Globals::Pi/180.0);
         n2 = std::sin(theta*Globals::Pi/180.0)*std::sin(phi*Globals::Pi/180.0);
         n3 = std::cos(theta*Globals::Pi/180.0);
         NMatrix(0,0) = n1; NMatrix(1,1) = n2; NMatrix(2,2) = n3;
         NMatrix(3,0) = n2; NMatrix(3,1) = n1;
         NMatrix(4,0) = n3; NMatrix(4,2) = n1;
         NMatrix(5,1) = n3; NMatrix(5,2) = n2;

         noalias( Q )  = prod( trans(NMatrix), Matrix( prod( mConstitutiveTensor, NMatrix)));
         noalias( Q0 ) = prod( trans(NMatrix), Matrix( prod( mElasticTensor, NMatrix)));
         ratio = MathUtils<double>::Det3(Q) / MathUtils<double>::Det3(Q0);

         if ( ratio < MinRatio)
         {
            MinRatio = ratio;
            mDirectionN2(0) = n1;
            mDirectionN2(1) = n2;
            mDirectionN2(2) = n3;
         }

      }
      //}
      // 2. Compute direction M
      n1 = mDirectionN2(0);
      n2 = mDirectionN2(1);
      n3 = mDirectionN2(2);
      NMatrix(0,0) = n1; NMatrix(1,1) = n2; NMatrix(2,2) = n3;
      NMatrix(3,0) = n2; NMatrix(3,1) = n1;
      NMatrix(4,0) = n3; NMatrix(4,2) = n1;
      NMatrix(5,1) = n3; NMatrix(5,2) = n2;

      noalias( Q )  = prod( trans(NMatrix), Matrix( prod( mConstitutiveTensor, NMatrix)));


      //Matrix EigenValues(3,3);
      //Matrix EigenVectors(3,3);
      MathUtils<double>::GaussSeidelEigenSystem<Matrix, Matrix>(Q, EigenVectors, EigenValues);

      //std::vector<double> aux(3);
      for (unsigned int i = 0; i < 3; i++)
         aux[i] = EigenValues(i,i);
      std::sort( aux.begin(), aux.end());
      //unsigned int index = 0;
      for (unsigned int i = 0; i < 3; i++) {
         if ( aux[0] == EigenValues(i,i)) {
            index = i;
         }
      }

      if ( abs(EigenVectors(index, 2)) > 0.8) {
         unsigned int prevIndex(index);
         index = prevIndex;
         for (unsigned int i = 0; i < 3; i++) {
            if ( aux[1] == EigenValues(i,i) && prevIndex != i) {
               index = i;
            }
         }
      }

      for (unsigned int i = 0; i<3; i++) {
         mDirectionM2(i) = EigenVectors( i, index);
      }

      /*std::cout << " Q " << Q << std::endl;
      std::cout << mDirectionN2 << std::endl;
      std::cout << mDirectionM2 << std::endl;
      std::cout << EigenVectors << std::endl;
      std::cout << EigenValues << std::endl;
      std::cout << aux << std::endl;
      std::cout << std::endl;
      std::cout << std::endl;
      double merda = 0.0;
      for (unsigned int i = 0; i < 3; i++)
         merda += mDirectionN(i)*mDirectionN2(i);
      std::cout << (merda) <<  " " << abs(merda) << " " << fabs(merda) << " " << std::abs(merda) << std::endl;
      merda = 0.0;
      for (unsigned int i = 0; i < 3; i++)
         merda += mDirectionM(i)*mDirectionM2(i);
      std::cout << (merda) << std::endl;
      merda = 0.0;
      for (unsigned int i = 0; i < 3; i++)
         merda += mDirectionN(i)*mDirectionM(i);
      std::cout << (merda) << std::endl;
      merda = 0.0;
      for (unsigned int i = 0; i < 3; i++)
         merda += mDirectionN2(i)*mDirectionM2(i);
      std::cout << (merda) << std::endl;
      std::cout << std::endl;
      std::cout << std::endl;*/

      KRATOS_CATCH("")
   }


   void AcousticTensorUtilities::GetLocalizationAngle( double & rValue)
   {
      KRATOS_TRY

      rValue = 2.0;
      if ( mInitialized && mPlastic) {
         this->ComputeAcousticTensor();
         rValue = 0.0;
         for (unsigned int i = 0; i < 3; i++)
         {
            rValue += mDirectionN(i)*mDirectionM(i);
         }
      }

      rValue = fabs(rValue);

      KRATOS_CATCH("")
   }

   void AcousticTensorUtilities::GetDetAcousticTensor( double & rValue)
   {
      KRATOS_TRY

      rValue = 2.0;
      if ( mInitialized) {
         this->ComputeAcousticTensor();
         rValue = mDetAcousticTensor;
      }

      KRATOS_CATCH("")
   }

   void AcousticTensorUtilities::GetAcousticDirectionN( array_1d<double, 3> & rValue)
   {
      KRATOS_TRY

      noalias( rValue ) = ZeroVector(3);
      if ( mInitialized) {
         this->ComputeAcousticTensor();
         rValue = mDirectionN;
      }

      KRATOS_CATCH("")
   }

   void AcousticTensorUtilities::GetAcousticDirectionM( array_1d<double, 3> & rValue)
   {
      KRATOS_TRY

      noalias( rValue ) = ZeroVector(3);
      if ( mInitialized) {
         this->ComputeAcousticTensor();
         rValue = mDirectionM;
      }

      KRATOS_CATCH("")
   }

   void AcousticTensorUtilities::GetAcousticDirectionN2( array_1d<double, 3> & rValue)
   {
      KRATOS_TRY

      noalias( rValue ) = ZeroVector(3);
      if ( mInitialized) {
         this->ComputeAcousticTensor();
         rValue = mDirectionN2;
      }

      KRATOS_CATCH("")
   }

   void AcousticTensorUtilities::GetAcousticDirectionM2( array_1d<double, 3> & rValue)
   {
      KRATOS_TRY

      noalias( rValue ) = ZeroVector(3);
      if ( mInitialized) {
         this->ComputeAcousticTensor();
         rValue = mDirectionM2;
      }

      KRATOS_CATCH("")
   }

}  // namespace Kratos.
