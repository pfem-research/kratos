//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                  April 2017 $
//
//

// System includes

// External includes

// Project includes
#include "custom_laws/small_strain_laws/small_strain_3D_law.hpp"
#include "custom_utilities/constitutive_model_utilities.hpp"
#include "custom_utilities/properties_layout.hpp"

namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

SmallStrain3DLaw::SmallStrain3DLaw()
    : Constitutive3DLaw()
{
  mpModel = nullptr;
}

//******************************CONSTRUCTOR WITH THE MODEL****************************
//************************************************************************************

SmallStrain3DLaw::SmallStrain3DLaw(ModelTypePointer pModel) : Constitutive3DLaw()
{
  KRATOS_TRY

  //model
  mpModel = pModel->Clone();

  KRATOS_CATCH(" ")
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

SmallStrain3DLaw::SmallStrain3DLaw(const SmallStrain3DLaw &rOther)
    : Constitutive3DLaw(rOther)
{
  mpModel = rOther.mpModel->Clone();
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

SmallStrain3DLaw &SmallStrain3DLaw::operator=(const SmallStrain3DLaw &rOther)
{
  Constitutive3DLaw::operator=(rOther);
  mpModel = rOther.mpModel->Clone();
  return *this;
}

//********************************CLONE***********************************************
//************************************************************************************

ConstitutiveLaw::Pointer SmallStrain3DLaw::Clone() const
{
  return Kratos::make_shared<SmallStrain3DLaw>(*this);
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

SmallStrain3DLaw::~SmallStrain3DLaw()
{
}

//*******************************OPERATIONS FROM BASE CLASS***************************
//************************************************************************************
//*************************** SET VALUE: VECTOR **************************************
//************************************************************************************

void SmallStrain3DLaw::SetValue(const Variable<Vector> &rVariable, const Vector &rValue,
                                const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  mpModel->SetValue(rVariable, rValue, rCurrentProcessInfo);

  KRATOS_CATCH(" ")
}


//************************************************************************************
//************************************************************************************

void SmallStrain3DLaw::SetValue(const Variable<Matrix> &rVariable, const Matrix &rValue,
                                const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  mpModel->SetValue(rVariable, rValue, rCurrentProcessInfo);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void SmallStrain3DLaw::SetValue(const Variable<double> &rVariable, const double &rValue,
                                const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  mpModel->SetValue(rVariable, rValue, rCurrentProcessInfo);

  KRATOS_CATCH(" ")
}

//***********************GET VALUE: DOUBLE - VECTOR - MATRIX**************************
//************************************************************************************

double &SmallStrain3DLaw::GetValue(const Variable<double> &rVariable, double &rValue)
{
  KRATOS_TRY

  rValue = mpModel->GetValue(rVariable, rValue);

  return rValue;

  KRATOS_CATCH(" ")
}

Matrix &SmallStrain3DLaw::GetValue(const Variable<Matrix> &rVariable, Matrix &rValue)
{
  KRATOS_TRY

  rValue = Constitutive3DLaw::GetValue(rVariable, rValue);

  return rValue;

  KRATOS_CATCH(" ")
}

//************* COMPUTING  METHODS
//************************************************************************************
//************************************************************************************

void SmallStrain3DLaw::InitializeMaterial(const Properties &rProperties,
                                          const GeometryType &rElementGeometry,
                                          const Vector &rShapeFunctionsValues)
{
  KRATOS_TRY

  ConstitutiveLaw::InitializeMaterial(rProperties, rElementGeometry, rShapeFunctionsValues);

  mpModel->InitializeMaterial(rProperties);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void SmallStrain3DLaw::InitializeModelData(Parameters &rValues, ModelDataType &rModelValues)
{
  KRATOS_TRY

  if (rValues.GetMaterialProperties().Has(PROPERTIES_LAYOUT))
  {
    PropertiesLayout::Pointer pPropertiesLayout = rValues.GetMaterialProperties()[PROPERTIES_LAYOUT].Clone();
    pPropertiesLayout->Configure(rValues.GetMaterialProperties(), rValues.GetElementGeometry(), rValues.GetShapeFunctionsValues());
    rModelValues.SetPropertiesLayout(pPropertiesLayout);
  }

  rModelValues.SetOptions(rValues.GetOptions());
  rModelValues.SetProperties(rValues.GetMaterialProperties());
  rModelValues.SetProcessInfo(rValues.GetProcessInfo());
  rModelValues.SetVoigtSize(this->GetStrainSize());
  rModelValues.SetVoigtIndexTensor(this->GetVoigtIndexTensor());

  Vector &rStrainVector = rValues.GetStrainVector();
  MatrixType &rStrainMatrix = rModelValues.rStrainMatrix();
  rStrainMatrix = ConstitutiveModelUtilities::StrainVectorToTensor(rStrainVector, rStrainMatrix);

  if (rValues.GetOptions().Is(ConstitutiveLaw::FINALIZE_MATERIAL_RESPONSE))
    rModelValues.State.Set(ConstitutiveModelData::UPDATE_INTERNAL_VARIABLES);

  //initialize model
  mpModel->InitializeModel(rModelValues);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void SmallStrain3DLaw::FinalizeModelData(Parameters &rValues, ModelDataType &rModelValues)
{
  KRATOS_TRY

  //Finalize Material response
  if (rValues.GetOptions().Is(ConstitutiveLaw::FINALIZE_MATERIAL_RESPONSE))
  {

    //finalize model (update total strain measure)
    mpModel->FinalizeModel(rModelValues);
  }

  KRATOS_CATCH(" ")
}

//*****************************MATERIAL RESPONSES*************************************
//************************************************************************************

void SmallStrain3DLaw::CalculateMaterialResponseKirchhoff(Parameters &rValues)
{
  KRATOS_TRY

  ModelDataType ModelValues;

  this->CalculateMaterialResponseKirchhoff(rValues, ModelValues);

  KRATOS_CATCH(" ")
}

void SmallStrain3DLaw::CalculateMaterialResponseKirchhoff(Parameters &rValues, ModelDataType &rModelValues)
{
  KRATOS_TRY

  //0.- Check if the constitutive parameters are passed correctly to the law calculation
  //CheckParameters(rValues);

  const Flags &rOptions = rValues.GetOptions();

  //1.- Initialize hyperelastic model parameters
  LawDataType &rVariables = rModelValues.rConstitutiveLawData();
  rVariables.StressMeasure = ConstitutiveModelData::StressMeasureType::StressMeasure_Kirchhoff; //set required stress measure

  this->InitializeModelData(rValues, rModelValues);

  //2.-Calculate domain variables (Temperature, Pressure, Size) and calculate material parameters
  this->CalculateDomainVariables(rValues, rModelValues);

  //3.-Calculate material parameters and properties
  mpModel->CalculateMaterialParameters(rModelValues);

  //4.-Calculate Total kirchhoff stress and  Constitutive Matrix related to Total Kirchhoff stress
  if (rOptions.Is(ConstitutiveLaw::COMPUTE_STRESS) && rOptions.Is(ConstitutiveLaw::COMPUTE_CONSTITUTIVE_TENSOR))
  {

    Vector &rStressVector = rValues.GetStressVector();
    Matrix &rConstitutiveMatrix = rValues.GetConstitutiveMatrix();

    this->CalculateStressVectorAndConstitutiveMatrix(rModelValues, rStressVector, rConstitutiveMatrix);
  }
  else
  {

    //5.-Calculate Total Kirchhoff stress
    if (rOptions.Is(ConstitutiveLaw::COMPUTE_STRESS))
    {
      Vector &rStressVector = rValues.GetStressVector();
      this->CalculateStressVector(rModelValues, rStressVector);
    }
    //6.-Calculate Constitutive Matrix related to Total Kirchhoff stress
    else if (rOptions.Is(ConstitutiveLaw::COMPUTE_CONSTITUTIVE_TENSOR))
    {
      Matrix &rConstitutiveMatrix = rValues.GetConstitutiveMatrix();
      this->CalculateConstitutiveMatrix(rModelValues, rConstitutiveMatrix);
    }
    //7.-Calculate Requested Internal Variable
    else
    {
      this->CalculateInternalVariables(rModelValues);
    }
  }

  //8.- Finalize hyperelastic model parameters
  this->FinalizeModelData(rValues, rModelValues);

  // std::cout<<" ConstitutiveMatrix "<<rValues.GetConstitutiveMatrix()<<std::endl;
  // std::cout<<" StrainVector "<<rValues.GetStrainVector()<<std::endl;
  // std::cout<<" StressVector "<<rValues.GetStressVector()<<std::endl;

  //-----------------------------//
  // const Properties& rProperties  = rValues.GetMaterialProperties();

  // Vector& rStrainVector                  = rValues.GetStrainVector();
  // Vector& rStressVector                  = rValues.GetStressVector();

  // // Calculate total Kirchhoff stress

  // if( Options.Is( ConstitutiveLaw::COMPUTE_STRESS ) ){

  //   Matrix& rConstitutiveMatrix = rValues.GetConstitutiveMatrix();

  //   this->CalculateConstitutiveMatrix( rConstitutiveMatrix, rProperties);

  //   this->CalculateStress( rStrainVector, rConstitutiveMatrix, rStressVector );

  // }
  // else if( Options.Is( ConstitutiveLaw::COMPUTE_CONSTITUTIVE_TENSOR ) ){

  //   Matrix& rConstitutiveMatrix  = rValues.GetConstitutiveMatrix();
  //   this->CalculateConstitutiveMatrix(rConstitutiveMatrix, rProperties);

  // }
  //-----------------------------//

  KRATOS_CATCH(" ")
}

//*******************************COMPUTE STRESS VECTOR********************************
//************************************************************************************

void SmallStrain3DLaw::CalculateStressVector(ModelDataType &rModelValues, Vector &rStressVector)
{
  KRATOS_TRY

  MatrixType StressMatrix;
  noalias(StressMatrix) = ZeroMatrix(3,3);

  const Flags &rOptions = rModelValues.GetOptions();

  if (rOptions.IsNot(ConstitutiveLaw::ISOCHORIC_TENSOR_ONLY) && rOptions.IsNot(ConstitutiveLaw::VOLUMETRIC_TENSOR_ONLY))
  {
    mpModel->CalculateStressTensor(rModelValues, StressMatrix);
  }
  else if (rOptions.Is(ConstitutiveLaw::ISOCHORIC_TENSOR_ONLY))
  {
    mpModel->CalculateIsochoricStressTensor(rModelValues, StressMatrix);
  }
  else if (rOptions.Is(ConstitutiveLaw::VOLUMETRIC_TENSOR_ONLY))
  {
    mpModel->CalculateVolumetricStressTensor(rModelValues, StressMatrix);
  }
  else
  {
    mpModel->CalculateStressTensor(rModelValues, StressMatrix);
  }

  rStressVector = ConstitutiveModelUtilities::StressTensorToVector(StressMatrix, rStressVector);

  KRATOS_CATCH(" ")
}

//***********************COMPUTE ALGORITHMIC CONSTITUTIVE MATRIX**********************
//************************************************************************************

void SmallStrain3DLaw::CalculateConstitutiveMatrix(ModelDataType &rModelValues, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  const Flags &rOptions = rModelValues.GetOptions();

  //Calculate ConstitutiveMatrix
  if (rOptions.IsNot(ConstitutiveLaw::ISOCHORIC_TENSOR_ONLY) && rOptions.IsNot(ConstitutiveLaw::VOLUMETRIC_TENSOR_ONLY))
  {

    mpModel->CalculateConstitutiveTensor(rModelValues, rConstitutiveMatrix);
  }
  if (rOptions.Is(ConstitutiveLaw::ISOCHORIC_TENSOR_ONLY))
  {

    mpModel->CalculateIsochoricConstitutiveTensor(rModelValues, rConstitutiveMatrix);
  }
  else if (rOptions.Is(ConstitutiveLaw::VOLUMETRIC_TENSOR_ONLY))
  {

    mpModel->CalculateVolumetricConstitutiveTensor(rModelValues, rConstitutiveMatrix);
  }
  else
  {

    mpModel->CalculateConstitutiveTensor(rModelValues, rConstitutiveMatrix);
  }

  KRATOS_CATCH(" ")
}

//******************COMPUTE STRESS AND ALGORITHMIC CONSTITUTIVE MATRIX****************
//************************************************************************************

void SmallStrain3DLaw::CalculateStressVectorAndConstitutiveMatrix(ModelDataType &rModelValues, Vector &rStressVector, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  MatrixType StressMatrix;
  noalias(StressMatrix) = ZeroMatrix(3,3);

  const Flags &rOptions = rModelValues.GetOptions();

  //Calculate Stress and ConstitutiveMatrix
  if (rOptions.IsNot(ConstitutiveLaw::ISOCHORIC_TENSOR_ONLY) && rOptions.IsNot(ConstitutiveLaw::VOLUMETRIC_TENSOR_ONLY))
  {
    mpModel->CalculateStressAndConstitutiveTensors(rModelValues, StressMatrix, rConstitutiveMatrix);
  }
  else if (rOptions.Is(ConstitutiveLaw::ISOCHORIC_TENSOR_ONLY))
  {
    mpModel->CalculateIsochoricStressAndConstitutiveTensors(rModelValues, StressMatrix, rConstitutiveMatrix);
  }
  else if (rOptions.Is(ConstitutiveLaw::VOLUMETRIC_TENSOR_ONLY))
  {
    mpModel->CalculateVolumetricStressAndConstitutiveTensors(rModelValues, StressMatrix, rConstitutiveMatrix);
  }
  else
  {
    mpModel->CalculateStressAndConstitutiveTensors(rModelValues, StressMatrix, rConstitutiveMatrix);
  }

  rStressVector = ConstitutiveModelUtilities::StressTensorToVector(StressMatrix, rStressVector);

  KRATOS_CATCH(" ")
}

//***********************COMPUTE TOTAL STRESS ****************************************
//************************************************************************************

void SmallStrain3DLaw::CalculateStress(const Vector &rStrainVector,
                                       const Matrix &rConstitutiveMatrix,
                                       Vector &rStressVector)
{
  KRATOS_TRY

  // Cauchy StressVector
  noalias(rStressVector) = prod(rConstitutiveMatrix, rStrainVector);

  KRATOS_CATCH(" ")
}

//***********************COMPUTE ALGORITHMIC CONSTITUTIVE MATRIX**********************
//************************************************************************************

void SmallStrain3DLaw::CalculateConstitutiveMatrix(Matrix &rConstitutiveMatrix,
                                                   const Properties &rProperties)
{
  KRATOS_TRY

  // Lame constants
  const double &rYoungModulus = rProperties[YOUNG_MODULUS];
  const double &rPoissonCoefficient = rProperties[POISSON_RATIO];

  // 3D linear elastic constitutive matrix
  rConstitutiveMatrix(0, 0) = (rYoungModulus * (1.0 - rPoissonCoefficient) / ((1.0 + rPoissonCoefficient) * (1.0 - 2.0 * rPoissonCoefficient)));
  rConstitutiveMatrix(1, 1) = rConstitutiveMatrix(0, 0);
  rConstitutiveMatrix(2, 2) = rConstitutiveMatrix(0, 0);

  rConstitutiveMatrix(3, 3) = rConstitutiveMatrix(0, 0) * (1.0 - 2.0 * rPoissonCoefficient) / (2.0 * (1.0 - rPoissonCoefficient));
  rConstitutiveMatrix(4, 4) = rConstitutiveMatrix(3, 3);
  rConstitutiveMatrix(5, 5) = rConstitutiveMatrix(3, 3);

  rConstitutiveMatrix(0, 1) = rConstitutiveMatrix(0, 0) * rPoissonCoefficient / (1.0 - rPoissonCoefficient);
  rConstitutiveMatrix(1, 0) = rConstitutiveMatrix(0, 1);

  rConstitutiveMatrix(0, 2) = rConstitutiveMatrix(0, 1);
  rConstitutiveMatrix(2, 0) = rConstitutiveMatrix(0, 1);

  rConstitutiveMatrix(1, 2) = rConstitutiveMatrix(0, 1);
  rConstitutiveMatrix(2, 1) = rConstitutiveMatrix(0, 1);

  //initialize to zero other values
  for (unsigned int i = 0; i < 3; i++)
  {
    for (unsigned int j = 3; i < 6; i++)
    {
      rConstitutiveMatrix(i, j) = 0;
      rConstitutiveMatrix(j, i) = 0;
    }
  }

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void SmallStrain3DLaw::CalculateInternalVariables(ModelDataType &rModelValues)
{
  KRATOS_TRY

  mpModel->CalculateInternalVariables(rModelValues);

  KRATOS_CATCH(" ")
}

//*************************CONSTITUTIVE LAW GENERAL FEATURES *************************
//************************************************************************************

void SmallStrain3DLaw::GetLawFeatures(Features &rFeatures)
{
  KRATOS_TRY

  //Set the type of law
  rFeatures.mOptions.Set(THREE_DIMENSIONAL_LAW);
  rFeatures.mOptions.Set(INFINITESIMAL_STRAINS);
  rFeatures.mOptions.Set(ISOTROPIC);

  //Get model features
  GetModelFeatures(rFeatures);

  //Set strain measure required by the consitutive law
  //rFeatures.mStrainMeasures.push_back(StrainMeasure_Infinitesimal);
  //rFeatures.mStrainMeasures.push_back(StrainMeasure_Deformation_Gradient);

  //Set the strain size
  rFeatures.mStrainSize = GetStrainSize();

  //Set the spacedimension
  rFeatures.mSpaceDimension = WorkingSpaceDimension();

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void SmallStrain3DLaw::GetModelFeatures(Features &rFeatures)
{
  KRATOS_TRY

  //Get model variables and set law characteristics
  if (mpModel != NULL)
  {

    std::vector<Variable<double>*> ScalarVariables;
    std::vector<Variable<array_1d<double, 3>>*> ComponentVariables;

    mpModel->GetDomainVariablesList(ScalarVariables, ComponentVariables);
    for (auto &cv_it : ComponentVariables)
    {
      if (*cv_it == DISPLACEMENT)
      {
        for (auto &sv_it : ScalarVariables)
        {
          if (*sv_it == PRESSURE)
            rFeatures.mOptions.Set(U_P_LAW);
        }
      }
      // if( *cv_it == VELOCITY ){
      //   for(auto sv_it : ScalarVariables)
      //     {
      // 	if( *sv_it == PRESSURE )
      // 	  rFeatures.mOptions.Set( V_P_LAW );
      //     }
      // }
    }

    std::vector<ConstitutiveModelData::StrainMeasureType> StrainMeasures;
    mpModel->GetStrainMeasures(StrainMeasures);
    for (auto& measure: StrainMeasures)
    {
      rFeatures.mStrainMeasures.push_back(ConvertStrainMeasure(measure));
    }
    //...
  }

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

int SmallStrain3DLaw::Check(const Properties &rProperties,
                            const GeometryType &rElementGeometry,
                            const ProcessInfo &rCurrentProcessInfo) const
{
  KRATOS_TRY

  if (mpModel != NULL)
    mpModel->Check(rProperties, rCurrentProcessInfo);

  return 0;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void SmallStrain3DLaw::GetRequiredProperties(Properties &rProperties)
{
  KRATOS_TRY

  if (mpModel != NULL)
    mpModel->GetRequiredProperties(rProperties);

  KRATOS_CATCH(" ")
}


} // Namespace Kratos
