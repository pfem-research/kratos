//
//   Project Name:        KratosConstituiveModelsApplication $
//   Developed by:        $Developer:            JMCarbonell $
//   Maintained by:       $Maintainer:                   JMC $
//   Date:                $Date:                  April 2017 $
//
//

// System includes

// External includes

// Project includes
#include "custom_laws/large_strain_laws/large_strain_3D_law.hpp"

namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

LargeStrain3DLaw::LargeStrain3DLaw()
    : Constitutive3DLaw()
{
  KRATOS_TRY

  //member variables initialization
  mpModel = nullptr;

  mTotalDeformationDet = 1.0;

  MatrixType Identity = IdentityMatrix(3);
  noalias(mInverseTotalDeformationMatrix) = Identity;

  KRATOS_CATCH(" ")
}

//******************************CONSTRUCTOR WITH THE MODEL****************************
//************************************************************************************

LargeStrain3DLaw::LargeStrain3DLaw(ModelTypePointer pModel)
    : Constitutive3DLaw()
{
  KRATOS_TRY

  //model
  mpModel = pModel->Clone();

  //member variables initialization
  mTotalDeformationDet = 1.0;

  MatrixType Identity = IdentityMatrix(3);
  noalias(mInverseTotalDeformationMatrix) = Identity;

  KRATOS_CATCH(" ")
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

LargeStrain3DLaw::LargeStrain3DLaw(const LargeStrain3DLaw &rOther)
    : Constitutive3DLaw(rOther), mTotalDeformationDet(rOther.mTotalDeformationDet), mInverseTotalDeformationMatrix(rOther.mInverseTotalDeformationMatrix)
{
  mpModel = rOther.mpModel->Clone();
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

LargeStrain3DLaw &LargeStrain3DLaw::operator=(const LargeStrain3DLaw &rOther)
{
  Constitutive3DLaw::operator=(rOther);
  mpModel = rOther.mpModel->Clone();
  mTotalDeformationDet = rOther.mTotalDeformationDet;
  mInverseTotalDeformationMatrix = rOther.mInverseTotalDeformationMatrix;
  return *this;
}

//********************************CLONE***********************************************
//************************************************************************************

ConstitutiveLaw::Pointer LargeStrain3DLaw::Clone() const
{
  return Kratos::make_shared<LargeStrain3DLaw>(*this);
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

LargeStrain3DLaw::~LargeStrain3DLaw()
{
}

//*******************************OPERATIONS FROM BASE CLASS***************************
//************************************************************************************

//***********************HAS : DOUBLE - VECTOR - MATRIX*******************************
//************************************************************************************

bool LargeStrain3DLaw::Has(const Variable<double> &rVariable)
{
  KRATOS_TRY

  if (rVariable == DETERMINANT_F)
    return true;

  if (rVariable == INVERSE_DEFORMATION_GRADIENT)
    return true;

  if (rVariable == TOTAL_DEFORMATION_GRADIENT)
    return true;

  return mpModel->Has(rVariable);

  KRATOS_CATCH(" ")
}

//***********************SET VALUE: DOUBLE - VECTOR - MATRIX**************************
//************************************************************************************

void LargeStrain3DLaw::SetValue(const Variable<double> &rVariable, const double &rValue,
                                const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  mpModel->SetValue(rVariable, rValue, rCurrentProcessInfo);

  if (rVariable == DETERMINANT_F)
  {
    mTotalDeformationDet = rValue;
  }

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LargeStrain3DLaw::SetValue(const Variable<Vector> &rVariable, const Vector &rValue,
                                const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  mpModel->SetValue(rVariable, rValue, rCurrentProcessInfo);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LargeStrain3DLaw::SetValue(const Variable<Matrix> &rVariable, const Matrix &rValue,
                                const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  mpModel->SetValue(rVariable, rValue, rCurrentProcessInfo);

  if (rVariable == INVERSE_DEFORMATION_GRADIENT)
  {
    mInverseTotalDeformationMatrix = rValue;
  }
  else if (rVariable == TOTAL_DEFORMATION_GRADIENT)
  {
    double detF;
    Matrix TotalDeformationGradient(3,3);
    noalias(TotalDeformationGradient) = IdentityMatrix(3);

    for(unsigned int i=0; i<rValue.size1(); ++i)
      for(unsigned int j=0; j<rValue.size2(); ++j)
        TotalDeformationGradient(i,j) = rValue(i,j);

    ConstitutiveModelUtilities::InvertMatrix3(TotalDeformationGradient, mInverseTotalDeformationMatrix, detF);

    //mTotalDeformationDet = detF; //no improvement
  }

  KRATOS_CATCH(" ")
}

//***********************GET VALUE: DOUBLE - VECTOR - MATRIX**************************
//************************************************************************************

double &LargeStrain3DLaw::GetValue(const Variable<double> &rVariable, double &rValue)
{
  KRATOS_TRY

  if (rVariable == DETERMINANT_F)
  {
    rValue = mTotalDeformationDet;
  }
  else
  {
    rValue = mpModel->GetValue(rVariable, rValue);
  }

  return rValue;

  KRATOS_CATCH(" ")
}

array_1d<double, 3> &LargeStrain3DLaw::GetValue(const Variable<array_1d<double, 3> > &rVariable, array_1d<double, 3> &rValue)
{
  KRATOS_TRY

  rValue = mpModel->GetValue(rVariable, rValue);

  return rValue;

  KRATOS_CATCH(" ")
}

Vector &LargeStrain3DLaw::GetValue(const Variable<Vector> &rVariable, Vector &rValue)
{
  KRATOS_TRY

  rValue = mpModel->GetValue(rVariable, rValue);

  return rValue;

  KRATOS_CATCH(" ")
}

Matrix &LargeStrain3DLaw::GetValue(const Variable<Matrix> &rVariable, Matrix &rValue)
{
  KRATOS_TRY

  if (rVariable == INVERSE_DEFORMATION_GRADIENT)
  {
    rValue = mInverseTotalDeformationMatrix;
  }
  else if (rVariable == TOTAL_DEFORMATION_GRADIENT)
  {
    double detF;
    MatrixType TotalDeformationMatrix;
    ConstitutiveModelUtilities::InvertMatrix3(mInverseTotalDeformationMatrix, TotalDeformationMatrix, detF);
    rValue = TotalDeformationMatrix;
  }
  else
  {
    rValue = mpModel->GetValue(rVariable, rValue);
  }

  return rValue;

  KRATOS_CATCH(" ")
}
//************* STARTING - ENDING  METHODS
//************************************************************************************
//************************************************************************************

//************* COMPUTING  METHODS
//************************************************************************************
//************************************************************************************
void LargeStrain3DLaw::InitializeMaterial(const Properties &rProperties,
                                          const GeometryType &rElementGeometry,
                                          const Vector &rShapeFunctionsValues)
{
  KRATOS_TRY

  ConstitutiveLaw::InitializeMaterial(rProperties, rElementGeometry, rShapeFunctionsValues);

  //member variables initialization
  mTotalDeformationDet = 1.0;

  MatrixType Identity = IdentityMatrix(3);
  noalias(mInverseTotalDeformationMatrix) = Identity;

  mpModel->InitializeMaterial(rProperties);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LargeStrain3DLaw::InitializeModelData(Parameters &rValues, ModelDataType &rModelValues)
{
  KRATOS_TRY

  if (rValues.GetMaterialProperties().Has(PROPERTIES_LAYOUT))
  {
    PropertiesLayout::Pointer pPropertiesLayout = rValues.GetMaterialProperties()[PROPERTIES_LAYOUT].Clone();
    pPropertiesLayout->Configure(rValues.GetMaterialProperties(), rValues.GetElementGeometry(), rValues.GetShapeFunctionsValues());
    rModelValues.SetPropertiesLayout(pPropertiesLayout);
  }

  rModelValues.SetOptions(rValues.GetOptions());
  rModelValues.SetProperties(rValues.GetMaterialProperties());
  rModelValues.SetProcessInfo(rValues.GetProcessInfo());
  rModelValues.SetVoigtSize(this->GetStrainSize());
  rModelValues.SetVoigtIndexTensor(this->GetVoigtIndexTensor());

  LawDataType &rVariables = rModelValues.rConstitutiveLawData();

  //a.- Calculate incremental deformation gradient determinant
  rVariables.TotalDeformationDet = rValues.GetDeterminantF();
  rVariables.DeltaDeformationDet = rVariables.TotalDeformationDet / mTotalDeformationDet; //determinant incremental F

  //b.- Calculate incremental deformation gradient
  const MatrixType &rTotalDeformationMatrix = rValues.GetDeformationGradientF();

  rVariables.TotalDeformationMatrix = ConstitutiveModelUtilities::DeformationGradientTo3D(rTotalDeformationMatrix, rVariables.TotalDeformationMatrix);
  noalias(rVariables.DeltaDeformationMatrix) = prod(rVariables.TotalDeformationMatrix, mInverseTotalDeformationMatrix); //incremental F

  if (rValues.GetOptions().Is(ConstitutiveLaw::FINALIZE_MATERIAL_RESPONSE))
    rModelValues.State.Set(ConstitutiveModelData::UPDATE_INTERNAL_VARIABLES);

  //initialize model
  mpModel->InitializeModel(rModelValues);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LargeStrain3DLaw::FinalizeModelData(Parameters &rValues, ModelDataType &rModelValues)
{
  KRATOS_TRY

  //Finalize Material response
  if (rValues.GetOptions().Is(ConstitutiveLaw::FINALIZE_MATERIAL_RESPONSE))
  {

    const Matrix &rDeformationMatrix = rValues.GetDeformationGradientF();
    const double &rDeformationDet = rValues.GetDeterminantF();

    //update total deformation gradient
    MatrixType TotalDeformationMatrix;
    TotalDeformationMatrix = ConstitutiveModelUtilities::DeformationGradientTo3D(rDeformationMatrix, TotalDeformationMatrix);
    ConstitutiveModelUtilities::InvertMatrix3(TotalDeformationMatrix, mInverseTotalDeformationMatrix, mTotalDeformationDet);
    mTotalDeformationDet = rDeformationDet; //special treatment of the determinant

    //finalize model (update total strain measure)
    mpModel->FinalizeModel(rModelValues);

  }

  KRATOS_CATCH(" ")
}

//*****************************MATERIAL RESPONSES*************************************
//************************************************************************************

void LargeStrain3DLaw::CalculateMaterialResponsePK2(Parameters &rValues)
{
  KRATOS_TRY

  ModelDataType ModelValues;

  this->CalculateMaterialResponsePK2(rValues, ModelValues);

  KRATOS_CATCH(" ")
}

void LargeStrain3DLaw::CalculateMaterialResponsePK2(Parameters &rValues, ModelDataType &rModelValues)
{
  KRATOS_TRY

  //0.- Check if the constitutive parameters are passed correctly to the law calculation
  //CheckParameters(rValues);

  const Flags &rOptions = rValues.GetOptions();

  //1.- Initialize hyperelastic model parameters
  ModelDataType rModelValues;

  LawDataType &rVariables = rModelValues.rConstitutiveLawData();
  rVariables.StressMeasure = ConstitutiveModelData::StressMeasureType::StressMeasure_PK2; //required stress measure

  this->InitializeModelData(rValues, rModelValues);

  //2.-Calculate domain variables (Temperature, Pressure, Size) and calculate material parameters
  this->CalculateDomainVariables(rValues, rModelValues);

  //3.-Calculate material parameters and properties
  mpModel->CalculateMaterialParameters(rModelValues);

  //4.-Calculate Total PK2 stress and  Constitutive Matrix related to Total PK2 stress
  if (rOptions.Is(ConstitutiveLaw::COMPUTE_STRESS) && rOptions.Is(ConstitutiveLaw::COMPUTE_CONSTITUTIVE_TENSOR))
  {

    Vector &rStressVector = rValues.GetStressVector();
    Matrix &rConstitutiveMatrix = rValues.GetConstitutiveMatrix();

    this->CalculateStressVectorAndConstitutiveMatrix(rModelValues, rStressVector, rConstitutiveMatrix);
  }
  else
  {

    //5.-Calculate Total PK2 stress
    if (rOptions.Is(ConstitutiveLaw::COMPUTE_STRESS))
    {
      Vector &rStressVector = rValues.GetStressVector();
      this->CalculateStressVector(rModelValues, rStressVector);
    }
    //6.-Calculate Constitutive Matrix related to Total PK2 stress
    else if (rOptions.Is(ConstitutiveLaw::COMPUTE_CONSTITUTIVE_TENSOR))
    {
      Matrix &rConstitutiveMatrix = rValues.GetConstitutiveMatrix();
      this->CalculateConstitutiveMatrix(rModelValues, rConstitutiveMatrix);
    }
    //7.-Calculate Requested Internal Variable
    else
    {
      this->CalculateInternalVariables(rModelValues);
    }
  }

  // if(rOptions.Is(ConstitutiveLaw::COMPUTE_STRAIN_ENERGY))
  // {

  // }

  //8.- Finalize hyperelastic model parameters
  this->FinalizeModelData(rValues, rModelValues);

  // std::cout<<" StrainVector "<<rValues.GetStrainVector()<<std::endl;
  // std::cout<<" StressVector "<<rValues.GetStressVector()<<std::endl;
  // std::cout<<" ConstitutiveMatrix "<<rValues.GetConstitutiveMatrix()<<std::endl;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LargeStrain3DLaw::CalculateMaterialResponseKirchhoff(Parameters &rValues)
{
  KRATOS_TRY

  ModelDataType ModelValues;

  this->CalculateMaterialResponseKirchhoff(rValues, ModelValues);

  KRATOS_CATCH(" ")
}

void LargeStrain3DLaw::CalculateMaterialResponseKirchhoff(Parameters &rValues, ModelDataType &rModelValues)
{
  KRATOS_TRY

  //0.- Check if the constitutive parameters are passed correctly to the law calculation
  //CheckParameters(rValues);

  const Flags &rOptions = rValues.GetOptions();

  //1.- Initialize hyperelastic model parameters
  LawDataType &rVariables = rModelValues.rConstitutiveLawData();
  rVariables.StressMeasure = ConstitutiveModelData::StressMeasureType::StressMeasure_Kirchhoff; //set required stress measure

  this->InitializeModelData(rValues, rModelValues);

  //2.-Calculate domain variables (Temperature, Pressure, Size) and calculate material parameters
  this->CalculateDomainVariables(rValues, rModelValues);

  //3.-Calculate material parameters and properties
  mpModel->CalculateMaterialParameters(rModelValues);

  //4.-Calculate Total kirchhoff stress and  Constitutive Matrix related to Total Kirchhoff stress
  if (rOptions.Is(ConstitutiveLaw::COMPUTE_STRESS) && rOptions.Is(ConstitutiveLaw::COMPUTE_CONSTITUTIVE_TENSOR))
  {

    Vector &rStressVector = rValues.GetStressVector();
    Matrix &rConstitutiveMatrix = rValues.GetConstitutiveMatrix();

    this->CalculateStressVectorAndConstitutiveMatrix(rModelValues, rStressVector, rConstitutiveMatrix);
  }
  else
  {

    //5.-Calculate Total Kirchhoff stress
    if (rOptions.Is(ConstitutiveLaw::COMPUTE_STRESS))
    {
      Vector &rStressVector = rValues.GetStressVector();
      this->CalculateStressVector(rModelValues, rStressVector);
    }
    //6.-Calculate Constitutive Matrix related to Total Kirchhoff stress
    else if (rOptions.Is(ConstitutiveLaw::COMPUTE_CONSTITUTIVE_TENSOR))
    {
      Matrix &rConstitutiveMatrix = rValues.GetConstitutiveMatrix();
      this->CalculateConstitutiveMatrix(rModelValues, rConstitutiveMatrix);
    }
    //7.-Calculate Requested Internal Variable
    else
    {
      this->CalculateInternalVariables(rModelValues);
    }
  }

  // if(rOptions.Is(ConstitutiveLaw::COMPUTE_STRAIN_ENERGY))
  // {

  // }

  //8.- Finalize hyperelastic model parameters
  this->FinalizeModelData(rValues, rModelValues);

  // std::cout<<" StrainVector "<<rValues.GetStrainVector()<<std::endl;
  // std::cout<<" StressVector K "<<rValues.GetStressVector()<<std::endl;
  // std::cout<<" ConstitutiveMatrix Law "<<rValues.GetConstitutiveMatrix()<<std::endl;

  KRATOS_CATCH(" ")
}

//*******************************COMPUTE STRESS VECTOR********************************
//************************************************************************************

void LargeStrain3DLaw::CalculateStressVector(ModelDataType &rModelValues, Vector &rStressVector)
{
  KRATOS_TRY

  MatrixType StressMatrix;
  noalias(StressMatrix) = ZeroMatrix(3,3);

  const Flags &rOptions = rModelValues.GetOptions();

  if (rOptions.IsNot(ConstitutiveLaw::ISOCHORIC_TENSOR_ONLY) && rOptions.IsNot(ConstitutiveLaw::VOLUMETRIC_TENSOR_ONLY))
  {
    mpModel->CalculateStressTensor(rModelValues, StressMatrix);
  }
  else if (rOptions.Is(ConstitutiveLaw::ISOCHORIC_TENSOR_ONLY))
  {
    mpModel->CalculateIsochoricStressTensor(rModelValues, StressMatrix);
  }
  else if (rOptions.Is(ConstitutiveLaw::VOLUMETRIC_TENSOR_ONLY))
  {
    mpModel->CalculateVolumetricStressTensor(rModelValues, StressMatrix);
  }
  else
  {
    mpModel->CalculateStressTensor(rModelValues, StressMatrix);
  }

  rStressVector = ConstitutiveModelUtilities::StressTensorToVector(StressMatrix, rStressVector);

  KRATOS_CATCH(" ")
}

//***********************COMPUTE ALGORITHMIC CONSTITUTIVE MATRIX**********************
//************************************************************************************

void LargeStrain3DLaw::CalculateConstitutiveMatrix(ModelDataType &rModelValues, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  const Flags &rOptions = rModelValues.GetOptions();

  //Calculate ConstitutiveMatrix
  if (rOptions.IsNot(ConstitutiveLaw::ISOCHORIC_TENSOR_ONLY) && rOptions.IsNot(ConstitutiveLaw::VOLUMETRIC_TENSOR_ONLY))
  {
    mpModel->CalculateConstitutiveTensor(rModelValues, rConstitutiveMatrix);
  }
  if (rOptions.Is(ConstitutiveLaw::ISOCHORIC_TENSOR_ONLY))
  {
    mpModel->CalculateIsochoricConstitutiveTensor(rModelValues, rConstitutiveMatrix);
  }
  else if (rOptions.Is(ConstitutiveLaw::VOLUMETRIC_TENSOR_ONLY))
  {
    mpModel->CalculateVolumetricConstitutiveTensor(rModelValues, rConstitutiveMatrix);
  }
  else
  {
    mpModel->CalculateConstitutiveTensor(rModelValues, rConstitutiveMatrix);
  }

  KRATOS_CATCH(" ")
}

//******************COMPUTE STRESS AND ALGORITHMIC CONSTITUTIVE MATRIX****************
//************************************************************************************

void LargeStrain3DLaw::CalculateStressVectorAndConstitutiveMatrix(ModelDataType &rModelValues, Vector &rStressVector, Matrix &rConstitutiveMatrix)
{
  KRATOS_TRY

  MatrixType StressMatrix;
  noalias(StressMatrix) = ZeroMatrix(3,3);

  const Flags &rOptions = rModelValues.GetOptions();

  //Calculate Stress and ConstitutiveMatrix
  if (rOptions.IsNot(ConstitutiveLaw::ISOCHORIC_TENSOR_ONLY) && rOptions.IsNot(ConstitutiveLaw::VOLUMETRIC_TENSOR_ONLY))
  {

    mpModel->CalculateStressAndConstitutiveTensors(rModelValues, StressMatrix, rConstitutiveMatrix);
  }
  else if (rOptions.Is(ConstitutiveLaw::ISOCHORIC_TENSOR_ONLY))
  {

    mpModel->CalculateIsochoricStressAndConstitutiveTensors(rModelValues, StressMatrix, rConstitutiveMatrix);
  }
  else if (rOptions.Is(ConstitutiveLaw::VOLUMETRIC_TENSOR_ONLY))
  {

    mpModel->CalculateVolumetricStressAndConstitutiveTensors(rModelValues, StressMatrix, rConstitutiveMatrix);
  }
  else
  {

    mpModel->CalculateStressAndConstitutiveTensors(rModelValues, StressMatrix, rConstitutiveMatrix);
  }

  rStressVector = ConstitutiveModelUtilities::StressTensorToVector(StressMatrix, rStressVector);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LargeStrain3DLaw::CalculateInternalVariables(ModelDataType &rModelValues)
{
  KRATOS_TRY

  mpModel->CalculateInternalVariables(rModelValues);

  KRATOS_CATCH(" ")
}

//*************************CONSTITUTIVE LAW GENERAL FEATURES *************************
//************************************************************************************

void LargeStrain3DLaw::GetLawFeatures(Features &rFeatures)
{
  KRATOS_TRY

  //Set the type of law
  rFeatures.mOptions.Set(THREE_DIMENSIONAL_LAW);
  rFeatures.mOptions.Set(FINITE_STRAINS);
  rFeatures.mOptions.Set(ISOTROPIC);

  //Get model features
  GetModelFeatures(rFeatures);

  //Set strain measure required by the consitutive law
  //rFeatures.mStrainMeasures.push_back(StrainMeasure_Deformation_Gradient);

  //Set the strain size
  rFeatures.mStrainSize = GetStrainSize();

  //Set the spacedimension
  rFeatures.mSpaceDimension = WorkingSpaceDimension();

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LargeStrain3DLaw::GetModelFeatures(Features &rFeatures)
{
  KRATOS_TRY

  //Get model variables and set law characteristics
  if (mpModel != NULL)
  {

    std::vector<Variable<double>*> ScalarVariables;
    std::vector<Variable<array_1d<double, 3>>*> ComponentVariables;

    mpModel->GetDomainVariablesList(ScalarVariables, ComponentVariables);

    for (auto &cv_it : ComponentVariables)
    {
      if (*cv_it == DISPLACEMENT)
      {
        for (auto &sv_it : ScalarVariables)
        {
          if (*sv_it == PRESSURE)
            rFeatures.mOptions.Set(U_P_LAW);
        }
      }
      // if( *cv_it == VELOCITY ){
      //   for(auto sv_it : ScalarVariables)
      //     {
      // 	if( *sv_it == PRESSURE )
      // 	  rFeatures.mOptions.Set( V_P_LAW );
      //     }
      // }
    }

    std::vector<ConstitutiveModelData::StrainMeasureType> StrainMeasures;
    mpModel->GetStrainMeasures(StrainMeasures);
    for (auto& measure: StrainMeasures)
    {
      rFeatures.mStrainMeasures.push_back(ConvertStrainMeasure(measure));
    }

    //...
  }

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

int LargeStrain3DLaw::Check(const Properties &rProperties,
                            const GeometryType &rElementGeometry,
                            const ProcessInfo &rCurrentProcessInfo) const
{
  KRATOS_TRY

  if (mpModel != NULL)
    mpModel->Check(rProperties, rCurrentProcessInfo);

  return 0;

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void LargeStrain3DLaw::GetRequiredProperties(Properties &rProperties)
{
  KRATOS_TRY

  if (mpModel != NULL)
    mpModel->GetRequiredProperties(rProperties);

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

 void LargeStrain3DLaw::CalculateDomainVariables(Parameters &rValues, ModelDataType &rModelValues)
{
  KRATOS_TRY

  Constitutive3DLaw::CalculateDomainVariables( rValues, rModelValues);

  LawDataType &rVariables = rModelValues.rConstitutiveLawData();

  rVariables.SuctionNew *= rVariables.TotalDeformationDet;
  rVariables.SuctionOld *= mTotalDeformationDet;

  KRATOS_CATCH("")
 }

} // Namespace Kratos
