//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:             LMonforte $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                August 2016 $
//
//

#include "custom_friction/coulomb_adhesion_friction_law.hpp"

namespace Kratos
{

/**
    * Methods
    */
double CoulombAdhesionFrictionLaw::EvaluateHardening(const double &rNormalStress, const double &rPlasticSlip, FrictionLawVariables &rTangentVariables)
{
    return 0.0;
}

/**
    * Methods
    */
double CoulombAdhesionFrictionLaw::EvaluateContactYield(const double &rTangentStress, const double &rNormalStress, const double &rPlasticSlip, FrictionLawVariables &rTangentVariables)
{
    double YieldFunction = fabs(rTangentStress) - rTangentVariables.FrictionCoefficient * fabs(rNormalStress) - rTangentVariables.Adhesion;
    this->mFriction = rTangentVariables.FrictionCoefficient;
    return YieldFunction;
}

/**
    * Methods
    */
void CoulombAdhesionFrictionLaw::EvaluateYieldDerivativeRespectStress(double &rdF_dt, double &rdF_dp, const double &rTangentStress, const double &rNormalStress, const double &Gamma, FrictionLawVariables &rTangentVariables)
{

    rdF_dt = 1.0;
    rdF_dp = -rTangentVariables.FrictionCoefficient;
}

} // namespace Kratos
