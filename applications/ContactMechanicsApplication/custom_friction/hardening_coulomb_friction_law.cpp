//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:             LMonforte $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                August 2016 $
//
//

#include "custom_friction/hardening_coulomb_friction_law.hpp"

#include "contact_mechanics_application_variables.h"

namespace Kratos
{

/**
   * Methods
   */
double HardeningCoulombFrictionLaw::EvaluateHardening(const double &rNormalStress, const double &rPlasticSlip, FrictionLawVariables &rTangentVariables)
{
   KRATOS_TRY

   double H = -rTangentVariables.FrictionCoefficient * rTangentVariables.Alpha * (1.0-rTangentVariables.Beta) * std::exp(-rTangentVariables.Alpha * rPlasticSlip) * fabs(rNormalStress);
   return H;

   KRATOS_CATCH("")
}

/**
   * Methods
   */
double HardeningCoulombFrictionLaw::EvaluateContactYield(const double &rTangentStress, const double &rNormalStress, const double &rPlasticSlip, FrictionLawVariables &rTangentVariables)
{
   KRATOS_TRY

   double aux = rTangentVariables.Beta + (1.0-rTangentVariables.Beta)*std::exp(-rTangentVariables.Alpha * rPlasticSlip);
   double YieldFunction = fabs(rTangentStress) - rTangentVariables.FrictionCoefficient * aux * fabs(rNormalStress);

   this->mFriction = aux*rTangentVariables.FrictionCoefficient;
   return YieldFunction;

   KRATOS_CATCH("")
}

/**
   * Methods
   */
void HardeningCoulombFrictionLaw::EvaluateYieldDerivativeRespectStress(double &rdF_dt, double &rdF_dp, const double &rTangentStress, const double &rNormalStress, const double &rPlasticSlip, FrictionLawVariables &rTangentVariables)
{
   KRATOS_TRY
   rdF_dt = 1.0;

   double aux = rTangentVariables.Beta + (1.0-rTangentVariables.Beta)*std::exp(-rTangentVariables.Alpha * rPlasticSlip);
   rdF_dp = -rTangentVariables.FrictionCoefficient * aux;

   KRATOS_CATCH("")
}

} // end namespace Kratos
