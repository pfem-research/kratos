//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:               October 2018 $
//
//

// System includes

// External includes

// Project includes
#include "custom_elements/translational_rigid_body_segregated_V_element.hpp"

#include "contact_mechanics_application_variables.h"

namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

TranslationalRigidBodySegregatedVElement::TranslationalRigidBodySegregatedVElement(IndexType NewId, GeometryType::Pointer pGeometry)
    : TranslationalRigidBodyElement(NewId, pGeometry)
{
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

TranslationalRigidBodySegregatedVElement::TranslationalRigidBodySegregatedVElement(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : TranslationalRigidBodyElement(NewId, pGeometry, pProperties)
{
  mStepVariable = VELOCITY_STEP;
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

TranslationalRigidBodySegregatedVElement::TranslationalRigidBodySegregatedVElement(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties, NodesContainerType::Pointer pNodes)
    : TranslationalRigidBodyElement(NewId, pGeometry, pProperties, pNodes)
{
  mStepVariable = VELOCITY_STEP;
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

TranslationalRigidBodySegregatedVElement::TranslationalRigidBodySegregatedVElement(TranslationalRigidBodySegregatedVElement const &rOther)
    : TranslationalRigidBodyElement(rOther), mStepVariable(rOther.mStepVariable)
{
}

//*********************************CREATE*********************************************
//************************************************************************************

Element::Pointer TranslationalRigidBodySegregatedVElement::Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const
{
  return Kratos::make_intrusive<TranslationalRigidBodySegregatedVElement>(NewId, GetGeometry().Create(ThisNodes), pProperties);
}

//*********************************CLONE**********************************************
//************************************************************************************

Element::Pointer TranslationalRigidBodySegregatedVElement::Clone(IndexType NewId, NodesArrayType const &ThisNodes) const
{

  TranslationalRigidBodySegregatedVElement NewElement(NewId, GetGeometry().Create(ThisNodes), pGetProperties(), mpNodes);

  NewElement.mInitialLocalQuaternion = this->mInitialLocalQuaternion;
  NewElement.SetData(this->GetData());
  NewElement.SetFlags(this->GetFlags());
  NewElement.mStepVariable = mStepVariable;

  return Kratos::make_intrusive<TranslationalRigidBodySegregatedVElement>(NewElement);
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

TranslationalRigidBodySegregatedVElement::~TranslationalRigidBodySegregatedVElement()
{
}

//************************************************************************************
//************************************************************************************

void TranslationalRigidBodySegregatedVElement::GetDofList(DofsVectorType &rElementalDofList, const ProcessInfo &rCurrentProcessInfo) const
{
  rElementalDofList.resize(0);

  switch (StepType(rCurrentProcessInfo[SOLVER_STEP]))
  {
  case VELOCITY_STEP:
  {
    const unsigned int number_of_nodes = GetGeometry().size();
    const unsigned int dimension = GetGeometry().WorkingSpaceDimension();

    for (unsigned int i = 0; i < number_of_nodes; i++)
    {
      rElementalDofList.push_back(GetGeometry()[i].pGetDof(VELOCITY_X));
      rElementalDofList.push_back(GetGeometry()[i].pGetDof(VELOCITY_Y));
      if (dimension == 3)
        rElementalDofList.push_back(GetGeometry()[i].pGetDof(VELOCITY_Z));
    }
    break;
  }
  case PRESSURE_STEP:
  {
    break;
  }
  default:
    KRATOS_ERROR << "Unexpected value for SOLVER_STEP index: " << rCurrentProcessInfo[SOLVER_STEP] << std::endl;
  }
}

//************************************************************************************
//************************************************************************************

void TranslationalRigidBodySegregatedVElement::EquationIdVector(EquationIdVectorType &rResult, const ProcessInfo &rCurrentProcessInfo) const
{

  switch (StepType(rCurrentProcessInfo[SOLVER_STEP]))
  {
  case VELOCITY_STEP:
  {
    const unsigned int number_of_nodes = GetGeometry().size();
    const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
    const SizeType dofs_size = this->GetDofsSize();

    if (rResult.size() != dofs_size)
      rResult.resize(dofs_size, false);

    for (unsigned int i = 0; i < number_of_nodes; i++)
    {
      int index = i * (dimension);
      rResult[index] = GetGeometry()[i].GetDof(VELOCITY_X).EquationId();
      rResult[index + 1] = GetGeometry()[i].GetDof(VELOCITY_Y).EquationId();
      if (dimension == 3)
        rResult[index + 2] = GetGeometry()[i].GetDof(VELOCITY_Z).EquationId();
    }
    break;
  }
  case PRESSURE_STEP:
  {
    const SizeType dofs_size = this->GetDofsSize();

    if (rResult.size() != dofs_size)
      rResult.resize(dofs_size, false);

    break;
  }
  default:
    KRATOS_ERROR << "Unexpected value for SOLVER_STEP index: " << rCurrentProcessInfo[SOLVER_STEP] << std::endl;
  }
}

//*********************************DISPLACEMENT***************************************
//************************************************************************************

void TranslationalRigidBodySegregatedVElement::GetValuesVector(Vector &rValues, int Step) const
{
  KRATOS_TRY

  switch (mStepVariable)
  {
  case VELOCITY_STEP:
  {
    TranslationalRigidBodyElement::GetValuesVector(rValues, Step);
    break;
  }
  case PRESSURE_STEP:
  {
    SizeType dofs_size = this->GetDofsSize();
    if (rValues.size() != dofs_size)
      rValues.resize(dofs_size, false);
    break;
  }
  default:
    KRATOS_ERROR << "Unexpected value for SOLVER_STEP index: " << mStepVariable << std::endl;
  }

  KRATOS_CATCH("")
}

//************************************VELOCITY****************************************
//************************************************************************************

//************************************************************************************
//************************************************************************************
void TranslationalRigidBodySegregatedVElement::GetFirstDerivativesVector(Vector &rValues, int Step) const
{
  KRATOS_TRY

  switch (mStepVariable)
  {
  case VELOCITY_STEP:
  {
    TranslationalRigidBodyElement::GetFirstDerivativesVector(rValues, Step);
    break;
  }
  case PRESSURE_STEP:
  {
    SizeType dofs_size = this->GetDofsSize();
    if (rValues.size() != dofs_size)
      rValues.resize(dofs_size, false);
    break;
  }
  default:
    KRATOS_ERROR << "Unexpected value for SOLVER_STEP index: " << mStepVariable << std::endl;
  }

  KRATOS_CATCH("")
}

//*********************************ACCELERATION***************************************
//************************************************************************************

void TranslationalRigidBodySegregatedVElement::GetSecondDerivativesVector(Vector &rValues, int Step) const
{
  KRATOS_TRY

  switch (mStepVariable)
  {
  case VELOCITY_STEP:
  {
    TranslationalRigidBodyElement::GetSecondDerivativesVector(rValues, Step);
    break;
  }
  case PRESSURE_STEP:
  {
    SizeType dofs_size = this->GetDofsSize();
    if (rValues.size() != dofs_size)
      rValues.resize(dofs_size, false);
    break;
  }
  default:
    KRATOS_ERROR << "Unexpected value for SOLVER_STEP index: " << mStepVariable << std::endl;
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void TranslationalRigidBodySegregatedVElement::Initialize(const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  TranslationalRigidBodyElement::Initialize(rCurrentProcessInfo);

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void TranslationalRigidBodySegregatedVElement::InitializeSolutionStep(const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  this->SetProcessInformation(rCurrentProcessInfo);

  switch (mStepVariable)
  {
  case VELOCITY_STEP:
  {
    TranslationalRigidBodyElement::InitializeSolutionStep(rCurrentProcessInfo);
    break;
  }
  case PRESSURE_STEP:
  {
    break;
  }
  default:
    KRATOS_ERROR << "Unexpected value for SOLVER_STEP index: " << rCurrentProcessInfo[SOLVER_STEP] << std::endl;
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void TranslationalRigidBodySegregatedVElement::InitializeNonLinearIteration(const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  this->SetProcessInformation(rCurrentProcessInfo);

  TranslationalRigidBodyElement::InitializeNonLinearIteration(rCurrentProcessInfo);

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void TranslationalRigidBodySegregatedVElement::FinalizeNonLinearIteration(const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  this->SetProcessInformation(rCurrentProcessInfo);

  switch (mStepVariable)
  {
  case VELOCITY_STEP:
  {
    TranslationalRigidBodyElement::FinalizeNonLinearIteration(rCurrentProcessInfo);
    break;
  }
  case PRESSURE_STEP:
  {
    break;
  }
  default:
    KRATOS_ERROR << "Unexpected value for SOLVER_STEP index: " << rCurrentProcessInfo[SOLVER_STEP] << std::endl;
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void TranslationalRigidBodySegregatedVElement::FinalizeSolutionStep(const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  this->SetProcessInformation(rCurrentProcessInfo);

  switch (mStepVariable)
  {
  case VELOCITY_STEP:
  {
    TranslationalRigidBodyElement::FinalizeSolutionStep(rCurrentProcessInfo);
    break;
  }
  case PRESSURE_STEP:
  {
    //set as VELOCITY STEP for gauss point calculations:
    mStepVariable = VELOCITY_STEP;
    break;
  }
  default:
    KRATOS_ERROR << "Unexpected value for SOLVER_STEP index: " << rCurrentProcessInfo[SOLVER_STEP] << std::endl;
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void TranslationalRigidBodySegregatedVElement::CalculateRightHandSide(VectorType &rRightHandSideVector,
                                                                    const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  //process information
  this->SetProcessInformation(rCurrentProcessInfo);

  TranslationalRigidBodyElement::CalculateRightHandSide(rRightHandSideVector, rCurrentProcessInfo);

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void TranslationalRigidBodySegregatedVElement::CalculateLeftHandSide(MatrixType &rLeftHandSideMatrix,
                                                                   const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  //process information
  this->SetProcessInformation(rCurrentProcessInfo);

  TranslationalRigidBodyElement::CalculateLeftHandSide(rLeftHandSideMatrix, rCurrentProcessInfo);

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void TranslationalRigidBodySegregatedVElement::CalculateLocalSystem(MatrixType &rLeftHandSideMatrix,
                                                                  VectorType &rRightHandSideVector,
                                                                  const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  //process information
  this->SetProcessInformation(rCurrentProcessInfo);

  TranslationalRigidBodyElement::CalculateLocalSystem(rLeftHandSideMatrix, rRightHandSideVector, rCurrentProcessInfo);

  //KRATOS_INFO("")<<mStepVariable<<" LHS:"<<rLeftHandSideMatrix<<" RHS:"<<rRightHandSideVector<<std::endl;

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void TranslationalRigidBodySegregatedVElement::CalculateSecondDerivativesContributions(MatrixType &rLeftHandSideMatrix,
                                                                                     VectorType &rRightHandSideVector,
                                                                                     const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  //process information
  this->SetProcessInformation(rCurrentProcessInfo);

  switch (mStepVariable)
  {
  case VELOCITY_STEP:
  {
    TranslationalRigidBodyElement::CalculateSecondDerivativesContributions(rLeftHandSideMatrix, rRightHandSideVector, rCurrentProcessInfo);
    break;
  }
  case PRESSURE_STEP:
  {
    break;
  }
  default:
    KRATOS_ERROR << "Unexpected value for SOLVER_STEP index: " << rCurrentProcessInfo[SOLVER_STEP] << std::endl;
  }

  //KRATOS_INFO("")<<mStepVariable<<" 2LHS:"<<rLeftHandSideMatrix<<" 2RHS:"<<rRightHandSideVector<<std::endl;

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void TranslationalRigidBodySegregatedVElement::CalculateSecondDerivativesLHS(MatrixType &rLeftHandSideMatrix,
                                                                           const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  //process information
  this->SetProcessInformation(rCurrentProcessInfo);

  switch (mStepVariable)
  {
  case VELOCITY_STEP:
  {
    TranslationalRigidBodyElement::CalculateSecondDerivativesLHS(rLeftHandSideMatrix, rCurrentProcessInfo);
    break;
  }
  case PRESSURE_STEP:
  {
    break;
  }
  default:
    KRATOS_ERROR << "Unexpected value for SOLVER_STEP index: " << rCurrentProcessInfo[SOLVER_STEP] << std::endl;
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void TranslationalRigidBodySegregatedVElement::CalculateSecondDerivativesRHS(VectorType &rRightHandSideVector,
                                                                           const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  //process information
  this->SetProcessInformation(rCurrentProcessInfo);

  switch (mStepVariable)
  {
  case VELOCITY_STEP:
  {
    TranslationalRigidBodyElement::CalculateSecondDerivativesRHS(rRightHandSideVector, rCurrentProcessInfo);
    break;
  }
  case PRESSURE_STEP:
  {
    break;
  }
  default:
    KRATOS_ERROR << "Unexpected value for SOLVER_STEP index: " << rCurrentProcessInfo[SOLVER_STEP] << std::endl;
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void TranslationalRigidBodySegregatedVElement::CalculateMassMatrix(MatrixType &rMassMatrix, const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  //process information
  this->SetProcessInformation(rCurrentProcessInfo);

  switch (mStepVariable)
  {
  case VELOCITY_STEP:
  {
    TranslationalRigidBodyElement::CalculateMassMatrix(rMassMatrix, rCurrentProcessInfo);
    break;
  }
  case PRESSURE_STEP:
  {
    break;
  }
  default:
    KRATOS_ERROR << "Unexpected value for SOLVER_STEP index: " << rCurrentProcessInfo[SOLVER_STEP] << std::endl;
  }

  //KRATOS_INFO("")<<mStepVariable<<" MassM:"<<rMassMatrix<<std::endl;

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void TranslationalRigidBodySegregatedVElement::GetTimeIntegrationParameters(BoundedVector<double, 3> &rLinear, BoundedVector<double, 3> &rAngular, const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  double DeltaTime = rCurrentProcessInfo[DELTA_TIME];
  rLinear[0] = 1.0;
  rLinear[1] = (rCurrentProcessInfo[NEWMARK_GAMMA] / (DeltaTime * rCurrentProcessInfo[NEWMARK_BETA]));
  rLinear[2] = 1.0;

  rAngular = rLinear;

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

TranslationalRigidBodySegregatedVElement::SizeType TranslationalRigidBodySegregatedVElement::GetDofsSize() const
{
  KRATOS_TRY

  SizeType size = 0;
  switch (mStepVariable)
  {
  case VELOCITY_STEP:
  {
    size = TranslationalRigidBodyElement::GetDofsSize();
    break;
  }
  case PRESSURE_STEP:
  {
    size = 0;
    break;
  }
  default:
    KRATOS_ERROR << "Unexpected value for SOLVER_STEP index: " << mStepVariable << std::endl;
  }
  return size;

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void TranslationalRigidBodySegregatedVElement::UpdateRigidBodyNodes(const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  this->SetProcessInformation(rCurrentProcessInfo);

  switch (mStepVariable)
  {
  case VELOCITY_STEP:
  {
    TranslationalRigidBodyElement::UpdateRigidBodyNodes(rCurrentProcessInfo);
    break;
  }
  case PRESSURE_STEP:
  {
    break;
  }
  default:
    KRATOS_ERROR << "Unexpected value for SOLVER_STEP index: " << mStepVariable << std::endl;
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void TranslationalRigidBodySegregatedVElement::SetProcessInformation(const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  mStepVariable = StepType(rCurrentProcessInfo[SOLVER_STEP]);

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

int TranslationalRigidBodySegregatedVElement::Check(const ProcessInfo &rCurrentProcessInfo) const
{
  KRATOS_TRY

  if (GetGeometry().size() != 1)
  {
    KRATOS_THROW_ERROR(std::invalid_argument, "This element works only with 1 noded geometry", "")
  }

  //verify that the variables are correctly initialized
  if (VELOCITY.Key() == 0)
    KRATOS_THROW_ERROR(std::invalid_argument, "VELOCITY has Key zero! (check if the application is correctly registered", "")
  if (DISPLACEMENT.Key() == 0)
    KRATOS_THROW_ERROR(std::invalid_argument, "DISPLACEMENT has Key zero! (check if the application is correctly registered", "")
  if (ACCELERATION.Key() == 0)
    KRATOS_THROW_ERROR(std::invalid_argument, "ACCELERATION has Key zero! (check if the application is correctly registered", "")
  if (DENSITY.Key() == 0)
    KRATOS_THROW_ERROR(std::invalid_argument, "DENSITY has Key zero! (check if the application is correctly registered", "")
  if (NODAL_MASS.Key() == 0)
    KRATOS_THROW_ERROR(std::invalid_argument, "NODAL_MASS has Key zero! (check if the application is correctly registered", "")

  //verify that the dofs exist
  for (SizeType i = 0; i < this->GetGeometry().size(); i++)
  {
    if (this->GetGeometry()[i].SolutionStepsDataHas(VELOCITY) == false)
      KRATOS_THROW_ERROR(std::invalid_argument, "missing variable VELOCITY on node ", this->GetGeometry()[i].Id())
    if (this->GetGeometry()[i].HasDofFor(VELOCITY_X) == false || this->GetGeometry()[i].HasDofFor(VELOCITY_Y) == false || this->GetGeometry()[i].HasDofFor(VELOCITY_Z) == false)
      KRATOS_THROW_ERROR(std::invalid_argument, "missing one of the dofs for the variable VELOCITY on node ", GetGeometry()[i].Id())
  }

  //verify that the area is given by properties
  if (this->GetProperties().Has(NODAL_MASS) == false)
  {
    if (GetValue(NODAL_MASS) == 0.0)
      KRATOS_THROW_ERROR(std::logic_error, "NODAL_MASS not provided for this element", this->Id())
  }

  return 0;

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void TranslationalRigidBodySegregatedVElement::save(Serializer &rSerializer) const
{
  KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, TranslationalRigidBodyElement)
}

void TranslationalRigidBodySegregatedVElement::load(Serializer &rSerializer)
{
  KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, TranslationalRigidBodyElement)
}

} // Namespace Kratos
