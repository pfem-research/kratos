//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                August 2016 $
//
//

// System includes

// External includes

// Project includes
#include "custom_elements/translational_rigid_body_element.hpp"

#include "contact_mechanics_application_variables.h"

namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

TranslationalRigidBodyElement::TranslationalRigidBodyElement(IndexType NewId, GeometryType::Pointer pGeometry)
    : RigidBodyElement(NewId, pGeometry)
{
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

TranslationalRigidBodyElement::TranslationalRigidBodyElement(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : RigidBodyElement(NewId, pGeometry, pProperties)
{
  KRATOS_TRY

  KRATOS_CATCH("")
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

TranslationalRigidBodyElement::TranslationalRigidBodyElement(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties, NodesContainerType::Pointer pNodes)
    : RigidBodyElement(NewId, pGeometry, pProperties, pNodes)
{
  KRATOS_TRY

  KRATOS_CATCH("")
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

TranslationalRigidBodyElement::TranslationalRigidBodyElement(TranslationalRigidBodyElement const &rOther) : RigidBodyElement(rOther)
{
}

//*********************************CREATE*********************************************
//************************************************************************************

Element::Pointer TranslationalRigidBodyElement::Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const
{
    return Kratos::make_intrusive<TranslationalRigidBodyElement>(NewId, GetGeometry().Create(ThisNodes), pProperties);
}

//*********************************CLONE**********************************************
//************************************************************************************

Element::Pointer TranslationalRigidBodyElement::Clone(IndexType NewId, NodesArrayType const &ThisNodes) const
{

    TranslationalRigidBodyElement NewElement(NewId, GetGeometry().Create(ThisNodes), pGetProperties(), mpNodes);

    NewElement.mInitialLocalQuaternion = this->mInitialLocalQuaternion;
    NewElement.SetData(this->GetData());
    NewElement.SetFlags(this->GetFlags());

    return Kratos::make_intrusive<TranslationalRigidBodyElement>(NewElement);
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

TranslationalRigidBodyElement::~TranslationalRigidBodyElement()
{
}

//************* GETTING METHODS
//************************************************************************************
//************************************************************************************

void TranslationalRigidBodyElement::GetDofList(DofsVectorType &ElementalDofList, const ProcessInfo &rCurrentProcessInfo) const
{

    ElementalDofList.resize(0);

    const unsigned int number_of_nodes = GetGeometry().size();
    const unsigned int dimension = GetGeometry().WorkingSpaceDimension();

    for (unsigned int i = 0; i < number_of_nodes; i++)
    {
        ElementalDofList.push_back(GetGeometry()[i].pGetDof(DISPLACEMENT_X));
        ElementalDofList.push_back(GetGeometry()[i].pGetDof(DISPLACEMENT_Y));
        if (dimension == 3)
            ElementalDofList.push_back(GetGeometry()[i].pGetDof(DISPLACEMENT_Z));
    }
}

//************************************************************************************
//************************************************************************************

void TranslationalRigidBodyElement::EquationIdVector(EquationIdVectorType &rResult, const ProcessInfo &rCurrentProcessInfo) const
{

    const unsigned int number_of_nodes = GetGeometry().size();
    const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
    unsigned int element_size = number_of_nodes * (dimension);

    if (rResult.size() != element_size)
        rResult.resize(element_size, false);

    for (unsigned int i = 0; i < number_of_nodes; i++)
    {
        int index = i * (dimension);
        rResult[index] = GetGeometry()[i].GetDof(DISPLACEMENT_X).EquationId();
        rResult[index + 1] = GetGeometry()[i].GetDof(DISPLACEMENT_Y).EquationId();
        if (dimension == 3)
            rResult[index + 2] = GetGeometry()[i].GetDof(DISPLACEMENT_Z).EquationId();
    }
}

//*********************************DISPLACEMENT***************************************
//************************************************************************************

void TranslationalRigidBodyElement::GetValuesVector(Vector &rValues, int Step) const
{
    KRATOS_TRY

    const unsigned int number_of_nodes = GetGeometry().size();
    const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
    unsigned int element_size = number_of_nodes * (dimension);

    if (rValues.size() != element_size)
        rValues.resize(element_size, false);

    for (unsigned int i = 0; i < number_of_nodes; i++)
    {
        int index = i * (dimension);
        rValues[index] = GetGeometry()[i].FastGetSolutionStepValue(DISPLACEMENT_X, Step);
        rValues[index + 1] = GetGeometry()[i].FastGetSolutionStepValue(DISPLACEMENT_Y, Step);
        if (dimension == 3)
            rValues[index + 2] = GetGeometry()[i].FastGetSolutionStepValue(DISPLACEMENT_Z, Step);
    }

    KRATOS_CATCH("")
}

//************************************VELOCITY****************************************
//************************************************************************************

//************************************************************************************
//************************************************************************************
void TranslationalRigidBodyElement::GetFirstDerivativesVector(Vector &rValues, int Step) const
{
    KRATOS_TRY

    const unsigned int number_of_nodes = GetGeometry().size();
    const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
    unsigned int element_size = number_of_nodes * (dimension);

    if (rValues.size() != element_size)
        rValues.resize(element_size, false);

    for (unsigned int i = 0; i < number_of_nodes; i++)
    {
        int index = i * (dimension);
        rValues[index] = GetGeometry()[i].FastGetSolutionStepValue(VELOCITY_X, Step);
        rValues[index + 1] = GetGeometry()[i].FastGetSolutionStepValue(VELOCITY_Y, Step);
        if (dimension == 3)
            rValues[index + 2] = GetGeometry()[i].FastGetSolutionStepValue(VELOCITY_Z, Step);
    }

    KRATOS_CATCH("")
}

//*********************************ACCELERATION***************************************
//************************************************************************************

void TranslationalRigidBodyElement::GetSecondDerivativesVector(Vector &rValues, int Step) const
{
    KRATOS_TRY

    const unsigned int number_of_nodes = GetGeometry().size();
    const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
    unsigned int element_size = number_of_nodes * (dimension);

    if (rValues.size() != element_size)
        rValues.resize(element_size, false);

    for (unsigned int i = 0; i < number_of_nodes; i++)
    {
        int index = i * (dimension);
        rValues[index] = GetGeometry()[i].FastGetSolutionStepValue(ACCELERATION_X, Step);
        rValues[index + 1] = GetGeometry()[i].FastGetSolutionStepValue(ACCELERATION_Y, Step);
        if (dimension == 3)
            rValues[index + 2] = GetGeometry()[i].FastGetSolutionStepValue(ACCELERATION_Z, Step);
    }

    KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

//************* STARTING - ENDING  METHODS
//************************************************************************************
//************************************************************************************

void TranslationalRigidBodyElement::Initialize(const ProcessInfo &rCurrentProcessInfo)
{
    KRATOS_TRY

    Matrix InitialLocalMatrix = IdentityMatrix(3);

    mInitialLocalQuaternion = QuaternionType::FromRotationMatrix(InitialLocalMatrix);

    KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void TranslationalRigidBodyElement::InitializeSystemMatrices(MatrixType &rLeftHandSideMatrix,
                                                           VectorType &rRightHandSideVector,
                                                           Flags &rCalculationFlags)

{

    const unsigned int number_of_nodes = GetGeometry().size();
    const unsigned int dimension = GetGeometry().WorkingSpaceDimension();

    //resizing as needed the LHS
    unsigned int MatSize = number_of_nodes * (dimension);

    if (rCalculationFlags.Is(TranslationalRigidBodyElement::COMPUTE_LHS_MATRIX)) //calculation of the matrix is required
    {
        if (rLeftHandSideMatrix.size1() != MatSize)
            rLeftHandSideMatrix.resize(MatSize, MatSize, false);

        noalias(rLeftHandSideMatrix) = ZeroMatrix(MatSize, MatSize); //resetting LHS
    }

    //resizing as needed the RHS
    if (rCalculationFlags.Is(TranslationalRigidBodyElement::COMPUTE_RHS_VECTOR)) //calculation of the matrix is required
    {
        if (rRightHandSideVector.size() != MatSize)
            rRightHandSideVector.resize(MatSize, false);

        rRightHandSideVector = ZeroVector(MatSize); //resetting RHS
    }
}

//************************************************************************************
//************************************************************************************

void TranslationalRigidBodyElement::MapLocalToGlobalSystem(LocalSystemComponents &rLocalSystem)
{
    KRATOS_TRY

    // Note:
    // That means that the standard rotation K = Q·K'·QT and F = Q·F' is the correct transformation

    Matrix InitialLocalMatrix = ZeroMatrix(3, 3);
    mInitialLocalQuaternion.ToRotationMatrix(InitialLocalMatrix);

    // Transform Local to Global LHSMatrix:
    if (rLocalSystem.CalculationFlags.Is(RigidBodyElement::COMPUTE_LHS_MATRIX))
    {

        MatrixType &rLeftHandSideMatrix = rLocalSystem.GetLeftHandSideMatrix();

        // works for 2D and 3D case
        BeamMathUtilsType::MapLocalToGlobal2D(InitialLocalMatrix, rLeftHandSideMatrix);

        //std::cout<<"["<<this->Id()<<"] RB RotatedDynamic rLeftHandSideMatrix "<<rLeftHandSideMatrix<<std::endl;
    }

    // Transform Local to Global RHSVector:
    if (rLocalSystem.CalculationFlags.Is(RigidBodyElement::COMPUTE_RHS_VECTOR))
    {

        VectorType &rRightHandSideVector = rLocalSystem.GetRightHandSideVector();

        //std::cout<<"["<<this->Id()<<"] RB Dynamic rRightHandSideVector "<<rRightHandSideVector<<std::endl;

        // works for 2D and 3D case
        BeamMathUtilsType::MapLocalToGlobal2D(InitialLocalMatrix, rRightHandSideVector);

        //std::cout<<"["<<this->Id()<<"] RB RotatedDynamic rRightHandSideVector "<<rRightHandSideVector<<std::endl;
    }

    KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

//Inertia in the SPATIAL configuration
void TranslationalRigidBodyElement::CalculateAndAddInertiaLHS(MatrixType &rLeftHandSideMatrix, ElementVariables &rVariables)
{

    KRATOS_TRY

    const ProcessInfo &rCurrentProcessInfo = rVariables.GetProcessInfo();
    const SizeType dimension = GetGeometry().WorkingSpaceDimension();
    const SizeType dofs_size = this->GetDofsSize();

    if (rLeftHandSideMatrix.size1() != dofs_size)
        rLeftHandSideMatrix.resize(dofs_size, dofs_size, false);

    rLeftHandSideMatrix = ZeroMatrix(dofs_size, dofs_size);

    //rCurrentProcessInfo must give it:
    double AlphaM = rCurrentProcessInfo[BOSSAK_ALPHA];

    BoundedVector<double, 3> LinearNewmark;
    BoundedVector<double, 3> AngularNewmark;

    this->GetTimeIntegrationParameters(LinearNewmark, AngularNewmark, rCurrentProcessInfo);

    //block 1 of the mass matrix
    MatrixType m11(dimension, dimension);
    noalias(m11) = IdentityMatrix(dimension);
    m11 *= (1.0 - AlphaM) * LinearNewmark[1] * rVariables.RigidBody.Mass;

    //Building the Local Tangent Inertia Matrix
    BeamMathUtilsType::AddMatrix(rLeftHandSideMatrix, m11, 0, 0);

    //std::cout<<" rLeftHandSideMatrix "<<rLeftHandSideMatrix<<std::endl;

    KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

//Inertia in the SPATIAL configuration
void TranslationalRigidBodyElement::CalculateAndAddInertiaRHS(VectorType &rRightHandSideVector, ElementVariables &rVariables)
{
    KRATOS_TRY

    const ProcessInfo &rCurrentProcessInfo = rVariables.GetProcessInfo();

    const SizeType dimension = GetGeometry().WorkingSpaceDimension();
    const SizeType dofs_size = this->GetDofsSize();

    if (rRightHandSideVector.size() != dofs_size)
        rRightHandSideVector.resize(dofs_size, false);

    noalias(rRightHandSideVector) = ZeroVector(dofs_size);

    ArrayType CurrentLinearAccelerationVector = GetGeometry()[0].FastGetSolutionStepValue(ACCELERATION);
    CurrentLinearAccelerationVector = MapToInitialLocalFrame(CurrentLinearAccelerationVector);
    ArrayType PreviousLinearAccelerationVector = GetGeometry()[0].FastGetSolutionStepValue(ACCELERATION, 1);
    PreviousLinearAccelerationVector = MapToInitialLocalFrame(PreviousLinearAccelerationVector);

    double AlphaM = rCurrentProcessInfo[BOSSAK_ALPHA];

    ArrayType LinearAccelerationVector = (1.0 - AlphaM) * CurrentLinearAccelerationVector + AlphaM * (PreviousLinearAccelerationVector);

    //-----------------
    //block 1 of the inertial force vector

    //Compute Linear Term:
    Vector LinearInertialForceVector(dimension);

    for (SizeType i = 0; i < dimension; ++i)
        LinearInertialForceVector[i] = rVariables.RigidBody.Mass * LinearAccelerationVector[i];

    BeamMathUtilsType::AddVector(LinearInertialForceVector, rRightHandSideVector, 0);

    //std::cout<<" Rigid Body: rRightHandSideVector "<<rRightHandSideVector<<std::endl;

    KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void TranslationalRigidBodyElement::CalculateMassMatrix(MatrixType &rMassMatrix, const ProcessInfo &rCurrentProcessInfo)
{

    KRATOS_TRY

    const SizeType dimension = GetGeometry().WorkingSpaceDimension();
    const SizeType dofs_size = this->GetDofsSize();

    if (rMassMatrix.size1() != dofs_size)
        rMassMatrix.resize(dofs_size, dofs_size, false);

    rMassMatrix = ZeroMatrix(dofs_size, dofs_size);

    // Rigid Body Properties
    RigidBodyProperties RigidBody;
    this->CalculateRigidBodyProperties(RigidBody);

    //block m(1,1) of the mass matrix
    MatrixType m11(dimension, dimension);
    noalias(m11) = IdentityMatrix(dimension);
    m11 *= RigidBody.Mass;

    //Building the Local Tangent Inertia Matrix
    BeamMathUtilsType::AddMatrix(rMassMatrix, m11, 0, 0);

    KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void TranslationalRigidBodyElement::UpdateRigidBodyNodes(const ProcessInfo &rCurrentProcessInfo)
{

    KRATOS_TRY

    const Node<3> &rCenterOfGravity = this->GetGeometry()[0];

    const ArrayType &Displacement = rCenterOfGravity.FastGetSolutionStepValue(DISPLACEMENT);
    const ArrayType &Velocity = rCenterOfGravity.FastGetSolutionStepValue(VELOCITY);
    const ArrayType &Acceleration = rCenterOfGravity.FastGetSolutionStepValue(ACCELERATION);

    for (auto &i_node : *mpNodes)
    {
      noalias(i_node.FastGetSolutionStepValue(DISPLACEMENT)) = Displacement;
      noalias(i_node.FastGetSolutionStepValue(VELOCITY)) = Velocity;
      noalias(i_node.FastGetSolutionStepValue(ACCELERATION)) = Acceleration;
    }

    KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

TranslationalRigidBodyElement::SizeType TranslationalRigidBodyElement::GetDofsSize() const
{
    KRATOS_TRY

    const SizeType dimension = GetGeometry().WorkingSpaceDimension();
    const SizeType number_of_nodes = GetGeometry().PointsNumber();

    SizeType size = number_of_nodes * dimension; //size for velocity

    return size;

    KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

int TranslationalRigidBodyElement::Check(const ProcessInfo &rCurrentProcessInfo) const
{
    KRATOS_TRY

    if (GetGeometry().size() != 1)
    {
        KRATOS_THROW_ERROR(std::invalid_argument, "This element works only with 1 noded geometry", "")
    }

    //verify that the variables are correctly initialized
    if (VELOCITY.Key() == 0)
        KRATOS_THROW_ERROR(std::invalid_argument, "VELOCITY has Key zero! (check if the application is correctly registered", "")
    if (DISPLACEMENT.Key() == 0)
        KRATOS_THROW_ERROR(std::invalid_argument, "DISPLACEMENT has Key zero! (check if the application is correctly registered", "")
    if (ACCELERATION.Key() == 0)
        KRATOS_THROW_ERROR(std::invalid_argument, "ACCELERATION has Key zero! (check if the application is correctly registered", "")
    if (DENSITY.Key() == 0)
        KRATOS_THROW_ERROR(std::invalid_argument, "DENSITY has Key zero! (check if the application is correctly registered", "")
    if (NODAL_MASS.Key() == 0)
        KRATOS_THROW_ERROR(std::invalid_argument, "NODAL_MASS has Key zero! (check if the application is correctly registered", "")

    //verify that the dofs exist
    for (SizeType i = 0; i < this->GetGeometry().size(); i++)
    {
        if (this->GetGeometry()[i].SolutionStepsDataHas(DISPLACEMENT) == false)
            KRATOS_THROW_ERROR(std::invalid_argument, "missing variable DISPLACEMENT on node ", this->GetGeometry()[i].Id())
        if (this->GetGeometry()[i].HasDofFor(DISPLACEMENT_X) == false || this->GetGeometry()[i].HasDofFor(DISPLACEMENT_Y) == false || this->GetGeometry()[i].HasDofFor(DISPLACEMENT_Z) == false)
            KRATOS_THROW_ERROR(std::invalid_argument, "missing one of the dofs for the variable DISPLACEMENT on node ", GetGeometry()[i].Id())
    }

    //verify that the area is given by properties
    if (this->GetProperties().Has(NODAL_MASS) == false)
    {
        if (GetValue(NODAL_MASS) == 0.0)
            KRATOS_THROW_ERROR(std::logic_error, "NODAL_MASS not provided for this element", this->Id())
    }

    return 0;

    KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void TranslationalRigidBodyElement::save(Serializer &rSerializer) const
{
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, RigidBodyElement)
}

void TranslationalRigidBodyElement::load(Serializer &rSerializer)
{
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, RigidBodyElement)
}

} // Namespace Kratos
