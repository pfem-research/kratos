//-------------------------------------------------------------
//          ___         _           _
//  KRATOS / __|___ _ _| |_ __ _ __| |_
//        | (__/ _ \ ' \  _/ _` / _|  _|
//         \___\___/_||_\__\__,_\__|\__|MECHANICS
//
//  License:(BSD)    ContactMechanicsApplication/license.txt
//
//  Main authors:    Josep Maria Carbonell
//                   ...
//
//-------------------------------------------------------------
//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                August 2016 $
//
//

#if !defined(KRATOS_CONTACT_MECHANICS_APPLICATION_H_INCLUDED)
#define KRATOS_CONTACT_MECHANICS_APPLICATION_H_INCLUDED

// System includes
#include <string>
#include <iostream>

// External includes

// Project includes
#include "includes/define.h"
#include "includes/kratos_application.h"
#include "includes/variables.h"

// elements
#include "custom_elements/rigid_body_segregated_V_element.hpp"
#include "custom_elements/translational_rigid_body_segregated_V_element.hpp"

// conditions
#include "custom_conditions/deformable_contact/contact_domain_condition.hpp"
#include "custom_conditions/deformable_contact/contact_domain_LM_3D_condition.hpp"
#include "custom_conditions/deformable_contact/contact_domain_penalty_3D_condition.hpp"
#include "custom_conditions/deformable_contact/contact_domain_LM_2D_condition.hpp"
#include "custom_conditions/deformable_contact/contact_domain_penalty_2D_condition.hpp"
#include "custom_conditions/deformable_contact/axisymmetric_contact_domain_LM_2D_condition.hpp"
#include "custom_conditions/deformable_contact/axisymmetric_contact_domain_penalty_2D_condition.hpp"
#include "custom_conditions/deformable_contact/contact_domain_EP_2D_condition.hpp"
#include "custom_conditions/deformable_contact/axisymmetric_contact_domain_EP_2D_condition.hpp"

#include "custom_conditions/thermal_contact/thermal_contact_domain_penalty_2D_condition.hpp"
#include "custom_conditions/thermal_contact/axisymmetric_thermal_contact_domain_penalty_2D_condition.hpp"

#include "custom_conditions/rigid_contact/point_rigid_contact_condition.hpp"
#include "custom_conditions/rigid_contact/point_rigid_contact_penalty_2D_condition.hpp"
#include "custom_conditions/rigid_contact/axisymmetric_point_rigid_contact_penalty_2D_condition.hpp"

#include "custom_conditions/rigid_contact/point_rigid_contact_segregated_V_condition.hpp"

#include "custom_conditions/rigid_contact/EP_point_rigid_contact_penalty_3D_condition.hpp"
#include "custom_conditions/rigid_contact/EP_point_rigid_contact_penalty_2D_condition.hpp"
#include "custom_conditions/rigid_contact/EP_axisymmetric_point_rigid_contact_penalty_2D_condition.hpp"
#include "custom_conditions/rigid_contact/EP_cpt_condition.hpp"

#include "custom_conditions/hydraulic_contact/hydraulic_rigid_contact_penalty_3D_condition.hpp"
#include "custom_conditions/hydraulic_contact/hydraulic_axisymmetric_rigid_contact_penalty_2D_condition.hpp"

#include "custom_conditions/beam_contact/beam_point_rigid_contact_LM_3D_condition.hpp"
#include "custom_conditions/beam_contact/beam_point_rigid_contact_penalty_3D_condition.hpp"
#include "custom_conditions/rigid_contact/rigid_body_point_rigid_contact_condition.hpp"

// friction laws
#include "custom_friction/friction_law.hpp"
#include "custom_friction/coulomb_adhesion_friction_law.hpp"
#include "custom_friction/hardening_coulomb_friction_law.hpp"

// bounding boxes
#include "custom_bounding/plane_bounding_box.hpp"
#include "custom_bounding/sphere_bounding_box.hpp"
#include "custom_bounding/circle_bounding_box.hpp"
#include "custom_bounding/cylinder_bounding_box.hpp"
#include "custom_bounding/tube_bounding_box.hpp"
#include "custom_bounding/compound_noses_bounding_box.hpp"

// rigid body links
#include "custom_conditions/rigid_body_links/rigid_body_point_link_segregated_V_condition.hpp"

// Core applications
#include "contact_mechanics_application_variables.h"

#include "custom_bounding/circle_bounding_box.hpp"
#include "custom_bounding/cylinder_bounding_box.hpp"
#include "custom_bounding/sphere_bounding_box.hpp"
#include "custom_bounding/compound_noses_bounding_box.hpp"
#include "custom_bounding/plane_bounding_box.hpp"
#include "custom_bounding/tube_bounding_box.hpp"


namespace Kratos
{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
*/
class KRATOS_API(CONTACT_MECHANICS_APPLICATION) KratosContactMechanicsApplication : public KratosApplication
{
public:
	///@name Type Definitions
	///@{

	/// Pointer definition of KratosContactMechanicsApplication
	KRATOS_CLASS_POINTER_DEFINITION(KratosContactMechanicsApplication);

	///@}
	///@name Life Cycle
	///@{

	/// Default constructor.
	KratosContactMechanicsApplication();

	/// Destructor.
	virtual ~KratosContactMechanicsApplication() {}

	///@}
	///@name Operators
	///@{

	///@}
	///@name Operations
	///@{

	void Register() override;

	///@}
	///@name Access
	///@{

	///@}
	///@name Inquiry
	///@{

	///@}
	///@name Input and output
	///@{

	/// Turn back information as a string.
	std::string Info() const override
	{
		return "KratosContactMechanicsApplication";
	}

	/// Print information about this object.
	void PrintInfo(std::ostream &rOStream) const override
	{
		rOStream << Info();
		PrintData(rOStream);
	}

	///// Print object's data.
	void PrintData(std::ostream &rOStream) const override
	{
		KRATOS_WATCH("in my application");
		KRATOS_WATCH(KratosComponents<VariableData>::GetComponents().size());

		rOStream << "Variables:" << std::endl;
		KratosComponents<VariableData>().PrintData(rOStream);
		rOStream << std::endl;
		rOStream << "Elements:" << std::endl;
		KratosComponents<Element>().PrintData(rOStream);
		rOStream << std::endl;
		rOStream << "Conditions:" << std::endl;
		KratosComponents<Condition>().PrintData(rOStream);
	}

	///@}
	///@name Friends
	///@{

	///@}

protected:
	///@name Protected static Member Variables
	///@{

	///@}
	///@name Protected member Variables
	///@{

	///@}
	///@name Protected Operators
	///@{

	///@}
	///@name Protected Operations
	///@{

	///@}
	///@name Protected  Access
	///@{

	///@}
	///@name Protected Inquiry
	///@{

	///@}
	///@name Protected LifeCycle
	///@{

	///@}

private:
	///@name Static Member Variables
	///@{

	// static const ApplicationCondition  msApplicationCondition;

	///@}
	///@name Member Variables
	///@{

	//elements
	const RigidBodyElement mRigidBodyElement;
	const RigidBodySegregatedVElement mRigidBodySegregatedVElement;
	const TranslationalRigidBodyElement mTranslationalRigidBodyElement;
	const TranslationalRigidBodyElement mTranslationalRigidBodySegregatedVElement;

	//conditions
	const ContactDomainLM3DCondition mContactDomainLMCondition3D4N;
	const ContactDomainPenalty3DCondition mContactDomainPenaltyCondition3D4N;

	const ContactDomainLM2DCondition mContactDomainLMCondition2D3N;
	const ContactDomainPenalty2DCondition mContactDomainPenaltyCondition2D3N;
	const ContactDomainEP2DCondition mContactDomainEPCondition2D3N;

	const AxisymmetricContactDomainLM2DCondition mAxisymContactDomainLMCondition2D3N;
	const AxisymmetricContactDomainPenalty2DCondition mAxisymContactDomainPenaltyCondition2D3N;
	const AxisymmetricContactDomainEP2DCondition mAxisymContactDomainEPCondition2D3N;

        const ThermalContactDomainCondition mThermalContactDomainPenaltyCondition3D4N;
	const ThermalContactDomainPenalty2DCondition mThermalContactDomainPenaltyCondition2D3N;
	const AxisymmetricThermalContactDomainPenalty2DCondition mAxisymThermalContactDomainPenaltyCondition2D3N;

	const RigidBodyPointLinkCondition mRigidBodyPointLinkCondition2D1N;
	const RigidBodyPointLinkCondition mRigidBodyPointLinkCondition3D1N;

	const RigidBodyPointLinkSegregatedVCondition mRigidBodyPointLinkSegregatedVCondition2D1N;
	const RigidBodyPointLinkSegregatedVCondition mRigidBodyPointLinkSegregatedVCondition3D1N;

	const PointRigidContactPenalty2DCondition mPointRigidContactPenaltyCondition2D1N;
	const PointRigidContactPenalty3DCondition mPointRigidContactPenaltyCondition3D1N;
	const AxisymmetricPointRigidContactPenalty2DCondition mAxisymPointRigidContactPenaltyCondition2D1N;

	const PointRigidContactVCondition mPointRigidContactPenaltyVCondition2D1N;
	const PointRigidContactVCondition mPointRigidContactPenaltyVCondition3D1N;

	const PointRigidContactSegregatedVCondition mPointRigidContactPenaltySegregatedVCondition2D1N;
	const PointRigidContactSegregatedVCondition mPointRigidContactPenaltySegregatedVCondition3D1N;

        const BeamPointRigidContactLM3DCondition mBeamPointRigidContactLMCondition3D1N;
        const BeamPointRigidContactPenalty3DCondition mBeamPointRigidContactPenaltyCondition3D1N;
        const RigidBodyPointRigidContactCondition mRigidBodyPointRigidContactCondition3D1N;

	const EPPointRigidContactPenalty2DCondition mEPPointRigidContactPenaltyCondition2D1N;
	const EPPointRigidContactPenalty3DCondition mEPPointRigidContactPenaltyCondition3D1N;
	const EPAxisymmetricPointRigidContactPenalty2DCondition mEPAxisymPointRigidContactPenaltyCondition2D1N;
	const EPCPTCondition mEPCPTCondition2D1N;

	const HydraulicRigidContactPenalty3DCondition mHydraulicRigidContactPenaltyCondition3D1N;
	const HydraulicAxisymmetricRigidContactPenalty2DCondition mHydraulicAxisymRigidContactPenaltyCondition2D1N;

	//friction laws
	const FrictionLaw mFrictionLaw;
	const CoulombAdhesionFrictionLaw mCoulombAdhesionFrictionLaw;
	const HardeningCoulombFrictionLaw mHardeningCoulombFrictionLaw;

	//bounding boxes
	const PlaneBoundingBox mPlaneBoundingBox;
	const SphereBoundingBox mSphereBoundingBox;
	const CircleBoundingBox mCircleBoundingBox;
        const CylinderBoundingBox mCylinderBoundingBox;
	const CompoundNosesBoundingBox mCompoundNosesBoundingBox;
	const TubeBoundingBox mTubeBoundingBox;

	///@}
	///@name Private Operators
	///@{

	///@}
	///@name Private Operations
	///@{

	///@}
	///@name Private  Access
	///@{

	///@}
	///@name Private Inquiry
	///@{

	///@}
	///@name Un accessible methods
	///@{

	/// Assignment operator.
	KratosContactMechanicsApplication &operator=(KratosContactMechanicsApplication const &rOther);

	/// Copy constructor.
	KratosContactMechanicsApplication(KratosContactMechanicsApplication const &rOther);

	///@}

}; // Class KratosContactMechanicsApplication

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

} // namespace Kratos.

#endif // KRATOS_CONTACT_MECHANICS_APPLICATION_H_INCLUDED  defined
