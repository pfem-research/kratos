//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                August 2016 $
//
//

#if !defined(KRATOS_BUILD_CONTACT_MODEL_STRUCTURE_PROCESS_HPP_INCLUDED)
#define KRATOS_BUILD_CONTACT_MODEL_STRUCTURE_PROCESS_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_processes/build_model_structure_process.hpp"

namespace Kratos
{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{
typedef ModelPart::NodesContainerType NodesContainerType;
typedef ModelPart::ElementsContainerType ElementsContainerType;
typedef ModelPart::ConditionsContainerType ConditionsContainerType;

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
class BuildContactModelStructureProcess
    : public BuildModelStructureProcess
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of BuildContactModelStructureProcess
  KRATOS_CLASS_POINTER_DEFINITION(BuildContactModelStructureProcess);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  BuildContactModelStructureProcess(ModelPart &rModelPart,
                                     Flags Options,
                                     int EchoLevel = 0)
      : BuildModelStructureProcess(rModelPart, Options, EchoLevel)
  {
  }

  /// Destructor.
  virtual ~BuildContactModelStructureProcess()
  {
  }

  ///@}
  ///@name Operators
  ///@{

  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  void Execute() override{

  };

  /// this function is designed for being called at the beginning of the computations
  /// right after reading the model and the groups
  void ExecuteInitialize() override
  {

    KRATOS_TRY

    this->ClearContactConditions();

    this->ClearContactFlags();

    //CONDITIONS PRIMARY_ELEMENTS and PRIMARY_NODES SEARCH
    if (mrModelPart.GetProcessInfo()[IS_RESTARTED] == true && mrModelPart.GetProcessInfo()[INITIALIZED_DOMAINS] == false)
    {
      BuiltinTimer time_counter;
      BuildModelPartBoundaryProcess BuildBoundaryProcess(mrModelPart, mrModelPart.Name(), mEchoLevel);
      BuildBoundaryProcess.SearchConditionMasters();
      std::cout<<" Search masters time :"<<time_counter.ElapsedSeconds()<<std::endl;
    }

    //Update Boundary Normals before Contact Search
    //(needed when meshing of the domains is not performed:
    // normal directions change with mesh movement)
    BuiltinTimer time_counter;
    BoundaryNormalsUtilities BoundaryNormals;
    BoundaryNormals.CalculateWeightedBoundaryNormals(mrModelPart, mEchoLevel);
    std::cout<<" Calculate masters time :"<<time_counter.ElapsedSeconds()<<std::endl;

    KRATOS_CATCH(" ")
  }

  /// this function is designed for being called at the end of the computations
  /// right after reading the model and the groups
  void ExecuteFinalize() override
  {
    KRATOS_TRY

    BuiltinTimer time_counter;

    // Add contact conditions
    this->AddContactConditions();

     // Restore contact nodal flags
    this->RestoreContactFlags();

    // Renumerate contact conditions to not coincide with element ids
    ModelPartUtilities::SetUniqueIdContactConditions(mrModelPart);

    // Clear Contact Forces
    this->ClearContactForces();

     // Clear Contact Normals
    this->ClearContactNormals();

    std::cout<<" Finalize contact buildment :"<<time_counter.ElapsedSeconds()<<std::endl;


    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "BuildContactModelStructureProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "BuildContactModelStructureProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{
  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{

  //**************************************************************************
  //**************************************************************************

  void ClearContactForces()
  {
    KRATOS_TRY

    ModelPart::NodesContainerType &rNodes = mrModelPart.Nodes();

    // create contact condition for rigid and deformable bodies
    for (ModelPart::NodesContainerType::ptr_iterator nd = rNodes.ptr_begin(); nd != rNodes.ptr_end(); ++nd)
    {
      if ((*nd)->Is(BOUNDARY) && (*nd)->IsNot(CONTACT))
      {
        array_1d<double, 3> &ContactForce = (*nd)->FastGetSolutionStepValue(CONTACT_FORCE);
        ContactForce.clear();
      }
    }

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  void ClearContactNormals()
  {
    KRATOS_TRY

    ModelPart::NodesContainerType &rNodes = mrModelPart.Nodes();

    // create contact condition for rigid and deformable bodies
    for (ModelPart::NodesContainerType::ptr_iterator nd = rNodes.ptr_begin(); nd != rNodes.ptr_end(); ++nd)
    {
      // if ((*nd)->Is(BOUNDARY) && (*nd)->IsNot(CONTACT))
      // {
        array_1d<double, 3> &ContactNormal = (*nd)->FastGetSolutionStepValue(CONTACT_NORMAL);
        ContactNormal.clear();
      // }
    }

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  void ClearContactFlags()
  {
    KRATOS_TRY

    for (ModelPart::NodesContainerType::iterator i_node = mrModelPart.NodesBegin(); i_node != mrModelPart.NodesEnd(); ++i_node)
    {
      if (i_node->Is(CONTACT))
      {
        i_node->Set(CONTACT, false);
      }
    }

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  void RestoreContactFlags()
  {

    KRATOS_TRY

    for (ModelPart::ConditionsContainerType::iterator i_cond = mrModelPart.ConditionsBegin(); i_cond != mrModelPart.ConditionsEnd(); ++i_cond)
    {
      if (i_cond->Is(CONTACT))
      {
        for (unsigned int i = 0; i < i_cond->GetGeometry().size(); ++i)
        {
          i_cond->GetGeometry()[i].Set(CONTACT, true);
        }
      }
    }

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  void AddContactConditions()
  {

    KRATOS_TRY

    //Add contact conditions to computing domain
    std::string ComputingModelPartName;
    for (ModelPart::SubModelPartIterator i_mp = mrModelPart.SubModelPartsBegin(); i_mp != mrModelPart.SubModelPartsEnd(); ++i_mp)
    {
      if (i_mp->IsNot(THERMAL) && i_mp->Is(ACTIVE))
        ComputingModelPartName = i_mp->Name();
    }

    ModelPart &ComputingModelPart = mrModelPart.GetSubModelPart(ComputingModelPartName);

    //Add contact conditions from contact domain
    for (ModelPart::SubModelPartIterator i_mp = mrModelPart.SubModelPartsBegin(); i_mp != mrModelPart.SubModelPartsEnd(); ++i_mp)
    {
      if (i_mp->Is(CONTACT)){
        ModelPart &ContactModelPart = mrModelPart.GetSubModelPart(i_mp->Name());

	AddContactConditions(ContactModelPart, ComputingModelPart);

	//Add contact conditions to  main domain (if added in computing domaing with AddCondition, are automatically added to the main domain, else use push_back)
	//not needed : AddContactConditions(ContactModelPart, mrModelPart);
      }
    }

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  void AddContactConditions(ModelPart &rOriginModelPart, ModelPart &rDestinationModelPart)
  {

    KRATOS_TRY

    //*******************************************************************
    //adding contact conditions
    //

    if (mEchoLevel >= 1)
    {
      std::cout << " [" << rDestinationModelPart.Name() << " :: CONDITIONS [OLD:" << rDestinationModelPart.NumberOfConditions();
    }

    ModelPart::ConditionsContainerType new_condition_list;
    for (ModelPart::ConditionsContainerType::iterator ic = rOriginModelPart.ConditionsBegin(); ic != rOriginModelPart.ConditionsEnd(); ++ic)
    {

      if (ic->Is(CONTACT))
        new_condition_list.push_back(*(ic.base()));
    }

    rDestinationModelPart.AddConditions(new_condition_list.begin(), new_condition_list.end());

    if (mEchoLevel >= 1)
    {
      std::cout << " / NEW:" << rDestinationModelPart.NumberOfConditions() << "] " << std::endl;
    }

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  void ClearContactConditions()
  {

    KRATOS_TRY

    //Clear contact conditions from computing domain
    for (ModelPart::SubModelPartIterator i_mp = mrModelPart.SubModelPartsBegin(); i_mp != mrModelPart.SubModelPartsEnd(); ++i_mp)
    {
      if(i_mp->Is(ACTIVE))
      {//thermal and solid solving model parts...
        ClearContactConditions(mrModelPart.GetSubModelPart(i_mp->Name()));
      }
    }

    //Clear contact conditions from the main domain
    ClearContactConditions(mrModelPart);

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  void ClearContactConditions(ModelPart &rModelPart)
  {

    KRATOS_TRY

    //*******************************************************************
    //clearing contact conditions
    //

    if (mEchoLevel >= 1)
    {
      std::cout << " [" << rModelPart.Name() << " :: CONDITIONS [OLD:" << rModelPart.NumberOfConditions();
    }

    ModelPart::ConditionsContainerType PreservedConditions;

    for (ModelPart::ConditionsContainerType::iterator ic = rModelPart.ConditionsBegin(); ic != rModelPart.ConditionsEnd(); ++ic)
    {

      if (ic->IsNot(CONTACT) && ic->GetGeometry().size() > 1)
      {
        PreservedConditions.push_back(*(ic.base()));
      }
    }

    rModelPart.Conditions().swap(PreservedConditions);

    //rModelPart.Conditions().Sort();
    //rModelPart.Conditions().Unique();

    if (mEchoLevel >= 1)
    {
      std::cout << " / NEW:" << rModelPart.NumberOfConditions() << "] " << std::endl;
    }

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{

  /// Assignment operator.
  BuildContactModelStructureProcess &operator=(BuildContactModelStructureProcess const &rOther);

  /// Copy constructor.
  //BuildContactModelStructureProcess(BuildContactModelStructureProcess const& rOther);

  ///@}

}; // Class BuildContactModelStructureProcess

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                BuildContactModelStructureProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const BuildContactModelStructureProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_BUILD_CONTACT_MODEL_STRUCTURE_PROCESS_HPP_INCLUDED  defined
