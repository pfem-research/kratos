//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                August 2016 $
//
//

#if !defined(KRATOS_GENERATE_NEW_CONTACT_CONDITIONS_MESHER_PROCESS_HPP_INCLUDED)
#define KRATOS_GENERATE_NEW_CONTACT_CONDITIONS_MESHER_PROCESS_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_processes/select_elements_mesher_process.hpp"

namespace Kratos
{

///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
class GenerateNewContactConditionsMesherProcess
    : public MesherProcess
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of GenerateNewContactConditionsMesherProcess
  KRATOS_CLASS_POINTER_DEFINITION(GenerateNewContactConditionsMesherProcess);

  typedef array_1d<double,3> VectorType;
  typedef ModelPart::ConditionType ConditionType;
  typedef ModelPart::PropertiesType PropertiesType;
  typedef ConditionType::GeometryType GeometryType;

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  GenerateNewContactConditionsMesherProcess(ModelPart &rModelPart,
                                            MesherData::MeshingParameters &rRemeshingParameters,
                                            int EchoLevel)
      : mrModelPart(rModelPart),
        mrRemesh(rRemeshingParameters)
  {
    mEchoLevel = EchoLevel;

    mTransferVariables.SetVariable(CAUCHY_STRESS_VECTOR);
    mTransferVariables.SetVariable(DEFORMATION_GRADIENT);
  }

  /// Destructor.
  virtual ~GenerateNewContactConditionsMesherProcess()
  {
  }

  ///@}
  ///@name Operators
  ///@{

  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  void Execute() override
  {
    KRATOS_TRY

    BuiltinTimer time_counter;

    if (mEchoLevel > 0)
    {
      std::cout << " [ GENERATE NEW CONTACT ELEMENTS: " << std::endl;
      std::cout << "   Total Conditions BEFORE: [" << mrModelPart.Conditions().size() << "] ];" << std::endl;
    }

    this->CheckInitialized();

    //set consecutive ids for global conditions
    ModelPartUtilities::SetConsecutiveIdConditions(mrModelPart.GetParentModelPart());

    //*******************************************************************
    //properties to be used in the generation
    Properties::Pointer pProperties = mrRemesh.GetProperties();

    Condition const &rReferenceCondition = mrRemesh.GetReferenceCondition(); //contact element

    const unsigned int number_of_nodes = rReferenceCondition.GetGeometry().size();

    if (mEchoLevel > 0)
      std::cout << "   [START contact Element Generation " << std::endl;

    int &OutNumberOfElements = mrRemesh.OutMesh.GetNumberOfElements();
    int *OutElementList = mrRemesh.OutMesh.GetElementList();

    unsigned int initial_id = mrModelPart.GetParentModelPart().Conditions().back().Id();
    unsigned int id = initial_id;

    ModelPart::NodesContainerType::iterator nodes_begin = mrModelPart.NodesBegin();

    //Restore Nodes previous IDs
    ModelPartUtilities::SetPreviousIdNodes(mrModelPart,mrRemesh.NodalPreIds);

    //Get point Neighbour Conditions
    ModelPartUtilities::SetNodeNeighbourConditions(mrModelPart);


    for (int el = 0; el < OutNumberOfElements; ++el)
    {
      if (mrRemesh.PreservedElements[el])
      {
        GeometryType Vertices;
        for (unsigned int i = 0; i < number_of_nodes; ++i)
        {
          Vertices.push_back(*(nodes_begin + OutElementList[el * number_of_nodes + i] - 1).base());
          Vertices.back().Set(CONTACT);
        }

	// PRIMARY_NODES, PRIMARY_ELEMENTS, PRIMARY_CONDITIONS will be set

	//First PATCH-A elements (Face elements) or 2D
	if(!this->AddContactElementPatchA(mrModelPart,rReferenceCondition,Vertices,pProperties,id)) //First PATCH-A elements (Face elements) or 2D
	  if(!this->AddContactElementPatchB(mrModelPart,rReferenceCondition,Vertices,pProperties,id)) //Second PATCH-B elements (Edge elements)
            if (mEchoLevel > 0)
              std::cout<<" ContactElement PATCH-B no match found "<<std::endl;
      }
    }

    if (mEchoLevel > 0)
    {
      std::cout << "   [END   contact Elements Generation [" << id - initial_id << "] ]" << std::endl;
      std::cout << "   Total Conditions AFTER: [" << mrModelPart.Conditions().size() << "] ];" << std::endl;
    }

    std::cout << "  [Contact Candidates:" << id - initial_id << "]" << std::endl;
    std::cout <<"  Contact generation :"<<time_counter.ElapsedSeconds()<<std::endl;

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "GenerateNewContactConditionsMesherProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "GenerateNewContactConditionsMesherProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{

  //*******************************************************************************************
  //*******************************************************************************************

  void CheckInitialized()
  {
    if (mrModelPart.Name() != mrRemesh.SubModelPartName)
      std::cout << " ModelPart Supplied do not corresponds to the Meshing Domain: (" << mrModelPart.Name() << " != " << mrRemesh.SubModelPartName << ")" << std::endl;

    //*******************************************************************
    //selecting elements
    if (!mrRemesh.MeshElementsSelectedFlag) //Select Mesh Elements not performed  ... is needed to be done before building new elements
    {
      std::cout << " ERROR : no selection of elements performed before building the elements " << std::endl;
      SelectElementsMesherProcess SelectElements(mrModelPart, mrRemesh, mEchoLevel);
      SelectElements.Execute();
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool AddContactElementPatchA(ModelPart& rModelPart, Condition const &rReferenceCondition, GeometryType &rVertices, PropertiesType::Pointer &pProperties, unsigned int &id)
  {

    //search the model part condition associated to a face condition Patch-A or 2D element
    Condition::Pointer pMainCondition;

    DenseMatrix<unsigned int> lpofa; //points that define the faces
    rReferenceCondition.GetGeometry().NodesInFaces(lpofa);

    if (SearchUtilities::FindMainFaceConditionAndReorderVertices(rModelPart.Conditions(), rVertices, pMainCondition, lpofa))
    {
      const ProcessInfo& rCurrentProcessInfo = rModelPart.GetProcessInfo();

      if (rCurrentProcessInfo[SPACE_DIMENSION]==3)
	if (IsDistortedPatchA(rModelPart, rVertices, pMainCondition->GetValue(NORMAL)))
	  return true; //to not search Patch-B

      TransferUtilities::InitializeBoundaryData(*pMainCondition, mTransferVariables, rCurrentProcessInfo);

      // std::cout<<"Patch-A contact masters "<<std::endl;

      Condition::Pointer pContactCondition = rReferenceCondition.Create(++id, rVertices, pProperties);

      // contact domain 2D or 3D Patch-A :  PRIMARY_CONDITIONS = 1 / PRIMARY_ELEMENTS = 1 / PRIMARY_NODES = 1
      pContactCondition->GetValue(PRIMARY_CONDITIONS).push_back(pMainCondition);
      pContactCondition->SetValue(PRIMARY_ELEMENTS, pMainCondition->GetValue(PRIMARY_ELEMENTS));
      pContactCondition->SetValue(PRIMARY_NODES, pMainCondition->GetValue(PRIMARY_NODES));

      // std::cout<<"Patch-A ["<<pContactCondition->Id()<<"] m_conditions "<<pContactCondition->GetValue(PRIMARY_CONDITIONS).size()<<std::endl;
      // std::cout<<"Patch-A ["<<pContactCondition->Id()<<"] m_elements "<<pContactCondition->GetValue(PRIMARY_ELEMENTS).size()<<std::endl;
      // std::cout<<"Patch-A ["<<pContactCondition->Id()<<"] m_nodes "<<pContactCondition->GetValue(PRIMARY_NODES).size()<<std::endl;
      // std::cout<<std::endl;

      //set the condition NORMAL (not sure it is needed)
      pContactCondition->SetValue(NORMAL, pMainCondition->GetValue(NORMAL));

      //set ACTIVE if it is going to be considered in computation:
      //here one can check the geometrical gap to dismiss some contact elements but
      //it will be done later in the contact element calculation
      pContactCondition->Set(ACTIVE);

      //meaning that is a element that shares faces PATCH-A
      pContactCondition->Set(FREE_SURFACE,false);

      //setting new elements
      rModelPart.AddCondition(pContactCondition);

      return true;
    }

    return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool IsDistortedPatchA(ModelPart& rModelPart, GeometryType &rVertices, const array_1d<double, 3> &rNormal)
  {
    ConditionWeakPtrVectorType &nConditions = rVertices.back().GetValue(NEIGHBOUR_CONDITIONS);

    for (auto &i_cond : nConditions)
    {
      const array_1d<double, 3> &rFaceNormal = i_cond.GetValue(NORMAL);

      double projection = inner_prod(rFaceNormal, -rNormal);
      // Check overlapping faces if coincident (opposite) normals
      if (projection > 0.955){
	GeometryType &cGeometry = i_cond.GetGeometry();
	// Both Triangles must be expressed anti-clockwise
	if (GeometryUtilities::CheckSurfaceTrianglesIntersection(rVertices[0].Coordinates(),rVertices[1].Coordinates(),rVertices[2].Coordinates(),cGeometry[0].Coordinates(),cGeometry[2].Coordinates(),cGeometry[1].Coordinates()))
	  return false;
      }
    }
    return true;
  }


  //*******************************************************************************************
  //*******************************************************************************************

  bool IsDistortedPatchB(ModelPart& rModelPart, GeometryType &rVertices, GlobalPointersVector<Condition>& rPrimaryConditions, const array_1d<double, 3> &rNormal)
  {
    //check edge size projection
    double edge_size = GeometryUtilities::CalculateMinEdgeSize(rVertices);
    for (auto &i_node :  rVertices)
    {
      for (auto &j_node :  rVertices)
      {
        if(i_node.Id() != j_node.Id())
        {
          VectorType D = j_node.Coordinates()-i_node.Coordinates();
          double distance = inner_prod(rNormal, D);
	  // if (mEchoLevel > 0)
	  //   std::cout<<" PATCH B fictious :: edge size "<<edge_size<<" normal "<<rNormal<<std::endl;
          if (std::abs(distance)>0.5*edge_size){ //first check
            // if (mEchoLevel > 0)
            //   std::cout<<" fictious A "<<distance<<std::endl;
            distance = inner_prod(i_node.FastGetSolutionStepValue(NORMAL), D);
            if (std::abs(distance)>0.5*edge_size){ //second check
              // if (mEchoLevel > 0)
              //   std::cout<<" fictious B "<<distance<<std::endl;
              return true;
            }
          }
        }
      }
    }

    //check face projection
    for (auto &i_cond : rPrimaryConditions)
    {
      for (auto &j_cond : rPrimaryConditions)
      {
        if (i_cond.Id() != j_cond.Id()){
          const array_1d<double, 3> &i_normal = i_cond.GetValue(NORMAL);
          const array_1d<double, 3> &j_normal = j_cond.GetValue(NORMAL);

          GeometryType &iGeometry = i_cond.GetGeometry();
          GeometryType &jGeometry = j_cond.GetGeometry();

          double projection = inner_prod(i_normal, -j_normal);
          // Check overlapping faces if coincident (opposite) normals
          if (projection > 0.955){
            // Both Triangles must be expressed anti-clockwise
            if (GeometryUtilities::CheckSurfaceTrianglesIntersection(iGeometry[0].Coordinates(),iGeometry[1].Coordinates(),iGeometry[2].Coordinates(),jGeometry[0].Coordinates(),jGeometry[2].Coordinates(),jGeometry[1].Coordinates()))
              return false;
          }
        }
      }
    }

    if (mEchoLevel > 0)
      std::cout<<" Distorted Patch-B Face projection not found "<<rPrimaryConditions.size()<<std::endl;

    return true;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool AddContactElementPatchB(ModelPart& rModelPart, Condition const &rReferenceCondition, GeometryType &rVertices, PropertiesType::Pointer &pProperties, unsigned int &id)
  {
    //search the model part conditions sharing edges with the contact domain PATCH-B element
    GlobalPointersVector<Condition> PrimaryConditions;
    Condition::Pointer pMainCondition;

    DenseMatrix<unsigned int> lpofa; //points that define the faces
    rReferenceCondition.GetGeometry().NodesInFaces(lpofa);

    if (SearchUtilities::FindMainEdgeConditionsAndReorderVertices(rModelPart.Conditions(), rVertices, pMainCondition, PrimaryConditions, lpofa))
    {
      const ProcessInfo& rCurrentProcessInfo = rModelPart.GetProcessInfo();

      if (rCurrentProcessInfo[SPACE_DIMENSION]==3)
	if (IsDistortedPatchB(rModelPart, rVertices, PrimaryConditions, pMainCondition->GetValue(NORMAL))){
          if (mEchoLevel > 0)
            std::cout<<" ContactElement PATCH-B distorted "<<std::endl;
	  return false; //to discard element
        }

      for (auto &i_cond : PrimaryConditions)
	TransferUtilities::InitializeBoundaryData(i_cond, mTransferVariables, rCurrentProcessInfo);

      // std::cout<<"Patch-B contact masters "<<PrimaryConditions.size()<<std::endl;

      Condition::Pointer pContactCondition = rReferenceCondition.Create(++id, rVertices, pProperties->pGetSubProperties(pProperties->Id() + 1));

      pContactCondition->SetValue(PRIMARY_CONDITIONS, PrimaryConditions);

      //get the primary element // condition is a face condition sharing edge with contact PATCH-B element
      Element &rMainElement = pMainCondition->GetValue(PRIMARY_ELEMENTS).front();  //must have only one PRIMARY_ELEMENT

      //const GeometryType &cGeometry = pMainCondition->GetGeometry();
      const GeometryType &rGeometry = pContactCondition->GetGeometry();

      //set primary element and primary nodes
      constexpr std::array<unsigned int,4> permute {2,3,1,2};
      std::vector<unsigned int> global_contact_edge;
      global_contact_edge.push_back(rGeometry[0].Id());
      global_contact_edge.push_back(rGeometry[1].Id());

      // std::cout<<" global_contact_edge "<<global_contact_edge<<" Geometry 0,1 ["<<rGeometry[0].Id()<<", "<<rGeometry[1].Id()<<"]"<<std::endl;

      DenseMatrix<unsigned int> lpofa; //points that define the faces
      rGeometry.NodesInFaces(lpofa);
      // std::cout<<" lpofa "<<lpofa<<std::endl;

      ElementWeakPtrVectorType &nElements = rMainElement.GetValue(NEIGHBOUR_ELEMENTS);

      for (auto i_nelem(nElements.begin()); i_nelem != nElements.end(); ++i_nelem)
      {
	GeometryType &eGeometry = i_nelem->GetGeometry();
	unsigned int counter = 0;
	std::vector<unsigned int> local_edge(2);
	for (unsigned int i = 0; i < eGeometry.size(); ++i)
	{
	  for (unsigned int j = 0; j < global_contact_edge.size(); ++j)
	  {
	    if (global_contact_edge[j] == eGeometry[i].Id())
	    {
	      local_edge[j] = i;
	      ++counter;
	      break;
	    }
	  }
	}

	if (counter == 2)
	{
	  pContactCondition->GetValue(PRIMARY_ELEMENTS).push_back(*i_nelem.base());
	  // std::cout<<" local_edge "<<local_edge<<std::endl;
	  for (unsigned int i = 1; i <= 3; ++i)
	  {
	    if (lpofa(i,local_edge.front()) == local_edge.back())
	    {
	      // std::cout<<" m "<<lpofa(permute[i-1],local_edge.front()) <<" n "<<lpofa(permute[i],local_edge.front()) <<std::endl;
	      pContactCondition->GetValue(PRIMARY_NODES).push_back(eGeometry(lpofa(permute[i-1],local_edge.front())));
	      pContactCondition->GetValue(PRIMARY_NODES).push_back(eGeometry(lpofa(permute[i],local_edge.front())));
	      break;
	    }
	  }
	  break;
	}
      }

      //set the condition NORMAL (not sure it is needed)
      pContactCondition->SetValue(NORMAL, pMainCondition->GetValue(NORMAL));

      //add a mean value of the normal (not sure it is needed)
      for (auto &i_cond : PrimaryConditions)
      {
	if (i_cond.Id() != pMainCondition->Id())
	{
	  GeometryType &cGeometry = i_cond.GetGeometry();
	  unsigned int counter = 0;
	  for (unsigned int i = 0; i < cGeometry.size(); ++i)
	  {
	    if (global_contact_edge[0] == cGeometry[i].Id() || global_contact_edge[1] == cGeometry[i].Id())
	      ++counter;
	  }
	  if (counter == 2)
	  {
	    array_1d<double, 3> Normal = pContactCondition->GetValue(NORMAL) + i_cond.GetValue(NORMAL);
	    double norm = norm_2(Normal);
	    if (norm != 0)
	      Normal /= norm;
	    // std::cout<<" Set NORMAL "<<Normal<<std::endl;
	    pContactCondition->SetValue(NORMAL, Normal);
	  }
	}
      }

      // std::cout<<"Patch-B["<<pContactCondition->Id()<<"] m_conditions "<<pContactCondition->GetValue(PRIMARY_CONDITIONS).size()<<std::endl;
      // std::cout<<"Patch-B["<<pContactCondition->Id()<<"] m_elements "<<pContactCondition->GetValue(PRIMARY_ELEMENTS).size()<<std::endl;
      // std::cout<<"Patch-B["<<pContactCondition->Id()<<"] m_nodes "<<pContactCondition->GetValue(PRIMARY_NODES).size()<<std::endl;
      // std::cout<<std::endl;
      //set ACTIVE if it is going to be considered in computation:
      //here one can check the geometrical gap to dismiss some contact elements here
      //it will be done later in the contact element calculation
      pContactCondition->Set(ACTIVE);

      //meaning that is a element that shares edges instead of faces PATCH-B
      pContactCondition->Set(FREE_SURFACE, true);

      //setting new elements
      mrModelPart.AddCondition(pContactCondition);

      return true;
    }

    return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************


  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{
  ModelPart &mrModelPart;

  MesherData::MeshingParameters &mrRemesh;

  TransferUtilities::DataVariables mTransferVariables;

  int mEchoLevel;

  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{

  /// Assignment operator.
  GenerateNewContactConditionsMesherProcess &operator=(GenerateNewContactConditionsMesherProcess const &rOther);

  /// Copy constructor.
  //GenerateNewContactConditionsMesherProcess(GenerateNewContactConditionsMesherProcess const& rOther);

  ///@}

}; // Class GenerateNewContactConditionsMesherProcess

///@}

///@name Type Definitions
///@{
///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                GenerateNewContactConditionsMesherProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const GenerateNewContactConditionsMesherProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_GENERATE_NEW_CONTACT_CONDITIONS_MESHER_PROCESS_HPP_INCLUDED  defined
