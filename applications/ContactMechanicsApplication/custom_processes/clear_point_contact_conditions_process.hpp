//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                August 2016 $
//
//

#if !defined(KRATOS_CLEAR_POINT_CONTACT_CONDITIONS_PROCESS_HPP_INCLUDED)
#define KRATOS_CLEAR_POINT_CONTACT_CONDITIONS_PROCESS_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "processes/process.h"

namespace Kratos
{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{
typedef ModelPart::NodesContainerType NodesContainerType;
typedef ModelPart::ElementsContainerType ElementsContainerType;
typedef ModelPart::ConditionsContainerType ConditionsContainerType;

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
   */
class ClearPointContactConditionsProcess
    : public Process
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of ClearPointContactConditionsProcess
  KRATOS_CLASS_POINTER_DEFINITION(ClearPointContactConditionsProcess);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  ClearPointContactConditionsProcess(ModelPart &rModelPart,
                                     int EchoLevel = 0)
      : mrModelPart(rModelPart)
  {
    mEchoLevel = EchoLevel;
  }

  /// Destructor.
  virtual ~ClearPointContactConditionsProcess()
  {
  }

  ///@}
  ///@name Operators
  ///@{

  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  void Execute() override
  {
    KRATOS_TRY

    this->ClearContactConditions(mrModelPart);

    KRATOS_CATCH(" ")
  };

  /// this function is designed for being called at the beginning of the computations
  /// right after reading the model and the groups
  void ExecuteInitialize() override
  {
    KRATOS_TRY

    this->Execute();

    KRATOS_CATCH("")
  }

  /// this function is designed for being called at the end of the computations
  /// right after reading the model and the groups
  void ExecuteFinalize() override
  {
    KRATOS_TRY

    // Clear Contact Forces
    this->ClearContactForce(mrModelPart);

    // Clear Contact Normals
    this->ClearContactNormal(mrModelPart);

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "ClearPointContactConditionsProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "ClearPointContactConditionsProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  ModelPart &mrModelPart;

  int mEchoLevel;

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  //**************************************************************************
  //**************************************************************************

  void ClearContactForce(ModelPart &rModelPart)
  {
    KRATOS_TRY

    const std::vector<Flags> Flags = {BOUNDARY, CONTACT.AsFalse()};

    block_for_each(rModelPart.Nodes(),[&](Node<3>& i_node){
      if (i_node.Is(Flags))
        noalias(i_node.FastGetSolutionStepValue(CONTACT_FORCE)) = ZeroVector(3);
    });

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  void ClearContactNormal(ModelPart &rModelPart)
  {
    KRATOS_TRY

    const std::vector<Flags> Flags = {};

    block_for_each(rModelPart.Nodes(),[&](Node<3>& i_node){
      if (i_node.Is(Flags))
        noalias(i_node.FastGetSolutionStepValue(CONTACT_NORMAL)) = ZeroVector(3);
    });

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  void ClearContactConditions(ModelPart &rModelPart)
  {

    KRATOS_TRY

    ModelPart &rParentModelPart = rModelPart.GetParentModelPart();

    const std::vector<Flags> NotFlags = {BOUNDARY};
    for (auto &i_mp : rParentModelPart.SubModelParts())
    {
      if (i_mp.IsNot(NotFlags))
        ClearPointContactConditions(i_mp);
    }

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  void ClearPointContactConditions(ModelPart &rModelPart)
  {

    KRATOS_TRY

    if (mEchoLevel > 1)
      std::cout << " [" << rModelPart.Name() << " :: CONDITIONS [OLD:" << rModelPart.NumberOfConditions();

    ModelPart::ConditionsContainerType PreservedConditions;

    for (ModelPart::ConditionsContainerType::iterator i_cond = rModelPart.ConditionsBegin(); i_cond != rModelPart.ConditionsEnd(); i_cond++)
    {
      if (!(i_cond->Is(CONTACT) && i_cond->GetGeometry().size() == 1))
      {
        PreservedConditions.push_back(*(i_cond.base()));
      }
    }

    rModelPart.Conditions().swap(PreservedConditions);

    if (mEchoLevel > 1)
      std::cout << " / NEW:" << rModelPart.NumberOfConditions() << "] " << std::endl;

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  /// Assignment operator.
  ClearPointContactConditionsProcess &operator=(ClearPointContactConditionsProcess const &rOther);

  /// Copy constructor.
  //ClearPointContactConditionsProcess(ClearPointContactConditionsProcess const& rOther);

  ///@}

}; // Class ClearPointContactConditionsProcess

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                ClearPointContactConditionsProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const ClearPointContactConditionsProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_CLEAR_POINT_CONTACT_CONDITIONS_PROCESS_HPP_INCLUDED  defined
