//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:             LMonforte $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:               January 2018 $
//
//


// ATTENTION Lluis: this process must be reviewed 09/2020 and adapted to the new standard

#if !defined(KRATOS_HM_PARAMETRIC_WALL_CONTACT_SEARCH_PROCESS_HPP_INCLUDED)
#define KRATOS_HM_PARAMETRIC_WALL_CONTACT_SEARCH_PROCESS_HPP_INCLUDED

// External includes

// System includes

// Project includes
#include "custom_processes/parametric_wall_contact_search_process.hpp"

#include "custom_conditions/hydraulic_contact/hydraulic_rigid_contact_penalty_3D_condition.hpp"
#include "custom_conditions/hydraulic_contact/hydraulic_axisymmetric_rigid_contact_penalty_2D_condition.hpp"

namespace Kratos
{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@name Kratos Classes
///@{

class HMParametricWallContactSearchProcess
    : public ParametricWallContactSearchProcess
{
public:
   ///@name Type Definitions
   ///@{
   typedef ModelPart::NodeType NodeType;
   typedef ModelPart::ConditionType ConditionType;
   typedef ModelPart::PropertiesType PropertiesType;
   typedef ConditionType::GeometryType GeometryType;
   typedef Point2D<ModelPart::NodeType> Point2DType;
   typedef Point3D<ModelPart::NodeType> Point3DType;

   /// Pointer definition of Process
   KRATOS_CLASS_POINTER_DEFINITION(HMParametricWallContactSearchProcess);

   ///@}
   ///@name Life Cycle
   ///@{

   /// Default constructor.
   HMParametricWallContactSearchProcess(ModelPart &rModelPart) : ParametricWallContactSearchProcess(rModelPart) {}

   /// Constructor.
   HMParametricWallContactSearchProcess(ModelPart &rModelPart,
                                        std::string rContactModelPartName,
                                        PropertiesType::Pointer pContactProperties,
                                        SpatialBoundingBox::Pointer pParametricWall,
                                        Parameters CustomParameters)
     : ParametricWallContactSearchProcess(rModelPart, rContactModelPartName, pContactProperties, pParametricWall, CustomParameters)
   {
      KRATOS_TRY

      mpConditionType->SetValue(HYDRAULIC, false);

      mpHydraulicConditionType = CreateConditionPrototypeHM(CustomParameters);
      mpHydraulicConditionType->SetValue(HYDRAULIC, true);

      KRATOS_CATCH(" ")
   }

   /// Destructor.
   virtual ~HMParametricWallContactSearchProcess() {}

   ///@}
   ///@name Operators
   ///@{

   /// This operator is provided to call the process as a function and simply calls the Execute method.
   void operator()()
   {
      Execute();
   }

   ///@}
   ///@name Operations
   ///@{
   ///@}
   ///@name Access
   ///@{
   ///@}
   ///@name Inquiry
   ///@{
   ///@}
   ///@name Input and output
   ///@{

   /// Turn back information as a string.
   virtual std::string Info() const override
   {
      return "HMParametricWallContactSearchProcess";
   }

   /// Print information about this object.
   virtual void PrintInfo(std::ostream &rOStream) const override
   {
      rOStream << "HMParametricWallContactSearchProcess";
   }

   /// Print object's data.
   virtual void PrintData(std::ostream &rOStream) const override
   {
   }

   ///@}
   ///@name Friends
   ///@{
   ///@}

protected:
   ///@name Protected static Member Variables
   ///@{
   ///@}
   ///@name Protected member Variables
   ///@{
   ///@}
   ///@name Protected Operators
   ///@{

   void CreateContactConditions() override
   {
      KRATOS_TRY

      const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();
      double Dimension = rCurrentProcessInfo[SPACE_DIMENSION];

      ModelPart::ConditionsContainerType ContactConditions;

      ModelPart &rContactModelPart = mrModelPart.GetSubModelPart(mContactModelPartName);

      if (mEchoLevel > 1)
      {
         std::cout << "    [" << rContactModelPart.Name() << " :: CONDITIONS [OLD:" << rContactModelPart.NumberOfConditions();
      }

      unsigned int id = mrModelPart.Conditions().back().Id() + 1;

      ModelPart::NodesContainerType &rNodes = mrModelPart.Nodes();

      // create contact condition for rigid and deformable bodies
      for (ModelPart::NodesContainerType::ptr_iterator nd = rNodes.ptr_begin(); nd != rNodes.ptr_end(); ++nd)
      {
         if ((*nd)->Is(BOUNDARY) && (*nd)->Is(CONTACT))
         {

            ConditionType::Pointer pCondition;

            if ((*nd)->Is(RIGID))
            { //rigid wall contacting with a rigid body

               GeometryType::Pointer pGeometry;
               if (Dimension == 2)
                  pGeometry = Kratos::make_shared<Point2DType>(*nd);
               else if (Dimension == 3)
                  pGeometry = Kratos::make_shared<Point3DType>(*nd);

               //pCondition= Kratos::make_intrusive<RigidBodyPointRigidContactCondition>(id, pGeometry, mpProperties, mpParametricWall);

               ContactConditions.push_back(pCondition);
            }
            else
            { //rigid wall contacting with a deformable body

               Condition::NodesArrayType pConditionNode;
               pConditionNode.push_back((*nd));

               ConditionType::Pointer pConditionType = FindPointConditionHM(rContactModelPart, (*nd), false);
               pCondition = pConditionType->Clone(id, pConditionNode);
               pCondition->Set(CONTACT);
               pCondition->SetValue(HYDRAULIC, false);
               ContactConditions.push_back(pCondition);

               pConditionType = FindPointConditionHM(rContactModelPart, (*nd), true);
               pCondition = pConditionType->Clone(id + 1, pConditionNode);
               pCondition->Set(CONTACT);
               pCondition->SetValue(HYDRAULIC, true);
               ContactConditions.push_back(pCondition);
            }

            id += 2;
         }
      }

      rContactModelPart.Conditions().swap(ContactConditions);

      if (mEchoLevel > 1)
      {
         std::cout << " / NEW:" << rContactModelPart.NumberOfConditions() << "] " << std::endl;
      }

      std::string ModelPartName;

      //Add contact conditions to computing domain
      for (ModelPart::SubModelPartIterator i_mp = mrModelPart.SubModelPartsBegin(); i_mp != mrModelPart.SubModelPartsEnd(); i_mp++)
      {
         if (i_mp->Is(SOLID) && i_mp->Is(ACTIVE))
            ModelPartName = i_mp->Name();
      }

      AddContactConditions(rContactModelPart, mrModelPart.GetSubModelPart(ModelPartName));

      //Add contact conditions to  main domain( with AddCondition are already added )
      //AddContactConditions(rContactModelPart, mrModelPart);

      if (mEchoLevel >= 1)
         std::cout << "  [CONTACT CANDIDATES : " << rContactModelPart.NumberOfConditions() << "] (" << mContactModelPartName << ") " << std::endl;

      KRATOS_CATCH("")
   }

   ///@}
   ///@name Protected Operations
   ///@{
   ///@}
   ///@name Protected  Access
   ///@{
   ///@}
   ///@name Protected Inquiry
   ///@{
   ///@}
   ///@name Protected LifeCycle
   ///@{

   ///@}

private:
   ///@name Static Member Variables
   ///@{
   ///@}
   ///@name Member Variables
   ///@{

   ConditionType::Pointer mpHydraulicConditionType;

   ///@}
   ///@name Private Operators
   ///@{

   //**************************************************************************
   //**************************************************************************

   ConditionType::Pointer CreateConditionPrototypeHM(Parameters &CustomParameters)
   {
      KRATOS_TRY

      const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();
      const double &dimension = rCurrentProcessInfo[SPACE_DIMENSION];

      // create geometry prototype for the contact conditions
      GeometryType::Pointer pGeometry;
      if (dimension == 2)
         pGeometry = Kratos::make_shared<Point2DType>(*((mrModelPart.Nodes().begin()).base()));
      else if (dimension == 3)
         pGeometry = Kratos::make_shared<Point3DType>(*((mrModelPart.Nodes().begin()).base()));

      unsigned int LastConditionId = 1;
      if (mrModelPart.NumberOfConditions() != 0)
         LastConditionId = mrModelPart.Conditions().back().Id() + 1;

      std::string ConditionName = CustomParameters["hydraulic_condition_type"].GetString();

      if (ConditionName == "HydraulicPointContactCondition2D1N")
      {
         return Kratos::make_intrusive<HydraulicRigidContactPenalty3DCondition>(LastConditionId, pGeometry, mpProperties, mpParametricWall);
      }
      else if (ConditionName == "HydraulicAxisymPointContactCondition2D1N")
      {
         return Kratos::make_intrusive<HydraulicAxisymmetricRigidContactPenalty2DCondition>(LastConditionId, pGeometry, mpProperties, mpParametricWall);
      }
      else
      {
         std::cout << ConditionName << std::endl;
         KRATOS_ERROR << "the specified hydraulic contact condition does not exist " << std::endl;
      }

      return NULL;

      KRATOS_CATCH("")
   }

   ///@}
   ///@name Private Operations
   ///@{

   ConditionType::Pointer FindPointConditionHM(ModelPart &rModelPart, Node<3>::Pointer pPoint, bool rHydraulic)
   {
      KRATOS_TRY

      for (ModelPart::ConditionsContainerType::iterator i_cond = rModelPart.ConditionsBegin(); i_cond != rModelPart.ConditionsEnd(); ++i_cond)
      {
         if (i_cond->Is(CONTACT) && i_cond->GetGeometry().size() == 1)
         {
            if (i_cond->GetGeometry()[0].Id() == pPoint->Id())
            {
               if (i_cond->GetValue(HYDRAULIC) == rHydraulic)
               {
                  return (*(i_cond.base()));
               }
            }
         }
      }

      if (rHydraulic == false)
      {
         return mpConditionType;
      }
      return mpHydraulicConditionType;

      KRATOS_CATCH("")
   }

   ///@}
   ///@name Private  Access
   ///@{

   ///@}
   ///@name Private Inquiry
   ///@{

   ///@}
   ///@name Un accessible methods
   ///@{

   /// Assignment operator.
   HMParametricWallContactSearchProcess &operator=(HMParametricWallContactSearchProcess const &rOther);

   /// Copy constructor.
   //Process(Process const& rOther);

   ///@}

}; // Class Process

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                HMParametricWallContactSearchProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const HMParametricWallContactSearchProcess &rThis)
{
   rThis.PrintInfo(rOStream);
   rOStream << std::endl;
   rThis.PrintData(rOStream);

   return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_HM_PARAMETRIC_WALL_CONTACT_SEARCH_PROCESS_HPP_INCLUDED  defined
