//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                August 2016 $
//
//

#if !defined(KRATOS_PARAMETRIC_WALL_CONTACT_SEARCH_PROCESS_HPP_INCLUDED)
#define KRATOS_PARAMETRIC_WALL_CONTACT_SEARCH_PROCESS_HPP_INCLUDED

// External includes

// System includes

// Project includes
#include "geometries/point_2d.h"
#include "geometries/point_3d.h"
#include "custom_utilities/set_flags_utilities.hpp"

#include "contact_mechanics_application_variables.h"

namespace Kratos
{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@name Kratos Classes
///@{


class ParametricWallContactSearchProcess
    : public Process
{
public:
  ///@name Type Definitions
  ///@{
  typedef ModelPart::NodeType NodeType;
  typedef ModelPart::ConditionType ConditionType;
  typedef ModelPart::PropertiesType PropertiesType;
  typedef ConditionType::GeometryType GeometryType;
  typedef Point2D<ModelPart::NodeType> Point2DType;
  typedef Point3D<ModelPart::NodeType> Point3DType;
  typedef FrictionLaw::Pointer FrictionLawType;

  /// Pointer definition of Process
  KRATOS_CLASS_POINTER_DEFINITION(ParametricWallContactSearchProcess);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  ParametricWallContactSearchProcess(ModelPart &rModelPart) : mrModelPart(rModelPart) {}

  /// Constructor.
  ParametricWallContactSearchProcess(ModelPart &rModelPart,
                                     std::string ContactModelPartName,
                                     PropertiesType::Pointer pContactProperties,
                                     SpatialBoundingBox::Pointer pParametricWall,
                                     Parameters CustomParameters)
    : mrModelPart(rModelPart), mContactModelPartName(ContactModelPartName), mpProperties(pContactProperties), mpParametricWall(pParametricWall)
  {
    KRATOS_TRY

    mEchoLevel = 1;

    Parameters DefaultParameters(R"(
     {
      "contact_condition": "PointContactCondition2D1N",
      "hydraulic_condition_type": "HydraulicPointContactCondition2D1N",
      "friction_law": "FrictionLaw",
      "friction_properties":{
        "FRICTION_ACTIVE": false,
        "MU_STATIC": 0.3,
        "MU_DYNAMIC": 0.2,
        "DESIRED_CONTACT_FRICTION_ANGLE": 20.0,
        "PENALTY_PARAMETER": 1000,
        "TANGENTIAL_PENALTY_RATIO": 0.1,
        "TAU_STAB": 1
       }
     }  )");

    //validate against defaults -- this also ensures no type mismatch
    CustomParameters.ValidateAndAssignDefaults(DefaultParameters);

    //create condition prototype:
    mpConditionType = CreateConditionPrototype(CustomParameters);

    if (mpConditionType.get() == nullptr)
      std::cout << " ERROR:: PROTOTYPE CONTACT WALL CONDITION NOT DEFINED PROPERLY " << std::endl;

    KRATOS_CATCH(" ")
  }

  /// Destructor.
  virtual ~ParametricWallContactSearchProcess() {}

  ///@}
  ///@name Operators
  ///@{

  /// This operator is provided to call the process as a function and simply calls the Execute method.
  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  /// Execute method is used to execute the Process algorithms.
  void Execute() override
  {
    KRATOS_TRY

    if (mEchoLevel > 1)
      std::cout << "  [PARAMETRIC_CONTACT_SEARCH]:: -START- " << std::endl;

    //update parametric wall position
    //const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();
    //const double Time = rCurrentProcessInfo[TIME];
    //mpParametricWall->UpdateBoxPosition(Time); //already updated in InitializeSearch python process

    //clear contact flags
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{CONTACT}}, {CONTACT.AsFalse()});

    //search contact conditions
    this->SearchContactConditions();

    //create contact conditions
    this->CreateContactConditions();

    if (mEchoLevel > 1)
      std::cout << "  [PARAMETRIC_CONTACT_SEARCH]:: -END- " << std::endl;

    KRATOS_CATCH("")
  }

  /// this function is designed for being called at the beginning of the computations
  /// right after reading the model and the groups
  void ExecuteInitialize() override
  {
    KRATOS_TRY

    int spline_id = -1;
    double arch_length = 0.0;
    // set BOUNDARY flag to nodes of structural elements (beam elements) :: NOT_RIGID and BOUNDARY -> CONTACT
    for (auto &i_mp: mrModelPart.SubModelParts())
    {
      if (i_mp.Is(SOLID))
      {
        for (auto &i_elem: i_mp.Elements())
        {
          GeometryType &eGeometry = i_elem.GetGeometry();
          PropertiesType &rProperties = i_elem.GetProperties();
          double radius = 0.0;
          if (rProperties.Has(CROSS_SECTION_RADIUS))
            radius = rProperties[CROSS_SECTION_RADIUS];

          if (eGeometry.LocalSpaceDimension() == 1 && eGeometry.size() > 1)
          {
            for (auto &i_node: eGeometry)
            {
              i_node.Set(BOUNDARY,true);
              // add flags to fasten spline search
              i_node.SetValue(SPLINE_ID, spline_id);
              i_node.SetValue(NORMALIZED_ARCH_LENGTH, arch_length);
              i_node.SetValue(CROSS_SECTION_RADIUS, radius);
              //std::cout<<" Assign Radius ["<<i_node.Id()<<"]: "<<radius<<std::endl;
            }
          }
        }

      }
    }

    //Initialize contact area
    ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();
    rCurrentProcessInfo[CONTACT_AREA] = 0;

    KRATOS_CATCH("")
  }

  /// this function is designed for being execute once before the solution loop but after all of the
  /// solvers where built
  void ExecuteBeforeSolutionLoop() override
  {
  }

  /// this function will be executed at every time step BEFORE performing the solve phase
  void ExecuteInitializeSolutionStep() override
  {
  }

  /// this function will be executed at every time step AFTER performing the solve phase
  void ExecuteFinalizeSolutionStep() override
  {
  }

  /// this function will be executed at every time step BEFORE  writing the output
  void ExecuteBeforeOutputStep() override
  {
  }

  /// this function will be executed at every time step AFTER writing the output
  void ExecuteAfterOutputStep() override
  {
  }

  /// this function is designed for being called at the end of the computations
  /// right after reading the model and the groups
  void ExecuteFinalize() override
  {
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "ParametricWallContactSearchProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "ParametricWallContactSearchProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ModelPart &mrModelPart;

  std::string mContactModelPartName;

  PropertiesType::Pointer mpProperties;

  SpatialBoundingBox::Pointer mpParametricWall;

  ConditionType::Pointer mpConditionType;

  int mEchoLevel;

  ///@}
  ///@name Protected Operators
  ///@{

  virtual void CreateContactConditions()
  {
    KRATOS_TRY

    ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();
    double Dimension = rCurrentProcessInfo[SPACE_DIMENSION];

    ModelPart::ConditionsContainerType ContactConditions;

    ModelPart &rContactModelPart = mrModelPart.GetSubModelPart(mContactModelPartName);


    if (mEchoLevel > 1)
    {
      std::cout << "    [" << rContactModelPart.Name() << " :: CONDITIONS [OLD:" << rContactModelPart.NumberOfConditions();
    }

    unsigned int id = 1;
    if (mrModelPart.NumberOfConditions() > 0)
      id = mrModelPart.Conditions().back().Id() + 1;

    ModelPart::NodesContainerType &rNodes = mrModelPart.Nodes();

    // create contact condition for rigid and deformable bodies
    for (ModelPart::NodesContainerType::ptr_iterator nd = rNodes.ptr_begin(); nd != rNodes.ptr_end(); ++nd)
    {
      if ((*nd)->Is(BOUNDARY) && (*nd)->Is(CONTACT))
      {

        ConditionType::Pointer pCondition;

        if ((*nd)->Is(RIGID))
        { //rigid wall contacting with a rigid body

          GeometryType::Pointer pGeometry;
          if (Dimension == 2)
            pGeometry = Kratos::make_shared<Point2DType>(*nd);
          else if (Dimension == 3)
            pGeometry = Kratos::make_shared<Point3DType>(*nd);

          ContactConditions.push_back(pCondition);
        }
        else
        { //rigid wall contacting with a deformable body

          Condition::NodesArrayType pConditionNode;
          pConditionNode.push_back((*nd));

          ConditionType::Pointer pConditionType = FindPointCondition(rContactModelPart, (*nd));

          pCondition = pConditionType->Clone(id, pConditionNode);

          pCondition->Set(CONTACT);

          ContactConditions.push_back(pCondition);
        }

        id += 1;
      }
    }

    rContactModelPart.Conditions().swap(ContactConditions);

    if (mEchoLevel > 1)
    {
      std::cout << " / NEW:" << rContactModelPart.NumberOfConditions() << "] " << std::endl;
    }

    //Add contact conditions to computing domain
    for (ModelPart::SubModelPartIterator i_mp = mrModelPart.SubModelPartsBegin(); i_mp != mrModelPart.SubModelPartsEnd(); i_mp++)
    {
      if (i_mp->Is(ACTIVE) && i_mp->IsNot(THERMAL))
        AddContactConditions(rContactModelPart, mrModelPart.GetSubModelPart(i_mp->Name()));
    }

    rCurrentProcessInfo[CONTACT_AREA] = CalculateContactArea(rContactModelPart);

    //Add contact conditions to  main domain( with AddCondition are already added )
    //AddContactConditions(rContactModelPart, mrModelPart);

    if (mEchoLevel >= 1)
      std::cout << "  [CONTACT CANDIDATES : " << rContactModelPart.NumberOfConditions() << "] (" << mContactModelPartName << ") " << std::endl;

    KRATOS_CATCH("")
  }


  //**************************************************************************
  //**************************************************************************

  double CalculateContactArea(ModelPart &rContactModelPart)
  {
    KRATOS_TRY

    double area = 0;
    for (auto& ic : rContactModelPart.Conditions())
    {
      if (ic.Is(CONTACT))
      {
        GeometryType &cGeometry = ic.GetGeometry();
        if ( cGeometry.size() == 1 )
        {
          NodeWeakPtrVectorType &nNodes = cGeometry[0].GetValue(NEIGHBOUR_NODES);
          int counter = 0;
          double distance = 0;
          for (auto &i_nnode : nNodes)
          {
            if (i_nnode.Is(BOUNDARY)){
              distance += norm_2(cGeometry[0].Coordinates() - i_nnode.Coordinates());
              ++counter;
            }
          }

          if( counter != 0)
            distance /= counter;

          area += distance;

        }
        else{
          area += cGeometry.DomainSize();
        }
      }

    }

    return area;

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  void AddContactConditions(ModelPart &rOriginModelPart, ModelPart &rDestinationModelPart)
  {

    KRATOS_TRY

    //*******************************************************************
    //adding contact conditions
    //

    if (mEchoLevel > 1)
    {
      std::cout << "    [" << rDestinationModelPart.Name() << " :: CONDITIONS [OLD:" << rDestinationModelPart.NumberOfConditions();
    }

    for (ModelPart::ConditionsContainerType::iterator ic = rOriginModelPart.ConditionsBegin(); ic != rOriginModelPart.ConditionsEnd(); ic++)
    {

      if (ic->Is(CONTACT))
        rDestinationModelPart.AddCondition(*(ic.base()));
    }

    if (mEchoLevel > 1)
    {
      std::cout << " / NEW:" << rDestinationModelPart.NumberOfConditions() << "] " << std::endl;
    }

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Protected Operations
  ///@{
  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{

  //**************************************************************************
  //**************************************************************************
  ConditionType::Pointer CreateConditionPrototype(Parameters &CustomParameters)
  {
    KRATOS_TRY

    const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();
    const double &dimension = rCurrentProcessInfo[SPACE_DIMENSION];


    // Friction Law is not a Kratos Component
    // std::string FrictionLawName = CustomParameters["friction_law_type"].GetString();
    // const FrictionLawType pCloneFrictionLaw = KratosComponents<FrictionLawType>::Get(FrictionLawName);
    // mpProperties->SetValue(FRICTION_LAW, pCloneFrictionLaw->Clone() );
    // This process is done in python

    Parameters CustomProperties = CustomParameters["friction_properties"];

    double young_modulus = 0;
    double thickness = 1.0;
    for (auto &prop : *mrModelPart.pProperties())
    {
      if (prop.Has(YOUNG_MODULUS))
        if (young_modulus < prop[YOUNG_MODULUS])
          young_modulus = prop[YOUNG_MODULUS];
      if (prop.Has(THICKNESS))
        thickness = prop[THICKNESS];
    }

    mpProperties->SetValue(YOUNG_MODULUS, young_modulus);
    mpProperties->SetValue(FRICTION_ACTIVE, CustomProperties["FRICTION_ACTIVE"].GetBool());
    mpProperties->SetValue(MU_STATIC, CustomProperties["MU_STATIC"].GetDouble());
    mpProperties->SetValue(MU_DYNAMIC, CustomProperties["MU_DYNAMIC"].GetDouble());
    mpProperties->SetValue(DESIRED_CONTACT_FRICTION_ANGLE, CustomProperties["DESIRED_CONTACT_FRICTION_ANGLE"].GetDouble());
    mpProperties->SetValue(PENALTY_PARAMETER, CustomProperties["PENALTY_PARAMETER"].GetDouble());
    mpProperties->SetValue(PENALTY_PARAMETER_SHAFT, CustomProperties["PENALTY_PARAMETER_SHAFT"].GetDouble());
    mpProperties->SetValue(TANGENTIAL_PENALTY_RATIO, CustomProperties["TANGENTIAL_PENALTY_RATIO"].GetDouble());
    mpProperties->SetValue(TAU_STAB, CustomProperties["TAU_STAB"].GetDouble());
    mpProperties->SetValue(THICKNESS, thickness);
    mpProperties->SetValue(CONTACT_FRICTION_ANGLE, 0.0);
    mpProperties->SetValue(BOUNDING_BOX, mpParametricWall);

    mrModelPart.AddProperties(mpProperties);

    // create geometry prototype for the contact conditions
    GeometryType::Pointer pGeometry;
    if (dimension == 2)
      pGeometry = Kratos::make_shared<Point2DType>(*((mrModelPart.Nodes().begin()).base()));
    else if (dimension == 3)
      pGeometry = Kratos::make_shared<Point3DType>(*((mrModelPart.Nodes().begin()).base()));

    // create condition prototype
    std::string ConditionName = CustomParameters["contact_condition"].GetString();

    //------------------//

    unsigned int max_id = mrModelPart.Conditions().back().Id();

    for (auto &i_cond : mrModelPart.Conditions())
      if (i_cond.Id() > max_id)
        max_id = i_cond.Id();

    ++max_id;

    //------------------//

    Condition::NodesArrayType ConditionNodes;
    ConditionNodes.push_back( *((mrModelPart.Nodes().begin()).base()) );
    ConditionType const& rCloneCondition = KratosComponents<ConditionType>::Get(ConditionName);

    return rCloneCondition.Create(max_id, ConditionNodes, mpProperties);

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  void SearchContactConditions()
  {
    KRATOS_TRY

    const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();
    const double &Time = rCurrentProcessInfo[TIME];


    ModelPart::NodesContainerType &rNodes = mrModelPart.Nodes();
    const int nnodes = static_cast<int>(rNodes.size());
#pragma omp parallel for firstprivate(nnodes)
    for (int i = 0; i < nnodes; ++i)
    {
      typename NodesContainerType::iterator i_node = rNodes.begin() + i;

      if (i_node->Is(BOUNDARY) && i_node->IsNot(RIGID))
      {
        //to perform contact with a tube radius must be set
        double Radius = 0;
        if (i_node->IsNot(SLAVE))
        {
          if (i_node->Has(CROSS_SECTION_RADIUS))
            Radius = i_node->GetValue(CROSS_SECTION_RADIUS);
        }
        else
        {
          Radius = 0;
        }

        if (mpParametricWall->IsInside(*i_node, Time, Radius))
        {
          i_node->Set(CONTACT);
        }
      }
    }

    // **************** Serial version of the same search:  **************** //

    // ModelPart::NodesContainerType& rNodes = mrModelPart.Nodes();

    // for(ModelPart::NodesContainerType::const_iterator i_node = rNodes.begin(); i_node != rNodes.end(); i_node++)
    // 	{
    // 	  if( i_node->Is(BOUNDARY) ){

    // 	    if( i_node->IsNot(RIGID) )
    // 	      {
    // 		//to perform contact with a tube radius must be set
    // 		double Radius = 0;

    // 		if( i_node->IsNot(SLAVE) ){
    // 		  //Radius = i_node->GetValue(CROSS_SECTION_RADIUS);
    // 		}
    // 		else{
    // 		  Radius = 0;
    // 		}

    // 		if( mpParametricWall->IsInside(*i_node,Time,Radius) ){
    // 		  i_node->Set(CONTACT);
    // 		}
    // 	      }

    // 	  }

    // 	}

    // **************** Serial version of the same search:  **************** //

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  Condition::Pointer FindPointCondition(ModelPart &rModelPart, Node<3>::Pointer pPoint)
  {

    KRATOS_TRY

    // if restarted rigid contact conditions can not be cloned
    // mpRigidWall (BBX) is rebuild and not stored in the restart save/load
    const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();
    if (rCurrentProcessInfo.Has(IS_RESTARTED) && rCurrentProcessInfo.Has(RESTART_STEP_TIME))
    {
      if (rCurrentProcessInfo[IS_RESTARTED] == true)
      {
        if (rCurrentProcessInfo.GetPreviousTimeStepInfo()[TIME] == rCurrentProcessInfo[RESTART_STEP_TIME])
        { //time updated before this call, it means it has been restarted
          return mpConditionType;
        }
      }
    }

    for (ModelPart::ConditionsContainerType::iterator i_cond = rModelPart.ConditionsBegin(); i_cond != rModelPart.ConditionsEnd(); i_cond++)
    {
      if (i_cond->Is(CONTACT) && i_cond->GetGeometry().size() == 1)
      {
        if (i_cond->GetGeometry()[0].Id() == pPoint->Id())
        {
          return (*(i_cond.base()));
        }
      }
    }

    return mpConditionType;

    KRATOS_CATCH("")
  }

  void RestoreContactFlags()
  {
    KRATOS_TRY

    for (ModelPart::ConditionsContainerType::iterator i_cond = mrModelPart.ConditionsBegin(); i_cond != mrModelPart.ConditionsEnd(); i_cond++)
    {
      if (i_cond->Is(CONTACT))
      {
        for (unsigned int i = 0; i < i_cond->GetGeometry().size(); i++)
        {
          i_cond->GetGeometry()[i].Set(CONTACT, true);
        }
      }
    }

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  /// Assignment operator.
  ParametricWallContactSearchProcess &operator=(ParametricWallContactSearchProcess const &rOther);

  /// Copy constructor.
  //Process(Process const& rOther);

  ///@}

}; // Class Process

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                ParametricWallContactSearchProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const ParametricWallContactSearchProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_PARAMETRIC_WALL_CONTACT_SEARCH_PROCESS_HPP_INCLUDED  defined
