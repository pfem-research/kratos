//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:              November 2021 $
//
//

#if !defined(KRATOS_SELECT_CONTACT_CONDITIONS_MESHER_PROCESS_HPP_INCLUDED)
#define KRATOS_SELECT_CONTACT_CONDITIONS_MESHER_PROCESS_HPP_INCLUDED

// External includes

// System includes

// Project includes
#include "custom_utilities/mesher_utilities.hpp"

#include "custom_processes/mesher_process.hpp"

namespace Kratos
{

///@name Kratos Classes
///@{

/// Refine Mesh Elements Process 2D and 3D
/** The process labels the elements to be refined in the mesher
    it applies a size constraint to elements that must be refined.

*/
class SelectContactConditionsMesherProcess
    : public MesherProcess
{
public:
  ///@name Type Definitions
  ///@{

  typedef Node<3> NodeType;
  typedef Geometry<NodeType> GeometryType;

  /// Pointer definition of Process
  KRATOS_CLASS_POINTER_DEFINITION(SelectContactConditionsMesherProcess);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  SelectContactConditionsMesherProcess(ModelPart &rModelPart,
                              MesherData::MeshingParameters &rRemeshingParameters,
                              int EchoLevel)
      : mrModelPart(rModelPart),
        mrRemesh(rRemeshingParameters)
  {
    mEchoLevel = EchoLevel;
  }

  /// Destructor.
  virtual ~SelectContactConditionsMesherProcess() {}

  ///@}
  ///@name Operators
  ///@{

  /// This operator is provided to call the process as a function and simply calls the Execute method.
  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  /// Execute method is used to execute the Process algorithms.
  void Execute() override
  {
    KRATOS_TRY

    BuiltinTimer time_counter;

    if (mEchoLevel > 0)
      std::cout << " [ SELECT CONTACT ELEMENTS: (" << mrRemesh.OutMesh.GetNumberOfElements() << ") " << std::endl;

    const int &OutNumberOfElements = mrRemesh.OutMesh.GetNumberOfElements();
    mrRemesh.PreservedElements.clear();
    mrRemesh.PreservedElements.resize(OutNumberOfElements);
    std::fill(mrRemesh.PreservedElements.begin(), mrRemesh.PreservedElements.end(), 0);
    mrRemesh.MeshElementsSelectedFlag = true;

    mrRemesh.Info->Current.Elements = 0;

    if (mEchoLevel > 0)
      std::cout << "   Start Contact Element Selection " << OutNumberOfElements << std::endl;

    if (mrRemesh.ExecutionOptions.IsNot(MesherData::SELECT_TESSELLATION_ELEMENTS))
    {
      for (int el = 0; el < OutNumberOfElements; ++el)
      {
        mrRemesh.PreservedElements[el] = 1;
        mrRemesh.Info->Current.Elements += 1;
      }
    }
    else
    {
      this->SelectContactConditions();
      if (mEchoLevel > 0)
        std::cout << "   Finished Contact Element Selection " << std::endl;
    }


    if (mEchoLevel > 0)
    {
      std::cout << "   SELECT CONTACT ELEMENTS (" << mrRemesh.Info->Current.Elements << ") ]; " << std::endl;

      if (mrRemesh.Options.Is(MesherData::CONSTRAINED))
      {
        int released_elements = mrRemesh.OutMesh.GetNumberOfElements() - mrRemesh.Info->Current.Elements;
        if (released_elements > 0)
          std::cout << "   RELEASED CONTACT ELEMENTS (" << released_elements << ") IN CONSTRAINED MESH ]; " << std::endl;
      }
    }

    if( mEchoLevel >= 0 )
      std::cout<<"  SELECT CONTACT ELEMENTS time = "<<time_counter.ElapsedSeconds()<<std::endl;

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "SelectContactConditionsMesherProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "SelectContactConditionsMesherProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{

  ModelPart &mrModelPart;

  MesherData::MeshingParameters &mrRemesh;

  MesherUtilities mMesherUtilities;

  int mEchoLevel;


  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{

  void SelectContactConditions()
  {
    KRATOS_TRY

    const int &OutNumberOfElements = mrRemesh.OutMesh.GetNumberOfElements();

    if (OutNumberOfElements==0)
      KRATOS_ERROR << " 0 elements to select : NO ELEMENTS GENERATED " <<std::endl;

    unsigned int passed_boundary = 0;
    unsigned int passed_alpha_shape = 0;
    unsigned int passed_inner_outer = 0;


    unsigned int dimension = 0;
    unsigned int number_of_vertices = 0;

    this->GetElementDimension(dimension, number_of_vertices);

    const int *OutElementList = mrRemesh.OutMesh.GetElementList();

    ModelPart::NodesContainerType::iterator nodes_begin = mrModelPart.NodesBegin();

    int el = 0;
    int number = 0;

    //set flags for the local process execution
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{VISITED}}, {VISITED.AsFalse()});

    ModelPartUtilities::CheckNodalH(mrModelPart);

    //#pragma omp parallel for reduction(+:number) //get same node from model part and flags set in vertices in parallel it is not threadsafe...
    for (el = 0; el < OutNumberOfElements; ++el)
    {
      GeometryType Vertices;

      bool accepted = this->GetVertices(el, nodes_begin, OutElementList, Vertices, number_of_vertices);

      if (!accepted)
        continue;

      // monitoring bools (for checking purposes)
      // bool boundary_accepted = false;
      // bool alpha_accepted = false;
      // bool subdomain_accepted = false;
      // bool center_accepted = false;
      // bool shape_accepted = false;

      double Alpha = mrRemesh.AlphaParameter;

      MesherUtilities MesherUtils;

      //2.- to control the alpha size
      if (accepted)
      {
        //boundary_accepted = true;
        ++passed_boundary;
        accepted = GeometryUtilities::ShrankAlphaShape(Alpha, Vertices, mrRemesh.OffsetFactor, dimension);
      }

      //3.- to control all nodes from the same subdomain (problem, domain is not already set for new inserted particles on mesher)
      if (accepted)
      {
        //alpha_accepted = true;
        ++passed_alpha_shape;
	if (MesherUtils.SameSubdomain(Vertices)) //self-contact
	  accepted = GeometryUtilities::CheckOuterCentre(Vertices, mrRemesh.OffsetFactor, true);
      }

      //4.- to control that the element has a good shape
      if (accepted)
      {
        //center_accepted = true;
        ++passed_inner_outer;
        accepted = this->CheckElementShape(Vertices, dimension);
      }

      // if all checks have been passed, accept the element
      if (accepted)
      {
        //shape_accepted = true;
        number += 1;
        mrRemesh.PreservedElements[el] = number;
      }

    }

    mrRemesh.Info->Current.Elements = number;

    if (mEchoLevel > 0)
    {
      std::cout << "  [Preserved Contact Elements " << mrRemesh.Info->Current.Elements << "] (" << mrModelPart.NumberOfElements() << ")" << std::endl;
      std::cout << "  (passed_boundary: "<<passed_boundary<<", passed_alpha_shape: " << passed_alpha_shape << ", passed_inner_outer: " << passed_inner_outer << ") accepted: "<<number<<" of " <<OutNumberOfElements<< std::endl;
    }

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool GetVertices(int &el, ModelPart::NodesContainerType::iterator nodes_begin, const int *OutElementList, GeometryType &rVertices, unsigned int &number_of_vertices)
  {
    KRATOS_TRY

    for (unsigned int pn = 0; pn < number_of_vertices; ++pn)
    {
      unsigned int id = el * number_of_vertices + pn;

      //std::cout<<" pn "<<pn<<" id "<<OutElementList[id]<<" pre_ids_size "<<mrRemesh.NodalPreIds.size()<<" preid "<<mrRemesh.NodalPreIds[OutElementList[id]]<<std::endl;

      if (OutElementList[id] <= 0)
        std::cout << " ERROR: something is wrong: nodal id < 0 " << el << std::endl;

      //check if the number of nodes are considered in the nodal pre ids
      if ((unsigned int)OutElementList[id] >= mrRemesh.NodalPreIds.size())
      {
        std::cout << " ERROR: something is wrong: node added by the mesher : " << (unsigned int)OutElementList[id] << std::endl;
        if (mrRemesh.Options.Is(MesherData::CONTACT_SEARCH))
          return false;
      }

      //check if the point is a vertex of an artificial external bounding box
      if (mrRemesh.NodalPreIds[OutElementList[id]] < 0)
      {
        if (mrRemesh.Options.IsNot(MesherData::CONTACT_SEARCH))
        {
          std::cout << " ERROR: something is wrong: nodal id < 0 " << std::endl;
        }
        return false;
      }

      //get node from model part and set it as vertex
      rVertices.push_back(*(nodes_begin + OutElementList[id] - 1).base());
    }

    return true;

    KRATOS_CATCH("")
  }


  //*******************************************************************************************
  //*******************************************************************************************

  void GetElementDimension(unsigned int &dimension, unsigned int &number_of_vertices)
  {
    if (mrModelPart.NumberOfElements())
    {
      ModelPart::ElementsContainerType::iterator element_begin = mrModelPart.ElementsBegin();
      dimension = element_begin->GetGeometry().WorkingSpaceDimension();
      number_of_vertices = element_begin->GetGeometry().size();
    }
    else if (mrModelPart.NumberOfConditions())
    {
      ModelPart::ConditionsContainerType::iterator condition_begin = mrModelPart.ConditionsBegin();
      dimension = condition_begin->GetGeometry().WorkingSpaceDimension();
      if (dimension == 3) //number of nodes of a tetrahedron
        number_of_vertices = 4;
      else if (dimension == 2) //number of nodes of a triangle
        number_of_vertices = 3;
    }
  }


  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckElementShape(GeometryType &rVertices, const unsigned int &rDimension)
  {
    unsigned int NumberOfVertices = rVertices.size();

    if (rDimension == 3 && NumberOfVertices == 4)
    {
      Tetrahedra3D4<NodeType> Tetrahedron(rVertices);

      MesherUtilities MesherUtils;
      return MesherUtils.CheckContactShape(Tetrahedron);
    }

    return true;
  }


  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Private Static Member Variables
  ///@{
  ///@}
  ///@name Private Static Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{
  ///@}

}; // Class Process

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                SelectContactConditionsMesherProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const SelectContactConditionsMesherProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_SELECT_CONTACT_CONDITIONS_MESHER_PROCESS_HPP_INCLUDED defined
