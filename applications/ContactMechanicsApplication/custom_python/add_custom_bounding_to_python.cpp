//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                August 2016 $
//
//

// System includes

// External includes

// Project includes
#include "custom_python/add_custom_bounding_to_python.h"

// Bounding Boxes
#include "custom_bounding/spatial_bounding_box.hpp"
#include "custom_bounding/plane_bounding_box.hpp"
#include "custom_bounding/sphere_bounding_box.hpp"
#include "custom_bounding/circle_bounding_box.hpp"
#include "custom_bounding/cylinder_bounding_box.hpp"
#include "custom_bounding/tube_bounding_box.hpp"
#include "custom_bounding/compound_noses_bounding_box.hpp"

namespace Kratos
{

namespace Python
{

void AddCustomBoundingToPython(pybind11::module &m)
{

    namespace py = pybind11;

    //plane-wall
    py::class_<PlaneBoundingBox, typename PlaneBoundingBox::Pointer, SpatialBoundingBox>(m, "PlaneBoundingBox")
        .def(py::init<ModelPart &, Parameters>())
        .def(py::init<Parameters>())
        .def(py::init<Vector, Vector, Vector, int>())
        .def("UpdateBoxPosition", &PlaneBoundingBox::UpdateBoxPosition)
        .def("CreateBoundingBoxBoundaryMesh", &PlaneBoundingBox::CreateBoundingBoxBoundaryMesh);

    //sphere-wall
    py::class_<SphereBoundingBox, typename SphereBoundingBox::Pointer, SpatialBoundingBox>(m, "SphereBoundingBox")
        .def(py::init<ModelPart &, Parameters>())
        .def(py::init<Parameters>())
        .def(py::init<Vector, double, Vector, int>())
        .def("UpdateBoxPosition", &SphereBoundingBox::UpdateBoxPosition)
        .def("CreateBoundingBoxBoundaryMesh", &SphereBoundingBox::CreateBoundingBoxBoundaryMesh);

    //circle-wall
    py::class_<CircleBoundingBox, typename CircleBoundingBox::Pointer, SphereBoundingBox>(m, "CircleBoundingBox")
        .def(py::init<ModelPart &, Parameters>())
        .def(py::init<Parameters>())
        .def(py::init<Vector, double, Vector, int>())
        .def("UpdateBoxPosition", &CircleBoundingBox::UpdateBoxPosition)
        .def("CreateBoundingBoxBoundaryMesh", &CircleBoundingBox::CreateBoundingBoxBoundaryMesh);

    //cylinder-wall
    py::class_<CylinderBoundingBox, typename CylinderBoundingBox::Pointer, SpatialBoundingBox>(m, "CylinderBoundingBox")
        .def(py::init<ModelPart &, Parameters>())
        .def(py::init<Vector, Vector, double, Vector, int>())
        .def("UpdateBoxPosition", &CylinderBoundingBox::UpdateBoxPosition)
        .def("CreateBoundingBoxBoundaryMesh", &CylinderBoundingBox::CreateBoundingBoxBoundaryMesh);

    //tube-wall
    py::class_<TubeBoundingBox, typename TubeBoundingBox::Pointer, SpatialBoundingBox>(m, "TubeBoundingBox")
        .def(py::init<ModelPart &, Parameters>())
        .def(py::init<ModelPart &, double, int>())
        .def("UpdateBoxPosition", &TubeBoundingBox::UpdateBoxPosition)
        .def("CreateBoundingBoxBoundaryMesh", &TubeBoundingBox::CreateBoundingBoxBoundaryMesh);

    //compound_noses-wall
    py::class_<CompoundNosesBoundingBox, typename CompoundNosesBoundingBox::Pointer, SpatialBoundingBox>(m, "CompoundNosesBoundingBox")
        .def(py::init<ModelPart &, Parameters>())
        .def(py::init<Parameters>())
        .def(py::init<Vector, Vector, Vector, Matrix, Vector, Vector, Vector, Vector, Vector, Matrix>())
        .def("UpdateBoxPosition", &CompoundNosesBoundingBox::UpdateBoxPosition)
        .def("CreateBoundingBoxBoundaryMesh", &CompoundNosesBoundingBox::CreateBoundingBoxBoundaryMesh);
}

} // namespace Python.

} // Namespace Kratos
