//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                August 2016 $
//
//

// System includes

// External includes

// Project includes
#include "custom_python/add_custom_utilities_to_python.h"

// Utilities
#include "custom_utilities/rigid_body_element_creation_utility.hpp"
#include "custom_utilities/contact_domain_utilities.hpp"
#include "custom_utilities/spline_curve_utilities.hpp"

namespace Kratos
{

namespace Python
{

void AddCustomUtilitiesToPython(pybind11::module &m)
{
  namespace py = pybind11;

  py::class_<RigidBodyElementCreationUtility>(m, "RigidBodyCreationUtility")
      .def(py::init<>())
      .def("CreateRigidBody", &RigidBodyElementCreationUtility::CreateRigidBody)
      .def("CreateLinks", &RigidBodyElementCreationUtility::CreateLinks);

  py::class_<SplineCurveUtilities>(m, "SplineCurveUtilities")
      .def(py::init<>())
      .def("CreateDiscretizedCurve", &SplineCurveUtilities::CreateDiscretizedCurve);
}

} // namespace Python.

} // Namespace Kratos
