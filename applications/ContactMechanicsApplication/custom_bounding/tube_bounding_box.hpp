//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                August 2016 $
//
//

#if !defined(KRATOS_TUBE_BOUNDING_BOX_HPP_INCLUDED)
#define KRATOS_TUBE_BOUNDING_BOX_HPP_INCLUDED

// External includes

// System includes

// Project includes
#include "custom_bounding/spatial_bounding_box.hpp"
#include "geometries/quadrilateral_3d_4.h"
#include "custom_utilities/spline_curve_utilities.hpp"

namespace Kratos
{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.

    This Box represents a 3D wall composed by a cylindrical tube

    A convexity parameter is given to determine if
    the internal or external space is considered as boundary

    This bounding box is essentially used for rigid wall contact purposes
*/

class TubeBoundingBox
    : public SpatialBoundingBox
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of TubeBoundingBox
  KRATOS_CLASS_POINTER_DEFINITION(TubeBoundingBox);

  typedef Node<3> NodeType;
  typedef array_1d<double, 3> PointType;
  typedef ModelPart::NodesContainerType NodesContainerType;
  typedef NodesContainerType::Pointer NodesContainerTypePointer;
  typedef ModelPart::ElementsContainerType ElementsContainerType;
  typedef Quaternion<double> QuaternionType;
  typedef ModelPart::ElementType ElementType;
  typedef ElementType::GeometryType GeometryType;
  typedef BoundingBoxData::BoxParameters BoxParametersType;
  typedef BoundingBoxData::BoxVariables  BoxVariablesType;
  typedef SplineCurveUtilities::NodePointerVectorType NodePointerVectorType;

public:
  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  TubeBoundingBox() : SpatialBoundingBox() {}

  //**************************************************************************
  //**************************************************************************

  TubeBoundingBox(ModelPart &rModelPart, Parameters CustomParameters)
  {

    KRATOS_TRY

    Parameters DefaultParameters(R"(
            {
                "parameters_list":[{
                   "radius": 0.0,
                   "convexity": 1
                 }],
                 "plane_size": 1.0,
                 "velocity": [0.0, 0.0, 0.0]
            }  )");

    //validate against defaults -- this also ensures no type mismatch
    CustomParameters.ValidateAndAssignDefaults(DefaultParameters);

    if (CustomParameters["parameters_list"].IsArray() == true && CustomParameters["parameters_list"].size() != 1)
    {
      KRATOS_THROW_ERROR(std::runtime_error, "paramters_list for the Tube BBX must contain only one term", CustomParameters.PrettyPrintJsonString());
    }

    mBox.Initialize();

    Parameters BoxParameters = CustomParameters["parameters_list"][0];

    mBox.Radius = BoxParameters["radius"].GetDouble();

    mBox.Velocity[0] = CustomParameters["velocity"][0].GetDouble();
    mBox.Velocity[1] = CustomParameters["velocity"][1].GetDouble();
    mBox.Velocity[2] = CustomParameters["velocity"][2].GetDouble();

    mBox.Convexity = BoxParameters["convexity"].GetInt();

    mBox.SetInitialValues();

    mRigidBodyCenterSupplied = false;

    this->CreateGeneratrix(rModelPart);

    std::cout << "  [TUBE DEFINED BY: " << mGeneratrix.size() << " control points]" << std::endl;

    this->CreateKnots();

    std::cout << "  [REDEFINED WITH: " << mKnots.size() << " knots]" << std::endl;

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  TubeBoundingBox(ModelPart &rModelPart, double Radius, int Convexity)
  {

    KRATOS_TRY

    mBox.Initialize();

    mBox.Radius = Radius;

    mBox.Convexity = Convexity;

    mBox.SetInitialValues();

    mRigidBodyCenterSupplied = false;

    this->CreateGeneratrix(rModelPart);

    this->CreateKnots();

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  /// Assignment operator.
  TubeBoundingBox &operator=(TubeBoundingBox const &rOther)
  {
    KRATOS_TRY

    SpatialBoundingBox::operator=(rOther);

    mGeneratrix = rOther.mGeneratrix;
    mKnots = rOther.mKnots;

    return *this;

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  /// Copy constructor.
  TubeBoundingBox(TubeBoundingBox const &rOther)
      : SpatialBoundingBox(rOther), mGeneratrix(rOther.mGeneratrix), mKnots(rOther.mKnots)
  {
  }

  //**************************************************************************
  //**************************************************************************

  /// Destructor.
  virtual ~TubeBoundingBox(){};

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  //**************************************************************************
  //**************************************************************************

  void UpdateBoxPosition(const double &rCurrentTime) override
  {
    KRATOS_TRY

    bool movement = false;
    for (const auto& i_node: mGeneratrix)
      if (norm_2(i_node->FastGetSolutionStepValue(DISPLACEMENT)) != 0)
        movement = true;

// Update mesh positions : node coordinates
    // const int nnodes = mGeneratrix.size();
    // ModelPart::NodesContainerType::iterator it_begin = mGeneratrix.begin();

// #pragma omp parallel for
    // for (int i = 0; i < nnodes; i++)
    //   {
    //     ModelPart::NodesContainerType::iterator it_node = it_begin + i;

    //     //noalias(it_node->Coordinates()) = it_node->GetInitialPosition().Coordinates();
    //     std::cout<<" Displacement "<<it_node->Coordinates()-it_node->GetInitialPosition().Coordinates()<<std::endl;
    //     //noalias(it_node->Coordinates()) += it_node->FastGetSolutionStepValue(DISPLACEMENT);
    //   }


    if (movement)
    {
      this->CreateKnots();
    }

    KRATOS_CATCH("")
  }

  //************************************************************************************
  //************************************************************************************

  // Used in the search detection (loop nodes in parallel region)
  bool IsInside(NodeType &rNode, const double &rCurrentTime, double Radius = 0) override
  {

    KRATOS_TRY

    bool is_inside = false;

    double TubeRadius = mBox.Radius - Radius;

    //outside
    if (mBox.Convexity == 1)
      TubeRadius *= 1.25; //increase the bounding box

    //inside
    if (mBox.Convexity == -1)
      TubeRadius *= 0.75; //decrease the bounding box

    is_inside = ContactSearch(rNode, TubeRadius);

    return is_inside;

    KRATOS_CATCH("")
  }

  //************************************************************************************
  //************************************************************************************

  bool IsInside(BoxParametersType &rValues, const ProcessInfo &rCurrentProcessInfo) const override
  {
    KRATOS_TRY

    bool is_inside = false;

    rValues.SetContactFace(2);

    is_inside = ContactSearch(rValues, rCurrentProcessInfo);

    return is_inside;

    KRATOS_CATCH("")
  }

  //************************************************************************************
  //************************************************************************************

  //Tube
  void CreateBoundingBoxBoundaryMesh(ModelPart &rModelPart, int linear_partitions = 4, int angular_partitions = 4) override
  {
    KRATOS_TRY

    //std::cout<<" Create Tube Mesh "<<std::endl;
    unsigned int NodeId = 0;
    if (rModelPart.IsSubModelPart())
      NodeId = this->GetMaxNodeId(rModelPart.GetParentModelPart());
    else
      NodeId = this->GetMaxNodeId(rModelPart);

    unsigned int InitialNodeId = NodeId;

    //get boundary model parts ( temporary implementation )
    std::vector<std::string> BoundaryModelPartsName;

    ModelPart *pMainModelPart = &rModelPart;
    if (rModelPart.IsSubModelPart())
      pMainModelPart = &rModelPart.GetParentModelPart();

    for (ModelPart::SubModelPartIterator i_mp = pMainModelPart->SubModelPartsBegin(); i_mp != pMainModelPart->SubModelPartsEnd(); i_mp++)
    {
      if (i_mp->Is(BOUNDARY) || i_mp->Is(ACTIVE))
      {
        for (ModelPart::NodesContainerType::iterator i_node = i_mp->NodesBegin(); i_node != i_mp->NodesEnd(); ++i_node)
        {
          if (i_node->Id() == rModelPart.Nodes().front().Id())
          {
            BoundaryModelPartsName.push_back(i_mp->Name());
            break;
          }
        }
      }
    }
    //get boundary model parts ( temporary implementation )

    SplineCurveUtilities::SplineType Spline;
    double PointId = 0;
    int KnotId = 0;
    int number_of_segments = linear_partitions;

    PointType Point(3);

    // Create Axis generatrix
    // for(int i=1; i<mKnots.size()-2; i++)
    // 	{
    // 	  Point[0] = mKnots[i]->X();
    // 	  Point[1] = mKnots[i]->Y();
    // 	  Point[2] = mKnots[i]->Z();

    // 	  NodeId += 1;
    // 	  NodeType::Pointer pNode = this->CreateNode(rModelPart, Point, NodeId);
    // 	  pNode->Set(RIGID,true);
    // 	  rModelPart.AddNode( pNode );

    //     //get boundary model parts ( temporary implementation )
    // 	  for(unsigned int j=0; j<BoundaryModelPartsName.size(); j++)
    // 	    (pMainModelPart->GetSubModelPart(BoundaryModelPartsName[j])).AddNode( pNode );
    //     //get boundary model parts ( temporary implementation )

    //     KnotId = mKnots[i]->Id();

    //     mSplineCurveUtilities.SetSpline(Spline, mKnots, KnotId);

    //     PointId = KnotId;

    // 	  if( i == mKnots.size()-3 ) //last segment write last point only
    // 	    number_of_segments = 1;

    // 	  for(int j=0; j<number_of_segments; j++)
    // 	    {
    // 	      PointId = j/double(linear_partitions);

    // 	      Point = ZeroVector(3);
    // 	      Point = mSplineCurveUtilities.PointOnCurve(Point, Spline, PointId);

    //         NodeId += 1;
    //         NodeType::Pointer pNode = this->CreateNode(rModelPart, Point, NodeId);
    //         pNode->Set(RIGID,true);
    //         rModelPart.AddNode( pNode );

    //         //get boundary model parts ( temporary implementation )
    // 	      for(unsigned int j=0; j<BoundaryModelPartsName.size(); j++)
    // 		(pMainModelPart->GetSubModelPart(BoundaryModelPartsName[j])).AddNode( pNode );
    // 	      //get boundary model parts ( temporary implementation )

    //       }

    // 	}
    // number_of_segments = linear_partitions;
    // InitialNodeId = NodeId;

    PointType DirectionX(3);
    noalias(DirectionX) = ZeroVector(3);
    PointType DirectionY(3);
    noalias(DirectionY) = ZeroVector(3);
    PointType DirectionZ(3);
    noalias(DirectionZ) = ZeroVector(3);

    PointType BasePoint(3);
    PointType RotationAxis(3);
    PointType RotatedDirectionY(3);

    double alpha = 0;
    QuaternionType Quaternion;

    for (unsigned int i = 1; i < mKnots.size() - 2; i++)
    {
      Point[0] = mKnots[i]->X();
      Point[1] = mKnots[i]->Y();
      Point[2] = mKnots[i]->Z();

      KnotId = mKnots[i]->Id();

      mSplineCurveUtilities.SetSpline(Spline, mKnots, KnotId);

      PointId = KnotId;

      if (i == mKnots.size() - 3) //last segment write last point only
        number_of_segments = 1;

      for (int j = 0; j < number_of_segments; j++)
      {

        PointId = j / double(linear_partitions);

        Point = ZeroVector(3);
        Point = mSplineCurveUtilities.PointOnCurve(Point, Spline, PointId);

        DirectionX = mSplineCurveUtilities.PointOnCurveFirstDerivative(DirectionX, Spline, PointId);

        this->CalculateOrthonormalBase(DirectionX, DirectionY, DirectionZ);

        for (int k = 0; k < angular_partitions; k++)
        {
          alpha = (2.0 * Globals::Pi * k) / (double)angular_partitions;

          //vector of rotation
          RotationAxis = DirectionX * alpha;
          Quaternion = QuaternionType::FromRotationVector(RotationAxis);

          RotatedDirectionY = DirectionY;

          Quaternion.RotateVector3(RotatedDirectionY);

           //add the angular_partitions points number along the circular base of the cylinder
          NodeId += 1;
          noalias(BasePoint) = Point + mBox.Radius * RotatedDirectionY;

          //std::cout<<" BasePoint["<<NodeId<<"] "<<BasePoint<<" radius "<<mBox.Radius<<std::endl;

          NodeType::Pointer pNode = this->CreateNode(rModelPart, BasePoint, NodeId);

          pNode->Set(RIGID, true);
          rModelPart.AddNode(pNode);

          //get boundary model parts ( temporary implementation )
          for (unsigned int j = 0; j < BoundaryModelPartsName.size(); j++)
            (pMainModelPart->GetSubModelPart(BoundaryModelPartsName[j])).AddNode(pNode);
          //get boundary model parts ( temporary implementation )
        }
      }
    }

    //std::cout<<" Create Tube Mesh NODES "<<std::endl;

    this->CreateQuadrilateralBoundaryMesh(rModelPart, InitialNodeId, angular_partitions);

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "TubeBoundingBox";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << Info();
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
    rOStream << this->mBox.UpperPoint << " , " << this->mBox.LowerPoint;
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{

  NodePointerVectorType mGeneratrix;

  NodePointerVectorType mKnots;

  SplineCurveUtilities mSplineCurveUtilities;

  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{


  //************************************************************************************
  //************************************************************************************

  void CreateKnots(unsigned int NumberOfSegments=250)
  {
    KRATOS_TRY


    mSplineCurveUtilities.CreateParametrizedCurve(mGeneratrix, mKnots, NumberOfSegments);

    // for(const auto& i_knot: mKnots)
    // 	std::cout<<" Knot ["<<i_knot.Id()<<"]"<<std::endl;
    std::cout << "  [REDEFINED WITH: " << mKnots.size() << " knots]" << std::endl;

    KRATOS_CATCH("")
  }

  //************************************************************************************
  //************************************************************************************

  void CreateGeneratrix(ModelPart &rModelPart)
  {
    KRATOS_TRY

    //Set generatrix control points for a given set of line elements
    for (auto &i_elem: rModelPart.Elements())
    {
     GeometryType &eGeometry = i_elem.GetGeometry();
     for (unsigned int i = 0; i < eGeometry.size()-1; ++i)
     {
       mGeneratrix.push_back(eGeometry(i));
     }
    }

    GeometryType &rGeometry = rModelPart.Elements().back().GetGeometry();
    mGeneratrix.push_back(rGeometry(rGeometry.size()-1));

    KRATOS_CATCH("")
  }


  //************************************************************************************
  //************************************************************************************

  bool ContactSearch(NodeType &rNode, const double &rRadius)
  {

    KRATOS_TRY

    const PointType &rPoint = rNode.Coordinates();

    //1.-compute point projection
    PointType Projection(3);
    Projection = mSplineCurveUtilities.CalculatePointProjection(rNode, mKnots, Projection);

    //2.-compute gap
    double GapNormal = norm_2(rPoint - Projection) - rRadius;

    GapNormal *= mBox.Convexity;

    if (GapNormal < 0)
      return true;
    else
      return false;

    KRATOS_CATCH("")
  }

  //************************************************************************************
  //************************************************************************************

  //Tube (note: box position has been updated previously)
  bool ContactSearch(BoxParametersType &rValues, const ProcessInfo &rCurrentProcessInfo) const
  {
    KRATOS_TRY

    const PointType &rPoint = rValues.GetNode().Coordinates();
    PointType &rNormal = rValues.GetNormal();
    PointType &rTangent = rValues.GetTangent();

    double &rGapNormal = rValues.GetGapNormal();

    rNormal = ZeroVector(3);
    rTangent = ZeroVector(3);

    //1.-compute point projection
    PointType Projection(3);
    Projection = mSplineCurveUtilities.GetPointProjection(rValues.GetNode(), mKnots, Projection);

    //2.-compute contact normal
    rNormal = rPoint - Projection;

    if (norm_2(rNormal))
      rNormal /= norm_2(rNormal);

    rNormal *= mBox.Convexity;

    //outside
    double TubeRadius = mBox.Radius - rValues.GetRadius();
    // if (mBox.Convexity == 1)
    //   TubeRadius *= 1.25; //increase the bounding box
    // //inside
    // if (mBox.Convexity == -1)
    //   TubeRadius *= 0.75; //decrease the bounding box

    //3.-compute gap
    rGapNormal = norm_2(rPoint - Projection) - TubeRadius;

    rGapNormal *= mBox.Convexity;

    //std::cout<<" Point "<<rPoint<<" Projection "<<Projection<<" Gap "<<rGapNormal<<" Normal "<<rNormal<<std::endl;

    this->ComputeContactTangent(rValues, rCurrentProcessInfo);

    if (rGapNormal < 0)
      return true;
    else
      return false;

    KRATOS_CATCH("")
  }

  //************************************************************************************
  //************************************************************************************

  void CreateQuadrilateralBoundaryMesh(ModelPart &rModelPart, const unsigned int &rInitialNodeId, const unsigned int &angular_partitions)
  {

    KRATOS_TRY

    //std::cout<<" Create Tube Mesh ELEMENTS "<<std::endl;

    //add elements to computing model part: (in order to be written)
    ModelPart *pComputingModelPart = NULL;
    if (rModelPart.IsSubModelPart())
      for (ModelPart::SubModelPartIterator i_mp = rModelPart.GetParentModelPart().SubModelPartsBegin(); i_mp != rModelPart.GetParentModelPart().SubModelPartsEnd(); i_mp++)
      {
        if (i_mp->Is(ACTIVE)) //computing_domain
          pComputingModelPart = &rModelPart.GetParentModelPart().GetSubModelPart(i_mp->Name());
      }
    else
    {
      for (ModelPart::SubModelPartIterator i_mp = rModelPart.SubModelPartsBegin(); i_mp != rModelPart.SubModelPartsEnd(); i_mp++)
      {
        if (i_mp->Is(ACTIVE)) //computing_domain
          pComputingModelPart = &rModelPart.GetSubModelPart(i_mp->Name());
      }
    }

    // Create surface of the cylinder/tube with quadrilateral shell conditions
    unsigned int ElementId = 0;
    if (rModelPart.IsSubModelPart())
      ElementId = this->GetMaxElementId(rModelPart.GetParentModelPart());
    else
      ElementId = this->GetMaxElementId(rModelPart);

    unsigned int NodeId = 0;
    if (rModelPart.IsSubModelPart())
      NodeId = this->GetMaxNodeId(rModelPart.GetParentModelPart());
    else
      NodeId = this->GetMaxNodeId(rModelPart);

    //GEOMETRY:
    GeometryType::Pointer pFace;
    ElementType::Pointer pElement;

    //PROPERTIES:
    //create properties for the rigid body
    unsigned int prop_id = 0;
    for (auto &prop : *rModelPart.GetParentModelPart().pProperties())
      if (prop.Id() > prop_id)
        prop_id = prop.Id();

    Properties::Pointer pProperties = Kratos::make_shared<Properties>(prop_id+1);
    rModelPart.AddProperties(pProperties);

    unsigned int local_counter = 1;
    unsigned int counter = 0;
    unsigned int Id = rInitialNodeId;

    Vector FaceNodesIds = ZeroVector(4);

    while (Id < NodeId)
    {

      counter += 1;
      ElementId += 1;

      if (local_counter < angular_partitions)
      {

        FaceNodesIds[0] = rInitialNodeId + counter;
        FaceNodesIds[1] = rInitialNodeId + counter + 1;
        FaceNodesIds[2] = rInitialNodeId + counter + angular_partitions + 1;
        FaceNodesIds[3] = rInitialNodeId + counter + angular_partitions;

        //std::cout<<" ElementA "<<FaceNodesIds<<std::endl;

        GeometryType::PointsArrayType FaceNodes;
        FaceNodes.reserve(4);

        //NOTE: when creating a PointsArrayType
        //important ask for pGetNode, if you ask for GetNode a copy is created
        //if a copy is created a segmentation fault occurs when the node destructor is called

        for (unsigned int j = 0; j < 4; j++)
          FaceNodes.push_back(rModelPart.pGetNode(FaceNodesIds[j]));

        pFace = Kratos::make_shared<Quadrilateral3D4<NodeType>>(FaceNodes);
        pElement = Kratos::make_intrusive<Element>(ElementId, pFace, pProperties);

        rModelPart.AddElement(pElement);
        pElement->Set(ACTIVE, false);
        pComputingModelPart->AddElement(pElement);

        local_counter++;
      }
      else if (local_counter == angular_partitions)
      {

        FaceNodesIds[0] = rInitialNodeId + counter;
        FaceNodesIds[1] = rInitialNodeId + counter + 1 - angular_partitions;
        FaceNodesIds[2] = rInitialNodeId + counter + 1;
        FaceNodesIds[3] = rInitialNodeId + counter + angular_partitions;

        //std::cout<<" ElementB "<<FaceNodesIds<<std::endl;

        GeometryType::PointsArrayType FaceNodes;
        FaceNodes.reserve(4);

        for (unsigned int j = 0; j < 4; j++)
          FaceNodes.push_back(rModelPart.pGetNode(FaceNodesIds[j]));

        pFace = Kratos::make_shared<Quadrilateral3D4<NodeType>>(FaceNodes);
        pElement = Kratos::make_intrusive<Element>(ElementId, pFace, pProperties);

        rModelPart.AddElement(pElement);
        pElement->Set(ACTIVE, false);
        pComputingModelPart->AddElement(pElement);

        local_counter = 1;
      }

      Id = rInitialNodeId + counter + angular_partitions;
    }

    //std::cout<<" Create Tube Mesh ELEMENTS CREATED "<<std::endl;

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}

private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Serialization
  ///@{

  friend class Serializer;

  void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, SpatialBoundingBox)
    rSerializer.save("mGeneratrix", mGeneratrix);
    rSerializer.save("mKnots", mKnots);
  }

  void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, SpatialBoundingBox)
    rSerializer.load("mGeneratrix", mGeneratrix);
    rSerializer.load("mKnots", mKnots);

    this->CreateKnots();
  }

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class TubeBoundingBox

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

} // namespace Kratos.

#endif // KRATOS_TUBE_BOUNDING_BOX_HPP_INCLUDED  defined
