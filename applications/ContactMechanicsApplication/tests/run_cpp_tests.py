from KratosMultiphysics import *
from KratosMultiphysics.ContactMechanicsApplication import *


def run():
    Tester.SetVerbosity(Tester.Verbosity.PROGRESS)  # TESTS_OUTPUTS
    try:
        Tester.RunTestSuite("KratosContactMechanicsFastSuite")
    except RuntimeError:
        print(" cpp tests not included ")


if __name__ == '__main__':
    run()
