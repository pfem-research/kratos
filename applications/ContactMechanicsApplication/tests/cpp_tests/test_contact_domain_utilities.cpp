//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:              December 2021 $
//
//

// System includes

// External includes

// Project includes
#include "testing/testing.h"
#include "geometries/triangle_2d_3.h"
#include "custom_utilities/contact_domain_utilities.hpp"

namespace Kratos
{

namespace Testing
{

/** Generates a node type sample triangle2D3.
 * Generates a point type right triangle with origin in the origin and edge size 1.
 * @return  Pointer to a triangle2D3
 */
Triangle2D3<Node<3>>::Pointer GenerateUnitTriangle2D3() {
  return Triangle2D3<Node<3>>::Pointer(new Triangle2D3<Node<3>>(
      Node<3>::Pointer(new Node<3>(1, 0.0, 0.0, 0.0)),
      Node<3>::Pointer(new Node<3>(2, 1.0, 0.0, 0.0)),
      Node<3>::Pointer(new Node<3>(3, 0.0, 1.0, 0.0))
    ));
}

KRATOS_TEST_CASE_IN_SUITE(CalculateVolume, KratosContactMechanicsFastSuite)
{
  constexpr double tolerance = 1e-12;

  auto pGeometry = GenerateUnitTriangle2D3();
  double volume = ContactDomainUtilities::CalculateVolume((*pGeometry)[0].X(),(*pGeometry)[0].Y(),
                                                          (*pGeometry)[1].X(),(*pGeometry)[1].Y(),
                                                          (*pGeometry)[2].X(),(*pGeometry)[2].Y());

  KRATOS_CHECK_NEAR(volume, 0.5, tolerance);
}

KRATOS_TEST_CASE_IN_SUITE(CalculatePosition, KratosContactMechanicsFastSuite)
{
  auto pGeometry = GenerateUnitTriangle2D3();
  double xc = 0;
  double yc = 0;
  bool inside = ContactDomainUtilities::CalculatePosition((*pGeometry)[0].X(),(*pGeometry)[0].Y(),
                                                          (*pGeometry)[1].X(),(*pGeometry)[1].Y(),
                                                          (*pGeometry)[2].X(),(*pGeometry)[2].Y(),
                                                          xc,yc);

  KRATOS_CHECK(inside);

  xc = 0.6;
  yc = 0.6;
  inside = ContactDomainUtilities::CalculatePosition((*pGeometry)[0].X(),(*pGeometry)[0].Y(),
                                                     (*pGeometry)[1].X(),(*pGeometry)[1].Y(),
                                                     (*pGeometry)[2].X(),(*pGeometry)[2].Y(),
                                                     xc,yc);

  KRATOS_CHECK(!inside);
}

KRATOS_TEST_CASE_IN_SUITE(CalculateObtuseAngle, KratosContactMechanicsFastSuite)
{
  auto pGeometry = GenerateUnitTriangle2D3();
  bool is_obtuse = ContactDomainUtilities::CalculateObtuseAngle((*pGeometry)[0].X(),(*pGeometry)[0].Y(),
                                                                (*pGeometry)[1].X(),(*pGeometry)[1].Y(),
                                                                (*pGeometry)[2].X()-0.5,(*pGeometry)[2].Y());

  KRATOS_CHECK(is_obtuse);
}


} // namespace Testing
} // namespace Kratos.
