""" Project: ContactMechanicsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Kratos Imports
from KratosMultiphysics import _ImportApplication
import KratosMultiphysics.MeshersApplication

# Application dependent names and paths
from KratosMultiphysics import _ImportApplication
from KratosContactMechanicsApplication import *
application = KratosContactMechanicsApplication()
application_name = "KratosContactMechanicsApplication"

_ImportApplication(application, application_name)
