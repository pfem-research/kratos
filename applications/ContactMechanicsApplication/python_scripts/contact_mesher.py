""" Project: ContactMechanicsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""


# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.MeshersApplication as KratosMeshers
import KratosMultiphysics.ContactMechanicsApplication as KratosContact
import KratosMultiphysics.MeshersApplication.mesher as mesher


def CreateMesher(main_model_part, meshing_parameters):
    return ContactMesher(main_model_part, meshing_parameters)


class ContactMesher(mesher.Mesher):
    #
    def Initialize(self, dimension):

        self.dimension = dimension

        # set mesher
        if self.dimension == 2:
            self.mesher = KratosContact.ContactDomain2DMesher()
        elif self.dimension == 3:
            self.mesher = KratosContact.ContactDomain3DMesher()

        self.mesher.SetEchoLevel(self.echo_level)
        self.mesher.SetMeshingParameters(self.MeshingParameters)

        self.SetPreMeshingProcesses()
        self.SetPostMeshingProcesses()

        self.mesher.Initialize()

    #
    def SetPreMeshingProcesses(self):

        # The order set is the order of execution:
        processes = []

        # clear contact conditions
        processes.append(KratosContact.ClearContactConditions(self.model_part, self.echo_level))
        # print GiD mesh output for checking purposes
        #processes.append(KratosMeshers.PrintMeshOutput(self.model_part, self.MeshingParameters, "input", self.echo_level))

        [self.mesher.SetPreMeshingProcess(process) for process in processes]
    #
    def SetPostMeshingProcesses(self):

        # The order set is the order of execution:
        processes = []

        # print GiD mesh output for checking purposes (current print)
        #processes.append(KratosMeshers.PrintMeshOutput(self.model_part, self.MeshingParameters, "output", self.echo_level))
        # select mesh elements (general)
        # processes.append(KratosMeshers.SelectElements(self.model_part, self.MeshingParameters, self.echo_level))
        # select mesh contact conditions
        processes.append(KratosContact.SelectContactConditions(self.model_part, self.MeshingParameters, self.echo_level))
        # build contact conditions
        processes.append(KratosContact.BuildContactConditions(self.model_part, self.MeshingParameters, self.echo_level))

        [self.mesher.SetPostMeshingProcess(process) for process in processes]

    #
    def _get_mesher_info(self):
        return "Reconnect a cloud of contact points"

    #
    def _get_mesher_flags(self):
        # set mesher flags: to set options for the mesher (triangle 2D, tetgen 3D)
        # RECONNECT
        mesher_flags = ""
        if self.dimension == 2:
            if self._constrained():
                mesher_flags = "pBYYQ"
            else:
                mesher_flags = "QNP"
        elif self.dimension == 3:
            if self._constrained():
                mesher_flags = "pYYJMFBQO4/4"  # tetgen 1.5.0
                # mesher_flags = "pJFBMYYCCQu0"  #tetgen 1.4.3
                # mesher_flags = "pJFBMYYCCQ"  #tetgen 1.5.0
            else:
                mesher_flags = "JFMQO4/4"

        return mesher_flags

    #
    def _get_execution_options(self):
        execution_options = KratosMultiphysics.Flags()

        execution_options.Set(
            KratosMeshers.MesherData.INITIALIZE_MESHER_INPUT, True)
        execution_options.Set(
            KratosMeshers.MesherData.FINALIZE_MESHER_INPUT,  True)

        execution_options.Set(
            KratosMeshers.MesherData.TRANSFER_KRATOS_NODES_TO_MESHER, True)
        execution_options.Set(
            KratosMeshers.MesherData.TRANSFER_KRATOS_ELEMENTS_TO_MESHER, False)
        execution_options.Set(
            KratosMeshers.MesherData.TRANSFER_KRATOS_NEIGHBOURS_TO_MESHER, False)

        if self._constrained():
            print(" TRANSFER FACES TO MESHER ")
            execution_options.Set(
                KratosMeshers.MesherData.TRANSFER_KRATOS_FACES_TO_MESHER, True)

        execution_options.Set(
            KratosMeshers.MesherData.SELECT_TESSELLATION_ELEMENTS, True)
        execution_options.Set(
            KratosMeshers.MesherData.KEEP_ISOLATED_NODES, True)

        return execution_options

    #
    @classmethod
    def _class_prefix(self):
        header = "::[---Contact Mesher--]::"
        return header
