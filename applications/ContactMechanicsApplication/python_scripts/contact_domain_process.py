""" Project: ContactMechanicsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""


# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.MeshersApplication as KratosMeshers
import KratosMultiphysics.ContactMechanicsApplication as KratosContact
import KratosMultiphysics.MeshersApplication.remesh_domains_process as BaseProcess

def Factory(settings, Model):
    if(not isinstance(settings, KratosMultiphysics.Parameters)):
        raise Exception(
            "Expected input shall be a Parameters object, encapsulating a json string")
    return ContactDomainProcess(Model, settings["Parameters"])


class ContactDomainProcess(BaseProcess.RemeshDomainsProcess):
    #
    def __init__(self, Model, custom_settings):

        super(ContactDomainProcess, self).__init__(Model, custom_settings)
    #
    def ExecuteInitialize(self):
        BaseProcess.RemeshDomainsProcess.ExecuteInitialize(self)

        if self.remesh_domains_active:
            self.counter = 0
            self.RemeshDomains()
    #
    def ExecuteBeforeOutputStep(self):

        if self._domain_parts_changed() or self.control.IsExecutionStep():
            if self.execution_before_output:
                self.RemeshDomains()
    #
    def ExecuteAfterOutputStep(self):

        if self._domain_parts_changed() or self.control.IsExecutionStep():
            if not self.execution_before_output:
                self.RemeshDomains()
    #
    def GetVariables(self):

        nodal_variables = super(ContactDomainProcess, self).GetVariables()

        nodal_variables = nodal_variables + ['OFFSET']
        nodal_variables = nodal_variables + ['CONTACT_NORMAL', 'CONTACT_FORCE']
        nodal_variables = nodal_variables + \
            ['CONTACT_STRESS', 'EFFECTIVE_CONTACT_STRESS', 'EFFECTIVE_CONTACT_FORCE']

        return nodal_variables

    ###
    def _create_smoothing_processes(self):
        return []
    #
    def _InitializeDomains(self):
        if(self.remesh_domains_active):
            if not self._is_not_restarted():
                self.main_model_part.ProcessInfo.SetValue(KratosMeshers.INITIALIZED_DOMAINS, False)
            # execute initialize from base class
            self._initialize_domains()
            # initialize contact domains
            for domain in self.domains:
                domain.SetEchoLevel(self.echo_level)
                domain.Initialize()
    #
    def _domains_module(self):
        import importlib
        module_name = "KratosMultiphysics.ContactMechanicsApplication.contact_domain"
        domain_module = importlib.import_module(module_name)
        return domain_module

    #
    def _domain_parts_changed(self):
        update_time = False
        process_info = self.main_model_part.ProcessInfo
        if not update_time and process_info.Has(KratosMeshers.MESHING_STEP_TIME):
            update_time = self._check_current_time_step(
                process_info[KratosMeshers.MESHING_STEP_TIME])
            #print(" MESHING_STEP_TIME ",self.process_info[KratosMeshers.MESHING_STEP_TIME], update_time)

        return update_time

    #
    def _domain_parts_updated(self):
        update_time = False
        process_info = self.main_model_part.ProcessInfo
        if not self._is_not_restarted():
            if process_info.Has(KratosMeshers.RESTART_STEP_TIME):
                update_time = self._check_current_time_step(
                    process_info[KratosMeshers.RESTART_STEP_TIME])
                #print(" RESTART_STEP_TIME ",process_info[KratosMeshers.RESTART_STEP_TIME], update_time)

        if not update_time and process_info.Has(KratosMeshers.MESHING_STEP_TIME):
            update_time = self._check_previous_time_step(
                process_info[KratosMeshers.MESHING_STEP_TIME])
            #print(" MESHING_STEP_TIME ",process_info[KratosMeshers.MESHING_STEP_TIME], update_time)

        return update_time

    #
    def _check_current_time_step(self, step_time):
        process_info = self.main_model_part.ProcessInfo
        current_time = process_info[KratosMultiphysics.TIME]
        delta_time = process_info[KratosMultiphysics.DELTA_TIME]

        # arithmetic floating point tolerance
        tolerance = delta_time * 0.001
        if(step_time > current_time-tolerance and step_time < current_time+tolerance):
            return True
        else:
            return False

    #
    def _check_previous_time_step(self, step_time):
        process_info = self.main_model_part.ProcessInfo
        current_time = process_info[KratosMultiphysics.TIME]
        delta_time = process_info[KratosMultiphysics.DELTA_TIME]
        previous_time = current_time - delta_time

        # arithmetic floating point tolerance
        tolerance = delta_time * 0.001

        if(step_time > previous_time-tolerance and step_time < previous_time+tolerance):
            return True
        else:
            return False

    #
    def _GetModelManager(self):
        meshing_options = KratosMultiphysics.Flags()
        return KratosContact.ContactModelStructure(self.main_model_part, meshing_options, self.echo_level)

    #
    def _SetMeshingStepTime(self):
        current_time = self.main_model_part.ProcessInfo[KratosMultiphysics.TIME]
        self.main_model_part.ProcessInfo.SetValue(
            KratosContact.CONTACT_STEP_TIME, current_time)

    #
    @classmethod
    def _class_prefix(self):
        header = "::[--Meshing Contact--]::"
        return header
