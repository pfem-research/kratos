""" Project: ContactMechanicsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""


# Built-in/Generic Imports
# from multiprocessing import Pool

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.MeshersApplication as KratosMeshers
import KratosMultiphysics.ContactMechanicsApplication as KratosContact


def Factory(settings, Model):
    if(not isinstance(settings, KratosMultiphysics.Parameters)):
        raise Exception(
            "Expected input shall be a Parameters object, encapsulating a json string")
    return ParametricWallsProcess(Model, settings["Parameters"])


class ParametricWallsProcess(KratosMultiphysics.Process):
    #
    def __init__(self, Model, custom_settings):

        KratosMultiphysics.Process.__init__(self)

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
            "model_part_name"         : "Solid Domain",
            "execution_control_type"  : "step",
            "execution_frequency"     : 1.0,
            "echo_level"              : 0,
            "parametric_walls"        : []
        }
        """)

        # legacy transform
        custom_settings = TransformSettings(custom_settings)

        # overwrite the default settings with user-provided parameters
        self.settings = custom_settings
        self.settings.ValidateAndAssignDefaults(default_settings)

        self.counter = 1
        self.echo_level = self.settings["echo_level"].GetInt()

        self.model = Model

        # prepend parent model part name
        model_name = self.settings["model_part_name"].GetString()
        for wall in self.settings["parametric_walls"]:
            if wall["Parameters"].Has("model_part_name"):
                if not model_name in wall["Parameters"]["model_part_name"].GetString():
                    name = model_name+"."+wall["Parameters"]["model_part_name"].GetString()
                    wall["Parameters"]["model_part_name"].SetString(name)
            else:
                name = model_name+".Wall_Domain"
                wall["Parameters"].AddEmptyValue("model_part_name").SetString(name)


        # Construct parametric wall domains
        self.parametric_walls = self._ConstructWalls()
    #
    def ExecuteInitialize(self):

        self.main_model_part = self.model[self.settings["model_part_name"].GetString()]
        self.dimension = self.main_model_part.ProcessInfo[KratosMultiphysics.SPACE_DIMENSION]

        self.control = KratosMeshers.ExecutionControl(self.main_model_part.ProcessInfo, self.settings["execution_control_type"].GetString(), self.settings["execution_frequency"].GetDouble());

        # contact search initial values
        self.search_contact_active = False
        for wall in self.parametric_walls:
            if wall.Active():
                self.search_contact_active = True

        # initialize walls
        self._InitializeWalls()


        print(self._class_prefix()+" Ready")


    #
    def ExecuteInitializeSolutionStep(self):
        self.control.Update()

        # clean all contacts from main_model_part
        for wall in self.parametric_walls:
            wall.InitializeSearch()

        # build all contacts :: when building check if the condition exists and clone it
        if self.search_contact_active:
            if self._domain_parts_changed() or self.control.IsExecutionStep():
                self.SearchContact()

    #
    def ExecuteFinalizeSolutionStep(self):
        pass

    ###
    #
    def SearchContact(self):

        if self.echo_level > 0:
            print(self._class_prefix()+" [ Contact Search (call:", str(self.counter)+") ]")

        self.wall_contact_model = KratosContact.ClearPointContactConditions(self.main_model_part, self.echo_level)

        self.wall_contact_model.ExecuteInitialize()

        # serial
        for wall in self.parametric_walls:
            wall.ExecuteSearch()


        self.wall_contact_model.ExecuteFinalize()

        self.counter += 1

        # set contact step time
        self._SetContactStepTime()

        # schedule next search
        self.control.SetNextExecution()

    #
    def GetSearchStep(self):
        return self.counter

    #
    def GetVariables(self):
        nodal_variables = ['RIGID_WALL', 'VELOCITY',
                           'ACCELERATION', 'CONTACT_STRESS']
        nodal_variables = nodal_variables + ['CONTACT_FORCE', 'CONTACT_NORMAL']
        nodal_variables = nodal_variables + ['VOLUME_ACCELERATION']
        nodal_variables = nodal_variables + ['NORMAL', 'NODAL_H']
        nodal_variables = nodal_variables + ['EFFECTIVE_CONTACT_FORCE', 'EFFECTIVE_CONTACT_STRESS']
        nodal_variables = nodal_variables + ['CONTACT_PLASTIC_SLIP', 'CONTACT_FRICTION_ANGLE']
        return nodal_variables

    ###
    #
    def _ConstructWalls(self):
        import importlib
        walls = []
        for item in self.settings["parametric_walls"]:
            wall_module_name = None
            if item.Has("process_module"):
                wall_module_name = item["process_module"].GetString()
                if not "KratosMultiphysics" in wall_module_name:
                    wall_module_name = "KratosMultiphysics."+wall_module_name
            elif item.Has("python_module"):
                wall_module_name = item["python_module"].GetString()
                if not '.' in wall_module_name:
                    wall_module_name = str(
                        "KratosMultiphysics.ContactMechanicsApplication."+item["python_module"].GetString())

            parametric_wall_module = importlib.import_module(wall_module_name)

            wall = parametric_wall_module.CreateParametricWall(self.model, item["Parameters"])
            walls.append(wall)
        return walls

    #
    def _SetContactStepTime(self):
        # search in initialize store previous time step
        time = self.main_model_part.ProcessInfo.GetPreviousTimeStepInfo()[KratosMultiphysics.TIME]
        self.main_model_part.ProcessInfo.SetValue(
            KratosContact.CONTACT_STEP_TIME, time)


    def _InitializeWalls(self):
        if(self.search_contact_active):
            self._initialize_domains()

        # build parametric walls
        for wall in self.parametric_walls:
            wall.SetEchoLevel(self.echo_level)
            wall.Initialize()
    #
    def _initialize_domains(self):
        if(self.main_model_part.ProcessInfo[KratosMeshers.INITIALIZED_DOMAINS] == False):
            print(self._class_prefix()+" Initialize Domains")
            import KratosMultiphysics.MeshersApplication.domain_utilities as domain_utilities
            domain_utils = domain_utilities.DomainUtilities()
            domain_utils.InitializeDomains(self.main_model_part, self.echo_level)
    #
    def _domain_parts_changed(self):
        update_time = False
        process_info = self.main_model_part.ProcessInfo
        if not update_time and process_info.Has(KratosMeshers.MESHING_STEP_TIME):
            #update_time = self._check_current_time_step(
            #    process_info[KratosMeshers.MESHING_STEP_TIME])
            update_time = self._check_previous_time_step(
                process_info[KratosMeshers.MESHING_STEP_TIME])
            #print(" MESHING_STEP_TIME ",self.process_info[KratosMeshers.MESHING_STEP_TIME], update_time)

        return update_time

    #
    def _check_current_time_step(self, step_time):
        process_info = self.main_model_part.ProcessInfo
        current_time = process_info[KratosMultiphysics.TIME]
        delta_time = process_info[KratosMultiphysics.DELTA_TIME]

        # arithmetic floating point tolerance
        tolerance = delta_time * 0.001
        if(step_time > current_time-tolerance and step_time < current_time+tolerance):
            return True
        else:
            return False

   #
    def _check_previous_time_step(self, step_time):
        process_info = self.main_model_part.ProcessInfo
        current_time = process_info[KratosMultiphysics.TIME]
        delta_time = process_info[KratosMultiphysics.DELTA_TIME]
        previous_time = current_time - delta_time

        # arithmetic floating point tolerance
        tolerance = delta_time * 0.001

        if(step_time > previous_time-tolerance and step_time < previous_time+tolerance):
            return True
        else:
            return False

    #
    def _get_pretty_time(self, time):
        pretty_time = "{0:.12g}".format(time)
        pretty_time = float(pretty_time)
        return pretty_time

    #
    @classmethod
    def _class_prefix(self):
        header = "::[---Walls Contact---]::"
        return header

## *************** LEGACY TRANSFORM ***************** ##
def TransformSettings(custom_settings):
    if custom_settings.Has("search_control_type"):
        custom_settings.AddEmptyValue("execution_control_type").SetString(custom_settings["search_control_type"].GetString())
        custom_settings.RemoveValue("search_control_type")
    if custom_settings.Has("search_frequency"):
        custom_settings.AddEmptyValue("execution_frequency").SetDouble(custom_settings["search_frequency"].GetDouble())
        custom_settings.RemoveValue("search_frequency")

    counter = 0
    for wall in custom_settings["parametric_walls"]:
        if not wall.Has("Parameters"):
            wall_settings = KratosMultiphysics.Parameters('''
            {
            "process_module": "KratosMultiphysics.ContactMechanicsApplication.parametric_wall",
            "Parameters":{}
            }
            ''')
            wall_settings["Parameters"] = custom_settings["parametric_walls"][counter]
            if wall_settings["Parameters"].Has("process_module"):
                wall_settings["Parameters"].RemoveValue("process_module")
            if wall_settings["Parameters"].Has("python_module"):
                wall_settings["Parameters"].RemoveValue("python_module")
            custom_settings["parametric_walls"][counter] = wall_settings
        counter+=1

    return custom_settings
