""" Project: ContactMechanicsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""



# Built-in/Generic Imports
import sys

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.ContactMechanicsApplication as KratosContact


def CreateParametricWall(Model, custom_settings):
    custom_settings = TransformSettings(custom_settings)
    return ParametricWall(Model, custom_settings)


class ParametricWall(object):

    # constructor. the constructor shall only take care of storing the settings
    # and the pointer to the main_model part.
    ##
    # real construction shall be delayed to the function "Initialize" which
    # will be called once the mesher is already filled
    def __init__(self, Model, custom_settings):

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
            "model_part_name" : "Wall_Domain",
            "bounding_box_settings":{
               "bounding_box_type": "SpatialBoundingBox",
               "build_mesh": false,
               "bounding_box_parameters":{
                   "parameters_list":[],
                   "velocity" : [0.0, 0.0, 0.0],
                   "plane_size": 10.0
               }
            },
            "rigid_body": true,
            "body_settings":{
               "element_type": "TranslationalRigidBodyElement2D1N",
               "constrained": true,
               "compute_parameters": false,
               "body_parameters":{
                   "center_of_gravity": [0.0 ,0.0, 0.0],
                   "mass":0.0,
                   "main_inertias": [0.0, 0.0, 0.0],
                   "main_axes": [ [1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0] ]
               }
            },
            "contact": true,
            "contact_options":{
                "contact_condition": "PointContactCondition2D1N",
                "friction_law": "FrictionLaw",
                "friction_properties":{
                    "FRICTION_ACTIVE": false,
                    "MU_STATIC": 0.3,
                    "MU_DYNAMIC": 0.2,
                    "DESIRED_CONTACT_FRICTION_ANGLE": 0.0,
                    "PENALTY_PARAMETER": 1000,
                    "PENALTY_PARAMETER_SHAFT": 0,
                    "TANGENTIAL_PENALTY_RATIO": 0.1,
                    "CONTACT_ADHESION": 0.0,
                    "CONTACT_FRICTION": 0.0,
                    "VISCOSITY": 0.0,
                    "TAU_STAB": 1
                }
            }
        }
        """)

        # new node and rigid body element inside the same mesh : boundary conditions also applied
        # this node and elements must be considered in the computing model part
        # new contact conditions must be already assembled

        # if exist a movement from a point different from CG a link condition must be used

        # overwrite the default settings with user-provided parameters
        self.settings = custom_settings
        self.settings.ValidateAndAssignDefaults(default_settings)
        self.settings["bounding_box_settings"].ValidateAndAssignDefaults(default_settings["bounding_box_settings"])
        self.settings["contact_options"].ValidateAndAssignDefaults(default_settings["contact_options"])
        self.settings["contact_options"]["friction_properties"].ValidateAndAssignDefaults(default_settings["contact_options"]["friction_properties"])
        self.settings["body_settings"].RecursivelyValidateAndAssignDefaults(default_settings["body_settings"])

        self.echo_level = 0

        self.model = Model

        self.active_search = self.settings["contact"].GetBool()

    ####

    def Initialize(self):

        # construct rigid wall // it will contain the array of nodes, array of elements, and the array of conditions
        self.model_part = self.model[self.settings["model_part_name"].GetString()].GetRootModelPart()

        # set rigid wall flags
        self.wall_model_part = self.model[self.settings["model_part_name"].GetString()]

        self._set_rigid_wall_flags(self.wall_model_part)

        # build bounding box
        self.wall_bounding_box = self._get_bounding_box(self.wall_model_part)

        # contact model part name
        self.contact_model_part_name = "contact_" + self.wall_model_part.Name

        if not self.model_part.ProcessInfo[KratosMultiphysics.IS_RESTARTED]:
            # contruct parametric wall mesh
            # created BX mesh is added to wall model_part
            if self.settings["bounding_box_settings"]["build_mesh"].GetBool():
                self.CreateBoundingBoxMesh(self.wall_bounding_box, self.wall_model_part)
            else:
                self.SetBoundingBoxLimits(self.wall_bounding_box, self.wall_model_part)
            # construct rigid element
            # must pass an array of nodes to the element, create a node (CG) and a rigid_body element
            # created CG is set as the reference node of the wall_bounding_box, BLOCKED,
            # created CG node and rigid_body element are added to in the wall model_part
            # created CG nodes are added to boundary parts where wall_model_part nodes coincide (to apply same process to CG node)
            if self.settings["rigid_body"].GetBool():
                KratosContact.RigidBodyCreationUtility().CreateRigidBody(self.wall_model_part, self.wall_bounding_box, self.settings["body_settings"])
            # set contact wall model part (note: can not be a child of wall_model_part due to process imposed variables)
            self.contact_wall_model_part = self.model_part.CreateSubModelPart(self.contact_model_part_name)
            self.contact_wall_model_part.Set(KratosMultiphysics.CONTACT)
        else:
            # reset parametric wall mesh
            self.SetBoundingBoxLimits(self.wall_bounding_box, self.wall_model_part)
            # get rigid body center
            for node in self.wall_model_part.Nodes:
                # MASTER reserved for the Rigid Body center in wall model part
                if node.Is(KratosMultiphysics.MASTER):
                    self.wall_bounding_box.SetRigidBodyCenter(node)
                    print(self._class_prefix()+" Rigid Body Center ["+str(node.Id)+"]("+str(node.X0)+","+str(node.Y0)+","+str(node.Z0)+")")
                    break
            # set contact wall model part (note: can not be a child of wall_model_part due to process imposed variables)
            self.contact_wall_model_part = self.model_part.GetSubModelPart(self.contact_model_part_name)

        print(self._class_prefix() + " Contact Part ("+self.contact_model_part_name+")")


        # get search strategy
        self.SearchStrategy = KratosContact.ParametricWallContactSearch(self.model_part, self.contact_model_part_name, self._get_contact_properties(), self.wall_bounding_box, self.settings["contact_options"])
        self.SearchStrategy.ExecuteInitialize()

        print(self._class_prefix()+" Ready")

    ####

    def InitializeSearch(self):
        self.wall_bounding_box.UpdateBoxPosition(self.model_part.ProcessInfo[KratosMultiphysics.TIME])

    def FinalizeSearch(self):
        pass

    def ExecuteSearch(self):
        self.SearchStrategy.Execute()

    #
    def SetEchoLevel(self, echo_level):
        self.echo_level = echo_level

    #
    def Active(self):
        return self.active_search

    ###
    #
    def _get_contact_properties(self):
        prop_id = 0
        for prop in self.model_part.Properties:
            if prop_id < prop.Id:
                prop_id = prop.Id

        if self.model_part.ProcessInfo[KratosMultiphysics.IS_RESTARTED]:
            if self.model_part.Properties[prop_id].Has(KratosContact.FRICTION_ACTIVE):
                prop_id -= 1
                print(self._class_prefix()+" Parametric wall properties ["+str(prop_id+1)+"] already exist")

        contact_properties = self.model_part.Properties[prop_id+1]

        # set friction law to properties:
        friction_law_name = self.settings["contact_options"]["friction_law"].GetString()
        material_law = "KratosMultiphysics.ContactMechanicsApplication."+friction_law_name+"()"
        FrictionLaw = eval(material_law)
        FrictionLaw.AddToProperties(KratosContact.FRICTION_LAW, FrictionLaw.Clone(), contact_properties)

        return contact_properties
    #
    def _set_rigid_wall_flags(self, model_part):
        import KratosMultiphysics
        import KratosMultiphysics.SolidMechanicsApplication as KratosSolid
        entity_type = "Nodes"
        assign_flags = [self._get_flag("RIGID"), self._get_flag("NOT_BOUNDARY")]
        self.wall_model_part.Set(KratosMultiphysics.RIGID)
        KratosSolid.AssignFlagsToEntitiesProcess(model_part, entity_type, assign_flags).Execute()

        entity_type = "Conditions"
        assign_flags = [self._get_flag("NOT_ACTIVE")]
        KratosSolid.AssignFlagsToEntitiesProcess(model_part, entity_type, assign_flags).Execute()

    #
    def _get_bounding_box(self, model_part):
        import importlib
        box_name = self.settings["bounding_box_settings"]["bounding_box_type"].GetString()
        module_name = "KratosMultiphysics.ContactMechanicsApplication"
        bounding_box = getattr(importlib.import_module(module_name), box_name)
        return bounding_box(model_part, self.settings["bounding_box_settings"]["bounding_box_parameters"])

    #
    def CreateBoundingBoxMesh(self, bounding_box, model_part):

        # construct bounding box mesh of surface elements
        # Note: new nodes must be inserted in boundary conditions subdomains
        number_of_linear_partitions = 10
        number_of_angular_partitions = 15

        bounding_box.CreateBoundingBoxBoundaryMesh(
            model_part, number_of_linear_partitions, number_of_angular_partitions)
        # set flag RIGID to the mesh elements and nodes
        for node in model_part.Nodes:
            node.Set(KratosMultiphysics.RIGID)
        for element in model_part.Elements:
            element.Set(KratosMultiphysics.RIGID)
            # in order to write them
            element.Set(KratosMultiphysics.ACTIVE)

        self.SetBoundingBoxLimits(bounding_box, model_part)

        print(self._class_prefix()+" Bounding Box Mesh Created")

    def SetBoundingBoxLimits(self, bounding_box, model_part):

        # set mesh upper and lower points
        upper_point = KratosMultiphysics.Array3()
        upper = self.GetUpperPoint(model_part)
        # print("upper",upper)
        for i in range(0, len(upper)):
            upper_point[i] = upper[i]
            bounding_box.SetUpperPoint(upper_point)

        lower_point = KratosMultiphysics.Array3()
        lower = self.GetLowerPoint(model_part)
        # print("lower",lower)
        for i in range(0, len(lower)):
            lower_point[i] = lower[i]
            bounding_box.SetLowerPoint(lower_point)

        print(self._class_prefix() +
              " Bounding Box limits:[", upper_point, ",", lower_point, "]")

    #
    def GetUpperPoint(self, model_part):

        dimension = model_part.ProcessInfo[KratosMultiphysics.SPACE_DIMENSION]

        max_x = sys.float_info.min
        max_y = sys.float_info.min
        max_z = sys.float_info.min

        for node in model_part.Nodes:
            if(node.X0 > max_x):
                max_x = node.X0
            if(node.Y0 > max_y):
                max_y = node.Y0
            if(node.Z0 > max_z):
                max_z = node.Z0

        if(dimension == 2):
            return [max_x+20.0, max_y+20.0, 0]
        else:
            return [max_x+20.0, max_y+20.0, max_z+20]

    #
    def GetLowerPoint(self, model_part):

        dimension = model_part.ProcessInfo[KratosMultiphysics.SPACE_DIMENSION]

        min_x = sys.float_info.max
        min_y = sys.float_info.max
        min_z = sys.float_info.max

        for node in model_part.Nodes:
            if(node.X0 < min_x):
                min_x = node.X0
            if(node.Y0 < min_y):
                min_y = node.Y0
            if(node.Z0 < min_z):
                min_z = node.Z0

        if(dimension == 2):
            return [min_x-10.0, min_y-10.0, 0]
        else:
            return [min_x-10.0, min_y-10.0, min_z]

    #
    def _get_flag(self,flag_name):
        if flag_name.find("NOT_")==0:
            return KratosMultiphysics.KratosGlobals.GetFlag(flag_name[4:]).AsFalse()
        else:
            return KratosMultiphysics.KratosGlobals.GetFlag(flag_name)

    #
    @classmethod
    def _class_prefix(self):
        header = "::[--Parametric Wall--]::"
        return header

## *************** LEGACY TRANSFORM ***************** ##
def TransformSettings(custom_settings):
    if custom_settings.Has("contact_search_settings"):
        if custom_settings["contact_search_settings"].Has("contact_parameters"):
            custom_settings.AddValue("contact_options",custom_settings["contact_search_settings"]["contact_parameters"])
        custom_settings.RemoveValue("contact_search_settings")

        print(custom_settings)
        if custom_settings["contact_options"].Has("contact_condition_type"):
            custom_settings["contact_options"].AddValue("contact_condition", custom_settings["contact_options"]["contact_condition_type"])
            custom_settings["contact_options"].RemoveValue("contact_condition_type")

        if custom_settings["contact_options"].Has("friction_law_type"):
            custom_settings["contact_options"].AddValue("friction_law", custom_settings["contact_options"]["friction_law_type"])
            custom_settings["contact_options"].RemoveValue("friction_law_type")

        if custom_settings["contact_options"].Has("variables_of_properties"):
            custom_settings["contact_options"].AddValue("friction_properties", custom_settings["contact_options"]["variables_of_properties"])
            custom_settings["contact_options"].RemoveValue("variables_of_properties")

    return custom_settings
