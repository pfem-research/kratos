""" Project: ContactMechanicsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""


# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.MeshersApplication as KratosMeshers
import KratosMultiphysics.ContactMechanicsApplication as KratosContact
import KratosMultiphysics.MeshersApplication.meshing_domain as meshing_domain


def CreateDomain(Model, custom_settings):
    if custom_settings.Has("meshing_strategy"):
        custom_settings = TransformSettings(custom_settings)
    return ContactDomain(Model, custom_settings)

class ContactDomain(meshing_domain.MeshingDomain):

    # constructor. the constructor shall only take care of storing the settings
    # and the pointer to the main_model part.
    ##
    # real construction shall be delayed to the function "Initialize" which
    # will be called once the mesher is already filled
    def __init__(self, Model, custom_settings):

        self.echo_level = 0

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
            "model_part_name": "contact_domain",
            "process_module": "ContactMechanicsApplication.contact_meshing_strategy",
            "Parameters":{
                "remesh": true,
                "meshing_options": {
                   "alpha_shape": 0.0,
                   "offset_factor": 0.0,
                   "constrained": true
                 },
                "contact": true,
                "contact_options":{
                   "contact_condition": "ContactDomainLM2DCondition",
                   "friction_law": "FrictionLaw",
                   "friction_properties":{
                        "FRICTION_ACTIVE": false,
                        "MU_STATIC": 0.3,
                        "MU_DYNAMIC": 0.2,
                        "PENALTY_PARAMETER": 1000,
                        "TANGENTIAL_PENALTY_RATIO": 0.1,
                        "TAU_STAB": 1.0
                   },
                   "contact_parts": []
                }
            }
        }
        """)

        # overwrite the default settings with user-provided parameters
        self.settings = custom_settings
        default_settings["Parameters"]["contact_options"]["friction_properties"].AddMissingParameters(self.settings["Parameters"]["contact_options"]["friction_properties"])
        self.settings.RecursivelyValidateAndAssignDefaults(default_settings)
        self.echo_level = 0

        self.model = Model

        self.active_remeshing = self.settings["Parameters"]["remesh"].GetBool()
        #print(" CONTACT_DOMAIN ", self.settings.PrettyPrintJsonString())

    ####

    def Initialize(self):

         # Parent name is prepended to "model_part_name"
        parent_name = self.settings["model_part_name"].GetString().split(".", 1)[0]

        self.model_part = self.model[parent_name].GetRootModelPart()
        self.dimension = self.model_part.ProcessInfo[KratosMultiphysics.SPACE_DIMENSION]

        # Construct the meshing/constact search strategy
        import importlib
        module_name = self.settings["process_module"].GetString()
        if not "KratosMultiphysics" in module_name:
            module_name = "KratosMultiphysics."+module_name
        meshing_module = importlib.import_module(module_name)
        self.MeshingStrategy = meshing_module.CreateMeshingStrategy(self.model_part, self.settings["Parameters"])

        # Meshing Stratety
        self.MeshingStrategy.SetEchoLevel(self.echo_level)
        self.MeshingStrategy.Initialize()

        # Print Status
        self._print_status()

    ####

    #
    def Check(self):
        pass
    #
    def _print_status(self):
        print(self._class_prefix() +
              " ("+self.settings["model_part_name"].GetString()+") Ready")
    #
    @classmethod
    def _class_prefix(self):
        header = "::[---Contact Domain--]::"
        return header


## *************** LEGACY TRANSFORM ***************** ##
def CreateMeshingDomain(Model, custom_settings):
    custom_settings = TransformSettings(custom_settings)
    return ContactDomain(Model, custom_settings)

def TransformSettings(custom_settings):

    # settings string in json format
    default_settings = KratosMultiphysics.Parameters("""
    {
       "python_module": "contact_domain",
       "process_module": "ContactMechanicsApplication.contact_domain",
       "model_part_name": "contact_domain",
       "alpha_shape": 0.0,
       "offset_factor": 0.0,
       "meshing_strategy":{
           "python_module": "contact_meshing_strategy",
           "process_module": "ContactMechanicsApplication.contact_meshing_strategy",
           "meshing_frequency": 0.0,
           "remesh": true,
           "constrained": true,
           "contact_parameters":{
               "contact_condition_type": "ContactDomainLM2DCondition",
               "kratos_module": "KratosMultiphysics.ContactMechanicsApplication",
               "friction_law_type": "FrictionLaw",
               "variables_of_properties":{
                   "FRICTION_ACTIVE": false,
                   "MU_STATIC": 0.3,
                   "MU_DYNAMIC": 0.2,
                   "PENALTY_PARAMETER": 1000,
                   "TANGENTIAL_PENALTY_RATIO": 0.1,
                   "TAU_STAB": 1.0
               }
            }
        },
        "elemental_variables_to_transfer":[],
        "contact_bodies_list": []
    }
    """)

    if not custom_settings["meshing_strategy"].Has("process_module"):
        module_name = custom_settings["meshing_strategy"]["python_module"].GetString().split(".",1)[1]
        custom_settings["meshing_strategy"].AddEmptyValue("process_module").SetString(module_name)

    # overwrite the default settings with user-provided parameters
    old_settings = custom_settings
    old_settings.ValidateAndAssignDefaults(default_settings)
    old_settings["meshing_strategy"].ValidateAndAssignDefaults(default_settings["meshing_strategy"])
    old_settings["meshing_strategy"]["contact_parameters"].ValidateAndAssignDefaults(default_settings["meshing_strategy"]["contact_parameters"])


    # transform to new settings with user-provided parameters
    new_settings = KratosMultiphysics.Parameters("""
    {
        "model_part_name": "contact_domain",
        "process_module": "ContactMechanicsApplication.contact_meshing_strategy",
        "Parameters":{
            "remesh": true,
            "meshing_options": {
               "alpha_shape": 0.0,
               "offset_factor": 0.0,
               "constrained": true
             },
            "contact": true,
            "contact_options":{
               "contact_condition": "ContactDomainLM2DCondition",
               "friction_law": "FrictionLaw",
               "friction_properties":{
                    "FRICTION_ACTIVE": false,
                    "MU_STATIC": 0.3,
                    "MU_DYNAMIC": 0.2,
                    "PENALTY_PARAMETER": 1000,
                    "TANGENTIAL_PENALTY_RATIO": 0.1,
                    "TAU_STAB": 1.0
               },
               "contact_parts": []
            }
         }
    }
    """)


    new_settings["process_module"] = old_settings["meshing_strategy"]["process_module"]
    new_settings["model_part_name"] = old_settings["model_part_name"]

    # Parameters
    new_settings["Parameters"]["remesh"] = old_settings["meshing_strategy"]["remesh"]
    new_settings["Parameters"]["meshing_options"]["alpha_shape"] = old_settings["alpha_shape"]
    new_settings["Parameters"]["meshing_options"]["offset_factor"] = old_settings["offset_factor"]
    new_settings["Parameters"]["meshing_options"]["constrained"] = old_settings["meshing_strategy"]["constrained"]


    old_settings["meshing_strategy"]["contact_parameters"].AddValue("contact_parts",old_settings["contact_bodies_list"])
    new_settings["Parameters"]["contact_options"]["contact_parts"] = old_settings["meshing_strategy"]["contact_parameters"]["contact_parts"]
    new_settings["Parameters"]["contact_options"]["contact_condition"] = old_settings["meshing_strategy"]["contact_parameters"]["contact_condition_type"]
    new_settings["Parameters"]["contact_options"]["friction_law"] = old_settings["meshing_strategy"]["contact_parameters"]["friction_law_type"]
    new_settings["Parameters"]["contact_options"]["friction_properties"] = old_settings["meshing_strategy"]["contact_parameters"]["variables_of_properties"]

    new_settings["Parameters"]["contact_options"].RemoveValue("kratos_module")

    #print(" OLD CONTACT_STRATEGY settings ", old_settings.PrettyPrintJsonString())
    #print(" NEW CONTACT_STRATEGY settings ", new_settings.PrettyPrintJsonString())
    #input("Press Enter to continue...")

    return new_settings
