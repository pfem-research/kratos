""" Project: ContactMechanicsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""



# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.MeshersApplication as KratosMeshers
import KratosMultiphysics.ContactMechanicsApplication as KratosContact
import KratosMultiphysics.MeshersApplication.meshing_strategy as meshing_strategy


def CreateMeshingStrategy(model_part, custom_settings):
    return ContactMeshingStrategy(model_part, custom_settings)


class ContactMeshingStrategy(meshing_strategy.MeshingStrategy):

    #
    def __init__(self, model_part, custom_settings):

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
             "remesh": true,
             "meshing_options": {
                "alpha_shape": 0.0,
                "offset_factor": 0.0,
                "constrained": true,
                "execution_control_type": "step",
                "mesh_smoothing_frequency": 4.0,
                "variables_smoothing_frequency": 4.0
             },
             "refine": false,
             "refine_options": {},
             "contact": true,
             "contact_options":{
                 "contact_condition": "ContactDomainLM2DCondition",
                 "friction_law": "FrictionLaw",
                 "friction_properties":{
                     "FRICTION_ACTIVE": false,
                     "MU_STATIC": 0.3,
                     "MU_DYNAMIC": 0.2,
                     "CONTACT_ADHESION": 0.0,
                     "PENALTY_PARAMETER": 1000,
                     "TANGENTIAL_PENALTY_RATIO": 1.0,
                     "TAU_STAB": 1.0
                 },
                 "contact_parts": []
             }
        }
        """)

        # overwrite the default settings with user-provided parameters
        self.settings = custom_settings
        default_settings["contact_options"]["friction_properties"].AddMissingParameters(self.settings["contact_options"]["friction_properties"])
        self.settings.RecursivelyValidateAndAssignDefaults(default_settings)

        #print("::[Contact_Mesher_Strategy]:: Construction of Meshing Strategy finished")
        self.echo_level = 0
        self.model_part = model_part

    #
    def Initialize(self):
        self.dimension = self.model_part.ProcessInfo[KratosMultiphysics.SPACE_DIMENSION]

        # set meshing parameters
        self.MeshingParameters = self._get_meshing_parameters()

        # set contact parameters
        contact_options = self.settings["contact_options"]
        domain_name = "contact"
        self.contact_parts = []
        for part in contact_options["contact_parts"].values():
            self.contact_parts.append(part.GetString())
            domain_name = domain_name + "_" + part.GetString()
        if not self.contact_parts:
            raise Exception(self._class_prefix()+" Contact model parts not supplied ",self.contact_parts)

        # set model part name
        self.MeshingParameters.SetSubModelPartName(domain_name)

        # Create contact domain model_part (considering prepended parent name)
        if not self.model_part.HasSubModelPart(domain_name):
            self.model_part.CreateSubModelPart(domain_name)

        self.build_contact_model_part = KratosContact.BuildContactModelPart(
            self.model_part, self.MeshingParameters, self.contact_parts, 1)

        print(self._class_prefix()+" Build contact model part", domain_name)
        self.build_contact_model_part.Execute()

        # set contact properties
        properties = self._get_contact_properties(contact_options)
        self.MeshingParameters.SetProperties(properties)

        # mesh meshers for the current strategy
        self.meshers = self._get_meshers()

        # initialize meshers
        for mesher in self.meshers:
            mesher.SetEchoLevel(self.echo_level)
            mesher.Initialize(self.dimension)

        print(self._class_prefix()+" Ready")

    #
    def InitializeMeshGeneration(self):

        # if the boundaries has been changed the contact domain has to be updated
        self.build_contact_model_part.Execute()

        info_parameters = self.MeshingParameters.GetInfoParameters()
        info_parameters.Initialize(self.model_part)

        self._set_mesh_info()

    #
    def FinalizeMeshGeneration(self):
        self._set_mesh_info()


    #### Contact Meshing strategy internal methods ####

    #
    def _get_meshers(self):
        meshers = []
        if(self.echo_level > 0):
            print(self._class_prefix()+" ["+self.MeshingParameters.GetSubModelPartName()+" model part ] (REMESH:", self.settings["remesh"].GetBool(
            ), "/ REFINE:", self.settings["refine"].GetBool(), ")")

        meshers_list = []
        if(self.settings["remesh"].GetBool()):
            meshers_list.append("KratosMultiphysics.ContactMechanicsApplication.contact_mesher")

        import importlib
        for mesher in meshers_list:
            meshing_module = importlib.import_module(mesher)
            new_mesher = meshing_module.CreateMesher(
                self.model_part, self.MeshingParameters)
            meshers.append(new_mesher)

        return meshers

    #
    def _get_contact_properties(self, contact_options):
        properties_id = KratosMeshers.ModelPartUtilities.GetMaxPropertiesId(self.model_part)
        properties = KratosMultiphysics.Properties(properties_id+1)

        # build friction law :: pass it as a property parameter
        friction_law_module = "KratosMultiphysics.ContactMechanicsApplication"
        friction_law_type_name = contact_options["friction_law"].GetString()

        # import module if not previously imported
        import importlib
        module = importlib.import_module(friction_law_module)
        FrictionLaw = getattr(module, friction_law_type_name)
        friction_law = FrictionLaw()

        # properties have not python interface for this variable type
        #properties.SetValue( KratosContact.FRICTION_LAW, friction_law.Clone() )
        properties.SetValue(KratosMultiphysics.KratosGlobals.GetVariable(
            "FRICTION_LAW_NAME"), friction_law_type_name)

        contact_variables = contact_options["friction_properties"]

        # iterators of a json list are not working right now :: must be done by hand:
        properties.SetValue(KratosMultiphysics.KratosGlobals.GetVariable(
            "FRICTION_ACTIVE"), contact_variables["FRICTION_ACTIVE"].GetBool())
        properties.SetValue(KratosMultiphysics.KratosGlobals.GetVariable(
            "MU_STATIC"), contact_variables["MU_STATIC"].GetDouble())
        properties.SetValue(KratosMultiphysics.KratosGlobals.GetVariable(
            "MU_DYNAMIC"), contact_variables["MU_DYNAMIC"].GetDouble())
        properties.SetValue(KratosMultiphysics.KratosGlobals.GetVariable(
            "CONTACT_ADHESION"), contact_variables["CONTACT_ADHESION"].GetDouble())
        properties.SetValue(KratosMultiphysics.KratosGlobals.GetVariable(
            "PENALTY_PARAMETER"), contact_variables["PENALTY_PARAMETER"].GetDouble())
        properties.SetValue(KratosMultiphysics.KratosGlobals.GetVariable(
            "TANGENTIAL_PENALTY_RATIO"), contact_variables["TANGENTIAL_PENALTY_RATIO"].GetDouble())
        properties.SetValue(KratosMultiphysics.KratosGlobals.GetVariable(
            "TAU_STAB"), contact_variables["TAU_STAB"].GetDouble())

        sub_properties = KratosMultiphysics.Properties(properties)
        sub_properties.Id = properties_id+2

        properties.AddSubProperties(sub_properties)

        return properties

    #
    def _get_meshing_parameters(self):

        # Create MeshingParameters
        parameters = KratosMeshers.MeshingParameters()
        parameters.Initialize()

        if self.settings["remesh"].GetBool():
            domain_sizes = self._get_domain_size_parameters()
            alpha_shape = self.settings["meshing_options"]["alpha_shape"].GetDouble()
            if alpha_shape == 0.0:
                alpha_shape = domain_sizes["alpha_shape"]
            parameters.SetAlphaParameter(alpha_shape)
            offset_factor = self.settings["meshing_options"]["offset_factor"].GetDouble()
            if offset_factor == 0.0:
                offset_factor = domain_sizes["boundary_size"]/4.0
            parameters.SetOffsetFactor(offset_factor)

            parameters.SetInfoParameters(self._get_info_parameters())


        # set meshing options
        parameters.SetOptions(self._get_meshing_options())

        parameters.SetReferenceCondition(self.settings["contact_options"]["contact_condition"].GetString())

        # set reference elements and condition
        """
        if self.dimension == 2:
            parameters.SetReferenceCondition("ContactDomainLMCondition2D3N")
        elif self.dimension == 3:
            parameters.SetReferenceCondition("ContactDomainLMCondition3D4N")
        """

        return parameters

    #
    def _get_info_parameters(self):
        parameters = KratosMeshers.MeshingInfoParameters()
        parameters.Initialize(self.model_part)
        return parameters
    #
    def _get_meshing_options(self):

        meshing_options = KratosMultiphysics.Flags()

        # mesher options
        meshing_options.Set(KratosMeshers.MesherData.REMESH, self.settings["remesh"].GetBool())
        meshing_options.Set(KratosMeshers.MesherData.REFINE, self.settings["refine"].GetBool())
        meshing_options.Set(KratosMeshers.MesherData.CONTACT_SEARCH, self.settings["contact"].GetBool())

        # meshing options
        meshing_options.Set(KratosMeshers.MesherData.CONSTRAINED, self.settings["meshing_options"]["constrained"].GetBool())

        return meshing_options

    #
    def _get_domain_size_parameters(self):

        # contact alpha shape
        alpha_shape = {"2":1.2, "3":5.0}

        element_size, boundary_size = self._calculate_mesh_sizes()

        print(self._class_prefix()+" Global mesh sizes: (interior:" + f"{element_size:2.6f}" + "/boundary:" + f"{boundary_size:2.6f}"+")")

        element_volume = self._calculate_mean_volume(element_size)

        domain_size = {"alpha_shape":alpha_shape[str(self.dimension)], "element_size":element_size, "boundary_size":boundary_size, "element_volume": element_volume}

        return domain_size

    #
    def _calculate_mesh_sizes(self):
        number_of_nodes = 0
        mean_nodal_size = 0
        for node in self.model_part.Nodes: #slow
            if (node.IsNot(KratosMultiphysics.RIGID)):
                number_of_nodes += 1
                mean_nodal_size = mean_nodal_size + node.GetSolutionStepValue(KratosMultiphysics.NODAL_H)

            interior_mesh_size = 0.25 * mean_nodal_size / number_of_nodes
            boundary_mesh_size = interior_mesh_size * 3

        return interior_mesh_size, boundary_mesh_size
    #
    @classmethod
    def _class_prefix(self):
        header = "::[--Contact Strategy-]::"
        return header
