""" Project: ContactMechanicsApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""


# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.MeshersApplication as KratosMeshers
import KratosMultiphysics.ContactMechanicsApplication as KratosContact


def Factory(settings, Model):
    if(not isinstance(settings, KratosMultiphysics.Parameters)):
        raise Exception(
            "Expected input shall be a Parameters object, encapsulating a json string")
    return RigidBodiesProcess(Model, settings["Parameters"])


class RigidBodiesProcess(KratosMultiphysics.Process):
    #
    def __init__(self, Model, custom_settings):

        KratosMultiphysics.Process.__init__(self)

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
            "model_part_name" : "Solid Domain",
            "rigid_bodies"    : []
        }
        """)

        # overwrite the default settings with user-provided parameters
        self.settings = custom_settings
        self.settings.ValidateAndAssignDefaults(default_settings)

        self.echo_level = 1

        self.Model = Model

    #
    def ExecuteInitialize(self):

        self.main_model_part = self.Model[self.settings["model_part_name"].GetString(
        )]

        # construct rigid body domains
        self.rigid_bodies = []
        bodies_list = self.settings["rigid_bodies"]
        self.number_of_bodies = bodies_list.size()
        import importlib
        for i in range(0, self.number_of_bodies):
            item = bodies_list[i]
            rigid_body_module_name = None
            if item.Has("python_module"):
                rigid_body_module_name = "KratosMultiphysics.ContactMechanicsApplication." + \
                    item["python_module"].GetString()
            if item.Has("process_module"):
                 rigid_body_module_name = "KratosMultiphysics." + \
                    item["process_module"].GetString()
            rigid_body_module = importlib.import_module(rigid_body_module_name)
            body = rigid_body_module.CreateRigidBody(
                self.main_model_part, item["Parameters"])
            self.rigid_bodies.append(body)

        # initialize rigid body domains
        import KratosMultiphysics.MeshersApplication.domain_utilities as domain_utilities
        domain_utils = domain_utilities.DomainUtilities()
        domain_utils.InitializeDomains(self.main_model_part, self.echo_level)

        for body in self.rigid_bodies:
            body.ExecuteInitialize()

        print(self._class_prefix()+" Ready")

    ###

    #
    def ExecuteInitializeSolutionStep(self):
        for body in self.rigid_bodies:
            body.ExecuteInitializeSolutionStep()

    #
    def ExecuteFinalizeSolutionStep(self):
        for body in self.rigid_bodies:
            body.ExecuteFinalizeSolutionStep()

    ###

    #
    @classmethod
    def GetVariables(self):
        import KratosMultiphysics.MeshersApplication.domain_utilities as domain_utilities
        nodal_variables = domain_utilities.DomainUtilities().GetVariables()
        nodal_variables = nodal_variables + ['RIGID_WALL']
        return nodal_variables

    #
    @classmethod
    def _class_prefix(self):
        header = "::[----Rigid Bodies---]::"
        return header
