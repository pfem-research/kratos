//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                August 2016 $
//
//

// System includes

// External includes

// Project includes
#include "geometries/point_2d.h"
#include "geometries/point_3d.h"
#include "geometries/triangle_2d_3.h"
#include "geometries/tetrahedra_3d_4.h"

#include "contact_mechanics_application.h"

namespace Kratos
{

//Application variables creation: (see contact_mechanics_application_variables.cpp)

//Application Constructor:
KratosContactMechanicsApplication::KratosContactMechanicsApplication()
    : KratosApplication("ContactMechanicsApplication"),
      mContactDomainLMCondition3D4N(0, Kratos::make_shared<Tetrahedra3D4<Node<3>>>(Condition::GeometryType::PointsArrayType(4))),
      mContactDomainPenaltyCondition3D4N(0, Kratos::make_shared<Tetrahedra3D4<Node<3>>>(Condition::GeometryType::PointsArrayType(4))),
      mContactDomainLMCondition2D3N(0, Kratos::make_shared<Triangle2D3<Node<3>>>(Condition::GeometryType::PointsArrayType(3))),
      mContactDomainPenaltyCondition2D3N(0, Kratos::make_shared<Triangle2D3<Node<3>>>(Condition::GeometryType::PointsArrayType(3))),
      mContactDomainEPCondition2D3N(0, Kratos::make_shared<Triangle2D3<Node<3>>>(Condition::GeometryType::PointsArrayType(3))),
      mAxisymContactDomainLMCondition2D3N(0, Kratos::make_shared<Triangle2D3<Node<3>>>(Condition::GeometryType::PointsArrayType(3))),
      mAxisymContactDomainPenaltyCondition2D3N(0, Kratos::make_shared<Triangle2D3<Node<3>>>(Condition::GeometryType::PointsArrayType(3))),
      mAxisymContactDomainEPCondition2D3N(0, Kratos::make_shared<Triangle2D3<Node<3>>>(Condition::GeometryType::PointsArrayType(3))),
      mThermalContactDomainPenaltyCondition3D4N(0, Kratos::make_shared<Tetrahedra3D4<Node<3>>>(Condition::GeometryType::PointsArrayType(4))),
      mThermalContactDomainPenaltyCondition2D3N(0, Kratos::make_shared<Triangle2D3<Node<3>>>(Condition::GeometryType::PointsArrayType(3))),
      mAxisymThermalContactDomainPenaltyCondition2D3N(0, Kratos::make_shared<Triangle2D3<Node<3>>>(Condition::GeometryType::PointsArrayType(3))),
      mRigidBodyPointLinkCondition2D1N(0, Kratos::make_shared<Point2D<Node<3>>>(Condition::GeometryType::PointsArrayType(1))),
      mRigidBodyPointLinkCondition3D1N(0, Kratos::make_shared<Point3D<Node<3>>>(Condition::GeometryType::PointsArrayType(1))),
      mRigidBodyPointLinkSegregatedVCondition2D1N(0, Kratos::make_shared<Point2D<Node<3>>>(Condition::GeometryType::PointsArrayType(1))),
      mRigidBodyPointLinkSegregatedVCondition3D1N(0, Kratos::make_shared<Point3D<Node<3>>>(Condition::GeometryType::PointsArrayType(1))),
      mPointRigidContactPenaltyCondition2D1N(0, Kratos::make_shared<Point2D<Node<3>>>(Condition::GeometryType::PointsArrayType(1))),
      mPointRigidContactPenaltyCondition3D1N(0, Kratos::make_shared<Point3D<Node<3>>>(Condition::GeometryType::PointsArrayType(1))),
      mAxisymPointRigidContactPenaltyCondition2D1N(0, Kratos::make_shared<Point2D<Node<3>>>(Condition::GeometryType::PointsArrayType(1))),
      mPointRigidContactPenaltyVCondition2D1N(0, Kratos::make_shared<Point2D<Node<3>>>(Condition::GeometryType::PointsArrayType(1))),
      mPointRigidContactPenaltyVCondition3D1N(0, Kratos::make_shared<Point3D<Node<3>>>(Condition::GeometryType::PointsArrayType(1))),
      mPointRigidContactPenaltySegregatedVCondition2D1N(0, Kratos::make_shared<Point2D<Node<3>>>(Condition::GeometryType::PointsArrayType(1))),
      mPointRigidContactPenaltySegregatedVCondition3D1N(0, Kratos::make_shared<Point3D<Node<3>>>(Condition::GeometryType::PointsArrayType(1))),
      mBeamPointRigidContactLMCondition3D1N(0, Kratos::make_shared<Point3D<Node<3>>>(Condition::GeometryType::PointsArrayType(1))),
      mBeamPointRigidContactPenaltyCondition3D1N(0, Kratos::make_shared<Point3D<Node<3>>>(Condition::GeometryType::PointsArrayType(1))),
      mRigidBodyPointRigidContactCondition3D1N(0, Kratos::make_shared<Point3D<Node<3>>>(Condition::GeometryType::PointsArrayType(1))),
      mEPPointRigidContactPenaltyCondition2D1N(0, Kratos::make_shared<Point2D<Node<3>>>(Condition::GeometryType::PointsArrayType(1))),
      mEPPointRigidContactPenaltyCondition3D1N(0, Kratos::make_shared<Point3D<Node<3>>>(Condition::GeometryType::PointsArrayType(1))),
      mEPAxisymPointRigidContactPenaltyCondition2D1N(0, Kratos::make_shared<Point2D<Node<3>>>(Condition::GeometryType::PointsArrayType(1))),
      mEPCPTCondition2D1N(0, Kratos::make_shared<Point2D<Node<3>>>(Condition::GeometryType::PointsArrayType(1))),
      mHydraulicRigidContactPenaltyCondition3D1N(0, Kratos::make_shared<Point3D<Node<3>>>(Condition::GeometryType::PointsArrayType(1))),
      mHydraulicAxisymRigidContactPenaltyCondition2D1N(0, Kratos::make_shared<Point2D<Node<3>>>(Condition::GeometryType::PointsArrayType(1)))
{
}

void KratosContactMechanicsApplication::Register()
{
  std::stringstream banner;

  banner << "             ___         _           _           \n"
         << "    KRATOS  / __|___ _ _| |_ __ _ __| |_           \n"
         << "           | (__/ _ \\ ' \\  _/ _` / _|  _|          \n"
         << "            \\___\\___/_||_\\__\\__,_\\__|\\__| MECHANICS\n"
         << "Initialize KratosContactMechanicsApplication... " << std::endl;

  // mpi initialization
  int mpi_is_initialized = 0;
  int rank = -1;

#ifdef KRATOS_MPI

  MPI_Initialized(&mpi_is_initialized);

  if (mpi_is_initialized)
  {
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  }

#endif

  if (mpi_is_initialized)
  {
    if (rank == 0)
      KRATOS_INFO("") << banner.str();
  }
  else
  {
    KRATOS_INFO("") << banner.str();
  }

  //Register Rigid Bodies
  KRATOS_REGISTER_ELEMENT("RigidBodyElement", mRigidBodyElement)
  KRATOS_REGISTER_ELEMENT("RigidBodySegregatedVElement", mRigidBodySegregatedVElement)
  KRATOS_REGISTER_ELEMENT("TranslationalRigidBodyElement", mTranslationalRigidBodyElement)
  KRATOS_REGISTER_ELEMENT("TranslationalRigidBodySegregatedVElement", mTranslationalRigidBodySegregatedVElement)

  //Register Conditions
  KRATOS_REGISTER_CONDITION("ContactDomainLMCondition3D4N", mContactDomainLMCondition3D4N)
  KRATOS_REGISTER_CONDITION("ContactDomainPenaltyCondition3D4N", mContactDomainPenaltyCondition3D4N)
  KRATOS_REGISTER_CONDITION("ContactDomainLMCondition2D3N", mContactDomainLMCondition2D3N)
  KRATOS_REGISTER_CONDITION("ContactDomainPenaltyCondition2D3N", mContactDomainPenaltyCondition2D3N)
  KRATOS_REGISTER_CONDITION("ContactDomainEPCondition2D3N", mContactDomainEPCondition2D3N)

  KRATOS_REGISTER_CONDITION("AxisymContactDomainLMCondition2D3N", mAxisymContactDomainLMCondition2D3N)
  KRATOS_REGISTER_CONDITION("AxisymContactDomainPenaltyCondition2D3N", mAxisymContactDomainPenaltyCondition2D3N)
  KRATOS_REGISTER_CONDITION("AxisymContactDomainEPCondition2D3N", mAxisymContactDomainEPCondition2D3N)

  KRATOS_REGISTER_CONDITION("ThermalContactDomainPenaltyCondition3D4N", mThermalContactDomainPenaltyCondition3D4N)
  KRATOS_REGISTER_CONDITION("ThermalContactDomainPenaltyCondition2D3N", mThermalContactDomainPenaltyCondition2D3N)
  KRATOS_REGISTER_CONDITION("AxisymThermalContactDomainPenaltyCondition2D3N", mAxisymThermalContactDomainPenaltyCondition2D3N)

  KRATOS_REGISTER_CONDITION("RigidBodyPointLinkCondition2D1N", mRigidBodyPointLinkCondition2D1N)
  KRATOS_REGISTER_CONDITION("RigidBodyPointLinkCondition3D1N", mRigidBodyPointLinkCondition3D1N)

  KRATOS_REGISTER_CONDITION("RigidBodyPointLinkSegregatedVCondition2D1N", mRigidBodyPointLinkSegregatedVCondition2D1N)
  KRATOS_REGISTER_CONDITION("RigidBodyPointLinkSegregatedVCondition3D1N", mRigidBodyPointLinkSegregatedVCondition3D1N)

  KRATOS_REGISTER_CONDITION("PointContactPenaltyCondition2D1N", mPointRigidContactPenaltyCondition2D1N)
  KRATOS_REGISTER_CONDITION("PointContactPenaltyCondition3D1N", mPointRigidContactPenaltyCondition3D1N)
  KRATOS_REGISTER_CONDITION("AxisymPointContactPenaltyCondition2D1N", mAxisymPointRigidContactPenaltyCondition2D1N)

  KRATOS_REGISTER_CONDITION("PointContactPenaltyVCondition2D1N", mPointRigidContactPenaltyVCondition2D1N)
  KRATOS_REGISTER_CONDITION("PointContactPenaltyVCondition3D1N", mPointRigidContactPenaltyVCondition3D1N)

  KRATOS_REGISTER_CONDITION("PointContactPenaltySegregatedVCondition2D1N", mPointRigidContactPenaltySegregatedVCondition2D1N)
  KRATOS_REGISTER_CONDITION("PointContactPenaltySegregatedVCondition3D1N", mPointRigidContactPenaltySegregatedVCondition3D1N)

  KRATOS_REGISTER_CONDITION("BeamPointContactLMCondition3D1N", mBeamPointRigidContactLMCondition3D1N)
  KRATOS_REGISTER_CONDITION("BeamPointContactPenaltyCondition3D1N", mBeamPointRigidContactPenaltyCondition3D1N)
  KRATOS_REGISTER_CONDITION("RigidBodyPointContactCondition3D1N", mRigidBodyPointRigidContactCondition3D1N)

  KRATOS_REGISTER_CONDITION("EPPointContactPenaltyCondition2D1N", mEPPointRigidContactPenaltyCondition2D1N)
  KRATOS_REGISTER_CONDITION("EPPointContactPenaltyCondition3D1N", mEPPointRigidContactPenaltyCondition3D1N)
  KRATOS_REGISTER_CONDITION("EPAxisymPointContactPenaltyCondition2D1N", mEPAxisymPointRigidContactPenaltyCondition2D1N)
  KRATOS_REGISTER_CONDITION("EPCPTCondition2D1N", mEPCPTCondition2D1N);

  KRATOS_REGISTER_CONDITION("HydraulicPointContactPenaltyCondition3D1N", mHydraulicRigidContactPenaltyCondition3D1N)
  KRATOS_REGISTER_CONDITION("HydraulicAxisymPointContactPenaltyCondition2D1N", mHydraulicAxisymRigidContactPenaltyCondition2D1N)

  //Register friction laws
  Serializer::Register("FrictionLaw", mFrictionLaw);
  Serializer::Register("CoulombAdhesionFrictionLaw", mCoulombAdhesionFrictionLaw);
  Serializer::Register("HardeningCoulombFrictionLaw", mHardeningCoulombFrictionLaw);

  //Register bounding boxes
  Serializer::Register("PlaneBoundingBox", mPlaneBoundingBox);
  Serializer::Register("SphereBoundingBox", mSphereBoundingBox);
  Serializer::Register("CircleBoundingBox", mCircleBoundingBox);
  Serializer::Register("CylinderBoundingBox", mCylinderBoundingBox);
  Serializer::Register("CompoundNosesBoundingBox", mCompoundNosesBoundingBox);
  Serializer::Register("TubeBoundingBox", mTubeBoundingBox);

  //Register Variables
  KRATOS_REGISTER_VARIABLE(FRICTION_LAW_NAME)
  KRATOS_REGISTER_VARIABLE(FRICTION_LAW)
  KRATOS_REGISTER_VARIABLE(HYDRAULIC)

  //contact properties
  KRATOS_REGISTER_VARIABLE(BOUNDING_BOX)
  KRATOS_REGISTER_VARIABLE(FRICTION_ACTIVE)
  KRATOS_REGISTER_VARIABLE(PENALTY_PARAMETER)
  KRATOS_REGISTER_VARIABLE(PENALTY_PARAMETER_SHAFT)
  KRATOS_REGISTER_VARIABLE(LAGRANGE_MULTIPLIER_NORMAL)
  KRATOS_REGISTER_VARIABLE(LAGRANGE_MULTIPLIER_NORMAL_REACTION)
  KRATOS_REGISTER_VARIABLE(LAGRANGE_MULTIPLIER_TANGENTIAL)
  KRATOS_REGISTER_VARIABLE(LAGRANGE_MULTIPLIER_TANGENTIAL_REACTION)
  KRATOS_REGISTER_VARIABLE(TAU_STAB)
  KRATOS_REGISTER_VARIABLE(MU_STATIC)
  KRATOS_REGISTER_VARIABLE(MU_DYNAMIC)

  //contact postprocess
  KRATOS_REGISTER_3D_VARIABLE_WITH_COMPONENTS(WATER_CONTACT_FORCE)
  KRATOS_REGISTER_3D_VARIABLE_WITH_COMPONENTS(CONTACT_STRESS)
  KRATOS_REGISTER_3D_VARIABLE_WITH_COMPONENTS(EFFECTIVE_CONTACT_STRESS)
  KRATOS_REGISTER_3D_VARIABLE_WITH_COMPONENTS(EFFECTIVE_CONTACT_FORCE)
  KRATOS_REGISTER_VARIABLE(CONTACT_ADHESION)
  KRATOS_REGISTER_VARIABLE(CONTACT_FRICTION_ANGLE)
  KRATOS_REGISTER_VARIABLE(DESIRED_CONTACT_FRICTION_ANGLE)
  KRATOS_REGISTER_VARIABLE(TANGENTIAL_PENALTY_RATIO)
  KRATOS_REGISTER_VARIABLE(CONTACT_PLASTIC_SLIP)
  KRATOS_REGISTER_VARIABLE(CONTACT_AREA)
  KRATOS_REGISTER_VARIABLE(CONTACT_DEGRADATION)
  KRATOS_REGISTER_VARIABLE(CONTACT_VELOCITY)
  KRATOS_REGISTER_VARIABLE(FRICTION_EFFICIENCY)

  //thermal properties
  KRATOS_REGISTER_VARIABLE(THERMAL_CONDUCTIVITY)

  //beam properties
  KRATOS_REGISTER_VARIABLE(CROSS_SECTION_AREA)
  KRATOS_REGISTER_VARIABLE(CROSS_SECTION_RADIUS)
  KRATOS_REGISTER_VARIABLE(SPLINE_ID)
  KRATOS_REGISTER_VARIABLE(NORMALIZED_ARCH_LENGTH)

  //solution
  KRATOS_REGISTER_VARIABLE(SOLVER_STEP)
  KRATOS_REGISTER_VARIABLE(CONTACT_STEP_TIME)
  KRATOS_REGISTER_VARIABLE(RESTART_STEP_TIME)

  KRATOS_REGISTER_VARIABLE(DEGREE_OF_SATURATION_EFF)

  Serializer::Register("CircleBoundingBox", mCircleBoundingBox);
  Serializer::Register("CylinderBoundingBox", mCylinderBoundingBox);
  Serializer::Register("SphereBoundingBox", mSphereBoundingBox);
  Serializer::Register("CompoundNosesBoundingBox", mCompoundNosesBoundingBox);
  Serializer::Register("PlaneBoundingBox", mPlaneBoundingBox);
  Serializer::Register("TubeBoundingBox", mTubeBoundingBox);
}

} // namespace Kratos.
