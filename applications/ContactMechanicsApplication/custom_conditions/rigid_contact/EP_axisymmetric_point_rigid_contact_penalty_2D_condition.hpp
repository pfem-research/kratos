//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:             LMonforte $
//   Maintained by:       $Maintainer:                   LM $
//   Date:                $Date:                  July 2016 $
//
//

#if !defined(KRATOS_EP_AXISYMMETRIC_POINT_RIGID_CONTACT_PENALTY_2D_CONDITION_HPP_INCLUDED)
#define KRATOS_EP_AXISYMMETRIC_POINT_RIGID_CONTACT_PENALTY_2D_CONDITION_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_conditions/rigid_contact/EP_point_rigid_contact_penalty_2D_condition.hpp"

namespace Kratos
{

///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
*/
class KRATOS_API(CONTACT_MECHANICS_APPLICATION) EPAxisymmetricPointRigidContactPenalty2DCondition
    : public EPPointRigidContactPenalty2DCondition
{
public:
    ///@name Type Definitions
    typedef EPPointRigidContactPenalty2DCondition BaseType;

    typedef BaseType::NodeType NodeType;
    typedef BaseType::PointType PointType;
    typedef BaseType::SizeType SizeType;
    typedef BaseType::NodeWeakPtrVectorType NodeWeakPtrVectorType;
    typedef BaseType::ElementWeakPtrVectorType ElementWeakPtrVectorType;
    typedef BaseType::ConditionWeakPtrVectorType ConditionWeakPtrVectorType;

    ///@{
    // Counted pointer of PointRigidContactPenalty2DCondition
    KRATOS_CLASS_INTRUSIVE_POINTER_DEFINITION(EPAxisymmetricPointRigidContactPenalty2DCondition);
    ///@}

    ///@}
    ///@name Life Cycle
    ///@{

    /// Serialization constructor
    EPAxisymmetricPointRigidContactPenalty2DCondition(){};

    /// Default constructor
    EPAxisymmetricPointRigidContactPenalty2DCondition(IndexType NewId, GeometryType::Pointer pGeometry);

    EPAxisymmetricPointRigidContactPenalty2DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties);

    EPAxisymmetricPointRigidContactPenalty2DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties, SpatialBoundingBox::Pointer pRigidWall);

    /// Copy constructor
    EPAxisymmetricPointRigidContactPenalty2DCondition(EPAxisymmetricPointRigidContactPenalty2DCondition const &rOther);

    /// Destructor.
    virtual ~EPAxisymmetricPointRigidContactPenalty2DCondition();

    ///@}
    ///@name Operators
    ///@{

    ///@}
    ///@name Operations
    ///@{

    /**
     * creates a new condition pointer
     * @param NewId: the ID of the new condition
     * @param ThisNodes: the nodes of the new condition
     * @param pProperties: the properties assigned to the new condition
     * @return a Pointer to the new condition
     */
    Condition::Pointer Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const override;

    /**
     * clones the selected condition variables, creating a new one
     * @param NewId: the ID of the new condition
     * @param ThisNodes: the nodes of the new condition
     * @param pProperties: the properties assigned to the new condition
     * @return a Pointer to the new condition
     */
    Condition::Pointer Clone(IndexType NewId,
                             NodesArrayType const &ThisNodes) const override;

    ///@}
    ///@name Access
    ///@{
    ///@}
    ///@name Inquiry
    ///@{
    ///@}
    ///@name Input and output
    ///@{
    ///@}
    ///@name Friends
    ///@{
    ///@}

protected:
    ///@name Protected static Member Variables
    ///@{
    ///@}
    ///@name Protected member Variables
    ///@{
    ///@}
    ///@name Protected Operators
    ///@{
    ///@}
    ///@name Protected Operations
    ///@{

    /**
     * Calculate Condition Kinematics
     */
    void CalculateKinematics(ConditionVariables &rVariables,
                             const ProcessInfo &rCurrentProcessInfo,
                             const double &rPointNumber) override;

    /**
     * Calculation of the contidion radius (axisymmetry)
     */
    void CalculateRadius(double &rCurrentRadius,
                         double &rReferenceRadius);


    /**
     * Calculation of the Integration Weight
     */
    double &CalculateIntegrationWeight(ConditionVariables &rVariables,
                                       double &rIntegrationWeight) override;

    ///@}
    ///@name Protected  Access
    ///@{
    ///@}
    ///@name Protected Inquiry
    ///@{
    ///@}
    ///@name Protected LifeCycle
    ///@{
    ///@}

private:
    ///@name Static Member Variables
    ///@{
    ///@}
    ///@name Member Variables
    ///@{
    ///@}
    ///@name Private Operators
    ///@{
    ///@}
    ///@name Private Operations
    ///@{
    ///@}
    ///@name Private  Access
    ///@{
    ///@}
    ///@name Private Inquiry
    ///@{

    ///@}
    ///@name Serialization
    ///@{

    friend class Serializer;

    virtual void save(Serializer &rSerializer) const override
    {
        KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, EPPointRigidContactPenalty2DCondition)
    }

    virtual void load(Serializer &rSerializer) override
    {
        KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, EPPointRigidContactPenalty2DCondition)
    }

}; // Class EPAxisymmetricPointRigidContactPenalty2DCondition

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
///@}

} // namespace Kratos.

#endif // KRATOS_AXISYMMETRIC_POINT_RIGID_CONTACT_PENALTY_2D_CONDITION_HPP_INCLUDED  defined
