//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:             LMonforte $
//   Maintained by:       $Maintainer:                   LM $
//   Date:                $Date:                  July 2016 $
//
//

// System includes

// External includes

// Project includes
#include "custom_conditions/rigid_contact/EP_cpt_condition.hpp"

#include "contact_mechanics_application_variables.h"
#include "custom_friction/hardening_coulomb_friction_law.hpp"

namespace Kratos
{
//************************************************************************************
//************************************************************************************
EPCPTCondition::EPCPTCondition(IndexType NewId, GeometryType::Pointer
                                                    pGeometry)
    : EPAxisymmetricPointRigidContactPenalty2DCondition(NewId, pGeometry)
{

   //DO NOT ADD DOFS HERE!!!
}

//************************************************************************************
//************************************************************************************
EPCPTCondition::EPCPTCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : EPAxisymmetricPointRigidContactPenalty2DCondition(NewId, pGeometry, pProperties)
{
}

//************************************************************************************
//************************************************************************************
EPCPTCondition::EPCPTCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties, SpatialBoundingBox::Pointer pRigidWall)
    : EPAxisymmetricPointRigidContactPenalty2DCondition(NewId, pGeometry, pProperties, pRigidWall)
{
}

//************************************************************************************
//************************************************************************************
EPCPTCondition::EPCPTCondition(EPCPTCondition const &rOther)
    : EPAxisymmetricPointRigidContactPenalty2DCondition(rOther)
{
   this->mpFrictionLaw = Kratos::make_shared<HardeningCoulombFrictionLaw>();
   if ( rOther.mpFrictionLaw != nullptr) {
      this->mpFrictionLaw->SetHistoryVariable( rOther.mpFrictionLaw->GetHistoryVariable() );
   }
}

//************************************************************************************
//************************************************************************************

Condition::Pointer EPCPTCondition::Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const
{
   return Kratos::make_intrusive<EPCPTCondition>(NewId, GetGeometry().Create(ThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************

Condition::Pointer EPCPTCondition::Clone(IndexType NewId, NodesArrayType const &ThisNodes) const
{
   EPCPTCondition ClonedCondition(NewId, GetGeometry().Create(ThisNodes), pGetProperties(), mpRigidWall);
   if (this->mpFrictionLaw!=nullptr) {
     ClonedCondition.mpFrictionLaw = this->mpFrictionLaw->Clone();
   }
   ClonedCondition.mCurrentInfo = this->mCurrentInfo;
   ClonedCondition.mSavedInfo = this->mSavedInfo;
   return Kratos::make_intrusive<EPCPTCondition>(ClonedCondition);
}

//************************************************************************************
//************************************************************************************

EPCPTCondition::~EPCPTCondition()
{
}

void EPCPTCondition::InitializeNonLinearIteration(const ProcessInfo &rCurrentProcessInfo)
{
   KRATOS_TRY

   mTime = rCurrentProcessInfo[TIME];

   EPAxisymmetricPointRigidContactPenalty2DCondition::InitializeNonLinearIteration(rCurrentProcessInfo);

   KRATOS_CATCH("")
}

// *********************** CalculateFrictionLaw *************************************
//***********************************************************************************
bool EPCPTCondition::CalculateFrictionLaw(ConditionVariables &rVariables, ConstitutiveVariables &rConstitutiveVariables, Vector &rTangentForceVector)
{
   KRATOS_TRY


   if ( mImplex) {
      Matrix ForceMatrix = MathUtils<double>::StressVectorToTensor(mSavedInfo.PreviousStepForceVector);
      ForceMatrix = ConvertToTheAppropriateSize(ForceMatrix); // 2D and Axis are 2DMatrices
      const Vector &  rPreviousNormal = mSavedInfo.n;
      rTangentForceVector = prod(ForceMatrix, rPreviousNormal);
      rVariables.Slip = true;

      rConstitutiveVariables.ForceDirection = rTangentForceVector;
      if (norm_2(rTangentForceVector) > 1e-15)
         rConstitutiveVariables.ForceDirection /= norm_2(rTangentForceVector);
      rConstitutiveVariables.TangentForceRatio = 0.0;

      return rVariables.Slip;

   }


   // 0. Resize rTangentVector
   rTangentForceVector = ZeroVector(3);

   // 1. Compute the deformation gradient and the push forward of the previous force
   const Vector PreviousNormal = mSavedInfo.n;
   Vector PreviousT1 = mSavedInfo.t1;
   Vector PreviousT2 = mSavedInfo.t2;

   // if the bounding box does not have the parametric directions implemented use some sort of guess
   Vector Normal = rVariables.Surface.Normal;
   Vector T1 = rVariables.Surface.Tangent;

   if (MathUtils<double>::Dot(T1, PreviousT1) < 0)
      T1 *= (-1.0);

   Vector T2 = ZeroVector(3);
   T2(0) = Normal(1) * T1(2) - Normal(2) * T1(1);
   T2(1) = -Normal(0) * T1(2) + Normal(2) * T1(0);
   T2(2) = Normal(0) * T1(1) - Normal(1) * T1(0);

   {
      // Get the real parametrization of the Box (if exists)
      SpatialBoundingBox::BoxParametersType BoxParameters(this->GetGeometry()[0]);
      this->mpRigidWall->GetParametricDirections(BoxParameters, T1, T2);
   }

   mCurrentInfo.n = rVariables.Surface.Normal;
   mCurrentInfo.t1 = T1;
   mCurrentInfo.t2 = T2;

   Matrix F = ZeroMatrix(3);
   for (SizeType i = 0; i < 3; i++)
   {
      for (SizeType j = 0; j < 3; j++)
      {
         F(i, j) = T1(i) * PreviousT1(j) + T2(i) * PreviousT2(j) + Normal(i) * PreviousNormal(j);
      }
   }

   Matrix ForceMatrix = MathUtils<double>::StressVectorToTensor(mSavedInfo.PreviousStepForceVector);

   ForceMatrix = ConvertToTheAppropriateSize(ForceMatrix); // 2D and Axis are 2DMatrices

   ForceMatrix = prod(ForceMatrix, trans(F));
   ForceMatrix = prod(F, ForceMatrix);

   Vector PreviousForce = prod(ForceMatrix, Normal);
   PreviousForce -= inner_prod(PreviousForce, Normal) * Normal; // remove spurious normal

   // 2. Calculate the return mapping
   Vector TrialTangentForce = PreviousForce + rVariables.Penalty.Tangent * rVariables.Gap.Tangent * rVariables.Surface.Tangent;
   double ForceModulus = norm_2(TrialTangentForce);

   Vector ForceDirection = TrialTangentForce;
   if (ForceModulus > 1e-15)
      ForceDirection /= ForceModulus;

   // 2.a Prepare information
   double NormalForceModulus = 0;
   NormalForceModulus = this->CalculateNormalForceModulus(NormalForceModulus, rVariables);

   double Area = this->CalculateSomeSortOfArea();

   double Integration = 1;
   if (rVariables.CurrentRadius > 0.0)
      Integration *= 2.0 * Globals::Pi * rVariables.CurrentRadius;

   double EffectiveNormalForce = CalculateEffectiveNormalForceModulus(NormalForceModulus, Area, Integration);
   if (EffectiveNormalForce < 0.0)
      EffectiveNormalForce = 0.0;

   //std::cout << std::endl;
   //std::cout << " THIS POINT " << this->GetGeometry()[0].X() << " , " <<  this->GetGeometry()[0].Y() << std::endl;
   //std::cout << " AREA " << Area << " NormalForce " << NormalForceModulus << " EFF " << EffectiveNormalForce << std::endl;

   // FrictionVariables & contact constitutive parameters
   FrictionLaw::FrictionLawVariables FrictionVariables;
   FrictionVariables.Initialize(rVariables.Penalty.Tangent, mpFrictionLaw->GetPlasticSlip(), Area, false);


   // ConstitutiveParameters
   FrictionVariables.FrictionCoefficient = 0.0;
   FrictionVariables.Adhesion = 0;

   FrictionVariables.FrictionCoefficient = GetContactFrictionCoefficient( FrictionVariables.FrictionCoefficient);

   // no està bé, s'han de posar les coses al seu lloc
   FrictionVariables.Alpha = 0.0;
   FrictionVariables.Beta = 0.0;
   if ( GetProperties().Has(CONTACT_DEGRADATION)) {
      FrictionVariables.Alpha = GetProperties()[CONTACT_VELOCITY];
      FrictionVariables.Beta = GetProperties()[CONTACT_DEGRADATION];
      if ( GetGeometry()[0].X() < 0.05) {
         FrictionVariables.Alpha = 0.0;
         FrictionVariables.Beta = 0.0;
         mpFrictionLaw->SetHistoryVariable(0.0);
      }

   }
   if ( GetGeometry()[0].SolutionStepsDataHas(CONTACT_PLASTIC_SLIP) ) {
      double & rCPS = GetGeometry()[0].FastGetSolutionStepValue(CONTACT_PLASTIC_SLIP);
      this->mpFrictionLaw->SetHistoryVariable( rCPS );
   }

   // 2.b Perform the return mapping
   rVariables.Slip = mpFrictionLaw->EvaluateFrictionLaw(ForceModulus, EffectiveNormalForce, FrictionVariables);

   // 2.c Calculate the contact constitutive matrix, or whatever
   if (rVariables.Slip)
   {
      mpFrictionLaw->EvaluateConstitutiveComponents(rConstitutiveVariables.NormalTangentMatrix, rConstitutiveVariables.TangentTangentMatrix, ForceModulus, EffectiveNormalForce, FrictionVariables);

      rConstitutiveVariables.TangentForceRatio = ForceModulus / norm_2(TrialTangentForce);
   }
   rConstitutiveVariables.ForceDirection = ForceDirection;

   // 3. Save geometrical information
   mCurrentInfo.PreviousStepForceVector = MathUtils<double>::StressTensorToVector(ForceModulus * (outer_prod(rVariables.Surface.Normal, ForceDirection) + outer_prod(ForceDirection, rVariables.Surface.Normal)), mSavedInfo.PreviousStepForceVector.size());

   rTangentForceVector = ForceModulus * ForceDirection;


   if ( GetGeometry()[0].SolutionStepsDataHas(CONTACT_PLASTIC_SLIP) ) {
      double & rCPS = GetGeometry()[0].FastGetSolutionStepValue(CONTACT_PLASTIC_SLIP);
      rCPS = this->mpFrictionLaw->GetHistoryVariable();
   }
   if ( GetGeometry()[0].SolutionStepsDataHas(CONTACT_FRICTION_ANGLE) ) {
      double & rCPS = GetGeometry()[0].FastGetSolutionStepValue(CONTACT_FRICTION_ANGLE);
      rCPS = this->mpFrictionLaw->GetFrictionalVariable();
   }


   return rVariables.Slip;

   KRATOS_CATCH("")
}


double & EPCPTCondition::GetContactFrictionCoefficient( double & rFrictionCoefficient)
{
   KRATOS_TRY
   double YMax = 0.05;

   rFrictionCoefficient = 0.0;
   if (GetGeometry()[0].Y() > YMax) {
      rFrictionCoefficient = 0.0;
      return rFrictionCoefficient;
   }
   if (GetGeometry()[0].Y() < YMax)
   {
      if (GetProperties().Has(CONTACT_FRICTION_ANGLE))
      {
         rFrictionCoefficient = GetProperties()[CONTACT_FRICTION_ANGLE];
      }
   }
   rFrictionCoefficient = std::tan(3.14159 * rFrictionCoefficient / 180.0);

   ElementWeakPtrVectorType & nElements = GetGeometry()[0].GetValue(NEIGHBOUR_ELEMENTS);
   double FrictionAngle(0.0);
   int nElem(0);
   for (auto &i_nelem: nElements) {
      const Properties & rProperties = i_nelem.GetProperties();
      if ( rProperties.Has( FRICTION_EFFICIENCY) && rProperties.Has(INTERNAL_FRICTION_ANGLE) ) {
         FrictionAngle += rProperties[FRICTION_EFFICIENCY] * rProperties[INTERNAL_FRICTION_ANGLE];
         nElem += 1;
      }
   }

   if (nElem > 0) {
      FrictionAngle /= double(nElem);
      rFrictionCoefficient = std::tan(3.14159 * FrictionAngle / 180.0);
   }

   return rFrictionCoefficient;
   KRATOS_CATCH("")
}


//************************************************************************************
//************************************************************************************
// i have to copy it because I save the value of the Young modulus of the continuum elements
void EPCPTCondition::CalculateContactFactors(ConditionVariables &rVariables, const ProcessInfo &rCurrentProcessInfo)
{

   KRATOS_TRY

   //Compute the neighbour distance, then a stress-"like" may be computed.
   NodeWeakPtrVectorType &nNodes = GetGeometry()[0].GetValue(NEIGHBOUR_NODES);
   array_1d<double, 3> Contact_Point = GetGeometry()[0].Coordinates();
   array_1d<double, 3> Neighb_Point;

   double distance = 0;
   double counter = 0;

   for (auto &i_nnode : nNodes)
   {
      if (i_nnode.Is(BOUNDARY))
      {

         Neighb_Point = i_nnode.Coordinates();

         distance += norm_2(Contact_Point - Neighb_Point);

         counter++;
      }
   }

   if (counter != 0)
      distance /= counter;

   if (distance == 0)
      distance = 1;

   rVariables.ContributoryFactor = distance;

   //get contact properties and parameters
   double PenaltyParameter = 1;
   if (GetProperties().Has(PENALTY_PARAMETER))
      PenaltyParameter = GetProperties()[PENALTY_PARAMETER];

   const array_1d<double, 3> & rNormal = GetGeometry()[0].GetSolutionStepValue(NORMAL);
   if ( abs(rNormal(0)) > 0.99) {
      if ( GetProperties().Has(PENALTY_PARAMETER_SHAFT) ){
         if ( GetProperties()[PENALTY_PARAMETER_SHAFT] > 0.0) {
            PenaltyParameter = GetProperties()[PENALTY_PARAMETER_SHAFT];
         }
      }
   }

   ElementWeakPtrVectorType &nElements = GetGeometry()[0].GetValue(NEIGHBOUR_ELEMENTS);
   double ElasticModulus = 0;
   if (GetProperties().Has(YOUNG_MODULUS))
      ElasticModulus = GetProperties()[YOUNG_MODULUS];
   else
      ElasticModulus = nElements.front().GetProperties()[YOUNG_MODULUS];

   // the Modified Cam Clay model does not have a constant Young modulus, so something similar to that is computed
   if (ElasticModulus <= 1.0e-5)
   {
      if (mElasticYoungModulus < 0)
      {
         std::vector<double> ModulusVector;
         const ProcessInfo SomeProcessInfo;
         for (auto &i_nelem : nElements)
         {
            i_nelem.CalculateOnIntegrationPoints(YOUNG_MODULUS, ModulusVector, SomeProcessInfo);
            ElasticModulus += ModulusVector[0];
         }
         ElasticModulus /= double(nElements.size());
         mElasticYoungModulus = ElasticModulus;
      }
      else
      {
         ElasticModulus = mElasticYoungModulus;
      }
   }

   rVariables.Penalty.Normal = 1e4 * PenaltyParameter * ElasticModulus;

   double ContributoryArea = this->CalculateSomeSortOfArea();

   //std::cout << " this->Id() " << this->Id() << " , " << rVariables.Penalty.Normal << " area " << ContributoryArea << " , " << Contact_Point[0] << " , " << rVariables.Penalty.Normal * ContributoryArea << std::endl;

   //std::cout << "                nodePosition " << this->GetGeometry()[0].X() << " , " << this->GetGeometry()[0].Y() << std::endl;

   rVariables.Penalty.Normal *= ContributoryArea;

   double PenaltyRatio = 1;
   if (GetProperties().Has(TANGENTIAL_PENALTY_RATIO))
      PenaltyRatio = GetProperties()[TANGENTIAL_PENALTY_RATIO];

   rVariables.Penalty.Tangent = rVariables.Penalty.Normal * PenaltyRatio;

   //std::cout<<" ContactPoint["<<this->Id()<<"]: penalty_n"<<rVariables.Penalty.Normal<<", ElasticModulus: "<<ElasticModulus<<", distance: "<<distance<<std::endl;

   //set contact normal
   const SizeType number_of_nodes = GetGeometry().PointsNumber();

   for (SizeType i = 0; i < number_of_nodes; i++)
   {
      GetGeometry()[i].SetLock();

      array_1d<double, 3> &ContactNormal = GetGeometry()[i].FastGetSolutionStepValue(CONTACT_NORMAL);

      for (SizeType i = 0; i < 3; i++)
         ContactNormal[i] = rVariables.Surface.Normal[i];

      GetGeometry()[i].UnSetLock();
   }

   KRATOS_CATCH("")
}



} // Namespace Kratos
