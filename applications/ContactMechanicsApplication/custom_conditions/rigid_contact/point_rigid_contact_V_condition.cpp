//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:             September 2021 $
//
//

// System includes

// External includes

// Project includes
#include "custom_conditions/rigid_contact/point_rigid_contact_V_condition.hpp"

namespace Kratos
{


//***********************************************************************************
//***********************************************************************************
PointRigidContactVCondition::PointRigidContactVCondition(IndexType NewId, GeometryType::Pointer pGeometry)
    : PointRigidContactCondition(NewId, pGeometry)
{
}

//***********************************************************************************
//***********************************************************************************
PointRigidContactVCondition::PointRigidContactVCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : PointRigidContactCondition(NewId, pGeometry, pProperties)
{
}

//************************************************************************************
//************************************************************************************
PointRigidContactVCondition::PointRigidContactVCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties, SpatialBoundingBox::Pointer pRigidWall)
    : PointRigidContactCondition(NewId, pGeometry, pProperties)
{
}

//************************************************************************************
//************************************************************************************
PointRigidContactVCondition::PointRigidContactVCondition(PointRigidContactVCondition const &rOther)
    : PointRigidContactCondition(rOther)
{
}

//***********************************************************************************
//***********************************************************************************
Condition::Pointer PointRigidContactVCondition::Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const
{
   return Kratos::make_intrusive<PointRigidContactVCondition>(NewId, GetGeometry().Create(ThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************

Condition::Pointer PointRigidContactVCondition::Clone(IndexType NewId, NodesArrayType const &ThisNodes) const
{
   PointRigidContactVCondition ClonedCondition(NewId, GetGeometry().Create(ThisNodes), pGetProperties(), mpRigidWall);
   if (this->mpFrictionLaw!=nullptr)
     ClonedCondition.mpFrictionLaw = this->mpFrictionLaw->Clone();
   return Kratos::make_intrusive<PointRigidContactVCondition>(ClonedCondition);
}

//***********************************************************************************
//***********************************************************************************
PointRigidContactVCondition::~PointRigidContactVCondition()
{
}

//***********************************************************************************
//***********************************************************************************

void PointRigidContactVCondition::GetDofList(DofsVectorType &rConditionDofList,
                                            const ProcessInfo &rCurrentProcessInfo) const
{
   KRATOS_TRY

   rConditionDofList.resize(0);
   const SizeType number_of_nodes = GetGeometry().PointsNumber();
   const SizeType dimension = GetGeometry().WorkingSpaceDimension();

   for (SizeType i = 0; i < number_of_nodes; i++)
   {
      rConditionDofList.push_back(GetGeometry()[i].pGetDof(VELOCITY_X));
      rConditionDofList.push_back(GetGeometry()[i].pGetDof(VELOCITY_Y));
      if (dimension == 3)
         rConditionDofList.push_back(GetGeometry()[i].pGetDof(VELOCITY_Z));
   }

   KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void PointRigidContactVCondition::EquationIdVector(EquationIdVectorType &rResult,
                                                  const ProcessInfo &rCurrentProcessInfo) const
{
   KRATOS_TRY

   const SizeType number_of_nodes = GetGeometry().PointsNumber();
   const SizeType dimension = GetGeometry().WorkingSpaceDimension();
   SizeType condition_size = number_of_nodes * dimension;

   if (rResult.size() != condition_size)
      rResult.resize(condition_size, false);

   for (SizeType i = 0; i < number_of_nodes; i++)
   {
      int index = i * dimension;
      rResult[index] = GetGeometry()[i].GetDof(VELOCITY_X).EquationId();
      rResult[index + 1] = GetGeometry()[i].GetDof(VELOCITY_Y).EquationId();
      if (dimension == 3)
         rResult[index + 2] = GetGeometry()[i].GetDof(VELOCITY_Z).EquationId();
   }

   KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void PointRigidContactVCondition::GetValuesVector(Vector &rValues, int Step) const
{
   const SizeType number_of_nodes = GetGeometry().PointsNumber();
   const SizeType dimension = GetGeometry().WorkingSpaceDimension();
   SizeType condition_size = number_of_nodes * dimension;

   if (rValues.size() != condition_size)
      rValues.resize(condition_size, false);

   for (SizeType i = 0; i < number_of_nodes; i++)
   {
      SizeType index = i * dimension;
      rValues[index] = GetGeometry()[i].FastGetSolutionStepValue(VELOCITY_X, Step);
      rValues[index + 1] = GetGeometry()[i].FastGetSolutionStepValue(VELOCITY_Y, Step);

      if (dimension == 3)
         rValues[index + 2] = GetGeometry()[i].FastGetSolutionStepValue(VELOCITY_Z, Step);
   }
}

//***********************************************************************************
//***********************************************************************************

void PointRigidContactVCondition::GetFirstDerivativesVector(Vector &rValues, int Step) const
{
   const SizeType number_of_nodes = GetGeometry().PointsNumber();
   const SizeType dimension = GetGeometry().WorkingSpaceDimension();
   SizeType condition_size = number_of_nodes * dimension;

   if (rValues.size() != condition_size)
      rValues.resize(condition_size, false);

   for (SizeType i = 0; i < number_of_nodes; i++)
   {
      SizeType index = i * dimension;
      rValues[index] = GetGeometry()[i].FastGetSolutionStepValue(ACCELERATION_X, Step);
      rValues[index + 1] = GetGeometry()[i].FastGetSolutionStepValue(ACCELERATION_Y, Step);

      if (dimension == 3)
         rValues[index + 2] = GetGeometry()[i].FastGetSolutionStepValue(ACCELERATION_Z, Step);
   }
}

//***********************************************************************************
//***********************************************************************************

void PointRigidContactVCondition::GetSecondDerivativesVector(Vector &rValues, int Step) const
{
   const SizeType number_of_nodes = GetGeometry().PointsNumber();
   const SizeType dimension = GetGeometry().WorkingSpaceDimension();
   SizeType condition_size = number_of_nodes * dimension;

   if (rValues.size() != condition_size)
      rValues.resize(condition_size, false);
}



//*********************************COMPUTE KINEMATICS*********************************
//************************************************************************************

void PointRigidContactVCondition::CalculateKinematics(ConditionVariables &rVariables, const ProcessInfo &rCurrentProcessInfo, const double &rPointNumber)
{
  KRATOS_TRY

  SpatialBoundingBox::BoxParametersType BoxParameters(this->GetGeometry()[0], rVariables.Gap.Normal, rVariables.Gap.Tangent, rVariables.Surface.Normal, rVariables.Surface.Tangent, rVariables.RelativeDisplacement);

  if (this->mpRigidWall->IsInside(BoxParameters, rCurrentProcessInfo))
  {
    rVariables.Options.Set(ACTIVE, true);

    //consider positive gap
    rVariables.Gap.Normal = fabs(rVariables.Gap.Normal);

    //get contact properties and parameters
    this->CalculateContactFactors(rVariables, rCurrentProcessInfo);
  }
  else
  {
    rVariables.Options.Set(ACTIVE, false);
  }

  rVariables.DeltaTime = rCurrentProcessInfo[DELTA_TIME];

  PointRigidContactCondition::CalculateKinematics(rVariables, rCurrentProcessInfo, rPointNumber);

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void PointRigidContactVCondition::CalculateContactFactors(ConditionVariables &rVariables, const ProcessInfo &rCurrentProcessInfo)
{

  KRATOS_TRY

  //Compute the neighbour distance, then a stress-"like" may be computed.
  NodeWeakPtrVectorType &nNodes = GetGeometry()[0].GetValue(NEIGHBOUR_NODES);
  array_1d<double, 3> Contact_Point = GetGeometry()[0].Coordinates();
  array_1d<double, 3> Neighb_Point;

  double distance = 0;
  double counter = 0;

  for (auto &i_nnode : nNodes)
  {
    if (i_nnode.Is(BOUNDARY))
    {
      Neighb_Point = i_nnode.Coordinates();
      distance += norm_2(Contact_Point - Neighb_Point);
      ++counter;
    }
  }

  if (counter != 0)
    distance /= counter;

  if (distance == 0)
    distance = 1;

  rVariables.ContributoryFactor = distance;

  const double &contact_area = rCurrentProcessInfo[CONTACT_AREA];
  if (contact_area != 0)
    distance /= contact_area;

  //get contact properties and parameters
  double PenaltyParameter = 1;
  if (GetProperties().Has(PENALTY_PARAMETER))
    PenaltyParameter = GetProperties()[PENALTY_PARAMETER];

  ElementWeakPtrVectorType &nElements = GetGeometry()[0].GetValue(NEIGHBOUR_ELEMENTS);
  double ElasticModulus = 0;
  if (GetProperties().Has(YOUNG_MODULUS))
    ElasticModulus = GetProperties()[YOUNG_MODULUS];
  else
    ElasticModulus = nElements.front().GetProperties()[YOUNG_MODULUS];

  // the Modified Cam Clay model does not have a constant Young modulus, so something similar to that is computed
  if (ElasticModulus <= 1.0e-5)
  {
    std::vector<double> mModulus;
    const ProcessInfo SomeProcessInfo;
    for (auto &i_nelem : nElements)
    {
      i_nelem.CalculateOnIntegrationPoints(YOUNG_MODULUS, mModulus, SomeProcessInfo);
      ElasticModulus += mModulus[0];
    }
    ElasticModulus /= double(nElements.size());
  }

  double factor = 1; //4;
  if (distance < 1.0)
  { //take a number bigger than 1.0 (length units)
    int order = (int)((-1) * std::log10(distance) + 1);
    distance *= factor * pow(10, order);
  }

  if(ElasticModulus <= 1.0e-5)
  {
    if (GetProperties().Has(BULK_MODULUS))
      ElasticModulus = GetProperties()[BULK_MODULUS];
    else
      ElasticModulus = nElements.front().GetProperties()[BULK_MODULUS];
  }

  rVariables.Penalty.Normal = distance * PenaltyParameter * ElasticModulus;

  // to give the order defined by the PenaltyParameter*ElasticModulus
  int penalty_order = (int)((-1) * std::log10(rVariables.Penalty.Normal) + 1);
  penalty_order -= (int)((-1) * std::log10(PenaltyParameter * ElasticModulus) + 1);
  rVariables.Penalty.Normal *= pow(10, penalty_order);

  double PenaltyRatio = 1;
  if (GetProperties().Has(TANGENTIAL_PENALTY_RATIO))
    PenaltyRatio = GetProperties()[TANGENTIAL_PENALTY_RATIO];

  rVariables.Penalty.Tangent = rVariables.Penalty.Normal * PenaltyRatio;

  // std::cout<<" ContactPoint["<<this->Id()<<"]: penalty: "<<rVariables.Penalty.Normal<<", ElasticModulus: "<<ElasticModulus<<", distance: "<<distance<<" contributory: "<<rVariables.ContributoryFactor<<" Gap "<<rVariables.Gap.Normal<<std::endl;

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void PointRigidContactVCondition::CalculateAndAddKuug(MatrixType &rLeftHandSideMatrix,
                                                              ConditionVariables &rVariables,
                                                              double &rIntegrationWeight)

{
  KRATOS_TRY

  const SizeType dimension = GetGeometry().WorkingSpaceDimension();
  const SizeType dofs_size = this->GetNodeDofsSize();

  if (rVariables.Options.Is(ACTIVE) && dofs_size != 0)
  {

    MatrixType Kuug(3,3);
    noalias(Kuug) = rVariables.Penalty.Normal * rIntegrationWeight * outer_prod(rVariables.Surface.Normal, rVariables.Surface.Normal);


    this->CalculateAndAddKuugTangent(Kuug, rVariables, rIntegrationWeight);

    for (SizeType i = 0; i < dimension; i++)
    {
      for (SizeType j = 0; j < dimension; j++)
      {
        rLeftHandSideMatrix(i, j) += Kuug(i, j);
      }
    }
  }
  else
  {
    rLeftHandSideMatrix = ZeroMatrix(dofs_size, dofs_size);
  }

  // if( rVariables.Options.Is(ACTIVE))
  //   std::cout<<" Contact Tangent Matrix ["<<this->Id()<<"]: "<<rLeftHandSideMatrix<<std::endl;

  KRATOS_CATCH("")
}

//************* Tangent Contact Force constitutive matrix      **********************
//***********************************************************************************

void PointRigidContactVCondition::CalculateAndAddKuugTangent(MatrixType &rLeftHandSideMatrix, ConditionVariables &rVariables, double &rIntegrationWeight)
{
  KRATOS_TRY

  const SizeType dimension = GetGeometry().WorkingSpaceDimension();

  double NormalForceModulus = 0;
  NormalForceModulus = this->CalculateNormalForceModulus(NormalForceModulus, rVariables);

  double TangentForceModulus = this->CalculateCoulombsFrictionLaw(rVariables.Gap.Tangent, NormalForceModulus, rVariables);

  if (fabs(TangentForceModulus) >= 1e-25)
  {

    MatrixType Identity(3, 3);
    noalias(Identity) = IdentityMatrix(3);

    if (rVariables.Slip)
    {

      noalias(rLeftHandSideMatrix) += rVariables.FrictionCoefficient * rVariables.Penalty.Normal * rIntegrationWeight * (outer_prod(rVariables.Surface.Tangent, rVariables.Surface.Normal));

      noalias(rLeftHandSideMatrix) += rVariables.FrictionCoefficient * rVariables.Penalty.Normal * rIntegrationWeight * (rVariables.Gap.Normal / rVariables.Gap.Tangent) * (Identity - outer_prod(rVariables.Surface.Normal, rVariables.Surface.Normal));

      //extra term (2D)
      if (dimension == 2)
      {
        noalias(rLeftHandSideMatrix) -= rVariables.FrictionCoefficient * rVariables.Penalty.Normal * rIntegrationWeight * (rVariables.Gap.Normal / rVariables.Gap.Tangent) * (outer_prod(rVariables.Surface.Tangent, VectorType(rVariables.Surface.Tangent - (inner_prod(rVariables.RelativeDisplacement, rVariables.Surface.Normal) * rVariables.Surface.Tangent) - (inner_prod(rVariables.Surface.Normal, rVariables.RelativeDisplacement) * rVariables.Surface.Normal))));
      }
      else
      {
        noalias(rLeftHandSideMatrix) -= rVariables.FrictionCoefficient * rVariables.Penalty.Normal * rIntegrationWeight * (rVariables.Gap.Normal / rVariables.Gap.Tangent) * (outer_prod(rVariables.Surface.Tangent, rVariables.Surface.Tangent));
      }

      //std::cout<<" Slip:Kuug "<<rLeftHandSideMatrix<<std::endl;
    }
    else
    {

      noalias(rLeftHandSideMatrix) += rVariables.Penalty.Tangent * rIntegrationWeight * outer_prod(rVariables.Surface.Tangent, rVariables.Surface.Tangent);

      noalias(rLeftHandSideMatrix) += rVariables.Penalty.Tangent * rIntegrationWeight * (Identity - outer_prod(rVariables.Surface.Normal, rVariables.Surface.Normal));

      //extra term (2D)
      if (dimension == 2)
        noalias(rLeftHandSideMatrix) -= rVariables.Penalty.Tangent * rIntegrationWeight * (outer_prod(rVariables.Surface.Tangent, VectorType(rVariables.Surface.Tangent - (inner_prod(rVariables.RelativeDisplacement, rVariables.Surface.Normal) * rVariables.Surface.Tangent) - (inner_prod(rVariables.Surface.Normal, rVariables.RelativeDisplacement) * rVariables.Surface.Normal))));

      //std::cout<<" Stick:Kuug "<<rLeftHandSideMatrix<<std::endl;
    }
  }

  KRATOS_CATCH("")
}

void PointRigidContactVCondition::CalculateAndAddContactForces(VectorType &rRightHandSideVector,
                                                                       ConditionVariables &rVariables,
                                                                       double &rIntegrationWeight)
{
  KRATOS_TRY

  const SizeType dofs_size = this->GetNodeDofsSize();

  if (rVariables.Options.Is(ACTIVE) && dofs_size != 0)
  {

    this->CalculateAndAddNormalContactForce(rRightHandSideVector, rVariables, rIntegrationWeight);
    this->CalculateAndAddTangentContactForce(rRightHandSideVector, rVariables, rIntegrationWeight);

    // if( rVariables.Options.Is(ACTIVE))
    //   std::cout<<" Contact Force Vector ["<<this->Id()<<"] ("<<GetGeometry()[0].Id()<<"): "<<rRightHandSideVector<<std::endl;

  }
  else
  {
    noalias(rRightHandSideVector) = ZeroVector(dofs_size);
  }

  KRATOS_CATCH("")
}

//**************************** Calculate Normal Contact Force ***********************
//***********************************************************************************

void PointRigidContactVCondition::CalculateAndAddNormalContactForce(VectorType &rRightHandSideVector,
                                                                            ConditionVariables &rVariables,
                                                                            double &rIntegrationWeight)
{

  const SizeType dimension = GetGeometry().WorkingSpaceDimension();

  double NormalForceModulus = 0;
  NormalForceModulus = this->CalculateNormalForceModulus(NormalForceModulus, rVariables);

  NormalForceModulus *= rIntegrationWeight;

  for (SizeType j = 0; j < dimension; j++)
  {
    rRightHandSideVector[j] = NormalForceModulus * rVariables.Surface.Normal[j];
  }

  GetGeometry()[0].SetLock();
  array_1d<double, 3> &ContactForce = GetGeometry()[0].FastGetSolutionStepValue(CONTACT_FORCE);
  for (SizeType j = 0; j < dimension; j++)
    ContactForce[j] = rRightHandSideVector[j];
  GetGeometry()[0].UnSetLock();

  rVariables.ContactStressVector = MathUtils<double>::StressTensorToVector(NormalForceModulus * outer_prod(rVariables.Surface.Normal, rVariables.Surface.Normal), rVariables.ContactStressVector.size());

}

//**************************** Calculate Tangent Contact Force **********************
//***********************************************************************************

void PointRigidContactVCondition::CalculateAndAddTangentContactForce(VectorType &rRightHandSideVector,
                                                                             ConditionVariables &rVariables,
                                                                             double &rIntegrationWeight)
{
  KRATOS_TRY

  const SizeType &dimension = GetGeometry().WorkingSpaceDimension();

  double NormalForceModulus = 0;
  NormalForceModulus = this->CalculateNormalForceModulus(NormalForceModulus, rVariables);

  double TangentForceModulus = this->CalculateCoulombsFrictionLaw(rVariables.Gap.Tangent, NormalForceModulus, rVariables);

  TangentForceModulus *= rIntegrationWeight;

  for (SizeType i = 0; i < dimension; ++i)
    rRightHandSideVector[i] += TangentForceModulus * rVariables.Surface.Tangent[i];

  rVariables.ContactStressVector += MathUtils<double>::StressTensorToVector(TangentForceModulus * (outer_prod(rVariables.Surface.Normal, rVariables.Surface.Tangent) + outer_prod(rVariables.Surface.Tangent, rVariables.Surface.Normal)), rVariables.ContactStressVector.size());


  GetGeometry()[0].SetLock();
  array_1d<double, 3> &ContactForce = GetGeometry()[0].FastGetSolutionStepValue(CONTACT_FORCE);
  for (SizeType j = 0; j < dimension; j++)
    ContactForce[j] += TangentForceModulus * rVariables.Surface.Tangent[j];
  GetGeometry()[0].UnSetLock();


  KRATOS_CATCH("")
}

//**************************** Calculate Normal Force Modulus ***********************
//***********************************************************************************

double &PointRigidContactVCondition::CalculateNormalForceModulus(double &rNormalForceModulus, ConditionVariables &rVariables)
{
  KRATOS_TRY

  rNormalForceModulus = (rVariables.Penalty.Normal * rVariables.Gap.Normal);

  return rNormalForceModulus;

  KRATOS_CATCH("")
}


//************************************************************************************
//************************************************************************************

void PointRigidContactVCondition::CalculateAndAddLHS(LocalSystemComponents &rLocalSystem, ConditionVariables &rVariables, double &rIntegrationWeight)
{
    MatrixType &rLeftHandSideMatrix = rLocalSystem.GetLeftHandSideMatrix();
    // operation performed: add Kg to the rLefsHandSideMatrix
    this->CalculateAndAddKuug(rLeftHandSideMatrix, rVariables, rIntegrationWeight);

    rLeftHandSideMatrix *= rVariables.GetProcessInfo()[DELTA_TIME];
}

//***********************************************************************************
//***********************************************************************************

int PointRigidContactVCondition::Check(const ProcessInfo &rCurrentProcessInfo) const
{
   return 0;
}

} // Namespace Kratos.
