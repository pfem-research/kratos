//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                August 2016 $
//
//

// System includes

// External includes

// Project includes
#include "custom_conditions/rigid_contact/point_rigid_contact_penalty_2D_condition.hpp"

namespace Kratos
{
//************************************************************************************
//************************************************************************************
PointRigidContactPenalty2DCondition::PointRigidContactPenalty2DCondition(IndexType NewId, GeometryType::Pointer pGeometry)
    : PointRigidContactPenalty3DCondition(NewId, pGeometry)
{
}

//************************************************************************************
//************************************************************************************
PointRigidContactPenalty2DCondition::PointRigidContactPenalty2DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : PointRigidContactPenalty3DCondition(NewId, pGeometry, pProperties)
{
}

//************************************************************************************
//************************************************************************************
PointRigidContactPenalty2DCondition::PointRigidContactPenalty2DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties, SpatialBoundingBox::Pointer pRigidWall)
    : PointRigidContactPenalty3DCondition(NewId, pGeometry, pProperties, pRigidWall)
{
}

//************************************************************************************
//************************************************************************************
PointRigidContactPenalty2DCondition::PointRigidContactPenalty2DCondition(PointRigidContactPenalty2DCondition const &rOther)
    : PointRigidContactPenalty3DCondition(rOther)
{
}

//************************************************************************************
//************************************************************************************

Condition::Pointer PointRigidContactPenalty2DCondition::Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const
{
  return Kratos::make_intrusive<PointRigidContactPenalty2DCondition>(NewId, GetGeometry().Create(ThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************

Condition::Pointer PointRigidContactPenalty2DCondition::Clone(IndexType NewId, NodesArrayType const &ThisNodes) const
{
  PointRigidContactPenalty2DCondition ClonedCondition(NewId, GetGeometry().Create(ThisNodes), pGetProperties(), mpRigidWall);
  if (this->mpFrictionLaw!=nullptr)
    ClonedCondition.mpFrictionLaw = this->mpFrictionLaw->Clone();
  return Kratos::make_intrusive<PointRigidContactPenalty2DCondition>(ClonedCondition);
}

//************************************************************************************
//************************************************************************************

PointRigidContactPenalty2DCondition::~PointRigidContactPenalty2DCondition()
{
}

} // Namespace Kratos
