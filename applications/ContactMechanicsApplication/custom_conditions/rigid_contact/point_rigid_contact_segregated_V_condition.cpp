//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:             September 2021 $
//
//

// System includes

// External includes

// Project includes
#include "custom_conditions/rigid_contact/point_rigid_contact_segregated_V_condition.hpp"

#include "contact_mechanics_application_variables.h"

namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

PointRigidContactSegregatedVCondition::PointRigidContactSegregatedVCondition(IndexType NewId, GeometryType::Pointer pGeometry)
    : PointRigidContactVCondition(NewId, pGeometry)
{
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

PointRigidContactSegregatedVCondition::PointRigidContactSegregatedVCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : PointRigidContactVCondition(NewId, pGeometry, pProperties)
{
  mStepVariable = VELOCITY_STEP;
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

PointRigidContactSegregatedVCondition::PointRigidContactSegregatedVCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties, SpatialBoundingBox::Pointer pRigidWall)
    : PointRigidContactVCondition(NewId, pGeometry, pProperties)
{
  mStepVariable = VELOCITY_STEP;
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

PointRigidContactSegregatedVCondition::PointRigidContactSegregatedVCondition(PointRigidContactSegregatedVCondition const &rOther)
    : PointRigidContactVCondition(rOther), mStepVariable(rOther.mStepVariable)
{
}

//*********************************CREATE*********************************************
//************************************************************************************

Condition::Pointer PointRigidContactSegregatedVCondition::Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const
{
  return Kratos::make_intrusive<PointRigidContactSegregatedVCondition>(NewId, GetGeometry().Create(ThisNodes), pProperties);
}

//*********************************CLONE**********************************************
//************************************************************************************

Condition::Pointer PointRigidContactSegregatedVCondition::Clone(IndexType NewId, NodesArrayType const &ThisNodes) const
{

  PointRigidContactSegregatedVCondition ClonedCondition(NewId, GetGeometry().Create(ThisNodes), pGetProperties());

  ClonedCondition.SetData(this->GetData());
  ClonedCondition.SetFlags(this->GetFlags());
  ClonedCondition.mStepVariable = mStepVariable;
  if (this->mpFrictionLaw!=nullptr)
    ClonedCondition.mpFrictionLaw = this->mpFrictionLaw->Clone();

  return Kratos::make_intrusive<PointRigidContactSegregatedVCondition>(ClonedCondition);
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

PointRigidContactSegregatedVCondition::~PointRigidContactSegregatedVCondition()
{
}

//************************************************************************************
//************************************************************************************

void PointRigidContactSegregatedVCondition::GetDofList(DofsVectorType &rConditionDofList, const ProcessInfo &rCurrentProcessInfo) const
{
  switch (StepType(rCurrentProcessInfo[SOLVER_STEP]))
  {
  case VELOCITY_STEP:
  {
    PointRigidContactVCondition::GetDofList(rConditionDofList, rCurrentProcessInfo);
    break;
  }
  case PRESSURE_STEP:
  {
    rConditionDofList.resize(0);
    break;
  }
  default:
    KRATOS_ERROR << "Unexpected value for SOLVER_STEP index: " << rCurrentProcessInfo[SOLVER_STEP] << std::endl;
  }
}

//************************************************************************************
//************************************************************************************

void PointRigidContactSegregatedVCondition::EquationIdVector(EquationIdVectorType &rResult, const ProcessInfo &rCurrentProcessInfo) const
{

  switch (StepType(rCurrentProcessInfo[SOLVER_STEP]))
  {
  case VELOCITY_STEP:
  {
    PointRigidContactVCondition::EquationIdVector(rResult, rCurrentProcessInfo);
    break;
  }
  case PRESSURE_STEP:
  {
    const SizeType dofs_size = this->GetDofsSize();

    if (rResult.size() != dofs_size)
      rResult.resize(dofs_size, false);

    break;
  }
  default:
    KRATOS_ERROR << "Unexpected value for SOLVER_STEP index: " << rCurrentProcessInfo[SOLVER_STEP] << std::endl;
  }
}

//************************************************************************************
//************************************************************************************

void PointRigidContactSegregatedVCondition::GetValuesVector(Vector &rValues, int Step) const
{
  KRATOS_TRY

  switch (mStepVariable)
  {
  case VELOCITY_STEP:
  {
    PointRigidContactVCondition::GetValuesVector(rValues, Step);
    break;
  }
  case PRESSURE_STEP:
  {
    SizeType dofs_size = this->GetDofsSize();
    if (rValues.size() != dofs_size)
      rValues.resize(dofs_size, false);
    break;
  }
  default:
    KRATOS_ERROR << "Unexpected value for SOLVER_STEP index: " << mStepVariable << std::endl;
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void PointRigidContactSegregatedVCondition::GetFirstDerivativesVector(Vector &rValues, int Step) const
{
  KRATOS_TRY

  switch (mStepVariable)
  {
  case VELOCITY_STEP:
  {
    PointRigidContactVCondition::GetFirstDerivativesVector(rValues, Step);
    break;
  }
  case PRESSURE_STEP:
  {
    SizeType dofs_size = this->GetDofsSize();
    if (rValues.size() != dofs_size)
      rValues.resize(dofs_size, false);
    break;
  }
  default:
    KRATOS_ERROR << "Unexpected value for SOLVER_STEP index: " << mStepVariable << std::endl;
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void PointRigidContactSegregatedVCondition::GetSecondDerivativesVector(Vector &rValues, int Step) const
{
  KRATOS_TRY

  switch (mStepVariable)
  {
  case VELOCITY_STEP:
  {
    PointRigidContactVCondition::GetSecondDerivativesVector(rValues, Step);
    break;
  }
  case PRESSURE_STEP:
  {
    SizeType dofs_size = this->GetDofsSize();
    if (rValues.size() != dofs_size)
      rValues.resize(dofs_size, false);
    break;
  }
  default:
    KRATOS_ERROR << "Unexpected value for SOLVER_STEP index: " << mStepVariable << std::endl;
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void PointRigidContactSegregatedVCondition::Initialize(const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  PointRigidContactVCondition::Initialize(rCurrentProcessInfo);

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void PointRigidContactSegregatedVCondition::InitializeSolutionStep(const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  this->SetProcessInformation(rCurrentProcessInfo);

  switch (mStepVariable)
  {
  case VELOCITY_STEP:
  {
    PointRigidContactVCondition::InitializeSolutionStep(rCurrentProcessInfo);
    break;
  }
  case PRESSURE_STEP:
  {
    break;
  }
  default:
    KRATOS_ERROR << "Unexpected value for SOLVER_STEP index: " << rCurrentProcessInfo[SOLVER_STEP] << std::endl;
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void PointRigidContactSegregatedVCondition::InitializeNonLinearIteration(const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  this->SetProcessInformation(rCurrentProcessInfo);

  PointRigidContactVCondition::InitializeNonLinearIteration(rCurrentProcessInfo);

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void PointRigidContactSegregatedVCondition::FinalizeNonLinearIteration(const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  this->SetProcessInformation(rCurrentProcessInfo);

  switch (mStepVariable)
  {
  case VELOCITY_STEP:
  {
    PointRigidContactVCondition::FinalizeNonLinearIteration(rCurrentProcessInfo);
    break;
  }
  case PRESSURE_STEP:
  {
    break;
  }
  default:
    KRATOS_ERROR << "Unexpected value for SOLVER_STEP index: " << rCurrentProcessInfo[SOLVER_STEP] << std::endl;
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void PointRigidContactSegregatedVCondition::FinalizeSolutionStep(const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  this->SetProcessInformation(rCurrentProcessInfo);

  switch (mStepVariable)
  {
  case VELOCITY_STEP:
  {
    PointRigidContactVCondition::FinalizeSolutionStep(rCurrentProcessInfo);
    break;
  }
  case PRESSURE_STEP:
  {
    //set as VELOCITY STEP for gauss point calculations:
    mStepVariable = VELOCITY_STEP;
    break;
  }
  default:
    KRATOS_ERROR << "Unexpected value for SOLVER_STEP index: " << rCurrentProcessInfo[SOLVER_STEP] << std::endl;
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void PointRigidContactSegregatedVCondition::CalculateRightHandSide(VectorType &rRightHandSideVector,
                                                                    const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  //process information
  this->SetProcessInformation(rCurrentProcessInfo);

  PointRigidContactVCondition::CalculateRightHandSide(rRightHandSideVector, rCurrentProcessInfo);

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void PointRigidContactSegregatedVCondition::CalculateLeftHandSide(MatrixType &rLeftHandSideMatrix,
                                                                   const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  //process information
  this->SetProcessInformation(rCurrentProcessInfo);

  PointRigidContactVCondition::CalculateLeftHandSide(rLeftHandSideMatrix, rCurrentProcessInfo);

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void PointRigidContactSegregatedVCondition::CalculateLocalSystem(MatrixType &rLeftHandSideMatrix,
                                                                  VectorType &rRightHandSideVector,
                                                                  const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  //process information
  this->SetProcessInformation(rCurrentProcessInfo);

  PointRigidContactVCondition::CalculateLocalSystem(rLeftHandSideMatrix, rRightHandSideVector, rCurrentProcessInfo);

  //KRATOS_INFO("")<<mStepVariable<<" LHS:"<<rLeftHandSideMatrix<<" RHS:"<<rRightHandSideVector<<std::endl;

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void PointRigidContactSegregatedVCondition::CalculateSecondDerivativesContributions(MatrixType &rLeftHandSideMatrix,
                                                                                     VectorType &rRightHandSideVector,
                                                                                     const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  //process information
  this->SetProcessInformation(rCurrentProcessInfo);

  switch (mStepVariable)
  {
  case VELOCITY_STEP:
  {
    PointRigidContactVCondition::CalculateSecondDerivativesContributions(rLeftHandSideMatrix, rRightHandSideVector, rCurrentProcessInfo);
    break;
  }
  case PRESSURE_STEP:
  {
    break;
  }
  default:
    KRATOS_ERROR << "Unexpected value for SOLVER_STEP index: " << rCurrentProcessInfo[SOLVER_STEP] << std::endl;
  }

  //KRATOS_INFO("")<<mStepVariable<<" 2LHS:"<<rLeftHandSideMatrix<<" 2RHS:"<<rRightHandSideVector<<std::endl;

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void PointRigidContactSegregatedVCondition::CalculateSecondDerivativesLHS(MatrixType &rLeftHandSideMatrix,
                                                                           const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  //process information
  this->SetProcessInformation(rCurrentProcessInfo);

  switch (mStepVariable)
  {
  case VELOCITY_STEP:
  {
    PointRigidContactVCondition::CalculateSecondDerivativesLHS(rLeftHandSideMatrix, rCurrentProcessInfo);
    break;
  }
  case PRESSURE_STEP:
  {
    break;
  }
  default:
    KRATOS_ERROR << "Unexpected value for SOLVER_STEP index: " << rCurrentProcessInfo[SOLVER_STEP] << std::endl;
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void PointRigidContactSegregatedVCondition::CalculateSecondDerivativesRHS(VectorType &rRightHandSideVector,
                                                                           const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  //process information
  this->SetProcessInformation(rCurrentProcessInfo);

  switch (mStepVariable)
  {
  case VELOCITY_STEP:
  {
    PointRigidContactVCondition::CalculateSecondDerivativesRHS(rRightHandSideVector, rCurrentProcessInfo);
    break;
  }
  case PRESSURE_STEP:
  {
    break;
  }
  default:
    KRATOS_ERROR << "Unexpected value for SOLVER_STEP index: " << rCurrentProcessInfo[SOLVER_STEP] << std::endl;
  }

  KRATOS_CATCH("")
}


//************************************************************************************
//************************************************************************************

PointRigidContactSegregatedVCondition::SizeType PointRigidContactSegregatedVCondition::GetDofsSize() const
{
  KRATOS_TRY

  SizeType size = 0;
  switch (mStepVariable)
  {
  case VELOCITY_STEP:
  {
    size = PointRigidContactVCondition::GetDofsSize();
    break;
  }
  case PRESSURE_STEP:
  {
    size = 0;
    break;
  }
  default:
    KRATOS_ERROR << "Unexpected value for SOLVER_STEP index: " << mStepVariable << std::endl;
  }
  return size;

  KRATOS_CATCH("")
}


//************************************************************************************
//************************************************************************************

PointRigidContactSegregatedVCondition::SizeType PointRigidContactSegregatedVCondition::GetNodeDofsSize() const
{
  KRATOS_TRY

  SizeType size = 0;
  switch (mStepVariable)
  {
  case VELOCITY_STEP:
  {
    size = PointRigidContactVCondition::GetNodeDofsSize();
    break;
  }
  case PRESSURE_STEP:
  {
    size = 0;
    break;
  }
  default:
    KRATOS_ERROR << "Unexpected value for SOLVER_STEP index: " << mStepVariable << std::endl;
  }
  return size;

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void PointRigidContactSegregatedVCondition::SetProcessInformation(const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  mStepVariable = StepType(rCurrentProcessInfo[SOLVER_STEP]);

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

int PointRigidContactSegregatedVCondition::Check(const ProcessInfo &rCurrentProcessInfo) const
{
  KRATOS_TRY

  return 0;

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void PointRigidContactSegregatedVCondition::save(Serializer &rSerializer) const
{
  KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, PointRigidContactVCondition)
}

void PointRigidContactSegregatedVCondition::load(Serializer &rSerializer)
{
  KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, PointRigidContactVCondition)
}

} // Namespace Kratos
