//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                August 2016 $
//
//

// System includes

// External includes

// Project includes
#include "custom_conditions/rigid_contact/point_rigid_contact_condition.hpp"

namespace Kratos
{

/**
    * Flags related to the condition computation
    */
KRATOS_CREATE_LOCAL_FLAG(PointRigidContactCondition, COMPUTE_RHS_VECTOR, 0);
KRATOS_CREATE_LOCAL_FLAG(PointRigidContactCondition, COMPUTE_LHS_MATRIX, 1);
KRATOS_CREATE_LOCAL_FLAG(PointRigidContactCondition, FINALIZED_STEP, 2);

//***********************************************************************************
//***********************************************************************************
PointRigidContactCondition::PointRigidContactCondition(IndexType NewId, GeometryType::Pointer pGeometry)
    : Condition(NewId, pGeometry)
{
   //DO NOT ADD DOFS HERE!!!
}

//***********************************************************************************
//***********************************************************************************
PointRigidContactCondition::PointRigidContactCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : Condition(NewId, pGeometry, pProperties)
{
   mThisIntegrationMethod = GetGeometry().GetDefaultIntegrationMethod();
   InitializeSpatialBoundingBox();
}

//************************************************************************************
//************************************************************************************
PointRigidContactCondition::PointRigidContactCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties, SpatialBoundingBox::Pointer pRigidWall)
    : Condition(NewId, pGeometry, pProperties)
{

   mThisIntegrationMethod = GetGeometry().GetDefaultIntegrationMethod();
   mpRigidWall = pRigidWall;
}

//************************************************************************************
//************************************************************************************
PointRigidContactCondition::PointRigidContactCondition(PointRigidContactCondition const &rOther)
    : Condition(rOther), mThisIntegrationMethod(rOther.mThisIntegrationMethod), mpRigidWall(rOther.mpRigidWall), mpFrictionLaw(rOther.mpFrictionLaw), mContactStressVector(rOther.mContactStressVector)
{
}

//***********************************************************************************
//***********************************************************************************
Condition::Pointer PointRigidContactCondition::Create(
    IndexType NewId,
    NodesArrayType const &ThisNodes,
    PropertiesType::Pointer pProperties) const
{
   return Kratos::make_intrusive<PointRigidContactCondition>(NewId, GetGeometry().Create(ThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************

Condition::Pointer PointRigidContactCondition::Clone(IndexType NewId, NodesArrayType const &ThisNodes) const
{
  PointRigidContactCondition ClonedCondition(NewId, GetGeometry().Create(ThisNodes), pGetProperties(), mpRigidWall);
  ClonedCondition.SetData(this->GetData());
  ClonedCondition.SetFlags(this->GetFlags());
  if (this->mpFrictionLaw!=nullptr)
    ClonedCondition.mpFrictionLaw = this->mpFrictionLaw->Clone();
  return Kratos::make_intrusive<PointRigidContactCondition>(ClonedCondition);
}

//***********************************************************************************
//***********************************************************************************
PointRigidContactCondition::~PointRigidContactCondition()
{
}

//************* GETTING METHODS

//***********************************************************************************
//***********************************************************************************

void PointRigidContactCondition::GetDofList(DofsVectorType &rConditionDofList,
                                            const ProcessInfo &rCurrentProcessInfo) const
{
   KRATOS_TRY

   rConditionDofList.resize(0);
   const SizeType number_of_nodes = GetGeometry().PointsNumber();
   const SizeType dimension = GetGeometry().WorkingSpaceDimension();

   for (SizeType i = 0; i < number_of_nodes; i++)
   {
      rConditionDofList.push_back(GetGeometry()[i].pGetDof(DISPLACEMENT_X));
      rConditionDofList.push_back(GetGeometry()[i].pGetDof(DISPLACEMENT_Y));
      if (dimension == 3)
         rConditionDofList.push_back(GetGeometry()[i].pGetDof(DISPLACEMENT_Z));
   }

   KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void PointRigidContactCondition::EquationIdVector(EquationIdVectorType &rResult,
                                                  const ProcessInfo &rCurrentProcessInfo) const
{
   KRATOS_TRY

   const SizeType number_of_nodes = GetGeometry().PointsNumber();
   const SizeType dimension = GetGeometry().WorkingSpaceDimension();
   SizeType condition_size = number_of_nodes * dimension;

   if (rResult.size() != condition_size)
      rResult.resize(condition_size, false);

   for (SizeType i = 0; i < number_of_nodes; i++)
   {
      int index = i * dimension;
      rResult[index] = GetGeometry()[i].GetDof(DISPLACEMENT_X).EquationId();
      rResult[index + 1] = GetGeometry()[i].GetDof(DISPLACEMENT_Y).EquationId();
      if (dimension == 3)
         rResult[index + 2] = GetGeometry()[i].GetDof(DISPLACEMENT_Z).EquationId();
   }

   KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void PointRigidContactCondition::GetValuesVector(Vector &rValues, int Step) const
{
   const SizeType number_of_nodes = GetGeometry().PointsNumber();
   const SizeType dimension = GetGeometry().WorkingSpaceDimension();
   SizeType condition_size = number_of_nodes * dimension;

   if (rValues.size() != condition_size)
      rValues.resize(condition_size, false);

   for (SizeType i = 0; i < number_of_nodes; i++)
   {
      SizeType index = i * dimension;
      rValues[index] = GetGeometry()[i].FastGetSolutionStepValue(DISPLACEMENT_X, Step);
      rValues[index + 1] = GetGeometry()[i].FastGetSolutionStepValue(DISPLACEMENT_Y, Step);

      if (dimension == 3)
         rValues[index + 2] = GetGeometry()[i].FastGetSolutionStepValue(DISPLACEMENT_Z, Step);
   }
}

//***********************************************************************************
//***********************************************************************************

void PointRigidContactCondition::GetFirstDerivativesVector(Vector &rValues, int Step) const
{
   const SizeType number_of_nodes = GetGeometry().PointsNumber();
   const SizeType dimension = GetGeometry().WorkingSpaceDimension();
   SizeType condition_size = number_of_nodes * dimension;

   if (rValues.size() != condition_size)
      rValues.resize(condition_size, false);

   for (SizeType i = 0; i < number_of_nodes; i++)
   {
      SizeType index = i * dimension;
      rValues[index] = GetGeometry()[i].FastGetSolutionStepValue(VELOCITY_X, Step);
      rValues[index + 1] = GetGeometry()[i].FastGetSolutionStepValue(VELOCITY_Y, Step);

      if (dimension == 3)
         rValues[index + 2] = GetGeometry()[i].FastGetSolutionStepValue(VELOCITY_Z, Step);
   }
}

//***********************************************************************************
//***********************************************************************************

void PointRigidContactCondition::GetSecondDerivativesVector(Vector &rValues, int Step) const
{
   const SizeType number_of_nodes = GetGeometry().PointsNumber();
   const SizeType dimension = GetGeometry().WorkingSpaceDimension();
   SizeType condition_size = number_of_nodes * dimension;

   if (rValues.size() != condition_size)
      rValues.resize(condition_size, false);

   for (SizeType i = 0; i < number_of_nodes; i++)
   {
      SizeType index = i * dimension;
      rValues[index] = GetGeometry()[i].FastGetSolutionStepValue(ACCELERATION_X, Step);
      rValues[index + 1] = GetGeometry()[i].FastGetSolutionStepValue(ACCELERATION_Y, Step);

      if (dimension == 3)
         rValues[index + 2] = GetGeometry()[i].FastGetSolutionStepValue(ACCELERATION_Z, Step);
   }
}

//************************************************************************************
//************************************************************************************

void PointRigidContactCondition::InitializeSpatialBoundingBox()
{
  KRATOS_TRY

  if (GetProperties().Has(BOUNDING_BOX) && GetProperties()[BOUNDING_BOX] != NULL)
  {
    mpRigidWall = GetProperties()[BOUNDING_BOX];
  }
  else
  {
    KRATOS_ERROR << " a bounding box must be defined in properties for a rigid contact condition " << std::endl;
  }

  KRATOS_CATCH("")
}


//************************************************************************************
//************************************************************************************
void PointRigidContactCondition::ClearNodalForces()
{
   KRATOS_TRY

   const SizeType number_of_nodes = GetGeometry().PointsNumber();

   for (SizeType i = 0; i < number_of_nodes; i++)
   {
      GetGeometry()[i].SetLock();
      array_1d<double, 3> &ContactForce = GetGeometry()[i].FastGetSolutionStepValue(CONTACT_FORCE);
      ContactForce.clear();
      array_1d<double, 3> &ContactNormal = GetGeometry()[i].FastGetSolutionStepValue(CONTACT_NORMAL);
      ContactNormal.clear();
      GetGeometry()[i].UnSetLock();
   }

   KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void PointRigidContactCondition::AddExplicitContribution(const VectorType &rRHSVector,
                                                         const Variable<VectorType> &rRHSVariable,
                                                         const Variable<array_1d<double, 3>> &rDestinationVariable,
                                                         const ProcessInfo &rCurrentProcessInfo)
{
   KRATOS_TRY

   const SizeType number_of_nodes = GetGeometry().PointsNumber();
   const SizeType dimension = GetGeometry().WorkingSpaceDimension();

   if (rRHSVariable == CONTACT_FORCES_VECTOR && rDestinationVariable == CONTACT_FORCE)
   {

      for (SizeType i = 0; i < number_of_nodes; i++)
      {
         int index = i * this->GetNodeDofsSize();

         GetGeometry()[i].SetLock();

         array_1d<double, 3> &ContactForce = GetGeometry()[i].FastGetSolutionStepValue(CONTACT_FORCE);
         for (SizeType j = 0; j < dimension; j++)
         {
            ContactForce[j] += rRHSVector[index + j];
         }

         GetGeometry()[i].UnSetLock();
      }
   }

   if (rRHSVariable == RESIDUAL_VECTOR && rDestinationVariable == FORCE_RESIDUAL)
   {

      for (SizeType i = 0; i < number_of_nodes; i++)
      {
         int index =  i * this->GetNodeDofsSize();

         GetGeometry()[i].SetLock();

         array_1d<double, 3> &ForceResidual = GetGeometry()[i].FastGetSolutionStepValue(FORCE_RESIDUAL);
         for (SizeType j = 0; j < dimension; j++)
         {
            ForceResidual[j] += rRHSVector[index + j];
         }

         GetGeometry()[i].UnSetLock();
      }
   }

   KRATOS_CATCH("")
}

//************* STARTING - ENDING  METHODS
//***********************************************************************************
//***********************************************************************************

void PointRigidContactCondition::Initialize(const ProcessInfo &rCurrentProcessInfo)
{
   KRATOS_TRY

   const SizeType dimension = GetGeometry().WorkingSpaceDimension();
   const SizeType voigt_size = dimension * (dimension + 1) * 0.5;

   mContactStressVector.resize(voigt_size);
   noalias(mContactStressVector) = ZeroVector(voigt_size);

   mpFrictionLaw = GetProperties()[FRICTION_LAW]->Clone();
   //mpFrictionLaw = Kratos::make_intrusive<CoulombAdhesionFrictionLaw>();
   //mpFrictionLaw = Kratos::make_intrusive<HardeningCoulombFrictionLaw>();

   KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void PointRigidContactCondition::InitializeSolutionStep(const ProcessInfo &rCurrentProcessInfo)
{
   KRATOS_TRY

   const SizeType dimension = GetGeometry().WorkingSpaceDimension();
   const SizeType voigt_size = dimension * (dimension + 1) * 0.5;

   if (mContactStressVector.size() != voigt_size)
   {
      mContactStressVector.resize(voigt_size);
      noalias(mContactStressVector) = ZeroVector(voigt_size);
   }

   KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************
void PointRigidContactCondition::InitializeNonLinearIteration(const ProcessInfo &rCurrentProcessInfo)
{
   KRATOS_TRY

   ClearNodalForces();

   KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void PointRigidContactCondition::FinalizeNonLinearIteration(const ProcessInfo &rCurrentProcessInfo)
{
}

//************************************************************************************
//************************************************************************************

void PointRigidContactCondition::FinalizeSolutionStep(const ProcessInfo &rCurrentProcessInfo)
{
}

//************************************************************************************
//************************************************************************************

PointRigidContactCondition::SizeType PointRigidContactCondition::GetNodeDofsSize() const
{
  return GetGeometry().WorkingSpaceDimension(); //usual size for displacement based elements
}

//************************************************************************************
//************************************************************************************

PointRigidContactCondition::SizeType PointRigidContactCondition::GetDofsSize() const
{
  return (GetGeometry().PointsNumber() * this->GetNodeDofsSize()); //usual size for displacement based elements
}

//***********************************************************************************
//***********************************************************************************

void PointRigidContactCondition::InitializeSystemMatrices(MatrixType &rLeftHandSideMatrix,
                                                          VectorType &rRightHandSideVector,
                                                          Flags &rCalculationFlags)

{
   //resizing as needed the LHS
   const SizeType MatSize = this->GetDofsSize();

   if (rCalculationFlags.Is(PointRigidContactCondition::COMPUTE_LHS_MATRIX)) //calculation of the matrix is required
   {
      if (rLeftHandSideMatrix.size1() != MatSize)
         rLeftHandSideMatrix.resize(MatSize, MatSize, false);

      noalias(rLeftHandSideMatrix) = ZeroMatrix(MatSize, MatSize); //resetting LHS
   }

   //resizing as needed the RHS
   if (rCalculationFlags.Is(PointRigidContactCondition::COMPUTE_RHS_VECTOR)) //calculation of the matrix is required
   {
      if (rRightHandSideVector.size() != MatSize)
         rRightHandSideVector.resize(MatSize, false);

      noalias(rRightHandSideVector) = ZeroVector(MatSize); //resetting RHS
   }
}

//************************************************************************************
//************************************************************************************

void PointRigidContactCondition::InitializeConditionVariables(ConditionVariables &rVariables, const ProcessInfo &rCurrentProcessInfo)
{
   KRATOS_TRY

   const SizeType dimension = GetGeometry().WorkingSpaceDimension();
   const SizeType voigt_size = dimension * (dimension + 1) * 0.5;

   rVariables.Initialize();

   rVariables.ContactStressVector.resize(voigt_size);
   noalias(rVariables.ContactStressVector) = ZeroVector(voigt_size);

   //set process info
   rVariables.SetProcessInfo(rCurrentProcessInfo);

   KRATOS_CATCH("")
}

//*********************************COMPUTE KINEMATICS*********************************
//************************************************************************************

void PointRigidContactCondition::CalculateKinematics(ConditionVariables &rVariables, const ProcessInfo &rCurrentProcessInfo, const double &rPointNumber)
{
  KRATOS_TRY

  //set contact normal
  const SizeType number_of_nodes = GetGeometry().PointsNumber();

  for (SizeType i = 0; i < number_of_nodes; i++)
  {
    GetGeometry()[i].SetLock();
    GetGeometry()[i].FastGetSolutionStepValue(CONTACT_NORMAL) = rVariables.Surface.Normal;
    GetGeometry()[i].UnSetLock();
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void PointRigidContactCondition::CalculateConditionSystem(LocalSystemComponents &rLocalSystem,
                                                          const ProcessInfo &rCurrentProcessInfo)
{
   KRATOS_TRY

   //create and initialize condition variables:
   ConditionVariables Variables;
   this->InitializeConditionVariables(Variables, rCurrentProcessInfo);

   //reading integration points
   for (SizeType PointNumber = 0; PointNumber < 1; PointNumber++)
   {
      //compute element kinematics B, F, DN_DX ...
      this->CalculateKinematics(Variables, rCurrentProcessInfo, PointNumber);

      //calculating weights for integration on the "reference configuration"
      double IntegrationWeight = 1;
      IntegrationWeight = this->CalculateIntegrationWeight(Variables, IntegrationWeight);

      if (rLocalSystem.CalculationFlags.Is(PointRigidContactCondition::COMPUTE_LHS_MATRIX)) //calculation of the matrix is required
      {
         //contributions to stiffness matrix calculated on the reference config
         this->CalculateAndAddLHS(rLocalSystem, Variables, IntegrationWeight);
      }

      if (rLocalSystem.CalculationFlags.Is(PointRigidContactCondition::COMPUTE_RHS_VECTOR)) //calculation of the vector is required
      {
         //contribution to external forces
         this->CalculateAndAddRHS(rLocalSystem, Variables, IntegrationWeight);
      }

      if (Variables.Options.Is(ACTIVE))
      {
         noalias(mContactStressVector) = Variables.ContactStressVector;
      }
   }

   KRATOS_CATCH("")
}

//************* COMPUTING  METHODS
//************************************************************************************
//************************************************************************************

void PointRigidContactCondition::CalculateAndAddLHS(LocalSystemComponents &rLocalSystem, ConditionVariables &rVariables, double &rIntegrationWeight)
{
    MatrixType &rLeftHandSideMatrix = rLocalSystem.GetLeftHandSideMatrix();
    // operation performed: add Kg to the rLefsHandSideMatrix
    this->CalculateAndAddKuug(rLeftHandSideMatrix, rVariables, rIntegrationWeight);
}

//************************************************************************************
//************************************************************************************

void PointRigidContactCondition::CalculateAndAddRHS(LocalSystemComponents &rLocalSystem, ConditionVariables &rVariables, double &rIntegrationWeight)
{
    VectorType &rRightHandSideVector = rLocalSystem.GetRightHandSideVector();
    // operation performed: rRightHandSideVector += ExtForce*IntToReferenceWeight
    this->CalculateAndAddContactForces(rRightHandSideVector, rVariables, rIntegrationWeight);
}

//***********************************************************************************
//************************************************************************************

double &PointRigidContactCondition::CalculateIntegrationWeight(ConditionVariables &rVariables, double &rIntegrationWeight)
{
  const SizeType dimension = GetGeometry().WorkingSpaceDimension();

  if (dimension == 2)
  {
    if (this->GetProperties().Has(THICKNESS))
      rIntegrationWeight *= GetProperties()[THICKNESS];
  }

  return rIntegrationWeight;
}

//************************************************************************************
//************************************************************************************

void PointRigidContactCondition::CalculateLeftHandSide(MatrixType &rLeftHandSideMatrix, const ProcessInfo &rCurrentProcessInfo)
{
  //create local system components
  LocalSystemComponents LocalSystem;

  //calculation flags
  LocalSystem.CalculationFlags.Set(PointRigidContactCondition::COMPUTE_LHS_MATRIX);

  VectorType RightHandSideVector = Vector();

  //Initialize sizes for the system components:
  this->InitializeSystemMatrices(rLeftHandSideMatrix, RightHandSideVector, LocalSystem.CalculationFlags);

  //Set Variables to Local system components
  LocalSystem.SetLeftHandSideMatrix(rLeftHandSideMatrix);
  LocalSystem.SetRightHandSideVector(RightHandSideVector);

  //Calculate condition system
  this->CalculateConditionSystem(LocalSystem, rCurrentProcessInfo);
}

//************************************************************************************
//************************************************************************************

void PointRigidContactCondition::CalculateRightHandSide(VectorType &rRightHandSideVector, const ProcessInfo &rCurrentProcessInfo)
{
   //create local system components
   LocalSystemComponents LocalSystem;

   //calculation flags
   LocalSystem.CalculationFlags.Set(PointRigidContactCondition::COMPUTE_RHS_VECTOR);

   MatrixType LeftHandSideMatrix = Matrix();

   //Initialize sizes for the system components:
   this->InitializeSystemMatrices(LeftHandSideMatrix, rRightHandSideVector, LocalSystem.CalculationFlags);

   //Set Variables to Local system components
   LocalSystem.SetLeftHandSideMatrix(LeftHandSideMatrix);
   LocalSystem.SetRightHandSideVector(rRightHandSideVector);

   //Calculate condition system
   this->CalculateConditionSystem(LocalSystem, rCurrentProcessInfo);
}

//************************************************************************************
//************************************************************************************

void PointRigidContactCondition::CalculateLocalSystem(MatrixType &rLeftHandSideMatrix, VectorType &rRightHandSideVector, const ProcessInfo &rCurrentProcessInfo)
{
   //create local system components
   LocalSystemComponents LocalSystem;

   //calculation flags
   LocalSystem.CalculationFlags.Set(PointRigidContactCondition::COMPUTE_LHS_MATRIX);
   LocalSystem.CalculationFlags.Set(PointRigidContactCondition::COMPUTE_RHS_VECTOR);

   //Initialize sizes for the system components:
   this->InitializeSystemMatrices(rLeftHandSideMatrix, rRightHandSideVector, LocalSystem.CalculationFlags);

   //Set Variables to Local system components
   LocalSystem.SetLeftHandSideMatrix(rLeftHandSideMatrix);
   LocalSystem.SetRightHandSideVector(rRightHandSideVector);

   //Calculate condition system
   this->CalculateConditionSystem(LocalSystem, rCurrentProcessInfo);

   // std::cout<<" ContactID ["<<this->Id()<<"]: LHS "<<rLeftHandSideMatrix<<std::endl;
   // std::cout<<" ContactID ["<<this->Id()<<"]: RHS "<<rRightHandSideVector<<std::endl;
}

//***********************************************************************************
//***********************************************************************************

void PointRigidContactCondition::CalculateMassMatrix(MatrixType &rMassMatrix, const ProcessInfo &rCurrentProcessInfo)
{
   KRATOS_TRY

   rMassMatrix.resize(0, 0, false);

   KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void PointRigidContactCondition::CalculateDampingMatrix(MatrixType &rDampingMatrix, const ProcessInfo &rCurrentProcessInfo)
{
   KRATOS_TRY

   rDampingMatrix.resize(0, 0, false);

   KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void PointRigidContactCondition::CalculateAndAddKuug(MatrixType &rLeftHandSideMatrix,
                                                     ConditionVariables &rVariables,
                                                     double &rIntegrationWeight)
{
   KRATOS_TRY

   KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void PointRigidContactCondition::CalculateAndAddContactForces(VectorType &rRightHandSideVector,
                                                              ConditionVariables &rVariables,
                                                              double &rIntegrationWeight)
{
   KRATOS_TRY

   KRATOS_CATCH("")
}


//**************************** Check Coulomb law for Tangent Contact Force **********
//***********************************************************************************

double PointRigidContactCondition::CalculateCoulombsFrictionLaw(double &rTangentRelativeMovement, double &rNormalForceModulus, ConditionVariables &rVariables)
{

  rVariables.FrictionCoefficient = this->CalculateFrictionCoefficient(rTangentRelativeMovement, rVariables.DeltaTime);

  double FrictionalForceModulus = rVariables.Penalty.Tangent * rVariables.Gap.Tangent;
  double SlipFrictionalForceModulus = rVariables.FrictionCoefficient * fabs(rNormalForceModulus);

  if (fabs(FrictionalForceModulus) > fabs(SlipFrictionalForceModulus))
  {

    rVariables.Slip = true;

    FrictionalForceModulus = SlipFrictionalForceModulus;
  }
  else
  {
    rVariables.Slip = false;
  }

  return FrictionalForceModulus;
}

//**************************** Check friction coefficient ***************************
//***********************************************************************************

double PointRigidContactCondition::CalculateFrictionCoefficient(const double &rTangentRelativeMovement, const double &rDeltaTime)
{

  KRATOS_TRY

  //---FRICTION LAW in function of the relative sliding velocity ---//

  double DynamicFrictionCoefficient = 0.0; //0.2;
  double StaticFrictionCoefficient = 0.0;  //0.3;

  if (GetProperties().Has(FRICTION_ACTIVE))
  {
    if (GetProperties()[FRICTION_ACTIVE])
    {
      if (GetProperties().Has(MU_DYNAMIC))
        DynamicFrictionCoefficient = GetProperties()[MU_DYNAMIC];
      if (GetProperties().Has(MU_STATIC))
        StaticFrictionCoefficient = GetProperties()[MU_STATIC];
    }
  }

  double Velocity = 0;
  Velocity = rTangentRelativeMovement / rDeltaTime;

  //Addicional constitutive parameter  C
  //which describes how fast the static coefficient approaches the dynamic:
  double C = 0.1;

  //Addicional constitutive parameter  E
  //regularization parameter (->0, classical Coulomb law)
  double E = 0.01;

  double FrictionCoefficient = DynamicFrictionCoefficient + (StaticFrictionCoefficient - DynamicFrictionCoefficient) * exp((-1) * C * fabs(Velocity));

  //Square root regularization
  FrictionCoefficient *= fabs(Velocity) / sqrt((Velocity * Velocity) + (E * E));

  //Hyperbolic regularization
  //FrictionCoefficient *= tanh( fabs(Velocity)/E );

  return FrictionCoefficient;

  KRATOS_CATCH("")
}


//***********************************************************************************
//***********************************************************************************

int PointRigidContactCondition::Check(const ProcessInfo &rCurrentProcessInfo) const
{
   return 0;
}

} // Namespace Kratos.
