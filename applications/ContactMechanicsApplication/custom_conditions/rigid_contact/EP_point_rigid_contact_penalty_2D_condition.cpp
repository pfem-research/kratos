//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:             LMonforte $
//   Maintained by:       $Maintainer:                   LM $
//   Date:                $Date:                  July 2016 $
//
//

// System includes

// External includes

// Project includes
#include "custom_conditions/rigid_contact/EP_point_rigid_contact_penalty_2D_condition.hpp"

#include "contact_mechanics_application_variables.h"

namespace Kratos
{
//************************************************************************************
//************************************************************************************
EPPointRigidContactPenalty2DCondition::EPPointRigidContactPenalty2DCondition(IndexType NewId, GeometryType::Pointer
                                                                                                  pGeometry)
    : EPPointRigidContactPenalty3DCondition(NewId, pGeometry)
{
   //DO NOT ADD DOFS HERE!!!
}

//************************************************************************************
//************************************************************************************
EPPointRigidContactPenalty2DCondition::EPPointRigidContactPenalty2DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : EPPointRigidContactPenalty3DCondition(NewId, pGeometry, pProperties)
{
}

//************************************************************************************
//************************************************************************************
EPPointRigidContactPenalty2DCondition::EPPointRigidContactPenalty2DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties, SpatialBoundingBox::Pointer pRigidWall)
    : EPPointRigidContactPenalty3DCondition(NewId, pGeometry, pProperties, pRigidWall)
{
}

//************************************************************************************
//************************************************************************************
EPPointRigidContactPenalty2DCondition::EPPointRigidContactPenalty2DCondition(EPPointRigidContactPenalty2DCondition const &rOther)
    : EPPointRigidContactPenalty3DCondition(rOther)
{
}

//************************************************************************************
//************************************************************************************

Condition::Pointer EPPointRigidContactPenalty2DCondition::Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const
{
   return Kratos::make_intrusive<EPPointRigidContactPenalty2DCondition>(NewId, GetGeometry().Create(ThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************

Condition::Pointer EPPointRigidContactPenalty2DCondition::Clone(IndexType NewId, NodesArrayType const &ThisNodes) const
{
   EPPointRigidContactPenalty2DCondition ClonedCondition(NewId, GetGeometry().Create(ThisNodes), pGetProperties(), mpRigidWall);
   if (this->mpFrictionLaw!=nullptr)
     ClonedCondition.mpFrictionLaw = this->mpFrictionLaw->Clone();
   ClonedCondition.mCurrentInfo = this->mCurrentInfo;
   ClonedCondition.mSavedInfo = this->mSavedInfo;
   return Kratos::make_intrusive<EPPointRigidContactPenalty2DCondition>(ClonedCondition);
}

//************************************************************************************
//************************************************************************************
EPPointRigidContactPenalty2DCondition::~EPPointRigidContactPenalty2DCondition()
{
}

//**************************** CalculateSomeSortOfArea ******************************
//***********************************************************************************
double EPPointRigidContactPenalty2DCondition::CalculateSomeSortOfArea()
{

   double Area = 1;
   const SizeType dimension = GetGeometry().WorkingSpaceDimension();
   if (dimension != 2)
      return Area;

   ElementWeakPtrVectorType &nElements = GetGeometry()[0].GetValue(NEIGHBOUR_ELEMENTS);

   std::vector<double> AreaVector;
   for (auto &i_nelem : nElements)
   {
      const Geometry<Node<3>> &rElemGeom = i_nelem.GetGeometry();
      SizeType nBoundary = 0;

      std::vector<SizeType> BoundaryNodes;
      for (SizeType i = 0; i < rElemGeom.size(); i++)
      {

         if (rElemGeom[i].Is(BOUNDARY))
         {
            array_1d<double, 3>  CN = rElemGeom[i].FastGetSolutionStepValue(CONTACT_NORMAL);
            if ( fabs(CN[0]) + fabs(CN[1]) < 0.01 )
               CN = rElemGeom[i].FastGetSolutionStepValue(NORMAL);

            if ( (fabs(CN[0]) + fabs(CN[1])) > 0.01 )
            {
               BoundaryNodes.push_back(i);
               nBoundary += 1;
               if (nBoundary == 2)
                  break;
            }
         }
      }

      if (nBoundary == 2)
      {
         array_1d<double, 3> Vector1 = rElemGeom[BoundaryNodes[1]].Coordinates() - rElemGeom[BoundaryNodes[0]].Coordinates();
         double ThisArea = MathUtils<double>::Norm3(Vector1);
         AreaVector.push_back(ThisArea);
      }
   }

   if (AreaVector.size() > 0)
   {
      Area = 0;
      for (SizeType i = 0; i < AreaVector.size(); i++)
         Area += AreaVector[i];
      Area /= 2.0;
   }

   return Area;
}

} // Namespace Kratos
