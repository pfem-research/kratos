//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:             LMonforte $
//   Maintained by:       $Maintainer:                   LM $
//   Date:                $Date:                  July 2013 $
//
//

#if !defined(KRATOS_EP_POINT_RIGID_CONTACT_PENALTY_WP_3D_CONDITION_HPP_INCLUDED)
#define KRATOS_EP_POINT_RIGID_CONTACT_PENALTY_WP_3D_CONDITION_HPP_INCLUDED

#include "custom_conditions/rigid_contact/EP_point_rigid_contact_penalty_3D_condition.hpp"

namespace Kratos
{

///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
*/
class KRATOS_API(CONTACT_MECHANICS_APPLICATION) EPPointRigidContactPenaltywP3DCondition
    : public EPPointRigidContactPenalty3DCondition
{

public:
    ///@name Type Definitions
    typedef EPPointRigidContactPenalty3DCondition BaseType;

    typedef BaseType::NodeType NodeType;
    typedef BaseType::PointType PointType;
    typedef BaseType::SizeType SizeType;
    typedef BaseType::NodeWeakPtrVectorType NodeWeakPtrVectorType;
    typedef BaseType::ElementWeakPtrVectorType ElementWeakPtrVectorType;
    typedef BaseType::ConditionWeakPtrVectorType ConditionWeakPtrVectorType;


    typedef Vector VectorType;

    KRATOS_CLASS_INTRUSIVE_POINTER_DEFINITION(EPPointRigidContactPenaltywP3DCondition);

    /// Serialization constructor
    EPPointRigidContactPenaltywP3DCondition(){};

    /// Default constructor.
    EPPointRigidContactPenaltywP3DCondition(IndexType NewId, GeometryType::Pointer pGeometry);

    EPPointRigidContactPenaltywP3DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties);

    EPPointRigidContactPenaltywP3DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties, SpatialBoundingBox::Pointer pRigidWall);

    /// Copy constructor
    EPPointRigidContactPenaltywP3DCondition(EPPointRigidContactPenaltywP3DCondition const &rOther);

    /// Destructor.
    virtual ~EPPointRigidContactPenaltywP3DCondition();

    ///@}
    ///@name Operators
    ///@{

    ///@}
    ///@name Operations
    ///@{

    /**
     * creates a new condition pointer
     * @param NewId: the ID of the new condition
     * @param ThisNodes: the nodes of the new condition
     * @param pProperties: the properties assigned to the new condition
     * @return a Pointer to the new condition
     */
    Condition::Pointer Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const override;

    /**
     * clones the selected condition variables, creating a new one
     * @param NewId: the ID of the new condition
     * @param ThisNodes: the nodes of the new condition
     * @param pProperties: the properties assigned to the new condition
     * @return a Pointer to the new condition
     */
    Condition::Pointer Clone(IndexType NewId,
                             NodesArrayType const &ThisNodes) const override;

    /**
     * Get dofs list
     */
    void GetDofList(DofsVectorType &rConditionDofList,
                    const ProcessInfo &rCurrentProcessInfo) const override;

    /**
     * Sets on rResult the ID's of the element degrees of freedom
     */
    void EquationIdVector(EquationIdVectorType &rResult,
                          const ProcessInfo &rCurrentProcessInfo) const override;

    /**
     * Sets on rValues the nodal displacements
     */
    void GetValuesVector(Vector &rValues,
                         int Step = 0) const override;

    /**
     * Sets on rValues the nodal velocities
     */
    void GetFirstDerivativesVector(Vector &rValues,
                                   int Step = 0) const override;

    /**
     * Sets on rValues the nodal accelerations
     */
    void GetSecondDerivativesVector(Vector &rValues,
                                    int Step = 0) const override;
    ///@}
    ///@name Access
    ///@{
    ///@}
    ///@name Inquiry
    ///@{
    ///@}
    ///@name Input and output
    ///@{
    ///@}
    ///@name Friends
    ///@{
    ///@}
protected:
    ///@name Protected static Member Variables
    ///@{
    ///@}
    ///@name Protected member Variables
    ///@{
    ///@}
    ///@name Protected Operators
    ///@{
    ///@}
    ///@name Protected Operations
    ///@{
    void InitializeSystemMatrices(MatrixType &rLeftHandSideMatrix,
                                  VectorType &rRightHandSideVector,
                                  Flags &rCalculationFlags) override;

    /**
     * Calculation of the Load Stiffness Matrix which usually is subtracted to the global stiffness matrix
     */
    void CalculateAndAddKuug(MatrixType &rLeftHandSideMatrix,
                             ConditionVariables &rVariables,
                             double &rIntegrationWeight) override;

    void CalculateAndAddKuugTangent(MatrixType &rLeftHandSideMatrix,
                                    ConditionVariables &rVariables,
                                    double &rIntegrationWeight) override;

    ///@}
    ///@name Protected  Access
    ///@{
    ///@}
    ///@name Protected Inquiry
    ///@{
    ///@}
    ///@name Protected LifeCycle
    ///@{
    ///@}

private:
    ///@name Static Member Variables
    ///@{
    ///@}
    ///@name Member Variables
    ///@{
    ///@}
    ///@name Private Operators
    ///@{
    ///@}
    ///@name Private Operations
    ///@{
    ///@}
    ///@name Private  Access
    ///@{
    ///@}
    ///@name Private Inquiry
    ///@{
    ///@}
    ///@name Serialization
    ///@{

    friend class Serializer;

    void save(Serializer &rSerializer) const override
    {
        KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, EPPointRigidContactPenalty3DCondition)
    }

    void load(Serializer &rSerializer) override
    {
        KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, EPPointRigidContactPenalty3DCondition)
    }

}; // Class EPPointRigidContactPenaltywP3DCondition

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
///@}

} // end Namespace Kratos

#endif // KRATOS_EP_POINT_RIGID_CONTACT_PENALTY_WP_3D_CONDITION_HPP_INCLUDED  defined
