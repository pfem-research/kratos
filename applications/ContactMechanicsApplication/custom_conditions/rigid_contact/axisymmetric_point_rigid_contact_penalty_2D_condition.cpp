//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                August 2016 $
//
//

// System includes

// External includes

// Project includes
#include "custom_conditions/rigid_contact/axisymmetric_point_rigid_contact_penalty_2D_condition.hpp"

namespace Kratos
{
//************************************************************************************
//************************************************************************************
AxisymmetricPointRigidContactPenalty2DCondition::AxisymmetricPointRigidContactPenalty2DCondition(IndexType NewId, GeometryType::Pointer pGeometry)
    : PointRigidContactPenalty2DCondition(NewId, pGeometry)
{
}

//************************************************************************************
//************************************************************************************
AxisymmetricPointRigidContactPenalty2DCondition::AxisymmetricPointRigidContactPenalty2DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : PointRigidContactPenalty2DCondition(NewId, pGeometry, pProperties)
{
}

//************************************************************************************
//************************************************************************************
AxisymmetricPointRigidContactPenalty2DCondition::AxisymmetricPointRigidContactPenalty2DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties, SpatialBoundingBox::Pointer pRigidWall)
    : PointRigidContactPenalty2DCondition(NewId, pGeometry, pProperties, pRigidWall)
{
}

//************************************************************************************
//************************************************************************************
AxisymmetricPointRigidContactPenalty2DCondition::AxisymmetricPointRigidContactPenalty2DCondition(AxisymmetricPointRigidContactPenalty2DCondition const &rOther)
    : PointRigidContactPenalty2DCondition(rOther)
{
}

//************************************************************************************
//************************************************************************************

Condition::Pointer AxisymmetricPointRigidContactPenalty2DCondition::Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const
{
  return Kratos::make_intrusive<AxisymmetricPointRigidContactPenalty2DCondition>(NewId, GetGeometry().Create(ThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************

Condition::Pointer AxisymmetricPointRigidContactPenalty2DCondition::Clone(IndexType NewId, NodesArrayType const &ThisNodes) const
{
  AxisymmetricPointRigidContactPenalty2DCondition ClonedCondition(NewId, GetGeometry().Create(ThisNodes), pGetProperties(), mpRigidWall);
  if (this->mpFrictionLaw!=nullptr)
    ClonedCondition.mpFrictionLaw = this->mpFrictionLaw->Clone();
  return Kratos::make_intrusive<AxisymmetricPointRigidContactPenalty2DCondition>(ClonedCondition);
}

//************************************************************************************
//************************************************************************************

AxisymmetricPointRigidContactPenalty2DCondition::~AxisymmetricPointRigidContactPenalty2DCondition()
{
}

//*********************************COMPUTE KINEMATICS*********************************
//************************************************************************************

void AxisymmetricPointRigidContactPenalty2DCondition::CalculateKinematics(ConditionVariables &rVariables,
                                                                    const ProcessInfo &rCurrentProcessInfo,
                                                                    const double &rPointNumber)
{
  KRATOS_TRY

  CalculateRadius(rVariables.CurrentRadius, rVariables.ReferenceRadius);

  PointRigidContactPenalty2DCondition::CalculateKinematics(rVariables, rCurrentProcessInfo, rPointNumber);

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void AxisymmetricPointRigidContactPenalty2DCondition::CalculateContactFactors(ConditionVariables &rVariables, const ProcessInfo &rCurrentProcessInfo)
{

  KRATOS_TRY

  NodeWeakPtrVectorType &nNodes = GetGeometry()[0].GetValue(NEIGHBOUR_NODES);

  array_1d<double, 3> Contact_Point = GetGeometry()[0].Coordinates();
  array_1d<double, 3> Neighb_Point;

  double distance = 0;
  double counter = 0;

  // double radius   = 0;
  // double meanradius = 0;

  for (auto &i_nnode : nNodes)
  {
    if (i_nnode.Is(BOUNDARY))
    {

      Neighb_Point = i_nnode.Coordinates();

      // radius = fabs(Contact_Point[0] + rN[i].X()) * 0.5;
      // meanradius += radius;
      // distance += norm_2(Contact_Point-Neighb_Point) * radius;

      distance += norm_2(Contact_Point - Neighb_Point);

      counter++;
    }
  }

  // if( Contact_Point[0] > 0)
  //   distance /= ( counter * Contact_Point[0] );
  // else
  //   distance /= ( counter * (meanradius/counter) );

  if (counter != 0)
    distance /= counter;

  if (distance == 0)
    distance = 1;

  rVariables.ContributoryFactor = distance;

  const double &contact_area = rCurrentProcessInfo[CONTACT_AREA];
  if (contact_area != 0)
    distance /= contact_area;

  //get contact properties and parameters
  double PenaltyParameter = 1;
  if (GetProperties().Has(PENALTY_PARAMETER))
    PenaltyParameter = GetProperties()[PENALTY_PARAMETER];

  ElementWeakPtrVectorType &nElements = GetGeometry()[0].GetValue(NEIGHBOUR_ELEMENTS);
  double ElasticModulus = 0;
  if (GetProperties().Has(YOUNG_MODULUS))
  {
    ElasticModulus = GetProperties()[YOUNG_MODULUS];
  }
  else if (GetProperties().Has(C10))
  {
    ElasticModulus = GetProperties()[C10];
  }
  else
  {

    if (nElements.front().GetProperties().Has(YOUNG_MODULUS))
    {
      ElasticModulus = nElements.front().GetProperties()[YOUNG_MODULUS];
    }
    else if (nElements.front().GetProperties().Has(C10))
    {
      ElasticModulus = nElements.front().GetProperties()[C10];
    }
  }

  // the Modified Cam Clay model does not have a constant Young modulus, so something similar to that is computed
  if (ElasticModulus <= 1.0e-5)
  {
    std::vector<double> mModulus;
    const ProcessInfo SomeProcessInfo;
    for (auto &i_nelem : nElements)
    {
      i_nelem.CalculateOnIntegrationPoints(YOUNG_MODULUS, mModulus, SomeProcessInfo);
      ElasticModulus += mModulus[0];
    }
    ElasticModulus /= double(nElements.size());
  }

  double factor = 4;
  if (distance < 1.0)
  { //take a number bigger than 1.0 (length units)
    int order = (int)((-1) * std::log10(distance) + 1);
    distance *= factor * pow(10, order);
  }

  rVariables.Penalty.Normal = distance * PenaltyParameter * ElasticModulus;

  double PenaltyRatio = 1;
  if (GetProperties().Has(TANGENTIAL_PENALTY_RATIO))
    PenaltyRatio = GetProperties()[TANGENTIAL_PENALTY_RATIO];

  rVariables.Penalty.Tangent = rVariables.Penalty.Normal * PenaltyRatio;

  //std::cout<<" Node "<<GetGeometry()[0].Id()<<" Contact Factors "<<rVariables.Penalty.Normal<<" Gap Normal "<<rVariables.Gap.Normal<<" Gap Tangent "<<rVariables.Gap.Tangent<<" Surface.Normal "<<rVariables.Surface.Normal<<" Surface.Tangent "<<rVariables.Surface.Tangent<<" distance "<<distance<<" ElasticModulus "<<ElasticModulus<<" PenaltyParameter "<<PenaltyParameter<<std::endl;
  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void AxisymmetricPointRigidContactPenalty2DCondition::CalculateRadius(double &rCurrentRadius, double &rReferenceRadius)
{

  KRATOS_TRY

  rCurrentRadius = 0;
  rReferenceRadius = 0;

  //Displacement from the reference to the current configuration
  const array_1d<double, 3> &CurrentDisplacement = GetGeometry()[0].FastGetSolutionStepValue(DISPLACEMENT);
  const array_1d<double, 3> &PreviousDisplacement = GetGeometry()[0].FastGetSolutionStepValue(DISPLACEMENT, 1);
  array_1d<double, 3> DeltaDisplacement = CurrentDisplacement - PreviousDisplacement;
  const array_1d<double, 3> &CurrentPosition = GetGeometry()[0].Coordinates();
  array_1d<double, 3> ReferencePosition = CurrentPosition - DeltaDisplacement;

  rCurrentRadius = CurrentPosition[0];
  rReferenceRadius = ReferencePosition[0];

  if (rCurrentRadius == 0)
  {

    NodeWeakPtrVectorType &nNodes = GetGeometry()[0].GetValue(NEIGHBOUR_NODES);

    double counter = 0;

    for (auto &i_nnode : nNodes)
    {
      const array_1d<double, 3> &NodePosition = i_nnode.Coordinates();
      if (NodePosition[0] != 0)
      {
        rCurrentRadius += NodePosition[0] * 0.225;
        counter++;
      }
    }

    rCurrentRadius /= counter;
  }

  KRATOS_CATCH("")
}

//***********************************************************************************
//************************************************************************************

double & AxisymmetricPointRigidContactPenalty2DCondition::CalculateIntegrationWeight(ConditionVariables &rVariables, double &rIntegrationWeight)
{
  KRATOS_TRY

  rIntegrationWeight *= 2.0 * Globals::Pi * rVariables.CurrentRadius;
  rIntegrationWeight = fabs(rIntegrationWeight);
  return rIntegrationWeight;

  KRATOS_CATCH("")
}

} // Namespace Kratos
