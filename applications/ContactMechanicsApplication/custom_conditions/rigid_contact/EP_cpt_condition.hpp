//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:             LMonforte $
//   Maintained by:       $Maintainer:                   LM $
//   Date:                $Date:                  July 2016 $
//
//

#if !defined(KRATOS_EP_CPT_CONDITION_HPP_INCLUDED)
#define KRATOS_EP_CPT_CONDITION_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_conditions/rigid_contact/EP_axisymmetric_point_rigid_contact_penalty_2D_condition.hpp"

namespace Kratos
{

///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
*/
class KRATOS_API(CONTACT_MECHANICS_APPLICATION) EPCPTCondition
    : public EPAxisymmetricPointRigidContactPenalty2DCondition
{
public:
    ///@name Type Definitions
    typedef EPAxisymmetricPointRigidContactPenalty2DCondition BaseType;

    typedef BaseType::NodeType NodeType;
    typedef BaseType::PointType PointType;
    typedef BaseType::SizeType SizeType;
    typedef BaseType::NodeWeakPtrVectorType NodeWeakPtrVectorType;
    typedef BaseType::ElementWeakPtrVectorType ElementWeakPtrVectorType;
    typedef BaseType::ConditionWeakPtrVectorType ConditionWeakPtrVectorType;

    ///@{
    // Counted pointer of PointRigidContactPenalty2DCondition
    KRATOS_CLASS_POINTER_DEFINITION(EPCPTCondition);
    ///@}

    ///@}
    ///@name Life Cycle
    ///@{

    /// Serialization constructor
    EPCPTCondition(){};

    /// Default constructor
    EPCPTCondition(IndexType NewId, GeometryType::Pointer pGeometry);

    EPCPTCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties);

    EPCPTCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties, SpatialBoundingBox::Pointer pRigidWall);

    /// Copy constructor
    EPCPTCondition(EPCPTCondition const &rOther);

    /// Destructor.
    virtual ~EPCPTCondition();

    ///@}
    ///@name Operators
    ///@{

    ///@}
    ///@name Operations
    ///@{

    /**
     * creates a new condition pointer
     * @param NewId: the ID of the new condition
     * @param ThisNodes: the nodes of the new condition
     * @param pProperties: the properties assigned to the new condition
     * @return a Pointer to the new condition
     */
    Condition::Pointer Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const override;

    /**
     * clones the selected condition variables, creating a new one
     * @param NewId: the ID of the new condition
     * @param ThisNodes: the nodes of the new condition
     * @param pProperties: the properties assigned to the new condition
     * @return a Pointer to the new condition
     */
    Condition::Pointer Clone(IndexType NewId,
                             NodesArrayType const &ThisNodes) const override;

    void InitializeNonLinearIteration(const ProcessInfo &rCurrentProcessInfo) override;

    ///@}
    ///@name Access
    ///@{
    ///@}
    ///@name Inquiry
    ///@{
    ///@}
    ///@name Input and output
    ///@{
    ///@}
    ///@name Friends
    ///@{
    ///@}

protected:
    ///@name Protected static Member Variables
    ///@{
    ///@}
    ///@name Protected member Variables
    ///@{
    ///@}
    ///@name Protected Operators
    ///@{
    ///@}
    ///@name Protected Operations
    ///@{

    bool CalculateFrictionLaw(ConditionVariables &rVariables, ConstitutiveVariables &rConstitutiveVariables, Vector &rTangentForce) override;
    
    void CalculateContactFactors(ConditionVariables &rContact, const ProcessInfo &rCurrentProcessInfo) override;

    double & GetContactFrictionCoefficient( double & rFrictionCoefficient);

    ///@}
    ///@name Protected  Access
    ///@{
    ///@}
    ///@name Protected Inquiry
    ///@{
    ///@}
    ///@name Protected LifeCycle
    ///@{
    ///@}

private:
    ///@name Static Member Variables
    ///@{
    ///@}
    ///@name Member Variables
    ///@{

    double mTime;

    ///@}
    ///@name Private Operators
    ///@{
    ///@}
    ///@name Private Operations
    ///@{
    ///@}
    ///@name Private  Access
    ///@{
    ///@}
    ///@name Private Inquiry
    ///@{
    ///@}
    ///@name Serialization
    ///@{

    friend class Serializer;

    virtual void save(Serializer &rSerializer) const override
    {
        KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, EPAxisymmetricPointRigidContactPenalty2DCondition)
    }

    virtual void load(Serializer &rSerializer) override
    {
        KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, EPAxisymmetricPointRigidContactPenalty2DCondition)
    }

}; // Class EPCPTCondition

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

} // namespace Kratos.

#endif // KRATOS_AXISYMMETRIC_POINT_RIGID_CONTACT_PENALTY_2D_CONDITION_HPP_INCLUDED  defined
