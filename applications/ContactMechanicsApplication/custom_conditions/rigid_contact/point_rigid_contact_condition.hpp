//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                August 2016 $
//
//

#if !defined(KRATOS_POINT_RIGID_CONTACT_CONDITION_HPP_INCLUDED)
#define KRATOS_POINT_RIGID_CONTACT_CONDITION_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "includes/define.h"
#include "includes/serializer.h"
#include "includes/condition.h"
#include "includes/ublas_interface.h"
#include "includes/variables.h"
#include "contact_mechanics_application_variables.h"
#include "custom_bounding/spatial_bounding_box.hpp"
#include "custom_friction/friction_law.hpp"

namespace Kratos
{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Point Rigid Contact Condition for 3D and 2D geometries. (base class)

/**
 * Implements a Contact Point Load definition for solid nodes.
 * This works for arbitrary geometries in 3D and 2D (base class)
 */
class KRATOS_API(CONTACT_MECHANICS_APPLICATION) PointRigidContactCondition
    : public Condition
{
public:
   ///@name Type Definitions
   typedef Node<3> NodeType;
   typedef array_1d<double, 3> PointType;
   typedef GeometryData::SizeType SizeType;
   typedef GlobalPointersVector<NodeType> NodeWeakPtrVectorType;
   typedef GlobalPointersVector<Element> ElementWeakPtrVectorType;
   typedef GlobalPointersVector<Condition> ConditionWeakPtrVectorType;
   ///@{
   // Counted pointer of PointRigidContactCondition
   KRATOS_CLASS_INTRUSIVE_POINTER_DEFINITION(PointRigidContactCondition);
   ///@}

protected:
   /**
     * Flags related to the condition computation
     */

   KRATOS_DEFINE_LOCAL_FLAG(COMPUTE_RHS_VECTOR);
   KRATOS_DEFINE_LOCAL_FLAG(COMPUTE_LHS_MATRIX);
   KRATOS_DEFINE_LOCAL_FLAG(FINALIZED_STEP);

   /**
     * Parameters to be used in the Condition as they are.
     */

   typedef struct
   {
      PointType Normal;  //normal direction
      PointType Tangent; //tangent direction

   } SurfaceVector;

   typedef struct
   {
      double Normal;  //normal component
      double Tangent; //tangent component

   } SurfaceScalar;

   struct ConditionVariables
   {
   private:

     const ProcessInfo *pProcessInfo;

   public:

      Flags Options; //calculation options

      //Geometrical gaps:
      SurfaceScalar Gap;         //normal and tangential gap
      double ContributoryFactor; //distance to neighbour points

      //Friction:
      bool Slip;                      //slip = true / stick = false
      PointType RelativeDisplacement; //relative point displacement
      double FrictionCoefficient;     //total friction coeffitient mu
      double DeltaTime;               //time step

      SurfaceScalar Penalty; //Penalty Parameter normal and tangent

      //Geometric variables
      SurfaceVector Surface; //normal and tangent vector to the surface

      // Elasto-plastic constitutive matrix (axisym and PS) (Normal and Tangent stiffness)
      SurfaceScalar TangentMatrix;

      //Contact stress
      Vector ContactStressVector;

      //for axisymmetric use only
      double CurrentRadius;
      double ReferenceRadius;

      //for rigid body
      Matrix SkewSymDistance; //compute the skewsymmmetric tensor of the distance

      void Initialize()
      {
         Gap.Normal = 0;
         Gap.Tangent = 0;

         ContributoryFactor = 0;

         RelativeDisplacement.resize(3);
         noalias(RelativeDisplacement) = ZeroVector(3);

         Slip = false;
         DeltaTime = 0;
         FrictionCoefficient = 0;
         Penalty.Normal = 0;
         Penalty.Tangent = 0;

         Surface.Normal.resize(3);
         noalias(Surface.Normal) = ZeroVector(3);
         Surface.Tangent.resize(3);
         noalias(Surface.Tangent) = ZeroVector(3);

         SkewSymDistance.resize(3, 3, false);
         noalias(SkewSymDistance) = ZeroMatrix(3, 3);

         TangentMatrix.Normal = 0;
         TangentMatrix.Tangent = 0;

         CurrentRadius = 0;
         ReferenceRadius = 0;
      }

      void SetProcessInfo(const ProcessInfo &rProcessInfo)
      {
         pProcessInfo = &rProcessInfo;
      };

      const ProcessInfo &GetProcessInfo()
      {
         return *pProcessInfo;
      };

   };

   struct LocalSystemComponents
   {
   private:
      //for calculation local system with compacted LHS and RHS
      MatrixType *mpLeftHandSideMatrix;
      VectorType *mpRightHandSideVector;

   public:
      //calculation flags
      Flags CalculationFlags;

      /**
       * sets the value of a specified pointer variable
       */
      void SetLeftHandSideMatrix(MatrixType &rLeftHandSideMatrix) { mpLeftHandSideMatrix = &rLeftHandSideMatrix; };

      void SetRightHandSideVector(VectorType &rRightHandSideVector) { mpRightHandSideVector = &rRightHandSideVector; };

      /**
       * returns the value of a specified pointer variable
       */
      MatrixType &GetLeftHandSideMatrix() { return *mpLeftHandSideMatrix; };

      VectorType &GetRightHandSideVector() { return *mpRightHandSideVector; };
   };

public:
   ///@name Life Cycle
   ///@{

   /// Serialization constructor
   PointRigidContactCondition(){};

   /// Default constructor
   PointRigidContactCondition(IndexType NewId, GeometryType::Pointer pGeometry);

   PointRigidContactCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties);

   PointRigidContactCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties, SpatialBoundingBox::Pointer pRigidWall);

   /// Copy constructor
   PointRigidContactCondition(PointRigidContactCondition const &rOther);

   /// Destructor
   virtual ~PointRigidContactCondition();

   ///@}
   ///@name Operators
   ///@{

   ///@}
   ///@name Operations
   ///@{

   /**
     * creates a new condition pointer
     * @param NewId: the ID of the new condition
     * @param ThisNodes: the nodes of the new condition
     * @param pProperties: the properties assigned to the new condition
     * @return a Pointer to the new condition
     */
   Condition::Pointer Create(IndexType NewId,
                             NodesArrayType const &ThisNodes,
                             PropertiesType::Pointer pProperties) const override;

   /**
     * clones the selected condition variables, creating a new one
     * @param NewId: the ID of the new condition
     * @param ThisNodes: the nodes of the new condition
     * @param pProperties: the properties assigned to the new condition
     * @return a Pointer to the new condition
     */
   Condition::Pointer Clone(IndexType NewId,
                            NodesArrayType const &ThisNodes) const override;

   //************* STARTING - ENDING  METHODS

   /**
     * Called at the beginning
     */
   void Initialize(const ProcessInfo &rCurrentProcessInfo) override;

   /**
     * Called at the end of each solution step
     */
   void InitializeSolutionStep(const ProcessInfo &rCurrentProcessInfo) override;

   /**
     * Called at the beginning of each iteration
     */
   void InitializeNonLinearIteration(const ProcessInfo &rCurrentProcessInfo) override;

   /**
     * Called at the end of each iteration
     */
   void FinalizeNonLinearIteration(const ProcessInfo &rCurrentProcessInfo) override;

   /**
     * Called at the end of each solution step
     */
   void FinalizeSolutionStep(const ProcessInfo &rCurrentProcessInfo) override;

   /**
     * Initialize spatial bounding box requiered
     */
   void InitializeSpatialBoundingBox();

   //************* GETTING METHODS

   /**
     * Sets on rConditionDofList the degrees of freedom of the considered element geometry
     */
   void GetDofList(DofsVectorType &rConditionDofList,
                   const ProcessInfo &rCurrentProcessInfo) const override;

   /**
     * Sets on rResult the ID's of the element degrees of freedom
     */
   void EquationIdVector(EquationIdVectorType &rResult,
                         const ProcessInfo &rCurrentProcessInfo) const override;

   /**
     * Sets on rValues the nodal displacements
     */
   void GetValuesVector(Vector &rValues,
                        int Step = 0) const override;

   /**
     * Sets on rValues the nodal velocities
     */
   void GetFirstDerivativesVector(Vector &rValues,
                                  int Step = 0) const override;

   /**
     * Sets on rValues the nodal accelerations
     */
   void GetSecondDerivativesVector(Vector &rValues,
                                   int Step = 0) const override;

   //************* COMPUTING  METHODS

   /**
     * this is called during the assembling process in order
     * to calculate all condition contributions to the global system
     * matrix and the right hand side
     * @param rLeftHandSideMatrix: the condition left hand side matrix
     * @param rRightHandSideVector: the condition right hand side
     * @param rCurrentProcessInfo: the current process info instance
     */
   void CalculateLocalSystem(MatrixType &rLeftHandSideMatrix,
                             VectorType &rRightHandSideVector,
                             const ProcessInfo &rCurrentProcessInfo) override;

   /**
      * this is called during the assembling process in order
      * to calculate the condition right hand side vector only
      * @param rRightHandSideVector: the condition right hand side vector
      * @param rCurrentProcessInfo: the current process info instance
      */
   void CalculateRightHandSide(VectorType &rRightHandSideVector,
                               const ProcessInfo &rCurrentProcessInfo) override;

   /**
      * this is called during the assembling process in order
      * to calculate the elemental left hand side vector only
      * @param rLeftHandSideVector: the elemental left hand side vector
      * @param rCurrentProcessInfo: the current process info instance
      */
   void CalculateLeftHandSide(MatrixType &rLeftHandSideMatrix,
                              const ProcessInfo &rCurrentProcessInfo) override;

   /**
      * this is called during the assembling process in order
      * to calculate the condition mass matrix
      * @param rMassMatrix: the condition mass matrix
      * @param rCurrentProcessInfo: the current process info instance
      */
   void CalculateMassMatrix(
       MatrixType &rMassMatrix,
       const ProcessInfo &rCurrentProcessInfo) override;

   /**
      * this is called during the assembling process in order
      * to calculate the condition damping matrix
      * @param rDampingMatrix: the condition damping matrix
      * @param rCurrentProcessInfo: the current process info instance
      */
   void CalculateDampingMatrix(
       MatrixType &rDampingMatrix,
       const ProcessInfo &rCurrentProcessInfo) override;

   /**
     * this function is designed to make the element to assemble an rRHS vector
     * identified by a variable rRHSVariable by assembling it to the nodes on the variable
     * rDestinationVariable.
     * @param rRHSVector: input variable containing the RHS vector to be assembled
     * @param rRHSVariable: variable describing the type of the RHS vector to be assembled
     * @param rDestinationVariable: variable in the database to which the rRHSvector will be assembled
      * @param rCurrentProcessInfo: the current process info instance
     */
   void AddExplicitContribution(const VectorType &rRHSVector,
                                const Variable<VectorType> &rRHSVariable,
                                const Variable<array_1d<double, 3>> &rDestinationVariable,
                                const ProcessInfo &rCurrentProcessInfo) override;

   //************************************************************************************
   //************************************************************************************
   /**
     * This function provides the place to perform checks on the completeness of the input.
     * It is designed to be called only once (or anyway, not often) typically at the beginning
     * of the calculations, so to verify that nothing is missing from the input
     * or that no common error is found.
     * @param rCurrentProcessInfo
     */
   int Check(const ProcessInfo &rCurrentProcessInfo) const  override;


   ///@}
   ///@name Access
   ///@{
   ///@}
   ///@name Inquiry
   ///@{
   ///@}
   ///@name Input and output
   ///@{
   ///@}
   ///@name Friends
   ///@{
   ///@}

protected:
   ///@name Protected static Member Variables
   ///@{
   ///@}
   ///@name Protected member Variables
   ///@{

   /**
     * Currently selected integration methods
     */
   IntegrationMethod mThisIntegrationMethod;

   /**
     * Pointer to the spatial bounding box defining the rigid wall
     */
   SpatialBoundingBox::Pointer mpRigidWall=nullptr;

   /**
     * Pointer to the friction law
     */
   FrictionLaw::Pointer mpFrictionLaw;

   /**
     * Contact stress tensor
     */
   Vector mContactStressVector;

   ///@}
   ///@name Protected Operators
   ///@{
   ///@}
   ///@name Protected Operations
   ///@{

   /**
     * Get dof size of a node
     */
   virtual SizeType GetNodeDofsSize() const;

   /**
     * Get element size from the dofs
     */
   virtual SizeType GetDofsSize() const;

   /**
     * Clear Nodal Forces
     */
   void ClearNodalForces();

   /**
     * Initialize System Matrices
     */
   virtual void InitializeSystemMatrices(MatrixType &rLeftHandSideMatrix,
                                         VectorType &rRightHandSideVector,
                                         Flags &rCalculationFlags);

   /**
     * Initialize General Variables
     */
   virtual void InitializeConditionVariables(ConditionVariables &rVariables,
                                             const ProcessInfo &rCurrentProcessInfo);

   /**
     * Calculate Condition Kinematics
     */
   virtual void CalculateKinematics(ConditionVariables &rVariables,
                                    const ProcessInfo &rCurrentProcessInfo,
                                    const double &rPointNumber);

   /**
     * Calculation of the Integration Weight
     */
   virtual double &CalculateIntegrationWeight(ConditionVariables &rVariables,
                                              double &rIntegrationWeight);

   /**
     * Calculates the condition contributions
     */
   virtual void CalculateConditionSystem(LocalSystemComponents &rLocalSystem,
                                         const ProcessInfo &rCurrentProcessInfo);

   /**
     * Calculation and addition of the matrices of the LHS
     */
   virtual void CalculateAndAddLHS(LocalSystemComponents &rLocalSystem,
                                   ConditionVariables &rVariables,
                                   double &rIntegrationWeight);

   /**
     * Calculation and addition of the vectors of the RHS
     */
   virtual void CalculateAndAddRHS(LocalSystemComponents &rLocalSystem,
                                   ConditionVariables &rVariables,
                                   double &rIntegrationWeight);

   /**
     * Calculation of the Load Stiffness Matrix which usually is subtracted to the global stiffness matrix
     */
   virtual void CalculateAndAddKuug(MatrixType &rLeftHandSideMatrix,
                                    ConditionVariables &rVariables,
                                    double &rIntegrationWeight);

   /**
     * Calculation of the Contact Forces Vector
     */
   virtual void CalculateAndAddContactForces(Vector &rRightHandSideVector,
                                             ConditionVariables &rVariables,
                                             double &rIntegrationWeight);


   virtual double CalculateCoulombsFrictionLaw(double &rTangentForceModulus, double &rNormalForceModulus, ConditionVariables &rVariables);

   virtual double CalculateFrictionCoefficient(const double &rTangentRelativeMovement, const double &rDeltaTime);


   ///@}
   ///@name Protected  Access
   ///@{

   ///@}
   ///@name Protected Inquiry
   ///@{

   ///@}
   ///@name Protected LifeCycle
   ///@{

   ///@}

private:
   ///@name Static Member Variables
   ///@{

   ///@}
   ///@name Member Variables
   ///@{

   ///@}
   ///@name Private Operators
   ///@{

   ///@}
   ///@name Private Operations
   ///@{

   ///@}
   ///@name Private  Access
   ///@{

   ///@}
   ///@name Private Inquiry
   ///@{

   ///@}
   ///@name Serialization
   ///@{

   friend class Serializer;

   void save(Serializer &rSerializer) const override
   {
      KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, Condition)
      //rSerializer.save("mpRigidWall",mpRigidWall); //rebuild in contact search and in restart
      rSerializer.save("mpFrictionLaw", mpFrictionLaw);
      rSerializer.save("mContactStressVector", mContactStressVector);
   }

   void load(Serializer &rSerializer) override
   {
      KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, Condition)
      //rSerializer.load("mpRigidWall",mpRigidWall);
      rSerializer.load("mpFrictionLaw", mpFrictionLaw);
      rSerializer.load("mContactStressVector", mContactStressVector);
   }

   ///@}

}; // class PointRigidContactCondition.

} // namespace Kratos.

#endif // KRATOS_POINT_RIGID_CONTACT_CONDITION_HPP_INCLUDED defined
