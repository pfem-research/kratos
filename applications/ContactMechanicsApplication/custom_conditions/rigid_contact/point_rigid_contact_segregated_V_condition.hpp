//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:             September 2021 $
//
//

#if !defined(KRATOS_POINT_RIGID_CONTACT_SEGREGATED_V_CONDITION_HPP_INCLUDED)
#define KRATOS_POINT_RIGID_CONTACT_SEGREGATED_V_CONDITION_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_conditions/rigid_contact/point_rigid_contact_V_condition.hpp"

namespace Kratos
{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Rigid Body Segregated V Condition for 3D space dimension

class KRATOS_API(CONTACT_MECHANICS_APPLICATION) PointRigidContactSegregatedVCondition
    : public PointRigidContactVCondition
{
public:
  ///@name Type Definitions
  ///@{
  typedef PointRigidContactVCondition BaseType;

  typedef BaseType::NodeType NodeType;
  typedef BaseType::PointType PointType;
  typedef BaseType::SizeType SizeType;
  typedef BaseType::NodeWeakPtrVectorType NodeWeakPtrVectorType;
  typedef BaseType::ElementWeakPtrVectorType ElementWeakPtrVectorType;
  typedef BaseType::ConditionWeakPtrVectorType ConditionWeakPtrVectorType;

  /// Counted pointer of PointRigidContactSegregatedVCondition
  KRATOS_CLASS_INTRUSIVE_POINTER_DEFINITION(PointRigidContactSegregatedVCondition);

  enum StepType
  {
    VELOCITY_STEP = 0,
    PRESSURE_STEP = 1
  };

  ///@}
  ///@name Life Cycle
  ///@{

  /// Serializer constructor
  PointRigidContactSegregatedVCondition() : PointRigidContactVCondition(){};

  /// Default constructors
  PointRigidContactSegregatedVCondition(IndexType NewId, GeometryType::Pointer pGeometry);

  PointRigidContactSegregatedVCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties);

  PointRigidContactSegregatedVCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties, SpatialBoundingBox::Pointer pRigidWall);

  ///Copy constructor
  PointRigidContactSegregatedVCondition(PointRigidContactSegregatedVCondition const &rOther);

  /// Destructor.
  virtual ~PointRigidContactSegregatedVCondition();

  ///@}
  ///@name Operators
  ///@{

  /**
     * creates a new element pointer
     * @param NewId: the ID of the new element
     * @param ThisNodes: the nodes of the new element
     * @param pProperties: the properties assigned to the new element
     * @return a Pointer to the new element
     */
  Condition::Pointer Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const override;

  /**
     * creates a new element pointer and clones the previous element data
     * @param NewId: the ID of the new element
     * @param ThisNodes: the nodes of the new element
     * @param pProperties: the properties assigned to the new element
     * @return a Pointer to the new element
     */
  Condition::Pointer Clone(IndexType NewId, NodesArrayType const &ThisNodes) const override;

  //************* GETTING METHODS

  /**
     * Sets on rElementalDofList the degrees of freedom of the considered element geometry
     */
  void GetDofList(DofsVectorType &rConditionDofList, const ProcessInfo &rCurrentProcessInfo) const override;

  /**
     * Sets on rResult the ID's of the element degrees of freedom
     */
  void EquationIdVector(EquationIdVectorType &rResult, const ProcessInfo &rCurrentProcessInfo) const override;

  /**
     * Sets on rValues the nodal displacements
     */
  void GetValuesVector(Vector &rValues, int Step = 0) const override;

  /**
     * Sets on rValues the nodal velocities
     */
  void GetFirstDerivativesVector(Vector &rValues, int Step = 0) const override;

  /**
     * Sets on rValues the nodal accelerations
     */
  void GetSecondDerivativesVector(Vector &rValues, int Step = 0) const override;

  //************* STARTING - ENDING  METHODS

  /**
      * Called to initialize the element.
      * Must be called before any calculation is done
      */
  void Initialize(const ProcessInfo &rCurrentProcessInfo) override;

  /**
     * Called at the beginning of each solution step
     */
  void InitializeSolutionStep(const ProcessInfo &rCurrentProcessInfo) override;

  /**
     * this is called for non-linear analysis at the beginning of the iteration process
     */
  void InitializeNonLinearIteration(const ProcessInfo &rCurrentProcessInfo) override;

  /**
     * this is called for non-linear analysis at the beginning of the iteration process
     */
  void FinalizeNonLinearIteration(const ProcessInfo &rCurrentProcessInfo) override;

  /**
     * Called at the end of eahc solution step
     */
  void FinalizeSolutionStep(const ProcessInfo &rCurrentProcessInfo) override;


  //************* COMPUTING  METHODS

  /**
     * this is called during the assembling process in order
     * to calculate all elemental contributions to the global system
     * matrix and the right hand side
     * @param rLeftHandSideMatrix: the elemental left hand side matrix
     * @param rRightHandSideVector: the elemental right hand side
     * @param rCurrentProcessInfo: the current process info instance
     */
  void CalculateLocalSystem(MatrixType &rLeftHandSideMatrix, VectorType &rRightHandSideVector, const ProcessInfo &rCurrentProcessInfo) override;

  /**
     * this is called during the assembling process in order
     * to calculate the elemental right hand side vector only
     * @param rRightHandSideVector: the elemental right hand side vector
     * @param rCurrentProcessInfo: the current process info instance
     */
  void CalculateRightHandSide(VectorType &rRightHandSideVector, const ProcessInfo &rCurrentProcessInfo) override;

  /**
     * this is called during the assembling process in order
     * to calculate the elemental left hand side vector only
     * @param rLeftHandSideVector: the elemental left hand side vector
     * @param rCurrentProcessInfo: the current process info instance
     */
  void CalculateLeftHandSide(MatrixType &rLeftHandSideMatrix, const ProcessInfo &rCurrentProcessInfo) override;

  /**
     * this is called during the assembling process in order
     * to calculate the second derivatives contributions for the LHS and RHS
     * @param rLeftHandSideMatrix: the elemental left hand side matrix
     * @param rRightHandSideVector: the elemental right hand side
     * @param rCurrentProcessInfo: the current process info instance
     */
  void CalculateSecondDerivativesContributions(MatrixType &rLeftHandSideMatrix,
                                               VectorType &rRightHandSideVector,
                                               const ProcessInfo &rCurrentProcessInfo) override;

  /**
     * this is called during the assembling process in order
     * to calculate the elemental left hand side matrix for the second derivatives constributions
     * @param rLeftHandSideMatrix: the elemental left hand side matrix
     * @param rCurrentProcessInfo: the current process info instance
     */
  void CalculateSecondDerivativesLHS(MatrixType &rLeftHandSideMatrix,
                                     const ProcessInfo &rCurrentProcessInfo) override;

  /**
     * this is called during the assembling process in order
     * to calculate the elemental right hand side vector for the second derivatives constributions
     * @param rRightHandSideVector: the elemental right hand side vector
     * @param rCurrentProcessInfo: the current process info instance
     */
  void CalculateSecondDerivativesRHS(VectorType &rRightHandSideVector,
                                     const ProcessInfo &rCurrentProcessInfo) override;


  //************************************************************************************
  //************************************************************************************
  /**
     * This function provides the place to perform checks on the completeness of the input.
     * It is designed to be called only once (or anyway, not often) typically at the beginning
     * of the calculations, so to verify that nothing is missing from the input
     * or that no common error is found.
     * @param rCurrentProcessInfo
     */
  int Check(const ProcessInfo &rCurrentProcessInfo) const override;

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{
  /// Turn back information as a string.
  std::string Info() const override
  {
    std::stringstream buffer;
    buffer << "Point rigid contact segregated condition #" << this->Id();
    return buffer.str();
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "Point rigid contact segregated condition #" << this->Id();
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
    GetGeometry().PrintData(rOStream);
  }
  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{

  StepType mStepVariable;

  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{

  /**
   * Sets process information to set member variables like mStepVariable
   */
  virtual void SetProcessInformation(const ProcessInfo &rCurrentProcessInfo);


  /**
   * Get element size from the dofs
   */
  SizeType GetDofsSize() const override;

  /**
   * Get node size from the dofs
   */
  SizeType GetNodeDofsSize() const override;


  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@}
  ///@name Serialization
  ///@{
  friend class Serializer;

  virtual void save(Serializer &rSerializer) const override;

  virtual void load(Serializer &rSerializer) override;

  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{
  ///@}

}; // Class PointRigidContactSegregatedVCondition

} // namespace Kratos.
#endif //  KRATOS_POINT_RIGID_CONTACT_SEGREGATED_V_CONDITION_HPP_INCLUDED defined
