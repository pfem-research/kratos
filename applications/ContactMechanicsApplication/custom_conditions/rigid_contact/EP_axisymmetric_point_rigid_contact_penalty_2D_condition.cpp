//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:             LMonforte $
//   Maintained by:       $Maintainer:                   LM $
//   Date:                $Date:                  July 2016 $
//
//

// System includes

// External includes

// Project includes
#include "custom_conditions/rigid_contact/EP_axisymmetric_point_rigid_contact_penalty_2D_condition.hpp"

#include "contact_mechanics_application_variables.h"

namespace Kratos
{
//************************************************************************************
//************************************************************************************
EPAxisymmetricPointRigidContactPenalty2DCondition::EPAxisymmetricPointRigidContactPenalty2DCondition(IndexType NewId, GeometryType::Pointer
                                                                                                              pGeometry)
    : EPPointRigidContactPenalty2DCondition(NewId, pGeometry)
{
   //DO NOT ADD DOFS HERE!!!
}

//************************************************************************************
//************************************************************************************
EPAxisymmetricPointRigidContactPenalty2DCondition::EPAxisymmetricPointRigidContactPenalty2DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : EPPointRigidContactPenalty2DCondition(NewId, pGeometry, pProperties)
{
}

//************************************************************************************
//************************************************************************************
EPAxisymmetricPointRigidContactPenalty2DCondition::EPAxisymmetricPointRigidContactPenalty2DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties, SpatialBoundingBox::Pointer pRigidWall)
    : EPPointRigidContactPenalty2DCondition(NewId, pGeometry, pProperties, pRigidWall)
{
}

//************************************************************************************
//************************************************************************************
EPAxisymmetricPointRigidContactPenalty2DCondition::EPAxisymmetricPointRigidContactPenalty2DCondition(EPAxisymmetricPointRigidContactPenalty2DCondition const &rOther)
    : EPPointRigidContactPenalty2DCondition(rOther)
{
}

//************************************************************************************
//************************************************************************************

Condition::Pointer EPAxisymmetricPointRigidContactPenalty2DCondition::Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const
{
   return Kratos::make_intrusive<EPAxisymmetricPointRigidContactPenalty2DCondition>(NewId, GetGeometry().Create(ThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************

Condition::Pointer EPAxisymmetricPointRigidContactPenalty2DCondition::Clone(IndexType NewId, NodesArrayType const &ThisNodes) const
{
   EPAxisymmetricPointRigidContactPenalty2DCondition ClonedCondition(NewId, GetGeometry().Create(ThisNodes), pGetProperties(), mpRigidWall);
   if (this->mpFrictionLaw!=nullptr)
     ClonedCondition.mpFrictionLaw = this->mpFrictionLaw->Clone();
   ClonedCondition.mCurrentInfo = this->mCurrentInfo;
   ClonedCondition.mSavedInfo = this->mSavedInfo;
   return Kratos::make_intrusive<EPAxisymmetricPointRigidContactPenalty2DCondition>(ClonedCondition);
}

//************************************************************************************
//************************************************************************************

EPAxisymmetricPointRigidContactPenalty2DCondition::~EPAxisymmetricPointRigidContactPenalty2DCondition()
{
}

//*********************************COMPUTE KINEMATICS*********************************
//************************************************************************************

void EPAxisymmetricPointRigidContactPenalty2DCondition::CalculateKinematics(ConditionVariables &rVariables,
                                                                      const ProcessInfo &rCurrentProcessInfo,
                                                                      const double &rPointNumber)
{
   KRATOS_TRY

   EPPointRigidContactPenalty2DCondition::CalculateKinematics(rVariables, rCurrentProcessInfo, rPointNumber);

   CalculateRadius(rVariables.CurrentRadius, rVariables.ReferenceRadius);

   KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void EPAxisymmetricPointRigidContactPenalty2DCondition::CalculateRadius(double &rCurrentRadius, double &rReferenceRadius)
{

   KRATOS_TRY

   rCurrentRadius = 0;
   rReferenceRadius = 0;

   //Displacement from the reference to the current configuration
   const array_1d<double, 3> &CurrentDisplacement = GetGeometry()[0].FastGetSolutionStepValue(DISPLACEMENT);
   const array_1d<double, 3> &PreviousDisplacement = GetGeometry()[0].FastGetSolutionStepValue(DISPLACEMENT, 1);
   array_1d<double, 3> DeltaDisplacement = CurrentDisplacement - PreviousDisplacement;
   const array_1d<double, 3> &CurrentPosition = GetGeometry()[0].Coordinates();
   array_1d<double, 3> ReferencePosition = CurrentPosition - DeltaDisplacement;

   rCurrentRadius = CurrentPosition[0];
   rReferenceRadius = ReferencePosition[0];

   if (rCurrentRadius == 0)
   {

      NodeWeakPtrVectorType &nNodes = GetGeometry()[0].GetValue(NEIGHBOUR_NODES);

      double counter = 0;

      for (auto &i_nnode : nNodes)
      {
         const array_1d<double, 3> &NodePosition = i_nnode.Coordinates();
         if (NodePosition[0] != 0)
         {
            rCurrentRadius += NodePosition[0] * 0.225;
            counter++;
         }
      }

      rCurrentRadius /= counter;
   }

   KRATOS_CATCH("")
}


//***********************************************************************************
//************************************************************************************

double & EPAxisymmetricPointRigidContactPenalty2DCondition::CalculateIntegrationWeight(ConditionVariables &rVariables, double &rIntegrationWeight)
{
  KRATOS_TRY

  rIntegrationWeight *= 2.0 * Globals::Pi * rVariables.CurrentRadius;
  rIntegrationWeight = fabs(rIntegrationWeight);
  return rIntegrationWeight;

  KRATOS_CATCH("")
}

} // Namespace Kratos
