//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:               October 2013 $
//
//

// System includes

// External includes

// Project includes
#include "includes/kratos_flags.h"
#include "custom_conditions/thermal_contact/axisymmetric_thermal_contact_domain_penalty_2D_condition.hpp"

#include "contact_mechanics_application_variables.h"

namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

AxisymmetricThermalContactDomainPenalty2DCondition::AxisymmetricThermalContactDomainPenalty2DCondition(IndexType NewId, GeometryType::Pointer pGeometry)
    : ThermalContactDomainPenalty2DCondition(NewId, pGeometry)
{
    //DO NOT ADD DOFS HERE!!!
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

AxisymmetricThermalContactDomainPenalty2DCondition::AxisymmetricThermalContactDomainPenalty2DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : ThermalContactDomainPenalty2DCondition(NewId, pGeometry, pProperties)
{
    mThisIntegrationMethod = GetGeometry().GetDefaultIntegrationMethod();
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

AxisymmetricThermalContactDomainPenalty2DCondition::AxisymmetricThermalContactDomainPenalty2DCondition(AxisymmetricThermalContactDomainPenalty2DCondition const &rOther)
    : ThermalContactDomainPenalty2DCondition(rOther)
{
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

AxisymmetricThermalContactDomainPenalty2DCondition &AxisymmetricThermalContactDomainPenalty2DCondition::operator=(AxisymmetricThermalContactDomainPenalty2DCondition const &rOther)
{
    ThermalContactDomainPenalty2DCondition::operator=(rOther);

    return *this;
}

//*********************************OPERATIONS*****************************************
//************************************************************************************

Condition::Pointer AxisymmetricThermalContactDomainPenalty2DCondition::Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const
{
    return Kratos::make_intrusive<AxisymmetricThermalContactDomainPenalty2DCondition>(NewId, GetGeometry().Create(ThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************

Condition::Pointer AxisymmetricThermalContactDomainPenalty2DCondition::Clone(IndexType NewId, NodesArrayType const &ThisNodes) const
{
    return this->Create(NewId, ThisNodes, pGetProperties());
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

AxisymmetricThermalContactDomainPenalty2DCondition::~AxisymmetricThermalContactDomainPenalty2DCondition()
{
}

//************* COMPUTING  METHODS
//************************************************************************************
//************************************************************************************

//*********************************COMPUTE RADIUS*************************************
//************************************************************************************

void AxisymmetricThermalContactDomainPenalty2DCondition::CalculateRadius(double &rCurrentRadius,
                                                                   double &rReferenceRadius,
                                                                   const Vector &rN)

{

    KRATOS_TRY

    const unsigned int number_of_nodes = GetGeometry().PointsNumber();

    unsigned int dimension = GetGeometry().WorkingSpaceDimension();

    rCurrentRadius = 0;
    rReferenceRadius = 0;

    if (dimension == 2)
    {
        for (unsigned int i = 0; i < number_of_nodes; i++)
        {
            //Displacement from the reference to the current configuration
            array_1d<double, 3> &CurrentDisplacement = GetGeometry()[i].FastGetSolutionStepValue(DISPLACEMENT);
            array_1d<double, 3> &PreviousDisplacement = GetGeometry()[i].FastGetSolutionStepValue(DISPLACEMENT, 1);
            array_1d<double, 3> DeltaDisplacement = CurrentDisplacement - PreviousDisplacement;
            array_1d<double, 3> &CurrentPosition = GetGeometry()[i].Coordinates();
            array_1d<double, 3> ReferencePosition = CurrentPosition - DeltaDisplacement;

            rCurrentRadius += CurrentPosition[0] * rN[i];
            rReferenceRadius += ReferencePosition[0] * rN[i];
            //std::cout<<" node "<<i<<" -> DeltaDisplacement : "<<DeltaDisplacement<<std::endl;
        }
    }

    if (dimension == 3)
    {
        std::cout << " AXISYMMETRIC case and 3D is not possible " << std::endl;
    }

    KRATOS_CATCH("")
}

//*********************************COMPUTE KINEMATICS*********************************
//************************************************************************************

void AxisymmetricThermalContactDomainPenalty2DCondition::CalculateKinematics(GeneralVariables &rVariables,
                                                                       const ProcessInfo &rCurrentProcessInfo,
                                                                       const unsigned int &rPointNumber)
{
    KRATOS_TRY

    //reading shape functions
    const Matrix &Ncontainer = GetGeometry().ShapeFunctionsValues(mThisIntegrationMethod);

    //Set Shape Functions Values for this integration point
    Vector N = row(Ncontainer, rPointNumber);

    //Calculate radius
    rVariables.CurrentRadius = 0;
    rVariables.ReferenceRadius = 0;
    CalculateRadius(rVariables.CurrentRadius, rVariables.ReferenceRadius, N);

    //Calculate Current Contact Projections
    this->CalcProjections(rVariables, rCurrentProcessInfo);

    KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void AxisymmetricThermalContactDomainPenalty2DCondition::CalculateAndAddLHS(MatrixType &rLeftHandSideMatrix, GeneralVariables &rVariables, double &rIntegrationWeight)
{

    double IntegrationWeight = rIntegrationWeight * 2.0 * Globals::Pi * rVariables.CurrentRadius;

    if (GetProperties()[THICKNESS] > 0)
        IntegrationWeight /= GetProperties()[THICKNESS];

    //contributions to stiffness matrix calculated on the reference config
    this->CalculateAndAddThermalKm(rLeftHandSideMatrix, rVariables, IntegrationWeight);

    //KRATOS_WATCH(rLeftHandSideMatrix)
}

//************************************************************************************
//************************************************************************************

void AxisymmetricThermalContactDomainPenalty2DCondition::CalculateAndAddRHS(VectorType &rRightHandSideVector, GeneralVariables &rVariables, double &rIntegrationWeight)
{
    double IntegrationWeight = rIntegrationWeight * 2.0 * Globals::Pi * rVariables.CurrentRadius;

    if (GetProperties()[THICKNESS] > 0)
        IntegrationWeight /= GetProperties()[THICKNESS];

    //contribution to contact forces
    this->CalculateAndAddThermalContactForces(rRightHandSideVector, rVariables, IntegrationWeight);

    //KRATOS_WATCH(rRightHandSideVector)
}

//************************************************************************************
//************************************************************************************
/**
 * This function provides the place to perform checks on the completeness of the input.
 * It is designed to be called only once (or anyway, not often) typically at the beginning
 * of the calculations, so to verify that nothing is missing from the input
 * or that no common error is found.
 * @param rCurrentProcessInfo
 */
int AxisymmetricThermalContactDomainPenalty2DCondition::Check(const ProcessInfo &rCurrentProcessInfo) const
{
    KRATOS_TRY

    return 0;

    KRATOS_CATCH("");
}

void AxisymmetricThermalContactDomainPenalty2DCondition::save(Serializer &rSerializer) const
{
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, ThermalContactDomainPenalty2DCondition);
}

void AxisymmetricThermalContactDomainPenalty2DCondition::load(Serializer &rSerializer)
{
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, ThermalContactDomainPenalty2DCondition);
}

} // Namespace Kratos
