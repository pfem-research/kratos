//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                August 2016 $
//
//

// System includes

// External includes

// Project includes
#include "custom_conditions/deformable_contact/axisymmetric_contact_domain_LM_2D_condition.hpp"

#include "contact_mechanics_application_variables.h"

namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

AxisymmetricContactDomainLM2DCondition::AxisymmetricContactDomainLM2DCondition(IndexType NewId, GeometryType::Pointer pGeometry)
    : ContactDomainLM2DCondition(NewId, pGeometry)
{
    //DO NOT ADD DOFS HERE!!!
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

AxisymmetricContactDomainLM2DCondition::AxisymmetricContactDomainLM2DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : ContactDomainLM2DCondition(NewId, pGeometry, pProperties)
{
    mThisIntegrationMethod = GetGeometry().GetDefaultIntegrationMethod();
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

AxisymmetricContactDomainLM2DCondition::AxisymmetricContactDomainLM2DCondition(AxisymmetricContactDomainLM2DCondition const &rOther)
    : ContactDomainLM2DCondition(rOther)
{
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

AxisymmetricContactDomainLM2DCondition &AxisymmetricContactDomainLM2DCondition::operator=(AxisymmetricContactDomainLM2DCondition const &rOther)
{
    ContactDomainLM2DCondition::operator=(rOther);

    return *this;
}

//*********************************OPERATIONS*****************************************
//************************************************************************************

Condition::Pointer AxisymmetricContactDomainLM2DCondition::Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const
{
    return Kratos::make_intrusive<AxisymmetricContactDomainLM2DCondition>(NewId, GetGeometry().Create(ThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************

Condition::Pointer AxisymmetricContactDomainLM2DCondition::Clone(IndexType NewId, NodesArrayType const &ThisNodes) const
{
    return this->Create(NewId, ThisNodes, pGetProperties());
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

AxisymmetricContactDomainLM2DCondition::~AxisymmetricContactDomainLM2DCondition()
{
}

//************* COMPUTING  METHODS
//************************************************************************************
//************************************************************************************

//************************************************************************************
//************************************************************************************

void AxisymmetricContactDomainLM2DCondition::InitializeConditionVariables(ConditionVariables &rVariables, const ProcessInfo &rCurrentProcessInfo)
{

    GeometryType &MasterGeometry = mContactVariables.GetMasterGeometry();

    const unsigned int number_of_nodes = MasterGeometry.size();
    const unsigned int dimension = MasterGeometry.WorkingSpaceDimension();

    unsigned int voigtsize = 4;

    rVariables.F.resize(3, 3, false);

    rVariables.ConstitutiveMatrix.resize(voigtsize, voigtsize, false);

    rVariables.StressVector.resize(voigtsize);

    rVariables.DN_DX.resize(number_of_nodes, dimension, false);

    //set variables including all integration points values

    //reading shape functions
    rVariables.SetShapeFunctions(GetGeometry().ShapeFunctionsValues(mThisIntegrationMethod));

    //reading shape functions local gradients
    rVariables.SetShapeFunctionsGradients(GetGeometry().ShapeFunctionsLocalGradients(mThisIntegrationMethod));

    // UL
    //Calculate Delta Position
    //rVariables.DeltaPosition = CalculateDeltaPosition(rVariables.DeltaPosition);

    //calculating the reference jacobian from cartesian coordinates to parent coordinates for all integration points [dx_n/d£]
    //rVariables.J = MasterGeometry.Jacobian( rVariables.J, mThisIntegrationMethod, rVariables.DeltaPosition );

    // SL
    //calculating the current jacobian from cartesian coordinates to parent coordinates for all integration points [dx_n+1/d£]
    rVariables.j = MasterGeometry.Jacobian(rVariables.j, mThisIntegrationMethod);
}

//*********************************COMPUTE RADIUS*************************************
//************************************************************************************

void AxisymmetricContactDomainLM2DCondition::CalculateRadius(double &rCurrentRadius,
                                                       double &rReferenceRadius,
                                                       const Vector &rN)

{

    KRATOS_TRY

    const unsigned int number_of_nodes = GetGeometry().PointsNumber();

    unsigned int dimension = GetGeometry().WorkingSpaceDimension();

    rCurrentRadius = 0;
    rReferenceRadius = 0;

    if (dimension == 2)
    {
        for (unsigned int i = 0; i < number_of_nodes; i++)
        {
            //Displacement from the reference to the current configuration
            const array_1d<double, 3> &CurrentDisplacement = GetGeometry()[i].FastGetSolutionStepValue(DISPLACEMENT);
            const array_1d<double, 3> &PreviousDisplacement = GetGeometry()[i].FastGetSolutionStepValue(DISPLACEMENT, 1);
            array_1d<double, 3> DeltaDisplacement = CurrentDisplacement - PreviousDisplacement;
            const array_1d<double, 3> &CurrentPosition = GetGeometry()[i].Coordinates();
            array_1d<double, 3> ReferencePosition = CurrentPosition - DeltaDisplacement;

            rCurrentRadius += CurrentPosition[0] * rN[i];
            rReferenceRadius += ReferencePosition[0] * rN[i];
            //std::cout<<" node "<<i<<" -> DeltaDisplacement : "<<DeltaDisplacement<<std::endl;
        }
    }

    if (dimension == 3)
    {
        std::cout << " AXISYMMETRIC case and 3D is not possible " << std::endl;
    }

    KRATOS_CATCH("")
}

//*********************************COMPUTE KINEMATICS*********************************
//************************************************************************************

void AxisymmetricContactDomainLM2DCondition::CalculateKinematics(ConditionVariables &rVariables, const ProcessInfo &rCurrentProcessInfo, const unsigned int &rPointNumber)
{
    KRATOS_TRY

    ElementType &MasterElement = mContactVariables.GetMasterElement();
    GeometryType &MasterGeometry = mContactVariables.GetMasterGeometry();

    //Get the parent coodinates derivative [dN/d£]
    const GeometryType::ShapeFunctionsGradientsType &DN_De = MasterGeometry.ShapeFunctionsLocalGradients(mThisIntegrationMethod);

    //Get integration points number
    unsigned int integration_points_number = MasterGeometry.IntegrationPointsNumber(MasterElement.GetIntegrationMethod());

    unsigned int voigtsize = 4;
    //int dimension = GetGeometry().WorkingSpaceDimension();

    //Get the shape functions for the order of the integration method [N]
    const Matrix &Ncontainer = rVariables.GetShapeFunctions();

    //Set Shape Functions Values for this integration point
    rVariables.N = row(Ncontainer, rPointNumber);

    //Calculate radius
    rVariables.CurrentRadius = 0;
    rVariables.ReferenceRadius = 0;
    CalculateRadius(rVariables.CurrentRadius, rVariables.ReferenceRadius, rVariables.N);

    // UL
    // //Calculating the inverse of the jacobian and the parameters needed [d£/dx_n]
    // Matrix Invj;
    // MathUtils<double>::InvertMatrix( rVariables.J[rPointNumber], InvJ, rVariables.detJ);

    // //Compute cartesian derivatives [dN/dx_n]
    // noalias( rVariables.DN_DX ) = prod( DN_De[rPointNumber] , InvJ );

    // SL
    //Calculating the inverse of the jacobian and the parameters needed [d£/dx_n+1]
    Matrix Invj;
    MathUtils<double>::InvertMatrix(rVariables.j[rPointNumber], Invj, rVariables.detJ); //overwrites detJ

    //Compute cartesian derivatives [dN/dx_n+1]
    rVariables.DN_DX = prod(DN_De[rPointNumber], Invj); //overwrites DX now is the current position dx

    //Get Current DeformationGradient
    std::vector<Matrix> DeformationGradientVector(integration_points_number);
    DeformationGradientVector[rPointNumber] = IdentityMatrix(3);
    MasterElement.CalculateOnIntegrationPoints(DEFORMATION_GRADIENT, DeformationGradientVector, rCurrentProcessInfo);
    rVariables.F = DeformationGradientVector[rPointNumber];

    //std::cout<<" F "<<rVariables.F<<std::endl;

    rVariables.detF = MathUtils<double>::Det(rVariables.F);

    //Get Current Stress
    std::vector<Vector> StressVector(integration_points_number);
    StressVector[rPointNumber] = ZeroVector(voigtsize);
    //MasterElement.CalculateOnIntegrationPoints(PK2_STRESS_VECTOR,StressVector,rCurrentProcessInfo);
    MasterElement.CalculateOnIntegrationPoints(CAUCHY_STRESS_VECTOR, StressVector, rCurrentProcessInfo);

    //std::cout<<" Stress vector "<<StressVector<<std::endl;

    // ConstitutiveLaw Constitutive;
    // for( unsigned int i=0; i<StressVector.size(); i++)
    //   {
    // 	StressVector[i] = Constitutive.TransformStresses(StressVector[i], rVariables.F, rVariables.detF, ConstitutiveLaw::StressMeasure_Cauchy, ConstitutiveLaw::StressMeasure_PK2);
    //   }

    SetContactIntegrationVariable(rVariables.StressVector, StressVector, rPointNumber);

    //std::cout<<" StressVector "<<rVariables.StressVector<<std::endl;

    //Get Current Strain
    // std::vector<Matrix> StrainTensor(integration_points_number);
    // StrainTensor[rPointNumber] = ZeroMatrix(dimension, dimension);
    // MasterElement.CalculateOnIntegrationPoints(GREEN_LAGRANGE_STRAIN_TENSOR, StrainTensor, rCurrentProcessInfo);
    // std::vector<Vector> StrainVector(integration_points_number);
    // for (unsigned int i = 1; i < integration_points_number; i++)
    // {
    //     StrainVector[i] = MathUtils<double>::StrainTensorToVector(StrainTensor[i], voigtsize);
    // }

    // SetContactIntegrationVariable(rVariables.StrainVector, StrainVector, rPointNumber);

    //Get Current Constitutive Matrix
    std::vector<Matrix> ConstitutiveMatrix(integration_points_number);
    MasterElement.CalculateOnIntegrationPoints(CONSTITUTIVE_MATRIX, ConstitutiveMatrix, rCurrentProcessInfo);
    //std::cout<<" Constitutive Matrix "<<ConstitutiveMatrix<<std::endl;
    rVariables.ConstitutiveMatrix = ConstitutiveMatrix[rPointNumber];

    //Calculate Explicit Lagrange Multipliers or Penalty Factors
    this->CalculateExplicitFactors(rVariables, rCurrentProcessInfo);

    KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void AxisymmetricContactDomainLM2DCondition::CalculateTractionVector(PointType& rTractionVector, const PointType& rNormal, unsigned int rUsePreviousConfiguration)
{
  Condition &rMasterCondition = GetValue(PRIMARY_CONDITIONS).front();

  //Get previous mechanics stored in the master node/condition
  Matrix F(3,3);
  noalias(F) = ZeroMatrix(3,3);

  Vector StressVector;
  StressVector = rMasterCondition.GetValue(CAUCHY_STRESS_VECTOR); //it means that has been stored
  F = rMasterCondition.GetValue(DEFORMATION_GRADIENT);            //it means that has been stored

  Matrix StressMatrix(3, 3);
  StressMatrix = MathUtils<double>::StressVectorToTensor(StressVector);

  if (rUsePreviousConfiguration == 1)
  {
    //Compute the tension (or traction) vector T=P*N (in the Reference configuration)
    // F here from Cn-1 to Cn
    double detF = MathUtils<double>::Det(F);
    // Compute the 1srt Piola Kirchhoff stress tensor  (P=J*CauchyStress*F^-T)
    ConstitutiveLaw ConstitutiveUtils;
    ConstitutiveUtils.TransformStresses(StressMatrix, F, detF, ConstitutiveLaw::StressMeasure_Cauchy, ConstitutiveLaw::StressMeasure_PK1);
  }

  rTractionVector = prod(StressMatrix, rNormal);  //vector T=P*N (in the Reference configuration)
}

//************************************************************************************
//************************************************************************************

void AxisymmetricContactDomainLM2DCondition::CalculateTractionVector(const ConditionVariables &rVariables, PointType& rTractionVector)
{
  Matrix StressMatrix(3, 3);
  // Compute tension vector:  (must be updated each iteration)
  StressMatrix = MathUtils<double>::StressVectorToTensor(rVariables.StressVector);

  if (mUsePreviousConfiguration)
  {
  // Compute the 1srt Piola Kirchhoff stress tensor
    ConstitutiveLaw ConstitutiveUtils;
    StressMatrix = ConstitutiveUtils.TransformStresses(StressMatrix, rVariables.F, rVariables.detF, ConstitutiveLaw::StressMeasure_Cauchy, ConstitutiveLaw::StressMeasure_PK1);

    rTractionVector = prod(StressMatrix, mContactVariables.ReferenceSurface.Normal); //vector T=P*N (in the Reference configuration)

  }
  else
  {
    rTractionVector = prod(StressMatrix, rVariables.Contact.CurrentSurface.Normal); //vector t=sigma*n (in the Current configuration)
  }
}

//***********************************************************************************
//************************************************************************************

double &AxisymmetricContactDomainLM2DCondition::CalculateIntegrationWeight(ConditionVariables &rVariables, double &rIntegrationWeight) const
{
  if (this->mUsePreviousConfiguration)
  {
    // UL (ask for the last known configuration size)
    rIntegrationWeight = rVariables.Contact.ReferenceBase[0].L * Globals::Pi * rVariables.ReferenceRadius;  //all components are multiplied by this
  }
  else{
    // SL (ask for the current configuration size)
    rIntegrationWeight = rVariables.Contact.CurrentBase[0].L * Globals::Pi * rVariables.CurrentRadius; //all components are multiplied by this
  }

  return rIntegrationWeight;
}


//************************************************************************************
//************************************************************************************

void AxisymmetricContactDomainLM2DCondition::SigmaPnd(ConditionVariables &rVariables, std::vector<Vector> &SigmaP, PointType &DirVector, unsigned int &ndi, unsigned int &ndj)
{
  //Computation with the ndi and storage to ndj
  SigmaP[ndj] = ZeroVector(3);

  //simplify nomenclature
  PointType &Normal = rVariables.Contact.CurrentSurface.Normal;

  Matrix &C = rVariables.ConstitutiveMatrix;

  Matrix D(5, 5, false);
  noalias(D) = ZeroMatrix(5, 5);

  for (unsigned int i = 0; i < 3; ++i)
  {
    D(i, 0) = C(i, 0);
    D(i, 1) = C(i, 1);
    D(i, 2) = C(i, 2);
    D(i, 3) = C(i, 3);
    D(i, 4) = C(i, 3);
  }

  D(3, 0) = C(3, 0);
  D(3, 1) = C(3, 1);
  D(3, 2) = C(3, 2);
  D(3, 3) = C(3, 3);
  D(3, 4) = C(3, 3);

  D(4, 0) = C(3, 0);
  D(4, 1) = C(3, 1);
  D(4, 2) = C(3, 2);
  D(4, 3) = C(3, 3);
  D(4, 4) = C(3, 3);

  std::vector<Vector> DB(5);

  DB[0] = ZeroVector(2);
  DB[1] = ZeroVector(2);
  DB[2] = ZeroVector(2);
  DB[3] = ZeroVector(2);
  DB[4] = ZeroVector(2);

  for (int i = 0; i < 5; i++)
  {
      DB[i][0] = D(i,0) * rVariables.DN_DX(ndi, 0) +
                 (D(i,3) + D(i,4)) * 0.5 * rVariables.DN_DX(ndi, 1);

      DB[i][1] = D(i,1) * rVariables.DN_DX(ndi, 1) +
                 (D(i,3) + D(i,4)) * 0.5 * rVariables.DN_DX(ndi, 0);

      DB[i][0] += D(i,2) * (1.0/3.0) / rVariables.ReferenceRadius;
  }

  for (int i = 0; i < 2; i++)
  {
    SigmaP[ndj][i] += DirVector[0] * Normal[0] * (DB[0][i]) +
                      DirVector[1] * Normal[1] * (DB[1][i]) +
                      DirVector[0] * Normal[1] * (DB[2][i]) +
                      DirVector[1] * Normal[0] * (DB[3][i]);
  }
}



//************************************************************************************
//************************************************************************************

void AxisymmetricContactDomainLM2DCondition::FSigmaPnd(ConditionVariables &rVariables, std::vector<Vector> &SigmaP, PointType &DirVector, unsigned int &ndi, unsigned int &ndj)
{
  //TO BE CHANGED // ALSO THE GEOMETRICAL STIFFNESS MATRIX
  //Computation with the ndi and storage to ndj
  SigmaP[ndj] = ZeroVector(2);

  //simplify nomenclature
  PointType &Normal = mContactVariables.ReferenceSurface.Normal;
  Matrix &C = rVariables.ConstitutiveMatrix;

  //part1:
  SigmaP[ndj][0] = DirVector[0] * Normal[0] * (rVariables.StressVector[0] * rVariables.DN_DX(ndi, 0) + rVariables.StressVector[3] * rVariables.DN_DX(ndi, 1)) + DirVector[0] * Normal[1] * (rVariables.StressVector[1] * rVariables.DN_DX(ndi, 1) + rVariables.StressVector[3] * rVariables.DN_DX(ndi, 0));

  SigmaP[ndj][1] = DirVector[1] * Normal[1] * (rVariables.StressVector[1] * rVariables.DN_DX(ndi, 1) + rVariables.StressVector[3] * rVariables.DN_DX(ndi, 0)) + DirVector[1] * Normal[0] * (rVariables.StressVector[0] * rVariables.DN_DX(ndi, 0) + rVariables.StressVector[3] * rVariables.DN_DX(ndi, 1));

  //part2:
  std::vector<Vector> FD(4);

  FD[0].resize(5);
  FD[0][0] = (rVariables.F(0, 0) * C(0, 0) + rVariables.F(0, 1) * C(3, 0));
  FD[0][1] = (rVariables.F(0, 0) * C(0, 1) + rVariables.F(0, 1) * C(3, 1));
  FD[0][2] = (rVariables.F(0, 0) * C(0, 3) + rVariables.F(0, 1) * C(3, 3));
  FD[0][3] = (rVariables.F(0, 0) * C(0, 3) + rVariables.F(0, 1) * C(3, 3));
  FD[0][4] =  rVariables.F(0, 0) * C(0, 2) + rVariables.F(0, 1) * C(1, 2);

  FD[1].resize(5);
  FD[1][0] = (rVariables.F(1, 1) * C(1, 0) + rVariables.F(1, 0) * C(3, 0));
  FD[1][1] = (rVariables.F(1, 1) * C(1, 1) + rVariables.F(1, 0) * C(3, 1));
  FD[1][2] = (rVariables.F(1, 1) * C(1, 3) + rVariables.F(1, 0) * C(3, 3));
  FD[1][3] = (rVariables.F(1, 1) * C(1, 3) + rVariables.F(1, 0) * C(3, 3));
  FD[1][4] =  rVariables.F(1, 1) * C(1, 2) + rVariables.F(1, 0) * C(3, 2);

  FD[2].resize(5);
  FD[2][0] = (rVariables.F(0, 1) * C(1, 0) + rVariables.F(0, 0) * C(3, 0));
  FD[2][1] = (rVariables.F(0, 1) * C(1, 1) + rVariables.F(0, 0) * C(3, 1));
  FD[2][2] = (rVariables.F(0, 1) * C(1, 3) + rVariables.F(0, 0) * C(3, 3));
  FD[2][3] = (rVariables.F(0, 1) * C(1, 3) + rVariables.F(0, 0) * C(3, 3));
  FD[2][4] =  rVariables.F(0, 1) * C(1, 2) + rVariables.F(0, 0) * C(3, 2);

  FD[3].resize(5);
  FD[3][0] = (rVariables.F(1, 0) * C(0, 0) + rVariables.F(1, 1) * C(3, 0));
  FD[3][1] = (rVariables.F(1, 0) * C(0, 1) + rVariables.F(1, 1) * C(3, 1));
  FD[3][2] = (rVariables.F(1, 0) * C(0, 3) + rVariables.F(1, 1) * C(3, 3));
  FD[3][3] = (rVariables.F(1, 0) * C(0, 3) + rVariables.F(1, 1) * C(3, 3));
  FD[3][4] =  rVariables.F(1, 0) * C(0, 2) + rVariables.F(1, 1) * C(3, 2);

  std::vector<Vector> FDB(4);

  FDB[0] = ZeroVector(2);
  FDB[1] = ZeroVector(2);
  FDB[2] = ZeroVector(2);
  FDB[3] = ZeroVector(2);

  for (int i = 0; i < 4; i++)
  {
    for (int j = 0; j < 2; j++)
    {
      FDB[i][j] = FD[i][0] * rVariables.F(j, 0) * rVariables.DN_DX(ndi, 0) + FD[i][1] * rVariables.F(j, 1) * rVariables.DN_DX(ndi, 1) +
                  (FD[i][2] + FD[i][3]) * (0.5) * (rVariables.F(j, 0) * rVariables.DN_DX(ndi, 1) + rVariables.F(j, 1) * rVariables.DN_DX(ndi, 0));
    }
    FDB[i][0] += FD[i][4] * rVariables.F(2,2) * (1.0/3.0) / rVariables.ReferenceRadius;
  }

  /*std::cout << " ....................................... " << std::endl;
  std::cout << " this " << this->Id() << std::endl;
  std::cout<<" FBD [0] "<<FDB[0]<<std::endl;
  std::cout<<" FBD [1] "<<FDB[1]<<std::endl;
  std::cout<<" FBD [2] "<<FDB[2]<<std::endl;
  std::cout<<" FBD [3] "<<FDB[3]<<std::endl;

  std::cout << " ndi " << ndi << std::endl;

  std::cout<<" FB [0] "<<FD[0]<<std::endl;
  std::cout<<" FB [1] "<<FD[1]<<std::endl;
  std::cout<<" FB [2] "<<FD[2]<<std::endl;
  std::cout<<" FB [3] "<<FD[3]<<std::endl;

  std::cout << " F " << rVariables.F << std::endl;
  std::cout << " C " << C << std::endl;
  std::cout << " radius " << rVariables.ReferenceRadius << std::endl;
  */
  for (int i = 0; i < 2; i++)
  {
    SigmaP[ndj][i] += DirVector[0] * Normal[0] * (FDB[0][i]) +
                      DirVector[1] * Normal[1] * (FDB[1][i]) +
                      DirVector[0] * Normal[1] * (FDB[2][i]) +
                      DirVector[1] * Normal[0] * (FDB[3][i]);
  }
}

//************************************************************************************
//************************************************************************************

void AxisymmetricContactDomainLM2DCondition::save(Serializer &rSerializer) const
{
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, ContactDomainLM2DCondition)
}

void AxisymmetricContactDomainLM2DCondition::load(Serializer &rSerializer)
{
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, ContactDomainLM2DCondition)
}

} // Namespace Kratos
