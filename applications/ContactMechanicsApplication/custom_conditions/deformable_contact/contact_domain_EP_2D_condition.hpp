//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                August 2016 $
//
//

#if !defined(KRATOS_CONTACT_DOMAIN_EP_2D_CONDITION_HPP_INCLUDED)
#define KRATOS_CONTACT_DOMAIN_EP_2D_CONDITION_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_conditions/deformable_contact/contact_domain_penalty_2D_condition.hpp"

namespace Kratos
{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

class KRATOS_API(CONTACT_MECHANICS_APPLICATION) ContactDomainEP2DCondition
    : public ContactDomainPenalty2DCondition
{
public:
    ///@name Type Definitions
    ///@{
    ///Reference type definition for constitutive laws
    typedef ConstitutiveLaw ConstitutiveLawType;
    ///Pointer type for constitutive laws
    typedef ConstitutiveLawType::Pointer ConstitutiveLawPointerType;
    ///Type definition for integration methods
    typedef GeometryData::IntegrationMethod IntegrationMethod;

    ///NodeType
    typedef Node<3> NodeType;
    ///Geometry Type
    typedef Geometry<NodeType> GeometryType;
    ///Element Type
    typedef Element::ElementType ElementType;

    ///Tensor order 1 definition
    typedef ContactDomainUtilities::PointType PointType;
    ///SurfaceVector
    typedef ContactDomainUtilities::SurfaceVector SurfaceVector;
    ///SurfaceScalar
    typedef ContactDomainUtilities::SurfaceScalar SurfaceScalar;
    ///BaseLengths
    typedef ContactDomainUtilities::BaseLengths BaseLengths;

    /// Counted pointer of ContactDomainEP2DCondition
    KRATOS_CLASS_INTRUSIVE_POINTER_DEFINITION(ContactDomainEP2DCondition);

    ///@}
    ///@name Life Cycle
    ///@{

    /// Default constructors.
    ContactDomainEP2DCondition() : ContactDomainPenalty2DCondition(){};

    ContactDomainEP2DCondition(IndexType NewId, GeometryType::Pointer pGeometry);

    ContactDomainEP2DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties);

    ///Copy constructor
    ContactDomainEP2DCondition(ContactDomainEP2DCondition const &rOther);

    /// Destructor.
    virtual ~ContactDomainEP2DCondition();

    ///@}
    ///@name Operators
    ///@{

    /// Assignment operator.
    ContactDomainEP2DCondition &operator=(ContactDomainEP2DCondition const &rOther);

    ///@}
    ///@name Operations
    ///@{

    /**
     * creates a new total lagrangian updated element pointer
     * @param NewId: the ID of the new element
     * @param ThisNodes: the nodes of the new element
     * @param pProperties: the properties assigned to the new element
     * @return a Pointer to the new element
     */
    Condition::Pointer Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const override;

    /**
     * clones the selected condition variables, creating a new one
     * @param NewId: the ID of the new condition
     * @param ThisNodes: the nodes of the new condition
     * @param pProperties: the properties assigned to the new condition
     * @return a Pointer to the new condition
     */
    Condition::Pointer Clone(IndexType NewId, NodesArrayType const &ThisNodes) const override;

    //************************************************************************************
    //************************************************************************************
    /**
     * This function provides the place to perform checks on the completeness of the input.
     * It is designed to be called only once (or anyway, not often) typically at the beginning
     * of the calculations, so to verify that nothing is missing from the input
     * or that no common error is found.
     * @param rCurrentProcessInfo
     */
    //int Check(const ProcessInfo &rCurrentProcessInfo);

    //std::string Info() const;

    ///@}
    ///@name Access
    ///@{
  void CalculateOnIntegrationPoints(const Variable<array_1d<double, 3> > &rVariable, std::vector<array_1d<double, 3> > &rOutput, const ProcessInfo &rCurrentProcessInfo) override;
  
  
  /**
   * Called at the end of eahc solution step
   */
  void FinalizeSolutionStep(const ProcessInfo &rCurrentProcessInfo) override;


    ///@}
    ///@name Inquiry
    ///@{
    ///@}
    ///@name Input and output
    ///@{

    /// Turn back information as a string.
    //      String Info() const override;

    /// Print information about this object.
    //      void PrintInfo(std::ostream& rOStream) const override;

    /// Print object's data.
    //      void PrintData(std::ostream& rOStream) const override;
    ///@}
    ///@name Friends
    ///@{
    ///@}

protected:
    ///@name Protected static Member Variables
    ///@{
    ///@}
    ///@name Protected member Variables
    ///@{
    ///@}
    ///@name Protected Operators
    ///@{


    /**
     * Calculation of the Contact Multipliers or Penalty Factors
     */
    void CalculateExplicitFactors(ConditionVariables &rVariables,
                                  const ProcessInfo &rCurrentProcessInfo) override;

    /**
     * Calculation of the Material Stiffness Matrix by components
     */
    void CalculateContactStiffness(double &Kcont, ConditionVariables &rVariables,
                                   unsigned int &ndi, unsigned int &ndj,
                                   unsigned int &idir, unsigned int &jdir) override;


    /**
     * Tangent Stick Force construction by components
     */
    void CalculateTangentStickForce(double &F, ConditionVariables &rVariables,
                                    unsigned int &ndi, unsigned int &idir) override;
    /**
     * Tangent Slip Force construction by components
     */
    void CalculateTangentSlipForce(double &F, ConditionVariables &rVariables,
                                   unsigned int &ndi, unsigned int &idir) override;

    double & CalculateEffectiveNormalStress( double & rNormalStress);

    ///@}
    ///@name Protected Operations
    ///@{
    ///@}
    ///@name Protected  Access
    ///@{
    ///@}
    ///@name Serialization
    ///@{
    friend class Serializer;

    void save(Serializer &rSerializer) const override;

    void load(Serializer &rSerializer) override;

    ///@}
    ///@name Protected Inquiry
    ///@{
    ///@}
    ///@name Protected LifeCycle
    ///@{
    ///@}

private:
    ///@name Private static Member Variables
    ///@{
    ///@}
    ///@name Private member Variables
    ///@{

    array_1d<double, 3> mContactStress;

    array_1d<double, 3> mEffectiveContactStress;
    
    double mCurrentTangentStress;

    double mPreviousTangentStress;

    SurfaceScalar mConstitutiveComponents;


    ///@}
    ///@name Private Operators
    ///@{
    ///@}
    ///@name Private Operations
    ///@{
    ///@}
    ///@name Private  Access
    ///@{
    ///@}
    ///@name Serialization
    ///@{
    ///@}
    ///@name Private Inquiry
    ///@{
    ///@}
    ///@name Un accessible methods
    ///@{
    ///@}

}; // Class ContactDomainEP2DCondition

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
/// input stream function
/*  inline std::istream& operator >> (std::istream& rIStream,
    ContactDomainEP2DCondition& rThis);
*/
/// output stream function
/*  inline std::ostream& operator << (std::ostream& rOStream,
    const ContactDomainEP2DCondition& rThis)
    {
    rThis.PrintInfo(rOStream);
    rOStream << std::endl;
    rThis.PrintData(rOStream);

    return rOStream;
    }*/
///@}

} // namespace Kratos.
#endif // KRATOS_CONTACT_DOMAIN_EP_2D_CONDITION_HPP_INCLUDED  defined
