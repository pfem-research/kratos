//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                August 2016 $
//
//

// System includes

// External includes

// Project includes
#include "custom_conditions/deformable_contact/axisymmetric_contact_domain_EP_2D_condition.hpp"

#include "contact_mechanics_application_variables.h"

namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

AxisymmetricContactDomainEP2DCondition::AxisymmetricContactDomainEP2DCondition(IndexType NewId, GeometryType::Pointer pGeometry)
    : ContactDomainEP2DCondition(NewId, pGeometry)
{
    //DO NOT ADD DOFS HERE!!!
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

AxisymmetricContactDomainEP2DCondition::AxisymmetricContactDomainEP2DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : ContactDomainEP2DCondition(NewId, pGeometry, pProperties)
{
    mThisIntegrationMethod = GetGeometry().GetDefaultIntegrationMethod();
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

AxisymmetricContactDomainEP2DCondition::AxisymmetricContactDomainEP2DCondition(AxisymmetricContactDomainEP2DCondition const &rOther)
    : ContactDomainEP2DCondition(rOther)
{
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

AxisymmetricContactDomainEP2DCondition &AxisymmetricContactDomainEP2DCondition::operator=(AxisymmetricContactDomainEP2DCondition const &rOther)
{
    ContactDomainEP2DCondition::operator=(rOther);

    return *this;
}

//*********************************OPERATIONS*****************************************
//************************************************************************************

Condition::Pointer AxisymmetricContactDomainEP2DCondition::Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const
{
    return Kratos::make_intrusive<AxisymmetricContactDomainEP2DCondition>(NewId, GetGeometry().Create(ThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************

Condition::Pointer AxisymmetricContactDomainEP2DCondition::Clone(IndexType NewId, NodesArrayType const &ThisNodes) const
{
    return this->Create(NewId, ThisNodes, pGetProperties());
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

AxisymmetricContactDomainEP2DCondition::~AxisymmetricContactDomainEP2DCondition()
{
}

//************* COMPUTING  METHODS
//************************************************************************************
//************************************************************************************

//************************************************************************************
//************************************************************************************

void AxisymmetricContactDomainEP2DCondition::InitializeConditionVariables(ConditionVariables &rVariables, const ProcessInfo &rCurrentProcessInfo)
{
    GeometryType &MasterGeometry = mContactVariables.GetMasterGeometry();

    const unsigned int number_of_nodes = MasterGeometry.size();
    const unsigned int dimension = MasterGeometry.WorkingSpaceDimension();

    unsigned int voigtsize = 4;

    rVariables.F.resize(dimension, dimension, false);

    rVariables.ConstitutiveMatrix.resize(voigtsize, voigtsize, false);

    rVariables.StressVector.resize(voigtsize);

    rVariables.DN_DX.resize(number_of_nodes, dimension, false);

    //set variables including all integration points values

    //reading shape functions
    rVariables.SetShapeFunctions(GetGeometry().ShapeFunctionsValues(mThisIntegrationMethod));

    //reading shape functions local gradients
    rVariables.SetShapeFunctionsGradients(GetGeometry().ShapeFunctionsLocalGradients(mThisIntegrationMethod));

    // UL
    //Calculate Delta Position
    //rVariables.DeltaPosition = CalculateDeltaPosition(rVariables.DeltaPosition);

    //calculating the reference jacobian from cartesian coordinates to parent coordinates for all integration points [dx_n/d£]
    //rVariables.J = MasterGeometry.Jacobian( rVariables.J, mThisIntegrationMethod, rVariables.DeltaPosition );

    // SL
    //calculating the current jacobian from cartesian coordinates to parent coordinates for all integration points [dx_n+1/d£]
    rVariables.j = MasterGeometry.Jacobian(rVariables.j, mThisIntegrationMethod);
}

//*********************************COMPUTE RADIUS*************************************
//************************************************************************************

void AxisymmetricContactDomainEP2DCondition::CalculateRadius(double &rCurrentRadius,
                                                            double &rReferenceRadius,
                                                            const Vector &rN)

{

    KRATOS_TRY

    const unsigned int number_of_nodes = GetGeometry().PointsNumber();

    unsigned int dimension = GetGeometry().WorkingSpaceDimension();

    rCurrentRadius = 0;
    rReferenceRadius = 0;

    if (dimension == 2)
    {
        for (unsigned int i = 0; i < number_of_nodes; i++)
        {
            //Displacement from the reference to the current configuration
            const array_1d<double, 3> &CurrentDisplacement = GetGeometry()[i].FastGetSolutionStepValue(DISPLACEMENT);
            const array_1d<double, 3> &PreviousDisplacement = GetGeometry()[i].FastGetSolutionStepValue(DISPLACEMENT, 1);
            array_1d<double, 3> DeltaDisplacement = CurrentDisplacement - PreviousDisplacement;
            const array_1d<double, 3> &CurrentPosition = GetGeometry()[i].Coordinates();
            array_1d<double, 3> ReferencePosition = CurrentPosition - DeltaDisplacement;

            rCurrentRadius += CurrentPosition[0] * rN[i];
            rReferenceRadius += ReferencePosition[0] * rN[i];
            //std::cout<<" node "<<i<<" -> DeltaDisplacement : "<<DeltaDisplacement<<std::endl;
        }
    }

    if (dimension == 3)
    {
        std::cout << " AXISYMMETRIC case and 3D is not possible " << std::endl;
    }

    KRATOS_CATCH("")
}

//*********************************COMPUTE KINEMATICS*********************************
//************************************************************************************

void AxisymmetricContactDomainEP2DCondition::CalculateKinematics(ConditionVariables &rVariables, const ProcessInfo &rCurrentProcessInfo, const unsigned int &rPointNumber)
{
    KRATOS_TRY

    ElementType &MasterElement = mContactVariables.GetMasterElement();
    GeometryType &MasterGeometry = mContactVariables.GetMasterGeometry();

    //Get the parent coodinates derivative [dN/d£]
    const GeometryType::ShapeFunctionsGradientsType &DN_De = MasterGeometry.ShapeFunctionsLocalGradients(mThisIntegrationMethod);

    //Get integration points number
    unsigned int integration_points_number = MasterGeometry.IntegrationPointsNumber(MasterElement.GetIntegrationMethod());

    unsigned int voigtsize = 4;
    int dimension = GetGeometry().WorkingSpaceDimension();

    //Get the shape functions for the order of the integration method [N]
    const Matrix &Ncontainer = rVariables.GetShapeFunctions();

    //Set Shape Functions Values for this integration point
    rVariables.N = row(Ncontainer, rPointNumber);

    //Calculate radius
    rVariables.CurrentRadius = 0;
    rVariables.ReferenceRadius = 0;
    CalculateRadius(rVariables.CurrentRadius, rVariables.ReferenceRadius, rVariables.N);

    // UL
    // //Calculating the inverse of the jacobian and the parameters needed [d£/dx_n]
    // Matrix Invj;
    // MathUtils<double>::InvertMatrix( rVariables.J[rPointNumber], InvJ, rVariables.detJ);

    // //Compute cartesian derivatives [dN/dx_n]
    // noalias( rVariables.DN_DX ) = prod( DN_De[rPointNumber] , InvJ );

    // SL
    //Calculating the inverse of the jacobian and the parameters needed [d£/dx_n+1]
    Matrix Invj;
    MathUtils<double>::InvertMatrix(rVariables.j[rPointNumber], Invj, rVariables.detJ); //overwrites detJ

    //Compute cartesian derivatives [dN/dx_n+1]
    rVariables.DN_DX = prod(DN_De[rPointNumber], Invj); //overwrites DX now is the current position dx

    //Get Current DeformationGradient
    std::vector<Matrix> DeformationGradientVector(integration_points_number);
    DeformationGradientVector[rPointNumber] = IdentityMatrix(dimension);
    MasterElement.CalculateOnIntegrationPoints(DEFORMATION_GRADIENT, DeformationGradientVector, rCurrentProcessInfo);
    rVariables.F = DeformationGradientVector[rPointNumber];

    rVariables.detF = MathUtils<double>::Det(rVariables.F);

    //Get Current Stress
    std::vector<Vector> StressVector(integration_points_number);
    StressVector[rPointNumber] = ZeroVector(voigtsize);
    //MasterElement.CalculateOnIntegrationPoints(PK2_STRESS_VECTOR,StressVector,rCurrentProcessInfo);
    MasterElement.CalculateOnIntegrationPoints(CAUCHY_STRESS_VECTOR, StressVector, rCurrentProcessInfo);

    // ConstitutiveLaw Constitutive;
    // for( unsigned int i=0; i<StressVector.size(); i++)
    //   {
    // 	StressVector[i] = Constitutive.TransformStresses(StressVector[i], rVariables.F, rVariables.detF, ConstitutiveLaw::StressMeasure_Cauchy, ConstitutiveLaw::StressMeasure_PK2);
    //   }

    SetContactIntegrationVariable(rVariables.StressVector, StressVector, rPointNumber);

    //std::cout<<" StressVector "<<rVariables.StressVector<<std::endl;

    //Get Current Strain
    std::vector<Matrix> StrainTensor(integration_points_number);
    StrainTensor[rPointNumber] = ZeroMatrix(dimension, dimension);
    MasterElement.CalculateOnIntegrationPoints(GREEN_LAGRANGE_STRAIN_TENSOR, StrainTensor, rCurrentProcessInfo);
    std::vector<Vector> StrainVector(integration_points_number);
    for (unsigned int i = 1; i < integration_points_number; i++)
    {
        StrainVector[i] = MathUtils<double>::StrainTensorToVector(StrainTensor[i], voigtsize);
    }

    SetContactIntegrationVariable(rVariables.StrainVector, StrainVector, rPointNumber);

    //Get Current Constitutive Matrix
    std::vector<Matrix> ConstitutiveMatrix(integration_points_number);
    MasterElement.CalculateOnIntegrationPoints(CONSTITUTIVE_MATRIX, ConstitutiveMatrix, rCurrentProcessInfo);

    rVariables.ConstitutiveMatrix = ConstitutiveMatrix[rPointNumber];

    //Calculate Explicit Lagrange Multipliers or Penalty Factors
    this->CalculateExplicitFactors(rVariables, rCurrentProcessInfo);

    KRATOS_CATCH("")
}

//***********************************************************************************
//************************************************************************************

double &AxisymmetricContactDomainEP2DCondition::CalculateIntegrationWeight(ConditionVariables &rVariables, double &rIntegrationWeight) const
{
  if (this->mUsePreviousConfiguration)
  {
    // UL (ask for the last known configuration size)
    rIntegrationWeight = rVariables.Contact.ReferenceBase[0].L * Globals::Pi * rVariables.ReferenceRadius;  //all components are multiplied by this
  }
  else{
    // SL (ask for the current configuration size)
    rIntegrationWeight = rVariables.Contact.CurrentBase[0].L * Globals::Pi * rVariables.CurrentRadius; //all components are multiplied by this
  }

  return rIntegrationWeight;
}

//************************************************************************************
//************************************************************************************

void AxisymmetricContactDomainEP2DCondition::save(Serializer &rSerializer) const
{
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, ContactDomainEP2DCondition)
}

void AxisymmetricContactDomainEP2DCondition::load(Serializer &rSerializer)
{
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, ContactDomainEP2DCondition)
}

} // Namespace Kratos
