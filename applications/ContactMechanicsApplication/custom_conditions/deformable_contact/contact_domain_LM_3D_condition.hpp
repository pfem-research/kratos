//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                August 2016 $
//
//

#if !defined(KRATOS_CONTACT_DOMAIN_LM_3D_CONDITION_HPP_INCLUDED)
#define KRATOS_CONTACT_DOMAIN_LM_3D_CONDITION_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_conditions/deformable_contact/contact_domain_condition.hpp"

namespace Kratos
{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

class KRATOS_API(CONTACT_MECHANICS_APPLICATION) ContactDomainLM3DCondition
    : public ContactDomainCondition
{
public:
    ///@name Type Definitions
    ///@{
    ///Reference type definition for constitutive laws
    typedef ConstitutiveLaw ConstitutiveLawType;
    ///Pointer type for constitutive laws
    typedef ConstitutiveLawType::Pointer ConstitutiveLawPointerType;
    ///Type definition for integration methods
    typedef GeometryData::IntegrationMethod IntegrationMethod;

    ///NodeType
    typedef Node<3> NodeType;
    ///Geometry Type
    typedef Geometry<NodeType> GeometryType;
    ///Element Type
    typedef Element::ElementType ElementType;

    ///Tensor order 1 definition
    typedef ContactDomainUtilities::PointType PointType;
    ///SurfaceVector
    typedef ContactDomainUtilities::SurfaceVector SurfaceVector;
    ///SurfaceScalar
    typedef ContactDomainUtilities::SurfaceScalar SurfaceScalar;
    ///BaseLengths
    typedef ContactDomainUtilities::BaseLengths BaseLengths;

    ///For 3D contact surfaces definition
    typedef ContactDomainUtilities::SurfaceBase SurfaceBase;

    /// Counted pointer of ContactDomainLM3DCondition
    KRATOS_CLASS_INTRUSIVE_POINTER_DEFINITION(ContactDomainLM3DCondition);

    ///@}
    ///@name Life Cycle
    ///@{

    /// Default constructors.
    ContactDomainLM3DCondition() : ContactDomainCondition(){};

    ContactDomainLM3DCondition(IndexType NewId, GeometryType::Pointer pGeometry);

    ContactDomainLM3DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties);

    ///Copy constructor
    ContactDomainLM3DCondition(ContactDomainLM3DCondition const &rOther);

    /// Destructor.
    virtual ~ContactDomainLM3DCondition();

    ///@}
    ///@name Operators
    ///@{

    /// Assignment operator.
    ContactDomainLM3DCondition &operator=(ContactDomainLM3DCondition const &rOther);

    ///@}
    ///@name Operations
    ///@{

    /**
     * creates a new total lagrangian updated element pointer
     * @param NewId: the ID of the new element
     * @param ThisNodes: the nodes of the new element
     * @param pProperties: the properties assigned to the new element
     * @return a Pointer to the new element
     */
    Condition::Pointer Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const override;

    /**
     * clones the selected condition variables, creating a new one
     * @param NewId: the ID of the new condition
     * @param ThisNodes: the nodes of the new condition
     * @param pProperties: the properties assigned to the new condition
     * @return a Pointer to the new condition
     */
    Condition::Pointer Clone(IndexType NewId, NodesArrayType const &ThisNodes) const override;

    //************************************************************************************
    //************************************************************************************
    /**
     * This function provides the place to perform checks on the completeness of the input.
     * It is designed to be called only once (or anyway, not often) typically at the beginning
     * of the calculations, so to verify that nothing is missing from the input
     * or that no common error is found.
     * @param rCurrentProcessInfo
     */
    //int Check(const ProcessInfo &rCurrentProcessInfo);

    //std::string Info() const;

    ///@}
    ///@name Access
    ///@{

    ///@}
    ///@name Inquiry
    ///@{
    ///@}
    ///@name Input and output
    ///@{

    /// Turn back information as a string.
    //      String Info() const override;

    /// Print information about this object.
    //      void PrintInfo(std::ostream& rOStream) const override;

    /// Print object's data.
    //      void PrintData(std::ostream& rOStream) const override;
    ///@}
    ///@name Friends
    ///@{
    ///@}

protected:
    ///@name Protected static Member Variables
    ///@{

    constexpr static bool mUsePreviousConfiguration = false;

    ///@}
    ///@name Protected member Variables
    ///@{
    ///@}
    ///@name Protected Operators
    ///@{

    /**
     * Calculation of condition traction vector
     */
    virtual void CalculateTractionVector(PointType& rTractionVector, const PointType& rNormal, unsigned int rUsePreviousConfiguration = 0);

    /**
     * Calculation of domain traction vector
     */
    virtual void CalculateTractionVector(const ConditionVariables &rVariables, PointType& rTractionVector);

    /**
     * Check and resolve the element type EDGE_TO_EDGE (EdgeType) or FaceType
     */
    void ResolveElementType();

    /**
     * Calculation of the Contact Master Nodes and Mechanical variables
     */
    void SetMasterGeometry() override;

    /**
     * Calculate Tau stabilization or Penalty factor
     */
    void CalculateContactFactor(const ProcessInfo &rCurrentProcessInfo) override;

    /**
     * Calculation of the Contact Previous Gap
     */
    void CalculatePreviousGap() override;

    /**
     * Calculation of the Contact Previous Gap EdgeType
     */
    void CalculatePreviousGapEdgeType();

    /**
     * Calculation of the Contact Previous Gap FaceType
     */
    void CalculatePreviousGapFaceType();

    /**
     * Calculation of the Contact Multipliers or Penalty Factors
     */
    void CalculateExplicitFactors(ConditionVariables &rVariables,
                                  const ProcessInfo &rCurrentProcessInfo) override;

    /**
     * Calculation of the Contact Multipliers or Penalty Factors EdgeType element
     */
    virtual void CalculateExplicitFactorsEdgeType(ConditionVariables &rVariables,
                                                  const ProcessInfo &rCurrentProcessInfo);

    /**
     * Calculation of the Contact Multipliers or Penalty Factors EdgeType element
     */
    virtual void CalculateExplicitFactorsFaceType(ConditionVariables &rVariables,
                                                  const ProcessInfo &rCurrentProcessInfo);

    /**
     * Tangent Matrix construction methods:
     */
    void CalculateDomainShapeN(ConditionVariables &rVariables) override;

    /**
     * Calculate Integration Weight:
     */
    double &CalculateIntegrationWeight(ConditionVariables &rVariables, double &rIntegrationWeight) const override;

    /**
     * Calculation of the Kuug
     */
    void CalculateAndAddKuug(MatrixType &rLeftHandSideMatrix,
                             ConditionVariables &rVariables,
                             double &rIntegrationWeight) override;

    /**
     * Calculation of Stiffness Matrix by components
     */
    void CalculateContactStiffness(double &Kcont, ConditionVariables &rVariables,
                                   unsigned int &ndi, unsigned int &ndj,
                                   unsigned int &idir, unsigned int &jdir) override;

    /**
     * Calculation of Stiffness Matrix 1 by components
     */
    void CalculateContactStiffness1(double &Kcont, ConditionVariables &rVariables,
				    unsigned int &ndi, unsigned int &ndj,
				    unsigned int &idir, unsigned int &jdir);

    /**
     * Calculation of Stiffness Matrix 2 by components
     */
    void CalculateContactStiffness2(double &Kcont, ConditionVariables &rVariables,
				    unsigned int &ndi, unsigned int &ndj,
				    unsigned int &idir, unsigned int &jdir);

    /**
     * Calculation of the Spatial Stiffness Matrix I
     */
    void CalculateContactStiffnessI(Matrix &KI, ConditionVariables &rVariables);

    /**
     * Calculation of the Spatial Stiffness Matrix II
     */
    void CalculateContactStiffnessII(Matrix &KII, ConditionVariables &rVariables);


    /**
     * Calculation of the Material Stiffness Matrix II
     */
    void CalculateContactStiffnessIIn(Matrix &KII, ConditionVariables &rVariables);


    /**
     * Normal Force construction by components
     */
    void CalculateNormalForce(double &F, ConditionVariables &rVariables,
                              unsigned int &ndi, unsigned int &idir) override;

    /**
     * Tangent Stick Force construction by components
     */
    void CalculateTangentStickForce(double &F, ConditionVariables &rVariables,
                                    unsigned int &ndi, unsigned int &idir) override;
    /**
     * Tangent Slip Force construction by components
     */
    void CalculateTangentSlipForce(double &F, ConditionVariables &rVariables,
                                   unsigned int &ndi, unsigned int &idir) override;

    ///@}
    ///@name Protected Operations
    ///@{

    bool CheckFictiousContacts(ConditionVariables &rVariables);

    PointType &CalculateCurrentTangent(PointType &rTangent) override;

    void SigmaPnd(ConditionVariables &rVariables, std::vector<Vector> &rSigmaP, PointType &rDirVector, unsigned int &ndi, unsigned int &ndj);

    void FSigmaP(ConditionVariables &rVariables, std::vector<Vector> &rSigmaP, PointType &rDirVector, std::vector<unsigned int> &ndi, std::vector<unsigned int> &ndj);

    void FSigmaPnd(ConditionVariables &rVariables, std::vector<Vector> &rSigmaP, PointType &rDirVector, unsigned int &ndi, unsigned int &ndj);

    void TransformCovariantToContravariantBase(SurfaceBase &Covariant, SurfaceBase &Contravariant);

    void CheckCovariantToContravariantBase(SurfaceBase &Covariant, SurfaceBase &Contravariant, array_1d<double, 3> &Normal);

    void CheckContactNormal(array_1d<double, 3> &CurrentNormal, array_1d<double, 3> &ReferenceNormal, std::string configuration);

    ///@}
    ///@name Protected  Access
    ///@{
    ///@}
    ///@name Serialization
    ///@{
    friend class Serializer;

    void save(Serializer &rSerializer) const override;

    void load(Serializer &rSerializer) override;

    ///@}
    ///@name Protected Inquiry
    ///@{
    ///@}
    ///@name Protected LifeCycle
    ///@{
    ///@}

private:
    ///@name Private static Member Variables
    ///@{
    ///@}
    ///@name Private member Variables
    ///@{
    ///@}
    ///@name Private Operators
    ///@{
    ///@}
    ///@name Private Operations
    ///@{
    ///@}
    ///@name Private  Access
    ///@{
    ///@}
    ///@name Serialization
    ///@{
    ///@}
    ///@name Private Inquiry
    ///@{
    ///@}
    ///@name Un accessible methods
    ///@{
    ///@}

}; // Class ContactDomainLM3DCondition

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
/// input stream function
/*  inline std::istream& operator >> (std::istream& rIStream,
    ContactDomainLM3DCondition& rThis);
*/
/// output stream function
/*  inline std::ostream& operator << (std::ostream& rOStream,
    const ContactDomainLM3DCondition& rThis)
    {
    rThis.PrintInfo(rOStream);
    rOStream << std::endl;
    rThis.PrintData(rOStream);

    return rOStream;
    }*/
///@}

} // namespace Kratos.
#endif // KRATOS_CONTACT_DOMAIN_LM_3D_CONDITION_HPP_INCLUDED  defined
