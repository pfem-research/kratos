//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                August 2016 $
//
//

#if !defined(KRATOS_AXISYMMETRIC_CONTACT_DOMAIN_LM_2D_CONDITION_HPP_INCLUDED)
#define KRATOS_AXISYMMETRIC_CONTACT_DOMAIN_LM_2D_CONDITION_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_conditions/deformable_contact/contact_domain_LM_2D_condition.hpp"

namespace Kratos
{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

class KRATOS_API(CONTACT_MECHANICS_APPLICATION) AxisymmetricContactDomainLM2DCondition
    : public ContactDomainLM2DCondition
{
public:
    ///@name Type Definitions
    ///@{
    ///Reference type definition for constitutive laws
    typedef ConstitutiveLaw ConstitutiveLawType;
    ///Pointer type for constitutive laws
    typedef ConstitutiveLawType::Pointer ConstitutiveLawPointerType;
    ///Type definition for integration methods
    typedef GeometryData::IntegrationMethod IntegrationMethod;

    ///NodeType
    typedef Node<3> NodeType;
    ///Geometry Type
    typedef Geometry<NodeType> GeometryType;
    ///Element Type
    typedef Element::ElementType ElementType;

    ///Tensor order 1 definition
    typedef ContactDomainUtilities::PointType PointType;
    ///SurfaceVector
    typedef ContactDomainUtilities::SurfaceVector SurfaceVector;
    ///SurfaceScalar
    typedef ContactDomainUtilities::SurfaceScalar SurfaceScalar;
    ///BaseLengths
    typedef ContactDomainUtilities::BaseLengths BaseLengths;

    /// Counted pointer of AxisymmetricContactDomainLM2DCondition
    KRATOS_CLASS_INTRUSIVE_POINTER_DEFINITION(AxisymmetricContactDomainLM2DCondition);

    ///@}
    ///@name Life Cycle
    ///@{

    /// Default constructors.
    AxisymmetricContactDomainLM2DCondition() : ContactDomainLM2DCondition(){};

    AxisymmetricContactDomainLM2DCondition(IndexType NewId, GeometryType::Pointer pGeometry);

    AxisymmetricContactDomainLM2DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties);

    ///Copy constructor
    AxisymmetricContactDomainLM2DCondition(AxisymmetricContactDomainLM2DCondition const &rOther);

    /// Destructor.
    virtual ~AxisymmetricContactDomainLM2DCondition();

    ///@}
    ///@name Operators
    ///@{

    /// Assignment operator.
    AxisymmetricContactDomainLM2DCondition &operator=(AxisymmetricContactDomainLM2DCondition const &rOther);

    ///@}
    ///@name Operations
    ///@{

    /**
     * creates a new total lagrangian updated element pointer
     * @param NewId: the ID of the new element
     * @param ThisNodes: the nodes of the new element
     * @param pProperties: the properties assigned to the new element
     * @return a Pointer to the new element
     */
    Condition::Pointer Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const override;

    /**
     * clones the selected condition variables, creating a new one
     * @param NewId: the ID of the new condition
     * @param ThisNodes: the nodes of the new condition
     * @param pProperties: the properties assigned to the new condition
     * @return a Pointer to the new condition
     */
    Condition::Pointer Clone(IndexType NewId, NodesArrayType const &ThisNodes) const override;

    //************************************************************************************
    //************************************************************************************
    /**
     * This function provides the place to perform checks on the completeness of the input.
     * It is designed to be called only once (or anyway, not often) typically at the beginning
     * of the calculations, so to verify that nothing is missing from the input
     * or that no common error is found.
     * @param rCurrentProcessInfo
     */
    //int Check(const ProcessInfo &rCurrentProcessInfo);

    //std::string Info() const;

    ///@}
    ///@name Access
    ///@{

    ///@}
    ///@name Inquiry
    ///@{
    ///@}
    ///@name Input and output
    ///@{

    /// Turn back information as a string.
    //      String Info() const override;

    /// Print information about this object.
    //      void PrintInfo(std::ostream& rOStream) const override;

    /// Print object's data.
    //      void PrintData(std::ostream& rOStream) const override;
    ///@}
    ///@name Friends
    ///@{
    ///@}

protected:
    ///@name Protected static Member Variables
    ///@{
    ///@}
    ///@name Protected member Variables
    ///@{
    ///@}
    ///@name Protected Operators
    ///@{

    /**
     * Calculation of condition traction vector
     */
    void CalculateTractionVector(PointType& rTractionVector, const PointType& rNormal, unsigned int rUsePreviousConfiguration = 1) override;

    /**
     * Calculation of domain traction vector
     */
    void CalculateTractionVector(const ConditionVariables &rVariables, PointType& rTractionVector) override;

    /**
     * Initialize Variables
     */
    void InitializeConditionVariables(ConditionVariables &rVariables,
                                      const ProcessInfo &rCurrentProcessInfo) override;

    /**
     * Calculate Condition Kinematics
     */
    void CalculateKinematics(ConditionVariables &rVariables,
                             const ProcessInfo &rCurrentProcessInfo,
                             const unsigned int &rPointNumber) override;

    /**
     * Calculate Radius:
     */
    void CalculateRadius(double &rCurrentRadius,
                         double &rReferenceRadius,
                         const Vector &rN);

    /**
     * Calculate Integration Weight:
     */
    double &CalculateIntegrationWeight(ConditionVariables &rVariables, double &rIntegrationWeight) const override;


    void SigmaPnd(ConditionVariables &rVariables, std::vector<Vector> &SigmaP, PointType &AuxVector, unsigned int &ndi, unsigned int &ndj) override;

    void FSigmaPnd(ConditionVariables &rVariables, std::vector<Vector> &SigmaP, PointType &AuxVector, unsigned int &ndi, unsigned int &ndj) override;

    ///@}
    ///@name Protected Operations
    ///@{
    ///@}
    ///@name Protected  Access
    ///@{
    ///@}
    ///@name Serialization
    ///@{
    friend class Serializer;

    void save(Serializer &rSerializer) const override;

    void load(Serializer &rSerializer) override;

    ///@}
    ///@name Protected Inquiry
    ///@{
    ///@}
    ///@name Protected LifeCycle
    ///@{
    ///@}

private:
    ///@name Private static Member Variables
    ///@{
    ///@}
    ///@name Private member Variables
    ///@{
    ///@}
    ///@name Private Operators
    ///@{
    ///@}
    ///@name Private Operations
    ///@{
    ///@}
    ///@name Private  Access
    ///@{
    ///@}
    ///@name Serialization
    ///@{
    ///@}
    ///@name Private Inquiry
    ///@{
    ///@}
    ///@name Un accessible methods
    ///@{
    ///@}

}; // Class AxisymmetricContactDomainLM2DCondition

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
/// input stream function
/*  inline std::istream& operator >> (std::istream& rIStream,
    ContactDomainLM2DCondition& rThis);
*/
/// output stream function
/*  inline std::ostream& operator << (std::ostream& rOStream,
    const ContactDomainLM2DCondition& rThis)
    {
    rThis.PrintInfo(rOStream);
    rOStream << std::endl;
    rThis.PrintData(rOStream);

    return rOStream;
    }*/
///@}

} // namespace Kratos.
#endif // KRATOS_AXISYMMETRIC_CONTACT_DOMAIN_LM_2D_CONDITION_HPP_INCLUDED  defined
