//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:              December 2019 $
//
//

// System includes

// External includes

// Project includes
#include "custom_conditions/deformable_contact/contact_domain_penalty_3D_condition.hpp"

#include "contact_mechanics_application_variables.h"

namespace Kratos
{

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

ContactDomainPenalty3DCondition::ContactDomainPenalty3DCondition(IndexType NewId, GeometryType::Pointer pGeometry)
    : ContactDomainLM3DCondition(NewId, pGeometry)
{
  //DO NOT ADD DOFS HERE!!!
}

//******************************CONSTRUCTOR*******************************************
//************************************************************************************

ContactDomainPenalty3DCondition::ContactDomainPenalty3DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : ContactDomainLM3DCondition(NewId, pGeometry, pProperties)
{
  mThisIntegrationMethod = GetGeometry().GetDefaultIntegrationMethod();
}

//******************************COPY CONSTRUCTOR**************************************
//************************************************************************************

ContactDomainPenalty3DCondition::ContactDomainPenalty3DCondition(ContactDomainPenalty3DCondition const &rOther)
    : ContactDomainLM3DCondition(rOther)
{
}

//*******************************ASSIGMENT OPERATOR***********************************
//************************************************************************************

ContactDomainPenalty3DCondition &ContactDomainPenalty3DCondition::operator=(ContactDomainPenalty3DCondition const &rOther)
{
  ContactDomainLM3DCondition::operator=(rOther);

  return *this;
}

//*********************************OPERATIONS*****************************************
//************************************************************************************

Condition::Pointer ContactDomainPenalty3DCondition::Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const
{
  return Kratos::make_intrusive<ContactDomainPenalty3DCondition>(NewId, GetGeometry().Create(ThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************

Condition::Pointer ContactDomainPenalty3DCondition::Clone(IndexType NewId, NodesArrayType const &ThisNodes) const
{
  return this->Create(NewId, ThisNodes, pGetProperties());
}

//*******************************DESTRUCTOR*******************************************
//************************************************************************************

ContactDomainPenalty3DCondition::~ContactDomainPenalty3DCondition()
{
}

//************* STARTING - ENDING  METHODS
//************************************************************************************
//************************************************************************************

//**********************************COMPUTE PENALTY FACTOR****************************
//************************************************************************************

void ContactDomainPenalty3DCondition::CalculateContactFactor(const ProcessInfo &rCurrentProcessInfo)
{
  //Initilialize penalty parameter
  double penalty_parameter = 1000;
  penalty_parameter = GetProperties()[PENALTY_PARAMETER];

  ElementType &rMasterElement = mContactVariables.GetMasterElement();

  //Look at the nodes, get the slave and get the Emin

  //Contact face segment node1-node2
  unsigned int slave = mContactVariables.slaves.front();

  const Properties &SlaveProperties = GetGeometry()[slave].GetValue(NEIGHBOUR_ELEMENTS).front().GetProperties();
  const Properties &MasterProperties = rMasterElement.GetProperties();
  double Eslave = 1e9;
  if (SlaveProperties.Has(YOUNG_MODULUS))
  {
    Eslave = SlaveProperties[YOUNG_MODULUS];
  }
  else if (SlaveProperties.Has(C10))
  {
    Eslave = SlaveProperties[C10];
  }

  double Emaster = 1e9;
  if (MasterProperties.Has(YOUNG_MODULUS))
  {
    Emaster = MasterProperties[YOUNG_MODULUS];
  }
  else if (MasterProperties.Has(C10))
  {
    Emaster = MasterProperties[C10];
  }

  //STANDARD OPTION
  if (Emaster > Eslave)
    Emaster = Eslave;

  mContactVariables.PenaltyFactor = 0.5 * penalty_parameter * Emaster;

  // mContactVariables.PenaltyParameter = 0.5 / mContactVariables.StabilizationFactor ;
}

//************* COMPUTING  METHODS
//************************************************************************************
//************************************************************************************

//****************************CALCULATE EXPLICIT PENALTY PARAMETERS*******************
//************************************************************************************

void ContactDomainPenalty3DCondition::CalculateExplicitFactorsFaceType(ConditionVariables &rVariables, const ProcessInfo &rCurrentProcessInfo)
{

  //Contact face node1-node2-node3
  unsigned int node1 = mContactVariables.nodes[0];
  unsigned int node2 = mContactVariables.nodes[1];
  unsigned int node3 = mContactVariables.nodes[2];
  unsigned int slave = mContactVariables.slaves.front();

  //1.- Compute the Current Normal and Tangent

  //Current Position
  PointType P1 = GetGeometry()[node1].Coordinates();
  PointType P2 = GetGeometry()[node2].Coordinates();
  PointType P3 = GetGeometry()[node3].Coordinates();
  PointType PS = GetGeometry()[slave].Coordinates();

  //Set Current Tangent
  rVariables.Contact.Tangent.CovariantBase.DirectionA = P2 - P1;
  rVariables.Contact.Tangent.CovariantBase.DirectionB = P3 - P1;

  TransformCovariantToContravariantBase(rVariables.Contact.Tangent.CovariantBase, rVariables.Contact.Tangent.ContravariantBase);

  //Compute Current Normal
  rVariables.Contact.CurrentSurface.Normal = mContactUtilities.CalculateSurfaceNormal(rVariables.Contact.CurrentSurface.Normal, rVariables.Contact.Tangent.CovariantBase.DirectionA, rVariables.Contact.Tangent.CovariantBase.DirectionB);

  //check normals
  CheckContactNormal(rVariables.Contact.CurrentSurface.Normal,mContactVariables.ReferenceSurface.Normal, "n+1");

  //check covariant and contravariant equivalence
  //CheckCovariantToContravariantBase(rVariables.Contact.Tangent.CovariantBase, rVariables.Contact.Tangent.ContravariantBase, rVariables.Contact.CurrentSurface.Normal);

  //4.- Compute Effective Gaps: (g^eff=g_n3+2*Tau*tn=2*Tau*Multiplier.Normal)

  //Reference normal: n_n,t_n  -> mContactVariables.ReferenceSurface.Normal / rVariables.Contact.Tangent
  //Current normal:   n,t      -> rVariables.Contact.CurrentSurface.Normal /  rVariables.Contact.CurrentSurface.Tangent

  //e.- Compute A_n,B_n,L_n
  rVariables.Contact.ReferenceBase.resize(3);
  rVariables.Contact.CurrentBase.resize(3);

  //a, b, l:
  mContactUtilities.CalculateBaseDistances(rVariables.Contact.CurrentBase, P1, P2, P3, PS, rVariables.Contact.CurrentSurface.Normal);
  mContactUtilities.CalculateBaseArea(rVariables.Contact.Tangent.CurrentArea, rVariables.Contact.CurrentBase[0].L, rVariables.Contact.CurrentBase[1].L, rVariables.Contact.CurrentBase[2].L);

  //A, B, L:

  //Reference Position
  P1 -= (GetGeometry()[node1].FastGetSolutionStepValue(DISPLACEMENT) - GetGeometry()[node1].FastGetSolutionStepValue(DISPLACEMENT, 1));
  P2 -= (GetGeometry()[node2].FastGetSolutionStepValue(DISPLACEMENT) - GetGeometry()[node2].FastGetSolutionStepValue(DISPLACEMENT, 1));
  P3 -= (GetGeometry()[node3].FastGetSolutionStepValue(DISPLACEMENT) - GetGeometry()[node3].FastGetSolutionStepValue(DISPLACEMENT, 1));
  PS -= (GetGeometry()[slave].FastGetSolutionStepValue(DISPLACEMENT) - GetGeometry()[slave].FastGetSolutionStepValue(DISPLACEMENT, 1));

  mContactUtilities.CalculateBaseDistances(rVariables.Contact.ReferenceBase, P1, P2, P3, PS, mContactVariables.ReferenceSurface.Normal);

  // std::cout<<" l1 :"<<rVariables.Contact.CurrentBase[0].L<<" a :"<<rVariables.Contact.CurrentBase[0].A<<" b :"<<rVariables.Contact.CurrentBase[0].B<<std::endl;
  // std::cout<<" l2 :"<<rVariables.Contact.CurrentBase[1].L<<" a :"<<rVariables.Contact.CurrentBase[1].A<<" b :"<<rVariables.Contact.CurrentBase[1].B<<std::endl;
  // std::cout<<" l3 :"<<rVariables.Contact.CurrentBase[2].L<<" a :"<<rVariables.Contact.CurrentBase[2].A<<" b :"<<rVariables.Contact.CurrentBase[2].B<<std::endl;

  // std::cout<<" L1 :"<<rVariables.Contact.ReferenceBase[0].L<<" A :"<<rVariables.Contact.ReferenceBase[0].A<<" B :"<<rVariables.Contact.ReferenceBase[0].B<<std::endl;
  // std::cout<<" L2 :"<<rVariables.Contact.ReferenceBase[1].L<<" A :"<<rVariables.Contact.ReferenceBase[1].A<<" B :"<<rVariables.Contact.ReferenceBase[1].B<<std::endl;
  // std::cout<<" L3 :"<<rVariables.Contact.ReferenceBase[2].L<<" A :"<<rVariables.Contact.ReferenceBase[2].A<<" B :"<<rVariables.Contact.ReferenceBase[2].B<<std::endl;

  //reference integration weight update
  mContactUtilities.CalculateBaseArea(rVariables.Contact.Tangent.ReferenceArea, rVariables.Contact.ReferenceBase[0].L, rVariables.Contact.ReferenceBase[1].L, rVariables.Contact.ReferenceBase[2].L);

  rVariables.Contact.Tangent.FactorArea = sqrt(rVariables.Contact.Tangent.ReferenceArea);

  //complete the computation of the contact factor
  rVariables.Contact.ContactFactor.Normal = mContactVariables.PenaltyFactor;
  rVariables.Contact.ContactFactor.Tangent = rVariables.Contact.ContactFactor.Normal * GetProperties()[TANGENTIAL_PENALTY_RATIO];

  //f.-obtain the (g_N)3 and (g_T)3 for the n configuration
  double ReferenceGapN = inner_prod((PS - P1), mContactVariables.ReferenceSurface.Normal);

  double H = ReferenceGapN;

  //covariant gap
  double ReferenceGapcvTA = ReferenceGapN * inner_prod(rVariables.Contact.Tangent.CovariantBase.DirectionA, mContactVariables.ReferenceSurface.Normal);
  double ReferenceGapcvTB = ReferenceGapN * inner_prod(rVariables.Contact.Tangent.CovariantBase.DirectionB, mContactVariables.ReferenceSurface.Normal);

  //contravariant gap
  double ReferenceGapcnTA = ReferenceGapN * inner_prod(rVariables.Contact.Tangent.ContravariantBase.DirectionA, mContactVariables.ReferenceSurface.Normal);
  double ReferenceGapcnTB = ReferenceGapN * inner_prod(rVariables.Contact.Tangent.ContravariantBase.DirectionB, mContactVariables.ReferenceSurface.Normal);

  PointType D1 = GetGeometry()[node1].FastGetSolutionStepValue(DISPLACEMENT) - GetGeometry()[node1].FastGetSolutionStepValue(DISPLACEMENT, 1);
  PointType D2 = GetGeometry()[node2].FastGetSolutionStepValue(DISPLACEMENT) - GetGeometry()[node2].FastGetSolutionStepValue(DISPLACEMENT, 1);
  PointType D3 = GetGeometry()[node3].FastGetSolutionStepValue(DISPLACEMENT) - GetGeometry()[node3].FastGetSolutionStepValue(DISPLACEMENT, 1);
  PointType DS = GetGeometry()[slave].FastGetSolutionStepValue(DISPLACEMENT) - GetGeometry()[slave].FastGetSolutionStepValue(DISPLACEMENT, 1);

  //(g_N)3
  ReferenceGapN *= inner_prod(rVariables.Contact.CurrentSurface.Normal, mContactVariables.ReferenceSurface.Normal);
  ReferenceGapN += inner_prod(rVariables.Contact.CurrentSurface.Normal, (D1 * (-rVariables.Contact.ReferenceBase[0].A / rVariables.Contact.ReferenceBase[0].L)));
  ReferenceGapN += inner_prod(rVariables.Contact.CurrentSurface.Normal, (D2 * (-rVariables.Contact.ReferenceBase[1].A / rVariables.Contact.ReferenceBase[1].L)));
  ReferenceGapN += inner_prod(rVariables.Contact.CurrentSurface.Normal, (D3 * (-rVariables.Contact.ReferenceBase[2].A / rVariables.Contact.ReferenceBase[2].L)));
  ReferenceGapN += inner_prod(rVariables.Contact.CurrentSurface.Normal, DS);

  //(g_cvTA)3
  ReferenceGapcvTA += inner_prod(rVariables.Contact.Tangent.CovariantBase.DirectionA, (D1 * (-rVariables.Contact.ReferenceBase[0].A / rVariables.Contact.ReferenceBase[0].L)));
  ReferenceGapcvTA += inner_prod(rVariables.Contact.Tangent.CovariantBase.DirectionA, (D2 * (-rVariables.Contact.ReferenceBase[1].A / rVariables.Contact.ReferenceBase[1].L)));
  ReferenceGapcvTA += inner_prod(rVariables.Contact.Tangent.CovariantBase.DirectionA, (D3 * (-rVariables.Contact.ReferenceBase[2].A / rVariables.Contact.ReferenceBase[2].L)));
  ReferenceGapcvTA += inner_prod(rVariables.Contact.Tangent.CovariantBase.DirectionA, DS);

  //(g_cvTB)3
  ReferenceGapcvTB += inner_prod(rVariables.Contact.Tangent.CovariantBase.DirectionB, (D1 * (-rVariables.Contact.ReferenceBase[0].A / rVariables.Contact.ReferenceBase[0].L)));
  ReferenceGapcvTB += inner_prod(rVariables.Contact.Tangent.CovariantBase.DirectionB, (D2 * (-rVariables.Contact.ReferenceBase[1].A / rVariables.Contact.ReferenceBase[1].L)));
  ReferenceGapcvTB += inner_prod(rVariables.Contact.Tangent.CovariantBase.DirectionB, (D3 * (-rVariables.Contact.ReferenceBase[2].A / rVariables.Contact.ReferenceBase[2].L)));
  ReferenceGapcvTB += inner_prod(rVariables.Contact.Tangent.CovariantBase.DirectionB, DS);

  //(g_cnTA)3
  ReferenceGapcnTA += inner_prod(rVariables.Contact.Tangent.ContravariantBase.DirectionA, (D1 * (-rVariables.Contact.ReferenceBase[0].A / rVariables.Contact.ReferenceBase[0].L)));
  ReferenceGapcnTA += inner_prod(rVariables.Contact.Tangent.ContravariantBase.DirectionA, (D2 * (-rVariables.Contact.ReferenceBase[1].A / rVariables.Contact.ReferenceBase[1].L)));
  ReferenceGapcnTA += inner_prod(rVariables.Contact.Tangent.ContravariantBase.DirectionA, (D3 * (-rVariables.Contact.ReferenceBase[2].A / rVariables.Contact.ReferenceBase[2].L)));
  ReferenceGapcnTA += inner_prod(rVariables.Contact.Tangent.ContravariantBase.DirectionA, DS);

  //(g_cnTB)3
  ReferenceGapcnTB += inner_prod(rVariables.Contact.Tangent.ContravariantBase.DirectionB, (D1 * (-rVariables.Contact.ReferenceBase[0].A / rVariables.Contact.ReferenceBase[0].L)));
  ReferenceGapcnTB += inner_prod(rVariables.Contact.Tangent.ContravariantBase.DirectionB, (D2 * (-rVariables.Contact.ReferenceBase[1].A / rVariables.Contact.ReferenceBase[1].L)));
  ReferenceGapcnTB += inner_prod(rVariables.Contact.Tangent.ContravariantBase.DirectionB, (D3 * (-rVariables.Contact.ReferenceBase[2].A / rVariables.Contact.ReferenceBase[2].L)));
  ReferenceGapcnTB += inner_prod(rVariables.Contact.Tangent.ContravariantBase.DirectionB, DS);

  rVariables.Contact.Tangent.EquivalentHeigh = 1;

  //...

  rVariables.Contact.CurrentGap.Normal = ReferenceGapN; //(g_N)3 -- needed in the Kcont1 computation

  rVariables.Contact.Tangent.A.CurrentGap.Covariant = ReferenceGapcvTA;     //(g_cvTA)3 -- needed in the Kcont1 computation
  rVariables.Contact.Tangent.A.CurrentGap.Contravariant = ReferenceGapcnTA; //(g_cvTB)3 -- needed in the Kcont1 computation

  rVariables.Contact.Tangent.B.CurrentGap.Covariant = ReferenceGapcvTB;     //(g_cnTA)3 -- needed in the Kcont1 computation
  rVariables.Contact.Tangent.B.CurrentGap.Contravariant = ReferenceGapcnTB; //(g_cnTB)3 -- needed in the Kcont1 computation

  //g.- get total effective gap as: gap_n^eff=gap_n+(PreviousTimeStep/CurrentTimeStep)*(gap_n-gap_n-1)

  //(1/(3*Tau)) is now ContactFactor.Normal, the penalty parameter
  rVariables.Contact.Penalty.Normal = rVariables.Contact.CurrentGap.Normal * rVariables.Contact.ContactFactor.Normal;

  //i.- Compute tangent component of the tension vector:  (tt=cvt·P·N) PREVIOUS CONFIGURATION: (CalculatePreviousGap needed)
  rVariables.Contact.Tangent.A.Penalty = 0.0; //inner_prod(mContactVariables.Tangent.CovariantBase.DirectionA, mContactVariables.TractionVector);  //(TractionVector is the stress in the master element)
  rVariables.Contact.Tangent.B.Penalty = 0.0; //inner_prod(mContactVariables.Tangent.CovariantBase.DirectionB, mContactVariables.TractionVector);  //(TractionVector is the stress in the master element)

  // rVariables.Contact.Tangent.A.CurrentGap.Covariant *= H;
  // rVariables.Contact.Tangent.A.CurrentGap.Covariant *= H;
  // rVariables.Contact.Tangent.A.CurrentGap.Contravariant *= H;
  // rVariables.Contact.Tangent.B.CurrentGap.Contravariant *= H;

  rVariables.Contact.Tangent.A.Penalty += rVariables.Contact.ContactFactor.Tangent * rVariables.Contact.Tangent.A.CurrentGap.Covariant;
  rVariables.Contact.Tangent.B.Penalty += rVariables.Contact.ContactFactor.Tangent * rVariables.Contact.Tangent.B.CurrentGap.Covariant;

  //From effective gaps set active contact domain:

  double EffectiveGapN = ReferenceGapN;

  double EffectiveGapcvTA = ReferenceGapcvTA;
  double EffectiveGapcvTB = ReferenceGapcvTB;

  double EffectiveGapcnTA = ReferenceGapcnTA;
  double EffectiveGapcnTB = ReferenceGapcnTB;

  double CurrentTimeStep = rCurrentProcessInfo[DELTA_TIME];
  const ProcessInfo &rPreviousProcessInfo = rCurrentProcessInfo.GetPreviousSolutionStepInfo();
  double PreviousTimeStep = rPreviousProcessInfo[DELTA_TIME];

  if (mContactVariables.PreviousGap.Normal != 0 && mContactVariables.IterationCounter < 1)
  {
    EffectiveGapN += (CurrentTimeStep / PreviousTimeStep) * (ReferenceGapN - mContactVariables.PreviousGap.Normal);
  }

  if (mContactVariables.Tangent.PreviousGapA.Covariant != 0 && mContactVariables.IterationCounter < 1)
  {
    EffectiveGapcvTA += (CurrentTimeStep / PreviousTimeStep) * (ReferenceGapcvTA - mContactVariables.Tangent.PreviousGapA.Covariant);
  }

  if (mContactVariables.Tangent.PreviousGapB.Covariant != 0 && mContactVariables.IterationCounter < 1)
  {
    EffectiveGapcvTB += (CurrentTimeStep / PreviousTimeStep) * (ReferenceGapcvTB - mContactVariables.Tangent.PreviousGapB.Covariant);
  }

  if (mContactVariables.Tangent.PreviousGapA.Contravariant != 0 && mContactVariables.IterationCounter < 1)
  {
    EffectiveGapcnTA += (CurrentTimeStep / PreviousTimeStep) * (ReferenceGapcnTA - mContactVariables.Tangent.PreviousGapA.Contravariant);
  }

  if (mContactVariables.Tangent.PreviousGapB.Contravariant != 0 && mContactVariables.IterationCounter < 1)
  {
    EffectiveGapcnTB += (CurrentTimeStep / PreviousTimeStep) * (ReferenceGapcnTB - mContactVariables.Tangent.PreviousGapB.Contravariant);
  }

  //CHECK IF THE ELEMENT IS ACTIVE:

  rVariables.Contact.Options.Set(SLIP, false);

  //CORRECTION: to skip tip contact elements problems:

  bool check_fictious_geometry = false;

  //Check ORTHOGONAL FACES in contact
  if (check_fictious_geometry == true)
  {
    PointType &SlaveNormal = GetGeometry()[slave].FastGetSolutionStepValue(NORMAL);
    double orthogonal = inner_prod(SlaveNormal, rVariables.Contact.CurrentSurface.Normal);

    if (EffectiveGapN <= 0 && fabs(orthogonal) <= 1)
    {

      bool real_contact = CheckFictiousContacts(rVariables);

      if (!real_contact || fabs(orthogonal) <= 0.25)
      {
        EffectiveGapN = 1; //not active element: geometrically wrong
        std::cout << " DISABLE ContactElement " << this->Id() << " real contact " << real_contact << " geometrically orthogonal " << orthogonal << std::endl;
      }
    }
  }
  //Check ORTHOGONAL FACES in contact

  //decimal correction from tension vector calculation
  // if(fabs(EffectiveGapN)<= 1e-20 && fabs(EffectiveGapN)<= 1e-20)
  //   EffectiveGapN = 0;
  //decimal correction from tension vector calculation

  // std::cout<<" PreviousGapN  "<< mContactVariables.PreviousGap.Normal <<std::endl;
  // std::cout<<" ReferenceGapN "<< ReferenceGapN <<std::endl;
  // std::cout<<" EffectiveGapN "<< EffectiveGapN <<std::endl;
  // std::cout<<" CurrentTensil "<< rVariables.Contact.CurrentTensil.Normal <<std::endl;
  if (EffectiveGapN <= 0) //if(EffectiveGap<0){
  {

    rVariables.Contact.Options.Set(ACTIVE, true); //normal contact active

    //Initialize friction parameter
    rVariables.Contact.FrictionCoefficient = 0;

    if ((fabs(EffectiveGapcvTA + EffectiveGapcvTB) <= rVariables.Contact.FrictionCoefficient * fabs(EffectiveGapN)) ||
        (fabs(EffectiveGapcnTA + EffectiveGapcnTB) <= rVariables.Contact.FrictionCoefficient * fabs(EffectiveGapN)))
    {
      rVariables.Contact.Options.Set(SLIP, false); //contact stick case active
    }
    else
    {

      rVariables.Contact.Options.Set(SLIP, true); //contact slip  case active
    }
  }
  else
  {
    rVariables.Contact.Options.Set(ACTIVE, false); //normal contact not active
  }

  //temporary
  rVariables.Contact.Options.Set(SLIP, false); //impose stick

  //From total current gap compute multipliers:

  if (rVariables.Contact.Tangent.A.Penalty < 0) //add the sign of the penalty
  {
    rVariables.Contact.Tangent.A.GapSign *= (-1);
  }

  if (rVariables.Contact.Tangent.B.Penalty < 0) //add the sign of the penalty
  {
    rVariables.Contact.Tangent.B.GapSign *= (-1);
  }

  if (H == 0)
  {
    rVariables.Contact.Tangent.A.GapSign = 0;
    rVariables.Contact.Tangent.B.GapSign = 0;
  }

  //check for distorted patches
  if (rVariables.Contact.Options.Is(ACTIVE))
  {

    double distorted_0 = 1, distorted_1 = 1, distorted_2 = 1, distorted_3 = 1;
    distorted_0 = fabs((-rVariables.Contact.ReferenceBase[0].A / rVariables.Contact.ReferenceBase[0].L));
    distorted_1 = fabs((-rVariables.Contact.ReferenceBase[1].A / rVariables.Contact.ReferenceBase[1].L));
    distorted_2 = fabs((-rVariables.Contact.ReferenceBase[2].A / rVariables.Contact.ReferenceBase[2].L));
    distorted_3 = 1;

    double dist = 1.01 + fabs(rVariables.Contact.CurrentGap.Normal); //1e12;

    if (distorted_0 > dist || distorted_1 > dist || distorted_2 > dist || distorted_3 > dist)
    {
      // Domain overlap check is needed to activate or deactivate elements
      // rVariables.Contact.Options.Set(ACTIVE, false);
      // std::cout << " DISTORTED FACE ELEMENT " << this->Id() << " : (d0=" << distorted_0 << ",d1=" << distorted_1 << ",d2=" << distorted_2 << ",d3=" << distorted_3 << ") " << std::endl;
    }
  }

  // set contact normal
  GetGeometry()[slave].SetLock();
  array_1d<double, 3> &ContactNormal = GetGeometry()[slave].FastGetSolutionStepValue(CONTACT_NORMAL);
  ContactNormal += rVariables.Contact.CurrentSurface.Normal;
  double modulus = norm_2(ContactNormal);
  if (modulus!=0)
    ContactNormal/=modulus;
  else
    std::cout<<" Surface normal "<<rVariables.Contact.CurrentSurface.Normal;
  GetGeometry()[slave].UnSetLock();

}

//************************************************************************************
//************************************************************************************

void ContactDomainPenalty3DCondition::CalculateExplicitFactorsEdgeType(ConditionVariables &rVariables, const ProcessInfo &rCurrentProcessInfo)
{

  //Contact face node1-node2-node3
  unsigned int node1 = mContactVariables.nodes[0];
  unsigned int node2 = mContactVariables.nodes[1];
  unsigned int slave1 = mContactVariables.slaves[0];
  unsigned int slave2 = mContactVariables.slaves[1];

  //1.- Compute the Current Normal and Tangent

  //Current Position
  PointType P1 = GetGeometry()[node1].Coordinates();
  PointType P2 = GetGeometry()[node2].Coordinates();
  PointType PS1 = GetGeometry()[slave1].Coordinates();
  PointType PS2 = GetGeometry()[slave2].Coordinates();

  //Set Current Tangent
  rVariables.Contact.Tangent.CovariantBase.DirectionA = P2 - P1;
  rVariables.Contact.Tangent.CovariantBase.DirectionB = PS1 - PS2;

  TransformCovariantToContravariantBase(rVariables.Contact.Tangent.CovariantBase, rVariables.Contact.Tangent.ContravariantBase);

  //Compute Current Normal
  rVariables.Contact.CurrentSurface.Normal = mContactUtilities.CalculateSurfaceNormal(rVariables.Contact.CurrentSurface.Normal, rVariables.Contact.Tangent.CovariantBase.DirectionA, rVariables.Contact.Tangent.CovariantBase.DirectionB);

  //check normals
  //CheckContactNormal(rVariables.Contact.CurrentSurface.Normal,mContactVariables.ReferenceSurface.Normal);

  //check covariant and contravariant equivalence
  //CheckCovariantToContravariantBase(rVariables.Contact.Tangent.CovariantBase, rVariables.Contact.Tangent.ContravariantBase, rVariables.Contact.CurrentSurface.Normal);

  //4.- Compute Effective Gaps: (g^eff=g_n3+2*Tau*tn=2*Tau*Multiplier.Normal)

  //Reference normal: n_n,t_n  -> mContactVariables.ReferenceSurface.Normal / rVariables.Contact.Tangent
  //Current normal:   n,t      -> rVariables.Contact.CurrentSurface.Normal /  rVariables.Contact.CurrentSurface.Tangent

  //e.- Compute A_n,B_n,L_n
  rVariables.Contact.ReferenceBase.resize(2);
  rVariables.Contact.CurrentBase.resize(2);

  //a, b, l:
  mContactUtilities.CalculateEdgeDistances(rVariables.Contact.CurrentBase, P1, P2, PS1, PS2, rVariables.Contact.CurrentSurface.Normal);
  PointType NormalDirection;
  MathUtils<double>::CrossProduct(NormalDirection, rVariables.Contact.Tangent.CovariantBase.DirectionA, rVariables.Contact.Tangent.CovariantBase.DirectionB);
  rVariables.Contact.Tangent.CurrentArea = 0.5 * norm_2(NormalDirection);

  //A, B, L:

  //Reference Position
  P1 -= (GetGeometry()[node1].FastGetSolutionStepValue(DISPLACEMENT) - GetGeometry()[node1].FastGetSolutionStepValue(DISPLACEMENT, 1));
  P2 -= (GetGeometry()[node2].FastGetSolutionStepValue(DISPLACEMENT) - GetGeometry()[node2].FastGetSolutionStepValue(DISPLACEMENT, 1));
  PS1 -= (GetGeometry()[slave1].FastGetSolutionStepValue(DISPLACEMENT) - GetGeometry()[slave1].FastGetSolutionStepValue(DISPLACEMENT, 1));
  PS2 -= (GetGeometry()[slave2].FastGetSolutionStepValue(DISPLACEMENT) - GetGeometry()[slave2].FastGetSolutionStepValue(DISPLACEMENT, 1));

  mContactUtilities.CalculateEdgeDistances(rVariables.Contact.ReferenceBase, P1, P2, PS1, PS2, mContactVariables.ReferenceSurface.Normal);

  //reference integration weight update
  PointType V1 = P2 - P1;
  PointType V2 = PS1 - PS2;

  PointType V3;
  MathUtils<double>::CrossProduct(V3, V1, V2);
  rVariables.Contact.Tangent.ReferenceArea = 0.5 * norm_2(V3);

  rVariables.Contact.Tangent.FactorArea = 0.25 * (rVariables.Contact.ReferenceBase[0].L + rVariables.Contact.ReferenceBase[1].L) * (rVariables.Contact.ReferenceBase[0].L + rVariables.Contact.ReferenceBase[1].L);

  rVariables.Contact.ContactFactor.Normal = mContactVariables.PenaltyFactor;
  rVariables.Contact.ContactFactor.Tangent = rVariables.Contact.ContactFactor.Normal * GetProperties()[TANGENTIAL_PENALTY_RATIO];
  ;

  //f.-obtain the (g_N)3 and (g_T)3 for the n configuration

  PointType M1 = 0.5 * (P1 + P2);
  PointType M2 = 0.5 * (PS1 + PS2);
  double ReferenceGapN = inner_prod((M2 - M1), mContactVariables.ReferenceSurface.Normal);

  double H = ReferenceGapN;

  //covariant gap
  double ReferenceGapcvTA = ReferenceGapN * inner_prod(rVariables.Contact.Tangent.CovariantBase.DirectionA, mContactVariables.ReferenceSurface.Normal);
  double ReferenceGapcvTB = ReferenceGapN * inner_prod(rVariables.Contact.Tangent.CovariantBase.DirectionB, mContactVariables.ReferenceSurface.Normal);

  //contravariant gap
  double ReferenceGapcnTA = ReferenceGapN * inner_prod(rVariables.Contact.Tangent.ContravariantBase.DirectionA, mContactVariables.ReferenceSurface.Normal);
  double ReferenceGapcnTB = ReferenceGapN * inner_prod(rVariables.Contact.Tangent.ContravariantBase.DirectionB, mContactVariables.ReferenceSurface.Normal);

  PointType D1 = GetGeometry()[node1].FastGetSolutionStepValue(DISPLACEMENT) - GetGeometry()[node1].FastGetSolutionStepValue(DISPLACEMENT, 1);
  PointType D2 = GetGeometry()[node2].FastGetSolutionStepValue(DISPLACEMENT) - GetGeometry()[node2].FastGetSolutionStepValue(DISPLACEMENT, 1);
  PointType DS1 = GetGeometry()[slave1].FastGetSolutionStepValue(DISPLACEMENT) - GetGeometry()[slave1].FastGetSolutionStepValue(DISPLACEMENT, 1);
  PointType DS2 = GetGeometry()[slave2].FastGetSolutionStepValue(DISPLACEMENT) - GetGeometry()[slave2].FastGetSolutionStepValue(DISPLACEMENT, 1);

  //(g_N)3
  ReferenceGapN *= inner_prod(rVariables.Contact.CurrentSurface.Normal, mContactVariables.ReferenceSurface.Normal);

  ReferenceGapN += inner_prod(rVariables.Contact.CurrentSurface.Normal, (D1 * (-rVariables.Contact.ReferenceBase[0].A / rVariables.Contact.ReferenceBase[0].L)));
  ReferenceGapN += inner_prod(rVariables.Contact.CurrentSurface.Normal, (D2 * (-rVariables.Contact.ReferenceBase[0].B / rVariables.Contact.ReferenceBase[0].L)));
  ReferenceGapN += inner_prod(rVariables.Contact.CurrentSurface.Normal, (DS1 * (rVariables.Contact.ReferenceBase[1].A / rVariables.Contact.ReferenceBase[1].L)));
  ReferenceGapN += inner_prod(rVariables.Contact.CurrentSurface.Normal, (DS2 * (rVariables.Contact.ReferenceBase[1].B / rVariables.Contact.ReferenceBase[1].L)));

  //(g_cvTA)3
  ReferenceGapcvTA += inner_prod(rVariables.Contact.Tangent.CovariantBase.DirectionA, (D1 * (-rVariables.Contact.ReferenceBase[0].A / rVariables.Contact.ReferenceBase[0].L)));
  ReferenceGapcvTA += inner_prod(rVariables.Contact.Tangent.CovariantBase.DirectionA, (D2 * (-rVariables.Contact.ReferenceBase[0].B / rVariables.Contact.ReferenceBase[0].L)));
  ReferenceGapcvTA += inner_prod(rVariables.Contact.Tangent.CovariantBase.DirectionA, (DS1 * (rVariables.Contact.ReferenceBase[1].A / rVariables.Contact.ReferenceBase[1].L)));
  ReferenceGapcvTA += inner_prod(rVariables.Contact.Tangent.CovariantBase.DirectionA, (DS2 * (rVariables.Contact.ReferenceBase[1].B / rVariables.Contact.ReferenceBase[1].L)));

  //(g_cvTB)3
  ReferenceGapcvTB += inner_prod(rVariables.Contact.Tangent.CovariantBase.DirectionB, (D1 * (-rVariables.Contact.ReferenceBase[0].A / rVariables.Contact.ReferenceBase[0].L)));
  ReferenceGapcvTB += inner_prod(rVariables.Contact.Tangent.CovariantBase.DirectionB, (D2 * (-rVariables.Contact.ReferenceBase[0].B / rVariables.Contact.ReferenceBase[0].L)));
  ReferenceGapcvTB += inner_prod(rVariables.Contact.Tangent.CovariantBase.DirectionB, (DS1 * (rVariables.Contact.ReferenceBase[1].A / rVariables.Contact.ReferenceBase[1].L)));
  ReferenceGapcvTB += inner_prod(rVariables.Contact.Tangent.CovariantBase.DirectionB, (DS2 * (rVariables.Contact.ReferenceBase[1].B / rVariables.Contact.ReferenceBase[1].L)));

  //(g_cnTA)3
  ReferenceGapcnTA += inner_prod(rVariables.Contact.Tangent.ContravariantBase.DirectionA, (D1 * (-rVariables.Contact.ReferenceBase[0].A / rVariables.Contact.ReferenceBase[0].L)));
  ReferenceGapcnTA += inner_prod(rVariables.Contact.Tangent.ContravariantBase.DirectionA, (D2 * (-rVariables.Contact.ReferenceBase[0].B / rVariables.Contact.ReferenceBase[0].L)));
  ReferenceGapcnTA += inner_prod(rVariables.Contact.Tangent.ContravariantBase.DirectionA, (DS1 * (rVariables.Contact.ReferenceBase[1].A / rVariables.Contact.ReferenceBase[1].L)));
  ReferenceGapcnTA += inner_prod(rVariables.Contact.Tangent.ContravariantBase.DirectionA, (DS2 * (rVariables.Contact.ReferenceBase[1].B / rVariables.Contact.ReferenceBase[1].L)));

  //(g_cnTB)3
  ReferenceGapcnTB += inner_prod(rVariables.Contact.Tangent.ContravariantBase.DirectionB, (D1 * (-rVariables.Contact.ReferenceBase[0].A / rVariables.Contact.ReferenceBase[0].L)));
  ReferenceGapcnTB += inner_prod(rVariables.Contact.Tangent.ContravariantBase.DirectionB, (D2 * (-rVariables.Contact.ReferenceBase[0].B / rVariables.Contact.ReferenceBase[0].L)));
  ReferenceGapcnTB += inner_prod(rVariables.Contact.Tangent.ContravariantBase.DirectionB, (DS1 * (rVariables.Contact.ReferenceBase[1].A / rVariables.Contact.ReferenceBase[1].L)));
  ReferenceGapcnTB += inner_prod(rVariables.Contact.Tangent.ContravariantBase.DirectionB, (DS2 * (rVariables.Contact.ReferenceBase[1].B / rVariables.Contact.ReferenceBase[1].L)));

  rVariables.Contact.Tangent.EquivalentHeigh = rVariables.Contact.Tangent.ReferenceArea / rVariables.Contact.ReferenceBase[0].L;

  //...

  rVariables.Contact.CurrentGap.Normal = ReferenceGapN; //(g_N)3 -- needed in the Kcont1 computation

  rVariables.Contact.Tangent.A.CurrentGap.Covariant = ReferenceGapcvTA;     //(g_cvTA)3 -- needed in the Kcont1 computation
  rVariables.Contact.Tangent.A.CurrentGap.Contravariant = ReferenceGapcnTA; //(g_cnTA)3 -- needed in the Kcont1 computation

  rVariables.Contact.Tangent.B.CurrentGap.Covariant = ReferenceGapcvTB;     //(g_cvTB)3 -- needed in the Kcont1 computation
  rVariables.Contact.Tangent.B.CurrentGap.Contravariant = ReferenceGapcnTB; //(g_nTB)3 -- needed in the Kcont1 computation

  //g.- get total effective gap as: gap_n^eff=gap_n+(PreviousTimeStep/CurrentTimeStep)*(gap_n-gap_n-1)

  //(1/(3*Tau)) is now ContactFactor.Normal, the penalty parameter
  rVariables.Contact.Penalty.Normal = rVariables.Contact.CurrentGap.Normal * rVariables.Contact.ContactFactor.Normal;

  //i.- Compute tangent component of the tension vector:  (tt=cvt·P·N)
  rVariables.Contact.Tangent.A.Penalty = 0.0; //inner_prod(rVariables.Contact.Tangent.CovariantBase.DirectionA, mContactVariables.TractionVector);
  rVariables.Contact.Tangent.B.Penalty = 0.0; //inner_prod(rVariables.Contact.Tangent.CovariantBase.DirectionB, mContactVariables.TractionVector);

  rVariables.Contact.Tangent.A.CurrentGap.Covariant *= H;
  rVariables.Contact.Tangent.A.CurrentGap.Covariant *= H;
  rVariables.Contact.Tangent.A.CurrentGap.Contravariant *= H;
  rVariables.Contact.Tangent.B.CurrentGap.Contravariant *= H;

  rVariables.Contact.Tangent.A.Penalty += rVariables.Contact.ContactFactor.Tangent * rVariables.Contact.Tangent.A.CurrentGap.Covariant;
  rVariables.Contact.Tangent.B.Penalty += rVariables.Contact.ContactFactor.Tangent * rVariables.Contact.Tangent.B.CurrentGap.Covariant;

  //From effective gaps set active contact domain:
  double EffectiveGapN = ReferenceGapN;

  double EffectiveGapcvTA = ReferenceGapcvTA;
  double EffectiveGapcvTB = ReferenceGapcvTB;

  double EffectiveGapcnTA = ReferenceGapcnTA;
  double EffectiveGapcnTB = ReferenceGapcnTB;

  double CurrentTimeStep = rCurrentProcessInfo[DELTA_TIME];
  const ProcessInfo &rPreviousProcessInfo = rCurrentProcessInfo.GetPreviousSolutionStepInfo();
  double PreviousTimeStep = rPreviousProcessInfo[DELTA_TIME];

  if (mContactVariables.PreviousGap.Normal != 0 && mContactVariables.IterationCounter < 1)
  {
    EffectiveGapN += (CurrentTimeStep / PreviousTimeStep) * (ReferenceGapN - mContactVariables.PreviousGap.Normal);
  }

  if (mContactVariables.Tangent.PreviousGapA.Covariant != 0 && mContactVariables.IterationCounter < 1)
  {
    EffectiveGapcvTA += (CurrentTimeStep / PreviousTimeStep) * (ReferenceGapcvTA - mContactVariables.Tangent.PreviousGapA.Covariant);
  }

  if (mContactVariables.Tangent.PreviousGapB.Covariant != 0 && mContactVariables.IterationCounter < 1)
  {
    EffectiveGapcvTB += (CurrentTimeStep / PreviousTimeStep) * (ReferenceGapcvTB - mContactVariables.Tangent.PreviousGapB.Covariant);
  }

  if (mContactVariables.Tangent.PreviousGapA.Contravariant != 0 && mContactVariables.IterationCounter < 1)
  {
    EffectiveGapcnTA += (CurrentTimeStep / PreviousTimeStep) * (ReferenceGapcnTA - mContactVariables.Tangent.PreviousGapA.Contravariant);
  }

  if (mContactVariables.Tangent.PreviousGapB.Contravariant != 0 && mContactVariables.IterationCounter < 1)
  {
    EffectiveGapcnTB += (CurrentTimeStep / PreviousTimeStep) * (ReferenceGapcnTB - mContactVariables.Tangent.PreviousGapB.Contravariant);
  }

  //CHECK IF THE ELEMENT IS ACTIVE:
  rVariables.Contact.Options.Set(SLIP, false);

  //CORRECTION: to skip tip contact elements problems:

  bool check_fictious_geometry = false;

  //Check ORTHOGONAL FACES in contact
  if (check_fictious_geometry == true)
  {
    //slave -> slave1
    PointType &SlaveNormal = GetGeometry()[slave1].FastGetSolutionStepValue(NORMAL);
    double orthogonal = inner_prod(SlaveNormal, rVariables.Contact.CurrentSurface.Normal);

    if (EffectiveGapN <= 0 && fabs(orthogonal) <= 1)
    {
      bool real_contact = CheckFictiousContacts(rVariables);

      if (!real_contact || fabs(orthogonal) <= 0.25)
      {
        EffectiveGapN = 1; //not active element: geometrically wrong
        std::cout << " DISABLE ContactElement " << this->Id() << " real contact " << real_contact << " geometrically orthogonal " << orthogonal << std::endl;
      }
    }
  }
  //Check ORTHOGONAL FACES in contact

  //decimal correction from tension vector calculation
  // if(fabs(EffectiveGapN)<= 1e-15 && fabs(EffectiveGapN)<= 1e-15)
  //   EffectiveGapN = 0;
  //decimal correction from tension vector calculation

  // std::cout<<" PreviousGapN  "<< mContactVariables.PreviousGap.Normal <<std::endl;
  // std::cout<<" ReferenceGapN "<< ReferenceGapN <<std::endl;
  // std::cout<<" EffectiveGapN "<< EffectiveGapN <<std::endl;
  // std::cout<<" CurrentTensil "<< rVariables.Contact.CurrentTensil.Normal <<std::endl;
  if (EffectiveGapN <= 0) //if(EffectiveGap<0){
  {
    //Initialize friction parameter
    rVariables.Contact.FrictionCoefficient = 0;

    rVariables.Contact.Options.Set(ACTIVE, true); //normal contact active

    if ((fabs(EffectiveGapcvTA + EffectiveGapcvTB) <= rVariables.Contact.FrictionCoefficient * fabs(EffectiveGapN)) ||
        (fabs(EffectiveGapcnTA + EffectiveGapcnTB) <= rVariables.Contact.FrictionCoefficient * fabs(EffectiveGapN)))
    {
      rVariables.Contact.Options.Set(SLIP, false); //contact stick case active
    }
    else
    {
      rVariables.Contact.Options.Set(SLIP, true); //contact slip  case active
    }
  }
  else
  {
    rVariables.Contact.Options.Set(ACTIVE, false); //normal contact not active
  }

  //temporary
  rVariables.Contact.Options.Set(SLIP, false); //impose stick

  //From total current gap compute multipliers:

  if (rVariables.Contact.Tangent.A.Penalty < 0) //add the sign of the penalty
  {
    rVariables.Contact.Tangent.A.GapSign *= (-1);
  }

  if (rVariables.Contact.Tangent.B.Penalty < 0) //add the sign of the penalty
  {
    rVariables.Contact.Tangent.B.GapSign *= (-1);
  }

  if (H == 0)
  {
    rVariables.Contact.Tangent.A.GapSign = 0;
    rVariables.Contact.Tangent.B.GapSign = 0;
  }

  //check for distorted patches
  if (rVariables.Contact.Options.Is(ACTIVE))
  {

    double distorted_0 = 1, distorted_1 = 1, distorted_2 = 1, distorted_3 = 1;
    distorted_0 = fabs((-rVariables.Contact.ReferenceBase[0].A / rVariables.Contact.ReferenceBase[0].L));
    distorted_1 = fabs((-rVariables.Contact.ReferenceBase[0].B / rVariables.Contact.ReferenceBase[0].L));
    distorted_2 = fabs((rVariables.Contact.ReferenceBase[1].A / rVariables.Contact.ReferenceBase[1].L));
    distorted_3 = fabs((rVariables.Contact.ReferenceBase[1].B / rVariables.Contact.ReferenceBase[1].L));

    double dist = 1.01 + fabs(rVariables.Contact.CurrentGap.Normal); //1e12;

    if (distorted_0 > dist || distorted_1 > dist || distorted_2 > dist || distorted_3 > dist)
    {
      // Domain overlap check is needed to activate or deactivate elements
      // rVariables.Contact.Options.Set(ACTIVE, false);
      // std::cout << " DISTORTED EDGE ELEMENT " << this->Id() << " : (d0=" << distorted_0 << ",d1=" << distorted_1 << ",d2=" << distorted_2 << ",d3=" << distorted_3 << ") " << std::endl;
    }

    //Check orthogonal sliver
    double OrthogonalDistance = 0.5 * norm_2(PS1 + PS2 - P1 - P2);
    if (OrthogonalDistance < 0.02 * rVariables.Contact.ReferenceBase[0].L)
    {
      PointType T1 = P2 - P1;
      T1 /= norm_2(T1);
      PointType T2 = PS1 - PS2;
      T2 /= norm_2(T2);
      double Projection = std::abs(inner_prod(T1, T2));
      if (Projection > 0.998)
      {
        rVariables.Contact.Options.Set(ACTIVE, false);
        std::cout << " EDGE SLIVER INACTIVE " << OrthogonalDistance << " edges projection " << Projection << std::endl;
      }
    }
  }

  // set contact normal
  const unsigned int number_of_nodes = GetGeometry().PointsNumber();
  for (unsigned int i = 0; i < number_of_nodes; i++)
  {
    GetGeometry()[i].SetLock();
    array_1d<double, 3> &ContactNormal = GetGeometry()[i].FastGetSolutionStepValue(CONTACT_NORMAL);
    if (i == slave1 || i == slave2)
      ContactNormal += rVariables.Contact.CurrentSurface.Normal;

    double modulus = norm_2(ContactNormal);
    if (modulus!=0)
      ContactNormal/=modulus;
    GetGeometry()[i].UnSetLock();
  }

}

//***********************************************************************************
//************************************************************************************

double &ContactDomainPenalty3DCondition::CalculateIntegrationWeight(ConditionVariables &rVariables, double &rIntegrationWeight) const
{
  if (mUsePreviousConfiguration)
  {
  // UL (ask for the last known configuration size)
    rIntegrationWeight = (1.0 / 3.0) * rVariables.Contact.Tangent.ReferenceArea;  //with this passes patch test, wrong convergence
  }
  else{
    // SL (ask for the current configuration size)
    rIntegrationWeight = (1.0/3.0) * rVariables.Contact.Tangent.CurrentArea;  //with this better convergence wrong patch test
  }
  //rIntegrationWeight *= rVariables.Contact.Tangent.ElementSize;
  return rIntegrationWeight;
}

//************************************************************************************
//************************************************************************************

void ContactDomainPenalty3DCondition::CalculateNormalForce(double &F, ConditionVariables &rVariables, unsigned int &ndi, unsigned int &idir)
{
  KRATOS_TRY

  if (mUsePreviousConfiguration)
  {
    F = rVariables.Contact.Penalty.Normal * rVariables.Contact.dN_dn[ndi] * mContactVariables.ReferenceSurface.Normal[idir];
  }
  else{
     F = rVariables.Contact.Penalty.Normal * rVariables.Contact.dN_dn[ndi] * rVariables.Contact.CurrentSurface.Normal[idir];
  }

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void ContactDomainPenalty3DCondition::CalculateTangentStickForce(double &F, ConditionVariables &rVariables, unsigned int &ndi, unsigned int &idir)
{
  KRATOS_TRY

  if (rVariables.Contact.Options.Is(ContactDomainUtilities::COMPUTE_FRICTION_FORCES))
  {
    F = rVariables.Contact.CurrentGap.Normal * rVariables.Contact.CurrentSurface.Normal[idir] +
        rVariables.Contact.Tangent.A.CurrentGap.Contravariant * rVariables.Contact.Tangent.CovariantBase.DirectionA[idir] +
        rVariables.Contact.Tangent.B.CurrentGap.Contravariant * rVariables.Contact.Tangent.CovariantBase.DirectionB[idir];

    F *= (rVariables.Contact.Tangent.A.Penalty * rVariables.Contact.Tangent.A.dN_dt[ndi] +
          rVariables.Contact.Tangent.B.Penalty * rVariables.Contact.Tangent.B.dN_dt[ndi]);

    F += rVariables.Contact.dN_drn[ndi] * (rVariables.Contact.Tangent.A.Penalty * rVariables.Contact.Tangent.ContravariantBase.DirectionA[idir] + rVariables.Contact.Tangent.B.Penalty * rVariables.Contact.Tangent.ContravariantBase.DirectionB[idir]);
  }
  else
  {
    F = 0.0;
  }

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void ContactDomainPenalty3DCondition::CalculateTangentSlipForce(double &F, ConditionVariables &rVariables, unsigned int &ndi, unsigned int &idir)
{
  KRATOS_TRY

  if (rVariables.Contact.Options.Is(ContactDomainUtilities::COMPUTE_FRICTION_FORCES))
  {
    F = (rVariables.Contact.Tangent.A.GapSign * rVariables.Contact.Tangent.CovariantBase.DirectionA[ndi] +
         rVariables.Contact.Tangent.B.GapSign * rVariables.Contact.Tangent.CovariantBase.DirectionB[ndi]);

    F *= rVariables.Contact.CurrentGap.Normal * rVariables.Contact.CurrentSurface.Normal[idir];

    F += rVariables.Contact.dN_drn[ndi] * (rVariables.Contact.Tangent.ContravariantBase.DirectionA[idir] +
                                           rVariables.Contact.Tangent.ContravariantBase.DirectionB[idir]);

    F *= (rVariables.Contact.Penalty.Normal * rVariables.Contact.FrictionCoefficient);
  }
  else
  {
    F = 0.0;
  }

  KRATOS_CATCH(" ")
}

//************************************************************************************
//************************************************************************************

void ContactDomainPenalty3DCondition::CalculateContactStiffness(double &Kcont, ConditionVariables &rVariables, unsigned int &ndi, unsigned int &ndj, unsigned int &idir, unsigned int &jdir)
{
  KRATOS_TRY

  Kcont = 0;

  //double athird = 1.0 / 3.0;

  //Normal contact contribution:
  //KI:
  Kcont = rVariables.Contact.ContactFactor.Normal * rVariables.Contact.Tangent.ElementSize *
          (rVariables.Contact.dN_dn[ndi] * rVariables.Contact.CurrentSurface.Normal[idir] * rVariables.Contact.dN_dn[ndj] * rVariables.Contact.CurrentSurface.Normal[jdir]);

  Kcont -= (rVariables.Contact.ContactFactor.Normal * rVariables.Contact.CurrentGap.Normal * rVariables.Contact.CurrentGap.Normal) * rVariables.Contact.Tangent.ElementSize *
           (rVariables.Contact.Tangent.ContravariantBase.Metric(0, 0) * rVariables.Contact.Tangent.A.dN_dt[ndi] * rVariables.Contact.CurrentSurface.Normal[idir] * rVariables.Contact.Tangent.A.dN_dt[ndj] * rVariables.Contact.CurrentSurface.Normal[jdir] +
            rVariables.Contact.Tangent.ContravariantBase.Metric(0, 1) * rVariables.Contact.Tangent.A.dN_dt[ndi] * rVariables.Contact.CurrentSurface.Normal[idir] * rVariables.Contact.Tangent.B.dN_dt[ndj] * rVariables.Contact.CurrentSurface.Normal[jdir] +
            rVariables.Contact.Tangent.ContravariantBase.Metric(1, 0) * rVariables.Contact.Tangent.B.dN_dt[ndi] * rVariables.Contact.CurrentSurface.Normal[idir] * rVariables.Contact.Tangent.A.dN_dt[ndj] * rVariables.Contact.CurrentSurface.Normal[jdir] +
            rVariables.Contact.Tangent.ContravariantBase.Metric(1, 1) * rVariables.Contact.Tangent.B.dN_dt[ndi] * rVariables.Contact.CurrentSurface.Normal[idir] * rVariables.Contact.Tangent.B.dN_dt[ndj] * rVariables.Contact.CurrentSurface.Normal[jdir]);

  Kcont -= (rVariables.Contact.ContactFactor.Normal * rVariables.Contact.CurrentGap.Normal) * rVariables.Contact.Tangent.ElementSize *
           (rVariables.Contact.Tangent.A.dN_dt[ndi] * rVariables.Contact.CurrentSurface.Normal[idir] * rVariables.Contact.dN_dn[ndj] * rVariables.Contact.Tangent.ContravariantBase.DirectionA[jdir] +
            rVariables.Contact.Tangent.B.dN_dt[ndi] * rVariables.Contact.CurrentSurface.Normal[idir] * rVariables.Contact.dN_dn[ndj] * rVariables.Contact.Tangent.ContravariantBase.DirectionB[jdir] +
            rVariables.Contact.dN_dn[ndi] * rVariables.Contact.Tangent.ContravariantBase.DirectionA[idir] * rVariables.Contact.Tangent.A.dN_dt[ndj] * rVariables.Contact.CurrentSurface.Normal[jdir] +
            rVariables.Contact.dN_dn[ndi] * rVariables.Contact.Tangent.ContravariantBase.DirectionB[idir] * rVariables.Contact.Tangent.B.dN_dt[ndj] * rVariables.Contact.CurrentSurface.Normal[jdir]);

  //std::cout<<" Kb "<<Kcont;

  //Stick contact contribution:
  if (rVariables.Contact.Options.IsNot(SLIP))
  {

    //std::cout<<" + stick ";
    if (rVariables.Contact.Options.Is(ContactDomainUtilities::COMPUTE_FRICTION_STIFFNESS))
    {
      //std::cout<<"(mu_on)";
      //KI:
      double raux = rVariables.Contact.CurrentGap.Normal * rVariables.Contact.CurrentSurface.Normal[idir] +
                    rVariables.Contact.Tangent.A.CurrentGap.Contravariant * rVariables.Contact.Tangent.CovariantBase.DirectionA[idir] +
                    rVariables.Contact.Tangent.B.CurrentGap.Contravariant * rVariables.Contact.Tangent.CovariantBase.DirectionB[idir]; //it must be GapcnTA (contravariant gap)
      //std::cout<<" raux "<<raux;
      //KI:
      Kcont += (raux * rVariables.Contact.Tangent.ElementSize * rVariables.Contact.Tangent.A.dN_dt[ndi] + rVariables.Contact.dN_drn[ndi] * rVariables.Contact.Tangent.ContravariantBase.DirectionA[idir]) *
               (rVariables.Contact.ContactFactor.Normal * rVariables.Contact.dN_drn[ndj] * rVariables.Contact.Tangent.CovariantBase.DirectionA[jdir] +
                rVariables.Contact.Penalty.Normal * rVariables.Contact.CurrentSurface.Normal[jdir] * (rVariables.Contact.Tangent.CovariantBase.Metric(0, 0) * rVariables.Contact.Tangent.A.dN_dt[ndj] + rVariables.Contact.Tangent.CovariantBase.Metric(0, 1) * rVariables.Contact.Tangent.B.dN_dt[ndj]));

      Kcont += (raux * rVariables.Contact.Tangent.ElementSize * rVariables.Contact.Tangent.B.dN_dt[ndi] + rVariables.Contact.dN_drn[ndi] * rVariables.Contact.Tangent.ContravariantBase.DirectionB[idir]) *
               (rVariables.Contact.ContactFactor.Normal * rVariables.Contact.dN_drn[ndj] * rVariables.Contact.Tangent.CovariantBase.DirectionB[jdir] +
                rVariables.Contact.Penalty.Normal * rVariables.Contact.CurrentSurface.Normal[jdir] * (rVariables.Contact.Tangent.CovariantBase.Metric(1, 0) * rVariables.Contact.Tangent.A.dN_dt[ndj] + rVariables.Contact.Tangent.CovariantBase.Metric(1, 1) * rVariables.Contact.Tangent.B.dN_dt[ndj]));

      Kcont += rVariables.Contact.Tangent.ElementSize * rVariables.Contact.Tangent.A.Penalty * (rVariables.Contact.Tangent.A.dN_dt[ndi] * rVariables.Contact.CurrentSurface.Normal[idir] * rVariables.Contact.dN_dn[ndj] * rVariables.Contact.CurrentSurface.Normal[jdir] + rVariables.Contact.dN_drn[ndi] * rVariables.Contact.CurrentSurface.Normal[idir] * rVariables.Contact.Tangent.A.dN_dt[ndj] * rVariables.Contact.CurrentSurface.Normal[jdir] + (rVariables.Contact.Tangent.ContravariantBase.DirectionA[idir] * rVariables.Contact.Tangent.CovariantBase.DirectionA[jdir] + rVariables.Contact.Tangent.ContravariantBase.DirectionB[idir] * rVariables.Contact.Tangent.CovariantBase.DirectionB[jdir]) * rVariables.Contact.Tangent.A.dN_dt[ndi] * rVariables.Contact.dN_drn[ndj] + (rVariables.Contact.Tangent.A.CurrentGap.Contravariant * rVariables.Contact.Tangent.A.dN_dt[ndj] * rVariables.Contact.CurrentSurface.Normal[jdir] + rVariables.Contact.Tangent.B.CurrentGap.Contravariant * rVariables.Contact.Tangent.B.dN_dt[ndj] * rVariables.Contact.CurrentSurface.Normal[jdir]) * rVariables.Contact.Tangent.A.dN_dt(ndi) * rVariables.Contact.CurrentSurface.Normal(idir) - raux * rVariables.Contact.Tangent.A.dN_dt(ndi) * rVariables.Contact.Tangent.CovariantBase.DirectionA(jdir) * rVariables.Contact.Tangent.A.dN_dt(ndj) - raux * rVariables.Contact.Tangent.B.dN_dt(ndi) * rVariables.Contact.Tangent.CovariantBase.DirectionB(jdir) * rVariables.Contact.Tangent.A.dN_dt(ndj));

      Kcont -= rVariables.Contact.Tangent.ElementSize * rVariables.Contact.Tangent.B.Penalty * (rVariables.Contact.Tangent.B.dN_dt[ndi] * rVariables.Contact.CurrentSurface.Normal[idir] * rVariables.Contact.dN_dn[ndj] * rVariables.Contact.CurrentSurface.Normal[jdir] + rVariables.Contact.dN_drn[ndi] * rVariables.Contact.CurrentSurface.Normal[idir] * rVariables.Contact.Tangent.B.dN_dt[ndj] * rVariables.Contact.CurrentSurface.Normal[jdir] + (rVariables.Contact.Tangent.ContravariantBase.DirectionA[idir] * rVariables.Contact.Tangent.CovariantBase.DirectionA[jdir] + rVariables.Contact.Tangent.ContravariantBase.DirectionB[idir] * rVariables.Contact.Tangent.CovariantBase.DirectionB[jdir]) * rVariables.Contact.Tangent.B.dN_dt[ndi] * rVariables.Contact.dN_drn[ndj] + (rVariables.Contact.Tangent.A.CurrentGap.Contravariant * rVariables.Contact.Tangent.A.dN_dt[ndj] * rVariables.Contact.CurrentSurface.Normal[jdir] + rVariables.Contact.Tangent.B.CurrentGap.Contravariant * rVariables.Contact.Tangent.B.dN_dt[ndj] * rVariables.Contact.CurrentSurface.Normal[jdir]) * rVariables.Contact.Tangent.B.dN_dt(ndi) * rVariables.Contact.CurrentSurface.Normal(idir) - raux * rVariables.Contact.Tangent.A.dN_dt(ndi) * rVariables.Contact.Tangent.CovariantBase.DirectionA(jdir) * rVariables.Contact.Tangent.B.dN_dt(ndj) - raux * rVariables.Contact.Tangent.B.dN_dt(ndi) * rVariables.Contact.Tangent.CovariantBase.DirectionB(jdir) * rVariables.Contact.Tangent.B.dN_dt(ndj));

      // rstiff = rstiff + (raux*dNt1_a(in) + t1cn(idime)*hdNn_n(in))*
      //                    ( cte*geofct_n/tau_t*t1cv(jdime)*hdNn_n(jn) +
      //                      lam_n*ne_a(jdime)*(gmcov(1,1)*dNt1_a(jn) + gmcov(1,2)*dNt2_a(jn)) ) +

      //                    (raux*dNt2_a(in) + t2cn(idime)*hdNn_n(in))*
      //                    ( cte*geofct_n/tau_t*t2cv(jdime)*hdNn_n(jn) +
      //                      lam_n*ne_a(jdime)*(gmcov(2,1)*dNt1_a(jn) + gmcov(2,2)*dNt2_a(jn)) ) +

      //           lam_t1*( ne_a(idime)*dNt1_a(in)*ne_a(jdime)*hdNn_a(jn) +
      // 			  ne_a(idime)*hdNn_n(in)*ne_a(jdime)*dNt1_a(jn) +  &

      //                    ( t1cn(idime)*t1cv(jdime) + t2cn(idime)*t2cv(jdime) )*dNt1_a(in)*hdNn_n(jn) +
      //                    ( gt1_cv*ne_a(jdime)*dNt1_a(jn) + gt2_cv*ne_a(jdime)*dNt2_a(jn) )*ne_a(idime)*dNt1_a(in) -
      //                    raux*dNt1_a(in)*t1cv(jdime)*dNt1_a(jn) -
      // 			  raux*dNt2_a(in)*t2cv(jdime)*dNt1_a(jn) ) +              &

      //           lam_t2*( ne_a(idime)*dNt2_a(in)*ne_a(jdime)*hdNn_a(jn) + ne_a(idime)*hdNn_n(in)*ne_a(jdime)*dNt2_a(jn) +
      //                    ( t1cn(idime)*t1cv(jdime) + t2cn(idime)*t2cv(jdime) )*dNt2_a(in)*hdNn_n(jn) +
      //                    ( gt1_cv*ne_a(jdime)*dNt1_a(jn) + gt2_cv*ne_a(jdime)*dNt2_a(jn) )*ne_a(idime)*dNt2_a(in) -
      //                    raux*dNt1_a(in)*t1cv(jdime)*dNt2_a(jn) -
      // 			  raux*dNt2_a(in)*t2cv(jdime)*dNt2_a(jn) )

      //std::cout<<" Kc "<<Kcont;

      //std::cout<<" Kd "<<Kcont<<std::endl;

      // rstiff = rstiff + ( he_a*ne_a(idime) + gt1_cn*t1cv(idime) + gt2_cn*t2cv(idime) )*
      //                               ( dNt1_a(in)*raux1 + dNt2_a(in)*raux2 ) +                        &
      //                               hdNn_n(in)*( t1cn(idime)*raux1 + t2cn(idime)*raux2 )
    }
  }
  else
  {
    //Slip contact contribution:
    //std::cout<<" + slip ";
    if (rVariables.Contact.Options.Is(ContactDomainUtilities::COMPUTE_FRICTION_STIFFNESS))
    {

      //          raux = he_a*ne_a(idime) + gt1_cn*t1cv(idime) + gt2_cn*t2cv(idime)

      //std::cout<<"(mu_on)";
      //KI:
      //     rstiff = rstiff + (raux*dNt1_a(in) + t1cn(idime)*hdNn_n(in))*
      //                                   ( cte*geofct_n/tau_t*t1cv(jdime)*hdNn_n(jn) +
      //                                     lam_n*ne_a(jdime)*(gmcov(1,1)*dNt1_a(jn) + gmcov(1,2)*dNt2_a(jn)) ) +
      //                       (raux*dNt2_a(in) + t2cn(idime)*hdNn_n(in))*
      //   	                             ( cte*geofct_n/tau_t*t2cv(jdime)*hdNn_n(jn) +
      //                                     lam_n*ne_a(jdime)*(gmcov(2,1)*dNt1_a(jn) + gmcov(2,2)*dNt2_a(jn)) ) +

      //                          lam_t1*( ( gt1_cv*ne_a(jdime)*dNt1_a(jn) + gt2_cv*ne_a(jdime)*dNt2_a(jn) )*ne_a(idime)*dNt1_a(in) -
      //                                   raux*dNt1_a(in)*t1cv(jdime)*dNt1_a(jn) - raux*dNt2_a(in)*t2cv(jdime)*dNt1_a(jn) -
      //                                   t1cn(idime)*hdNn_n(in)*t1cv(jdime)*dNt1_a(jn) - t2cn(idime)*hdNn_n(in)*t2cv(jdime)*dNt1_a(jn) )+

      //                          lam_t2*( ( gt1_cv*ne_a(jdime)*dNt1_a(jn) + gt2_cv*ne_a(jdime)*dNt2_a(jn) )*ne_a(idime)*dNt2_a(in) -
      //                                   raux*dNt1_a(in)*t1cv(jdime)*dNt2_a(jn) - raux*dNt2_a(in)*t2cv(jdime)*dNt2_a(jn) -
      //                                   t1cn(idime)*hdNn_n(in)*t1cv(jdime)*dNt2_a(jn) - t2cn(idime)*hdNn_n(in)*t2cv(jdime)*dNt2_a(jn)

      //KII:
    }
  }

  // std::cout<<" ndi "<<ndi<<" ndj "<<ndj<<" i "<<idir<<" j "<<jdir<<std::endl;
  // std::cout<<" Kcont "<<Kcont;
  // std::cout<<" constant "<<athird * rVariables.Contact.Tangent.CurrentArea<<std::endl;

  //std::cout<<" Ks "<<Kcont-K1<<std::endl;

  KRATOS_CATCH(" ")
}

void ContactDomainPenalty3DCondition::save(Serializer &rSerializer) const
{
  KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, ContactDomainLM3DCondition)
}

void ContactDomainPenalty3DCondition::load(Serializer &rSerializer)
{
  KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, ContactDomainLM3DCondition)
}

} // Namespace Kratos
