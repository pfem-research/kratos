//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                  July 2013 $
//
//

// System includes

// External includes

// Project includes
#include "custom_conditions/beam_contact/beam_point_rigid_contact_LM_3D_condition.hpp"

namespace Kratos
{
//************************************************************************************
//************************************************************************************
BeamPointRigidContactLM3DCondition::BeamPointRigidContactLM3DCondition(IndexType NewId, GeometryType::Pointer
                                                                                            pGeometry)
    : BeamPointRigidContactPenalty3DCondition(NewId, pGeometry)
{
  //DO NOT ADD DOFS HERE!!!
}

//************************************************************************************
//************************************************************************************
BeamPointRigidContactLM3DCondition::BeamPointRigidContactLM3DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : BeamPointRigidContactPenalty3DCondition(NewId, pGeometry, pProperties)
{
}

//************************************************************************************
//************************************************************************************
BeamPointRigidContactLM3DCondition::BeamPointRigidContactLM3DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties, SpatialBoundingBox::Pointer pRigidWall)
    : BeamPointRigidContactPenalty3DCondition(NewId, pGeometry, pProperties, pRigidWall)
{
}

//************************************************************************************
//************************************************************************************
BeamPointRigidContactLM3DCondition::BeamPointRigidContactLM3DCondition(BeamPointRigidContactLM3DCondition const &rOther)
    : BeamPointRigidContactPenalty3DCondition(rOther)
{
}

//************************************************************************************
//************************************************************************************

Condition::Pointer BeamPointRigidContactLM3DCondition::Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const
{
  return Kratos::make_intrusive<BeamPointRigidContactLM3DCondition>(NewId, GetGeometry().Create(ThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************

Condition::Pointer BeamPointRigidContactLM3DCondition::Clone(IndexType NewId, NodesArrayType const &ThisNodes) const
{
  BeamPointRigidContactLM3DCondition ClonedCondition(NewId, GetGeometry().Create(ThisNodes), pGetProperties(), mpRigidWall);
  if (this->mpFrictionLaw!=nullptr)
    ClonedCondition.mpFrictionLaw = this->mpFrictionLaw->Clone();
  return Kratos::make_intrusive<BeamPointRigidContactLM3DCondition>(ClonedCondition);
}

//************************************************************************************
//************************************************************************************

BeamPointRigidContactLM3DCondition::~BeamPointRigidContactLM3DCondition()
{
}

//************* GETTING METHODS

//***********************************************************************************
//***********************************************************************************

void BeamPointRigidContactLM3DCondition::GetDofList(DofsVectorType &rConditionDofList,
                                                    const ProcessInfo &rCurrentProcessInfo) const
{
  KRATOS_TRY

  rConditionDofList.resize(0);
  const unsigned int number_of_nodes = GetGeometry().PointsNumber();
  const unsigned int dimension = GetGeometry().WorkingSpaceDimension();

  for (unsigned int i = 0; i < number_of_nodes; i++)
  {
    rConditionDofList.push_back(GetGeometry()[i].pGetDof(DISPLACEMENT_X));
    rConditionDofList.push_back(GetGeometry()[i].pGetDof(DISPLACEMENT_Y));

    if (dimension == 3)
    {
      rConditionDofList.push_back(GetGeometry()[i].pGetDof(DISPLACEMENT_Z));
      rConditionDofList.push_back(GetGeometry()[i].pGetDof(ROTATION_X));
      rConditionDofList.push_back(GetGeometry()[i].pGetDof(ROTATION_Y));
    }

    rConditionDofList.push_back(GetGeometry()[i].pGetDof(ROTATION_Z));
    rConditionDofList.push_back(GetGeometry()[i].pGetDof(LAGRANGE_MULTIPLIER_NORMAL));
  }

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void BeamPointRigidContactLM3DCondition::EquationIdVector(EquationIdVectorType &rResult,
                                                          const ProcessInfo &rCurrentProcessInfo) const
{
  KRATOS_TRY

  const unsigned int number_of_nodes = GetGeometry().PointsNumber();
  const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
  unsigned int condition_size = number_of_nodes * this->GetNodeDofsSize();

  if (rResult.size() != condition_size)
    rResult.resize(condition_size, false);

  for (unsigned int i = 0; i < number_of_nodes; i++)
  {
    int index = i * this->GetNodeDofsSize();
    rResult[index] = GetGeometry()[i].GetDof(DISPLACEMENT_X).EquationId();
    rResult[index + 1] = GetGeometry()[i].GetDof(DISPLACEMENT_Y).EquationId();

    if (dimension == 3)
    {
      rResult[index + 2] = GetGeometry()[i].GetDof(DISPLACEMENT_Z).EquationId();
      rResult[index + 3] = GetGeometry()[i].GetDof(ROTATION_X).EquationId();
      rResult[index + 4] = GetGeometry()[i].GetDof(ROTATION_Y).EquationId();
    }

    rResult[index + 5] = GetGeometry()[i].GetDof(ROTATION_Z).EquationId();
    rResult[index + 6] = GetGeometry()[i].GetDof(LAGRANGE_MULTIPLIER_NORMAL).EquationId();
  }

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void BeamPointRigidContactLM3DCondition::GetValuesVector(Vector &rValues, int Step) const
{
  KRATOS_TRY

  const unsigned int number_of_nodes = GetGeometry().PointsNumber();
  const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
  unsigned int condition_size = number_of_nodes * this->GetNodeDofsSize();

  if (rValues.size() != condition_size)
    rValues.resize(condition_size, false);

  for (unsigned int i = 0; i < number_of_nodes; i++)
  {
    unsigned int index = i * this->GetNodeDofsSize();
    rValues[index] = GetGeometry()[i].FastGetSolutionStepValue(DISPLACEMENT_X, Step);
    rValues[index + 1] = GetGeometry()[i].FastGetSolutionStepValue(DISPLACEMENT_Y, Step);

    if (dimension == 3)
    {
      rValues[index + 2] = GetGeometry()[i].FastGetSolutionStepValue(DISPLACEMENT_Z, Step);
      rValues[index + 3] = GetGeometry()[i].FastGetSolutionStepValue(ROTATION_X, Step);
      rValues[index + 4] = GetGeometry()[i].FastGetSolutionStepValue(ROTATION_Y, Step);
    }

    rValues[index + 5] = GetGeometry()[i].FastGetSolutionStepValue(ROTATION_Z, Step);
    rValues[index + 6] = GetGeometry()[i].FastGetSolutionStepValue(LAGRANGE_MULTIPLIER_NORMAL, Step);
  }

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void BeamPointRigidContactLM3DCondition::GetFirstDerivativesVector(Vector &rValues, int Step) const
{
  KRATOS_TRY

  const unsigned int number_of_nodes = GetGeometry().PointsNumber();
  const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
  unsigned int condition_size = number_of_nodes * this->GetNodeDofsSize();

  if (rValues.size() != condition_size)
    rValues.resize(condition_size, false);

  for (unsigned int i = 0; i < number_of_nodes; i++)
  {
    unsigned int index = i * this->GetNodeDofsSize();
    rValues[index] = GetGeometry()[i].FastGetSolutionStepValue(VELOCITY_X, Step);
    rValues[index + 1] = GetGeometry()[i].FastGetSolutionStepValue(VELOCITY_Y, Step);

    if (dimension == 3)
    {
      rValues[index + 2] = GetGeometry()[i].FastGetSolutionStepValue(VELOCITY_Z, Step);
      rValues[index + 3] = GetGeometry()[i].FastGetSolutionStepValue(ANGULAR_VELOCITY_X, Step);
      rValues[index + 4] = GetGeometry()[i].FastGetSolutionStepValue(ANGULAR_VELOCITY_Y, Step);
    }

    rValues[index + 5] = GetGeometry()[i].FastGetSolutionStepValue(ANGULAR_VELOCITY_Z, Step);
    rValues[index + 6] = 0;
  }

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void BeamPointRigidContactLM3DCondition::GetSecondDerivativesVector(Vector &rValues, int Step) const
{
  KRATOS_TRY

  const unsigned int number_of_nodes = GetGeometry().PointsNumber();
  const unsigned int dimension = GetGeometry().WorkingSpaceDimension();
  unsigned int condition_size = number_of_nodes * this->GetNodeDofsSize();

  if (rValues.size() != condition_size)
    rValues.resize(condition_size, false);

  for (unsigned int i = 0; i < number_of_nodes; i++)
  {
    unsigned int index = i * this->GetNodeDofsSize();
    rValues[index] = GetGeometry()[i].FastGetSolutionStepValue(ACCELERATION_X, Step);
    rValues[index + 1] = GetGeometry()[i].FastGetSolutionStepValue(ACCELERATION_Y, Step);

    if (dimension == 3)
    {
      rValues[index + 2] = GetGeometry()[i].FastGetSolutionStepValue(ACCELERATION_Z, Step);
      rValues[index + 3] = GetGeometry()[i].FastGetSolutionStepValue(ANGULAR_ACCELERATION_X, Step);
      rValues[index + 4] = GetGeometry()[i].FastGetSolutionStepValue(ANGULAR_ACCELERATION_Y, Step);
    }

    rValues[index + 5] = GetGeometry()[i].FastGetSolutionStepValue(ANGULAR_ACCELERATION_Z, Step);
    rValues[index + 6] = 0;
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

BeamPointRigidContactLM3DCondition::SizeType BeamPointRigidContactLM3DCondition::GetNodeDofsSize() const
{
  return (GetGeometry().WorkingSpaceDimension()*(GetGeometry().WorkingSpaceDimension()-1)+1); //usual size for displacement based elements
}

//*********************************COMPUTE KINEMATICS*********************************
//************************************************************************************

void BeamPointRigidContactLM3DCondition::CalculateKinematics(ConditionVariables &rVariables,
                                                             const ProcessInfo &rCurrentProcessInfo,
                                                             const double &rPointNumber)
{
  KRATOS_TRY

  BeamPointRigidContactPenalty3DCondition::CalculateKinematics(rVariables, rCurrentProcessInfo, rPointNumber);

  //check previous normal reaction from the lagrange multiplier
  // if (GetGeometry()[0].FastGetSolutionStepValue(LAGRANGE_MULTIPLIER_NORMAL) > 0)
  // 	rVariables.Options.Set(ACTIVE,false);

  KRATOS_CATCH("")
}


//***********************************************************************************
//***********************************************************************************

void BeamPointRigidContactLM3DCondition::CalculateAndAddKuug(MatrixType &rLeftHandSideMatrix,
                                                             ConditionVariables &rVariables,
                                                             double &rIntegrationWeight)

{
  KRATOS_TRY

  const SizeType dimension = GetGeometry().WorkingSpaceDimension();
  const SizeType dofs_size = this->GetNodeDofsSize();

  if (rVariables.Options.Is(ACTIVE))
  {
    MatrixType Kuug = ZeroMatrix(dofs_size, dofs_size);

    for (SizeType i = 0; i < dimension + 1; i++)
    {
      Kuug(i, dimension * 2) = -rVariables.Surface.Normal[i];
      Kuug(dimension * 2, i) = -rVariables.Surface.Normal[i];
    }

    //Stabilization term
    double penalty = 1;
    double stab_LM = -rVariables.Gap.Normal * rVariables.Gap.Normal * penalty;

    Kuug(dofs_size-1, dofs_size-1) = stab_LM;


    this->CalculateAndAddKuugTangent( Kuug, rVariables, rIntegrationWeight );

    //Building the Local Stiffness Matrix
    BeamMathUtilsType::AddMatrix(rLeftHandSideMatrix, Kuug, 0, 0);
    //
    // std::cout<<std::endl;
    //std::cout<<" Kcont "<<rLeftHandSideMatrix<<std::endl
  }
  else
  {
    rLeftHandSideMatrix = ZeroMatrix(dofs_size, dofs_size);

    //to avoid system with a zero row and column
    rLeftHandSideMatrix(dofs_size-1, dofs_size-1) = 1;
  }

  KRATOS_CATCH("")
}

//************* Tangent Contact Force constitutive matrix      **********************
//***********************************************************************************

void BeamPointRigidContactLM3DCondition::CalculateAndAddKuugTangent(MatrixType &rLeftHandSideMatrix, ConditionVariables &rVariables, double &rIntegrationWeight)
{
}

//***********************************************************************************
//***********************************************************************************

void BeamPointRigidContactLM3DCondition::CalculateAndAddContactForces(VectorType &rRightHandSideVector,
                                                                      ConditionVariables &rVariables,
                                                                      double &rIntegrationWeight)
{
  KRATOS_TRY

  const SizeType dofs_size = this->GetNodeDofsSize();

  if (rVariables.Options.Is(ACTIVE))
  {
    this->CalculateAndAddNormalContactForce(rRightHandSideVector, rVariables, rIntegrationWeight);
    this->CalculateAndAddTangentContactForce( rRightHandSideVector, rVariables, rIntegrationWeight );
  }
  else
  {
    rRightHandSideVector = ZeroVector(dofs_size);
  }

  KRATOS_CATCH("")
}

//**************************** Calculate Normal Contact Force ***********************
//***********************************************************************************

void BeamPointRigidContactLM3DCondition::CalculateAndAddNormalContactForce(VectorType &rRightHandSideVector,
                                                                           ConditionVariables &rVariables,
                                                                           double &rIntegrationWeight)
{
  KRATOS_TRY

  const SizeType dimension = GetGeometry().WorkingSpaceDimension();

  double &NormalLM = GetGeometry()[0].FastGetSolutionStepValue(LAGRANGE_MULTIPLIER_NORMAL);

  double penalty = 1;

  rRightHandSideVector[dimension * 2] = rVariables.Gap.Normal * (1.0 + NormalLM * rVariables.Gap.Normal * penalty);

  GetGeometry()[0].SetLock();
  array_1d<double, 3> &ContactForce = GetGeometry()[0].FastGetSolutionStepValue(CONTACT_FORCE);
  for (SizeType j = 0; j < dimension; j++)
  {
    ContactForce[j] = NormalLM * rVariables.Surface.Normal[j];
    rRightHandSideVector[j] += ContactForce[j];
  }

  if (NormalLM > 0)
    ContactForce.clear();
  GetGeometry()[0].UnSetLock();

  //std::cout<<"[ID: "<<GetGeometry()[0].Id()<<"]:  Normal "<<rVariables.Surface.Normal<<" Gap "<<rVariables.Gap.Normal<<" LM_N "<<NormalLM<<std::endl;

  KRATOS_CATCH("")
}

//**************************** Calculate Tangent Contact Force **********************
//***********************************************************************************

void BeamPointRigidContactLM3DCondition::CalculateAndAddTangentContactForce(VectorType &rRightHandSideVector,
                                                                            ConditionVariables &rVariables,
                                                                            double &rIntegrationWeight)
{
}

} // Namespace Kratos
