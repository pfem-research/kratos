//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                  July 2013 $
//
//

#if !defined(KRATOS_BEAM_POINT_RIGID_CONTACT_LM_3D_CONDITION_HPP_INCLUDED)
#define KRATOS_BEAM_POINT_RIGID_CONTACT_LM_3D_CONDITION_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_conditions/beam_contact/beam_point_rigid_contact_penalty_3D_condition.hpp"

namespace Kratos
{

///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
*/
class KRATOS_API(CONTACT_MECHANICS_APPLICATION) BeamPointRigidContactLM3DCondition
    : public BeamPointRigidContactPenalty3DCondition
{
public:
    ///@name Type Definitions
    typedef BeamPointRigidContactPenalty3DCondition BaseType;

    typedef BaseType::NodeType NodeType;
    typedef BaseType::PointType PointType;
    typedef BaseType::SizeType SizeType;
    typedef BaseType::NodeWeakPtrVectorType NodeWeakPtrVectorType;
    typedef BaseType::ElementWeakPtrVectorType ElementWeakPtrVectorType;
    typedef BaseType::ConditionWeakPtrVectorType ConditionWeakPtrVectorType;

    typedef BaseType::BeamMathUtilsType BeamMathUtilsType;

    ///@{
    // Counted pointer of BeamPointRigidContactCondition
    KRATOS_CLASS_INTRUSIVE_POINTER_DEFINITION(BeamPointRigidContactLM3DCondition);
    ///@}

    ///@}
    ///@name Life Cycle
    ///@{

    /// Serialization constructor
    BeamPointRigidContactLM3DCondition(){};

    /// Default constructor.
    BeamPointRigidContactLM3DCondition(IndexType NewId, GeometryType::Pointer pGeometry);

    BeamPointRigidContactLM3DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties);

    BeamPointRigidContactLM3DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties, SpatialBoundingBox::Pointer pRigidWall);

    /// Copy constructor
    BeamPointRigidContactLM3DCondition(BeamPointRigidContactLM3DCondition const &rOther);

    /// Destructor.
    virtual ~BeamPointRigidContactLM3DCondition();

    ///@}
    ///@name Operators
    ///@{

    ///@}
    ///@name Operations
    ///@{

    /**
     * creates a new condition pointer
     * @param NewId: the ID of the new condition
     * @param ThisNodes: the nodes of the new condition
     * @param pProperties: the properties assigned to the new condition
     * @return a Pointer to the new condition
     */
    Condition::Pointer Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const override;


    /**
     * clones the selected condition variables, creating a new one
     * @param NewId: the ID of the new condition
     * @param ThisNodes: the nodes of the new condition
     * @param pProperties: the properties assigned to the new condition
     * @return a Pointer to the new condition
     */
    Condition::Pointer Clone(IndexType NewId,
                             NodesArrayType const &ThisNodes) const override;

    //************* GETTING METHODS

    /**
     * Sets on rConditionDofList the degrees of freedom of the considered element geometry
     */
    void GetDofList(DofsVectorType &rConditionDofList,
                    const ProcessInfo &rCurrentProcessInfo) const override;

    /**
     * Sets on rResult the ID's of the element degrees of freedom
     */
    void EquationIdVector(EquationIdVectorType &rResult,
                          const ProcessInfo &rCurrentProcessInfo) const override;

    /**
     * Sets on rValues the nodal displacements
     */
    void GetValuesVector(Vector &rValues,
                         int Step = 0) const override;

    /**
     * Sets on rValues the nodal velocities
     */
    void GetFirstDerivativesVector(Vector &rValues,
                                   int Step = 0) const override;

    /**
     * Sets on rValues the nodal accelerations
     */
    void GetSecondDerivativesVector(Vector &rValues,
                                    int Step = 0) const override;

    ///@}
    ///@name Access
    ///@{
    ///@}
    ///@name Inquiry
    ///@{
    ///@}
    ///@name Input and output
    ///@{
    ///@}
    ///@name Friends
    ///@{
    ///@}

protected:
    ///@name Protected static Member Variables
    ///@{
    ///@}
    ///@name Protected member Variables
    ///@{
    ///@}
    ///@name Protected Operators
    ///@{
    ///@}
    ///@name Protected Operations
    ///@{

    /**
     * Get dof size of a node
     */
    SizeType GetNodeDofsSize() const override;

    /**
     * Calculate Condition Kinematics
     */
    void CalculateKinematics(ConditionVariables &rVariables,
                             const ProcessInfo &rCurrentProcessInfo,
                             const double &rPointNumber) override;

    /**
     * Calculation of the Load Stiffness Matrix which usually is subtracted to the global stiffness matrix
     */
    void CalculateAndAddKuug(MatrixType &rLeftHandSideMatrix,
                             ConditionVariables &rVariables,
                             double &rIntegrationWeight) override;

    void CalculateAndAddKuugTangent(MatrixType &rLeftHandSideMatrix,
                                    ConditionVariables &rVariables,
                                    double &rIntegrationWeight) override;

    /**
     * Calculation of the External Forces Vector for a force or pressure vector
     */
    void CalculateAndAddContactForces(Vector &rRightHandSideVector,
                                      ConditionVariables &rVariables,
                                      double &rIntegrationWeight) override;

    void CalculateAndAddNormalContactForce(Vector &rRightHandSideVector, ConditionVariables &rVariables, double &rIntegrationWeight) override;

    void CalculateAndAddTangentContactForce(Vector &rRightHandSideVector, ConditionVariables &rVariables, double &rIntegrationWeight) override;

    ///@}
    ///@name Protected  Access
    ///@{
    ///@}
    ///@name Protected Inquiry
    ///@{
    ///@}
    ///@name Protected LifeCycle
    ///@{
    ///@}

private:
    ///@name Static Member Variables
    ///@{
    ///@}
    ///@name Member Variables
    ///@{
    ///@}
    ///@name Private Operators
    ///@{
    ///@}
    ///@name Private Operations
    ///@{
    ///@}
    ///@name Private  Access
    ///@{
    ///@}
    ///@name Private Inquiry
    ///@{
    ///@}
    ///@name Serialization
    ///@{

    friend class Serializer;

    virtual void save(Serializer &rSerializer) const override
    {
        KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BeamPointRigidContactPenalty3DCondition)
    }

    virtual void load(Serializer &rSerializer) override
    {
        KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BeamPointRigidContactPenalty3DCondition)
    }

}; // Class BeamPointRigidContactLM3DCondition

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
///@}

} // namespace Kratos.

#endif // KRATOS_POINT_RIGID_CONTACT_LM_3D_CONDITION_HPP_INCLUDED  defined
