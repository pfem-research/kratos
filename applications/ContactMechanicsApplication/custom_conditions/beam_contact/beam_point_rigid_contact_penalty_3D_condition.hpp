//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                  July 2013 $
//
//

#if !defined(KRATOS_BEAM_POINT_RIGID_CONTACT_PENALTY_3D_CONDITION_HPP_INCLUDED)
#define KRATOS_BEAM_POINT_RIGID_CONTACT_PENALTY_3D_CONDITION_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_conditions/beam_contact/beam_point_rigid_contact_condition.hpp"

namespace Kratos
{

///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
*/
class KRATOS_API(CONTACT_MECHANICS_APPLICATION) BeamPointRigidContactPenalty3DCondition
    : public BeamPointRigidContactCondition
{
public:
    ///@name Type Definitions
    typedef BeamPointRigidContactCondition BaseType;

    typedef BaseType::NodeType NodeType;
    typedef BaseType::PointType PointType;
    typedef BaseType::SizeType SizeType;
    typedef BaseType::NodeWeakPtrVectorType NodeWeakPtrVectorType;
    typedef BaseType::ElementWeakPtrVectorType ElementWeakPtrVectorType;
    typedef BaseType::ConditionWeakPtrVectorType ConditionWeakPtrVectorType;

    typedef BaseType::BeamMathUtilsType BeamMathUtilsType;

    ///@{
    // Counted pointer of BeamPointRigidContactCondition
    KRATOS_CLASS_INTRUSIVE_POINTER_DEFINITION(BeamPointRigidContactPenalty3DCondition);
    ///@}

    ///@}
    ///@name Life Cycle
    ///@{

    /// Serialization constructor
    BeamPointRigidContactPenalty3DCondition(){};

    /// Default constructor.
    BeamPointRigidContactPenalty3DCondition(IndexType NewId, GeometryType::Pointer pGeometry);

    BeamPointRigidContactPenalty3DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties);

    BeamPointRigidContactPenalty3DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties, SpatialBoundingBox::Pointer pRigidWall);

    /// Copy constructor
    BeamPointRigidContactPenalty3DCondition(BeamPointRigidContactPenalty3DCondition const &rOther);

    /// Destructor.
    virtual ~BeamPointRigidContactPenalty3DCondition();

    ///@}
    ///@name Operators
    ///@{

    ///@}
    ///@name Operations
    ///@{

    /**
     * creates a new condition pointer
     * @param NewId: the ID of the new condition
     * @param ThisNodes: the nodes of the new condition
     * @param pProperties: the properties assigned to the new condition
     * @return a Pointer to the new condition
     */
    Condition::Pointer Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const override;


    /**
     * clones the selected condition variables, creating a new one
     * @param NewId: the ID of the new condition
     * @param ThisNodes: the nodes of the new condition
     * @param pProperties: the properties assigned to the new condition
     * @return a Pointer to the new condition
     */
    Condition::Pointer Clone(IndexType NewId,
                             NodesArrayType const &ThisNodes) const override;


    ///@}
    ///@name Access
    ///@{
    ///@}
    ///@name Inquiry
    ///@{
    ///@}
    ///@name Input and output
    ///@{
    ///@}
    ///@name Friends
    ///@{
    ///@}

protected:
    ///@name Protected static Member Variables
    ///@{
    ///@}
    ///@name Protected member Variables
    ///@{
    ///@}
    ///@name Protected Operators
    ///@{
    ///@}
    ///@name Protected Operations
    ///@{


    /**
     * Calculate Condition Kinematics
     */
    void CalculateKinematics(ConditionVariables &rVariables,
                             const ProcessInfo &rCurrentProcessInfo,
                             const double &rPointNumber) override;

    /**
     * Calculation of the Load Stiffness Matrix which usually is subtracted to the global stiffness matrix
     */
    void CalculateAndAddKuug(MatrixType &rLeftHandSideMatrix,
                             ConditionVariables &rVariables,
                             double &rIntegrationWeight) override;

    virtual void CalculateAndAddKuugTangent(MatrixType &rLeftHandSideMatrix,
                                            ConditionVariables &rVariables,
                                            double &rIntegrationWeight);

    /**
     * Calculation of the External Forces Vector for a force or pressure vector
     */
    void CalculateAndAddContactForces(Vector &rRightHandSideVector,
                                      ConditionVariables &rVariables,
                                      double &rIntegrationWeight) override;

    virtual void CalculateAndAddNormalContactForce(Vector &rRightHandSideVector, ConditionVariables &rVariables, double &rIntegrationWeight);

    virtual void CalculateAndAddTangentContactForce(Vector &rRightHandSideVector, ConditionVariables &rVariables, double &rIntegrationWeight);

    virtual double &CalculateNormalForceModulus(double &rNormalForceModulus, ConditionVariables &rVariables);

    virtual void CalculateTangentRelativeMovement(ConditionVariables &rVariables);

    /**
     * Calculation of the Contact Force Factors
     */
    virtual void CalculateContactFactors(ConditionVariables &rContact);

    ///@}
    ///@name Protected  Access
    ///@{
    ///@}
    ///@name Protected Inquiry
    ///@{
    ///@}
    ///@name Protected LifeCycle
    ///@{
    ///@}

private:
    ///@name Static Member Variables
    ///@{
    ///@}
    ///@name Member Variables
    ///@{
    ///@}
    ///@name Private Operators
    ///@{
    ///@}
    ///@name Private Operations
    ///@{
    ///@}
    ///@name Private  Access
    ///@{
    ///@}
    ///@name Private Inquiry
    ///@{
    ///@}
    ///@name Serialization
    ///@{

    friend class Serializer;

    virtual void save(Serializer &rSerializer) const override
    {
        KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, BeamPointRigidContactCondition)
    }

    virtual void load(Serializer &rSerializer) override
    {
        KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, BeamPointRigidContactCondition)
    }

}; // Class BeamPointRigidContactPenalty3DCondition

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
///@}

} // namespace Kratos.

#endif // KRATOS_POINT_RIGID_CONTACT_PENALTY_3D_CONDITION_HPP_INCLUDED  defined
