//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                  July 2013 $
//
//

// System includes

// External includes

// Project includes
#include "custom_conditions/beam_contact/beam_point_rigid_contact_penalty_3D_condition.hpp"
namespace Kratos
{
//************************************************************************************
//************************************************************************************
BeamPointRigidContactPenalty3DCondition::BeamPointRigidContactPenalty3DCondition(IndexType NewId, GeometryType::Pointer
                                                                                                      pGeometry)
    : BeamPointRigidContactCondition(NewId, pGeometry)
{
  //DO NOT ADD DOFS HERE!!!
}

//************************************************************************************
//************************************************************************************
BeamPointRigidContactPenalty3DCondition::BeamPointRigidContactPenalty3DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    : BeamPointRigidContactCondition(NewId, pGeometry, pProperties)
{
}

//************************************************************************************
//************************************************************************************
BeamPointRigidContactPenalty3DCondition::BeamPointRigidContactPenalty3DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties, SpatialBoundingBox::Pointer pRigidWall)
    : BeamPointRigidContactCondition(NewId, pGeometry, pProperties, pRigidWall)
{
}

//************************************************************************************
//************************************************************************************
BeamPointRigidContactPenalty3DCondition::BeamPointRigidContactPenalty3DCondition(BeamPointRigidContactPenalty3DCondition const &rOther)
    : BeamPointRigidContactCondition(rOther)
{
}

//************************************************************************************
//************************************************************************************

Condition::Pointer BeamPointRigidContactPenalty3DCondition::Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const
{
  return Kratos::make_intrusive<BeamPointRigidContactPenalty3DCondition>(NewId, GetGeometry().Create(ThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************

Condition::Pointer BeamPointRigidContactPenalty3DCondition::Clone(IndexType NewId, NodesArrayType const &ThisNodes) const
{
  BeamPointRigidContactPenalty3DCondition ClonedCondition(NewId, GetGeometry().Create(ThisNodes), pGetProperties(), mpRigidWall);
  if (this->mpFrictionLaw!=nullptr)
    ClonedCondition.mpFrictionLaw = this->mpFrictionLaw->Clone();
  return Kratos::make_intrusive<BeamPointRigidContactPenalty3DCondition>(ClonedCondition);
}

//************************************************************************************
//************************************************************************************

BeamPointRigidContactPenalty3DCondition::~BeamPointRigidContactPenalty3DCondition()
{
}

//*********************************COMPUTE KINEMATICS*********************************
//************************************************************************************

void BeamPointRigidContactPenalty3DCondition::CalculateKinematics(ConditionVariables &rVariables,
                                                                  const ProcessInfo &rCurrentProcessInfo,
                                                                  const double &rPointNumber)
{
  KRATOS_TRY

  SpatialBoundingBox::BoxParametersType BoxParameters(this->GetGeometry()[0], rVariables.Gap.Normal, rVariables.Gap.Tangent, rVariables.Surface.Normal, rVariables.Surface.Tangent, rVariables.RelativeDisplacement);

  //to perform contact with a tube radius must be set
  BoxParameters.SetRadius(GetGeometry()[0].GetValue(CROSS_SECTION_RADIUS));

  if (this->mpRigidWall->IsInside(BoxParameters, rCurrentProcessInfo))
  {
    rVariables.Options.Set(ACTIVE, true);

    //consider positive gap
    rVariables.Gap.Normal = fabs(rVariables.Gap.Normal);

    //get contact properties and parameters
    this->CalculateContactFactors(rVariables);
  }
  else
  {
    rVariables.Options.Set(ACTIVE, false);
  }

  rVariables.DeltaTime = rCurrentProcessInfo[DELTA_TIME];

  PointRigidContactCondition::CalculateKinematics(rVariables, rCurrentProcessInfo, rPointNumber);

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void BeamPointRigidContactPenalty3DCondition::CalculateContactFactors(ConditionVariables &rVariables)
{

  KRATOS_TRY

  NodeWeakPtrVectorType &nNodes = GetGeometry()[0].GetValue(NEIGHBOUR_NODES);

  array_1d<double, 3> Contact_Point = GetGeometry()[0].Coordinates();
  array_1d<double, 3> Neighb_Point;

  double distance = 0;
  double counter = 0;

  for (auto &i_nnode : nNodes)
  {
    if (i_nnode.Is(BOUNDARY))
    {

      Neighb_Point = i_nnode.Coordinates();

      distance += norm_2(Contact_Point - Neighb_Point);

      counter++;
    }
  }

  if (counter != 0)
    distance /= counter;

  if (distance == 0)
    distance = 1;

  //get contact properties and parameters
  double PenaltyParameter = 1;
  if (GetProperties().Has(PENALTY_PARAMETER))
    PenaltyParameter = GetProperties()[PENALTY_PARAMETER];

  ElementWeakPtrVectorType &nElements = GetGeometry()[0].GetValue(NEIGHBOUR_ELEMENTS);
  double ElasticModulus = 0;
  if (GetProperties().Has(YOUNG_MODULUS))
    ElasticModulus = GetProperties()[YOUNG_MODULUS];
  else
    ElasticModulus = nElements.front().GetProperties()[YOUNG_MODULUS];


  double factor = 1; //4;
  if (distance < 1.0)
  { //take a number bigger than 1.0 (length units)
    int order = (int)((-1) * std::log10(distance) + 1);
    distance *= factor * pow(10, order);
  }

  //reduction of the penalty parameter:
  PenaltyParameter *= 1e-6;
  rVariables.Penalty.Normal = distance * PenaltyParameter * ElasticModulus;

  // to give the order defined by the PenaltyParameter*ElasticModulus
  int penalty_order = (int)((-1) * std::log10(rVariables.Penalty.Normal) + 1);
  penalty_order -= (int)((-1) * std::log10(PenaltyParameter * ElasticModulus) + 1);
  rVariables.Penalty.Normal *= pow(10, penalty_order);

  double PenaltyRatio = 1;
  if (GetProperties().Has(TANGENTIAL_PENALTY_RATIO))
    PenaltyRatio = GetProperties()[TANGENTIAL_PENALTY_RATIO];

  rVariables.Penalty.Tangent = rVariables.Penalty.Normal * PenaltyRatio;

  //std::cout<<" Node "<<GetGeometry()[0].Id()<<" Contact Factors "<<rVariables.Penalty.Normal<<" Gap Normal "<<rVariables.Gap.Normal<<" Gap Tangent "<<rVariables.Gap.Tangent<<" Surface.Normal "<<rVariables.Surface.Normal<<" Surface.Tangent "<<rVariables.Surface.Tangent<<" distance "<<distance<<" ElasticModulus "<<ElasticModulus<<" PenaltyParameter "<<PenaltyParameter<<std::endl;

  // std::cout<<" Penalty.Normal "<<rVariables.Penalty.Normal<<" Penalty.Tangent "<<rVariables.Penalty.Tangent<<std::endl;

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void BeamPointRigidContactPenalty3DCondition::CalculateAndAddKuug(MatrixType &rLeftHandSideMatrix,
                                                                  ConditionVariables &rVariables,
                                                                  double &rIntegrationWeight)

{
  KRATOS_TRY

  const SizeType dimension = GetGeometry().WorkingSpaceDimension();
  const SizeType dofs_size = this->GetDofsSize();

  if (rVariables.Options.Is(ACTIVE))
  {
    MatrixType Kuug(dimension, dimension);
    noalias(Kuug) = rVariables.Penalty.Normal * rIntegrationWeight * outer_prod(rVariables.Surface.Normal, rVariables.Surface.Normal);


    this->CalculateAndAddKuugTangent( Kuug, rVariables, rIntegrationWeight );

    //Building the Local Stiffness Matrix
    BeamMathUtilsType::AddMatrix(rLeftHandSideMatrix, Kuug, 0, 0);
    //
    // std::cout<<std::endl;
    // std::cout<<" Kcont "<<rLeftHandSideMatrix<<std::endl;
  }
  else
  {
    rLeftHandSideMatrix = ZeroMatrix(dofs_size, dofs_size);
  }

  //KRATOS_WATCH( rLeftHandSideMatrix )

  KRATOS_CATCH("")
}

//************* Tangent Contact Force constitutive matrix      **********************
//***********************************************************************************

void BeamPointRigidContactPenalty3DCondition::CalculateAndAddKuugTangent(MatrixType &rLeftHandSideMatrix, ConditionVariables &rVariables, double &rIntegrationWeight)
{
  KRATOS_TRY

  double NormalForceModulus = 0;
  NormalForceModulus = this->CalculateNormalForceModulus(NormalForceModulus, rVariables);

  this->CalculateTangentRelativeMovement(rVariables);

  double TangentForceModulus = this->CalculateCoulombsFrictionLaw(rVariables.Gap.Tangent, NormalForceModulus, rVariables);

  if (fabs(TangentForceModulus) >= 1e-25)
  {
    MatrixType Identity(3, 3);
    noalias(Identity) = IdentityMatrix(3);
    if (rVariables.Slip)
    {
      //simpler expression:
      noalias(rLeftHandSideMatrix) += rVariables.FrictionCoefficient * rVariables.Penalty.Normal * rIntegrationWeight * (outer_prod(rVariables.Surface.Tangent, rVariables.Surface.Normal));

      //added extra term:
      noalias(rLeftHandSideMatrix) += rVariables.FrictionCoefficient * rVariables.Penalty.Normal * rIntegrationWeight * (rVariables.Gap.Normal / rVariables.Gap.Tangent) * (Identity - outer_prod(rVariables.Surface.Normal, rVariables.Surface.Normal));
      noalias(rLeftHandSideMatrix) -= rVariables.FrictionCoefficient * rVariables.Penalty.Normal * rIntegrationWeight * (rVariables.Gap.Normal / rVariables.Gap.Tangent) * (outer_prod(rVariables.Surface.Tangent, rVariables.Surface.Tangent));
    }
    else
    {
      //simpler expression:
      noalias(rLeftHandSideMatrix) += rVariables.Penalty.Tangent * rIntegrationWeight * outer_prod(rVariables.Surface.Tangent, rVariables.Surface.Tangent);

      //added extra term:
      noalias(rLeftHandSideMatrix) += rVariables.Penalty.Tangent * rIntegrationWeight * (Identity - outer_prod(rVariables.Surface.Normal, rVariables.Surface.Normal));
    }

    //The geometric part of the torque contribution must be added here:
    //....


    //....
  }

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void BeamPointRigidContactPenalty3DCondition::CalculateAndAddContactForces(VectorType &rRightHandSideVector,
                                                                           ConditionVariables &rVariables,
                                                                           double &rIntegrationWeight)
{
  KRATOS_TRY

  const SizeType dofs_size = this->GetDofsSize();

  if (rVariables.Options.Is(ACTIVE))
  {
    this->CalculateAndAddNormalContactForce(rRightHandSideVector, rVariables, rIntegrationWeight);
    this->CalculateAndAddTangentContactForce(rRightHandSideVector, rVariables, rIntegrationWeight);
  }
  else
  {
    rRightHandSideVector = ZeroVector(dofs_size);
  }

  KRATOS_CATCH("")
}

//**************************** Calculate Normal Contact Force ***********************
//***********************************************************************************

void BeamPointRigidContactPenalty3DCondition::CalculateAndAddNormalContactForce(VectorType &rRightHandSideVector,
                                                                                ConditionVariables &rVariables,
                                                                                double &rIntegrationWeight)
{
    const SizeType dimension = GetGeometry().WorkingSpaceDimension();

    double NormalForceModulus = 0;
    NormalForceModulus = this->CalculateNormalForceModulus(NormalForceModulus, rVariables);

    NormalForceModulus *= rIntegrationWeight;

    for (SizeType j = 0; j < dimension; j++)
    {
        rRightHandSideVector[j] = NormalForceModulus * rVariables.Surface.Normal[j];
    }

    GetGeometry()[0].SetLock();
    array_1d<double, 3> &ContactForce = GetGeometry()[0].FastGetSolutionStepValue(CONTACT_FORCE);
    for (SizeType j = 0; j < dimension; j++)
    {
        ContactForce[j] = rRightHandSideVector[j];
    }
    GetGeometry()[0].UnSetLock();
}

//**************************** Calculate Tangent Contact Force **********************
//***********************************************************************************

void BeamPointRigidContactPenalty3DCondition::CalculateAndAddTangentContactForce(VectorType &rRightHandSideVector,
                                                                                 ConditionVariables &rVariables,
                                                                                 double &rIntegrationWeight)
{
  const SizeType &dimension = GetGeometry().WorkingSpaceDimension();

  double NormalForceModulus = 0;
  NormalForceModulus = this->CalculateNormalForceModulus(NormalForceModulus, rVariables);

  this->CalculateTangentRelativeMovement(rVariables);

  double TangentForceModulus = this->CalculateCoulombsFrictionLaw(rVariables.Gap.Tangent, NormalForceModulus, rVariables);

  TangentForceModulus *= rIntegrationWeight;

  GetGeometry()[0].SetLock();

  array_1d<double, 3> &ContactForce = GetGeometry()[0].FastGetSolutionStepValue(CONTACT_FORCE);

  //Contact tangent force in beam center
  for (SizeType i = 0; i < dimension; ++i)
  {
    rRightHandSideVector[i] += TangentForceModulus * rVariables.Surface.Tangent[i];
    ContactForce[i] += TangentForceModulus * rVariables.Surface.Tangent[i];
  }

  // adding moment due to contact
  double SectionMeanRadius = GetGeometry()[0].GetValue(CROSS_SECTION_RADIUS); //GetProperties()[CROSS_SECTION_RADIUS];
  PointType RadiusVector = SectionMeanRadius * rVariables.Surface.Normal;
  PointType ContactTorque;
  MathUtils<double>::CrossProduct(ContactTorque, rVariables.Surface.Tangent, RadiusVector);
  ContactTorque *= TangentForceModulus;

  //std::cout<<" [ContactTorque]: "<<ContactTorque<<" [TangentForceModulus]: "<<TangentForceModulus<<std::endl;

  //Contact torque due to contact tangent force on beam surface
  for (SizeType i = dimension; i < (dimension * 2); ++i)
  {
    rRightHandSideVector[i] += ContactTorque[i-dimension];
  }

  GetGeometry()[0].UnSetLock();
}

//**************************** Calculate Normal Force Modulus ***********************
//***********************************************************************************

double &BeamPointRigidContactPenalty3DCondition::CalculateNormalForceModulus(double &rNormalForceModulus, ConditionVariables &rVariables)
{
  KRATOS_TRY

  rNormalForceModulus = (rVariables.Penalty.Normal * rVariables.Gap.Normal);

  return rNormalForceModulus;

  KRATOS_CATCH("")
}

//**************************** Calculate Tangent Force Modulus **********************
//***********************************************************************************

void BeamPointRigidContactPenalty3DCondition::CalculateTangentRelativeMovement(ConditionVariables &rVariables)
{
  const SizeType dimension = GetGeometry().WorkingSpaceDimension();

  const array_1d<double, 3> &CurrentDisplacement = GetGeometry()[0].FastGetSolutionStepValue(DISPLACEMENT);
  const array_1d<double, 3> &PreviousDisplacement = GetGeometry()[0].FastGetSolutionStepValue(DISPLACEMENT, 1);
  array_1d<double, 3> DeltaDisplacement = CurrentDisplacement - PreviousDisplacement;

  //Calculate the relative rotation
  const array_1d<double, 3> &CurrentRotation = GetGeometry()[0].FastGetSolutionStepValue(ROTATION);
  const array_1d<double, 3> &PreviousRotation = GetGeometry()[0].FastGetSolutionStepValue(ROTATION, 1);
  array_1d<double, 3> DeltaRotation = CurrentRotation - PreviousRotation;

  double SectionMeanRadius = GetGeometry()[0].GetValue(CROSS_SECTION_RADIUS); //GetProperties()[CROSS_SECTION_RADIUS];
  array_1d<double, 3> RadiusVector;
  for (SizeType i = 0; i < 3; i++)
  {
    RadiusVector[i] = SectionMeanRadius * rVariables.Surface.Normal[i];
  }

  array_1d<double, 3> DeltaRotationDisplacement;
  MathUtils<double>::CrossProduct(DeltaRotationDisplacement, DeltaRotation, RadiusVector);

  // bool Regularization = false;
  // if( Regularization == true ){
  // 	 //regularization taking the contigous boundary segments
  // 	 NodeWeakPtrVectorType& nNodes = GetGeometry()[0].GetValue(NEIGHBOUR_NODES);
  // 	 array_1d<double, 3 > NeighbDeltaDisplacement;
  // 	 double counter = 0;
  // 	 for(auto& i_nnode : nNodes)
  // 	   {
  // 	     if(i_nnode.Is(BOUNDARY)){
  // 	       const array_1d<double, 3> & NeighbCurrentDisplacement  =  GetGeometry()[0].FastGetSolutionStepValue(DISPLACEMENT);
  // 	       const array_1d<double, 3> & NeighbPreviousDisplacement =  GetGeometry()[0].FastGetSolutionStepValue(DISPLACEMENT, 1);
  // 	       array_1d<double, 3 > NeighbDeltaDisplacement           =  NeighbCurrentDisplacement-NeighbPreviousDisplacement;
  // 	       DeltaDisplacement += NeighbDeltaDisplacement;
  // 	       counter ++;
  // 	     }
  // 	   }
  //        if( counter!= 0)
  // 	    DeltaDisplacement /= counter;
  // }

  PointType WallDisplacement = rVariables.DeltaTime * this->mpRigidWall->GetVelocity();
  PointType TotalTangentRelativeMovement;

  for (SizeType i = 0; i < dimension; ++i)
  {
    TotalTangentRelativeMovement[i] = DeltaDisplacement[i] + DeltaRotationDisplacement[i] - WallDisplacement[i];
  }

  rVariables.Gap.Tangent = MathUtils<double>::Norm3(TotalTangentRelativeMovement);

  if (rVariables.Gap.Tangent != 0)
    rVariables.Surface.Tangent = TotalTangentRelativeMovement / rVariables.Gap.Tangent;

}

} // Namespace Kratos
