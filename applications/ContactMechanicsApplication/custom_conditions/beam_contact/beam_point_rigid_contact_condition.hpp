//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                August 2020 $
//
//

#if !defined(KRATOS_BEAM_POINT_RIGID_CONTACT_CONDITION_HPP_INCLUDED)
#define KRATOS_BEAM_POINT_RIGID_CONTACT_CONDITION_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_conditions/rigid_contact/point_rigid_contact_condition.hpp"
#include "utilities/beam_math_utilities.hpp"

namespace Kratos
{
///@name Kratos Globals
///@{
///@}
///@name Type Definitions
///@{
///@}
///@name  Enum's
///@{
///@}
///@name  Functions
///@{
///@}
///@name Kratos Classes
///@{

/// Beam Point Rigid Contact Condition for 3D and 2D geometries. (base class)

/**
 * Implements a Contact Point Load definition for beam nodes
 * This works for arbitrary geometries in 3D and 2D (base class)
 */
class KRATOS_API(CONTACT_MECHANICS_APPLICATION) BeamPointRigidContactCondition
    : public PointRigidContactCondition
{
public:
  ///@name Type Definitions
  typedef PointRigidContactCondition BaseType;

  typedef BaseType::NodeType NodeType;
  typedef BaseType::PointType PointType;
  typedef BaseType::SizeType SizeType;
  typedef BaseType::NodeWeakPtrVectorType NodeWeakPtrVectorType;
  typedef BaseType::ElementWeakPtrVectorType ElementWeakPtrVectorType;
  typedef BaseType::ConditionWeakPtrVectorType ConditionWeakPtrVectorType;

  typedef BeamMathUtils<double> BeamMathUtilsType;

  ///@{
  // Counted pointer of BeamPointRigidContactCondition
  KRATOS_CLASS_INTRUSIVE_POINTER_DEFINITION(BeamPointRigidContactCondition);
  ///@}


public:
  ///@name Life Cycle
  ///@{

  /// Serialization constructor
  BeamPointRigidContactCondition(){};

  /// Default constructor.
  BeamPointRigidContactCondition(IndexType NewId, GeometryType::Pointer pGeometry);

  BeamPointRigidContactCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties);

  BeamPointRigidContactCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties, SpatialBoundingBox::Pointer pRigidWall);

  /// Copy constructor
  BeamPointRigidContactCondition(BeamPointRigidContactCondition const &rOther);

  /// Destructor
  virtual ~BeamPointRigidContactCondition();

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /**
     * creates a new condition pointer
     * @param NewId: the ID of the new condition
     * @param ThisNodes: the nodes of the new condition
     * @param pProperties: the properties assigned to the new condition
     * @return a Pointer to the new condition
     */
  Condition::Pointer Create(IndexType NewId,
                            NodesArrayType const &ThisNodes,
                            PropertiesType::Pointer pProperties) const override;

  /**
     * clones the selected condition variables, creating a new one
     * @param NewId: the ID of the new condition
     * @param ThisNodes: the nodes of the new condition
     * @param pProperties: the properties assigned to the new condition
     * @return a Pointer to the new condition
     */
  Condition::Pointer Clone(IndexType NewId,
                           NodesArrayType const &ThisNodes) const override;

  //************* STARTING - ENDING  METHODS

  /**
     * Called at the beginning
     */
  void Initialize(const ProcessInfo &rCurrentProcessInfo) override;

  /**
     * Called at the beginning of each solution step
     */
  void InitializeSolutionStep(const ProcessInfo &rCurrentProcessInfo) override;


  /**
     * Called at the beginning of each iteration
     */
  void InitializeNonLinearIteration(const ProcessInfo &rCurrentProcessInfo) override;

  /**
     * Called at the end of each iteration
     */
  void FinalizeNonLinearIteration(const ProcessInfo &rCurrentProcessInfo) override;


  /**
     * Called at the end of each solution step
     */
  void FinalizeSolutionStep(const ProcessInfo &rCurrentProcessInfo) override;



  //************* GETTING METHODS


  /**
     * Sets on rConditionDofList the degrees of freedom of the considered element geometry
     */
  void GetDofList(DofsVectorType &rConditionDofList,
                  const ProcessInfo &rCurrentProcessInfo) const override;

  /**
     * Sets on rResult the ID's of the element degrees of freedom
     */
  void EquationIdVector(EquationIdVectorType &rResult,
                        const ProcessInfo &rCurrentProcessInfo) const override;

  /**
     * Sets on rValues the nodal displacements
     */
  void GetValuesVector(Vector &rValues,
                       int Step = 0) const override;

  /**
     * Sets on rValues the nodal velocities
     */
  void GetFirstDerivativesVector(Vector &rValues,
                                 int Step = 0) const override;

  /**
     * Sets on rValues the nodal accelerations
     */
  void GetSecondDerivativesVector(Vector &rValues,
                                  int Step = 0) const override;

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{
  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{

  /**
   * Get dof size of a node
   */
  SizeType GetNodeDofsSize() const override;

  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Serialization
  ///@{

  friend class Serializer;

  virtual void save(Serializer &rSerializer) const override
  {
    KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, PointRigidContactCondition)
  }

  virtual void load(Serializer &rSerializer) override
  {
    KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, PointRigidContactCondition)
  }

}; // class BeamPointRigidContactCondition.

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{
///@}

} // namespace Kratos.

#endif // KRATOS_BEAM_POINT_RIGID_CONTACT_CONDITION_HPP_INCLUDED defined
