//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                August 2020 $
//
//

// System includes

// External includes

// Project includes
#include "custom_conditions/beam_contact/beam_point_rigid_contact_condition.hpp"

namespace Kratos
{


//***********************************************************************************
//***********************************************************************************
BeamPointRigidContactCondition::BeamPointRigidContactCondition(IndexType NewId, GeometryType::Pointer pGeometry)
    :  PointRigidContactCondition(NewId, pGeometry)
{
  //DO NOT ADD DOFS HERE!!!
}

//***********************************************************************************
//***********************************************************************************
BeamPointRigidContactCondition::BeamPointRigidContactCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties)
    :  PointRigidContactCondition(NewId, pGeometry, pProperties)
{
}

//************************************************************************************
//************************************************************************************
BeamPointRigidContactCondition::BeamPointRigidContactCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties, SpatialBoundingBox::Pointer pRigidWall)
    : PointRigidContactCondition(NewId, pGeometry, pProperties)
{
}

//************************************************************************************
//************************************************************************************
BeamPointRigidContactCondition::BeamPointRigidContactCondition(BeamPointRigidContactCondition const &rOther)
     : PointRigidContactCondition(rOther)

{
}

//***********************************************************************************
//***********************************************************************************
Condition::Pointer BeamPointRigidContactCondition::Create(
    IndexType NewId,
    NodesArrayType const &ThisNodes,
    PropertiesType::Pointer pProperties) const
{
  return Kratos::make_intrusive<BeamPointRigidContactCondition>(NewId, GetGeometry().Create(ThisNodes), pProperties);
}

//************************************CLONE*******************************************
//************************************************************************************

Condition::Pointer BeamPointRigidContactCondition::Clone(IndexType NewId, NodesArrayType const &rThisNodes) const
{
   BeamPointRigidContactCondition ClonedCondition(NewId, GetGeometry().Create(rThisNodes), pGetProperties(), mpRigidWall);
   if (this->mpFrictionLaw!=nullptr)
     ClonedCondition.mpFrictionLaw = this->mpFrictionLaw->Clone();
   return Kratos::make_intrusive<BeamPointRigidContactCondition>(ClonedCondition);
}

//***********************************************************************************
//***********************************************************************************
BeamPointRigidContactCondition::~BeamPointRigidContactCondition()
{
}

//************* GETTING METHODS

//***********************************************************************************
//***********************************************************************************

void BeamPointRigidContactCondition::GetDofList(DofsVectorType &rConditionDofList,
                                                const ProcessInfo &rCurrentProcessInfo) const
{
  KRATOS_TRY

  rConditionDofList.resize(0);
  const SizeType number_of_nodes = GetGeometry().PointsNumber();
  const SizeType dimension = GetGeometry().WorkingSpaceDimension();

  for (SizeType i = 0; i < number_of_nodes; i++)
  {
    rConditionDofList.push_back(GetGeometry()[i].pGetDof(DISPLACEMENT_X));
    rConditionDofList.push_back(GetGeometry()[i].pGetDof(DISPLACEMENT_Y));

    if (dimension == 3)
    {
      rConditionDofList.push_back(GetGeometry()[i].pGetDof(DISPLACEMENT_Z));
      rConditionDofList.push_back(GetGeometry()[i].pGetDof(ROTATION_X));
      rConditionDofList.push_back(GetGeometry()[i].pGetDof(ROTATION_Y));
    }

    rConditionDofList.push_back(GetGeometry()[i].pGetDof(ROTATION_Z));
  }

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void BeamPointRigidContactCondition::EquationIdVector(EquationIdVectorType &rResult,
                                                      const ProcessInfo &rCurrentProcessInfo) const
{
  KRATOS_TRY

  const SizeType number_of_nodes = GetGeometry().PointsNumber();
  const SizeType dimension = GetGeometry().WorkingSpaceDimension();
  SizeType condition_size = number_of_nodes * this->GetNodeDofsSize();

  if (rResult.size() != condition_size)
    rResult.resize(condition_size, false);

  for (SizeType i = 0; i < number_of_nodes; i++)
  {
    int index = i * this->GetNodeDofsSize();
    rResult[index] = GetGeometry()[i].GetDof(DISPLACEMENT_X).EquationId();
    rResult[index + 1] = GetGeometry()[i].GetDof(DISPLACEMENT_Y).EquationId();

    if (dimension == 3)
    {
      rResult[index + 2] = GetGeometry()[i].GetDof(DISPLACEMENT_Z).EquationId();
      rResult[index + 3] = GetGeometry()[i].GetDof(ROTATION_X).EquationId();
      rResult[index + 4] = GetGeometry()[i].GetDof(ROTATION_Y).EquationId();
      rResult[index + 5] = GetGeometry()[i].GetDof(ROTATION_Z).EquationId();
    }
    else
      rResult[index + 2] = GetGeometry()[i].GetDof(ROTATION_Z).EquationId();


  }

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void BeamPointRigidContactCondition::GetValuesVector(Vector &rValues, int Step) const
{
  KRATOS_TRY

  const SizeType number_of_nodes = GetGeometry().PointsNumber();
  const SizeType dimension = GetGeometry().WorkingSpaceDimension();
  SizeType condition_size = number_of_nodes * this->GetNodeDofsSize();

  if (rValues.size() != condition_size)
    rValues.resize(condition_size, false);

  for (SizeType i = 0; i < number_of_nodes; i++)
  {
    SizeType index = i * this->GetNodeDofsSize();
    rValues[index] = GetGeometry()[i].FastGetSolutionStepValue(DISPLACEMENT_X, Step);
    rValues[index + 1] = GetGeometry()[i].FastGetSolutionStepValue(DISPLACEMENT_Y, Step);

    if (dimension == 3)
    {
      rValues[index + 2] = GetGeometry()[i].FastGetSolutionStepValue(DISPLACEMENT_Z, Step);
      rValues[index + 3] = GetGeometry()[i].FastGetSolutionStepValue(ROTATION_X, Step);
      rValues[index + 4] = GetGeometry()[i].FastGetSolutionStepValue(ROTATION_Y, Step);
      rValues[index + 5] = GetGeometry()[i].FastGetSolutionStepValue(ROTATION_Z, Step);
    }
    else
      rValues[index + 2] = GetGeometry()[i].FastGetSolutionStepValue(ROTATION_Z, Step);

  }

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void BeamPointRigidContactCondition::GetFirstDerivativesVector(Vector &rValues, int Step) const
{
  KRATOS_TRY

  const SizeType number_of_nodes = GetGeometry().PointsNumber();
  const SizeType dimension = GetGeometry().WorkingSpaceDimension();
  SizeType condition_size = number_of_nodes * this->GetNodeDofsSize();

  if (rValues.size() != condition_size)
    rValues.resize(condition_size, false);

  for (SizeType i = 0; i < number_of_nodes; i++)
  {
    SizeType index = i * this->GetNodeDofsSize();
    rValues[index] = GetGeometry()[i].FastGetSolutionStepValue(VELOCITY_X, Step);
    rValues[index + 1] = GetGeometry()[i].FastGetSolutionStepValue(VELOCITY_Y, Step);

    if (dimension == 3)
    {
      rValues[index + 2] = GetGeometry()[i].FastGetSolutionStepValue(VELOCITY_Z, Step);
      rValues[index + 3] = GetGeometry()[i].FastGetSolutionStepValue(ANGULAR_VELOCITY_X, Step);
      rValues[index + 4] = GetGeometry()[i].FastGetSolutionStepValue(ANGULAR_VELOCITY_Y, Step);
      rValues[index + 5] = GetGeometry()[i].FastGetSolutionStepValue(ANGULAR_VELOCITY_Z, Step);
    }
    else
      rValues[index + 2] = GetGeometry()[i].FastGetSolutionStepValue(ANGULAR_VELOCITY_Z, Step);
  }

  KRATOS_CATCH("")
}

//***********************************************************************************
//***********************************************************************************

void BeamPointRigidContactCondition::GetSecondDerivativesVector(Vector &rValues, int Step) const
{
  KRATOS_TRY

  const SizeType number_of_nodes = GetGeometry().PointsNumber();
  const SizeType dimension = GetGeometry().WorkingSpaceDimension();
  SizeType condition_size = number_of_nodes * this->GetNodeDofsSize();

  if (rValues.size() != condition_size)
    rValues.resize(condition_size, false);

  for (SizeType i = 0; i < number_of_nodes; i++)
  {
    SizeType index = i * this->GetNodeDofsSize();
    rValues[index] = GetGeometry()[i].FastGetSolutionStepValue(ACCELERATION_X, Step);
    rValues[index + 1] = GetGeometry()[i].FastGetSolutionStepValue(ACCELERATION_Y, Step);

    if (dimension == 3)
    {
      rValues[index + 2] = GetGeometry()[i].FastGetSolutionStepValue(ACCELERATION_Z, Step);
      rValues[index + 3] = GetGeometry()[i].FastGetSolutionStepValue(ANGULAR_ACCELERATION_X, Step);
      rValues[index + 4] = GetGeometry()[i].FastGetSolutionStepValue(ANGULAR_ACCELERATION_Y, Step);
      rValues[index + 5] = GetGeometry()[i].FastGetSolutionStepValue(ANGULAR_ACCELERATION_Z, Step);
    }
    else
      rValues[index + 2] = GetGeometry()[i].FastGetSolutionStepValue(ANGULAR_ACCELERATION_Z, Step);
  }
  KRATOS_CATCH("")
}


//************* STARTING - ENDING  METHODS
//***********************************************************************************
//***********************************************************************************

void BeamPointRigidContactCondition::Initialize(const ProcessInfo &rCurrentProcessInfo)
{
   KRATOS_TRY

   const SizeType dimension = GetGeometry().WorkingSpaceDimension();
   const SizeType voigt_size = dimension * (dimension + 1);

   mContactStressVector.resize(voigt_size);
   noalias(mContactStressVector) = ZeroVector(voigt_size);

   mpFrictionLaw = GetProperties()[FRICTION_LAW]->Clone();

   KRATOS_CATCH("")
}


void BeamPointRigidContactCondition::InitializeSolutionStep(const ProcessInfo &rCurrentProcessInfo)
{
  KRATOS_TRY

  const SizeType dimension = GetGeometry().WorkingSpaceDimension();
  const SizeType voigt_size = dimension * (dimension + 1) * 0.5;

  if (mContactStressVector.size() != voigt_size)
  {
    mContactStressVector.resize(voigt_size);
    noalias(mContactStressVector) = ZeroVector(voigt_size);
  }

  KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************
void BeamPointRigidContactCondition::InitializeNonLinearIteration(const ProcessInfo &rCurrentProcessInfo)
{
   KRATOS_TRY

   ClearNodalForces();

   KRATOS_CATCH("")
}

//************************************************************************************
//************************************************************************************

void BeamPointRigidContactCondition::FinalizeNonLinearIteration(const ProcessInfo &rCurrentProcessInfo)
{
}

//************************************************************************************
//************************************************************************************

void BeamPointRigidContactCondition::FinalizeSolutionStep(const ProcessInfo &rCurrentProcessInfo)
{
}

//************************************************************************************
//************************************************************************************

 BeamPointRigidContactCondition::SizeType BeamPointRigidContactCondition::GetNodeDofsSize() const
{
  return (GetGeometry().WorkingSpaceDimension()*(GetGeometry().WorkingSpaceDimension()-1)); //usual size for displacement based elements
}

} // Namespace Kratos.
