//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:           JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:                  July 2013 $
//
//

#if !defined(KRATOS_BEAM_PRESSURE_CONDITION_HPP_INCLUDED)
#define KRATOS_BEAM_PRESSURE_CONDITION_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_conditions/beam_contact/beam_point_rigid_contact_condition.hpp"

namespace Kratos
{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
*/
class KRATOS_API(CONTACT_MECHANICS_APPLICATION) BeamPointPressureCondition
    : public Condition
{
protected:
public:
    ///@name Type Definitions

    ///Tensor order 1 definition
    typedef Vector VectorType;
    typedef Node<3>::Pointer PointPointerType;

    std::vector<PointPointerType> mNodesList;
    array_1d<double, 3> mForce;

    ///@{
    // Counted pointer of BeamPointRigidContactCondition
    KRATOS_CLASS_INTRUSIVE_POINTER_DEFINITION(BeamPointPressureCondition);
    ///@}

    /// Default constructor.
    BeamPointPressureCondition(IndexType NewId, GeometryType::Pointer pGeometry);

    BeamPointPressureCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties);

    BeamPointPressureCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties, SpatialBoundingBox::Pointer pRigidWall);

    /// Copy constructor
    BeamPointPressureCondition(BeamPointPressureCondition const &rOther);

    /// Destructor.
    virtual ~BeamPointPressureCondition();

    Condition::Pointer Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const override;
    virtual void InitializeSolutionStep(const ProcessInfo &rCurrentProcessInfo) override;
    virtual void InitializeNonLinearIteration(const ProcessInfo &rCurrentProcessInfo) override;
    void GetDofList(DofsVectorType &rConditionDofList, const ProcessInfo &rCurrentProcessInfo) const override;
    void EquationIdVector(EquationIdVectorType &rResult, const ProcessInfo &rCurrentProcessInfo) const override;
    void AddExplicitContribution(const VectorType &rRHSVector,
                                 const Variable<VectorType> &rRHSVariable,
                                 const Variable<array_1d<double, 3>> &rDestinationVariable,
                                 const ProcessInfo &rCurrentProcessInfo) override;
    void InitializeSystemMatrices(MatrixType &rLeftHandSideMatrix, VectorType &rRightHandSideVector);
    void CalculateRightHandSide(VectorType &rRightHandSideVector, const ProcessInfo &rCurrentProcessInfo) override;
    void CalculateLocalSystem(MatrixType &rLeftHandSideMatrix, VectorType &rRightHandSideVector, const ProcessInfo &rCurrentProcessInfo) override;
    void GetValuesVector(Vector &rValues, int Step) const override;
    void GetFirstDerivativesVector(Vector &rValues, int Step) const override;
    void GetSecondDerivativesVector(Vector &rValues, int Step) const override;

protected:
    BeamPointPressureCondition(){};

private:
    friend class Serializer;

    virtual void save(Serializer &rSerializer) const override
    {
        KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, Condition)
    }

    virtual void load(Serializer &rSerializer) override
    {
        KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, Condition)
    }

}; // Class BeamPointPressureCondition

} // namespace Kratos.

#endif // KRATOS_BEAM_PRESSURE_CONDITION_HPP_INCLUDED  defined
