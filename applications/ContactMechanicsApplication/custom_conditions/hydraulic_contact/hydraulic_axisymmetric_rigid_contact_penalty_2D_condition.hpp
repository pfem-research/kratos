//
//   Project Name:        KratosContactMechanicsApplication $
//   Developed by:        $Developer:             LMonforte $
//   Maintained by:       $Maintainer:                   LM $
//   Date:                $Date:               January 2018 $
//
//

#if !defined(KRATOS_HYDRAULIC_AXISYMMETRIC_RIGID_CONTACT_PENALTY_CONDITION_HPP_INCLUDED)
#define KRATOS_HYDRAULIC_AXISYMMETRIC_RIGID_CONTACT_PENALTY_CONDITION_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_conditions/hydraulic_contact/hydraulic_rigid_contact_penalty_3D_condition.hpp"

namespace Kratos
{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
*/
class KRATOS_API(CONTACT_MECHANICS_APPLICATION) HydraulicAxisymmetricRigidContactPenalty2DCondition
    : public HydraulicRigidContactPenalty3DCondition
{
public:
    ///@name Type Definitions

    ///Tensor order 1 definition
    typedef BoundedVector<double, 3> PointType;

    ///@{
    // Counted pointer of WaterPointRigidContactCondition
    KRATOS_CLASS_INTRUSIVE_POINTER_DEFINITION(HydraulicAxisymmetricRigidContactPenalty2DCondition);
    ///@}

    ///@name Life Cycle
    ///@{

    /// Serialization constructor
    HydraulicAxisymmetricRigidContactPenalty2DCondition(){};

    /// Default constructor.
    HydraulicAxisymmetricRigidContactPenalty2DCondition(IndexType NewId, GeometryType::Pointer pGeometry);

    HydraulicAxisymmetricRigidContactPenalty2DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties);

    HydraulicAxisymmetricRigidContactPenalty2DCondition(IndexType NewId, GeometryType::Pointer pGeometry, PropertiesType::Pointer pProperties, SpatialBoundingBox::Pointer pRigidWall);

    /// Copy constructor
    HydraulicAxisymmetricRigidContactPenalty2DCondition(HydraulicAxisymmetricRigidContactPenalty2DCondition const &rOther);

    /// Destructor.
    virtual ~HydraulicAxisymmetricRigidContactPenalty2DCondition();

    ///@}
    ///@name Operators
    ///@{

    ///@}
    ///@name Operations
    ///@{

    /**
     * creates a new condition pointer
     * @param NewId: the ID of the new condition
     * @param ThisNodes: the nodes of the new condition
     * @param pProperties: the properties assigned to the new condition
     * @return a Pointer to the new condition
     */
    Condition::Pointer Create(IndexType NewId, NodesArrayType const &ThisNodes, PropertiesType::Pointer pProperties) const override;

    /**
     * clones the selected condition variables, creating a new one
     * @param NewId: the ID of the new condition
     * @param ThisNodes: the nodes of the new condition
     * @param pProperties: the properties assigned to the new condition
     * @return a Pointer to the new condition
     */
    Condition::Pointer Clone(IndexType NewId,
                             NodesArrayType const &ThisNodes) const override;

    ///@name Access
    ///@{
    ///@}
    ///@name Inquiry
    ///@{
    ///@}
    ///@name Input and output
    ///@{
    ///@}
    ///@name Friends
    ///@{
    ///@}

protected:
    ///@name Protected static Member Variables
    ///@{

    ///@}
    ///@name Protected member Variables
    ///@{

    ///@}
    ///@name Protected Operators
    ///@{

    ///@}
    ///@name Protected Operations
    ///@{

    /**
     * Calculate Condition Kinematics
     */
    virtual void CalculateKinematics(ConditionVariables &rVariables,
                                     const ProcessInfo &rCurrentProcessInfo,
                                     const double &rPointNumber) override;

    /**
     * Calculation and addition of the matrices of the LHS
     */
    virtual void CalculateAndAddLHS(LocalSystemComponents &rLocalSystem,
                                    ConditionVariables &rVariables,
                                    double &rIntegrationWeight) override;

    /**
     * Calculation and addition of the vectors of the RHS
     */
    virtual void CalculateAndAddRHS(LocalSystemComponents &rLocalSystem,
                                    ConditionVariables &rVariables,
                                    double &rIntegrationWeight) override;

    /**
     * Calculate Radius:
     */
    void CalculateRadius(double &rCurrentRadius,
                         double &rReferenceRadius);

    ///@}
    ///@name Protected  Access
    ///@{
    ///@}
    ///@name Protected Inquiry
    ///@{
    ///@}
    ///@name Protected LifeCycle
    ///@{
    ///@}

private:
    ///@name Static Member Variables
    ///@{

    ///@}
    ///@name Member Variables
    ///@{

    ///@}
    ///@name Private Operators
    ///@{

    ///@}
    ///@name Private Operations
    ///@{

    ///@}
    ///@name Private  Access
    ///@{

    ///@}
    ///@name Private Inquiry
    ///@{

    ///@}
    ///@name Serialization
    ///@{

    friend class Serializer;

    virtual void save(Serializer &rSerializer) const override
    {
        KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, PointRigidContactCondition)
    }

    virtual void load(Serializer &rSerializer) override
    {
        KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, PointRigidContactCondition)
    }

}; // Class HydraulicAxisymmetricRigidContactPenalty2DCondition

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

/// input stream function
/*  inline std::istream& operator >> (std::istream& rIStream,
				    HydraulicAxisymmetricRigidContactPenalty2DCondition& rThis);
*/
/// output stream function
/*  inline std::ostream& operator << (std::ostream& rOStream,
				    const HydraulicAxisymmetricRigidContactPenalty2DCondition& rThis)
    {
      rThis.PrintInfo(rOStream);
      rOStream << std::endl;
      rThis.PrintData(rOStream);

      return rOStream;
    }*/
///@}

} // namespace Kratos.

#endif // KRATOS_HYDRAULIC_AXISYMMETRIC_RIGID_CONTACT_PENALTY_CONDITION_HPP_INCLUDED  defined
