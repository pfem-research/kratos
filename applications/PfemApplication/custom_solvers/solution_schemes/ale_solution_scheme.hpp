//
//   Project Name:        KratosPfemApplication             $
//   Developed by:        $Developer: IPouplana-JMCarbonell $
//   Maintained by:       $Maintainer:                  JMC $
//   Date:                $Date:               October 2016 $
//

#if !defined(KRATOS_ALE_SOLUTION_SCHEME_HPP_INCLUDED)
#define KRATOS_ALE_SOLUTION_SCHEME_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "includes/variables.h"
#include "includes/deprecated_variables.h"
#include "includes/cfd_variables.h"
#include "includes/node.h"
#include "geometries/geometry.h"

#include "pfem_application_variables.h"

#include "custom_solvers/solution_schemes/dynamic_scheme.hpp"
#include "custom_utilities/slip_coordinate_transformation.hpp"

namespace Kratos
{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Bossak time scheme for the incompressible flow problem.
/** This class provides a second order time scheme of the generalized-alpha Newmark
    family of methods. It also includes code required to implement slip conditions
    on the incompressible flow problem and provides the possibility of using a RANS
    model by passing a turbulence model as an argument to the constructor.
    This time scheme is intended to be used in combination with elements of type
    ASGS2D, ASGS3D, VMS or derived classes.
    To use the slip condition SLIP flag must be assigned to nodes. To use
    a wall law in combination with the slip condition, use MonolithicWallCondition to
    mesh the boundary
    @see ASGS2D, ASGS3D, VMS, MonolithicWallConditon
*/

template <class TSparseSpace, class TDenseSpace>
class AleSolutionScheme : public DynamicScheme<TSparseSpace, TDenseSpace>
{
public:
  ///@name Type Definitions
  ///@{
  KRATOS_CLASS_POINTER_DEFINITION(AleSolutionScheme);

  typedef SolutionScheme<TSparseSpace, TDenseSpace> BaseType;
  typedef typename BaseType::SolutionSchemePointerType BasePointerType;
  typedef typename BaseType::LocalFlagType LocalFlagType;

  typedef DynamicScheme<TSparseSpace, TDenseSpace> DerivedType;

  typedef typename BaseType::NodeType NodeType;
  typedef typename BaseType::DofsArrayType DofsArrayType;
  typedef typename BaseType::SystemMatrixType SystemMatrixType;
  typedef typename BaseType::SystemVectorType SystemVectorType;
  typedef typename BaseType::LocalSystemVectorType LocalSystemVectorType;
  typedef typename BaseType::LocalSystemMatrixType LocalSystemMatrixType;

  typedef typename BaseType::NodesContainerType NodesContainerType;
  typedef typename BaseType::ElementsContainerType ElementsContainerType;
  typedef typename BaseType::ConditionsContainerType ConditionsContainerType;

  typedef typename BaseType::IntegrationMethodsVectorType IntegrationMethodsVectorType;
  typedef typename BaseType::IntegrationMethodsScalarType IntegrationMethodsScalarType;

  typedef Geometry<NodeType> GeometryType;
  typedef typename GeometryType::SizeType SizeType;

  ///@}
  ///@name Life Cycle
  ///@{

  /// Constructor.
  AleSolutionScheme(IntegrationMethodsVectorType &rTimeVectorIntegrationMethods,
                    IntegrationMethodsScalarType &rTimeScalarIntegrationMethods)
      : DerivedType(rTimeVectorIntegrationMethods, rTimeScalarIntegrationMethods), mRotationTool()
  {
    mlagrangian_layers = 3;
  }

  /// Constructor.
  AleSolutionScheme(IntegrationMethodsVectorType &rTimeVectorIntegrationMethods,
                    IntegrationMethodsScalarType &rTimeScalarIntegrationMethods,
                    Flags &rOptions)
      : DerivedType(rTimeVectorIntegrationMethods, rTimeScalarIntegrationMethods, rOptions), mRotationTool()
  {
    mlagrangian_layers = 3;
  }

  /// Constructor.
  AleSolutionScheme(IntegrationMethodsVectorType &rTimeVectorIntegrationMethods,
                    IntegrationMethodsScalarType &rTimeScalarIntegrationMethods,
                    Flags &rOptions, int lagrangian_layers, bool eulerian_interaction_layer)
      : DerivedType(rTimeVectorIntegrationMethods, rTimeScalarIntegrationMethods, rOptions), mRotationTool()
  {
    mlagrangian_layers = lagrangian_layers;
    meulerian_interaction_layer = eulerian_interaction_layer;
  }

  // Class SolutionScheme
  ~AleSolutionScheme() override {}

  ///@}
  ///@name Operators
  ///@{

  ///@}
  ///@name Operations
  ///@{

  /**
   * @brief Performs all the required operations that should be done (for each step) before solving the solution step.
   * @details This is intended to be called just once when the strategy is initialized
   */
  void Initialize(ModelPart &rModelPart) override
  {
    KRATOS_TRY

    DerivedType::Initialize(rModelPart);

    const SizeType dimension = rModelPart.GetProcessInfo()[SPACE_DIMENSION];

    // Set dofs size
    unsigned int DofsSize = 0;
    for (typename IntegrationMethodsVectorType::iterator it = (this->mTimeVectorIntegrationMethods).begin();
         it != (this->mTimeVectorIntegrationMethods).end(); ++it)
      DofsSize += dimension;

    for (typename IntegrationMethodsScalarType::iterator it = (this->mTimeScalarIntegrationMethods).begin();
         it != (this->mTimeScalarIntegrationMethods).end(); ++it)
      ++DofsSize;

    mRotationTool.SetBlockSize(DofsSize);

    // Set time integration of MESH_ variables needed for ALE
    for (typename IntegrationMethodsVectorType::iterator it = (this->mTimeVectorIntegrationMethods).begin(); it != (this->mTimeVectorIntegrationMethods).end(); ++it)
    {
      if ((*it)->GetPrimaryVariableName() == "VELOCITY")
      {
        (this->mTimeVectorIntegrationMethods).push_back((*it)->Clone());
        (this->mTimeVectorIntegrationMethods).back()->SetVariables(MESH_DISPLACEMENT, MESH_VELOCITY, MESH_ACCELERATION, MESH_VELOCITY);
        break;
      }
    }

    KRATOS_CATCH("")
  }

  /**
   * @brief Performs all the required operations that should be done (for each step) before solving the solution step.
   * @details this function must be called only once per step.
   */
  void InitializeSolutionStep(ModelPart &rModelPart) override
  {
    KRATOS_TRY

    // std::cout<<" INITIALIZE STEP ["<<rModelPart.Nodes().back().Id()<<"] "<<rModelPart.Nodes().back().Coordinates()<<" Displacement Old "<< rModelPart.Nodes().back().FastGetSolutionStepValue(DISPLACEMENT,1)<<" Displacement "<< rModelPart.Nodes().back().FastGetSolutionStepValue(DISPLACEMENT)<<" is rigid "<<rModelPart.Nodes().back().Is(RIGID)<<std::endl;

    DerivedType::InitializeSolutionStep(rModelPart);

    this->Set(LocalFlagType::LOCAL_CONSTRAINTS, true); //SLIP conditions present

    this->InitializeEulerianVariables(rModelPart);

    // std::cout<<" INITIALIZE EULERIAN ["<<rModelPart.Nodes().back().Id()<<"] "<<rModelPart.Nodes().back().Coordinates()<<" Displacement Old "<< rModelPart.Nodes().back().FastGetSolutionStepValue(DISPLACEMENT,1)<<" Displacement "<< rModelPart.Nodes().back().FastGetSolutionStepValue(DISPLACEMENT)<<std::endl;

    this->PredictSlipCurvedWalls(rModelPart);

    // std::cout<<" INITIALIZE CURVED ["<<rModelPart.Nodes().back().Id()<<"] "<<rModelPart.Nodes().back().Coordinates()<<" Displacement Old "<< rModelPart.Nodes().back().FastGetSolutionStepValue(DISPLACEMENT,1)<<" Displacement "<< rModelPart.Nodes().back().FastGetSolutionStepValue(DISPLACEMENT)<<std::endl;

    KRATOS_CATCH("")
  }

  /**
   * @brief Performs all the required operations that should be done (for each step) after solving the solution step.
   * @details this function must be called only once per step.
   */

  void FinalizeSolutionStep(ModelPart &rModelPart) override
  {
    KRATOS_TRY

    this->FinalizeEulerianVariables(rModelPart);

    //this->UpdateSlipCurvedWalls(rModelPart);

    DerivedType::FinalizeSolutionStep(rModelPart);

    KRATOS_CATCH("")
  }

  /**
     Performing the update of the solution.
  */
  //***************************************************************************

  /**
   * Performing the update of the solution
   * @param rModelPart: The model of the problem to solve
   * @param rDofSet: Set of all primary variables
   * @param rDx: incremental update of primary variables
   */
  void Update(ModelPart &rModelPart,
              DofsArrayType &rDofSet,
              SystemVectorType &rDx) override
  {
    KRATOS_TRY;

    this->UpdateDofs(rModelPart, rDofSet, rDx);

    if (this->mOptions.Is(LocalFlagType::MOVE_MESH))
      this->UpdateMeshVelocity(rModelPart, rDx);

    this->UpdateVariables(rModelPart);

    this->MoveMesh(rModelPart, "MESH_DISPLACEMENT");

    KRATOS_CATCH("");
  }

  /**
   * @brief Performing Global to Local basis transformation for the solution variables
   * @details this function must be called before Dofs update
   */
  void SetLocalDofs(ModelPart &rModelPart) override
  {
    KRATOS_TRY

    this->mLocalConstraints.MapToLocal(rModelPart, "MESH_VELOCITY");

    KRATOS_CATCH("")
  }

  /**
   * @brief Performing Local to Global basis transformation for the solution variables
   * @details this function must be called after Dofs update
   */
  void SetGlobalDofs(ModelPart &rModelPart) override
  {
    KRATOS_TRY

    this->mLocalConstraints.MapToGlobal(rModelPart, "MESH_VELOCITY");

    KRATOS_CATCH("")
  }

  /**
   * @brief Performing the update of the mesh velocity except for slip conditions
   * for slip conditions, mesh velocity must be imposed through a process
   * @details this function must be called only once per iteration
   */
  void UpdateMeshVelocity(ModelPart &rModelPart,
                          SystemVectorType &rDx)
  {
    KRATOS_TRY

    const int nnodes = static_cast<int>(rModelPart.Nodes().size());
    typename NodesContainerType::iterator it_begin = rModelPart.Nodes().begin();

    const unsigned int dimension = rModelPart.GetProcessInfo()[SPACE_DIMENSION];

#pragma omp parallel for firstprivate(it_begin)
    for (int i = 0; i < nnodes; i++)
    {
      double ale_coefficient = 1.0;
      typename NodesContainerType::iterator itNode = it_begin + i;

      // if( norm_2(itNode->FastGetSolutionStepValue(MESH_ACCELERATION) ) >  100 )
      // {
      //   std::cout<<" Node["<<itNode->Id()<<"] new_entity "<<itNode->Is(NEW_ENTITY)<<" inside "<<itNode->Is(INSIDE)<<" interaction "<<itNode->Is(INTERACTION)<<" rigid "<<itNode->Is(RIGID)<<std::endl;
      //   std::cout<<" MD "<<itNode->FastGetSolutionStepValue(MESH_DISPLACEMENT)<<" MD1 "<<itNode->FastGetSolutionStepValue(MESH_DISPLACEMENT,1)<<std::endl;
      //   std::cout<<" MV "<<itNode->FastGetSolutionStepValue(MESH_VELOCITY)<<" MV1 "<<itNode->FastGetSolutionStepValue(MESH_VELOCITY,1)<<std::endl;
      //   std::cout<<" MA "<<itNode->FastGetSolutionStepValue(MESH_ACCELERATION)<<" MA1 "<<itNode->FastGetSolutionStepValue(MESH_ACCELERATION,1)<<std::endl;
      // }

      if (itNode->Is(FLUID) && itNode->IsNot(ISOLATED))
      {

	if (itNode->Is(INSIDE))
	{
	  ale_coefficient = 0.0;
          // smooth interaction (layer) nodes close to free surface
	  NodeWeakPtrVectorType &nNodes = itNode->GetValue(NEIGHBOUR_NODES);
	  unsigned int inside_nodes = 0;
	  unsigned int non_inside = 0;
	  for (auto &i_nnode : nNodes)
	  {
	    if (i_nnode.Is(FREE_SURFACE))
	    {
	      ale_coefficient = 0.50;
	      break;
	    }
	    if (i_nnode.Is(INSIDE))
	      ++inside_nodes;
	    else if (i_nnode.IsNot(RIGID))
	      ++non_inside;
	  }

	  if (ale_coefficient == 0.0)
	  {
	    if (non_inside > 0)
	    {
	      if (itNode->Is(INTERACTION))
		ale_coefficient = 0.25; //interaction single layer
	      else
		ale_coefficient = 0.0; //first inside layer
	    }
	  }
	}


	if (itNode->IsNot(SLIP))
	{
	  if (itNode->GetDof(VELOCITY_X).IsFree())
	  {
	    itNode->FastGetSolutionStepValue(MESH_VELOCITY_X) += ale_coefficient * TSparseSpace::GetValue(rDx, itNode->GetDof(VELOCITY_X).EquationId());
	  }
	  else
	  {
	    itNode->FastGetSolutionStepValue(MESH_VELOCITY_X) = itNode->FastGetSolutionStepValue(VELOCITY_X);
	  }
	  if (itNode->GetDof(VELOCITY_Y).IsFree())
	  {
	    itNode->FastGetSolutionStepValue(MESH_VELOCITY_Y) += ale_coefficient * TSparseSpace::GetValue(rDx, itNode->GetDof(VELOCITY_Y).EquationId());
	  }
	  else
	  {
	    itNode->FastGetSolutionStepValue(MESH_VELOCITY_Y) = itNode->FastGetSolutionStepValue(VELOCITY_Y);
	  }
	  if (dimension == 3)
	  {
	    if (itNode->GetDof(VELOCITY_Z).IsFree())
	    {
	      itNode->FastGetSolutionStepValue(MESH_VELOCITY_Z) += ale_coefficient * TSparseSpace::GetValue(rDx, itNode->GetDof(VELOCITY_Z).EquationId());
	    }
	    else
	    {
	      itNode->FastGetSolutionStepValue(MESH_VELOCITY_Z) = itNode->FastGetSolutionStepValue(VELOCITY_Z);
	    }
	  }
	}
      }
    }

    KRATOS_CATCH("")
  }

  //***************************************************************************

  /**
   * Performing the prediction of the solution
   * @param rModelPart: The model of the problem to solve
   * @param rDofSet set of all primary variables
   * @param rDx: Incremental update of primary variables
   */

  void Predict(ModelPart &rModelPart,
               DofsArrayType &rDofSet,
               SystemVectorType &rDx) override
  {
    KRATOS_TRY;

    // std::cout<<" PREDICT ["<<rModelPart.Nodes().back().Id()<<"] "<<rModelPart.Nodes().back().Coordinates()<<" Displacement Old "<< rModelPart.Nodes().back().FastGetSolutionStepValue(DISPLACEMENT,1)<<" Displacement "<< rModelPart.Nodes().back().FastGetSolutionStepValue(DISPLACEMENT)<<std::endl;

    this->PredictVariables(rModelPart);

    this->MoveMesh(rModelPart, "MESH_DISPLACEMENT");

    // std::cout<<" MOVED ["<<rModelPart.Nodes().back().Id()<<"] "<<rModelPart.Nodes().back().Coordinates()<<" Displacement Old "<< rModelPart.Nodes().back().FastGetSolutionStepValue(DISPLACEMENT,1)<<" Displacement "<< rModelPart.Nodes().back().FastGetSolutionStepValue(DISPLACEMENT)<<std::endl;

    KRATOS_CATCH("");
  }

  /**
   * @brief Performing the prediction of the solution variables
   * @details this function must be called only once per iteration
   */
  void PredictVariables(ModelPart &rModelPart) override
  {
    KRATOS_TRY

    DerivedType::PredictVariables(rModelPart);

    this->UpdateRigidWallVariables(rModelPart);

    KRATOS_CATCH("")
  }

  /**
   * @brief Performing the update of the solution variables
   * @details this function must be called only once per iteration
   */
  void UpdateVariables(ModelPart &rModelPart) override
  {
    KRATOS_TRY

    DerivedType::UpdateVariables(rModelPart);

    this->UpdateRigidWallVariables(rModelPart);

    // std::cout<<" UPDATED ["<<rModelPart.Nodes().back().Id()<<"] "<<rModelPart.Nodes().back().Coordinates()<<" Displacement Old "<< rModelPart.Nodes().back().FastGetSolutionStepValue(DISPLACEMENT,1)<<" Displacement "<< rModelPart.Nodes().back().FastGetSolutionStepValue(DISPLACEMENT)<<std::endl;

    KRATOS_CATCH("")
  }

  //***************************************************************************

  /** this function is designed to be called in the builder and solver
      to introduce
      the selected time integration scheme. It "asks" the matrix needed to
      the element and
      performs the operations needed to introduce the seected time
      integration scheme.

      this function calculates at the same time the contribution to the
      LHS and to the RHS
      of the system
  */
  void CalculateSystemContributions(Element::Pointer pCurrentElement,
                                    LocalSystemMatrixType &rLHS_Contribution,
                                    LocalSystemVectorType &rRHS_Contribution,
                                    Element::EquationIdVectorType &EquationId,
                                    const ProcessInfo &rCurrentProcessInfo) override
  {
    KRATOS_TRY

    int thread = OpenMPUtils::ThisThread();

    //Initializing the non linear iteration for the current element

    (pCurrentElement)->CalculateLocalSystem(rLHS_Contribution, rRHS_Contribution, rCurrentProcessInfo);

    (pCurrentElement)->EquationIdVector(EquationId, rCurrentProcessInfo);

    if (rCurrentProcessInfo[COMPUTE_DYNAMIC_TANGENT] == true)
    {
      (pCurrentElement)->CalculateSecondDerivativesContributions(this->mMatrix.M[thread], this->mVector.a[thread], rCurrentProcessInfo);
      (pCurrentElement)->CalculateFirstDerivativesContributions(this->mMatrix.D[thread], this->mVector.v[thread], rCurrentProcessInfo);

      this->AddDynamicTangentsToLHS(rLHS_Contribution, this->mMatrix.D[thread], this->mMatrix.M[thread], rCurrentProcessInfo);
      this->AddDynamicForcesToRHS(rRHS_Contribution, this->mVector.v[thread], this->mVector.a[thread], rCurrentProcessInfo);
    }
    else
    {
      (pCurrentElement)->CalculateMassMatrix(this->mMatrix.M[thread], rCurrentProcessInfo);
      (pCurrentElement)->CalculateLocalVelocityContribution(rLHS_Contribution, rRHS_Contribution, rCurrentProcessInfo);

      this->AddDynamicsToLHS(rLHS_Contribution, this->mMatrix.D[thread], this->mMatrix.M[thread], rCurrentProcessInfo);
      this->AddDynamicsToRHS(pCurrentElement, rRHS_Contribution, this->mMatrix.D[thread], this->mMatrix.M[thread], rCurrentProcessInfo);
    }

    if (this->Is(LocalFlagType::LOCAL_CONSTRAINTS))
      this->SetLocalConstraints(pCurrentElement, rLHS_Contribution, rRHS_Contribution, rCurrentProcessInfo);

    KRATOS_CATCH("")
  }

  void Calculate_RHS_Contribution(Element::Pointer pCurrentElement,
                                  LocalSystemVectorType &rRHS_Contribution,
                                  Element::EquationIdVectorType &EquationId,
                                  const ProcessInfo &rCurrentProcessInfo) override
  {
    KRATOS_TRY

    int thread = OpenMPUtils::ThisThread();

    //Initializing the non linear iteration for the current element

    // Basic operations for the element considered
    (pCurrentElement)->CalculateRightHandSide(rRHS_Contribution, rCurrentProcessInfo);

    (pCurrentElement)->EquationIdVector(EquationId, rCurrentProcessInfo);

    if (rCurrentProcessInfo[COMPUTE_DYNAMIC_TANGENT] == true)
    {

      (pCurrentElement)->CalculateSecondDerivativesRHS(this->mVector.a[thread], rCurrentProcessInfo);
      (pCurrentElement)->CalculateFirstDerivativesRHS(this->mVector.v[thread], rCurrentProcessInfo);

      this->AddDynamicForcesToRHS(rRHS_Contribution, this->mVector.v[thread], this->mVector.a[thread], rCurrentProcessInfo);
    }
    else
    {
      LocalSystemMatrixType LHS_Contribution(0,0);
      (pCurrentElement)->CalculateMassMatrix(this->mMatrix.M[thread], rCurrentProcessInfo);
      (pCurrentElement)->CalculateLocalVelocityContribution(LHS_Contribution, rRHS_Contribution, rCurrentProcessInfo);

      this->AddDynamicsToRHS(pCurrentElement, rRHS_Contribution, this->mMatrix.D[thread], this->mMatrix.M[thread], rCurrentProcessInfo);
    }

    if (this->Is(LocalFlagType::LOCAL_CONSTRAINTS))
      this->SetLocalConstraints(pCurrentElement, rRHS_Contribution, rCurrentProcessInfo);

    KRATOS_CATCH("")
  }

  /** functions totally analogous to the precedent but applied to
      the "condition" objects
  */
  void CalculateSystemContributions(Condition::Pointer pCurrentCondition,
                                    LocalSystemMatrixType &rLHS_Contribution,
                                    LocalSystemVectorType &rRHS_Contribution,
                                    Element::EquationIdVectorType &EquationId,
                                    const ProcessInfo &rCurrentProcessInfo) override
  {
    KRATOS_TRY

    int thread = OpenMPUtils::ThisThread();

    // Basic operations for the condition considered
    (pCurrentCondition)->CalculateLocalSystem(rLHS_Contribution, rRHS_Contribution, rCurrentProcessInfo);

    (pCurrentCondition)->EquationIdVector(EquationId, rCurrentProcessInfo);

    if (rCurrentProcessInfo[COMPUTE_DYNAMIC_TANGENT] == true)
    {
      (pCurrentCondition)->CalculateSecondDerivativesContributions(this->mMatrix.M[thread], this->mVector.a[thread], rCurrentProcessInfo);
      (pCurrentCondition)->CalculateFirstDerivativesContributions(this->mMatrix.D[thread], this->mVector.v[thread], rCurrentProcessInfo);

      this->AddDynamicTangentsToLHS(rLHS_Contribution, this->mMatrix.D[thread], this->mMatrix.M[thread], rCurrentProcessInfo);
      this->AddDynamicForcesToRHS(rRHS_Contribution, this->mVector.v[thread], this->mVector.a[thread], rCurrentProcessInfo);
    }
    else
    {
      (pCurrentCondition)->CalculateMassMatrix(this->mMatrix.M[thread], rCurrentProcessInfo);
      (pCurrentCondition)->CalculateLocalVelocityContribution(rLHS_Contribution, rRHS_Contribution, rCurrentProcessInfo);

      this->AddDynamicsToLHS(rLHS_Contribution, this->mMatrix.D[thread], this->mMatrix.M[thread], rCurrentProcessInfo);
      this->AddDynamicsToRHS(pCurrentCondition, rRHS_Contribution, this->mMatrix.D[thread], this->mMatrix.M[thread], rCurrentProcessInfo);
    }

    if (this->Is(LocalFlagType::LOCAL_CONSTRAINTS))
      this->SetLocalConstraints(pCurrentCondition, rLHS_Contribution, rRHS_Contribution, rCurrentProcessInfo);

    KRATOS_CATCH("")
  }

  void Calculate_RHS_Contribution(Condition::Pointer pCurrentCondition,
                                  LocalSystemVectorType &rRHS_Contribution,
                                  Element::EquationIdVectorType &EquationId,
                                  const ProcessInfo &rCurrentProcessInfo) override
  {
    KRATOS_TRY

    int thread = OpenMPUtils::ThisThread();

    // Basic operations for the condition considered
    (pCurrentCondition)->CalculateRightHandSide(rRHS_Contribution, rCurrentProcessInfo);

    (pCurrentCondition)->EquationIdVector(EquationId, rCurrentProcessInfo);

    if (rCurrentProcessInfo[COMPUTE_DYNAMIC_TANGENT] == true)
    {
      (pCurrentCondition)->CalculateSecondDerivativesRHS(this->mVector.a[thread], rCurrentProcessInfo);
      (pCurrentCondition)->CalculateFirstDerivativesRHS(this->mVector.v[thread], rCurrentProcessInfo);

      this->AddDynamicForcesToRHS(rRHS_Contribution, this->mVector.v[thread], this->mVector.a[thread], rCurrentProcessInfo);
    }
    else
    {
      (pCurrentCondition)->CalculateMassMatrix(this->mMatrix.M[thread], rCurrentProcessInfo);
      (pCurrentCondition)->CalculateLocalVelocityContribution(this->mMatrix.D[thread], rRHS_Contribution, rCurrentProcessInfo);

      this->AddDynamicsToRHS(pCurrentCondition, rRHS_Contribution, this->mMatrix.D[thread], this->mMatrix.M[thread], rCurrentProcessInfo);
    }

    if (this->Is(LocalFlagType::LOCAL_CONSTRAINTS))
      this->SetLocalConstraints(pCurrentCondition, rRHS_Contribution, rCurrentProcessInfo);

    KRATOS_CATCH("")
  }

  // TODO

  // void InitializeNonLinIteration(ModelPart& r_model_part,
  //                                        TSystemMatrixType& A,
  //                                        TSystemVectorType& Dx,
  //                                        TSystemVectorType& b) override
  // {
  //     KRATOS_TRY

  //     if (mpTurbulenceModel != 0) // If not null
  //         mpTurbulenceModel->Execute();

  //     KRATOS_CATCH("")
  // }

  // void FinalizeNonLinIteration(ModelPart &rModelPart, TSystemMatrixType &A, TSystemVectorType &Dx, TSystemVectorType &b) override
  // {
  //     ProcessInfo& rCurrentProcessInfo = rModelPart.GetProcessInfo();

  //     //if orthogonal subscales are computed
  //     if (rCurrentProcessInfo[OSS_SWITCH] == 1.0) {
  //         if (rModelPart.GetCommunicator().MyPID() == 0)
  //             std::cout << "Computing OSS projections" << std::endl;

  //         const int nnodes = static_cast<int>(rModelPart.Nodes().size());
  //         auto nbegin = rModelPart.NodesBegin();
  //         #pragma omp parallel for firstprivate(nbegin,nnodes)
  //         for(int i=0; i<nnodes; ++i)
  //         {
  //             auto ind = nbegin + i;
  //             noalias(ind->FastGetSolutionStepValue(ADVPROJ)) = ZeroVector(3);

  //             ind->FastGetSolutionStepValue(DIVPROJ) = 0.0;

  //             ind->FastGetSolutionStepValue(NODAL_AREA) = 0.0;

  //         }//end of loop over nodes

  //         //loop on nodes to compute ADVPROJ   CONVPROJ NODALAREA
  //         array_1d<double, 3 > output = ZeroVector(3);

  //         const int nel = static_cast<int>(rModelPart.Elements().size());
  //         auto elbegin = rModelPart.ElementsBegin();
  //         #pragma omp parallel for firstprivate(elbegin,nel,output)
  //         for(int i=0; i<nel; ++i)
  //         {
  //             auto elem = elbegin + i;
  //             elem->Calculate(ADVPROJ, output, rCurrentProcessInfo);
  //         }

  //         rModelPart.GetCommunicator().AssembleCurrentData(NODAL_AREA);
  //         rModelPart.GetCommunicator().AssembleCurrentData(DIVPROJ);
  //         rModelPart.GetCommunicator().AssembleCurrentData(ADVPROJ);

  //         // Correction for periodic conditions
  //         this->PeriodicConditionProjectionCorrection(rModelPart);

  //         #pragma omp parallel for firstprivate(nbegin,nnodes)
  //         for(int i=0; i<nnodes; ++i)
  //         {
  //             auto ind = nbegin + i;
  //             if (ind->FastGetSolutionStepValue(NODAL_AREA) == 0.0)
  //             {
  //                 ind->FastGetSolutionStepValue(NODAL_AREA) = 1.0;
  //                 //KRATOS_WATCH("*********ATTENTION: NODAL AREA IS ZERRROOOO************");
  //             }
  //             const double Area = ind->FastGetSolutionStepValue(NODAL_AREA);
  //             ind->FastGetSolutionStepValue(ADVPROJ) /= Area;
  //             ind->FastGetSolutionStepValue(DIVPROJ) /= Area;
  //         }
  //     }
  // }

  // void FinalizeSolutionStep(ModelPart &rModelPart, TSystemMatrixType &A, TSystemVectorType &Dx, TSystemVectorType &b) override
  // {
  //     Element::EquationIdVectorType EquationId;
  //     LocalSystemVectorType RHS_Contribution;
  //     LocalSystemMatrixType LHS_Contribution;
  //     ProcessInfo& rCurrentProcessInfo = rModelPart.GetProcessInfo();

  //     //for (ModelPart::NodeIterator itNode = rModelPart.NodesBegin(); itNode != rModelPart.NodesEnd(); ++itNode)

  //     #pragma omp parallel for
  //     for(int k = 0; k<static_cast<int>(rModelPart.Nodes().size()); k++)
  //     {
  //         auto itNode = rModelPart.NodesBegin() + k;
  //         (itNode->FastGetSolutionStepValue(REACTION)).clear();

  //         // calculating relaxed acceleration
  //         const array_1d<double, 3 > & CurrentAcceleration = (itNode)->FastGetSolutionStepValue(ACCELERATION, 0);
  //         const array_1d<double, 3 > & OldAcceleration = (itNode)->FastGetSolutionStepValue(ACCELERATION, 1);
  //         const array_1d<double, 3> relaxed_acceleration = (1 - mAlphaBossak) * CurrentAcceleration
  //                                                             + mAlphaBossak * OldAcceleration;
  //         (itNode)->SetValue(RELAXED_ACCELERATION, relaxed_acceleration);
  //     }

  //     //for (ModelPart::ElementsContainerType::ptr_iterator itElem = rModelPart.Elements().ptr_begin(); itElem != rModelPart.Elements().ptr_end(); ++itElem)

  //     #pragma omp parallel for firstprivate(EquationId,RHS_Contribution,LHS_Contribution)
  //     for(int k = 0; k<static_cast<int>(rModelPart.Elements().size()); k++)
  //     {
  //         auto itElem = rModelPart.Elements().ptr_begin()+k;
  //         int thread_id = OpenMPUtils::ThisThread();

  //         (*itElem)->InitializeNonLinearIteration(rCurrentProcessInfo);
  //         //KRATOS_WATCH(LHS_Contribution);
  //         //basic operations for the element considered
  //         (*itElem)->CalculateLocalSystem(LHS_Contribution, RHS_Contribution, rCurrentProcessInfo);

  //         //std::cout << rCurrentElement->Id() << " RHS = " << RHS_Contribution << std::endl;
  //         (*itElem)->CalculateMassMatrix(mMass[thread_id], rCurrentProcessInfo);
  //         (*itElem)->CalculateLocalVelocityContribution(rLHS_Contribution, RHS_Contribution, rCurrentProcessInfo);

  //         (*itElem)->EquationIdVector(EquationId, rCurrentProcessInfo);

  //         //adding the dynamic contributions (statics is already included)
  //         AddDynamicsToLHS(LHS_Contribution, mDamp[thread_id], mMass[thread_id], rCurrentProcessInfo);
  //         AddDynamicsToRHS((*itElem), RHS_Contribution, mDamp[thread_id], mMass[thread_id], rCurrentProcessInfo);

  //         Element::GeometryType& rGeom = (*itElem)->GetGeometry();
  //         unsigned int NumNodes = rGeom.PointsNumber();
  //         unsigned int Dimension = rGeom.WorkingSpaceDimension();

  //         unsigned int index = 0;
  //         for (unsigned int i = 0; i < NumNodes; i++)
  //         {
  //             auto& reaction = rGeom[i].FastGetSolutionStepValue(REACTION);

  //             double& target_value0 = reaction[0];
  //             const double& origin_value0 = RHS_Contribution[index++];
  //             #pragma omp atomic
  //             target_value0 -= origin_value0;

  //             double& target_value1 = reaction[1];
  //             const double& origin_value1 = RHS_Contribution[index++];
  //             #pragma omp atomic
  //             target_value1 -= origin_value1;

  //             if (Dimension == 3)
  //             {
  //               double& target_value2 = reaction[2];
  //               const double& origin_value2 = RHS_Contribution[index++];
  //               #pragma omp atomic
  //               target_value2 -= origin_value2;
  //             }
  //     //        rGeom[i].FastGetSolutionStepValue(REACTION_X,0) -= RHS_Contribution[index++];
  //      //          rGeom[i].FastGetSolutionStepValue(REACTION_Y,0) -= RHS_Contribution[index++];
  //     //        if (Dimension == 3) rGeom[i].FastGetSolutionStepValue(REACTION_Z,0) -= RHS_Contribution[index++];
  //             index++; // skip pressure dof
  //         }
  //     }

  //     rModelPart.GetCommunicator().AssembleCurrentData(REACTION);

  //     // Base scheme calls FinalizeSolutionStep method of elements and conditions
  //     Scheme<TSparseSpace, TDenseSpace>::FinalizeSolutionStep(rModelPart, A, Dx, b);
  // }

  //************************************************************************************************
  //************************************************************************************************

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Friends
  ///@{

  ///@}
protected:
  ///@name Protected static Member Variables
  ///@{

  ///@}
  ///@name Protected member Variables
  ///@{

  ///@}
  ///@name Protected Operators
  ///@{

  ///@}
  ///@name Protected Operations
  ///@{

  void UpdateRigidWallVariables(ModelPart &rModelPart)
  {
    KRATOS_TRY

    const int nnodes = static_cast<int>(rModelPart.Nodes().size());
    typename NodesContainerType::iterator it_begin = rModelPart.Nodes().begin();

#pragma omp parallel for firstprivate(it_begin)
    for (int i = 0; i < nnodes; i++)
    {
      typename NodesContainerType::iterator itNode = it_begin + i;
      if (itNode->Is(RIGID) && itNode->IsNot(SLIP))
      {
        noalias(itNode->FastGetSolutionStepValue(MESH_DISPLACEMENT)) = itNode->FastGetSolutionStepValue(DISPLACEMENT);
        noalias(itNode->FastGetSolutionStepValue(MESH_VELOCITY)) = itNode->FastGetSolutionStepValue(VELOCITY);
        noalias(itNode->FastGetSolutionStepValue(MESH_ACCELERATION)) = itNode->FastGetSolutionStepValue(ACCELERATION);
      }
    }

    KRATOS_CATCH("")
  }

  void InitializeEulerianVariables(ModelPart &rModelPart)
  {
    KRATOS_TRY

    //Set INSIDE flag
    if (mlagrangian_layers >= 0)
      this->SetEulerianNodesFlag(rModelPart, mlagrangian_layers, meulerian_interaction_layer);

    // correction for for slip wall conditions
    //double free_slip_damping = 0.95;

    const int nnodes = rModelPart.NumberOfNodes();
    ModelPart::NodesContainerType::iterator it_begin = rModelPart.NodesBegin();

#pragma omp parallel for firstprivate(it_begin)
    for (int i = 0; i < nnodes; ++i)
    {
      ModelPart::NodesContainerType::iterator itNode = it_begin + i;

      // set displacements to correct value to all nodes
      array_1d<double, 3> &Displacement = itNode->FastGetSolutionStepValue(DISPLACEMENT, 1);
      noalias(Displacement) = (itNode->Coordinates() - itNode->GetInitialPosition());
      noalias(itNode->FastGetSolutionStepValue(DISPLACEMENT)) = Displacement;

      noalias(itNode->FastGetSolutionStepValue(MESH_DISPLACEMENT)) = Displacement;
      noalias(itNode->FastGetSolutionStepValue(MESH_DISPLACEMENT, 1)) = Displacement;

      // // correction slip rigid walls with no fluid elements
      // if( itNode->Is(RIGID) && itNode->Is(SLIP) && itNode->IsNot(FLUID) ){
      //   noalias(itNode->FastGetSolutionStepValue(VELOCITY))       = ZeroVector(3);
      //   noalias(itNode->FastGetSolutionStepValue(ACCELERATION))   = ZeroVector(3);
      //   noalias(itNode->FastGetSolutionStepValue(VELOCITY,1))     = ZeroVector(3);
      //   noalias(itNode->FastGetSolutionStepValue(ACCELERATION,1)) = ZeroVector(3);
      // }

      // // correction for slip walls and free surface sliding nodes
      // if( itNode->Is(FREE_SURFACE) ){
      //   if( itNode->Is(SLIP) ){
      //     //itNode->FastGetSolutionStepValue(VELOCITY) *= free_slip_damping;
      //     itNode->FastGetSolutionStepValue(VELOCITY,1)     *= free_slip_damping;
      //     itNode->FastGetSolutionStepValue(ACCELERATION,1) *= free_slip_damping;
      //   }
      // }

      // new eulerian and lagrangian nodes
      if (itNode->Is(INSIDE))
      {
        if (itNode->IsNot(SELECTED))
        { //new eulerian node
          noalias(itNode->FastGetSolutionStepValue(MESH_VELOCITY)) = ZeroVector(3);
          noalias(itNode->FastGetSolutionStepValue(MESH_VELOCITY, 1)) = ZeroVector(3);

          if (itNode->Is(NEW_ENTITY))
          {
            noalias(itNode->FastGetSolutionStepValue(MESH_ACCELERATION)) = ZeroVector(3);
            noalias(itNode->FastGetSolutionStepValue(MESH_ACCELERATION, 1)) = ZeroVector(3);

            //correction for displacements in INSIDE and NEW_ENTITY nodes.
            noalias(itNode->GetInitialPosition()) = itNode->Coordinates();
            array_1d<double, 3> &Displacement = itNode->FastGetSolutionStepValue(DISPLACEMENT, 1);
            noalias(Displacement) = ZeroVector(3);
            noalias(itNode->FastGetSolutionStepValue(DISPLACEMENT)) = Displacement;
            noalias(itNode->FastGetSolutionStepValue(MESH_DISPLACEMENT)) = Displacement;
            noalias(itNode->FastGetSolutionStepValue(MESH_DISPLACEMENT, 1)) = Displacement;
          }
          else
          {
            noalias(itNode->FastGetSolutionStepValue(MESH_ACCELERATION)) = itNode->FastGetSolutionStepValue(ACCELERATION);
            noalias(itNode->FastGetSolutionStepValue(MESH_ACCELERATION, 1)) = itNode->FastGetSolutionStepValue(ACCELERATION, 1);
          }
        }
        else{
          if (norm_2(itNode->FastGetSolutionStepValue(MESH_VELOCITY))!=0 && itNode->AndNot({INTERACTION,RIGID}))
          {
            //correction for displacements in INSIDE and NEW_ENTITY nodes.
            noalias(itNode->GetInitialPosition()) = itNode->Coordinates();
            array_1d<double, 3> &Displacement = itNode->FastGetSolutionStepValue(DISPLACEMENT, 1);
            noalias(Displacement) = ZeroVector(3);
            noalias(itNode->FastGetSolutionStepValue(DISPLACEMENT)) = Displacement;
            noalias(itNode->FastGetSolutionStepValue(MESH_DISPLACEMENT)) = Displacement;
            noalias(itNode->FastGetSolutionStepValue(MESH_DISPLACEMENT, 1)) = Displacement;

            noalias(itNode->FastGetSolutionStepValue(MESH_VELOCITY)) = ZeroVector(3);
            noalias(itNode->FastGetSolutionStepValue(MESH_VELOCITY, 1)) = ZeroVector(3);
            noalias(itNode->FastGetSolutionStepValue(MESH_ACCELERATION)) = ZeroVector(3);
            noalias(itNode->FastGetSolutionStepValue(MESH_ACCELERATION, 1)) = ZeroVector(3);
          }
        }
      }
      else
      {
        if (itNode->Is(SELECTED) || itNode->Is(NEW_ENTITY))
        { //new lagrangian node
          noalias(itNode->FastGetSolutionStepValue(MESH_VELOCITY)) = itNode->FastGetSolutionStepValue(VELOCITY);
          noalias(itNode->FastGetSolutionStepValue(MESH_VELOCITY, 1)) = itNode->FastGetSolutionStepValue(VELOCITY, 1);
          noalias(itNode->FastGetSolutionStepValue(MESH_ACCELERATION)) = itNode->FastGetSolutionStepValue(ACCELERATION);
          noalias(itNode->FastGetSolutionStepValue(MESH_ACCELERATION, 1)) = itNode->FastGetSolutionStepValue(ACCELERATION, 1);
        }
        else if (itNode->IsNot(SLIP) && itNode->IsNot(INTERACTION))
        { // reassign to all lagrangian nodes the velocity
          noalias(itNode->FastGetSolutionStepValue(MESH_VELOCITY)) = itNode->FastGetSolutionStepValue(VELOCITY);
          noalias(itNode->FastGetSolutionStepValue(MESH_VELOCITY, 1)) = itNode->FastGetSolutionStepValue(VELOCITY, 1);
          noalias(itNode->FastGetSolutionStepValue(MESH_ACCELERATION)) = itNode->FastGetSolutionStepValue(ACCELERATION);
          noalias(itNode->FastGetSolutionStepValue(MESH_ACCELERATION, 1)) = itNode->FastGetSolutionStepValue(ACCELERATION, 1);
        }
      }

      itNode->Set(SELECTED, false);
    }

    KRATOS_CATCH("")
  }

  //***************************************************************************

  void SetEulerianNodesFlag(ModelPart &rModelPart, unsigned int number_of_free_layers, bool eulerian_interaction_layer) //INSIDE
  {
    const int nnodes = rModelPart.NumberOfNodes();
    ModelPart::NodesContainerType::iterator it_begin = rModelPart.NodesBegin();

#pragma omp parallel for firstprivate(it_begin)
    for (int i = 0; i < nnodes; ++i)
    {
      ModelPart::NodesContainerType::iterator itNode = it_begin + i;
      itNode->Set(VISITED, false);
      itNode->Set(BLOCKED, false);
      itNode->Set(SELECTED, false);
      if (itNode->Is(INSIDE))
      {
        itNode->Set(SELECTED, true);
        itNode->Set(INSIDE, false);
      }
      if (eulerian_interaction_layer){
	//add interaction nodes as inside (one eulerian layer by default)
	if (itNode->Is(INTERACTION) && itNode->IsNot(FREE_SURFACE))
	  {
	    itNode->Set(INSIDE, true);
	  }
      }
    }

    //first layer (FREE_SURFACE)
    for (auto &i_elem : rModelPart.Elements())
    {
      Geometry<Node<3>> &eGeometry = i_elem.GetGeometry();

      bool inside = false;
      unsigned int inside_nodes = 0;
      unsigned int rigid_nodes = 0;
      for (auto &i_node : eGeometry)
      {
        if (i_node.Is(FREE_SURFACE))
        {
          inside = true;
        }
        else
        {
          if (i_node.Or({RIGID,SOLID}))
            ++rigid_nodes;

          if (i_node.Is(INSIDE))
            ++inside_nodes;
        }
      }
      if (inside)
      {
        for (auto &i_node : eGeometry)
          if (i_node.AndNot({FREE_SURFACE,RIGID}))
            i_node.Set(BLOCKED, true);
      }
      else
      {
	if (eulerian_interaction_layer){
	  // add layer nodes with no boundary face (one eulerian layer by default)
	  if (rigid_nodes > 0 && inside_nodes > 0)
	    {
	      for (auto &i_node : eGeometry)
		if (i_node.IsNot(BOUNDARY))
		  i_node.Set(INSIDE, true);
	    }
	}
      }
    }

    //other layer (BLOCKED-VISITED)
    for (unsigned int i = 0; i < number_of_free_layers; ++i)
    {
#pragma omp parallel for firstprivate(it_begin)
      for (int i = 0; i < nnodes; ++i)
      {
        ModelPart::NodesContainerType::iterator itNode = it_begin + i;
        if (itNode->Is(BLOCKED))
          itNode->Set(VISITED, true);
      }

      for (auto &i_elem : rModelPart.Elements())
      {
        Geometry<Node<3>> &eGeometry = i_elem.GetGeometry();
        bool inside = false;
        for (auto &i_node : eGeometry)
        {
          if (i_node.Is(BLOCKED) && i_node.Is(VISITED))
          {
            inside = true;
            break;
          }
        }
        if (inside)
        {
          for (auto &i_node : eGeometry)
            if (i_node.IsNot(BOUNDARY))
              i_node.Set(BLOCKED, true);
        }
      }
    }

#pragma omp parallel for firstprivate(it_begin)
    for (int i = 0; i < nnodes; ++i)
    {
      ModelPart::NodesContainerType::iterator itNode = it_begin + i;
      itNode->Set(VISITED, false);
      if (itNode->IsNot(BOUNDARY))
      {
        if (itNode->IsNot(BLOCKED))
          itNode->Set(INSIDE, true);
        else
          itNode->Set(BLOCKED, false);
      }
      if (itNode->Is(ISOLATED)) //to avoid ISOLATED nodes fixed in space
        itNode->Set(INSIDE, false);
    }
  }

  //***************************************************************************

  void FinalizeEulerianVariables(ModelPart &rModelPart)
  {
    KRATOS_TRY

    const int nnodes = rModelPart.NumberOfNodes();
    //const unsigned int dimension = rModelPart.GetProcessInfo()[SPACE_DIMENSION];
    double slip_damping = 0.99;
    // const double& delta_time = rModelPart.GetProcessInfo()[DELTA_TIME];

    ModelPart::NodesContainerType::iterator it_begin = rModelPart.NodesBegin();

#pragma omp parallel for firstprivate(it_begin)
    for (int i = 0; i < nnodes; ++i)
    {
      ModelPart::NodesContainerType::iterator itNode = it_begin + i;

      // set displacements to correct value
      const array_1d<double, 3> &Displacement = itNode->FastGetSolutionStepValue(MESH_DISPLACEMENT);
      noalias(itNode->FastGetSolutionStepValue(DISPLACEMENT)) = Displacement;

      // reassign to all lagrangian nodes the velocity (not necessary)
      // if( itNode->IsNot(INSIDE) && itNode->IsNot(SLIP) ){
      //   itNode->FastGetSolutionStepValue(MESH_VELOCITY) = itNode->FastGetSolutionStepValue(VELOCITY);
      //   itNode->FastGetSolutionStepValue(MESH_VELOCITY,1) = itNode->FastGetSolutionStepValue(VELOCITY,1);
      // }

      // correction slip rigid walls with no fluid elements
      if (itNode->And({RIGID,SLIP}))
      {
        // array_1d<double, 3> &movement = itNode->FastGetSolutionStepValue(DISPLACEMENT);
        // for (unsigned int j = 0; j < 3; ++j)
        //   {
        //     if (movement[j] != 0)
        //       {
        //         KRATOS_WARNING("") << " Node Finalize [" << itNode->Id() << "] is moving " << movement << std::endl;
        //         break;
        //       }
        //   }
        if (itNode->IsNot(FLUID))
        {
          noalias(itNode->FastGetSolutionStepValue(VELOCITY)) = ZeroVector(3);
          noalias(itNode->FastGetSolutionStepValue(ACCELERATION)) = ZeroVector(3);
        }
        else  //(combined with assigned artificial viscosity .. slip_damping = 1.0 means volume loss in lagrangian elements, something related to the system solve conditioning)
        {
          if (norm_2(itNode->FastGetSolutionStepValue(ACCELERATION))>100 )
          {
            itNode->FastGetSolutionStepValue(ACCELERATION) *= 0.1;
            itNode->FastGetSolutionStepValue(ACCELERATION,1) *= 0.1;

            itNode->FastGetSolutionStepValue(VELOCITY) *= 0.5;
            itNode->FastGetSolutionStepValue(VELOCITY,1) *= 0.5;
          }
          // correction for slip walls
          itNode->FastGetSolutionStepValue(VELOCITY) *= slip_damping;
          itNode->FastGetSolutionStepValue(ACCELERATION) *= slip_damping;
          itNode->FastGetSolutionStepValue(VELOCITY,1) *= slip_damping;
          itNode->FastGetSolutionStepValue(ACCELERATION,1) *= slip_damping;
        }

        // if (itNode->Is(FLUID) && dimension == 3)
        // {
        //   if (itNode->Is(SLIP)) // correction residual accelerations
        //   {
        //     if (norm_2(itNode->FastGetSolutionStepValue(VELOCITY)) < norm_2(itNode->FastGetSolutionStepValue(ACCELERATION)) * 0.1 )
        //     {
        //       itNode->FastGetSolutionStepValue(ACCELERATION) *= 0.1;
        //     }
        //   }
        // }
      }

    }

    KRATOS_CATCH("")
  }

  //***************************************************************************

  void PredictSlipCurvedWalls(ModelPart &rModelPart)
  {
    KRATOS_TRY

    // correction for curved walls
    for (auto &i_cond : rModelPart.Conditions())
    {
      bool rigid = false;
      bool slip = false;
      unsigned int rigid_nodes = 0;
      unsigned int slip_nodes = 0;
      GeometryType &cGeometry = i_cond.GetGeometry();
      for (auto &i_node : cGeometry)
      {
        if (i_node.Is(RIGID))
          ++rigid_nodes;
        if (i_node.Is(SLIP))
          ++slip_nodes;
      }

      if (rigid_nodes == cGeometry.size())
        rigid = true;

      if (slip_nodes == cGeometry.size())
        slip = true;

      if (rigid && slip)
      {
        const array_1d<double, 3> &rNormal = i_cond.GetValue(NORMAL);
        for (auto &i_node : cGeometry)
        {
          const array_1d<double, 3> &rNodeNormal = i_node.FastGetSolutionStepValue(NORMAL);

          double projection = inner_prod(rNormal, rNodeNormal);
          if (projection < 0.866) //projection bigger than 35º -> corner node
          {
            // const array_1d<double,3>& rVelocity = i_node.FastGetSolutionStepValue(VELOCITY);
            // double norm = norm_2(rVelocity);
            // double projection = 0;
            // if(norm>0)
            //   projection = inner_prod(rNormal,rVelocity)/norm;

            // only wall projection
            i_node.FastGetSolutionStepValue(VELOCITY) -= inner_prod(rNormal, i_node.FastGetSolutionStepValue(VELOCITY)) * rNormal;
            i_node.FastGetSolutionStepValue(VELOCITY, 1) -= inner_prod(rNormal, i_node.FastGetSolutionStepValue(VELOCITY, 1)) * rNormal;

            i_node.FastGetSolutionStepValue(DISPLACEMENT) -= inner_prod(rNormal, i_node.FastGetSolutionStepValue(DISPLACEMENT)) * rNormal;
            //i_node.FastGetSolutionStepValue(DISPLACEMENT,1) -= inner_prod(rNormal,i_node.FastGetSolutionStepValue(DISPLACEMENT,1)) * rNormal;

            i_node.FastGetSolutionStepValue(ACCELERATION) -= inner_prod(rNormal, i_node.FastGetSolutionStepValue(ACCELERATION)) * rNormal;
            //i_node.FastGetSolutionStepValue(ACCELERATION,1) -= inner_prod(rNormal,i_node.FastGetSolutionStepValue(ACCELERATION,1)) * rNormal;

            // assign stick velocity
            // i_node.FastGetSolutionStepValue(VELOCITY)       = i_node.FastGetSolutionStepValue(MESH_VELOCITY);
            // i_node.FastGetSolutionStepValue(VELOCITY,1)     = i_node.FastGetSolutionStepValue(MESH_VELOCITY,1);
            // i_node.FastGetSolutionStepValue(DISPLACEMENT,1) = i_node.FastGetSolutionStepValue(MESH_DISPLACEMENT,1);

            // i_node.FastGetSolutionStepValue(DISPLACEMENT)   = i_node.FastGetSolutionStepValue(MESH_DISPLACEMENT);
            // i_node.FastGetSolutionStepValue(ACCELERATION)   = i_node.FastGetSolutionStepValue(MESH_ACCELERATION);
            // i_node.FastGetSolutionStepValue(ACCELERATION,1) = i_node.FastGetSolutionStepValue(MESH_ACCELERATION,1);
          }
        }
      }
    }

    KRATOS_CATCH("")
  }

  //***************************************************************************

  void UpdateSlipCurvedWalls(ModelPart &rModelPart)
  {
    KRATOS_TRY

    // correction for curved walls
    for (auto &i_cond : rModelPart.Conditions())
    {
      bool rigid = false;
      bool slip = false;
      unsigned int rigid_nodes = 0;
      unsigned int slip_nodes = 0;
      GeometryType &cGeometry = i_cond.GetGeometry();
      for (auto &i_node : cGeometry)
      {
        if (i_node.Is(RIGID))
          ++rigid_nodes;
        if (i_node.Is(SLIP))
          ++slip_nodes;
      }

      if (rigid_nodes == cGeometry.size())
        rigid = true;

      if (slip_nodes == cGeometry.size())
        slip = true;

      if (rigid && slip)
      {
        const array_1d<double, 3> &rNormal = i_cond.GetValue(NORMAL);
        for (auto &i_node : cGeometry)
        {
          const array_1d<double, 3> &rVelocity = i_node.FastGetSolutionStepValue(VELOCITY);
          double norm = norm_2(rVelocity);
          double projection = 0;
          if (norm > 0)
            projection = inner_prod(rNormal, rVelocity) / norm;

          //if( projection>0 && projection>0.0761 ){ // if is a corner correct slip velocity
          if (fabs(projection) > 0.0761)
          { // if is a corner correct slip velocity
            // only wall projection
            i_node.FastGetSolutionStepValue(VELOCITY) -= inner_prod(rNormal, i_node.FastGetSolutionStepValue(VELOCITY)) * rNormal;
            i_node.FastGetSolutionStepValue(DISPLACEMENT) -= inner_prod(rNormal, i_node.FastGetSolutionStepValue(DISPLACEMENT)) * rNormal;
            i_node.FastGetSolutionStepValue(ACCELERATION) -= inner_prod(rNormal, i_node.FastGetSolutionStepValue(ACCELERATION)) * rNormal;

            // i_node.FastGetSolutionStepValue(VELOCITY,1)       -= inner_prod(rNormal,i_node.FastGetSolutionStepValue(VELOCITY,1)) * rNormal;
            // i_node.FastGetSolutionStepValue(DISPLACEMENT,1)   -= inner_prod(rNormal,i_node.FastGetSolutionStepValue(DISPLACEMENT,1)) * rNormal;
            // i_node.FastGetSolutionStepValue(ACCELERATION,1)   -= inner_prod(rNormal,i_node.FastGetSolutionStepValue(ACCELERATION,1)) * rNormal;
          }
        }
      }
    }

    KRATOS_CATCH("")
  }

  /** On periodic boundaries, the nodal area and the values to project need to take into account contributions from elements on
   * both sides of the boundary. This is done using the conditions and the non-historical nodal data containers as follows:\n
   * 1- The partition that owns the PeriodicCondition adds the values on both nodes to their non-historical containers.\n
   * 2- The non-historical containers are added across processes, communicating the right value from the condition owner to all partitions.\n
   * 3- The value on all periodic nodes is replaced by the one received in step 2.
   */
  // void PeriodicConditionProjectionCorrection(ModelPart& rModelPart)
  // {
  //     const int num_nodes = rModelPart.NumberOfNodes();
  //     const int num_conditions = rModelPart.NumberOfConditions();

  //     #pragma omp parallel for
  //     for (int i = 0; i < num_nodes; i++) {
  //         auto it_node = rModelPart.NodesBegin() + i;

  //         it_node->SetValue(NODAL_AREA,0.0);
  //         it_node->SetValue(ADVPROJ,ZeroVector(3));
  //         it_node->SetValue(DIVPROJ,0.0);
  //     }

  //     #pragma omp parallel for
  //     for (int i = 0; i < num_conditions; i++) {
  //         auto it_cond = rModelPart.ConditionsBegin() + i;

  //         if(it_cond->Is(PERIODIC)) {
  //             this->AssemblePeriodicContributionToProjections(it_cond->GetGeometry());
  //         }
  //     }

  //     rModelPart.GetCommunicator().AssembleNonHistoricalData(NODAL_AREA);
  //     rModelPart.GetCommunicator().AssembleNonHistoricalData(ADVPROJ);
  //     rModelPart.GetCommunicator().AssembleNonHistoricalData(DIVPROJ);

  //     #pragma omp parallel for
  //     for (int i = 0; i < num_nodes; i++) {
  //         auto it_node = rModelPart.NodesBegin() + i;
  //         this->CorrectContributionsOnPeriodicNode(*it_node);
  //     }
  // }

  // void AssemblePeriodicContributionToProjections(Geometry< Node<3> >& rGeometry)
  // {
  //     unsigned int nodes_in_cond = rGeometry.PointsNumber();

  //     double nodal_area = 0.0;
  //     array_1d<double,3> momentum_projection = ZeroVector(3);
  //     double mass_projection = 0.0;
  //     for ( unsigned int i = 0; i < nodes_in_cond; i++ )
  //     {
  //         auto& r_node = rGeometry[i];
  //         nodal_area += r_node.FastGetSolutionStepValue(NODAL_AREA);
  //         noalias(momentum_projection) += r_node.FastGetSolutionStepValue(ADVPROJ);
  //         mass_projection += r_node.FastGetSolutionStepValue(DIVPROJ);
  //     }

  //     for ( unsigned int i = 0; i < nodes_in_cond; i++ )
  //     {
  //         auto& r_node = rGeometry[i];
  //         /* Note that this loop is expected to be threadsafe in normal conditions,
  //         * since each node should belong to a single periodic link. However, I am
  //         * setting the locks for openmp in case that we try more complicated things
  //         * in the future (like having different periodic conditions for different
  //         * coordinate directions).
  //         */
  //         r_node.SetLock();
  //         r_node.GetValue(NODAL_AREA) = nodal_area;
  //         noalias(r_node.GetValue(ADVPROJ)) = momentum_projection;
  //         r_node.GetValue(DIVPROJ) = mass_projection;
  //         r_node.UnSetLock();
  //     }
  // }

  // void CorrectContributionsOnPeriodicNode(Node<3>& rNode)
  // {
  //     if (rNode.GetValue(NODAL_AREA) != 0.0) // Only periodic nodes will have a non-historical NODAL_AREA set.
  //     {
  //         rNode.FastGetSolutionStepValue(NODAL_AREA) = rNode.GetValue(NODAL_AREA);
  //         noalias(rNode.FastGetSolutionStepValue(ADVPROJ)) = rNode.GetValue(ADVPROJ);
  //         rNode.FastGetSolutionStepValue(DIVPROJ) = rNode.GetValue(DIVPROJ);
  //     }
  // }

  ///@}
  ///@name Protected  Access
  ///@{

  ///@}
  ///@name Protected Inquiry
  ///@{

  ///@}
  ///@name Protected LifeCycle
  ///@{

  ///@}
private:
  ///@name Static Member Variables
  ///@{

  ///@}
  ///@name Member Variables
  ///@{

  SlipCoordinateTransformation<LocalSystemMatrixType, LocalSystemVectorType, double> mRotationTool;

  int mlagrangian_layers;

  bool meulerian_interaction_layer;

  ///@}
  ///@name Private Operators
  ///@{

  ///@}
  ///@name Private Operations
  ///@{

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Serialization
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  ///@}

}; // Class AleSolutionScheme
///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_ALE_SOLUTION_SCHEME_HPP_INCLUDED  defined
