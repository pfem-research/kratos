//
//   Project Name:        KratosPfemApplication    $
//   Developed by:        $Developer:      AFranci $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:         July 2018 $
//
//

#if !defined(KRATOS_INSERT_FLUID_NODES_MESHER_PROCESS_HPP_INCLUDED)
#define KRATOS_INSERT_FLUID_NODES_MESHER_PROCESS_HPP_INCLUDED

// External includes

// System includes

// Project includes
#include "custom_processes/mesher_process.hpp"

namespace Kratos
{

///@name Kratos Classes
///@{

/// Refine Mesh Elements Process 2D and 3D
/** Inserts as many nodes as were released in the removal process
    the nodes are inserted in the edges of the largest elements
*/

class InsertFluidNodesMesherProcess
    : public MesherProcess
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of Process
  KRATOS_CLASS_POINTER_DEFINITION(InsertFluidNodesMesherProcess);

  typedef ModelPart::NodeType NodeType;
  typedef ModelPart::ConditionType ConditionType;
  typedef ModelPart::PropertiesType PropertiesType;
  typedef ConditionType::GeometryType GeometryType;

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  InsertFluidNodesMesherProcess(ModelPart &rModelPart,
                                MesherData::MeshingParameters &rRemeshingParameters,
                                int EchoLevel)
      : mrModelPart(rModelPart),
        mrRemesh(rRemeshingParameters)
  {
    mEchoLevel = EchoLevel;
  }

  /// Destructor.
  virtual ~InsertFluidNodesMesherProcess() {}

  ///@}
  ///@name Operators
  ///@{

  /// This operator is provided to call the process as a function and simply calls the Execute method.
  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  /// Execute method is used to execute the Process algorithms.
  void Execute() override
  {
    KRATOS_TRY

    // double begin_time = OpenMPUtils::GetCurrentTime();

    if (mEchoLevel > 0)
      std::cout << " [ INSERT NEW NODES for homogeneous mesh: " << std::endl;

    if (mrModelPart.Name() != mrRemesh.SubModelPartName)
      std::cout << " ModelPart Supplied do not corresponds to the Meshing Domain: (" << mrModelPart.Name() << " != " << mrRemesh.SubModelPartName << ")" << std::endl;


    const unsigned int dimension = mrModelPart.GetProcessInfo()[SPACE_DIMENSION];
    unsigned int ElementsToRefine = mrRemesh.Info->Removed.Nodes + dimension;

    if (mrRemesh.IsInterior(RefineData::REFINE) && ElementsToRefine > 0)
    {
      // label TO_SPLIT to not select same edge twice
      SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{TO_SPLIT}}, {TO_SPLIT.AsFalse()});

      std::vector<array_1d<double, 3>> NewPositions(ElementsToRefine);
      std::vector<array_1d<unsigned int, 4>> NodeIdsToInterpolate(ElementsToRefine);
      std::vector<Node<3>::DofsContainerType> NewDofs(ElementsToRefine);
      std::vector<double> LargestVolumes(ElementsToRefine);
      std::fill(LargestVolumes.begin(), LargestVolumes.end(), -1);

      unsigned int NodesToRefine = 0;

      for (auto i_elem(mrModelPart.ElementsBegin()); i_elem != mrModelPart.ElementsEnd(); ++i_elem)
      {
        const unsigned int dimension = i_elem->GetGeometry().WorkingSpaceDimension();

        MesherData MeshData;
        RefineData& Refine = MeshData.GetRefineData(*i_elem, mrRemesh, mrModelPart.GetProcessInfo());

        const double& rSize = Refine.Interior.DistanceMeshSize * 3;

        if (dimension == 2)
        {
          if (i_elem->GetGeometry().size() > 2) //not boundary elements
            SelectEdgeToRefine2D(i_elem->GetGeometry(), NewPositions, LargestVolumes, NodeIdsToInterpolate, NewDofs, rSize, NodesToRefine, ElementsToRefine);
        }
        else if (dimension == 3)
        {
          if (i_elem->GetGeometry().size() > 3) //not boundary elements
            SelectEdgeToRefine3D(i_elem->GetGeometry(), NewPositions, LargestVolumes, NodeIdsToInterpolate, NewDofs, rSize, NodesToRefine, ElementsToRefine);
        }
      }

      //std::cout<<" Candidates to refine "<<NodesToRefine<<std::endl;

      mrRemesh.Info->Inserted.Nodes = NodesToRefine;

      // Here only splits as vertices as removed nodes ( without checking the longest vertices )
      this->CreateAndAddNewNodes(NewPositions, NodeIdsToInterpolate, NewDofs, NodesToRefine);

      SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{TO_SPLIT}}, {TO_SPLIT.AsFalse()});
      //check dofs exist:
      // Dof<double>::Pointer NodeDof;
      // for (auto i_node(mrModelPart.NodesBegin()); i_node != mrModelPart.NodesEnd(); ++i_node)
      //   NodeDof = i_node->pGetDof(VELOCITY_X);
      // std::cout<<" DOFS CHECKED insert "<<std::endl;
    }

    if (mEchoLevel > 2)
      std::cout << "   INSERT NEW NODES ]; (" << mrRemesh.Info->Inserted.Nodes << ")" << std::endl;

    // if( mEchoLevel >= 0 ){
    //   double end_time = OpenMPUtils::GetCurrentTime();
    //   std::cout<<"  INSERT NODES time = "<<end_time-begin_time<<std::endl;
    // }

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "InsertFluidNodesMesherProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "InsertFluidNodesMesherProcess";
  }

  /// Print object's data.s
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Static Member Variables
  ///@{

  ModelPart &mrModelPart;

  MesherData::MeshingParameters &mrRemesh;

  int mEchoLevel;

  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{

  //**************************************************************************
  //**************************************************************************

  void CopyDofs(Node<3>::DofsContainerType const &From, Node<3>::DofsContainerType &To)
  {
    for (auto &p_dof : From)
    {
      To.push_back(Kratos::unique_ptr<Dof<double>>(new Dof<double>(*p_dof)));
    }
  }

  //**************************************************************************
  //**************************************************************************

  bool SelectEdgeToRefine2D(Element::GeometryType &rGeometry,
                            std::vector<array_1d<double, 3>> &rNewPositions,
                            std::vector<double> &rLargestVolumes,
                            std::vector<array_1d<unsigned int, 4>> &rNodeIdsToInterpolate,
                            std::vector<Node<3>::DofsContainerType> &rNewDofs,
                            const double &rSize,
                            unsigned int &rNodesToRefine,
                            const unsigned int &rElementsToRefine)
  {
    KRATOS_TRY

    const unsigned int NumberOfNodes = rGeometry.size();

    std::map<std::string, unsigned int> GeometryFlags {{"RIGID", 0}, {"FREE_SURFACE", 0}, {"INLET", 0}, {"TO_SPLIT", 0}, {"INSIDE", 0}, {"INTERACTION", 0}, {"TO_ERASE", 0}};
    SetFlagsUtilities::CountGeometryFlags(rGeometry, GeometryFlags);

    bool any_node_to_erase = (GeometryFlags.at("TO_ERASE") > 0 ? true : false);

    // for (auto &[flag, number] : GeometryFlags)
    //   std::cout<<" Flags "<<flag<<" number "<<number<<std::endl;

    double critical_edge_length = 3.0 * rSize;

    double length_tolerance = 1.5;
    double penalization = 1.0;


    if (GeometryFlags.at("INLET") > 0)
      penalization = 0.9;

    if (GeometryFlags.at("INTERACTION") > 0) // no penalize
      penalization = 1.2;

    if (GeometryFlags.at("FREE_SURFACE") == 1)
      penalization = 0.8; //penalize inserting in the boundary

    if (GeometryFlags.at("RIGID") > 1)
    {
      penalization = 0.8;
      if (GeometryFlags.at("INSIDE") > 0)
        penalization = 0.1;
    }

    double ElementalVolume = rGeometry.Area();

    array_1d<double, 3> Edges(3, 0.0);
    array_1d<unsigned int, 3> FirstEdgeNode(3, 0);
    array_1d<unsigned int, 3> SecondEdgeNode(3, 0);

    double WallCharacteristicDistance = 0;
    array_1d<double, 3> CoorDifference;
    noalias(CoorDifference) = rGeometry[1].Coordinates() - rGeometry[0].Coordinates();

    double SquaredLength = CoorDifference[0] * CoorDifference[0] + CoorDifference[1] * CoorDifference[1];
    Edges[0] = sqrt(SquaredLength);
    FirstEdgeNode[0] = 0;
    SecondEdgeNode[0] = 1;
    if (rGeometry[0].Is(RIGID) && rGeometry[1].Is(RIGID))
    {
      WallCharacteristicDistance = Edges[0];
    }
    unsigned int Counter = 0;
    for (unsigned int i = 2; i < NumberOfNodes; ++i)
    {
      for (unsigned int j = 0; j < i; ++j)
      {
        noalias(CoorDifference) = rGeometry[i].Coordinates() - rGeometry[j].Coordinates();
        SquaredLength = CoorDifference[0] * CoorDifference[0] + CoorDifference[1] * CoorDifference[1];
        Counter += 1;
        Edges[Counter] = sqrt(SquaredLength);
        FirstEdgeNode[Counter] = j;
        SecondEdgeNode[Counter] = i;
        if (rGeometry[i].Is(RIGID) && rGeometry[j].Is(RIGID) && Edges[Counter] > WallCharacteristicDistance)
        {
          WallCharacteristicDistance = Edges[Counter];
        }
      }
    }

    bool dangerousElement = false;

    if (GeometryFlags.at("RIGID") > 1)
    {

      for (unsigned int i = 0; i < 3; ++i)
      {
        if ((Edges[i] < WallCharacteristicDistance * length_tolerance) || (rGeometry[FirstEdgeNode[i]].Is(RIGID) && rGeometry[SecondEdgeNode[i]].Is(RIGID)))
        {
          Edges[i] = 0;
        }
        if (rGeometry[FirstEdgeNode[i]].Or({FREE_SURFACE,RIGID}) &&
	    rGeometry[SecondEdgeNode[i]].Or({FREE_SURFACE,RIGID}))
        {
          Edges[i] = 0;
        }
      }
    }

    if ((Edges[0] == 0 && Edges[1] == 0 && Edges[2] == 0) || GeometryFlags.at("RIGID") == 3)
    {
      dangerousElement = true;
    }


    if (dangerousElement == false && any_node_to_erase == false && GeometryFlags.at("TO_SPLIT") < 2)
    {

      unsigned int maxCount = 2;
      double LargestEdge = 0;

      for (unsigned int i = 0; i < 3; ++i)
      {
        if (Edges[i] > LargestEdge)
        {
          maxCount = i;
          LargestEdge = Edges[i];
        }
      }


      if (rNodesToRefine < rElementsToRefine && LargestEdge > critical_edge_length)
      {
        array_1d<double, 3> NewPosition;
        noalias(NewPosition) = 0.5 * (rGeometry[FirstEdgeNode[maxCount]].Coordinates() + rGeometry[SecondEdgeNode[maxCount]].Coordinates());

        rNodeIdsToInterpolate[rNodesToRefine][0] = rGeometry[FirstEdgeNode[maxCount]].GetId();
        rNodeIdsToInterpolate[rNodesToRefine][1] = rGeometry[SecondEdgeNode[maxCount]].GetId();

        rGeometry[FirstEdgeNode[maxCount]].Set(TO_SPLIT);
        rGeometry[SecondEdgeNode[maxCount]].Set(TO_SPLIT);

        if (rGeometry[SecondEdgeNode[maxCount]].IsNot(RIGID))
        {
          CopyDofs(rGeometry[SecondEdgeNode[maxCount]].GetDofs(), rNewDofs[rNodesToRefine]);
        }
        else if (rGeometry[FirstEdgeNode[maxCount]].IsNot(RIGID))
        {
          CopyDofs(rGeometry[FirstEdgeNode[maxCount]].GetDofs(), rNewDofs[rNodesToRefine]);
        }
        else
        {
          std::cout << "CAUTION! THIS IS A WALL EDGE" << std::endl;
        }

        //std::cout<<" NewPosition" <<NewPosition<<" maxCount "<<maxCount<<" LargestEdge "<<LargestEdge<<" critical size "<<critical_edge_length<<std::endl;
        rLargestVolumes[rNodesToRefine] = ElementalVolume;
        rNewPositions[rNodesToRefine] = NewPosition;
        rNodesToRefine++;
      }
      else if (GeometryFlags.at("FREE_SURFACE") < 3 && GeometryFlags.at("RIGID") < 3)
      {

        ElementalVolume *= penalization;
        for (unsigned int nn = 0; nn < rElementsToRefine; ++nn)
        {
          if (ElementalVolume > rLargestVolumes[nn])
          {

            bool suitableElement = true;
            if (maxCount < 3 && LargestEdge > critical_edge_length)
            {
              array_1d<double, 3> NewPosition;
              noalias(NewPosition) = 0.5 * (rGeometry[FirstEdgeNode[maxCount]].Coordinates() + rGeometry[SecondEdgeNode[maxCount]].Coordinates());

              for (unsigned int j = 0; j < rElementsToRefine; ++j)
              {
                if (rNewPositions[j][0] == NewPosition[0] && rNewPositions[j][1] == NewPosition[1])
                {
                  suitableElement = false;
                }
              }

              if (suitableElement == true)
              {

                rNodeIdsToInterpolate[nn][0] = rGeometry[FirstEdgeNode[maxCount]].GetId();
                rNodeIdsToInterpolate[nn][1] = rGeometry[SecondEdgeNode[maxCount]].GetId();

                rGeometry[FirstEdgeNode[maxCount]].Set(TO_SPLIT);
                rGeometry[SecondEdgeNode[maxCount]].Set(TO_SPLIT);

                if (rGeometry[SecondEdgeNode[maxCount]].IsNot(RIGID))
                {
                  CopyDofs(rGeometry[SecondEdgeNode[maxCount]].GetDofs(), rNewDofs[nn]);
                }
                else if (rGeometry[FirstEdgeNode[maxCount]].IsNot(RIGID))
                {
                  CopyDofs(rGeometry[FirstEdgeNode[maxCount]].GetDofs(), rNewDofs[nn]);
                }
                else
                {
                  std::cout << "CAUTION! THIS IS A WALL EDGE" << std::endl;
                }
                rLargestVolumes[nn] = ElementalVolume;
                rNewPositions[nn] = NewPosition;
              }
            }

            break;
          }
        }
      }
    }

    return true;

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  bool SelectEdgeToRefine3D(GeometryType &rGeometry,
                            std::vector<array_1d<double, 3>> &rNewPositions,
                            std::vector<double> &rLargestVolumes,
                            std::vector<array_1d<unsigned int, 4>> &rNodeIdsToInterpolate,
                            std::vector<Node<3>::DofsContainerType> &rNewDofs,
                            const double &rSize,
                            unsigned int &rNodesToRefine,
                            const unsigned int &rElementsToRefine)
  {
    KRATOS_TRY

    const unsigned int NumberOfNodes = rGeometry.size();
    ;
    std::map<std::string, unsigned int> GeometryFlags {{"RIGID", 0}, {"FREE_SURFACE", 0}, {"INLET", 0}, {"TO_SPLIT", 0}, {"INSIDE", 0}, {"INTERACTION", 0}, {"TO_ERASE", 0}};
    SetFlagsUtilities::CountGeometryFlags(rGeometry, GeometryFlags);

    bool any_node_to_erase = (GeometryFlags.at("TO_ERASE") > 0 ? true : false);
    if(any_node_to_erase)
      return false;

    double critical_edge_length = 3.0 * rSize;
    double length_tolerance = 1.6;
    double penalization = 1.0;


    if (GeometryFlags.at("INLET") > 0)
      penalization = 0.9;

    if (GeometryFlags.at("INTERACTION") > 0) // no penalize
      penalization = 1.2;

    if (GeometryFlags.at("FREE_SURFACE") == 1)
      penalization = 0.8; //penalize inserting in the boundary

    if (GeometryFlags.at("RIGID") > 2)
    {
      penalization = 0.7;
      if (GeometryFlags.at("INSIDE") > 0)
        penalization = 0.1;
    }

    double ElementalVolume = rGeometry.Volume();

    array_1d<double, 6> Edges(6, 0.0);
    array_1d<unsigned int, 6> FirstEdgeNode(6, 0);
    array_1d<unsigned int, 6> SecondEdgeNode(6, 0);
    double WallCharacteristicDistance = 0;
    array_1d<double, 3> CoorDifference;
    noalias(CoorDifference) = rGeometry[1].Coordinates() - rGeometry[0].Coordinates();

    double SquaredLength = CoorDifference[0] * CoorDifference[0] + CoorDifference[1] * CoorDifference[1] + CoorDifference[2] * CoorDifference[2];
    Edges[0] = sqrt(SquaredLength);
    FirstEdgeNode[0] = 0;
    SecondEdgeNode[0] = 1;

    if (rGeometry[0].Is(RIGID) && rGeometry[1].Is(RIGID))
    {
      WallCharacteristicDistance = Edges[0];
    }

    unsigned int Counter = 0;
    for (unsigned int i = 2; i < NumberOfNodes; ++i)
    {
      for (unsigned int j = 0; j < i; ++j)
      {
        noalias(CoorDifference) = rGeometry[i].Coordinates() - rGeometry[j].Coordinates();
        SquaredLength = CoorDifference[0] * CoorDifference[0] + CoorDifference[1] * CoorDifference[1] + CoorDifference[2] * CoorDifference[2];
        Counter += 1;
        Edges[Counter] = sqrt(SquaredLength);
        FirstEdgeNode[Counter] = j;
        SecondEdgeNode[Counter] = i;
        if (rGeometry[i].Is(RIGID) && rGeometry[j].Is(RIGID) && Edges[Counter] > WallCharacteristicDistance)
        {
          WallCharacteristicDistance = Edges[Counter];
        }
      }
    }

    //Edges connectivity: Edges[0]=d01, Edges[1]=d02, Edges[2]=d12, Edges[3]=d03, Edges[4]=d13, Edges[5]=d23
    bool dangerousElement = false;

    if (GeometryFlags.at("RIGID") > 1)
    {
      for (unsigned int i = 0; i < 6; ++i)
      {
        if ((Edges[i] < WallCharacteristicDistance * length_tolerance) || (rGeometry[FirstEdgeNode[i]].Is(RIGID) && rGeometry[SecondEdgeNode[i]].Is(RIGID)))
        {
          Edges[i] = 0;
        }

        if (rGeometry[FirstEdgeNode[i]].Or({FREE_SURFACE,RIGID}) && rGeometry[SecondEdgeNode[i]].Or({FREE_SURFACE,RIGID}) )
        {
          Edges[i] = 0;
        }
      }
    }
    else if (GeometryFlags.at("RIGID") == 1)
    {

      if (rGeometry[0].Is(RIGID))
      {
        Edges[0] = 0;
        Edges[1] = 0;
        Edges[3] = 0;
      }
      if (rGeometry[1].Is(RIGID))
      {
        Edges[0] = 0;
        Edges[2] = 0;
        Edges[4] = 0;
      }
      if (rGeometry[2].Is(RIGID))
      {
        Edges[1] = 0;
        Edges[2] = 0;
        Edges[5] = 0;
      }
      if (rGeometry[3].Is(RIGID))
      {
        Edges[3] = 0;
        Edges[4] = 0;
        Edges[5] = 0;
      }
    }

    if ((Edges[0] == 0 && Edges[1] == 0 && Edges[2] == 0 && Edges[3] == 0 && Edges[4] == 0 && Edges[5] == 0) || GeometryFlags.at("RIGID") > 2)
    {
      dangerousElement = true;
    }

    //just to fill the vector
    if (dangerousElement == false && any_node_to_erase == false && GeometryFlags.at("TO_SPLIT") < 2)
    {

      unsigned int maxCount = 6;
      double LargestEdge = 0;

      for (unsigned int i = 0; i < 6; ++i)
      {
        if (Edges[i] > LargestEdge)
        {
          maxCount = i;
          LargestEdge = Edges[i];
        }
      }

      if (rNodesToRefine < rElementsToRefine && LargestEdge > critical_edge_length)
      {

        array_1d<double, 3> NewPosition;
        double weight1 = 0.5;
        double weight2 = 0.5;
        if (rGeometry[FirstEdgeNode[maxCount]].Is(RIGID) && rGeometry[SecondEdgeNode[maxCount]].IsNot(RIGID))
        {
          weight1 = 0.2;
          weight2 = 0.8;
        }
        else if (rGeometry[FirstEdgeNode[maxCount]].IsNot(RIGID) && rGeometry[SecondEdgeNode[maxCount]].Is(RIGID))
        {
          weight1 = 0.8;
          weight2 = 0.2;
        }
        else if (rGeometry[FirstEdgeNode[maxCount]].Is(RIGID) && rGeometry[SecondEdgeNode[maxCount]].Is(RIGID))
        {
          std::cout<< " trying to insert in RIGID edge, something is not filtering correctly "<<std::endl;
          return false;
        }

        noalias(NewPosition) = (weight1 * rGeometry[FirstEdgeNode[maxCount]].Coordinates() + weight2 * rGeometry[SecondEdgeNode[maxCount]].Coordinates());

        rNodeIdsToInterpolate[rNodesToRefine][0] = rGeometry[FirstEdgeNode[maxCount]].GetId();
        rNodeIdsToInterpolate[rNodesToRefine][1] = rGeometry[SecondEdgeNode[maxCount]].GetId();

        rGeometry[FirstEdgeNode[maxCount]].Set(TO_SPLIT);
        rGeometry[SecondEdgeNode[maxCount]].Set(TO_SPLIT);

        if (rGeometry[SecondEdgeNode[maxCount]].IsNot(RIGID))
        {
          CopyDofs(rGeometry[SecondEdgeNode[maxCount]].GetDofs(), rNewDofs[rNodesToRefine]);
        }
        else if (rGeometry[FirstEdgeNode[maxCount]].IsNot(RIGID))
        {
          CopyDofs(rGeometry[FirstEdgeNode[maxCount]].GetDofs(), rNewDofs[rNodesToRefine]);
        }
        else
        {
          std::cout << "CAUTION! THIS IS A WALL EDGE" << std::endl;
        }
        rLargestVolumes[rNodesToRefine] = ElementalVolume;
        rNewPositions[rNodesToRefine] = NewPosition;
        rNodesToRefine++;

        //check
        //std::cout<<" INSERT NODE IN AN EDGE : rigid nodes "<<rigid_nodes<<std::endl;
      }
      else if (GeometryFlags.at("FREE_SURFACE") < 4 && GeometryFlags.at("RIGID") < 4)
      {

        ElementalVolume *= penalization;
        for (unsigned int nn = 0; nn < rElementsToRefine; ++nn)
        {
          if (ElementalVolume > rLargestVolumes[nn])
          {

            bool suitableElement = true;

            if ((maxCount < 6) && (LargestEdge > critical_edge_length))
            {
              array_1d<double, 3> NewPosition;

              double weight1 = 0.5;
              double weight2 = 0.5;
              if (rGeometry[FirstEdgeNode[maxCount]].Is(RIGID) && rGeometry[SecondEdgeNode[maxCount]].IsNot(RIGID))
              {
                weight1 = 0.2;
                weight2 = 0.8;
              }
              else if (rGeometry[FirstEdgeNode[maxCount]].IsNot(RIGID) && rGeometry[SecondEdgeNode[maxCount]].Is(RIGID))
              {
                weight1 = 0.8;
                weight2 = 0.2;
              }
              else if (rGeometry[FirstEdgeNode[maxCount]].Is(RIGID) && rGeometry[SecondEdgeNode[maxCount]].Is(RIGID))
              {
                std::cout<< " trying to insert in RIGID edge 2, something is not filtering correctly "<<std::endl;
                return false;
              }

              noalias(NewPosition) = (weight1 * rGeometry[FirstEdgeNode[maxCount]].Coordinates() + weight2 * rGeometry[SecondEdgeNode[maxCount]].Coordinates());

              for (unsigned int j = 0; j < rElementsToRefine; ++j)
              {
                if (rNewPositions[j][0] == NewPosition[0] && rNewPositions[j][1] == NewPosition[1] && rNewPositions[j][2] == NewPosition[2])
                {
                  suitableElement = false; //this is a repeated node, I have already choose this from another element
                }
              }

              if (suitableElement == true)
              {

                rNodeIdsToInterpolate[nn][0] = rGeometry[FirstEdgeNode[maxCount]].GetId();
                rNodeIdsToInterpolate[nn][1] = rGeometry[SecondEdgeNode[maxCount]].GetId();

                rGeometry[FirstEdgeNode[maxCount]].Set(TO_SPLIT);
                rGeometry[SecondEdgeNode[maxCount]].Set(TO_SPLIT);

                if (rGeometry[SecondEdgeNode[maxCount]].IsNot(RIGID))
                {
                  CopyDofs(rGeometry[SecondEdgeNode[maxCount]].GetDofs(), rNewDofs[nn]);
                }
                else if (rGeometry[FirstEdgeNode[maxCount]].IsNot(RIGID))
                {
                  CopyDofs(rGeometry[FirstEdgeNode[maxCount]].GetDofs(), rNewDofs[nn]);
                }
                else
                {
                  std::cout << "CAUTION! THIS IS A WALL EDGE" << std::endl;
                }
                rLargestVolumes[nn] = ElementalVolume;
                rNewPositions[nn] = NewPosition;

                //check
                //std::cout<<" INSERT NODE IN AN EDGE : rigid nodes "<<rigid_nodes<<std::endl;
              }
            }

            break;
          }
        }
      }
    }

    return true;

    KRATOS_CATCH("")
  }

  void CreateAndAddNewNodes(std::vector<array_1d<double, 3>> &rNewPositions,
                            std::vector<array_1d<unsigned int, 4>> &rNodeIdsToInterpolate,
                            std::vector<Node<3>::DofsContainerType> &rNewDofs,
                            const unsigned int &rNodesToRefine)
  {
    KRATOS_TRY

    const unsigned int dimension = mrModelPart.ElementsBegin()->GetGeometry().WorkingSpaceDimension();

    std::vector<Node<3>::Pointer> list_of_new_nodes;
    const unsigned int NodeIdParent = ModelPartUtilities::GetMaxNodeId(mrModelPart.GetParentModelPart());
    const unsigned int NodeId = ModelPartUtilities::GetMaxNodeId(mrModelPart);

    unsigned int Id = NodeIdParent + 1; //total model part node size

    if (NodeId > NodeIdParent)
    {
      Id = NodeId + 1;
      std::cout << "initial_node_size  " << Id << std::endl;
    }

    //assign data to dofs
    VariablesList &rVariablesList = mrModelPart.GetNodalSolutionStepVariablesList();

    unsigned int size = rNewPositions.size();
    if (size > rNodesToRefine)
    {
      KRATOS_WARNING("") << " Elements to Refine " << size << " but only " << rNodesToRefine << " inserted " <<std::endl;
      size = rNodesToRefine;
    }


    for (unsigned int nn = 0; nn < size; ++nn)
    {

      double x = rNewPositions[nn][0];
      double y = rNewPositions[nn][1];
      double z = 0;
      if (dimension == 3)
        z = rNewPositions[nn][2];

      //create a new node
      Node<3>::Pointer pnode = mrModelPart.CreateNewNode(Id, x, y, z);
      ++Id;

      //to control the inserted nodes
      if (mEchoLevel > 0)
        std::cout << " Insert new node [" << pnode->Id() << "] (" << x << "," << y << "," << z << ")" << std::endl;

      pnode->Set(NEW_ENTITY, true); //not boundary
      list_of_new_nodes.push_back(pnode);

      // commented JMC august 15-18 :: must be included if used in the second step of two meshing procedure
      // if(mrRemesh.InputInitializedFlag){
      //   mrRemesh.NodalPreIds.push_back( pnode->Id() );
      //   pnode->SetId(Id);
      // }

      //giving model part variables list to the node
      pnode->SetSolutionStepVariablesList(&rVariablesList);

      //set buffer size
      pnode->SetBufferSize(mrModelPart.GetBufferSize());

      //generating the dofs
      Node<3>::DofsContainerType& Reference_dofs = rNewDofs[nn];
      for(Node<3>::DofsContainerType::iterator iii = Reference_dofs.begin(); iii != Reference_dofs.end(); ++iii)
      {
        Node<3>::DofType& rDof = **iii;
        pnode->pAddDof(rDof);

        //Node<3>::DofType::Pointer p_new_dof = pnode->pAddDof(rDof);
        //(p_new_dof)->FreeDof();
      }

      Geometry<Node<3>>::PointsArrayType PointsArray;
      PointsArray.push_back(mrModelPart.pGetNode(rNodeIdsToInterpolate[nn][0]));
      PointsArray.push_back(mrModelPart.pGetNode(rNodeIdsToInterpolate[nn][1]));

      Geometry<Node<3>> LineGeometry(PointsArray);

      Vector ShapeFunctionsN(2,0.5);

      double alpha = 1;
      TransferUtilities::InterpolateStepData(*pnode, LineGeometry, ShapeFunctionsN, rVariablesList, alpha);

      if (PointsArray[0].Is(FREE_SURFACE) && PointsArray[1].Is(FREE_SURFACE))
        pnode->Set(FREE_SURFACE);

      if (PointsArray[0].Is(BOUNDARY) && PointsArray[1].Is(BOUNDARY))
        pnode->Set(BOUNDARY);

      pnode->Set(ISOLATED, false);
      pnode->Set(INTERACTION, false);
      pnode->Set(INSIDE, false);
      pnode->Set(SLIP, false);
    }

    //set the coordinates to the original value
    const array_1d<double, 3> ZeroNormal(3, 0.0);

    for (std::vector<Node<3>::Pointer>::iterator it = list_of_new_nodes.begin(); it != list_of_new_nodes.end(); ++it)
    {
      const array_1d<double, 3> &displacement = (*it)->FastGetSolutionStepValue(DISPLACEMENT);
      (*it)->X0() = (*it)->X() - displacement[0];
      (*it)->Y0() = (*it)->Y() - displacement[1];
      (*it)->Z0() = (*it)->Z() - displacement[2];

      (*it)->Set(FLUID);

      // std::cout<<" New Node [ "<<(*it)->Id()<<"]: Displacement "<<(*it)->FastGetSolutionStepValue(DISPLACEMENT)<<" Position "<<(*it)->Coordinates()<<std::endl;

      //correct contact_normal interpolation
      if ((*it)->SolutionStepsDataHas(CONTACT_FORCE))
        noalias((*it)->GetSolutionStepValue(CONTACT_FORCE)) = ZeroNormal;

      // if( mEchoLevel > 0 ){
      //   std::cout<<" New node ["<<(*it)->Id()<<"] velocity "<<(*it)->FastGetSolutionStepValue(VELOCITY)<<" acceleration "<<(*it)->FastGetSolutionStepValue(ACCELERATION)<<std::endl;
      // }
    }

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{

  /// Assignment operator.
  InsertFluidNodesMesherProcess &operator=(InsertFluidNodesMesherProcess const &rOther);

  ///@}

}; // Class Process

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                InsertFluidNodesMesherProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const InsertFluidNodesMesherProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_INSERT_FLUID_NODES_MESHER_PROCESS_HPP_INCLUDED  defined
