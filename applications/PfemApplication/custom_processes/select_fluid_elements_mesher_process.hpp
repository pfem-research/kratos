//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

#if !defined(KRATOS_SELECT_FLUID_ELEMENTS_MESHER_PROCESS_HPP_INCLUDED)
#define KRATOS_SELECT_FLUID_ELEMENTS_MESHER_PROCESS_HPP_INCLUDED

// External includes

// System includes
#include <numeric>

// Project includes
#include "custom_processes/select_elements_mesher_process.hpp"

namespace Kratos
{

///@name Kratos Classes
///@{

/// Refine Mesh Elements Process 2D and 3D
/** The process labels the elements to be refined in the mesher
    it applies a size constraint to elements that must be refined.

*/
class SelectFluidElementsMesherProcess
    : public SelectElementsMesherProcess
{
public:
  ///@name Type Definitions
  ///@{

  typedef Node<3> NodeType;
  typedef array_1d<double,3> VectorType;
  typedef BoundedVector<double,2> Point2DType;
  typedef Geometry<NodeType> GeometryType;

  /// Pointer definition of Process
  KRATOS_CLASS_POINTER_DEFINITION(SelectFluidElementsMesherProcess);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  SelectFluidElementsMesherProcess(ModelPart &rModelPart,
                                   MesherData::MeshingParameters &rRemeshingParameters,
                                   int EchoLevel)
      : SelectElementsMesherProcess(rModelPart, rRemeshingParameters, EchoLevel)
  {
  }

  /// Destructor.
  virtual ~SelectFluidElementsMesherProcess() {}

  ///@}
  ///@name Operators
  ///@{

  /// This operator is provided to call the process as a function and simply calls the Execute method.
  void operator()()
  {
    this->Execute();
  }

  ///@}
  ///@name Operations
  ///@{
  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "SelectFluidElementsMesherProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "SelectFluidElementsMesherProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{

  void SelectElementsClassic()
  {
    KRATOS_TRY

    const int &OutNumberOfElements = mrRemesh.OutMesh.GetNumberOfElements();

    if (OutNumberOfElements==0)
      KRATOS_ERROR << " 0 elements to select : NO ELEMENTS GENERATED " <<std::endl;

    //set flags for the local process execution marking slivers as SELECTED
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{SELECTED}}, {SELECTED.AsFalse()});

    //set full rigid element particles only surrounded by full rigid elements as VISITED
    this->LabelEdgeNodes(mrModelPart);

    unsigned int dimension = 0;
    unsigned int number_of_vertices = 0;
    this->GetElementDimension(dimension, number_of_vertices);

    const int *OutElementList = mrRemesh.OutMesh.GetElementList();

    ModelPartUtilities::CheckNodalH(mrModelPart);

    // LevelSetUtilities LevelSet;
    // LevelSet.CreateLevelSetFunction(mrModelPart, 0.001);
    //std::vector<array_1d<double,3>> PointsLevelSet(OutNumberOfElements);

    const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();

    ModelPart::NodesContainerType::iterator nodes_begin = mrModelPart.NodesBegin();

    std::vector<GeometryType> VerticesList(OutNumberOfElements);
    std::vector<unsigned int> slivers(OutNumberOfElements,0);

    std::vector<bool> Preserved(OutNumberOfElements,true);
    int el = 0;
    //#pragma omp parallel for
    for (el = 0; el < OutNumberOfElements; ++el)
    {
      GeometryType Vertices;
      NodalFlags VerticesFlags;
      bool accepted = this->GetVertices(el, nodes_begin, OutElementList, VerticesFlags, Vertices, number_of_vertices);

      if (VerticesFlags.Fluid == 0)
        Preserved[el] = false;
      else if (VerticesFlags.Structure > 0){
        if (VerticesFlags.Structure == number_of_vertices){
          if (VerticesFlags.Solid == number_of_vertices)
            Preserved[el] = false;
          else
            Preserved[el] = this->CheckElementFSI_STR(Vertices, VerticesFlags, dimension, rCurrentProcessInfo, slivers[el]);
        }
        else
          Preserved[el] = this->CheckElementFSI(Vertices, VerticesFlags, dimension, rCurrentProcessInfo, slivers[el]);
      }
      else if (VerticesFlags.Fluid == number_of_vertices)
        Preserved[el] = this->CheckElementFULL(Vertices, VerticesFlags, dimension, rCurrentProcessInfo, slivers[el]);

      if (VerticesFlags.Isolated != 0) //can be repeated in other lists
        if(Preserved[el])
          Preserved[el] = this->CheckElementFSI_ISO(Vertices, VerticesFlags, dimension, rCurrentProcessInfo, slivers[el]);

      //PointsLevelSet[el] = Vertices.Center();
    }

    //LevelSet.PrintPointsXYQ(mrModelPart, PointsLevelSet, "LevelSet" );

    unsigned int number_of_slivers = std::accumulate(slivers.begin(), slivers.end(), 0);
    if (number_of_slivers>=0)
      this->MarkSliverNodes(slivers, number_of_vertices);

    unsigned int counter = 0;
    for (el = 0; el < Preserved.size(); ++el)
    {
      if (Preserved[el])
	mrRemesh.PreservedElements[el] =++counter;
    }
    mrRemesh.Info->Current.Elements = counter;

    if (mEchoLevel > 0)
      std::cout << "  [Preserved Elements " << mrRemesh.Info->Current.Elements << "]  Total out: " <<OutNumberOfElements<< " slivers "<< number_of_slivers << std::endl;

    this->SelectNodesToErase();

    //set flags for the local process execution marking slivers
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{SELECTED}}, {SELECTED.AsFalse()});

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void SelectElements() override
  {
    KRATOS_TRY

    const int &OutNumberOfElements = mrRemesh.OutMesh.GetNumberOfElements();

    if (OutNumberOfElements==0)
      KRATOS_ERROR << " 0 elements to select : NO ELEMENTS GENERATED " <<std::endl;

    //set flags for the local process execution marking slivers as SELECTED
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{SELECTED}}, {SELECTED.AsFalse()});

    //set full rigid element particles only surrounded by full rigid elements as VISITED
    this->LabelEdgeNodes(mrModelPart);

    unsigned int dimension = 0;
    unsigned int number_of_vertices = 0;
    this->GetElementDimension(dimension, number_of_vertices);

    const int *OutElementList = mrRemesh.OutMesh.GetElementList();

    ModelPartUtilities::CheckNodalH(mrModelPart);

    // LevelSetUtilities LevelSet;
    // LevelSet.CreateLevelSetFunction(mrModelPart, 0.001);
    //std::vector<array_1d<double,3>> PointsLevelSet(OutNumberOfElements);

    const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();

    ModelPart::NodesContainerType::iterator nodes_begin = mrModelPart.NodesBegin();

    //create element maps FULL:FuLL fluid
    std::map<const unsigned int, std::pair<GeometryType*,NodalFlags> > TypeA;  //FULL
    std::map<const unsigned int, std::pair<GeometryType*,NodalFlags> > TypeB;  //FULL FSI / FSI
    std::map<const unsigned int, std::pair<GeometryType*,NodalFlags> > TypeC;  //FULL FSI STR / FSI STR
    std::map<const unsigned int, std::pair<GeometryType*,NodalFlags> > TypeD;  //ISOLATED

    std::vector<GeometryType> VerticesList(OutNumberOfElements);

    std::vector<bool> Preserved(OutNumberOfElements,true);
    int el = 0;
    //#pragma omp parallel for
    for (el = 0; el < OutNumberOfElements; ++el)
    {
      NodalFlags VerticesFlags;
      bool accepted = this->GetVertices(el, nodes_begin, OutElementList, VerticesFlags, VerticesList[el], number_of_vertices);

      if (VerticesFlags.Fluid == 0)
        Preserved[el] = false;
      else if (VerticesFlags.Structure > 0){
        if (VerticesFlags.Structure == number_of_vertices){
          if (VerticesFlags.Solid == number_of_vertices)
            Preserved[el] = false;
          else
            TypeC[el] = {&VerticesList[el], VerticesFlags};
        }
        else
          TypeB[el] = {&VerticesList[el], VerticesFlags};
      }
      else if (VerticesFlags.Fluid == number_of_vertices)
        TypeA[el] = {&VerticesList[el], VerticesFlags};

      if (VerticesFlags.Isolated != 0) //can be repeated in other lists
        TypeD[el] = {&VerticesList[el], VerticesFlags};

      //PointsLevelSet[el] = Vertices.Center();
    }
    //LevelSet.PrintPointsXYQ(mrModelPart, PointsLevelSet, "LevelSet" );

    std::cout<<"   FULL: "<<TypeA.size()<<" FSI :"<<TypeB.size()<<" FSI_STR "<<TypeC.size()<<" FSI_ISO "<<TypeD.size()<<std::endl;

    std::vector<unsigned int> slivers(OutNumberOfElements,0);

    std::map<const unsigned int, std::pair<GeometryType*,NodalFlags> >::iterator i_elem;

    //#pragma omp parallel for shared(TypeA) private(i_elem) //FULL
    for(i_elem = TypeA.begin(); i_elem != TypeA.end();  ++i_elem)
    {
      const unsigned int &id = i_elem->first;
      GeometryType &Vertices = *i_elem->second.first;
      NodalFlags &VerticesFlags = i_elem->second.second;
      Preserved[id] = this->CheckElementFULL(Vertices, VerticesFlags, dimension, rCurrentProcessInfo, slivers[id]);
    }

    //#pragma omp parallel for shared(TypeB) private(i_elem) //FULL FSI / FSI
    for(i_elem = TypeB.begin(); i_elem != TypeB.end();  ++i_elem)
    {
      const unsigned int &id = i_elem->first;
      GeometryType &Vertices = *i_elem->second.first;
      NodalFlags &VerticesFlags = i_elem->second.second;
      Preserved[id] = this->CheckElementFSI(Vertices, VerticesFlags, dimension, rCurrentProcessInfo, slivers[id]);
    }

    //#pragma omp parallel for shared(TypeC) private(i_elem)//FULL FSI STR / FSI STR   (complex ones)
    for(i_elem = TypeC.begin(); i_elem != TypeC.end(); ++i_elem)
    {
      const unsigned int &id = i_elem->first;
      GeometryType &Vertices = *i_elem->second.first;
      NodalFlags &VerticesFlags = i_elem->second.second;
      Preserved[id] = this->CheckElementFSI_STR(Vertices, VerticesFlags, dimension, rCurrentProcessInfo, slivers[id]);
    }

    //#pragma omp parallel for shared(TypeD) private(i_elem)//ISOLATED
    for(i_elem = TypeD.begin(); i_elem != TypeD.end();  ++i_elem)
    {
      const unsigned int &id = i_elem->first;
      GeometryType &Vertices = *i_elem->second.first;
      NodalFlags &VerticesFlags = i_elem->second.second;
      if(Preserved[id]) //this is a second check
        Preserved[id] = this->CheckElementFSI_ISO(Vertices, VerticesFlags, dimension, rCurrentProcessInfo, slivers[id]);
    }

    unsigned int number_of_slivers = std::accumulate(slivers.begin(), slivers.end(), 0);
    if (number_of_slivers>=0)
      this->MarkSliverNodes(slivers, number_of_vertices);

    unsigned int counter = 0;
    for (el = 0; el < (int)Preserved.size(); ++el)
    {
      if (Preserved[el])
	mrRemesh.PreservedElements[el] =++counter;
    }
    mrRemesh.Info->Current.Elements = counter;

    if (mEchoLevel > 0)
      std::cout << "  [Preserved Elements " << mrRemesh.Info->Current.Elements << "]  Total out: " <<OutNumberOfElements<< " slivers "<< number_of_slivers << std::endl;

    this->SelectNodesToErase();

    //set flags for the local process execution marking slivers
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{SELECTED}}, {SELECTED.AsFalse()});

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckElementFULL(GeometryType &rVertices, const NodalFlags &rVerticesFlags, const unsigned int &rDimension, const ProcessInfo &rCurrentProcessInfo, unsigned int &number_of_slivers)
  {
    unsigned int number_of_vertices = rVertices.size();
    const double &rTimeStep = rCurrentProcessInfo[DELTA_TIME];

    //1.- Check elements with a free_surface face
    double Size  = this->GetReferenceSize(rVertices, rVerticesFlags, mrRemesh, rCurrentProcessInfo);

    if (rVerticesFlags.FreeSurface >= number_of_vertices-1)
      if (GeometryUtilities::CalculateMinEdgeSize(rVertices) < 0.5*Size) //check free_surface needles -> do not accept
        return false;

    //2.- Check Alpha Shape
    double Alpha = mrRemesh.AlphaParameter;

    //FULL FREE
    if (rVerticesFlags.NoWallFreeSurface == number_of_vertices)
    {
      Alpha = 0.35;

      if (GeometryUtilities::CompareEdgeSizes(rVertices)>2.0) //2D and 3D maximum/minimum edge
        Alpha = 0.3;

      const double tolerance = 0.15; //relative side distance before and after approaching
      if(KinematicUtilities::CheckFreeSurfaceApproachingEdges(rVertices,rTimeStep,tolerance) ){
        //KRATOS_WARNING("")<<" Attention two free surfaces approaching at high speed "<<std::endl;
        Alpha = 0.4; //let the element more chances to be created if the approaching is higher than 75%
      }
    }
    else //FULL
    {
      Alpha = 0.7;
    }

    Alpha *= rDimension;

    if(GeometryUtilities::AlphaShape(Alpha, rVertices, rDimension, 4.0 * Size))
      if(this->CheckElementShape(rVertices, rVerticesFlags, Size, rDimension, rTimeStep, number_of_slivers)) // to label slivers and do not touch them
        return true;

    return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckElementFSI(GeometryType &rVertices, const NodalFlags &rVerticesFlags, const unsigned int &rDimension, const ProcessInfo &rCurrentProcessInfo, unsigned int &number_of_slivers)
  {
    if(CheckElementFSI_basic(rVertices, rVerticesFlags, rDimension, rCurrentProcessInfo, number_of_slivers)){
      if(rDimension == 3)
        return CheckElementFSI_wall(rVertices, rVerticesFlags, rDimension, rCurrentProcessInfo, number_of_slivers);
      return true;
    }
    return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckElementFSI_basic(GeometryType &rVertices, const NodalFlags &rVerticesFlags, const unsigned int &rDimension, const ProcessInfo &rCurrentProcessInfo, unsigned int &number_of_slivers)
  {
    unsigned int number_of_vertices = rVertices.size();
    const double &rTimeStep = rCurrentProcessInfo[DELTA_TIME];

    double Size  = this->GetReferenceSize(rVertices, rVerticesFlags, mrRemesh, rCurrentProcessInfo);

    bool approaching = false;

    //1.- Check layer elements with walls PRESSURE
    if (rVerticesFlags.FreeSurface > 0 && rVerticesFlags.Interaction > 0)
      if (std::abs(KinematicUtilities::CheckMeanPressure(rVertices))>1e5) // semiisolated element with large pressure errors -> do not accept
        return false;

    //2.- Check elements with a free_surface face NEEDLES
    if (rVerticesFlags.FreeSurface >= number_of_vertices-1)
      if (GeometryUtilities::CalculateMinEdgeSize(rVertices) < 0.5*Size) //check free_surface needles -> do not accept
        return false;

    //3.- Check Alpha Shape
    double Alpha = mrRemesh.AlphaParameter;

    //FULL FSI
    if (rVerticesFlags.Fluid == number_of_vertices)
    {
      Alpha = 0.7;
    }
    else //FSI
    {
      //element with nodes ouside of the free_surface
      if(rVerticesFlags.FluidWall == 0 && rVerticesFlags.FreeSurfaceWall == 0)
        if(!KinematicUtilities::CheckFreeSurfaceApproachingWall(rVertices)){
          return false;
        }

      if(rVerticesFlags.Isolated != 0){
        const double tolerance = 0.5; //relative side distance before and after approaching
        if(!KinematicUtilities::CheckFreeSurfaceApproachingWall(rVertices,rTimeStep,tolerance))
          return false;
      }

      Alpha = 0.5;
    }

    Alpha *= rDimension;

    if(GeometryUtilities::AlphaShape(Alpha, rVertices, rDimension, 4.0 * Size))
      if(this->CheckElementShape(rVertices, rVerticesFlags, Size, rDimension, rTimeStep, number_of_slivers))
        return true;

    return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckElementFSI_wall(GeometryType &rVertices, const NodalFlags &rVerticesFlags, const unsigned int &rDimension, const ProcessInfo &rCurrentProcessInfo, unsigned int &number_of_slivers)
  {
    double Size  = this->GetReferenceSize(rVertices, rVerticesFlags, mrRemesh, rCurrentProcessInfo);
    unsigned int number_of_vertices = rVertices.size();
    bool accepted = true;
    if(rVerticesFlags.FreeSurfaceWall != 0 || rVerticesFlags.OutletWall != 0)
    {
      //std::cout<<" checking advanced wall "<<std::endl;
      if(!KinematicUtilities::CheckFreeSurfaceApproachingWall(rVertices))
      {
        //std::cout<<" it seams that is not approaching "<<std::endl;
        accepted = this->CheckMeniscusFSI(rVertices, rCurrentProcessInfo[GRAVITY]);

        if (!accepted)
          accepted = CheckFSIFaceIntersection(rVertices,rVerticesFlags);

        //std::cout<<" meniscus accepted: "<<accepted<<std::endl;
        if (accepted)
          if (GeometryUtilities::CompareWallEdgeSizes(rVertices,Size)>1.6) //2D and 3D maximum/minimum edge
            accepted = false;

        //accept if the projection is inside:
        bool check_projection = false;
        if (check_projection){
          if(rVerticesFlags.FreeSurface == number_of_vertices)
          {
            if(rVerticesFlags.Structure == 3) //patchA elements
            {
              if(CheckPatchAProjection(rVertices,rVerticesFlags))
                accepted = true;
            }
            else if(rVerticesFlags.Structure == 1) //opposite patchA elements
            {
              if(CheckOppositePatchAProjection(rVertices,rVerticesFlags))
                accepted = true;
            }
          }
        }

        if(rVerticesFlags.Structure == 2 && rVerticesFlags.OutletWall != 0) //patchB elements
        {
          accepted = false;
        }

      }
      else{
        // accepted = this->CheckMeniscusFSI(rVertices, rCurrentProcessInfo[GRAVITY]);
        double size = 1.6;
        // if (!accepted)
        //   size = 1.7;
        // accepted = true;
        if (GeometryUtilities::CompareWallEdgeSizes(rVertices,Size)>size) //2D and 3D maximum/minimum edge
          accepted =  false;
      }

      // if(accepted && rVerticesFlags.Structure >= 2 && rVerticesFlags.FreeSurface == number_of_vertices)
      //   accepted = GeometryUtilities::CheckWallNeighbours(rVertices);

    }
    return accepted;

  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckElementFSI_complex(GeometryType &rVertices, const NodalFlags &rVerticesFlags, const unsigned int &rDimension, const ProcessInfo &rCurrentProcessInfo, unsigned int &number_of_slivers)
  {
    unsigned int number_of_vertices = rVertices.size();
    const double &rTimeStep = rCurrentProcessInfo[DELTA_TIME];

    double Size  = this->GetReferenceSize(rVertices, rVerticesFlags, mrRemesh, rCurrentProcessInfo);

    bool check_edges = false;
    double edge_value = (4+rDimension)/double(rDimension);

    bool check_meniscus = false;
    bool check_fluid_meniscus = false;

    bool check_approaching = false;
    bool check_approaching_distance = false;

    bool check_at_free_surface = false;
    bool check_projections = false;

    //FULL FSI
    if (rVerticesFlags.Fluid == number_of_vertices)
    {
      if (rVerticesFlags.FreeSurface > 0)
      {
        if (rVerticesFlags.Fluid == rVerticesFlags.FreeSurface)
        {
          //Alpha = 0.35;  //build above the previous surface
          if (rDimension == 3)
          {
            //PatchB elements at free surface
            if( rVerticesFlags.NoWallFreeSurface == 2 )
              return false;

            //PatchA elements at free surface --> some surrounded elements are not created
            // if( rVerticesFlags.NoWallFreeSurface == 3 )
            //   check_projections = true;

            // //Oposite PatchA elements
            // if( rVerticesFlags.NoWallFreeSurface == 1 )
            //   check_projections = true;

            if (rVerticesFlags.NoBoundaryFluid == 0)
            {
              check_fluid_meniscus = true;
              check_approaching = true;
            }
          }
        }
        else{
          //2D 10 possibilities / 2 FREE_SURFACE ISOLATED elements only problematic
          if (rDimension == 2 && rVerticesFlags.FreeSurface == 3)
          {
            check_edges = true;
          }

          //3D 22 possibilities / 3 FREE_SURFACE ISOLATED elements problematic / 4 FREE_SURFACE elements problematic
          if (rDimension == 3 && rVerticesFlags.NoBoundaryFluid == 0)
          {
            check_edges = true;
            edge_value = 1.8;
          }
        }
      }
    }
    else //FSI
    {

      //2D 3 possibilities :
      if (rDimension == 2 && rVerticesFlags.NoBoundaryFluid == 0) // 2D 2 possibilities
      {
        check_edges = true;
        check_approaching = true;
      }

      //3D 6 possibilities :
      if (rDimension == 3) // 3D 3 possibilities
      {
        check_projections = true;
        check_approaching = true;
      }

    }

    if (rVerticesFlags.NoWallFreeSurface <= 2 && rVerticesFlags.NoBoundaryFluid == 0){
      check_approaching = true;
      check_fluid_meniscus = true;
    }

    bool accepted = true;
    if(check_approaching){
      if(!this->CheckApproaching(rVertices,rVerticesFlags,rDimension,rTimeStep))
        accepted = false;
    }
    if(check_approaching_distance){
      const double tolerance = 0.5; //relative side distance before and after approaching
      if(!KinematicUtilities::CheckFreeSurfaceApproachingWall(rVertices,rTimeStep,tolerance))
        accepted = false;
    }

    if(check_edges){
      if (GeometryUtilities::CompareWallEdgeSizes(rVertices, Size) > edge_value) //2D and 3D maximum_wall_edge/maximum_non_wall_edge
        accepted = false;;
    }


    if(check_projections){
      if(rVerticesFlags.Structure == 3) //patchA elements
      {
        if(!CheckPatchAProjection(rVertices,rVerticesFlags))
          accepted = false;
      }
      else if(rVerticesFlags.Structure == 1)
      {
        if(!CheckOppositePatchAProjection(rVertices,rVerticesFlags))
          accepted = false;
      }
      else if(rVerticesFlags.Structure == 2 && rVerticesFlags.Fluid != number_of_vertices)
      {
        accepted = false;
      }
    }

    //can only be applied to new created elements on the surface with non-fluid nodes (else problems)
    if(check_at_free_surface)
      if (CheckElementAtFreeSurface(rVertices, rVerticesFlags, 0.5*Size))
        accepted = false;

    if(check_meniscus){
      if (!this->CheckMeniscusFSI(rVertices, rCurrentProcessInfo[GRAVITY])) //check 3d faces looking at the meniscus
        accepted = false;
    }

    if(check_fluid_meniscus){
      if (!this->CheckMeniscusFullFSI(rVertices, rVerticesFlags, rCurrentProcessInfo[GRAVITY])) //check 3d faces looking at the meniscus
        accepted = false;
    }

    return accepted;
  }


  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckElementFSIclassical(GeometryType &rVertices, const NodalFlags &rVerticesFlags, const unsigned int &rDimension, const ProcessInfo &rCurrentProcessInfo, unsigned int &number_of_slivers)
  {
    unsigned int number_of_vertices = rVertices.size();
    const double &rTimeStep = rCurrentProcessInfo[DELTA_TIME];

    double Size  = this->GetReferenceSize(rVertices, rVerticesFlags, mrRemesh, rCurrentProcessInfo);

    bool approaching = false;

    //1.- Check layer elements with walls PRESSURE
    if (rVerticesFlags.FreeSurface > 0 && rVerticesFlags.Interaction > 0)
      if (std::abs(KinematicUtilities::CheckMeanPressure(rVertices))>1e6) // semiisolated element with large pressure errors -> do not accept
        return false;

    //2.- Check elements with a free_surface face NEEDLES
    if (rVerticesFlags.FreeSurface >= number_of_vertices-1)
      if (GeometryUtilities::CalculateMinEdgeSize(rVertices) < 0.5*Size) //check free_surface needles -> do not accept
        return false;

    //3.- Check Alpha Shape
    double Alpha = mrRemesh.AlphaParameter;

    bool check_edges = false;
    bool check_meniscus = false;
    bool check_approaching = false;
    bool check_fluid_meniscus = false;
    bool check_at_free_surface = false;
    double edge_value = (4+rDimension)/double(rDimension);

    //FULL FSI
    if (rVerticesFlags.Fluid == number_of_vertices)
    {
      Alpha = 0.7;

      //be carefull, checking meniscus in a full fluid elements is dangerous

      //old created element conditions must be improved: check old meniscus

      if (rVerticesFlags.FreeSurface > 0)
      {
        //2D 10 possibilities / 2 FREE_SURFACE ISOLATED elements only problematic
        if (rDimension == 2 && rVerticesFlags.FreeSurface == 3)
        {
          check_edges = true;
        }

        //3D 22 possibilities / 3 FREE_SURFACE ISOLATED elements problematic / 4 FREE_SURFACE elements problematic
        if (rDimension == 3 && rVerticesFlags.FreeSurface == 4)
        {
          //Alpha = 0.35;
          //check_edges = true;
          //edge_value = 1.2;
          check_fluid_meniscus = true;
        }
        else if (rDimension == 3 && rVerticesFlags.FreeSurface == 3)
        {
          //Alpha = 0.5;
          // check_edges = true;
          // edge_value = 1.4;
          check_fluid_meniscus = true;
        }

      }
      else
        Alpha = 0.7; // no free-surface --> accept

      // this is perfomed in CompareWallEdgeSizes:
      if (rVerticesFlags.Fluid == rVerticesFlags.FreeSurface)
      {
        Alpha = 0.3;  //build above the previous surface
        //PatchA elements at free surface
        check_at_free_surface = true;
        //PatchB elements at free surface
        if( rVerticesFlags.NoWallFreeSurface == 2 )
          check_edges = true;
      }
      // improve this part to release big edge elements with walls 3D and 2D
      // if (rVerticesFlags.FreeSurface == number_of_vertices-1)
      //   if (GeometryUtilities::CompareEdgeSizes(rVertices)>2.5)
      //     Alpha = 0.7;
    }
    else //FSI
    {
      //new created element selection is ok

      //can be node-to-face (node in fluid or face in fluid) or edge-to-edge elements
      Alpha = 0.5;

      //2D 3 possibilities :
      if (rDimension == 2 && rVerticesFlags.NoBoundaryFluid == 0) // 2D 2 possibilities
      {
        check_edges = true;
        check_meniscus = true;
        check_approaching = true;
      }

      //3D 6 possibilities :
      if (rDimension == 3 && rVerticesFlags.NoBoundaryFluid == 0) // 3D 3 possibilities
      {
        check_edges = true;
        check_meniscus = true;
        check_approaching = true;
      }

      //ALE : check if mesh_velocity is higher than the velocity -> do not accept
      // if (KinematicUtilities::CheckMeshVelocity(rVertices)){
      //   std::cout<<" remove FSI element for MESH velocity "<<std::endl;
      //   return false;
      // }

    }

    if(check_approaching){
      if (rDimension==3) {
        if(this->CheckApproaching(rVertices,rVerticesFlags,rDimension,rTimeStep)){
          const double tolerance = 0.5; //relative side distance before and after approaching
          if(KinematicUtilities::CheckFreeSurfaceApproachingWall(rVertices,rTimeStep,tolerance))
            approaching = true;
        }
      }
    }

    if(check_edges){
      if (GeometryUtilities::CompareWallEdgeSizes(rVertices, Size)>edge_value){ //2D and 3D maximum_wall_edge/maximum_non_wall_edge
        return false;
      }
    }

    Alpha *= rDimension;

    if(GeometryUtilities::AlphaShape(Alpha, rVertices, rDimension, 4.0 * Size))
      if(this->CheckElementShape(rVertices, rVerticesFlags, Size, rDimension, rTimeStep, number_of_slivers)){
        //last check for meniscus (refinement of the FSI)
        if (rDimension==3 && !approaching){

          if(check_at_free_surface)
            if (CheckElementAtFreeSurface(rVertices, rVerticesFlags, 0.5*Size))
              return false;

          if(check_meniscus){
            if (CheckElementAtFreeSurface(rVertices, rVerticesFlags, 0.5*Size)){ //check condition normal if element is outside
              //std::cout<<" Fluid Meniscus FSI "<<std::endl;
              if (!this->CheckMeniscusFSI(rVertices, rCurrentProcessInfo[GRAVITY])){ //check 3d faces looking at the meniscus
                //std::cout<<" Fluid Meniscus FSI : FS "<<rVerticesFlags.FreeSurface<<std::endl;
                return false;
              }
            }
          }

          if(check_fluid_meniscus){
            if(!this->CheckApproaching(rVertices,rVerticesFlags,rDimension,rTimeStep)){
              if (!this->CheckMeniscusFullFSI(rVertices, rVerticesFlags, rCurrentProcessInfo[GRAVITY])){ //check 3d faces looking at the meniscus
                //std::cout<<" Fluid Meniscus Full FSI : FS "<<rVerticesFlags.FreeSurface<<std::endl;
                return false;
              }
            }
          }

        }
        return true;
      }

    return false;
  }


  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckElementFSI_STR(GeometryType &rVertices, const NodalFlags &rVerticesFlags, const unsigned int &rDimension, const ProcessInfo &rCurrentProcessInfo, unsigned int &number_of_slivers)
  {
    unsigned int number_of_vertices = rVertices.size();
    const double &rTimeStep = rCurrentProcessInfo[DELTA_TIME];
    double Size  = this->GetReferenceSize(rVertices, rVerticesFlags, mrRemesh, rCurrentProcessInfo);
    //FULL FSI STR
    if (rVerticesFlags.Fluid == number_of_vertices)
    {
      //isolated free_surface STR
      if (rVerticesFlags.FreeSurface == number_of_vertices){
        return false;
      }
      //other configuraions for free_surface STR stange but possible
      // if (rVerticesFlags.FreeSurface == 1){
      //   return false;
      // }
      // if (rDimension == 3 && rVerticesFlags.FreeSurface == 2){
      //   return false;
      // }

      //release isolated elements in walls
      if (rVerticesFlags.Visited == number_of_vertices){
        std::cout<<" remove FSI_STR full visited "<<rVerticesFlags.Visited<<std::endl;
        return false;
      }
      //release isolated elements in walls
      if (rDimension == 3){
        if (rVerticesFlags.Visited > 2 && rVerticesFlags.FreeSurface > 0){
          std::cout<<" remove FSI_STR visited "<<rVerticesFlags.Visited<<std::endl;
          return false;
        }
      }

    }
    else //FSI STR
    {
      if (rVerticesFlags.Visited !=0 || rVerticesFlags.FreeSurface ==0)
        return false;

      if ( KinematicUtilities::CheckMeshVelocity(rVertices) ){ //ALE : check if mesh_velocity is higher than the velocity -> do not accept
        std::cout<<" remove FSI_STR for MESH Velocity "<<std::endl;
        return false;
      }

      //3D
      if (rDimension == 3){
        if (rVerticesFlags.Fluid < number_of_vertices){
          if (rVerticesFlags.Fluid < number_of_vertices - 1)
            return false;
          else if (rVerticesFlags.Interaction == 0)
            return false;
        }
        if (rVerticesFlags.FreeSurface >= 2){
          if (CheckElementAtFreeSurface(rVertices, rVerticesFlags, 0.5*Size)){ //check condition normal if element is outside
            //std::cout<<" remove FSI_STR at free_surface "<<std::endl;
            return false;
          }
        }
      }
    }

    // Check Alpha Shape
    double Alpha = mrRemesh.AlphaParameter;
    Alpha *= 0.8; //reduction -> default too big

    // if (rDimension == 2){
    //   if(!GeometryUtilities::CheckInnerCentre(rVertices))
    //     return false;
    // }

    // Check if element is outside the walls : node NEIGBOUR_ELEMENTS (wall elements) and NEIGHBOUR_CONDITIONS (solid boundary faces) must be set
    if(GeometryUtilities::CheckWallFaceOuterCentre(rVertices)){
      // if(rVerticesFlags.Solid!=0 && rVerticesFlags.Rigid!=0)
      //   std::cout<<" SOLID/RIGID element not accepted "<<rVertices[0].Coordinates()<<" "<<rVertices[1].Coordinates()<<" "<<rVertices[2].Coordinates()<<std::endl;
      // if(rVerticesFlags.Solid==0)
      //   std::cout<<" RIGID element not accepted "<<rVertices[0].Coordinates()<<" "<<rVertices[1].Coordinates()<<" "<<rVertices[2].Coordinates()<<std::endl;
      // if(rVerticesFlags.Rigid==0)
      //   std::cout<<" RIGID element not accepted "<<rVertices[0].Coordinates()<<" "<<rVertices[1].Coordinates()<<" "<<rVertices[2].Coordinates()<<std::endl;
      return false;
    }


    if(GeometryUtilities::AlphaShape(Alpha, rVertices, rDimension, 4.0 * Size)){
      if (rVerticesFlags.FreeSurface>2)
        return this->CheckElementShape(rVertices, rVerticesFlags, Size, rDimension, rTimeStep, number_of_slivers);   // to label slivers and do not touch them
      else
        return true;
    }
    else
      return false;
  }


  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckElementFSI_ISO(GeometryType &rVertices, const NodalFlags &rVerticesFlags, const unsigned int &rDimension, const ProcessInfo &rCurrentProcessInfo, unsigned int &number_of_slivers)
  {
    unsigned int number_of_vertices = rVertices.size();
    const double &rTimeStep = rCurrentProcessInfo[DELTA_TIME];
    //FULL FSI
    if ((rVerticesFlags.Isolated + rVerticesFlags.FreeSurface) == number_of_vertices)
    {
      const double MaxRelativeVelocity = 10; //arbitrary value :: (try a criteria depending on time and size)
      if (KinematicUtilities::CheckRelativeApproach(rVertices, MaxRelativeVelocity, rTimeStep))
      {
        //set isolated nodes to erase if they approach too fast
        for (unsigned int i = 0; i < rVertices.size(); ++i)
        {
          if (rVertices[i].Is(ISOLATED))
          {
            //rVertices[i].Set(TO_ERASE); // commented to not erase ISOLATED nodes
            //to_preserve == false;
            std::cout<<" ISOLATED FLUID node ["<<rVertices[i].Id()<<"] fast approaching NODE NOT KEPT in select_elements_mesher_process.hpp"<<std::endl;
          }

        }
      }
    }

    //criterion for isolated nodes forming new wall-fluid elements
    if ( (rVerticesFlags.Isolated + rVerticesFlags.Structure) == number_of_vertices)
    {
      std::cout<<" ISOLATED with wall check volume "<<std::endl;
      MesherUtilities MesherUtils;
      double VolumeIncrement = KinematicUtilities::GetDeformationGradientDeterminant(rVertices, rDimension, rTimeStep);
      if (VolumeIncrement > 1.0)
        return false;
    }

    return true;
  }


  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckElementShape(GeometryType &rVertices, const NodalFlags &rVerticesFlags,  const double &rSize, const unsigned int &rDimension, const double &rTimeStep, unsigned int &number_of_slivers) override
  {
    bool accepted = true;
    unsigned int number_of_vertices = rVertices.size();

    if (rDimension == 2 && number_of_vertices == 3){
      accepted = CheckTriangleShape(rVertices, rVerticesFlags, rSize, rDimension, rTimeStep, number_of_slivers);
    }
    else if (rDimension == 3 && number_of_vertices == 4){
      accepted = CheckTetrahedronShape(rVertices, rVerticesFlags, rSize, rDimension, rTimeStep, number_of_slivers);
    }
    return accepted;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckTriangleShape(GeometryType &rVertices, const NodalFlags &rVerticesFlags,  const double &rSize, const unsigned int &rDimension, const double &rTimeStep, unsigned int &number_of_slivers)
  {
    bool accepted = true;
    unsigned int number_of_vertices = rVertices.size();

    //check 2D distorted elements with edges decreasing very fast
    if (rVerticesFlags.NoWallFreeSurface > 1 && rVerticesFlags.Structure == 0)
    {
      double MaxEdgeLength = std::numeric_limits<double>::min();
      double MinEdgeLength = std::numeric_limits<double>::max();

      for (unsigned int i = 0; i < number_of_vertices - 1; ++i)
      {
        for (unsigned int j = i + 1; j < number_of_vertices; ++j)
        {
          double Length = norm_2(rVertices[j].Coordinates() - rVertices[i].Coordinates());
          if (Length < MinEdgeLength)
            MinEdgeLength = Length;

          if (Length > MaxEdgeLength)
            MaxEdgeLength = Length;
        }
      }

      MesherUtilities MesherUtils;
      const double MaxRelativeVelocity = 1.5; //arbitrary value :: (try a criteria depending on time and size)
      // bool sliver = false;
      if (MinEdgeLength * 5 < MaxEdgeLength)
      {
        if (rVerticesFlags.FreeSurface == number_of_vertices)
        {
          std::cout << " WARNING 2D sliver on FreeSurface " << std::endl;
          accepted = false;
          ++number_of_slivers;
        }
        else if (KinematicUtilities::CheckRelativeVelocities(rVertices, MaxRelativeVelocity))
        {
          std::cout << " WARNING 2D sliver inside domain " << std::endl;
          // sliver = true; //commented to no erase but labeled to not calculate them
          ++number_of_slivers;
        }
      }

      // if (sliver){
      //   for (unsigned int i = 0; i < number_of_vertices; ++i)
      //   {
      //     if (rVertices[i].Is(SELECTED))
      //       std::cout << " WARNING Second sliver in the same node bis " << std::endl;
      //     rVertices[i].Set(SELECTED);
      //   }
      // }
    }

    return accepted;

  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckTetrahedronShape(GeometryType &rVertices, const NodalFlags &rVerticesFlags,  const double &rSize, const unsigned int &rDimension, const double &rTimeStep, unsigned int &number_of_slivers)
  {
    bool accepted = true;
    unsigned int number_of_vertices = rVertices.size();

    if (rVerticesFlags.FreeSurface >= 2 || (rVerticesFlags.Boundary == 4 && rVerticesFlags.NoWallFreeSurface != 4))
    {

      Tetrahedra3D4<NodeType> Tetrahedron(rVertices);
      double Volume = Tetrahedron.Volume();

      if (Volume<0){
        std::cout<<" Negative Volume Element Generated SLIVER"<<std::endl;
        return false;
      }

      if (Volume < 0.01 * pow(4.0 * rSize , rDimension))
      {
        if (mEchoLevel > 0)
          std::cout<<" SLIVER Volume="<<Volume<<" VS Critical Volume="<< 0.01*pow(4.0*rSize ,rDimension) <<std::endl;

        //only erase full free surface sliver elements (visualization purposes)
        if (rVerticesFlags.Fluid != number_of_vertices)
        {
          accepted = false;
          std::cout<<" Release partially_fluid SLIVER "<<std::endl;
        }
        else if (rVerticesFlags.FreeSurface == number_of_vertices)
        {
          accepted = false;
          std::cout<<" Release free_surface SLIVER "<<std::endl;
        }
        else if ((rVerticesFlags.FreeSurface == number_of_vertices - 1) && rVerticesFlags.Interaction > 1)
        {
          accepted = false;
          std::cout<<" Release interaction SLIVER "<<std::endl;
        }

        ++number_of_slivers;

        //other elements labeled to not calculate them
        if (mEchoLevel > 0)
          if (accepted)
            std::cout<<" Label fluid SLIVER (free_surface: "<<rVerticesFlags.FreeSurface<<" fluid "<<rVerticesFlags.Fluid<<" structure: "<<rVerticesFlags.Structure<<" interaction: "<<rVerticesFlags.Interaction<<")"<<std::endl;

      }

    }
    else if ((rVerticesFlags.Structure == 1 || rVerticesFlags.Structure == 2) && rVerticesFlags.Inside == 0)
    {
      MesherUtilities MesherUtils;

      double VolumeIncrement = KinematicUtilities::GetDeformationGradientDeterminant(rVertices, rDimension, rTimeStep);
      if (mEchoLevel > 0)
        if (VolumeIncrement < 0.0)
          std::cout << "  NEGATIVE Volume Increment (detF: " << VolumeIncrement << ")" << std::endl;

      double MovedVolume = KinematicUtilities::GetMovedVolume(rVertices, rDimension, rTimeStep, 1.0);

      //check if the element is going to invert
      if (VolumeIncrement < 0.0 || MovedVolume < 0.0)
      {
        if (mEchoLevel>0)
          std::cout<<" SLIVER FOR INVERSION "<<VolumeIncrement<<" VS Critical Volume="<< 0.01*pow(4.0*rSize ,rDimension) <<"( rigid: "<<rVerticesFlags.Rigid<<" ) moved volume "<<MovedVolume<<std::endl;

        ++number_of_slivers;
      }
    }

    return accepted;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  // only works for PatchA elements
  bool CheckElementAtFreeSurface(GeometryType &rVertices, const NodalFlags &rVerticesFlags, const double Size)
  {
    unsigned int dimension = 0;
    unsigned int number_of_vertices = 0;
    this->GetElementDimension(dimension, number_of_vertices);

    if (dimension == 3 && rVerticesFlags.FreeSurface > 0) //always true from FSI and FSI STR
    {
      Tetrahedra3D4<NodeType> Tetrahedron(rVertices);
      GeometryType &eGeometry = Tetrahedron;

      //get matrix nodes in faces
      DenseMatrix<unsigned int> lpofa; //points that define the faces
      eGeometry.NodesInFaces(lpofa);

      std::vector<unsigned int> fluid_face_candidates;
      std::vector<unsigned int> fluid_edge_candidates;
      std::vector<unsigned int> fluid_point_candidates;
      for (unsigned int iface = 0; iface < eGeometry.size(); ++iface)
      {
        //check if free surface
        unsigned int free_surface = 0;
        for (unsigned int i = 1; i <= 3; ++i)
          if (eGeometry[lpofa(i, iface)].Is(FREE_SURFACE))
            ++free_surface;

        if(free_surface==3)
          fluid_face_candidates.push_back(iface);
        else if (free_surface==2)
          fluid_edge_candidates.push_back(iface);
        else if (free_surface==1)
          fluid_point_candidates.push_back(iface);
      }

      // std::cout<<" Check Element at Free Surface face: ["<<rVerticesFlags.Structure<<"] "<<fluid_face_candidates.size()<<" edge: "<<fluid_edge_candidates.size()<<" point: "<<fluid_point_candidates.size()<<" structure nodes "<<structure_nodes<<std::endl;

      //face conditions
      for (unsigned int i = 0; i < fluid_face_candidates.size(); ++i)
      {
        unsigned int &iface = fluid_face_candidates[i];
        std::vector<unsigned int> element_face;
        for (unsigned int j = 3; j >= 1; --j)
          element_face.push_back(eGeometry[lpofa(j, iface)].Id());

        ConditionWeakPtrVectorType &nConditions = eGeometry[lpofa(1, iface)].GetValue(NEIGHBOUR_CONDITIONS);

        for (auto &i_cond : nConditions)
        {
          GeometryType& cGeometry = i_cond.GetGeometry();
          bool found = SearchUtilities::FindCondition(cGeometry, element_face);
          if (found)
          {
            double projection = inner_prod(eGeometry.Center()-cGeometry.Center(), i_cond.GetValue(NORMAL));
            if (projection > 0) //is outside
            {
              //std::cout<<" projection face "<<projection<<" "<<Size<<std::endl;
              if(projection > Size)
                return true;
              // else
              //   return false;
            }
          }
        }
      }

      //edge conditions
      for (unsigned int i = 0; i < fluid_edge_candidates.size(); ++i)
      {
        unsigned int &iface = fluid_edge_candidates[i];

        for (unsigned int j = 1; j <= 3; ++j)
        {
          if(eGeometry[lpofa(j, iface)].Is(FREE_SURFACE)){
            ConditionWeakPtrVectorType &nConditions = eGeometry[lpofa(j, iface)].GetValue(NEIGHBOUR_CONDITIONS);

            for (auto &i_cond : nConditions)
            {
              GeometryType& cGeometry = i_cond.GetGeometry();
              unsigned int structure = 0;
              unsigned int freesurface = 0;
              for (auto &i_node : cGeometry)
              {
                if(i_node.Is(STRUCTURE))
                  ++structure;
                if(i_node.Is(FREE_SURFACE))
                  ++freesurface;
              }

              if ( freesurface == 3 && structure == 2 ) //structure can be 1 also
              {
                double projection = inner_prod(eGeometry.Center()-cGeometry.Center(), i_cond.GetValue(NORMAL));
                if (projection > 0) //is outside
                {
                  if(projection > Size)
                    return true;
                  // else
                  //   return false;
                }
              }
            }
          }
        }
      }

      //point conditions
      for (unsigned int i = 0; i < fluid_point_candidates.size(); ++i)
      {
        unsigned int &iface = fluid_point_candidates[i];

        for (unsigned int j = 1; j <= 3; ++j)
        {
          if(eGeometry[lpofa(j, iface)].Is(FREE_SURFACE)){

            ConditionWeakPtrVectorType &nConditions = eGeometry[lpofa(j, iface)].GetValue(NEIGHBOUR_CONDITIONS);
            for (auto &i_cond : nConditions)
            {
              GeometryType& cGeometry = i_cond.GetGeometry();

              unsigned int structure = 0;
              unsigned int freesurface = 0;
              for (auto &i_node : cGeometry)
              {
                if(i_node.Is(STRUCTURE))
                  ++structure;
                if(i_node.Is(FREE_SURFACE))
                  ++freesurface;
              }

              if ( freesurface == 3 && structure == 2 ) //structure can be 1 also
              {
                double projection = inner_prod(eGeometry.Center()-cGeometry.Center(), i_cond.GetValue(NORMAL));
                if (projection > 0) //is outside
                {
                  if(projection > Size)
                    return true;
                  // else
                  //   return false;
                }
              }
            }
          }
        }

      }
    }

    return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckMeniscusFSI(GeometryType &rVertices, const array_1d<double,3> &rGravity)
  {
    double slope = 0.3;
    return GeometryUtilities::CheckNewFluidMeniscus(rVertices, rGravity, slope);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckGravityMeniscusFSI(GeometryType &rVertices, const array_1d<double,3> &rGravity)
  {
    double slope = 0.3;
    return GeometryUtilities::CheckOldFluidMeniscus(rVertices, rGravity, slope);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckMeniscusFullFSI(GeometryType &rVertices, const NodalFlags &rVerticesFlags, const array_1d<double,3> &rGravity)
  {
    double slope = 0.4;
    if (rVerticesFlags.NoWallFreeSurface > 0 && rVerticesFlags.Interaction > 0)
      if (rVerticesFlags.FreeSurface == rVerticesFlags.Fluid || (rVerticesFlags.FreeSurface == rVerticesFlags.Structure-1 && rVerticesFlags.NoBoundaryFluid == 0))
        return GeometryUtilities::CheckOldFluidMeniscus(rVertices, rGravity, slope);
  }

  //*******************************************************************************************
  //*******************************************************************************************

  //it can set TO_ERASE to new entities inserted in full rigid elements
  bool CheckElementFluidMeniscus(GeometryType &rVertices, const NodalFlags &rVerticesFlags, const array_1d<double,3> &rGravity, const double &rSize, const unsigned int &rDimension, const double &rTimeStep)
  {
    unsigned int dimension = 0;
    unsigned int number_of_vertices = 0;
    double slope = 0.4;

    this->GetElementDimension(dimension, number_of_vertices);

    if (dimension == 3)
    {
      bool check = false;
      if (rVerticesFlags.NoWallFreeSurface > 0){
        if (rVerticesFlags.FreeSurface == rVerticesFlags.Fluid){
          check = true;
        }
        else if (rVerticesFlags.Fluid == number_of_vertices && rVerticesFlags.FreeSurface == number_of_vertices-1 && rVerticesFlags.FreeSurface == rVerticesFlags.Structure){
          check = true;
        }
      }

      // if (rVerticesFlags.Fluid == number_of_vertices-1 && rVerticesFlags.Structure>=2 && rVerticesFlags.FreeSurface==2){ //case that the free surface element has disappeared and the menistus is formed with an interior node
      //   check = true;
      // }

      // Check all elements with a free surface having wall and non-wall nodes
      if (check)
      {
        if (GeometryUtilities::CheckOldFluidMeniscus(rVertices, rGravity, slope))
        {
          //do not accept full FULL FSI elements with a new inserted node
          if ((rVerticesFlags.Structure + rVerticesFlags.NewEntity) == number_of_vertices && rVerticesFlags.Fluid == number_of_vertices && rVerticesFlags.Visited >= number_of_vertices - 1)
          {
              std::cout << " Fluid Meniscus EDGE (old_entity: " << rVerticesFlags.Visited << " fluid: " << rVerticesFlags.Fluid << " rigid: " << rVerticesFlags.Rigid << " free_surface: " << rVerticesFlags.FreeSurface << ")" << std::endl;
              return false;
          }

          //do not accept rigid wall elements with fluid node separating from walls
          if (rVerticesFlags.Structure == number_of_vertices-1 && rVerticesFlags.NoWallFreeSurface > 0)
          {
            if (KinematicUtilities::CheckFreeSurfaceApproachingWall(rVertices)){
              double VolumeIncrement = KinematicUtilities::GetDeformationGradientDeterminant(rVertices, rDimension, rTimeStep);
              if (VolumeIncrement > 1.1){
                std::cout<<" Fluid MENISCUS separating "<<VolumeIncrement<<std::endl;
                return false;
              }
            }
          }

        }
        else{
          return false;
        }
      }
    }
    return true;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckPatchAProjection(GeometryType &rVertices, const NodalFlags &rVerticesFlags)
  {
    std::vector<VectorType> face;
    VectorType P;
    for(auto& i_node : rVertices)
      if(i_node.Is(STRUCTURE))
        face.push_back(i_node.Coordinates());
      else
        P = i_node.Coordinates();

    const VectorType& V0 = face[0];
    const VectorType& V1 = face[1];
    const VectorType& V2 = face[2];

    //double check for three corner elements
    if(rVerticesFlags.NoWallFreeSurface == 1 && rVerticesFlags.Structure == 3 && rVerticesFlags.NoBoundaryFluid == 0)
    {
      VectorType N;
      for(auto& i_node : rVertices){
        if(i_node.Is(STRUCTURE)){
          N = i_node.FastGetSolutionStepValue(NORMAL);
          if(!GeometryUtilities::CheckPointInSurfaceTriangle(V0,V1,V2,P,N))
            return false;
        }
      }
    }

    return GeometryUtilities::CheckPointInSurfaceTriangle(V0,V1,V2,P);
  }


  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckOppositePatchAProjection(GeometryType &rVertices, const NodalFlags &rVerticesFlags)
  {
    std::vector<VectorType> face;
    VectorType P, N;
    for(auto& i_node : rVertices)
      if(i_node.IsNot(STRUCTURE))
        face.push_back(i_node.Coordinates());
      else{
        P = i_node.Coordinates();
        N = i_node.FastGetSolutionStepValue(NORMAL);
      }

    const VectorType& V0 = face[0];
    const VectorType& V1 = face[1];
    const VectorType& V2 = face[2];

    return GeometryUtilities::CheckPointInSurfaceTriangle(V0,V1,V2,P,N);
  }


  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckFSIFaceIntersection(GeometryType &rVertices, const NodalFlags &rVerticesFlags)
  {

    if(rVerticesFlags.Structure == 1){
      //std::cout<<" Opposite PATCH A "<<std::endl;
      int iface = -1;
      for(unsigned int i=0; i < rVertices.size(); ++i){
        if(rVertices[i].Is(STRUCTURE)){
          iface = i;
          break;
        }
      }
      if(iface>=0){
        Tetrahedra3D4<NodeType> Tetrahedron(rVertices);
        GeometryType &rGeometry = Tetrahedron;

        //get matrix nodes in faces
        DenseMatrix<unsigned int> lpofa; //points that define the faces
        rGeometry.NodesInFaces(lpofa);

        ElementWeakPtrVectorType &nElements = rVertices[iface].GetValue(NEIGHBOUR_ELEMENTS);
        // std::cout<<" NEIGHBOUR ELEMENTS "<<nElements.size()<<std::endl;
        for (auto &i_elem : nElements)
        {
          // Check overlapping faces if coincident (opposite) normals
          GeometryType &eGeometry = i_elem.GetGeometry();
          if(eGeometry.size() < rGeometry.size()){
            // Both Triangles must be expressed anti-clockwise
            if (GeometryUtilities::CheckSurfaceTrianglesIntersection(eGeometry[0].Coordinates(),eGeometry[2].Coordinates(),eGeometry[1].Coordinates(),rVertices[lpofa(1, iface)].Coordinates(),rVertices[lpofa(2, iface)].Coordinates(),rVertices[lpofa(3, iface)].Coordinates()))
              return true;
          }
        }
      }
      else
        return true;
    }
    else if(rVerticesFlags.Structure == 3){
      //std::cout<<" PATCH A "<<std::endl;
      int iface = -1;
      for(unsigned int i=0; i < rVertices.size(); ++i){
        if(rVertices[i].IsNot(STRUCTURE) && rVertices[i].Is(BOUNDARY)){
          iface = i;
          break;
        }
      }
      if(iface>=0){
        Tetrahedra3D4<NodeType> Tetrahedron(rVertices);
        GeometryType &rGeometry = Tetrahedron;

        //get matrix nodes in faces
        DenseMatrix<unsigned int> lpofa; //points that define the faces
        rGeometry.NodesInFaces(lpofa);

        ConditionWeakPtrVectorType &nConditions = rVertices[iface].GetValue(NEIGHBOUR_CONDITIONS);
        for (auto &i_cond : nConditions)
        {
          // Check overlapping faces if coincident (opposite) normals
          GeometryType &cGeometry = i_cond.GetGeometry();

          // std::cout<<" Vertices["<<rVertices[lpofa(1, iface)].Id()<<", "<<rVertices[lpofa(2, iface)].Id()<<", "<<rVertices[lpofa(3, iface)].Id()<<"] vs ["<<cGeometry[0].Id()<<", "<<cGeometry[2].Id()<<", "<<cGeometry[1].Id()<<"] "<<std::endl;

          // std::cout<<" Vertices Coordinates["<<rVertices[lpofa(1, iface)].Coordinates()<<" "<<rVertices[lpofa(2, iface)].Coordinates()<<" "<<rVertices[lpofa(3, iface)].Coordinates()<<"] vs ["<<cGeometry[0].Coordinates()<<" "<<cGeometry[2].Coordinates()<<" "<<cGeometry[1].Coordinates()<<"] "<<std::endl;

          // Both Triangles must be expressed anti-clockwise
          if (GeometryUtilities::CheckSurfaceTrianglesIntersection(rVertices[lpofa(1, iface)].Coordinates(),rVertices[lpofa(2, iface)].Coordinates(),rVertices[lpofa(3, iface)].Coordinates(),cGeometry[0].Coordinates(),cGeometry[2].Coordinates(),cGeometry[1].Coordinates()))
            return true;

        }
      }
      else
        return true;

    }
    else if (rVerticesFlags.Structure == 2){
      //std::cout<<" PATCH B "<<std::endl;
      std::vector<unsigned int> wall_nodes;
      std::vector<unsigned int> fluid_nodes;
      for(unsigned int i=0; i < rVertices.size(); ++i){
        if(rVertices[i].Is(STRUCTURE))
          wall_nodes.push_back(i);
        else if (rVertices[i].Is(BOUNDARY))
          fluid_nodes.push_back(i);
      }
      if(wall_nodes.size() != 2 || fluid_nodes.size() != 2){
        return true;
      }
      ElementWeakPtrVectorType wall_elements;
      ConditionWeakPtrVectorType fluid_conditions;
      if(wall_nodes.size() == 2){
        for (auto &i_elem : rVertices[wall_nodes.front()].GetValue(NEIGHBOUR_ELEMENTS))
        {
          if(i_elem.GetGeometry().size() < rVertices.size()){
            for (auto &j_elem : rVertices[wall_nodes.back()].GetValue(NEIGHBOUR_ELEMENTS))
              if(j_elem.GetGeometry().size() < rVertices.size()){
                if(i_elem.Id() == j_elem.Id()){
                  unsigned int count = 0;
                  for(auto &i_node : i_elem.GetGeometry()){
                    if(i_node.Id() == rVertices[wall_nodes.front()].Id() || i_node.Id() == rVertices[wall_nodes.back()].Id())
                      ++count;
                  }
                  if(count==2)
                    wall_elements.push_back(&i_elem);
                }
              }
          }
        }
      }
      if(fluid_nodes.size() == 2){

        for (auto &i_cond : rVertices[fluid_nodes.front()].GetValue(NEIGHBOUR_CONDITIONS))
        {
          for (auto &j_cond : rVertices[fluid_nodes.back()].GetValue(NEIGHBOUR_CONDITIONS))
          {
            if(i_cond.Id() == j_cond.Id()){
              unsigned int count = 0;
              for(auto &i_node : i_cond.GetGeometry())
              {
                if(i_node.Id() == rVertices[fluid_nodes.front()].Id() || i_node.Id() == rVertices[fluid_nodes.back()].Id())
                  ++count;
              }
              if(count==2)
                fluid_conditions.push_back(&i_cond);
            }
          }
        }
      }
      if(wall_elements.size() == 0 || fluid_conditions.size() == 0){
        //std::cout<<" NO EDGE FACES OR CONDITIONS FOUND wall: "<<wall_elements.size()<<" fluid: "<<fluid_conditions.size()<<" FS "<<rVerticesFlags.Structure<<std::endl;
        return true;
      }

      for (auto &i_elem : wall_elements)
      {
        for (auto &i_cond : fluid_conditions)
        {
          // Check overlapping faces if coincident (opposite) normals
          GeometryType &eGeometry = i_elem.GetGeometry();
          GeometryType &cGeometry = i_cond.GetGeometry();
          // Both Triangles must be expressed anti-clockwise
          if (GeometryUtilities::CheckSurfaceTrianglesIntersection(eGeometry[0].Coordinates(),eGeometry[1].Coordinates(),eGeometry[2].Coordinates(),cGeometry[0].Coordinates(),cGeometry[1].Coordinates(),cGeometry[2].Coordinates()))
            return true;
        }
      }
    }
    else
      return true;

    return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  // Check approaching node
  bool CheckApproaching(GeometryType &rVertices, const NodalFlags &rVerticesFlags, const unsigned int &rDimension, const double &rTimeStep)
  {
    bool approaching = false;
    //do not accept rigid wall elements with fluid node separating from walls
    if ( (rVerticesFlags.NoWallFreeSurface > 0 && rVerticesFlags.Interaction > 0 && rVerticesFlags.Fluid > 1) || rVerticesFlags.Isolated > 0)
    {
      approaching = KinematicUtilities::CheckFreeSurfaceApproachingWall(rVertices);

      if (approaching){
        double VolumeIncrement = KinematicUtilities::GetDeformationGradientDeterminant(rVertices, rDimension, rTimeStep);
        if (VolumeIncrement > 1.01)
          return false;
      }
    }
    return approaching;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  // It sets full previous rigid element nodes with the flag VISITED
  void LabelEdgeNodes(ModelPart &rModelPart)
  {
    //set flags for the local process execution
    SetFlagsUtilities::SetFlagsToElements(mrModelPart, {{VISITED}}, {VISITED.AsFalse()});
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{VISITED}}, {VISITED.AsFalse()});

    //set label to full rigid element nodes
    for (auto &i_elem : rModelPart.Elements())
    {
      GeometryType &rGeometry = i_elem.GetGeometry();

      unsigned int count_rigid = 0;
      // unsigned int count_free_surface = 0;
      for (auto &i_node : rGeometry)
      {
        if (i_node.Is(RIGID))
        {
          count_rigid++;
        }
        // else if (i_node.Is(FREE_SURFACE))
        // {
        //   count_free_surface++;
        // }
      }

      if (count_rigid == rGeometry.size())
      {
        i_elem.Set(VISITED, true);
        for (auto &i_node : rGeometry)
        {
          i_node.Set(VISITED, true);
        }
      }
    }

    //unset label for nodes not fully rigid
    for (auto &i_elem : rModelPart.Elements())
    {
      GeometryType &rGeometry = i_elem.GetGeometry();

      //to release full rigid elements with no fluid surrounding them
      if (i_elem.IsNot(VISITED))
      {
        for (auto &i_node : rGeometry)
        {
          i_node.Set(VISITED, false);
        }
      }
      else{
        i_elem.Set(VISITED, false);
      }

    }

  }


  //*******************************************************************************************
  //*******************************************************************************************

  void SelectNodesToErase() override
  {
    // Set disconnected nodes to erase
    unsigned int dimension = 0;
    unsigned int number_of_vertices = 0;
    unsigned int isolated_nodes = 0;

    this->GetElementDimension(dimension, number_of_vertices);

    int *OutElementList = mrRemesh.OutMesh.GetElementList();

    ModelPart::NodesContainerType &rNodes = mrModelPart.Nodes();

    const int &OutNumberOfElements = mrRemesh.OutMesh.GetNumberOfElements();

    //set flags for the local process execution
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{BLOCKED}}, {BLOCKED.AsFalse()});
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{MARKER}}, {MARKER.AsFalse()});

    //check engaged nodes
    for (int el = 0; el < OutNumberOfElements; ++el)
    {
      if (mrRemesh.PreservedElements[el])
      {
        for (unsigned int pn = 0; pn < number_of_vertices; ++pn)
        {
          //set vertices
          rNodes[OutElementList[el * number_of_vertices + pn]].Set(BLOCKED);
        }
      }
    }

    int count_release = 0;
    for (ModelPart::NodesContainerType::iterator i_node = rNodes.begin(); i_node != rNodes.end(); ++i_node)
    {
      if (i_node->IsNot(BLOCKED))
      {

        if (i_node->Is(TO_ERASE) && i_node->Or({RIGID,SOLID,INLET}))
        {
          i_node->Set(TO_ERASE, false);
          std::cout << " WARNING TRYING TO DELETE A WALL NODE (fluid): " << i_node->Id() << std::endl;
        }
        else if (i_node->AndNot({RIGID,SOLID,INLET}))
        {
          if (mrRemesh.ExecutionOptions.Is(MesherData::KEEP_ISOLATED_NODES))
          {
            if (i_node->IsNot(TO_ERASE))
            {
              // if (i_node->Is(FREE_SURFACE))
              // {
                i_node->Set(ISOLATED, true);
                ++isolated_nodes;
                //std::cout<<" ISOLATED FREE SURFACE FLUID "<<std::endl;
              // }
              // else
              // {
              //   // in fact can be a new free surface node -> check if it happens
              //   i_node->Set(TO_ERASE, true);
              //   std::cout<<"  ISOLATED inside FLUID node TO ERASE in select_elements_mesher_process.hpp "<<i_node->IsNot(BLOCKED)<<std::endl;
              // }
            }
          }
          else
          {
            std::cout<<" ISOLATED FLUID NODES NOT KEPT in select_elements_mesher_process.hpp"<<std::endl;
            i_node->Set(TO_ERASE, true);
            if (mEchoLevel > 0)
            {
              if (i_node->Is(BOUNDARY))
                std::cout << " NODE " << i_node->Id() << " IS BOUNDARY RELEASE " << std::endl;
              else
                std::cout << " NODE " << i_node->Id() << " IS DOMAIN RELEASE " << std::endl;
            }
            ++count_release;
          }
        }

      }
      else  // BLOCKED NODE
      {
        i_node->Set(TO_ERASE, false);
        i_node->Set(ISOLATED, false);
      }

      i_node->Set(BLOCKED, false);

      i_node->Set(VISITED, false);

      if (i_node->Is(SELECTED)) // Belongs to a SLIVER
        i_node->Set(MARKER, true);
      else
        i_node->Set(MARKER, false);

      i_node->Set(SELECTED, false);
    }

    if (mEchoLevel > 0)
    {
      std::cout << "   NUMBER OF RELEASED NODES " << count_release << std::endl;
      std::cout << "   NUMBER OF ISOLATED NODES " << isolated_nodes << std::endl;
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void MarkSliverNodes(const std::vector<unsigned int> &slivers, unsigned int &number_of_vertices)
  {
    ModelPart::NodesContainerType::iterator nodes_begin = mrModelPart.NodesBegin();
    const int *OutElementList = mrRemesh.OutMesh.GetElementList();

    int el = 0;

#pragma omp parallel for
    for (el = 0; el < (int)slivers.size(); ++el)
    {
      if (slivers[el]>0){
	GeometryType Vertices;
	NodalFlags VerticesFlags;
	this->GetVertices(el, nodes_begin, OutElementList, VerticesFlags, Vertices, number_of_vertices);
	for (auto &i_node : Vertices)
	  i_node.Set(MARKER);
      }

    }
  }

  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Private Static Member Variables
  ///@{
  ///@}
  ///@name Private Static Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{
  ///@}

}; // Class Process

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                SelectFluidElementsMesherProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const SelectFluidElementsMesherProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_SELECT_FLUID_ELEMENTS_MESHER_PROCESS_HPP_INCLUDED defined
