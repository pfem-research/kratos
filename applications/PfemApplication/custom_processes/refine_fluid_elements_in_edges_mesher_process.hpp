//
//   Project Name:        KratosPfemApplication    $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:         July 2018 $
//
//

#if !defined(KRATOS_REFINE_FLUID_ELEMENTS_IN_EDGES_MESHER_PROCESS_HPP_INCLUDED)
#define KRATOS_REFINE_FLUID_ELEMENTS_IN_EDGES_MESHER_PROCESS_HPP_INCLUDED

// External includes

// System includes

// Project includes
#include "custom_processes/refine_elements_in_edges_mesher_process.hpp"

namespace Kratos
{

///@name Kratos Classes
///@{

/// Refine Mesh Elements Process 2D and 3D
/** The process inserts nodes in the rigid edge elements to be splitted in the remeshing process
*/

class RefineFluidElementsInEdgesMesherProcess
    : public RefineElementsInEdgesMesherProcess
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of Process
  KRATOS_CLASS_POINTER_DEFINITION(RefineFluidElementsInEdgesMesherProcess);

  typedef ModelPart::ConditionType ConditionType;
  typedef ModelPart::PropertiesType PropertiesType;
  typedef ConditionType::GeometryType GeometryType;

  typedef RefineElementsInEdgesMesherProcess BaseType;

  typedef GlobalPointersVector<Node<3>> NodeWeakPtrVectorType;
  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  RefineFluidElementsInEdgesMesherProcess(ModelPart &rModelPart,
                                          MesherData::MeshingParameters &rRemeshingParameters,
                                          int EchoLevel) : BaseType(rModelPart, rRemeshingParameters, EchoLevel)
  {
  }

  /// Destructor.
  virtual ~RefineFluidElementsInEdgesMesherProcess() {}

  ///@}
  ///@name Operators
  ///@{

  /// This operator is provided to call the process as a function and simply calls the Execute method.
  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{
  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "RefineFluidElementsInEdgesMesherProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "RefineFluidElementsInEdgesMesherProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{
  ///@}

  //**************************************************************************
  //**************************************************************************

  void SelectFullBoundaryEdgedElements(ModelPart &rModelPart,
                                       std::vector<Element*> &rEdgedElements) override
  {
    KRATOS_TRY

    bool is_full_rigid_boundary = false;
    bool is_full_fluid_boundary = false;
    bool is_dangerous_element = false;
    ModelPart::ElementsContainerType &rElements = rModelPart.Elements();

    for (auto& i_elem : rElements)
    {
      Geometry<Node<3>> &rGeometry = i_elem.GetGeometry();

      is_full_rigid_boundary = true;
      for (unsigned int i = 0; i < rGeometry.size(); ++i)
      {
        if (rGeometry[i].IsNot(STRUCTURE))
        {
          is_full_rigid_boundary = false;
          break;
        }
      }

      is_full_fluid_boundary = true;
      for (unsigned int i = 0; i < rGeometry.size(); ++i)
      {
        if (rGeometry[i].IsNot(FREE_SURFACE) || rGeometry[i].Is(STRUCTURE))
        {
          is_full_fluid_boundary = false;
          break;
        }
      }

      if (is_full_fluid_boundary){
        for (unsigned int i = 0; i < rGeometry.size(); ++i)
        {
          NodeWeakPtrVectorType &nNodes = rGeometry[i].GetValue(NEIGHBOUR_NODES);
          for (auto &i_nnode : nNodes)
          {
            if (i_nnode.Is(STRUCTURE))
            {
              is_full_fluid_boundary = false;
              break;
            }
          }
        }
      }

      is_dangerous_element = false;
      if (is_full_rigid_boundary || is_full_fluid_boundary){
        is_dangerous_element = true;
        for (unsigned int i = 0; i < rGeometry.size(); ++i)
        {
          NodeWeakPtrVectorType &nNodes = rGeometry[i].GetValue(NEIGHBOUR_NODES);
          for (auto &i_nnode : nNodes)
          {
            if (i_nnode.IsNot(FREE_SURFACE) && i_nnode.IsNot(STRUCTURE))
            {
              is_dangerous_element = false;
              break;
            }
          }
          if (!is_dangerous_element){
            break;
          }
        }
      }

      if(!is_dangerous_element && rGeometry.WorkingSpaceDimension() == 3)
        is_dangerous_element = GeometryUtilities::CheckSliver(rGeometry);

      // if( is_full_rigid_boundary )
      //   std::cout<<" is full rigid boundary "<<std::endl;

      // if( is_full_fluid_boundary )
      //   std::cout<<" is full fluid boundary "<<std::endl;

      if ((is_full_rigid_boundary || is_full_fluid_boundary) && !is_dangerous_element)
      {
        rEdgedElements.push_back(&i_elem);
      }
    }

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Private Static Member Variables
  ///@{
  ///@}
  ///@name Private Static Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{

  /// Assignment operator.
  RefineFluidElementsInEdgesMesherProcess &operator=(RefineFluidElementsInEdgesMesherProcess const &rOther);

  ///@}

}; // Class Process

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                RefineFluidElementsInEdgesMesherProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const RefineFluidElementsInEdgesMesherProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_REFINE_FLUID_ELEMENTS_IN_EDGES_MESHER_PROCESS_HPP_INCLUDED  defined
