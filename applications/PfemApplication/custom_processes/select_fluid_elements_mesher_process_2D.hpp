//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

#if !defined(KRATOS_SELECT_FLUID_ELEMENTS_MESHER_PROCESS_HPP_INCLUDED)
#define KRATOS_SELECT_FLUID_ELEMENTS_MESHER_PROCESS_HPP_INCLUDED

// External includes

// System includes
#include <numeric>

// Project includes
#include "custom_processes/select_elements_mesher_process.hpp"

namespace Kratos
{

///@name Kratos Classes
///@{

/// Refine Mesh Elements Process 2D and 3D
/** The process labels the elements to be refined in the mesher
    it applies a size constraint to elements that must be refined.

*/
class SelectFluidElementsMesherProcess
    : public SelectElementsMesherProcess
{
public:
  ///@name Type Definitions
  ///@{

  typedef Node<3> NodeType;
  typedef Geometry<NodeType> GeometryType;

  /// Pointer definition of Process
  KRATOS_CLASS_POINTER_DEFINITION(SelectFluidElementsMesherProcess);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  SelectFluidElementsMesherProcess(ModelPart &rModelPart,
                                   MesherData::MeshingParameters &rRemeshingParameters,
                                   int EchoLevel)
      : SelectElementsMesherProcess(rModelPart, rRemeshingParameters, EchoLevel)
  {
  }

  /// Destructor.
  virtual ~SelectFluidElementsMesherProcess() {}

  ///@}
  ///@name Operators
  ///@{

  /// This operator is provided to call the process as a function and simply calls the Execute method.
  void operator()()
  {
    this->Execute();
  }

  ///@}
  ///@name Operations
  ///@{
  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "SelectFluidElementsMesherProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "SelectFluidElementsMesherProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{

  void SelectElementsClassic()
  {
    KRATOS_TRY

    const int &OutNumberOfElements = mrRemesh.OutMesh.GetNumberOfElements();

    if (OutNumberOfElements==0)
      KRATOS_ERROR << " 0 elements to select : NO ELEMENTS GENERATED " <<std::endl;

    //set flags for the local process execution marking slivers as SELECTED
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{SELECTED}}, {SELECTED.AsFalse()});

    //set full rigid element particles only surrounded by full rigid elements as VISITED
    this->LabelEdgeNodes(mrModelPart);

    unsigned int dimension = 0;
    unsigned int number_of_vertices = 0;
    this->GetElementDimension(dimension, number_of_vertices);

    const int *OutElementList = mrRemesh.OutMesh.GetElementList();

    ModelPartUtilities::CheckNodalH(mrModelPart);

    // LevelSetUtilities LevelSet;
    // LevelSet.CreateLevelSetFunction(mrModelPart, 0.001);
    //std::vector<array_1d<double,3>> PointsLevelSet(OutNumberOfElements);

    const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();

    ModelPart::NodesContainerType::iterator nodes_begin = mrModelPart.NodesBegin();

    std::vector<GeometryType> VerticesList(OutNumberOfElements);
    std::vector<unsigned int> slivers(OutNumberOfElements,0);

    std::vector<bool> Preserved(OutNumberOfElements,true);
    int el = 0;
    //#pragma omp parallel for
    for (el = 0; el < OutNumberOfElements; ++el)
    {
      GeometryType Vertices;
      NodalFlags VerticesFlags;
      bool accepted = this->GetVertices(el, nodes_begin, OutElementList, VerticesFlags, Vertices, number_of_vertices);

      if (VerticesFlags.Fluid == 0)
        Preserved[el] = false;
      else if (VerticesFlags.Structure > 0){
        if (VerticesFlags.Structure == number_of_vertices){
          if (VerticesFlags.Solid == number_of_vertices)
            Preserved[el] = false;
          else
            Preserved[el] = this->CheckElementFSI_STR(Vertices, VerticesFlags, dimension, rCurrentProcessInfo, slivers[el]);
        }
        else
          Preserved[el] = this->CheckElementFSI(Vertices, VerticesFlags, dimension, rCurrentProcessInfo, slivers[el]);
      }
      else if (VerticesFlags.Fluid == number_of_vertices)
        Preserved[el] = this->CheckElementFULL(Vertices, VerticesFlags, dimension, rCurrentProcessInfo, slivers[el]);

      if (VerticesFlags.Isolated != 0) //can be repeated in other lists
        if(Preserved[el])
          Preserved[el] = this->CheckElementFSI_ISO(Vertices, VerticesFlags, dimension, rCurrentProcessInfo, slivers[el]);

      //PointsLevelSet[el] = Vertices.Center();
    }

    //LevelSet.PrintPointsXYQ(mrModelPart, PointsLevelSet, "LevelSet" );

    unsigned int number_of_slivers = std::accumulate(slivers.begin(), slivers.end(), 0);
    if (number_of_slivers>=0)
      this->MarkSliverNodes(slivers, number_of_vertices);

    unsigned int counter = 0;
    for (el = 0; el < Preserved.size(); ++el)
    {
      if (Preserved[el])
	mrRemesh.PreservedElements[el] =++counter;
    }
    mrRemesh.Info->Current.Elements = counter;

    if (mEchoLevel > 0)
      std::cout << "  [Preserved Elements " << mrRemesh.Info->Current.Elements << "]  Total out: " <<OutNumberOfElements<< " slivers "<< number_of_slivers << std::endl;

    this->SelectNodesToErase();

    //set flags for the local process execution marking slivers
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{SELECTED}}, {SELECTED.AsFalse()});

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void SelectElements() override
  {
    KRATOS_TRY

    //Get point Neighbour Conditions
    ModelPartUtilities::SetNodeNeighbourConditions(mrModelPart);

    const int &OutNumberOfElements = mrRemesh.OutMesh.GetNumberOfElements();

    if (OutNumberOfElements==0)
      KRATOS_ERROR << " 0 elements to select : NO ELEMENTS GENERATED " <<std::endl;

    //set flags for the local process execution marking slivers as SELECTED
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{SELECTED}}, {SELECTED.AsFalse()});

    //set full rigid element particles only surrounded by full rigid elements as VISITED
    this->LabelEdgeNodes(mrModelPart);

    unsigned int dimension = 0;
    unsigned int number_of_vertices = 0;
    this->GetElementDimension(dimension, number_of_vertices);

    const int *OutElementList = mrRemesh.OutMesh.GetElementList();

    ModelPartUtilities::CheckNodalH(mrModelPart);

    // LevelSetUtilities LevelSet;
    // LevelSet.CreateLevelSetFunction(mrModelPart, 0.001);
    //std::vector<array_1d<double,3>> PointsLevelSet(OutNumberOfElements);

    const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();

    ModelPart::NodesContainerType::iterator nodes_begin = mrModelPart.NodesBegin();

    //create element maps FULL:FuLL fluid
    std::map<const unsigned int, std::pair<GeometryType*,NodalFlags> > TypeA;  //FULL
    std::map<const unsigned int, std::pair<GeometryType*,NodalFlags> > TypeB;  //FULL FSI / FSI
    std::map<const unsigned int, std::pair<GeometryType*,NodalFlags> > TypeC;  //FULL FSI STR / FSI STR
    std::map<const unsigned int, std::pair<GeometryType*,NodalFlags> > TypeD;  //ISOLATED

    std::vector<GeometryType> VerticesList(OutNumberOfElements);

    std::vector<bool> Preserved(OutNumberOfElements,true);
    int el = 0;
    //#pragma omp parallel for
    for (el = 0; el < OutNumberOfElements; ++el)
    {
      NodalFlags VerticesFlags;
      bool accepted = this->GetVertices(el, nodes_begin, OutElementList, VerticesFlags, VerticesList[el], number_of_vertices);

      if (VerticesFlags.Fluid == 0)
        Preserved[el] = false;
      else if (VerticesFlags.Structure > 0){
        if (VerticesFlags.Structure == number_of_vertices){
          if (VerticesFlags.Solid == number_of_vertices)
            Preserved[el] = false;
          else
            TypeC[el] = {&VerticesList[el], VerticesFlags};
        }
        else
          TypeB[el] = {&VerticesList[el], VerticesFlags};
      }
      else if (VerticesFlags.Fluid == number_of_vertices)
        TypeA[el] = {&VerticesList[el], VerticesFlags};

      if (VerticesFlags.Isolated != 0) //can be repeated in other lists
        TypeD[el] = {&VerticesList[el], VerticesFlags};

      //PointsLevelSet[el] = Vertices.Center();
    }
    //LevelSet.PrintPointsXYQ(mrModelPart, PointsLevelSet, "LevelSet" );

    std::cout<<"   FULL: "<<TypeA.size()<<" FSI :"<<TypeB.size()<<" FSI_STR "<<TypeC.size()<<" FSI_ISO "<<TypeD.size()<<std::endl;

    std::vector<unsigned int> slivers(OutNumberOfElements,0);

    std::map<const unsigned int, std::pair<GeometryType*,NodalFlags> >::iterator i_elem;

    //#pragma omp parallel for shared(TypeA) private(i_elem) //FULL
    for(i_elem = TypeA.begin(); i_elem != TypeA.end();  ++i_elem)
    {
      const unsigned int &id = i_elem->first;
      GeometryType &Vertices = *i_elem->second.first;
      NodalFlags &VerticesFlags = i_elem->second.second;
      Preserved[id] = this->CheckElementFULL(Vertices, VerticesFlags, dimension, rCurrentProcessInfo, slivers[id]);
    }

    //#pragma omp parallel for shared(TypeB) private(i_elem) //FULL FSI / FSI
    for(i_elem = TypeB.begin(); i_elem != TypeB.end();  ++i_elem)
    {
      const unsigned int &id = i_elem->first;
      GeometryType &Vertices = *i_elem->second.first;
      NodalFlags &VerticesFlags = i_elem->second.second;
      Preserved[id] = this->CheckElementFSI(Vertices, VerticesFlags, dimension, rCurrentProcessInfo, slivers[id]);
    }

    //#pragma omp parallel for shared(TypeC) private(i_elem)//FULL FSI STR / FSI STR   (complex ones)
    for(i_elem = TypeC.begin(); i_elem != TypeC.end(); ++i_elem)
    {
      const unsigned int &id = i_elem->first;
      GeometryType &Vertices = *i_elem->second.first;
      NodalFlags &VerticesFlags = i_elem->second.second;
      Preserved[id] = this->CheckElementFSI_STR(Vertices, VerticesFlags, dimension, rCurrentProcessInfo, slivers[id]);
    }

    //#pragma omp parallel for shared(TypeD) private(i_elem)//ISOLATED
    for(i_elem = TypeD.begin(); i_elem != TypeD.end();  ++i_elem)
    {
      const unsigned int &id = i_elem->first;
      GeometryType &Vertices = *i_elem->second.first;
      NodalFlags &VerticesFlags = i_elem->second.second;
      if(Preserved[id]) //this is a second check
        Preserved[id] = this->CheckElementFSI_ISO(Vertices, VerticesFlags, dimension, rCurrentProcessInfo, slivers[id]);
    }

    unsigned int number_of_slivers = std::accumulate(slivers.begin(), slivers.end(), 0);
    if (number_of_slivers>=0)
      this->MarkSliverNodes(slivers, number_of_vertices);

    unsigned int counter = 0;
    for (el = 0; el < (int)Preserved.size(); ++el)
    {
      if (Preserved[el])
	mrRemesh.PreservedElements[el] =++counter;
    }
    mrRemesh.Info->Current.Elements = counter;

    if (mEchoLevel > 0)
      std::cout << "  [Preserved Elements " << mrRemesh.Info->Current.Elements << "]  Total out: " <<OutNumberOfElements<< " slivers "<< number_of_slivers << std::endl;

    this->SelectNodesToErase();

    //set flags for the local process execution marking slivers
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{SELECTED}}, {SELECTED.AsFalse()});

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckElementFULL(GeometryType &rVertices, const NodalFlags &rVerticesFlags, const unsigned int &rDimension, const ProcessInfo &rCurrentProcessInfo, unsigned int &number_of_slivers)
  {
    unsigned int number_of_vertices = rVertices.size();
    const double &rTimeStep = rCurrentProcessInfo[DELTA_TIME];

    //1.- Check elements with a free_surface face
    double Size  = this->GetReferenceSize(rVertices, rVerticesFlags, mrRemesh, rCurrentProcessInfo);

    if (rVerticesFlags.FreeSurface >= number_of_vertices-1)
      if (GeometryUtilities::CalculateMinEdgeSize(rVertices) < 0.5*Size) //check free_surface needles -> do not accept
        return false;

    //2.- Check Alpha Shape
    double Alpha = mrRemesh.AlphaParameter;

    //FULL FREE
    if (rVerticesFlags.NoWallFreeSurface == number_of_vertices)
    {
      Alpha = 0.8;

      if (GeometryUtilities::CompareEdgeSizes(rVertices)>2.0) //2D and 3D maximum/minimum edge
        Alpha = 0.6;

      // const double tolerance = 0.25; //relative side distance before and after approaching
      // if(KinematicUtilities::CheckFreeSurfaceApproachingEdges(rVertices,rTimeStep,tolerance) ){
      //   KRATOS_WARNING("")<<" Attention two free surfaces approaching at high speed "<<std::endl;
      //   Alpha = 1.0; //let the element to be created
      // }
    }
    else //FULL
    {
      Alpha = 1.6;
    }

    if (rDimension==3)
      Alpha *= 1.4;

    if(GeometryUtilities::AlphaShape(Alpha, rVertices, rDimension, 4.0 * Size))
      if(this->CheckElementShape(rVertices, rVerticesFlags, Size, rDimension, rTimeStep, number_of_slivers)) // to label slivers and do not touch them
        return true;

    return false;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckElementFSI(GeometryType &rVertices, const NodalFlags &rVerticesFlags, const unsigned int &rDimension, const ProcessInfo &rCurrentProcessInfo, unsigned int &number_of_slivers)
  {
    unsigned int number_of_vertices = rVertices.size();
    const double &rTimeStep = rCurrentProcessInfo[DELTA_TIME];

    double Size  = this->GetReferenceSize(rVertices, rVerticesFlags, mrRemesh, rCurrentProcessInfo);

    bool approaching = false;

    //1.- Check layer elements with walls
    if (rVerticesFlags.FreeSurface > 0 && rVerticesFlags.Interaction > 0)
      if (KinematicUtilities::CheckMeanPressure(rVertices)>1e6) // semiisolated element with large pressure errors -> do not accept
        return false;

    //2.- Check elements with a free_surface face
    if (rVerticesFlags.FreeSurface >= number_of_vertices-1)
      if (GeometryUtilities::CalculateMinEdgeSize(rVertices) < 0.5*Size) //check free_surface needles -> do not accept
        return false;

    //3.- Check Alpha Shape
    double Alpha = mrRemesh.AlphaParameter;

    //FULL FSI
    if (rVerticesFlags.Fluid == number_of_vertices)
    {
      Alpha = 1.4;

      // this is perfomed in CompareWallEdgeSizes:
      // if (rDimension == 2 && rVerticesFlags.Fluid == rVerticesFlags.FreeSurface)
      //   Alpha = 0.7;  //build on the previous surface
      // improve this part to release big edge elements with walls 3D and 2D
      // if (rVerticesFlags.FreeSurface == number_of_vertices-1)
      //   if (GeometryUtilities::CompareEdgeSizes(rVertices)>2.5)
      //     Alpha = 0.7;
    }
    else //FSI
    {
      if (KinematicUtilities::CheckMeshVelocity(rVertices)){ //ALE : check if mesh_velocity is higher than the velocity -> do not accept
        //std::cout<<" remove FSI element for MESH velocity "<<std::endl;
        return false;
      }
      //can be node-to-face (node in fluid or face in fluid) or edge-to-edge elements
      Alpha = 0.7;

      if (rDimension==3){
        if(this->CheckApproaching(rVertices,rVerticesFlags,rDimension,rTimeStep)){
          Alpha = 1.4;
          const double tolerance = 0.1; //relative side distance before and after approaching
          if(KinematicUtilities::CheckFreeSurfaceApproachingWall(rVertices,rTimeStep,tolerance))
            approaching = true;
        }
      }

    }

    if (rDimension==3)
      Alpha *= 1.4;

    double value = (4+rDimension)/double(rDimension);
    if (GeometryUtilities::CompareWallEdgeSizes(rVertices, Size)>value){ //2D and 3D maximum_wall_edge/maximum_non_wall_edge
      return false;
    }

    if(GeometryUtilities::AlphaShape(Alpha, rVertices, rDimension, 4.0 * Size))
      if(this->CheckElementShape(rVertices, rVerticesFlags, Size, rDimension, rTimeStep, number_of_slivers)){
        //last check for meniscus (refinement of the FSI)
        if (rDimension==3 && !approaching){

          if (CheckElementAtFreeSurface(rVertices, rVerticesFlags, 0.5*Size)) //check condition normal if element is outside
            if (!this->CheckElementFluidMeniscus(rVertices, rVerticesFlags, rCurrentProcessInfo[GRAVITY], Size, rDimension, rTimeStep)) //check 3d faces looking at the meniscus
              return false;
        }
        return true;
      }

    return false;
  }


  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckElementFSI_STR(GeometryType &rVertices, const NodalFlags &rVerticesFlags, const unsigned int &rDimension, const ProcessInfo &rCurrentProcessInfo, unsigned int &number_of_slivers)
  {
    unsigned int number_of_vertices = rVertices.size();
    const double &rTimeStep = rCurrentProcessInfo[DELTA_TIME];

    //FULL FSI STR
    if (rVerticesFlags.Fluid == number_of_vertices)
    {
      if (rVerticesFlags.Visited == number_of_vertices){ //release isolated elements in walls
        std::cout<<" remove FSI_STR full visited "<<rVerticesFlags.Visited<<std::endl;
        return false;
      }
      //3D
      if (rDimension == 3){
        if (rVerticesFlags.Visited > 2 && rVerticesFlags.FreeSurface > 0){
          std::cout<<" remove FSI_STR visited "<<rVerticesFlags.Visited<<std::endl;
          return false;
        }
      }
    }
    else //FSI STR
    {
      if (rVerticesFlags.Visited !=0 || rVerticesFlags.FreeSurface ==0)
        return false;

      if ( KinematicUtilities::CheckMeshVelocity(rVertices) ){ //ALE : check if mesh_velocity is higher than the velocity -> do not accept
        std::cout<<" remove FSI_STR for MESH Velocity "<<std::endl;
        return false;
      }

      //3D
      if (rDimension == 3){
        if (rVerticesFlags.Fluid < number_of_vertices){
          if (rVerticesFlags.Fluid < number_of_vertices - 1)
            return false;
          else if (rVerticesFlags.Interaction == 0)
            return false;
        }
      }
    }

    double Size  = this->GetReferenceSize(rVertices, rVerticesFlags, mrRemesh, rCurrentProcessInfo);

    if (rDimension == 2){
      if(!GeometryUtilities::CheckInnerCentre(rVertices))
        return false;
    }
    else{
      if (rVerticesFlags.FreeSurface >= 2){
        if (CheckElementAtFreeSurface(rVertices, rVerticesFlags, 0.5*Size)){ //check condition normal if element is outside
          //std::cout<<" remove FSI_STR at free_surface "<<std::endl;
          return false;
        }
      }
    }

    // Check Alpha Shape
    double Alpha = mrRemesh.AlphaParameter;

    Alpha *= 0.8; //reduction -> default too big

    if(GeometryUtilities::AlphaShape(Alpha, rVertices, rDimension, 4.0 * Size)){
      this->CheckElementShape(rVertices, rVerticesFlags, Size, rDimension, rTimeStep, number_of_slivers);   // to label slivers and do not touch them
      return true;
    }
    else
      return false;
  }


  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckElementFSI_ISO(GeometryType &rVertices, const NodalFlags &rVerticesFlags, const unsigned int &rDimension, const ProcessInfo &rCurrentProcessInfo, unsigned int &number_of_slivers)
  {
    unsigned int number_of_vertices = rVertices.size();
    const double &rTimeStep = rCurrentProcessInfo[DELTA_TIME];
    //FULL FSI
    if ((rVerticesFlags.Isolated + rVerticesFlags.FreeSurface) == number_of_vertices)
    {
      const double MaxRelativeVelocity = 10; //arbitrary value :: (try a criteria depending on time and size)
      if (KinematicUtilities::CheckRelativeApproach(rVertices, MaxRelativeVelocity, rTimeStep))
      {
        //set isolated nodes to erase if they approach too fast
        for (unsigned int i = 0; i < rVertices.size(); ++i)
        {
          if (rVertices[i].Is(ISOLATED))
          {
            //rVertices[i].Set(TO_ERASE); // commented to not erase ISOLATED nodes
            //to_preserve == false;
            std::cout<<" ISOLATED FLUID node ["<<rVertices[i].Id()<<"] fast approaching NODE NOT KEPT in select_elements_mesher_process.hpp"<<std::endl;
          }

        }
      }
    }

    //criterion for isolated nodes forming new wall-fluid elements
    if ( (rVerticesFlags.Isolated + rVerticesFlags.Structure) == number_of_vertices)
    {
      MesherUtilities MesherUtils;
      double VolumeIncrement = KinematicUtilities::GetDeformationGradientDeterminant(rVertices, rDimension, rTimeStep);
      if (VolumeIncrement > 1.0)
        return false;
    }

    return true;
  }


  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckElementShape(GeometryType &rVertices, const NodalFlags &rVerticesFlags,  const double &rSize, const unsigned int &rDimension, const double &rTimeStep, unsigned int &number_of_slivers) override
  {
    bool accepted = true;
    unsigned int number_of_vertices = rVertices.size();

    if (rDimension == 2 && number_of_vertices == 3){
      accepted = CheckTriangleShape(rVertices, rVerticesFlags, rSize, rDimension, rTimeStep, number_of_slivers);
    }
    else if (rDimension == 3 && number_of_vertices == 4){
      accepted = CheckTetrahedronShape(rVertices, rVerticesFlags, rSize, rDimension, rTimeStep, number_of_slivers);
    }
    return accepted;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckTriangleShape(GeometryType &rVertices, const NodalFlags &rVerticesFlags,  const double &rSize, const unsigned int &rDimension, const double &rTimeStep, unsigned int &number_of_slivers)
  {
    bool accepted = true;
    unsigned int number_of_vertices = rVertices.size();

    //check 2D distorted elements with edges decreasing very fast
    if (rVerticesFlags.NoWallFreeSurface > 1 && rVerticesFlags.Structure == 0)
    {
      double MaxEdgeLength = std::numeric_limits<double>::min();
      double MinEdgeLength = std::numeric_limits<double>::max();

      for (unsigned int i = 0; i < number_of_vertices - 1; ++i)
      {
        for (unsigned int j = i + 1; j < number_of_vertices; ++j)
        {
          double Length = norm_2(rVertices[j].Coordinates() - rVertices[i].Coordinates());
          if (Length < MinEdgeLength)
            MinEdgeLength = Length;

          if (Length > MaxEdgeLength)
            MaxEdgeLength = Length;
        }
      }

      MesherUtilities MesherUtils;
      const double MaxRelativeVelocity = 1.5; //arbitrary value :: (try a criteria depending on time and size)
      // bool sliver = false;
      if (MinEdgeLength * 5 < MaxEdgeLength)
      {
        if (rVerticesFlags.FreeSurface == number_of_vertices)
        {
          std::cout << " WARNING 2D sliver on FreeSurface " << std::endl;
          accepted = false;
          ++number_of_slivers;
        }
        else if (KinematicUtilities::CheckRelativeVelocities(rVertices, MaxRelativeVelocity))
        {
          std::cout << " WARNING 2D sliver inside domain " << std::endl;
          // sliver = true; //commented to no erase but labeled to not calculate them
          ++number_of_slivers;
        }
      }

      // if (sliver){
      //   for (unsigned int i = 0; i < number_of_vertices; ++i)
      //   {
      //     if (rVertices[i].Is(SELECTED))
      //       std::cout << " WARNING Second sliver in the same node bis " << std::endl;
      //     rVertices[i].Set(SELECTED);
      //   }
      // }
    }

    return accepted;

  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckTetrahedronShape(GeometryType &rVertices, const NodalFlags &rVerticesFlags,  const double &rSize, const unsigned int &rDimension, const double &rTimeStep, unsigned int &number_of_slivers)
  {
    bool accepted = true;
    unsigned int number_of_vertices = rVertices.size();

    if (rVerticesFlags.FreeSurface >= 2 || (rVerticesFlags.Boundary == 4 && rVerticesFlags.NoWallFreeSurface != 4))
    {

      Tetrahedra3D4<NodeType> Tetrahedron(rVertices);
      double Volume = Tetrahedron.Volume();

      if (Volume<0){
        std::cout<<" Negative Volume Element Generated "<<std::endl;
        return false;
      }

      if (Volume < 0.01 * pow(4.0 * rSize , rDimension))
      {
        if (mEchoLevel > 0)
          std::cout<<" SLIVER Volume="<<Volume<<" VS Critical Volume="<< 0.01*pow(4.0*rSize ,rDimension) <<std::endl;

        //only erase full free surface sliver elements (visualization purposes)
        if (rVerticesFlags.Fluid != number_of_vertices)
        {
          accepted = false;
          std::cout<<" Release partially_fluid SLIVER "<<std::endl;
        }
        else if (rVerticesFlags.FreeSurface == number_of_vertices)
        {
          accepted = false;
          std::cout<<" Release free_surface SLIVER "<<std::endl;
        }
        else if ((rVerticesFlags.FreeSurface == number_of_vertices - 1) && rVerticesFlags.Interaction > 1)
        {
          accepted = false;
          std::cout<<" Release interaction SLIVER "<<std::endl;
        }

        ++number_of_slivers;

        //other elements labeled to not calculate them
        if (mEchoLevel > 0)
          if (accepted)
            std::cout<<" Label fluid SLIVER (free_surface: "<<rVerticesFlags.FreeSurface<<" fluid "<<rVerticesFlags.Fluid<<" structure: "<<rVerticesFlags.Structure<<" interaction: "<<rVerticesFlags.Interaction<<")"<<std::endl;

      }

    }
    else if ((rVerticesFlags.Structure == 1 || rVerticesFlags.Structure == 2) && rVerticesFlags.Inside == 0)
    {
      MesherUtilities MesherUtils;

      double VolumeIncrement = KinematicUtilities::GetDeformationGradientDeterminant(rVertices, rDimension, rTimeStep);
      if (mEchoLevel > 0)
        if (VolumeIncrement < 0.0)
          std::cout << "  NEGATIVE Volume Increment (detF: " << VolumeIncrement << ")" << std::endl;

      double MovedVolume = KinematicUtilities::GetMovedVolume(rVertices, rDimension, rTimeStep, 1.0);

      //check if the element is going to invert
      if (VolumeIncrement < 0.0 || MovedVolume < 0.0)
      {
        if (mEchoLevel>0)
          std::cout<<" SLIVER FOR INVERSION "<<VolumeIncrement<<" VS Critical Volume="<< 0.01*pow(4.0*rSize ,rDimension) <<"( rigid: "<<rVerticesFlags.Rigid<<" ) moved volume "<<MovedVolume<<std::endl;

        ++number_of_slivers;
      }
    }

    return accepted;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckElementAtFreeSurface(GeometryType &rVertices, const NodalFlags &rVerticesFlags, const double Size)
  {
    unsigned int dimension = 0;
    unsigned int number_of_vertices = 0;
    this->GetElementDimension(dimension, number_of_vertices);

    if (dimension == 3 && rVerticesFlags.FreeSurface > 0) //always true from FSI and FSI STR
    {
      Tetrahedra3D4<NodeType> Tetrahedron(rVertices);
      GeometryType &eGeometry = Tetrahedron;

      //get matrix nodes in faces
      DenseMatrix<unsigned int> lpofa; //points that define the faces
      eGeometry.NodesInFaces(lpofa);

      std::vector<unsigned int> fluid_face_candidates;
      std::vector<unsigned int> fluid_edge_candidates;
      std::vector<unsigned int> fluid_point_candidates;
      for (unsigned int iface = 0; iface < eGeometry.size(); ++iface)
      {
        //check if free surface
        unsigned int free_surface = 0;
        for (unsigned int i = 1; i <= 3; ++i)
          if (eGeometry[lpofa(i, iface)].Is(FREE_SURFACE))
            ++free_surface;

        if(free_surface==3)
          fluid_face_candidates.push_back(iface);
        else if (free_surface==2)
          fluid_edge_candidates.push_back(iface);
        else if (free_surface==1)
          fluid_point_candidates.push_back(iface);
      }

      // std::cout<<" Check Element at Free Surface face: ["<<rVerticesFlags.Structure<<"] "<<fluid_face_candidates.size()<<" edge: "<<fluid_edge_candidates.size()<<" point: "<<fluid_point_candidates.size()<<" structure nodes "<<structure_nodes<<std::endl;

      //face conditions
      for (unsigned int i = 0; i < fluid_face_candidates.size(); ++i)
      {
        unsigned int &iface = fluid_face_candidates[i];
        std::vector<unsigned int> element_face;
        for (unsigned int j = 3; j >= 1; --j)
          element_face.push_back(eGeometry[lpofa(j, iface)].Id());

        ConditionWeakPtrVectorType &nConditions = eGeometry[lpofa(1, iface)].GetValue(NEIGHBOUR_CONDITIONS);

        for (auto &i_cond : nConditions)
        {
          GeometryType& cGeometry = i_cond.GetGeometry();
          bool found = SearchUtilities::FindCondition(cGeometry, element_face);
          if (found)
          {
            double projection = inner_prod(eGeometry.Center()-cGeometry.Center(), i_cond.GetValue(NORMAL));
            if (projection > 0) //is outside
            {
              //std::cout<<" projection face "<<projection<<" "<<Size<<std::endl;
              if(projection > Size)
                return true;
              // else
              //   return false;
            }
          }
        }
      }

      //edge conditions
      for (unsigned int i = 0; i < fluid_edge_candidates.size(); ++i)
      {
        unsigned int &iface = fluid_edge_candidates[i];

        for (unsigned int j = 1; j <= 3; ++j)
        {
          if(eGeometry[lpofa(j, iface)].Is(FREE_SURFACE)){
            ConditionWeakPtrVectorType &nConditions = eGeometry[lpofa(j, iface)].GetValue(NEIGHBOUR_CONDITIONS);

            for (auto &i_cond : nConditions)
            {
              GeometryType& cGeometry = i_cond.GetGeometry();
              unsigned int structure = 0;
              unsigned int freesurface = 0;
              for (auto &i_node : cGeometry)
              {
                if(i_node.Is(STRUCTURE))
                  ++structure;
                if(i_node.Is(FREE_SURFACE))
                  ++freesurface;
              }

              if ( freesurface == 3 && structure == 2 ) //structure can be 1 also
              {
                double projection = inner_prod(eGeometry.Center()-cGeometry.Center(), i_cond.GetValue(NORMAL));
                if (projection > 0) //is outside
                {
                  if(projection > Size)
                    return true;
                  // else
                  //   return false;
                }
              }
            }
          }
        }
      }

      //point conditions
      for (unsigned int i = 0; i < fluid_point_candidates.size(); ++i)
      {
        unsigned int &iface = fluid_point_candidates[i];

        for (unsigned int j = 1; j <= 3; ++j)
        {
          if(eGeometry[lpofa(j, iface)].Is(FREE_SURFACE)){

            ConditionWeakPtrVectorType &nConditions = eGeometry[lpofa(j, iface)].GetValue(NEIGHBOUR_CONDITIONS);
            for (auto &i_cond : nConditions)
            {
              GeometryType& cGeometry = i_cond.GetGeometry();

              unsigned int structure = 0;
              unsigned int freesurface = 0;
              for (auto &i_node : cGeometry)
              {
                if(i_node.Is(STRUCTURE))
                  ++structure;
                if(i_node.Is(FREE_SURFACE))
                  ++freesurface;
              }

              if ( freesurface == 3 && structure == 2 ) //structure can be 1 also
              {
                double projection = inner_prod(eGeometry.Center()-cGeometry.Center(), i_cond.GetValue(NORMAL));
                if (projection > 0) //is outside
                {
                  if(projection > Size)
                    return true;
                  // else
                  //   return false;
                }
              }
            }
          }
        }

      }
    }

    return false;
  }


  //*******************************************************************************************
  //*******************************************************************************************

  //it can set TO_ERASE to new entities inserted in full rigid elements
  bool CheckElementFluidMeniscus(GeometryType &rVertices, const NodalFlags &rVerticesFlags, const array_1d<double,3> &rGravity, const double &rSize, const unsigned int &rDimension, const double &rTimeStep)
  {
    unsigned int dimension = 0;
    unsigned int number_of_vertices = 0;
    double slope = 0.4;

    this->GetElementDimension(dimension, number_of_vertices);

    if (dimension == 3)
    {
      bool check = false;
      if (rVerticesFlags.NoWallFreeSurface > 0){
        if (rVerticesFlags.FreeSurface == rVerticesFlags.Fluid){
          check = true;
        }
        else if (rVerticesFlags.Fluid == number_of_vertices && rVerticesFlags.FreeSurface == number_of_vertices-1 && rVerticesFlags.FreeSurface == rVerticesFlags.Structure){
          check = true;
        }

      }

      if (rVerticesFlags.Fluid == number_of_vertices-1 && rVerticesFlags.Structure>=2 && rVerticesFlags.FreeSurface==2){ //case that the free surface element has disappeared and the menistus is formed with an interior node
        check = true;
      }

      // Check all elements with a free surface having wall and non-wall nodes
      if (check)
      {
        if (GeometryUtilities::CheckFluidMeniscus(rVertices, rGravity, slope))
        {
          //do not accept full FULL FSI elements with a new inserted node
          if ((rVerticesFlags.Structure + rVerticesFlags.NewEntity) == number_of_vertices && rVerticesFlags.Fluid == number_of_vertices && rVerticesFlags.Visited >= number_of_vertices - 1)
          {
              std::cout << " Fluid Meniscus EDGE (old_entity: " << rVerticesFlags.Visited << " fluid: " << rVerticesFlags.Fluid << " rigid: " << rVerticesFlags.Rigid << " free_surface: " << rVerticesFlags.FreeSurface << ")" << std::endl;
              return false;
          }

          //do not accept rigid wall elements with fluid node separating from walls
          if (rVerticesFlags.Structure == number_of_vertices-1 && rVerticesFlags.NoWallFreeSurface > 0)
          {
            if (KinematicUtilities::CheckFreeSurfaceApproachingWall(rVertices)){
              double VolumeIncrement = KinematicUtilities::GetDeformationGradientDeterminant(rVertices, rDimension, rTimeStep);
              if (VolumeIncrement > 1.1){
                std::cout<<" Fluid MENISCUS separating "<<VolumeIncrement<<std::endl;
                return false;
              }
            }
          }

        }
        else{
          return false;
        }
      }
    }
    return true;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  // Check approaching node
  bool CheckApproaching(GeometryType &rVertices, const NodalFlags &rVerticesFlags, const unsigned int &rDimension, const double &rTimeStep)
  {
    bool approaching = false;
    //do not accept rigid wall elements with fluid node separating from walls
    if ( (rVerticesFlags.NoWallFreeSurface > 0 && rVerticesFlags.Interaction > 0 && rVerticesFlags.Fluid > 1) || rVerticesFlags.Isolated > 0)
    {
      approaching = KinematicUtilities::CheckFreeSurfaceApproachingWall(rVertices);

      if (approaching){
        double VolumeIncrement = KinematicUtilities::GetDeformationGradientDeterminant(rVertices, rDimension, rTimeStep);
        if (VolumeIncrement > 1.1)
          return false;
      }
    }
    return approaching;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  // It sets full previous rigid element nodes with the flag VISITED
  void LabelEdgeNodes(ModelPart &rModelPart)
  {
    //set flags for the local process execution
    SetFlagsUtilities::SetFlagsToElements(mrModelPart, {{VISITED}}, {VISITED.AsFalse()});
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{VISITED}}, {VISITED.AsFalse()});

    //set label to full rigid element nodes
    for (auto &i_elem : rModelPart.Elements())
    {
      GeometryType &rGeometry = i_elem.GetGeometry();

      unsigned int count_rigid = 0;
      // unsigned int count_free_surface = 0;
      for (auto &i_node : rGeometry)
      {
        if (i_node.Is(RIGID))
        {
          count_rigid++;
        }
        // else if (i_node.Is(FREE_SURFACE))
        // {
        //   count_free_surface++;
        // }
      }

      if (count_rigid == rGeometry.size())
      {
        i_elem.Set(VISITED, true);
        for (auto &i_node : rGeometry)
        {
          i_node.Set(VISITED, true);
        }
      }
    }

    //unset label for nodes not fully rigid
    for (auto &i_elem : rModelPart.Elements())
    {
      GeometryType &rGeometry = i_elem.GetGeometry();

      //to release full rigid elements with no fluid surrounding them
      if (i_elem.IsNot(VISITED))
      {
        for (auto &i_node : rGeometry)
        {
          i_node.Set(VISITED, false);
        }
      }
      else{
        i_elem.Set(VISITED, false);
      }

    }

  }


  //*******************************************************************************************
  //*******************************************************************************************

  void SelectNodesToErase() override
  {
    // Set disconnected nodes to erase
    unsigned int dimension = 0;
    unsigned int number_of_vertices = 0;
    unsigned int isolated_nodes = 0;

    this->GetElementDimension(dimension, number_of_vertices);

    int *OutElementList = mrRemesh.OutMesh.GetElementList();

    ModelPart::NodesContainerType &rNodes = mrModelPart.Nodes();

    const int &OutNumberOfElements = mrRemesh.OutMesh.GetNumberOfElements();

    //set flags for the local process execution
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{BLOCKED}}, {BLOCKED.AsFalse()});
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{MARKER}}, {MARKER.AsFalse()});

    //check engaged nodes
    for (int el = 0; el < OutNumberOfElements; ++el)
    {
      if (mrRemesh.PreservedElements[el])
      {
        for (unsigned int pn = 0; pn < number_of_vertices; ++pn)
        {
          //set vertices
          rNodes[OutElementList[el * number_of_vertices + pn]].Set(BLOCKED);
        }
      }
    }

    int count_release = 0;
    for (ModelPart::NodesContainerType::iterator i_node = rNodes.begin(); i_node != rNodes.end(); ++i_node)
    {
      if (i_node->IsNot(BLOCKED))
      {

        if (i_node->Is(TO_ERASE) && i_node->Or({RIGID,SOLID,INLET}))
        {
          i_node->Set(TO_ERASE, false);
          std::cout << " WARNING TRYING TO DELETE A WALL NODE (fluid): " << i_node->Id() << std::endl;
        }
        else if (i_node->AndNot({RIGID,SOLID,INLET}))
        {
          if (mrRemesh.ExecutionOptions.Is(MesherData::KEEP_ISOLATED_NODES))
          {
            if (i_node->IsNot(TO_ERASE))
            {
              // if (i_node->Is(FREE_SURFACE))
              // {
                i_node->Set(ISOLATED, true);
                ++isolated_nodes;
                //std::cout<<" ISOLATED FREE SURFACE FLUID "<<std::endl;
              // }
              // else
              // {
              //   // in fact can be a new free surface node -> check if it happens
              //   i_node->Set(TO_ERASE, true);
              //   std::cout<<"  ISOLATED inside FLUID node TO ERASE in select_elements_mesher_process.hpp "<<i_node->IsNot(BLOCKED)<<std::endl;
              // }
            }
          }
          else
          {
            std::cout<<" ISOLATED FLUID NODES NOT KEPT in select_elements_mesher_process.hpp"<<std::endl;
            i_node->Set(TO_ERASE, true);
            if (mEchoLevel > 0)
            {
              if (i_node->Is(BOUNDARY))
                std::cout << " NODE " << i_node->Id() << " IS BOUNDARY RELEASE " << std::endl;
              else
                std::cout << " NODE " << i_node->Id() << " IS DOMAIN RELEASE " << std::endl;
            }
            ++count_release;
          }
        }

      }
      else  // BLOCKED NODE
      {
        i_node->Set(TO_ERASE, false);
        i_node->Set(ISOLATED, false);
      }

      i_node->Set(BLOCKED, false);

      i_node->Set(VISITED, false);

      if (i_node->Is(SELECTED)) // Belongs to a SLIVER
        i_node->Set(MARKER, true);
      else
        i_node->Set(MARKER, false);

      i_node->Set(SELECTED, false);
    }

    if (mEchoLevel > 0)
    {
      std::cout << "   NUMBER OF RELEASED NODES " << count_release << std::endl;
      std::cout << "   NUMBER OF ISOLATED NODES " << isolated_nodes << std::endl;
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void MarkSliverNodes(const std::vector<unsigned int> &slivers, unsigned int &number_of_vertices)
  {
    ModelPart::NodesContainerType::iterator nodes_begin = mrModelPart.NodesBegin();
    const int *OutElementList = mrRemesh.OutMesh.GetElementList();

    int el = 0;

#pragma omp parallel for
    for (el = 0; el < (int)slivers.size(); ++el)
    {
      if (slivers[el]>0){
	GeometryType Vertices;
	NodalFlags VerticesFlags;
	this->GetVertices(el, nodes_begin, OutElementList, VerticesFlags, Vertices, number_of_vertices);
	for (auto &i_node : Vertices)
	  i_node.Set(MARKER);
      }

    }
  }

  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Private Static Member Variables
  ///@{
  ///@}
  ///@name Private Static Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{
  ///@}

}; // Class Process

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                SelectFluidElementsMesherProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const SelectFluidElementsMesherProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_SELECT_FLUID_ELEMENTS_MESHER_PROCESS_HPP_INCLUDED defined
