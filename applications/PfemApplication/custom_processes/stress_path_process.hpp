//
//   Project Name:        KratosPfemSolidMechanicsApplication $
//   Created by:          $Author:                  LMonforte $
//   Last modified by:    $Co-Author:                         $
//   Date:                $Date:                    July 2015 $
//   Revision:            $Revision:                      0.0 $
//
//

#if !defined ( KRATOS_STRESS_PATH_PROCESS_H_INCLUDED )
#define        KRATOS_STRESS_PATH_PROCESS_H_INCLUDED


/* System includes */


/* External includes */


/* Project includes */
#include "processes/process.h"
#include "includes/model_part.h"
#include "includes/kratos_flags.h"
#include "utilities/math_utils.h"
#include "pfem_solid_mechanics_application_variables.h"

#include "includes/kratos_parameters.h"


namespace Kratos
{

   class KRATOS_API(PFEM_SOLID_MECHANICS_APPLICATION) StressPathProcess
      : public Process 
   {
      public:


         /**@name Type Definitions */
         /*@{ */

         // Pointer definition of Process
         KRATOS_CLASS_POINTER_DEFINITION( StressPathProcess );


         typedef ModelPart::NodesContainerType               NodesArrayType;
         typedef ModelPart::ConditionsContainerType ConditionsContainerType;
         typedef ModelPart::MeshType                               MeshType;
         /*@} */
         /**@name Life Cycle
          */
         /*@{ */

         // Constructor.

         //StressPathProcess(ModelPart& rModelPart);

         //StressPathProcess(ModelPart& rModelPart, const bool rGravity, const double rSV = 0.0, const double rSH = 0.0, const double rWaterPressure = 0.0, const bool rYmaxBool = false, const double rYmax = 0.0, const double rWaterLoad = 0.0);

         StressPathProcess( ModelPart& rModelPart, Parameters rParameters);


         /** Destructor.
          */

         virtual ~StressPathProcess();

         /*@} */
         /**@name Operators
          */
         /*@{ */


         /*@} */
         /**@name Operations */
         /*@{ */



         void operator()()
         {
            Execute();
         }

         void Execute() override;

      protected:

         Element::Pointer SearchTheElement( const double & rX, const double & rY);

         void WriteStressPathToFile( Element::Pointer & rpElement, std::string & rLine);

         void AppendVectorToLine( std::string & rLine, const Vector & rVector);

         void GetAndAppendScalarVariableToLine( const std::string & rVariable, Element::Pointer & prElement, std::string & rLine);

      private:

         // member variables

         ModelPart & mrModelPart;
         std::vector<double> mXPVector;
         std::vector<double> mYPVector;
         bool mDeformed;
         std::string mFileName;


   }; //end class StressPathProcess

   }  // END namespace Kratos

#endif //KRATOS_STRESS_PATH_PROCESS_H_INCLUDED
