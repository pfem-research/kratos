//
//   Project Name:        KratosPfemApplication    $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:         July 2018 $
//
//

#if !defined(KRATOS_MANAGE_MARKER_ELEMENTS_PROCESS_HPP_INCLUDED)
#define KRATOS_MANAGE_MARKER_ELEMENTS_PROCESS_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_bounding/spatial_bounding_box.hpp"

#include "custom_utilities/model_part_utilities.hpp"

#include "processes/process.h"

namespace Kratos
{

///@name Kratos Classes
///@{

/// Process for managing the time integration of variables for selected elements
/**
   Selected elements are SLIVERS and not COMPUTED ELEMENTS++
   Setting geometrical SLIVERS to NOT ACTIVE before computation
   Integrating variables for Not Computed elements and nodes
 */
class ManageSelectedElementsProcess : public Process
{
public:
  ///@name Type Definitions
  ///@{

  typedef ModelPart::ElementType::GeometryType GeometryType;

  /// Pointer definition of ManageSelectedElementsProcess
  KRATOS_CLASS_POINTER_DEFINITION(ManageSelectedElementsProcess);

  ///@}
  ///@name Life Cycle
  ///@{

  ManageSelectedElementsProcess(ModelPart &rModelPart) : Process(Flags()), mrModelPart(rModelPart)
  {
  }

  /// Destructor.
  virtual ~ManageSelectedElementsProcess() {}

  ///@}
  ///@name Operators
  ///@{

  /// This operator is provided to call the process as a function and simply calls the Execute method.
  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  /// Execute method is used to execute the ManageSelectedElementsProcess algorithms.
  void Execute() override
  {
  }

  /// this function is designed for being called at the beginning of the computations
  /// right after reading the model and the groups
  void ExecuteInitialize() override
  {
  }

  /// this function is designed for being execute once before the solution loop but after all of the
  /// solvers where built
  void ExecuteBeforeSolutionLoop() override
  {
    KRATOS_TRY

    double Radius = 0.0;
    //BOUNDARY flag must be set in model part nodes
    mBoundingBox = SpatialBoundingBox(mrModelPart, Radius, 0.1);

    KRATOS_CATCH("")
  }

  /// this function will be executed at every time step BEFORE performing the solve phase
  void ExecuteInitializeSolutionStep() override
  {
    KRATOS_TRY

    const int nelements = mrModelPart.NumberOfElements();
    //const unsigned int dimension = mrModelPart.GetProcessInfo()[SPACE_DIMENSION];
    if (nelements != 0)
    {
      ModelPart::ElementsContainerType::iterator it_begin = mrModelPart.ElementsBegin();

#pragma omp parallel for
      for (int i = 0; i < nelements; ++i)
      {
        ModelPart::ElementsContainerType::iterator i_elem = it_begin + i;

        if (i_elem->Is(FLUID))
        {

          GeometryType &eGeometry = i_elem->GetGeometry();
          const unsigned int number_of_nodes = eGeometry.size();
          unsigned int selected_nodes = 0;
          unsigned int free_surface_nodes = 0;
          unsigned int rigid_nodes = 0;
          unsigned int slip_nodes = 0;
          unsigned int non_rigid_free_surface_nodes = 0;
          for (auto &i_node : eGeometry)
          {
            if (i_node.Is(MARKER)) // Belongs to a SLIVER
              ++selected_nodes;
            if (i_node.Is(RIGID))
              ++rigid_nodes;
            if (i_node.Is(FREE_SURFACE))
              ++free_surface_nodes;
            if (i_node.Is(SLIP))
              ++slip_nodes;
            if (i_node.IsNot(RIGID) && i_node.Is(FREE_SURFACE))
              ++non_rigid_free_surface_nodes;
          }

          ElementWeakPtrVectorType &nElements = i_elem->GetValue(NEIGHBOUR_ELEMENTS);
          unsigned int number_of_faces = nElements.size();
          unsigned int free_faces = 0;
          for (auto &i_nelem : nElements) //loop over elements surronding a point
          {
            if(i_elem->Id() == i_nelem.Id())
              ++free_faces;
          }

          bool unstable = false;
          if (free_faces == number_of_faces-1)
          {
            //std::cout<<" Free face element "<<i_elem->Id()<<std::endl;
            if (non_rigid_free_surface_nodes == number_of_nodes)
              unstable = true;
            else if (free_surface_nodes == number_of_nodes && rigid_nodes > 0)
              unstable = true;
            //std::cout<<" unstable "<<unstable<<std::endl;
          }

          bool selected = false;
          if (selected_nodes == number_of_nodes && rigid_nodes != number_of_nodes)
          {
            selected = true;
          }

          if (selected || unstable)
          {
            i_elem->Set(MARKER, true);
            i_elem->Set(ACTIVE, false);
            //std::cout<<" Unstable FULL free surface element disabled "<<i_elem->Id()<<std::endl;
          }
          else
          { //add full rigid elements as selected (to not compute)
            if (rigid_nodes == number_of_nodes && slip_nodes == 0)
            {
              // i_elem->Set(MARKER, true);
              // i_elem->Set(ACTIVE, false); //do not disable rigid elements: ALE problems singular in coarse meshes
              // std::cout<<" FULL rigid element disabled "<<std::endl;
            }
            if (rigid_nodes > 0 && non_rigid_free_surface_nodes > 0 && (non_rigid_free_surface_nodes + rigid_nodes) == number_of_nodes)
            {

              bool visited = true;
              for (auto &i_node : eGeometry)
              {
                if (i_node.IsNot(RIGID))
                {
                  if (i_node.Is(FREE_SURFACE))
                  {
                    NodeWeakPtrVectorType &nNodes = i_node.GetValue(NEIGHBOUR_NODES);
                    for (auto &i_nnode : nNodes)
                    {
                      if (i_nnode.AndNot({RIGID,FREE_SURFACE}))
                      {
                        visited = false;
                        break;
                      }
                    }
                  }
                  else
                  {
                    visited = false;
                  }
                }
                if (!visited)
                  break;
              }
              if (visited) // && slip_nodes == 0)
              {
                i_elem->Set(MARKER, true);
                i_elem->Set(ACTIVE, false); //do not disable rigid element-freesurface patches: ALE problems singular matrix / block_builder required
                //std::cout<<" ISOLATED PATCH of FLUID WALL elements disabled "<<std::endl;
              }
              //}
            }
          }
        }
      }
    }

    KRATOS_CATCH("")
  }

  /// this function will be executed at every time step AFTER performing the solve phase
  void ExecuteFinalizeSolutionStep() override
  {
    KRATOS_TRY

    const int nelements = mrModelPart.NumberOfElements();

    // Set MARKER nodes from MARKER elements
    if (nelements != 0)
    {
      ModelPart::ElementsContainerType::iterator it_begin = mrModelPart.ElementsBegin();

      //(set on nodes, not in parallel)
      for (int i = 0; i < nelements; ++i)
      {
        ModelPart::ElementsContainerType::iterator i_elem = it_begin + i;

        if (i_elem->And({FLUID,MARKER}))
        {
          GeometryType &eGeometry = i_elem->GetGeometry();
          for (auto &i_node : eGeometry)
          {
            i_node.Set(MARKER, true);
          }
          i_elem->Set(MARKER, false);
          i_elem->Set(ACTIVE, true);
        }
      }
    }

    const int nnodes = mrModelPart.NumberOfNodes();

    // Decide modification of Kinematic variables or TO_ERASE nodes if MARKER
    if (nnodes != 0)
    {
      ModelPart::NodesContainerType::iterator it_begin = mrModelPart.NodesBegin();

      bool mesh_movement = false;
      if (it_begin->SolutionStepsDataHas(MESH_DISPLACEMENT))
      {
        mesh_movement = true;
      }

#pragma omp parallel for
      for (int i = 0; i < nnodes; ++i)
      {
        ModelPart::NodesContainerType::iterator i_node = it_begin + i;

        if (i_node->Is(MARKER))
        {

          NodeWeakPtrVectorType &nNodes = i_node->GetValue(NEIGHBOUR_NODES);
          unsigned int selected = false;
          for (auto &i_nnode : nNodes)
          {
            if (i_nnode.Or({FREE_SURFACE,MARKER}))
              ++selected;
          }
          if (i_node->Is(FREE_SURFACE) && i_node->IsNot(RIGID) && selected == nNodes.size())
          {
            i_node->Set(TO_ERASE, true);
          }
          else
          {
            if (i_node->Is(FREE_SURFACE))
            {
              i_node->FastGetSolutionStepValue(PRESSURE) = 0.0;

              if (i_node->IsNot(RIGID)) //check if free surface nodes are outside the bounding box
              {
                if (!mBoundingBox.IsInside(*i_node))
                {
                  i_node->Set(TO_ERASE, true);
                  std::cout << " MARKER element outside Node  TO_ERASE ["<<i_node->Id()<<"] to erase in manage_selected elements_proces.hpp" << std::endl;
                }
              }
            }

            if (i_node->AndNot({RIGID,TO_ERASE}))
            {
              if (this->CheckMovementIncrement(*i_node))
              {
                //std::cout<<" MARKER Node Pre ["<<i_node->Id()<<"] "<<i_node->Coordinates()<<" DISPLACEMENT "<<i_node->FastGetSolutionStepValue(DISPLACEMENT)<<" VELOCITY "<<i_node->FastGetSolutionStepValue(VELOCITY)<<" ACCELERATION "<<i_node->FastGetSolutionStepValue(ACCELERATION)<<" free surface "<<i_node->Is(FREE_SURFACE)<<" norm vel "<<norm_velocity<<" norm accel "<<norm_acceleration<<std::endl;

                noalias(i_node->FastGetSolutionStepValue(ACCELERATION)) = i_node->FastGetSolutionStepValue(ACCELERATION, 1);
                noalias(i_node->FastGetSolutionStepValue(VELOCITY)) = i_node->FastGetSolutionStepValue(VELOCITY, 1);
                noalias(i_node->FastGetSolutionStepValue(DISPLACEMENT)) = i_node->FastGetSolutionStepValue(DISPLACEMENT, 1);
                noalias(i_node->Coordinates()) = i_node->GetInitialPosition() + i_node->FastGetSolutionStepValue(DISPLACEMENT);


                if (mesh_movement && i_node->IsNot(INSIDE)) //Lagrangian node
                {
                  noalias(i_node->FastGetSolutionStepValue(MESH_VELOCITY)) = i_node->FastGetSolutionStepValue(MESH_VELOCITY, 1);
                  noalias(i_node->FastGetSolutionStepValue(MESH_ACCELERATION)) = i_node->FastGetSolutionStepValue(MESH_ACCELERATION, 1);
                }

                //std::cout<<" MARKER Node Post ["<<i_node->Id()<<"] "<<i_node->Coordinates()<<" DISPLACEMENT "<<i_node->FastGetSolutionStepValue(DISPLACEMENT)<<" VELOCITY "<<i_node->FastGetSolutionStepValue(VELOCITY)<<" ACCELERATION "<<i_node->FastGetSolutionStepValue(ACCELERATION)<<std::endl;
              }
            }
          }
        }
      }
    }

    //set flags to allow printing elements
    SetFlagsUtilities::SetFlagsToElements(mrModelPart, {{ACTIVE.AsFalse()}}, {MARKER, ACTIVE});

    KRATOS_CATCH("")
  }

  /// this function will be executed at every time step BEFORE  writing the output
  void ExecuteBeforeOutputStep() override
  {
  }

  /// this function will be executed at every time step AFTER writing the output
  void ExecuteAfterOutputStep() override
  {
    //set flags for the local process execution
    //SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{MARKER}}, {MARKER.AsFalse()}); // to unset and set in the new generated mesh
    SetFlagsUtilities::SetFlagsToElements(mrModelPart, {{MARKER}}, {MARKER.AsFalse()});
  }

  /// this function is designed for being called at the end of the computations
  /// right after reading the model and the groups
  void ExecuteFinalize() override
  {
  }

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "ManageSelectedElementsProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "ManageSelectedElementsProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{

  /// Copy constructor.
  ManageSelectedElementsProcess(ManageSelectedElementsProcess const &rOther);

  ///@}
  ///@name Protected Operations
  ///@{
  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{

  ModelPart &mrModelPart;

  SpatialBoundingBox mBoundingBox;

  ///@}
  ///@name Private Operators
  ///@{

  bool CheckMovementIncrement(Node<3>& rNode)
  {

    double norm_velocity = norm_2(rNode.FastGetSolutionStepValue(VELOCITY, 1));
    double norm_acceleration = norm_2(rNode.FastGetSolutionStepValue(ACCELERATION, 1));

    if (norm_velocity != 0)
      norm_velocity = norm_2(rNode.FastGetSolutionStepValue(VELOCITY)) / norm_velocity;

    if (norm_acceleration != 0)
      norm_acceleration = norm_2(rNode.FastGetSolutionStepValue(ACCELERATION)) / norm_acceleration;


    if (rNode.Is(FREE_SURFACE))
    {
      if (norm_velocity > 10 || norm_acceleration > 20)
        return false;
    }
    else
    {
      if (norm_velocity > 4 || norm_acceleration > 8)
        return false;
    }

    return true;

  }

  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{

  /// Assignment operator.
  ManageSelectedElementsProcess &operator=(ManageSelectedElementsProcess const &rOther);

  ///@}
  ///@name Serialization
  ///@{
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{
  ///@}

}; // Class ManageSelectedElementsProcess

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                ManageSelectedElementsProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const ManageSelectedElementsProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_MANAGE_MARKER_ELEMENTS_PROCESS_HPP_INCLUDED  defined
