//
//   Project Name:        KratosPfemSolidMechanicsApplication $
//   Created by:          $Author:                  LMonforte $
//   Last modified by:    $Co-Author:                         $
//   Date:                $Date:                    July 2019 $
//   Revision:            $Revision:                      0.0 $
//
//

#include "includes/model_part.h"

/* System includes */


/* External includes */
#include <fstream>

/* Project includes */
#include "custom_processes/stress_path_process.hpp"

namespace Kratos
{

   StressPathProcess::StressPathProcess( ModelPart & rModelPart, Parameters rParameters)
      : Process(Flags()), mrModelPart( rModelPart)
   {
      KRATOS_TRY

      std::cout << " StressPathProcessConstructed " << std::endl;
      Parameters default_parameters( R"(
      {
         "xp": [0.0],
         "yp": [0.0],
         "file_name": "output.txt",
         "deformed": false
      } )" );

         rParameters.ValidateAndAssignDefaults(default_parameters);

      mXPVector.resize(0);
      mYPVector.resize(0);
      for (unsigned int ii = 0; ii< rParameters["xp"].size() ; ++ii) {
         mXPVector.push_back( rParameters["xp"][ii].GetDouble() );
         mYPVector.push_back( rParameters["yp"][ii].GetDouble() );
      }


      std::cout << mXPVector << std::endl;
      std::cout << mYPVector << std::endl;

      mDeformed = rParameters["deformed"].GetBool();
      mFileName = rParameters["file_name"].GetString();


      KRATOS_CATCH("")
   }

   // destructor 
   StressPathProcess::~StressPathProcess()
   {

   }

   void StressPathProcess::Execute()
   {

      KRATOS_TRY


      std::ofstream thisFile;
      thisFile.open( mFileName, std::fstream::app);
      std::string ThisLine;
      for (unsigned nn = 0; nn < mXPVector.size(); nn++)
      {
         Element::Pointer pTheElement;
         pTheElement = SearchTheElement( mXPVector[nn], mYPVector[nn] );
         //std::cout << " THE ELEMENT " << pTheElement << " " << pTheElement->Id() << std::endl;
         WriteStressPathToFile( pTheElement, ThisLine);
         thisFile << ThisLine << " \n";
      }
      thisFile.close();

      KRATOS_CATCH("")

   }


   Element::Pointer StressPathProcess::SearchTheElement(const double & rX, const double & rY)
   {
      KRATOS_TRY

      double MaxDistance = 1000;
      Element::Pointer pBestElement;

      GeometryData::IntegrationMethod MyIntegrationMethod;
      array_1d<double,3> AuxLocalCoordinates;
      array_1d<double,3> AuxGlobalCoordinates;

      for ( ModelPart::ElementsContainerType::iterator ie = mrModelPart.ElementsBegin(); ie != mrModelPart.ElementsEnd(); ie++)
      {


         if ( ie->IsNot(SOLID) )
            continue;

         Element::GeometryType & rGeom = ie->GetGeometry();
         MyIntegrationMethod = ie->GetIntegrationMethod();
         const Element::GeometryType::IntegrationPointsArrayType& IntegrationPoints = rGeom.IntegrationPoints(MyIntegrationMethod);
         unsigned int numberOfGP = IntegrationPoints.size();


         std::vector<ConstitutiveLaw::Pointer> ConstitutiveLawVector(numberOfGP);
         //ie->GetValueOnIntegrationPoints(CONSTITUTIVE_LAW, ConstitutiveLawVector, rCurrentProcessInfo);



         for ( unsigned int nGP = 0 ; nGP < numberOfGP; nGP++) {

            for (unsigned int i = 0; i < 3; i++)
               AuxLocalCoordinates[i] = IntegrationPoints[nGP][i];
            rGeom.GlobalCoordinates(AuxGlobalCoordinates, AuxLocalCoordinates);

            double ElementX = AuxGlobalCoordinates[0];
            double ElementY = AuxGlobalCoordinates[1];

            if ( mDeformed == true) {
               double xx(0), yy(0);
               for ( unsigned int ii = 0; ii < ie->GetGeometry().size() ; ii++) {
                  const array_1d<double, 3> & Displacement = ie->GetGeometry()[ii].FastGetSolutionStepValue( DISPLACEMENT) ;
                  xx += Displacement[0];
                  yy += Displacement[1];
               }
               double Size = double( ie->GetGeometry().size());
               xx /= Size;
               yy /= Size;
               ElementX -= xx;
               ElementY -=yy;
            }



            double ThisDistance = pow( ElementX-rX, 2);
            ThisDistance += pow( ElementY-rY, 2);

            if ( ThisDistance < MaxDistance)
            {
               MaxDistance = ThisDistance;
               pBestElement = *(ie.base());
            }


         }

      }

      return pBestElement;


      KRATOS_CATCH("")
   }


   void StressPathProcess::WriteStressPathToFile( Element::Pointer & prElement, std::string & rThisLine)
   {
      KRATOS_TRY


      const ProcessInfo & rCurrentProcessInfo = mrModelPart.GetProcessInfo();

      std::vector<Vector> VectorVector;

      rThisLine = "";
      double Time = rCurrentProcessInfo[TIME];
      rThisLine += std::to_string(Time);
      rThisLine.append(" , ");

      prElement->CalculateOnIntegrationPoints( CAUCHY_STRESS_VECTOR, VectorVector, rCurrentProcessInfo);
      AppendVectorToLine( rThisLine, VectorVector[0]);

      GetAndAppendScalarVariableToLine( "PS", prElement, rThisLine); 
      GetAndAppendScalarVariableToLine( "PT", prElement, rThisLine); 
      GetAndAppendScalarVariableToLine( "PM", prElement, rThisLine); 
      GetAndAppendScalarVariableToLine( "PLASTIC_VOL_DEF", prElement, rThisLine); 
      GetAndAppendScalarVariableToLine( "PLASTIC_DEV_DEF", prElement, rThisLine); 
      GetAndAppendScalarVariableToLine( "PLASTIC_VOL_DEF_ABS", prElement, rThisLine); 
      GetAndAppendScalarVariableToLine( "NONLOCAL_PLASTIC_VOL_DEF", prElement, rThisLine); 
      GetAndAppendScalarVariableToLine( "NONLOCAL_PLASTIC_DEV_DEF", prElement, rThisLine); 
      GetAndAppendScalarVariableToLine( "NONLOCAL_PLASTIC_VOL_DEF_ABS", prElement, rThisLine); 


      KRATOS_CATCH("")
   }

   void StressPathProcess::GetAndAppendScalarVariableToLine( const std::string & rVariable, Element::Pointer & prElement, std::string & rLine)
   {
      KRATOS_TRY

      const ProcessInfo & rCurrentProcessInfo = mrModelPart.GetProcessInfo();

      std::vector<double> Vector;

      const Variable<double> & rTheVariable = KratosComponents< Variable<double> >::Get( rVariable);

      prElement->CalculateOnIntegrationPoints( rTheVariable, Vector, rCurrentProcessInfo);
      rLine += std::to_string( Vector[0] );
      rLine.append(" , ");


      KRATOS_CATCH("")
   }

   void StressPathProcess::AppendVectorToLine( std::string & rLine, const Vector & rVector)
   {
      KRATOS_TRY

      for (unsigned int i = 0; i < rVector.size();  i++)
      {
         rLine += std::to_string( rVector[i] );
         rLine.append(" , ");
      }

      KRATOS_CATCH("")
   }

} // end namespace Kratos
