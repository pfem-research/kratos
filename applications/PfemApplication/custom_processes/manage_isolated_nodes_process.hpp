//
//   Project Name:        KratosPfemApplication    $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:         July 2018 $
//
//

#if !defined(KRATOS_MANAGE_ISOLATED_NODES_PROCESS_HPP_INCLUDED)
#define KRATOS_MANAGE_ISOLATED_NODES_PROCESS_HPP_INCLUDED

// System includes

// External includes

// Project includes
#include "custom_bounding/spatial_bounding_box.hpp"
#include "custom_utilities/kinematic_utilities.hpp"
#include "custom_utilities/set_flags_utilities.hpp"

#include "processes/process.h"

namespace Kratos
{

///@name Kratos Classes
///@{

/// Process for managing the time integration of variables for isolated nodes
class ManageIsolatedNodesProcess : public Process
{
public:
  ///@name Type Definitions
  ///@{

  typedef Node<3> NodeType;
  typedef Geometry<NodeType> GeometryType;
  typedef GlobalPointersVector<Node<3>> NodeWeakPtrVectorType;

  /// Pointer definition of ManageIsolatedNodesProcess
  KRATOS_CLASS_POINTER_DEFINITION(ManageIsolatedNodesProcess);

  ///@}
  ///@name Life Cycle
  ///@{

  ManageIsolatedNodesProcess(ModelPart &rModelPart) : Process(Flags()), mrModelPart(rModelPart)
  {
  }

  /// Destructor.
  virtual ~ManageIsolatedNodesProcess() {}

  ///@}
  ///@name Operators
  ///@{

  /// This operator is provided to call the process as a function and simply calls the Execute method.
  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  /// Execute method is used to execute the ManageIsolatedNodesProcess algorithms.
  void Execute() override
  {
  }

  /// this function is designed for being called at the beginning of the computations
  /// right after reading the model and the groups
  void ExecuteInitialize() override
  {
  }

  /// this function is designed for being execute once before the solution loop but after all of the
  /// solvers where built
  void ExecuteBeforeSolutionLoop() override
  {
    KRATOS_TRY

    double Radius = 0.0;
    //BOUNDARY flag must be set in model part nodes
    mBoundingBox = SpatialBoundingBox(mrModelPart, Radius, 0.4);

    KRATOS_CATCH("")
  }

  /// this function will be executed at every time step BEFORE performing the solve phase
  void ExecuteInitializeSolutionStep() override
  {
    KRATOS_TRY

    this->InitializePressureState(mrModelPart);
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{PERIODIC}}, {PERIODIC.AsFalse()});
    this->MoveIsolatedNodes(mrModelPart);

    KRATOS_CATCH("")
  }

  /// this function will be executed at every time step AFTER performing the solve phase
  void ExecuteFinalizeSolutionStep() override
  {
    KRATOS_TRY

    this->UpdatePressureState(mrModelPart);

    KRATOS_CATCH("")
  }

  /// this function will be executed at every time step BEFORE  writing the output
  void ExecuteBeforeOutputStep() override
  {
  }

  /// this function will be executed at every time step AFTER writing the output
  void ExecuteAfterOutputStep() override
  {
  }

  /// this function is designed for being called at the end of the computations
  /// right after reading the model and the groups
  void ExecuteFinalize() override
  {
  }

  ///@}
  ///@name Access
  ///@{

  ///@}
  ///@name Inquiry
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "ManageIsolatedNodesProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "ManageIsolatedNodesProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{

  /// Copy constructor.
  ManageIsolatedNodesProcess(ManageIsolatedNodesProcess const &rOther);

  ///@}
  ///@name Protected Operations
  ///@{
  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{

  ModelPart &mrModelPart;

  SpatialBoundingBox mBoundingBox;

  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{

  bool InitializePressureState(ModelPart &rModelPart)
  {
    const int nnodes = rModelPart.Nodes().size();

    if (nnodes == 0)
      return false;

    ModelPart::NodesContainerType::iterator it_begin = rModelPart.NodesBegin();

#pragma omp parallel for
    for (int i = 0; i < nnodes; ++i)
    {
      ModelPart::NodesContainerType::iterator i_node = it_begin + i;

      if ( i_node->Is(ISOLATED) || (i_node->Is(RIGID) && i_node->IsNot(FLUID)))
      {
        if (i_node->SolutionStepsDataHas(PRESSURE))
        {
          i_node->FastGetSolutionStepValue(PRESSURE, 0) = 0.0;
          i_node->FastGetSolutionStepValue(PRESSURE, 1) = 0.0;
        }
        if (i_node->SolutionStepsDataHas(PRESSURE_VELOCITY))
        {
          i_node->FastGetSolutionStepValue(PRESSURE_VELOCITY, 0) = 0.0;
          i_node->FastGetSolutionStepValue(PRESSURE_VELOCITY, 1) = 0.0;
        }
        if (i_node->SolutionStepsDataHas(PRESSURE_ACCELERATION))
        {
          i_node->FastGetSolutionStepValue(PRESSURE_ACCELERATION, 0) = 0.0;
          i_node->FastGetSolutionStepValue(PRESSURE_ACCELERATION, 1) = 0.0;
        }
        if (i_node->SolutionStepsDataHas(MESH_VELOCITY)) //ALE
        {
          i_node->FastGetSolutionStepValue(MESH_DISPLACEMENT) = i_node->FastGetSolutionStepValue(DISPLACEMENT);
          i_node->FastGetSolutionStepValue(MESH_VELOCITY) = i_node->FastGetSolutionStepValue(VELOCITY);
          i_node->FastGetSolutionStepValue(MESH_ACCELERATION) = i_node->FastGetSolutionStepValue(ACCELERATION);
        }
      }
    }

    return true;
  }

  bool UpdatePressureState(ModelPart &rModelPart)
  {
    const int nnodes = rModelPart.Nodes().size();

    if (nnodes == 0)
      return false;

    //const double TimeStep = rModelPart.GetProcessInfo()[DELTA_TIME];

    SetFlagsUtilities::SetFlagsToNodes(rModelPart, {{VISITED}}, {VISITED.AsFalse()});

    // correct isolated element nodes
    this->SetIsolatedElementNodes(rModelPart);

    this->SetRigidElementEdgeNodes(rModelPart);

    this->SetSemiIsolatedPressureNodes(rModelPart);


    ModelPart::NodesContainerType::iterator it_begin = rModelPart.NodesBegin();

    // const Variable<array_1d<double, 3>>& VelocityVariable = KinematicUtilities::GetMeshVariable(*it_begin,"VELOCITY");
    // const Variable<array_1d<double, 3>>& AccelerationVariable = KinematicUtilities::GetMeshVariable(*it_begin,"ACCELERATION");

#pragma omp parallel for
    for (int i = 0; i < nnodes; ++i)
    {
      ModelPart::NodesContainerType::iterator i_node = it_begin + i;

      if ( i_node->Is(VISITED) )
      {
        if (i_node->SolutionStepsDataHas(PRESSURE))
        {
          i_node->FastGetSolutionStepValue(PRESSURE, 0) = 0.0;
          i_node->FastGetSolutionStepValue(PRESSURE, 1) = 0.0;
        }
        if (i_node->SolutionStepsDataHas(PRESSURE_VELOCITY))
        {
          i_node->FastGetSolutionStepValue(PRESSURE_VELOCITY, 0) = 0.0;
          i_node->FastGetSolutionStepValue(PRESSURE_VELOCITY, 1) = 0.0;
        }
        if (i_node->SolutionStepsDataHas(PRESSURE_ACCELERATION))
        {
          i_node->FastGetSolutionStepValue(PRESSURE_ACCELERATION, 0) = 0.0;
          i_node->FastGetSolutionStepValue(PRESSURE_ACCELERATION, 1) = 0.0;
        }

        // if (i_node->SolutionStepsDataHas(VOLUME_ACCELERATION))
        // {
        //   const array_1d<double, 3> &VolumeAcceleration = i_node->FastGetSolutionStepValue(VOLUME_ACCELERATION);
        //   noalias(i_node->FastGetSolutionStepValue(AccelerationVariable)) = VolumeAcceleration;
        // }
        // else if (i_node->SolutionStepsDataHas(BODY_FORCE))
        // {
        //   const array_1d<double, 3> &VolumeAcceleration = i_node->FastGetSolutionStepValue(BODY_FORCE);
        //   noalias(i_node->FastGetSolutionStepValue(AccelerationVariable)) = VolumeAcceleration;
        // }

        // i_node->FastGetSolutionStepValue(VelocityVariable) = i_node->FastGetSolutionStepValue(VelocityVariable, 1) + i_node->FastGetSolutionStepValue(AccelerationVariable) * TimeStep;

        // i_node->FastGetSolutionStepValue(DISPLACEMENT) = i_node->FastGetSolutionStepValue(DISPLACEMENT, 1) + i_node->FastGetSolutionStepValue(VelocityVariable) * TimeStep + 0.5 * i_node->FastGetSolutionStepValue(AccelerationVariable) * TimeStep * TimeStep;

        // if (i_node->SolutionStepsDataHas(MESH_VELOCITY)) //ALE
        // {
        //   i_node->FastGetSolutionStepValue(DISPLACEMENT) = i_node->FastGetSolutionStepValue(MESH_DISPLACEMENT);
        //   i_node->FastGetSolutionStepValue(VELOCITY) = i_node->FastGetSolutionStepValue(MESH_VELOCITY);
        //   i_node->FastGetSolutionStepValue(ACCELERATION) = i_node->FastGetSolutionStepValue(MESH_ACCELERATION);
        // }

      }

      if (i_node->Is(ISOLATED))
      {
        // Ale integartion predicts a wrong MESH quantities for isolated nodes
        if (i_node->SolutionStepsDataHas(MESH_VELOCITY)) //ALE
        {
          //i_node->FastGetSolutionStepValue(MESH_DISPLACEMENT) = i_node->FastGetSolutionStepValue(DISPLACEMENT);
          i_node->FastGetSolutionStepValue(MESH_VELOCITY) = i_node->FastGetSolutionStepValue(VELOCITY);
          i_node->FastGetSolutionStepValue(MESH_ACCELERATION) = i_node->FastGetSolutionStepValue(ACCELERATION);
        }
      }


    }

    SetFlagsUtilities::SetFlagsToNodes(rModelPart, {{VISITED}}, {VISITED.AsFalse()});

    return true;
  }

  bool MoveIsolatedNodes(ModelPart &rModelPart)
  {
    const int nnodes = rModelPart.Nodes().size();

    if (nnodes == 0)
      return false;

    const double TimeStep = rModelPart.GetProcessInfo()[DELTA_TIME];

    SetFlagsUtilities::SetFlagsToNodes(rModelPart, {{VISITED}}, {VISITED.AsFalse()});

    // correct isolated and semi-isolated nodes kinematics
    this->SetSemiIsolatedNodes(rModelPart);

    ModelPart::NodesContainerType::iterator it_begin = rModelPart.NodesBegin();

    const Variable<array_1d<double, 3>>& VelocityVariable = KinematicUtilities::GetMeshVariable(*it_begin,"VELOCITY");
    const Variable<array_1d<double, 3>>& AccelerationVariable = KinematicUtilities::GetMeshVariable(*it_begin,"ACCELERATION");

#pragma omp parallel for
    for (int i = 0; i < nnodes; ++i)
    {
      ModelPart::NodesContainerType::iterator i_node = it_begin + i;

      if (i_node->Is(ISOLATED))
      {
        // std::cout<<" ISOLATED node "<<i_node->Id()<<" manage_isolated_nodes_process.hpp "<<std::endl;
        // std::cout<<"PRE POSITION "<<i_node->Coordinates()<<"  ACCELERATION "<<i_node->FastGetSolutionStepValue(AccelerationVariable)<<std::endl;
        // std::cout<<"DISPLACEMENT "<<i_node->FastGetSolutionStepValue(DISPLACEMENT)<<"  VELOCITY "<<i_node->FastGetSolutionStepValue(VelocityVariable)<<std::endl;
        // std::cout<<" Mesh_V "<<i_node->FastGetSolutionStepValue(MESH_VELOCITY)<<" Mesh_A "<<i_node->FastGetSolutionStepValue(MESH_ACCELERATION)<<std::endl;
        // std::cout<<" V "<<i_node->FastGetSolutionStepValue(VELOCITY)<<" A "<<i_node->FastGetSolutionStepValue(ACCELERATION)<<std::endl;

        if (i_node->SolutionStepsDataHas(VOLUME_ACCELERATION))
        {
          const array_1d<double, 3> &VolumeAcceleration = i_node->FastGetSolutionStepValue(VOLUME_ACCELERATION);
          noalias(i_node->FastGetSolutionStepValue(AccelerationVariable)) = VolumeAcceleration;
        }
        else if (i_node->SolutionStepsDataHas(BODY_FORCE))
        {
          const array_1d<double, 3> &VolumeAcceleration = i_node->FastGetSolutionStepValue(BODY_FORCE);
          noalias(i_node->FastGetSolutionStepValue(AccelerationVariable)) = VolumeAcceleration;
        }

        noalias(i_node->FastGetSolutionStepValue(VelocityVariable)) += i_node->FastGetSolutionStepValue(AccelerationVariable) * TimeStep;

        if (i_node->SolutionStepsDataHas(MESH_VELOCITY)) //ALE, velocities modified in the nodes removal
        {
          // noalias(i_node->Coordinates()) -= i_node->FastGetSolutionStepValue(DISPLACEMENT);
          // noalias(i_node->FastGetSolutionStepValue(DISPLACEMENT)) = i_node->FastGetSolutionStepValue(DISPLACEMENT, 1) + i_node->FastGetSolutionStepValue(VelocityVariable) * TimeStep + 0.5 * i_node->FastGetSolutionStepValue(AccelerationVariable) * TimeStep * TimeStep;
          // noalias(i_node->Coordinates()) += i_node->FastGetSolutionStepValue(DISPLACEMENT);

          i_node->FastGetSolutionStepValue(VELOCITY) = i_node->FastGetSolutionStepValue(MESH_VELOCITY);
          i_node->FastGetSolutionStepValue(ACCELERATION) = i_node->FastGetSolutionStepValue(MESH_ACCELERATION);
        }

        // std::cout<<"POST POSITION "<<i_node->Coordinates()<<"  ACCELERATION "<<i_node->FastGetSolutionStepValue(AccelerationVariable)<<std::endl;
        // std::cout<<"DISPLACEMENT "<<i_node->FastGetSolutionStepValue(DISPLACEMENT)<<"  VELOCITY "<<i_node->FastGetSolutionStepValue(VelocityVariable)<<std::endl;

        if (!mBoundingBox.IsInside(*i_node))
        {
          i_node->Set(TO_ERASE);
          std::cout << " ISOLATED node["<<i_node->Id()<<"] TO_ERASE in manage_isolated_nodes_proces.hpp: VISITED -> " << i_node->Is(VISITED) << std::endl;


          if (i_node->Is(RIGID))
            KRATOS_ERROR << "RIGID NODE REMOVED "<<std::endl;
        }
      }
      else if (i_node->Is(VISITED)) // semi-isolated
      {
	std::cout << " semi ISOLATED node MODIFIED["<<i_node->Id()<<"]  in manage_isolated_nodes_proces.hpp: VISITED -> " << std::endl;

	i_node->Set(PERIODIC);
        if (i_node->SolutionStepsDataHas(VOLUME_ACCELERATION))
        {
          const array_1d<double, 3> &VolumeAcceleration = i_node->FastGetSolutionStepValue(VOLUME_ACCELERATION);
          noalias(i_node->FastGetSolutionStepValue(AccelerationVariable)) = VolumeAcceleration;
        }
        else if (i_node->SolutionStepsDataHas(BODY_FORCE))
        {
          const array_1d<double, 3> &VolumeAcceleration = i_node->FastGetSolutionStepValue(BODY_FORCE);
          noalias(i_node->FastGetSolutionStepValue(AccelerationVariable)) = VolumeAcceleration;
        }

	// double factor = 0.1;
	// array_1d<double, 3> Direction = i_node->FastGetSolutionStepValue(AccelerationVariable);
	// double norm = norm_2(Direction);
	// if (norm !=0)
	//   Direction /= norm;

	// double projection = inner_prod(i_node->FastGetSolutionStepValue(VelocityVariable), Direction);
	// i_node->FastGetSolutionStepValue(VelocityVariable) *= (1.0-factor);
	// noalias(i_node->FastGetSolutionStepValue(VelocityVariable)) += factor* projection * Direction;

	// projection = inner_prod(i_node->FastGetSolutionStepValue(VelocityVariable,1), Direction);
	// i_node->FastGetSolutionStepValue(VelocityVariable,1) *= (1.0-factor);
	// noalias(i_node->FastGetSolutionStepValue(VelocityVariable,1)) += factor* projection * Direction;

	noalias(i_node->FastGetSolutionStepValue(VelocityVariable)) += i_node->FastGetSolutionStepValue(AccelerationVariable) * TimeStep;
	noalias(i_node->FastGetSolutionStepValue(VelocityVariable,1)) += i_node->FastGetSolutionStepValue(AccelerationVariable) * TimeStep;

        //noalias(i_node->FastGetSolutionStepValue(VelocityVariable)) = 0.25 * (i_node->FastGetSolutionStepValue(VelocityVariable) + i_node->FastGetSolutionStepValue(VelocityVariable, 1)) + i_node->FastGetSolutionStepValue(AccelerationVariable) * TimeStep;

        //noalias(i_node->FastGetSolutionStepValue(VelocityVariable)) += i_node->FastGetSolutionStepValue(AccelerationVariable) * TimeStep;
        //noalias(i_node->FastGetSolutionStepValue(VelocityVariable,1)) += i_node->FastGetSolutionStepValue(AccelerationVariable) * TimeStep;


        if (i_node->SolutionStepsDataHas(MESH_VELOCITY)) //ALE, velocities modified in the nodes removal
        {
          // noalias(i_node->Coordinates()) -= i_node->FastGetSolutionStepValue(DISPLACEMENT);
          // noalias(i_node->FastGetSolutionStepValue(DISPLACEMENT)) = 0.5 * (i_node->FastGetSolutionStepValue(DISPLACEMENT) + i_node->FastGetSolutionStepValue(DISPLACEMENT, 1) )  + i_node->FastGetSolutionStepValue(VelocityVariable) * TimeStep + 0.5 * i_node->FastGetSolutionStepValue(AccelerationVariable) * TimeStep * TimeStep;
          // noalias(i_node->Coordinates()) += i_node->FastGetSolutionStepValue(DISPLACEMENT);

          i_node->FastGetSolutionStepValue(VELOCITY) = i_node->FastGetSolutionStepValue(MESH_VELOCITY);
          i_node->FastGetSolutionStepValue(ACCELERATION) = i_node->FastGetSolutionStepValue(MESH_ACCELERATION);
        }

      }
    }

    SetFlagsUtilities::SetFlagsToNodes(rModelPart, {{VISITED}}, {VISITED.AsFalse()});

    /**
     // correct critical nodes (interaction+freesurface) in slip contours kinematics
     this->SetCriticalNodes(rModelPart);

     #pragma omp parallel for
     for (int i = 0; i < nnodes; ++i)
     {
     ModelPart::NodesContainerType::iterator i_node = it_begin + i;

     if (i_node->Is(VISITED))
     {
     noalias(i_node->FastGetSolutionStepValue(AccelerationVariable)) = 0.5 * (i_node->FastGetSolutionStepValue(AccelerationVariable) + i_node->FastGetSolutionStepValue(AccelerationVariable, 1));
     noalias(i_node->FastGetSolutionStepValue(VelocityVariable)) = 0.5 * (i_node->FastGetSolutionStepValue(VelocityVariable) + i_node->FastGetSolutionStepValue(VelocityVariable, 1));
     }

     }

     SetFlagsUtilities::SetFlagsToNodes(rModelPart, {{VISITED}}, {VISITED.AsFalse()});
    **/
    return true;
  }


  void SetSemiIsolatedNodes(ModelPart &rModelPart)
  {
    const int nnodes = rModelPart.Nodes().size();

    if (nnodes != 0)
    {
      ModelPart::NodesContainerType::iterator it_begin = rModelPart.NodesBegin();

#pragma omp parallel for
      for (int i = 0; i < nnodes; ++i)
      {
        ModelPart::NodesContainerType::iterator i_node = it_begin + i;

        if (i_node->And({FREE_SURFACE,INTERACTION}))
        {
          NodeWeakPtrVectorType &nNodes = i_node->GetValue(NEIGHBOUR_NODES);
          unsigned int rigid = 0;
          for (auto &i_nnodes : nNodes)
            if (i_nnodes.Is(STRUCTURE))
              ++rigid;

          if (rigid == nNodes.size()){
            i_node->Set(VISITED, true);
          }
        }
      }
    }
  }

  void SetSemiIsolatedPressureNodes(ModelPart &rModelPart)
  {
    const int nnodes = rModelPart.Nodes().size();

    if (nnodes != 0)
    {
      ModelPart::NodesContainerType::iterator it_begin = rModelPart.NodesBegin();

#pragma omp parallel for
      for (int i = 0; i < nnodes; ++i)
      {
        ModelPart::NodesContainerType::iterator i_node = it_begin + i;
        std::vector<double> pressures;
        if(std::abs(i_node->FastGetSolutionStepValue(PRESSURE))>1e4){
          NodeWeakPtrVectorType &nNodes = i_node->GetValue(NEIGHBOUR_NODES);
          for (auto &i_nnode : nNodes){
            pressures.push_back(std::abs(i_nnode.FastGetSolutionStepValue(PRESSURE)));
          }
          if(std::abs(i_node->FastGetSolutionStepValue(PRESSURE))*0.01 > *std::max_element(pressures.begin(), pressures.end()))
          {
            i_node->Set(VISITED, true);
          }
        }

        if ( i_node->And({FREE_SURFACE,INTERACTION}) && std::abs(i_node->FastGetSolutionStepValue(PRESSURE))>1e3)
        {
          NodeWeakPtrVectorType &nNodes = i_node->GetValue(NEIGHBOUR_NODES);
          unsigned int rigid = 0;
          unsigned int free_surface = 0;
          for (auto &i_nnode : nNodes){
            if (i_nnode.Is(STRUCTURE))
              ++rigid;
            else if (i_nnode.Is(FREE_SURFACE))
              ++free_surface;
          }

          if (rigid + free_surface == nNodes.size())
            i_node->Set(VISITED, true);

        }
	else if ( i_node->And({FREE_SURFACE,STRUCTURE}) && std::abs(i_node->FastGetSolutionStepValue(PRESSURE))>1e4)
        {
	  i_node->Set(VISITED, true);
	}
	else if ( i_node->And({FREE_SURFACE,STRUCTURE.AsFalse(),INTERACTION.AsFalse()}) && std::abs(i_node->FastGetSolutionStepValue(PRESSURE))>1e4)
        {
	  i_node->Set(VISITED, true);
	}
      }
    }
  }

  void SetCriticalNodes(ModelPart &rModelPart)
  {

    const int nnodes = rModelPart.Nodes().size();

    if (nnodes != 0)
    {
      ModelPart::NodesContainerType::iterator it_begin = rModelPart.NodesBegin();

#pragma omp parallel for
      for (int i = 0; i < nnodes; ++i)
      {
        ModelPart::NodesContainerType::iterator i_node = it_begin + i;

        if (i_node->Is(FREE_SURFACE) && i_node->Is(INTERACTION))
        {
          NodeWeakPtrVectorType &nNodes = i_node->GetValue(NEIGHBOUR_NODES);
          unsigned int slip = 0;
          for (auto &i_nnodes : nNodes)
          {
            if (i_nnodes.Is(SLIP) )
              ++slip;
          }
          if (slip > 2)
            i_node->Set(VISITED, true);
        }
      }
    }
  }

  void SetRigidElementEdgeNodes(ModelPart &rModelPart)
  {
    const int nelements = rModelPart.Elements().size();
    if (nelements != 0)
    {
      for (auto &i_elem : rModelPart.Elements())
      {
	GeometryType &rGeometry = i_elem.GetGeometry();
	if (SetFlagsUtilities::CheckAllNodesFlag(rGeometry,{STRUCTURE})){
	  for(auto &i_node : rGeometry){
	    if(i_node.GetValue(NEIGHBOUR_NODES).size() == rGeometry.size()-1)
	      i_node.Set(VISITED, true);
	  }
	}
      }
    }
  }

  void SetIsolatedElementNodes(ModelPart &rModelPart)
  {
    const int nelements = rModelPart.Elements().size();
    if (nelements != 0)
    {
      for (auto &i_elem : rModelPart.Elements())
      {
        if (i_elem.Is(ISOLATED))
          for(auto &i_node : i_elem.GetGeometry())
            i_node.Set(VISITED, true);
      }
    }
  }


  ///@}
  ///@name Private  Access
  ///@{

  /// Assignment operator.
  ManageIsolatedNodesProcess &operator=(ManageIsolatedNodesProcess const &rOther);

  ///@}
  ///@name Serialization
  ///@{
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{
  ///@}

}; // Class ManageIsolatedNodesProcess

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                ManageIsolatedNodesProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const ManageIsolatedNodesProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_MANAGE_ISOLATED_NODES_PROCESS_HPP_INCLUDED  defined
