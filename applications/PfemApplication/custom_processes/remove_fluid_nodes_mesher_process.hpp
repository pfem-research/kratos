//
//   Project Name:        KratosPfemApplication    $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:         July 2018 $
//
//

#if !defined(KRATOS_REMOVE_FLUID_NODES_MESHER_PROCESS_HPP_INCLUDED)
#define KRATOS_REMOVE_FLUID_NODES_MESHER_PROCESS_HPP_INCLUDED

// External includes

// System includes

// Project includes

namespace Kratos
{

///@name Kratos Classes
///@{

/// Remove Mesh Nodes Process for 2D and 3D cases
/** The process labels the nodes to be erased (TO_ERASE)
    if they are too close (mRemoveOnDistance == true)
    if the error of the patch they belong is very small (REMOVE_NODES_ON_ERROR)
    In the interior of the domain or in the boundary (REMOVE_BOUNDARY_NODES) ...

    Additional treatment of the nonconvex boundaries is also going to erase nodes.

    At the end of the execution nodes are cleaned (only in the current mesh)
    If boundary nodes are removed, conditions must be build again (new conditions are build in the current mesh)
*/

/**NOTE: as kratos uses shared pointers, the main list of nodes can be changed.
   the element contains the geometry which has pointers to nodes, so that nodes are kept in memory and their information can be recovered.
   that means also that if the element information is needed it is kept until the end when new elements are created and replace the old ones.
*/

class RemoveFluidNodesMesherProcess
    : public MesherProcess
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of Process
  KRATOS_CLASS_POINTER_DEFINITION(RemoveFluidNodesMesherProcess);

  typedef Node<3> NodeType;
  typedef array_1d<double,3> VectorType;
  typedef ModelPart::MeshType::GeometryType GeometryType;
  typedef GeometryType::PointsArrayType PointsArrayType;

  typedef GlobalPointersVector<NodeType> NodeWeakPtrVectorType;
  typedef GlobalPointersVector<Element> ElementWeakPtrVectorType;
  typedef GlobalPointersVector<Condition> ConditionWeakPtrVectorType;

  typedef Bucket<3, NodeType, std::vector<NodeType::Pointer>, NodeType::Pointer, std::vector<NodeType::Pointer>::iterator, std::vector<double>::iterator> BucketType;
  typedef Tree<KDTreePartition<BucketType>> KdtreeType; //Kdtree

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  RemoveFluidNodesMesherProcess(ModelPart &rModelPart,
                                MesherData::MeshingParameters &rRemeshingParameters,
                                int EchoLevel)
        : mrModelPart(rModelPart),
        mrRemesh(rRemeshingParameters)
  {
    mEchoLevel = EchoLevel;
  }

  /// Destructor.
  virtual ~RemoveFluidNodesMesherProcess() {}

  ///@}
  ///@name Operators
  ///@{

  /// This operator is provided to call the process as a function and simply calls the Execute method.
  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  /// Execute method is used to execute the Process algorithms.
  void Execute() override
  {
    KRATOS_TRY

    this->Initialize();

    this->RemoveNodes(mrModelPart, mrRemesh);

    this->Finalize();

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "RemoveFluidNodesMesherProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "RemoveFluidNodesMesherProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{

  ModelPart &mrModelPart;

  MesherData::MeshingParameters &mrRemesh;

  int mEchoLevel;

  // struct for monitoring the process information
  struct ProcessInfoData
  {
    unsigned int initial_conditions;
    unsigned int initial_nodes;
    unsigned int final_conditions;
    unsigned int final_nodes;

    bool any_condition_removed;
    std::vector<bool> node_removals;

    void Initialize(int EchoLevel)
    {
      initial_conditions = 0;
      initial_nodes = 0;
      final_conditions = 0;
      final_nodes = 0;

      any_condition_removed = false;
      node_removals.clear();

      if (EchoLevel > 0)
        std::cout << " [ REMOVE CLOSE NODES: " << std::endl;
    }

    void Finalize(int EchoLevel)
    {
      if (EchoLevel > 0)
      {
        std::cout << "   [ NODES      ( removed : " << initial_nodes-final_nodes << " total: " << final_nodes << " ) ]" << std::endl;
        std::cout << "   [ CONDITIONS ( removed : " << final_conditions-initial_conditions << " total: " << final_conditions << " ) ]" << std::endl;
        std::cout << "   REMOVE CLOSE NODES ]; " << std::endl;
      }
    }
  };

  ProcessInfoData mInfoData;


  struct ItemCounter
  {
    unsigned int number_of_nodes;
    unsigned int number_of_conditions;

    bool any_node_removed;
    bool any_condition_removed;

    std::vector<bool> node_removals;
    std::vector<bool> condition_removals;
  };

  ItemCounter mCounter;



  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{
  ///@}

  //**************************************************************************
  //**************************************************************************

  unsigned int CheckInitialState()
  {
    KRATOS_TRY

    //Check
    if (mrModelPart.Name() != mrRemesh.SubModelPartName)
      std::cout << " ModelPart Supplied do not corresponds to the Meshing Domain: (" << mrModelPart.Name() << " != " << mrRemesh.SubModelPartName << ")" << std::endl;

    //check existing TO_ERASE flags in Nodes
    unsigned int counter = SetFlagsUtilities::CountFlagsInEntities(mrModelPart,"Nodes",{TO_ERASE});

    if(counter!=0){ //check if there are non ISOLATED nodes TO_ERASE (which may not occur)
      for (auto& i_node: mrModelPart.Nodes()){
	if (i_node.Is(TO_ERASE) && i_node.IsNot(ISOLATED)){
	  std::cout<<" Node ["<<i_node.Id()<<"] is TO_ERASE from heritage :: RESETING nodes TO_ERASE flag"<<std::endl;
	  //SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{TO_ERASE,ISOLATED.AsFalse()}}, {TO_ERASE.AsFalse()});
	  break;
	}
      }
    }

    return counter;

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  void Initialize()
  {
    KRATOS_TRY

    mrRemesh.Refine->Interior.Info.Initialize();
    mrRemesh.Refine->Boundary.Info.Initialize();

    mInfoData.Initialize(mEchoLevel);
    mInfoData.initial_conditions = mrModelPart.NumberOfConditions();
    mInfoData.initial_nodes = mrModelPart.NumberOfNodes();

    //in order to erase And{ISOLATED,TO_ERASE} nodes
    mInfoData.node_removals.push_back(this->CheckInitialState());

    //reset flags for the local process execution
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{BLOCKED}}, {BLOCKED.AsFalse()});

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  void Finalize()
  {
    KRATOS_TRY

    //Clean removed entities
    bool any_node_removed = std::any_of(mInfoData.node_removals.begin(), mInfoData.node_removals.end(), [](bool n) {return n;});

    if (any_node_removed || mInfoData.any_condition_removed)
    {
      if (mEchoLevel > 0)
        std::cout << " Removed Nodes: INTERIOR (error: " << mrRemesh.Refine->Interior.Info.on_error << " distance: " << mrRemesh.Refine->Interior.Info.on_distance << ")  BOUNDARY (distance: " << mrRemesh.Refine->Boundary.Info.on_distance << ")" << std::endl;
    }

    if (any_node_removed)
      RemoveTO_ERASENodes(mrModelPart);

    if (mInfoData.any_condition_removed)
      RemoveTO_ERASEConditions(mrModelPart);

    //Check
    for (auto &i_node : mrModelPart.Nodes())
      if (i_node.And({TO_ERASE,BLOCKED}))
        std::cout << " BLOCKED node set TO_ERASE (node: " << i_node.Id() << " " << i_node.Coordinates() <<")"<<std::endl;

    //Set local variables

    //reset flags for the local process execution
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{BLOCKED}}, {BLOCKED.AsFalse()});

    mInfoData.final_conditions = mrModelPart.NumberOfConditions();
    mInfoData.final_nodes = mrModelPart.NumberOfNodes();

    //Set Global variables
    mrRemesh.Info->Removed.Nodes = (mInfoData.initial_nodes-mInfoData.final_nodes);

    //Print
    mInfoData.Finalize(mEchoLevel);

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  void RemoveNodes(ModelPart &rModelPart, MesherData::MeshingParameters &rRemesh)
  {
    KRATOS_TRY

    //set BLOCKED particles for the local process execution
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{SOLID},{RIGID},{INLET},{ISOLATED}}, {BLOCKED});
    //if the remove_node switch is activated, we check if the nodes got too close
    if (rRemesh.IsInterior(RefineData::COARSEN) || rRemesh.IsBoundary(RefineData::COARSEN))
    {
      //Error node removal
      if (rRemesh.IsInterior(RefineData::COARSEN_ON_ERROR))
        mInfoData.node_removals.push_back(this->RemoveNodesOnError(rModelPart, rRemesh.Refine->Interior.Info.on_error)); //2D and 3D

      //Distance node removal
      if (rRemesh.IsInterior(RefineData::COARSEN_ON_DISTANCE) || rRemesh.IsBoundary(RefineData::COARSEN_ON_DISTANCE))
        mInfoData.node_removals.push_back(this->RemoveNodesOnDistance(rModelPart, rRemesh.Refine->Interior.Info.on_distance, rRemesh.Refine->Boundary.Info.on_distance, mInfoData.any_condition_removed));
    }
    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  bool RemoveNodesOnError(ModelPart &rModelPart, unsigned int &error_removed_nodes)
  {
    KRATOS_TRY

    if (mEchoLevel > 0)
    {
      std::cout << "   [ REMOVE NODES ON ERROR : " << std::endl;
    }
    //***SIZES :::: parameters do define the tolerance in mesh size:
    double factor_for_criterion_error = 0.5;

    bool any_node_removed = false;

    std::vector<std::pair<const Variable<double>*, double> > ErrorVariables = mrRemesh.Refine->Interior.ErrorVariables;
    for (auto& refine : mrRemesh.RefineVector)
    {
      for (auto& variable : refine->Interior.ErrorVariables)
      {
        ErrorVariables.push_back(variable);
      }
    }

    unsigned int number_of_nodes = 0;

    if (mrRemesh.InputInitializedFlag)
      number_of_nodes = mrRemesh.NodeMaxId + 1;
    else
      number_of_nodes = ModelPartUtilities::GetMaxNodeId(rModelPart) + 1;

    std::vector<int> nodes_ids(number_of_nodes);

    for(auto& error_variable : ErrorVariables)
    {
      std::vector<double> NodalError(rModelPart.NumberOfNodes() + 1);

      ModelPartUtilities::CalculateNodalError(rModelPart, NodalError, nodes_ids, *error_variable.first);

      for (auto &i_node : rModelPart.Nodes())
      {
        MesherData MeshData;
        RefineData& Refine = MeshData.GetRefineData(i_node, mrRemesh, rModelPart.GetProcessInfo());

        NodeWeakPtrVectorType &nNodes = i_node.GetValue(NEIGHBOUR_NODES);
        int erased_nodes = 0;
        for (auto &i_nnode : nNodes)
        {
          if (i_nnode.Is(TO_ERASE))
            erased_nodes += 1;
        }

        if (i_node.IsNot(BOUNDARY) && erased_nodes < 1)
        {
          double &MeanError = i_node.FastGetSolutionStepValue(MEAN_ERROR);
          MeanError = NodalError[nodes_ids[i_node.Id()]];

          ElementWeakPtrVectorType &nElements = i_node.GetValue(NEIGHBOUR_ELEMENTS);
          double mean_node_radius = 0;
          for (auto &i_nelem : nElements)
          {
            if (i_nelem.Is(FLUID))
              mean_node_radius += GeometryUtilities::CalculateElementRadius(i_nelem.GetGeometry()); //Triangle 2D, Tetrahedron 3D
          }

          mean_node_radius /= double(nElements.size());

          double error_value = 0;
          for(auto& variable : Refine.Interior.ErrorVariables)
          {
             if (error_variable.first->Name() == variable.first->Name())
                error_value = variable.second;
          }

          if (NodalError[nodes_ids[i_node.Id()]] < error_value && mean_node_radius < factor_for_criterion_error * Refine.Interior.DistanceMeshSize && i_node.IsNot(TO_REFINE))
          {
            std::cout<<"   Energy : node remove ["<<i_node.Id()<<"] : "<<NodalError[nodes_ids[i_node.Id()]]<<std::endl;
            std::cout<<"   mean_node_radius "<<mean_node_radius<<" < "<<factor_for_criterion_error * Refine.Interior.DistanceMeshSize<<" size_for_criterion_error"<<std::endl;
            i_node.Set(TO_ERASE);
            std::cout<<"     Set Node ["<<i_node.Id()<<"] TO_ERASE for interior nodal error removal in remove_nodes_mesher_process.hpp"<<std::endl;
            any_node_removed = true;
            error_removed_nodes++;
          }
        }
      }
    }

    return any_node_removed;

    KRATOS_CATCH(" ")
  }


  //**************************************************************************
  //**************************************************************************

  void RemoveTO_ERASEConditions(ModelPart &rModelPart)
  {
    //Clean Conditions
    ModelPart::ConditionsContainerType PreservedConditions;

    //id = 0;
    for (auto i_cond(rModelPart.Conditions().begin()); i_cond != rModelPart.Conditions().end(); ++i_cond)
      if (i_cond->IsNot(TO_ERASE))
        PreservedConditions.push_back(*i_cond.base());
      else
        std::cout << "   Condition ERASED :" << i_cond->Id() << "in remove_fluid_nodes_mesher_process.hpp" << std::endl;

    rModelPart.Conditions().swap(PreservedConditions);

    //Get point Neighbour Conditions
    ModelPartUtilities::SetNodeNeighbourConditions(rModelPart);
  }


  //**************************************************************************
  //**************************************************************************

  void RemoveTO_ERASENodes(ModelPart &rModelPart)
  {
    KRATOS_TRY

    ModelPart::NodesContainerType temporal_nodes;
    temporal_nodes.reserve(rModelPart.NumberOfNodes());
    temporal_nodes.swap(rModelPart.Nodes());

    for (auto i_node(temporal_nodes.begin()); i_node != temporal_nodes.end(); ++i_node)
    {
      if (i_node->IsNot(TO_ERASE))
        rModelPart.Nodes().push_back(*i_node.base());
      else
      {
        if (mEchoLevel > 0)
        {
          if (i_node->Is(BOUNDARY))
            std::cout << " Removing BOUNDARY Node: " << i_node->Id() << " :: " << i_node->X() << " , " << i_node->Y() << " , " << i_node->Z() << std::endl;
          else
            std::cout << " Removing INTERIOR Node: " << i_node->Id() << " :: " << i_node->X() << " , " << i_node->Y() << " , " << i_node->Z() << std::endl;
        }
        if (i_node->Is(STRUCTURE))
          std::cout << " Remving STRUCTURE Node: " << i_node->Id() << " :: " << i_node->X() << " , " << i_node->Y() << " , " << i_node->Z() << std::endl;


      }
    }

    rModelPart.Nodes().Sort();

    KRATOS_CATCH("")
  }


  //**************************************************************************
  //**************************************************************************


  bool RemoveNodesOnDistance(ModelPart &rModelPart,
                             unsigned int &inside_nodes_removed,
                             unsigned int &boundary_nodes_removed,
                             bool &any_condition_removed)
  {
    KRATOS_TRY

    for (auto &i_node : rModelPart.Nodes())
      if (i_node.Or({TO_ERASE,NEW_ENTITY})) //not possible at this stage
        std::cout << " WARNING 1: detected node TO_ERASE or NEW_ENTITY " << i_node.Id() << " " << i_node.Coordinates() << std::endl;

    //1.- Check critical elements with rigid walls first
    this->RemoveCriticalElementNodes(rModelPart,inside_nodes_removed,boundary_nodes_removed); //pfem fluid

    const unsigned int dimension = rModelPart.ElementsBegin()->GetGeometry().WorkingSpaceDimension();

    //2.- Check node neighbours
    bool any_node_removed = false;

    //create search tree
    KdtreeType::Pointer SearchTree;
    std::vector<NodeType::Pointer> list_of_nodes;
    ModelPartUtilities::CreateSearchTree(rModelPart, SearchTree, list_of_nodes, 20);

    //all of the nodes in this list will be preserved
    unsigned int num_neighbours = 20;
    std::vector<NodeType::Pointer> neighbours(num_neighbours);
    std::vector<double> neighbour_distances(num_neighbours);

    //radius means the distance, if the distance between two nodes is closer to radius -> mark for removing
    unsigned int n_points_in_radius;

    //***SIZES :::: parameters do define the tolerance in mesh size:
    double size_for_distance_inside = 0;
    double size_for_distance_boundary_squared =0;

    for (auto &i_node : rModelPart.Nodes())
    {
      if (i_node.Or({TO_ERASE,NEW_ENTITY})) //not possible at this stage
      {
        any_node_removed = true;
        std::cout << " WARNING 2: detected node TO_ERASE or NEW_ENTITY " << i_node.Id() << " " << i_node.Coordinates() << std::endl;
      }

      if (i_node.IsNot(BLOCKED))
      {
        MesherData MeshData;
        RefineData& Refine = MeshData.GetRefineData(i_node, mrRemesh, rModelPart.GetProcessInfo());

        //***SIZES :::: parameters do define the tolerance in mesh size:
        size_for_distance_inside =  Refine.Interior.DistanceMeshSize; //compared with element radius
        size_for_distance_boundary_squared = Refine.Interior.DistanceMeshSize * Refine.Interior.DistanceMeshSize; //compared with element radius

        this->CustomizeMeshSizes(i_node, size_for_distance_inside, size_for_distance_boundary_squared);

        n_points_in_radius = SearchTree->SearchInRadius(i_node, size_for_distance_inside, neighbours.begin(), neighbour_distances.begin(), num_neighbours);

        if (n_points_in_radius > 1)
        {
          if (i_node.IsNot(BOUNDARY)) // Inside nodes
          {
            if (Refine.Interior.Options.Is(RefineData::COARSEN_ON_DISTANCE))
            {
              if (!this->CheckEngagedNode(i_node, neighbours, neighbour_distances, n_points_in_radius))
              { //we release the node if no other nodes neighbours are being erased

                std::map<std::string, unsigned int> FlagsMap {{"FREE_SURFACE", 0}, {"INTERACTION", 0}};
                SetFlagsUtilities::CountNeighbourNodesFlags(i_node, FlagsMap);

                // if the node is close to the surface, do not erase it, move to a mean (laplacian) position
                if (FlagsMap.at("FREE_SURFACE")>=dimension && FlagsMap.at("INTERACTION") == 0){

                  KinematicUtilities::MoveNodeAndSmoothKinematics(i_node);
                  i_node.Set(MODIFIED);
                }
                else{
                  i_node.Set(TO_ERASE);
                  any_node_removed = true;
                  ++inside_nodes_removed;
                }
              }
            }
          }
          else    // Boundary nodes
          {
            // std::cout<<"  Remove close boundary nodes: Candidate ["<<i_node.Id()<<"]"<<std::endl;
            bool engaged_node = false;
            unsigned int counter = 0;
            for (std::vector<NodeType::Pointer>::iterator nn = neighbours.begin(); nn != neighbours.begin() + n_points_in_radius; ++nn)
            {
              if ((*nn)->Is(BOUNDARY) && (*nn)->Or({TO_ERASE,BLOCKED}))
              {
                if ((neighbour_distances[counter] < 4.0 * size_for_distance_boundary_squared))
                {
		  engaged_node = true;
		  break;
		}
              }
              ++counter;
            }

            if (!engaged_node)
            { //Can be inserted in the boundary refine
              i_node.Set(TO_ERASE);
              ++boundary_nodes_removed;
              //std::cout<<"   Distance Criterion Boundary Node ["<<i_node.Id()<<"] TO_ERASE in remove_fluid_nodes_mesher_process"<<i_node.Coordinates()<<std::endl;

            }
          }
        }
      }
    }

    if (boundary_nodes_removed > 0)
      this->MoveBoundaries(rModelPart, boundary_nodes_removed);

    if (boundary_nodes_removed > 0)
      any_node_removed = true;

    bool critical_nodes_removed = false;
    // critical_nodes_removed = this->MoveCriticalNodes(rModelPart,inside_nodes_removed);
    critical_nodes_removed = this->MoveInteractionNodes(rModelPart, inside_nodes_removed);

    if (any_node_removed || critical_nodes_removed)
      any_node_removed = true;

    //Build boundary after removing boundary nodes due distance criterion
    // if (this->mEchoLevel >= 0)
    // {
    // std::cout << "   Fluid: boundary_nodes_removed " << boundary_nodes_removed << std::endl;
    // std::cout << "   Fluid: inside_nodes_removed " << inside_nodes_removed << std::endl;
    // std::cout << "   Fluid: critical_nodes_removed " << critical_nodes_removed << std::endl;
    // }

    //Build boundary after removing boundary nodes due distance criterion // not it fluids
    // if(boundary_nodes_removed){
    //   std::cout<<"     Rebuild boundary needed after release on Distance "<<std::endl;
    //   //any_condition_removed = this->RebuildBoundary(rModelPart);
    // }

    return any_node_removed;

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{WorkingSpaceDimension
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Private Static Member Variables
  ///@{
  ///@}
  ///@name Private Static Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{

  //**************************************************************************
  //**************************************************************************

  void CustomizeMeshSizes(NodeType &rNode,
                          double &size_for_distance_inside,
                          double &size_for_distance_boundary_squared)
  {
    KRATOS_TRY

    size_for_distance_inside *=  2.0; //factor_for_distance_inside = 2.0;
    size_for_distance_boundary_squared *= 3.0; // factor_for_distance_boundary_squared = 3.0;

    unsigned int nnodes = 0;
    unsigned int rigid_nnodes = SetFlagsUtilities::CountNeighbourNodesFlag(rNode, nnodes, {BLOCKED});

    if (rNode.Is(FREE_SURFACE))
    { // it must be more difficult to erase a free_surface node, otherwise, a lot of volume is lost
      if (rigid_nnodes == nnodes)
        size_for_distance_inside *= 0.25;
      else
        size_for_distance_inside *= 0.50;
    }

    KRATOS_CATCH("")
  }
  //**************************************************************************
  //**************************************************************************

  bool RemoveCriticalElementNodes(ModelPart &rModelPart,
                                  unsigned int &inside_nodes_removed,
                                  unsigned int &boundary_nodes_removed)
  {
    KRATOS_TRY

    //const unsigned int dimension = rModelPart.ElementsBegin()->GetGeometry().WorkingSpaceDimension();

    unsigned int erased_nodes = 0;
    for (auto &i_elem : rModelPart.Elements())
    {
      GeometryType& rGeometry = i_elem.GetGeometry();
      //flag counter in the order the flags are supplied
      std::vector<unsigned int> NodeFlagCounter = SetFlagsUtilities::CountNodesFlags(rGeometry, {{FREE_SURFACE,RIGID.AsFalse(),SOLID.AsFalse()},{BLOCKED}});

      if (NodeFlagCounter[0] == rGeometry.size()){
        MesherData MeshData;
        RefineData& Refine = MeshData.GetRefineData(rGeometry[0], mrRemesh, rModelPart.GetProcessInfo());
        const double& rSize = Refine.Interior.DistanceMeshSize;
        if (GeometryUtilities::CalculateMinEdgeSize(rGeometry) < rSize)
	  SetFlagsUtilities::SetFlagsToNodes(rGeometry,{TO_ERASE,BLOCKED});
      }

      // if (dimension == 2)
      // {
      //   if (rigid_nodes > 1)
      //     this->EraseCriticalNodes2D(i_elem.GetGeometry(), erased_nodes, inside_nodes_removed);
      // }
      // else if (dimension == 3)
      // {
      //   if (rigid_nodes > 1)
      //     this->EraseCriticalNodes3D(i_elem.GetGeometry(), erased_nodes, inside_nodes_removed, rigid_nodes);
      // }

    }

    //Check surfing nodes and set them TO_ERASE
    for (auto &i_node : rModelPart.Nodes())
    {
      if (i_node.Is(FREE_SURFACE) && i_node.IsNot(STRUCTURE) && i_node.FastGetSolutionStepValue(PRESSURE) > 100){
	  double velocity = norm_2(i_node.FastGetSolutionStepValue(VELOCITY));
	  NodeWeakPtrVectorType &nNodes = i_node.GetValue(NEIGHBOUR_NODES);
	  for (auto &i_nnode : nNodes){
	    if (i_nnode.Is(FREE_SURFACE) && i_nnode.IsNot(STRUCTURE) && i_nnode.IsNot(INTERACTION)){
	      if(velocity>10*norm_2(i_nnode.FastGetSolutionStepValue(VELOCITY))){
		i_node.Set(TO_ERASE);
		i_node.Set(BLOCKED);
		break;
	      }
	    }
	  }
      }

    }


    return erased_nodes;

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  void MoveBoundaries(ModelPart &rModelPart, unsigned int &boundary_nodes_removed)
  {

    KRATOS_TRY

    double smooth_factor = 1.0;
    std::vector<Flags> PositionFlags = {FREE_SURFACE};
    std::vector<Flags> VariableFlags = {FREE_SURFACE, STRUCTURE.AsFalse()};

    for (auto &i_cond : rModelPart.Conditions())
    {
      GeometryType &cGeometry = i_cond.GetGeometry();

      unsigned int id = 0, counter = 0, sliver = 0, rigid = 0, interaction = 0, free_surface = 0, total_inside_neighbours = 0;

      unsigned int size = cGeometry.size();

      for (unsigned int i=0; i<size; ++i)
      {
	NodeType& i_node = cGeometry[i];
        if (i_node.Is(TO_ERASE))
        {
          id = i;
          ++counter;
        }

        if (i_node.Is(INTERACTION))
          ++interaction;

        if (i_node.Is(RIGID))
          ++rigid;

        if (i_node.Is(MARKER))
          ++sliver;

	unsigned int nnodes = 0;
	sliver += SetFlagsUtilities::CountNeighbourNodesFlag(i_node, nnodes, {MARKER});

	//to detect flying patches and not modify them
	NodeWeakPtrVectorType &nNodes = i_node.GetValue(NEIGHBOUR_NODES);
	for (auto &i_nnode : nNodes)
	  if (i_nnode.IsNot(FREE_SURFACE)){
	    free_surface += SetFlagsUtilities::CountNeighbourNodesFlag(i_nnode, nnodes, {FREE_SURFACE});
	    total_inside_neighbours++;
	  }
      }

      const unsigned int dimension = cGeometry.WorkingSpaceDimension();

      if (counter == 1 && sliver == 0 && interaction < size-1 && rigid < size-1 && free_surface <= total_inside_neighbours * dimension)
      {
        if (cGeometry[id].IsNot(BLOCKED))
        {
          if (KinematicUtilities::MoveBoundaryNodeAndSmoothKinematics(cGeometry[id], PositionFlags, VariableFlags, smooth_factor))
          {
            cGeometry[id].Set(MODIFIED);
            cGeometry[id].Set(TO_ERASE, false);
            --boundary_nodes_removed;
            std::cout<<" Boundary Node ["<<cGeometry[id].Id()<<"] Moved "<<std::endl;
          }
          else
          {
            std::cout<<" Boundary Node ["<<cGeometry[id].Id()<<"] Erased "<<std::endl;
          }
        }
      }
    }

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  bool MoveInteractionNodes(ModelPart &rModelPart, unsigned int &inside_nodes_removed)
  {
    KRATOS_TRY

    bool any_node_removed = false;
    unsigned int erased_nodes = 0;
    unsigned int removed_nodes = 0;

    const int nnodes = rModelPart.NumberOfNodes();

    ModelPart::NodesContainerType::iterator it_begin = rModelPart.NodesBegin();

#pragma omp parallel for reduction(+ : removed_nodes, erased_nodes)
    for (int i = 0; i < nnodes; ++i)
    {
      ModelPart::NodesContainerType::iterator i_node = it_begin + i;

      if (i_node->Is(INTERACTION) && i_node->AndNot({TO_ERASE,BLOCKED}))
      {
        array_1d<double, 3> Direction;
        double distance = 0;

        NodeWeakPtrVectorType &nNodes = i_node->GetValue(NEIGHBOUR_NODES);

        MesherData MeshData;
        RefineData& Refine = MeshData.GetRefineData(*i_node, mrRemesh, rModelPart.GetProcessInfo());
        const double& rSize = 0.5 * Refine.Interior.DistanceMeshSize;

        bool erased = false;
        for (auto &i_nnode : nNodes)
        {
          if (erased)
            break;

          if (i_nnode.Or({RIGID,SOLID}))
          {
            if (KinematicUtilities::CheckApproachingPoint(*i_node, i_nnode))
            {
              distance = GeometryUtilities::GetDistanceToNode(*i_node, i_nnode, Direction);
              //solid boundary normals point inside
              if (i_nnode.Is(SOLID))
                Direction*=(-1);

              if (distance < 0.25 * rSize)
              {
                i_node->Set(TO_ERASE);
                std::cout<<"   Remove INTERACTION nodes:  Node ["<<i_node->Id()<<"] TO_ERASE in remove_fluid_nodes_mesher_process"<<i_node->Coordinates()<<std::endl;
                ++erased_nodes;
                erased = true;
                if (i_node->Is(RIGID))
                  KRATOS_ERROR << "RIGID NODE REMOVED interaction nodes "<<std::endl;
              }
              else
              {
                if (distance > 0 && distance < 3.0 * rSize)
                {
                  if( i_node->Is(FREE_SURFACE) )
                  {
                    if (distance < rSize)
                    {
                      distance = (rSize - distance);
                      KinematicUtilities::MoveAndProjectMeshVelocity(*i_node, Direction, distance);
                      i_node->Set(MODIFIED);
                    }
                    else{
		      distance = (3.2 * rSize - distance);
		      KinematicUtilities::MoveNode(*i_node, Direction, distance);
                      //KinematicUtilities::ProjectMeshVelocity(*i_node, Direction);
                      i_node->Set(MODIFIED);
                    }
                  }
                  else{
                    distance = (3.2 * rSize - distance);
                    KinematicUtilities::MoveAndProjectMeshVelocity(*i_node, Direction, distance);
                    i_node->Set(MODIFIED);
                  }
                }
                else  //do nothing please for free surface nodes stuck at walls
                {
                  if( i_node->IsNot(FREE_SURFACE) ){
                    KinematicUtilities::ProjectMeshVelocity(*i_node, Direction);
                    i_node->Set(MODIFIED);
                  }
                  else if (distance > 0 && distance < 3.0 * rSize)
                  {
                    distance = (3.2 * rSize - distance);
                    KinematicUtilities::MoveNode(*i_node, Direction, distance);
                    i_node->Set(MODIFIED);
                  }

                }
              }
            }
            else
            {
              if (i_node->Is(FREE_SURFACE))
              {
                //this option corrects an unusual situation
                distance = GeometryUtilities::GetDistanceToNode(*i_node, i_nnode, Direction);
                //solid boundary normals point inside
                if (i_nnode.Is(SOLID))
                  Direction*=(-1);

                if (distance < 0.15 * rSize)
                {
                  i_node->Set(TO_ERASE);
                  std::cout<<"   Remove CRITICAL Boundary nodes:  Node ["<<i_node->Id()<<"] TO_ERASE in remove_fluid_nodes_mesher_process "<<i_node->Coordinates()<<std::endl;
                  ++erased_nodes;
                  ++removed_nodes;
                  erased = true;
                  if (i_node->Is(RIGID))
                    KRATOS_ERROR << "RIGID NODE REMOVED free surface "<<std::endl;
                }
                else if (distance > 0 && distance < 3.0 * rSize)  //do nothing please
                {
                  std::cout<<"   Move CRITICAL Boundary nodes:  Node ["<<i_node->Id()<<"]  in remove_fluid_nodes_mesher_process "<<i_node->Coordinates()<<std::endl;
                  distance = (3.2 * rSize - distance);
                  KinematicUtilities::MoveNode(*i_node, Direction, distance);
		  i_node->Set(MODIFIED);
                }
              }
            }
          }
        }
      }
    }

    //}

    if (erased_nodes > 0)
    {
      std::cout<<" Layer Nodes removed "<<erased_nodes<<" unusually removed "<<removed_nodes<<std::endl;
      any_node_removed = true;
      inside_nodes_removed += removed_nodes;
    }

    return any_node_removed;

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  bool CheckEngagedNode(Node<3> &rNode, std::vector<Node<3>::Pointer> &rNeighbours, std::vector<double> &rNeighbourDistances, unsigned int &rn_points_in_radius)
  {
    KRATOS_TRY

    //look if we are already erasing any of the other nodes
    bool engaged_node = false;

    //trying to remove the node in the shorter distance anyway
    double min_distance = std::numeric_limits<double>::max();
    unsigned int counter = 0;
    unsigned int closest_node = 0;

    //get closest node
    for (std::vector<double>::iterator nd = rNeighbourDistances.begin(); nd != rNeighbourDistances.begin() + rn_points_in_radius; ++nd)
    {
      if ((*nd) < min_distance && (*nd) > 0.0)
      {
        min_distance = (*nd);
        closest_node = counter;
      }
      ++counter;
    }

    //check if this node is already TO_ERASE
    if (rNeighbours[closest_node]->IsNot(BOUNDARY) && rNeighbours[closest_node]->Is(TO_ERASE))
    {
      engaged_node = true;
    }
    else
    {
      counter = 0;
      unsigned int erased_node = 0;
      for (std::vector<Node<3>::Pointer>::iterator i_nnode = rNeighbours.begin(); i_nnode != rNeighbours.begin() + rn_points_in_radius; ++i_nnode)
      {
        if ((*i_nnode)->IsNot(BOUNDARY) && (*i_nnode)->Is(TO_ERASE))
        {
          erased_node = counter;
          engaged_node = true;
          break;
        }
        ++counter;
      }
      if (engaged_node)
      {
        // if the distance is 5 times smaller remove anyway
        if (rNeighbourDistances[closest_node] * 5 < rNeighbourDistances[erased_node])
        {
          engaged_node = false;
          //std::cout<<"     Distance Criterion Node ["<<i_node.Id()<<"] TO_ERASE "<<closest_node<<" "<<erased_node<<std::endl;
        }
      }
    }

    return engaged_node;

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{

  /// Assignment operator.
  RemoveFluidNodesMesherProcess &operator=(RemoveFluidNodesMesherProcess const &rOther);

  ///@}

}; // Class Process

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                RemoveFluidNodesMesherProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const RemoveFluidNodesMesherProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_REMOVE_FLUID_NODES_MESHER_PROCESS_HPP_INCLUDED  defined
