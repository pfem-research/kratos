//
//   Project Name:        KratosPfemApplication    $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:       August 2018 $
//
//

#if !defined(KRATOS_RECOVER_VOLUME_LOSSES_PROCESS_HPP_INCLUDED)
#define KRATOS_RECOVER_VOLUME_LOSSES_PROCESS_HPP_INCLUDED

// External includes

// System includes

// Project includes
#include "custom_utilities/mesher_utilities.hpp"
#include "custom_utilities/model_part_utilities.hpp"
#include "custom_utilities/set_flags_utilities.hpp"
#include "processes/process.h"

namespace Kratos
{

///@name Kratos Classes
///@{

/// Move free surface to restore volume losses
/** The process moves free surface nodes artificially in order to recover the loss of volume
 */

class RecoverVolumeLossesProcess : public Process
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of Process
  KRATOS_CLASS_POINTER_DEFINITION(RecoverVolumeLossesProcess);

  typedef ModelPart::NodeType NodeType;
  typedef ModelPart::ConditionType ConditionType;
  typedef ModelPart::PropertiesType PropertiesType;
  typedef ConditionType::GeometryType GeometryType;

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  RecoverVolumeLossesProcess(ModelPart &rModelPart,
                             Parameters rParameters)
      : Process(Flags()), mrModelPart(rModelPart)
  {

    Parameters default_parameters(R"(
        {
            "solving_loss" : true,
            "meshing_loss" : false
        }  )");

    // Validate against defaults -- this ensures no type mismatch
    rParameters.ValidateAndAssignDefaults(default_parameters);

    mComputingLossRecover = rParameters["solving_loss"].GetBool();
    mMeshingLossRecover = rParameters["meshing_loss"].GetBool();

    this->SetVisitedElements(mrModelPart);
    mInitialVolume = this->ComputeVolume(mrModelPart);
    this->ResetVisitedElements(mrModelPart);

    mTotalVolume = 0;

    mStepComputingVolumeLoss = 0;
    mStepMeshingVolumeLoss = 0;

    mTotalComputingVolumeLoss = 0;
    mTotalMeshingVolumeLoss = 0;

    mStepComputingVolumeRecover = 0;
    mStepMeshingVolumeRecover = 0;

    mTotalComputingVolumeRecover = 0;
    mTotalMeshingVolumeRecover = 0;
  }

  /// Destructor.
  virtual ~RecoverVolumeLossesProcess() {}

  ///@}
  ///@name Operators
  ///@{

  /// This operator is provided to call the process as a function and simply calls the Execute method.
  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  /// Execute method is used to execute the AssignPropertiesToNodesProcess algorithms.
  void Execute() override
  {
  }

  /// this function is designed for being called at the beginning of the computations
  /// right after reading the model and the groups
  void ExecuteInitialize() override
  {
  }

  /// this function is designed for being execute once before the solution loop but after all of the
  /// solvers where built
  void ExecuteBeforeSolutionLoop() override
  {
    this->SetVisitedElements(mrModelPart);
    mTotalVolume = this->ComputeVolume(mrModelPart);
    this->ResetVisitedElements(mrModelPart);
  }

  /// this function will be executed at every time step BEFORE performing the solve phase
  void ExecuteInitializeSolutionStep() override
  {
    this->SetVisitedElements(mrModelPart);

    double ReferenceVolume = mTotalVolume;
    double CurrentVolume = this->ComputeVolume(mrModelPart);
    mStepMeshingVolumeLoss = CurrentVolume-ReferenceVolume;
    mTotalMeshingVolumeLoss += mStepMeshingVolumeLoss;

    if (mMeshingLossRecover)
    {
      //recover volume losses due meshing : option1
      //mTotalVolume = this->RecoverVolume(mrModelPart,ReferenceVolume);

      //restore volume losses due meshing : option2
      mTotalVolume = this->RestoreVolume(mrModelPart,ReferenceVolume);
    }
    else
    {
      //recover volume losses due computation only
      mTotalVolume = this->ComputeVolume(mrModelPart);
    }

    mStepMeshingVolumeRecover = mTotalVolume-CurrentVolume;
    mTotalMeshingVolumeRecover += mStepMeshingVolumeRecover;

    this->ResetVisitedElements(mrModelPart);
  }

  /// this function will be executed at every time step AFTER performing the solve phase
  void ExecuteFinalizeSolutionStep() override
  {
    this->SetVisitedElements(mrModelPart);

    double ReferenceVolume = mTotalVolume;
    double CurrentVolume = this->ComputeVolume(mrModelPart);
    mStepComputingVolumeLoss = CurrentVolume-ReferenceVolume;
    mTotalComputingVolumeLoss += mStepComputingVolumeLoss;

    if (mComputingLossRecover)
    {
      //recover volume losses due computation : option1
      //mTotalVolume = this->RecoverVolume(mrModelPart,ReferenceVolume);
      //recover volume losses due computation : option2
      mTotalVolume = this->RestoreVolume(mrModelPart,ReferenceVolume);
    }
    else
    {
      //recover volume losses due meshing only
      mTotalVolume = this->ComputeVolume(mrModelPart);
    }

    mStepComputingVolumeRecover = mTotalVolume-CurrentVolume;
    mTotalComputingVolumeRecover += mStepComputingVolumeRecover;

    this->ResetVisitedElements(mrModelPart);
  }

  /// this function will be executed at every time step BEFORE  writing the output
  void ExecuteBeforeOutputStep() override
  {
  }

  /// this function will be executed at every time step AFTER writing the output
  void ExecuteAfterOutputStep() override
  {
    this->PrintStats(0);
  }

  /// this function is designed for being called at the end of the computations
  /// right after reading the model and the groups
  void ExecuteFinalize() override
  {
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "RecoverVolumeLossesProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "RecoverVolumeLossesProcess";
  }

  /// Print object's data.s
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{
  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Static Member Variables
  ///@{
  ///@}
  ///@name Member Variables
  ///@{

  ModelPart &mrModelPart;

  double mInitialVolume;

  double mTotalVolume;
  double mTotalComputingVolumeLoss;
  double mTotalMeshingVolumeLoss;

  double mStepComputingVolumeLoss;
  double mStepMeshingVolumeLoss;

  double mTotalComputingVolumeRecover;
  double mTotalMeshingVolumeRecover;

  double mStepComputingVolumeRecover;
  double mStepMeshingVolumeRecover;

  bool mComputingLossRecover;
  bool mMeshingLossRecover;

  struct ShapingVariables
  {

    double CurrentVolume;
    double TotalVolumeLoss;
    double CurrentSurface;
    double OffsetFactor;

    std::vector<unsigned int> FreeSurfaceNodesIds;
    std::vector<double> NodalSurface;
    std::vector<double> NodalVolume;
  };

  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{


  //*******************************************************************************************
  //*******************************************************************************************

  void PrintStats(int echo_level = 0)
  {
    if (echo_level >= 0){
      if (mInitialVolume-mTotalVolume>0)
        std::cout<<"  Volume (initial: "<<mInitialVolume<<" / current: "<<mTotalVolume<<" / loss: "<<mInitialVolume-mTotalVolume<<")"<<std::endl;
      else
        std::cout<<"  Volume (initial: "<<mInitialVolume<<" / current: "<<mTotalVolume<<" / gain: "<<fabs(mInitialVolume-mTotalVolume)<<")"<<std::endl;
    }

    if (echo_level >= 1){
      std::cout<<"  Step Loss     (meshing: "<<mStepMeshingVolumeLoss<<" / computing: "<<mStepComputingVolumeLoss<<" / sum: "<<mStepMeshingVolumeLoss+mStepComputingVolumeLoss<<")"<<std::endl;
      std::cout<<"  Total Loss    (meshing: "<<mTotalMeshingVolumeLoss<<" / computing: "<<mTotalComputingVolumeLoss<<" / sum: "<<mTotalMeshingVolumeLoss+mTotalComputingVolumeLoss<<")"<<std::endl;
      std::cout<<"  Step Recover  (meshing: "<<mStepMeshingVolumeRecover<<" / computing: "<<mStepComputingVolumeRecover<<" / sum: "<<mStepMeshingVolumeRecover+mStepComputingVolumeRecover<<")"<<std::endl;
      std::cout<<"  Total Recover (meshing: "<<mTotalMeshingVolumeRecover<<" / computing: "<<mTotalComputingVolumeRecover<<" / sum: "<<mTotalMeshingVolumeRecover+mTotalComputingVolumeRecover<<")"<<std::endl;

    }
    if (echo_level >= 2){
      std::cout<<"  Step Error    (meshing: "<<mStepMeshingVolumeLoss+mStepMeshingVolumeRecover<<" / computing: "<<mStepComputingVolumeLoss+mStepComputingVolumeRecover<<" / sum: "<<mStepMeshingVolumeLoss+mStepComputingVolumeLoss+mStepMeshingVolumeRecover+mStepComputingVolumeRecover<<")"<<std::endl;
      std::cout<<"  Total Error   (meshing: "<<mTotalMeshingVolumeRecover+mTotalMeshingVolumeLoss<<" / computing: "<<mTotalComputingVolumeRecover+mTotalComputingVolumeLoss<<" / sum: "<<mTotalMeshingVolumeLoss+mTotalComputingVolumeLoss+mTotalMeshingVolumeRecover+mTotalComputingVolumeRecover<<")"<<std::endl;
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  double RestoreVolume(ModelPart &rModelPart, const double &ReferenceTotalVolume)
  {

    KRATOS_TRY

    double Tolerance = 1e-10;
    unsigned int NumberOfIterations = 5;
    int iteration = -1;

    ShapingVariables Variables;

    unsigned int MaxNodeId = ModelPartUtilities::GetMaxNodeId(rModelPart);
    Variables.FreeSurfaceNodesIds.resize(MaxNodeId + 1);
    std::fill(Variables.FreeSurfaceNodesIds.begin(), Variables.FreeSurfaceNodesIds.end(), 0);

    unsigned int id = 1;
    for (auto &i_node : rModelPart.Nodes())
    {
      if (i_node.Is(FREE_SURFACE))
      {
        Variables.FreeSurfaceNodesIds[i_node.Id()] = id;
      }
      ++id;
    }

    Variables.NodalSurface.resize(id);
    std::fill(Variables.NodalSurface.begin(), Variables.NodalSurface.end(), 0);

    Variables.NodalVolume.resize(id);
    std::fill(Variables.NodalVolume.begin(), Variables.NodalVolume.end(), 0);

    Variables.TotalVolumeLoss = ReferenceTotalVolume - this->ComputeVolume(rModelPart);

    Variables.CurrentVolume = this->ComputeFreeSurfaceVolume(rModelPart, Variables);
    Variables.CurrentSurface = this->ComputeFreeSurfaceArea(rModelPart, Variables);

    double recovery_factor = 0.95;
    double VolumeIncrement = Variables.TotalVolumeLoss * recovery_factor;

    //initial prediction of the offset
    Variables.OffsetFactor = 0.1 * VolumeIncrement / Variables.CurrentSurface;

    double Ratio = fabs(Variables.CurrentVolume - VolumeIncrement / (Variables.CurrentVolume - Variables.TotalVolumeLoss));

    double Error = fabs(Variables.TotalVolumeLoss);

    //std::cout<<" Volume Increment Start "<<VolumeIncrement<<std::endl;

    while (++iteration < NumberOfIterations && (Ratio < 0.9 || Ratio > 1) && Error > Tolerance)
    {

      this->MoveFreeSurface(rModelPart, Variables);

      double UpdatedVolume = this->ComputeFreeSurfaceVolume(rModelPart);
      VolumeIncrement = Variables.CurrentVolume - UpdatedVolume;

      // double UpdatedVolume = this->ComputeVolume(rModelPart);
      // VolumeIncrement = ReferenceTotalVolume-CurrentVolume;

      //std::cout<<" Volume Increment Moved "<<VolumeIncrement<<" Offset factor "<<Variables.OffsetFactor<<std::endl;

      if (VolumeIncrement != 0)
      {
        if (Variables.TotalVolumeLoss / VolumeIncrement > 0)
          Variables.OffsetFactor *= (1.0 - Variables.TotalVolumeLoss / VolumeIncrement);
        else
          Variables.OffsetFactor *= (1.0 + Variables.TotalVolumeLoss / VolumeIncrement);
      }

      //std::cout<<" Offset factor "<<Variables.OffsetFactor<<" prod "<<(Variables.TotalVolumeLoss/VolumeIncrement)<<std::endl;

      if ((VolumeIncrement - Variables.TotalVolumeLoss) * Variables.OffsetFactor > 0)
      {
        Variables.OffsetFactor *= (-1);
        //std::cout<<" Change offset sign "<<std::endl;
      }

      Ratio = fabs(Variables.CurrentVolume - VolumeIncrement / (Variables.CurrentVolume - Variables.TotalVolumeLoss));

      Error = fabs(Variables.TotalVolumeLoss - VolumeIncrement);

      //std::cout<<" Iteration: "<<iteration<<" Error in Volume "<<Variables.TotalVolumeLoss-VolumeIncrement<<" OffsetFactor "<<Variables.OffsetFactor<<" Ratio "<<Ratio<<std::endl;
    }

    // try to move first layer under free surface for a best mesh node distribution
    //this->MoveLayerNodes(rModelPart);

    double CurrentVolume = this->ComputeVolume(rModelPart);

    //std::cout<<"  Recover Volume Losses perfomed in "<<iteration<<" iterations : Error "<<Error<<std::endl;
    //std::cout<<"  Recover Volume Losses :: CurrentVolume: "<<CurrentVolume<<" PreviousVolume: "<<mTotalVolume<<std::endl;

    return CurrentVolume;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void MoveFreeSurface(ModelPart &rModelPart, ShapingVariables &rVariables)
  {
    KRATOS_TRY

    ModelPart::NodesContainerType::iterator it_begin = rModelPart.NodesBegin();
    int NumberOfNodes = rModelPart.NumberOfNodes();

#pragma omp parallel for
    for (int i = 0; i < NumberOfNodes; ++i)
    {
      ModelPart::NodesContainerType::iterator i_node = it_begin + i;

      // bool clasical = (i_node->Is(FREE_SURFACE) && (i_node->IsNot(RIGID) && i_node->IsNot(SOLID)) && i_node->IsNot(INTERACTION) && i_node->IsNot(VISITED) );
      // bool compacted = (i_node->Is(FREE_SURFACE) && i_node->IsNot({RIGID, SOLID, INTERACTION, VISITED});

      // if( clasical != compacted )
      //   std::cout<<" something is wrong in CheckFLAGS "<<std::endl;

      if (i_node->Is(FREE_SURFACE) && i_node->IsNot({RIGID, SOLID, INTERACTION, VISITED}))
      {
        unsigned int id = rVariables.FreeSurfaceNodesIds[i_node->Id()];
        const array_1d<double, 3> &rNormal = i_node->FastGetSolutionStepValue(NORMAL);
        double &rShrinkFactor = i_node->FastGetSolutionStepValue(SHRINK_FACTOR);
        double rOffset = rVariables.OffsetFactor * rShrinkFactor * rVariables.NodalSurface[id] / rVariables.CurrentSurface;
        // if(norm_2(i_node->FastGetSolutionStepValue(DISPLACEMENT)-i_node->FastGetSolutionStepValue(DISPLACEMENT,1)) < rOffset)
        //   std::cout<<" Offset out of bonds :: shrink "<<rShrinkFactor<<" area "<<rVariables.NodalSurface[id]<<" total_area "<<rVariables.CurrentSurface<<std::endl;

        // i_node->Coordinates() += rOffset * rNormal;
        // i_node->FastGetSolutionStepValue(DISPLACEMENT) += rOffset * rNormal;
        // i_node->FastGetSolutionStepValue(DISPLACEMENT,1) += rOffset * rNormal;

        // to not modify displacement
        i_node->Coordinates() += rOffset * rNormal;
        i_node->GetInitialPosition() += rOffset * rNormal;
      }
    }

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void MoveLayerNodes(ModelPart &rModelPart)
  {
    KRATOS_TRY

    //set flags for the local process execution
    SetFlagsUtilities::SetFlagsToNodes(rModelPart, {{BLOCKED}}, {BLOCKED.AsFalse()});


    ModelPart::NodesContainerType::iterator it_begin = rModelPart.NodesBegin();
    int NumberOfNodes = rModelPart.NumberOfNodes();
    double smooth_factor = 0.20;
#pragma omp parallel for
    for (int i = 0; i < NumberOfNodes; ++i)
    {
      ModelPart::NodesContainerType::iterator i_node = it_begin + i;
      if (i_node->Is(FREE_SURFACE) && i_node->OrNot({RIGID,SOLID}))
      {
        NodeWeakPtrVectorType &nNodes = i_node->GetValue(NEIGHBOUR_NODES);
        for (auto &i_nnode: nNodes)
        {
          if(i_nnode.IsNot({BLOCKED, FREE_SURFACE, RIGID, INSIDE}))
            if (this->MoveLayerNode(i_nnode, smooth_factor))
              i_nnode.Set(BLOCKED, true);
        }
      }
    }

    //set flags for the local process execution
    SetFlagsUtilities::SetFlagsToNodes(rModelPart, {{BLOCKED}}, {BLOCKED.AsFalse()});

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool MoveLayerNode(Node<3> &rNode, const double &rsmooth_factor)
  {

    KRATOS_TRY

    if (rsmooth_factor == 0.0)
      return true;

    bool moved_node = false;
    NodeWeakPtrVectorType &nNodes = rNode.GetValue(NEIGHBOUR_NODES);

    bool mesh_movement = false;
    if (rNode.SolutionStepsDataHas(MESH_DISPLACEMENT))
    {
      mesh_movement = true;
    }

    double Distance = 0;
    const unsigned int size = nNodes.size();

    if (size == 2)
    {
      array_1d<double, 3> MidPoint = 0.5 * (nNodes.front().Coordinates() + nNodes.back().Coordinates());
      array_1d<double, 3> Direction = (nNodes.front().Coordinates() - nNodes.back().Coordinates());

      if (norm_2(Direction))
        Direction /= norm_2(Direction);

      array_1d<double, 3> Displacement = inner_prod((MidPoint - rNode.Coordinates()), Direction) * Direction * rsmooth_factor;
      noalias(rNode.Coordinates()) += Displacement;
      noalias(rNode.GetInitialPosition()) += Displacement;

      Distance = norm_2(Displacement);

      //std::cout<<" Layer 2 Move Post ["<<rNode.Id()<<"] "<<rNode.Coordinates()<<" Displacement "<<Displacement<<std::endl;
      moved_node = true;
    }
    else if (size > 2)
    {
      array_1d<double, 3> MidPoint;
      noalias(MidPoint) = ZeroVector(3);
      double quotient = 1.0 / double(size);

      for (auto &i_nnode : nNodes)
      {
        MidPoint += i_nnode.Coordinates();
      }

      MidPoint *= quotient;

      array_1d<double, 3> Displacement = (MidPoint - rNode.Coordinates());
      Displacement *= rsmooth_factor;

      noalias(rNode.Coordinates()) += Displacement;
      noalias(rNode.GetInitialPosition()) += Displacement;

      Distance = norm_2(Displacement);

      //std::cout<<" Layer X Move Post ["<<rNode.Id()<<"] "<<rNode.Coordinates()<<" Displacement "<<Displacement<<std::endl;
      moved_node = true;
    }
    else
    {
      std::cout << " Layer nodes with only one neighbour " << std::endl;
    }

    if (moved_node)
    {
      if (Distance != 0)
      {
        //give weight to current value of fluid variables
        double weight = 1.0 / Distance;
        MesherUtilities::MultiplyFluidVariables(rNode, weight);
        MesherUtilities::MultiplyFluidVariables(rNode, weight, 1);
        // rNode.FastGetSolutionStepValue(PRESSURE_VELOCITY) *= weight;
        // rNode.FastGetSolutionStepValue(PRESSURE_VELOCITY,1) *= weight;

        if (mesh_movement)
        {
          MesherUtilities::MultiplyFluidMeshVariables(rNode, weight);
          MesherUtilities::MultiplyFluidMeshVariables(rNode, weight, 1);
        }
        double total_weight = weight;
        for (auto &i_fnnode : nNodes)
        {
          weight = norm_2(rNode.Coordinates() - i_fnnode.Coordinates());
          if (weight != 0)
            weight = 1.0 / weight;

          MesherUtilities::SmoothFluidVariables(rNode, i_fnnode, weight);
          MesherUtilities::SmoothFluidVariables(rNode, i_fnnode, weight, 1);

          if (mesh_movement)
          {
            MesherUtilities::SmoothFluidMeshVariables(rNode, i_fnnode, weight);
            MesherUtilities::SmoothFluidMeshVariables(rNode, i_fnnode, weight, 1);
          }
          total_weight += weight;
        }

        double quotient = 1.0 / total_weight;
        MesherUtilities::MultiplyFluidVariables(rNode, quotient);
        MesherUtilities::MultiplyFluidVariables(rNode, quotient, 1);

        if (mesh_movement)
        {
          MesherUtilities::MultiplyFluidMeshVariables(rNode, quotient);
          MesherUtilities::MultiplyFluidMeshVariables(rNode, quotient, 1);
        }
      }
    }

    return moved_node;

    KRATOS_CATCH("")
  }


  //*******************************************************************************************
  //*******************************************************************************************

  double RecoverVolume(ModelPart &rModelPart, const double &ReferenceTotalVolume)
  {

    KRATOS_TRY

    double Tolerance = 1e-6;
    unsigned int NumberOfIterations = 5;
    int iteration = -1;

    double CurrentVolume = this->ComputeVolume(rModelPart);

    double Error = fabs(ReferenceTotalVolume - CurrentVolume);

    double FreeSurfaceVolume = this->ComputeFreeSurfaceVolume(rModelPart);
    double FreeSurfaceArea = this->ComputeFreeSurfaceArea(rModelPart);

    double VolumeIncrement = ReferenceTotalVolume - CurrentVolume;

    //initial prediction of the offset
    double Offset = VolumeIncrement / FreeSurfaceArea;

    FreeSurfaceVolume += VolumeIncrement;

    double CurrentFreeSurfaceVolume = 0;

    while (++iteration < NumberOfIterations && Error > Tolerance)
    {

      this->MoveFreeSurface(rModelPart, Offset);

      CurrentFreeSurfaceVolume = this->ComputeFreeSurfaceVolume(rModelPart);

      VolumeIncrement = (FreeSurfaceVolume - CurrentFreeSurfaceVolume);

      Offset = (VolumeIncrement / FreeSurfaceArea);

      Error = fabs(VolumeIncrement);

      //std::cout<<" Iteration: "<<iteration<<" Error in Volume "<<Error<<std::endl;
    }

    CurrentVolume = this->ComputeVolume(rModelPart);

    //std::cout<<"  Recover Volume Losses perfomed in "<<iteration<<" iterations : Error "<<Error<<std::endl;
    //std::cout<<"  Recover Volume Losses :: CurrentVolume: "<<CurrentVolume<<" PreviousVolume: "<<ReferenceTotalVolume<<std::endl;

    return CurrentVolume;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void MoveFreeSurface(ModelPart &rModelPart, const double &rOffset)
  {
    KRATOS_TRY

    ModelPart::NodesContainerType::iterator it_begin = rModelPart.NodesBegin();
    int NumberOfNodes = rModelPart.NumberOfNodes();

#pragma omp parallel for
    for (int i = 0; i < NumberOfNodes; ++i)
    {
      ModelPart::NodesContainerType::iterator i_node = it_begin + i;
      if (i_node->Is(FREE_SURFACE) && i_node->OrNot({RIGID,SOLID}))
      {
        const array_1d<double, 3> &rNormal = i_node->FastGetSolutionStepValue(NORMAL);
        i_node->Coordinates() += rOffset * rNormal;
        i_node->FastGetSolutionStepValue(DISPLACEMENT) += rOffset * rNormal;
        i_node->FastGetSolutionStepValue(DISPLACEMENT, 1) += rOffset * rNormal;
      }
    }

    KRATOS_CATCH("")
  }


  //*******************************************************************************************
  //*******************************************************************************************

  void SetVisitedElements(ModelPart &rModelPart)
  {
    KRATOS_TRY

    for (ModelPart::ElementsContainerType::iterator i_elem = rModelPart.ElementsBegin(); i_elem != rModelPart.ElementsEnd(); ++i_elem)
    {
      i_elem->Set(VISITED, false);
      const GeometryType &eGeometry = i_elem->GetGeometry();
      unsigned int sliver = 0;
      unsigned int wall = 0;
      unsigned int outlet = 0;
      unsigned int counter = 0;
      for (auto &i_node : eGeometry)
      {
        if (i_node.Is(OUTLET))
          ++outlet;

        if (i_node.Is(MARKER))
        {
          i_node.Set(VISITED, true);
          ++sliver;
        }
        if (i_node.And({RIGID,FREE_SURFACE}))
          ++wall;

        ++counter;
      }
      if (sliver == counter || wall == counter - 1)
        i_elem->Set(VISITED, true);
      if(i_elem->Is(BOUNDARY) && outlet==0) //trying to avoid contraction of flying fluid single elements and element layers
        i_elem->Set(VISITED, true);
    }

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void ResetVisitedElements(ModelPart &rModelPart)
  {
    KRATOS_TRY

    for (ModelPart::ElementsContainerType::iterator i_elem = rModelPart.ElementsBegin(); i_elem != rModelPart.ElementsEnd(); ++i_elem)
    {
      if (i_elem->Is(VISITED))
      {
        i_elem->Set(VISITED, false);
        const GeometryType &eGeometry = i_elem->GetGeometry();
        for (auto &i_node : eGeometry)
        {
          if (i_node.Is(VISITED))
            i_node.Set(VISITED, false);
        }
      }
    }

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  double ComputeVolume(ModelPart &rModelPart)
  {
    KRATOS_TRY

    const unsigned int dimension = rModelPart.GetProcessInfo()[SPACE_DIMENSION];
    double ModelPartVolume = 0;

    if (dimension == 2)
    {

      ModelPart::ElementsContainerType::iterator it_begin = rModelPart.ElementsBegin();
      int NumberOfElements = rModelPart.NumberOfElements();

#pragma omp parallel for reduction(+ \
                                   : ModelPartVolume)
      for (int i = 0; i < NumberOfElements; ++i)
      {
        ModelPart::ElementsContainerType::iterator i_elem = it_begin + i;
        if (i_elem->GetGeometry().WorkingSpaceDimension() == 2 && i_elem->Is(FLUID) && i_elem->IsNot(VISITED))
          ModelPartVolume += i_elem->GetGeometry().Area();
      }
    }
    else
    { //dimension == 3

      ModelPart::ElementsContainerType::iterator it_begin = rModelPart.ElementsBegin();
      int NumberOfElements = rModelPart.NumberOfElements();
#pragma omp parallel for reduction(+ \
                                   : ModelPartVolume)
      for (int i = 0; i < NumberOfElements; ++i)
      {
        ModelPart::ElementsContainerType::iterator i_elem = it_begin + i;
        if (i_elem->GetGeometry().WorkingSpaceDimension() == 3 && i_elem->Is(FLUID) && i_elem->IsNot(VISITED))
        {
          ModelPartVolume += i_elem->GetGeometry().Volume();
        }
      }
    }

    return ModelPartVolume;

    KRATOS_CATCH("")
  }
  //*******************************************************************************************
  //*******************************************************************************************

  double ComputeFreeSurfaceVolume(ModelPart &rModelPart, ShapingVariables &rVariables)
  {
    KRATOS_TRY

    const unsigned int dimension = rModelPart.GetProcessInfo()[SPACE_DIMENSION];
    double ModelPartVolume = 0;

    if (dimension == 2)
    {

      ModelPart::ElementsContainerType::iterator it_begin = rModelPart.ElementsBegin();
      int NumberOfElements = rModelPart.NumberOfElements();

      //#pragma omp parallel for reduction(+:ModelPartVolume)
      for (int i = 0; i < NumberOfElements; ++i)
      {
        ModelPart::ElementsContainerType::iterator i_elem = it_begin + i;
        const GeometryType &eGeometry = i_elem->GetGeometry();
        double Area = 0;
        if (eGeometry.WorkingSpaceDimension() == 2 && i_elem->Is(BOUNDARY) && i_elem->Is(FLUID) && i_elem->IsNot(VISITED))
        {
          Area = eGeometry.Area();
          ModelPartVolume += Area;
        }

        Area /= double(eGeometry.size());
        for (auto &i_node : eGeometry)
        {
          if (i_node.Is(FREE_SURFACE))
            rVariables.NodalVolume[rVariables.FreeSurfaceNodesIds[i_node.Id()]] += Area;
        }
      }
    }
    else
    { //dimension == 3

      ModelPart::ElementsContainerType::iterator it_begin = rModelPart.ElementsBegin();
      int NumberOfElements = rModelPart.NumberOfElements();

      //#pragma omp parallel for reduction(+:ModelPartVolume)
      for (int i = 0; i < NumberOfElements; ++i)
      {
        ModelPart::ElementsContainerType::iterator i_elem = it_begin + i;
        const GeometryType &eGeometry = i_elem->GetGeometry();
        double Volume = 0;
        if (eGeometry.WorkingSpaceDimension() == 3 && i_elem->Is(BOUNDARY) && i_elem->Is(FLUID) && i_elem->IsNot(VISITED))
        {
          Volume = eGeometry.Volume();
          ModelPartVolume += Volume;
        }
        Volume /= double(eGeometry.size());
        for (auto &i_node : eGeometry)
        {
          if (i_node.Is(FREE_SURFACE))
            rVariables.NodalVolume[rVariables.FreeSurfaceNodesIds[i_node.Id()]] += Volume;
        }
      }
    }

    return ModelPartVolume;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  double ComputeFreeSurfaceArea(ModelPart &rModelPart, ShapingVariables &rVariables)
  {
    KRATOS_TRY

    const unsigned int dimension = rModelPart.GetProcessInfo()[SPACE_DIMENSION];
    double FreeSurfaceArea = 0;

    if (dimension == 2)
    {

      ModelPart::ElementsContainerType::iterator it_begin = rModelPart.ElementsBegin();
      int NumberOfElements = rModelPart.NumberOfElements();

      //#pragma omp parallel for reduction(+:FreeSurfaceArea)
      for (int i = 0; i < NumberOfElements; ++i)
      {
        ModelPart::ElementsContainerType::iterator i_elem = it_begin + i;
        GeometryType &rGeometry = i_elem->GetGeometry();
        if (rGeometry.WorkingSpaceDimension() == 2 && i_elem->Is(BOUNDARY) && i_elem->Is(FLUID) && i_elem->IsNot(VISITED))
        {
          for (unsigned int j = 0; j < rGeometry.size() - 1; ++j)
          {
            if (rGeometry[j].Is(FREE_SURFACE))
            {
              double Area = 0;
              for (unsigned int k = j + 1; k < rGeometry.size(); ++k)
              {
                if (rGeometry[k].Is(FREE_SURFACE))
                {
                  Area = norm_2(rGeometry[k].Coordinates() - rGeometry[j].Coordinates());
                  FreeSurfaceArea += Area;
                  rVariables.NodalSurface[rVariables.FreeSurfaceNodesIds[rGeometry[j].Id()]] += 0.5 * Area;
                  rVariables.NodalSurface[rVariables.FreeSurfaceNodesIds[rGeometry[k].Id()]] += 0.5 * Area;
                }
              }
            }
          }
        }
      }
    }
    else
    { //dimension == 3

      DenseMatrix<unsigned int> lpofa; //connectivities of points defining faces
      DenseVector<unsigned int> lnofa; //number of points defining faces

      ModelPart::ElementsContainerType::iterator it_begin = rModelPart.ElementsBegin();
      int NumberOfElements = rModelPart.NumberOfElements();

#pragma omp parallel for private(lpofa, lnofa) reduction(+ \
                                                         : FreeSurfaceArea)
      for (int i = 0; i < NumberOfElements; ++i)
      {
        ModelPart::ElementsContainerType::iterator i_elem = it_begin + i;

        GeometryType &rGeometry = i_elem->GetGeometry();

        if (rGeometry.WorkingSpaceDimension() == 3 && i_elem->Is(BOUNDARY) && i_elem->Is(FLUID) && i_elem->IsNot(VISITED))
        {

          rGeometry.NodesInFaces(lpofa);
          rGeometry.NumberNodesInFaces(lnofa);

          for (unsigned int iface = 0; iface < rGeometry.FacesNumber(); ++iface)
          {
            unsigned int free_surface = 0;
            for (unsigned int j = 1; j <= lnofa[iface]; ++j)
              if (rGeometry[j].Is(FREE_SURFACE))
                ++free_surface;

            double Area = 0;
            if (free_surface == lnofa[iface])
            {
              Area = Compute3DArea(rGeometry[lpofa(1, iface)].Coordinates(),
                                   rGeometry[lpofa(2, iface)].Coordinates(),
                                   rGeometry[lpofa(3, iface)].Coordinates());
              FreeSurfaceArea += Area;
              Area *= (1.0 / 3.0);
              rVariables.NodalSurface[rVariables.FreeSurfaceNodesIds[rGeometry[lpofa(1, iface)].Id()]] += Area;
              rVariables.NodalSurface[rVariables.FreeSurfaceNodesIds[rGeometry[lpofa(2, iface)].Id()]] += Area;
              rVariables.NodalSurface[rVariables.FreeSurfaceNodesIds[rGeometry[lpofa(3, iface)].Id()]] += Area;
            }
          }
        }
      }
    }

    return FreeSurfaceArea;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  double ComputeFreeSurfaceVolume(ModelPart &rModelPart)
  {
    KRATOS_TRY

    const unsigned int dimension = rModelPart.GetProcessInfo()[SPACE_DIMENSION];
    double ModelPartVolume = 0;

    if (dimension == 2)
    {

      ModelPart::ElementsContainerType::iterator it_begin = rModelPart.ElementsBegin();
      int NumberOfElements = rModelPart.NumberOfElements();

#pragma omp parallel for reduction(+ \
                                   : ModelPartVolume)
      for (int i = 0; i < NumberOfElements; ++i)
      {
        ModelPart::ElementsContainerType::iterator i_elem = it_begin + i;
        if (i_elem->GetGeometry().WorkingSpaceDimension() == 2 && i_elem->Is(BOUNDARY) && i_elem->Is(FLUID) && i_elem->IsNot(VISITED))
          ModelPartVolume += i_elem->GetGeometry().Area();
      }
    }
    else
    { //dimension == 3

      ModelPart::ElementsContainerType::iterator it_begin = rModelPart.ElementsBegin();
      int NumberOfElements = rModelPart.NumberOfElements();

#pragma omp parallel for reduction(+ \
                                   : ModelPartVolume)
      for (int i = 0; i < NumberOfElements; ++i)
      {
        ModelPart::ElementsContainerType::iterator i_elem = it_begin + i;
        if (i_elem->GetGeometry().WorkingSpaceDimension() == 3 && i_elem->Is(BOUNDARY) && i_elem->Is(FLUID) && i_elem->IsNot(VISITED))
          ModelPartVolume += i_elem->GetGeometry().Volume();
      }
    }

    return ModelPartVolume;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  double ComputeFreeSurfaceArea(ModelPart &rModelPart)
  {
    KRATOS_TRY

    const unsigned int dimension = rModelPart.GetProcessInfo()[SPACE_DIMENSION];
    double FreeSurfaceArea = 0;

    if (dimension == 2)
    {

      ModelPart::ElementsContainerType::iterator it_begin = rModelPart.ElementsBegin();
      int NumberOfElements = rModelPart.NumberOfElements();

#pragma omp parallel for reduction(+ \
                                   : FreeSurfaceArea)
      for (int i = 0; i < NumberOfElements; ++i)
      {
        ModelPart::ElementsContainerType::iterator i_elem = it_begin + i;
        GeometryType &rGeometry = i_elem->GetGeometry();
        if (rGeometry.WorkingSpaceDimension() == 2 && i_elem->Is(BOUNDARY) && i_elem->Is(FLUID) && i_elem->IsNot(VISITED))
        {
          for (unsigned int j = 0; j < rGeometry.size() - 1; ++j)
          {
            if (rGeometry[j].Is(FREE_SURFACE))
            {
              for (unsigned int k = j + 1; k < rGeometry.size(); ++k)
              {
                if (rGeometry[k].Is(FREE_SURFACE))
                {
                  FreeSurfaceArea += norm_2(rGeometry[k].Coordinates() - rGeometry[j].Coordinates());
                }
              }
            }
          }
        }
      }
    }
    else
    { //dimension == 3

      DenseMatrix<unsigned int> lpofa; //connectivities of points defining faces
      DenseVector<unsigned int> lnofa; //number of points defining faces

      ModelPart::ElementsContainerType::iterator it_begin = rModelPart.ElementsBegin();
      int NumberOfElements = rModelPart.NumberOfElements();

#pragma omp parallel for private(lpofa, lnofa) reduction(+ \
                                                         : FreeSurfaceArea)
      for (int i = 0; i < NumberOfElements; ++i)
      {
        ModelPart::ElementsContainerType::iterator i_elem = it_begin + i;

        GeometryType &rGeometry = i_elem->GetGeometry();

        if (rGeometry.WorkingSpaceDimension() == 3 && i_elem->Is(BOUNDARY) && i_elem->Is(FLUID) && i_elem->IsNot(VISITED))
        {

          rGeometry.NodesInFaces(lpofa);
          rGeometry.NumberNodesInFaces(lnofa);

          for (unsigned int iface = 0; iface < rGeometry.FacesNumber(); ++iface)
          {
            unsigned int free_surface = 0;
            for (unsigned int j = 1; j <= lnofa[iface]; ++j)
              if (rGeometry[j].Is(FREE_SURFACE))
                ++free_surface;

            if (free_surface == lnofa[iface])
              FreeSurfaceArea += Compute3DArea(rGeometry[lpofa(1, iface)].Coordinates(),
                                               rGeometry[lpofa(2, iface)].Coordinates(),
                                               rGeometry[lpofa(3, iface)].Coordinates());
          }
        }
      }
    }

    return FreeSurfaceArea;

    KRATOS_CATCH("")
  }

  //*******************************************************************************************
  //*******************************************************************************************

  double Compute3DArea(array_1d<double, 3> PointA, array_1d<double, 3> PointB, array_1d<double, 3> PointC)
  {
    double a = MathUtils<double>::Norm3(PointA - PointB);
    double b = MathUtils<double>::Norm3(PointB - PointC);
    double c = MathUtils<double>::Norm3(PointC - PointA);
    double s = (a + b + c) / 2.0;
    double Area = std::sqrt(s * (s - a) * (s - b) * (s - c));
    return Area;
  }

  ///@}
  ///@name Private  Access
  ///@{

  ///@}
  ///@name Private Inquiry
  ///@{

  ///@}
  ///@name Un accessible methods
  ///@{

  /// Assignment operator.
  RecoverVolumeLossesProcess &operator=(RecoverVolumeLossesProcess const &rOther);

  /// this function is a private function

  /// Copy constructor.
  //Process(Process const& rOther);

  ///@}

}; // Class Process

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                RecoverVolumeLossesProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const RecoverVolumeLossesProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_RECOVER_VOLUME_LOSSES_PROCESS_HPP_INCLUDED  defined
