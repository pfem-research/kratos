//
//   Project Name:        KratosPfemApplication    $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:         July 2018 $
//
//

#if !defined(KRATOS_SMOOTH_FREE_SURFACE_MESHER_PROCESS_HPP_INCLUDED)
#define KRATOS_SMOOTH_FREE_SURFACE_MESHER_PROCESS_HPP_INCLUDED

// External includes

// System includes

// Project includes
#include "custom_utilities/mesher_utilities.hpp"

#include "custom_processes/mesher_process.hpp"

namespace Kratos
{

///@name Kratos Classes
///@{

/// Remove Fluid Nodes Process for 2D and 3D cases

class SmoothFreeSurfaceMesherProcess
    : public MesherProcess
{
public:
  ///@name Type Definitions
  ///@{

  /// Pointer definition of Process
  KRATOS_CLASS_POINTER_DEFINITION(SmoothFreeSurfaceMesherProcess);

  typedef ModelPart::ConditionType ConditionType;
  typedef ModelPart::PropertiesType PropertiesType;
  typedef ConditionType::GeometryType GeometryType;

  typedef GlobalPointersVector<Node<3>> NodeWeakPtrVectorType;
  typedef GlobalPointersVector<Element> ElementWeakPtrVectorType;
  typedef GlobalPointersVector<Condition> ConditionWeakPtrVectorType;
  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  SmoothFreeSurfaceMesherProcess(ModelPart &rModelPart,
                                 MesherData::MeshingParameters &rRemeshingParameters,
                                 int EchoLevel)
      : mrModelPart(rModelPart),
        mrRemesh(rRemeshingParameters),
        mEchoLevel(EchoLevel)
  {
  }

  /// Destructor.
  virtual ~SmoothFreeSurfaceMesherProcess() {}

  ///@}
  ///@name Operators
  ///@{

  /// This operator is provided to call the process as a function and simply calls the Execute method.
  void operator()()
  {
    Execute();
  }

  ///@}
  ///@name Operations
  ///@{

  /// Execute method is used to execute the Process algorithms.
  void Execute() override
  {
    KRATOS_TRY

    //set flags for the local process execution
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{VISITED}}, {VISITED.AsFalse()});

    this->MoveBoundaries(mrModelPart);

    //set flags for the local process execution
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{VISITED}}, {VISITED.AsFalse()});

    KRATOS_CATCH(" ")
  }

  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "SmoothFreeSurfaceMesherProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "SmoothFreeSurfaceMesherProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{

  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{

  ModelPart &mrModelPart;

  MesherData::MeshingParameters &mrRemesh;

  int mEchoLevel;

  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{
  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Private Static Member Variables
  ///@{
  ///@}
  ///@name Private Static Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{

  //**************************************************************************
  //**************************************************************************

  void MoveBoundaries(ModelPart &rModelPart)
  {

    KRATOS_TRY

    const unsigned int dimension = mrModelPart.GetProcessInfo()[SPACE_DIMENSION];

    //if (mrModelPart.GetProcessInfo()[SPACE_DIMENSION] == 3)
    //{
      //smooth free surface interaction nodes
      double smooth_factor = 0.2;
      //interpolate position using neighbours with:
      std::vector<Flags> PositionFlags = {FREE_SURFACE};
      //interpolate variables using neighbours with:
      std::vector<Flags> VariableFlags = {FREE_SURFACE, STRUCTURE.AsFalse(), MARKER.AsFalse()};
      for (auto &i_node : rModelPart.Nodes())
      {
        if (i_node.Is({FREE_SURFACE, INTERACTION, INSIDE.AsFalse()}))
        {
	  NodeWeakPtrVectorType &nNodes = i_node.GetValue(NEIGHBOUR_NODES);

          if (dimension==3){
            for (auto &i_nnode : nNodes)
            {
              if (i_nnode.Is(FREE_SURFACE) && i_nnode.Is(INTERACTION))
              {
                this->MoveBoundaryNode(i_node, PositionFlags, VariableFlags, smooth_factor);
                //std::cout<<" Smooth surface interaction node "<<i_node.Id()<<std::endl;
                break;
              }
            }
          }
          else{
            bool blocked = true;
            for (auto &j_nnode : nNodes)
              if (j_nnode.IsNot(STRUCTURE) || j_nnode.Is(INTERACTION)){
                blocked = false;
              }
            //to avoid moving bloked nodes
            if(!blocked){
              for (auto &i_nnode : nNodes)
              {
                if (i_nnode.Is(FREE_SURFACE) && i_nnode.Is(STRUCTURE))
                {
                  this->MoveBoundaryNode(i_node, PositionFlags, VariableFlags, smooth_factor);
                  std::cout<<" Smooth surface interaction node "<<i_node.Id()<<std::endl;
                  break;
                }
              }
            }
          }
        }
      }

      //smooth free surface nodes
      smooth_factor = 0.10;
      //interpolate position using neighbours with:
      PositionFlags = {FREE_SURFACE, STRUCTURE.AsFalse()};
      //interpolate variables using neighbours with:
      VariableFlags = {FREE_SURFACE, STRUCTURE.AsFalse(), MARKER.AsFalse()};
      for (auto &i_node : rModelPart.Nodes())
      {
        if (i_node.Is(FREE_SURFACE) && i_node.AndNot({STRUCTURE, INTERACTION}))
        {
          this->MoveBoundaryNode(i_node, PositionFlags, VariableFlags, smooth_factor);
          //std::cout<<" Smooth free surface node "<<i_node.Id()<<std::endl;
        }
      }
      //}

    KRATOS_CATCH("")
  }

  //**************************************************************************
  //**************************************************************************

  bool MoveBoundaryNode(Node<3> &rNode, const std::vector<Flags> &rPositionFlags, const std::vector<Flags> &rVariableFlags, const double &rsmooth_factor)
  {
    KRATOS_TRY

    if (rsmooth_factor == 0.0)
      return true;

    unsigned int dimension = mrModelPart.GetProcessInfo()[SPACE_DIMENSION];

    unsigned int selected_nodes = 0;
    unsigned int free_surface_nodes = 0;
    unsigned int wall_nodes = 0;

    NodeWeakPtrVectorType &nNodes = rNode.GetValue(NEIGHBOUR_NODES);
    NodeWeakPtrVectorType SelectedNeighbours;

    array_1d<double, 3> Tangent;
    noalias (Tangent) =  ZeroVector(3);
    for (auto i_nnodes(nNodes.begin()); i_nnodes != nNodes.end(); ++i_nnodes)
    {
      if (i_nnodes->Is(rPositionFlags))
      {
        SelectedNeighbours.push_back(*i_nnodes.base());
        ++selected_nodes;
        if (i_nnodes->Is(STRUCTURE)){
          ++wall_nodes;
          if(i_nnodes->Is(SOLID))
            Tangent += i_nnodes->FastGetSolutionStepValue(NORMAL); // wall normals are tangent to the surface ~
          else
            Tangent -= i_nnodes->FastGetSolutionStepValue(NORMAL); // wall normals are tangent to the surface ~
        }
      }

      if (i_nnodes->Is(FREE_SURFACE))
        ++free_surface_nodes;
    }

    bool mesh_movement = false;
    if (rNode.SolutionStepsDataHas(MESH_DISPLACEMENT))
    {
      mesh_movement = true;
    }

    if (dimension == 2 && free_surface_nodes > 2){// edge elements, do not move closer nodes
      //std::cout<<" 2d case with free_surface_nodes "<<free_surface_nodes<<std::endl;
      return false;
    }


    bool moved_node = false;
    double Distance = 0;
    if (selected_nodes == 2)
    {
      if (wall_nodes==0){

        const array_1d<double, 3> &i_Normal = SelectedNeighbours.front().FastGetSolutionStepValue(NORMAL);
        const array_1d<double, 3> &j_Normal = SelectedNeighbours.back().FastGetSolutionStepValue(NORMAL);

        double sign_i = 1.0, sign_j = 1.0;
        if(SelectedNeighbours.front().Is(SOLID))
          sign_i = -1.0;
        if(SelectedNeighbours.back().Is(SOLID))
          sign_j = -1.0;

        double projection = inner_prod(sign_i*i_Normal, sign_j*j_Normal);
        if (projection<0.2)
          return false;
      }
      array_1d<double, 3> MidPoint = 0.5 * (SelectedNeighbours.front().Coordinates() + SelectedNeighbours.back().Coordinates());
      array_1d<double, 3> Direction = (SelectedNeighbours.front().Coordinates() - SelectedNeighbours.back().Coordinates());

      if (norm_2(Direction))
        Direction /= norm_2(Direction);

      //array_1d<double, 3> Displacement = inner_prod((MidPoint - rNode.Coordinates()), Direction) * Direction * rsmooth_factor;
      // const array_1d<double, 3> &rNormal = rNode.FastGetSolutionStepValue(NORMAL);
      array_1d<double, 3> Displacement = (MidPoint - rNode.Coordinates());
      Displacement *= rsmooth_factor;

      if (rNode.Is(INTERACTION) && wall_nodes!=0){
        Tangent/=double(wall_nodes);
        if (norm_2(Tangent))
          Tangent /= norm_2(Tangent);
        Displacement  = inner_prod(Displacement, Tangent) * Tangent;
      }

      noalias(rNode.Coordinates()) += Displacement;
      noalias(rNode.GetInitialPosition()) += Displacement;

      // noalias(rNode.FastGetSolutionStepValue(DISPLACEMENT))   += Displacement;
      // noalias(rNode.FastGetSolutionStepValue(DISPLACEMENT,1)) += Displacement;
      // rNode.GetInitialPosition() = Point(rNode.Coordinates() - rNode.FastGetSolutionStepValue(DISPLACEMENT));

      Distance = norm_2(Displacement);
      if (mEchoLevel > 0)
        std::cout<<" Boundary 2 Move Post ["<<rNode.Id()<<"] "<<rNode.Coordinates()<<" Displacement "<<Displacement<<std::endl;
      moved_node = true;
    }

    if (selected_nodes > 2 && dimension == 3)
    {
      if (wall_nodes == 0){
        for (auto &i_fnnode : SelectedNeighbours)
        {
          double sign_i = 1.0;
          if(i_fnnode.Is(SOLID))
            sign_i = -1.0;
          const array_1d<double, 3> &i_Normal = i_fnnode.FastGetSolutionStepValue(NORMAL);
          for (auto &j_fnnode : SelectedNeighbours)
          {
            double sign_j = 1.0;
            if(j_fnnode.Is(SOLID))
              sign_j = -1.0;
            const array_1d<double, 3> &j_Normal = j_fnnode.FastGetSolutionStepValue(NORMAL);
            if (inner_prod(sign_i*i_Normal, sign_j*j_Normal)<0.2)
              return false;
          }
        }
      }

      array_1d<double, 3> MidPoint;
      noalias(MidPoint) = ZeroVector(3);
      double quotient = 1.0 / double(selected_nodes);

      for (auto &i_fnnode : SelectedNeighbours)
        MidPoint += i_fnnode.Coordinates();

      MidPoint *= quotient;

      array_1d<double, 3> Displacement = (MidPoint - rNode.Coordinates());
      Displacement *= rsmooth_factor;

      if (rNode.Is(INTERACTION) && wall_nodes!=0){
        Tangent/=double(wall_nodes);
        if (norm_2(Tangent)){
          Tangent /= norm_2(Tangent);
          Displacement  = inner_prod(Displacement, Tangent) * Tangent;
        }
      }

      noalias(rNode.Coordinates()) += Displacement;
      noalias(rNode.GetInitialPosition()) += Displacement;

      // noalias(rNode.FastGetSolutionStepValue(DISPLACEMENT)) += Displacement;
      // noalias(rNode.FastGetSolutionStepValue(DISPLACEMENT,1)) += Displacement;
      //rNode.GetInitialPosition() = Point(rNode.Coordinates() - rNode.FastGetSolutionStepValue(DISPLACEMENT));

      Distance = norm_2(Displacement);
      if (mEchoLevel > 0)
        std::cout<<" Boundary X Move Post ["<<rNode.Id()<<"] "<<rNode.Coordinates()<<" Displacement "<<Displacement<<std::endl;
      moved_node = true;
    }
    // else
    // {
    //   std::cout << " Boundary node with only one FREE_SURFACE neighbour or with more than two (in 2D)" << std::endl;
    // }

    if (moved_node)
    {
      SelectedNeighbours.clear();
      SelectedNeighbours.resize(0);
      for (auto i_nnodes(nNodes.begin()); i_nnodes != nNodes.end(); ++i_nnodes)
        if(i_nnodes->Is(rVariableFlags))
          SelectedNeighbours.push_back(*i_nnodes.base());


      if (Distance != 0)
      {
        //give weight to current value of fluid variables
        double weight = 1.0 / Distance;
        MesherUtilities::MultiplyFluidVariables(rNode, weight);
        MesherUtilities::MultiplyFluidVariables(rNode, weight, 1);
        // rNode.FastGetSolutionStepValue(PRESSURE_VELOCITY) *= weight;
        // rNode.FastGetSolutionStepValue(PRESSURE_VELOCITY,1) *= weight;

        if (mesh_movement)
        {
          MesherUtilities::MultiplyFluidMeshVariables(rNode, weight);
          MesherUtilities::MultiplyFluidMeshVariables(rNode, weight, 1);
        }
        double total_weight = weight;
        for (auto &i_fnnode : SelectedNeighbours)
        {
          weight = norm_2(rNode.Coordinates() - i_fnnode.Coordinates());
          if (weight != 0)
            weight = 1.0 / weight;

          MesherUtilities::SmoothFluidVariables(rNode, i_fnnode, weight);
          MesherUtilities::SmoothFluidVariables(rNode, i_fnnode, weight, 1);
          // rNode.FastGetSolutionStepValue(PRESSURE_VELOCITY) += weight*i_fnnode.FastGetSolutionStepValue(PRESSURE_VELOCITY);
          // rNode.FastGetSolutionStepValue(PRESSURE_VELOCITY,1) += weight*i_fnnode.FastGetSolutionStepValue(PRESSURE_VELOCITY,1);

          if (mesh_movement)
          {
            MesherUtilities::SmoothFluidMeshVariables(rNode, i_fnnode, weight);
            MesherUtilities::SmoothFluidMeshVariables(rNode, i_fnnode, weight, 1);
          }
          total_weight += weight;
        }

        double quotient = 1.0 / total_weight;
        MesherUtilities::MultiplyFluidVariables(rNode, quotient);
        MesherUtilities::MultiplyFluidVariables(rNode, quotient, 1);
        // rNode.FastGetSolutionStepValue(PRESSURE_VELOCITY) *= quotient;
        // rNode.FastGetSolutionStepValue(PRESSURE_VELOCITY,1) *= quotient;

        if (mesh_movement)
        {
          MesherUtilities::MultiplyFluidMeshVariables(rNode, quotient);
          MesherUtilities::MultiplyFluidMeshVariables(rNode, quotient, 1);
        }
      }

      rNode.Set(VISITED, true);
    }

    // if(rNode.Is(INSIDE))
    //   std::cout<<" INSIDE NODE MOVED :: WARNING "<<std::endl;

    return moved_node;

    KRATOS_CATCH("")
  }

  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{

  /// Assignment operator.
  SmoothFreeSurfaceMesherProcess &operator=(SmoothFreeSurfaceMesherProcess const &rOther);

  /// this function is a private function

  /// Copy constructor.
  //Process(Process const& rOther);

  ///@}

}; // Class Process

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                SmoothFreeSurfaceMesherProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const SmoothFreeSurfaceMesherProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_SMOOTH_FREE_SURFACE_MESHER_PROCESS_HPP_INCLUDED  defined
