//
//   Project Name:        KratosPfemApplication    $
//   Developed by:        $Developer:    LMonforte $
//   Maintained by:       $Maintainer:          LM $
//   Date:                $Date:     February 2016 $
//
//

#if !defined(KRATOS_REFINE_CONDITIONS_IN_CONTACT_MESHER_PROCESS_HPP_INCLUDED)
#define KRATOS_REFINE_CONDITIONS_IN_CONTACT_MESHER_PROCESS_HPP_INCLUDED

// External includes

// System includes

// Project includes
#include "custom_processes/refine_conditions_mesher_process.hpp"

#include "pfem_application_variables.h"

namespace Kratos
{

  ///@name Kratos Classes
  ///@{

  /// Refine Mesh Boundary Process
  /** The process labels the boundary conditions (TO_SPLIT)
      Dependencies: RemoveMeshNodesProcess.Execute()  is needed as a previous step

      Determines if new conditions must be inserted in boundary.
      If boundary must to be kept (CONSTRAINED),
      New conditions will be rebuild (spliting the old ones and inserting new nodes)
      Old conditions will be erased at the end.

  */

  class RefineConditionsInContactMesherProcess
    : public RefineConditionsMesherProcess
  {
  public:
    ///@name Type Definitions
    ///@{

    /// Pointer definition of Process
    KRATOS_CLASS_POINTER_DEFINITION(RefineConditionsInContactMesherProcess);

    typedef ModelPart::NodeType NodeType;
    typedef ModelPart::ConditionType ConditionType;
    typedef ModelPart::PropertiesType PropertiesType;
    typedef ConditionType::GeometryType GeometryType;
    typedef PointerVector<NodeType> PointsArrayType;

    typedef PointerVectorSet<ConditionType, IndexedObject> ConditionsContainerType;
    typedef ConditionsContainerType::iterator ConditionIterator;
    typedef ConditionsContainerType::const_iterator ConditionConstantIterator;

    struct RefineCounters
    {
    public:
      //counters:
      int number_of_contact_conditions;
      int number_of_contacts;
      int number_of_active_contacts;

      int number_contact_size_insertions;
      int number_contact_tip_insertions;

      int number_of_exterior_bounds;
      int number_of_tip_bounds;
      int number_of_energy_bounds;

      void Initialize()
      {
        //counters:
        number_of_contact_conditions = 0;

        number_of_contacts = 0;
        number_of_active_contacts = 0;

        number_contact_size_insertions = 0;
        number_contact_tip_insertions = 0;

        number_of_exterior_bounds = 0;
        number_of_tip_bounds = 0;
        number_of_energy_bounds = 0;
      }
    };

    ///@}
    ///@name Life Cycle
    ///@{

    /// Default constructor
    RefineConditionsInContactMesherProcess(ModelPart &rModelPart,
                                           std::vector<SpatialBoundingBox::Pointer> mRigidWalls,
                                           MesherData::MeshingParameters &rRemeshingParameters,
                                           int EchoLevel)
      : RefineConditionsMesherProcess(rModelPart, rRemeshingParameters, EchoLevel)
    {
    }

    /// Destructor.
    virtual ~RefineConditionsInContactMesherProcess() {}

    ///@}
    ///@name Operators
    ///@{

    /// This operator is provided to call the process as a function and simply calls the Execute method.
    void operator()()
    {
      Execute();
    }

    ///@}
    ///@name Operations
    ///@{

    /// Execute method is used to execute the Process algorithms.
    void Execute() override
    {

      KRATOS_TRY

        if (this->mEchoLevel > 0)
          {
            std::cout << " [ REFINE BOUNDARY : " << std::endl;
          }

      mrRemesh.Info->Inserted.Conditions = mrModelPart.NumberOfConditions();
      mrRemesh.Info->Inserted.BoundaryNodes = mrModelPart.NumberOfNodes();

      RefineCounters LocalRefineInfo;
      LocalRefineInfo.Initialize();

      //if the insert switches are activated, we check if the boundaries got too coarse
      if (mrRemesh.IsBoundary(RefineData::REFINE))
        {

          std::vector<Point> list_of_points;
          std::vector<Condition *> list_of_conditions;

          unsigned int conditions_size = mrModelPart.Conditions().size();

          if (mrModelPart.Name() == mrRemesh.SubModelPartName)
            {

              list_of_points.reserve(conditions_size);
              list_of_conditions.reserve(conditions_size);

              RefineContactBoundary(mrModelPart, list_of_points, list_of_conditions, LocalRefineInfo);

              RefineOtherBoundary(mrModelPart, list_of_points, list_of_conditions, LocalRefineInfo);

              BuildNewConditions(mrModelPart, list_of_points, list_of_conditions, LocalRefineInfo);

              CleanConditionsAndFlags(mrModelPart);
            }
          else
            {

              ModelPart &rModelPart = mrModelPart.GetSubModelPart(mrRemesh.SubModelPartName);

              conditions_size = rModelPart.Conditions().size();
              list_of_points.reserve(conditions_size);
              list_of_conditions.reserve(conditions_size);

              RefineContactBoundary(rModelPart, list_of_points, list_of_conditions, LocalRefineInfo);

              RefineOtherBoundary(rModelPart, list_of_points, list_of_conditions, LocalRefineInfo);

              BuildNewConditions(rModelPart, list_of_points, list_of_conditions, LocalRefineInfo);

              CleanConditionsAndFlags(rModelPart);
            }

        } // REFINE END;

      mrRemesh.Info->Inserted.Conditions = mrModelPart.NumberOfConditions() - mrRemesh.Info->Inserted.Conditions;
      mrRemesh.Info->Inserted.BoundaryNodes = mrModelPart.NumberOfNodes() - mrRemesh.Info->Inserted.BoundaryNodes;

      if (this->mEchoLevel > 0)
        {
          std::cout << "   [ CONDITIONS ( inserted : " << mrRemesh.Info->Inserted.Conditions << " ) ]" << std::endl;
          std::cout << "   [ NODES      ( inserted : " << mrRemesh.Info->Inserted.BoundaryNodes << " ) ]" << std::endl;
          std::cout << "   [ contact(TIP: " << LocalRefineInfo.number_contact_tip_insertions << ", SIZE: " << LocalRefineInfo.number_contact_size_insertions << ") -  bound(TIP: " << LocalRefineInfo.number_of_tip_bounds << ", SIZE: " << LocalRefineInfo.number_of_exterior_bounds << ")]" << std::endl;

          std::cout << "   REFINE BOUNDARY ]; " << std::endl;
        }

      KRATOS_CATCH(" ")
	}

    ///@}
    ///@name Access
    ///@{

    ///@}
    ///@name Inquiry
    ///@{

    ///@}
    ///@name Input and output
    ///@{

    /// Turn back information as a string.
    std::string Info() const override
    {
      return "RefineConditionsInContactMesherProcess";
    }

    /// Print information about this object.
    void PrintInfo(std::ostream &rOStream) const override
    {
      rOStream << "RefineConditionsInContactMesherProcess";
    }

    /// Print object's data.
    void PrintData(std::ostream &rOStream) const override
    {
    }

    ///@}
    ///@name Friends
    ///@{

    ///@}

  private:
    ///@name Static Member Variables
    ///@{

    ///@}
    ///@name Member Variables
    ///@{

    std::vector<SpatialBoundingBox::Pointer> mRigidWalls;

    ///@}
    ///@name Un accessible methods
    ///@{

    //*******************************************************************************************
    //*******************************************************************************************

    void CleanConditionsAndFlags(ModelPart &rModelPart)
    {

      KRATOS_TRY

      //swap conditions for a temporary use
      ModelPart::ConditionsContainerType TemporaryConditions;
      TemporaryConditions.reserve(rModelPart.Conditions().size());
      TemporaryConditions.swap(rModelPart.Conditions());

      for (auto i_cond(rModelPart.ConditionsBegin()); i_cond != rModelPart.ConditionsEnd(); ++i_cond)
      {
        Geometry<Node<3>> rGeometry = i_cond->GetGeometry();
        for (unsigned int i = 0; i < rGeometry.size(); ++i)
          {
            rGeometry[i].Reset(TO_SPLIT);
          }

        if (i_cond->IsNot(TO_ERASE))
          {
            rModelPart.AddCondition(*i_cond.base());
          }
        // else{
        //   std::cout<<"   Condition RELEASED:"<<i_cond->Id()<<std::endl;
        // }
      }

      KRATOS_CATCH(" ")
	}

    //*******************************************************************************************
    //*******************************************************************************************

    void BuildNewConditions(ModelPart &rModelPart, std::vector<Point> &list_of_points, std::vector<Condition *> &list_of_conditions, RefineCounters &rLocalRefineInfo)
    {

      KRATOS_TRY

      //node to get the DOFs from
      Node<3>::DofsContainerType &reference_dofs = (rModelPart.NodesBegin())->GetDofs();
      //unsigned int step_data_size = rModelPart.GetNodalSolutionStepDataSize();
      double z = 0.0;

      unsigned int initial_node_size = mrModelPart.Nodes().size() + 1;	  //total model part node size
      unsigned int initial_cond_size = mrModelPart.Conditions().size() + 1; //total model part node size

      Node<3> new_point(0, 0.0, 0.0, 0.0);

      int id = 0;
      //if points were added, new nodes must be added to ModelPart
      for (unsigned int i = 0; i < list_of_points.size(); ++i)
        {
          id = initial_node_size + i;

          double &x = list_of_points[i].X();
          double &y = list_of_points[i].Y();

          Node<3>::Pointer pnode = rModelPart.CreateNewNode(id, x, y, z);

          pnode->SetBufferSize(rModelPart.NodesBegin()->GetBufferSize());

          //assign data to dofs

          //2D edges:
          Geometry<Node<3>> &rConditionGeometry = list_of_conditions[i]->GetGeometry();

          MesherData MeshData;
          RefineData& Refine = MeshData.GetRefineData(rConditionGeometry, mrRemesh, rModelPart.GetProcessInfo());

          //generating the dofs
          for (Node<3>::DofsContainerType::iterator iii = reference_dofs.begin(); iii != reference_dofs.end(); ++iii)
            {
              Node<3>::DofType &rDof = **iii;
              Node<3>::DofType::Pointer p_new_dof = pnode->pAddDof(rDof);

              if (rConditionGeometry[0].IsFixed(rDof.GetVariable()) && rConditionGeometry[1].IsFixed(rDof.GetVariable()))
                (p_new_dof)->FixDof();
              else
                (p_new_dof)->FreeDof();
            }

          //assign data to dofs
          VariablesList &variables_list = rModelPart.GetNodalSolutionStepVariablesList();

          PointsArrayType PointsArray;
          PointsArray.push_back(rConditionGeometry(0));
          PointsArray.push_back(rConditionGeometry(1));

          Geometry<Node<3>> geom(PointsArray);

          Vector N(2,0.5);

          double alpha = 1;
          TransferUtilities::InterpolateStepData(*pnode, geom, N, variables_list, alpha);

          // //int cond_id = list_of_points[i].Id();
          // //Geometry< Node<3> >& rConditionGeometry = (*(rModelPart.Conditions().find(cond_id).base()))->GetGeometry();

          //set specific control values and flags:
          pnode->Set(BOUNDARY);
          pnode->Set(NEW_ENTITY); //if boundary is rebuild, the flag INSERTED must be set to new conditions too
          //std::cout<<"   Node ["<<pnode->Id()<<"] is a NEW_ENTITY "<<std::endl;

          double &nodal_h = pnode->FastGetSolutionStepValue(NODAL_H);
          //nodal_h = 0.5*(nodal_h+Refine.Boundary.DistanceMeshSize; //modify nodal_h for security
          nodal_h = Refine.Boundary.DistanceMeshSize; //modify nodal_h for security

          const array_1d<double, 3> ZeroNormal(3, 0.0);
          //correct normal interpolation
          noalias(pnode->GetSolutionStepValue(NORMAL)) = list_of_conditions[i]->GetValue(NORMAL);

          //correct contact_normal interpolation (laplacian boundary projection uses it)
          array_1d<double, 3> &ContactForceNormal1 = rConditionGeometry[0].FastGetSolutionStepValue(CONTACT_FORCE);
          array_1d<double, 3> &ContactForceNormal2 = rConditionGeometry[1].FastGetSolutionStepValue(CONTACT_FORCE);
          if (norm_2(ContactForceNormal1) == 0 || norm_2(ContactForceNormal2) == 0)
            noalias(pnode->FastGetSolutionStepValue(CONTACT_FORCE)) = ZeroNormal;

          //recover the original position of the node
          const array_1d<double, 3> &disp = pnode->FastGetSolutionStepValue(DISPLACEMENT);
          pnode->X0() = pnode->X() - disp[0];
          pnode->Y0() = pnode->Y() - disp[1];
          pnode->Z0() = pnode->Z() - disp[2];

          //Conditions must be also created with the add of a new node:
          Condition::NodesArrayType face1;
          Condition::NodesArrayType face2;
          face1.reserve(2);
          face2.reserve(2);

          face1.push_back(rConditionGeometry(0));
          face1.push_back(pnode);

          face2.push_back(pnode);
          face2.push_back(rConditionGeometry(1));

          id = initial_cond_size + (i * 2);

          ConditionType::Pointer pcond1 = list_of_conditions[i]->Clone(id, face1);
          //std::cout<<" ID"<<id<<" 1s "<<pcond1->GetGeometry()[0].Id()<<" "<<pcond1->GetGeometry()[1].Id()<<std::endl;
          id = initial_cond_size + (i * 2 + 1);
          ConditionType::Pointer pcond2 = list_of_conditions[i]->Clone(id, face2);
          //std::cout<<" ID"<<id<<" 2s "<<pcond2->GetGeometry()[0].Id()<<" "<<pcond2->GetGeometry()[1].Id()<<std::endl;

          pcond1->Set(NEW_ENTITY);
          pcond2->Set(NEW_ENTITY);

          //pcond1->SetValue(PRIMARY_ELEMENTS, list_of_conditions[i]->GetValue(PRIMARY_ELEMENTS));
          pcond1->SetValue(PRIMARY_NODES, list_of_conditions[i]->GetValue(PRIMARY_NODES));
          pcond1->SetValue(NORMAL, list_of_conditions[i]->GetValue(NORMAL));
          pcond1->SetValue(CAUCHY_STRESS_VECTOR, list_of_conditions[i]->GetValue(CAUCHY_STRESS_VECTOR));
          pcond1->SetValue(DEFORMATION_GRADIENT, list_of_conditions[i]->GetValue(DEFORMATION_GRADIENT));

          //pcond2->SetValue(PRIMARY_ELEMENTS, list_of_conditions[i]->GetValue(PRIMARY_ELEMENTS));
          pcond2->SetValue(PRIMARY_NODES, list_of_conditions[i]->GetValue(PRIMARY_NODES));
          pcond2->SetValue(NORMAL, list_of_conditions[i]->GetValue(NORMAL));
          pcond2->SetValue(CAUCHY_STRESS_VECTOR, list_of_conditions[i]->GetValue(CAUCHY_STRESS_VECTOR));
          pcond2->SetValue(DEFORMATION_GRADIENT, list_of_conditions[i]->GetValue(DEFORMATION_GRADIENT));

          //std::cout<<" pcond0 "<<rModelPart.NumberOfConditions()<<" "<<mrRemesh.Info->Inserted.Conditions<<std::endl;

          rModelPart.AddCondition(pcond1);

          //std::cout<<" pcond1 "<<rModelPart.NumberOfConditions()<<" "<<mrRemesh.Info->Inserted.Conditions<<std::endl;

          rModelPart.AddCondition(pcond2);

          //std::cout<<" pcond2 "<<rModelPart.NumberOfConditions()<<" "<<mrRemesh.Info->Inserted.Conditions<<std::endl;
        }

      KRATOS_CATCH(" ")
	}

    //*******************************************************************************************
    //*******************************************************************************************

    bool RefineContactBoundary(ModelPart &rModelPart, std::vector<Point> &list_of_points, std::vector<Condition *> &list_of_conditions, RefineCounters &rLocalRefineInfo)
    {

      KRATOS_TRY

      //ProcessInfo& rCurrentProcessInfo = rModelPart.GetProcessInfo();

      //***SIZES :::: parameters do define the tolerance in mesh size:

      //DEFORMABLE CONTACT:
      double factor_for_tip_radius = 0.2;   //deformable contact tolerance in radius for detection tip sides to refine
      double factor_for_non_tip_side = 3.0; // will be multiplied for nodal_h of the master node to compare with boundary nodes average nodal_h in a contact conditio which master node do not belongs to a tip

      double factor_for_tip_contact_side = 0.4;  //factor for length size for the contact tip side
      double factor_for_non_tip_contact_side = 2.0; //factor for side compared with contact size which master node do not belongs to a tip

      //*********************************************************************************
      // DETECTION OF NODES ON TIP CONTACTS START
      //*********************************************************************************

      unsigned int nodes_on_wall_tip = 0;
      double ContactFace = 0;
      for (ModelPart::ConditionsContainerType::iterator i_cond = rModelPart.ConditionsBegin(); i_cond != rModelPart.ConditionsEnd(); i_cond++)
        {
          if (i_cond->Is(CONTACT) && i_cond->GetGeometry().size() == 1)
            {

              // i_cond->CalculateOnIntegrationPoints(ContactFace, CONTACT_FACE, rCurrentProcessInfo);

              if (int(ContactFace) == 2)
                { // tip

                  i_cond->GetGeometry()[0].Set(TO_SPLIT);
                  nodes_on_wall_tip++;
                }
            }
        }

      if (this->mEchoLevel > 0)
        std::cout << "   [ NODES ON WALL TIP: ( " << nodes_on_wall_tip << " ) ]" << std::endl;

      //*********************************************************************************
      // DETECTION OF NODES ON TIP CONTACTS END
      //*********************************************************************************

      // std::vector<int> nodes_ids;
      // nodes_ids.resize(rModelPart.Conditions().size()); //mesh 0
      // std::fill( nodes_ids.begin(), nodes_ids.end(), 0 );

      //std::cout<<"   List of Conditions Reserved Size: "<<conditions_size<<std::endl;

      double tool_radius = 0;
      double side_length = 0;

      bool size_insert = false;
      bool radius_insert = false;
      bool contact_active = false;

      bool contact_semi_active = false;
      bool tool_project = false;

      std::vector<bool> semi_active_nodes;
      Node<3> new_point(0, 0.0, 0.0, 0.0);

      for (auto i_cond(rModelPart.ConditionsBegin()); i_cond != rModelPart.ConditionsEnd(); ++i_cond)
      {
        size_insert = false;
        radius_insert = false;
        tool_project = false;
        contact_active = false;

        tool_radius = 0;
        side_length = 0;

        Geometry<Node<3>> rConditionGeometry;
        array_1d<double, 3> tip_center;
        tip_center.clear();

        //LOOP TO CONSIDER ONLY CONTACT CONDITIONS
        if (i_cond->Is(CONTACT)) //Refine radius on the workpiece for the ContactDomain zone
        {

          Node<3> MasterNode;
          ConditionType::Pointer MasterCondition;
          bool condition_found = SearchUtilities::FindMasterCondition(rModelPart.Conditions(), *i_cond.base(), MasterCondition, MasterNode);


          if (condition_found)
          {

            if (MasterCondition->IsNot(TO_ERASE))
            {

              rConditionGeometry = MasterCondition->GetGeometry();

              MesherData MeshData;
              RefineData &Refine = MeshData.GetRefineData(rConditionGeometry, mrRemesh, rModelPart.GetProcessInfo());
              double size_for_tip_contact_side = factor_for_tip_contact_side * Refine.Boundary.DistanceMeshSize;         // length size for the contact tip side
              double size_for_non_tip_contact_side = factor_for_non_tip_contact_side * Refine.Boundary.DistanceMeshSize; // compared with contact size which master node do not belongs to a tip

              //to recover TIP definition on conditions
              if (MasterNode.SolutionStepsDataHas(WALL_TIP_RADIUS)) //master node in tool -->  refine workpiece  //
              {

                tool_radius = MasterNode.FastGetSolutionStepValue(WALL_TIP_RADIUS);
                tip_center = MasterNode.FastGetSolutionStepValue(WALL_REFERENCE_POINT);
                // WARNING THE UPDATE OF THE TIP CENTER IS NEEDED !!!!

                array_1d<double, 3> radius;
                radius[0] = rConditionGeometry[0].X() - tip_center[0];
                radius[1] = rConditionGeometry[0].Y() - tip_center[1];
                radius[2] = rConditionGeometry[0].Z() - tip_center[2];
                double distance1 = norm_2(radius);

                radius[0] = rConditionGeometry[1].X() - tip_center[0];
                radius[1] = rConditionGeometry[1].Y() - tip_center[1];
                radius[2] = rConditionGeometry[1].Z() - tip_center[2];

                double distance2 = norm_2(radius);

                // TO SPLIT DETECTION START
                //If a node is detected in the wall tip is set TO_SPLIT
                //the criteria to splitting will be applied later in the nodes marked as TO_SPLIT

                if ((1 - factor_for_tip_radius) * tool_radius < distance1 && distance1 < (1 + factor_for_tip_radius) * tool_radius)
                  rConditionGeometry[0].Set(TO_SPLIT);

                if ((1 - factor_for_tip_radius) * tool_radius < distance2 && distance2 < (1 + factor_for_tip_radius) * tool_radius)
                  rConditionGeometry[1].Set(TO_SPLIT);

                // TO SPLIT DETECTION END

                // ACTIVE CONTACT DETECTION START

                contact_active = mMesherUtilities.CheckContactActive(rConditionGeometry, contact_semi_active, semi_active_nodes);
                if (contact_active)
                {
                  rLocalRefineInfo.number_of_active_contacts++;
                }

                // ACTIVE CONTACT DETECTION END

                side_length = GeometryUtilities::CalculateSideLength(rConditionGeometry[0], rConditionGeometry[1]);

                if (side_length > size_for_tip_contact_side)
                {

                  if (((1 - factor_for_tip_radius) * tool_radius < distance1 && (1 - factor_for_tip_radius) * tool_radius < distance2) &&
                      (distance1 < (1 + factor_for_tip_radius) * tool_radius && distance2 < (1 + factor_for_tip_radius) * tool_radius))
                  {
                    radius_insert = true;
                  }
                  // else if( side_length > size_for_tip_contact_side &&
                  // 	       ( distance1 < (1 + (side_size_factor+factor_for_tip_radius))*tool_radius && distance2 < (1 + (side_size_factor+factor_for_tip_radius))*tool_radius) ) {

                  // 	size_insert = true;
                  // 	std::cout<<" insert on radius-size "<<std::endl;
                  // }
                }

                if (radius_insert)
                {

                  if (!contact_active)
                  {

                    radius_insert = false;
                    // std::cout<<" contact_not_active "<<std::endl;
                    // double& nodal_h1 = rConditionGeometry[0].FastGetSolutionStepValue(NODAL_H);
                    // double& nodal_h2 = rConditionGeometry[1].FastGetSolutionStepValue(NODAL_H);
                    // double& nodal_h0 = MasterNode.FastGetSolutionStepValue( NODAL_H );

                    // double side = norm_2(rConditionGeometry[0]-rConditionGeometry[1]);
                    // // double d1 = GeometryUtilities::FindBoundaryH (rConditionGeometry[0]);
                    // // double d2 = GeometryUtilities::FindBoundaryH (rConditionGeometry[1]);
                    // // double d0 = GeometryUtilities::FindBoundaryH (MasterNode);
                    // // double size_master = nodal_h0;

                    // bool candidate =false;
                    // if( ((nodal_h1+nodal_h2)*0.5) > factor_for_non_tip_side * nodal_h0 ){
                    //   candidate = true;
                    // }

                    // double crit_factor = 2;
                    // if( (side > size_for_non_tip_contact_side) && candidate ){
                    //   radius_insert = true;
                    // }
                  }
                }
              }
              else
              { //refine boundary with nodal_h sizes to large

                double &nodal_h1 = rConditionGeometry[0].FastGetSolutionStepValue(NODAL_H);
                double &nodal_h2 = rConditionGeometry[1].FastGetSolutionStepValue(NODAL_H);
                double &nodal_h0 = MasterNode.FastGetSolutionStepValue(NODAL_H);

                double side = norm_2(rConditionGeometry[0] - rConditionGeometry[1]);
                // double d1 = GeometryUtilities::FindBoundaryH (rConditionGeometry[0]);
                // double d2 = GeometryUtilities::FindBoundaryH (rConditionGeometry[1]);
                // double d0 = GeometryUtilities::FindBoundaryH (MasterNode);
                // double size_master = nodal_h0;

                bool candidate = false;
                if (((nodal_h1 + nodal_h2) * 0.5) > factor_for_non_tip_side * nodal_h0)
                {
                  candidate = true;
                }

                if ((side > size_for_non_tip_contact_side) && candidate)
                {
                  size_insert = true;
                }
              }

              if (radius_insert || size_insert) //Boundary must be rebuild
              {

                //std::cout<<"   CONTACT DOMAIN ELEMENT REFINED "<<i_cond->Id()<<std::endl;

                new_point.X() = 0.5 * (rConditionGeometry[1].X() + rConditionGeometry[0].X());
                new_point.Y() = 0.5 * (rConditionGeometry[1].Y() + rConditionGeometry[0].Y());
                new_point.Z() = 0.5 * (rConditionGeometry[1].Z() + rConditionGeometry[0].Z());

                new_point.SetId(i_cond->Id()); //set condition Id

                //Condition *ContactMasterCondition = i_cond->GetValue(MAIN_CONDITION).get();

                if ((rConditionGeometry[0].Is(TO_SPLIT) && rConditionGeometry[1].Is(TO_SPLIT)))
                  tool_project = true;

                if ((rConditionGeometry[0].Is(TO_SPLIT) || rConditionGeometry[1].Is(TO_SPLIT)) && contact_active)
                  tool_project = true;

                if (tool_project) //master node in tool -->  refine workpiece  // (tool_radius ==0 in workpiece nodes)
                {

                  if (new_point.Y() < (tip_center[1]) && new_point.Y() > (tip_center[1] - tool_radius))
                  {

                    // std::cout<<"   new_point  ("<<new_point.X()<<", "<<new_point.Y()<<") "<<std::endl;
                    // std::cout<<"   tip_center ("<<tip_center[0]<<", "<<tip_center[1]<<") radius "<<tool_radius<<std::endl;

                    array_1d<double, 3> tip_normal = tip_center - new_point;

                    if (norm_2(tip_normal) < tool_radius * 0.95)
                    { //if is in the tool tip
                      tip_normal -= (tool_radius / norm_2(tip_normal)) * tip_normal;
                      if (norm_2(tip_normal) < tool_radius * 0.05)
                        new_point += tip_normal;

                      // std::cout<<"   A: Tool Tip Correction COND ("<<ContactMasterCondition->Id()<<") "<<std::endl;
                      // std::cout<<"   new_point ("<<new_point.X()<<", "<<new_point.Y()<<") "<<std::endl;
                    }
                  }
                }

                if (radius_insert)
                  rLocalRefineInfo.number_contact_tip_insertions++;
                if (size_insert)
                  rLocalRefineInfo.number_contact_size_insertions++;

                // std::cout<<"   MasterCondition RELEASED (Id: "<<ContactMasterCondition->Id()<<") "<<std::endl;
                //ContactMasterCondition->Set(TO_ERASE);
                std::cout<<" Set Master Condition TO_ERASE in refine_conditions_in_contact_mesher_process.hpp "<<std::endl;
                list_of_points.push_back(new_point);
                //list_of_conditions.push_back(ContactMasterCondition);
              }
            }

            rLocalRefineInfo.number_of_contacts++;
          }
          // else{

          //   std::cout<<"   Master Condition not found "<<std::endl;

          // }

          rLocalRefineInfo.number_of_contact_conditions++;
        }
      }

      // std::cout<<"   [ Contact Conditions : "<<rLocalRefineInfo.number_of_contact_conditions<<", (contacts in domain: "<<rLocalRefineInfo.number_of_contacts<<", of them active: "<<rLocalRefineInfo.number_of_active_contacts<<") ] "<<std::endl;
      // std::cout<<"   Contact Search End ["<<list_of_conditions.size()<<" : "<<list_of_points.size()<<"]"<<std::endl;

      return true;

      KRATOS_CATCH(" ")
	}

    //*******************************************************************************************
    //*******************************************************************************************

    bool RefineOtherBoundary(ModelPart &rModelPart, std::vector<Point> &list_of_points, std::vector<Condition *> &list_of_conditions, RefineCounters &rLocalRefineInfo)
    {

      KRATOS_TRY

        const ProcessInfo &rCurrentProcessInfo = rModelPart.GetProcessInfo();

      //***SIZES :::: parameters do define the tolerance in mesh size:

      //RIGID WALL CONTACT:
      double factor_for_tip_radius = 0.20; //deformable contact tolerance in radius for detection tip sides to refine

      double factor_for_wall_tip_contact_side = 0.50;
      double factor_for_wall_semi_tip_contact_side = 0.75; // semi contact or contact which a node in a tip
      double factor_for_wall_non_tip_contact_side = 1.50;  // semi contact or contact which no node in a tip

      //NON CONTACT:
      double factor_for_energy_side = 1.50; // non contact side which dissipates energy
      double factor_for_non_contact_side = 3.50;

      double tool_radius = 0;
      double side_length = 0;
      //double plastic_power = 0;

      bool radius_insert = false;
      bool energy_insert = false;
      bool mesh_size_insert = false;
      bool contact_active = false;
      bool contact_semi_active = false;
      bool tool_project = false;

      Node<3> new_point(0, 0.0, 0.0, 0.0);

      std::vector<bool> semi_active_nodes;

      //LOOP TO CONSIDER ALL SUBDOMAIN CONDITIONS
      //double cond_counter = 0;
      for (auto i_cond(rModelPart.ConditionsBegin()); i_cond != rModelPart.ConditionsEnd(); ++i_cond)
      {
          //cond_counter++;
          bool refine_candidate = false;
          if (mrRemesh.Options.Is(MesherData::CONSTRAINED))
            {
              if (i_cond->Is(BOUNDARY)) //ONLY SET TO THE BOUNDARY SKIN CONDITIONS (CompositeCondition)
                refine_candidate = true;
              else
                refine_candidate = false;
            }
          else
            {
              if (mrRemesh.IsBoundary(RefineData::REFINE))
                refine_candidate = true;
            }

          MesherData MeshData;
          RefineData& Refine = MeshData.GetRefineData(*i_cond, mrRemesh, rCurrentProcessInfo);

          //CONTACT:
          double size_for_wall_tip_contact_side = factor_for_wall_tip_contact_side * Refine.Boundary.DistanceMeshSize;
          double size_for_wall_semi_tip_contact_side = factor_for_wall_semi_tip_contact_side * Refine.Boundary.DistanceMeshSize; // semi contact or contact which a node in a tip
          double size_for_wall_non_tip_contact_side = factor_for_wall_non_tip_contact_side * Refine.Boundary.DistanceMeshSize;  // semi contact or contact which no node in a tip

          //NON CONTACT:
          double size_for_energy_side = factor_for_energy_side * Refine.Boundary.DistanceMeshSize; // non contact side which dissipates energy
          double size_for_non_contact_side = factor_for_non_contact_side * Refine.Boundary.DistanceMeshSize;

          if (refine_candidate)
            {

              radius_insert = false;
              energy_insert = false;
              mesh_size_insert = false;
              tool_project = false;
              contact_active = false;
              contact_semi_active = false;

              side_length = 0;
              tool_radius = 0;
              //plastic_power = 0;

              //double condition_radius = 0;
              Geometry<Node<3>> rConditionGeometry;
              array_1d<double, 3> tip_center;

              if (i_cond->IsNot(TO_ERASE))
                {

                  //*********************************************************************************
                  // RIGID CONTACT CONDITIONS ON TIP START
                  //*********************************************************************************

                  // TOOL TIP INSERT;

                  // ACTIVE CONTACT DETECTION START

                  rConditionGeometry = i_cond->GetGeometry();
                  contact_active = mMesherUtilities.CheckContactActive(rConditionGeometry, contact_semi_active, semi_active_nodes);

                  // ACTIVE CONTACT DETECTION END

                  if (contact_active)
                    {

                      side_length = GeometryUtilities::CalculateSideLength(rConditionGeometry[0], rConditionGeometry[1]);

                      if (side_length > size_for_wall_tip_contact_side)
                        {

                          bool on_tip = false;
                          if (rConditionGeometry[0].Is(TO_SPLIT) && rConditionGeometry[1].Is(TO_SPLIT))
                            {
                              on_tip = true;
                            }
                          else if (rConditionGeometry[0].Is(TO_SPLIT) || rConditionGeometry[1].Is(TO_SPLIT))
                            {
                              if (side_length > size_for_wall_tip_contact_side)
                                {
                                  on_tip = true;
                                }
                            }

                          bool on_radius = false;

                          if (on_tip && mRigidWalls.size())
                            {

                              int index = -1;
                              if (rConditionGeometry[0].Is(TO_SPLIT))
                                {
                                  index = 0;
                                  on_radius = true;
                                }
                              else if (rConditionGeometry[1].Is(TO_SPLIT))
                                {
                                  index = 1;
                                  on_radius = true;
                                }
                              else
                                {
                                  on_radius = false;
                                }

                              if (on_radius)
                                {
                                  const ProcessInfo &rCurrentProcessInfo = rModelPart.GetProcessInfo();
                                  double Time = rCurrentProcessInfo[TIME];
                                  for (unsigned int i = 0; i < mRigidWalls.size(); ++i)
                                    {
                                      if (mRigidWalls[i]->IsInside(rConditionGeometry[index], Time))
                                        {
                                          tool_radius = mRigidWalls[i]->GetRadius(rConditionGeometry[index].Coordinates());
                                          tip_center = mRigidWalls[i]->GetCenter(rConditionGeometry[index].Coordinates());
                                          break;
                                        }
                                    }
                                }
                            }

                          if (on_radius && on_tip) //master node in tool -->  refine workpiece  // (tool_radius ==0 in workpiece nodes)
                            {
                              Node<3> center(0, tip_center[0], tip_center[1], tip_center[2]);
                              array_1d<double, 3> radius;
                              radius[0] = rConditionGeometry[0].X() - center.X();
                              radius[1] = rConditionGeometry[0].Y() - center.Y();
                              radius[2] = rConditionGeometry[0].Z() - center.Z();

                              double distance1 = norm_2(radius);

                              radius[0] = rConditionGeometry[1].X() - center.X();
                              radius[1] = rConditionGeometry[1].Y() - center.Y();
                              radius[2] = rConditionGeometry[1].Z() - center.Z();

                              double distance2 = norm_2(radius);

                              if (((1 - factor_for_tip_radius) * tool_radius < distance1 && (1 - factor_for_tip_radius) * tool_radius < distance2) &&
                                  (distance1 < (1 + factor_for_tip_radius) * tool_radius && distance2 < (1 + factor_for_tip_radius) * tool_radius))
                                {
                                  radius_insert = true;
                                }
                            }
                        }
                    }

                  //*********************************************************************************
                  // RIGID CONTACT CONDITIONS ON TIP END
                  //*********************************************************************************

                  //*********************************************************************************
                  // FREE BOUNDARY CONDITIONS ENERGY INSERTION START
                  //*********************************************************************************

                  // ENERGY INSERT

                  unsigned int vsize = i_cond->GetValue(PRIMARY_ELEMENTS).size();

                  if (!radius_insert && Refine.Boundary.Options.Is(RefineData::REFINE_ON_THRESHOLD) && vsize > 0)
                    {

                      Element &MasterElement = i_cond->GetValue(PRIMARY_ELEMENTS)[vsize - 1];

                      for(auto& variable : Refine.Boundary.ThresholdVariables)
                      {
                        std::vector<double> Value;

                        MasterElement.CalculateOnIntegrationPoints(*variable.first, Value, rCurrentProcessInfo);

                        //calculate threshold value (plastic power)
                        double threshold_value = 0;

                        for (std::vector<double>::iterator v = Value.begin(); v != Value.end(); ++v)
                          threshold_value += *v;

                        threshold_value /= double(Value.size());
                        threshold_value *= MasterElement.GetGeometry().DomainSize();

                        //calculate condition length
                        double face_size = GeometryUtilities::CalculateSideLength(rConditionGeometry[0], rConditionGeometry[1]);

                        // if( threshold_value > 0 ){
                        //   std::cout<<" threshold_value ["<<pCondition->Id()<<"]"<<threshold_value<<" vs "<<variable.second<<" domain_size "<<MasterElement.GetGeometry().DomainSize()<<" "<<threshold_value/MasterElement.GetGeometry().DomainSize()<<std::endl;
                        //   std::cout<<" face_size ["<<pCondition->Id()<<"]"<<face_size<<" vs "<<critical_size<<std::endl;
                        // }

                        double critical_threshold = variable.second * MasterElement.GetGeometry().DomainSize();
                        if (threshold_value > critical_threshold && face_size > size_for_energy_side)
                          energy_insert = true;
                      }
                    }

                  //*********************************************************************************
                  // FREE BOUNDARY CONDITIONS ENERGY INSERTION END
                  //*********************************************************************************

                  //*********************************************************************************
                  // FREE BOUNDARY CONDITIONS SIZE INSERTION
                  //*********************************************************************************

                  // BOUNDARY SIZE INSERT

                  if ((!radius_insert || !energy_insert) && vsize > 0)
                    {

                      Element &MasterElement = i_cond->GetValue(PRIMARY_ELEMENTS)[vsize - 1];

                      //std::cout<<" MAIN_ELEMENT "<<MasterElement.Id()<<std::endl;

                      Geometry<Node<3>> &vertices = MasterElement.GetGeometry();
                      double Alpha = mrRemesh.AlphaParameter;

                      bool accepted = GeometryUtilities::AlphaShape(Alpha, vertices, 2);

                      //condition_radius is side_length
                      side_length = GeometryUtilities::CalculateSideLength(rConditionGeometry[0], rConditionGeometry[1]);

                      //condition_radius = GeometryUtilities::CalculateTriangleRadius (pGeom);
                      double critical_side_size = 0;

                      bool on_tip = false;
                      if (contact_semi_active)
                        {

                          if (rConditionGeometry[0].Is(TO_SPLIT) || rConditionGeometry[1].Is(TO_SPLIT))
                            on_tip = true;

                          if (on_tip == true)
                            critical_side_size = size_for_wall_semi_tip_contact_side;
                          else
                            critical_side_size = size_for_wall_non_tip_contact_side;
                        }
                      else if (contact_active)
                        {

                          if (rConditionGeometry[0].Is(TO_SPLIT) || rConditionGeometry[1].Is(TO_SPLIT))
                            on_tip = true;

                          if (on_tip == true)
                            critical_side_size = size_for_wall_semi_tip_contact_side;
                          else
                            critical_side_size = size_for_wall_non_tip_contact_side;
                        }
                      else
                        {

                          critical_side_size = size_for_non_contact_side;
                        }

                      if (!accepted && !contact_semi_active && !contact_active && side_length > critical_side_size)
                        {
                          mesh_size_insert = true;

                          // std::cout<<"   insert on mesh size "<<std::endl;
                        }
                      else if (contact_semi_active && side_length > critical_side_size)
                        {
                          mesh_size_insert = true;

                          // std::cout<<"   insert on mesh size semi_contact "<<std::endl;
                        }
                      else if (contact_active && side_length > critical_side_size)
                        {
                          mesh_size_insert = true;

                          // std::cout<<"   insert on mesh size on contact "<<std::endl;
                        }
                    }

                  //*********************************************************************************
                  //                   BOUNDARY REBUILD START                                      //
                  //*********************************************************************************

                  if (radius_insert || energy_insert || mesh_size_insert) //Boundary must be rebuild
                    {

                      // std::cout<<"   BOUNDARY DOMAIN ELEMENT REFINED "<<i_cond->Id()<<std::endl;

                      new_point.X() = 0.5 * (rConditionGeometry[1].X() + rConditionGeometry[0].X());
                      new_point.Y() = 0.5 * (rConditionGeometry[1].Y() + rConditionGeometry[0].Y());
                      new_point.Z() = 0.5 * (rConditionGeometry[1].Z() + rConditionGeometry[0].Z());

                      if (this->mEchoLevel > 0)
                        {
                          std::cout << "   radius_insert " << radius_insert << " energy_insert " << energy_insert << " mesh_size_insert " << mesh_size_insert << std::endl;
                          std::cout << "   NEW NODE  " << new_point << std::endl;
                        }

                      new_point.SetId(i_cond->Id()); //set condition Id

                      //it will be good if the node is detected in the tool tip using the rigid contact standards:

                      if ((rConditionGeometry[0].Is(TO_SPLIT) && rConditionGeometry[1].Is(TO_SPLIT)))
                        tool_project = true;

                      if ((rConditionGeometry[0].Is(TO_SPLIT) || rConditionGeometry[1].Is(TO_SPLIT)) && contact_active)
                        tool_project = true;

                      if ((rConditionGeometry[0].Is(TO_SPLIT) || rConditionGeometry[1].Is(TO_SPLIT)) && contact_semi_active)
                        tool_project = true;

                      bool on_radius = false;

                      if (tool_project && mRigidWalls.size())
                        {

                          int index = -1;
                          if (rConditionGeometry[0].Is(TO_SPLIT))
                            {
                              index = 0;
                              on_radius = true;
                            }
                          else if (rConditionGeometry[1].Is(TO_SPLIT))
                            {
                              index = 1;
                              on_radius = true;
                            }
                          else
                            {
                              on_radius = false;
                            }

                          if (on_radius)
                            {

                              const ProcessInfo &rCurrentProcessInfo = rModelPart.GetProcessInfo();
                              double Time = rCurrentProcessInfo[TIME];
                              for (unsigned int i = 0; i < mRigidWalls.size(); ++i)
                                {
                                  if (mRigidWalls[i]->IsInside(rConditionGeometry[index], Time))
                                    {
                                      tool_radius = mRigidWalls[i]->GetRadius(rConditionGeometry[index].Coordinates());
                                      tip_center = mRigidWalls[i]->GetCenter(rConditionGeometry[index].Coordinates());
                                      break;
                                    }
                                }
                            }

                          if (on_radius)
                            {

                              if (new_point.Y() < (tip_center[1]) && new_point.Y() > (tip_center[1] - tool_radius))
                                {

                                  array_1d<double, 3> tip_normal = tip_center - new_point;

                                  if (norm_2(tip_normal) < tool_radius)
                                    { //if is in the tool tip

                                      tip_normal -= (tool_radius / norm_2(tip_normal)) * tip_normal;

                                      if (norm_2(tip_normal) < tool_radius * 0.08)
                                        new_point += tip_normal;
                                    }
                                }

                              if (this->mEchoLevel > 0)
                                std::cout << "   TOOL PROJECT::on radius  " << new_point << std::endl;
                            }
                        }

                      if (radius_insert)
                        rLocalRefineInfo.number_of_tip_bounds++;
                      if (energy_insert)
                        rLocalRefineInfo.number_of_energy_bounds++;
                      if (mesh_size_insert)
                        rLocalRefineInfo.number_of_exterior_bounds++;

                      i_cond->Set(TO_ERASE);
                      std::cout<<" Set Condition TO_ERASE in refine_doncitions_in_contact_mesher_process.hpp "<<std::endl;

                      if (this->mEchoLevel > 0)
                        std::cout << "   INSERTED NODE  " << new_point << std::endl;

                      list_of_points.push_back(new_point);
                      list_of_conditions.push_back((*i_cond.base()).get());

                      // if( this->mEchoLevel > 0 ){
                      //   std::cout<<"   Refine Boundary  (Id:"<<i_cond->Id()<<"): ["<<rConditionGeometry[0].Id()<<", "<<rConditionGeometry[1].Id()<<"]"<<std::endl;
                      //   std::cout<<"   (x1:"<<rConditionGeometry[0].X()<<", y1: "<<rConditionGeometry[0].Y()<<") "<<" (x2:"<<rConditionGeometry[1].X()<<", y2: "<<rConditionGeometry[1].Y()<<") "<<std::endl;

                      //   //std::cout<<" Added Node [Rcrit:"<<condition_radius<<",Scrit:"<<side_length<<",PlasticPower:"<<plastic_power<<"]"<<std::endl;
                      //   std::cout<<"   Added Node [Scrit:"<<side_length<<",PlasticPower:"<<plastic_power<<"]"<<std::endl;
                      // }
                    }

                  //*********************************************************************************
                  //                   BOUNDARY REBUILD END                                        //
                  //*********************************************************************************
                }
              else
                {
                  if (this->mEchoLevel > 0)
                    std::cout << " Condition " << i_cond->Id() << " Released " << std::endl;
                }
            }
        }

      return true;

      KRATOS_CATCH(" ")
	}

    /// Assignment operator.
    RefineConditionsInContactMesherProcess &operator=(RefineConditionsInContactMesherProcess const &rOther);

    /// this function is a private function

    /// Copy constructor.
    //Process(Process const& rOther);

    ///@}

  }; // Class Process

  ///@}

  ///@name Type Definitions
  ///@{

  ///@}
  ///@name Input and output
  ///@{

  /// input stream function
  inline std::istream &operator>>(std::istream &rIStream,
                                  RefineConditionsInContactMesherProcess &rThis);

  /// output stream function
  inline std::ostream &operator<<(std::ostream &rOStream,
                                  const RefineConditionsInContactMesherProcess &rThis)
  {
    rThis.PrintInfo(rOStream);
    rOStream << std::endl;
    rThis.PrintData(rOStream);

    return rOStream;
  }
  ///@}

} // namespace Kratos.

#endif // KRATOS_REFINE_CONDITIONS_IN_CONTACT_MESHER_PROCESS_HPP_INCLUDED  defined
