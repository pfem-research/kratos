//
//   Project Name:        KratosMeshersApplication $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:        April 2018 $
//
//

#if !defined(KRATOS_SELECT_FLUID_ELEMENTS_MESHER_PROCESS_HPP_INCLUDED)
#define KRATOS_SELECT_FLUID_ELEMENTS_MESHER_PROCESS_HPP_INCLUDED

// External includes

// System includes
#include <numeric>

// Project includes
#include "custom_processes/select_elements_mesher_process.hpp"

namespace Kratos
{

///@name Kratos Classes
///@{

/// Refine Mesh Elements Process 2D and 3D
/** The process labels the elements to be refined in the mesher
    it applies a size constraint to elements that must be refined.

*/
class SelectFluidElementsMesherProcess
    : public SelectElementsMesherProcess
{
public:
  ///@name Type Definitions
  ///@{

  typedef Node<3> NodeType;
  typedef Geometry<NodeType> GeometryType;

  /// Pointer definition of Process
  KRATOS_CLASS_POINTER_DEFINITION(SelectFluidElementsMesherProcess);

  ///@}
  ///@name Life Cycle
  ///@{

  /// Default constructor.
  SelectFluidElementsMesherProcess(ModelPart &rModelPart,
                                   MesherData::MeshingParameters &rRemeshingParameters,
                                   int EchoLevel)
      : SelectElementsMesherProcess(rModelPart, rRemeshingParameters, EchoLevel)
  {
  }

  /// Destructor.
  virtual ~SelectFluidElementsMesherProcess() {}

  ///@}
  ///@name Operators
  ///@{

  /// This operator is provided to call the process as a function and simply calls the Execute method.
  void operator()()
  {
    this->Execute();
  }

  ///@}
  ///@name Operations
  ///@{
  ///@}
  ///@name Access
  ///@{
  ///@}
  ///@name Inquiry
  ///@{
  ///@}
  ///@name Input and output
  ///@{

  /// Turn back information as a string.
  std::string Info() const override
  {
    return "SelectFluidElementsMesherProcess";
  }

  /// Print information about this object.
  void PrintInfo(std::ostream &rOStream) const override
  {
    rOStream << "SelectFluidElementsMesherProcess";
  }

  /// Print object's data.
  void PrintData(std::ostream &rOStream) const override
  {
  }

  ///@}
  ///@name Friends
  ///@{
  ///@}

protected:
  ///@name Protected static Member Variables
  ///@{
  ///@}
  ///@name Protected member Variables
  ///@{
  ///@}
  ///@name Protected Operators
  ///@{
  ///@}
  ///@name Protected Operations
  ///@{

  void SelectElements() override
  {
    KRATOS_TRY

    const int &OutNumberOfElements = mrRemesh.OutMesh.GetNumberOfElements();

    if (OutNumberOfElements==0)
      KRATOS_ERROR << " 0 elements to select : NO ELEMENTS GENERATED " <<std::endl;

    //set flags for the local process execution marking slivers as SELECTED
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{SELECTED}}, {SELECTED.AsFalse()});

    //set full rigid element particles only surrounded by full rigid elements as VISITED
    this->LabelEdgeNodes(mrModelPart);

    unsigned int dimension = 0;
    unsigned int number_of_vertices = 0;
    this->GetElementDimension(dimension, number_of_vertices);

    const int *OutElementList = mrRemesh.OutMesh.GetElementList();

    ModelPartUtilities::CheckNodalH(mrModelPart);

    // LevelSetUtilities LevelSet;
    // LevelSet.CreateLevelSetFunction(mrModelPart, 0.001);

    const ProcessInfo &rCurrentProcessInfo = mrModelPart.GetProcessInfo();
    const double &TimeStep = rCurrentProcessInfo[DELTA_TIME];

    ModelPart::NodesContainerType::iterator nodes_begin = mrModelPart.NodesBegin();

    std::vector<unsigned int> slivers(OutNumberOfElements,0);
    std::vector<bool> Preserved(OutNumberOfElements,false);
    //std::vector<array_1d<double,3>> PointsLevelSet(OutNumberOfElements);
    int el = 0;
#pragma omp parallel for
    for (el = 0; el < OutNumberOfElements; ++el)
    {
      GeometryType Vertices;
      NodalFlags VerticesFlags;

      bool accepted = this->GetVertices(el, nodes_begin, OutElementList, VerticesFlags, Vertices, number_of_vertices);

      double Size  = this->GetReferenceSize(Vertices, VerticesFlags, mrRemesh, rCurrentProcessInfo);
      double Alpha = this->GetAlphaParameter(Vertices, VerticesFlags, Size, dimension, TimeStep);

      //1.- directly dismiss Fluid elements if criterion not fulfilled
      if (accepted)
	accepted = this->CheckElementAtBoundaries(Vertices, VerticesFlags, Size, dimension, TimeStep);

      //2.- to control the alpha size
      if(accepted)
	accepted=GeometryUtilities::AlphaShape(Alpha, Vertices, dimension, 4.0 * Size);

      //3.- to control that the element is inside of the domain boundaries
      //accepted=GeometryUtilities::CheckInnerCentre(Vertices); //problems in 3D: when slivers are released, a boundary is created and the normals calculated, then elements that are inside suddently its center is calculated as outside... // some corrections are needded.

      //4.- to control that the element has a good shape
      if(accepted)
	accepted = this->CheckElementShape(Vertices, VerticesFlags, Size, dimension, TimeStep, slivers[el]);

      //PointsLevelSet[el] = Vertices.Center();

      // if all checks have been passed, accept the element
      Preserved[el] = accepted;
    }

    //LevelSet.PrintPointsXYQ(mrModelPart, PointsLevelSet, "LevelSet" );


    unsigned int number_of_slivers = std::accumulate(slivers.begin(), slivers.end(), 0);
    if (number_of_slivers>=0)
      this->MarkSliverNodes(slivers, number_of_vertices);


    unsigned int counter = 0;
    for (el = 0; el < Preserved.size(); ++el)
    {
      if (Preserved[el])
	mrRemesh.PreservedElements[el] =++counter;
    }
    mrRemesh.Info->Current.Elements = counter;


    if (mEchoLevel > 0)
      std::cout << "  [Preserved Elements " << mrRemesh.Info->Current.Elements << "]  Total out: " <<OutNumberOfElements<< " slivers "<< number_of_slivers << std::endl;

    this->CheckRigidElementNeighbours();

    this->SelectNodesToErase();

    //set flags for the local process execution marking slivers
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{SELECTED}}, {SELECTED.AsFalse()});

    KRATOS_CATCH("")
  }


  //*******************************************************************************************
  //*******************************************************************************************


  void MarkSliverNodes(const std::vector<unsigned int> &slivers, unsigned int &number_of_vertices)
  {
    ModelPart::NodesContainerType::iterator nodes_begin = mrModelPart.NodesBegin();
    const int *OutElementList = mrRemesh.OutMesh.GetElementList();

    int el = 0;

#pragma omp parallel for
    for (el = 0; el < slivers.size(); ++el)
    {
      if (slivers[el]>0){
	GeometryType Vertices;
	NodalFlags VerticesFlags;
	this->GetVertices(el, nodes_begin, OutElementList, VerticesFlags, Vertices, number_of_vertices);
	for (auto &i_node : Vertices)
	  i_node.Set(MARKER);
      }

    }
  }


  //*******************************************************************************************
  //*******************************************************************************************

  //it can set TO_ERASE to new entities inserted in full rigid elements
  bool CheckElementAtBoundaries(GeometryType &rVertices, const NodalFlags &rVerticesFlags, const double &rSize, const unsigned int &rDimension, const double &rTimeStep)
  {
    unsigned int number_of_vertices = rVertices.size();

    //a.- Check elements with all nodes at a fixed or solid boundary

    //0.- do not accept non fluid elements
    if(rVerticesFlags.Fluid == 0)
      return false;

    //1.- do not accept full solid elements
    if (rVerticesFlags.Solid == number_of_vertices)
      return false;

    //2.- do not accept full rigid-solid elements
    if (rVerticesFlags.Structure == number_of_vertices){
      // here, there are fluid nodes
      if (rVerticesFlags.Visited == number_of_vertices) //release isolated elements in walls
        return false;
      else if ( (rVerticesFlags.Visited !=0 || rVerticesFlags.FreeSurface ==0) && rVerticesFlags.Fluid != number_of_vertices) //release new isolated elements in walls
        return false;
      else if (rDimension == 2)
        if(!GeometryUtilities::CheckInnerCentre(rVertices))
          return false;
    }

    //b.- Check elements that have been created with rigid and solid walls

    if (rVerticesFlags.Fluid != number_of_vertices){ //they have particles which are not fluid yet

      if ( KinematicUtilities::CheckMeshVelocity(rVertices) ) //ALE : check if mesh_velocity is higher than the velocity -> do not accept
        return false;

    }


    //1.- Check layer elements with walls
    if (rVerticesFlags.FreeSurface > 0 && rVerticesFlags.Structure > 0 && rVerticesFlags.Interaction > 0)
      if (KinematicUtilities::CheckMeanPressure(rVertices)>1e6) // semiisolated element with large pressure errors -> do not accept
        return false;

    //2.- Check elements with a free_surface face
    if (rVerticesFlags.FreeSurface >= number_of_vertices-1){
      if (GeometryUtilities::CalculateMinEdgeSize(rVertices) < 0.5*rSize) //check free_surface needles -> do not accept
        return false;

      //check 3d faces looking at the meniscus (to review :: too complex)
      //return this->CheckElementAtFreeSurface(rVertices, rVerticesFlags, rSize, rDimension, rTimeStep);
    }

    if (!CheckFreeSurfaceElementsWithIsolatedParticles(rVertices, rVerticesFlags, rSize, rDimension, rTimeStep))
      return false;

    return true;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckFreeSurfaceElementsWithIsolatedParticles(GeometryType &rVertices, const NodalFlags &rVerticesFlags, const double &rSize, const unsigned int &rDimension, const double &rTimeStep)
  {
    unsigned int NumberOfVertices = rVertices.size();

    //avoid penetrating/scaping isolated nodes at large speeds and full free surface fluid nodes at large speeds
    bool to_preserve = true;

    if (rVerticesFlags.Isolated > 0)
    {
      if ((rVerticesFlags.Isolated + rVerticesFlags.FreeSurface) == NumberOfVertices)
      {
        const double MaxRelativeVelocity = 10; //arbitrary value :: (try a criteria depending on time and size)
        if (KinematicUtilities::CheckRelativeApproach(rVertices, MaxRelativeVelocity, rTimeStep))
        {
          //set isolated nodes to erase if they approach too fast
          for (unsigned int i = 0; i < rVertices.size(); ++i)
          {
            if (rVertices[i].Is(ISOLATED))
            {
              //rVertices[i].Set(TO_ERASE); // commented to not erase ISOLATED nodes
              std::cout<<" ISOLATED FLUID node ["<<rVertices[i].Id()<<"] fast approaching NODE NOT KEPT in select_elements_mesher_process.hpp"<<std::endl;
            }

          }
        }
      }

    }

    return to_preserve;

  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckFreeSurfaceElements(GeometryType &rVertices, const NodalFlags &rVerticesFlags, const double &rSize, const unsigned int &rDimension, const double &rTimeStep)
  {
    unsigned int NumberOfVertices = rVertices.size();

    if (rVerticesFlags.NoWallFreeSurface == NumberOfVertices)
    {
      const double MaxRelativeVelocity = 4.5; //arbitrary value, will depend on time step (AF)
      if(GeometryUtilities::CheckRelativeVelocities(rVertices, MaxRelativeVelocity) ){
        KRATOS_WARNING("")<<" Attention two free surfaces approaching at high speed "<<std::endl;
      }
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  double GetAlphaParameter(GeometryType &rVertices, const NodalFlags &rVerticesFlags, const double &rSize, const unsigned int &rDimension, const double &rTimeStep) override
  {

    if (Alpha != 0)
    {
      if (rDimension == 2)
      {
        this->GetTriangleFluidElementSimplerAlpha(Alpha, rVertices, rVerticesFlags, rSize, rDimension, rTimeStep);
      }
      else if (rDimension == 3)
      {
        //this->GetTetrahedronFluidElementAlpha(Alpha, rVertices, rVerticesFlags, rSize, rDimension, rTimeStep);
        this->GetTetrahedronFluidElementSimplerAlpha(Alpha,rVertices,rVerticesFlags, rSize, rDimension,rTimeStep);
      }
    }

    return Alpha;

  }

  //*******************************************************************************************
  //*******************************************************************************************

  void GetTriangleFluidElementSimplerAlpha(double &rAlpha, GeometryType &rVertices, const NodalFlags &rVerticesFlags, const double &rSize, const unsigned int &rDimension, const double &rTimeStep)
  {
    unsigned int NumberOfVertices = rVertices.size();

    if (rVerticesFlags.Fluid != NumberOfVertices)
    { //element formed with a wall
      if (rVerticesFlags.Structure == NumberOfVertices)
        rAlpha = 1.0;
      else
        rAlpha = 0.7;
    }
    else if (rVerticesFlags.Fluid == rVerticesFlags.FreeSurface) // here will be better to check coplanarity (compare edge sizes)
    {
      if (rVerticesFlags.Structure>0)
        rAlpha = 0.8;
      else
        rAlpha = 1.4;

      if (GeometryUtilities::CompareEdgeSizes(rVertices)>2.5)
        rAlpha = 0.7;
    }
    else
    { //all nodes are fluid (pre-existing elements) or new elements formed in the free-surface
      rAlpha = 1.4;
    }


    //criterion for elements formed with new wall nodes //pfemfluid element
    // if (rVerticesFlags.Structure == NumberOfVertices)
    // { //element formed with a wall
    //   rAlpha = 0.0;
    // }

    //criterion for isolated nodes forming new wall-fluid elements
    if ( rVerticesFlags.Isolated > 0 &&  (rVerticesFlags.Isolated + rVerticesFlags.Structure) == NumberOfVertices)
    {
      MesherUtilities MesherUtils;
      double VolumeIncrement = KinematicUtilities::GetDeformationGradientDeterminant(rVertices, rDimension, rTimeStep);
      if (VolumeIncrement > 1.0)
        rAlpha = 0.7;
    }

  }

  //*******************************************************************************************
  //*******************************************************************************************

  void GetTetrahedronFluidElementSimplerAlpha(double &rAlpha, GeometryType &rVertices, const NodalFlags &rVerticesFlags, const double &rSize, const unsigned int &rDimension, const double &rTimeStep)
  {
    MesherUtilities MesherUtils;
    double VolumeChange = 0;
    double VolumeTolerance = 2.5e-3 * pow(4.0 * rSize , rDimension);
    unsigned int NumberOfVertices = rVertices.size();

    //reference alpha 2.0

    if (rVerticesFlags.Fluid != NumberOfVertices)
    { //element formed with a wall

      if (rVerticesFlags.Structure == NumberOfVertices) //new elements with walls
        rAlpha = 1.0;
      else
        rAlpha = 0.7;
      // //if the element is decreasing its volume:
      // //to avoid closing fluid voids with new elements
      // if (!KinematicUtilities::CheckVolumeDecrease(rVertices, rDimension, rTimeStep, VolumeTolerance, VolumeChange))
      //   rAlpha *= 0.50;
      // else
      //   rAlpha *= 0.70;
      // if (VolumeChange == 0)
      //   rAlpha *= 0.30;
    }
    else if (rVerticesFlags.Fluid == rVerticesFlags.FreeSurface) // all fluid and free surface
    {
      if (rVerticesFlags.Structure>0)
        rAlpha = 0.8;
      else
        rAlpha = 1.4;
    }
    else
    { //inner
      rAlpha = 1.6;
      // if (GeometryUtilities::CheckInnerCentre(rVertices))
      // {
      //   rAlpha *= 2.0;
      // }
      // else
      // {
      //   rAlpha *= 0.70;
      // }
    }

    //criterion for elements formed with new wall nodes //pfemfluid element
    // if (rVerticesFlags.Structure == NumberOfVertices)
    // { //element formed with a wall
    //   rAlpha = 0.0;
    // }


    //criterion for isolated nodes forming new wall-fluid elements
    if ( rVerticesFlags.Isolated > 0 &&  (rVerticesFlags.Isolated + rVerticesFlags.Structure) == NumberOfVertices)
    {
      MesherUtilities MesherUtils;
      double VolumeIncrement = KinematicUtilities::GetDeformationGradientDeterminant(rVertices, rDimension, rTimeStep);
      if (VolumeIncrement > 1.0)
        rAlpha = 0.7;
    }

    // if (rVerticesFlags.Structure > 0  && rVerticesFlags.FreeSurface > 0)
    //   if (KinematicUtilities::CheckMeshVelocity(rVertices) && !KinematicUtilities::CheckVolumeDecrease(rVertices, rDimension, rTimeStep, VolumeTolerance, VolumeChange))
    //     rAlpha *= 0.4;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  void GetTriangleFluidElementAlpha(double &rAlpha, GeometryType &rVertices, const NodalFlags &rVerticesFlags, const double &rSize, const unsigned int &rDimension, const double &rTimeStep)
  {

    MesherUtilities MesherUtils;

    double VolumeChange = 0;
    double VolumeTolerance = 1.15e-4 * pow(4.0 * rSize , rDimension);
    unsigned int NumberOfVertices = rVertices.size();

    //criterion for elements formed with new wall nodes
    if (rVerticesFlags.Fluid != NumberOfVertices)
    { //element formed with a wall
      //there are not fluid nodes
      if (rVerticesFlags.Fluid == 0)
      {
        rAlpha = 0;
      }
      else
      {
        //if the element is decreasing its volume:
        VolumeChange = 0;
        if (KinematicUtilities::CheckVolumeDecrease(rVertices, rDimension, rTimeStep, VolumeTolerance, VolumeChange))
        {
          //accept new elements formed with isolated nodes (here speedy test passed)
          if (rVerticesFlags.Isolated > 0)
          {
            rAlpha *= 0.80;
          }
          //one wall vertex
          else if (rVerticesFlags.Structure == 1)
          {
            //to avoid new approaching wall elements with two non-wall free-surface nodes
            if (rVerticesFlags.NoWallFreeSurface == 2)
            {
              //std::cout<<" Vertices NWFS 2["<<rVertices[0].Id()<<","<<rVertices[1].Id()<<","<<rVertices[2].Id()<<"] ["<<rVertices[0].Is(RIGID)<<","<<rVertices[1].Is(RIGID)<<","<<rVertices[2].Is(RIGID)<<"]"<<std::endl;
              rAlpha *= 0.50;
            }
            else
              rAlpha *= 0.80;
          }
          //two wall vertices
          else if (rVerticesFlags.Structure == 2)
          {
            //to avoid new approaching wall elements with only one non-wall free-surface node
            if (rVerticesFlags.NoWallFreeSurface == 1)
            {
              //std::cout<<" Vertices NWFS 1["<<rVertices[0].Id()<<","<<rVertices[1].Id()<<","<<rVertices[2].Id()<<"] ["<<rVertices[0].Is(RIGID)<<","<<rVertices[1].Is(RIGID)<<","<<rVertices[2].Is(RIGID)<<"]"<<std::endl;
              rAlpha *= 0.50;
            }
            else
            {
              rAlpha *= 0.80;
            }
          }
          //there are no wall vertices and all nodes are not fluid (impossible/inserted nodes by the mesher)
          else
          {
            rAlpha *= 0.80;
            std::cout<<" WARNING: new element with non-fluid particles and non wall-particles (rigid: "<<rVerticesFlags.Rigid<<" solid: "<<rVerticesFlags.Solid<<" fluid: "<<rVerticesFlags.Fluid<<" free-surface: "<<rVerticesFlags.FreeSurface<<")"<<std::endl;
          }
        }
        //if the element is not decreasing its volume:
        else
        {
          //if the element does not change the volume (all rigid or moving tangentially to the wall)
          if (VolumeChange > 0 && VolumeChange < VolumeTolerance)
          {
            rAlpha *= 0.95;
            //std::cout<<" CONSIDERING VOLUME CHANGE "<<VolumeChange<<std::endl;
          }
          else
          {
            //std::cout<<" Vertices Volume ["<<rVertices[0].Id()<<","<<rVertices[1].Id()<<","<<rVertices[2].Id()<<"] ["<<rVertices[0].Is(RIGID)<<","<<rVertices[1].Is(RIGID)<<","<<rVertices[2].Is(RIGID)<<"]"<<VolumeChange<<std::endl;
            if (VolumeChange == 0)
              rAlpha *= 0.40;
            else
              rAlpha = 0;
          }
        }
      }
    }
    //all nodes are fluid (pre-existing elements) or new elements formed in the free-surface
    else
    { //fluid element
      //all nodes in the free-surface
      if (rVerticesFlags.FreeSurface == 3)
      {
        //all nodes in the non-wall free-surface
        if (rVerticesFlags.NoWallFreeSurface == 3)
        {
          //if the element is decreasing its volume:
          VolumeChange = 0;
          //to avoid closing fluid voids with new elements
          if (KinematicUtilities::CheckVolumeDecrease(rVertices, rDimension, rTimeStep, VolumeTolerance, VolumeChange))
          {
            //to avoid too avoid approaching free surfaces at very high speeds
            const double MaxRelativeVelocity = 1.5; //arbitrary value :: (try a criteria depending on time and size)
            if (KinematicUtilities::CheckRelativeVelocities(rVertices, MaxRelativeVelocity))
              rAlpha = 0;
            else
              rAlpha *= 0.80;
          }
          //to avoid increasing fluid surface with new elements
          else
          {
            rAlpha *= 0.60;
          }
        }
        //two nodes in non-wall free-surface
        else if (rVerticesFlags.NoWallFreeSurface == 2)
        {
          rAlpha *= 0.80;
        }
        //one node in non-wall free-surface
        else if (rVerticesFlags.NoWallFreeSurface == 1)
        {
          rAlpha *= 0.80;
        }
        //one node in non-wall free-surface
        else
        {
          rAlpha *= 1.20;
        }
      }
      //two nodes in the free-surface
      else if (rVerticesFlags.FreeSurface == 2)
      {

        //to avoid closing fluid voids with new elements with a wall fluid node
        if (rVerticesFlags.NoWallFreeSurface == 2 && rVerticesFlags.Structure == 1)
          rAlpha *= 0.80;
        else
          rAlpha *= 1.20;
      }
      else
      {
        rAlpha *= 1.20;
      }
    }
  }


  //*******************************************************************************************
  //*******************************************************************************************

  void GetTetrahedronFluidElementAlpha(double &rAlpha, GeometryType &rVertices, const NodalFlags &rVerticesFlags, const double &rSize, const unsigned int &rDimension, const double &rTimeStep)
  {
    MesherUtilities MesherUtils;

    double VolumeChange = 0;
    double VolumeTolerance = 2.5e-3 * pow(4.0 * rSize , rDimension);
    unsigned int NumberOfVertices = rVertices.size();

    //criterion for elements formed with new wall nodes
    if (rVerticesFlags.Fluid != NumberOfVertices)
    { //element formed with a wall

      //there are not fluid nodes
      if (rVerticesFlags.Fluid == 0)
      {
        rAlpha = 0;
      }
      else
      {
        //if the element is decreasing its volume:
        VolumeChange = 0;
        if (KinematicUtilities::CheckVolumeDecrease(rVertices, rDimension, rTimeStep, VolumeTolerance, VolumeChange))
        {
          //accept new elements formed with isolated nodes (here speedy test passed)
          if (rVerticesFlags.Isolated > 0)
          {
            rAlpha *= 0.9;
          }
          //one wall vertex
          else if (rVerticesFlags.Structure == 1)
          {
            //to avoid new approaching wall elements with three non-wall free-surface nodes
            if (rVerticesFlags.NoWallFreeSurface == 3)
              rAlpha *= 0.7;
            else
              rAlpha *= 0.8;
          }
          //two wall vertices
          else if (rVerticesFlags.Structure == 2)
          {
            //to avoid new approaching wall elements with two non-wall free-surface nodes
            if (rVerticesFlags.NoWallFreeSurface == 2)
              rAlpha *= 0.6;
            else
              rAlpha *= 0.90;
          }
          //three wall vertices
          else if (rVerticesFlags.Structure == 3)
          {
            //to avoid new approaching wall elements with only one non-wall free-surface node
            if (rVerticesFlags.NoWallFreeSurface == 1)
            {
              if (rVerticesFlags.Fluid == 1)
                rAlpha *= 0.5;
              else
                rAlpha *= 0.7;
            }
            else
            {
              rAlpha *= 0.9;
            }
          }
          //four wall vertices
          else if (rVerticesFlags.Structure)
          {
            rAlpha *= 1.2; //keep full rigid and fluid elements - will be released using other criterion if needed
          }
          //there are no wall vertices
          else
          {
            rAlpha *= 0.9;
            //std::cout<<" WARNING: new element with non-fluid particles and non wall-particles (rigid: "<<rVerticesFlags.Rigid<<" solid: "<<rVerticesFlags.Solid<<" fluid: "<<rVerticesFlags.Fluid<<" free-surface: "<<rVerticesFlags.FreeSurface<<" new_entity: "<<rVerticesFlags.NewEntity<<" isolated: "<<rVerticesFlags.Isolated<<" old_entity: "<<rVerticesFlags.Visited<<")"<<std::endl;
          }
        }
        //if the element is not decreasing its volume:
        else
        {
          //if the element does not change the volume (all rigid or moving tangentially to the wall)
          if (VolumeChange > 0 && VolumeChange < VolumeTolerance && rVerticesFlags.Rigid == NumberOfVertices)
          {
            rAlpha *= 0.90;
          }
          else
          {
            //if( !(VolumeChange == 0 && MesherUtils.CheckInnerCentre(rVertices)) ){//no moving elements VolumeChange == 0 are accepted if previously inside
            if (rVerticesFlags.Interaction + rVerticesFlags.Structure >= 4)
            {

              bool accepted = false;

              if (rVerticesFlags.Structure == 4)
              {
                accepted = true;
              }
              else
              {
                for (auto &i_node : rVertices)
                {
                  if (accepted)
                    break;
                  if (i_node.Is(INTERACTION))
                  {

                    for (auto &j_node : rVertices)
                    {
                      if (j_node.Is(RIGID))
                      {
                        double projection = inner_prod(i_node.FastGetSolutionStepValue(NORMAL), j_node.FastGetSolutionStepValue(NORMAL));
                        if (projection > 0.35)
                        {
                          accepted = true;
                          break;
                        }
                      }
                    }
                  }
                }
              }
              if (accepted)
                rAlpha *= 0.7;
              else
                rAlpha = 0;
            }
            else
            {
              rAlpha = 0;
            }
          }
        }
      }
    }
    //all nodes are fluid (pre-existing elements) or new elements formed inside or in the free-surface
    else
    { //fluid element
      VolumeChange = 0;
      //all nodes in the free-surface
      if (rVerticesFlags.FreeSurface == 4 && rVerticesFlags.Sliver == 0)
      {

        //all nodes in the non-wall free-surface (free surface flat element can be artificial)
        if (rVerticesFlags.NoWallFreeSurface == 4)
        {
          //if the element is decreasing its volume:
          //to avoid closing fluid voids with new elements
          if (KinematicUtilities::CheckVolumeDecrease(rVertices, rDimension, rTimeStep, VolumeTolerance, VolumeChange))
          {
            rAlpha *= 0.80;
          }
          else
          { //to avoid increasing fluid surface with new elements
            rAlpha *= 0.50;
          }
        }
        //three nodes in non-wall free-surface
        else if (rVerticesFlags.NoWallFreeSurface == 3)
        {
          if (KinematicUtilities::CheckVolumeDecrease(rVertices, rDimension, rTimeStep, VolumeTolerance, VolumeChange))
            rAlpha *= 0.80;
          else
            rAlpha *= 0.60;
        }
        //two nodes in non-wall free-surface
        else if (rVerticesFlags.NoWallFreeSurface == 2)
        {
          if (KinematicUtilities::CheckVolumeDecrease(rVertices, rDimension, rTimeStep, VolumeTolerance, VolumeChange))
            rAlpha *= 0.80;
          else
            rAlpha *= 0.60;
        }
        //one node in non-wall free-surface
        else
        {
          VolumeChange = 1;
          rAlpha *= 0.90;
        }

        if (VolumeChange == 0)
          rAlpha *= 2.2;
      }
      else
      {
        if (rVerticesFlags.FreeSurface > 0 && rVerticesFlags.Structure > 0)
        {
          if (KinematicUtilities::CheckVolumeDecrease(rVertices, rDimension, rTimeStep, VolumeTolerance, VolumeChange))
            rAlpha *= 1.65;
          else
            rAlpha *= 1.25;

          if (VolumeChange == 0)
            rAlpha *= 2.2;
        }
        else if (rVerticesFlags.FreeSurface == 0 && rVerticesFlags.Structure == 3)
        {
          rAlpha *= 2.2; //accept large elements
        }
        else
        {
          rAlpha *= 1.9;
        }
      }
    }
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckElementShape(GeometryType &rVertices, const NodalFlags &rVerticesFlags,  const double &rSize, const unsigned int &rDimension, const double &rTimeStep, unsigned int &number_of_slivers) override
  {
    bool accepted = true;
    unsigned int NumberOfVertices = rVertices.size();

    if (rDimension == 2 && NumberOfVertices == 3)
      accepted = CheckTriangleShape(rVertices, rVerticesFlags, rSize, rDimension, rTimeStep, number_of_slivers);
    else if (rDimension == 3 && NumberOfVertices == 4)
      return true;
    //accepted = CheckTetrahedronShape(rVertices, rVerticesFlags, rSize, rDimension, rTimeStep, number_of_slivers);

    return accepted;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckTriangleShape(GeometryType &rVertices, const NodalFlags &rVerticesFlags,  const double &rSize, const unsigned int &rDimension, const double &rTimeStep, unsigned int &number_of_slivers)
  {
    bool accepted = true;
    unsigned int NumberOfVertices = rVertices.size();

    //check 2D distorted elements with edges decreasing very fast
    if (rVerticesFlags.NoWallFreeSurface > 1 && rVerticesFlags.Structure == 0)
    {
      double MaxEdgeLength = std::numeric_limits<double>::min();
      double MinEdgeLength = std::numeric_limits<double>::max();

      for (unsigned int i = 0; i < NumberOfVertices - 1; ++i)
      {
        for (unsigned int j = i + 1; j < NumberOfVertices; ++j)
        {
          double Length = norm_2(rVertices[j].Coordinates() - rVertices[i].Coordinates());
          if (Length < MinEdgeLength)
            MinEdgeLength = Length;

          if (Length > MaxEdgeLength)
            MaxEdgeLength = Length;
        }
      }

      MesherUtilities MesherUtils;
      const double MaxRelativeVelocity = 1.5; //arbitrary value :: (try a criteria depending on time and size)
      bool sliver = false;
      if (MinEdgeLength * 5 < MaxEdgeLength)
      {
        if (rVerticesFlags.FreeSurface == NumberOfVertices)
        {
          std::cout << " WARNING 2D sliver on FreeSurface " << std::endl;
          accepted = false;
          ++number_of_slivers;
        }
        else if (KinematicUtilities::CheckRelativeVelocities(rVertices, MaxRelativeVelocity))
        {
          std::cout << " WARNING 2D sliver inside domain " << std::endl;
          sliver = true; //commented to no erase but labeled to not calculate them
          ++number_of_slivers;
        }
      }

      if (sliver){
        for (unsigned int i = 0; i < NumberOfVertices; ++i)
        {
          if (rVertices[i].Is(VISITED))
            std::cout << " WARNING Second sliver in the same node bis " << std::endl;
          rVertices[i].Set(VISITED);
        }
      }
    }

    return accepted;

  }

  //*******************************************************************************************
  //*******************************************************************************************

  bool CheckTetrahedronShape(GeometryType &rVertices, const NodalFlags &rVerticesFlags,  const double &rSize, const unsigned int &rDimension, const double &rTimeStep, unsigned int &number_of_slivers)
  {
    bool accepted = true;
    unsigned int NumberOfVertices = rVertices.size();

    if (rVerticesFlags.FreeSurface >= 2 || (rVerticesFlags.Boundary == 4 && rVerticesFlags.NoWallFreeSurface != 4))
    {

      Tetrahedra3D4<NodeType> Tetrahedron(rVertices);
      double Volume = Tetrahedron.Volume();

      if (Volume < 0.01 * pow(4.0 * rSize , rDimension))
      {
        //KRATOS_INFO("SLIVER")<<" Volume="<<Volume<<" VS Critical Volume="<<0.01*pow(4.0*rSize ,rDimension)<<std::endl;
        //std::cout<<" SLIVER Volume="<<Volume<<" VS Critical Volume="<< 0.01*pow(4.0*rSize ,rDimension) <<std::endl;

        //commented to no erase but labeled to not calculate them
        //accepted = false;
        //only erase full free surface sliver elements (visualization purposes)
        if (rVerticesFlags.FreeSurface == NumberOfVertices)
        {
          accepted = false;
        }
        else if ((rVerticesFlags.FreeSurface == NumberOfVertices - 1) && rVerticesFlags.Interaction > 1)
        {
          accepted = false;
          //std::cout<<" Release interaction SLIVER "<<std::endl;
        }

        ++number_of_slivers;

        if (accepted)
        {
          for (unsigned int i = 0; i < NumberOfVertices; ++i)
          {
            // if( rVertices[i].Is(VISITED) )
            //   std::cout<<" WARNING Second sliver in the same node "<<std::endl;
            rVertices[i].Set(VISITED);
          }
        }
      }
      // else if( Volume < 0.02*pow(4.0*rSize ,rDimension) ){
      //   int sliver = 0;
      //   MesherUtilities MesherUtils;
      //   bool distorted = MesherUtils.CheckGeometryShape(Tetrahedron,sliver);

      //   //std::cout << " SLIVER " << sliver <<" DISTORTED "<< distorted << std::endl;

      //   if( sliver ){
      //     std::cout<<" SLIVER SHAPE Volume="<<Volume<<" VS Critical Volume="<< 0.02*pow(4.0*rSize ,rDimension) <<std::endl;
      //     accepted = false;
      //     ++number_of_slivers;

      //     for(unsigned int i=0; i<rVertices.size(); ++i)
      //     {
      //       rVertices[i].Set(VISITED);
      //     }

      //   }

      // }
    }
    else if ((rVerticesFlags.Structure == 1 || rVerticesFlags.Structure == 2) && rVerticesFlags.Inside == 0)
    {
      MesherUtilities MesherUtils;

      double VolumeIncrement = KinematicUtilities::GetDeformationGradientDeterminant(rVertices, rDimension, rTimeStep);
      if (mEchoLevel > 0)
        if (VolumeIncrement < 0.0)
          std::cout << "  NEGATIVE Volume Increment (detF: " << VolumeIncrement << ")" << std::endl;

      double MovedVolume = KinematicUtilities::GetMovedVolume(rVertices, rDimension, rTimeStep, 1.0);

      //check if the element is going to invert
      if (VolumeIncrement < 0.0 || MovedVolume < 0.0)
      {

        // std::cout<<" SLIVER FOR INVERSION "<<VolumeIncrement<<" VS Critical Volume="<< 0.01*pow(4.0*rSize ,rDimension) <<"( rigid: "<<rVerticesFlags.Rigid<<" ) moved volume "<<MovedVolume<<std::endl;

        //commented to no erase but labeled to not calculate them
        //accepted = false;

        ++number_of_slivers;

        for (unsigned int i = 0; i < NumberOfVertices; ++i)
        {
          // if( rVertices[i].Is(VISITED) )
          //   std::cout<<" WARNING Second sliver in the same node bis "<<std::endl;
          rVertices[i].Set(VISITED);
        }
      }
    }

    return accepted;
  }


  //*******************************************************************************************
  //*******************************************************************************************

  //it can set TO_ERASE to new entities inserted in full rigid elements
  bool CheckElementAtFreeSurface(GeometryType &rVertices, const NodalFlags &rVerticesFlags, const double &rSize, const unsigned int &rDimension, const double &rTimeStep)
  {
    unsigned int dimension = 0;
    unsigned int number_of_vertices = 0;

    this->GetElementDimension(dimension, number_of_vertices);

    if (dimension == 3)
    {
      // Check all elements with a free surface having wall and non-wall nodes
      if (rVerticesFlags.Structure > 0 && rVerticesFlags.NoWallFreeSurface > 0)
      {
        // complex check if the face defines an acceptable geometrical meniscus concerning gravity direction
        if (GeometryUtilities::CheckFluidMeniscus(rVertices, rDimension))
        {
          //do not accept full rigid elements (no fluid)
          if (rVerticesFlags.Structure == number_of_vertices && rVerticesFlags.Fluid > 0)
          {
            //accept when it has more than 2 fluid nodes (2D) and more than 3 fluid nodes (3D)
            if (rVerticesFlags.Fluid < number_of_vertices - 1)
            {
              return false;
            }
            else if (rVerticesFlags.Fluid == number_of_vertices && rVerticesFlags.Visited > 2 && rVerticesFlags.FreeSurface > 0)
            {
              return false;
            }
          }

          //do not accept full rigid elements with a new inserted node (no fluid)
          if ((rVerticesFlags.Structure + rVerticesFlags.NewEntity) == number_of_vertices && rVerticesFlags.Fluid > 0)
          {
            if (rVerticesFlags.Fluid == number_of_vertices && rVerticesFlags.Visited >= number_of_vertices - 1)
            {
              std::cout << " OLD RIGID NEW ENTITY EDGE DISCARDED (old_entity: " << rVerticesFlags.Visited << " fluid: " << rVerticesFlags.Fluid << " rigid: " << rVerticesFlags.Rigid << " free_surface: " << rVerticesFlags.FreeSurface << ")" << std::endl;
              return false;
            }
          }

          //do not accept rigid wall elements with fluid node separating from walls
          if (rVerticesFlags.Structure == number_of_vertices-1 && rVerticesFlags.NoWallFreeSurface > 0)
          {
            MesherUtilities MesherUtils;
            double CriticalDistance = 0.5 * rSize;
            if (rVerticesFlags.FreeSurface > 1)
              CriticalDistance *= 0.1;
            bool approaching = KinematicUtilities::CheckFreeSurfaceApproachingWall(rVertices, CriticalDistance, rTimeStep);

            double VolumeIncrement = KinematicUtilities::GetDeformationGradientDeterminant(rVertices, rDimension, rTimeStep);

            if (VolumeIncrement > 1.1)
              approaching = false;

            // std::cout << "  Volume Increment (detF: " << VolumeIncrement << ")" << std::endl;
            // std::cout << "  Approaching Free Surfaces: " << approaching << std::endl;

            return approaching;
          }
        }
        else
          return false;
      }
      return true;
    }
    return true;
  }

  //*******************************************************************************************
  //*******************************************************************************************

  // It sets full previous rigid element nodes with the flag VISITED
  void LabelEdgeNodes(ModelPart &rModelPart)
  {

    //set flags for the local process execution
    SetFlagsUtilities::SetFlagsToElements(mrModelPart, {{VISITED}}, {VISITED.AsFalse()});
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{VISITED}}, {VISITED.AsFalse()});

    //set label to full rigid element nodes
    for (auto &i_elem : rModelPart.Elements())
    {
      GeometryType &rGeometry = i_elem.GetGeometry();

      unsigned int count_rigid = 0;
      // unsigned int count_free_surface = 0;
      for (auto &i_node : rGeometry)
      {
        if (i_node.Is(RIGID))
        {
          count_rigid++;
        }
        // else if (i_node.Is(FREE_SURFACE))
        // {
        //   count_free_surface++;
        // }
      }

      if (count_rigid == rGeometry.size())
      {
        i_elem.Set(VISITED, true);
        for (auto &i_node : rGeometry)
        {
          i_node.Set(VISITED, true);
        }
      }
    }

    //unset label for nodes not fully rigid
    for (auto &i_elem : rModelPart.Elements())
    {
      GeometryType &rGeometry = i_elem.GetGeometry();

      //to release full rigid elements with no fluid surrounding them
      if (i_elem.IsNot(VISITED))
      {
        for (auto &i_node : rGeometry)
        {
          i_node.Set(VISITED, false);
        }
      }
      else{
        i_elem.Set(VISITED, false);
      }

    }

  }

  //**************************************************************************
  //**************************************************************************

  // Only for 3D trying to delete rigid elements remaining attached to walls
  void CheckRigidElementNeighbours()
  {
    KRATOS_TRY

    unsigned int dimension = 0;
    unsigned int number_of_vertices = 0;

    this->GetElementDimension(dimension, number_of_vertices);

    if (dimension == 3)
    {
      const int &OutNumberOfElements = mrRemesh.OutMesh.GetNumberOfElements();
      const int *OutElementList = mrRemesh.OutMesh.GetElementList();
      const int *OutElementNeighbourList = mrRemesh.OutMesh.GetElementNeighbourList();

      //ModelPart::NodesContainerType &rNodes = mrModelPart.Nodes();
      ModelPart::NodesContainerType::iterator nodes_begin = mrModelPart.NodesBegin();

      int number_of_released_elements = -1; // start the while loop

      int max_iters = 4;
      int iter = 0;
      while (number_of_released_elements != 0 && iter <= max_iters)
      {
        number_of_released_elements = 0;
        ++iter;
        #pragma omp parallel for reduction(+: number_of_released_elements)
        for (int el = 0; el < OutNumberOfElements; ++el)
        {
          if (mrRemesh.PreservedElements[el]>0)
          {
            unsigned int element_faces = 0;
            unsigned int rigid_nodes = 0;
            unsigned int free_surface_nodes = 0;
            unsigned int selected_boundary_nodes = 0;
            unsigned int isolated_nodes = 0;

            int index = 0;
            const unsigned int id = el * number_of_vertices;
            //defined for triangles and tetrahedra (number of vertices = number of faces)
            for (unsigned int i = 0; i < number_of_vertices; ++i)
            {
              index = OutElementNeighbourList[id + i];
              if (index > 0)
                index = mrRemesh.PreservedElements[index - 1];

              if (index <= 0){
                ++element_faces;
              }
              else{

                ModelPart::NodesContainerType::iterator it_node = nodes_begin + OutElementList[id + i] - 1;
                if (it_node->Is(ISOLATED))
                  ++isolated_nodes;

                if (it_node->Is(RIGID))
                  ++rigid_nodes;
                else if (it_node->Is(FREE_SURFACE))
                  ++free_surface_nodes;

                if (it_node->Is(MARKER) && it_node->Is(BOUNDARY))
                  ++selected_boundary_nodes;
              }
            }

            //check full rigid isolated element
            if (element_faces == number_of_vertices || selected_boundary_nodes == number_of_vertices)
            {
              // mrRemesh.PreservedElements[el] = 0;
              // ++number_of_released_elements;

              if (rigid_nodes >= number_of_vertices - 1)
              {
                mrRemesh.PreservedElements[el] = 0;
                ++number_of_released_elements;
                //std::cout<<" RELEASE ISOLATED RIGID ELEMENT "<<std::endl;
              }
              else if (rigid_nodes > 0 && free_surface_nodes > 0 && (rigid_nodes + free_surface_nodes == number_of_vertices))
              {
                mrRemesh.PreservedElements[el] = 0;
                ++number_of_released_elements;
                //std::cout<<" RELEASE ISOLATED ELEMENT WITH RIGID NODES "<<std::endl;
              }

            }
            else if (element_faces > 0)
            {
              if (rigid_nodes == number_of_vertices)
              {
                unsigned int rigid_neighbours = 0;
                unsigned int almost_rigid_neighbours = 0;

                for (unsigned int i = 0; i < number_of_vertices; ++i)
                {
                  index = OutElementNeighbourList[id + i];
                  if (index > 0)
                  {
                    if (mrRemesh.PreservedElements[index - 1] > 0)
                    {
                      unsigned int rigid_nnodes = 0;
                      const unsigned int nid = (index - 1) * number_of_vertices;
                      for (unsigned int j = 0; j < number_of_vertices; ++j)
                      {
                        ModelPart::NodesContainerType::iterator it_node = nodes_begin + OutElementList[nid + j] - 1;
                        if (it_node->Is(RIGID))
                          ++rigid_nnodes;
                      }

                      if (rigid_nnodes == number_of_vertices)
                        ++rigid_neighbours;
                      else if (rigid_nnodes == number_of_vertices - 1)
                        ++almost_rigid_neighbours;
                    }
                  }
                }

                if (element_faces == number_of_vertices - 1)
                {
                  rigid_neighbours += almost_rigid_neighbours;
                  // if( rigid_neighbours == (number_of_vertices-element_faces) )
                  //   std::cout<<" Surrounded with almost rigid "<<std::endl;
                }

                //erase rigid elements surrounded by rigid elements
                if (rigid_neighbours == (number_of_vertices - element_faces) && free_surface_nodes > 1)
                {
                  mrRemesh.PreservedElements[el] = 0;
                  ++number_of_released_elements;
                  std::cout<<" RELEASE SURROUNDED ISOLATED RIGID ELEMENT "<<std::endl;
                }
              }
              else if (rigid_nodes + isolated_nodes == number_of_vertices)
              {
                mrRemesh.PreservedElements[el] = 0;
                ++number_of_released_elements;
                std::cout<<" RELEASE ELEMENT WITH ISOLATED NODE/S set selected node for ALE "<<std::endl;
                for (unsigned int i = 0; i < number_of_vertices; ++i)
                {
                  ModelPart::NodesContainerType::iterator it_node = nodes_begin + OutElementList[id + i] - 1;
                   if (it_node->Is(ISOLATED))
                    it_node->Set(MARKER);
                }

                // MesherUtilities MesherUtils;
                // GeometryType Vertices;
                // for(unsigned int i = 0; i<number_of_vertices; ++i)
                // {
                //   Vertices.push_back(rNodes(OutElementList[el*number_of_vertices+i]));
                // }

                // if(!MesherUtils.CheckFreeSurfaceApproachingWall(Vertices) )
                // {
                //   mrRemesh.PreservedElements[el] = 0;
                //   ++number_of_released_elements;
                //   std::cout<<" RELEASE ELEMENT WITH ISOLATED NODE/s non approaching "<<std::endl;
                // }
              }
              else if (element_faces == number_of_vertices - 1)
              {

                if (free_surface_nodes == number_of_vertices && rigid_nodes <= number_of_vertices - 1)
                {
                  mrRemesh.PreservedElements[el] = 0;
                  ++number_of_released_elements;
                  //std::cout<<" RELEASE SINGLE ELEMENT "<<std::endl;
                }
              }
            }
          }
        }

        mrRemesh.Info->Current.Elements -= number_of_released_elements;
      }
      //std::cout<<" ITERS "<<iter<<std::endl;
    }

    KRATOS_CATCH("")
  }


  //*******************************************************************************************
  //*******************************************************************************************

  void SelectNodesToErase() override
  {
    // Set disconnected nodes to erase
    unsigned int dimension = 0;
    unsigned int number_of_vertices = 0;
    unsigned int isolated_nodes = 0;

    this->GetElementDimension(dimension, number_of_vertices);

    int *OutElementList = mrRemesh.OutMesh.GetElementList();

    ModelPart::NodesContainerType &rNodes = mrModelPart.Nodes();

    const int &OutNumberOfElements = mrRemesh.OutMesh.GetNumberOfElements();

    //set flags for the local process execution
    SetFlagsUtilities::SetFlagsToNodes(mrModelPart, {{BLOCKED}}, {BLOCKED.AsFalse()});

    //check engaged nodes
    for (int el = 0; el < OutNumberOfElements; ++el)
    {
      if (mrRemesh.PreservedElements[el])
      {
        for (unsigned int pn = 0; pn < number_of_vertices; ++pn)
        {
          //set vertices
          rNodes[OutElementList[el * number_of_vertices + pn]].Set(BLOCKED);
        }
      }
    }

    int count_release = 0;
    for (ModelPart::NodesContainerType::iterator i_node = rNodes.begin(); i_node != rNodes.end(); ++i_node)
    {
      if (i_node->IsNot(BLOCKED))
      {

        if (i_node->Is(TO_ERASE) && i_node->Or({RIGID,SOLID,INLET}))
        {
          i_node->Set(TO_ERASE, false);
          std::cout << " WARNING TRYING TO DELETE A WALL NODE (fluid): " << i_node->Id() << std::endl;
        }
        else if (i_node->AndNot({RIGID,SOLID,INLET}))
        {
          if (mrRemesh.ExecutionOptions.Is(MesherData::KEEP_ISOLATED_NODES))
          {
            if (i_node->IsNot(TO_ERASE))
            {
              // if (i_node->Is(FREE_SURFACE))
              // {
                i_node->Set(ISOLATED, true);
                ++isolated_nodes;
                //std::cout<<" ISOLATED FREE SURFACE FLUID "<<std::endl;
              // }
              // else
              // {
              //   // in fact can be a new free surface node -> check if it happens
              //   i_node->Set(TO_ERASE, true);
              //   std::cout<<"  ISOLATED inside FLUID node TO ERASE in select_elements_mesher_process.hpp "<<i_node->IsNot(BLOCKED)<<std::endl;
              // }
            }
          }
          else
          {
            std::cout<<" ISOLATED FLUID NODES NOT KEPT in select_elements_mesher_process.hpp"<<std::endl;
            i_node->Set(TO_ERASE, true);
            if (mEchoLevel > 0)
            {
              if (i_node->Is(BOUNDARY))
                std::cout << " NODE " << i_node->Id() << " IS BOUNDARY RELEASE " << std::endl;
              else
                std::cout << " NODE " << i_node->Id() << " IS DOMAIN RELEASE " << std::endl;
            }
            ++count_release;
          }
        }

      }
      else  // BLOCKED NODE
      {
        i_node->Set(TO_ERASE, false);
        i_node->Set(ISOLATED, false);
      }

      i_node->Set(BLOCKED, false);

      i_node->Set(VISITED, false);

      if (i_node->Is(SELECTED)) // Belongs to a SLIVER
        i_node->Set(MARKER, true);
      else
        i_node->Set(MARKER, false);

      i_node->Set(SELECTED, false);
    }

    if (mEchoLevel > 0)
    {
      std::cout << "   NUMBER OF RELEASED NODES " << count_release << std::endl;
      std::cout << "   NUMBER OF ISOLATED NODES " << isolated_nodes << std::endl;
    }
  }


  ///@}
  ///@name Protected  Access
  ///@{
  ///@}
  ///@name Protected Inquiry
  ///@{
  ///@}
  ///@name Protected LifeCycle
  ///@{
  ///@}

private:
  ///@name Private Static Member Variables
  ///@{
  ///@}
  ///@name Private Static Member Variables
  ///@{
  ///@}
  ///@name Private Operators
  ///@{
  ///@}
  ///@name Private Operations
  ///@{
  ///@}
  ///@name Private  Access
  ///@{
  ///@}
  ///@name Private Inquiry
  ///@{
  ///@}
  ///@name Un accessible methods
  ///@{
  ///@}

}; // Class Process

///@}
///@name Type Definitions
///@{
///@}
///@name Input and output
///@{

/// input stream function
inline std::istream &operator>>(std::istream &rIStream,
                                SelectFluidElementsMesherProcess &rThis);

/// output stream function
inline std::ostream &operator<<(std::ostream &rOStream,
                                const SelectFluidElementsMesherProcess &rThis)
{
  rThis.PrintInfo(rOStream);
  rOStream << std::endl;
  rThis.PrintData(rOStream);

  return rOStream;
}
///@}

} // namespace Kratos.

#endif // KRATOS_SELECT_FLUID_ELEMENTS_MESHER_PROCESS_HPP_INCLUDED defined
