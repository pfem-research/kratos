{
    "problem_data"             : {
        "problem_name" : "pfem_vp_open_cavity",
        "echo_level"   : 1,
	"threads" : 1
    },
    "model_settings"           : {
        "model_name"           : "pfem_vp_open_cavity",
        "dimension"            : 2,
        "input_file_settings"  : {
            "name" : "pfem_vp_open_cavity"
        },
        "bodies_list"          : [{
            "body_type"  : "Fluid",
            "body_name"  : "Body0",
            "parts_list" : ["Parts_Parts_Auto1"]
        },{
            "body_type"  : "Rigid",
            "body_name"  : "Body1",
            "parts_list" : ["Parts_Parts_Auto2"]
        }],
        "domain_parts_list"    : ["Parts_Parts_Auto1","Parts_Parts_Auto2"],
        "processes_parts_list" : ["VELOCITY_Velocity_Auto1"]
    },
    "time_settings"            : {
        "time_step" : 0.001,
        "end_time"  : 0.5
    },
    "solver_settings"          : {
        "solver_type" : "solid_mechanics_segregated_solver",
        "Parameters"  : {
            "solvers" : [{
                "solver_type" : "solid_mechanics_implicit_dynamic_solver",
                "Parameters"  : {
                    "time_integration_settings"      : {
                        "solution_type"      : "Dynamic",
                        "time_integration"   : "Implicit",
                        "integration_method" : "Newmark",
                        "buffer_size"        : 2
                    },
                    "convergence_criterion_settings" : {
                        "convergence_criterion"       : "Variable_criterion",
                        "variable_relative_tolerance" : 1e-5,
                        "variable_absolute_tolerance" : 1e-9,
                        "residual_relative_tolerance" : 1e-5,
                        "residual_absolute_tolerance" : 1e-9
                    },
                    "solving_strategy_settings"      : {
                        "max_iteration"            : 20,
                        "lumped_mass_matrix"       : true,
                        "consistent_mass_matrix"   : false,
                        "reform_dofs_at_each_step" : true
                    },
            "linear_solver_settings":{
                "solver_type": "ExternalSolversApplication.super_lu"
            },
                    "dofs"                           : ["VELOCITY"]
                }
            },{
                "solver_type" : "solid_mechanics_implicit_dynamic_solver",
                "Parameters"  : {
                    "time_integration_settings"      : {
                        "solution_type"          : "Dynamic",
                        "integration_method"     : "Bdf",
                        "time_integration_order" : 1,
                        "analysis_type"          : "Non-linear",
                        "buffer_size"            : 2
                    },
                    "convergence_criterion_settings" : {
                        "convergence_criterion"       : "Variable_criterion",
                        "variable_relative_tolerance" : 1e-5,
                        "variable_absolute_tolerance" : 1e-9,
                        "residual_relative_tolerance" : 1e-5,
                        "residual_absolute_tolerance" : 1e-9
                    },
                    "solving_strategy_settings"      : {
                        "max_iteration"            : 20,
                        "lumped_mass_matrix"       : true,
                        "consistent_mass_matrix"   : false,
                        "reform_dofs_at_each_step" : true
                    },
            "linear_solver_settings":{
                "solver_type": "ExternalSolversApplication.super_lu"
            },
                    "dofs"                           : ["PRESSURE"]
                }
            }]
        }
    },
    "problem_process_list"     : [{
        "help"          : "This process applies meshing to the problem domains",
        "kratos_module" : "KratosMultiphysics.PfemApplication",
        "python_module" : "remesh_fluid_domains_process",
        "process_name"  : "RemeshFluidDomainsProcess",
        "Parameters"    : {
            "model_part_name"       : "pfem_vp_open_cavity",
            "meshing_control_type"  : "step",
            "meshing_frequency"     : 1,
            "meshing_before_output" : true,
            "meshing_domains"       : [{
                "model_part_name"                 : "Body0",
                "python_module"                   : "meshing_domain",
                "alpha_shape"                     : 1.25,
                "offset_factor"                   : 0.0,
                "meshing_strategy"                : {
                    "python_module"                 : "KratosMultiphysics.PfemApplication.fluid_meshing_strategy",
                    "meshing_frequency"             : 0,
                    "remesh"                        : true,
                    "refine"                        : true,
                    "reconnect"                     : false,
                    "transfer"                      : false,
                    "constrained"                   : false,
                    "mesh_smoothing"                : false,
                    "variables_smoothing"           : false,
                    "elemental_variables_to_smooth" : ["DETERMINANT_F"],
                    "reference_element_type"        : "UpdatedLagrangianSegregatedFluidElement2D3N",
                    "reference_condition_type"      : "CompositeCondition2D2N"
                },
                "spatial_bounding_box"            : {
                    "upper_point" : [0.0,0.0,0.0],
                    "lower_point" : [0.0,0.0,0.0],
                    "velocity"    : [0.0,0.0,0.0]
                },
                "refining_parameters"             : {
                    "critical_size"       : 0.0,
                    "threshold_variable"  : "PLASTIC_STRAIN",
                    "reference_threshold" : 0.0,
                    "error_variable"      : "NORM_ISOCHORIC_STRESS",
                    "reference_error"     : 0.0,
                    "add_nodes"           : false,
                    "insert_nodes"        : true,
                    "remove_nodes"        : {
                        "apply_removal" : true,
                        "on_distance"   : true,
                        "on_threshold"  : false,
                        "on_error"      : false
                    },
                    "remove_boundary"     : {
                        "apply_removal" : false,
                        "on_distance"   : false,
                        "on_threshold"  : false,
                        "on_error"      : false
                    },
                    "refine_elements"     : {
                        "apply_refinement" : true,
                        "on_distance"      : true,
                        "on_threshold"     : false,
                        "on_error"         : false
                    },
                    "refine_boundary"     : {
                        "apply_refinement" : false,
                        "on_distance"      : false,
                        "on_threshold"     : false,
                        "on_error"         : false
                    },
                    "refining_box"        : {
                        "refine_in_box_only" : false,
                        "upper_point"        : [0.0,0.0,0.0],
                        "lower_point"        : [0.0,0.0,0.0],
                        "velocity"           : [0.0,0.0,0.0]
                    }
                },
                "elemental_variables_to_transfer" : ["CAUCHY_STRESS_VECTOR","DEFORMATION_GRADIENT"]
            }]
        }
    }],
    "constraints_process_list" : [{
        "python_module" : "assign_vector_components_to_nodes_process",
        "kratos_module" : "KratosMultiphysics.SolidMechanicsApplication",
        "help"          : "This process fixes the selected components of a given vector variable",
        "process_name"  : "AssignVectorComponentsToNodesProcess",
        "Parameters"    : {
            "model_part_name"     : "VELOCITY_Velocity_Auto1",
            "variable_name"       : "VELOCITY",
            "value"               : [0.0,0.0,0.0],
            "compound_assignment" : "direct",
            "interval"            : [0.0,"End"]
        }
    }],
    "loads_process_list"       : [{
        "python_module" : "assign_vector_components_to_nodes_process",
        "kratos_module" : "KratosMultiphysics.SolidMechanicsApplication",
        "Parameters"    : {
            "model_part_name" : "Body0",
            "variable_name"   : "VOLUME_ACCELERATION",
            "value"           : [0.0,-9.81,0.0],
            "constrained"     : false
        }
    }],
    "output_process_list"      : [{
        "python_module" : "restart_process",
        "kratos_module" : "KratosMultiphysics.SolidMechanicsApplication",
        "help"          : "This process writes restart files",
        "process_name"  : "RestartProcess",
        "Parameters"    : {
            "model_part_name"     : "pfem_vp_open_cavity",
            "save_restart"        : false,
            "restart_file_name"   : "pfem_vp_open_cavity",
            "restart_file_label"  : "step",
            "output_control_type" : "step",
            "output_frequency"    : 1,
            "json_output"         : false
        }
    }],
    "output_configuration"     : {
        "result_file_configuration" : {
            "gidpost_flags"       : {
                "GiDPostMode"           : "GiD_PostBinary",
                "WriteDeformedMeshFlag" : "WriteDeformed",
                "WriteConditionsFlag"   : "WriteConditions",
                "MultiFileFlag"         : "MultipleFiles"
            },
            "file_label"          : "step",
            "output_control_type" : "step",
            "output_frequency"    : 1,
            "node_output"         : true,
            "nodal_flags_results" : ["FREE_SURFACE","INLET"],
            "nodal_results"       : ["DISPLACEMENT","VELOCITY","ACCELERATION","PRESSURE","NORMAL"],
            "gauss_point_results" : []
        }
    }
}
