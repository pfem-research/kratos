<p align=center><img src="https://gitlab.com/uploads/-/system/group/avatar/4560710/logo.png"> </p>

# Pfem Application

It includes all the necessary applications for the solution with the Particle Finite Element Method (PFEM)

## Build

This application is part of the Kratos Multiphysics Platform.Instructions on how to get a copy of the project and run it on your local machine are available for both [Linux](https://github.com/KratosMultiphysics/Kratos/wiki/Linux-Build) and [Windows](https://https://github.com/KratosMultiphysics/Kratos/wiki/Windows-Install) systems.

The guide to build the application can be found in the [Install instructions](https://gitlab.com/pfem-research/kratos/tree/master/INSTALL.md) or in the [Kratos wiki](https://github.com/KratosMultiphysics/Kratos/wiki).

The application to be activated is:

``` cmake
add_app ${KRATOS_APP_DIR}/PfemApplication
```

## License

The Pfem application is OPEN SOURCE. The main code and program structure is available and aimed to grow with the need of any user willing to expand it. The BSD (Berkeley Software Distribution) licence allows to use and distribute the existing code without any restriction, but with the possibility to develop new parts of the code on an open or close source depending on the developers.

## Contact

* **Josep Maria Carbonell** - *Core Development* - [cpuigbo@cimne.upc.edu](mailto:cpuigbo@cimne.upc.edu)
* **Lluís Monforte Vila** - *Core Development* - [lluis.monforte@upc.edu](mailto:lluis.monforte@upc.edu)