""" Project: PfemApplication
    Developer: IPouplana
    Maintainer: IP
"""

# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics.PfemApplication as KratosPfem


def Factory(settings, Model):
    if(type(settings) != KratosMultiphysics.Parameters):
        raise Exception(
            "expected input shall be a Parameters object, encapsulating a json string")
    return AleRotationProcess(Model, settings["Parameters"])


class AleRotationProcess(KratosMultiphysics.Process):
    def __init__(self, Model, settings):
        KratosMultiphysics.Process.__init__(self)

        self.model = Model

        self.settings = settings

        params = KratosMultiphysics.Parameters("{}")
        params.AddValue("model_part_name", settings["model_part_name"])
        params.AddValue("angular_velocity", settings["angular_velocity"])
        params.AddValue("rotation_axis_initial_point",
                        settings["rotation_axis_initial_point"])
        params.AddValue("rotation_axis_final_point",
                        settings["rotation_axis_final_point"])
        params.AddValue("initial_time", settings["initial_time"])

        self.parameters = params

    def ExecuteInitialize(self):

        self.model_part = self.model[self.settings["model_part_name"].GetString(
        )]
        self.process = KratosPfem.AleRotationProcess(
            self.model_part, self.parameters)
        self.process.ExecuteInitialize()

    def ExecuteInitializeSolutionStep(self):

        self.process.ExecuteInitializeSolutionStep()
