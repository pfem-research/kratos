""" Project: PfemApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.PfemApplication as KratosPfem
import KratosMultiphysics.MeshersApplication as KratosMeshers
import KratosMultiphysics.MeshersApplication.mesher as BaseMesher


def CreateMesher(main_model_part, meshing_parameters):
    return FluidRefiningMesher(main_model_part, meshing_parameters)


class FluidRefiningMesher(BaseMesher.Mesher):

    #
    def __init__(self, main_model_part, meshing_parameters):

        BaseMesher.Mesher.__init__(self, main_model_part, meshing_parameters)

    #
    def InitializeMeshing(self):

        self.MeshingParameters.InitializeMeshing()

        # set execution flags: to set the options to be executed in methods and processes
        meshing_options = self.MeshingParameters.GetOptions()

        execution_options = KratosMultiphysics.Flags()

        execution_options.Set(
            KratosMeshers.MesherData.INITIALIZE_MESHER_INPUT, True)
        execution_options.Set(
            KratosMeshers.MesherData.FINALIZE_MESHER_INPUT,  True)

        execution_options.Set(
            KratosMeshers.MesherData.TRANSFER_KRATOS_NODES_TO_MESHER, True)
        execution_options.Set(
            KratosMeshers.MesherData.TRANSFER_KRATOS_ELEMENTS_TO_MESHER, False)
        execution_options.Set(
            KratosMeshers.MesherData.TRANSFER_KRATOS_NEIGHBOURS_TO_MESHER, False)

        if(meshing_options.Is(KratosMeshers.MesherData.CONSTRAINED)):
            execution_options.Set(
                KratosMeshers.MesherData.TRANSFER_KRATOS_FACES_TO_MESHER, True)

        execution_options.Set(
            KratosMeshers.MesherData.SELECT_TESSELLATION_ELEMENTS, True)
        execution_options.Set(
            KratosMeshers.MesherData.KEEP_ISOLATED_NODES, True)

        self.MeshingParameters.SetExecutionOptions(execution_options)

        # set mesher flags: to set options for the mesher (triangle 2D, tetgen 3D)
        mesher_flags = ""
        mesher_info = "Prepare domain for refinement"
        if(self.dimension == 2):

            if(meshing_options.Is(KratosMeshers.MesherData.CONSTRAINED)):
                mesher_flags = "pnBYYQ"
            else:
                mesher_flags = "nQP"

        elif(self.dimension == 3):

            if(meshing_options.Is(KratosMeshers.MesherData.CONSTRAINED)):
                mesher_flags = "pnBJFMYYQ"
            else:
                #mesher_flags = "rQYYCCJF"
                #mesher_flags = "nQMu0"
                mesher_flags = "nJFQ"
                # mesher_flags ="VJFu0"; #PSOLID
                #mesher_flags ="rMfjYYaq2.5nQ";
                #mesher_flags = "nJFMQO4/4"

        self.MeshingParameters.SetTessellationFlags(mesher_flags)
        self.MeshingParameters.SetTessellationInfo(mesher_info)

    #
    def SetPreMeshingProcesses(self):

        # processes to refine elements /refine boundary
        processes = []

        # The order set is the order of execution:
        processes.append(KratosPfem.SmoothFreeSurface(self.model_part, self.MeshingParameters, self.echo_level))
        processes.append(KratosPfem.InsertInlet(self.model_part, self.MeshingParameters, self.echo_level))
        processes.append(KratosPfem.RemoveFluidNodes(self.model_part, self.MeshingParameters, self.echo_level))
        processes.append(KratosPfem.InsertFluidNodes(self.model_part, self.MeshingParameters, self.echo_level))
        processes.append(KratosPfem.RefineFluidElementsInEdges(self.model_part, self.MeshingParameters, self.echo_level))

        [self.mesher.SetPreMeshingProcess(process) for process in processes]
    #
    def SetPostMeshingProcesses(self):

        # processes to rebuild domain
        processes = []

        # The order set is the order of execution:
        processes.append(KratosPfem.SelectFluidElements(self.main_model_part, self.MeshingParameters, self.echo_level))
        processes.append(KratosMeshers.GenerateNewElements(self.model_part, self.MeshingParameters, self.echo_level))
        processes.append(KratosMeshers.GenerateNewConditions(self.model_part, self.MeshingParameters, self.echo_level))

        [self.mesher.SetPostMeshingProcess(process) for process in processes]
    #
    @classmethod
    def _class_prefix(self):
        header = "::[--Refining Mesher--]::"
        return header
