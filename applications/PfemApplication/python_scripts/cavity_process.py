""" Project: PfemApplication
    Developer: LMonforte
    Maintainer: LM
"""

# Built-in/Generic Imports
import os.path

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolidMechanicsApplication as KratosSolid
import KratosMultiphysics.PfemApplication as KratosPfem
import KratosMultiphysics.ContactMechanicsApplication as KratosContact


def Factory(custom_settings, Model):
    if(type(custom_settings) != KratosMultiphysics.Parameters):
        raise Exception(
            "expected input shall be a Parameters object, encapsulating a json string")
    return CavityProcess(Model, custom_settings["Parameters"])


class CavityProcess(KratosMultiphysics.Process):

    #
    def __init__(self, model_part, custom_settings):

        KratosMultiphysics.Process.__init__(self)

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
        }
        """)

        self.model_part = model_part['Main_Domain']

        # initialize figure path
        problem_path = os.getcwd()
        self.csv_path = os.path.join(problem_path, "cavity.csv")
        csv_file = open(self.csv_path, "a")
        line = " 0.0 , 0.0 , 0.0 , 0.0  \n"
        csv_file.write(line)
        csv_file.close()

    def ExecuteFinalizeSolutionStep(self):

        time = self._GetStepTime()

        SomeProcessInfo = KratosMultiphysics.ProcessInfo()
        import math
        Force = 0.0
        WaterPressure = 0.0
        NWaterPressure = 0.0
        for node in self.model_part.GetNodes(0):
            x = node.X0
            y = node.Y0
            r = x*x + y*y
            r = x*x
            r = math.sqrt(r)
            xDir = x
            yDir = y
            if (abs(r) > 1e-12):
                xDir = x/r
                xDir = x/r
                yDir = 0.0
            if (r < 1.00001):
                res = node.GetSolutionStepValue(
                    KratosSolid.DISPLACEMENT_REACTION)
                Force = Force + res[0]*xDir + res[1]*yDir
                if (node.SolutionStepsDataHas(KratosMultiphysics.WATER_PRESSURE)):
                    WaterPressure = WaterPressure + \
                        node.GetSolutionStepValue(
                            KratosMultiphysics.WATER_PRESSURE)
                    NWaterPressure = NWaterPressure + 1.0
        WaterPressure = WaterPressure / (NWaterPressure+1e-12)

        line = str(time) + " , " + str(Force) + \
            " , " + str(WaterPressure) + " \n"

        csv_file = open(self.csv_path, "a")
        csv_file.write(line)
        csv_file.close()

    def _AddToLine(self, line, vector):

        for i in vector:
            line = line + str(i) + " , "

        return line

    def _GetStepTime(self):

        return self.model_part.ProcessInfo[KratosMultiphysics.TIME]
