""" Project: PfemApplication
    Developer: LMonforte
    Maintainer: LM
"""

# Built-in/Generic Imports
import os.path

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolidMechanicsApplication as KratosSolid
import KratosMultiphysics.PfemApplication as KratosPfem
import KratosMultiphysics.ContactMechanicsApplication as KratosContact


def Factory(custom_settings, Model):
    if(type(custom_settings) != KratosMultiphysics.Parameters):
        raise Exception(
            "expected input shall be a Parameters object, encapsulating a json string")
    return FootingProblemProcess3(Model, custom_settings["Parameters"])


class FootingProblemProcess3(KratosMultiphysics.Process):

    #
    def __init__(self, model_part, custom_settings):

        KratosMultiphysics.Process.__init__(self)

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
           "model_part_name": "Main_Domain",
           "initial_height": 0.0,
           "width": 1.0
        }
        """)

        settings = custom_settings
        settings.ValidateAndAssignDefaults(default_settings)

        self.model = model_part
        self.model_part_name =  settings["model_part_name"].GetString()

        self.Y0 = settings["initial_height"].GetDouble()
        self.B = settings["width"].GetDouble()

        # initialize figure path
        problem_path = os.getcwd()
        self.csv_path = os.path.join(problem_path, "footing.csv")
        csv_file = open(self.csv_path, "a")
        csv_file.close()

        self.main_model_part = model_part['Main_Domain']

    def ExecuteFinalizeSolutionStep(self):

        self.model_part = self.model[self.model_part_name]

        time = self._GetStepTime()

        SomeProcessInfo = KratosMultiphysics.ProcessInfo()

        Vel = 0.0*KratosMultiphysics.Array3()
        Dis = 0.0*KratosMultiphysics.Array3()
        Acc = 0.0*KratosMultiphysics.Array3()
        Force = 0.0*KratosMultiphysics.Array3()
        yMax = -100.0;

        for node in self.main_model_part.GetNodes(0):
            if (node.Y0 > yMax):
                yMax = node.Y0
                Dis = node.GetSolutionStepValue( KratosMultiphysics.DISPLACEMENT )
                if ( node.SolutionStepsDataHas( KratosMultiphysics.ACCELERATION) ):
                    Acc = node.GetSolutionStepValue( KratosMultiphysics.ACCELERATION )
                    Vel = node.GetSolutionStepValue( KratosMultiphysics.VELOCITY )
        for node in self.model_part.GetNodes(0):
            CF = node.GetSolutionStepValue( KratosMultiphysics.CONTACT_FORCE )
            if ( (abs(CF[1])+abs(CF[0])) > 1.0E-8):
                Force = Force - CF;


        line = str(time) + " , "

        line = self._AddToLine(line, Dis)
        line = self._AddToLine(line, Vel)
        line = self._AddToLine(line, Acc)
        line = self._AddToLine(line, Force)
        line = line + " \n"

        csv_file = open(self.csv_path, "a")
        csv_file.write(line)
        csv_file.close()

        self.TryToUnFixNodes()

    # function to try to unfix the water pressure of nodes in that are in contact
    def TryToUnFixNodes(self):


        for node in self.model_part.GetNodes(0):
            Force = node.GetSolutionStepValue(KratosMultiphysics.CONTACT_FORCE)
            Normal = node.GetSolutionStepValue(KratosMultiphysics.NORMAL)
            IsBoundary = False
            if (abs(Normal[0]) + abs(Normal[1]) > 1.0e-7):
                IsBoundary = True

            if (node.HasDofFor(KratosMultiphysics.WATER_PRESSURE) and IsBoundary ):
                if ((abs(Force[0]) + abs(Force[1]) > 1.0e-7)):
                    if (node.IsFixed(KratosMultiphysics.WATER_PRESSURE)):
                        node.Free(KratosMultiphysics.WATER_PRESSURE)
                        print(' UNFIXING ')
                elif ( Normal[0] < -0.999):
                    if (node.IsFixed(KratosMultiphysics.WATER_PRESSURE)):
                        node.Free(KratosMultiphysics.WATER_PRESSURE)
                        print(' UNFIXING ')
                elif ( IsBoundary):
                    if (node.IsFixed(KratosMultiphysics.WATER_PRESSURE) == False):
                        node.Fix(KratosMultiphysics.WATER_PRESSURE)
                        node.SetSolutionStepValue( KratosMultiphysics.WATER_PRESSURE, 0.0)
                        print(' UNFIXING :: FIXING ')




    def _AddToLine(self, line, vector):

        for i in vector:
            line = line + str(i) + " , "

        return line

    def _GetStepTime(self):

        return self.model_part.ProcessInfo[KratosMultiphysics.TIME]
