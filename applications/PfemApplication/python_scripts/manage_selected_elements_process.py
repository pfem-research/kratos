""" Project: PfemApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.PfemApplication as KratosPfem
import KratosMultiphysics.MeshersApplication as KratosMeshers

def Factory(custom_settings, Model):
    if(not isinstance(custom_settings, KratosMultiphysics.Parameters)):
        raise Exception(
            "expected input shall be a Parameters object, encapsulating a json string")
    return ManageSelectedElementsProcess(Model, custom_settings["Parameters"])


class ManageSelectedElementsProcess(KratosMultiphysics.Process):
    def __init__(self, Model, custom_settings):

        KratosMultiphysics.Process.__init__(self)

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
             "model_part_name": "MODEL_PART_NAME"
        }
        """)

        # overwrite the default settings with user-provided parameters
        self.settings = custom_settings
        self.settings.ValidateAndAssignDefaults(default_settings)

        self.model = Model

    def ExecuteInitialize(self):

        # set model part
        self.model_part = self.model[self.settings["model_part_name"].GetString()]

        # Process Info
        self.process_info = self.model_part.ProcessInfo

        self.ManageElementsProcess = KratosPfem.ManageSelectedElementsProcess(
            self.model_part)

    def ExecuteBeforeSolutionLoop(self):

        self.ManageElementsProcess.ExecuteBeforeSolutionLoop()

    def ExecuteInitializeSolutionStep(self):
        if self._domain_parts_changed():
            self.ManageElementsProcess.ExecuteInitializeSolutionStep()

    def ExecuteFinalizeSolutionStep(self):
        if self._domain_parts_changed():
            self.ManageElementsProcess.ExecuteFinalizeSolutionStep()
    #
    def _domain_parts_changed(self):
        if self.process_info.Has(KratosMeshers.MESHING_STEP_TIME):
            return self._check_current_time_step(self.process_info[KratosMeshers.MESHING_STEP_TIME])
        else:
            return False
    #
    def _check_current_time_step(self, step_time):
        current_time = self.process_info[KratosMultiphysics.TIME]
        delta_time = self.process_info[KratosMultiphysics.DELTA_TIME]
        # arithmetic floating point tolerance
        tolerance = delta_time * 0.001

        if(step_time > current_time-tolerance and step_time < current_time+tolerance):
            return True
        else:
            return False
