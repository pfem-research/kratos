""" Project: PfemApplication
    Developer: LMonforte
    Mantainer: LM
"""

# Built-in/Generic Imports
import os.path
import numpy as np

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolidMechanicsApplication as KratosSolid
import KratosMultiphysics.PfemApplication as KratosPfem
import KratosMultiphysics.ContactMechanicsApplication as KratosContact


def Factory(custom_settings, Model):
    if(type(custom_settings) != KratosMultiphysics.Parameters):
        raise Exception(
            "expected input shall be a Parameters object, encapsulating a json string")
    return DynamicSamplingUtility(Model, custom_settings["Parameters"])


class DynamicSamplingUtility(KratosMultiphysics.Process):
    #

    def __init__(self, Model, custom_settings):

        KratosMultiphysics.Process.__init__(self)

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
        "model_part_name": "",
         "soil_model_part"        : "soil",
         "rigid_model_part"   : "soil"
        }
        """)

        settings = custom_settings
        settings.ValidateAndAssignDefaults(default_settings)

        model_name = settings["model_part_name"].GetString()
        self.soil_model_part_name  = settings["soil_model_part"].GetString() 
        self.rigid_model_part_name = settings["rigid_model_part"].GetString()
        self.soil_model_part_name = model_name + self.soil_model_part_name
        self.rigid_model_part_name = model_name + self.rigid_model_part_name
        self.model = Model


        problem_path = os.getcwd()
        self.figure_path = os.path.join(
            problem_path, "dynamic_sampling_data.csv")



    #
    def ExecuteFinalizeSolutionStep(self):
       


        self.rigid_model_part = self.model[self.rigid_model_part_name]
        self.soil_model_part = self.model[self.soil_model_part_name]

        Q  = self._GetForces(self.soil_model_part)
        Q2 = self._GetForces(self.rigid_model_part)

        SoilInfo = self._GetThisInfo( self.soil_model_part )
        PileInfo = self._GetThisInfo( self.rigid_model_part )


        time = self._GetStepTime()  # - self.GetStepDeltaTime()

        line_value = str(time) + " , " +  self.ConvertToString(Q) + " , " + self.ConvertToString(Q2) + "  " + SoilInfo + " , " + PileInfo + " , \n"

        figure_file = open(self.figure_path, "a")
        figure_file.write(line_value)
        figure_file.close()


        # self.MakeTheOtherFile();



    def _GetThisInfo(self, model_part):

        MaxDistance = 1.0E9

        for node in model_part.GetNodes():
            distance = abs( node.X) + abs(node.Y-100000.0)
            if ( distance < MaxDistance):
                MaxDistance = distance
                thisNode = node;
        
        
        Displ = thisNode.GetSolutionStepValue( KratosMultiphysics.DISPLACEMENT )
        Vel   = thisNode.GetSolutionStepValue( KratosMultiphysics.VELOCITY )
        Acc   = thisNode.GetSolutionStepValue( KratosMultiphysics.ACCELERATION )

        thisInfo = self.ConvertToString(Displ) + " , " +  self.ConvertToString(Vel) 
        thisInfo = thisInfo + " , " + self.ConvertToString( Acc)

        return thisInfo
    

    def _GetForces(self, model_part):

        Q = 0.0*KratosMultiphysics.Array3()
        
        for node in model_part.GetNodes():
            CF = node.GetSolutionStepValue( KratosMultiphysics.CONTACT_FORCE)
            Q = Q + CF;
        return Q

    def ConvertToString( self,  v):

        string = str(v[0]) + " , " + str(v[1]) + " , " + str(v[2])
        return string


    #
    def _GetStepTime(self):

        return self.soil_model_part.ProcessInfo[KratosMultiphysics.TIME]

