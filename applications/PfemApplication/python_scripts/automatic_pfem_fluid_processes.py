""" Project: PfemApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics


class FluidDefaultPfemProcesses(object):

    def __init__(self, project_parameters):
        self._problem_name = None
        self._project_parameters = project_parameters

        # Set echo level
        self._echo_level = 0
        if self._project_parameters.Has("problem_data"):
            if self._project_parameters["problem_data"].Has("echo_level"):
                self._echo_level = self._project_parameters["problem_data"]["echo_level"].GetInt()
            if self._project_parameters["problem_data"].Has("problem_name"):
                self._problem_name = self._project_parameters["problem_data"]["problem_name"].GetString()

    def AddFluidProcesses(self):
        # add process to manage assignation of material properties to particles
        # modify self._project_parameters to introduce this process in the problem_process_list
        # particles concept : assign initial material percent and properties vector pointer to nodes
        if(self._project_parameters.Has("problem_process_list")):
            problem_processes = self._project_parameters["problem_process_list"]
            #print(" PROBLEM_PROCESSES ", self._project_parameters["problem_process_list"].PrettyPrintJsonString())
            if self._has_slip_walls(self._project_parameters["constraints_process_list"]):
                extended_problem_processes = self._set_wall_properties_process(problem_processes)
                self._project_parameters.AddValue("problem_process_list", extended_problem_processes)
            extended_problem_processes = self._set_particle_properties_process(problem_processes)
            self._project_parameters.AddValue("problem_process_list", extended_problem_processes)
            extended_problem_processes = self._set_volume_recovery_process(problem_processes)
            self._project_parameters.AddValue("problem_process_list", extended_problem_processes)
            if(self._echo_level > 0):
                print(" EXTENDED_PROBLEM_PROCESSES ",
                      self._project_parameters["problem_process_list"].PrettyPrintJsonString())

        if(self._project_parameters.Has("loads_process_list")):
            loads_processes = self._project_parameters["loads_process_list"]
            if(self._echo_level > 1):
                print(" LOADS_PROCESSES ",
                      self._project_parameters["loads_process_list"].PrettyPrintJsonString())
            extended_loads_processes = self._set_volume_acceleration_process(
                loads_processes)
            self._project_parameters.AddValue(
                "loads_process_list", extended_loads_processes)
            if(self._echo_level > 0):
                print(" EXTENDED_LOADS_PROCESSES ",
                      self._project_parameters["loads_process_list"].PrettyPrintJsonString())
            self._project_parameters["loads_process_list"] = self._project_parameters["loads_process_list"]

        if(self._project_parameters.Has("constraints_process_list")):
            constraints_processes = self._project_parameters["constraints_process_list"]
            if(self._echo_level > 1):
                print(" CONSTRAINTS_PROCESSES ",
                      self._project_parameters["constraints_process_list"].PrettyPrintJsonString())
            extended_constraints_processes = self._set_isolated_nodes_management_process(constraints_processes)
            self._project_parameters.AddValue("constraints_process_list", extended_constraints_processes)
            extended_constraints_processes = self._set_selected_elements_management_process(constraints_processes)
            self._project_parameters.AddValue("constraints_process_list", extended_constraints_processes)
            extended_constraints_processes = self._set_ale_mesh_movement_constraints(constraints_processes)
            self._project_parameters.AddValue("constraints_process_list", extended_constraints_processes)
            # extended_constraints_processes = self._set_free_surface_pressure(constraints_processes)
            # self._project_parameters.AddValue("constraints_process_list", extended_constraints_processes)
            if(self._echo_level > 0):
                print(" EXTENDED_CONSTRAINTS_PROCESSES ",
                      self._project_parameters["constraints_process_list"].PrettyPrintJsonString())

        # Attention some variables from solver settings are also extended in ale mesh movement constraints
        if(self._echo_level > 0):
            print(" EXTENDED_SOLVER_PARAMETERS ", self._project_parameters["solver_settings"].PrettyPrintJsonString())

        return self._project_parameters

    def _set_isolated_nodes_management_process(self, constraints_processes):

        default_settings = KratosMultiphysics.Parameters("""
        {
             "process_module" : "PfemApplication.manage_isolated_nodes_process",
             "Parameters"    : {}
        }
        """)


        default_settings["Parameters"].AddEmptyValue(
            "model_part_name").SetString(self._problem_name)

        constraints_processes.Append(default_settings)

        return constraints_processes

    def _set_selected_elements_management_process(self, constraints_processes):

        default_settings = KratosMultiphysics.Parameters("""
        {
             "process_module" : "PfemApplication.manage_selected_elements_process",
             "Parameters"    : {}
        }
        """)


        default_settings["Parameters"].AddEmptyValue(
            "model_part_name").SetString(self._problem_name)

        constraints_processes.Append(default_settings)

        return constraints_processes

    def _set_ale_rotation_process(self, constraints_processes):

        default_settings = KratosMultiphysics.Parameters("""
        {
             "process_module" : "PfemApplication.ale_rotation_process",
             "Parameters"    : {}
        }
        """)


        default_settings["Parameters"].AddEmptyValue(
            "model_part_name").SetString(self._problem_name)

        constraints_processes.Append(default_settings)

        return constraints_processes

    def _set_volume_acceleration_process(self, loads_processes):

        gravity_active = False
        for process in loads_processes:
            if process["Parameters"].Has("variable_name"):
                if process["Parameters"]["variable_name"].GetString() == "VOLUME_ACCELERATION":
                    process["Parameters"]["model_part_name"].SetString(self._problem_name)
                    gravity_active = True
                    if not process["Parameters"].Has("flags_list"):
                        process["Parameters"].AddEmptyList("flags_list")
                        process["Parameters"]["flags_list"].Append("FLUID")
                    else:
                        flags_list = process["Parameters"]["flags_list"]
                        if not "FLUID" in flags_list:
                            process["Parameters"]["flags_list"].Append("FLUID")

        if not gravity_active:
            default_settings = KratosMultiphysics.Parameters("""
            {
                "process_module" : "SolidMechanicsApplication.assign_modulus_and_direction_to_nodes_process",
                "Parameters"    : {
                     "variable_name"   : "VOLUME_ACCELERATION",
                     "modulus"         : 9.81,
                     "direction"       : [0.0,-1.0,0.0]
                 }
            }
            """)

            if(self._project_parameters.Has("problem_data")):
                if(self._project_parameters["problem_data"].Has("gravity_vector")):
                    import math

                    # get normalized direction
                    direction = []
                    scalar_prod = 0
                    for i in range(self._project_parameters["problem_data"]["gravity_vector"].size()):
                        direction.append(self._project_parameters["problem_data"]["gravity_vector"][i].GetDouble())
                        scalar_prod = scalar_prod + direction[i]*direction[i]

                    norm = math.sqrt(scalar_prod)

                    value = []
                    if(norm != 0.0):
                        for j in direction:
                            value.append(j/norm)
                    else:
                        for j in direction:
                            value.append(0.0)

                    if(default_settings["Parameters"].Has("modulus")):
                        default_settings["Parameters"]["modulus"].SetDouble(norm)

                    if(default_settings["Parameters"].Has("direction")):
                        counter = 0
                        for i in value:
                            default_settings["Parameters"]["direction"][counter].SetDouble(i)
                            counter += 1

                    default_settings["Parameters"].AddEmptyValue(
                        "model_part_name").SetString(self._problem_name)

                    loads_processes.Append(default_settings)


        if not self._project_parameters["problem_data"].Has("gravity_vector"):
            for process in loads_processes:
                if process["Parameters"].Has("variable_name"):
                    if process["Parameters"]["variable_name"].GetString() == "VOLUME_ACCELERATION":
                        modulus = process["Parameters"]["modulus"].GetDouble()
                        direction = process["Parameters"]["direction"].GetVector()
                        direction = direction * modulus;
                        print(" Setting GRAVITY ", direction)
                        self._project_parameters["problem_data"].AddEmptyValue("gravity_vector").SetVector(direction)

        solver_parameters = self._project_parameters["solver_settings"]["Parameters"]
        if solver_parameters.Has("time_integration_settings"):
            if solver_parameters["time_integration_settings"].Has("formulation_type"):
                if solver_parameters["time_integration_settings"]["formulation_type"].GetString() == "ALE":
                    # Replace VOLUME_ACCELERATION for BODY_FORCE
                    for process in loads_processes:
                        if process["Parameters"].Has("variable_name"):
                            if process["Parameters"]["variable_name"].GetString() == "VOLUME_ACCELERATION":
                                process["Parameters"]["variable_name"].SetString("BODY_FORCE")

        return loads_processes

    def _set_wall_properties_process(self, problem_processes):

        add_default_process = True
        for process in problem_processes:
            if process.Has("python_module"):
                if process["python_module"].GetString() == 'assign_viscosity_to_slip_walls_process':
                    add_default_process = False
            if process.Has("process_module"):
                if process["process_module"].GetString() == 'PfemApplication.assign_viscosity_to_slip_walls_process':
                    add_default_process = False

        if add_default_process:
            default_settings = KratosMultiphysics.Parameters("""
            {
              "process_module" : "PfemApplication.assign_viscosity_to_slip_walls_process",
              "Parameters"    : {
                "properties_id" : 1,
                "variables" : {
                     "BULK_MODULUS" : 1e+9,
                     "DYNAMIC_VISCOSITY" : 0.005
                },
                "flags_list" : ["SLIP"]
              }
            }
            """)

            # it is important in slip problems to have a high dynamic viscosity for wall elements x100

            default_settings["Parameters"].AddEmptyValue(
                "model_part_name").SetString(self._problem_name)
            problem_processes.Append(default_settings)

        return problem_processes

    def _set_particle_properties_process(self, problem_processes):

        add_default_process = True
        for process in problem_processes:
            if process.Has("python_module"):
                if process["python_module"].GetString() == 'assign_properties_to_nodes_process':
                    add_default_process = False
            if process.Has("process_module"):
                if process["process_module"].GetString() == 'PfemApplication.assign_properties_to_nodes_process':
                    add_default_process = False

        if add_default_process:
            default_settings = KratosMultiphysics.Parameters("""
            {
              "process_module" : "PfemApplication.assign_properties_to_nodes_process",
              "Parameters"    : {
                "properties_id" : 1,
                "variables" : ["DENSITY","DYNAMIC_VISCOSITY","BULK_MODULUS"],
                "fluid_mixture": false,
                "solid_mixture": false
              }
            }
            """)

            default_settings["Parameters"].AddEmptyValue(
                "model_part_name").SetString(self._problem_name)
            problem_processes.Append(default_settings)

        return problem_processes

    def _set_volume_recovery_process(self, problem_processes):

        default_settings = KratosMultiphysics.Parameters("""
        {
            "process_module" : "PfemApplication.volume_recovery_process",
            "Parameters"    : {
              "solving_loss" : true,
              "meshing_loss" : true
            }
        }
        """)


        default_settings["Parameters"].AddEmptyValue(
            "model_part_name").SetString(self._problem_name)

        problem_processes.Append(default_settings)

        return problem_processes

    def _set_ale_mesh_movement_constraints(self, constraints_processes):

        solver_parameters = self._project_parameters["solver_settings"]["Parameters"]
        if solver_parameters.Has("time_integration_settings"):
            if solver_parameters["time_integration_settings"].Has("formulation_type"):
                if solver_parameters["time_integration_settings"]["formulation_type"].GetString() == "ALE":
                    # Add ALE variables (FluidDynamicsApplication)
                    if self._project_parameters.Has("model_settings"):
                        model_settings = self._project_parameters["model_settings"]
                        ale_variables = ["BODY_FORCE","ADVPROJ","DIVPROJ","NODAL_AREA"]
                        if not model_settings.Has("variables"):
                            model_settings.AddEmptyList("variables")
                        variables= model_settings["variables"]
                        for variable in ale_variables:
                            variables.Append(variable) if variable not in variables else variables

                    # Add ALE integration variables
                    if not solver_parameters["time_integration_settings"].Has("integration_variables"):
                        solver_parameters["time_integration_settings"].AddEmptyList("integration_variables")

                    integration_variables = solver_parameters["time_integration_settings"]["integration_variables"]
                    integration_variables.Append("MESH_VELOCITY") if "MESH_VELOCITY" not in integration_variables else integration_variables

                    if not solver_parameters["time_integration_settings"].Has("move_mesh_flag"):
                        solver_parameters["time_integration_settings"].AddEmptyValue("move_mesh_flag").SetBool(True)

                    # Check slip walls
                    slip_walls = False
                    for i in range(constraints_processes.size()):
                        assign_flags = False
                        if constraints_processes[i].Has("python_module"):
                            if constraints_processes[i]["python_module"].GetString() == 'assign_flags_process':
                                assign_flags = True
                        if constraints_processes[i].Has("process_module"):
                            if constraints_processes[i]["process_module"].GetString() == 'SolidMechanicsApplication.assign_flags_process':
                                assign_flags = True
                        if assign_flags:
                            if constraints_processes[i]["Parameters"].Has("flag_name"):
                                if constraints_processes[i]["Parameters"]["flag_name"].GetString() == "SLIP":
                                    slip_walls = True
                                    break
                            if constraints_processes[i]["Parameters"].Has("flags_list"):
                                for flag_name in constraints_processes[i]["Parameters"]["flags_list"]:
                                    if flag_name.GetString() == "SLIP":
                                        slip_walls = True
                                        break
                    if slip_walls:
                        print(self._class_prefix() +
                              " Add Processes : Mesh_Velocity")
                        for i in range(constraints_processes.size()):
                            if constraints_processes[i].Has("Parameters"):
                                if constraints_processes[i]["Parameters"].Has("variable_name"):
                                    variable_name = constraints_processes[i]["Parameters"]["variable_name"].GetString(
                                    )
                                    if variable_name in ("DISPLACEMENT", "VELOCITY", "ACCELERATION"):
                                        ale_settings = constraints_processes[i].Clone()
                                        ale_settings["Parameters"]["variable_name"].SetString(
                                            "MESH"+"_"+variable_name)
                                        ale_settings["Parameters"].AddEmptyList(
                                            "flags_list")
                                        ale_settings["Parameters"]["flags_list"].Append(
                                            "SLIP")
                                        if constraints_processes[i].Has("constrained"):
                                            ale_settings["Parameters"]["constrained"].SetBool(
                                                False)
                                            #constraints_processes[i]["Parameters"]["constrained"].SetBool(False)
                                        else:
                                            ale_settings["Parameters"].AddEmptyValue(
                                                "constrained").SetBool(False)
                                            #constraints_processes[i]["Parameters"].AddEmptyValue("constrained").SetBool(False)
                                        #ale_settings["Parameters"].AddEmptyValue("constrained").SetBool(False)
                                        constraints_processes[i]["Parameters"].AddEmptyList(
                                            "flags_list")
                                        constraints_processes[i]["Parameters"]["flags_list"].Append(
                                            "NOT_SLIP")
                                        # set settings at the end
                                        constraints_processes.Append(
                                            ale_settings)

        return constraints_processes

    def _has_slip_walls(self, constraints_processes):
        slip_walls = False
        solver_parameters = self._project_parameters["solver_settings"]["Parameters"]
        if solver_parameters.Has("time_integration_settings"):
            if solver_parameters["time_integration_settings"].Has("formulation_type"):
                if solver_parameters["time_integration_settings"]["formulation_type"].GetString() == "ALE":
                    # Check slip walls
                    for i in range(constraints_processes.size()):
                        assign_flags = False
                        if constraints_processes[i].Has("python_module"):
                            if constraints_processes[i]["python_module"].GetString() == 'assign_flags_process':
                                assign_flags = True
                        if constraints_processes[i].Has("process_module"):
                            if constraints_processes[i]["process_module"].GetString() == 'SolidMechanicsApplication.assign_flags_process':
                                assign_flags = True
                        if assign_flags:
                            if constraints_processes[i]["Parameters"].Has("flag_name"):
                                if constraints_processes[i]["Parameters"]["flag_name"].GetString() == "SLIP":
                                    slip_walls = True
                                    break
                            if constraints_processes[i]["Parameters"].Has("flags_list"):
                                for flag_name in constraints_processes[i]["Parameters"]["flags_list"]:
                                    if flag_name.GetString() == "SLIP":
                                        slip_walls = True
                                        break
        return slip_walls

    def _set_free_surface_pressure(self, constraints_processes):

        solver_parameters = self._project_parameters["solver_settings"]["Parameters"]
        if solver_parameters.Has("time_integration_settings"):
            if solver_parameters["time_integration_settings"].Has("formulation_type"):
                if solver_parameters["time_integration_settings"]["formulation_type"].GetString() == "ALE":

                    default_settings = KratosMultiphysics.Parameters("""
                    {
                        "process_module" : "PfemApplication.assign_free_surface_pressure_process",
                        "Parameters"    : {
                        }
                    }
                    """)


                    default_settings["Parameters"].AddEmptyValue(
                        "model_part_name").SetString(self._problem_name)

                    constraints_processes.Append(default_settings)

        return constraints_processes

    @classmethod
    def _class_prefix(self):
        header = "::[-- PFEM Defaults --]::"
        return header
