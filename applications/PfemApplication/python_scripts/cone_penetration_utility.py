""" Project: PfemApplication
    Developer: LMonforte
    Maintainer: LM
"""

# Built-in/Generic Imports
import os.path

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolidMechanicsApplication as KratosSolid
import KratosMultiphysics.PfemApplication as KratosPfem
import KratosMultiphysics.ContactMechanicsApplication as KratosContact


def Factory(custom_settings, Model):
    if(type(custom_settings) != KratosMultiphysics.Parameters):
        raise Exception(
            "expected input shall be a Parameters object, encapsulating a json string")
    return ConePenetrationUtility(Model, custom_settings["Parameters"])


class ConePenetrationUtility(KratosMultiphysics.Process):
    #

    def __init__(self, Model, custom_settings):

        KratosMultiphysics.Process.__init__(self)

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
         "cone_radius"        : 0.01784,
         "u2_initial_depth"   : 0.0332,
         "velocity"           : 0.02,
         "dissipation_set"    : false,
         "unfix_water_pressure": true,
         "dissipation_depth"  : 0.2676,
         "model_part_name"         : "Main_Domain",
         "model_part_name_BC" : "Null",
         "bothkennar_post": false
        }
        """)

        settings = custom_settings
        settings.ValidateAndAssignDefaults(default_settings)


        self.model_part_name = settings["model_part_name"].GetString() 
        
        self.displacement_BC = False
        if ( settings["model_part_name_BC"].GetString() != "Null"):
            self.model_part_name_BC = settings["model_part_name_BC"].GetString()
            name = self.model_part_name.split('.')
            name = name[0] +"."+self.model_part_name_BC
            self.model_part_name_BC = name
            self.displacement_BC = True;

        self.model = Model

        self.Y0 = settings["u2_initial_depth"].GetDouble()
        self.Vy = settings["velocity"].GetDouble()
        self.radius = settings["cone_radius"].GetDouble()

        self.dissipation_set = settings["dissipation_set"].GetBool()
        if (self.dissipation_set):
            self.dissipation_depth = settings["dissipation_depth"].GetDouble()

        self.Bothkennar = settings["bothkennar_post"].GetBool()

        problem_path = os.getcwd()
        self.figure_path = os.path.join(
            problem_path, "cone_penetration_data.csv")
        self.previousU2position = 1000.0

        self.unfix_water_pressure = settings["unfix_water_pressure"].GetBool()

    def ExecuteInitializeSolutionStep(self):

        self.model_part = self.model[self.model_part_name]
        main_model_part = self.model_part.GetRootModelPart()
        
        desiredFriction = 0.0;
        desiredPenalty = 0.0;
        for properties in main_model_part.Properties:
            aux1 = properties[KratosMultiphysics.ContactMechanicsApplication.DESIRED_CONTACT_FRICTION_ANGLE]
            if ( aux1 > desiredFriction):
                desiredFriction = aux1
            aux1 = properties[KratosMultiphysics.ContactMechanicsApplication.TANGENTIAL_PENALTY_RATIO]
            if ( aux1 > desiredPenalty):
                desiredPenalty = aux1

        for properties in main_model_part.Properties:

            value = properties[KratosMultiphysics.ContactMechanicsApplication.CONTACT_FRICTION_ANGLE]
            if (self.model_part.ProcessInfo[KratosMultiphysics.IS_RESTARTED] == True):
                value = properties[KratosMultiphysics.ContactMechanicsApplication.DESIRED_CONTACT_FRICTION_ANGLE]
            value = value+0.05
            if ( value > desiredFriction):
                value = desiredFriction
            properties[KratosMultiphysics.ContactMechanicsApplication.CONTACT_FRICTION_ANGLE] = value
            properties[KratosMultiphysics.ContactMechanicsApplication.TANGENTIAL_PENALTY_RATIO] = desiredPenalty







    def ExecuteBeforeOutputStep(self):

        self.model_part = self.model[self.model_part_name]
        if ( self.displacement_BC):
            self.model_part_BC = self.model[self.model_part_name_BC]


        delta_time = self.model_part.ProcessInfo[KratosMultiphysics.DELTA_TIME]

        for node in self.model_part.GetNodes(0):
            delta_disp = node.GetSolutionStepValue(
                KratosMultiphysics.DISPLACEMENT)
            delta_disp = delta_disp - \
                node.GetSolutionStepValue(KratosMultiphysics.DISPLACEMENT, 1)
            for ii in range(0, 2):
                delta_disp[ii] = delta_disp[ii] / delta_time
            if (node.SolutionStepsDataHas(KratosMultiphysics.VELOCITY)):
                node.SetSolutionStepValue(
                    KratosMultiphysics.VELOCITY, delta_disp)

    def ExecuteAfterOutputStep(self):
        
        self.model_part = self.model[self.model_part_name]
        if ( self.displacement_BC):
            self.model_part_BC = self.model[self.model_part_name_BC]

        for node in self.model_part.GetNodes(0):
            # if ( node.SolutionStepsDataHas( KratosMultiphysics.CONTACT_FORCE)):
            #    CF = node.GetSolutionStepValue(KratosMultiphysics.CONTACT_FORCE)
            #    CF = 0.0*CF
            #    node.SetSolutionStepValue( KratosMultiphysics.CONTACT_FORCE, CF)

            if (node.SolutionStepsDataHas(KratosContact.CONTACT_STRESS)):
                CF = node.GetSolutionStepValue(KratosContact.CONTACT_STRESS)
                CF = 0.0*CF
                node.SetSolutionStepValue(KratosContact.CONTACT_STRESS, CF)

            if (node.SolutionStepsDataHas(KratosContact.EFFECTIVE_CONTACT_FORCE)):
                CF = node.GetSolutionStepValue(
                    KratosContact.EFFECTIVE_CONTACT_FORCE)
                CF = 0.0*CF
                node.SetSolutionStepValue(
                    KratosContact.EFFECTIVE_CONTACT_FORCE, CF)

            if (node.SolutionStepsDataHas(KratosContact.EFFECTIVE_CONTACT_STRESS)):
                CF = node.GetSolutionStepValue(
                    KratosContact.EFFECTIVE_CONTACT_STRESS)
                CF = 0.0*CF
                node.SetSolutionStepValue(
                    KratosContact.EFFECTIVE_CONTACT_STRESS, CF)

            #if (node.SolutionStepsDataHas(KratosMultiphysics.CONTACT_NORMAL)):
            #    CF = node.GetSolutionStepValue(
            #        KratosMultiphysics.CONTACT_NORMAL)
            #    CF = 0.0*CF
            #    node.SetSolutionStepValue(
            #        KratosMultiphysics.CONTACT_NORMAL, CF)
            
            if (node.SolutionStepsDataHas(KratosContact.CONTACT_FRICTION_ANGLE)):
                CF = node.GetSolutionStepValue(KratosContact.CONTACT_FRICTION_ANGLE)
                CF = 0.0*CF
                node.SetSolutionStepValue(KratosContact.CONTACT_FRICTION_ANGLE, CF)

            if (node.SolutionStepsDataHas(KratosContact.CONTACT_PLASTIC_SLIP)):
                CF = node.GetSolutionStepValue(KratosMultiphysics.CONTACT_FORCE)
                if ( abs(CF[0]) + abs(CF[1]) < 1E-6):
                    CF = node.GetSolutionStepValue(KratosContact.CONTACT_PLASTIC_SLIP)
                    CF = 0.0*CF
                    node.SetSolutionStepValue(KratosContact.CONTACT_PLASTIC_SLIP, CF)

            if (node.SolutionStepsDataHas(KratosMultiphysics.VELOCITY)):
                    d1 = node.GetSolutionStepValue( KratosMultiphysics.DISPLACEMENT)
                    d2 = node.GetSolutionStepValue( KratosMultiphysics.DISPLACEMENT, 1)
                    node.SetSolutionStepValue( KratosMultiphysics.VELOCITY, d1-d2)

            if (node.SolutionStepsDataHas(KratosSolid.DISPLACEMENT_REACTION)):
                # print(node)
                for step in range(0, 2):
                    CF = node.GetSolutionStepValue(
                        KratosSolid.DISPLACEMENT_REACTION, step)
                    normal = node.GetSolutionStepValue(
                        KratosMultiphysics.NORMAL, step)
                    #print('NODE', node.Id, ' normal', normal)
                    #print('  step ', step, 'REACTION', CF)
                    CF = 0.0*CF
                    #print('  step ', step, 'REACTION', CF)
                    node.SetSolutionStepValue(
                        KratosSolid.DISPLACEMENT_REACTION, step, CF)
                # print('----------')

    #
    def ExecuteFinalizeSolutionStep(self):
        
        self.model_part = self.model[self.model_part_name]
        if ( self.displacement_BC):
            self.model_part_BC = self.model[self.model_part_name_BC]

        self.model_part = self.model[self.model_part_name]
        Q = self._GetResistance()
        Friction = self._GetFriction()

        U22 = self._GetPorePressureU2()
        U33 = self._GetPorePressureU3()
        U11 = self._GetPorePressureU1()

        time = self._GetStepTime()  # - self.GetStepDeltaTime()

        if ( self.Bothkennar):
            line_bothkennar = self._BothkennarPostProcess()
        else:
            line_bothkennar = ''
        
            


        line_value = str(time) + " , " + str(Q) + " , " + str(Friction) + \
            " , " + str(U22) + " , " + str(U11) + " , " + str(U33) +  line_bothkennar + " \n"

        figure_file = open(self.figure_path, "a")
        figure_file.write(line_value)
        figure_file.close()

        if ( self.unfix_water_pressure):
            self.TryToUnFixNodes()

        self.SetAccelerateTime()
        # self.MakeTheOtherFile();


    def SetAccelerateTime(self):

        self.model_part.ProcessInfo[KratosSolid.ACCELERATE_TIME] = 1.0;
        u2Now = self._GetU2Position()
        if ( abs(u2Now-self.previousU2position) < 1E-6):
            self.model_part.ProcessInfo[KratosSolid.ACCELERATE_TIME] = 1.02;
        self.previousU2position = u2Now





    def _BothkennarPostProcess(self):
        import numpy as np
        
        line = ""
        cfx = np.zeros(0,dtype=float)
        cfy = np.zeros(0,dtype=float)
        wp = np.zeros(0,dtype=float)
        z = np.zeros(0,dtype=float)

        for node in self.model_part.GetNodes(0):
            Force = node.GetSolutionStepValue(KratosMultiphysics.CONTACT_FORCE)
            if ( abs(Force[0]) + abs(Force[1]) > 1E-12):
                Force = node.GetSolutionStepValue(KratosContact.CONTACT_STRESS)
                #print(Force)
                cfx = np.append( cfx,  Force[0])
                cfy = np.append(cfy, Force[1])
    
                z = np.append(z, node.Y)
                if (node.HasDofFor(KratosMultiphysics.WATER_PRESSURE)):
                    aux = node.GetSolutionStepValue(
                        KratosMultiphysics.WATER_PRESSURE)
                    wp = np.append(wp, aux)
                else:
                    wp = np.append(wp, 0.0)
        
        index = np.argsort(z)
        z = z[index]
        cfx = cfx[index]
        cfy = cfy[index]
        wp = wp[index]

        xx = np.arange(1.75, 2.0)
        xx = np.append(xx,  np.arange(2.0, 40.0) )
        for depth_num in range(0,xx.size -1 ):
            Y = 0.0 - self.Vy * self._GetStepTime()
            if ( self.dissipation_set):
                if ( Y < - self.dissipation_depth):
                    Y = -self.dissipation_depth
            Y1 = Y + xx[depth_num]*self.radius
            Y2 = Y + xx[depth_num+1]*self.radius

            from scipy.interpolate import InterpolatedUnivariateSpline

            f = InterpolatedUnivariateSpline(z,cfx,k=1)
            rX = f.integral(Y1, Y2)
            f = InterpolatedUnivariateSpline(z,cfy, k=1)
            rY = f.integral(Y1, Y2)
            f = InterpolatedUnivariateSpline(z,wp, k=1)
            wP = f.integral(Y1, Y2)

        
            wP = wP/(Y2-Y1)
            rX = rX/(Y2-Y1)
            rY = rY/(Y2-Y1)
            line = line + " , " + str(rX) + "  , " + str(rY) + " , "  + str(wP)

        return line




    def _DoBothkennar2(self, depth):
        Y = self.Y0 - self.Vy * self._GetStepTime()
        Y1 = Y + depth*self.radius
        Y2 = Y + (depth+1.0)*self.radius

        rX = 0.0;
        rY = 0.0;
        wP = 0.0;
        num = 0.0;
       
        yMin = 100.0;
        yMax = -100.0;
        for node in self.model_part.GetNodes(0):
            if ( node.Y > Y1 and node.Y < Y2):
                Force = node.GetSolutionStepValue(
                    KratosMultiphysics.CONTACT_FORCE)
                rX = rX + Force[0]
                rY = rY + Force[1]
            if ( node.Y > yMax):
                yMax = node.Y
            if ( node.Y < yMin):
                yMin = node.Y
            if (node.HasDofFor(KratosMultiphysics.WATER_PRESSURE)):
                aux = node.GetSolutionStepValue(
                    KratosMultiphysics.WATER_PRESSURE)
                wP = wP+aux;
                num = num+1.0

        if ( abs(wP) > 0.1):
            wP = wP/num;

        rX = rX * 1.0/(yMax-yMin)
        rY = rY * 1.0/(yMax-yMin)
        return [rX, rY, wP]


    # function to try to unfix the water pressure of nodes in that are in contact
    def TryToUnFixNodes(self):

        YLim = self._GetU2Position()

        for node in self.model_part.GetNodes(0):
            Force = node.GetSolutionStepValue(KratosMultiphysics.CONTACT_FORCE)
            Normal = node.GetSolutionStepValue(KratosMultiphysics.NORMAL)
            IsBoundary = False
            if (abs(Normal[0]) + abs(Normal[1]) > 1.0e-7):
                IsBoundary = True

            if (node.HasDofFor(KratosMultiphysics.WATER_PRESSURE)):
                if ((abs(Force[0]) + abs(Force[1]) > 1.0e-7)):
                    if (node.IsFixed(KratosMultiphysics.WATER_PRESSURE)):
                        node.Free(KratosMultiphysics.WATER_PRESSURE)
                        print(' UNFIXING ')
                elif (node.Y > YLim and node.X < 4.0*self.radius and IsBoundary):
                    if (node.IsFixed(KratosMultiphysics.WATER_PRESSURE) == False):
                        node.Fix(KratosMultiphysics.WATER_PRESSURE)
                        print(' UNFIXING :: FIXING ')

                elif (node.Y > YLim and node.X < 4.0*self.radius and (IsBoundary == False)):
                    if (node.IsFixed(KratosMultiphysics.WATER_PRESSURE)):
                        print(' UNFIXING :: INNER NODE :: DISASTER ')
                        node.Free(KratosMultiphysics.WATER_PRESSURE)

    # make the other stupid file just to check everything is sort of correct
    def MakeTheOtherFile(self):

        time = self._GetStepTime()

        problem_path = os.getcwd()
        self.other_file = os.path.join(problem_path, "other_file_data.csv")
        self.other_file = open(self.other_file, "a")

        time = str(time) + " \n "
        self.other_file.write(time)

        YLim = self.Y0 - self.Vy * self._GetStepTime()
        YLim = self._GetU2Position()

        for node in self.model_part.GetNodes(0):
            Force = node.GetSolutionStepValue(KratosMultiphysics.CONTACT_FORCE)
            if ((abs(Force[0]) + abs(Force[1]) > 1.0e-7) or (node.X0 < 1e-5 and node.Y > YLim-0.1)):
                x = node.X
                y = node.Y
                Force = node.GetSolutionStepValue(
                    KratosMultiphysics.CONTACT_FORCE)
                Stress = node.GetSolutionStepValue(
                    KratosContact.CONTACT_STRESS)
                EfForce = node.GetSolutionStepValue(
                    KratosContact.EFFECTIVE_CONTACT_FORCE)
                EfStress = node.GetSolutionStepValue(
                    KratosContact.EFFECTIVE_CONTACT_STRESS)
                WP = node.GetSolutionStepValue(
                    KratosMultiphysics.WATER_PRESSURE)

                line = str(x) + "  " + str(y) + "  " + \
                    str(YLim) + " " + str(WP) + " "
                line = line + self.AddVector(Force)
                line = line + self.AddVector(Stress)
                line = line + self.AddVector(EfForce)
                line = line + self.AddVector(EfStress)
                line = line + "\n"
                self.other_file.write(line)

        self.other_file.close()

    def AddVector(self, vector):
        line = str(vector[0]) + " " + str(vector[1]) + " "
        return line
    #
    def _GetStepTime(self):

        return self.model_part.ProcessInfo[KratosMultiphysics.TIME]

    #
    def _GetStepDeltaTime(self):

        return self.model_part.ProcessInfo[KratosMultiphysics.DELTA_TIME]

    def _GetU2Position(self):
        avance = self.Vy*self._GetStepTime()
        if (self.dissipation_set):
            if (avance > self.dissipation_depth):
                avance = self.dissipation_depth

        if ( self.displacement_BC):
            for node in self.model_part_BC.GetNodes(0):
                displ = node.GetSolutionStepValue( KratosMultiphysics.DISPLACEMENT)
                avance = -1.0*displ[1]

        YSearch = self.Y0 - avance
        return YSearch

    def _GetPorePressureU2(self):
        YSearch = self._GetU2Position()
        U22 = self._GetPorePressureShaft(YSearch)
        return U22

    def _GetPorePressureU3(self):
        YSearch = self._GetU2Position()
        YSearch = YSearch + 7.5*self.radius
        U33 = self._GetPorePressureShaft(YSearch)
        return U33

    def _GetPorePressureU1(self):
        YSearch = self._GetU2Position()
        YSearch = YSearch - self.radius / 0.57734967 /2.0
        U11 = self._GetPorePressureShaft(YSearch)
        return U11

    def _GetResistance(self):
        result = 0.0
        
        YLim = self._GetU2Position()
        
        for node in self.model_part.GetNodes(0):
            if (node.Y < YLim):
                Force = node.GetSolutionStepValue(
                    KratosMultiphysics.CONTACT_FORCE)
                result = result + Force[1]

        return result

    #
    def _GetFriction(self):
        result = 0
        YMin = self._GetU2Position()
        YMax = YMin + 7.5*self.radius

        for node in self.model_part.GetNodes(0):
            if (node.Y >= YMin):
                if (node.Y <= YMax):
                    Force = node.GetSolutionStepValue(
                        KratosMultiphysics.CONTACT_FORCE)
                    result = result + Force[1]

        return result

    def _GetPorePressureShaft(self, YSearch):

        nodes = self.model_part.GetNodes()
        for node in nodes:
            if (node.HasDofFor(KratosMultiphysics.WATER_PRESSURE)):
                variable = KratosMultiphysics.WATER_PRESSURE
            elif (node.HasDofFor(KratosMultiphysics.PRESSURE)):
                variable = KratosMultiphysics.PRESSURE
            else:
                return 0.0

        YBestTop = 100000000
        YBestBottom = -100000000
        nBestTop = -100
        nBestBottom = -100

        for node in self.model_part.GetNodes(0):
            Force = node.GetSolutionStepValue(KratosMultiphysics.CONTACT_FORCE)
            condition1 = abs(Force[0]) + abs(Force[1]) > 1e-8
            Normal = node.GetSolutionStepValue(KratosMultiphysics.NORMAL)
            condition2 = (abs(Normal[0]) + abs(Normal[1])
                          > 1e-8 and node.X < 3*self.radius)
            condition3 = abs(node.GetSolutionStepValue(variable)) > 1e-8
            condition3 = node.Is(KratosMultiphysics.RIGID) == False
            if ((condition1 or condition2) and condition3):
                YThis = node.Y
                if ((YThis-YSearch) > 0):
                    if (abs(YThis-YSearch) < abs(YBestTop - YSearch)):
                        nBestTop = node.Id
                        YBestTop = YThis
                elif ((YThis-YSearch) <= 0):
                    if (abs(YThis-YSearch) < abs(YBestBottom - YSearch)):
                        nBestBottom = node.Id
                        YBestBottom = YThis

        if ((nBestTop < 1) or (nBestBottom < 1)):
            print(
                ' In the Usomething. NotFound Contacting nodes that are in the range of this kind of thing')
            return 0.0

        # Now i want to kwnow if there is an element that has these nodes
        ReallyFound = False
        for elem in self.model_part.GetElements(0):
            a = [1, 2, 3]
            conec = []
            found = 0
            for thisNodeGeom in elem.GetNodes():
                thisNode = thisNodeGeom.Id
                if (thisNode == nBestTop):
                    found = found + 1
                if (thisNode == nBestBottom):
                    found = found + 1
            if (found == 2):
                ReallyFound = True
                break

        if (ReallyFound == False):
            print(' In the U Something. The two nodes do not share an element ')
            return 0.0

        DeltaY = abs(YBestBottom - YBestTop)
        NTop = 1 - abs(YBestTop - YSearch) / DeltaY
        NBottom = 1 - NTop

        if (NTop > 1.0 or NTop < 0):
            print('ULTRA MEGA STUPID ERROR ')

        if (NBottom > 1.0 or NBottom < 0):
            print('ULTRA MEGA STUPID ERROR ')

        uBottom = nodes[nBestBottom].GetSolutionStepValue(variable)
        uTop = nodes[nBestTop].GetSolutionStepValue(variable)
        ThisV = NTop*uTop + NBottom*uBottom
        return ThisV
