""" Project: PfemApplication
    Developer: JMCarbonell
    Maintainer: LMonforte
"""

# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.PfemApplication as KratosPfem


def Factory(settings, Model):
    if(not isinstance(settings, KratosMultiphysics.Parameters)):
        raise Exception(
            "Expected input shall be a Parameters object, encapsulating a json string")
    return ManageTimeProcess(Model, settings["Parameters"])


class ManageTimeProcess(KratosMultiphysics.Process):
    #
    def __init__(self, Model, custom_settings):

        KratosMultiphysics.Process.__init__(self)

        # "model_part_name"       : "Main_Domain",
        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
        "end_time": 1.0,
        "start_time": 0.0,
        "time_step": 0.1,
        "adaptive_time_step":  {
           "desired_iterations": 4,
           "increase_factor": 2.0,
           "decrease_factor": 0.5,
           "maximum_time_step": 0.1,
           "minimum_time_step": 0.001
        }
        }
        """)

        self.settings = custom_settings
        self.settings.ValidateAndAssignDefaults(default_settings)

        self.model_part = Model
        #self.model_part_name = self.settings["model_part_name"].GetString()
        self.model_part_name = "Main_Domain"

        self.model = Model

    def ExecuteInitialize(self):

        self.main_model_part = self.model[self.model_part_name]

        self.manage_time_cxx_process = KratosPfem.ManageTimeStepProcess(
            self.main_model_part, self.settings)

    def Execute(self):

        self.manage_time_cxx_process.Execute()

    def CheckIfStepConverged(self, converged):
        converged = self.manage_time_cxx_process.CheckStepConverged(converged)
        return converged
