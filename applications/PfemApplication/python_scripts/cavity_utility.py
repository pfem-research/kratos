""" Project: PfemApplication
    Developer: LMonforte
    Maintainer: LM
"""

# Built-in/Generic Imports
import os.path

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolidMechanicsApplication as KratosSolid
import KratosMultiphysics.PfemApplication as KratosPfem
import KratosMultiphysics.ContactMechanicsApplication as KratosContact


def Factory(custom_settings, Model):
    if(type(custom_settings) != KratosMultiphysics.Parameters):
        raise Exception(
            "expected input shall be a Parameters object, encapsulating a json string")
    return CavityUtility(Model, custom_settings["Parameters"])


class CavityUtility(KratosMultiphysics.Process):

    #
    def __init__(self, model_part, custom_settings):

        KratosMultiphysics.Process.__init__(self)

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
           "model_part_name": "Main_Domain"
        }
        """)

        settings = custom_settings
        settings.ValidateAndAssignDefaults(default_settings)

        self.model_part_name = settings["model_part_name"].GetString()
        self.model_part = model_part

        # initialize figure path
        problem_path = os.getcwd()
        self.csv_path = os.path.join(problem_path, "cavity.csv")
        csv_file = open(self.csv_path, "a")
        csv_file.close()

    def ExecuteFinalizeSolutionStep(self):


        model_part = self.model_part[self.model_part_name]
        SomeProcessInfo = KratosMultiphysics.ProcessInfo()

        time = model_part.ProcessInfo[KratosMultiphysics.TIME]
        X0 = 100000.0
        for node in model_part.GetNodes(0):
            if ( node.X0 < X0):
                X0 = node.X0


        Force = 0.0*KratosMultiphysics.Array3()
        Disp = 0.0*KratosMultiphysics.Array3()
        WP = 0.0
        nWP = 0

        for node in model_part.GetNodes(0):
            if (abs(node.X0-X0) < 1E-8):
                    res = node.GetSolutionStepValue(
                        KratosSolid.DISPLACEMENT_REACTION)
                    Force = Force + res
                    Disp = node.GetSolutionStepValue( KratosMultiphysics.DISPLACEMENT)
                    WP = WP + node.GetSolutionStepValue( KratosMultiphysics.WATER_PRESSURE)
                    nWP = nWP +1;

        WP = WP/ float(nWP)

        line = str(time) + " , "

        line = self._AddToLine(line, Disp)
        line = self._AddToLine(line, Force)
        line = line +  str(WP) + " , " 
        line = line + " \n"

        csv_file = open(self.csv_path, "a")
        csv_file.write(line)
        csv_file.close()

    def _AddToLine(self, line, vector):

        for i in vector:
            line = line + str(i) + " , "

        return line


