""" Project: PfemApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolidMechanicsApplication
import KratosMultiphysics.PfemApplication
import KratosMultiphysics.SolidMechanicsApplication.solid_analysis as BaseProcess


class PfemSolution(BaseProcess.SolidAnalysis):

    def __init__(self, model, file_parameters="ProjectParameters.json", file_name=None):

        super(PfemSolution, self).__init__(model, file_parameters, file_name)

    #### Main internal methods ####

    @classmethod
    def _class_prefix(self):
        header = "::[--PFEM Simulation--]::"
        return header


if __name__ == "__main__":
    PfemSolution().Run()
