""" Project: PfemApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.PfemApplication as KratosPfem


def Factory(custom_settings, Model):
    if(not isinstance(custom_settings, KratosMultiphysics.Parameters)):
        raise Exception(
            "expected input shall be a Parameters object, encapsulating a json string")
    return VolumeRecoveryProcess(Model, custom_settings["Parameters"])

# All the processes python should be derived from "Process"


class VolumeRecoveryProcess(KratosMultiphysics.Process):
    def __init__(self, Model, custom_settings):
        KratosMultiphysics.Process.__init__(self)

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
             "model_part_name": "main_domain",
             "solving_loss" :  true,
             "meshing_loss" :  true
        }
        """)

        # overwrite the default settings with user-provided parameters
        self.settings = custom_settings
        self.settings.ValidateAndAssignDefaults(default_settings)

        self.model = Model

    @classmethod
    def GetVariables(self):
        nodal_variables = []
        return nodal_variables

    def ExecuteInitialize(self):

        # set model part
        self.model_part = self.model[self.settings["model_part_name"].GetString(
        )]

        params = KratosMultiphysics.Parameters("{}")
        params.AddValue("solving_loss", self.settings["solving_loss"])
        params.AddValue("meshing_loss", self.settings["meshing_loss"])

        self.VolumeRecoveryProcess = KratosPfem.RecoverVolumeLosses(
            self.model_part, params)

        self.VolumeRecoveryProcess.ExecuteInitialize()

    def ExecuteBeforeSolutionLoop(self):

        self.VolumeRecoveryProcess.ExecuteBeforeSolutionLoop()

    def ExecuteInitializeSolutionStep(self):

        self.VolumeRecoveryProcess.ExecuteInitializeSolutionStep()

    def ExecuteFinalizeSolutionStep(self):

        self.VolumeRecoveryProcess.ExecuteFinalizeSolutionStep()

    def ExecuteAfterOutputStep(self):

        self.VolumeRecoveryProcess.ExecuteAfterOutputStep()
