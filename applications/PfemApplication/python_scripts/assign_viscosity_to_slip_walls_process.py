""" Project: PfemApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.PfemApplication as KratosPfem


def Factory(custom_settings, Model):
    if(not isinstance(custom_settings, KratosMultiphysics.Parameters)):
        raise Exception(
            "expected input shall be a Parameters object, encapsulating a json string")
    return AssignViscosityToSlipWallsProcess(Model, custom_settings["Parameters"])

# All the processes python should be derived from "Process"


class AssignViscosityToSlipWallsProcess(KratosMultiphysics.Process):
    def __init__(self, Model, custom_settings):
        KratosMultiphysics.Process.__init__(self)

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
             "model_part_name" : "main_domain",
             "properties_id"  : -1,
             "variables" : {
                 "DYNAMIC_VISCOSITY" : 0.05
             },
             "flags_list" : ["SLIP","RIGID"]

        }
        """)

        # overwrite the default settings with user-provided parameters
        self.settings = custom_settings
        self.settings.ValidateAndAssignDefaults(default_settings)

        self.model = Model


    def ExecuteInitialize(self):

        # set model part
        self.model_part = self.model[self.settings["model_part_name"].GetString()]

        # original properties
        self.reference_properties = self.model_part.Properties[self.settings["properties_id"].GetInt()]


        import KratosMultiphysics.SolidMechanicsApplication
        self.PropertiesUtilities = KratosMultiphysics.SolidMechanicsApplication.PropertiesUtilities()

        # material properties
        self.properties = self._GetModifiedProperties(self.reference_properties)

        if self._is_not_restarted():
            self.model_part.AddProperties(self.properties)

        # multiple entities and flags assignment
        self.flags_list = []
        for flag in self.settings["flags_list"]:
            self.flags_list.append(self._GetFlag(flag.GetString()))

        # print properties
        for properties in self.model_part.Properties:
            print(" "+str(properties.Id)+":",properties)


    def ExecuteInitializeSolutionStep(self):
        self._AssignMaterialProperties()

    def ExecuteFinalizeSolutionStep(self):
        self._RestoreMaterialProperties()

    # ativate these two methods and meshing before output to see the wall elements
    """
    def ExecuteBeforeOutputStep(self):
        self._AssignMaterialProperties()

    def ExecuteAfterOutputStep(self):
        self._RestoreMaterialProperties()
    """

    #
    def _GetModifiedProperties(self, reference_properties):

        # material properties
        Id=0
        for prop in self.model_part.Properties:
            if prop.Id>Id :
                Id = prop.Id

        if self._is_restarted():
            return self.model_part.Properties[Id]

        import KratosMultiphysics
        properties = KratosMultiphysics.Properties(Id+1)

        print(" Clone properties",str(reference_properties.Id)," to properties ",str(properties.Id)," number of properties",Id)

        # clone reference properties
        self.PropertiesUtilities.CloneProperties(reference_properties, properties)

        # overwrite variables or assign new variables
        variables = self.settings["variables"]
        for key, value in variables.items():
            try:
                my_key = "KratosMultiphysics."+key
                variable = self._GetItemFromModule(my_key)
            except:
                try:
                    my_key = "KratosMultiphysics.ConstitutiveModelsApplication."+key
                    variable = self._GetItemFromModule(my_key)
                except:
                    try:
                        my_key = "KratosMultiphysics.SolidMechanicsApplication."+key
                        variable = self._GetItemFromModule(my_key)
                    except:
                        my_key = "KratosMultiphysics.ContactMechanicsApplication."+key
                        variable = self._GetItemFromModule(my_key)
            if(value.IsDouble()):
                properties.SetValue(variable, value.GetDouble())
            elif(value.IsInt()):
                properties.SetValue(variable, value.GetInt())
            elif(value.IsArray()):
                vector_value = KratosMultiphysics.Vector(value.size())
                for i in range(0, value.size()):
                    vector_value[i] = value[i].GetDouble()
                properties.SetValue(variable, vector_value)

        return properties

    #
    def _AssignMaterialProperties(self):
        if self.model_part != None:
            # Assign properties to the model_part elements
            self.PropertiesUtilities.AssignPropertiesToElements(self.model_part,self.properties, self.flags_list)
            #for Element in self.model_part.Elements:
            #    if self._CheckFlags(Element,self.settings["flags_list"]):
            #        Element.Properties = self.properties
    #
    def _RestoreMaterialProperties(self):
        if self.model_part != None:
            # Assign properties to the model_part element
            self.PropertiesUtilities.AssignPropertiesToElements(self.model_part,self.reference_properties, self.flags_list)
            #for Element in self.model_part.Elements:
            #    if self._CheckFlags(Element,self.settings["flags_list"]):
            #        Element.Properties = self.reference_properties

    #
    def _CheckFlags(self, element, flags_list):

        size = flags_list.size()
        for Node in element.GetNodes():
            match = 0
            for Flag in flags_list:
                flag = self._GetFlag(Flag.GetString())
                if Node.Is(flag):
                    match += 1
                if match == size:
                    return True
        return False

    #
    def _GetFlag(self, flag_name):

        if flag_name.find("NOT_")==0:
            return KratosMultiphysics.KratosGlobals.GetFlag(flag_name[4:]).AsFalse()
        else:
            return KratosMultiphysics.KratosGlobals.GetFlag(flag_name)

    #
    def _GetItemFromModule(self, my_string):
        splitted = my_string.split(".")
        if(len(splitted) == 0):
            raise Exception(
                "something wrong. Trying to split the string "+my_string)
        if(len(splitted) == 1):
            return eval(my_string)
        else:
            module_name = ""
            for i in range(len(splitted)-1):
                module_name += splitted[i]
                if i != len(splitted)-2:
                    module_name += "."
            import importlib
            module = importlib.import_module(module_name)
            try:
                return getattr(module, splitted[-1])
            except:
                raise
    #
    def _is_restarted(self):
        if self.model_part.ProcessInfo[KratosMultiphysics.IS_RESTARTED]:
            return True
        else:
            return False
    #
    def _is_not_restarted(self):
        return not self._is_restarted()
