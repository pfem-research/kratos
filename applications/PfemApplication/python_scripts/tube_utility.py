""" Project: PfemApplication
Developer: LMonforte
    Maintainer: LM
"""

# Built-in/Generic Imports
import os.path

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolidMechanicsApplication as KratosSolid
import KratosMultiphysics.PfemApplication as KratosPfem
import KratosMultiphysics.ContactMechanicsApplication as KratosContact


def Factory(custom_settings, Model):
    if(type(custom_settings) != KratosMultiphysics.Parameters):
        raise Exception(
            "expected input shall be a Parameters object, encapsulating a json string")
    return TubeUtility(Model, custom_settings["Parameters"])


class TubeUtility(KratosMultiphysics.Process):
    #

    def __init__(self, Model, custom_settings):

        KratosMultiphysics.Process.__init__(self)

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
         "model_part_name": "Null",
         "model_part_name_soil"         : "Main_Domain",
         "model_part_name_tube" : "Null",
         "tube_left_lower_point": [0.0, 0.0, 0.0],
         "tube_right_lower_point": [0.0, 0.0, 0.0]
        }
        """)

        settings = custom_settings
        settings.ValidateAndAssignDefaults(default_settings)


        self.model_part_name = settings["model_part_name"].GetString() 

        self.model_part_name_soil = settings["model_part_name_soil"].GetString() 
        name = self.model_part_name.split('.')
        name = name[0] +"."+self.model_part_name_soil
        self.model_part_name_soil = name
        
        self.model_part_name_tube = settings["model_part_name_tube"].GetString()
        name = self.model_part_name.split('.')
        name = name[0] +"."+self.model_part_name_tube
        self.model_part_name_tube = name

        self.left_lower_point  = KratosMultiphysics.Array3()
        self.right_lower_point = KratosMultiphysics.Array3()

        for ii in range(0,3):
            self.left_lower_point[ii]  = settings["tube_left_lower_point"][ii].GetDouble()
            self.right_lower_point[ii] = settings["tube_right_lower_point"][ii].GetDouble()

        self.model = Model
        
        problem_path = os.getcwd()
        self.file_path = os.path.join(
            problem_path, "tube_data.csv")


    def ExecuteInitializeSolutionStep(self):

        self.model_part = self.model[self.model_part_name]
        main_model_part = self.model_part.GetRootModelPart()
        

    def ExecuteBeforeOutputStep(self):

        self.model_part = self.model[self.model_part_name]
        self.model_part_tube = self.model[self.model_part_name_tube]
        
        delta_time = self.model_part.ProcessInfo[KratosMultiphysics.DELTA_TIME]


    def ExecuteAfterOutputStep(self):
        
        self.model_part = self.model[self.model_part_name]
        self.model_part_tube = self.model[self.model_part_name_tube]
        
        delta_time = self.model_part.ProcessInfo[KratosMultiphysics.DELTA_TIME]

    #
    def ExecuteFinalizeSolutionStep(self):
 
        self.model_part = self.model[self.model_part_name]
        self.model_part_tube = self.model[self.model_part_name_tube]
        self.model_part_soil = self.model[self.model_part_name_soil]
        
        # We save the information and write it at the same time
        line = ' '

        #1. Time 
        time = self._GetStepTime()
        line = self._AppendDouble(line, time)

        #2. Coordinates and velocity of the point in the top left.
        [pos, vel, acc] = self._GetTubeTopLeftPositionVelocity()
        line = self._AppendVector(line, pos)
        line = self._AppendVector(line, vel)
        line = self._AppendVector(line, acc)

        #3. Coordinates and velocity of the point in the top left.
        [pos, vel, acc] = self._GetSoilTopLeftPositionVelocity()
        line = self._AppendVector(line, pos)
        line = self._AppendVector(line, vel)
        line = self._AppendVector(line, acc)

        [Ftip, Finner, Fouter] = self._GetAllForces()
        line = self._AppendVector(line, Ftip)
        line = self._AppendVector(line, Finner)
        line = self._AppendVector(line, Fouter)

        line = line + " \n"

        thisFile = open(self.file_path, "a")
        thisFile.write(line)
        thisFile.close()



    def _GetAllForces(self):

        Ftip = KratosMultiphysics.Array3()
        Finner = KratosMultiphysics.Array3()
        Fouter = KratosMultiphysics.Array3()
        Ftip = Ftip*0.0
        Finner = Finner * 0.0
        Fouter = Fouter * 0.0
        for node in self.model_part_tube.Nodes:

            normal = KratosMultiphysics.Array3()
            if (node.SolutionStepsDataHas( KratosMultiphysics.NORMAL) ):
                normal = node.GetSolutionStepValue( KratosMultiphysics.NORMAL)
            if ( normal[0]*normal[0] + normal[1]*normal[1] < 0.5):
                continue
            x0 = node.X0
            y0 = node.Y0
            
            if (node.SolutionStepsDataHas( KratosMultiphysics.CONTACT_FORCE) ):
                CF = node.GetSolutionStepValue( KratosMultiphysics.CONTACT_FORCE)
            else:
                continue

            # Check if we are in the tip
            if ( y0 < self.left_lower_point[1] ):
                Ftip = Ftip + CF
            # are you in the left
            elif ( x0 < 0.5*(self.left_lower_point[0] + self.right_lower_point[0]) ):
                Finner = Finner + CF
            else:
                Fouter = Fouter + CF
        
        return [Ftip, Finner, Fouter]



    def _GetTubeTopLeftPositionVelocity(self):

        pos = KratosMultiphysics.Vector(2)

        dist = 100000.0
        for node in self.model_part_tube.Nodes:
            d = ( 0.0-node.X)**2 + (10.0-node.Y)**2
            if ( d < dist):
                dist = d
                pos[0] = node.X
                pos[1] = node.Y
                if ( node.SolutionStepsDataHas(KratosMultiphysics.VELOCITY) ):
                    vel = node.GetSolutionStepValue( KratosMultiphysics.VELOCITY)
                else:
                    vel = pos
                
                if ( node.SolutionStepsDataHas(KratosMultiphysics.ACCELERATION) ):
                    acc = node.GetSolutionStepValue( KratosMultiphysics.ACCELERATION)
                else:
                    acc = pos
        return [pos, vel, acc]
                
    def _GetSoilTopLeftPositionVelocity(self):

        pos = KratosMultiphysics.Vector(2)

        dist = 100000.0
        for node in self.model_part_soil.Nodes:
            d = ( 0.0-node.X)**2 + (10.0-node.Y)**2
            if ( d < dist):
                dist = d
                pos[0] = node.X
                pos[1] = node.Y
                if ( node.SolutionStepsDataHas(KratosMultiphysics.VELOCITY) ):
                    vel = node.GetSolutionStepValue( KratosMultiphysics.VELOCITY)
                else:
                    vel = pos
                
                if ( node.SolutionStepsDataHas(KratosMultiphysics.ACCELERATION) ):
                    acc = node.GetSolutionStepValue( KratosMultiphysics.ACCELERATION)
                else:
                    acc = pos
        return [pos, vel, acc]
                




    def _AppendDouble(self, line, dou):
        line = line + str(dou) + " , "
        return line

    def _AppendVector(self, line, vector):
        line = line + str(vector[0]) + " , " + str(vector[1]) + " , "
        return line
    #
    def _GetStepTime(self):

        return self.model_part.ProcessInfo[KratosMultiphysics.TIME]

    #
    def _GetStepDeltaTime(self):

        return self.model_part.ProcessInfo[KratosMultiphysics.DELTA_TIME]

