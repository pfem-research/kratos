""" Project: PfemApplication
    Developer: LMonforte
    Maintainer: LM
"""

# Built-in/Generic Imports
import os.path

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolidMechanicsApplication as KratosSolid
import KratosMultiphysics.PfemApplication as KratosPfem
import KratosMultiphysics.ContactMechanicsApplication as KratosContact


def Factory(custom_settings, Model):
    if(type(custom_settings) != KratosMultiphysics.Parameters):
        raise Exception(
            "expected input shall be a Parameters object, encapsulating a json string")
    return PrintOneGaussPointProcess(Model, custom_settings["Parameters"])


class PrintOneGaussPointProcess(KratosMultiphysics.Process):

    #
    def __init__(self, model_part, custom_settings):

        KratosMultiphysics.Process.__init__(self)

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
             "model_part_name" : "Main_Domain",
            "points": [ [0.5, 0.0, 0.0],
                 [0.5, 0.5, 0.0] ],
            "velocity": [0.0, 10.15, 0.0]
        }
        """)

        self.settings = custom_settings
        self.settings.ValidateAndAssignDefaults(default_settings)
        self.model_part = model_part

    #
    def ExecuteInitialize(self):

        # set model part
        self.model_part = self.model_part[self.settings["model_part_name"].GetString(
        )]

        # initialize figure path
        self.problem_path = os.getcwd()

        self.points = []
        for jj in range(0, self.settings["points"].size()):
            self.points.append(KratosMultiphysics.Array3())
            for ii in range(0, 3):
                self.points[jj][ii] = self.settings["points"][jj][ii].GetDouble()

        self.velocity = KratosMultiphysics.Array3()
        for ii in range(0, 3):
            self.velocity[ii] = self.settings["velocity"][ii].GetDouble()

    def ExecuteAfterOutputStep(self):

        #if ( self._GetStepTime() < 1.1*self._GetDeltaTime() ):
        #    return
        length = len(self.points)
        for i in range(0, length):
            self.point = self.points[i]
            self.OutputAPoint(i)

    def OutputAPoint(self, point_number):

        time = self._GetStepTime()
        SomeProcessInfo = KratosMultiphysics.ProcessInfo()

        ThisElement = self._search_for_the_element(time)

        pc = ThisElement.CalculateOnIntegrationPoints(
            KratosMultiphysics.PRE_CONSOLIDATION_STRESS, SomeProcessInfo)
        pcSat = ThisElement.CalculateOnIntegrationPoints(
            KratosMultiphysics.ConstitutiveModelsApplication.PRE_CONSOLIDATION_STRESS_SAT, SomeProcessInfo)
        Sr = ThisElement.CalculateOnIntegrationPoints(
            KratosMultiphysics.SolidMechanicsApplication.DEGREE_OF_SATURATION, SomeProcessInfo)


        epsi_vector = ThisElement.CalculateOnIntegrationPoints(
            KratosMultiphysics.GREEN_LAGRANGE_STRAIN_TENSOR, SomeProcessInfo)
        cauchy_vector = ThisElement.CalculateOnIntegrationPoints(
            KratosMultiphysics.CAUCHY_STRESS_TENSOR, SomeProcessInfo)
        cauchy_vector_total = ThisElement.CalculateOnIntegrationPoints(
            KratosMultiphysics.SolidMechanicsApplication.TOTAL_CAUCHY_STRESS, SomeProcessInfo)
        #cauchy_vector  = ThisElement.CalculateOnIntegrationPoints(
        #    KratosMultiphysics.GREEN_LAGRANGE_STRAIN_TENSOR, SomeProcessInfo)

        line = str(time) + " , "

        line = self._AddToLine(line, cauchy_vector[0])
        line = self._AddToLine(line, cauchy_vector_total[0])
        line = self._AddToLine(line, epsi_vector[0])
        line = line + str(pc[0]) + " , "
        line = line + str(pcSat[0]) + " , "
        line = line + str(Sr[0]) + " , "
        line = line + str(self.wp) + " , "
        line = line + str(self.wp) + " , "
        line = line + str(self.temp) + " , "
        line = line + " \n"

        fileName = "GaussPoint-" + str(point_number) + ".csv"
        self.csv_path = os.path.join(self.problem_path, fileName)
        csv_file = open(self.csv_path, "a")
        csv_file.write(line)
        csv_file.close()

    def _search_for_the_element(self, time):

        coord = self.point + time * self.velocity

        bestDistance = 10000.0

        for elem in self.model_part.GetElements():
            nodes = elem.GetNodes()
            numberOfPoints = len(nodes)
            if (len(nodes) > 0):
                ThisPosition = nodes[0]
                for ii in range(1, numberOfPoints):
                    ThisPosition = ThisPosition + nodes[ii]
                ThisPosition = ThisPosition*(1.0/float(numberOfPoints) )
                distance = (ThisPosition[0] - coord[0]
                            )**2 + (ThisPosition[1] - coord[1])**2
                if (distance < bestDistance):
                    bestDistance = distance
                    bestElement = elem
        wp = 0.0
        temp = 0.0
        for node in bestElement.GetNodes():
            if ( node.SolutionStepsDataHas( KratosMultiphysics.WATER_PRESSURE) ):
                wp = wp +  node.GetSolutionStepValue(KratosMultiphysics.WATER_PRESSURE)
            if ( node.SolutionStepsDataHas( KratosMultiphysics.TEMPERATURE) ):
                temp = temp +  node.GetSolutionStepValue(KratosMultiphysics.TEMPERATURE)
        self.wp = wp/float(numberOfPoints)
        self.temp = temp/float(numberOfPoints)
        return bestElement

    def _AddToLine(self, line, vector):

        #for i in vector:
        #    line = line + str(i) + " , "
        line = line + str(vector[0,0]) + " , "
        line = line + str(vector[1,1]) + " , "
        if ( vector.Size1() == 3):
            line = line + str(vector[2,2]) + " , "
        else:
            line = line + str(0.0) + " , "

        line = line + str(vector[0,1]) + " , "

        return line

    def _GetDeltaTime(self):

        return self.model_part.ProcessInfo[KratosMultiphysics.DELTA_TIME]

    def _GetStepTime(self):

        return self.model_part.ProcessInfo[KratosMultiphysics.TIME]
