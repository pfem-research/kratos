
import sys

from classes.elementCreator import ElementCreator
from classes.conditionCreator import ConditionCreator
from classes.classMemberCreator import ClassMemberCreator
from classes.variableCreator import VariableCreator
from utils.io import ToUpperFromCamel, ToLowerFromCamel

from applicationGenerator import ApplicationGenerator

# Set the application name and generate Camel, Caps and Low
appNameCamel = "MyFirst"

# Author Name
NSurname = "PfemTeam"

# Fetch the applications directory
debugApp = ApplicationGenerator(appNameCamel,NSurname)

AppNameUpper = ToUpperFromCamel(appNameCamel)+'_APPLICATION'

# Add KratosVariables
debugApp.AddVariables([
    VariableCreator(name='SECTION_AREA', app_name=AppNameUpper, vtype='double'),
])

# Add test element
debugApp.AddElements([
    ElementCreator('ElasticUnidimensionalElement',appNameCamel,NSurname)
    .AddDofs(['DISPLACEMENT_X'])
    .AddFlags([])
    .AddClassMemberVariables([])
])

debugApp.AddConditions([
    ConditionCreator('PointLoadUnidimensionalCondition',appNameCamel,NSurname)
    .AddDofs(['DISPLACEMENT_X'])
    .AddFlags([])
])


debugApp.Generate()

print("Your application has been generated in: applications/{}Application".format(appNameCamel))
