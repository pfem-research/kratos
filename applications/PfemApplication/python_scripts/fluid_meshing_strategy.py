""" Project: PfemApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Built-in/Generic Imports

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.PfemApplication as KratosPfem
import KratosMultiphysics.MeshersApplication as KratosMeshers
import KratosMultiphysics.MeshersApplication.meshing_strategy as meshing_strategy

def CreateMeshingStrategy(model_part, custom_settings):
    return FluidMeshingStrategy(model_part, custom_settings)

class FluidMeshingStrategy(meshing_strategy.MeshingStrategy):
    #
    def _get_meshers_list(self):
        meshers_list = []
        if(self.settings["remesh"].GetBool() and self.settings["refine"].GetBool()):
            meshers_list.append(
                "KratosMultiphysics.PfemApplication.fluid_refining_mesher")
        elif(self.settings["remesh"].GetBool()):
            meshers_list.append(
                "KratosMultiphysics.MeshersApplication.reconnect_mesher")
        elif(self.settings["transfer"].GetBool()):
            meshers_list.append(
                "KratosMultiphysics.MeshersApplication.transfer_mesher")

        return meshers_list
