""" Project: PfemApplication
    Developer: LMonforte
    Maintainer: LM
"""

# Built-in/Generic Imports
import os.path

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolidMechanicsApplication as KratosSolid
import KratosMultiphysics.PfemApplication as KratosPfem
import KratosMultiphysics.ContactMechanicsApplication as KratosContact


def Factory(custom_settings, Model):
    if(type(custom_settings) != KratosMultiphysics.Parameters):
        raise Exception(
            "expected input shall be a Parameters object, encapsulating a json string")
    return FootingProblemProcess2(Model, custom_settings["Parameters"])


class FootingProblemProcess2(KratosMultiphysics.Process):

    #
    def __init__(self, model_part, custom_settings):

        KratosMultiphysics.Process.__init__(self)

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
           "model_part_name": "Main_Domain",
           "initial_height": 0.0,
           "width": 1.0
        }
        """)

        settings = custom_settings
        settings.ValidateAndAssignDefaults(default_settings)

        self.model = model_part
        self.model_part_name =  settings["model_part_name"].GetString()

        self.Y0 = settings["initial_height"].GetDouble()
        self.B = settings["width"].GetDouble()

        # initialize figure path
        problem_path = os.getcwd()
        self.csv_path = os.path.join(problem_path, "footing.csv")
        csv_file = open(self.csv_path, "a")
        csv_file.close()

    def ExecuteFinalizeSolutionStep(self):

        self.model_part = self.model[self.model_part_name]

        time = self._GetStepTime()


        Force = 0.0*KratosMultiphysics.Array3()
        for node in self.model_part.GetNodes(0):
                res = node.GetSolutionStepValue(
                       KratosMultiphysics.CONTACT_FORCE)
                Force = Force + res

        line = str(time) + " , "

        line = self._AddToLine(line, Force)
        line = line + " \n"

        csv_file = open(self.csv_path, "a")
        csv_file.write(line)
        csv_file.close()

        self.TryToUnFixNodes()

        for node in self.model_part.GetNodes(0):

            if (node.SolutionStepsDataHas(KratosContact.CONTACT_STRESS)):
                CF = node.GetSolutionStepValue(KratosMultiphysics.CONTACT_FORCE)
                if ( abs(CF[0]) + abs(CF[1]) < 1E-8):
                    CF = 0.0*CF
                    node.SetSolutionStepValue(KratosContact.CONTACT_STRESS, CF)


    # function to try to unfix the water pressure of nodes in that are in contact
    def TryToUnFixNodes(self):


        for node in self.model_part.GetNodes(0):
            Force = node.GetSolutionStepValue(KratosMultiphysics.CONTACT_FORCE)
            Normal = node.GetSolutionStepValue(KratosMultiphysics.NORMAL)
            IsBoundary = False
            if (abs(Normal[0]) + abs(Normal[1]) > 1.0e-7):
                IsBoundary = True

            if (node.HasDofFor(KratosMultiphysics.WATER_PRESSURE)):
                if ((abs(Force[0]) + abs(Force[1]) > 1.0e-7)):
                    if (node.IsFixed(KratosMultiphysics.WATER_PRESSURE)):
                        node.Free(KratosMultiphysics.WATER_PRESSURE)
                        print(' UNFIXING ')
                elif ( IsBoundary):
                    if (node.IsFixed(KratosMultiphysics.WATER_PRESSURE) == False):
                        node.Fix(KratosMultiphysics.WATER_PRESSURE)
                        node.SetSolutionStepValue( KratosMultiphysics.WATER_PRESSURE, 0.0)
                        print(' UNFIXING :: FIXING ')



    def _AddToLine(self, line, vector):

        for i in vector:
            line = line + str(i) + " , "

        return line

    def _GetStepTime(self):

        return self.model_part.ProcessInfo[KratosMultiphysics.TIME]
