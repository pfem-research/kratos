""" Project: PfemApplication
    Developer: LMonforte
    Maintainer: LM
"""

# Built-in/Generic Imports
import os.path

# Kratos Imports
import KratosMultiphysics
import KratosMultiphysics.SolidMechanicsApplication as KratosSolid
import KratosMultiphysics.PfemApplication as KratosPfem
import KratosMultiphysics.ContactMechanicsApplication as KratosContact


def Factory(custom_settings, Model):
    if(type(custom_settings) != KratosMultiphysics.Parameters):
        raise Exception(
            "expected input shall be a Parameters object, encapsulating a json string")
    return FootingProblemProcess(Model, custom_settings["Parameters"])


class FootingProblemProcess(KratosMultiphysics.Process):

    #
    def __init__(self, model_part, custom_settings):

        KratosMultiphysics.Process.__init__(self)

        # settings string in json format
        default_settings = KratosMultiphysics.Parameters("""
        {
           "model_part_name": "Main_Domain",
           "initial_height": 0.0,
           "width": 1.0
        }
        """)

        settings = custom_settings
        settings.ValidateAndAssignDefaults(default_settings)

        self.model_part_name = settings["model_part_name"].GetString()
        self.model_part = model_part[self.model_part_name]

        self.Y0 = settings["initial_height"].GetDouble()
        self.B = settings["width"].GetDouble()

        # initialize figure path
        problem_path = os.getcwd()
        self.csv_path = os.path.join(problem_path, "footing.csv")
        csv_file = open(self.csv_path, "a")
        csv_file.close()

    def ExecuteFinalizeSolutionStep(self):

        time = self._GetStepTime()

        SomeProcessInfo = KratosMultiphysics.ProcessInfo()

        Force = 0.0*KratosMultiphysics.Array3()
        for node in self.model_part.GetNodes(0):
            if (abs(node.Y0-self.Y0) < 1E-8):
                if (abs(node.X0) < 1.1*self.B):
                    res = node.GetSolutionStepValue(
                        KratosSolid.DISPLACEMENT_REACTION)
                    Force = Force + res

        line = str(time) + " , "

        line = self._AddToLine(line, Force)
        line = line + " \n"

        csv_file = open(self.csv_path, "a")
        csv_file.write(line)
        csv_file.close()

    def _AddToLine(self, line, vector):

        for i in vector:
            line = line + str(i) + " , "

        return line

    def _GetStepTime(self):

        return self.model_part.ProcessInfo[KratosMultiphysics.TIME]
