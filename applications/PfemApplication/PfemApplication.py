""" Project: PfemApplication
    Developer: JMCarbonell
    Maintainer: JMC
"""

# Kratos Imports
from KratosMultiphysics import _ImportApplication
import KratosMultiphysics.MeshersApplication
import KratosMultiphysics.SolidMechanicsApplication
import KratosMultiphysics.ContactMechanicsApplication

from KratosPfemApplication import*
application = KratosPfemApplication()
application_name = "KratosPfemApplication"

_ImportApplication(application, application_name)
