*# Element and condition indices. We renumber them so each type is numbered from one.
*set var ielem=0
*set var icond=0

*# ModelPart block

Begin ModelPartData
//  VARIABLE_NAME value
End ModelPartData

*# Property blocks

Begin Properties 0
End Properties

*loop materials
*format "%i"
Begin Properties *MatNum
End Properties
*end materials

*# Mesh0 block

*# Nodes block
Begin Nodes
*#// id	  X	Y	Z
*loop nodes
*format "%i%10.5e%10.5e%10.5e"
*NodesNum	*NodesCoord(1)	*NodesCoord(2)	*NodesCoord(3)
*end nodes
End Nodes


*# Element blocks

*set cond surface_UpdatedLagrangianSolidElement2D3N *elems
*if(CondNumEntities > 0)
Begin Elements UpdatedLagrangianSolidElement2D3N
*#// id prop_id	 n1	n2	n3	...
*loop elems *OnlyInCond
*set var ielem=operation(ielem+1)
*set var i=0
*set var j=ElemsNnode
*format "%i%i%i%i%i%i%i%i"
*ElemsNum *ElemsMat*\
*for(i=1;i<=j;i=i+1)*\
 *ElemsConec(*i)*\
*end

*end elems
End Elements

*endif
*set cond surface_UpdatedLagrangianUwPElement2D3N *elems
*if(CondNumEntities > 0)
Begin Elements UpdatedLagrangianUwPElement2D3N
*#// id prop_id	 n1	n2	n3	...
*loop elems *OnlyInCond
*set var ielem=operation(ielem+1)
*set var i=0
*set var j=ElemsNnode
*format "%i%i%i%i%i%i%i%i"
*ElemsNum *ElemsMat*\
*for(i=1;i<=j;i=i+1)*\
 *ElemsConec(*i)*\
*end

*end elems
End Elements

*endif
*set cond surface_UpdatedLagrangianUJElement2D3N *elems
*if(CondNumEntities > 0)
Begin Elements UpdatedLagrangianUJElement2D3N
*#// id prop_id	 n1	n2	n3	...
*loop elems *OnlyInCond
*set var ielem=operation(ielem+1)
*set var i=0
*set var j=ElemsNnode
*format "%i%i%i%i%i%i%i%i"
*ElemsNum *ElemsMat*\
*for(i=1;i<=j;i=i+1)*\
 *ElemsConec(*i)*\
*end

*end elems
End Elements

*endif
*set cond surface_UpdatedLagrangianUJacobianElement2D3N *elems
*if(CondNumEntities > 0)
Begin Elements UpdatedLagrangianUJacobianElement2D3N
*#// id prop_id	 n1	n2	n3	...
*loop elems *OnlyInCond
*set var ielem=operation(ielem+1)
*set var i=0
*set var j=ElemsNnode
*format "%i%i%i%i%i%i%i%i"
*ElemsNum *ElemsMat*\
*for(i=1;i<=j;i=i+1)*\
 *ElemsConec(*i)*\
*end

*end elems
End Elements

*endif
*set cond surface_UpdatedLagrangianUWElement2D3N *elems
*if(CondNumEntities > 0)
Begin Elements UpdatedLagrangianUWElement2D3N
*#// id prop_id	 n1	n2	n3	...
*loop elems *OnlyInCond
*set var ielem=operation(ielem+1)
*set var i=0
*set var j=ElemsNnode
*format "%i%i%i%i%i%i%i%i"
*ElemsNum *ElemsMat*\
*for(i=1;i<=j;i=i+1)*\
 *ElemsConec(*i)*\
*end

*end elems
End Elements

*endif
*set cond surface_UpdatedLagrangianUWwPElement2D3N *elems
*if(CondNumEntities > 0)
Begin Elements UpdatedLagrangianUWwPElement2D3N
*#// id prop_id	 n1	n2	n3	...
*loop elems *OnlyInCond
*set var ielem=operation(ielem+1)
*set var i=0
*set var j=ElemsNnode
*format "%i%i%i%i%i%i%i%i"
*ElemsNum *ElemsMat*\
*for(i=1;i<=j;i=i+1)*\
 *ElemsConec(*i)*\
*end

*end elems
End Elements

*endif
*set cond surface_UpdatedLagrangianUJWwPElement2D3N *elems
*if(CondNumEntities > 0)
Begin Elements UpdatedLagrangianUJWwPElement2D3N
*#// id prop_id	 n1	n2	n3	...
*loop elems *OnlyInCond
*set var ielem=operation(ielem+1)
*set var i=0
*set var j=ElemsNnode
*format "%i%i%i%i%i%i%i%i"
*ElemsNum *ElemsMat*\
*for(i=1;i<=j;i=i+1)*\
 *ElemsConec(*i)*\
*end

*end elems
End Elements

*endif
*set cond surface_UpdatedLagrangianUwPStabElement2D3N *elems
*if(CondNumEntities > 0)
Begin Elements UpdatedLagrangianUwPStabElement2D3N
*#// id prop_id	 n1	n2	n3	...
*loop elems *OnlyInCond
*set var ielem=operation(ielem+1)
*set var i=0
*set var j=ElemsNnode
*format "%i%i%i%i%i%i%i%i"
*ElemsNum *ElemsMat*\
*for(i=1;i<=j;i=i+1)*\
 *ElemsConec(*i)*\
*end

*end elems
End Elements

*endif
*set cond surface_SmallDisplacementUwPElement2D3N *elems
*if(CondNumEntities > 0)
Begin Elements SmallDisplacementUwPElement2D3N
*#// id prop_id	 n1	n2	n3	...
*loop elems *OnlyInCond
*set var ielem=operation(ielem+1)
*set var i=0
*set var j=ElemsNnode
*format "%i%i%i%i%i%i%i%i"
*ElemsNum *ElemsMat*\
*for(i=1;i<=j;i=i+1)*\
 *ElemsConec(*i)*\
*end

*end elems
End Elements

*endif
*set cond surface_UpdatedLagrangianUJwPElement2D3N *elems
*if(CondNumEntities > 0)
Begin Elements UpdatedLagrangianUJwPElement2D3N
*#// id prop_id	 n1	n2	n3	...
*loop elems *OnlyInCond
*set var ielem=operation(ielem+1)
*set var i=0
*set var j=ElemsNnode
*format "%i%i%i%i%i%i%i%i"
*ElemsNum *ElemsMat*\
*for(i=1;i<=j;i=i+1)*\
 *ElemsConec(*i)*\
*end

*end elems
End Elements

*endif
*set cond surface_AxisymUpdatedLagrangianUwPStabElement2D3N *elems
*if(CondNumEntities > 0)
Begin Elements AxisymUpdatedLagrangianUwPStabElement2D3N
*#// id prop_id	 n1	n2	n3	...
*loop elems *OnlyInCond
*set var ielem=operation(ielem+1)
*set var i=0
*set var j=ElemsNnode
*format "%i%i%i%i%i%i%i%i"
*ElemsNum *ElemsMat*\
*for(i=1;i<=j;i=i+1)*\
 *ElemsConec(*i)*\
*end

*end elems
End Elements

*endif
*set cond surface_AxisymUpdatedLagrangianUJwPElement2D3N *elems
*if(CondNumEntities > 0)
Begin Elements AxisymUpdatedLagrangianUJwPElement2D3N
*#// id prop_id	 n1	n2	n3	...
*loop elems *OnlyInCond
*set var ielem=operation(ielem+1)
*set var i=0
*set var j=ElemsNnode
*format "%i%i%i%i%i%i%i%i"
*ElemsNum *ElemsMat*\
*for(i=1;i<=j;i=i+1)*\
 *ElemsConec(*i)*\
*end

*end elems
End Elements

*endif
*set cond surface_AxisymUpdatedLagrangianUJWwPElement2D3N *elems
*if(CondNumEntities > 0)
Begin Elements AxisymUpdatedLagrangianUJWwPElement2D3N
*#// id prop_id	 n1	n2	n3	...
*loop elems *OnlyInCond
*set var ielem=operation(ielem+1)
*set var i=0
*set var j=ElemsNnode
*format "%i%i%i%i%i%i%i%i"
*ElemsNum *ElemsMat*\
*for(i=1;i<=j;i=i+1)*\
 *ElemsConec(*i)*\
*end

*end elems
End Elements

*endif
*set cond surface_AxisymUpdatedLagrangianUJacobianElement2D3N *elems
*if(CondNumEntities > 0)
Begin Elements AxisymUpdatedLagrangianUJacobianElement2D3N
*#// id prop_id	 n1	n2	n3	...
*loop elems *OnlyInCond
*set var ielem=operation(ielem+1)
*set var i=0
*set var j=ElemsNnode
*format "%i%i%i%i%i%i%i%i"
*ElemsNum *ElemsMat*\
*for(i=1;i<=j;i=i+1)*\
 *ElemsConec(*i)*\
*end

*end elems
End Elements

*endif
*set cond surface_UpdatedLagrangianUPElement2D3N *elems
*if(CondNumEntities > 0)
Begin Elements UpdatedLagrangianUPElement2D3N
*#// id prop_id	 n1	n2	n3	...
*loop elems *OnlyInCond
*set var ielem=operation(ielem+1)
*set var i=0
*set var j=ElemsNnode
*format "%i%i%i%i%i%i%i%i"
*ElemsNum *ElemsMat*\
*for(i=1;i<=j;i=i+1)*\
 *ElemsConec(*i)*\
*end

*end elems
End Elements

*endif
*set cond surface_AxisymUpdatedLagrangianSolidElement2D3N *elems
*if(CondNumEntities > 0)
Begin Elements AxisymUpdatedLagrangianSolidElement2D3N
*#// id prop_id	 n1	n2	n3	...
*loop elems *OnlyInCond
*set var ielem=operation(ielem+1)
*set var i=0
*set var j=ElemsNnode
*format "%i%i%i%i%i%i%i%i"
*ElemsNum *ElemsMat*\
*for(i=1;i<=j;i=i+1)*\
 *ElemsConec(*i)*\
*end

*end elems
End Elements

*endif
*set cond surface_AxisymUpdatedLagrangianUPElement2D3N *elems
*if(CondNumEntities > 0)
Begin Elements AxisymUpdatedLagrangianUPElement2D3N
*#// id prop_id	 n1	n2	n3	...
*loop elems *OnlyInCond
*set var ielem=operation(ielem+1)
*set var i=0
*set var j=ElemsNnode
*format "%i%i%i%i%i%i%i%i"
*ElemsNum *ElemsMat*\
*for(i=1;i<=j;i=i+1)*\
 *ElemsConec(*i)*\
*end

*end elems
End Elements

*endif
*Set cond volume_UpdatedLagrangianSolidElement3D4N *elems
*if(CondNumEntities > 0)
Begin Elements UpdatedLagrangianSolidElement3D4N
*#// id prop_id	 n1	n2	n3	...
*loop elems *OnlyInCond
*set var ielem=operation(ielem+1)
*set var i=0
*set var j=ElemsNnode
*format "%i%i%i%i%i%i%i%i"
*ElemsNum *ElemsMat*\
*for(i=1;i<=j;i=i+1)*\
 *ElemsConec(*i)*\
*end

*end elems
End Elements

*endif
*Set cond volume_UpdatedLagrangianUPElement3D4N *elems
*if(CondNumEntities > 0)
Begin Elements UpdatedLagrangianUPElement3D4N
*#// id prop_id	 n1	n2	n3	...
*loop elems *OnlyInCond
*set var ielem=operation(ielem+1)
*set var i=0
*set var j=ElemsNnode
*format "%i%i%i%i%i%i%i%i"
*ElemsNum *ElemsMat*\
*for(i=1;i<=j;i=i+1)*\
 *ElemsConec(*i)*\
*end

*end elems
End Elements

*endif

*set cond volume_UpdatedLagrangianUwPStabElement3D4N *elems
*if(CondNumEntities > 0)
Begin Elements UpdatedLagrangianUwPStabElement3D4N
*#// id prop_id	 n1	n2	n3	...
*loop elems *OnlyInCond
*set var ielem=operation(ielem+1)
*set var i=0
*set var j=ElemsNnode
*format "%i%i%i%i%i%i%i%i"
*ElemsNum *ElemsMat*\
*for(i=1;i<=j;i=i+1)*\
 *ElemsConec(*i)*\
*end

*end elems
End Elements

*endif
*set cond volume_UpdatedLagrangianUJElement3D4N *elems
*if(CondNumEntities > 0)
Begin Elements UpdatedLagrangianUJElement3D4N
*#// id prop_id	 n1	n2	n3	...
*loop elems *OnlyInCond
*set var ielem=operation(ielem+1)
*set var i=0
*set var j=ElemsNnode
*format "%i%i%i%i%i%i%i%i"
*ElemsNum *ElemsMat*\
*for(i=1;i<=j;i=i+1)*\
 *ElemsConec(*i)*\
*end

*end elems
End Elements

*endif
*set cond volume_UpdatedLagrangianUJwPElement3D4N *elems
*if(CondNumEntities > 0)
Begin Elements UpdatedLagrangianUJwPElement3D4N
*#// id prop_id	 n1	n2	n3	...
*loop elems *OnlyInCond
*set var ielem=operation(ielem+1)
*set var i=0
*set var j=ElemsNnode
*format "%i%i%i%i%i%i%i%i"
*ElemsNum *ElemsMat*\
*for(i=1;i<=j;i=i+1)*\
 *ElemsConec(*i)*\
*end

*end elems
End Elements

*endif
*set cond group_RigidBodies *groups
*if(CondNumEntities > 0)
*loop groups *OnlyInCond
*if(strcmp(cond(Parametric_Wall),"False")==0)
*if(strcmp(cond(Body_Surface),"False")==0)
*if(GenData(DIMENSION,INT) == 3)
Begin Elements Element3D4N
*else
Begin Elements Element2D3N
*endif
*#// id prop_id	 n1	n2	n3	...
*set group *GroupName *elems
*loop elems *onlyingroup
*set var ielem=operation(ielem+1)
*set var i=0
*set var j=ElemsNnode
*format "%i%i%i%i%i"
*ielem *ElemsMat *\
*for(i=1;i<=j;i=i+1)*\
 *ElemsConec(*i)*\
*end

*end elems
End Elements

*endif
*endif
*end groups
*endif

*# Condition Blocks
*# start number for each condition type:
*set var RigidWallsstart = icond

*set cond group_RigidBodies *groups
*if(CondNumEntities > 0)
*loop groups *OnlyInCond
*if(strcmp(cond(Body_Surface),"True")==0 || strcmp(cond(Parametric_Wall),"True")==0)
*if(GenData(DIMENSION,INT) == 3)
Begin Elements Element3D3N
*else
Begin Elements Element2D2N
*endif
*#// id prop_id	 n1	n2	n3	...
*set group *GroupName *elems
*loop elems *onlyingroup
*set var ielem=operation(ielem+1)
*set var i=0
*set var j=ElemsNnode
*format "%i%i%i%i%i"
*ielem *ElemsMat *\
*for(i=1;i<=j;i=i+1)*\
 *ElemsConec(*i)*\
*end

*end elems
End Elements

*endif
*end groups
*endif

*set cond group_POINT_LOAD *groups
*if(CondNumEntities > 0)
*loop groups *OnlyInCond
Begin Conditions *cond(ConditionType)
*#// id prop_id	 n1	n2	n3	...
*set group *GroupName *nodes
*loop nodes *onlyingroup
*format "%i%i%i"
 *Tcl( setCondId *NodesNum 0 ) 0 *NodesNum
*end nodes
End Conditions

*end groups
*endif

*set cond group_LINE_LOAD *groups
*if(CondNumEntities > 0)
*loop groups *OnlyInCond
Begin Conditions *cond(ConditionType)
*#// id prop_id	 n1	n2	n3	...
*set group *GroupName *faces
*loop faces *onlyingroup
 *Tcl(Print2DFaceElement *FaceElemsNum *FaceIndex)
*end faces
End Conditions

*end groups
*endif

*set cond group_SURFACE_LOAD *groups
*if(CondNumEntities > 0)
*loop groups *OnlyInCond
Begin Conditions *cond(ConditionType)
*#// id prop_id	 n1	n2	n3	...
*set group *GroupName *faces
*loop faces *onlyingroup
 *Tcl(Print3DFaceElement *FaceElemsNum *FaceIndex)
*end faces
End Conditions

*end groups
*endif

*set cond group_LINE_PRESSURE *groups
*if(CondNumEntities > 0)
*loop groups *OnlyInCond
Begin Conditions *cond(ConditionType)
*#// id prop_id	 n1	n2	n3	...
*set group *GroupName *faces
*loop faces *onlyingroup
 *Tcl(Print2DFaceElement *FaceElemsNum *FaceIndex)
*end faces
End Conditions

*end groups
*endif

*set cond group_SURFACE_PRESSURE *groups
*if(CondNumEntities > 0)
*loop groups *OnlyInCond
Begin Conditions *cond(ConditionType)
*#// id prop_id	 n1	n2	n3	...
*set group *GroupName *faces
*loop faces *onlyingroup
 *Tcl(Print3DFaceElement *FaceElemsNum *FaceIndex)
*end faces
End Conditions

*end groups
*endif

*set cond group_POINT_MOMENT *groups
*if(CondNumEntities > 0)
*loop groups *OnlyInCond
Begin Conditions *cond(ConditionType)
*#// id prop_id	 n1	n2	n3	...
*set group *GroupName *nodes
*loop nodes *onlyingroup
*format "%i%i%i"
 *Tcl( setCondId *NodesNum 0 ) 0 *NodesNum
*end nodes
End Conditions

*end groups
*endif


*# SubModelPart Blocks
*set cond group_DeformableBodies *groups
*if(CondNumEntities > 0)
*loop groups *OnlyInCond
Begin SubModelPart *GroupName // *GroupNum

 Begin SubModelPartNodes
*set group *GroupName *nodes
*if(GroupNumEntities > 0)
*loop nodes *onlyingroup
*format "%i"
 *NodesNum
*end nodes
*endif
 End SubModelPartNodes

 Begin SubModelPartElements
*set group *GroupName *elems
*if(GroupNumEntities > 0)
*loop elems *onlyingroup
*format "%i"
 *ElemsNum
*end elems
*endif
 End SubModelPartElements

 Begin SubModelPartConditions
 End SubModelPartConditions

End SubModelPart
*end groups
*endif
*set cond group_LINEAR_MOVEMENT *groups
*if(CondNumEntities > 0)
*loop groups *OnlyInCond
Begin SubModelPart *GroupName // *GroupNum

 Begin SubModelPartNodes
*set group *GroupName *nodes
*if(GroupNumEntities)
*loop nodes *onlyingroup
 *NodesNum
*end nodes
*endif
 End SubModelPartNodes

 Begin SubModelPartElements
 End SubModelPartElements

 Begin SubModelPartConditions
 End SubModelPartConditions

End SubModelPart
*end groups
*endif
*set cond group_ANGULAR_MOVEMENT *groups
*if(CondNumEntities > 0)
*loop groups *OnlyInCond
Begin SubModelPart *GroupName // *GroupNum

 Begin SubModelPartNodes
*set group *GroupName *nodes
*if(GroupNumEntities)
*loop nodes *onlyingroup
 *NodesNum
*end nodes
*endif
 End SubModelPartNodes

 Begin SubModelPartElements
 End SubModelPartElements

 Begin SubModelPartConditions
 End SubModelPartConditions

End SubModelPart
*end groups
*endif
*set cond group_WATER_PRESSURE *groups
*if(CondNumEntities > 0)
*loop groups *OnlyInCond
Begin SubModelPart *GroupName // *GroupNum

 Begin SubModelPartNodes
*set group *GroupName *nodes
*if(GroupNumEntities)
*loop nodes *onlyingroup
 *NodesNum
*end nodes
*endif
 End SubModelPartNodes

 Begin SubModelPartElements
 End SubModelPartElements

 Begin SubModelPartConditions
 End SubModelPartConditions

End SubModelPart
*end groups
*endif
*set cond group_WATER_MOVEMENT *groups
*if(CondNumEntities > 0)
*loop groups *OnlyInCond
Begin SubModelPart *GroupName // *GroupNum

 Begin SubModelPartNodes
*set group *GroupName *nodes
*if(GroupNumEntities)
*loop nodes *onlyingroup
 *NodesNum
*end nodes
*endif
 End SubModelPartNodes

 Begin SubModelPartElements
 End SubModelPartElements

 Begin SubModelPartConditions
 End SubModelPartConditions

End SubModelPart
*end groups
*endif
*set cond group_POINT_LOAD *groups
*if(CondNumEntities)
*loop groups *OnlyInCond
Begin SubModelPart *GroupName // *GroupNum

 Begin SubModelPartNodes
*set group *GroupName *nodes
*if(GroupNumEntities)
*loop nodes *onlyingroup
 *NodesNum
*end nodes
*endif
 End SubModelPartNodes

 Begin SubModelPartElements
 End SubModelPartElements

 Begin SubModelPartConditions
*set group *GroupName *nodes
*if(GroupNumEntities)
*loop nodes *onlyingroup
*format "%i"
 *Tcl( getCondId *NodesNum 0 )
*end nodes
*endif
 End SubModelPartConditions

End SubModelPart
*end groups
*endif
*set cond group_LINE_LOAD *groups
*if(CondNumEntities > 0)
*loop groups *OnlyInCond
Begin SubModelPart *GroupName // *GroupNum

 Begin SubModelPartNodes
*set group *GroupName *nodes
*if(GroupNumEntities)
*loop nodes *onlyingroup
 *NodesNum
*end nodes
*endif
 End SubModelPartNodes

 Begin SubModelPartElements
 End SubModelPartElements

 Begin SubModelPartConditions
*set group *GroupName *faces
*if(GroupNumEntities)
*loop faces *onlyingroup
 *Tcl( getCondId *FaceElemsNum *FaceIndex )
*end faces
*endif
 End SubModelPartConditions

End SubModelPart
*end groups
*endif
*set cond group_LINE_PRESSURE *groups
*if(CondNumEntities > 0)
*loop groups *OnlyInCond
Begin SubModelPart *GroupName // *GroupNum

 Begin SubModelPartNodes
*set group *GroupName *nodes
*if(GroupNumEntities)
*loop nodes *onlyingroup
 *NodesNum
*end nodes
*endif
 End SubModelPartNodes

 Begin SubModelPartElements
 End SubModelPartElements

 Begin SubModelPartConditions
*set group *GroupName *faces
*if(GroupNumEntities)
*loop faces *onlyingroup
 *Tcl( getCondId *FaceElemsNum *FaceIndex )
*end faces
*endif
 End SubModelPartConditions

End SubModelPart
*end groups
*endif
*set cond group_SURFACE_LOAD *groups
*if(CondNumEntities > 0)
*loop groups *OnlyInCond
Begin SubModelPart *GroupName // *GroupNum

 Begin SubModelPartNodes
*set group *GroupName *nodes
*if(GroupNumEntities)
*loop nodes *onlyingroup
 *NodesNum
*end nodes
*endif
 End SubModelPartNodes

 Begin SubModelPartElements
 End SubModelPartElements

 Begin SubModelPartConditions
*set group *GroupName *faces
*if(GroupNumEntities)
*loop faces *onlyingroup
 *Tcl( getCondId *FaceElemsNum *FaceIndex )
*end faces
*endif
 End SubModelPartConditions

End SubModelPart
*end groups
*endif
*set cond group_SURFACE_PRESSURE *groups
*if(CondNumEntities > 0)
*loop groups *OnlyInCond
Begin SubModelPart *GroupName // *GroupNum

 Begin SubModelPartNodes
*set group *GroupName *nodes
*if(GroupNumEntities)
*loop nodes *onlyingroup
 *NodesNum
*end nodes
*endif
 End SubModelPartNodes

 Begin SubModelPartElements
 End SubModelPartElements

 Begin SubModelPartConditions
*set group *GroupName *faces
*if(GroupNumEntities)
*loop faces *onlyingroup
 *Tcl( getCondId *FaceElemsNum *FaceIndex )
*end faces
*endif
 End SubModelPartConditions

End SubModelPart
*end groups
*endif
*set cond group_POINT_MOMENT *groups
*if(CondNumEntities > 0)
*loop groups *OnlyInCond
Begin SubModelPart *GroupName // *GroupNum

 Begin SubModelPartNodes
*set group *GroupName *nodes
*if(GroupNumEntities)
*loop nodes *onlyingroup
 *NodesNum
*end nodes
*endif
 End SubModelPartNodes

 Begin SubModelPartElements
 End SubModelPartElements

 Begin SubModelPartConditions
*set group *GroupName *nodes
*if(GroupNumEntities)
*loop nodes *onlyingroup
*format "%i"
 *Tcl( getCondId *NodesNum 0 )
*end nodes
*endif
 End SubModelPartConditions

End SubModelPart
*end groups
*endif
*set cond group_VOLUME_ACCELERATION *groups
*if(CondNumEntities > 0)
*loop groups *OnlyInCond
Begin SubModelPart *GroupName // *GroupNum

 Begin SubModelPartNodes
*set group *GroupName *nodes
*if(GroupNumEntities)
*loop nodes *onlyingroup
 *NodesNum
*end nodes
*endif
 End SubModelPartNodes

 Begin SubModelPartElements
 End SubModelPartElements

 Begin SubModelPartConditions
 End SubModelPartConditions

End SubModelPart
*end groups
*endif

*set var RigidWallsElemNum = 0
*set var RigidWallsCondNum = 0
*set cond group_RigidBodies *groups
*if(CondNumEntities > 0)
*loop groups *OnlyInCond
Begin SubModelPart *GroupName // *GroupNum

 Begin SubModelPartNodes
*set group *GroupName *nodes
*if(GroupNumEntities > 0)
*loop nodes *onlyingroup
*format "%i"
 *NodesNum
*end nodes
*endif
 End SubModelPartNodes

 Begin SubModelPartElements
*set group *GroupName *elems
*if(GroupNumEntities > 0)
*loop elems *onlyingroup
*format "%i"
 *ElemsNum
*end elems
*endif
 End SubModelPartElements

 Begin SubModelPartConditions
 End SubModelPartConditions
End SubModelPart
*end groups
*endif

*# Note: About elements/conditions: it is important that point elements/conditions are added AFTER regular points/conditions to keep numeration of elemental/conditional data consistent.
*# This is why point elements/conditions get their own blocks.
*#
*Tcl(resetCondId) *\
*# Clear list of condition Ids
