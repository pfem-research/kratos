{
    "material_models_list": [
*set var GroupNumber = 0
*loop materials
*set var GroupNumber=operation(GroupNumber+1)
*end materials
*set var Counter=0
*loop materials
*set var Counter=operation(Counter+1)
      {
        "python_module" : "assign_materials_process",
        "kratos_module" : "ConstitutiveModelsApplication",
        "help"          : "This process creates a material and assigns its properties",
        "process_name"  : "AssignMaterialsProcess",
        "Parameters"    : {
*format "%i"
            "properties_id"    : *MatNum,
            "material_name"    : "Steel",
            "constitutive_law" : {
                 "name":        "*MatProp(DIMENSION_OF_THE_PROBLEM).*MatProp(CONSTITUTIVE_LAW_NAME)"
            },
            "variables"        : {
*if(strcmp(MatProp(Type),"TrescaPlasticity")==0)
*format "%10.5e"
                "YOUNG_MODULUS": *MatProp(YOUNG_MODULUS,real),
*format "%10.5e"
                "POISSON_RATIO": *MatProp(POISSON_RATIO,real),
*format "%10.5e"
                "YIELD_STRESS": *MatProp(YIELD_STRESS,real),
*elseif(strcmp(MatProp(Type),"LinearElasticModel")==0)
*format "%10.5e"
                "YOUNG_MODULUS": *MatProp(YOUNG_MODULUS,real),
*format "%10.5e"
                "POISSON_RATIO": *MatProp(POISSON_RATIO,real),
*elseif(strcmp(MatProp(Type),"MohrCoulombPlasticity")==0)
*format "%10.5e"
                "YOUNG_MODULUS": *MatProp(YOUNG_MODULUS,real),
*format "%10.5e"
                "POISSON_RATIO": *MatProp(POISSON_RATIO,real),
*format "%10.5e"
                "COHESION": *MatProp(COHESION,real),
*format "%10.5e"
                "INTERNAL_FRICTION_ANGLE": *MatProp(INTERNAL_FRICTION_ANGLE,real),
*format "%10.5e"
                "DILATANCY_ANGLE": *MatProp(DILATANCY_ANGLE,real),
*elseif(strcmp(MatProp(Type),"GensNovaPlasticity")==0)
*format "%10.5e"
                "YOUNG_MODULUS": *MatProp(YOUNG_MODULUS,real),
*format "%10.5e"
                "POISSON_RATIO": *MatProp(POISSON_RATIO,real),
*format "%10.5e"
                "ALPHA_SHEAR": *MatProp(ALPHA_SHEAR,real),
*format "%10.5e"
                "REFERENCE_PRESSURE": *MatProp(REFERENCE_PRESSURE,real),
*format "%10.5e"
                "CRITICAL_STATE_LINE": *MatProp(CRITICAL_STATE_LINE,real),
*format "%10.5e"
                "INTERNAL_FRICTION_ANGLE": *MatProp(INTERNAL_FRICTION_ANGLE,real),
*format "%10.5e"
                "KSIM": *MatProp(KSIM,real),
*format "%10.5e"
                "PS": *MatProp(PS,real),
*format "%10.5e"
                "PT": *MatProp(PT,real),
*format "%10.5e"
                "RHOS": *MatProp(RHOS,real),
*format "%10.5e"
                "RHOT": *MatProp(RHOT,real),
*format "%10.5e"
                "CHIS": *MatProp(CHIS,real),
*format "%10.5e"
                "CHIT": *MatProp(CHIT,real),
*elseif(strcmp(MatProp(Type),"LDMilanModelPlasticity")==0)
*format "%10.5e"
                "YOUNG_MODULUS": *MatProp(YOUNG_MODULUS,real),
*format "%10.5e"
                "POISSON_RATIO": *MatProp(POISSON_RATIO,real),
*format "%10.5e"
                "ALPHA_SHEAR": *MatProp(ALPHA_SHEAR,real),
*format "%10.5e"
                "REFERENCE_PRESSURE": *MatProp(REFERENCE_PRESSURE,real),
*format "%10.5e"
                "CRITICAL_STATE_LINE": *MatProp(CRITICAL_STATE_LINE,real),
*format "%10.5e"
                "ALPHA_F": *MatProp(ALPHA_F,real),
*format "%10.5e"
                "MU_F": *MatProp(MU_F,real),
*format "%10.5e"
                "CRITICAL_STATE_LINE_PP": *MatProp(CRITICAL_STATE_LINE_PP,real),
*format "%10.5e"
                "ALPHA_G": *MatProp(ALPHA_G,real),
*format "%10.5e"
                "MU_G": *MatProp(MU_G,real),
*format "%10.5e"
                "INTERNAL_FRICTION_ANGLE": *MatProp(INTERNAL_FRICTION_ANGLE,real),
*format "%10.5e"
                "KSIM": *MatProp(KSIM,real),
*format "%10.5e"
                "PS": *MatProp(PS,real),
*format "%10.5e"
                "PT": *MatProp(PT,real),
*format "%10.5e"
                "RHOS": *MatProp(RHOS,real),
*format "%10.5e"
                "RHOT": *MatProp(RHOT,real),
*format "%10.5e"
                "CHIS": *MatProp(CHIS,real),
*format "%10.5e"
                "CHIT": *MatProp(CHIT,real),
*elseif(strcmp(MatProp(Type),"CasmSoilPlasticity")==0)
*format "%10.5e"
                "SWELLING_SLOPE": *MatProp(SWELLING_SLOPE,real),
*format "%10.5e"
                "INITIAL_SHEAR_MODULUS": *MatProp(INITIAL_SHEAR_MODULUS,real),
*format "%10.5e"
                "ALPHA_SHEAR": *MatProp(ALPHA_SHEAR,real),
*format "%10.5e"
                "PRE_CONSOLIDATION_STRESS": *MatProp(PRE_CONSOLIDATION_STRESS,real),
*format "%10.5e"
                "OVER_CONSOLIDATION_RATIO": *MatProp(OVER_CONSOLIDATION_RATIO,real),
*format "%10.5e"
                "NORMAL_COMPRESSION_SLOPE": *MatProp(NORMAL_COMPRESSION_SLOPE,real),
*format "%10.5e"
                "CRITICAL_STATE_LINE": *MatProp(CRITICAL_STATE_LINE,real),
*format "%10.5e"
                "INTERNAL_FRICTION_ANGLE": *MatProp(INTERNAL_FRICTION_ANGLE,real),
*format "%10.5e"
                "SHAPE_PARAMETER": *MatProp(SHAPE_PARAMETER,real),
*format "%10.5e"
                "SPACING_RATIO": *MatProp(SPACING_RATIO,real),
*format "%10.5e"
                "CASM_M": *MatProp(CASM_M,real),
*elseif(strcmp(MatProp(Type),"CriticalStatePlasticity")==0)
*format "%10.5e"
                "SWELLING_SLOPE": *MatProp(SWELLING_SLOPE,real),
*format "%10.5e"
                "INITIAL_SHEAR_MODULUS": *MatProp(INITIAL_SHEAR_MODULUS,real),
*format "%10.5e"
                "ALPHA_SHEAR": *MatProp(ALPHA_SHEAR,real),
*format "%10.5e"
                "PRE_CONSOLIDATION_STRESS": *MatProp(PRE_CONSOLIDATION_STRESS,real),
*format "%10.5e"
                "OVER_CONSOLIDATION_RATIO": *MatProp(OVER_CONSOLIDATION_RATIO,real),
*format "%10.5e"
                "NORMAL_COMPRESSION_SLOPE": *MatProp(NORMAL_COMPRESSION_SLOPE,real),
*format "%10.5e"
                "CRITICAL_STATE_LINE": *MatProp(CRITICAL_STATE_LINE,real),
*format "%10.5e"
                "INTERNAL_FRICTION_ANGLE": *MatProp(INTERNAL_FRICTION_ANGLE,real),
*endif
*format "%10.5e"
                "DENSITY_WATER": *MatProp(WATER_DENSITY,real),
*format "%10.5e"
                "WATER_BULK_MODULUS": *MatProp(WATER_BULK_MODULUS,real),
*format "%10.5e"
                "PERMEABILITY_WATER": *MatProp(PERMEABILITY_WATER,real),
*format "%10.5e"
                "STABILIZATION_FACTOR_P": *MatProp(STABILIZATION_FACTOR_P,real),
*format "%10.5e"
                "STABILIZATION_FACTOR_J": *MatProp(STABILIZATION_FACTOR_J,real),
*format "%10.5e"
                "CONTACT_ADHESION": *MatProp(CONTACT_ADHESION,real),
*format "%10.5e"
                "CONTACT_FRICTION_ANGLE": *MatProp(CONTACT_FRICTION_ANGLE,real),
*format "%10.5e"
                "K0": *MatProp(K0,real),
*format "%10.5e"
                "DENSITY": *MatProp(DENSITY,real),
                "STABILIZATION_FACTOR_WP": 1.4,
                "KOZENY_CARMAN": 0,
                "INITIAL_POROSITY": 0.5,
*format "%10.5e"
                "THICKNESS": *MatProp(THICKNESS,real)
            },
            "tables"           : {}
        }
*if(Counter==GroupNumber)
     }
*else
     },
*endif
*end materials
   ]
}
