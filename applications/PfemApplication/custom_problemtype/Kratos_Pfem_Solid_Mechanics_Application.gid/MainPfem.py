""" Project: KRATOS_PFEM_GID
    Developer: JMCarbonell
    Mantainer: JMC
"""

import KratosMultiphysics
import KratosMultiphysics.PfemApplication
import KratosMultiphysics.ConstitutiveModelsApplication

from KratosMultiphysics.SolidMechanicsApplication.solid_composite_analysis import SolidCompositeAnalysis

try:
    import KratosMultiphysics.FluidDynamicsApplication
except ModuleNotFoundError:
    print("FluidDynamicsApplication not imported >> Needed if ALE and Eulerian elements are selected" )

SolidCompositeAnalysis(KratosMultiphysics.Model()).Run()