//
//   Project Name:        KratosPfemApplication    $
//   Developed by:        $Developer:  JMCarbonell $
//   Maintained by:       $Maintainer:         JMC $
//   Date:                $Date:      October 2016 $
//
//

// System includes

// External includes

// Project includes
#include "custom_python/add_custom_processes_to_python.h"

// Properties
#include "includes/properties.h"

// Processes
//#include "custom_processes/adaptive_time_interval_process.hpp"
#include "custom_processes/assign_properties_to_nodes_process.hpp"
#include "custom_processes/manage_isolated_nodes_process.hpp"
#include "custom_processes/manage_selected_elements_process.hpp"
#include "custom_processes/recover_volume_losses_process.hpp"
#include "custom_processes/volume_shaping_process.hpp"
#include "custom_processes/ale_rotation_process.hpp"

// PreMeshing processes
#include "custom_processes/inlet_mesher_process.hpp"
#include "custom_processes/insert_fluid_nodes_mesher_process.hpp"
#include "custom_processes/remove_fluid_nodes_mesher_process.hpp"
#include "custom_processes/select_fluid_elements_mesher_process.hpp"
#include "custom_processes/refine_fluid_elements_in_edges_mesher_process.hpp"
#include "custom_processes/smooth_free_surface_mesher_process.hpp"
#include "custom_processes/refine_conditions_in_contact_mesher_process.hpp"

// MiddleMeshing processes

// PostMeshing processes

namespace Kratos
{

namespace Python
{

void AddCustomProcessesToPython(pybind11::module &m)
{

    namespace py = pybind11;

    //**********MODEL PROPERTIES*********//

    /// Properties container. A vector set of properties with their Id's as key.
    typedef PointerVectorSet<Properties, IndexedObject> PropertiesContainerType;
    typedef Process ProcessBaseType;
    typedef typename PropertiesContainerType::Pointer PropertiesContainerPointerType;
    typedef std::vector<SpatialBoundingBox::Pointer> BoundingBoxContainer;

    //to define it as a variable
    py::class_<Variable<PropertiesContainerPointerType>, VariableData>(m, "PropertiesVectorPointerVariable")
        .def("__repr__", &Variable<PropertiesContainerPointerType>::Info);

    //**********MESHER PROCESSES*********//

    py::class_<RemoveFluidNodesMesherProcess, RemoveFluidNodesMesherProcess::Pointer, MesherProcess>(m, "RemoveFluidNodes")
        .def(py::init<ModelPart &, MesherData::MeshingParameters &, int>());

    py::class_<InsertFluidNodesMesherProcess, InsertFluidNodesMesherProcess::Pointer, MesherProcess>(m, "InsertFluidNodes")
        .def(py::init<ModelPart &, MesherData::MeshingParameters &, int>());

    py::class_<InletMesherProcess, InletMesherProcess::Pointer, MesherProcess>(m, "InsertInlet")
        .def(py::init<ModelPart &, MesherData::MeshingParameters &, int>());

    py::class_<SelectFluidElementsMesherProcess, SelectFluidElementsMesherProcess::Pointer, SelectElementsMesherProcess>(m, "SelectFluidElements")
        .def(py::init<ModelPart &, MesherData::MeshingParameters &, int>());

    py::class_<RefineFluidElementsInEdgesMesherProcess, RefineFluidElementsInEdgesMesherProcess::Pointer, RefineElementsInEdgesMesherProcess>(m, "RefineFluidElementsInEdges")
        .def(py::init<ModelPart &, MesherData::MeshingParameters &, int>());

    py::class_<SmoothFreeSurfaceMesherProcess, SmoothFreeSurfaceMesherProcess::Pointer, MesherProcess>(m, "SmoothFreeSurface")
        .def(py::init<ModelPart &, MesherData::MeshingParameters &, int>());

    py::class_<RefineConditionsInContactMesherProcess, RefineConditionsInContactMesherProcess::Pointer, RefineConditionsMesherProcess>(m, "RefineConditionsInContact")
        .def(py::init<ModelPart &, BoundingBoxContainer &, MesherData::MeshingParameters &, int>());

    //*********SET SOLVER PROCESSES*************//
    py::class_<AssignPropertiesToNodesProcess, AssignPropertiesToNodesProcess::Pointer, Process>(m, "AssignPropertiesToNodes")
        .def(py::init<ModelPart &, Parameters>())
        .def(py::init<ModelPart &, Parameters &>());

    //*********ADAPTIVE TIME STEP*************//
    // py::class_<AdaptiveTimeIntervalProcess, AdaptiveTimeIntervalProcess::Pointer, Process>
    //     (m, "AdaptiveTimeIntervalProcess")
    //     .def(py::init<ModelPart&, int>());

    //*********VOLUME RECOVERY PROCESS********//
    py::class_<RecoverVolumeLossesProcess, RecoverVolumeLossesProcess::Pointer, Process>(m, "RecoverVolumeLosses")
        .def(py::init<ModelPart &, Parameters &>());

    //*********MANAGE PARTICULAR ENTITIES PROCESS********//
    py::class_<ManageIsolatedNodesProcess, ManageIsolatedNodesProcess::Pointer, Process>(m, "ManageIsolatedNodesProcess")
        .def(py::init<ModelPart &>());

    py::class_<ManageSelectedElementsProcess, ManageSelectedElementsProcess::Pointer, Process>(m, "ManageSelectedElementsProcess")
        .def(py::init<ModelPart &>());

    //*********VOLUME SHAPING PROCESS********//
    py::class_<VolumeShapingProcess, VolumeShapingProcess::Pointer, Process>(m, "VolumeShapingProcess")
        .def(py::init<ModelPart &, Parameters>())
        .def(py::init<ModelPart &, Parameters &>());

    //*********ALE ROTATION PROCESS********//
    py::class_<AleRotationProcess, AleRotationProcess::Pointer, ProcessBaseType>(m, "AleRotationProcess")
        .def(py::init<ModelPart &, Parameters>());
    ;
}

} // namespace Python.

} // Namespace Kratos
