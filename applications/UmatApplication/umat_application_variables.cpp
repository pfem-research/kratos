//
//   Project Name:        KratosUmatApplication        $
//   Developed by:        $Developer:      JMCarbonell $
//   Maintained by:       $Maintainer:       LMonforte $
//   Date:                $Date:        September 2017 $
//
//

#include "umat_application_variables.h"

namespace Kratos
{
KRATOS_CREATE_VARIABLE(double, ALPHA)
KRATOS_CREATE_VARIABLE(double, BETA)
KRATOS_CREATE_VARIABLE(double, MF)
KRATOS_CREATE_VARIABLE(double, CC)
KRATOS_CREATE_VARIABLE(double, MM)
KRATOS_CREATE_VARIABLE(double, KSIS)
KRATOS_CREATE_VARIABLE(double, RHOM)
KRATOS_CREATE_VARIABLE(double, PC0)
KRATOS_CREATE_VARIABLE(double, VOID_RATIO)
KRATOS_CREATE_VARIABLE(double, PLASTIC_MULTIPLIER)

} // namespace Kratos
