//
//   Project Name:        KratosUmatApplication        $
//   Developed by:        $Developer:        LMonforte $
//   Maintained by:       $Maintainer:              LM $
//   Date:                $Date:          October 2017 $
//
//

// System includes

// External includes

// Project includes
#include "custom_models/small_strain_casm_model.hpp"
#include "custom_utilities/stress_invariants_utilities.hpp"


namespace Kratos
{

   //******************************CONSTRUCTOR*******************************************
   //************************************************************************************

   SmallStrainCasmModel::SmallStrainCasmModel()
      : ConstitutiveModel()
   {
      KRATOS_TRY

      mInitializedModel = false;
      mInitializedStress = false;

      KRATOS_CATCH("")
   }

   //******************************COPY CONSTRUCTOR**************************************
   //************************************************************************************

   SmallStrainCasmModel::SmallStrainCasmModel(const SmallStrainCasmModel &rOther)
      : ConstitutiveModel(rOther), mInitializedModel(rOther.mInitializedModel),
      mInitializedStress( rOther.mInitializedStress),
      mStateVariablesFinalized(rOther.mStateVariablesFinalized), mStressVectorFinalized(rOther.mStressVectorFinalized),
      mStrainVectorFinalized(rOther.mStrainVectorFinalized){
         KRATOS_TRY

      KRATOS_CATCH("")}

   //********************************CLONE***********************************************
   //************************************************************************************

   ConstitutiveModel::Pointer SmallStrainCasmModel::Clone() const
   {
      KRATOS_TRY

      return Kratos::make_shared<SmallStrainCasmModel>(*this);

      KRATOS_CATCH("")
   }

   //********************************ASSIGNMENT******************************************
   //************************************************************************************
   SmallStrainCasmModel &SmallStrainCasmModel::operator=(SmallStrainCasmModel const &rOther)
   {
      KRATOS_TRY

      ConstitutiveModel::operator=(rOther);
      this->mInitializedModel = rOther.mInitializedModel;
      this->mInitializedStress= rOther.mInitializedStress;
      return *this;

      KRATOS_CATCH("")
   }

   //*******************************DESTRUCTOR*******************************************
   //************************************************************************************

   SmallStrainCasmModel::~SmallStrainCasmModel()
   {
   }

   //***********************PUBLIC OPERATIONS FROM BASE CLASS****************************
   //************************************************************************************

   void SmallStrainCasmModel::InitializeModel(ModelDataType &rValues)
   {
      KRATOS_TRY

      if (mInitializedModel == false)
      {

         UmatDataType Variables;
         this->InitializeElasticData(rValues, Variables);

         const ModelDataType &rModelData = Variables.GetModelData();
         const Properties &rMaterialProperties = rModelData.GetProperties();

         int number_state_variables = 2;
         mStateVariablesFinalized = ZeroVector(number_state_variables);
         mStateVariablesFinalized(0) = -rMaterialProperties[PRE_CONSOLIDATION_STRESS];
         mStateVariablesFinalized(1) = 0.0;

         if ( mInitializedStress == false) {
            mStressVectorFinalized.clear();
            mStressVectorFinalized(0) = -rMaterialProperties[PRE_CONSOLIDATION_STRESS];
            mStressVectorFinalized(1) = -rMaterialProperties[PRE_CONSOLIDATION_STRESS];
            mStressVectorFinalized(2) = -rMaterialProperties[PRE_CONSOLIDATION_STRESS];
            mInitializedStress = true;
         }

         mStrainVectorFinalized.clear();
      }
      mInitializedModel = true;

      KRATOS_CATCH(" ")
   }

   //************************************************************************************
   //************************************************************************************

   void SmallStrainCasmModel::FinalizeModel(ModelDataType &rValues)
   {
      KRATOS_TRY

      KRATOS_CATCH(" ")
   }

   //************************************************************************************
   //************************************************************************************
   void SmallStrainCasmModel::InitializeElasticData(ModelDataType &rValues, UmatDataType &rVariables)
   {
      KRATOS_TRY

      //set model data pointer
      rVariables.SetModelData(rValues);

      rValues.State.Set(ConstitutiveModelData::PLASTIC_REGION, false);

      rValues.State.Set(ConstitutiveModelData::IMPLEX_ACTIVE, false);
      if (rValues.GetProcessInfo()[IMPLEX] == 1)
      {
         rValues.State.Set(ConstitutiveModelData::IMPLEX_ACTIVE, true);
      }

      rVariables.SetState(rValues.State);

      //add initial strain
      if (this->mOptions.Is(ConstitutiveModel::ADD_HISTORY_VECTOR) && this->mOptions.Is(ConstitutiveModel::HISTORY_STRAIN_MEASURE))
      {
         VectorType StrainVector;
         ConstitutiveModelUtilities::StrainTensorToVector(rValues.StrainMatrix, StrainVector);
         for (unsigned int i = 0; i < StrainVector.size(); i++)
         {
           StrainVector[i] += (*this->mpHistoryVector)[i];
         }
         rValues.StrainMatrix = ConstitutiveModelUtilities::StrainVectorToTensor(StrainVector, rValues.StrainMatrix);
      }

      rValues.SetStrainMeasure(ConstitutiveModelData::StrainMeasureType::StrainMeasure_None);

      rVariables.TotalStrainMatrix = rValues.StrainMatrix;
      rVariables.IncrementalDeformation = rValues.GetDeltaDeformationMatrix();

      KRATOS_CATCH(" ")
   }


   //************************************************************************************
   //************************************************************************************

   void SmallStrainCasmModel::CalculateStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix)
   {

      KRATOS_TRY

      Matrix ConstitutiveMatrix(6,6);
      noalias( ConstitutiveMatrix ) = ZeroMatrix(6,6);
      this->CalculateStressAndConstitutiveTensors(rValues, rStressMatrix, ConstitutiveMatrix);

      KRATOS_CATCH(" ")
   }

   //************************************************************************************
   //************************************************************************************

   void SmallStrainCasmModel::CalculateConstitutiveTensor(ModelDataType &rValues, Matrix &rConstitutiveMatrix)
   {
      KRATOS_TRY

      MatrixType StressMatrix;
      this->CalculateStressAndConstitutiveTensors(rValues, StressMatrix, rConstitutiveMatrix);

      KRATOS_CATCH(" ")
   }

   //************************************************************************************
   //************************************************************************************

   void SmallStrainCasmModel::CalculateStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix)
   {
      KRATOS_TRY


      UmatDataType Variables;
      this->InitializeElasticData(rValues, Variables);

      double Tolerance = 1E-12;
      double gamma = 0.0;

      const ModelDataType &rModelData = Variables.GetModelData();
      const Properties &rMaterialProperties = rModelData.GetProperties();

      Matrix  ConstitutiveMatrix(6,6);
      noalias( ConstitutiveMatrix ) = ZeroMatrix(6,6);


      // 1. Create DeltaStrain,....
      VectorType StrainVector;
      VectorType DeltaStrain;

      ConstitutiveModelUtilities::StrainTensorToVector( Variables.TotalStrainMatrix, StrainVector);
      DeltaStrain = StrainVector - mStrainVectorFinalized;

      double DeltaStrainVol = 0;
      for (unsigned int i = 0; i < 3; i++)
         DeltaStrainVol += DeltaStrain(i);

      VectorType DeltaStrainDev = DeltaStrain;
      for (unsigned int i = 0; i < 3; i++)
         DeltaStrainDev(i) -= DeltaStrainVol/3.0;

      double p0 = 0;
      for (unsigned int i = 0; i < 3; i++)
         p0 += mStressVectorFinalized(i)/3.0;

      VectorType s0;
      s0 = mStressVectorFinalized;
      for (unsigned int i = 0; i < 3; i++)
         s0(i) -= p0;

      double pc0 = mStateVariablesFinalized(0);
      double gamma0 = mStateVariablesFinalized(1);
      double pc = pc0;

      Matrix ThisDevMatrix(6,6);
      noalias( ThisDevMatrix ) = ZeroMatrix(6,6);
      for (unsigned int i = 0; i < 6; i++) {
         double number = 1.0;
         if ( i > 2) {
            number = 0.5;
         }
         ThisDevMatrix(i,i) = number;
      }

      Matrix IDevMatrix(6,6);
      noalias( IDevMatrix ) = ZeroMatrix(6,6);
      for (unsigned int i = 0; i < 3; i++) {
         IDevMatrix(i,i) = 1.0;
         IDevMatrix(3+i,3+i) = 2.0;
         for (unsigned int j = 0; j < 3; j++) {
            IDevMatrix(i,j) -= 1.0/3.0;
         }
      }

      // 2. Compute trial elastic
      const double &rKappa = rMaterialProperties[SWELLING_SLOPE];
      const double &rNu    = rMaterialProperties[POISSON_RATIO];

      double p = p0*std::exp( -DeltaStrainVol/rKappa);
      double G = 3.0*(1.0-2*rNu)/2.0/(1.0+rNu) * (-p)/rKappa;

      VectorType s;
      s = s0;
      s += 2.0*G* prod( ThisDevMatrix, DeltaStrainDev);

      /*std::cout << " TRIAL " << std::endl;
        std::cout << p0 << " s " << s0 << " Dvol " << DeltaStrainVol << " dev " << DeltaStrainDev << " pc " << pc0 << std::endl;
        std::cout << p << " s " << s << " Dvol " << DeltaStrainVol << " dev " << DeltaStrainDev << " pc " << pc << std::endl;
        std::cout << std::endl;
        std::cout << " thisDevMatrix DeltaStrainDev G " << ThisDevMatrix << DeltaStrainDev << G << std::endl;*/

      double yield0;
      yield0 = this->EvaluateYieldSurface( p, s, pc0, yield0, rMaterialProperties);

      const bool Implex =  Variables.State().Is(ConstitutiveModelData::IMPLEX_ACTIVE);

      if ( yield0 < Tolerance || ((Implex == true) && (gamma0 == 0)) ) {

         double dPdeVol = -p0 * exp(-DeltaStrainVol/ rKappa)/rKappa;
         for (unsigned int i = 0; i < 3; i++) {
            for (unsigned int j = 0; j < 3; j++) {
               ConstitutiveMatrix(i,j) += dPdeVol;

               ConstitutiveMatrix(i,j) -= 2.0/3.0*G;
            }
         }

         ConstitutiveMatrix += 2.0*G*ThisDevMatrix;

         double dGdeVol = 3.0*(1.0-2.0*rNu)/2.0/(1.0+rNu) * (-p0) * exp( -DeltaStrainVol/rKappa)/pow( rKappa, 2.0);
         VectorType UpdateVector;
         noalias( UpdateVector ) = prod( ThisDevMatrix, DeltaStrainDev);
         for (unsigned int i = 0; i < 6; i++) {
            for (unsigned int j = 0; j < 3; j++) {
               ConstitutiveMatrix(i,j) += 2*dGdeVol * UpdateVector(i);
            }
         }

         /*std::cout << " Elastic stiffness " << ConstitutiveMatrix << std::endl;*/
      } else {

         Matrix Jacobian(9,9);
         noalias( Jacobian) = ZeroMatrix(9,9);

         Vector Residual(9);
         noalias( Residual) = ZeroVector(9);

         Vector Dx(9);
         noalias( Dx ) = ZeroVector(9);

         VectorType AuxRes;

         double iter = 0;
         double NormResidual;

         if (Implex)
            gamma0 = gamma;

         while ( iter < 100) {


            Residual = ComputeResidual(Residual, gamma, p, p0, pc, pc0, s, s0, DeltaStrainVol, DeltaStrainDev, rMaterialProperties, gamma0, Implex);



            NormResidual = MathUtils<double>::Norm(Residual);



            /*std::cout << " iter " << iter << " norm " << NormResidual << " , " << Residual << std::endl;
              std::cout << std::endl;
              std::cout << p0 << " s " << s0 << " Dvol " << DeltaStrainVol << " dev " << DeltaStrainDev << " pc " << pc0 << std::endl;
              std::cout << p << " s " << s << " Dvol " << DeltaStrainVol << " dev " << DeltaStrainDev << " pc " << pc << std::endl;
              std::cout << " gamma " << gamma << std::endl;
              std::cout << std::endl;*/

            if ( NormResidual < Tolerance && iter > 1) {
               break;
            } else if (iter == 99 && NormResidual > 100.0*Tolerance) {
            std::cout << " iter " << iter << " norm " << NormResidual << " , " << Residual << std::endl;
              std::cout << std::endl;
              std::cout << p0 << " s0 " << s0 << " Dvol " << DeltaStrainVol << " dev " << DeltaStrainDev << " pc0 " << pc0 << std::endl;
              std::cout << p << " s " << s << " Dvol " << DeltaStrainVol << " dev " << DeltaStrainDev << " pc " << pc << std::endl;
              std::cout << " gamma " << gamma << std::endl;
              std::cout << " gamma0 " << gamma0 << std::endl;
              std::cout << " implex " << Implex << std::endl;
              std::cout << std::endl;
              KRATOS_ERROR << " this is not converging. CASMMMMM " << std::endl;
            }

            Jacobian = ComputeJacobian(Jacobian, gamma, p, p0, pc, pc0, s, s0, DeltaStrainVol, DeltaStrainDev, rMaterialProperties, gamma0, Implex);

            MathUtils<double>::Solve( Jacobian, Dx, Residual);
            Dx *= (-1.0);

            /*std::cout << " jacobian " << Jacobian << std::endl;
              std::cout << " dX " << Dx << std::endl;*/



            p = p + Dx(0);
            pc = pc + Dx(1);

            gamma = gamma + Dx(2);
            if ( gamma < 0)
               gamma = 0;

            for (unsigned int i = 0; i < 6; i++)
               s(i) = s(i) + Dx(3+i);

            iter = iter+1;

         }

         Matrix dXdV(6,9);
         noalias( dXdV ) = ZeroMatrix(6,9);
         dXdV(0,0) = 1.0;
         dXdV(1,0) = 1.0;
         dXdV(2,0) = 1.0;
         for (unsigned int i = 0; i < 6; i++)
            dXdV(i, 3+i) = 1.0;


         Matrix dRdE(9,6);
         noalias( dRdE ) = ZeroMatrix(9,6);
         dRdE = ComputedResidualdEpsilon( dRdE, gamma, p, p0, pc, pc0, s, s0, DeltaStrain, rMaterialProperties, gamma0, Implex);

         Matrix InverseJacobian(9,9);
         noalias (InverseJacobian) = ZeroMatrix(9,9);

         double dummy(0);
         MathUtils<double>::InvertMatrix(Jacobian, InverseJacobian, dummy);

         noalias( ConstitutiveMatrix ) = -prod( dXdV, Matrix( prod( InverseJacobian, dRdE) ) );


         /*std::cout << " ============================= " << std::endl;
           std::cout << " dRdE " << dRdE << std::endl;
           std::cout << InverseJacobian << std::endl;
           std::cout << " dXdV " << dXdV << std::endl;
           std::cout << " CONSTITUTIVE MATRIX " << ConstitutiveMatrix << std::endl;
           std::cout << " ============================= " << std::endl;*/


      } // end If-Plastic


      // Save stress vector
      VectorType StressVector;
      StressVector = s;
      for (unsigned int i = 0; i < 3; i++)
         StressVector(i) += p;

      rStressMatrix = ConstitutiveModelUtilities::VectorToSymmetricTensor(StressVector, rStressMatrix);

      this->SetConstitutiveMatrix(rConstitutiveMatrix, ConstitutiveMatrix, rStressMatrix);


      /*std::cout << " ========================== " << std::endl;
        std::cout << ConstitutiveMatrix << std::endl;
        std::cout << rStressMatrix << std::endl;
        std::cout << rConstitutiveMatrix << std::endl;
        std::cout << " ========================== " << std::endl;*/


      // update internal variables
      if (rValues.State.Is(ConstitutiveModelData::UPDATE_INTERNAL_VARIABLES))
      {
         VectorType StrainVector;
         ConstitutiveModelUtilities::StrainTensorToVector(Variables.TotalStrainMatrix, StrainVector);
         mStrainVectorFinalized = StrainVector;

         mStressVectorFinalized = StressVector;
         mStateVariablesFinalized(0) = pc;
         mStateVariablesFinalized(1) = gamma;
      }


      KRATOS_CATCH(" ")
   }

   Matrix & SmallStrainCasmModel::ComputedResidualdEpsilon(Matrix & dRdE, const double & gamma, const double & p, const double & p0, const double &pc, const double & pc0, const VectorType & s, const VectorType & s0, const VectorType &  DeltaStrain, const Properties & rMaterialProperties, const double & rGamma0, const bool & rImplex )
   {
      KRATOS_TRY

      double delta = 1E-4;

      Vector Res1(9);
      noalias(Res1) = ZeroVector(9);
      Vector Res2(9);
      noalias(Res2) = ZeroVector(9);

      VectorType DS;
      DS.clear();

      VectorType DeltaStrainDev;

      double DeltaStrainVol;

      for (unsigned int j = 0; j < 6; j++) {

         DS = DeltaStrain;
         DS(j) += delta;

         DeltaStrainVol = 0;
         for (unsigned int i = 0; i < 3; i++)
            DeltaStrainVol += DS(i);

         DeltaStrainDev = DS;
         for (unsigned int i = 0; i < 3; i++)
            DeltaStrainDev(i) -= DeltaStrainVol/3.0;

         Res1 = ComputeResidual(Res1, gamma, p, p0, pc, pc0, s, s0, DeltaStrainVol, DeltaStrainDev, rMaterialProperties, rGamma0, rImplex);

         DS = DeltaStrain;
         DS(j) -= delta;

         DeltaStrainVol = 0;
         for (unsigned int i = 0; i < 3; i++)
            DeltaStrainVol += DS(i);

         DeltaStrainDev = DS;
         for (unsigned int i = 0; i < 3; i++)
            DeltaStrainDev(i) -= DeltaStrainVol/3.0;

         Res2 = ComputeResidual(Res2, gamma, p, p0, pc, pc0, s, s0, DeltaStrainVol, DeltaStrainDev, rMaterialProperties, rGamma0, rImplex);

         Res1 -= Res2;
         Res1 /= (2.0*delta);

         for (unsigned int i = 0; i < 9; i++)
            dRdE(i,j) = Res1(i);


      }

      return dRdE;

      KRATOS_CATCH("")
   }



   Matrix & SmallStrainCasmModel::ComputeJacobian(Matrix & Jacobian, const double & gamma, const double & p, const double & p0, const double &pc, const double & pc0, const VectorType & s, const VectorType & s0, const double & DeltaStrainVol, const VectorType &  DeltaStrainDev, const Properties & rMaterialProperties,
         const double & rGamma0, const bool & rImplex)
   {
      KRATOS_TRY

      double delta = 1E-5;

      Vector Res1(9);
      noalias(Res1) = ZeroVector(9);
      Vector Res2(9);
      noalias(Res2) = ZeroVector(9);

      // 1. Pressure
      Res1 = ComputeResidual(Res1, gamma, p+delta, p0, pc, pc0, s, s0, DeltaStrainVol, DeltaStrainDev, rMaterialProperties, rGamma0, rImplex);
      Res2 = ComputeResidual(Res2, gamma, p-delta, p0, pc, pc0, s, s0, DeltaStrainVol, DeltaStrainDev, rMaterialProperties, rGamma0, rImplex);
      Res1 -= Res2;
      Res1 /= (2.0*delta);
      for (unsigned int i = 0; i < 9; i++)
         Jacobian(i,0) = Res1(i);

      // 2. PreCon
      Res1 = ComputeResidual(Res1, gamma, p, p0, pc+delta, pc0, s, s0, DeltaStrainVol, DeltaStrainDev, rMaterialProperties, rGamma0, rImplex);
      Res2 = ComputeResidual(Res2, gamma, p, p0, pc-delta, pc0, s, s0, DeltaStrainVol, DeltaStrainDev, rMaterialProperties, rGamma0, rImplex);
      Res1 -= Res2;
      Res1 /= (2.0*delta);
      for (unsigned int i = 0; i < 9; i++)
         Jacobian(i,1) = Res1(i);


      // 3. gamma
      Res1 = ComputeResidual(Res1, gamma+delta, p, p0, pc, pc0, s, s0, DeltaStrainVol, DeltaStrainDev, rMaterialProperties, rGamma0, rImplex);
      Res2 = ComputeResidual(Res2, gamma-delta, p, p0, pc, pc0, s, s0, DeltaStrainVol, DeltaStrainDev, rMaterialProperties, rGamma0, rImplex);
      Res1 -= Res2;
      Res1 /= (2.0*delta);
      for (unsigned int i = 0; i < 9; i++)
         Jacobian(i,2) = Res1(i);

      // 4. s
      for (unsigned j = 0; j < 6; j++) {
         VectorType s1;
         VectorType s2;
         s1 = s;
         s2 = s;
         s1(j) += delta;
         s2(j) -= delta;

         Res1 = ComputeResidual(Res1, gamma, p, p0, pc, pc0, s1, s0, DeltaStrainVol, DeltaStrainDev, rMaterialProperties, rGamma0, rImplex);
         Res2 = ComputeResidual(Res2, gamma, p, p0, pc, pc0, s2, s0, DeltaStrainVol, DeltaStrainDev, rMaterialProperties, rGamma0, rImplex);
         Res1 -= Res2;
         Res1 /= (2.0*delta);
         for (unsigned int i = 0; i < 9; i++)
            Jacobian(i,3+j) = Res1(i);
      }

      return Jacobian;

      KRATOS_CATCH("")
   }
   Vector & SmallStrainCasmModel::ComputeResidual(Vector & Residual, const double & gamma, const double & p, const double & p0, const double &pc, const double & pc0, const VectorType & s, const VectorType & s0, const double & DeltaStrainVol, const VectorType &  DeltaStrainDev, const Properties & rMaterialProperties,
         const double & rGamma0, const bool & rImplex )
   {
      KRATOS_TRY

      const double &rKappa = rMaterialProperties[SWELLING_SLOPE];
      const double &rLambda = rMaterialProperties[NORMAL_COMPRESSION_SLOPE];
      const double &rNu    = rMaterialProperties[POISSON_RATIO];
      const double &rM     = rMaterialProperties[CRITICAL_STATE_LINE];
      const double &rN     = rMaterialProperties[SHAPE_PARAMETER];
      const double &rR     = rMaterialProperties[SPACING_RATIO];
      const double &rmm    = rMaterialProperties[CASM_M];

      Matrix ThisDevMatrix(6,6);
      noalias( ThisDevMatrix ) = ZeroMatrix(6,6);
      for (unsigned int i = 0; i < 6; i++) {
         double number = 1.0;
         if ( i > 2) {
            number = 0.5;
         }
         ThisDevMatrix(i,i) = number;
      }



      double G = 3.0*(1.0-2*rNu)/2.0/(1.0+rNu) * (-p)/rKappa;
      double J2sqrt = 0;
      for (unsigned int i = 0; i < 6; i++) {
         double number = 1.0;
         if (i > 2)
            number = 2.0;
         J2sqrt += number * pow( s(i), 2.0);
      }
      J2sqrt = sqrt( 1.0/2.0*J2sqrt);

      VectorType V2;
      V2 = s;
      for (unsigned int i = 3; i <6; i++)
         V2(i) *= 2.0;
      if ( J2sqrt > 1E-8)
         V2 /= ( 2.0*J2sqrt);


      double dummy = (-p)/(rmm-1.0)*( (rmm-1.0) + pow( sqrt(3.0)*J2sqrt/rM/(-p), rmm) );
      double C1PlasticP  = rmm * pow(sqrt(3.0)*J2sqrt/rM/(-p), rmm-1.0 )  * sqrt(3.0)*J2sqrt/rM/pow(-p,2.0) - dummy*(rmm-1.0)/pow(p,2.0);
      double C2PlasticP  = rmm * pow(sqrt(3.0)*J2sqrt/rM/(-p), rmm-1.0) * sqrt(3.0)/rM/(-p);

      double DeltaStrainVolPlastic = gamma*C1PlasticP;
      VectorType DeltaStrainDevPlastic = gamma*C2PlasticP*V2;



      Residual(0) = p - p0 * exp( - (DeltaStrainVol - DeltaStrainVolPlastic )/rKappa);
      Residual(1) = pc - pc0 * exp( - DeltaStrainVolPlastic / ( rLambda-rKappa) );

      if ( rImplex == false) {
         Residual(2) = pow(  sqrt(3.0)*J2sqrt/ (rM * (-p)), rN) + 1.0/log(rR) * log( p/pc);
      } else {
         Residual(2) = gamma-rGamma0;
      }

      VectorType AuxRes;
      noalias( AuxRes ) =  s  - s0 - 2.0 * G * prod(ThisDevMatrix, DeltaStrainDev-DeltaStrainDevPlastic);
      for (unsigned i = 0; i < 6; i++)
         Residual(3+i) = AuxRes(i);

      /*std::cout << " help me s " << s << std::endl;
        std::cout << " help me s0 " << s0 << std::endl;
        std::cout << " help me G  " << G << std::endl;
        std::cout << " help me dS " << DeltaStrainDev << std::endl;
        std::cout << " help me dSP" << DeltaStrainDevPlastic << std::endl;
        std::cout << " thisTerm " << AuxRes << std::endl;*/


      return Residual;


      KRATOS_CATCH("")
   }

   double & SmallStrainCasmModel::EvaluateYieldSurface( const double & rP, const VectorType & rS, const double & rPc, double & rYield, const Properties & rMaterialProperties)
   {
      KRATOS_TRY

      const double &rM     = rMaterialProperties[CRITICAL_STATE_LINE];
      const double &rN     = rMaterialProperties[SHAPE_PARAMETER];
      const double &rR     = rMaterialProperties[SPACING_RATIO];
      //const double &rmm    = rMaterialProperties[CASM_M];


      double J2sqrt = 0;
      for (unsigned int i = 0; i < 6; i++) {
         double number = 1.0;
         if (i > 2)
            number = 2.0;
         J2sqrt += number * pow( rS(i), 2.0);
      }
      J2sqrt = sqrt( 1.0/2.0*J2sqrt);

      rYield = pow(  sqrt(3.0)*J2sqrt/ (rM * (-rP)), rN) + 1.0/log(rR) * log( rP/rPc);


      return rYield;
      KRATOS_CATCH("")
   }


   //************************************************************************************
   //************************************************************************************
   void SmallStrainCasmModel::SetConstitutiveMatrix(Matrix &rC, const Matrix &rpCBig, const MatrixType &rStressMatrix)

   {
      KRATOS_TRY

      if (rC.size1() == 6)
      {
         for (unsigned int i = 0; i < 6; i++)
            for (unsigned int j = 0; j < 6; j++)
               rC(i, j) = rpCBig(i, j);
      }
      else if (rC.size1() == 3)
      {
         rC(0, 0) = rpCBig(0, 0);
         rC(0, 1) = rpCBig(0, 1);
         rC(0, 2) = rpCBig(0, 3);
         rC(1, 0) = rpCBig(1, 0);
         rC(1, 1) = rpCBig(1, 1);
         rC(1, 2) = rpCBig(1, 3);
         rC(2, 0) = rpCBig(3, 0);
         rC(2, 1) = rpCBig(3, 1);
         rC(2, 2) = rpCBig(3, 3);
      }
      else
      {
         for (int i = 0; i < 4; i++)
         {
            for (int j = 0; j < 4; j++)
            {
               rC(i, j) = rpCBig(i, j);
            }
         }
      }

      KRATOS_CATCH("")
   }
   int SmallStrainCasmModel::Check(const Properties &rMaterialProperties, const ProcessInfo &rCurrentProcessInfo)
   {
      KRATOS_TRY

      return 0;

      KRATOS_CATCH(" ")
   }
   //******************************************************************
   // Get Value doubles
   double &SmallStrainCasmModel::GetValue(const Variable<double> &rVariable, double &rValue)
   {
      KRATOS_TRY

      if (rVariable == STRESS_INV_P)
      {
         double J2;
         StressInvariantsUtilities::CalculateStressInvariants(mStressVectorFinalized, rValue, J2);
      }
      else if (rVariable == STRESS_INV_Q)
      {
         double p;
         StressInvariantsUtilities::CalculateStressInvariants(mStressVectorFinalized, p, rValue);
         rValue = rValue * sqrt(3.0);
      }
      else if (rVariable == STRESS_INV_THETA)
      {
         double p, J2;
         StressInvariantsUtilities::CalculateStressInvariants(mStressVectorFinalized, p, J2, rValue);
      } else if (rVariable == PRE_CONSOLIDATION_STRESS) {
         rValue = mStateVariablesFinalized(0);
      }
      else{
         ConstitutiveModel::GetValue( rVariable, rValue);
      }

      return rValue;
      KRATOS_CATCH("")
   }

   //******************************************************************
   // Get Value doubles
   void SmallStrainCasmModel::SetValue(const Variable<Vector> &rThisVariable, const Vector &rValue,
         const ProcessInfo &rCurrentProcessInfo)
   {
      KRATOS_TRY

      if ( rThisVariable == ELASTIC_LEFT_CAUCHY_FROM_KIRCHHOFF_STRESS ){
         mStressVectorFinalized.clear();
         mStressVectorFinalized = rValue;
         mInitializedStress = true;
      } else {
         ConstitutiveModel::SetValue( rThisVariable, rValue, rCurrentProcessInfo);
      }


      KRATOS_CATCH(" ")
   }


} // Namespace Kratos
