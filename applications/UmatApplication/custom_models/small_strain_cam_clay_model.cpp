//
//   Project Name:        KratosUmatApplication        $
//   Developed by:        $Developer:        LMonforte $
//   Maintained by:       $Maintainer:              LM $
//   Date:                $Date:          October 2017 $
//
//

// System includes

// External includes

// Project includes
#include "custom_models/small_strain_cam_clay_model.hpp"
#include "custom_utilities/stress_invariants_utilities.hpp"


namespace Kratos
{

   //******************************CONSTRUCTOR*******************************************
   //************************************************************************************

   SmallStrainCamClayModel::SmallStrainCamClayModel()
      : ConstitutiveModel()
   {
      KRATOS_TRY

      mInitializedModel = false;
      mInitializedStress = false;

      KRATOS_CATCH("")
   }

   //******************************COPY CONSTRUCTOR**************************************
   //************************************************************************************

   SmallStrainCamClayModel::SmallStrainCamClayModel(const SmallStrainCamClayModel &rOther)
      : ConstitutiveModel(rOther), mInitializedModel(rOther.mInitializedModel),
      mInitializedStress( rOther.mInitializedStress),
      mStateVariablesFinalized(rOther.mStateVariablesFinalized), mStressVectorFinalized(rOther.mStressVectorFinalized),
      mStrainVectorFinalized(rOther.mStrainVectorFinalized){
         KRATOS_TRY

      KRATOS_CATCH("")}

   //********************************CLONE***********************************************
   //************************************************************************************

   ConstitutiveModel::Pointer SmallStrainCamClayModel::Clone() const
   {
      KRATOS_TRY

      return Kratos::make_shared<SmallStrainCamClayModel>(*this);

      KRATOS_CATCH("")
   }

   //********************************ASSIGNMENT******************************************
   //************************************************************************************
   SmallStrainCamClayModel &SmallStrainCamClayModel::operator=(SmallStrainCamClayModel const &rOther)
   {
      KRATOS_TRY

      ConstitutiveModel::operator=(rOther);
      this->mInitializedModel = rOther.mInitializedModel;
      this->mInitializedStress= rOther.mInitializedStress;
      return *this;

      KRATOS_CATCH("")
   }

   //*******************************DESTRUCTOR*******************************************
   //************************************************************************************

   SmallStrainCamClayModel::~SmallStrainCamClayModel()
   {
   }

   //***********************PUBLIC OPERATIONS FROM BASE CLASS****************************
   //************************************************************************************

   void SmallStrainCamClayModel::InitializeModel(ModelDataType &rValues)
   {
      KRATOS_TRY

      if (mInitializedModel == false)
      {

         UmatDataType Variables;
         this->InitializeElasticData(rValues, Variables);

         const ModelDataType &rModelData = Variables.GetModelData();
         const Properties &rMaterialProperties = rModelData.GetProperties();

         int number_state_variables = 1;
         mStateVariablesFinalized = ZeroVector(number_state_variables);
         mStateVariablesFinalized(0) = -0.5*rMaterialProperties[PRE_CONSOLIDATION_STRESS];
         if ( mInitializedStress == false) {
            mStressVectorFinalized.clear();
            mStressVectorFinalized(0) = -0.5*rMaterialProperties[PRE_CONSOLIDATION_STRESS];
            mStressVectorFinalized(1) = -0.5*rMaterialProperties[PRE_CONSOLIDATION_STRESS];
            mStressVectorFinalized(2) = -0.5*rMaterialProperties[PRE_CONSOLIDATION_STRESS];
            mInitializedStress = true;
         }


         mStrainVectorFinalized.clear();
      }
      mInitializedModel = true;

      KRATOS_CATCH(" ")
   }

   //************************************************************************************
   //************************************************************************************

   void SmallStrainCamClayModel::FinalizeModel(ModelDataType &rValues)
   {
      KRATOS_TRY

      KRATOS_CATCH(" ")
   }

   //************************************************************************************
   //************************************************************************************
   void SmallStrainCamClayModel::InitializeElasticData(ModelDataType &rValues, UmatDataType &rVariables)
   {
      KRATOS_TRY

      //set model data pointer
      rVariables.SetModelData(rValues);
      rVariables.SetState(rValues.State);

      //add initial strain
      if (this->mOptions.Is(ConstitutiveModel::ADD_HISTORY_VECTOR) && this->mOptions.Is(ConstitutiveModel::HISTORY_STRAIN_MEASURE))
      {
         VectorType StrainVector;
         ConstitutiveModelUtilities::StrainTensorToVector(rValues.StrainMatrix, StrainVector);

         for (unsigned int i = 0; i < StrainVector.size(); i++)
         {
           StrainVector[i] += (*this->mpHistoryVector)[i];
         }
         rValues.StrainMatrix = ConstitutiveModelUtilities::StrainVectorToTensor(StrainVector, rValues.StrainMatrix);
      }

      rValues.SetStrainMeasure(ConstitutiveModelData::StrainMeasureType::StrainMeasure_None);

      rVariables.TotalStrainMatrix = rValues.StrainMatrix;
      rVariables.IncrementalDeformation = rValues.GetDeltaDeformationMatrix();

      KRATOS_CATCH(" ")
   }


   //************************************************************************************
   //************************************************************************************

   void SmallStrainCamClayModel::CalculateStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix)
   {

      KRATOS_TRY

      Matrix ConstitutiveMatrix(6,6);
      noalias( ConstitutiveMatrix ) = ZeroMatrix(6,6);
      this->CalculateStressAndConstitutiveTensors(rValues, rStressMatrix, ConstitutiveMatrix);

      KRATOS_CATCH(" ")
   }

   //************************************************************************************
   //************************************************************************************

   void SmallStrainCamClayModel::CalculateConstitutiveTensor(ModelDataType &rValues, Matrix &rConstitutiveMatrix)
   {
      KRATOS_TRY

      MatrixType StressMatrix;
      this->CalculateStressAndConstitutiveTensors(rValues, StressMatrix, rConstitutiveMatrix);

      KRATOS_CATCH(" ")
   }

   //************************************************************************************
   //************************************************************************************

   void SmallStrainCamClayModel::CalculateStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix)
   {
      KRATOS_TRY


      UmatDataType Variables;
      this->InitializeElasticData(rValues, Variables);

      double Tolerance = 1E-12;

      const ModelDataType &rModelData = Variables.GetModelData();
      const Properties &rMaterialProperties = rModelData.GetProperties();

      Matrix  ConstitutiveMatrix(6,6);
      noalias( ConstitutiveMatrix ) = ZeroMatrix(6,6);


      // 1. Create DeltaStrain,....
      VectorType StrainVector;
      VectorType DeltaStrain;

      ConstitutiveModelUtilities::StrainTensorToVector( Variables.TotalStrainMatrix, StrainVector);
      DeltaStrain = StrainVector - mStrainVectorFinalized;

      double DeltaStrainVol = 0;
      for (unsigned int i = 0; i < 3; i++)
         DeltaStrainVol += DeltaStrain(i);

      VectorType DeltaStrainDev = DeltaStrain;
      for (unsigned int i = 0; i < 3; i++)
         DeltaStrainDev(i) -= DeltaStrainVol/3.0;

      double p0 = 0;
      for (unsigned int i = 0; i < 3; i++)
         p0 += mStressVectorFinalized(i)/3.0;

      VectorType s0;
      s0 = mStressVectorFinalized;
      for (unsigned int i = 0; i < 3; i++)
         s0(i) -= p0;

      double pc0 = mStateVariablesFinalized(0);
      double pc = pc0;

      Matrix ThisDevMatrix(6,6);
      noalias( ThisDevMatrix ) = ZeroMatrix(6,6);
      for (unsigned int i = 0; i < 6; i++) {
         double number = 1.0;
         if ( i > 2) {
            number = 0.5;
         }
         ThisDevMatrix(i,i) = number;
      }

      Matrix IDevMatrix(6,6);
      noalias( IDevMatrix ) = ZeroMatrix(6,6);
      for (unsigned int i = 0; i < 3; i++) {
         IDevMatrix(i,i) = 1.0;
         IDevMatrix(3+i,3+i) = 2.0;
         for (unsigned int j = 0; j < 3; j++) {
            IDevMatrix(i,j) -= 1.0/3.0;
         }
      }

      // 2. Compute trial elastic
      const double &rKappa = rMaterialProperties[SWELLING_SLOPE];
      const double &rLambda = rMaterialProperties[NORMAL_COMPRESSION_SLOPE];
      const double &rNu    = rMaterialProperties[POISSON_RATIO];
      const double &rM     = rMaterialProperties[CRITICAL_STATE_LINE];

      double p = p0*std::exp( -DeltaStrainVol/rKappa);
      double G = 3.0*(1.0-2*rNu)/2.0/(1.0+rNu) * (-p)/rKappa;

      VectorType s;
      s = s0;
      s += 2*G* prod( ThisDevMatrix, DeltaStrainDev);

      /*std::cout << p0 << " s " << s0 << " Dvol " << DeltaStrainVol << " dev " << DeltaStrainDev << " pc " << pc0 << std::endl;
      std::cout << p << " s " << s << " Dvol " << DeltaStrainVol << " dev " << DeltaStrainDev << " pc " << pc << std::endl;
      std::cout << std::endl;
      std::cout << " thisDevMatrix DeltaStrainDev G " << ThisDevMatrix << DeltaStrainDev << G << std::endl;*/

      double yield0;
      yield0 = this->EvaluateYieldSurface( p, s, pc0, yield0, rM);

      if ( yield0 < Tolerance) {
         double dPdeVol = -p0 * exp(-DeltaStrainVol/ rKappa)/rKappa;
         for (unsigned int i = 0; i < 3; i++) {
            for (unsigned int j = 0; j < 3; j++) {
               ConstitutiveMatrix(i,j) += dPdeVol;

               ConstitutiveMatrix(i,j) -= 2.0/3.0*G;
            }
         }

         ConstitutiveMatrix += 2.0*G*ThisDevMatrix;

         double dGdeVol = 3.0*(1.0-2.0*rNu)/2.0/(1.0+rNu) * (-p0) * exp( -DeltaStrainVol/rKappa)/pow( rKappa, 2.0);
         VectorType UpdateVector;
         noalias( UpdateVector ) = prod( ThisDevMatrix, DeltaStrainDev);
         for (unsigned int i = 0; i < 6; i++) {
            for (unsigned int j = 0; j < 3; j++) {
               ConstitutiveMatrix(i,j) += 2*dGdeVol * UpdateVector(i);
            }
         }
      } else {

         Matrix Jacobian(9,9);
         noalias( Jacobian) = ZeroMatrix(9,9);

         Vector Residual(9);
         noalias( Residual) = ZeroVector(9);

         Vector Dx(9);
         noalias( Dx ) = ZeroVector(9);

         VectorType AuxRes;

         double gamma = 0;
         double iter = 0;
         double NormResidual;

         while ( iter < 100) {

            G = 3.0*(1.0-2*rNu)/2.0/(1.0+rNu) * (-p)/rKappa;
            Residual(0) = p - p0 * exp( - (DeltaStrainVol - 2.0*gamma*(p-pc) )/rKappa);
            Residual(1) = pc - pc0 * exp( - 2.0 * gamma * ( p - pc) / ( rLambda-rKappa) );

            double J2 = ThreeProduct(s, IDevMatrix, s, J2);

            Residual(2) = 3.0/2.0 / pow( rM, 2.0) * J2  + pow( p-pc, 2.0) - pow(pc, 2.0);

            noalias( AuxRes ) = (1.0 + 6.0*G*gamma/ pow(rM, 2.0)) *s  - s0 - 2.0 * G * prod(ThisDevMatrix, DeltaStrainDev);
            for (unsigned i = 0; i < 6; i++)
               Residual(3+i) = AuxRes(i);

            NormResidual = MathUtils<double>::Norm(Residual);

            /*std::cout << " iter " << iter << " norm " << NormResidual << " , " << Residual << std::endl;
            std::cout << " jacobian " << Jacobian << std::endl;
            std::cout << " dX " << Dx << std::endl;*/


            if ( NormResidual < Tolerance && iter > 1) {
               break;
            }

            double A1 = p0 * exp( - (DeltaStrainVol - 2.0*gamma*(p-pc) )/rKappa);
            double A2 = pc0 * exp( - 2.0 * gamma * ( p - pc) / ( rLambda-rKappa) );

            Jacobian(0,0) = 1.0 - 2.0*gamma/rKappa * A1;
            Jacobian(0,1) = 2.0*gamma/rKappa * A1;
            Jacobian(0,2) = -2.0*(p-pc) /rKappa * A1;

            Jacobian(1,0) = 2.0 * gamma/(rLambda-rKappa) * A2;
            Jacobian(1,1) = 1.0 - 2.0 * gamma/(rLambda-rKappa) * A2;
            Jacobian(1,2) = 2.0 * (p-pc) / (rLambda-rKappa) * A2;

            Jacobian(2,0) = 2.0*(p-pc);
            Jacobian(2,1) = -2.0*p;
            for (unsigned int i = 0; i < 6; i++){
               double number = 1.0;
               if ( i > 2)
                  number = 2.0;
               Jacobian(2,3+i) = number * 3.0/pow(rM, 2.0) * s(i);
            }

            for (unsigned int i = 0; i < 6; i++) {
               double number = 2.0;
               if ( i > 2)
                  number = 1.0;
               Jacobian(3+i,0) = (-6.0*gamma/pow( rM, 2.0)  * s(i)  +  number * DeltaStrainDev(i) ) * 3.0/ rKappa * (1.0 - 2 * rNu)/ 2 / ( 1.0 + rNu);
            }

            for (unsigned int i = 0; i < 6; i++) {
               Jacobian(3+i, 2) = 6.0*G/pow( rM, 2.0) * s(i);
            }
            for (unsigned int i = 0; i < 6; i++) {
               Jacobian(3+i, 3+i) = (1.0+6.0*gamma*G/pow( rM, 2.0));
            }

            MathUtils<double>::Solve( Jacobian, Dx, Residual);
            Dx *= (-1.0);
            p = p + Dx(0);
            pc = pc + Dx(1);
            gamma = gamma + Dx(2);
            for (unsigned int i = 0; i < 6; i++)
               s(i) = s(i) + Dx(3+i);

            iter = iter+1;

         }

         Matrix dXdV(6,9);
         noalias( dXdV ) = ZeroMatrix(6,9);
         dXdV(0,0) = 1.0;
         dXdV(1,0) = 1.0;
         dXdV(2,0) = 1.0;
         for (unsigned int i = 0; i < 6; i++)
            dXdV(i, 3+i) = 1.0;


         Matrix dRdE(9,6);
         noalias( dRdE ) = ZeroMatrix(9,6);
         double dRdEv = p0 * exp( -(DeltaStrainVol-2*gamma*(p-pc))/rKappa) / rKappa;
         dRdE(0,0) = dRdEv;
         dRdE(0,1) = dRdEv;
         dRdE(0,2) = dRdEv;


         for (unsigned int i = 0; i < 6; i++)
            dRdE(3+i, 0) = 6.0*gamma/pow(rM, 2.0) * 3.0*(1.0-2.0*rNu)/2.0/(1.0+rNu) / rKappa;

         for (unsigned int i = 0; i < 6; i++) {
            double number = 1.0;
            if (i > 2)
               number = 0.25;
            for (unsigned int j = 0;  j < 6; j++) {
               dRdE(3+i, j) = - 2.0*G*number*IDevMatrix(i,j);
            }
         }

         Matrix InverseJacobian(9,9);
         noalias (InverseJacobian) = ZeroMatrix(9,9);

         double dummy(0);
         MathUtils<double>::InvertMatrix(Jacobian, InverseJacobian, dummy);

         noalias( ConstitutiveMatrix ) = -prod( dXdV, Matrix( prod( InverseJacobian, dRdE) ) );


      } // end If-Plastic



      // Save stress vector
      VectorType StressVector;
      StressVector = s;
      for (unsigned int i = 0; i < 3; i++)
         StressVector(i) += p;

      rStressMatrix = ConstitutiveModelUtilities::VectorToSymmetricTensor(StressVector, rStressMatrix);

      this->SetConstitutiveMatrix(rConstitutiveMatrix, ConstitutiveMatrix, rStressMatrix);


      // update internal variables
      if (rValues.State.Is(ConstitutiveModelData::UPDATE_INTERNAL_VARIABLES))
      {
         VectorType StrainVector;
         ConstitutiveModelUtilities::StrainTensorToVector(Variables.TotalStrainMatrix, StrainVector);
         mStrainVectorFinalized = StrainVector;

         mStressVectorFinalized = StressVector;
         mStateVariablesFinalized(0) = pc;
      }


      KRATOS_CATCH(" ")
   }

   double & SmallStrainCamClayModel::EvaluateYieldSurface( const double & rP, const VectorType & rS, const double & rPc, double & rYield, const double & rM)
   {
      KRATOS_TRY

      double J2sqrt = 0;
      for (unsigned int i = 0; i < 6; i++) {
         double number = 1.0;
         if (i > 2)
            number = 2.0;
         J2sqrt += number * pow( rS(i), 2.0);
      }
      J2sqrt = sqrt( 1.0/2.0*J2sqrt);

      rYield = pow(  sqrt(3.0)*J2sqrt/ rM, 2.0) + pow(rP-rPc, 2.0) - pow( rPc, 2.0);


      return rYield;
      KRATOS_CATCH("")
   }

   double & SmallStrainCamClayModel::ThreeProduct( const VectorType &rV, const Matrix & rM, const VectorType & rW, double & rProduct)
   {
      KRATOS_TRY
      rProduct = 0;

      for (unsigned int i = 0; i < 6; i++) {
         for (unsigned int j = 0; j < 6; j++) {
            rProduct += rV(i)*rM(i,j)*rW(j);
         }
      }

      return rProduct;
      KRATOS_CATCH("")
   }

   //************************************************************************************
   //************************************************************************************
   void SmallStrainCamClayModel::SetConstitutiveMatrix(Matrix &rC, const Matrix &rpCBig, const MatrixType &rStressMatrix)

   {
      KRATOS_TRY

      if (rC.size1() == 6)
      {
         for (unsigned int i = 0; i < 6; i++)
            for (unsigned int j = 0; j < 6; j++)
               rC(i, j) = rpCBig(i, j);
      }
      else if (rC.size1() == 3)
      {
         rC(0, 0) = rpCBig(0, 0);
         rC(0, 1) = rpCBig(0, 1);
         rC(0, 2) = rpCBig(0, 3);
         rC(1, 0) = rpCBig(1, 0);
         rC(1, 1) = rpCBig(1, 1);
         rC(1, 2) = rpCBig(1, 3);
         rC(2, 0) = rpCBig(3, 0);
         rC(2, 1) = rpCBig(3, 1);
         rC(2, 2) = rpCBig(3, 3);
      }
      else
      {
         for (int i = 0; i < 4; i++)
         {
            for (int j = 0; j < 4; j++)
            {
               rC(i, j) = rpCBig(i, j);
            }
         }
      }

      KRATOS_CATCH("")
   }
   int SmallStrainCamClayModel::Check(const Properties &rMaterialProperties, const ProcessInfo &rCurrentProcessInfo)
   {
      KRATOS_TRY

      return 0;

      KRATOS_CATCH(" ")
   }
   //******************************************************************
   // Get Value doubles
   double &SmallStrainCamClayModel::GetValue(const Variable<double> &rVariable, double &rValue)
   {
      KRATOS_TRY

      if (rVariable == STRESS_INV_P)
      {
         double J2;
         StressInvariantsUtilities::CalculateStressInvariants(mStressVectorFinalized, rValue, J2);
      }
      else if (rVariable == STRESS_INV_Q)
      {
         double p;
         StressInvariantsUtilities::CalculateStressInvariants(mStressVectorFinalized, p, rValue);
         rValue = rValue * sqrt(3.0);
      }
      else if (rVariable == STRESS_INV_THETA)
      {
         double p, J2;
         StressInvariantsUtilities::CalculateStressInvariants(mStressVectorFinalized, p, J2, rValue);
      } else if (rVariable == PRE_CONSOLIDATION_STRESS) {
         rValue = mStateVariablesFinalized(0);
      }
      else{
         ConstitutiveModel::GetValue( rVariable, rValue);
      }


      return rValue;
      KRATOS_CATCH("")
   }

   //******************************************************************
   // Get Value doubles
   void SmallStrainCamClayModel::SetValue(const Variable<Vector> &rThisVariable, const Vector &rValue,
                         const ProcessInfo &rCurrentProcessInfo)
   {
      KRATOS_TRY

      if ( rThisVariable == ELASTIC_LEFT_CAUCHY_FROM_KIRCHHOFF_STRESS ){
         mStressVectorFinalized.clear();
         mStressVectorFinalized = rValue;
         mInitializedStress = true;
      } else {
         ConstitutiveModel::SetValue( rThisVariable, rValue, rCurrentProcessInfo);
      }


      KRATOS_CATCH(" ")
   }


} // Namespace Kratos
