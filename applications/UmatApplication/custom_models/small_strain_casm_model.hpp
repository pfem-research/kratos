//
//   Project Name:        KratosUmatApplication        $
//   Developed by:        $Developer:        LMonforte $
//   Maintained by:       $Maintainer:              LM $
//   Date:                $Date:          October 2017 $
//
//

#if !defined(KRATOS_SMALL_STRAIN_CASM_MODEL_HPP_INCLUDED)
#define KRATOS_SMALL_STRAIN_CASM_MODEL_HPP_INCLUDED

// System includes
#include <string>
#include <iostream>

// External includes

// Project includes
#include "custom_models/constitutive_model.hpp"
#include "umat_application_variables.h"

namespace Kratos
{
///@addtogroup ConstitutiveModelsApplication
///@{

///@name Kratos Globals
///@{

///@}
///@name Type Definitions
///@{

///@}
///@name  Enum's
///@{

///@}
///@name  Functions
///@{

///@}
///@name Kratos Classes
///@{

/// Short class definition.
/** Detail class definition.
    */
class KRATOS_API(UMAT_APPLICATION) SmallStrainCasmModel : public ConstitutiveModel
{
protected:
   struct UmatModelData
   {
   private:
      Flags *mpState;
      const ModelDataType *mpModelData;

   public:
      MatrixType IncrementalDeformation;
      MatrixType TotalStrainMatrix;

      //Set Data Pointers
      void SetState(Flags &rState) { mpState = &rState; };
      void SetModelData(const ModelDataType &rModelData) { mpModelData = &rModelData; };

      //Get Data Pointers
      const ModelDataType &GetModelData() const { return *mpModelData; };
      const MaterialDataType &GetMaterialParameters() const { return mpModelData->GetMaterialParameters(); };

      //Get non const Data
      Flags &State() { return *mpState; };

      //Get const Data
      const Flags &GetState() const { return *mpState; };
   };

public:
   ///@name Type Definitions
   ///@{
   typedef UmatModelData UmatDataType;

   /// Pointer definition of SmallStrainCasmModel
   KRATOS_CLASS_POINTER_DEFINITION(SmallStrainCasmModel);

   ///@}
   ///@name Life Cycle
   ///@{

   /// Default constructor.
   SmallStrainCasmModel();

   /// Copy constructor.
   SmallStrainCasmModel(SmallStrainCasmModel const &rOther);

   /// Clone.
   virtual ConstitutiveModel::Pointer Clone() const override;

   /// Assignment operator.
   SmallStrainCasmModel &operator=(SmallStrainCasmModel const &rOther);

   /// Destructor.
   virtual ~SmallStrainCasmModel();

   ///@}
   ///@name Operators
   ///@{

   ///@}
   ///@name Operations
   ///@{

   /**
          * Initialize member data
          */
   virtual void InitializeModel(ModelDataType &rValues) override;

   /**
          * Finalize member data
          */
   virtual void FinalizeModel(ModelDataType &rValues) override;

   /**
          * Calculate Stresses
          */
   virtual void CalculateStressTensor(ModelDataType &rValues, MatrixType &rStressMatrix) override;

   /**
          * Calculate Constitutive Tensor
          */
   virtual void CalculateConstitutiveTensor(ModelDataType &rValues, Matrix &rConstitutiveMatrix) override;

   /**
          * Calculate Stress and Constitutive Tensor
          */
   virtual void CalculateStressAndConstitutiveTensors(ModelDataType &rValues, MatrixType &rStressMatrix, Matrix &rConstitutiveMatrix) override;

   /**
          * Check
          */
   virtual int Check(const Properties &rMaterialProperties, const ProcessInfo &rCurrentProcessInfo) override;

   ///@}
   ///@name Access
   ///@{

   virtual void SetValue(const Variable<Vector> &rThisVariable, const Vector &rValue,
                         const ProcessInfo &rCurrentProcessInfo) override;

   virtual void SetValue(const Variable<Matrix> &rThisVariable, const Matrix &rValue,
                         const ProcessInfo &rCurrentProcessInfo) override
   {
      KRATOS_TRY

      ConstitutiveModel::SetValue( rThisVariable, rValue, rCurrentProcessInfo);

      KRATOS_CATCH(" ")
   }



   ///@}
   ///@name Inquiry
   ///@{

   ///@}
   ///@name Access
   ///@{
   /**
          * method to ask the plasticity model the list of variables (dofs)  needed from the domain
          * @param rScalarVariables : list of scalar dofs
          * @param rComponentVariables :  list of vector dofs
          */
   virtual void GetDomainVariablesList(std::vector<Variable<double>*> &rScalarVariables,
                                       std::vector<Variable<array_1d<double, 3>>*> &rComponentVariables) override
   {
      KRATOS_TRY

      rComponentVariables.push_back(&DISPLACEMENT);

      KRATOS_CATCH(" ")
   }

   double &GetValue(const Variable<double> &, double &rValue) override;

   ///@}
   ///@name Input and output
   ///@{

   /// Turn back information as a string.
   virtual std::string Info() const override
   {
      std::stringstream buffer;
      buffer << "SmallStrainCasmModel";
      return buffer.str();
   }

   /// Print information about this object.
   virtual void PrintInfo(std::ostream &rOStream) const override
   {
      rOStream << "SmallStrainCasmModel";
   }

   /// Print object's data.
   virtual void PrintData(std::ostream &rOStream) const override
   {
      rOStream << "SmallStrainCasmModel Data";
   }

   ///@}
   ///@name Friends
   ///@{

   ///@}

protected:
   ///@name Protected static Member Variables
   ///@{

   ///@}
   ///@name Protected member Variables
   ///@{

   bool mInitializedModel;
   bool mInitializedStress;

   Vector mStateVariablesFinalized;
   VectorType mStressVectorFinalized;
   VectorType mStrainVectorFinalized;

   ///@}
   ///@name Protected Operators
   ///@{

   ///@}
   ///@name Protected Operations
   ///@{

   //************//

   void InitializeElasticData(ModelDataType &rValues, UmatDataType &rVariables);

   virtual void SetConstitutiveMatrix(Matrix &rC, const Matrix &rCBig, const MatrixType &rStressMatrix);

   double & EvaluateYieldSurface( const double & rP, const VectorType & s, const double & rPc0, double & rYield, const Properties & rMaterialProperties);


   Vector & ComputeResidual(Vector & Residual, const double & gamma, const double & p, const double & p0, const double &pc, const double & pc0, const VectorType & s, const VectorType & s0, const double & DeltaStrainVol,const VectorType &  DeltaStrainDev, const Properties & rMaterialProperties, const double & gammaOld, const bool & rImplex);

   Matrix & ComputeJacobian(Matrix & Jacobian, const double & gamma, const double & p, const double & p0, const double &pc, const double & pc0, const VectorType & s, const VectorType & s0, const double & DeltaStrainVol,const VectorType &  DeltaStrainDev, const Properties & rMaterialProperties,
         const double & gammaOld, const bool & rImplex);

   Matrix & ComputedResidualdEpsilon(Matrix & dRdV, const double & gamma, const double & p, const double & p0, const double &pc, const double & pc0, const VectorType & s, const VectorType & s0, const VectorType &  DeltaStrain, const Properties & rMaterialProperties,
         const double & gammaOld, const bool & rImplex);

   ///@}
   ///@name Protected  Access
   ///@{

   ///@}
   ///@name Protected Inquiry
   ///@{

   ///@}
   ///@name Protected LifeCycle
   ///@{

   ///@}

private:
   ///@name Static Member Variables
   ///@{

   ///@}
   ///@name Member Variables
   ///@{

   ///@}
   ///@name Private Operators
   ///@{

   ///@}
   ///@name Private Operations
   ///@{

   ///@}
   ///@name Private  Access
   ///@{

   ///@}
   ///@name Serialization
   ///@{
   friend class Serializer;

   virtual void save(Serializer &rSerializer) const override
   {
      KRATOS_SERIALIZE_SAVE_BASE_CLASS(rSerializer, ConstitutiveModel)
      rSerializer.save("InitializedModel", mInitializedModel);
      rSerializer.save("InitializedStress", mInitializedStress);
      rSerializer.save("StressVectorFinalized", mStressVectorFinalized);
      rSerializer.save("StrainVectorFinalized", mStrainVectorFinalized);
      rSerializer.save("StateVariablesFinalized", mStateVariablesFinalized);
   }

   virtual void load(Serializer &rSerializer) override
   {
      KRATOS_SERIALIZE_LOAD_BASE_CLASS(rSerializer, ConstitutiveModel)
      rSerializer.load("InitializedModel", mInitializedModel);
      rSerializer.load("InitializedStress", mInitializedStress);
      rSerializer.load("StressVectorFinalized", mStressVectorFinalized);
      rSerializer.load("StrainVectorFinalized", mStrainVectorFinalized);
      rSerializer.load("StateVariablesFinalized", mStateVariablesFinalized);
   }

   ///@}
   ///@name Private Inquiry
   ///@{

   ///@}
   ///@name Un accessible methods
   ///@{

   ///@}

}; // Class SmallStrainCasmModel

///@}

///@name Type Definitions
///@{

///@}
///@name Input and output
///@{

///@}

///@} addtogroup block

} // namespace Kratos.

#endif // KRATOS_MODIFIED_CAM_CLAY_MODEL_HPP_INCLUDED  defined
