## This is a platform for the development of the Particle Finite Element Method (PFEM)

<p align=center><img src="https://gitlab.com/pfem-research/kratos/-/wikis/logos/pfem_logo.png" width="150" height="150"> Developments for the modelling of: </p>

* __G-PFEM__ : The Particle Finite Element Method for __Geotechnical__ applications.
* __Manufacturing__ : forming processes (Forging) or cutting processes (Machining).

##  The developments are implemented in the applications:

- __Pfem__ application (fluids and solids)
- __Solid_Mechanics__ application (finite elements for solids and structures)
- __Constitutive_Models__ application (constitutive models)
- __Contact_Mechanics__ application (contact algorithms)
- __Solvers__ application (strategies for solving)
- __Meshers__ application (mesh generation interfases for triangle and tetgen)
- __Umat__ application (interface to UMAT fortran constitutive laws)
- __Constitutive_Legacy__ application (legacy implementacion of constitutive laws)

These applications are free and have a BSD license. The derived libraries used can have their own proprietary license.

Other applications related with the PFEM are not under development in this repository.

##  Integrated in the Kratos framework:

<p align=center><img height="36.100%" width="36.100%" src="https://raw.githubusercontent.com/KratosMultiphysics/Documentation/master/Wiki_files/Home/kratos.png"></p>

_KRATOS Multiphysics_ ("Kratos") is a framework for building parallel, multi-disciplinary simulation software, aiming at modularity, extensibility, and high performance. Kratos is written in C++, and counts with an extensive Python interface. More in [Overview](https://gitlab.com/pfem-research/kratos/-/wikis/Overview)

Kratos is __multiplatform__ and available for __Windows, Linux__ (several distros) and __macOS__, for  __OpenMP__ and __MPI__ parallelization.

Kratos is free under BSD-4 [license](https://gitlab.com/pfem-research/kratos/blob/develop/license.txt) and can be used even in comercial softwares as it is. Many of its main applications are also free and BSD-4 or BSD-3 licensed but each derived application can have its own proprietary license.

Kratos provides a core which defines the common framework and several applications which work like plug-ins that can be extended in diverse fields.

#### Contributors and Acknowledgments

[CIMNE](http://www.cimne.com) - [TUM](https://www.st.bgu.tum.de/) - [Boost](http://www.boost.org/) - [pybind11](https://github.com/pybind/pybind11) - [GidPost](https://www.gidhome.com/gid-plus/tools/476/gidpost/) - [AMGCL](https://github.com/ddemidov/amgcl) - [JSON](https://github.com/nlohmann/json) - [ZLib](https://zlib.net/) - [Trilinos](https://trilinos.org/) - [METIS](http://glaros.dtc.umn.edu/gkhome/views/metis)


## If you are already synchronized with the Kratos GitHub repository:

You can add this fork as another remote called "Pfem_GitLab":

```
$ git remote add Pfem_GitLab https://gitlab.com/pfem-research/kratos.git
```

#### * Developments done in this repository must not be merged into the Kratos GitHub repository
